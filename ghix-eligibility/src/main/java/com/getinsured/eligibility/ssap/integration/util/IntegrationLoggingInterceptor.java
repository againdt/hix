package com.getinsured.eligibility.ssap.integration.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.stereotype.Component;

@Component(value = "integrationLoggingInterceptor")
public class IntegrationLoggingInterceptor extends ChannelInterceptorAdapter  {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationLoggingInterceptor.class);

	@Override
	public void postSend(Message<?> message, MessageChannel channel, boolean sent) {
		if (LOGGER.isDebugEnabled()){
			LOGGER.debug("Post Send - Channel " + channel.getClass());
			LOGGER.debug("Post Send - Headers: " + message.getHeaders() + " Payload: " + message.getPayload() + " Message sent?: " + sent);
		}
	}

	@Override
	public Message<?> postReceive(Message<?> message, MessageChannel channel) {
		if (LOGGER.isDebugEnabled()){
			try {
				LOGGER.debug("Post Receive - Channel " + channel.getClass());
				LOGGER.debug("Post Receive - Headers: " + message.getHeaders() + " Payload: " + message.getPayload());
			} catch(Exception ex) {
				LOGGER.error("Error in post receive : ", ex);
			}
		}
		return message;
	}

}

