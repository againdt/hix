package com.getinsured.hix.batch.agency.service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.google.gson.Gson;
import com.opencsv.CSVReader;

@Service("agencyMigrationService")
public class AgencyMigrationServiceImpl implements AgencyMigrationService {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(AgencyMigrationServiceImpl.class);
	
	@Autowired
	AgencyRecordProcessor agencyRecordProcessor;
	@Autowired
	GIMonitorService giMonitorService;
	@Autowired
	Gson platformGson;
	@Autowired
	PlatformTransactionManager platformTransactionManager;
		
	@Override
	public void process(String sourceFilePath, String delimeter, int agencyColumnNo, int agencyRowNo) throws Exception {
		File sourceFile = null;
		FileReader sourceFileReader = null;
		CSVReader csvReader = null;
		int agencyColumnIndex = agencyColumnNo-1; 
		List<Map<String,String>> jsonErrorData = new LinkedList<Map<String,String>>();
		try {
			sourceFile = new File(sourceFilePath);
			if(!sourceFile.exists()){
				throw new Exception("File does not exist : "+sourceFilePath);
			}
			if(!sourceFile.canRead()){
				throw new Exception("No Read permission for : "+sourceFilePath);
			}
			sourceFileReader = new FileReader(sourceFile);
			csvReader = new CSVReader(sourceFileReader, delimeter.toCharArray()[0]);
			String[] columns;
			while ((columns = csvReader.readNext()) != null) {
				if (csvReader.getLinesRead() >= agencyRowNo) {
					if(isEmptyLine(columns)) {
						continue;
					}
					DefaultTransactionDefinition transactionDefinition = null;
					TransactionStatus transactionStatus = null;
					try{
						transactionDefinition = new DefaultTransactionDefinition();
						transactionStatus = platformTransactionManager.getTransaction(transactionDefinition);
						agencyRecordProcessor.process(columns,agencyColumnIndex,csvReader.getLinesRead());
						platformTransactionManager.commit(transactionStatus);
					}catch(Exception ex){
						if(transactionStatus != null){
							platformTransactionManager.rollback(transactionStatus);
						}
						LOGGER.error("Exception occured while processing record :"+csvReader.getLinesRead(), ex);
						buildLoggingData(logExceptionIntoGiMonitor(ExceptionUtils.getFullStackTrace(ex),csvReader.getLinesRead()+","+getAgentId(columns)), ex,jsonErrorData,csvReader.getLinesRead(),getAgentId(columns));
					}
				}
			}
			
			//build erro log json 
			if(!jsonErrorData.isEmpty()){
			 String json = platformGson.toJson(jsonErrorData);
			 logExceptionIntoGiMonitor(json, "AGENCY_MANAGER_MIGRATION_BATCH");
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while processing agency migration", ex);
			throw ex;
		} finally {
			if (csvReader != null) {
				try {
					csvReader.close();
				} catch (IOException e) {
					LOGGER.error("Exception occured while closing csv reader", e);
				}
			}
		}

	}

	private boolean isEmptyLine(String[] columns) {
		boolean isEmptyLine =  true;
		for(String value : columns) {
			if(!StringUtils.isBlank(value)) {
				isEmptyLine = false;
				break;
			}
		}
		return isEmptyLine;
	}

	private String getAgentId(String[] columns) {
		String agentId = "";
		if(columns != null && columns.length>1){
			agentId =columns[0];
		}
		return agentId;
	}

	
	private void buildLoggingData(Integer monitorId,Exception ex,List<Map<String,String>> jsonErrorData,Long rowNo,String agentId) {
		try{
			AgencyMigrationException agencyMigrationException = null;
			Map<String,String> jsonData = null;
			jsonData = new HashMap<String, String>();
			jsonData.put("error", ex.getMessage());
			jsonData.put("monitorId", String.valueOf(monitorId));
			if(ex instanceof AgencyMigrationException){
				agencyMigrationException = (AgencyMigrationException) ex;
				jsonData.put("rowNo",String.valueOf(agencyMigrationException.getRowNo()));
				jsonData.put("agencyId", String.valueOf(agencyMigrationException.getAgencyId()));
				jsonData.put("agentId",agencyMigrationException.getAgentId());
			}else{
				jsonData.put("agentId",agentId);
				jsonData.put("rowNo",String.valueOf(rowNo));
			}
			
			jsonErrorData.add(jsonData);
		}catch (Exception e) {
			LOGGER.error("Exception occured while saving into giMonitor:", e);
		}
		
	}
	
	

	
	private Integer logExceptionIntoGiMonitor(String exception,String url) {
		GIMonitor giMonitor = null;
		try{
			giMonitor = giMonitorService.saveOrUpdateErrorLog(GIRuntimeException.ERROR_CODE_UNKNOWN, new Date(), this.getClass().getName(),exception,
					 null, url, GIRuntimeException.Component.BATCH.getComponent(),null);
		}catch(Exception e){
			LOGGER.error("Exception occured while saving into giMonitor:", e);
		}
		if(giMonitor != null){
			return giMonitor.getId();
		}
		
		return null;
	}

	

}
