<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
#sidebar h4, #sidebar .header, .gray h4, .header{
	max-height: 10%;
}
h4 small.headerSmall{
	font-size:12px;
	color: #666;
}

.grayBgTable{
	background-color:#F8F8F8;
}

.page-breadcrumb{
	display:inline;
}

h4.rule, .span9 .rule{
	border-bottom:1px solid #999;
	padding-bottom:14px;
}

.top30{
	margin-top:30px;
}
</style>
</head>
<body>
	<div class="gutter10">
	<div class="row-fluid">
    	
		<h1 class="gutter10"><a name="skip"></a>Agent FAQs</h1>
 		<small class="gutter10"><a href="<c:url value='/faqhome'/>" class="pull-right">FAQ Home</a></small>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    	<div class="header">
			<h4 class="margin0">&nbsp;</h4>
		</div>
		<df:csrfToken/>
		<ul class="nav nav-list faq-nav" id="nav">
								<li><a href="#registerAsAgent"><spring:message code="label.agentLearn.HowToApply" htmlEscape="false"/></a></li>
								<li><a href="#manageYourProfile"><spring:message code="label.agentLearn.Profile" htmlEscape="false"/></a></li>
								<li><a href="#manageIndAccts"><spring:message code="label.agentLearn.IndividualAccounts" htmlEscape="false"/></a></li>
								<li><a href="#manageExistingAccts"><spring:message code="label.agentLearn.ExistingAccounts" htmlEscape="false"/></a></li>

		</ul>
	</div>
    	
       			<div class="span9 " id="rightpanel">
       			
       				<div id="registerAsAgent">
       				<div class="header">
       					<h4><spring:message code="label.agentLearn.HowToApply" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
       				</div>
       				<!--------------------------- question 1 ------------------------>
					
					<p><spring:message code="label.agentCreateAccount" htmlEscape="false"/></p>
				<ul>
					<li><spring:message code="label.agentCreateAccountEmail" htmlEscape="false"/></li>	
					<li><spring:message code="label.agentCreateAccountLicense" htmlEscape="false" /></li>
				</ul>
				<%-- <p><spring:message code="label.agentCreateAccountLicense" htmlEscape="false"/></p> --%>
				
				<div class="lightgrey">
		       	 <h4><spring:message code="label.agentMarketplace.Apply" htmlEscape="false"/></h4>
		        </div>
		            <ol>
		              <li><spring:message code="label.agentMarketplace.Navigate" htmlEscape="false"/></li>
		              <li><spring:message code="label.agentMarketplace.CreateAcct" htmlEscape="false"/></li>
		              <%-- <li><spring:message code="label.agentMarketplace.CertifiedCounselor" htmlEscape="false"/></li> --%>
		              <li><spring:message code="label.agentMarketplace.CompleteCreation" htmlEscape="false"/></li>
		              <li><spring:message code="label.agentMarketplace.AgentInfo" htmlEscape="false"/></li>
		              <li><spring:message code="label.agentMarketplace.BuildProfile" htmlEscape="false"/>
		                  <ul>
		                      <li><spring:message code="label.agentMarketplace.ClientsServed" htmlEscape="false"/></li>
		                      <li><spring:message code="label.agentMarketplace.Languages" htmlEscape="false"/></li>
		                      <li><spring:message code="label.agentMarketplace.ProductExpertise" htmlEscape="false"/></li>
		                      <%-- <li><spring:message code="label.agentMarketplace.Education" htmlEscape="false"/></li> --%>
		                      <li><spring:message code="label.agentMarketplace.AboutYourself" htmlEscape="false"/></li>
		                      <li><spring:message code="label.agentMarketplace.UploadPhoto" htmlEscape="false"/></li>
		                  </ul>
		              </li>
		             <%--  <li><spring:message code="label.agentMarketplace.PaymentInfo" /></li> --%>
		              <li><spring:message code="label.agentMarketplace.Finish" htmlEscape="false"/></li>
		              <li><spring:message code="label.agentMarketplace.Review" htmlEscape="false"/>
		              	<ol>
		              		<li><spring:message code="label.agentMarketplace.ReviewEdit" htmlEscape="false"/></li>
		              		<li><spring:message code="label.agentMarketplace.ReviewSave" htmlEscape="false"/></li>
		              	</ol>
		              </li>
		            </ol>   
		            <p><spring:message code="label.agentMarketplace.Notification" htmlEscape="false"/></p>
						</div>						
						
						<!--------------------------- question 2 ------------------------>
						<div  id="manageYourProfile">
						<div class="header">
							<h4><spring:message code="label.agentLearn.Profile" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
							
						</div>
						
						<p><spring:message code="label.agentManageProfileMessage" htmlEscape="false"/></p>    
						
						<div class="lightgrey">
					        <h4 id="create"><spring:message code="label.agentManage.Create" htmlEscape="false"/></h4>
					       </div>
					        <p><spring:message code="label.agentManage.CreateDetails" htmlEscape="false"/></p>
					        
					       <div class="lightgrey"> 
					       <h4 id="viewProfile"><spring:message code="label.agentManage.ViewProfile" htmlEscape="false"/></h4>
					       </div>
					        <p><spring:message code="label.agentManage.ViewProfileDetails" htmlEscape="false"/></p>
					        
					        <ol>
					          <li><spring:message code="label.agentManage.ViewProfileDetails1" htmlEscape="false"/></li>   
					          <li><spring:message code="label.agentManage.ViewProfileDetails2" htmlEscape="false"/></li>
					          <li><spring:message code="label.agentManage.ViewProfileDetails3" htmlEscape="false"/></li>   
					        </ol>
					        
					        <div class="lightgrey">
					       		 <h4 id="editProfile"><spring:message code="label.agentManage.EditProfile" htmlEscape="false"/></h4>
					        </div>
					        <p><spring:message code="label.agentManage.EditProfileDetails" htmlEscape="false"/></p>
					        
					        <ol>
					            <li><spring:message code="label.agentManage.EditProfileDetails1" htmlEscape="false"/></li>
					            <li><spring:message code="label.agentManage.EditProfileDetails2" htmlEscape="false"/></li>
					            <li><spring:message code="label.agentManage.EditProfileDetails3" htmlEscape="false"/></li>
					            <li><spring:message code="label.agentManage.EditProfileDetails4" htmlEscape="false"/></li>
					            <li><spring:message code="label.agentManage.EditProfileDetails5" htmlEscape="false"/></li>
					        </ol>
					        
					        <div class="lightgrey">
					        	<h4 id="renewCert"><spring:message code="label.agentManage.RenewCert" htmlEscape="false"/></h4>
					        </div>
					        <p><spring:message code="label.agentManage.RenewCertDetails" htmlEscape="false"/></p>
						        
						        <div class="lightgrey">
						        	<h4 id="deActivateProfile"><spring:message code="label.agentManage.Deactivate" htmlEscape="false"/></h4>
						        </div>
						        <p><spring:message code="label.agentManage.DeactivateDetails" htmlEscape="false"/></p>
					        </div>

		
		
								
						<!--------------------------- question 3 ------------------------>		
						<div id="manageIndAccts">
							<div class="header">
								<h4 class=""><spring:message code="label.agentLearn.IndividualAccounts" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
								
							</div>
          
					         <P> <spring:message code="label.agentManageIndAccts.Details" htmlEscape="false"/></P>
					          <div class="lightgrey">
					       		   <h4 id="viewIndList"><spring:message code="label.agentManageIndAccts.ViewIndividual" htmlEscape="false"/></h4>
					          </div>
					          <p><spring:message code="label.agentManageIndAccts.ViewIndividualDetails" htmlEscape="false"/></p>
					            <ol>
					              <li><spring:message code="label.agentManageIndAccts.ViewIndividualDetails1" htmlEscape="false"/></li>
					              <li><spring:message code="label.agentManageIndAccts.ViewIndividualDetails2" htmlEscape="false"/></li>
					              <li><spring:message code="label.agentManageIndAccts.ViewIndividualDetails3" htmlEscape="false"/></li>
					              <li><spring:message code="label.agentManageIndAccts.ViewIndividualDetails4" htmlEscape="false"/></li>
					            </ol>
					            
					            <div class="lightgrey">
					  		        <h4 id="findInd"><spring:message code="label.agentManageIndAccts.FindIndividual" htmlEscape="false"/></h4>
					  		     </div>
					          <p><spring:message code="label.agentManageIndAccts.FindIndividualDetails" htmlEscape="false"/></p>
					            <ol>
					              <li><spring:message code="label.agentManageIndAccts.FindIndividualDetails1" htmlEscape="false"/></li>
					              <li><spring:message code="label.agentManageIndAccts.FindIndividualDetails2" htmlEscape="false"/></li>
					              <li><spring:message code="label.agentManageIndAccts.FindIndividualDetails3" htmlEscape="false"/></li>
					              <li><spring:message code="label.agentManageIndAccts.FindIndividualDetails4" htmlEscape="false"/></li>
					            </ol>
					            
					           <div class="lightgrey"> 
					          		<h4 id="changeIndStatus"><spring:message code="label.agentManageIndAccts.ChangeIndStatus" htmlEscape="false"/></h4>
					          </div>
					          <p><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetailsA" htmlEscape="false"/></p>
					          <p><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetailsB" htmlEscape="false"/></p>
					            <ol>
					              <li><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails1" htmlEscape="false"/></li>
					              <li><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails2" htmlEscape="false"/><br>
					              <em><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails2a" htmlEscape="false"/></em></li>
					              <li><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails3" htmlEscape="false"/>
					              	<p><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails3a" htmlEscape="false"/></p>
					              </li>
					            </ol>
					            
					           <div class="lightgrey"> 
					          		<h4 id="viewIndIncome"><spring:message code="label.agentManageIndAccts.ViewSummary" htmlEscape="false"/></h4>
					          </div>
					          <p><spring:message code="label.agentManageIndAccts.ViewSummaryDetails" htmlEscape="false"/></p>
					            <ol>
					              <li><spring:message code="label.agentManageIndAccts.ViewSummaryDetails1" htmlEscape="false"/></li>
					              <li><spring:message code="label.agentManageIndAccts.ViewSummaryDetails2" htmlEscape="false"/></li>
					             <li> <spring:message code="label.agentManageIndAccts.ViewSummaryDetails3" htmlEscape="false"/><br>
					              <em><spring:message code="label.agentManageIndAccts.ViewSummaryDetails3a" htmlEscape="false"/></em></li>
					            </ol>
					            
					            
					          <div class="lightgrey">
					          		<h4 id="editIndComments"><spring:message code="label.agentManageIndAccts.EditComments" htmlEscape="false"/></h4>
					          </div>
					          <p><spring:message code="label.agentManageIndAccts.EditCommentsDetails" htmlEscape="false"/></p>
					              <ol>
					                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails1" htmlEscape="false"/></li>
					                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails2" htmlEscape="false"/><em>
					                <spring:message code="label.agentManageIndAccts.EditCommentsDetails2a" htmlEscape="false"/></em></li>
					                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails3" htmlEscape="false"/></li>
					                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails4" htmlEscape="false"/>
					                    <ol>
					                      <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails4a" htmlEscape="false"/></li>
					                      <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails4b" htmlEscape="false"/></li>
					                      <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails4c" htmlEscape="false"/></li>
					                    </ol>
					                </li>
					                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails5" htmlEscape="false"/></li>
					                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails6" htmlEscape="false"/>
					                    <ol>
					                      <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails6a" htmlEscape="false"/></li>
					                      <li> <spring:message code="label.agentManageIndAccts.EditCommentsDetails6b" htmlEscape="false"/></li>
					                       <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails6c" htmlEscape="false"/></li>
					                       <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails6d" htmlEscape="false"/></li>
					                    </ol>
					                </li>
					                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails7" htmlEscape="false"/>
					                    <ol>
					                        <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails7a" htmlEscape="false"/></li>
					                        <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails7b" htmlEscape="false"/></li>
					                        <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails7c" htmlEscape="false"/></li>
					                    </ol>
					                </li>
					              </ol>
					              <div class="lightgrey">
					      			    <h4 id="actAsInd"> <spring:message code="label.agentManageIndAccts.ActIndividual" htmlEscape="false"/></h4>
					      		</div>
					          <p><spring:message code="label.agentManageIndAccts.ActIndividualDetailsA" htmlEscape="false"/></p>
					
					          <p><spring:message code="label.agentManageIndAccts.ActIndividualDetailsB" htmlEscape="false"/></p>
					        
					              <ol>
					
					                  <li><spring:message code="label.agentManageIndAccts.ActIndividualDetails1" htmlEscape="false"/></li>
					                  <li><spring:message code="label.agentManageIndAccts.ActIndividualDetails2" htmlEscape="false"/><br>
					                  <em><spring:message code="label.agentManageIndAccts.ActIndividualDetails2a" htmlEscape="false"/></em></li>
					                  <li><spring:message code="label.agentManageIndAccts.ActIndividualDetails3" htmlEscape="false"/></li>
					                  <li><spring:message code="label.agentManageIndAccts.ActIndividualDetails4" htmlEscape="false"/></li>
					          
					             </ol>

						</div>
						
						<!--------------------------- question 4 ------------------------>
						<div class="header">
							<h4 id="manageExistingAccts"><spring:message code="label.agentLearn.ExistingAccounts" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>	
							
						</div>							
						<div class="entityFaq5 ">

				            <div class="lightgrey">
				           		 <h4 id="addExistingClient"><spring:message code="label.agentManageExistingAccts.AddExistingClient" htmlEscape="false"/></h4>
				            </div>
				           <p><spring:message code="label.agentManageExistingAccts.AddExistingClientDetails" htmlEscape="false"/></p>
				            
				            <div class="lightgrey">
				           		 <h4 id="setupNewClient"><spring:message code="label.agentManageExistingAccts.SetupNewClient" htmlEscape="false"/></h4>
				            </div>
				            <p><spring:message code="label.agentManageExistingAccts.SetupNewClientDetails" htmlEscape="false"/></p>
				            
				            <div class="lightgrey">
				           		 <h4 id="setupNewClient"><spring:message code="label.agentManageExistingAccts.NewClientNotification" htmlEscape="false"/></h4>
				            </div>
				            <p><spring:message code="label.agentManageExistingAccts.NewClientNotificationDetails" htmlEscape="false"/></p>
				            
				            <div class="lightgrey">
				           		 <h4 id="newClientNoteToDo"><spring:message code="label.agentManageExistingAccts.NewNotificationToDo" htmlEscape="false"/></h4>
				            </div>
				            <p><spring:message code="label.agentManageExistingAccts.NewNotificationToDoDetails" htmlEscape="false"/></p>
				            
				            <div class="lightgrey">
				           		 <h4 id="editClientInfo"><spring:message code="label.agentManageExistingAccts.EditClientInfo" htmlEscape="false"/></h4>
				            </div>
				            <p><spring:message code="label.agentManageExistingAccts.EditClientInfoDetails" htmlEscape="false"/></p>
										</div>										

																					
						 <p class="pull-right"><a id="role-top" href="#">Back to Top</a></p>
						
						
				</div>
		</div>		
		</div>	
		

<script>
$(document).ready(function(){
	document.title="Help for YHI";
});

$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});

</script>			
</body>
</html>