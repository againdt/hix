package com.getinsured.eligibility.at.ref.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * @author chopra_s
 * 
 */
@Component("lceDemoDobHandlerService")
@Scope("singleton")
public class LceDemoDobHandlerService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceDemoDobHandlerService.class);

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceDemoDobHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		createSepEvent(currentApplication);
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);//HIX-108047
		super.closePreviousERApplication(currentApplication,lceProcessRequestDTO);
		updateAllowEnrollment(currentApplication, Y);
		/**
		 * Not to trigger notice EE057 : Change action Required for DOB correction for MN
		 * if not MN then this notice will be triggered
		 */
		String stateCode = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if (!stateCode.equalsIgnoreCase("MN")) {
			triggerChangeActionEmail(currentApplication,lceProcessRequestDTO,false);	
		}	
		LOGGER.info("LceDemoDobHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	private void createSepEvent(SsapApplication currentApplication) {
		LOGGER.info("Create Dob SEP event - " + currentApplication.getId());
		ssapApplicationEventService.createDoBApplicationEvent(currentApplication.getId());
	}
}
