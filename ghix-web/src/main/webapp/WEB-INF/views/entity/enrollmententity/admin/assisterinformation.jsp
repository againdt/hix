<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<link href="<gi:cdnurl value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/phoneUtils.js" />"></script>

<script type="text/javascript">
	function highlightThis(val){
		
		var currUrl = window.location.href;
		var newUrl="";
		/* Check if Query String parameter is set or not */
		if(currUrl.indexOf("?", 0) > 0) { /* If Yes */
			if(currUrl.indexOf("?lang=", 0) > 0) {/* Check if locale is already set without querystring param  */
				 newUrl = "?lang="+val;
			} else if(currUrl.indexOf("&lang=", 0) > 0) { /*  Check if locale is already set with querystring param  */
				newUrl = currUrl.substring(0, currUrl.length-2)+val; 
			} else { 
				newUrl = currUrl + "&lang="+val;
			}
		} else { /* If No  */
			newUrl = currUrl + "?lang="+val;
		}
		window.location = newUrl;
	}
</script>
	
				<!-- end of secondary navbar -->
				<div class="gutter10-lr">
					<%-- <div class="l-page-breadcrumb">
						<!--start page-breadcrumb -->
						<div class="row-fluid">
								<li><a href="#">Assisters</a></li>
								<li>Manage</li>
								<li>${assister.user.firstName}&nbsp;${assister.user.lastName}</li>
							</ul>
							<!--page-breadcrumb ends-->
						</div><!-- end of .row-fluid -->
					</div><!--l-page-breadcrumb ends--> --%>
					
					
					<div class="row-fluid">
						<h1><a name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
					</div>
					<div class="row-fluid">
						<div id="sidebar" class="span3">
						<c:set var="assisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
						<c:set var="assisterEntityId" ><encryptor:enc value="${assister.entity.id}" isurl="true"/> </c:set>
							<div class="header"></div>
							<ul class="nav nav-list">
								<li class="active"><a href="#"><spring:message code="label.assister.certifiedCounselor"/></a></li>
								<li><a href="/hix/entity/entityadmin/assisterprofile?assisterId=${assisterId}"><spring:message code="label.assister.profile"/></a></li>
								<c:choose>	
									<c:when test="${loggedUser=='entityAdmin' }">
										<li><a href="/hix/entity/assisteradmin/certificationstatus?assisterId=${assisterId}&entityId=${assisterEntityId}">Certification Status</a></li>
									</c:when>
									</c:choose>
								<li><a href="/hix/entity/entityadmin/assisterstatus?assisterId=${assisterId}&entityId=${assisterEntityId}"><spring:message code="label.assister.status"/></a></li>
							</ul>
						</div><!-- end of span3 -->
						<div class="span9">
							<div class="header">
								<h4 class="pull-left"><spring:message code="label.assister.enrollmentCounselorInformation"/></h4>
								<c:if test = "${CA_STATE_CODE==false}" >
								<a class="btn btn-small pull-right" href="<c:url value="/entity/assister/editassisterinformation?assisterId=${assisterId}&entityId=${assisterEntityId}"/>"><spring:message  code="label.edit"/></a>
								</c:if>
								
							</div>
							<form class="form-horizontal" id="frmviewCertification" name="frmviewCertification" action="viewcertificationinformation" method="POST">
							<df:csrfToken/>
							<input type="hidden" id="entityId" name="entityId" value="<encryptor:enc value="${assister.entity.id}"/>">
								<div class="gutter10">
									<table class="table table-border-none table-condensed">
									
										<tbody>
											<tr>
												<th class="span4 txt-right" scope="row"><spring:message code="label.assister.Firstname"/></th>
												<td><strong>${assister.firstName}</strong></td>
											</tr>
											<tr>
												<th class="span4 txt-right" scope="row"><spring:message code="label.assister.Lastname"/></th>
												<td><strong>${assister.lastName}</strong></td>
											</tr>
												<tr>
												<td class="span4 txt-right"><spring:message code="label.assister.emailAddress"/></td>
												<td><strong>${assister.emailAddress}</strong></td>
											</tr>
											
											<tr>
												<th class="span4 txt-right" scope="row"><spring:message code="label.assister.phoneNumber"/></th>
												<td><strong>
													<c:if test="${assister.primaryPhoneNumber!=null}">
														(${fn:substring(assister.primaryPhoneNumber,0,3)})${fn:substring(assister.primaryPhoneNumber,3,6)}-${fn:substring(assister.primaryPhoneNumber,6,10)} 
													</c:if></strong>
												</td>
											</tr>
										
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.secondaryPhoneNumber"/>
</th>
									<td><strong>
										<c:if test="${assister.secondaryPhoneNumber!=null && not empty assister.secondaryPhoneNumber}"> 
											(${fn:substring(assister.secondaryPhoneNumber,0,3)})${fn:substring(assister.secondaryPhoneNumber,3,6)}-${fn:substring(assister.secondaryPhoneNumber,6,10)} 										
										</c:if>
									</strong></td>
								</tr>
	
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.preferredMethodofCommunication"/></th>
									<td><strong>${assister.communicationPreference}</strong></td>
								</tr>
								
								
								<c:choose>
						 						<c:when test="${ showPostalMailOption == 'true'  }">
						 								<tr>
															<td class="span4 txt-right" scope="row"><spring:message code="label.assister.receiveNotices" /> </td> 
															<td>
																<strong>
																	<c:if test="${assister.postalMail!=null && assister.postalMail == 'Y' }" > <spring:message code="label.assister.Yes" /></c:if>
																	<c:if test="${assister.postalMail == null || assister.postalMail == 'N' ||  assister.postalMail == 'n'}" > <spring:message code="label.assister.No" /> </c:if>
																</strong>
															</td>
														</tr>
						 						</c:when>
						 		</c:choose>
								
								
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.isThisAssisterCertified?"/></th>
									<td>
										<strong> 
											<c:if test="${assister.certificationStatus!='Certified'}">
												<spring:message code="label.assister.No"/>
											</c:if>
											<c:if test="${assister.certificationStatus=='Certified'}">
												<spring:message code="label.assister.Yes"/>
											</c:if>
										</strong>
									</td>
								</tr>
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.assister.assisterCertification"/> #</th>
										<td><strong><fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/></strong></td>
									</tr>
								<c:if test="${assister.primaryAssisterSite!=null}">
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.assister.primaryEnrollmentCounselorSite"/></th>
										<td><strong>${assister.primaryAssisterSite.siteLocationName}</strong></td>
									</tr>
								</c:if>	
								<c:if test="${assister.secondaryAssisterSite!=null}">
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.assister.secondaryEnrollmentCounselorSite"/></th>
										<td><strong>${assister.secondaryAssisterSite.siteLocationName}</strong></td>
									</tr>
								</c:if>
										</tbody>
									</table>
								</div>
					<div class="gutter10">
					<div class="header">
						<h4><spring:message code="label.assister.mailingAddress"/></h4>
					</div>
						<table class="table table-border-none table-condensed">
							<tbody>
								<tr>
									<td class="span4 txt-right"><spring:message code="label.assister.streetAddress"/></td>
									<td>${assister.mailingLocation.address1}</td>
								</tr>
								
								<c:if test="${assister.mailingLocation.address2!=null}">
									<tr>
										<td class="txt-right"><spring:message code="label.assister.suite"/></td>
										<td>${assister.mailingLocation.address2}</td>
									</tr>
								</c:if>
								
								<tr>
									<td class="span4 txt-right"><spring:message code="label.assister.city"/></td>
									<td>${assister.mailingLocation.city}</td>
								</tr>
								
								<tr>
									<td class="span4 txt-right"><spring:message code="label.assister.state"/></td>
									<td>${assister.mailingLocation.state}</td>
								</tr>
	
								<tr>
									<td class="span4 txt-right"><spring:message code="label.assister.zipCode"/></td>
									<td>${assister.mailingLocation.zip}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="gutter10">
					<div class="header">
						<h4><spring:message code="label.assister.profileInformation"/></h4>
					</div>
					
					<table class="table table-border-none table-condensed">
					<tr>
							<td class="span4 txt-right"><spring:message code="label.assister.spokenLanguagesSupport"/></td>
							<td class="comma"><strong>${assisterLanguages.spokenLanguages}</strong></td>
						</tr>
						<tr>
							<td class="span4 txt-right"><spring:message code="label.assister.writtenLanguagesSupport"/></td>
							<td class="comma"><strong>${assisterLanguages.writtenLanguages}</strong></td>
						</tr>
						<tr>
							<td class="span4 txt-right"><spring:message code="label.assister.education"/></td>
							<td class="comma"><strong>${assister.education}</strong></td>
						</tr>
						
							<tr><td class="span4 txt-right"><spring:message code="label.assister.photo"/></td>
							<td><spring:url value="/entity/assisteradmin/photo/${assisterId}"
											var="photoUrl" /> <img src="${photoUrl}" alt="Profile image of Counselor"
										class="profilephoto" />
									</td>
							</tr>
					</table>
					<input type="hidden" name="assisterId" id="assisterId" value="<encryptor:enc value="${assister.id}"/>"/>
					</div>
					</form>
				</div> 
			</div>
		</div>
		<script type="text/javascript">
			/*for comma seperated value*/
			$(".comma strong").text(function(i, val) {
			    return val.replace(/,/g, ", ");
			});
			
if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,194,new Date().getTime(),"","","","",""]);
		

		
		
	</script>
		