/*template XHTML for address worksite*/
var zipIdFocused="";
var application = {
		/*init function on click of add worksite link present on the JSP page*/
		init: function(){
			var empid = $('#employerId').val();
			/*call the related function on MODAL SHOW*/
			application.input_content_screen_reset(this);
			application.add_Address_function();
			application.populate_state_dropdown();
			
			/*set the EMPLOYEE ID value*/
			$('#id').val(empid);
		},
		/*remove the modal*/
		remove_modal:function(){
			if($('#editdependent').is(':visible')){
				application.resetCountyOnAllCallback();
			}else if($('#frmworksites').is(':visible') || $('#frmeditcompanyinfo').is(':visible') || $('#frmeditemployee').is(':visible') || $('#frmverifydependents').is(':visible') || $('#assisterTable').is(':visible')){
				$('#modal, .modal-backdrop').remove();
				$('#check-address-error, #address-failure-box, #suggestion-box, #addressProgressPopup').hide();

				application.resetCountyOnAllCallback();
			}else if($('#edit-mydependents').is(':visible')){
				$('#modal, .modal-backdrop, #addressProgressPopup').remove();
				$('#suggestion-box').hide();
			}else if($('#frmhomeaddress').is(':visible')){
				/*edit dependent page*/
				$('.modal-backdrop').remove();
				$('#worksite').modal('hide');
			}else if($('.entityAddressValidation').is(':visible')){
				$('#modal, .modal-backdrop').remove();
				$('#check-address-error, #address-failure-box, #suggestion-box, #addressProgressPopup').hide();
			}else{
				//console.log('remove modal else');
				$('#addressProgressPopup, .modal-backdrop').remove();
			}
		},
		/*reset county on all the callback from validAddressNew function*/
		resetCountyOnAllCallback : function(){
			var zipIdTarget = zipIdFocused;

			//console.log(zipIdTarget,'get zipIdFocused value remove modal');
			
			var	formId = $(parent.document.forms).attr('id'),
				parentElement = jQuery('#'+zipIdTarget).parents('.addressBlock').find('.zipCode'),
				zipCodeVal = parentElement.val(),
				zipId = parentElement.attr('id'),
				indexValue = zipId.split('_');
			
			//console.log(zipId, indexValue, zipCodeVal)
			application.populateCounty(zipId, indexValue, zipCodeVal);
		},
		resetCountyOnAllCallbackWithTwoArgs : function(){
			var zipIdTarget = zipIdFocused;

			/*console.log(zipIdTarget,'670 Gail Ave get zipIdFocused value remove modal');*/
			
			var	formId = $(parent.document.forms).attr('id'),
				parentElement = jQuery('#'+zipIdTarget).parents('.addressBlock').find('.zipCode'),
				zipCodeVal = parentElement.val(),
				zipId = parentElement.attr('id'),
				indexValue = zipId.split('_');
			
			//console.log(zipCodeVal, zipId)
			parent.postPopulateIndex(zipCodeVal, zipId);
		},
		setValueInFormForSplEnroll:function(){
			$('#suggestion-box').hide();
			$('#form-input-data').show();
		},
		/*return back to the input FORM div after after error from the SAME PAGE*/
		back_button_to_input_screen : function(){			
			application.modal_cross_button();
			$('#back_to_input_modal').bind('click',function(){
				//console.log('11111111 back_to_input_modal BIND')
				if($('#editdependent').is(':visible')){
					var formTextBoxId = jQuery('.addressValidator').find('input[type="text"]:first').attr('id'),
						zipCodeVal = jQuery('.addressValidator').find('.zipCode').val(),
						indexValue = formTextBoxId.split('_'),
						zipId = jQuery('.addressValidator').find('.zipCode').attr('id');
					
					$('#check-address-error, #address-failure-box').hide();
					$('#form-input-data').show();
					
					parent.postPopulateIndex(zipCodeVal, zipId);
				}else if(($('#frmeditemployee').is(':visible')) ||($('#frmeditmyinfo').is(':visible'))){
					var formTextBoxId = jQuery('.addressValidator').find('input[type="text"]:first').attr('id'),
						zipCodeVal = jQuery('.addressValidator').find('.zipCode').val(),
						indexValue = formTextBoxId.split('_'),
						zipId = jQuery('.addressValidator').find('.zipCode').attr('id');
					
					$('#check-address-error, #address-failure-box').hide();
					$('#form-input-data').show();
					//console.log('modal-zipcode', zipCodeVal, zipId);
					parent.postPopulateIndex(zipCodeVal, zipId);
					application.remove_modal();
				}else if($('#edit-mydependents').is(':visible')){
					//console.log('22222222 back_to_input_modal BIND',zipCodeSplEnrollId);
					//console.log($('#'+zipCodeSplEnrollId));
					var formTextBoxId = $('#'+zipCodeSplEnrollId).parents('.addressValidator').find('input[type="text"]:first').attr('id'),
						zipCodeVal = jQuery('.addressValidator').find('.zipCode').val(),
						indexValue = formTextBoxId.split('_'),
						zipId = jQuery('.addressValidator').find('.zipCode').attr('id');
					
					$('#check-address-error, #address-failure-box').hide();
					$('#form-input-data').show();
					parent.postPopulateIndex(indexValue[1], zipCodeVal, zipId);
					application.remove_modal();
					
				}else if($('#frmhomeaddress').is(':visible')){/*edit dependent page*/
					//console.log('!!!!!! form home address');
					$('#addressProgressPopup').remove();
					
					var formTextBoxId = jQuery('.addressValidator').find('input[type="text"]:first').attr('id'),
						zipCodeVal = jQuery('.addressValidator').find('.zipCode').val(),
						indexValue = formTextBoxId.split('_'),
						zipId = jQuery('.addressValidator').find('.zipCode').attr('id');
				
					$('#check-address-error, #address-failure-box').hide();
					$('#form-input-data').show();
					
					parent.postPopulateIndex(zipCodeVal, zipId);
				}else{
					//console.log('22222222 back_to_input_modal BIND')	
					$('#check-address-error, #address-failure-box').hide();
					application.remove_modal();
				}				
			});
		},
		/*return back to the input FORM div from IFRAME WINDOW*/
		back_button_from_iframe_verifyDependent:function(modalId){
			$(modalId.document).find('#back_button_from_iframe').bind('click',function(){
				if($('#editdependent').is(':visible')){
					$('input#firstName').focus();
					$('#check-address-error, #address-failure-box, #suggestion-box').hide();
					$('#form-input-data').show();
					if($('#address1_0').length){
						$('#address1_0').focus();
					}
				}else{
					$('#check-address-error, #address-failure-box, #suggestion-box').hide();
					application.remove_modal();
				}
   			});
		},
		back_button_from_iframe_worksite:function(modalId){
			//console.log("new zip code iframe window", modalId);
			
			var formTextBoxId = jQuery('#form-input-data').find('input[type="text"]:first').attr('id');
			////console.log('formTextBoxIdformTextBoxId----',formTextBoxId);
			
			$(modalId.document).find('#back_button_from_iframe').bind('click',function(){
				if($('#frmworksites').is(':visible')){
					//console.log('new zip code function modal template ifffffff');
					application.remove_modal();
					application.resetCountyOnAllCallback();
				}else if($('#editdependent').is(':visible') || $('#mainDiv').is(':visible')){
					//console.log('sdkjsdsd');
					application.setValueInFormForSplEnroll();
					application.resetCountyOnAllCallbackWithTwoArgs();
					
				}else if($('.entityAddressValidation').is(':visible')){
					//console.log('entityAddressValidation');
					application.remove_modal();
				}else{
					//console.log('new condition to be added')
					application.remove_modal();
					application.resetCountyOnAllCallback();
				}
			});
			
		},
		modal_cross_button:function(modalId){
			/*console.log('modal cross button', modalId, $(modalId.document), $('.closeModal'));*/
			$('.closeModal').bind('click',function(e){
				application.remove_modal();
				if($('#frmverifydependents').is(':visible')){
					application.resetCountyOnAllCallbackWithTwoArgs();
				}else{
					application.resetCountyOnAllCallback();
				}
			});
		},
		populateCounty: function(zipid, indexValue, zipCodeVal){
			//console.log(zipid, indexValue, zipCodeVal,'modal zipcode js function')
			if($('#'+zipid).val().length >= 5){
				
				if(indexValue.length === 1){
					//console.log('index value before',indexValue[0]);
					parent.postPopulateIndex(indexValue[0], zipCodeVal, zipid);
					application.setValueInFormForSplEnroll();
					//console.log('index value after',indexValue[0]);
					//$('#address1').focus();
				}
				else{
					//console.log('index value before',indexValue[1]);
					parent.postPopulateIndex(indexValue[1], zipCodeVal, zipid);
					//console.log('index value after',indexValue[1])
				}
				
			}
		},
		/*SUBMIT FUNCTION inside IFRAME*/
		submit_button_of_iframe:function(buttonId){
			////console.log('modal zipcode',button);
			var idss = $('#ids').val();
			var retVal=idss.split("~");	
			
			if ($("input:radio:checked").val() == undefined){
				alert('Please select one address.');
				return false;
			}
			/*get the value of CHECKED RADIO*/
			var actualVal=$("input:radio:checked").val().split(","),
				selectedState = $("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val();
			
			$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[0]).val(actualVal[0]);
			$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[2]).val($.trim(actualVal[2]));
			$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val(selectedState);
			$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[4]).val($.trim(actualVal[4]));
			$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[5]).val(actualVal[5]);
			$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[6]).val(actualVal[6]);
			$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[7]).val(actualVal[7] == undefined ? "" : actualVal[7]);
			$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[8]).val(actualVal[8] == undefined ? "" : actualVal[8]);
			/*trigger the BACK button*/
			$('#'+buttonId).parent().find("#back_button_from_iframe").trigger('click');
			parent.getCountyList($('#'+ zip_e).val(), '');
		},
		/*FOCUS OUT function on ZIP*/
		zipcode_focusout: function(e, that){

			var startindex = (e.target.id).indexOf("_");
			var index = (e.target.id).substring(startindex,(e.target.id).length);
			//console.log(startindex, index)
			
			var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
			var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
			/*check for the underscore in the id of the focus out event of the zipcode. for current scenarios id is "zip_0. hence startindex  ===3 "*/
			if (startindex === 3)
			{
				address1_e=address1_e+index;
				address2_e=address2_e+index;
				city_e=city_e+index;
				state_e=state_e+index;
				zip_e=zip_e+index;
				lat_e=lat_e+index;
				lon_e=lon_e+index;
				rdi_e+=index;
				county_e+=index;
			}
			

			var model_address1 = address1_e + '_hidden' ;
			var model_address2 = address2_e + '_hidden' ;
			var model_city = city_e + '_hidden' ;
			var model_state = state_e + '_hidden' ;
			var model_zip = zip_e + '_hidden' ;
			//parent.getCountyList($('#'+ zip_e).val(), '');
			
			var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
			if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
				if(($('#'+ address2_e).val())==="Address Line 2"){
					$('#'+ address2_e).val('');
				}	
				viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
					$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
					idsText);
				zipIdFocused = $(e.target).attr('id');
				//console.log(zipIdFocused,'1111');
			}
		},
		/*FOR ENTITY FOCUS OUT function on ZIP*/
		zipcode_focusout_entity: function(e, that){
			
			var startindexEntity = (e.target.id).indexOf("_");
			
			var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
			var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
			
			if($('.idMisMatch').is(':visible')){
				var indexEntity = (e.target.id).substring(startindexEntity,(e.target.id).length);
				address1_e1=address1_e+indexEntity;
				address2_e1=address2_e+indexEntity;
				city_e1=city_e+indexEntity;
				state_e1=state_e+indexEntity;
				zip_e1=zip_e+indexEntity;
			}else if($('.idMisMatchWithoutUnderScore').is(':visible')){
				address1_e1=address1_e;
				address2_e1=address2_e;
				city_e1=city_e;
				state_e1=state_e;
				zip_e1=zip_e;
			}else{
				var indexEntity = (e.target.id).substring(0,startindexEntity);
				address1_e1=indexEntity+'_'+address1_e;
				address2_e1=indexEntity+'_'+address2_e;
				city_e1=indexEntity+'_'+city_e;
				state_e1=indexEntity+'_'+state_e;
				zip_e1=indexEntity+'_'+zip_e;
			}
			
			//console.log(startindexEntity,indexEntity )
			//console.log(indexEntity, address1_e1, address2_e1, city_e1, state_e1, zip_e1)

			var model_address1 = address1_e + '_hidden' ;
			var model_address2 = address2_e + '_hidden' ;
			var model_city = city_e + '_hidden' ;
			var model_state = state_e + '_hidden' ;
			var model_zip = zip_e + '_hidden' ;
			
			
			var idsText=address1_e1+'~'+address2_e1+'~'+city_e1+'~'+state_e1+'~'+zip_e1+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
			if(($('#'+ address1_e).val() != "Street Name, P.O. Box, Company, c/o")&&($('#'+ city_e).val() != "City, Town")&&($('#'+ state_e).val() != "State")&&($('#'+ zip_e).val() != '')){
				if(($('#'+ address2_e).val())==="Apt, Suite, Unit, Bldg, Floor, etc"){
					$('#'+ address2_e).val('');
				}	
				viewValidAddressListNew($('#'+ address1_e1).val(),$('#'+ address2_e1).val(),$('#'+ city_e1).val(),$('#'+state_e1).val(),$('#'+ zip_e1).val(), 
					$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
					idsText);
				
				zipIdFocused = $(e.target).attr('id');
				//console.log(zipIdFocused,'1111');
			}
		},
		/*RESET the content in the FORM*/
		input_content_screen_reset: function(that){
        	$('.errorAddress, .errorAddressBtn, .suggestedAddress').remove();
        	$('#modal').find('.modal-header h3').text('Additional Worksite');
        	$('#modal-inputs,#input-footer').show();
        },
        /*VALIDATE the FORM elements*/
        add_Address_function:function(){
        	$("#frmworksites").validate({
                errorClass: "error",
                errorPlacement: function(error, element) {
            		var elementId = element.attr('id');
            		error.appendTo( $("#" + elementId + "_error"));
            		$("#" + elementId + "_error").attr('class','error help-inline');
                },
                rules: {
                	'locations[0].location.address1': { 
                        required: true
                    },
                    'locations[0].location.city': {
                        required: true
                    },
                    'locations[0].location.state': {
                        required: true
                    },
                    'locations[0].location.zip': {
                        required: true,
                        zipcheck : true,
                    },
                    'locations[0].location.county': {
                        required: true
                    }
                }
            });
			
            /*error message for address1*/
            $('input[name="locations[0].location.address1"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter address.</span>"
                }
            });
            /*error message for city*/
            $('input[name="locations[0].location.city"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter city.</span>"
                }
            });
            /*error message for state*/
            $('*[name="locations[0].location.state"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter city.</span>"
                }
            });
            
            /*error message for zip*/
            $('input[name="locations[0].location.zip"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter zip code.</span>",
                    zipcheck: "<span><em class='excl'>!</em> Please enter a valid zip code.</span>"
                }
            });
            
            /*error message for county*/
            $('*[name="locations[0].location.county"]').rules('add', {
                messages: {
                    required: "<span> <em class='excl'>!</em> Please enter a county.</span>"
                }
            });
            
            /*check the value of ZIP*/
            jQuery.validator.addMethod("zipcheck", function(value, element, param) {
              	elementId = element.id; 
              	var numzip = new Number($("#"+elementId).val());
                var filter = /^[0-9]+$/;
                if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
              		return false;	
              	}return true;
            });
        },
        /*populate STATE DROPDOWN inside MODAL on show*/
        populate_state_dropdown:function(){
        	////console.log($('#jsonStateValue').val())
        	var stateObj = $('#jsonStateValue').val(),
        		stateJson = $.parseJSON(stateObj);
        	
        	var defaultStateValue = $('#defaultStateValue').val();
        	$.each(stateJson, function(index, item){
        		if(item.code === defaultStateValue){
        			$('<option selected="selected" value='+[item.code]+'>'+[item.name]+'</option>').appendTo("select[name='locations[0].location.state']");
        		}else{
        			$('<option value='+[item.code]+'>'+[item.name]+'</option>').appendTo("select[name='locations[0].location.state']");
        		}
        	});
        },
        /*RESIZE the IFRAME HEIGHT*/
        iframeHeight:function(){
        	/*console.log('modal zipcode',$("#modalData", $(window).height()).height(),$('#suggestion-box'));*/
			$('#modalData').load(function(){
				var iFrameHeight = document.getElementById('modalData').contentDocument.body.scrollHeight + "px";
				/*console.log('iFrameHeight',iFrameHeight)*/
				$('#modalData').height(iFrameHeight);
				$('#modalData').width('100%');
			});
        },
        /*populate LOCATION on PARENT PAGE*/
        populate_location_on_parent_page: function(empDetailsJSON, contactlocationid){
			if($.browser.msie && $.browser.version <= 8){
				for(var i=0;i<empDetailsJSON.length;i++){
					var obj = empDetailsJSON[i];
					primarylocationid = obj['id'];
				}
				
				var selected = (obj['id'] == contactlocationid) ? contactlocationid : primarylocationid,
					selectLocationId = (locationval != undefined || locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];
					
				$('#location').prepend('<option value="'+selectLocationId+'" ' +'>'+obj['address1']+", "+obj['city']+", "+obj['state']+'</option>');
				$('#location').val(selected);
			}else{
				for(var i=0;i<empDetailsJSON.length;i++){
					var obj = empDetailsJSON[i];
					var selected = '';
					if(contactlocationid == 0){
						if(obj['primary'] == 'YES'){
							selected = 'selected="selected"';
						}
					}else if(obj['id'] == contactlocationid){
						selected = 'selected="selected"';
					}
					
					var selectLocationId = (locationval != undefined && locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];
                    $('#location').prepend('<option value="'+selectLocationId+'" '+ selected +'>'+obj['address1']+", "+obj['city']+", "+obj['state']+'</option>');
				} 
			}
        },
        /*ADDRESS Failure Message*/
        address_failure_message:function(message){        	
        	$('#address-failure-box').remove();
        	var addresFailureMessage = '<div id="address-failure-box"  class="modal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-header clearfix"><h3 class="pull-left">Confirm Address</h3>'+
    			'<button type="button" class="close closeModal">&times;</button></div>'+
				'<div class="modal-body"><div class="suggestedAddress">'+message+'</div></div>'+
				'<div class="modal-footer"><input type="button" class="btn errorAddressBtn closeModal" aria-hidden="true" id="back_to_input_modal" value="OK" /></div></div><div class="modal-backdrop fade in"></div>';
        	
			if($('#editdependent').is(':visible')){
				$('#frmemployeedetails').append(addresFailureMessage);
				$('#form-input-data').hide();
			}else{
				$('body').append(addresFailureMessage);
				$('#address-failure-box').addClass('modal popup-address');
			}
			
			application.back_button_to_input_screen();
        },
        ssp_address_success_modal:function(){
        	$('#editdependent').hide();
        	$('#suggestion-box').show();
        },
        ssp_dependent_modal:function(){
        	$('#editdependent').show();
        	$('#suggestion-box').hide();
        },
        match_found_proper_address:function(){
			//console.log(zipIdFocused,'1111');
			var getZipIndex = (zipIdFocused);
			$('#county').focus();
		}
};
var zipCodeSplEnrollId;
/*FOCUS OUT event of ZIP CODE*/
$('.zipCode').live('focusout',function(e){
	application.zipcode_focusout(e, $(this));
	if($('#edit-mydependents').is(':visible')){
		zipCodeSplEnrollId = e.target.id;
	}
});

$('.removeModal').live('click',function(){
	//////console.log('zipcode util');
	application.remove_modal();
});


$('.entityZipCode').live('focusout',function(e){
	application.zipcode_focusout_entity(e, $(this));
});