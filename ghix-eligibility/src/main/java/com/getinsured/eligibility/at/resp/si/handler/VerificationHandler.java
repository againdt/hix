package com.getinsured.eligibility.at.resp.si.handler;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.at.resp.service.SsapVerificationService;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.at.resp.si.dto.Verification;
import com.getinsured.eligibility.at.resp.si.handler.helper.MatchHandler;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapVerification;

/**
 * Verification Handler to compare stored ssap Verification with the received state's response.
 *
 * @author Ekram
 *
 */
@Component
@DependsOn("dynamicPropertiesUtil")
public class VerificationHandler {


	private static final String CONTINUE_THE_45_90_DAY_WAIT_PERIOD_PROCESS_FOR_VERIFICATIONS_FOR = "Continue the 45/90 day wait period process for verifications - for - ";
	private static final String INCOMPLETE_VERIFICATIONS = "INCOMPLETE_VERIFICATIONS";
	private static final String ALL_VERIFICATION_COMPLETED = "ALL_VERIFICATION_COMPLETED";
	private static final String VERIFICATION_MATCH_RESULT = "VERIFICATION_MATCH_RESULT";

	private static String stateCode;

	private static final Logger LOGGER = Logger.getLogger(VerificationHandler.class);

	@Autowired private SsapVerificationService ssapVerificationService;

	@Autowired
	private ExceptionUtil exceptionUtil;
	
	@PostConstruct
	public void createStateContext() {
		stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	public String processERP(Message<ERPResponse> message){

		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, erpResponse.getApplicationID());

		try {

			boolean flag = true;
			/*String applicantLevel_NonVerified = "";*/
			String applicantLevel_NonVerified = process(erpResponse);

			if (!StringUtils.isEmpty(applicantLevel_NonVerified)){
				flag = false;
			}

			if (flag){
				resultMap.put(VERIFICATION_MATCH_RESULT, EligibilityConstants.AUTO);
				resultMap.put(ALL_VERIFICATION_COMPLETED, EligibilityConstants.Y);
			} else {
				resultMap.put(VERIFICATION_MATCH_RESULT, EligibilityConstants.AUTO);
				resultMap.put(ALL_VERIFICATION_COMPLETED, EligibilityConstants.N);
				resultMap.put(INCOMPLETE_VERIFICATIONS, CONTINUE_THE_45_90_DAY_WAIT_PERIOD_PROCESS_FOR_VERIFICATIONS_FOR
						+ applicantLevel_NonVerified);
			}

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID).
					append(erpResponse.getApplicationID()).append(EligibilityConstants.REASON).append(e.getMessage());

			LOGGER.error(errorReason.toString());
			resultMap.put(VERIFICATION_MATCH_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());
			exceptionUtil.persistGiMonitorId(erpResponse.getSsapApplicationPrimaryKey(), e);
		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);
	}

	private String process(ERPResponse erpResponse){
		//1. Fetch all incoming applicants from AT Response
		List<TaxHouseholdMember> taxHHList =  erpResponse.getTaxHouseholdMemberList();

		//2. Fetch all existing applicants from SSAP_APPLICANT table
		List<TaxHouseholdMember> ssapHHList =  erpResponse.getSsapApplicantList();

		//3. Fetch corresponding giWsPayload object..
		Integer giWsPayloadId = erpResponse.getGiWsPayloadId();

		//4. Compare every AT Response applicant with DB..
		Map<String, List<String>> applicantLevel_NonVerifiedMap = new HashMap<>();
		for (TaxHouseholdMember ssapApplicant : ssapHHList) {
			
			TaxHouseholdMember atTaxPerson = MatchHandler.findMatchingMember(ssapApplicant, taxHHList);

			if (atTaxPerson != null){
				// Persist latest Verification data..
				List<String> nonVerifiedMap = processVerification(atTaxPerson, ssapApplicant, giWsPayloadId, erpResponse.getApplicationID());
				applicantLevel_NonVerifiedMap.put(ssapApplicant.getPersonInfo().getPrimaryKey().toString(), nonVerifiedMap);
			} else {
				LOGGER.error(EligibilityConstants.MEMBER_MATCHING_LOGIC_FAILED);
				throw new GIRuntimeException(EligibilityConstants.MEMBER_MATCHING_LOGIC_FAILED +
						"SSAP Person Detail - " + ssapApplicant +
						"AT HH List - " + taxHHList);
			}

		}

		// 5. Determine and prepare incompleteVerificationList for each and every applicant..
		StringBuilder incompleteVerificationList = new StringBuilder();
		for (String pKey : applicantLevel_NonVerifiedMap.keySet()) {
			List<String> nonVerifiedList = applicantLevel_NonVerifiedMap.get(pKey);

			if (nonVerifiedList != null && nonVerifiedList.size() > 0){
				incompleteVerificationList.append("SSAP Person in GI db with Primary Key - " + pKey + "\n");
				incompleteVerificationList.append("List of incomplete verifications - ");
				for (String string : nonVerifiedList) {
					incompleteVerificationList.append(" ");
					incompleteVerificationList.append(string);
				}
				incompleteVerificationList.append("\n");
			}
		}

		return incompleteVerificationList.toString();
	}

	private List<String> processVerification(TaxHouseholdMember atTaxPerson, TaxHouseholdMember ssapApplicant, Integer giWsPayloadId, String applicationID) {

		List<String> nonVerifiedList = new ArrayList<>();

		if ("ID".equals(stateCode) || "NV".equals(stateCode)){
			ssapVerificationService.updateVerificationStatus(ssapApplicant.getPersonInfo().getPrimaryKey());
			return nonVerifiedList;
		}

		// get Data from SSAP tables... &  prepare verification DO List...
		List<SsapVerification> resultDOs = ssapVerificationService.findBySsapApplicantId(ssapApplicant.getPersonInfo().getPrimaryKey());
		Map<String, SsapVerification> ssapVerificationMap = new HashMap<>();
		for (SsapVerification ssapVerification : resultDOs) {
			ssapVerificationMap.put(ssapVerification.getVerificationType(), ssapVerification);
		}

		// get Data from AT Response...
		Map<String, Boolean> ssapPersonVerificationsMap = ssapApplicant.getPersonInfo().getPersonVerificationsMap();

		Map<String, Verification> atTaxMemberVerifications = atTaxPerson.getVerificationMap();
		// TODO: try to refactor
		for (String verificationType : atTaxMemberVerifications.keySet()) {
			Verification verification = atTaxMemberVerifications.get(verificationType);

			String type = verification.getType();
			String status = verification.getStatus();

			if (ssapPersonVerificationsMap.containsKey(type)) {
				boolean isVerified = ssapPersonVerificationsMap.get(type);

				if (!isVerified){
					// check ssapVerificationMap, if record found update or insert new one...
					if (ssapVerificationMap.containsKey(type)){
						// Old Record..
						// Transform DTO to SsapVerification DO
						map(ssapVerificationMap.get(type), verification);

					} else {
						// New Record..
						// Transform DTO to SsapVerification DO and add to existing resultDO
						SsapVerification ssapVerificationDO = map(null, verification);
						resultDOs.add(ssapVerificationDO);
					}
				}
			}

			if (!StringUtils.equalsIgnoreCase(status, "VERIFIED")){
				nonVerifiedList.add(type);
			}
		}

		//persist results...
		if (!resultDOs.isEmpty()){
			ssapVerificationService.saveOrUpdateSsapVerification(ssapApplicant.getPersonInfo().getPrimaryKey(), resultDOs, giWsPayloadId);
		}

		// return all non-verified Map
		return nonVerifiedList;

	}

	private SsapVerification map(SsapVerification ssapVerification, Verification verification) {

		if (ssapVerification == null){
			ssapVerification = new SsapVerification();
		}

		ssapVerification.setVerificationType(verification.getType());
		ssapVerification.setEffectiveStartDate(setStartDate(verification));
		ssapVerification.setEffectiveEndDate(setEndDate(verification));
		ssapVerification.setSource(verification.getSource());
		ssapVerification.setVerificationStatus(verification.getStatus());

		return ssapVerification;

	}

	private Timestamp setEndDate(Verification verification) {
		Long timestamp = verification.getStartDate() != null ? verification.getEndDate().getTime() : null;
		return timestamp != null ? new java.sql.Timestamp(timestamp) : null ;
	}

	private Timestamp setStartDate(Verification verification) {
		Long timestamp = verification.getStartDate() != null ? verification.getStartDate().getTime() : null;
		return timestamp != null ? new java.sql.Timestamp(timestamp) : null ;
	}

}
