<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%
  String timeZone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);
%>
<c:set var="timeZone" value="<%=timeZone%>" />
<div class="header">
	<h4 class="title">
		${inboxMsg.msgSub}<span class="pull-right"> 
		
		<c:if test="${page_title == 'Message Drafts'}">
			<a href="#new-msg" id="edit" data-toggle="modal" title="Edit"> 
				<i class="icon-white" style="background-position: 0px -72px;"></i>
			</a>
		</c:if>
		
<%-- 		<c:if test="${page_title == 'Inbox' || page_title == 'Sent Messages'}"> --%>
<!-- 			<a href="#new-msg" id="reply" data-toggle="modal" title="Reply">  -->
<!-- 				<i class="icon-reply" ></i> -->
<!-- 			</a> 	 -->
<%-- 		</c:if> --%>
		
<%-- 		<c:if test="${page_title == 'Inbox' || page_title == 'Sent Messages'}"> --%>
<!-- 			<a href="#new-msg" id="forward" data-toggle="modal" title="Forward">  -->
<!-- 				<i class="icon-share-alt"></i> -->
<!-- 			</a> -->
<%-- 		</c:if> --%>
		
		<%-- <input id="msgId" name="msgId" type="hidden" value="${inboxMsg.id}"> --%>
		<c:set var="msgId" ><encryptor:enc value="${inboxMsg.id}" isurl="true"/></c:set>
<%-- 		<c:if test="${page_title == 'Inbox'}"> --%>
<%-- 			<a href="javascript: singleAction('Unread', '${inboxMsg.id}');" title="Mark as unread">  --%>
<!-- 				<i class="icon-envelope" ></i> -->
<!-- 			</a> -->
<%-- 		</c:if> --%>
		
		<a href="javascript:callPrint();"
			title="Print"> <i class="icon-print"></i>
		</a>
		
<%-- 		<c:if test="${page_title == 'Inbox' || page_title == 'Sent Messages' || page_title == 'Message Drafts'}"> --%>
<%-- 			<a href="javascript: singleAction('Archive', '${inboxMsg.id}');" title="Archive">  --%>
<!-- 				<i class="icon-archive"></i> -->
<!-- 			</a> -->
<%-- 		</c:if> --%>
		</span>
	</h4>
</div>
<div class="gray graybg gutter10 clearfix">
	<table class="table table-condensed table-border-none none">
		<tbody>
			<tr>
				<td class="span3"><p><strong>From:</strong></p></td>
				<td><p>${inboxMsg.fromUserName}</p></td>
			</tr>
			<tr>
				<td><p><strong>To:</strong></p></td>
				<td><p>${inboxMsg.toUserNameList}</p></td>
			</tr>
			<tr>
				<td><p><strong>Sent:</strong></p></td>
				<td>
				<fmt:parseDate value="${msgDetail.sentDate}" var="date" pattern="MMM dd, yyyy HH:mm"/>
				<fmt:timeZone value="${timeZone}">
				<p><fmt:formatDate type="both" value="${date}" pattern="MMM dd, yyyy HH:mm" timeZone="${timeZone}"/></p>
				</fmt:timeZone></td>
			</tr>
			<tr>
				<td><p><strong>Attachments:</strong></p></td>
				<td><p>
					<table>
						<c:forEach var="msgDoc" items="${inboxMsg.messageDocs}" >	
							<tr>
								<td><a
									target="_blank" href="getAttachment?ref=${msgDoc.getSecureReference("attachment")}">${msgDoc.docName}(${msgDetail[msgDoc.docId]})</a>
								</td>
							</tr>
							<input id="attachmentViewerParams" name="attachmentViewerParams" type="hidden" value="?ref=${msgDoc.getSecureReference("inline")}" />
						</c:forEach>
					</table>
				</p></td>
			</tr>
		</tbody>
	</table>
</div>
<div class="msgbody gutter10">${inboxMsg.msgBody}</div>
<div>
	<iframe id="attachmentViewer" style="width: 600px; height: 500px; border: 0;visibility: hidden;"></iframe> 
</div>

<script type="text/javascript">
	function getAttachment(parameters) {
		var documentUrl = "${pageContext.request.contextPath}/inbox/getAttachment" + parameters;
		document.getElementById('attachmentViewer').setAttribute("src", documentUrl);
		document.getElementById('attachmentViewer').style.visibility="visible";
	}
	
	document.body.onload=getAttachment(document.getElementById("attachmentViewerParams").value);
</script>
