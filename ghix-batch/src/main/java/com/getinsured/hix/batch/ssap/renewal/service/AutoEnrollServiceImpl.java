package com.getinsured.hix.batch.ssap.renewal.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.xml.sax.SAXException;

import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.indportal.CustomGroupingUtil;
import com.getinsured.eligibility.indportal.customGrouping.CustomGroupingController.EligibilityType;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.hix.batch.repository.RenewalEligibilityProgramRepository;
import com.getinsured.hix.batch.ssap.renewal.util.AutoRenewalValidationHandler;
import com.getinsured.hix.indportal.dto.AptcRatioRequest;
import com.getinsured.hix.indportal.dto.AptcRatioResponse;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.RenewalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member.Relationship;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.ResponsiblePerson;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalResponse;
import com.getinsured.hix.webservice.plandisplay.autorenewal.EnrollmentVal;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ObjectFactory;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ProductType;
import com.getinsured.hix.webservice.plandisplay.autorenewal.RoleTypeVal;
import com.getinsured.hix.webservice.plandisplay.autorenewal.YesNoVal;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.LanguageEnum;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.RenewalApplicant;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.RenewalApplicantRepository;
import com.getinsured.iex.ssap.repository.RenewalApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

/**
 * Class is used to provide services for Auto Enroll Job.
 * 
 * @since July 19, 2019
 */
@Service("autoEnrollService")
public class AutoEnrollServiceImpl implements AutoEnrollService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutoEnrollServiceImpl.class);
	private static final String DENTAL = "Dental";
	private static final String HEALTH = "Health";
	private static final int HARDSHIP_EXEMPTION_AGE = 30;
	private static final String Y = "Y";
	private static final String FAILURE = "Failure";
//	private static final String SUCCESS = "Success";
	private static final SimpleDateFormat REQUIRED_DATE_FORMAT = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
	private static final String _200 = "200";
	private static final String _131 = "131";
	private static final String _129 = "129";
	private static final String _125 = "125";
	private static final String US_CITIZEN = "1";
	private static final String NON_US_CITIZEN = "3";
	private static final String APTC = "APTC";
	private static final String SPTC = "SPTC";

	@Autowired
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired
	private RenewalApplicationRepository renewalApplicationRepository;
	@Autowired
	private RenewalApplicantRepository renewalApplicantRepository;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	private RenewalEligibilityProgramRepository renewalEligibilityProgramRepository;
	@Autowired
	private WebServiceTemplate ind71WebServiceTemplate;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private CustomGroupingUtil customGroupingUtil;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private GIMonitorService giMonitorService;
	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;
	@Autowired
	private GIWSPayloadRepository giwsPayloadRepository;
	@Autowired 
	private AptcHistoryRepository aptcHistoryRepository;

	/**
	 * Method is use to save and throws Auto Enroll Job Error.
	 */
	public synchronized void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException {
		giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50011", new TSDate(), this.getClass().getName(),
				errorMessage, null, errorMessage, GIRuntimeException.Component.BATCH.getComponent(), null);
		throw new UnexpectedJobExecutionException(errorMessage);
	}

	public Integer logToGIMonitor(String errorMessage, String errorCode) {
		Integer giMonitorId = null;
		GIMonitor giMonitor = giMonitorService.saveOrUpdateErrorLog(errorCode, new TSDate(), this.getClass().getName(),errorMessage, null, null, GIRuntimeException.Component.BATCH.getComponent(), null);
		if (giMonitor != null) {
			giMonitorId = giMonitor.getId();
		}
		return giMonitorId;
	}
	
	/**
	 * Method is use to save and throws Auto Enroll Job Error.
	 */
	public synchronized Integer logToGIMonitor(Exception e, int errorCode, String caseNumber) {
		Integer giMonitorId = null;
		GIMonitor giMonitor = giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_" + errorCode, new Date(), this.getClass().getName(),
				e.getLocalizedMessage() + "\n" + e.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(e), null,
				caseNumber, GIRuntimeException.Component.BATCH.getComponent(), null);
		if (giMonitor != null) {
			giMonitorId = giMonitor.getId();
		}
		return giMonitorId;
	}

	private void saveGiMonitorId(RenewalApplication renewalApplication, Integer giMonitorId) {

		if (renewalApplication != null && giMonitorId != null) {
			renewalApplication.setGiMonitorId(Long.valueOf(giMonitorId.longValue()));
			renewalApplicationRepository.save(renewalApplication);
		}
	}
	
	private void saveServerName(RenewalApplication renewalApplication, Integer serverName) {

		if (renewalApplication != null && serverName != null) {
			renewalApplication.setAutoEnrollServerName(serverName);
			renewalApplicationRepository.save(renewalApplication);
		}
	}

	private void saveGiMonitorId(List<RenewalApplicant> renewalApplicantList, Integer giMonitorId) {

		if (renewalApplicantList != null && giMonitorId != null) {

			for (RenewalApplicant renewalApplicant : renewalApplicantList) {
				renewalApplicant.setGiMonitorId(Long.valueOf(giMonitorId.longValue()));
			}
			renewalApplicantRepository.save(renewalApplicantList);
		}
	}

	@Override
	public synchronized List<Long> getAutoRenewalApplIdListByCoverageYear(boolean processErrorRecordFlag, Long renewalYear, long batchSize) {

		List<RenewalStatus> statusList = null;

		if (processErrorRecordFlag) {
			statusList = Arrays.asList(RenewalStatus.TO_CONSIDER, RenewalStatus.APP_ERROR);
		}
		else {
			statusList = Arrays.asList(RenewalStatus.TO_CONSIDER);
			
		}
		return ssapCloneApplicationService.getAutoRenewalApplIdListByCoverageYear(statusList, renewalYear, batchSize);
	}
	
	@Override
	public List<Long> getAutoRenewalApplIdListByServerName(boolean processErrorRecordFlag, Long renewalYear, int serverCount, int serverName, long batchSize) {

		List<RenewalStatus> statusList = null;

		if (processErrorRecordFlag) {
			statusList = Arrays.asList(RenewalStatus.TO_CONSIDER, RenewalStatus.APP_ERROR);
		}
		else {
			statusList = Arrays.asList(RenewalStatus.TO_CONSIDER);
			
		}
		return ssapCloneApplicationService.getAutoRenewalApplIdListByServerName(statusList, renewalYear, serverCount, serverName, batchSize);
	}

	@Override
	public synchronized List<RenewalApplication> getAutoRenewalApplListByIds(List<Long> renewalApplIdList) {
		return ssapCloneApplicationService.getAutoRenewalApplListByIds(renewalApplIdList);
	}

	@Override
	public synchronized AtomicBoolean processAutoEnrollData(RenewalApplication renewalApplication, AtomicBoolean processErrorRecordFlag, AtomicLong renewalYear, int serverName) throws Exception {

		AtomicBoolean autoEnrollStatus = new AtomicBoolean(false);

		try {
			long time = System.currentTimeMillis();
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Processing Auto Enroll for SSAP-Application " + renewalApplication.getSsapApplicationId() + " and renewal year " + renewalYear + " start time " + time);
			}
			
			saveServerName(renewalApplication, serverName);
			
			LOGGER.warn("Processing Auto Enroll for SSAP-Application " + renewalApplication.getSsapApplicationId() + " and renewal year " + renewalYear + " start time " + time);
			SsapApplication currentApplication = loadCurrentApplication(renewalApplication.getSsapApplicationId());

			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());

			Map<Long, SsapApplicant> applicantMap = currentApplication.getSsapApplicants().stream().collect(Collectors.toMap(SsapApplicant::getPersonId, Function.identity()));
			
			List<RenewalApplicant> renewalApplicantList = renewalApplicantRepository.findBySsapApplicationIdAndRenewalStatus(renewalApplication.getSsapApplicationId());
			
			if(RenewalStatus.NOT_TO_CONSIDER.equals(renewalApplication.getHealthRenewalStatus())) {
				//Filter Out Health Applicants
				renewalApplicantList = renewalApplicantList
										.stream()
										.filter(renewalApplicant -> renewalApplicant.getPlanType().equalsIgnoreCase("Dental"))
										.collect(Collectors.toList());
				
			} else if(RenewalStatus.NOT_TO_CONSIDER.equals(renewalApplication.getDentalRenewalStatus())) {
				//Filter Out Dental Applicants
				renewalApplicantList = renewalApplicantList
										.stream()
										.filter(renewalApplicant -> renewalApplicant.getPlanType().equalsIgnoreCase("Health"))
										.collect(Collectors.toList());
			}

			Map<String, List<RenewalApplicant>> applicantPlantypetMap = null;

			if (processErrorRecordFlag.get()) {
				applicantPlantypetMap = renewalApplicantList
										.stream()
										.filter(renewalApplicant -> (    RenewalStatus.APP_ERROR.equals(renewalApplicant.getRenewalStatus()) 
												                      || RenewalStatus.TO_CONSIDER.equals(renewalApplicant.getRenewalStatus())))
										.collect(Collectors.groupingBy(RenewalApplicant::getPlanType));
			} else {
				applicantPlantypetMap = renewalApplicantList
						                .stream()
						                .filter(renewalApplicant -> RenewalStatus.TO_CONSIDER.equals(renewalApplicant.getRenewalStatus()))
						                .collect(Collectors.groupingBy(RenewalApplicant::getPlanType));
			}
			
			autoEnrollStatus = processSsap(currentApplication, renewalApplication, applicantPlantypetMap, applicantMap, singleStreamlinedApplication, renewalYear.get());
			LOGGER.warn("Processed Auto Enroll for SSAP-Application " + renewalApplication.getSsapApplicationId() + " in  " + (System.currentTimeMillis() - time) + " ms");
		}
		catch (Exception e) {
			LOGGER.error("Exception occured for calling auto renewal" + ExceptionUtils.getFullStackTrace(e));
			Integer giMonitorId = this.logToGIMonitor(e, 50011, Long.toString(renewalApplication.getSsapApplicationId()));
			saveGiMonitorId(renewalApplication, giMonitorId);
			throw new Exception("Error occoured while getting health and dental enrollment count " + e);
		}
		return autoEnrollStatus;
	}

	public SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}
	
	private AtomicBoolean processSsap(SsapApplication currentApplication, RenewalApplication renewalApplication,
			Map<String, List<RenewalApplicant>> applicantPlantypetMap, Map<Long, SsapApplicant> applicantMap,
			SingleStreamlinedApplication singleStreamlinedApplication, Long renewalYear) throws Exception {

		List<RenewalApplicant> applicanHealthPlantList = applicantPlantypetMap.get(HEALTH);
		List<RenewalApplicant> applicanDentalPlantList = applicantPlantypetMap.get(DENTAL);
		Map<Long, String> ssapIDToGuidMap = new HashMap<>();
		boolean healthFailSetDentalAPTCNull = false;
		boolean appErroInHealthRenewal = false;

		// Calculate APTC and SPTC for each Health plan member
		Map<Long, BigDecimal> memberAptcMap = currentApplication.getMaximumAPTC() != null
				? getAptcSptcMemberList(currentApplication, applicanHealthPlantList, APTC) : null;
		Map<Long, BigDecimal> memberSptcMap = currentApplication.getMaximumStateSubsidy() != null
				? getAptcSptcMemberList(currentApplication, applicanHealthPlantList, SPTC) : null;

		// HEALTH RENEWAL
		Map<Long, List<RenewalApplicant>> applicantEnrollmentIdHealthtMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(applicanHealthPlantList)) {
			applicantEnrollmentIdHealthtMap = applicanHealthPlantList.stream().collect(Collectors.groupingBy(RenewalApplicant::getEnrollmentId));
		}

		Map<Long, String> healthStatusMap = new HashMap<>();
		List<SsapApplicant> healthApplicants = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(applicanHealthPlantList)) {

			for (RenewalApplicant renewalApplicant : applicanHealthPlantList) {

				for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {

					if (ssapApplicant.getId() == renewalApplicant.getSsapApplicantId()) {
						ssapIDToGuidMap.put(ssapApplicant.getPersonId(), ssapApplicant.getApplicantGuid());
					}
				}
			}
		}

		for (Long enrollmentId : applicantEnrollmentIdHealthtMap.keySet()) {

			LOGGER.info("Started processing health enrollment ID : " + enrollmentId);
			if (enrollmentId != null) {

				List<RenewalApplicant> renewalApplicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);

				String healthResponseCodeStatus = preapareAndSendIND71ForHealth(renewalApplication, renewalApplicantList,
						currentApplication, singleStreamlinedApplication, ssapIDToGuidMap, applicantMap,
						healthApplicants, healthFailSetDentalAPTCNull, enrollmentId, memberAptcMap, memberSptcMap);

				if (CollectionUtils.isNotEmpty(renewalApplicantList)) {

					List<SsapApplicant> healthApplicantListToCreateEvents = getApplicantListToCreateEvents(currentApplication, renewalApplicantList, applicantMap);
					ssapCloneApplicationService.createApplicationEvents(currentApplication, healthApplicantListToCreateEvents, healthResponseCodeStatus, enrollmentId,
							renewalApplicantList.get(0).getPlanId(), HEALTH);
				}
				else {
					LOGGER.warn("Renewal Applicant List is not found for creating Application Events.");
				}
				healthStatusMap.put(enrollmentId, healthResponseCodeStatus);
			}
		}

		for (Long enrollmentId : healthStatusMap.keySet()) {

			if (!healthStatusMap.get(enrollmentId).equalsIgnoreCase(_200)) {

				if (healthStatusMap.get(enrollmentId).equalsIgnoreCase(FAILURE)) {
					appErroInHealthRenewal = true;
				}
				healthFailSetDentalAPTCNull = true;
			}
		}

		boolean processDental = true;

		if (healthFailSetDentalAPTCNull) {

			String renewDental = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.RENEW_DENTAL);

			if (renewDental.equals("N") && !(ExchangeEligibilityStatus.QHP.equals(currentApplication.getExchangeEligibilityStatus()))) {
				processDental = false;
			}
		}

		// DENTAL RENEWAL
		Map<Long, String> dentalStatusMap = new HashMap<>();
		Map<Long, List<RenewalApplicant>> applicantEnrollmentIdDentalMap = new HashMap<>();
		List<SsapApplicant> dentalApplicants = new ArrayList<>();

		if (processDental) {
			/*
			 * As per current logic SSAP Application can only have one Dental enrollment
			 * Hence applicantEnrollmentIdDentalMap should contain only one entry.
			 */
			if (CollectionUtils.isNotEmpty(applicanDentalPlantList)) {
				applicantEnrollmentIdDentalMap = applicanDentalPlantList.stream().collect(Collectors.groupingBy(RenewalApplicant::getEnrollmentId));
			}

			ssapIDToGuidMap.clear();
			if (CollectionUtils.isNotEmpty(applicanDentalPlantList)) {

				for (RenewalApplicant renewalApplicant : applicanDentalPlantList) {

					for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {

						if (ssapApplicant.getId() == renewalApplicant.getSsapApplicantId()) {
							ssapIDToGuidMap.put(ssapApplicant.getPersonId(), ssapApplicant.getApplicantGuid());
						}
					}
				}
			}

			for (Long enrollmentId : applicantEnrollmentIdDentalMap.keySet()) {

				LOGGER.info("Started processing dental enrollment ID : " + enrollmentId);
				if (enrollmentId != null) {

					List<RenewalApplicant> renewalApplicantList = applicantEnrollmentIdDentalMap.get(enrollmentId);
					String dentalResponseCodeStatus = preapareAndSendIND71ForDental(renewalApplication, renewalApplicantList,
							currentApplication, singleStreamlinedApplication, ssapIDToGuidMap, applicantMap,
							dentalApplicants, enrollmentId, healthFailSetDentalAPTCNull, renewalYear);

					if (CollectionUtils.isNotEmpty(renewalApplicantList)) {

						List<SsapApplicant> dentalApplicantListToCreateEvents = getApplicantListToCreateEvents(currentApplication, renewalApplicantList, applicantMap);
						ssapCloneApplicationService.createApplicationEvents(currentApplication, dentalApplicantListToCreateEvents, dentalResponseCodeStatus, enrollmentId,
								renewalApplicantList.get(0).getPlanId(), DENTAL);
					}
					else {
						LOGGER.warn("Renewal Applicant List is not found for creating Application Events.");
					}
					dentalStatusMap.put(enrollmentId, dentalResponseCodeStatus);
				}
			}
		}
		else if (CollectionUtils.isNotEmpty(applicanDentalPlantList) && !appErroInHealthRenewal) {
			renewalApplication.setDentalRenewalStatus(RenewalStatus.NOT_TO_CONSIDER);
			Integer reasonCodeValueId = getReasonCode("DNC");
			renewalApplication.setDentalReasonCode(reasonCodeValueId != null ? reasonCodeValueId : null);
			renewalApplication = renewalApplicationRepository.save(renewalApplication);
		}
		updateApplicationStatus(healthStatusMap, dentalStatusMap, applicantEnrollmentIdHealthtMap,
				applicantEnrollmentIdDentalMap, currentApplication, renewalApplication);
		return new AtomicBoolean(true);
	}

	private Map<Long, BigDecimal> getAptcSptcMemberList(SsapApplication currentApplication,
			List<RenewalApplicant> applicantHealthPlantList, String type) throws GIException {

		List<AptcRatioResponse.Member> aptcMemberList;
		Map<Long, BigDecimal> memberAptcSptcMap = new HashMap<>();
		Date coverageDate = ssapCloneApplicationService.getCoverageStartDate(currentApplication);
		List<SsapApplicant> applicantList = new ArrayList<>();
		Map<Long, Boolean> programEligibilityMap = new HashMap<>();
		List<SsapApplicant> newApplicants = ssapApplicantRepository.findBySsapApplication(currentApplication);

		for (SsapApplicant applicant : newApplicants) {

			if (type.equalsIgnoreCase(APTC)) {
				programEligibilityMap.put(applicant.getId(), checkEligibility(getProgramEligibility(applicant.getId()), EligibilityType.APTC_ELIGIBILITY_TYPE));
			}
			else if (type.equalsIgnoreCase(SPTC)) {
				programEligibilityMap.put(applicant.getId(), checkEligibility(getProgramEligibility(applicant.getId()), EligibilityType.STATE_SUBSIDY_ELIGIBILITY_TYPE));
			}
		}

		for (SsapApplicant applicant : newApplicants) {

			if (programEligibilityMap.get(applicant.getId())) {

				applicantList.add(applicant);
			}
		}

		AptcRatioRequest aptcRatioRequest = new AptcRatioRequest();
		aptcRatioRequest.setCoverageYear((int) currentApplication.getCoverageYear());

		if (type.equalsIgnoreCase(APTC)) {
			aptcRatioRequest.setMaxAptc(currentApplication.getMaximumAPTC());
		}
		else if (type.equalsIgnoreCase(SPTC)) {
			aptcRatioRequest.setMaxAptc(currentApplication.getMaximumStateSubsidy());
		}

		List<AptcRatioRequest.Member> requestMemberList = new ArrayList<AptcRatioRequest.Member>();
		AptcRatioRequest.Member requestMember = null;

		for (SsapApplicant ssapApplicant : applicantList) {
			requestMember = aptcRatioRequest.new Member();
			requestMember.setAge(getAgeFromDob(DateUtil.dateToString(ssapApplicant.getBirthDate(), GhixConstants.REQUIRED_DATE_FORMAT), coverageDate));
			requestMember.setId(Long.toString(ssapApplicant.getId()));
			requestMemberList.add(requestMember);
		}
		aptcRatioRequest.setMemberList(requestMemberList);
		AptcRatioResponse aptcRatioResponse = null;

		try {
			long time = System.currentTimeMillis();
			if(LOGGER.isWarnEnabled()){
				LOGGER.warn("APTC Distribution API call start - "+ time);
			}
			Object responseObj = customGroupingUtil.getAptcDistribution(aptcRatioRequest);

			if(LOGGER.isWarnEnabled()){
				LOGGER.warn("received APTC Distribution response in " + (System.currentTimeMillis() - time) + " ms");
			}
			
			if (responseObj instanceof String) {
				String errorMessage = (String) responseObj;
				throw new GIRuntimeException("Error occoured while getting aptc ratio: " + errorMessage);
			}
			else if (responseObj instanceof Exception) {
				String errorMessage = (String) responseObj;
				throw new GIRuntimeException("Error occoured while getting aptc ratio: " + errorMessage);
			}
			else {
				aptcRatioResponse = (AptcRatioResponse) responseObj;
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception occured for calling FETCH_APTC_RATIO api for APTC", e);
			throw new GIRuntimeException("Error occoured while getting aptc ratio: " + e);
		}

		if (aptcRatioResponse != null && aptcRatioResponse.getMemberList() != null) {
			aptcMemberList = aptcRatioResponse.getMemberList();

			for (AptcRatioResponse.Member member : aptcMemberList) {
				memberAptcSptcMap.put(Long.valueOf(member.getId()), member.getAptc());
			}
			return memberAptcSptcMap;
		}
		return null;
	}

	private List<EligibilityProgram> getProgramEligibility(Long ssapApplicantId) {
		return renewalEligibilityProgramRepository.getApplicantEligibilities(ssapApplicantId);
	}

	private boolean checkEligibility(List<EligibilityProgram> programs, EligibilityType eligibilityType) {
		boolean flag1 = false;
		boolean flag2 = false;

		if (programs != null && programs.size() > 0) {

			for (EligibilityProgram eligibilityProgram : programs) {

				if (eligibilityType.toString().equals(eligibilityProgram.getEligibilityType())
						&& "TRUE".equals(eligibilityProgram.getEligibilityIndicator())) {
					flag1 = true;
				}

				if (eligibilityProgram.getEligibilityType().toString().equals(EligibilityType.EXCHANGE_ELIGIBILITY_TYPE.toString())
						&& "TRUE".equals(eligibilityProgram.getEligibilityIndicator())) {
					flag2 = true;
				}
			}
		}
		return flag1 && flag2;
	}

	private int getAgeFromDob(String dob, Date coverageStartDate) {

		if (StringUtils.isBlank(dob)) {
			return 0;
		}
		// Implementation change as per Joda Time
		LocalDate now = new LocalDate(coverageStartDate.getTime());

		Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
		LocalDate birthdate = new LocalDate(dobDate.getTime());

		Years years = Years.yearsBetween(birthdate, now);
		int age = years.getYears();
		return age;
	}

	private String preapareAndSendIND71ForHealth(RenewalApplication renewalApplication, List<RenewalApplicant> renewalApplicantList,
			SsapApplication currentApplication, SingleStreamlinedApplication singleStreamlinedApplication,
			Map<Long, String> ssapIDToGuidMap, Map<Long, SsapApplicant> applicantMap,
			List<SsapApplicant> healthApplicants, boolean healthFailSetDentalAPTCNull, Long enrollmentId,
			Map<Long, BigDecimal> memberAptcMap, Map<Long, BigDecimal> memberSptcMap) throws Exception {

		AutoRenewalResponse autoRenewalResponse = null;
		AutoRenewalRequest autoRenewalRequest = null;
		String healthRenewalStatus = FAILURE;

		try {

			if (CollectionUtils.isNotEmpty(renewalApplicantList)) {
				LOGGER.info("Started processing health enrollment for application " + currentApplication.getCaseNumber());

				ObjectFactory factory = new ObjectFactory();
				autoRenewalRequest = factory.createAutoRenewalRequest();

				Household household = getHousehold(currentApplication, singleStreamlinedApplication, ssapIDToGuidMap,
						applicantMap, renewalApplicantList, healthFailSetDentalAPTCNull, HEALTH, enrollmentId,
						memberAptcMap, memberSptcMap);
				autoRenewalRequest.setHousehold(household);

				autoRenewalResponse = invokeInd71(autoRenewalRequest);

				currentApplication = ssapApplicationRepository.findOne(currentApplication.getId());

				healthRenewalStatus = processAutoRenewalResponse(autoRenewalResponse);

				prepareApplicantsForLogging(ssapIDToGuidMap, applicantMap, healthApplicants);
			}
		}
		catch (GIException e) {
			Integer giMonitorId = logToGIMonitor(e, 50011,
					renewalApplication.getSsapApplicationId() + "--" + HEALTH + "--" + enrollmentId);
			saveGiMonitorId(renewalApplicantList, giMonitorId);
		}
		catch (Exception e) {
			Integer giMonitorId = logToGIMonitor(e, 50011,
					renewalApplication.getSsapApplicationId() + "--" + HEALTH + "--" + enrollmentId);
			saveGiMonitorId(renewalApplicantList, giMonitorId);
		}
		LOGGER.info("End processing health enrollment for application with status: " + healthRenewalStatus);
		return healthRenewalStatus;
	}

	private String preapareAndSendIND71ForDental(RenewalApplication renewalApplication, List<RenewalApplicant> renewalApplicantList,
			SsapApplication currentApplication, SingleStreamlinedApplication singleStreamlinedApplication,
			Map<Long, String> ssapIDToGuidMap, Map<Long, SsapApplicant> applicantMap,
			List<SsapApplicant> dentalApplicants, Long enrollmentId, boolean healthFailSetDentalAPTCNull,
			Long renewalYear) throws Exception {

		AutoRenewalResponse autoRenewalResponse = null;
		AutoRenewalRequest autoRenewalRequest = null;
		String dentalRenewalStatus = FAILURE;

//		DENTAL RENEWAL
		try {
			if (CollectionUtils.isNotEmpty(renewalApplicantList)) {
				LOGGER.info("Started processing dental renewal for application " + currentApplication.getCaseNumber());

// 				set var for dental enrollment existingSADPEnrollmentID = enrollmentId;
// 				create map of eligible enrollee;
//				prepareDentalEnrollee(currentApplication, ssapIDToGuidMap, singleStreamlinedApplication,  applicantMap);

// 				create auto renewal Request
				ObjectFactory factory = new ObjectFactory();
				autoRenewalRequest = factory.createAutoRenewalRequest();
				autoRenewalRequest.setHousehold(getHousehold(currentApplication, singleStreamlinedApplication, ssapIDToGuidMap, applicantMap,
						renewalApplicantList, healthFailSetDentalAPTCNull, DENTAL, enrollmentId, null, null));

// 				trigger ind71
				autoRenewalResponse = invokeInd71(autoRenewalRequest);

// 				fetch latest application
				currentApplication = ssapApplicationRepository.findOne(currentApplication.getId());

// 				process autorenewal reposne
				dentalRenewalStatus = processAutoRenewalResponse(autoRenewalResponse);

				prepareApplicantsForLogging(ssapIDToGuidMap, applicantMap, dentalApplicants);
			}
		}
		catch (GIException e) {
			Integer giMonitorId = logToGIMonitor(e, 50011,
					renewalApplication.getSsapApplicationId() + "--" + DENTAL + "--" + enrollmentId);
			saveGiMonitorId(renewalApplicantList, giMonitorId);
		}
		catch (Exception e) {
			Integer giMonitorId = logToGIMonitor(e, 50011,
					renewalApplication.getSsapApplicationId() + "--" + DENTAL + "--" + enrollmentId);
			saveGiMonitorId(renewalApplicantList, giMonitorId);
		}
		LOGGER.info("End processing dental enrollment for application with status: " + dentalRenewalStatus);
		return dentalRenewalStatus;
	}

	private Household getHousehold(SsapApplication currentApplication,
			SingleStreamlinedApplication singleStreamlinedApplication, Map<Long, String> ssapIDToGuidMap,
			Map<Long, SsapApplicant> applicantMap, List<RenewalApplicant> renewalApplicantList,
			boolean healthFailSetDentalAPTCNull, String enrollmentType, Long enrollmentId,
			Map<Long, BigDecimal> memberAptcMap, Map<Long, BigDecimal> memberSptcMap)
			throws GIException, ParseException {

		ObjectFactory factory = new ObjectFactory();
		Household household = factory.createAutoRenewalRequestHousehold();
		household.setApplicationId(currentApplication.getId());

		if (enrollmentType.equals(HEALTH)) {
			household.setProductType(ProductType.H);
		}
		else {
			household.setProductType(ProductType.D);
		}

		StringBuffer fipsCode = new StringBuffer();
		HouseHoldContact houseHoldContact = getHouseHoldContact(singleStreamlinedApplication, fipsCode);

		household.setHouseHoldContact(houseHoldContact);

		household.setIndividual(getIndividual(currentApplication, renewalApplicantList, enrollmentType,
				healthFailSetDentalAPTCNull, applicantMap, memberAptcMap, memberSptcMap));

		String coverageDate = household.getIndividual().getCoverageStartDate();
		household.setMembers(getMembers(currentApplication, houseHoldContact, singleStreamlinedApplication,
				ssapIDToGuidMap, applicantMap, renewalApplicantList, enrollmentType, coverageDate, enrollmentId, fipsCode.toString()));
		household.setResponsiblePerson(getResponsiblePerson(singleStreamlinedApplication));

		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

		if (!"MN".equalsIgnoreCase(stateCode)) {

			try {
				ResponseEntity<DesignateBroker> brokerDesignationResponse = ghixRestTemplate.exchange(
						GhixEndPoints.BrokerServiceEndPoints.GET_INDIVIDUAL_DESIGNATION, "exadmin@ghix.com",
						HttpMethod.POST, MediaType.APPLICATION_JSON, DesignateBroker.class,
						new Long(household.getIndividual().getHouseholdCaseId()).intValue());

				if (brokerDesignationResponse != null) {
					DesignateBroker designateBroker = brokerDesignationResponse.getBody();

					if (designateBroker != null && designateBroker.getStatus().toString().equalsIgnoreCase("Active")) {
						household.setUserRoleId(designateBroker.getBrokerId() + "");
						household.setUserRoleType(RoleTypeVal.AGENT);

					}
					else {
						ResponseEntity<DesignateAssister> assisterDesignationResponse = ghixRestTemplate.exchange(
								GhixEndPoints.EnrollmentEntityEndPoints.GET_INDIVIDUAL_DESIGNATION, "exadmin@ghix.com",
								HttpMethod.POST, MediaType.APPLICATION_JSON, DesignateAssister.class,
								new Long(household.getIndividual().getHouseholdCaseId()).intValue());

						if (assisterDesignationResponse != null) {
							DesignateAssister designateAssister = assisterDesignationResponse.getBody();

							if (designateAssister != null
									&& designateAssister.getStatus().toString().equalsIgnoreCase("Active")) {
								household.setUserRoleId(designateAssister.getAssisterId() + "");
								household.setUserRoleType(RoleTypeVal.ASSISTER);
							}
						}
					}
				}
			}
			catch (RestClientException e) {
				LOGGER.error("Exception finding designated assister or broker");
			}
		}
		return household;
	}

	private HouseHoldContact getHouseHoldContact(SingleStreamlinedApplication singleStreamlinedApplication, StringBuffer fipsCode) {

		ObjectFactory factory = new ObjectFactory();
		HouseHoldContact houseHoldContact = factory.createAutoRenewalRequestHouseholdHouseHoldContact();

		for (TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {

			for (HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {

				if (houseHoldMember.getPersonId() == 1) {

					if (houseHoldMember.getSocialSecurityCard() != null && !StringUtils.isBlank(houseHoldMember.getSocialSecurityCard().getSocialSecurityNumber())) {
						houseHoldContact.setHouseHoldContactFederalTaxIdNumber(
								houseHoldMember.getSocialSecurityCard().getSocialSecurityNumber());
					}

					if (houseHoldMember.getName() != null) {
						houseHoldContact.setHouseHoldContactFirstName(nullCheckedValue(houseHoldMember.getName().getFirstName()));
						houseHoldContact.setHouseHoldContactLastName(nullCheckedValue(houseHoldMember.getName().getLastName()));
						houseHoldContact.setHouseHoldContactMiddleName(nullCheckedValue(houseHoldMember.getName().getMiddleName()));
						houseHoldContact.setHouseHoldContactSuffix(nullCheckedValue(houseHoldMember.getName().getSuffix()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getHomeAddress() != null) {
						houseHoldContact.setHouseHoldContactHomeAddress1(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getStreetAddress1()));
						houseHoldContact.setHouseHoldContactHomeAddress2(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getStreetAddress2()));
						houseHoldContact.setHouseHoldContactHomeCity(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getCity()));
						houseHoldContact.setHouseHoldContactHomeState(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getState()));
						houseHoldContact.setHouseHoldContactHomeZip(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getPostalCode()));

						fipsCode.append(houseHoldMember.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode());
					}
					houseHoldContact.setHouseHoldContactId(nullCheckedValue(houseHoldMember.getApplicantGuid()));

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getContactPreferences() != null
							&& !(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("MAIL")
								|| houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("POSTAL MAIL"))) {
						houseHoldContact.setHouseHoldContactPreferredEmail(nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences().getEmailAddress()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getPhone() != null) {
						houseHoldContact.setHouseHoldContactPrimaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null
										? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()
										: nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getOtherPhone() != null 
							&& nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null) {
						houseHoldContact.setHouseHoldContactSecondaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}
				}
			}
		}
		return houseHoldContact;
	}

	private Individual getIndividual(SsapApplication currentApplication, List<RenewalApplicant> renewalApplicantList,
			String enrollmentType, boolean healthFailSetDentalAPTCNull, Map<Long, SsapApplicant> applicantMap,
			Map<Long, BigDecimal> memberAptcMap, Map<Long, BigDecimal> memberSptcMap)
			throws GIException, ParseException {

		ObjectFactory factory = new ObjectFactory();
		Individual individual = factory.createAutoRenewalRequestHouseholdIndividual();

		Date coverageDate = ssapCloneApplicationService.getCoverageStartDate(currentApplication);
		individual.setCoverageStartDate(getFormatedDate(coverageDate));
		individual.setEnrollmentType(EnrollmentVal.A);
		String planId = "";

		for (RenewalApplicant renewalApplicant : renewalApplicantList) {

			if (StringUtils.isNotBlank(renewalApplicant.getPlanId())) {
				planId = renewalApplicant.getPlanId();
				break;
			}
		}
		individual.setPlanId(planId);
		BigDecimal totalAptc = null;
		BigDecimal totalSptc = null;

		if (enrollmentType.equals(HEALTH)) {

			if (memberAptcMap != null) {

				for (RenewalApplicant renewalApplicant : renewalApplicantList) {

					if (memberAptcMap.containsKey(renewalApplicant.getSsapApplicantId())) {
						if(totalAptc == null)
						totalAptc = new BigDecimal("0.0");
						totalAptc = totalAptc.add(memberAptcMap.get(renewalApplicant.getSsapApplicantId()));
					}
				}
			}
			if(totalAptc!=null){
				individual.setAptc(totalAptc.floatValue());
			}

			if (memberSptcMap != null) {

				for (RenewalApplicant renewalApplicant : renewalApplicantList) {

					if (memberSptcMap.containsKey(renewalApplicant.getSsapApplicantId())) {
						if(totalSptc == null)
						totalSptc = new BigDecimal("0.0");
						totalSptc = totalSptc.add(memberSptcMap.get(renewalApplicant.getSsapApplicantId()));
					}
				}
			}
			individual.setStateSubsidy(totalSptc);
		}
		else if (healthFailSetDentalAPTCNull) {
			individual.setAptc(null); // As Health renewals not suceesfull setting dental APTC as null
		}
		else {

			final String applyAptcForDental = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_APPLY_APTC_FOR_DENTAL);

			if (applyAptcForDental != null && applyAptcForDental.equalsIgnoreCase("Y")) {
				individual.setAptc(ssapCloneApplicationService.getRemainingAPTCForDental(currentApplication));
			}
		}

		if (enrollmentType.equals(HEALTH)) {

			List<SsapApplicant> groupApplicants = getApplicantListToCreateEvents(currentApplication, renewalApplicantList, applicantMap);
			Set<String> set = new LinkedHashSet<String>();
			List<String> groupCSList = new ArrayList<String>();

			for (SsapApplicant ssapApplicant : groupApplicants) {

				if ("QHP".equalsIgnoreCase(ssapApplicant.getEligibilityStatus())) {

					if (StringUtils.isBlank(ssapApplicant.getCsrLevel())) {
						set.add("CS1");
					}
					else {
						set.add(ssapApplicant.getCsrLevel());
					}
				}
			}
			groupCSList.addAll(set);
			String groupCS = getLowPriorityCSLevel(groupCSList);
			individual.setCsr(groupCS);
		}
		setEhbAmount(currentApplication, individual);

		if (currentApplication.getCmrHouseoldId() != null) {
			individual.setHouseholdCaseId(currentApplication.getCmrHouseoldId().longValue());
		}
		return individual;
	}

	private Members getMembers(SsapApplication currentApplication, HouseHoldContact houseHoldContact,
			SingleStreamlinedApplication singleStreamlinedApplication, Map<Long, String> ssapIDToGuidMap,
			Map<Long, SsapApplicant> applicantMap, List<RenewalApplicant> renewalApplicantList, String enrollmentType,
			String coverageDate, Long enrollmentId, String fipsCode) throws ParseException, GIException {

		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		ObjectFactory factory = new ObjectFactory();
		Members members = factory.createAutoRenewalRequestHouseholdMembers();
		boolean isEveryoneUnder30 = true;
		String primaryTaxFilerPersonId = getPTFPersonId(singleStreamlinedApplication);
		Collection<SsapApplicant> ssapApplicantCollection = applicantMap.values();
		List<Long> renewSsapApplicantPersonIdList = new ArrayList<>();
		Map<Long, String> memberReasonCode = new HashMap<Long, String>();

		for (RenewalApplicant renewalApplicant : renewalApplicantList) {

			for (SsapApplicant ssapApplicant : ssapApplicantCollection) {

				if (renewalApplicant.getSsapApplicantId().equals(ssapApplicant.getId())) {

					if (renewalApplicant.getMemberFalloutReasonCode() != null) {
						LookupValue lookupValue = lookupService.findLookupValuebyId(renewalApplicant.getMemberFalloutReasonCode());

						if (lookupValue != null) {
							memberReasonCode.put(ssapApplicant.getPersonId(), lookupValue.getLookupValueCode());
						}
					}
					renewSsapApplicantPersonIdList.add(ssapApplicant.getPersonId());
				}
			}
		}

		List<HouseholdMember> hhMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		
		HouseholdMember responsiblePerson = getApplicantJson(hhMembers, Long.parseLong(primaryTaxFilerPersonId));

		for (TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {

			for (HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {

				if (ssapIDToGuidMap.get(new Long(houseHoldMember.getPersonId())) != null
						&& houseHoldMember.getApplyingForCoverageIndicator()
						&& renewSsapApplicantPersonIdList.contains(Long.valueOf(houseHoldMember.getPersonId()))) {

					Member member = factory.createAutoRenewalRequestHouseholdMembersMember();
					member.setMemberId(ssapIDToGuidMap.get(new Long(houseHoldMember.getPersonId())));
					member.setNewPersonFLAG(YesNoVal.N);
					boolean newMember = false;

					if (memberReasonCode.get(new Long(houseHoldMember.getPersonId())) != null 
							&& "NEW_MEMBER".equalsIgnoreCase(memberReasonCode.get(new Long(houseHoldMember.getPersonId())))) {
						newMember = true;
					}

					if (newMember) {
						member.setNewPersonFLAG(YesNoVal.Y);
					}
					member.setCatastrophicEligible(isExemptHousehold(currentApplication) ? YesNoVal.Y : YesNoVal.N);
					member.setChildOnlyPlanEligibile(YesNoVal.N);

					if (houseHoldMember.getLivesWithHouseholdContactIndicator()) {
						member.setCountyCode(nullCheckedValue(fipsCode));
						member.setHomeAddress1(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeAddress1()));
						member.setHomeAddress2(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeAddress2()));
						member.setHomeCity(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeCity()));
						member.setHomeState(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeState()));
						member.setHomeZip(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeZip()));

					}
					else {
						Address otherAddress = new Address();
						if(houseHoldMember.getOtherAddress() != null && houseHoldMember.getOtherAddress().getAddress() != null && houseHoldMember.getOtherAddress().getAddress().getPostalCode() != null){
							otherAddress = houseHoldMember.getOtherAddress().getAddress();	
						}else if(singleStreamlinedApplication.getTaxHousehold().get(0).getOtherAddresses() != null && singleStreamlinedApplication.getTaxHousehold().get(0).getOtherAddresses().size() > 0){	
							for(Address otherAddressObj : singleStreamlinedApplication.getTaxHousehold().get(0).getOtherAddresses()) {
								if(otherAddressObj.getAddressId() == houseHoldMember.getAddressId()) {
									otherAddress = otherAddressObj;
								}
							}							
						}
						
						member.setHomeAddress1(nullCheckedValue(otherAddress.getStreetAddress1()));
						member.setHomeAddress2(nullCheckedValue(otherAddress.getStreetAddress2()));
						member.setHomeCity(nullCheckedValue(otherAddress.getCity()));
						member.setHomeState(nullCheckedValue(otherAddress.getState()));
						member.setHomeZip(nullCheckedValue(otherAddress.getPostalCode()));
						member.setCountyCode(otherAddress.getCountyCode()!=null?otherAddress.getCountyCode():otherAddress.getCounty());
					}

					if (houseHoldMember.getDateOfBirth() != null) {
						member.setDob(nullCheckedValue(getFormatedDate(houseHoldMember.getDateOfBirth()).toString()));
					}

					if (!newMember) {

						if (enrollmentType.equals(HEALTH) && enrollmentId != null && enrollmentId != 0) {
							member.setExistingMedicalEnrollmentID(nullCheckedValue(String.valueOf(enrollmentId)));
						}
						else {
							member.setExistingSADPEnrollmentID(nullCheckedValue(String.valueOf(enrollmentId)));
						}
					}
					member.setFinancialHardshipExemption(isExemptHousehold(currentApplication) ? YesNoVal.Y : YesNoVal.N);
					member.setFirstName(nullCheckedValue(houseHoldMember.getName().getFirstName()));

					if ("male".equalsIgnoreCase(houseHoldMember.getGender())) {
						member.setGenderCode("M");
					}
					else if ("female".equalsIgnoreCase(houseHoldMember.getGender())) {
						member.setGenderCode("F");
					}
					
					if(responsiblePerson != null) {
						for (BloodRelationship relationship : responsiblePerson.getBloodRelationship()) {
	
							if (houseHoldMember.getPersonId().toString().equalsIgnoreCase(relationship.getIndividualPersonId())) {
	
								Relationship ind71Relationship = factory.createAutoRenewalRequestHouseholdMembersMemberRelationship();
								String memberId = ssapIDToGuidMap.get(Long.valueOf(relationship.getRelatedPersonId()));
	
								if (memberId != null) {
									ind71Relationship.setMemberId(nullCheckedValue(memberId));
									ind71Relationship.setRelationshipCode(relationship.getRelation());
									member.getRelationship().add(ind71Relationship);
								}
	
								if (relationship.getRelatedPersonId().equalsIgnoreCase(primaryTaxFilerPersonId)
										&& relationship.getIndividualPersonId().equalsIgnoreCase(houseHoldMember.getPersonId().toString())) {
									member.setResponsiblePersonRelationship(nullCheckedValue(relationship.getRelation()));
								}
							}
						}    
					}

					if (houseHoldMember.getName() != null) {
						member.setLastName(nullCheckedValue(houseHoldMember.getName().getLastName()));
						member.setSuffix(nullCheckedValue(houseHoldMember.getName().getSuffix()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getMailingAddressSameAsHomeAddressIndicator()) {
						member.setMailingAddress1(nullCheckedValue(member.getHomeAddress1()));
						member.setMailingAddress2(nullCheckedValue(member.getHomeAddress2()));
						member.setMailingCity(nullCheckedValue(member.getHomeCity()));
						member.setMailingState(nullCheckedValue(member.getHomeState()));
						member.setMailingZip(nullCheckedValue(member.getHomeZip()));
					}
					else if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getMailingAddress() != null) {
						member.setMailingAddress1(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getStreetAddress1()));
						member.setMailingAddress2(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getStreetAddress2()));
						member.setMailingCity(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getCity()));
						member.setMailingState(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getState()));
						member.setMailingZip(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getPostalCode()));
					}

					if ("CA".equalsIgnoreCase(stateCode)) {

						if (houseHoldMember.getMarriedIndicatorCode() != null) {
							member.setMaritalStatusCode(houseHoldMember.getMarriedIndicatorCode());
						}
						else {
							member.setMaritalStatusCode("R");
						}
					}
					else {

						if ("Yes".equalsIgnoreCase(ReferralUtil.converToYesNo(houseHoldMember.getMarriedIndicator()))) {
							member.setMaritalStatusCode("M");
						}
						else {
							member.setMaritalStatusCode("R");
						}
					}

					if (houseHoldMember.getCitizenshipImmigrationStatus() != null) {

						if (houseHoldMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null
								&& houseHoldMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator()) {
							member.setCitizenshipStatusCode(US_CITIZEN);
						}
						else {
							member.setCitizenshipStatusCode(NON_US_CITIZEN);
						}
					}

					if (houseHoldMember.getName() != null) {
						member.setMiddleName(nullCheckedValue(houseHoldMember.getName().getMiddleName()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getContactPreferences() != null
							&& !!(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("Email")
									|| houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("POSTAL MAIL"))) {
						member.setPreferredEmail(nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences().getEmailAddress()) != null
									? houseHoldMember.getHouseholdContact().getContactPreferences().getEmailAddress()
									: houseHoldContact.getHouseHoldContactPreferredEmail());
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getContactPreferences() != null
							&& !!houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("TextMessage")) {
						member.setPreferredPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null
										? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()
										: (nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()) != null
											? houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()
											: houseHoldContact.getHouseHoldContactPrimaryPhone()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getPhone() != null) {
						/*
						 * if(houseHoldMember.getHouseholdContact().getContactPreferences() != null &&
						 * nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences(
						 * ).getPreferredContactMethod()) != null &&
						 * !(houseHoldMember.getHouseholdContact().getContactPreferences().
						 * getPreferredContactMethod().equalsIgnoreCase("EMAIL") ||
						 * houseHoldMember.getHouseholdContact().getContactPreferences().
						 * getPreferredContactMethod().equalsIgnoreCase("MAIL"))) {
						 * member.setPreferredPhone(nullCheckedValue(houseHoldMember.getHouseholdContact
						 * ().getPhone().getPhoneNumber()) != null ?
						 * houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber() :
						 * (nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().
						 * getPhoneNumber()) != null ?
						 * houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber() :
						 * houseHoldContact.getHouseHoldContactPrimaryPhone())); }
						 */
						member.setPrimaryPhone(nullCheckedValue(
								houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null
										? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()
										: (nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()) != null
											? houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()
											: houseHoldContact.getHouseHoldContactPrimaryPhone()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getOtherPhone() != null 
							&& nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null) {
						member.setSecondaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getContactPreferences() != null) {

						String preferredSpokenLanguage = houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage();
						String languageSpokenCode = null;

						if (StringUtils.isNotBlank(preferredSpokenLanguage)) {
							languageSpokenCode = LanguageEnum.getCode(preferredSpokenLanguage);
						}
						if (StringUtils.isNotBlank(languageSpokenCode)) {
							member.setSpokenLanguageCode(languageSpokenCode);
						}

						String languageWrittenCode = null;
						String preferredWrittenLanguage = houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage();

						if (StringUtils.isNotBlank(preferredWrittenLanguage)) {
							languageWrittenCode = LanguageEnum.getCode(preferredWrittenLanguage);
						}
						if (StringUtils.isNotBlank(languageWrittenCode)) {
							member.setWrittenLanguageCode(languageWrittenCode);
						}
					}

					if (houseHoldMember.getSocialSecurityCard() != null) {
						member.setSsn(nullCheckedValue(houseHoldMember.getSocialSecurityCard().getSocialSecurityNumber()));
					}

					if (houseHoldMember.getPersonId() != null
							&& null != applicantMap.get(houseHoldMember.getPersonId().longValue())) {
						member.setTobacco("Y".equalsIgnoreCase(applicantMap.get(houseHoldMember.getPersonId().longValue()).getTobaccouser())
											? YesNoVal.Y
											: YesNoVal.N);
					}

					if (houseHoldMember.getEthnicityAndRace() != null) {
						// List<Ethnicity> ethnicities =
						// houseHoldMember.getEthnicityAndRace().getEthnicity();
						List<Race> races = houseHoldMember.getEthnicityAndRace().getRace();

						if (races != null) {
							for (Race race : races) {
								com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member.Race ind71Race = factory
										.createAutoRenewalRequestHouseholdMembersMemberRace();
								/*
								 * if(race.getCode().equalsIgnoreCase("0000-0")){ continue; }
								 */
								if (race.getCode().equalsIgnoreCase("2131-1")) {
									ind71Race.setDescription(race.getOtherLabel());
								}
								ind71Race.setRaceEthnicityCode(race.getCode());
								member.getRace().add(ind71Race);
							}
						}
						/*
						 * if(ethnicities != null) { for (Ethnicity race : ethnicities) {
						 * com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.
						 * Household.Members.Member.Race ind71Race =
						 * factory.createAutoRenewalRequestHouseholdMembersMemberRace();
						 * if(race.getCode().equalsIgnoreCase("0000-0")){ continue; }
						 * if(race.getCode().equalsIgnoreCase("2131-1")){
						 * ind71Race.setDescription(race.getOtherLabel()); }
						 * ind71Race.setRaceEthnicityCode(race.getCode());
						 * member.getRace().add(ind71Race); } }
						 */
					}

					if (getAgeFromDob(member.getDob(), REQUIRED_DATE_FORMAT.parse(coverageDate)) >= HARDSHIP_EXEMPTION_AGE) {
						isEveryoneUnder30 = false;
					}
					member.setExternalMemberId(houseHoldMember.getExternalId());
					members.getMember().add(member);
				}
			}
		}

		if (isEveryoneUnder30) {
			LOGGER.debug("Not hardship exempt. But everyone under 30");

			for (int i = 0; i < members.getMember().size(); i++) {
				members.getMember().get(i).setCatastrophicEligible(YesNoVal.Y);
			}
		}
		return members;
	}

	private ResponsiblePerson getResponsiblePerson(SingleStreamlinedApplication singleStreamlinedApplication) {

		ObjectFactory factory = new ObjectFactory();
		ResponsiblePerson responsiblePerson = factory.createAutoRenewalRequestHouseholdResponsiblePerson();

		Integer primaryTaxFilerPersonId = singleStreamlinedApplication.getPrimaryTaxFilerPersonId();

		if (primaryTaxFilerPersonId == null || primaryTaxFilerPersonId == 0) {
			primaryTaxFilerPersonId = 1;
		}

		for (TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {

			for (HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {

				if (houseHoldMember.getPersonId() == primaryTaxFilerPersonId) {

					if (houseHoldMember.getName() != null) {
						responsiblePerson.setResponsiblePersonFirstName(nullCheckedValue(houseHoldMember.getName().getFirstName()));
						responsiblePerson.setResponsiblePersonLastName(nullCheckedValue(houseHoldMember.getName().getLastName()));
						responsiblePerson.setResponsiblePersonMiddleName(nullCheckedValue(houseHoldMember.getName().getMiddleName()));
						responsiblePerson.setResponsiblePersonSuffix(nullCheckedValue(houseHoldMember.getName().getSuffix()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getHomeAddress() != null) {
						responsiblePerson.setResponsiblePersonHomeAddress1(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getStreetAddress1()));
						responsiblePerson.setResponsiblePersonHomeAddress2(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getStreetAddress2()));
						responsiblePerson.setResponsiblePersonHomeCity(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getCity()));
						responsiblePerson.setResponsiblePersonHomeState(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getState()));
						responsiblePerson.setResponsiblePersonHomeZip(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getPostalCode()));
					}
					responsiblePerson.setResponsiblePersonId(nullCheckedValue(houseHoldMember.getApplicantGuid()));

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getContactPreferences() != null
							&& !!(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("MAIL")
								|| houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("POSTAL MAIL"))) {
						responsiblePerson.setResponsiblePersonPreferredEmail(nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences().getEmailAddress()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getPhone() != null) {

						/*
						 * if(houseHoldMember.getHouseholdContact().getContactPreferences() != null &&
						 * nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences(
						 * ).getPreferredContactMethod()) != null &&
						 * !(houseHoldMember.getHouseholdContact().getContactPreferences().
						 * getPreferredContactMethod().equalsIgnoreCase("EMAIL") ||
						 * houseHoldMember.getHouseholdContact().getContactPreferences().
						 * getPreferredContactMethod().equalsIgnoreCase("MAIL"))) {
						 * responsiblePerson.setResponsiblePersonPreferredPhone(nullCheckedValue(
						 * houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null ?
						 * houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber() :
						 * nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().
						 * getPhoneNumber())); }
						 */
						responsiblePerson.setResponsiblePersonPrimaryPhone(nullCheckedValue(
								houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null
										? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()
										: nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone()
												.getPhoneNumber()));
					}

					if (houseHoldMember.getHouseholdContact() != null
							&& houseHoldMember.getHouseholdContact().getOtherPhone() != null && nullCheckedValue(
									houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null) {
						responsiblePerson.setResponsiblePersonSecondaryPhone(nullCheckedValue(
								houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}

					if (houseHoldMember.getSocialSecurityCard() != null) {
						responsiblePerson.setResponsiblePersonSsn(
								nullCheckedValue(houseHoldMember.getSocialSecurityCard().getSocialSecurityNumber()));
					}
					responsiblePerson.setResponsiblePersonType("QD");
				}
			}
		}
		return responsiblePerson;
	}

	private AutoRenewalResponse invokeInd71(AutoRenewalRequest autoRenewalRequest) throws Exception {

		ObjectFactory factory = new ObjectFactory();
		AutoRenewalResponse autoRenewalResponse = factory.createAutoRenewalResponse();
		String autoRenewalServiceDefaultUri = GhixEndPoints.PLANDISPLAY_URL + "webservice/plandisplay/endpoints/AutoRenewalService.wsdl";

		try {
			long time = System.currentTimeMillis();
		    LOGGER.info("calling ind71 for " + autoRenewalRequest.getHousehold().getApplicationId());
		
			ind71WebServiceTemplate.setDefaultUri(autoRenewalServiceDefaultUri);
			autoRenewalResponse = (AutoRenewalResponse) ind71WebServiceTemplate.marshalSendAndReceive(autoRenewalRequest);
			
			LOGGER.info("received ind71 response for " + autoRenewalRequest.getHousehold().getApplicationId() + " in " + (System.currentTimeMillis() - time) + " ms");
			
			if(LOGGER.isWarnEnabled()){
				LOGGER.warn("received ind71 response for " + autoRenewalRequest.getHousehold().getApplicationId() + " in " + (System.currentTimeMillis() - time) + " ms");
			}
		}catch(SoapFaultClientException e){
			
			validateAndLogError(autoRenewalRequest, autoRenewalResponse, autoRenewalServiceDefaultUri);
			throw e;
			
		}catch (Exception e) {
			
			validateAndLogError(autoRenewalRequest, autoRenewalResponse, autoRenewalServiceDefaultUri);
			throw e;
		}
		
		
		return autoRenewalResponse;
	}

	private void validateAndLogError(AutoRenewalRequest autoRenewalRequest, AutoRenewalResponse autoRenewalResponse,
									 String autoRenewalServiceDefaultUri) throws Exception {

		StringWriter autoRenewalRequestStringWriter = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(AutoRenewalRequest.class);
		context.createMarshaller().marshal(autoRenewalRequest, autoRenewalRequestStringWriter);

		GIWSPayload giwsPayload = populateGiWSPayload(autoRenewalRequestStringWriter, autoRenewalResponse, autoRenewalServiceDefaultUri, 
				                                      "AUTO_ENROLL_BATCH_IND71", autoRenewalRequest.getHousehold().getApplicationId(), null);

		InputStream inStream = getClass().getClassLoader().getResourceAsStream("/autoRenewal/AutoRenewal.xsd");
		Source schemaSource = new StreamSource(inStream);
		Source xmlFile = new StreamSource(new StringReader(autoRenewalRequestStringWriter.toString()));
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		AutoRenewalValidationHandler validationHandler = new AutoRenewalValidationHandler();
		Schema schema;

		try {
			schema = schemaFactory.newSchema(schemaSource);
			Validator validator = schema.newValidator();
			validator.setErrorHandler(validationHandler);
			validator.validate(xmlFile);

		} catch (SAXException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}

		String validationError = validationHandler.getErrors();

		giwsPayload.setExceptionMessage(validationError);

		giwsPayloadRepository.save(giwsPayload);
	}

	private GIWSPayload populateGiWSPayload(StringWriter requestStringWriter, AutoRenewalResponse autoRenewalResponse, String endpointUrl, String endpointFunction, 
			                         Long applicationId, String exceptionMessage) {

		GIWSPayload giWSPayload = new GIWSPayload();

		giWSPayload.setCreatedTimestamp(new TSDate());
		giWSPayload.setEndpointUrl(endpointUrl);
		
		if(requestStringWriter != null) {
			giWSPayload.setRequestPayload(requestStringWriter.toString());
		}
		
		giWSPayload.setEndpointFunction(endpointFunction);
		giWSPayload.setSsapApplicationId(applicationId);
		giWSPayload.setExceptionMessage(exceptionMessage);
		
		return giwsPayloadRepository.save(giWSPayload);
		
	}

	private String processAutoRenewalResponse(AutoRenewalResponse autoRenewalResponse) throws Exception {

		if (autoRenewalResponse != null) {
			return autoRenewalResponse.getResponseCode();
		}
		return null;
	}

	private void prepareApplicantsForLogging(Map<Long, String> ssapIDToGuidMap, Map<Long, SsapApplicant> applicantMap,
			List<SsapApplicant> healthApplicants) {

		if (ssapIDToGuidMap != null) {
			for (Long key : ssapIDToGuidMap.keySet()) {
				healthApplicants.add(applicantMap.get(key));
			}
		}
	}

	/**
	 * Method is used to get Applicant List to Create Events.
	 */
	private List<SsapApplicant> getApplicantListToCreateEvents(SsapApplication currentApplication,
			List<RenewalApplicant> renewalApplicantList, Map<Long, SsapApplicant> applicantMap) {

		List<SsapApplicant> applicantListToCreateEvents = new ArrayList<>();

		for (RenewalApplicant renewalApplicant : renewalApplicantList) {

			for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {

				if (ssapApplicant.getId() == renewalApplicant.getSsapApplicantId()) {
					applicantListToCreateEvents.add(applicantMap.get(ssapApplicant.getPersonId()));
		 		}
			}
		}
		return applicantListToCreateEvents;
	}

	private Integer getReasonCode(String lookupValueCode) {
		Integer reasonCode = lookupService.getlookupValueIdByTypeANDLookupValueCode("RENEWALS", lookupValueCode);
		return reasonCode;
	}

	private void updateApplicationStatus(Map<Long, String> healthStatusMap, Map<Long, String> dentalStatusMap,
			Map<Long, List<RenewalApplicant>> applicantEnrollmentIdHealthtMap,
			Map<Long, List<RenewalApplicant>> applicantEnrollmentIdDentalMap, SsapApplication currentApplication,
			RenewalApplication renewalApplication) {

		Map<RenewalStatus, Integer> applicationStatusMap = new HashMap<>();

		for (Long enrollmentId : healthStatusMap.keySet()) {
			updateApplicantStatus(enrollmentId, healthStatusMap.get(enrollmentId), applicationStatusMap, applicantEnrollmentIdHealthtMap, HEALTH);
		}
		
		rollupAppliationRenewalStatus(renewalApplication, applicationStatusMap, Integer.valueOf(healthStatusMap.size()), HEALTH);
		
		applicationStatusMap = new HashMap<>(); // RESET applicationStatusMap

		for (Long enrollmentId : dentalStatusMap.keySet()) {
			updateApplicantStatus(enrollmentId, dentalStatusMap.get(enrollmentId), applicationStatusMap, applicantEnrollmentIdDentalMap, DENTAL);
		}

		rollupAppliationRenewalStatus(renewalApplication, applicationStatusMap, Integer.valueOf(dentalStatusMap.size()), DENTAL);

		if ((RenewalStatus.MANUAL_ENROLMENT.equals(renewalApplication.getDentalRenewalStatus())
				|| RenewalStatus.NOT_TO_CONSIDER.equals(renewalApplication.getDentalRenewalStatus())
				|| RenewalStatus.RENEWED.equals(renewalApplication.getDentalRenewalStatus()))
				&& (RenewalStatus.MANUAL_ENROLMENT.equals(renewalApplication.getHealthRenewalStatus())
						|| RenewalStatus.NOT_TO_CONSIDER.equals(renewalApplication.getHealthRenewalStatus())
						|| RenewalStatus.RENEWED.equals(renewalApplication.getHealthRenewalStatus()))) {
			renewalApplication.setHouseholdRenewalStatus(RenewalStatus.RENEWAL_PROCESS_COMPLETED);
		}
		renewalApplicationRepository.save(renewalApplication);
	}

	private void updateApplicantStatus(Long enrollmentId, String responseCode, Map<RenewalStatus, Integer> applicationStatusMap,
									   Map<Long, List<RenewalApplicant>> applicantEnrollmentIdHealthtMap, String type) {

		List<RenewalApplicant> applicantList = null;

		if (_200.equalsIgnoreCase(responseCode)) {
			applicationStatusMap.putIfAbsent(RenewalStatus.RENEWED, 0);
			applicationStatusMap.put(RenewalStatus.RENEWED, applicationStatusMap.get(RenewalStatus.RENEWED) + 1);
			applicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);

			for (RenewalApplicant renewalApplicant : applicantList) {
				renewalApplicant.setRenewalStatus(RenewalStatus.RENEWED);

				if (HEALTH.equals(type)) {
					renewalApplicant.setGroupRenewalStatus(RenewalStatus.RENEWED);
				}
			}
			renewalApplicantRepository.save(applicantList);
			
		}else if (_131.equalsIgnoreCase(responseCode) || _129.equalsIgnoreCase(responseCode)) {
			applicationStatusMap.putIfAbsent(RenewalStatus.MANUAL_ENROLMENT, 0);
			applicationStatusMap.put(RenewalStatus.MANUAL_ENROLMENT, applicationStatusMap.get(RenewalStatus.MANUAL_ENROLMENT) + 1);
			applicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);

			for (RenewalApplicant renewalApplicant : applicantList) {
				renewalApplicant.setRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);

				if (_131.equalsIgnoreCase(responseCode)) {
					renewalApplicant.setMemberFalloutReasonCode(getReasonCode("NO_PLAN"));
					
				}else if (_129.equalsIgnoreCase(responseCode)) {
					renewalApplicant.setMemberFalloutReasonCode(getReasonCode("NO_CROSS_WALK_PLAN"));
					
				}
				
				if (HEALTH.equals(type)) {
					renewalApplicant.setGroupRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);

					if (_131.equalsIgnoreCase(responseCode)) {
						renewalApplicant.setGroupReasonCode(getReasonCode("NO_PLAN"));
						
					}else if (_129.equalsIgnoreCase(responseCode)) {
						renewalApplicant.setGroupReasonCode(getReasonCode("NO_CROSS_WALK_PLAN"));
						
					}
				}
			}
			
			renewalApplicantRepository.save(applicantList);
			
		}else if(_125.equalsIgnoreCase(responseCode)){
			
			applicationStatusMap.putIfAbsent(RenewalStatus.APP_ERROR, 0);
			applicationStatusMap.put(RenewalStatus.APP_ERROR, applicationStatusMap.get(RenewalStatus.APP_ERROR) + 1);
			
			applicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);

			for (RenewalApplicant renewalApplicant : applicantList) {
				
				renewalApplicant.setRenewalStatus(RenewalStatus.APP_ERROR);
				renewalApplicant.setMemberFalloutReasonCode(getReasonCode("ENMTEXP"));

				if (HEALTH.equals(type)) {
					renewalApplicant.setGroupRenewalStatus(RenewalStatus.APP_ERROR);
					renewalApplicant.setGroupReasonCode(getReasonCode("ENMTEXP"));
				}
			}
			
			renewalApplicantRepository.save(applicantList);
			
		}else {
			applicationStatusMap.putIfAbsent(RenewalStatus.APP_ERROR, 0);
			applicationStatusMap.put(RenewalStatus.APP_ERROR, applicationStatusMap.get(RenewalStatus.APP_ERROR) + 1);
			applicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);

			for (RenewalApplicant renewalApplicant : applicantList) {
				renewalApplicant.setRenewalStatus(RenewalStatus.APP_ERROR);
				renewalApplicant.setMemberFalloutReasonCode(getReasonCode("AUTO_RENEWAL_ERROR"));

				if (HEALTH.equals(type)) {
					renewalApplicant.setGroupRenewalStatus(RenewalStatus.APP_ERROR);
					renewalApplicant.setGroupReasonCode(getReasonCode("AUTO_RENEWAL_ERROR"));
				}
			}
			
			renewalApplicantRepository.save(applicantList);
		}
	}

	private void rollupAppliationRenewalStatus(RenewalApplication renewalApplication,
			Map<RenewalStatus, Integer> applicationStatusMap, Integer groupCount, String insuranceType) {

		if (groupCount > 0) {

			if (applicationStatusMap.containsKey(RenewalStatus.APP_ERROR)
					&& applicationStatusMap.get(RenewalStatus.APP_ERROR) > 0) {

				if (HEALTH.equalsIgnoreCase(insuranceType)) {
					renewalApplication.setHealthRenewalStatus(RenewalStatus.APP_ERROR);
				}
				else {
					renewalApplication.setDentalRenewalStatus(RenewalStatus.APP_ERROR);
				}
			}
			else if (applicationStatusMap.containsKey(RenewalStatus.RENEWED)
					&& groupCount.equals(applicationStatusMap.get(RenewalStatus.RENEWED))) {

				if (HEALTH.equalsIgnoreCase(insuranceType)) {
					renewalApplication.setHealthRenewalStatus(RenewalStatus.RENEWED);
				}
				else {
					renewalApplication.setDentalRenewalStatus(RenewalStatus.RENEWED);
				}
			}
			else if (applicationStatusMap.containsKey(RenewalStatus.RENEWED)) {

				if (HEALTH.equalsIgnoreCase(insuranceType)) {
					renewalApplication.setHealthRenewalStatus(RenewalStatus.PARTIAL_RENEWED);
				}
				else {
					renewalApplication.setDentalRenewalStatus(RenewalStatus.PARTIAL_RENEWED);
				}
			}
			else {

				if (HEALTH.equalsIgnoreCase(insuranceType)) {
					renewalApplication.setHealthRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);
				}
				else {
					renewalApplication.setDentalRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);
				}
			}
		}
	}

	private String nullCheckedValue(String value) {

		if (StringUtils.isBlank(value)) {
			return null;
		}
		return value;
	}

	private String getFormatedDate(Date date) throws ParseException {
		return REQUIRED_DATE_FORMAT.format(date.getTime());
	}

	private String getLowPriorityCSLevel(List<String> hhMemberCSLevels) {

		List<String> csPriorityList = new ArrayList<>(Arrays.asList("CS1", "CS4", "CS5", "CS6", "CS3", "CS2"));
		String CSLevel = null;
		Integer lowestIndex = null;
		Integer currentIndex = null;

		if (CollectionUtils.isNotEmpty(hhMemberCSLevels)) {

			for (String memberCS : hhMemberCSLevels) {
				currentIndex = csPriorityList.indexOf(memberCS);

				if (lowestIndex == null || lowestIndex > currentIndex) {
					lowestIndex = currentIndex;
				}
			}
			
			if(lowestIndex != null) {
				CSLevel = csPriorityList.get(lowestIndex);
			}
		}
		return CSLevel;
	}

	private void setEhbAmount(SsapApplication currentApplication, Individual individual) {
		AptcHistory aptcHistory = aptcHistoryRepository.findLatestSlcspAmount(currentApplication.getId());
		if (aptcHistory != null && aptcHistory.getSlcspAmount() != null) {
			individual.setEhbAmount(new Float(aptcHistory.getSlcspAmount().toString()));
		}
	}

	private String getPTFPersonId(SingleStreamlinedApplication singleStreamlinedApplication) {
		String primaryTaxFilerPersonId = "1";// Default behaviour

		for (TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {

			for (HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {

				if (houseHoldMember != null && houseHoldMember.isPrimaryTaxFiler()) {
					primaryTaxFilerPersonId = String.valueOf(houseHoldMember.getPersonId());
				}
			}
		}
		return primaryTaxFilerPersonId;
	}

	private HouseholdMember getApplicantJson(List<HouseholdMember> members, long personId) {
		HouseholdMember applicantJson = null;

		for (HouseholdMember householdMember : members) {

			if (householdMember.getPersonId().compareTo((int) personId) == 0) {
				applicantJson = householdMember;
				break;
			}
		}
		return applicantJson;
	}

	private boolean isExemptHousehold(SsapApplication currentApplication) {
		return currentApplication.getExemptHousehold() != null
				&& currentApplication.getExemptHousehold().trim().length() > 0
				&& currentApplication.getExemptHousehold().equalsIgnoreCase(Y);
	}
}
