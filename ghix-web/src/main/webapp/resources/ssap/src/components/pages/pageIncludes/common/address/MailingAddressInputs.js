import React, { Fragment } from 'react';
import _ from 'lodash';

import UniversalInput from '../../../../../lib/UniversalInput';

import INPUT_TYPES from '../../../../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../../../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../../../../constants/validationTypes';

const MailingAddressInputs = props => {

    const {
        _postalCode,
        _countyCode,
        _streetAddress1,
        _streetAddress2,
        _state,
        _city,
        currentData,
        counties,
        ...restProps
    } = props;

    const cleanedRestProps = _.omit(restProps, 'children');

    return (
        <Fragment>
            <UniversalInput
                label={{
                    text: 'Address 1',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'streetAddress1',
                    helperClasses: [],
                    value: _streetAddress1,
                    placeholder: 'Address 1',
                    validationType: VALIDATION_TYPES.ADDRESS_LINE1,
                    currentData: currentData,
                    fields: ['householdContact', 'mailingAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Address 2',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'streetAddress2',
                    helperClasses: [],
                    value: _streetAddress2,
                    placeholder: 'Address 2',
                    validationType: VALIDATION_TYPES.ADDRESS_LINE2,
                    currentData: currentData,
                    fields: ['householdContact', 'mailingAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'City',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'city',
                    helperClasses: [],
                    value: _city,
                    placeholder: 'City',
                    validationType: VALIDATION_TYPES.CITY,
                    currentData: currentData,
                    fields: ['householdContact', 'mailingAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Zip',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'postalCode',
                    helperClasses: [],
                    value: _postalCode,
                    placeholder: 'Zip',
                    validationType: VALIDATION_TYPES.ZIP_CODE,
                    currentData: currentData,
                    fields: ['householdContact', 'mailingAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'State',
                    isLabelRequired: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.DROPDOWN,
                    name: 'state',
                    value: _state,
                    options: DROPDOWN_OPTIONS.STATE,
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: JSON.parse(JSON.stringify(currentData)),
                    fields: ['householdContact', 'mailingAddress']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'County',
                    isLabelRequired: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.DROPDOWN,
                    name: 'countyCode',
                    value: _countyCode,
                    options: DROPDOWN_OPTIONS.COUNTIES(counties, _state),
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: currentData,
                    fields: ['householdContact', 'mailingAddress']
                }}
                {...cleanedRestProps}
            />
        </Fragment>
    );
};

export default MailingAddressInputs;