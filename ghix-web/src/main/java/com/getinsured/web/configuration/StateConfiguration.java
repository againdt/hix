package com.getinsured.web.configuration;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * State configuration.
 *
 * @author Yevgen Golubenko
 * @since 3/2/19
 */
@Configuration
public class StateConfiguration
{
  private static final Logger log = LoggerFactory.getLogger(StateConfiguration.class);

  @Value("#{configProp['exchange.code'] != null ? configProp['exchange.code'] : null}")
  private String stateCode;

  @Value("#{configProp['database.type'] != null ? configProp['database.type'] : 'POSTGRESQL'}")
  private String databaseType;

  @Value("#{configProp['appUrl'] != null ? configProp['appUrl'] : null}")
  private String appUrl;

  @Value("#{configProp['couchbase.binary.bucketname'] != null ? configProp['couchbase.binary.bucketname'] : null}")
  private String couchbaseBinaryBucketName;

  @PostConstruct
  public void postConstruct() {
    // If we don't have stateCode property set, try to resolve it by other means...
    if(stateCode == null || stateCode.trim().equals("")) {
      log.info("configuration.properties does not contain exchange.code property: {}", stateCode);

      // First, we only run ORACLE for CA, so if that is the case, its definitely CA.
      if(databaseType != null && databaseType.equals("ORACLE")) {
        log.info("configuration.properties has database.type set to {}, we are in CA", databaseType);
        stateCode = "CA";
      } else {

        // Lets see our binary bucket name, it should start with state code.
        log.info("Trying to resolve state code based on Couchbase bucket name: {}", couchbaseBinaryBucketName);
        if(couchbaseBinaryBucketName != null) {
          if(couchbaseBinaryBucketName.startsWith("ct")) {
            stateCode = "CT";
          } else if(couchbaseBinaryBucketName.startsWith("id")) {
            stateCode = "ID";
          } else if(couchbaseBinaryBucketName.startsWith("mn")) {
            stateCode = "MN";
          } else if(couchbaseBinaryBucketName.startsWith("nv")) {
            stateCode = "NV";
          } else if(couchbaseBinaryBucketName.startsWith("wa")) {
            stateCode = "WA";
          } else if(couchbaseBinaryBucketName.startsWith("ca")) {
            stateCode = "CA";
          }
        } else {
          // Try by appUrl... not the best, but give it a shot.
          log.info("Trying to resolve by appUrl: {}", appUrl);
          if(appUrl != null) {
            if(appUrl.contains("ct")) {
              stateCode = "CT";
            } else if(appUrl.contains("id")) {
              stateCode = "ID";
            } else if(appUrl.contains("mn")) {
              stateCode = "MN";
            } else if(appUrl.contains("nv")) {
              stateCode = "NV";
            } else if(appUrl.contains("wa")) {
              stateCode = "WA";
            } else if(appUrl.contains("ca")) {
              stateCode = "CA";
            }
          }
        }
      }
    }

    log.info("StateConfiguration resolved state code to be: {}", stateCode);
  }

  public String getStateCode()
  {
    return stateCode;
  }

  public void setStateCode(final String stateCode)
  {
    this.stateCode = stateCode;
  }

  /**
   * Returns true if we have development bucket name.
   * @return true if development bucket name (e.g. xxx_dev_nonbin)
   */
  public boolean isDevBucketName() {
    return this.couchbaseBinaryBucketName != null && this.couchbaseBinaryBucketName.contains("dev_nonbin");
  }
}
