package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService;

public class Enrollment1095DataValidation implements Tasklet{
	
	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095DataValidation.class);

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		ExecutionContext executionContext =  null;
		
		try{
			executionContext = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
			enrollmentAnnualIrsReportService.validateEnrollment1095Records();
		}
		catch(Exception ex){
			LOGGER.error("Exception Occurred while validating Enrollment 1095 data ",ex);
			if(executionContext != null){
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", ex.getMessage());
			}
			throw new Exception(ex);
		}
		return RepeatStatus.FINISHED;
	}

	public EnrollmentAnnualIrsReportService getEnrollmentAnnualIrsReportService() {
		return enrollmentAnnualIrsReportService;
	}

	public void setEnrollmentAnnualIrsReportService(EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService) {
		this.enrollmentAnnualIrsReportService = enrollmentAnnualIrsReportService;
	}

}
