package com.getinsured.hix.admin.web;

import static com.getinsured.hix.util.PlanMgmtConstants.UNSECURE_CHARS_REGEX;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.lf5.LogLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.admin.service.AdminService;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.dto.plandisplay.DrugDataResponseDTO;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AdminDocument;
import com.getinsured.hix.model.AdminDocument.DOCUMENT_TYPE;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Issuer.IssuerAccreditation;
import com.getinsured.hix.model.Issuer.certification_status;
import com.getinsured.hix.model.IssuerDocument;
import com.getinsured.hix.model.IssuerPaymentInformation;
import com.getinsured.hix.model.IssuerQualityRating;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.IssuerRepresentative.PrimaryContact;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.mapper.IssuerQualityRatingPojo;
import com.getinsured.hix.planmgmt.service.IssuerQualityRatingService;
import com.getinsured.hix.planmgmt.service.IssuerRepresentativeService;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.IssuerService.DocumentType;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.util.MarketProfileDTO;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.comment.service.CommentService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.serff.service.SerffDocumentPlanPOJO;
import com.getinsured.hix.serff.service.SerffService;
import com.getinsured.hix.serff.service.crosswalk.CrosswalkPlanReader;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.thoughtworks.xstream.XStream;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class AdminIssuerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminIssuerController.class);
	
	@Value("#{configProp['appUrl']}") 
	private static String appUrl;
	@Value("#{configProp['ecm.type']}") 
	private static String documentConfigurationType;
	
	@Autowired private AdminService adminService;
	@Autowired private IssuerService issuerService;
	@Autowired private PlanMgmtService planMgmtService;
	@Autowired private UserService userService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private CommentService commentService;
	@Autowired private IssuerQualityRatingService issuerQualityRatingService;
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private CrosswalkPlanReader crosswalkReader ;
	@Autowired private SerffService serffService;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IssuerRepresentativeService issuerRepresentativeService;

	private static final String API_RESPONSE_TRUE = "TRUE";
	private static final String IS_FIANCIAL_INFO_REQUIRED = "planManagement.isFinancialInfoRequired";
	private static final String ISSUER_OBJ = "issuerObj";
	private static final String ROLE_NAME ="activeRoleName";
	//private final String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
	//private final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
	//private final Boolean isEmailActivation = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION).toLowerCase());
	private static final String ATTR_MSG_CROSSWALK_EXCEL = "uploadCrossWalkExcelMessage";
	private static final String ATTR_MSG_CROSSWALK_UPLOAD_FAIL ="uploadFailedCrossWalkExcelMessage";
	private static final String RETURN_EXCEL_CROSSWALK_UPLOAD_PAGE ="admin/issuer/uploadExcel";
	private static final String RETURN_EXCEL_CROSSWALK_STATUS_PAGE ="admin/issuer/crossWalkStatus";
	private static final String RETURN_EXCEL_LOAD_NW_TRANS_PAGE ="admin/issuer/loadNetworkTransparency";
	private static final String RETURN_EXCEL_DISPLAY_NW_TRANS_DATA_PAGE ="admin/issuer/displayNetworkTransparencyData";
	private static final String RETURN_EXCEL_PROCESS_NW_TRANS_PAGE ="admin/issuer/processAndPersistNetworkTransExcel";
	private static final String RETURN_ISSUER_PAYMENT_PAGE ="admin/issuer/issuerPaymentInfo";
	private static final String RETURN_EDIT_ISSUER_PAYMENT_PAGE ="admin/issuer/editIssuerPaymentInfo";
	private static final String EXCEPTIONS = "Exception: ";
	private static final String APPLICABLE_YEAR ="applicableYear";
	private static final String UPLOADED_FILE_LINK = "uploadedFileLink";
	public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	private static final String  UPLOADEDFILENAME = "uploadedFileName";
	private static final String PAGE_SIZE = "pageSize";
	private static final String NULL = "null";
	private static final String QUALITYRATIING_FOLDER = "qualityRating";
	private static final String[] MIME_TYPES = {"image/jpg","image/jpeg","image/png","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/msword","application/vnd.ms-excel","application/pdf"}; 
	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/addissuer", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.ADD_NEW_ISSUER)
	public String addIssuer(Model model, HttpServletRequest request) {
		try {
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			request.getSession().setAttribute(PlanMgmtConstants.ALLOW_REVIEW_AND_SUBMIT, false);
			request.getSession().setAttribute(PlanMgmtConstants.SHOW_OTHER_NAVIGATION, false);
			
			Issuer issuerDetailsInSession = (Issuer) request.getSession().getAttribute(IssuerService.SESSION_ISSUER_DETAILS_VAR);
			if (issuerDetailsInSession != null) {
				request.getSession().setAttribute(IssuerService.SESSION_ISSUER_DETAILS_VAR, null);
			}
		} catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_CREATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}

		try {
			AccountUser primaryRepDetailsInSession = (AccountUser) request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR);
			if (primaryRepDetailsInSession != null) {
				request.getSession().setAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR, null);
			}
		} catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_CREATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return "redirect:/admin/addnewissuer";
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/addnewissuer", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.ADD_NEW_ISSUER)
	public String addIssuerAccInfo(Model model, HttpServletRequest request) {
		Issuer emptyIssuerObj = new Issuer();
		model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Exchange Admin Portal : Add Issuer Account Info");
		model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
		model.addAttribute("issuerLicenseStatusList", emptyIssuerObj.getIssuerLicStatuses());
		model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
		model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		try {
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Issuer issuerDetails = (Issuer) request.getSession().getAttribute(IssuerService.SESSION_ISSUER_DETAILS_VAR);
			String isBack = request.getParameter("isBack");
			if (issuerDetails != null) {
				if(StringUtils.isNotBlank(isBack) && isBack.equalsIgnoreCase(PlanMgmtConstants.TRUE_STRING)){
					model.addAttribute("issuerInfoSubmitted", issuerDetails);
				}else{
					request.getSession().setAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR, null);
					request.getSession().setAttribute(IssuerService.SESSION_PRIMARY_ADDITONAL_DETAILS_VAR, null);
				}
				model.addAttribute(PlanMgmtConstants.DUPLICATE_ISSUER_ERR, request.getSession().getAttribute(PlanMgmtConstants.DUPLICATE_ISSUER_ERR));
			}
		} catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.NEW_ISSUER_CREATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return "admin/addissuer";
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/addnewissuer", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.ADD_NEW_ISSUER)
	public String addNewIssuer(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuerInfo, Model model, HttpServletRequest request) {
		try{
			
			Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			Set<ConstraintViolation<Issuer>> violations = null;
			// ref HIX-88855 :: Make NAIC company code and NAIC group code optional for CA
			if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_CA)){
				violations = validator.validate(issuerInfo,Issuer.PhixAddIssuer.class);
			}else{
				violations = validator.validate(issuerInfo,Issuer.AddNewIssuer.class);
			}
			 if(violations != null && !violations.isEmpty()){
				 //model.addAttribute("fieldError", violations);
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			 }/*else{
				 if(PlanMgmtConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode) && issuerInfo.getNaicCompanyCode().isEmpty() && issuerInfo.getNaicGroupCode().isEmpty()){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			 }*/
			
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Boolean recordAlreadyExists = issuerService.isIssuerExists(issuerInfo);
			request.getSession().setAttribute(PlanMgmtConstants.DUPLICATE_ISSUER_ERR, false);
			if (recordAlreadyExists) {
				request.getSession().setAttribute(PlanMgmtConstants.DUPLICATE_ISSUER_ERR, true);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Exchange Admin Portal : Issuer record already exists ");
				}	
				return "redirect:/admin/addnewissuer";
			} else {
				// When issuer details submitted, store the data in the session then, check for duplicate issuer on the basis of license number
				// if new issuer is duplicate, then redirect to same page, with error msg else redirect to Primary Contact Details page (Primary representative page)
				request.getSession().setAttribute(IssuerService.SESSION_ISSUER_DETAILS_VAR, null);
				if (issuerInfo != null) {
					if(StringUtils.isNotBlank(issuerInfo.getCity())){
						issuerInfo.setCity(issuerInfo.getCity().replace(",",PlanMgmtConstants.EMPTY_STRING).trim());
					}
					request.getSession().setAttribute(IssuerService.SESSION_ISSUER_DETAILS_VAR, issuerInfo);
				}
				String isNext = request.getParameter("isNext");
				request.getSession().setAttribute("isNext", isNext);
				request.getSession().setAttribute(PlanMgmtConstants.SHOW_OTHER_NAVIGATION, true);
				return "redirect:/admin/issuer/primaryrepinfo";
			}
		}catch(Exception ex){
	          LOGGER.error("Error in saving Issuer details in session. Excep-" + ex.getMessage());
	          throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.NEW_ISSUER_CREATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
	          //return PlanMgmtConstants.MANAGE_ISSUER_PATH;	Commenting code for exception frawork
		}
	}

	@RequestMapping(value = "/admin/issuer/primaryrepinfo", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String primaryContactInfo(Model model, HttpServletRequest request) {
		try {
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			if (request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR) != null
					&& !(request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR).toString().isEmpty())) {
				AccountUser primaryRepDetails = (AccountUser) request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR);
				String isBack = request.getParameter("isBack");
				String isNext = (String)request.getSession().getAttribute("isNext");
				
				if (primaryRepDetails != null) {
					if(StringUtils.isNotBlank(isBack) && isBack.equalsIgnoreCase("true") || StringUtils.isNotBlank(isNext)){
						model.addAttribute("primaryRepInfoSubmitted", primaryRepDetails);
						if (primaryRepDetails.getPhone() != null && primaryRepDetails.getPhone().length() >= PlanMgmtConstants.TEN) {
							model.addAttribute(PlanMgmtConstants.PHONE1, primaryRepDetails.getPhone().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.THREE));
							model.addAttribute(PlanMgmtConstants.PHONE2, primaryRepDetails.getPhone().substring(PlanMgmtConstants.THREE, PlanMgmtConstants.SIX));
							model.addAttribute(PlanMgmtConstants.PHONE3, primaryRepDetails.getPhone().substring(PlanMgmtConstants.SIX, PlanMgmtConstants.TEN));
						}
					}
					model.addAttribute(PlanMgmtConstants.DUPLICATE_EMAIL_ERR, request.getSession().getAttribute(PlanMgmtConstants.DUPLICATE_EMAIL_ERR));
				}
			}
			if (request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_ADDITONAL_DETAILS_VAR) != null && !(request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_ADDITONAL_DETAILS_VAR).toString().isEmpty())) {
				IssuerRepresentative issuerRep = (IssuerRepresentative) request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_ADDITONAL_DETAILS_VAR);
				model.addAttribute("issuerInfoSubmitted", issuerRep != null ? issuerRep.getLocation()
						: null);
			}

			// if issuer details not found in the session, then show message to enter them
			model.addAttribute(PlanMgmtConstants.ISSUER_DETAILS_NOT_SUBMITTED, PlanMgmtConstants.TRUE);
			// model.addAttribute("IS_CA_CALL", GhixConfiguration.IS_CA_CALL);
			/**
			 * below code added by kuldeep for Jira HIX-10881
			 */
			model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
			
			String emailActivation = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION);
			Boolean isEmailActivation = Boolean.FALSE;
			if (StringUtils.isNotBlank(emailActivation)){
		        isEmailActivation = Boolean.parseBoolean(emailActivation.toLowerCase());
			}
			model.addAttribute(PlanMgmtConstants.IS_EMAIL_ACTIVATION, isEmailActivation.toString().toUpperCase());

			Issuer issuerDetailsOnRepForm = (Issuer) request.getSession().getAttribute(IssuerService.SESSION_ISSUER_DETAILS_VAR);
			if (issuerDetailsOnRepForm != null) {
				model.addAttribute(PlanMgmtConstants.ISSUER_DETAILS_NOT_SUBMITTED, "false");
			}
			return "admin/issuer/primaryrepinfo";
		} catch (Exception ex) {
			LOGGER.error("Issuer Primary Contact details not found in session. Excep- " + ex.getMessage()); // Commenting code for exception framework
			//return PlanMgmtConstants.MANAGE_ISSUER_PATH;
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.PRIMARY_CONTACT_DETAILS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
	}

	@RequestMapping(value = "/admin/issuer/primaryrepinfo", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String submitPrimaryContactInfo(@ModelAttribute(PlanMgmtConstants.ACCOUNT_USER) AccountUser primaryContactInfo, 
						@ModelAttribute("IssuerRepresentative") IssuerRepresentative issuerRepresentative, Model model, HttpServletRequest request) {
		try {
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			request.getSession().setAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR, null);
			if (primaryContactInfo != null) {
				request.getSession().setAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR, primaryContactInfo);
				//server side validation
				 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				 Set<ConstraintViolation<IssuerRepresentative>> violations = validator.validate(issuerRepresentative, IssuerRepresentative.AddNewIssuerRepresentative.class);
				 if(violations != null && !violations.isEmpty()){
					//throw exception in case client side validation breached
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			}
			if (issuerRepresentative != null) {
				request.getSession().setAttribute(IssuerService.SESSION_PRIMARY_ADDITONAL_DETAILS_VAR, issuerRepresentative);
			}
			Boolean recordAlreadyExists = false;
			if(null != primaryContactInfo.getEmail()){
				issuerService.isDuplicate(primaryContactInfo.getEmail());
			}	
			request.getSession().setAttribute(PlanMgmtConstants.DUPLICATE_EMAIL_ERR, false);
			if (recordAlreadyExists) {
				request.setAttribute("isBack", "true");
				request.getSession().setAttribute(PlanMgmtConstants.DUPLICATE_EMAIL_ERR, true);
				LOGGER.info("Exchange Admin Portal : Issuer Representative record already exists.");
				return "redirect:/admin/issuer/primaryrepinfo";
			}
			return "redirect:/admin/issuer/reviewissuer";
		} catch (Exception ex) {
			//LOGGER.error("Error in saving Primary Contact details in session"); commenting code for exception framework
			//return PlanMgmtConstants.MANAGE_ISSUER_PATH;
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.PRIMARY_CONTACT_DETAILS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
	}

	@RequestMapping(value = "/admin/issuer/checkDuplicate", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.ADD_NEW_ISSUER)
	@ResponseBody
	public boolean isDuplicate(@RequestParam(value = "userEmailId", required = false) String userEmailId) {
		boolean isDuplicateResponse = false;
		AccountUser accountUser = null;
		try{
			accountUser = userService.getLoggedInUser();
			isDuplicateResponse = issuerService.isDuplicate(userEmailId);
		}catch(Exception ex){
	       giMonitorService.saveOrUpdateErrorLog(String.valueOf(PlanMgmtErrorCodes.ErrorCode.DUPLICATE_EMAIL_ADDRESS_EXCEPTION), new TSDate(), Exception.class.getName(),ExceptionUtils.getFullStackTrace(ex), accountUser);
		}
		return isDuplicateResponse;
	}

	@RequestMapping(value = "/admin/issuer/reviewissuer", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String reviewIssuerAccInfo(@RequestParam(value = "reviewNSubmit", required = false) String reviewNSubmit, Model model, 
													HttpServletRequest request, RedirectAttributes redirectAttrs) {
		model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Exchange Admin Portal : Review Issuer Account Info");
		AccountUser primaryRepDetails = null;
		AccountUser newlyAddedRep = null;
		Issuer issuerDetails = null;
		IssuerRepresentative issuerRep = null;
		Boolean redirectToManageIssuer = true;
		model.addAttribute(PlanMgmtConstants.ISSUER_DETAILS_NOT_SUBMITTED, PlanMgmtConstants.TRUE);
		model.addAttribute("repDetailsNotSubmitted", PlanMgmtConstants.TRUE);
		model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		if (request.getSession().getAttribute(PlanMgmtConstants.DUPLICATE_HIOS) != null) {
			model.addAttribute(PlanMgmtConstants.DUPLICATE_HIOS, "error");
			request.getSession().removeAttribute(PlanMgmtConstants.DUPLICATE_HIOS);
		}
		try {
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			
			issuerDetails = (Issuer) request.getSession().getAttribute(IssuerService.SESSION_ISSUER_DETAILS_VAR);
			if (issuerDetails != null) {
				model.addAttribute("issuerDetailsSubmitted", issuerDetails);
				redirectToManageIssuer = false;
				model.addAttribute(PlanMgmtConstants.ISSUER_DETAILS_NOT_SUBMITTED, "false");
			}
			primaryRepDetails = (AccountUser) request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR);
			if (primaryRepDetails != null) {
				model.addAttribute("primaryRepDetailsSubmitted", primaryRepDetails);
				redirectToManageIssuer = false;
				model.addAttribute("repDetailsNotSubmitted", "false");
			}
			/**
			 * Jira HIX-10881
			 */
			if (request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_ADDITONAL_DETAILS_VAR) != null) {
				issuerRep = (IssuerRepresentative) request.getSession().getAttribute(IssuerService.SESSION_PRIMARY_ADDITONAL_DETAILS_VAR);
				model.addAttribute("primaryAdditionalDetailsSubmitted", issuerRep != null ? issuerRep.getLocation()
						: null);
			}
			if (redirectToManageIssuer) {
				LOGGER.error("Issuer details and Primary Contact details not found in session");
			}

			// if form submitted, add issuer and primary contact details
			if (reviewNSubmit != null && reviewNSubmit.equalsIgnoreCase("createIssuerRep")) {
				String returnVal = PlanMgmtConstants.EMPTY_STRING;
				boolean isDuplicate = false;
				// in PHIX profile set issuer default status as CERTIFIED else
				// as REGISTERED
				if ("PHIX".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE))) {
					issuerDetails.setCertificationStatus(IssuerService.CERT_STATUS_CERTIFIED);
				} else {
					issuerDetails.setCertificationStatus(IssuerService.CERT_STATUS_WHEN_REGISTERED);
				}

				try {
					issuerDetails.setLastUpdatedBy(userService.getLoggedInUser().getId());
					issuerDetails.setSendEnrollmentEndDateFlag(PlanMgmtConstants.TRUE_STRING);
					
					if (null != primaryRepDetails) {
						issuerDetails.setEmailAddress(primaryRepDetails.getEmail());
					}
					if (isDuplicateHIOS(issuerDetails.getHiosIssuerId()).equals(PlanMgmtConstants.EMPTY_STRING)) {

						Issuer newlyAddedIssuer = issuerService.saveIssuer(issuerDetails);
						if (newlyAddedIssuer != null) {
							// issuer added successfully.
							LOGGER.debug("Exchange Admin Portal : new issuer added successfully");
							// remove issuer data from session
							request.getSession().setAttribute(IssuerService.SESSION_ISSUER_DETAILS_VAR, null);
							// remove primary contact data from session
							request.getSession().setAttribute(IssuerService.SESSION_PRIMARY_REP_DETAILS_VAR, null);
							// remove show navigation data from session  
							request.getSession().setAttribute(PlanMgmtConstants.ALLOW_REVIEW_AND_SUBMIT, false);
							request.getSession().setAttribute(PlanMgmtConstants.SHOW_OTHER_NAVIGATION, false);
							
							// second create issuer representative (Primary Contact person/representative)
							primaryRepDetails.setPhone(primaryRepDetails.getPhone().replaceAll("\\(|\\)|\\-|\\s", PlanMgmtConstants.EMPTY_STRING));
							if (primaryRepDetails.getUsername() == null) {
								primaryRepDetails.setUserName(primaryRepDetails.getEmail());
							}
							newlyAddedRep = saveRepresentative(primaryRepDetails, newlyAddedIssuer, "Yes", redirectAttrs, issuerRep);
							if (newlyAddedRep != null) {
								// issuer added successfully.
								LOGGER.info("Exchange Admin Portal : Primaty Contact details added successfully");
							}
						}
					} else {
						request.getSession().setAttribute(PlanMgmtConstants.DUPLICATE_HIOS, "error");
						isDuplicate = true;
						returnVal = "redirect:/admin/issuer/reviewissuer";
					}
				} catch (Exception ex) {
					//LOGGER.error("Exchange Admin Portal : fail to add issuer" + ex); commenting code for exception framework
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.REVIEW_ISSUER_ACCOUNT_INFORMATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
				}
				// then redirect to manage issuer
				if (!isDuplicate) {
					returnVal = PlanMgmtConstants.MANAGE_ISSUER_PATH;
				}
				return returnVal;
			}
			request.getSession().setAttribute(PlanMgmtConstants.ALLOW_REVIEW_AND_SUBMIT, true);
			request.getSession().setAttribute(PlanMgmtConstants.SHOW_OTHER_NAVIGATION, true);
			return "admin/issuer/reviewaccinfo";

		} catch (Exception ex) {
			//LOGGER.error("Issuer details and Primary Contact details not found in session"); commenting code for exception framework
			//return PlanMgmtConstants.MANAGE_ISSUER_PATH;
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.REVIEW_ISSUER_ACCOUNT_INFORMATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
	}

	private AccountUser saveRepresentative(AccountUser accountUser, Issuer issuerObj, String primaryContact, RedirectAttributes redirectAttr, IssuerRepresentative issuerRepresentative) throws InvalidUserException {
		AccountUser representativeUser = null;
		if (!issuerService.isDuplicate(accountUser.getEmail())) {
			try {
				// only for CA user and module user need not be created at our end during issuer creation
				// through LDAP afterwards user and module user will be created for issuer representative
				/*	if (GhixConfiguration.EXCHANGE_TYPE.equalsIgnoreCase("PHIX") || !GhixConfiguration.STATE_CODE.equalsIgnoreCase("CA")){*/
				// create user with rep. details
				/*
				 * Changes done by Kuldeep for jira HIX-11649
				 * by this changes no account user will be created for any issuer, Accounts will be created via validation	
				 */
				IssuerRepresentative issuerRep = new IssuerRepresentative();
				issuerRep.setUserRecord(null); // Until user objected created in users table, IssureRep's user_id will be null
				Boolean isEmailActivation = Boolean.FALSE;
				String emailActivation = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION);
				
				if (StringUtils.isNotBlank(emailActivation)){
			        isEmailActivation = Boolean.parseBoolean(emailActivation.toLowerCase());
				}
				
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
				if (!isEmailActivation && !stateCode.equalsIgnoreCase("CA") && !stateCode.equalsIgnoreCase("MS")) {
					representativeUser = userService.createUser(accountUser, GhixRole.ISSUER_REP.getRoleName());
					// check whether representative record exists in ModuleUser
					ModuleUser issuerRepUser = userService.findModuleUser(issuerObj.getId(), ModuleUserService.ISSUER_MODULE, representativeUser);
					if (issuerRepUser == null) {
						userService.createModuleUser(issuerObj.getId(), ModuleUserService.ISSUER_MODULE, representativeUser);
					} else {
						LOGGER.error("Admin Portal: Adding New Issuer Rep: Representative record already exists in ModuleUser.");
					}
					if(representativeUser != null) {
						issuerRep.setUserRecord(representativeUser);
					}
				}
				
					issuerRep.setIssuer(issuerObj);
					
					issuerRep.setPrimaryContact((primaryContact.equalsIgnoreCase("YES")) ? PrimaryContact.YES : PrimaryContact.NO);
					issuerRep.setLocation(issuerRepresentative != null ? issuerRepresentative.getLocation() : null);
					issuerRep.setUpdatedBy(userService.getLoggedInUser());
					issuerRep.setFirstName(accountUser.getFirstName());
					issuerRep.setLastName(accountUser.getLastName());
					issuerRep.setTitle(accountUser.getTitle());
					issuerRep.setEmail(accountUser.getEmail());
					issuerRep.setPhone(accountUser.getPhone());
					issuerRep.setRole(ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString());
					
					// end code changes
					IssuerRepresentative newIssuerRepresentative = issuerService.saveRepresentative(issuerRep);
					if (newIssuerRepresentative != null && isEmailActivation
							&& !stateCode.equalsIgnoreCase("CA") && !stateCode.equalsIgnoreCase("MS")) {
						try {
							issuerService.generateActivationLink(newIssuerRepresentative.getId(), accountUser, ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString());
							redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sent to " + accountUser.getEmail());
						} catch (Exception e) {
							//LOGGER.error(" " + e);	commenting code for exception framework
							redirectAttr.addFlashAttribute(PlanMgmtConstants.GENERATEACTIVATIONLINKINFO, "Activation link sending process failed... Please take appropriate actions. -  "
									+ e.getMessage());
							throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.GENERATE_ACTIVATIONLINK_INFO_EXCEPTION, e, null, Severity.HIGH);
						}
					}
					if(newIssuerRepresentative != null && stateCode.equalsIgnoreCase("MS")){
						redirectAttr.addFlashAttribute(PlanMgmtConstants.IS_ISSUER_CREATED, "Issuer account successfully created for "+ issuerObj.getName());
					}
					
					// send delegation code
					issuerRepresentativeService.updateDelegationInfo(newIssuerRepresentative, redirectAttr, issuerObj, accountUser);
					
		
			} catch (Exception ex) {
				LOGGER.error("Admin Portal: Adding New Issuer Rep: Error in saving representative details.", ex);
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_REPRESENTATIVE_EXCEPTION.getCode(), ex, null, Severity.HIGH);
			}

		} else {
			LOGGER.error("Admin Portal: Adding New Issuer Rep: Email address already exists ");
		}
		return representativeUser;
	}

	

	/* Please don't remove following code, we will need it in future
	 * public void sendConfirmationEmail(int userId) throws MessagingException {
		AccountUser userObj = userService.findById(userId);
		issuerConfirmationEmail.setUserObj(userObj);
		Notice noticeObj = null;
		try {
			noticeObj = issuerConfirmationEmail.generateEmail();
			issuerConfirmationEmail.mail(noticeObj);
		} catch (NotificationTypeNotFound e) {
			e.printStackTrace();
			logger.error("Notification type \"Issuer Confirmation Email\" not found. Email cannot be sent.");
		}
	}*/

	@RequestMapping(value = "/admin/manageissuer", method = {RequestMethod.GET, RequestMethod.POST })
	@GiAudit(transactionName = "ManageIssuer", eventType = EventTypeEnum.ISSUER_ADMIN, eventName = EventNameEnum.PII_READ)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String manageIssuer(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuerInfo, Model model, HttpServletRequest request) {
		try{
			String status = PlanMgmtConstants.EMPTY_STRING;
			String issuerName = PlanMgmtConstants.EMPTY_STRING;
			if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS_STRING))){
				status = request.getParameter(PlanMgmtConstants.STATUS_STRING);
			}	
			
			if(StringUtils.isNotBlank(request.getParameter("issuerName"))){
				issuerName = request.getParameter("issuerName");
			}
			
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			// set the default sort column
			request.setAttribute(PlanMgmtConstants.DEFAULT_SORT, "lastUpdateTimestamp");
			// get the Pageable object consisting of pagination and sorting enabled properties
			List<String> sortableColumnsList = Issuer.getSortableColumnsList();
			Pageable pageable = issuerService.getPagingAndSorting(model, request, sortableColumnsList);
			// retrieve the list of issuers.
			Page<Issuer> issuerList = issuerService.getIssuerList(issuerName, status, pageable);
			model.addAttribute("status", status);
			model.addAttribute("issuerName", issuerName);
			model.addAttribute("issuerCertStatusList", issuerInfo.getIssuerStatuses());
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerList.getContent());
			model.addAttribute(PlanMgmtConstants.RESULT_SIZE, issuerList.getTotalElements());
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
	
			model.addAttribute(ROLE_NAME, activeRoleName);
			if (model.asMap().containsKey("sessionDelegationCode") && model.asMap().containsKey("sessionIssuerName")) {
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Redirect Parameters at manage issuer:  Delegation code: " + SecurityUtil.sanitizeForLogging(String.valueOf(model.asMap().get("sessionDelegationCode"))
																			+ " IssuerName: " + SecurityUtil.sanitizeForLogging(String.valueOf(model.asMap().get("sessionIssuerName")))));
				}
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS) , new GiAuditParameter("Issuer Name", issuerName),
					new GiAuditParameter("Issuer status", status));
		}catch(Exception ex){
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching issuers "));
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.MANAGE_ISSUER_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		
		return "admin/manageissuer";
	}

	
	/*
	 * @Suneel: Secured the method with permission
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/changeissuerstatus", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String changeIssuerStatus(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer,@RequestParam("successFullyUploaded") String successFullyUploaded, 
		@RequestParam(PlanMgmtConstants.ISSUER_ID) Integer issuerId, 
		@RequestParam("hdnCertSuppDoc") String certSuppDoc, 
		@RequestParam(value = "comment_text", required = false) String comment,RedirectAttributes redirectAttr){
		try{
			//server side validation
			 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			 Set<ConstraintViolation<Issuer>> violations = validator.validate(issuer, Issuer.EditCertificationStatus.class);
			 if(violations != null && !violations.isEmpty()){
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			 }
			
			String encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerId));
			String redirectUrl = "redirect:/admin/issuer/certification/status/" + encryptedIssuerId; 
			
			LOGGER.info("Exchange Admin Portal: Updating Issuer Certification Status");
			
			// During issuer de-certification, we call enrollment API to check is any active enrollment of that issuer exists or not. If exists then we show error message
			if(Issuer.certification_status.DECERTIFIED.name().equalsIgnoreCase(issuer.getCertificationStatus())){
				String hasActiveEnrollment = PlanMgmtConstants.EMPTY_STRING;
				try {
					hasActiveEnrollment = ghixRestTemplate.postForObject(EnrollmentEndPoints.IS_ISSUER_HAVING_ACTIVE_ENROLLMENT, issuerId, String.class);
				} catch (Exception e) {
					LOGGER.error("Error while getting response from API",e);
				}
				
				if(API_RESPONSE_TRUE.equalsIgnoreCase(hasActiveEnrollment)){
					redirectAttr.addAttribute("activeEnrollment",true);
					return "redirect:/admin/issuer/certification/status/edit/"+issuerId;
				}
			}
			
			Issuer issuerObj = issuerService.getIssuerById(issuerId);
			Integer commentId = 0;
			if (StringUtils.isNotBlank(comment)) {
				commentId = issuerService.saveComment(issuerId, comment, CommentTarget.TargetName.ISSUER);
			}
			if (commentId != 0) {
				issuerObj.setCommentId(commentId);
			} else {
				issuerObj.setCommentId(null);
			}
	
			issuerObj.setCertificationStatus(issuer.getCertificationStatus());
	
			if (successFullyUploaded.equalsIgnoreCase(PlanMgmtConstants.SUCCESSFULLYUPLOADED)) {
				if (!certSuppDoc.equals(PlanMgmtConstants.EMPTY_STRING)) {
					issuerObj.setCertificationDoc(certSuppDoc);
				} else {
					issuerObj.setCertificationDoc(null);
				}
			} else {
				issuerObj.setCertificationDoc(null);
			}
	
			issuerObj.setDecertifiedOn(null);
			// when issuer certification status is updated to Certified
			if (issuer.getCertificationStatus().equalsIgnoreCase(certification_status.CERTIFIED.toString())) {
				issuerObj.setCertifiedOn(new TSDate());
				issuerObj.setEffectiveStartDate(new TSDate());
				issuerObj.setEffectiveEndDate(null);
			} else if (issuer.getCertificationStatus().equalsIgnoreCase(certification_status.DECERTIFIED.toString())) {
				// when issuer certification status is updated to Decertified
				issuerObj.setDecertifiedOn(new TSDate());
				issuerObj.setEffectiveEndDate(new TSDate());
				// decertify respective plans
				planMgmtService.decertifyPlans(issuerId);
	
			}
	
			try {
				issuerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
			} catch (InvalidUserException e) {
				LOGGER.info("Error in setting updated by");
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.INVALID_USER_INFO_EXCEPTION, e, null, Severity.LOW);
			}
	
			/* following condition added because when user de-certify issuer planMgmtService.decertifyPlans(issuerId); method will add row in issuer_aud table 
			 so that no need to call  issuerService.saveIssuer(issuerObj); */
			if (null != issuer && !issuer.getCertificationStatus().equalsIgnoreCase(certification_status.DECERTIFIED.toString())) {
				try {
					issuerService.saveIssuer(issuerObj);
					String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					if(issuer.getCertificationStatus().equalsIgnoreCase(certification_status.CERTIFIED.toString()) && "ID".equalsIgnoreCase(stateCode)){
						issuerRepresentativeService.sendNotificationEmailToIssuerRepresentative(issuerObj);
					}
				} catch (Exception e) {
					LOGGER.error("Error while setting updatedBy");
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_ISSUER_EXCEPTION, e, null, Severity.HIGH);
				}
			}
	
			// On issuer certification call IND 05 to sync issuer data
			if (null != issuer && issuer.getCertificationStatus().equalsIgnoreCase(certification_status.CERTIFIED.toString())) {
				issuerService.syncIssuerWithAHBX(issuerId);
			}
	
			if (redirectUrl != null && !redirectUrl.isEmpty()) {
				return redirectUrl;
			} else {
				return PlanMgmtConstants.MANAGE_ISSUER_PATH;
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.CHANGE_ISSUER_STATUS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
	}

	
	
	
	
	@RequestMapping(value = "/admin/updateissuerstatus/{id}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String updateIssuerStatus(Model model, @ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuerInfo, @PathVariable("id") Integer id) {
		try{
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			if (adminService.isAdminLoggedIn()) {
				model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Exchange Admin Portal : Update Issuer Status");
				if (id != 0) {
					Issuer issuer = issuerService.getIssuerById(id);
					// fetch issuer docs
					List<IssuerDocument> documents = issuer.getIssuerDocuments();
					List<HashMap<String, String>> authFiles = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> marketingFiles = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> disclosureFiles = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> accreditationFiles = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> addInfoFiles = new ArrayList<HashMap<String, String>>();
	
					if (!documents.isEmpty()) {
						for (IssuerDocument document : documents) {
							HashMap<String, String> hm = new HashMap<String, String>();
							hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getDocumentName());
	
							if (DocumentType.AUTHORITY.name().equals(document.getDocumentType())) {
								authFiles.add(hm);
							} else if (DocumentType.MARKETING.name().equals(document.getDocumentType())) {
								marketingFiles.add(hm);
							} else if (DocumentType.DISCLOSURES.name().equals(document.getDocumentType())) {
								disclosureFiles.add(hm);
							} else if (DocumentType.ACCREDITATION.name().equals(document.getDocumentType())) {
								accreditationFiles.add(hm);
							} else if (DocumentType.ADDITIONAL_INFO.name().equals(document.getDocumentType())) {
								addInfoFiles.add(hm);
							}
						}
						model.addAttribute("authorityFile", authFiles);
						model.addAttribute("marketingFile", marketingFiles);
						model.addAttribute("disclosuresFile", disclosureFiles);
						model.addAttribute("accreditationFile", accreditationFiles);
						model.addAttribute("addInfoFile", addInfoFiles);
					}
	
					model.addAttribute("issuer", issuer);
					model.addAttribute("certificationDoc", issuer.getCertificationDoc());
					model.addAttribute("issuerCertStatusList", issuerInfo.getIssuerStatuses());
					return "admin/updateissuerstatus";
				} else {
					return PlanMgmtConstants.MANAGE_ISSUER_PATH;
				}
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.UPDATE_ISSUER_STATUS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return PlanMgmtConstants.ADMIN_LOGIN_PATH;
	}

	@RequestMapping(value = "/admin/uploadissuercertsuppdoc", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	@ResponseBody
	public void uploadQHPSupportingDocument(Model model, HttpServletRequest request, @RequestParam(value = "certSuppDoc", required = false) MultipartFile certSuppDoc, @RequestParam(PlanMgmtConstants.ISSUER_ID) String issuerId, HttpServletResponse response) throws GIException, ContentManagementServiceException, IOException {
		String returnString = null;
		String documentId = null;
		try{
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Long certSuppDocFileSize = Long.parseLong(DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CERTSUPPDOCFILESIZE));
			boolean isValidFileType=true;
			List<String> mimeTypeList = Arrays.asList(MIME_TYPES); 
			if(certSuppDoc != null && certSuppDoc.getContentType()!=null && !mimeTypeList.contains(certSuppDoc.getContentType().toLowerCase())) {
				isValidFileType=false;
			}	
	
			if ((certSuppDoc != null)
					&& (certSuppDoc.getSize() < certSuppDocFileSize) && isValidFileType) {
				try {
					String modifiedFileName = FilenameUtils.getBaseName(certSuppDoc.getOriginalFilename())
							+ GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT + FilenameUtils.getExtension(certSuppDoc.getOriginalFilename());
					// upload file to DMS
					try {
						//String filePath = GhixConstants.ISSUER_DOC_PATH + GhixConstants.FRONT_SLASH + issuerId + GhixConstants.FRONT_SLASH  + GhixConstants.ISSUER_CERTI_FOLDER;
						String filePath = GhixConstants.ISSUER_DOC_PATH + File.separator + issuerId + File.separator  + GhixConstants.ISSUER_CERTI_FOLDER;
						boolean isValidPath = planMgmtUtils.isValidPath(filePath);
						if(isValidPath){
							documentId = ecmService.createContent(filePath, modifiedFileName, certSuppDoc.getBytes()
									, ECMConstants.Issuer.DOC_CATEGORY, ECMConstants.Issuer.DOC_SUB_CATEGORY_SUPPORT, null);
							if(null != documentId){
								returnString = certSuppDoc.getOriginalFilename() + "|" + documentId; // return original file name and DMS id
								request.setAttribute(PlanMgmtConstants.SUCCESSFULLYUPLOADED, PlanMgmtConstants.SUCCESSFULLYUPLOADED);
							}
							LOGGER.info("Upload issuer certification doc successfully");
						}else{
							LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
						}
					} catch (ContentManagementServiceException e) {
						returnString = PlanMgmtConstants.EMPTY_STRING;
						giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.CONTENT_MANAGEMENT_SERVICE_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getMessage().toString(), null);
					}
	
				} catch (Exception ex) {
					returnString = PlanMgmtConstants.EMPTY_STRING;
					LOGGER.error("Fail to uplaod issuer certification supportive doc. Ex: " + ex);
					
				}
			}else if(!isValidFileType) {
				returnString = "INVALID_FILE_TYPE"; 
			} else {
				returnString = "SIZE_FAILURE";
				
			}
			
		}catch(Exception ex){
			returnString = PlanMgmtConstants.EMPTY_STRING;
//			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.UPLOAD_QHP_SUPPORTING_DOCUMENT_EXCEPTION.getCode(), ex, null, Severity.LOW);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.UPLOAD_QHP_SUPPORTING_DOCUMENT_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),ex.getMessage().toString(), null);
		}
		response.setContentType(PlanMgmtConstants.CONTENT_TYPE);
		response.getWriter().write(returnString);
		response.flushBuffer();
		return;	
	}

	@RequestMapping(value = "/admin/updateissuerstatus/filedownload", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public ModelAndView downloadFile(Model model ,@RequestParam(value = "documentId", required = false) String documentId, @RequestParam(value = PlanMgmtConstants.ISSUER_ID, required = false) String issuerId, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String folderName = PlanMgmtConstants.EMPTY_STRING;
		String fileName = PlanMgmtConstants.EMPTY_STRING;
		String uploadedFileName = PlanMgmtConstants.EMPTY_STRING;
		FileInputStream fis = null;
		try{
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			
			Issuer issuer = issuerService.getIssuerById(Integer.parseInt(issuerId));
			List<IssuerDocument> documents = issuer.getIssuerDocuments();
			for (IssuerDocument document : documents) {
				if (document.getId() == Integer.parseInt(documentId)) {
					folderName = document.getDocumentType().toLowerCase();
					fileName = document.getDocumentName();
					uploadedFileName = planMgmtService.getActualFileName(document.getDocumentName());
				}
			}
			String filePath = PlanMgmtConstants.UPLOADPATH + GhixConstants.FRONT_SLASH + folderName + GhixConstants.FRONT_SLASH + fileName;
			boolean isvalidPath = planMgmtUtils.isValidPath(filePath);
			if(isvalidPath){
				File file = new File(filePath);
				
				if (null != file && StringUtils.isNotBlank(uploadedFileName)) {
					response.setContentType("application/octet-stream");
					response.setContentLength((int) file.length());
					response.setHeader("Content-Disposition", "attachment; filename=\"" + uploadedFileName.replaceAll(UNSECURE_CHARS_REGEX, StringUtils.EMPTY).trim() + "\"");
					fis = new FileInputStream(file);
					
					if (null != fis) {
						FileCopyUtils.copy(fis, response.getOutputStream());
						fis.close();
					}
					file = null;
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_FILE_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}finally{
			IOUtils.closeQuietly(fis);
		}
		return null;
	}

	/* @author raja
	 * START : HIX-1807  
	 */

	// issuer details info
	@RequestMapping(value = "/admin/issuer/details/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String issuerDetailsInfo(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		String decryptedIssuerId = null;
		try{
			LOGGER.info("Admin Issuer Details Page Info");
			decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId)));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			model.addAttribute(ROLE_NAME, activeRoleName);
			model.addAttribute("sysDate", new TSDate());
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_DETAIL_INFORMATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return "admin/issuer/details";
	}

	// issuer edit details
	@RequestMapping(value = "/admin/issuer/details/edit/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String issuerEditDetails(Model model, @PathVariable("encIssuerId") String encIssuerId, HttpServletRequest request) {
		String decryptedIssuerId = null;
		try{
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			LOGGER.info("Admin Issuer Details Page Info");
			decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			String duplicateHOISId = request.getParameter("duplicateHoisId");
			String hiosIdValue = request.getParameter("duplicateHoisIdValue");
			String hoisId = request.getParameter("hoisId");
			model.addAttribute("duplicateHOISId", duplicateHOISId);
			model.addAttribute("HOISId", hiosIdValue);
			Issuer issuerObj = issuerService.getIssuerById(Integer.valueOf(decryptedIssuerId));
			if (StringUtils.isNotBlank(hoisId)) {
				issuerObj.setHiosIssuerId(hoisId);
			}
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.EDIT_ISSUER_DETAILS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return "admin/issuer/details/edit";
	}

	// save issuer details
	@RequestMapping(value = "/admin/issuer/details/save", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String saveissuerDetails(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, RedirectAttributes redirectAttributes) {
		LOGGER.info("Save Issuer Details");
		Issuer issuerObj = null;
		String encryptedIssuerId = null;
		try {
			//server side validation
			Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			Set<ConstraintViolation<Issuer>> violations = null;
			// ref HIX-88855 :: Make NAIC company code and NAIC group code optional for CA
			if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_CA)){
				violations = validator.validate(issuer, Issuer.PhixEditIssuerDetails.class);
			}else{
				violations = validator.validate(issuer, Issuer.EditIssuerDetails.class);
			}
			 
			 if(violations != null && !violations.isEmpty()){
				 //throw exception in case client side validation breached
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			 }/*else{
				 if(PlanMgmtConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode) && issuer.getNaicCompanyCode().isEmpty() && issuer.getNaicGroupCode().isEmpty()){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			 }*/
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Issuer checkIssuerHoisObj = issuerService.getIssuerByHiosID(issuer.getHiosIssuerId().toString());
			issuerObj = issuerService.getIssuerById(issuer.getId());
			if(null != issuerObj){
				if (!StringUtils.equals(issuerObj.getHiosIssuerId().trim(), issuer.getHiosIssuerId().trim())
						&& checkIssuerHoisObj != null) {
					redirectAttributes.addAttribute("duplicateHoisId", true);
					redirectAttributes.addAttribute("duplicateHoisIdValue", checkIssuerHoisObj.getId());
					redirectAttributes.addAttribute("hoisId", issuer.getHiosIssuerId().trim());
					encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuer.getId()));
					return "redirect:/admin/issuer/details/edit/" + encryptedIssuerId;
				}
				issuerObj.setName(issuer.getName());
				issuerObj.setShortName(issuer.getShortName());
				issuerObj.setNaicCompanyCode(issuer.getNaicCompanyCode());
				issuerObj.setNaicGroupCode(issuer.getNaicGroupCode());
				issuerObj.setFederalEin(issuer.getFederalEin());
				issuerObj.setHiosIssuerId(issuer.getHiosIssuerId());
				issuerObj.setTxn834Version(issuer.getTxn834Version());
				issuerObj.setTxn820Version(issuer.getTxn820Version());
				issuerObj.setAddressLine1(issuer.getAddressLine1());
				issuerObj.setAddressLine2(issuer.getAddressLine2());
				issuerObj.setCity(issuer.getCity());
				issuerObj.setState(issuer.getState());
				issuerObj.setZip(issuer.getZip());
				issuerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
				issuerObj.setCommentId(null);
				issuerObj = issuerService.saveIssuer(issuerObj);
				encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerObj.getId()));
			}

			LOGGER.info("Saved Issuer Details");
		} catch (Exception e) {
			LOGGER.error("Invalid Issuer Details" + e.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_ISSUER_EXCEPTION, e, null, Severity.HIGH);
		}
		return "redirect:/admin/issuer/details/" + encryptedIssuerId;
	}

	
	/**
	 * @param model, Model Object
	 * Not to be used, instead use [/admin/issuer/downloadlogo/{hiosIssuerId}
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/issuer/downloadlogobyid/{encIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	@ResponseBody
	public byte[] downloadIssuerLogo(@PathVariable("encIssuerId") String encIssuerId,HttpServletResponse response) {
		try {
			byte[] bytes = null;
			bytes = issuerService.getIssuerLogoById(encIssuerId);
			response.setHeader("Content-Disposition", "attachment;filename=logo.png");
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}catch (Exception e) {
			LOGGER.error("Exception to decrypt issuer id", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return null;
	}		

	/**
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/issuer/downloadlogo/{hiosIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	@ResponseBody
	public byte[] downloadIssuerLogoByHIOSID(@PathVariable("hiosIssuerId") String hiosIssuerId,HttpServletResponse response) {
		try {
			byte[] bytes = null;
			bytes = issuerService.getIssuerLogoByHiosId(hiosIssuerId);
			response.setHeader("Content-Disposition", "inline");
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}catch (Exception e) {
			LOGGER.error("Exception to get logo for issuer hios id", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return null;
	}		
		
	
	@RequestMapping(value = "/admin/issuer/company/profile/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String issuerCompanyProfile(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		String decryptedIssuerId =  null;
		try{
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			LOGGER.info("Save Issuer Details");	
			decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuer = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuer);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			model.addAttribute(PlanMgmtConstants.COMPANY_LOGO, PlanMgmtUtil.getIssuerLogoURLForAdminByHIOSID(issuer.getLogoURL(), issuer.getHiosIssuerId(), appUrl));
			if(null != issuer.getLogo()) {
				model.addAttribute(PlanMgmtConstants.COMPANY_LOGO_EXIST, true);
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_COMPANY_PROFILE_EXCEPTION, null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}
		return "admin/issuer/company/profile"; 
	}

	@RequestMapping(value = "/admin/issuer/company/profile/edit/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String editCompanyProfile(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		String decryptedIssuerId =  null;
		try{
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			LOGGER.info("Save Issuer Details");	
			decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuer = null;
			String error = (String)model.asMap().get("ERROR");
			if(StringUtils.isNotBlank(error)){
				issuer = (Issuer)model.asMap().get("ISSUER");
			}else {
				issuer = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			}
			
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuer);
			model.addAttribute(PlanMgmtConstants.STATE_LIST, new StateHelper().getAllStates());
			model.addAttribute(PlanMgmtConstants.REDIRECT_URL, PlanMgmtConstants.COMPANY_PROFILE_REDIRECT_URL+Integer.parseInt(decryptedIssuerId)+"");
			model.addAttribute("exchangeType", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
			if(stateCode.equals(PlanMgmtConstants.STATECODEFORCA)){
				model.addAttribute(PlanMgmtConstants.STATECODEONEXCHANGE, stateCode);
			}else if(stateCode.equals(GhixConstants.NM))
			{
				model.addAttribute(PlanMgmtConstants.STATECODEONEXCHANGE,GhixConstants.NM);
			}
			model.addAttribute(PlanMgmtConstants.COMPANY_LOGO, PlanMgmtUtil.getIssuerLogoURLForAdminByHIOSID(issuer.getLogoURL(), issuer.getHiosIssuerId(), appUrl));
			if(null != issuer.getLogo()) {
				model.addAttribute(PlanMgmtConstants.COMPANY_LOGO_EXIST, true);
			}		
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_EDIT_COMPANY_PROFILE_EXCEPTION, null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return "admin/issuer/company/profile/edit";
	}

	/*To display issuer logo on page load
	 * This should not be used
	 * This URL invoked by other module like SHOP,Consumer Portal. 
	 * that's why removed Pre Authorize*/
	@RequestMapping(value = "/admin/issuer/company/profile/logo/view/{encIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	/*@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)*/
	@ResponseBody
	public byte[] getIssuerLogoById(@PathVariable("encIssuerId") String encIssuerId, HttpServletResponse response)
			throws ContentManagementServiceException, IOException {

		byte[] bytes = null;

		try {
			bytes = issuerService.getIssuerLogoById(encIssuerId);
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}
		catch (Exception e) {
			LOGGER.error("Exception in download Documents", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION,
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return null;
	}

	/*To display issuer logo on page load
	 * This URL invoked by other module like SHOP,Consumer Portal. 
	 * that's why removed Pre Authorize*/
	@RequestMapping(value = "/admin/issuer/company/profile/logo/hid/{hiosIssuerId}", method = RequestMethod.GET, produces="image/png;image/jpeg; charset=utf-8")
	/*@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)*/
	@ResponseBody
	public byte[] getIssuerLogoByHiosId(@PathVariable("hiosIssuerId") String hiosIssuerId, HttpServletResponse response)
			throws ContentManagementServiceException, IOException {

		byte[] bytes = null;

		try {
			bytes = issuerService.getIssuerLogoByHiosId(hiosIssuerId);
			response.setContentType(planMgmtUtils.getContentType(bytes));
			FileCopyUtils.copy(bytes, response.getOutputStream());
		}
		catch (Exception e) {
			LOGGER.error("Exception in download Documents", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOADDOCUMENTS_EXCEPTION,
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return null;
	}

	// uploading the files into their specific folders
	private String uploadFileIntoSpecificFolder(String fileToUpload, String folderName, MultipartFile file, Integer issuerId) {
		Issuer issuerObj = issuerService.getIssuerById(issuerId);
		String returnString = null;
		String documentId = null;
		try {
			// creating a new file name with the time stamp
			String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename())
					+ GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT + FilenameUtils.getExtension(file.getOriginalFilename());
			// upload file to DMS
			try {
				String filePath = GhixConstants.ISSUER_DOC_PATH + GhixConstants.FRONT_SLASH + issuerObj.getId() + GhixConstants.FRONT_SLASH + folderName;
				boolean isValidPath = planMgmtUtils.isValidPath(filePath);
				if(isValidPath){
					documentId = ecmService.createContent(filePath, modifiedFileName, file.getBytes()
							, ECMConstants.Issuer.DOC_CATEGORY, ECMConstants.Issuer.DOC_SUB_CATEGORY_SUPPORT, null);
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			} catch (ContentManagementServiceException e) {
				LOGGER.error("Error while reading File from DMS : " + e.getMessage());
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.CONTENT_MANAGEMENT_EXCEPTION_FOR_UPLOAD, e, null, Severity.LOW);
			}
			if(null != documentId){
				returnString = fileToUpload + "|" + file.getOriginalFilename() + "|" + documentId; // return original file name and DMS id
			}
			LOGGER.info("Upload issuer documents DocumentId");
		} catch (Exception ex) {
			LOGGER.error("Fail to upload issuer file. Ex: ",ex);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.UPLOAD_LOGO_FILE_INTO_SPECIFIC_FOLDER_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
		if(null != returnString){
			return returnString;
		}else{
			return PlanMgmtConstants.EMPTY_STRING;
		}
	}

	// saving the company profile information into the issuer table.
	@RequestMapping(value = "/admin/issuer/company/profile/save", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String saveCompanyProfile(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model,
			@RequestParam(value = PlanMgmtConstants.COMPANY_LOGOUI, required = true) MultipartFile companyLogo,
			@RequestParam(value = PlanMgmtConstants.REDIRECT_TO, required = false) String redirectUrl, 
			@RequestParam(value = "id", required = false) Integer issuerId, 
			HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String redirectUrl_new = null;
		/*boolean siteUrlValidation = false;
		boolean companySiteUrlValidation = false;*/
		try {
			/*siteUrlValidation = planMgmtUtils.isValidUrl(issuer.getSiteUrl());
			companySiteUrlValidation = planMgmtUtils.isValidUrl(issuer.getCompanySiteUrl());*/
			
			//if(siteUrlValidation && companySiteUrlValidation){
				Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				Set<ConstraintViolation<Issuer>> violations = validator.validate(issuer, Issuer.EditCompanyProfile.class);
				if(violations != null && !violations.isEmpty()){
					//throw exception in case client side validation breached
					throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
		//	}else{
			//	throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			//}
			
			Issuer issuerObj = issuerService.getIssuerById(issuerId);
			String encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerObj.getId()));
			if(null != issuerObj){
				LOGGER.info("Admin Issuer Update profile : Save Company Page Info");
				// set the values to the issuer object.
				issuerObj.setCompanyLegalName(issuer.getCompanyLegalName());
				issuerObj.setStateOfDomicile(issuer.getStateOfDomicile());
				issuerObj.setCompanyAddressLine1(issuer.getCompanyAddressLine1());
				issuerObj.setCompanyAddressLine2(issuer.getCompanyAddressLine2());
				issuerObj.setCompanyCity(issuer.getCompanyCity());
				issuerObj.setCompanyState(issuer.getCompanyState());
				issuerObj.setCompanyZip(issuer.getCompanyZip());
				if(StringUtils.isNotBlank(issuer.getSiteUrl())){
					issuerObj.setSiteUrl(issuer.getSiteUrl());
				}else{
					issuerObj.setSiteUrl(null);
				}
				if(StringUtils.isNotBlank(issuer.getCompanySiteUrl())){
					issuerObj.setCompanySiteUrl(issuer.getCompanySiteUrl());
				}else{
					issuerObj.setCompanySiteUrl(null);
				}
				issuerObj.setNationalProducerNumber(issuer.getNationalProducerNumber());
				issuerObj.setAgentFirstName(issuer.getAgentFirstName());
				issuerObj.setAgentLastName(issuer.getAgentLastName());
				issuerObj.setPaymentUrl(issuer.getPaymentUrl());
				issuerObj.setDisclaimer(issuer.getDisclaimer());
				try {
					issuerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
				} catch (InvalidUserException e) {
					LOGGER.error("Error While Setting updatedBy");
				}
				// Upload Company LOGO at CDN server and set LOGO Content(BLOG) and CDN URL in Issuer table.
				String uploadStatus = planMgmtUtils.uploadLogoAndSetDetails(companyLogo, issuerObj);
				if("SUCCESS".equalsIgnoreCase(uploadStatus)) {
					// save the updated details of the issuer in the issuer table
					issuerService.saveIssuer(issuerObj);
				}else {
					LOGGER.error("Admin Issuer Update profile : Error while Issuer information save");
					redirectAttributes.addFlashAttribute("ERROR", uploadStatus);
					redirectAttributes.addFlashAttribute("ISSUER", issuerObj);
					return "redirect:/admin/issuer/company/profile/edit/"+ encryptedIssuerId;
				}
				
				LOGGER.info("Admin Issuer Update profile : Saved the Issuer information");
				redirectUrl_new = PlanMgmtConstants.COMPANY_PROFILE_REDIRECT_URL + encryptedIssuerId;
			}
		}
		catch (GIRuntimeException ex) {
			throw ex;
		}
		catch (Exception e) {
			LOGGER.error("Error occured while saving company profile: ", e.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_COMPANY_PROFILE_EXCEPTION, e, null, Severity.HIGH);
		}
		return redirectUrl_new;
	}

	// issuer account individual market profile
	/*
	 * @Suneel: Secured the method with permission
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/issuer/market/individual/profile/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String issuerIndividualMarketProfile(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		String decryptedIssuerId = null;
		try{
			decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			if(null != issuerObj){
				LOGGER.info("Admin :: Individual Market Profile Information Page");
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute("exchangeType", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_INDIVIDUAL_MARKET_PROFILE_EXCEPTION, null, ExceptionUtils.getFullStackTrace(ex), Severity.LOW);
		}
		return "admin/issuer/market/individual/profile"; 
	}

	// edit individual market profile
	/*
	 * @Suneel: Secured the method with permission
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/issuer/market/individual/profile/edit/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String editIndividualMarketProfile(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		String decryptedIssuerId = null;
		try{
			decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			LOGGER.info("Admin :: Edit Individual Market Profile Information Page");
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/admin/issuer/market/individual/profile/"+ decryptedIssuerId + "");
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getIndvCustServicePhone()) && issuerObj.getIndvCustServicePhone().length() >= PlanMgmtConstants.TEN) {
				String customerPhoneNumber = issuerObj.getIndvCustServicePhone().replace("-", PlanMgmtConstants.EMPTY_STRING);
				if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
					customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
				}
				model.addAttribute(PlanMgmtConstants.PHONE1, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
				model.addAttribute(PlanMgmtConstants.PHONE2, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
				model.addAttribute(PlanMgmtConstants.PHONE3, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
			}
			if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getIndvCustServiceTollFree()) && issuerObj.getIndvCustServiceTollFree().length() >= PlanMgmtConstants.TEN) {
				String customerPhoneNumber = issuerObj.getIndvCustServiceTollFree().replace("-", PlanMgmtConstants.EMPTY_STRING);
				if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
					customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
				}
				model.addAttribute(PlanMgmtConstants.PHONE4, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
				model.addAttribute(PlanMgmtConstants.PHONE5, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
				model.addAttribute(PlanMgmtConstants.PHONE6, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
			}
			if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getIndvCustServiceTTY()) && issuerObj.getIndvCustServiceTTY().length() >= PlanMgmtConstants.TEN) {
				String customerPhoneNumber = issuerObj.getIndvCustServiceTTY().replace("-", PlanMgmtConstants.EMPTY_STRING);
				if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
					customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
				}
				model.addAttribute(PlanMgmtConstants.PHONE7, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
				model.addAttribute(PlanMgmtConstants.PHONE8, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
				model.addAttribute(PlanMgmtConstants.PHONE9, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_EDIT_COMPANY_PROFILE_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
		return "admin/issuer/market/individual/profile/edit";
	}

	// save the individual market profile
	@RequestMapping(value = "/admin/issuer/market/profile/save", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String saveMarketProfile(Model model, @ModelAttribute(PlanMgmtConstants.MARKETPROFILE) MarketProfileDTO marketProfileDTO,
			@RequestParam(value = PlanMgmtConstants.MARKET, required = false) String market, 
			@RequestParam(value = "id", required = false) Integer issuerId) {

		String encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerId));
		String redirectUrl = "redirect:/admin/issuer/market/shop/profile/"+ encryptedIssuerId;
		//boolean facingUrlValidation = false;
		try{
			//server side validation
			//facingUrlValidation = planMgmtUtils.isValidUrl(marketProfileDTO.getFacingWebSite());
			//if(facingUrlValidation){
				Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				 Set<ConstraintViolation<MarketProfileDTO>> violations = validator.validate(marketProfileDTO, MarketProfileDTO.EditIndividualProfile.class);
				 if(violations != null && !violations.isEmpty()){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			//}else{
			//	throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			//}
			
			Issuer issuerObj = issuerService.getIssuerById(issuerId);
			if(null != issuerObj){
				LOGGER.info("Admin Issuer Update profile : Save Market Info");
				if ("individual".equalsIgnoreCase(market)) {
					// setting the individual market profile info to the issuer object
					issuerObj.setIndvCustServicePhone(marketProfileDTO.getCustomerServicePhone());
					issuerObj.setIndvCustServicePhoneExt(marketProfileDTO.getCustomerServiceExt());
					issuerObj.setIndvCustServiceTollFree(marketProfileDTO.getCustServiceTollFreeNumber());
					issuerObj.setIndvCustServiceTTY(marketProfileDTO.getCustServiceTTY());
					if(StringUtils.isNotBlank(marketProfileDTO.getFacingWebSite())){
						issuerObj.setIndvSiteUrl(marketProfileDTO.getFacingWebSite());
					}else{
						issuerObj.setIndvSiteUrl(null);
					}
					redirectUrl = "redirect:/admin/issuer/market/individual/profile/"+ encryptedIssuerId;
				} else if ("shop".equalsIgnoreCase(market)) {
					// setting the shop market profile info to the issuer object
					issuerObj.setShopCustServicePhone(marketProfileDTO.getCustomerServicePhone());
					issuerObj.setShopCustServicePhoneExt(marketProfileDTO.getCustomerServiceExt());
					issuerObj.setShopCustServiceTollFree(marketProfileDTO.getCustServiceTollFreeNumber());
					issuerObj.setShopCustServiceTTY(marketProfileDTO.getCustServiceTTY());
					
					if(StringUtils.isNotBlank(marketProfileDTO.getFacingWebSite())){
						issuerObj.setShopSiteUrl(marketProfileDTO.getFacingWebSite());
					}else{
						issuerObj.setShopSiteUrl(null);
					}
					redirectUrl = "redirect:/admin/issuer/market/shop/profile/"+ encryptedIssuerId;
				}
				try {
					issuerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
					issuerObj.setCommentId(null);
				} catch (Exception e) {
					LOGGER.error("Error while setting updatedBy: ", e.getMessage());
				}
				issuerService.saveIssuer(issuerObj);
				LOGGER.debug("Admin Issuer Update profile : Saved " + SecurityUtil.sanitizeForLogging(String.valueOf(market)) + " Market Info");
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_MARKET_PROFILE_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return redirectUrl;
	}

	// shop market profile
	/*
	 * @Suneel: Secured the method with permission
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/issuer/market/shop/profile/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String issuerShopMarketProfile(Model model, @PathVariable("encIssuerId") String encryptedIssuerId) {
		try{
			Integer issuerId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedIssuerId));
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Issuer issuerObj = issuerService.getIssuerById(issuerId);
			if(null != issuerObj){
				LOGGER.info("Admin :: Shop Market Profile Information Page");
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_SHOP_MARKET_PROFILE_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
		return "admin/issuer/market/shop/profile"; 
	}

	// edit shop market profile
	/*
	 * @Suneel: Secured the method with permission
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/issuer/market/shop/profile/edit/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String editShopMarketProfile(Model model, @PathVariable("encIssuerId") String encryptedIssuerId) {
		try{
			Integer issuerId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedIssuerId));
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Issuer issuerObj = issuerService.getIssuerById(issuerId);
			if(null != issuerObj){
				LOGGER.info("Admin :: Shop Market Profile Information Page");
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/admin/issuer/market/shop/profile/"+ issuerId + "");
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getShopCustServicePhone()) && issuerObj.getShopCustServicePhone().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getShopCustServicePhone().replace("-", PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE1, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE2, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE3, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getShopCustServiceTollFree()) && issuerObj.getShopCustServiceTollFree().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getShopCustServiceTollFree().replace("-", PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE4, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE5, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE6, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
				if(org.apache.commons.lang.StringUtils.isNotBlank(issuerObj.getShopCustServiceTTY()) && issuerObj.getShopCustServiceTTY().length() >= PlanMgmtConstants.TEN) {
					String customerPhoneNumber = issuerObj.getShopCustServiceTTY().replace("-", PlanMgmtConstants.EMPTY_STRING);
					if(customerPhoneNumber.length() > PlanMgmtConstants.TEN){
						customerPhoneNumber = customerPhoneNumber.substring(PlanMgmtConstants.ONE);
					}
					model.addAttribute(PlanMgmtConstants.PHONE7, customerPhoneNumber.substring(PlanMgmtConstants.ZERO,PlanMgmtConstants.THREE));
					model.addAttribute(PlanMgmtConstants.PHONE8, customerPhoneNumber.substring(PlanMgmtConstants.THREE,PlanMgmtConstants.SIX));
					model.addAttribute(PlanMgmtConstants.PHONE9, customerPhoneNumber.substring(PlanMgmtConstants.SIX));
				}
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_EDIT_SHOP_MARKET_PROFILE_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
		return "admin/issuer/market/shop/profile/edit";
	}

	// certification status
	/*
	 * @Suneel: Secured the method with permission
	 * Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/issuer/certification/status/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String issuerCertificationStatus(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		try{
			String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			if(null != issuerObj){
				LOGGER.info("Admin :: Certification Status Information Page");
				List<Map<String, Object>> certStatusHistory = issuerService.loadIssuerStatusHistory(Integer.parseInt(decryptedIssuerId));
		
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.STATUS_HISTORY, certStatusHistory);
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_CERTIFICATION_STATUS_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
		return "admin/issuer/certification/status";
	}

	// certification status
	/*
	 * @Suneel: Secured the method with permission
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/issuer/certification/status/edit/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String editCertificationStatus(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		try{
			String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			LOGGER.info("Admin :: Edit Certification Status Information Page");
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			// fetch issuer's uploaded documents
			List<IssuerDocument> documents = issuerObj.getIssuerDocuments();
			List<HashMap<String, String>> authFiles = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> marketingFiles = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> financeDisclosureFiles = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> accreditationFiles = new ArrayList<HashMap<String, String>>();
			List<HashMap<String, String>> organizationFiles = new ArrayList<HashMap<String, String>>();
	
			if (!documents.isEmpty()) {
				for (IssuerDocument document : documents) {
					try {
						if(document != null && StringUtils.isNotBlank(document.getDocumentName())) {
							
							HashMap<String, String> hm = new HashMap<String, String>();
	
							if (DocumentType.AUTHORITY.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
									hm.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
								} else {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								authFiles.add(hm);
							} else if (DocumentType.MARKETING.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
									hm.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
								} else {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								marketingFiles.add(hm);
							} else if (DocumentType.FINANCIAL_DISCLOSURE.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
									hm.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
								} else {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								financeDisclosureFiles.add(hm);
							} else if (DocumentType.ACCREDITATION.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
									hm.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
								} else {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								accreditationFiles.add(hm);
							} else if (DocumentType.ORGANIZATION.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
									hm.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
								} else {
									hm.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								organizationFiles.add(hm);
							}
						}
					} catch (Exception e) {
						LOGGER.error("ERROR :" + e.getMessage());
						throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_EDIT_CERTIFICATION_STATUS_EXCEPTION, e, null, Severity.LOW);
					}
				}
				if (!authFiles.isEmpty()) {
					model.addAttribute(PlanMgmtConstants.AUTHORITYFILE, authFiles);
				}
				if (!marketingFiles.isEmpty()) {
					model.addAttribute(PlanMgmtConstants.MARKETINGFILE, marketingFiles);
				}
				if (!financeDisclosureFiles.isEmpty()) {
					model.addAttribute(PlanMgmtConstants.FINANCDISCLOSURESFILE, financeDisclosureFiles);
				}
				if (!accreditationFiles.isEmpty()) {
					model.addAttribute(PlanMgmtConstants.ACCREDITATIONFILES, accreditationFiles);
				}
				if (!organizationFiles.isEmpty()) {
					model.addAttribute(PlanMgmtConstants.ORGANIZATIONFILES, organizationFiles);
				}
	
			}
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			// if state code is CA then call finance module API to get payment information  
			if(stateCode.equalsIgnoreCase("CA")){
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setModuleID(issuerObj.getId());
				paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
				 paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);
				String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
				Map<String, Object> paymentMethods = getPaymentMethodsMap(paymentMethodsString);
				List<PaymentMethods> paymentMethodList = (List<PaymentMethods>)paymentMethods.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
				PaymentMethods paymentMethod = null;
				if(paymentMethodList != null && !paymentMethodList.isEmpty()){
					paymentMethod = paymentMethodList.get(0);
				}
				
				if(null == paymentMethod){
					model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, 0);
				}else{
					model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, paymentMethod.getId());
				}
			}	
			
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute("certificationStatusList", issuerObj.getIssuerStatuses());
			model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/admin/issuer/certification/status/"
					+ encIssuerId + "");
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute("isFinancialInfoRequired",DynamicPropertiesUtil.getPropertyValue(IS_FIANCIAL_INFO_REQUIRED));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
			model.addAttribute(PlanMgmtConstants.MIME_TYPE_ARRAY,MIME_TYPES);
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_EDIT_CERTIFICATION_STATUS_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
		return "admin/issuer/certification/status/edit";
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/issuer/history/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String viewIssuerHistory(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		try{
			String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			LOGGER.info("Admin :: View Issuer History");
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			List<Map<String, Object>> issuerHistoryData = issuerService.loadIssuerHistory(Integer.parseInt(decryptedIssuerId));
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute("history", issuerHistoryData);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return "admin/issuer/history";
	}

	/*
	 * @Suneel: Secured the method with permission 
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/issuer/getComment", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	@ResponseBody
	public String getCommentTextById(HttpServletRequest request) {
		String commentId = request.getParameter("commentId");
		String commentText = null;
		try {
			commentText = commentService.getCommentTextById(Integer.parseInt(commentId));
			if (commentText == null) {
				commentText = "No comment added";
			}
		} catch (Exception e) {
			LOGGER.error("error while fetching comments" + e.getMessage());
//			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.GET_COMMENT_TEXT_EXCEPTION, e, null, Severity.LOW);
			commentText = PlanMgmtConstants.EXCEPTION +":"+ e.getMessage();
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.GET_COMMENT_TEXT_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
		}
		return commentText;
	}

	/* END : HIX-1807  
	 */

	@RequestMapping(value = "/admin/issuer/accreditationdocument/view/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String viewAccreditationDocument(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		String decryptedIssuerId = null;
		try{
			decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			if(null != issuerObj){
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				LOGGER.info("New Accreditation Document Page Info");
				AccountUser accountUser=userService.getLoggedInUser();
				String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
				model.addAttribute(ROLE_NAME, activeRoleName);
				List<IssuerDocument> documents = issuerObj.getIssuerDocuments();
				if(null != documents && !(documents.isEmpty())){
					LOGGER.debug("documentsSize: " + SecurityUtil.sanitizeForLogging(String.valueOf(documents.size())));		
					List<HashMap<String, String>> authFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> marketingFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> financeDisclosureFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> accreditationFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> addsuppFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> organizationFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> payPolPractFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> enrollDisDataFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> ratingPractFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> costSharPaymentFilesList = new ArrayList<HashMap<String, String>>();
			
					for (IssuerDocument document : documents) {
						HashMap<String, String> linkHM = new HashMap<String, String>();
						if (document.getDocumentName() != null
								&& !document.getDocumentName().isEmpty()) {
							if (document.getDocumentName() != null) {
								linkHM.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
							} else {
								linkHM.put(PlanMgmtConstants.UPLOADED_FILE_LINK, PlanMgmtConstants.EMPTY_STRING);
							}
			
							if (DocumentType.AUTHORITY.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String authorityDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, authorityDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								authFilesList.add(linkHM);
			
							} else if (DocumentType.MARKETING.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String marketingDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, marketingDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								marketingFilesList.add(linkHM);
							} else if (DocumentType.FINANCIAL_DISCLOSURE.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String disclosuresDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, disclosuresDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								financeDisclosureFilesList.add(linkHM);
			
							} else if (DocumentType.ACCREDITATION.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String accreditationDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, accreditationDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								accreditationFilesList.add(linkHM);
			
							} else if (DocumentType.ADDITIONAL_SUPPORT.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String addsupportDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, addsupportDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								addsuppFilesList.add(linkHM);
			
							} else if (DocumentType.ORGANIZATION.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String organizationalDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, organizationalDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								organizationFilesList.add(linkHM);
			
							} else if (DocumentType.CLAIMS_PAY_POL.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String paypoliciesDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, paypoliciesDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								payPolPractFilesList.add(linkHM);
			
							} else if (DocumentType.ENROLLMENT_DISENROLLMENT.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String enrollDisenrollDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, enrollDisenrollDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								enrollDisDataFilesList.add(linkHM);
			
							} else if (DocumentType.RATING_PRACTICES.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String ratingpracticesDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, ratingpracticesDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								ratingPractFilesList.add(linkHM);
			
							} else if (DocumentType.COST_SHARING_PAYMENTS.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String costSharingDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink 
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, costSharingDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								costSharPaymentFilesList.add(linkHM);
							}
						}
					}
					
					model.addAttribute("authFilesList", authFilesList);
					model.addAttribute("marketingFilesList", marketingFilesList);
					model.addAttribute("financeDisclosureFilesList", financeDisclosureFilesList);
					model.addAttribute("accreditationFilesList", accreditationFilesList);
					model.addAttribute("addsuppFilesList", addsuppFilesList);
					model.addAttribute("organizationFilesList", organizationFilesList);
					model.addAttribute("payPolPractFilesList", payPolPractFilesList);
					model.addAttribute("enrollDisDataFilesList", enrollDisDataFilesList);
					model.addAttribute("ratingPractFilesList", ratingPractFilesList);
					model.addAttribute("costSharPaymentFilesList", costSharPaymentFilesList);
				}
				model.addAttribute("accreditionEntity", issuerObj.getAccreditingEntity());
				model.addAttribute("issuerAccreditation", issuerObj.getIssuerAccreditation());
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.VIEW_ACCREDITATION_DOCUMENT_EXCEPTION.getCode(), ex, null, Severity.LOW);
		}
		return "admin/issuer/accreditationdocument/view";
	}

	@RequestMapping(value = "/admin/issuer/viaccreditationdocument/edit/{encIssuerId}", method = { RequestMethod.GET })
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String newAccreditationDocument(Model model, HttpServletRequest request, @PathVariable("encIssuerId") String encIssuerId) {
		String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
		Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
		model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		if(null != issuerObj){
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			model.addAttribute("sysDate", new TSDate());
			issuerService.removeDataFromSession(request);
			try {
				List<IssuerDocument> documents = issuerObj.getIssuerDocuments();
				if(null != documents && !(documents.isEmpty())){
					LOGGER.debug("documentsSize: " + SecurityUtil.sanitizeForLogging(String.valueOf(documents.size())));
					AccountUser accountUser=userService.getLoggedInUser();
					String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
					model.addAttribute(ROLE_NAME, activeRoleName);
					List<HashMap<String, String>> authFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> marketingFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> financeDisclosureFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> accreditationFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> addsuppFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> organizationFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> payPolPractFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> enrollDisDataFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> ratingPractFilesList = new ArrayList<HashMap<String, String>>();
					List<HashMap<String, String>> costSharPaymentFilesList = new ArrayList<HashMap<String, String>>();
		
					for (IssuerDocument document : documents) {
						HashMap<String, String> linkHM = new HashMap<String, String>();
						if (document.getDocumentName() != null
								&& !document.getDocumentName().isEmpty()) {
							if (document.getDocumentName() != null) {
								linkHM.put(PlanMgmtConstants.UPLOADED_FILE_LINK, document.getDocumentName());
							} else {
								linkHM.put(PlanMgmtConstants.UPLOADED_FILE_LINK, PlanMgmtConstants.EMPTY_STRING);
							}
		
							if (DocumentType.AUTHORITY.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String authorityDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, authorityDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								authFilesList.add(linkHM);
		
							} else if (DocumentType.MARKETING.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String marketingDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, marketingDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								marketingFilesList.add(linkHM);
							} else if (DocumentType.FINANCIAL_DISCLOSURE.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String disclosuresDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, disclosuresDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								financeDisclosureFilesList.add(linkHM);
		
							} else if (DocumentType.ACCREDITATION.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String accreditationDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, accreditationDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								accreditationFilesList.add(linkHM);
		
							} else if (DocumentType.ADDITIONAL_SUPPORT.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String addsupportDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, addsupportDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								addsuppFilesList.add(linkHM);
		
							} else if (DocumentType.ORGANIZATION.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String organizationalDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, organizationalDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								organizationFilesList.add(linkHM);
		
							} else if (DocumentType.CLAIMS_PAY_POL.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String paypoliciesDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, paypoliciesDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								payPolPractFilesList.add(linkHM);
		
							} else if (DocumentType.ENROLLMENT_DISENROLLMENT.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String enrollDisenrollDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, enrollDisenrollDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								enrollDisDataFilesList.add(linkHM);
		
							} else if (DocumentType.RATING_PRACTICES.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String ratingpracticesDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, ratingpracticesDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								ratingPractFilesList.add(linkHM);
		
							} else if (DocumentType.COST_SHARING_PAYMENTS.name().equals(document.getDocumentType())) {
								String fileNameandLink = planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, document.getDocumentName(), document.getFileName());
								if (fileNameandLink != null) {
									String costSharingDocumentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
											+ document.getDocumentName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
											+ document.getFileName()
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
											+ fileNameandLink
											+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, costSharingDocumentURL);
								} else {
									linkHM.put(PlanMgmtConstants.UPLOADED_FILE_NAME, document.getFileName());
								}
								costSharPaymentFilesList.add(linkHM);
							}
						}
					}
					model.addAttribute("authFilesList", authFilesList);
					model.addAttribute("marketingFilesList", marketingFilesList);
					model.addAttribute("financeDisclosureFilesList", financeDisclosureFilesList);
					model.addAttribute("accreditationFilesList", accreditationFilesList);
					model.addAttribute("addsuppFilesList", addsuppFilesList);
					model.addAttribute("organizationFilesList", organizationFilesList);
					model.addAttribute("payPolPractFilesList", payPolPractFilesList);
					model.addAttribute("enrollDisDataFilesList", enrollDisDataFilesList);
					model.addAttribute("ratingPractFilesList", ratingPractFilesList);
					model.addAttribute("costSharPaymentFilesList", costSharPaymentFilesList);
				}
			} catch (Exception e) {
				LOGGER.error("Error while fetching issuer documents: ",  e.getMessage());
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_NEW_ACCREDITATION_DOCUMENT_EXCEPTION, e, null, Severity.HIGH);
			}
			model.addAttribute("accreditionEntity", issuerObj.getAccreditingEntity());
			model.addAttribute("issuerAccreditation", issuerObj.getIssuerAccreditation());
			model.addAttribute("selectedAccreditionEntity", issuerObj.getAccreditingEntity());
			model.addAttribute("selectedIssuerAccreditation", issuerObj.getIssuerAccreditation());
			model.addAttribute("selectedAccreditationExpDate", issuerObj.getAccreditationExpDate());
		}

		if (null != request.getSession().getAttribute("accreditingForFilePath")) {
			model.addAttribute("accreditingForFilePath", request.getSession().getAttribute("accreditingForFilePath").toString());
		}

		LOGGER.info("New Accreditation Document Page Info");
		return "admin/issuer/accreditationdocument/edit";
	}

	@RequestMapping(value = "/admin/issuer/application/documents/upload/{id}", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	@ResponseBody
	public void uploadIssuerApplicationDocuments(HttpServletRequest request, 
			@RequestParam(value = "fileToUpload", required = false) String fileToUpload, 
			@RequestParam(value = "authority", required = false) List<MultipartFile> authority, 
			@RequestParam(value = "accrediting", required = false) List<MultipartFile> accrediting, 
			@RequestParam(value = "orgData", required = false) List<MultipartFile> orgData, 
			@RequestParam(value = "marketing", required = false) List<MultipartFile> marketing, 
			@RequestParam(value = "finDisclosures", required = false) List<MultipartFile> finDisclosures, 
			@RequestParam(value = "addSupportDoc", required = false) List<MultipartFile> addSupportDoc, 
			@RequestParam(value = "payPolPractices", required = false) List<MultipartFile> payPolPractices, 
			@RequestParam(value = "enrollmentAndDis", required = false) List<MultipartFile> enrollmentAndDis, 
			@RequestParam(value = "ratingPractices", required = false) List<MultipartFile> ratingPractices, 
			@RequestParam(value = "costSharAndPayment", required = false) List<MultipartFile> costSharAndPayment, 
			@PathVariable("id") Integer issuerId, HttpServletResponse response) throws IOException {

		String returnString = PlanMgmtConstants.EMPTY_STRING;
		try {
			if (fileToUpload != null) {
				boolean isSizeExceeds = issuerService.isFileSizeExceedsLimit(authority, accrediting, orgData, marketing, finDisclosures, addSupportDoc, payPolPractices, enrollmentAndDis, ratingPractices, costSharAndPayment);		
				boolean isValidExtension = issuerService.isValidExtension(authority, accrediting, orgData, marketing, finDisclosures, addSupportDoc, payPolPractices, enrollmentAndDis, ratingPractices, costSharAndPayment);

				if(!isSizeExceeds){
				   if(isValidExtension){
					  if (fileToUpload.equalsIgnoreCase("authority")) {
						
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_AUTHORITY);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile authFile: authority){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.AUTHORITY_FOLDER, authFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.AUTHORITY_FOLDER, authFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, authFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, authFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(authFile.getSize()));
							multiPartList.add(multiPartData);
						}
						
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_AUTHORITY, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP, multiPartList);
							
						
					} else if (fileToUpload.equalsIgnoreCase("accrediting")) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ACCREDITATION);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile accreditingFile: accrediting){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ACCREDITATION_FOLDER, accreditingFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ACCREDITATION_FOLDER, accreditingFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, accreditingFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, accreditingFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(accreditingFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_ACCREDITATION, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP, multiPartList);
					} else if (fileToUpload.equalsIgnoreCase("orgData")) {
						
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ORG_DATA);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile orgDataFile: orgData){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ORGANIZATION_FOLDER, orgDataFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ORGANIZATION_FOLDER, orgDataFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, orgDataFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, orgDataFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(orgDataFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_ORG_DATA, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP, multiPartList);
					
						
					} else if (fileToUpload.equalsIgnoreCase("marketing")) {
						
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_MARKETING);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile marketingFile: marketing){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.MARKETING_FOLDER, marketingFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.MARKETING_FOLDER, marketingFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, marketingFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, marketingFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(marketingFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_MARKETING, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP, multiPartList);
				
					} else if (fileToUpload.equalsIgnoreCase("finDisclosures")) {
						
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_FIN_CLOSURES);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile finDisclosuresFile: finDisclosures){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.DISCLOSURES_FOLDER, finDisclosuresFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.DISCLOSURES_FOLDER, finDisclosuresFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, finDisclosuresFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, finDisclosuresFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(finDisclosuresFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_FIN_CLOSURES, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP, multiPartList);
						
					} else if (fileToUpload.equalsIgnoreCase("addSupportDoc")) {
						
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ADD_SUPP_DOC);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile addSupportDocFile: addSupportDoc){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ADD_SUPPORT_FOLDER, addSupportDocFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ADD_SUPPORT_FOLDER, addSupportDocFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, addSupportDocFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, addSupportDocFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(addSupportDocFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_ADD_SUPP_DOC, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP, multiPartList);
					
					} else if (fileToUpload.equalsIgnoreCase("payPolPractices")) {
						
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_CLAIM_PAYPOL_DOC);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile payPolPracticesFile: payPolPractices){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.PAYMENT_POLICIES_PRACTICES_FOLDER, payPolPracticesFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.PAYMENT_POLICIES_PRACTICES_FOLDER, payPolPracticesFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, payPolPracticesFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, payPolPracticesFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(payPolPracticesFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_CLAIM_PAYPOL_DOC, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP, multiPartList);
					
					} else if (fileToUpload.equalsIgnoreCase("enrollmentAndDis")) {
						
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ENROLLMENT_DISENROLLMENT_DOCUMENT_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_ENROLLMENT_DISENROLLMENT);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile enrollmentAndDisFile: enrollmentAndDis){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ENROLLMENT_DISENROLLMENT_FOLDER, enrollmentAndDisFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.ENROLLMENT_DISENROLLMENT_FOLDER, enrollmentAndDisFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, enrollmentAndDisFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, enrollmentAndDisFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(enrollmentAndDisFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_ENROLLMENT_DISENROLLMENT, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.ENROLLMENT_DISENROLLMENT_DOCUMENT_INFO_MAP, multiPartList);
					
					} else if (fileToUpload.equalsIgnoreCase("ratingPractices")) {
						
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_RATING_PRACTICES);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile ratingPracticesFile: ratingPractices){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.RATING_PRACTICES_FOLDER, ratingPracticesFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.RATING_PRACTICES_FOLDER, ratingPracticesFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, ratingPracticesFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, ratingPracticesFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(ratingPracticesFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_RATING_PRACTICES, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP, multiPartList);
						
					} else if (fileToUpload.equalsIgnoreCase("costSharAndPayment")) {
						List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
						List<Map<String, String>> previousMultiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP);
						if(previousMultiPartList != null && !previousMultiPartList.isEmpty()){
							multiPartList.addAll(previousMultiPartList);
						}
						String previousReturnString = (String)request.getSession().getAttribute(PlanMgmtConstants.RETURN_STRING_COST_SHARING);
						if(StringUtils.isNotBlank(previousReturnString)){
							returnString = previousReturnString;
						}
						for(MultipartFile costSharAndPaymentFile: costSharAndPayment){
							if(returnString == ""){
								returnString =  uploadFileIntoSpecificFolder(fileToUpload, IssuerService.COST_SHARING_PAYMENTS_FOLDER, costSharAndPaymentFile, issuerId);
							}else{
								returnString = returnString + "^" + uploadFileIntoSpecificFolder(fileToUpload, IssuerService.COST_SHARING_PAYMENTS_FOLDER, costSharAndPaymentFile, issuerId);
							}
							Map<String, String> multiPartData = new HashMap<String, String>();
							multiPartData.put(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME, costSharAndPaymentFile.getOriginalFilename());
							multiPartData.put(PlanMgmtConstants.MULTIPART_CONTENTTYPE, costSharAndPaymentFile.getContentType());
							multiPartData.put(PlanMgmtConstants.MULTIPART_SIZE, String.valueOf(costSharAndPaymentFile.getSize()));	
							multiPartList.add(multiPartData);
						}
						request.getSession().setAttribute(PlanMgmtConstants.RETURN_STRING_COST_SHARING, returnString);
						request.getSession().setAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP, multiPartList);
						
					}
				 }else{
					 returnString = "EXT_FAILURE";
				 }	

				}else{
					returnString = "SIZE_FAILURE";
				}
			}
		
			response.setContentType(PlanMgmtConstants.CONTENT_TYPE);
			response.getWriter().write(returnString);
			response.flushBuffer();
			
		} catch (Exception ex) {
			LOGGER.error("Exception Occured in uploadIssuerApplicationDocuments", ex.getMessage());
			returnString = PlanMgmtConstants.EXCEPTION;
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.GET_COMMENT_TEXT_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),ex.getStackTrace().toString(), null);
			response.setContentType(PlanMgmtConstants.CONTENT_TYPE);
			response.getWriter().write(returnString);
			response.flushBuffer();
		}	
		
		return;
	}
	


	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/issuer/documents/save", method = { RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String saveIssuerApplicationDocuments(Model model, 
			HttpServletRequest request, 
			@RequestParam(value = "issuerAccreditation", required = false) String issuerAccreditation, 
			@RequestParam(value = "accreditingEntity", required = false) String accreditingEntity, 
			@RequestParam(value = "hdnAuthority", required = false) String authorityStr, 
			@RequestParam(value = "hdnAccrediting", required = false) String accrediting, 
			@RequestParam(value = "hdnOrgData", required = false) String orgData, 
			@RequestParam(value = "hdnMarketing", required = false) String marketing, 
			@RequestParam(value = "hdnFinDisclosures", required = false) String finDisclosures, 
			@RequestParam(value = "hdnAddSupportDoc", required = false) String addSupportDoc, 
			@RequestParam(value = "hdnPayment_policies_practices", required = false) String hdnPayment_policies_practices, 
			@RequestParam(value = "hdnEnrollment_Disenrollment", required = false) String hdnEnrollment_Disenrollment, 
			@RequestParam(value = "hdnRating_practices", required = false) String hdnRating_practices, 
			@RequestParam(value = "hdnCost_sharing_payments", required = false) String hdnCost_sharing_payments, 
			@RequestParam(value = PlanMgmtConstants.ISSUER_ID, required = false) Integer issuerId, 
			@RequestParam(value = "firstEdit", required = false) String firstEdit, 
			@RequestParam(value = "accreditationExpDate", required = false) String accreditationExpDate) {
		Issuer issuerObj = issuerService.getIssuerById(issuerId);
		List<IssuerDocument> authorityDocuments = null;
		List<IssuerDocument> accreditingDocuments = null;
		List<IssuerDocument> orgDataDocuments = null;
		List<IssuerDocument> marketingDocuments = null;
		List<IssuerDocument> finDisclosuresDocuments = null;
		List<IssuerDocument> addSupportDocDocuments = null;
		List<IssuerDocument> enrollmentAndDisenrollments = null;
		List<IssuerDocument> ratingPracticess = null;
		List<IssuerDocument> costSharingPaments = null;
		List<IssuerDocument> paymentyPoliciesAndPractices = null;
		List<IssuerDocument> issuerDocuments = new ArrayList<IssuerDocument>();
		String encIssuerId = null;
		try {
			
			encIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerObj.getId()));
			
			if (request.getSession().getAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.AUTHORITY_DOCUMENT_INFO_MAP);
					authorityDocuments = getNewIssuerApplicationDocuments(authorityStr, DocumentType.AUTHORITY, issuerObj, multiPartList);
					issuerDocuments.addAll(authorityDocuments);

			}

			if (request.getSession().getAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>)  request.getSession().getAttribute(PlanMgmtConstants.ACCREDITATION_DOC_INFO_MAP);
					accreditingDocuments = getNewIssuerApplicationDocuments(accrediting, DocumentType.ACCREDITATION, issuerObj, multiPartList);
					issuerDocuments.addAll(accreditingDocuments);
			}

			if (request.getSession().getAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ORG_DATA_DOC_INFO_MAP);
					orgDataDocuments = getNewIssuerApplicationDocuments(orgData, DocumentType.ORGANIZATION, issuerObj, multiPartList);
					issuerDocuments.addAll(orgDataDocuments);
			}

			if (request.getSession().getAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.MARKETING_DOC_INFO_MAP);
					marketingDocuments = getNewIssuerApplicationDocuments(marketing, DocumentType.MARKETING, issuerObj, multiPartList);
					issuerDocuments.addAll(marketingDocuments);
			}

			if (request.getSession().getAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.FIN_CLOSURES_DOC_INFO_MAP);
					finDisclosuresDocuments = getNewIssuerApplicationDocuments(finDisclosures, DocumentType.FINANCIAL_DISCLOSURE, issuerObj, multiPartList);
					issuerDocuments.addAll(finDisclosuresDocuments);
			}

			if (request.getSession().getAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.ADD_SUPP_DOC_INFO_MAP);
					addSupportDocDocuments = getNewIssuerApplicationDocuments(addSupportDoc, DocumentType.ADDITIONAL_SUPPORT, issuerObj, multiPartList);
					issuerDocuments.addAll(addSupportDocDocuments);
			}

			if (request.getSession().getAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.CLAIM_PAYPOL_DOC_INFO_MAP);
					paymentyPoliciesAndPractices = getNewIssuerApplicationDocuments(hdnPayment_policies_practices, DocumentType.CLAIMS_PAY_POL, issuerObj, multiPartList);
					issuerDocuments.addAll(paymentyPoliciesAndPractices);
			}

			if (request.getSession().getAttribute("enrollDisenrollDocumentInfoMap") != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>) request.getSession().getAttribute("enrollDisenrollDocumentInfoMap");
					enrollmentAndDisenrollments = getNewIssuerApplicationDocuments(hdnEnrollment_Disenrollment, DocumentType.ENROLLMENT_DISENROLLMENT, issuerObj, multiPartList);
					issuerDocuments.addAll(enrollmentAndDisenrollments);
			}

			if (request.getSession().getAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>) request.getSession().getAttribute(PlanMgmtConstants.RATING_PRACTICES_DOC_INFO_MAP);
					ratingPracticess = getNewIssuerApplicationDocuments(hdnRating_practices, DocumentType.RATING_PRACTICES, issuerObj, multiPartList);
					issuerDocuments.addAll(ratingPracticess);
			}

			if (request.getSession().getAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP) != null) {
				List<Map<String, String>> multiPartList = new ArrayList<Map<String, String>>();
				multiPartList = (List<Map<String, String>>)  request.getSession().getAttribute(PlanMgmtConstants.COST_SHARING_PAY_DOC_INFO_MAP);
					costSharingPaments = getNewIssuerApplicationDocuments(hdnCost_sharing_payments, DocumentType.COST_SHARING_PAYMENTS, issuerObj, multiPartList);
					issuerDocuments.addAll(costSharingPaments);
			}

			issuerObj.setIssuerDocuments(issuerDocuments);

			if (issuerAccreditation.equalsIgnoreCase(IssuerAccreditation.YES.toString())) {
				issuerObj.setIssuerAccreditation(IssuerAccreditation.YES);
			} else if (issuerAccreditation.equalsIgnoreCase(IssuerAccreditation.NO.toString())) {
				issuerObj.setIssuerAccreditation(IssuerAccreditation.NO);
			} else if (issuerAccreditation.equalsIgnoreCase(IssuerAccreditation.EXCELLENT.toString())) {
				issuerObj.setIssuerAccreditation(IssuerAccreditation.EXCELLENT);
			}

			issuerObj.setAccreditingEntity(accreditingEntity);

			if (accreditationExpDate != null && !accreditationExpDate.isEmpty()) {
				try {
					Date date;
					SimpleDateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
					date = dateFormat.parse(accreditationExpDate);
					issuerObj.setAccreditationExpDate(date);
				} catch (ParseException e) {
					LOGGER.error("Error while parsing date: ", e.getMessage());
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.DATEPARSING_EXCEPTION, e, null, Severity.LOW);
				}
			}else{
				issuerObj.setAccreditationExpDate(null);
			}
			
			try {
				issuerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
			} catch (InvalidUserException e) {
				LOGGER.error("ERROR: while setting updatedBy: ", e.getMessage());
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.INVALID_USER_INFO_EXCEPTION, e, null, Severity.LOW);
			}
			
			//server side validation
			Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			Set<ConstraintViolation<Issuer>> violations = validator.validate(issuerObj, Issuer.EditAccreditationDocuments.class);
			if(violations != null && !violations.isEmpty()){
				//throw exception in case client side validation breached
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			} 
			
			issuerService.saveIssuer(issuerObj);
			// if page was edited then set a flag
			request.getSession().setAttribute("isFirstEdit", PlanMgmtConstants.TRUE);
			// cath the file name in attribute
			issuerService.removeDataFromSession(request);
		} catch (Exception ex) {
			LOGGER.error("Exception in saveIssuerApplicationDocuments: " + ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_ISSUER_APPLICATION_DOCUMENTS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return "redirect:/admin/issuer/accreditationdocument/view/"+ encIssuerId;
	}

	private List<IssuerDocument> getNewIssuerApplicationDocuments(String documentId, DocumentType documentType, Issuer issuer, List<Map<String, String>> multipartFileData) {
		List<String> filesList = new ArrayList<String>();
		String []splittedDocumentIds = documentId.split("\\|");
		for(String docId: splittedDocumentIds){
			filesList.add(docId);
		}
		return getIssuerDocuments(filesList, documentType, issuer, multipartFileData);
	}

	private List<IssuerDocument> getIssuerDocuments(List<String> files, DocumentType documentType, Issuer issuer, List<Map<String, String>> multipartFileData) {
		List<IssuerDocument> documents = new ArrayList<IssuerDocument>();
		Integer userId = null;
		try {
			userId = userService.getLoggedInUser().getId();
			for(int i=0; i<files.size(); i++){
				IssuerDocument document = new IssuerDocument();
				document.setDocumentName(files.get(i));
				document.setDocumentType(documentType.name());
				document.setIssuer(issuer);
				documents.add(document);
				document.setCreatedBy(userId);
				document.setLastUpdatedBy(userId);
				document.setFileName(multipartFileData.get(i).get(PlanMgmtConstants.MULTIPART_ORIGINAL_FILE_NAME));
				document.setFileType(multipartFileData.get(i).get(PlanMgmtConstants.MULTIPART_CONTENTTYPE));
				document.setFileSize(Long.parseLong(multipartFileData.get(i).get(PlanMgmtConstants.MULTIPART_SIZE)));
			}
		} catch (InvalidUserException e) {
			LOGGER.error("Invalid User: ", e.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_DOCUMENT_EXCEPTION, e, null, Severity.LOW);
		}
		return documents;
	}

	@RequestMapping(value = "/admin/document/filedownload", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public ModelAndView downloadDocumentsUploaded(@RequestParam(value = "documentId", required = false) String documentId, @RequestParam(value = "fileName", required = false) String fileName, HttpServletResponse response) throws IOException {
		try {
			byte[] attachment = planMgmtService.getAttachment(documentId);

			if (fileName.equals("PlanSBC_Document")) {
				response.setContentType("application/pdf");
			} else if (fileName.contains("_Document")
					&& !fileName.equals("PlanSBC_Document")) {
				response.setContentType("application/xml");
			} else {
				response.setContentType("application/octet-stream");
			}
			response.setContentLength(attachment.length);
			Content content = planMgmtService.getContent(documentId);

			if (null != content && StringUtils.isNotBlank(content.getTitle())) {
				String originalFileName = content.getTitle().replaceAll("[ ]", PlanMgmtConstants.UNDERSCORE);
				response.setHeader("Content-Disposition", "attachment;filename="+ originalFileName.replaceAll(UNSECURE_CHARS_REGEX, StringUtils.EMPTY).trim());
				FileCopyUtils.copy(attachment, response.getOutputStream());
			}
		} catch (Exception ex) {
			LOGGER.error("Exception Occured: ", ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_DOCUMENTS_UPLOADED_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}

		return null;
	}

	private String getInvalidHIOSString(List<IssuerQualityRatingPojo> invalidDataList) {
		String invalidHiosString = null;
		try{
			for (IssuerQualityRatingPojo invalidQualityRatingPojo : invalidDataList) {
				if (invalidQualityRatingPojo.getHIOSId() != null) {
					if (invalidHiosString != null) {
						invalidHiosString = invalidHiosString + ","
								+ invalidQualityRatingPojo.getHIOSId();
					} else {
						invalidHiosString = invalidQualityRatingPojo.getHIOSId();
					}
				}
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.INVALID_HIOS_STRING_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return invalidHiosString;
	}	

	private String getSpecificFormat() {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
			Date date = new TSDate();
			return(sdf.format(date));
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.GET_SPECIFIC_FORMAT_EXCEPTION, null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}	
	}

	@RequestMapping(value = "/admin/issuer/checkDuplicateHios", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	@ResponseBody
	public synchronized String isDuplicateHIOS(@RequestParam(value = "hiosId", required = false) String hiosId) {
		AccountUser accountUser = null;
		try {
			accountUser = userService.getLoggedInUser();
			Issuer issuer = issuerService.getIssuerByHiosID(hiosId.trim());
			if (issuer != null) {
				return Integer.toString(issuer.getId());
			}
		} catch (Exception e) {
			giMonitorService.saveOrUpdateErrorLog(String.valueOf(PlanMgmtErrorCodes.ErrorCode.DUPLICATE_HIOS_EXCEPTION), new TSDate(), Exception.class.getName(),ExceptionUtils.getFullStackTrace(e), accountUser);
			return PlanMgmtConstants.MULTIPLE_ISSUERS_FOUND;
		}
		return PlanMgmtConstants.EMPTY_STRING;
	}
		
	
	
	

	

	@RequestMapping(value = "/admin/crosswalk/DownloadExcel", method = RequestMethod.GET)
	@ResponseBody public byte[] getCrosswalkFile(HttpServletRequest request ,HttpServletResponse response) {
		byte[] bytes = null;
		try {
			String ecmId = request.getParameter("ecmId");
			if(StringUtils.isNotEmpty(ecmId)){
				Content content = ecmService.getContentById(ghixJasyptEncrytorUtil.decryptStringByJasypt(ecmId));
				String fileName = content.getOriginalFileName();
				if(fileName.contains(PlanMgmtConstants.SPACE)){
					fileName = fileName.replace(PlanMgmtConstants.SPACE, PlanMgmtConstants.UNDERSCORE);
				}
				bytes = ecmService.getContentDataById(ghixJasyptEncrytorUtil.decryptStringByJasypt(ecmId));
				response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
				response.setContentType(content.getMimeType());
				FileCopyUtils.copy(bytes, response.getOutputStream());
			}
		} catch (Exception e) {
			LOGGER.error( " Error occurred " + e.getMessage());
		}
		return null;
	}
	
	@RequestMapping(value = "admin/issuer/uploadExcel/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String uploadCrossWalkPlan(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		String decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
		try{
			AccountUser currentAccountUser=userService.getLoggedInUser();
			String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
		}catch (Exception e) {
			LOGGER.error( " Error occurred " + e.getMessage());
		}
		Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerRepId));
		model.addAttribute(ISSUER_OBJ, issuerObj);
		model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		return RETURN_EXCEL_CROSSWALK_UPLOAD_PAGE;
	}

	@RequestMapping(value = "admin/issuer/uploadExcel/{encIssuerId}", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String uploadCrossWalkPlan2( @PathVariable("encIssuerId") String encIssuerId,
			Model model, final HttpServletRequest request,
			@RequestParam("fileUploadExcel") MultipartFile[] fileList
			) {
		try{
			String decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			AccountUser currentAccountUser=userService.getLoggedInUser();
			String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			String applicableYear = request.getParameter("field_name");
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerRepId));
			LOGGER.info("Admin ::Upload CROSSWALK Plan");
			model.addAttribute(ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(APPLICABLE_YEAR ,applicableYear );
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
			try {
				
				uploadCrossWalkExcels(model, fileList , applicableYear , issuerObj ,userService.getLoggedInUser().getId());
			}
			catch (Exception e) {
				model.addAttribute(ATTR_MSG_CROSSWALK_EXCEL, ATTR_MSG_CROSSWALK_UPLOAD_FAIL + e.getMessage());
				LOGGER.error(EXCEPTIONS + e.getMessage(), e);
			}
			finally {
				LOGGER.info("Loading CROSSWALK uploadCrossWalkExcel End");
			}
			
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return RETURN_EXCEL_CROSSWALK_UPLOAD_PAGE;
	}

	/**
	 * Method is used to upload CrossWalk Excel to FTP Server. 
	 */
	private void uploadCrossWalkExcels(Model model, MultipartFile[] fileList ,String applicableYear , Issuer currentIssuer ,int userId) throws UnknownHostException {

		try {
			
			if (null == fileList||null==applicableYear) {
				model.addAttribute(ATTR_MSG_CROSSWALK_UPLOAD_FAIL, "as required fields are missing.");
				return;
			}
			
			String fileName = null;
			SerffPlanMgmt trackingRecord = null;
			String msg = null;
			InputStream inputStream = null;
			
			for (MultipartFile multipartFile : fileList) {
				
				if (null != multipartFile) {
				
					// Creates tracking record in SERFF_PLAN_MGMT table.
					trackingRecord = planMgmtUtils.createSerffPlanMgmtRecord(PlanMgmtConstants.TRANSFER_CROSSWALK,
							SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.P);
					// HIX-89724: Display Plan Year for Crosswalk list view
					trackingRecord.setSerffTrackNum(applicableYear);
					
					trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);
					
					fileName = multipartFile.getOriginalFilename();
					 // Update tracking record from SERFF_PLAN_MGMT table.ss
					 trackingRecord.setAttachmentsList(fileName);
					 trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.W);
					 trackingRecord.setRequestStateDesc("CrossWalk upload In progress.");
					 Integer id = Integer.getInteger("" ,currentIssuer.getId());
					 trackingRecord.setIssuerId(id.toString());
					 trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);
					 //call	method to persist file
					 inputStream = multipartFile.getInputStream();
					 msg = crosswalkReader.loadExcel(inputStream, fileName, applicableYear, currentIssuer, userId, trackingRecord);
					 
					 if (null != inputStream) {
						 inputStream.close();
						 inputStream = null;
					 }
				}
			}

			if (msg.toLowerCase().contains("successfully")) {
				model.addAttribute(ATTR_MSG_CROSSWALK_EXCEL, msg);
			} else {
				model.addAttribute(ATTR_MSG_CROSSWALK_UPLOAD_FAIL, msg);
			}
		}
		catch (IOException e) {
			model.addAttribute(ATTR_MSG_CROSSWALK_UPLOAD_FAIL, "Failed to upload Plan ID Crosswalk file.");
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		catch (Exception e) {
			model.addAttribute(ATTR_MSG_CROSSWALK_UPLOAD_FAIL, "Failed to upload Plan ID Crosswalk file.");
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
	}

	@RequestMapping(value = "admin/issuer/crossWalkStatus/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String crossWalkPlan(Model model, @PathVariable("encIssuerId") String encIssuerId) {
		LOGGER.info("crossWalkPlan :: View crosswalk status");
		try{
			String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			List<SerffDocumentPlanPOJO> serffDocList = null;
			String encryptedECMcDocID = null;
			AccountUser currentAccountUser=userService.getLoggedInUser();
			String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			if(null!= issuerObj){
				
				Integer id = new Integer(issuerObj.getId());
				serffDocList = serffService.getSerffDocumentByIssuerId(id.toString(), PlanMgmtConstants.TRANSFER_CROSSWALK);
				
				if(!CollectionUtils.isEmpty(serffDocList)){
					for(SerffDocumentPlanPOJO serffDoc : serffDocList ){
						encryptedECMcDocID = ghixJasyptEncrytorUtil.encryptStringByJasypt(serffDoc.getEcmDocId());
						serffDoc.setEcmDocId(encryptedECMcDocID);
					}
					
				}
				
			}
			model.addAttribute(ISSUER_OBJ, issuerObj);
			model.addAttribute("serffDocList" , serffDocList);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		return RETURN_EXCEL_CROSSWALK_STATUS_PAGE;
	}

	/**
	 * Method is used Load Network Transparency Excel page.
	 */
	@RequestMapping(value = "admin/issuer/loadNetworkTransparency/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String loadNetworkTransparency(Model model, @PathVariable("encIssuerId") String encIssuerId) {

		LOGGER.debug("Load Network Transparency Excel page Start");
		String decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);

		try {
			AccountUser currentAccountUser = userService.getLoggedInUser();
			String activeRoleName = currentAccountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);

			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerRepId));
			model.addAttribute(ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		}
		catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.FAILED_TO_LOAD_NETWORK_TRANS.getCode(), ex, null, Severity.HIGH);
		}
		finally {
			LOGGER.debug("Load Network Transparency Excel page End");
		}
		return RETURN_EXCEL_LOAD_NW_TRANS_PAGE;
	}

	/**
	 * Method is used to Process and Persist Network Transparency Excel.
	 */
	@RequestMapping(value = "admin/issuer/processAndPersistNetworkTransExcel/{encIssuerId}", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String processAndPersistNetworkTransExcel(@PathVariable("encIssuerId") String encIssuerId, Model model,
			final HttpServletRequest request, @RequestParam("fileUploadExcel") MultipartFile[] fileList) {

		LOGGER.debug("Process and Persist Network Transparency Excel Start");

		try {
			String decryptedIssuerRepId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			AccountUser currentAccountUser = userService.getLoggedInUser();
			String activeRoleName = currentAccountUser.getActiveModuleName().toUpperCase();
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerRepId));
			String applicableYear = request.getParameter("field_name");
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			model.addAttribute(ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(APPLICABLE_YEAR, applicableYear);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, stateCode);
			// process and persist Network Transparency Data File.
			String statusMessage = issuerService.processAndPersistNetworkTransparencyDataFile(fileList, applicableYear, issuerObj, stateCode, currentAccountUser.getId());

			if (statusMessage.toLowerCase().contains("successfully")) {
				model.addAttribute("uploadSuccessNetworkTrans", statusMessage);
			}
			else {
				model.addAttribute("uploadFailedNetworkTrans", statusMessage);
			}
		}
		catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.FAILED_TO_LOAD_NETWORK_TRANS.getCode(), ex, null, Severity.HIGH);
		}
		finally {
			LOGGER.debug("Process and Persist Network Transparency Excel End");
		}
		return RETURN_EXCEL_PROCESS_NW_TRANS_PAGE;
	}

	/**
	 * Display Network Transparency Excel data.
	 */
	@RequestMapping(value = "admin/issuer/displayNetworkTransparencyData/{encIssuerId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String displayNetworkTransparencyData(Model model, @PathVariable("encIssuerId") String encIssuerId) {

		LOGGER.debug("Display Network Transparency Excel data Start");

		try {
			String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			List<SerffDocumentPlanPOJO> serffDocList = null;
			AccountUser currentAccountUser = userService.getLoggedInUser();
			String activeRoleName = currentAccountUser.getActiveModuleName().toUpperCase();

			model.addAttribute(ROLE_NAME, activeRoleName);

			if (null != issuerObj) {

				String encryptedECMcDocID = null;
				serffDocList = serffService.getSerffDocumentByIssuerId(String.valueOf(issuerObj.getId()), PlanMgmtConstants.TRANSFER_NW_TRANS);

				if (!CollectionUtils.isEmpty(serffDocList)) {

					for (SerffDocumentPlanPOJO serffDoc : serffDocList) {
						encryptedECMcDocID = ghixJasyptEncrytorUtil.encryptStringByJasypt(serffDoc.getEcmDocId());
						serffDoc.setEcmDocId(encryptedECMcDocID);
					}
				}
				model.addAttribute("serffDocList", serffDocList);
			}
			model.addAttribute(ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		}
		catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.FAILED_TO_POPULATE_NETWORK_TRANS_DATA.getCode(), ex, null, Severity.HIGH);
		}
		finally {
			LOGGER.debug("Display Network Transparency Excel data End");
		}
		return RETURN_EXCEL_DISPLAY_NW_TRANS_DATA_PAGE;
	}

	@RequestMapping(value = "admin/issuer/issuerPaymentInfo/{encIssuerId}",method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String issuerPaymentInfo(Model model ,@PathVariable("encIssuerId") String encIssuerId) {
		try{
			String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			IssuerPaymentInformation issuerPaymentInfo = null;
			String decryptPassword = "";
			String decryptPasswordSecureKey = "";
			AccountUser accountUser=userService.getLoggedInUser();
			String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
			
			if(null!= issuerObj){
				
				Integer id = new Integer(issuerObj.getId());
				issuerPaymentInfo = serffService.getIssuerPaymentInfo(id);
			}
			if(null!= issuerPaymentInfo ){
				
				if(StringUtils.isNotBlank(issuerPaymentInfo.getPassword())){
					decryptPassword	= ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInfo.getPassword());
				}

				if(StringUtils.isNotBlank(issuerPaymentInfo.getPasswordSecuredKey())){
					decryptPasswordSecureKey = ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInfo.getPasswordSecuredKey());
				}

				issuerPaymentInfo.setPassword(decryptPassword);
				issuerPaymentInfo.setPasswordSecuredKey(decryptPasswordSecureKey);
			}
			model.addAttribute(ISSUER_OBJ, issuerObj);
			model.addAttribute(ROLE_NAME, activeRoleName);
			model.addAttribute("issuerPaymentInfo" , issuerPaymentInfo);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		
		return RETURN_ISSUER_PAYMENT_PAGE ;
	}
	
	
	@RequestMapping(value = "admin/issuer/editIssuerPaymentInfo/{encIssuerId}", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String editIssuerPaymentInfo(Model model ,@PathVariable("encIssuerId") String issuerId) {
		try{
			String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
			IssuerPaymentInformation issuerPaymentInfo = null;
			String decryptPassword = "";
			String decryptPasswordSecureKey = "";
			AccountUser currentAccountUser=userService.getLoggedInUser();
			String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
			model.addAttribute(ROLE_NAME, activeRoleName);
			if(null!= issuerObj){
				
				Integer id = new Integer(issuerObj.getId());
				issuerPaymentInfo = serffService.getIssuerPaymentInfo(id);
				if(null!= issuerPaymentInfo ){
					if(StringUtils.isNotBlank(issuerPaymentInfo.getPassword())){
						decryptPassword	= ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInfo.getPassword());
					}

					if(StringUtils.isNotBlank(issuerPaymentInfo.getPasswordSecuredKey())){
						decryptPasswordSecureKey = ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInfo.getPasswordSecuredKey());
					}
					issuerPaymentInfo.setPassword(decryptPassword);
					issuerPaymentInfo.setPasswordSecuredKey(decryptPasswordSecureKey);
				}
			}
			model.addAttribute(ISSUER_OBJ, issuerObj);
			model.addAttribute("issuerPaymentInfo" , issuerPaymentInfo);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		
		return RETURN_EDIT_ISSUER_PAYMENT_PAGE ;
	}
	
	
	
	
	@RequestMapping(value = "/admin/issuer/application/documents/delete/{issuerId}", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	@ResponseBody 
	public String deleteAccDocuments(@PathVariable(value = "issuerId") String issuerId, @RequestParam(value = "documentId", required = false) String documentID) {
		try{
			String response = issuerService.deleteAccreditationDocument(documentID, Integer.parseInt(issuerId));
			if(!response.equals("")){
				return PlanMgmtConstants.SUCCESS;
			} else{
				return PlanMgmtConstants.ERROR;
			}	
		} catch (Exception ex){
			return PlanMgmtConstants.ERROR;
		}
		
	}

	@RequestMapping(value = "admin/issuer/details/saveIssuerPayment/{encIssuerId}", method = {RequestMethod.POST, RequestMethod.GET })
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String saveissuerPaymentDetails(@ModelAttribute(PlanMgmtConstants.ISSUER_PAYMENT) IssuerPaymentInformation issuerPayment, @PathVariable("encIssuerId") String encIssuerId, Model model, RedirectAttributes redirectAttributes) {
		
		Integer id = null;
		String decryptPassword = null;
		String decryptPasswordSecureKey = null;
			try{
				AccountUser currentAccountUser=userService.getLoggedInUser();
				String activeRoleName=currentAccountUser.getActiveModuleName().toUpperCase();
				model.addAttribute(ROLE_NAME, activeRoleName);
				//Issuer issuerObj = issuerService.getIssuerById(issuerId);
				String decryptedIssuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
				Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(decryptedIssuerId));
				IssuerPaymentInformation issuerPaymentInfo = null;
				String encryptedPassword = null;
				String encryptedSecurituKey = null;
				if(null!= issuerObj){
					id	= new Integer(issuerObj.getId());
					issuerPaymentInfo = serffService.getIssuerPaymentInfo(id);
				}
				if(issuerPaymentInfo == null){
					issuerPaymentInfo = new IssuerPaymentInformation();
				}
				if(null!= issuerPaymentInfo){
					//Encrypting password and security key
					if(StringUtils.isNotBlank(issuerPayment.getPassword())){
						encryptedPassword	= ghixJasyptEncrytorUtil.encryptStringByJasypt(issuerPayment.getPassword());
					}

					if(StringUtils.isNotBlank(issuerPayment.getPasswordSecuredKey())){
						encryptedSecurituKey = ghixJasyptEncrytorUtil.encryptStringByJasypt(issuerPayment.getPasswordSecuredKey());
					}
					issuerPaymentInfo.setIssuer(issuerObj);
					issuerPaymentInfo.setPassword(encryptedPassword);
					issuerPaymentInfo.setIssuerAuthURL(issuerPayment.getIssuerAuthURL());
					issuerPaymentInfo.setKeyStoreFileLocation(issuerPayment.getKeyStoreFileLocation());
					issuerPaymentInfo.setSecurityCertName(issuerPayment.getSecurityCertName());
					issuerPaymentInfo.setPasswordSecuredKey(encryptedSecurituKey);
					issuerPaymentInfo.setPrivateKeyName(issuerPayment.getPrivateKeyName());
					issuerPaymentInfo.setSecurityAddress(issuerPayment.getSecurityAddress());
					issuerPaymentInfo.setSecurityDnsName(issuerPayment.getSecurityDnsName());
					issuerPaymentInfo.setSecurityKeyInfo(issuerPayment.getSecurityKeyInfo());
				
					serffService.saveIssuerPaymentInfo(issuerPaymentInfo);
				}
				if(null!= issuerPaymentInfo ){
					if(StringUtils.isNotBlank(issuerPaymentInfo.getPassword())){
						decryptPassword	= ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInfo.getPassword());
					}

					if(StringUtils.isNotBlank(issuerPaymentInfo.getPasswordSecuredKey())){
						decryptPasswordSecureKey = ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInfo.getPasswordSecuredKey());
					}
					issuerPaymentInfo.setPassword(decryptPassword);
					issuerPaymentInfo.setPasswordSecuredKey(decryptPasswordSecureKey);
				}
				model.addAttribute(ISSUER_OBJ, issuerObj);
				model.addAttribute("issuerPaymentInfo" , issuerPaymentInfo);
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase());
				
			}catch(Exception ex){
				redirectAttributes.addAttribute("error", true);
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ex, null, Severity.HIGH);
			}
		return RETURN_ISSUER_PAYMENT_PAGE ;
	}
	
	// Quality Rating Implementation
	
	@RequestMapping(value = "/admin/qualityrating/view", method = { RequestMethod.GET })
	@PreAuthorize(PlanMgmtConstants.VIEW_QUALITY_RATING_PERMISSION)
	public String newQualityRating(@ModelAttribute(PlanMgmtConstants.ISSUERQUALITYRATING) IssuerQualityRating issuerQualityRating,Model model, HttpServletRequest request) {
		List<AdminDocument> documentList = issuerQualityRatingService.getAllAdminDocument(DOCUMENT_TYPE.QUALITY_RATING.toString());
		String commentText = null;
		AccountUser accountUser = null;
		List<HashMap<String, String>> documentFilesList = new ArrayList<HashMap<String, String>>();
		try {
			if (documentList.size() > 0) {
				for(AdminDocument adminDoc : documentList) {
					
					HashMap<String, String> documentInfoHM = new HashMap<String, String>();
					
					StringBuilder username = new StringBuilder();
					
					if (adminDoc.getCreatedBy() != null) {
						accountUser = userService.findById(adminDoc.getCreatedBy());
						if (accountUser != null && accountUser.getUsername() != null) {
							username.append(accountUser.getUsername());
						}else {
							username.append("--");
						}
					} else {
						username.append("--");
					}
					
//					String documentURL = DOCUMENT_DOWNLOAD_URL_START+appUrl+DOCUMENT_DOWNLOAD_ACC_DOCID+adminDoc.getDocumentName()+DOCUMENT_DOWNLOAD_ACC_FLNAME+adminDoc.getFileName()+DOCUMENT_DOWNLOAD_CLOSING_HREF+planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, adminDoc.getDocumentName(),adminDoc.getFileName())+DOCUMENT_DOWNLOAD_URL_END;
					String documentURL = PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_START
							+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_DOCID
							+ adminDoc.getDocumentName()
							+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_ACC_FLNAME
							+ adminDoc.getFileName()
							+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_CLOSING_HREF
							+ planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, adminDoc.getDocumentName(), adminDoc.getFileName())
							+ PlanMgmtConstants.DOCUMENT_DOWNLOAD_URL_END;
					
					LOGGER.info("documentURL: " + documentURL);
					documentInfoHM.put(UPLOADED_FILE_LINK,adminDoc.getFileName());
					documentInfoHM.put("uploadedBy",username.toString());
					documentInfoHM.put("uploadedDate",DateUtil.changeFormat(adminDoc.getCreationTimestamp().toString(), "yyyy-MM-dd", REQUIRED_DATE_FORMAT));
					documentInfoHM.put(UPLOADEDFILENAME, documentURL);
					
					if (adminDoc.getCommentID() > 0) {
						commentText = commentService.getCommentTextById(adminDoc.getCommentID());
						if (commentText != null) {
							String commentString = commentText.replace(" ", "_");
							commentString = commentString.replace("'", "\\'");
							String commentURL = "<a href='#' id='test' onclick="+"showComment('"+ commentString + "');>Comment</a>";
							LOGGER.info("commentURL: " + commentURL);
							documentInfoHM.put("uploadCommentText", commentURL);
						} else {
							documentInfoHM.put("uploadCommentText","No Comment Added");
						}
						documentInfoHM.put("uploadComment", String.valueOf(adminDoc.getCommentID()));
					}else {
						documentInfoHM.put("uploadCommentText","No Comment Added");
					}
					documentFilesList.add(documentInfoHM);
					username = null;
				}
				
//				LOGGER.info("documentFilesList: " + documentFilesList);
				model.addAttribute("documentList", documentFilesList);
				model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);

			}else{
//				LOGGER.info("documentFilesList: " + documentFilesList);
				model.addAttribute("documentList", documentFilesList);
				model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);				
			}
		} catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_QUALITY_RATING_EXCEPTION, null, 
					ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}
		return "admin/qualityrating/view";
	}
	
	@RequestMapping(value = "/admin/qualityrating/upload", method = {RequestMethod.GET})
	@PreAuthorize(PlanMgmtConstants.VIEW_QUALITY_RATING_PERMISSION)
	public String uploadQualityRating(Model model, HttpServletRequest request) {
		try {
			Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
			if (inputFlashMap != null) {
			    String invalidHiosIds = (String) inputFlashMap.get("invalidHios");
			    String invalidPlanHOISIds = (String) inputFlashMap.get("invalidPlanHOISId");
			    String dataMissMatches = (String) inputFlashMap.get("dataMissMatch");
			    String invalidDates = (String) inputFlashMap.get("invalidDate");
			    String effectiveDateValidationErrors = (String) inputFlashMap.get("effectiveDateValidationErrors");
			    String invalidGlobalRatings = (String) inputFlashMap.get("invalidGlobalRating");
			    
			    if(StringUtils.isNotBlank(invalidHiosIds)){
			    	List<String> invalidHiosErrorMsg = getErrorMessages(invalidHiosIds, "Invalid HIOS Issuer ID ");
			    	model.addAttribute("invalidHiosIds", invalidHiosErrorMsg);
			    }
			    if(StringUtils.isNotBlank(invalidPlanHOISIds)){
			    	List<String> invalidHiosErrorMsg = getErrorMessages(invalidPlanHOISIds, "Invalid Plan HIOS ID ");
			    	model.addAttribute("invalidPlanHOISIds", invalidHiosErrorMsg);
			    }
			    if(StringUtils.isNotBlank(dataMissMatches)){
			    	List<String> invalidHiosErrorMsg = getErrorMessages(dataMissMatches, "Data missmatch ");
			    	model.addAttribute("dataMissMatches", invalidHiosErrorMsg);
			    }
			    if(StringUtils.isNotBlank(invalidDates)){
			    	List<String> invalidHiosErrorMsg = getErrorMessages(invalidDates, "Invalid effective date ");
			    	model.addAttribute("invalidEffectiveDates", invalidHiosErrorMsg);
			    }
			    if(StringUtils.isNotBlank(effectiveDateValidationErrors)){
			    	List<String> invalidHiosErrorMsg = getErrorMessages(effectiveDateValidationErrors, "Effective date for quality ratings cannot be more than 12 months in the future, ");
			    	model.addAttribute("effectiveDateValidationErrors", invalidHiosErrorMsg);
			    }
			    if(StringUtils.isNotBlank(invalidGlobalRatings)){
			    	List<String> invalidHiosErrorMsg = getErrorMessages(invalidGlobalRatings, "Invalid global rating ");
			    	model.addAttribute("invalidGlobalRatings", invalidHiosErrorMsg);
			    }
			}
			
		} catch (Exception e) {
			LOGGER.error("error occured while loading upload page",e);
		}
		return "admin/qualityrating/upload"; 
	}
	
	@RequestMapping(value ="/admin/issuer/qualityratingfile/upload", method ={RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize(PlanMgmtConstants.VIEW_QUALITY_RATING_PERMISSION)
	@ResponseBody 
	public void uploadQualitRatingFile(Model model,@RequestParam(value = "qualityRatingFile", required = false) MultipartFile qualityRatingFile,HttpServletResponse response) throws IOException{

		if (null != qualityRatingFile && !qualityRatingFile.isEmpty()) {
		response.setContentType("text/html");
        response.getWriter().write(uploadFile("qualityRatingDocument",QUALITYRATIING_FOLDER,qualityRatingFile));
        response.flushBuffer();
		}
		else {
			LOGGER.error("Quality Rating File is empty.");
		}
        return;
	}

	private String uploadFile(String fileToUpload,String folderName, MultipartFile file){
		
		String returnString = null;
		String documentId=null;
		try{
			String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT
					+ FilenameUtils.getExtension(file.getOriginalFilename());
			
			try{
//				documentId = ecmService.createContent(GhixConstants.ISSUER_DOC_PATH + File.separatorChar + folderName, modifiedFileName, file.getBytes());
				String filePath = GhixConstants.ISSUER_DOC_PATH + GhixConstants.FRONT_SLASH + folderName;
				boolean isValidPath = planMgmtUtils.isValidPath(filePath);
				if(isValidPath){
					documentId = ecmService.createContent(filePath, modifiedFileName, file.getBytes()
												, ECMConstants.Issuer.DOC_CATEGORY, ECMConstants.Issuer.DOC_SUB_CATEGORY_SUPPORT, null);
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}catch (ContentManagementServiceException e) {
				LOGGER.error("Error while reading File from DMS : "+e.getMessage(),e,LogLevel.ERROR);
			}
			
			returnString = fileToUpload + "|" + file.getOriginalFilename() + "|"  + documentId;
			
			issuerService.saveFile(PlanMgmtConstants.UPLOADPATH + "/" + QUALITYRATIING_FOLDER, file, file.getOriginalFilename());
			
			LOGGER.info("Upload issuer documents DocumentId: " + documentId);
		}
		catch(Exception ex){
			LOGGER.error("Fail to upload issuer "+fileToUpload+" file. Ex: " + ex.getMessage(),LogLevel.ERROR);
		}
		return returnString;
	}
	
	@RequestMapping(value = "/admin/issuer/qualityRating/document/save", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.VIEW_QUALITY_RATING_PERMISSION)
	public String saveQualityRatings(Model model, RedirectAttributes redirectAttributes , @RequestParam(value = "qualityRatingFile", required = false) MultipartFile qualityRatingName,@RequestParam(value = "hdnQualityRating", required = false) String hdnQualityRating, @RequestParam(value = "hdnCommentText", required = false) String hdnCommentText) {
		try{
			if(StringUtils.isNotBlank(qualityRatingName.getOriginalFilename()) && StringUtils.isNotBlank(hdnQualityRating) && !NULL.equals(hdnQualityRating)) {
				String filePath = "";
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
				try {
					filePath = PlanMgmtConstants.UPLOADPATH + File.separatorChar + QUALITYRATIING_FOLDER + File.separatorChar + qualityRatingName.getOriginalFilename();
					/*if(LOGGER.isDebugEnabled()){
						LOGGER.debug("filePath: " + filePath);
					}	*/
				} catch (Exception e) {
					LOGGER.error("ERROR while fetching stored file message ex: ", e , LogLevel.ERROR);
				}

				List<List<IssuerQualityRatingPojo>> ratingModelsList = issuerQualityRatingService.processQualityRatingData(filePath, stateCode);
				
				if(ratingModelsList == null){
					redirectAttributes.addAttribute("badInput","upload process error");
					return "redirect:/admin/qualityrating/upload";
				}
				
				List<IssuerQualityRatingPojo> validHiosList = ratingModelsList.get(PlanMgmtConstants.ZERO);
				
				List<IssuerQualityRatingPojo> invalidHiosList = ratingModelsList.get(PlanMgmtConstants.ONE);
				
				if(validHiosList.size() == 0 && invalidHiosList.size() == 0){
					redirectAttributes.addAttribute("fileEmpty","file is empty");
					return "redirect:/admin/qualityrating/upload";
				}
				if(invalidHiosList.size() > 0){

					String invalidHios = getInvalidHIOSString(invalidHiosList);
					
					String invalidPlanHOISId = getInvalidPlanHOISId(invalidHiosList);
					
					String dataMissMatch = notMatchingIssuerToPlan(invalidHiosList);
					
					String invalidEffectiveDate = getInvalidEffectiveDate(invalidHiosList);
					
					String effectiveDateWithValidationErrors = getInvalidEffectiveDateForYearValidation(invalidHiosList);
					
					String invalidGlobalRating = getInvalidGlobalRating(invalidHiosList);
					
					if(StringUtils.isNotBlank(invalidHios)){
						redirectAttributes.addFlashAttribute("invalidHios", invalidHios);
					}
					if(StringUtils.isNotBlank(invalidPlanHOISId)){
						redirectAttributes.addFlashAttribute("invalidPlanHOISId", invalidPlanHOISId);
					}
					if(StringUtils.isNotBlank(dataMissMatch)){
						redirectAttributes.addFlashAttribute("dataMissMatch", dataMissMatch);
					}
					if(StringUtils.isNotBlank(invalidEffectiveDate)){
						redirectAttributes.addFlashAttribute("invalidDate", invalidEffectiveDate);
					}
					if(StringUtils.isNotBlank(effectiveDateWithValidationErrors)){
						redirectAttributes.addFlashAttribute("effectiveDateValidationErrors", effectiveDateWithValidationErrors);
					}
					if(StringUtils.isNotBlank(invalidGlobalRating)){
						redirectAttributes.addFlashAttribute("invalidGlobalRating", invalidGlobalRating);
					}
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("invalidHios:=" + invalidHiosList.toString() + "invalidDate:= " +invalidEffectiveDate + "invalidPlanHOISId :="+invalidPlanHOISId,LogLevel.DEBUG);
					}
					return "redirect:/admin/qualityrating/upload";
				}

				if(invalidHiosList.size() <= 0 ) {
					AccountUser user = null; 
					IssuerQualityRating issuerQualityRatingObj = null;
					try {
						user = userService.getLoggedInUser();
						if(null != user){
							for(IssuerQualityRatingPojo issuerQualityRatingPojo : validHiosList) {
								issuerQualityRatingObj = new IssuerQualityRating();
								issuerQualityRatingService.saveIssuerQualityRating(issuerQualityRatingPojo, user, issuerQualityRatingObj, stateCode);
								issuerQualityRatingObj = null;
							}
							
							Integer commentid = 0;
							if(StringUtils.isNotBlank(hdnCommentText)){
								if(user.getId() != 0 && StringUtils.isNotBlank(user.getUsername())){
									commentid = planMgmtService.saveComment(user.getId(), hdnCommentText,TargetName.ADMIN_HOME, user.getId(),user.getUsername());
								}else {
									commentid = planMgmtService.saveComment(user.getId(), hdnCommentText,TargetName.ADMIN_HOME, null,null);
								}
							}
							
							if (StringUtils.isNotBlank(user.getUsername())) {
								issuerQualityRatingService.saveDocument(user, commentid, qualityRatingName,hdnQualityRating);
							}else {
								user = new AccountUser();
								issuerQualityRatingService.saveDocument(user, commentid, qualityRatingName,hdnQualityRating);
							}
						}	
					} catch (InvalidUserException e) {
						LOGGER.error("Invalid User : ",e,LogLevel.ERROR);
					}
				} 
			} else {
				LOGGER.info("input data is null:" + qualityRatingName,LogLevel.INFO);
				redirectAttributes.addAttribute("invalidDocumentId", hdnQualityRating);
				return "redirect:/admin/qualityrating/upload";
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in saveQualityRatings " + ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_QUALITY_RATING_EXCEPTION, null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}
		return "redirect:/admin/qualityrating/view";
	}
	
	private List<String> getErrorMessages(String errorIds,String errorMsg) {
		List<String> errorMsgList = new ArrayList<String>();
		String[] tokans = errorIds.split(",");
		for(int i = 0; i < tokans.length; i++) {
			if(StringUtils.isNotBlank(tokans[i]) && tokans[i].split("@").length > 1){
				String[] innerTokan = tokans[i].split("@");
				errorMsgList.add(errorMsg + innerTokan[0] +" in row number "+ innerTokan[1]);
			}
		}
		return errorMsgList;
	}
	
	private String getInvalidPlanHOISId(List<IssuerQualityRatingPojo> invalidDataList) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < invalidDataList.size(); i++) {
			if(StringUtils.isNotBlank(invalidDataList.get(i).getHIOSPlanId())){
				if(i != 0){
					sb.append(",");
				}
				sb.append(invalidDataList.get(i).getHIOSPlanId());
			}
		}
		return sb.toString();
	}
	
	private String notMatchingIssuerToPlan(List<IssuerQualityRatingPojo> invalidDataList){
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < invalidDataList.size(); i++) {
			if(StringUtils.isNotBlank(invalidDataList.get(i).getNotExistIssuer())){
				if(i != 0){
					sb.append(",");
				}
				sb.append(invalidDataList.get(i).getNotExistIssuer());
			}
		}
		return sb.toString();
	}
	
	private String getInvalidEffectiveDate(List<IssuerQualityRatingPojo> invalidDataList){
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < invalidDataList.size(); i++) {
			if(StringUtils.isNotBlank(invalidDataList.get(i).getEffectiveDate())){
				if(!invalidDataList.get(i).getEffectiveDate().startsWith("FailedYearValidation_")){
					if(i != 0){
						sb.append(",");
					}
					sb.append(invalidDataList.get(i).getEffectiveDate());	
				}				
			}
		}
		return sb.toString();
	}
	
	private String getInvalidEffectiveDateForYearValidation(List<IssuerQualityRatingPojo> invalidDataList){
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < invalidDataList.size(); i++) {
			if(StringUtils.isNotBlank(invalidDataList.get(i).getEffectiveDate())){
				if(invalidDataList.get(i).getEffectiveDate().startsWith("FailedYearValidation_")){
					if(i != 0){
						sb.append(",");
					}
					sb.append(invalidDataList.get(i).getEffectiveDate().split("FailedYearValidation_")[1]);	
				}				
			}
		}
		return sb.toString();
	}
	
	private String getInvalidGlobalRating(List<IssuerQualityRatingPojo> invalidDataList){
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < invalidDataList.size(); i++) {
			if(StringUtils.isNotBlank(invalidDataList.get(i).getGlobalRatingError())){
				if(i != 0){
					sb.append(",");
				}
				sb.append(invalidDataList.get(i).getGlobalRatingError());
			}
		}
		return sb.toString();
	}

	private  Map<String, Object> getPaymentMethodsMap(String paymentMethodsString){
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
	
		PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
		return paymentMethodResponse.getPaymentMethodMap();
	}

	/**
	 * Method is used to manage Drugs data screen.
	 */
	@RequestMapping(value = "admin/manageDrugs", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	public String manageDrugs() {

		LOGGER.debug("Load Manage Drugs screen");
		return "admin/manageDrugs";
	}

	/**
	 * Method is used to get Drugs Data from Database.
	 */
	@RequestMapping(value = "admin/getDrugDataList", method = RequestMethod.GET, produces= "application/json")
	@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER_PERMISSION_STRING)
	@ResponseBody
	public DrugDataResponseDTO getDrugDataList(@RequestParam("rxcuiFilter") String rxcuiFilter, Model model, final HttpServletRequest request)
			throws GIException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Get Drugs Data from Database using Plan Display API by RXCUI Filter: " + rxcuiFilter);
		}

		StringBuffer errorMessage = new StringBuffer();
		DrugDataResponseDTO responseDTO = issuerService.getDrugDataByRxcuiSearchList(rxcuiFilter, errorMessage);

		if (0 < errorMessage.length()) {
			model.addAttribute("errorMessage", errorMessage.toString());
			request.setAttribute("errorMessage", errorMessage.toString());
			LOGGER.error("Error Message: " + errorMessage.toString());
		}

		if (null != responseDTO) {
			model.addAttribute("recordCount", responseDTO.getTotalRecordCount());
			model.addAttribute("pageSize", responseDTO.getPageSize());
		}
		return responseDTO;
	}
}
