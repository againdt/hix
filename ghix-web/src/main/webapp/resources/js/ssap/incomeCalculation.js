//Keys
function getIncomeData(name){
	
	var divName = "incomeCalculation_"+name;
	
	var strFirst = "<div class='color333' id='"+divName+"'><b>"+jQuery.i18n.prop('ssap.incomeCalculationFor')+" #NAME</b><hr/><table width='270px'><tr align='left'><th>"+jQuery.i18n.prop('ssap.incomeType')+"</th><th></th><th>"+jQuery.i18n.prop('ssap.amount')+"</th></tr>";
	
	var strFirstZero = "<div class='color333' id='"+divName+"'><b>"+jQuery.i18n.prop('ssap.incomeCalculationFor')+" #NAME</b><hr/><table>";
	
	var strTotal = " <tr><td></td><td colspan='3'><hr></td></tr><tr><th></th><th>"+jQuery.i18n.prop('ssap.total')+" (- 5%) </th><th class='alignRight'>#AMOUNT</th></tr></table></div>";
	
	var strAmt = " <tr><td class='italic'>#INCOMETYPE</td><td class='alignRight'>#MATH</td><td class='alignRight'>#AMOUNT</td></tr>";
	
	var zeroIncomeTr = " <tr><td colspan='4'><b>"+jQuery.i18n.prop('ssap.noIncome')+"</b></td></tr><td colspan='4'><hr></td></tr></table></div>";
	
	var div = "";
	
	div += strFirst.replace("#NAME", replaceall(name, '_', ' '));
	
	var zeroIncomeFlag = false;
	
	$.ajax({
		type : 'GET',
		url : 'SSAController/INCOME',
		dataType : 'json',
		data : {
			targetName : name
		},
		async : true,
		success : function(response) {
			
			for(var prop in response){
				var strToAppend = "";
				if(response[prop]['title']== 'MAGIFinalAmount'){
					strToAppend = strTotal.replace('#AMOUNT', response[prop]['amount']);
					div += strToAppend;
					
					if(parseInt(response[prop]['amount']) <= 0){
						zeroIncomeFlag = true;
						break;
					}
					
					break;
				}
				else{
					
					if(parseInt(response[prop]['amount'])<=0){
						continue;
					}
					
					strToAppend = strAmt.replace('#INCOMETYPE', response[prop]['title']);
					strToAppend = strToAppend.replace('#AMOUNT', response[prop]['amount']);
					strToAppend = strToAppend.replace('#MATH', response[prop]['type']);
					div += strToAppend;
				}
				
			}
			
			if(zeroIncomeFlag){
				div = strFirstZero.replace("#NAME", replaceall(name, '_', ' '));
				div += zeroIncomeTr;
			}
			
			var targetId = "table_incomeCalculation_"+name;
		
			setToolTipData(targetId , div);
			
			
		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {			
			alert('error');			
			$('#waiting').hide();
		}
	});
	
	
}

//Keys
function getIncomeDataAPTC(name){
	var divName = "incomeCalculationAPTC_"+name;
	
	var strFirst = "<div class='color333' id='"+divName+"'><b>"+jQuery.i18n.prop('ssap.annualIncomeCalculationFor')+" #NAME</b><hr/><table><tr><th>"+jQuery.i18n.prop('ssap.incomeType')+"</th><th></th><th>"+jQuery.i18n.prop('ssap.amount')+"</th></tr>";
	
	var strFirstZero = "<div class='color333' id='"+divName+"'><b>Annual Income Calculation for: #NAME</b><hr/><table>";
	
	var strTotal = " <tr><td></td><td colspan='3'><hr></td></tr><tr><th></th><th>Total </th><th class='alignRight'>#AMOUNT</th></tr></table></div>";
	
	var strAmt = " <tr><td class='italic'>#INCOMETYPE</td><td class='alignRight'>#MATH</td><td class='alignRight'>#AMOUNT</td></tr>";
	
	var zeroIncomeTr = " <tr><td colspan='4'><b>"+jQuery.i18n.prop('ssap.noIncome')+"</b></td></tr><td colspan='4'><hr></td></tr></table></div>";
	
	var div = "";
	
	div += strFirst.replace("#NAME", replaceall(name, '_', ' '));
	
	var zeroIncomeFlag = false;
	
	$.ajax({
		type : 'GET',
		url : 'SSAController/INCOME',
		dataType : 'json',
		data : {
			targetName : name
		},
		async : true,
		success : function(response) {
			
			for(var prop in response){
				var strToAppend = "";
				if(response[prop]['title']== 'MAGIFinalAmount'){
					strToAppend = strTotal.replace('#AMOUNT', ((parseInt(response[prop]['amount'])*100*12)/95));
					div += strToAppend;
					
					if(parseInt(response[prop]['amount']) <= 0){
						zeroIncomeFlag = true;
						break;
					}
					
					break;
				}
				else{
					
					if(parseInt(response[prop]['amount'])<=0){
						continue;
					}
					
					strToAppend = strAmt.replace('#INCOMETYPE', response[prop]['title']);
					strToAppend = strToAppend.replace('#AMOUNT', (parseInt(response[prop]['amount'])*12));
					strToAppend = strToAppend.replace('#MATH', response[prop]['type']);
					div += strToAppend;
				}
				
			}
			
			if(zeroIncomeFlag){
				div = strFirstZero.replace("#NAME", replaceall(name, '_', ' '));
				div += zeroIncomeTr;
			}
			
			var targetId = "table_incomeCalculationAPTC_"+name;
		
			setToolTipData(targetId , div);
			
			
		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {			
			alert('error');			
			$('#waiting').hide();
		}
	});
	
	
}


