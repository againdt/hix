package com.getinsured.eligibility.passive.enrollment.service;

import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Service("passiveEnrollmentNotice")
@Transactional(readOnly = true)
public class PassiveEnrollmentNoticeImpl implements PassiveEnrollmentNotice {

	private static final String PASSIVE_ENROLLMENT_NOTICE = "PassiveEnrollmentNotice";
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired private NoticeService noticeService;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;

	/*private static final Gson gson = new Gson();*/

	@Override
	public String generateNoticeInInbox(String caseNumber) throws Exception {

		SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumberId(caseNumber);



		Household household = fetchConsumer(ssapApplication);
		int moduleId = household.getId();
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = household.getFirstName() + " " + household.getLastName();
		Location location = household.getLocation();

		String relativePath = "cmr/" + moduleId + "/ssap/" + ssapApplication.getId() + "/notifications/";
		String ecmFileName = PASSIVE_ENROLLMENT_NOTICE + "_" + moduleId + (new TSDate().getTime()) + ".pdf";

		Notice notice = noticeService.createModuleNotice(PASSIVE_ENROLLMENT_NOTICE, GhixLanguage.US_EN,
				getReplaceableObjectData(ssapApplication),
				relativePath, ecmFileName,
				moduleName, moduleId, null,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
				fullName, location, GhixNoticeCommunicationMethod.Mail);

		return notice.getEcmId();
	}

	private Household fetchConsumer(SsapApplication ssapApplication) {

		int cmrId = fetchModuleId(ssapApplication);
		return cmrHouseholdRepository.findOne(cmrId);
	}

	private int fetchModuleId(SsapApplication ssapApplication) {

		int cmrId = ssapApplication.getCmrHouseoldId() != null ? ssapApplication.getCmrHouseoldId().intValue() : 0;

		if (cmrId == 0){
			throw new GIRuntimeException("Cannot generate notification! CMR Household ID not found for case number - " + ssapApplication.getCaseNumber());
		}
		return cmrId;
	}

	private Map<String, Object> getReplaceableObjectData(SsapApplication ssapApplication) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();
		tokens.put("primaryApplicantName", getName(ssapApplication));
		tokens.put(TemplateTokens.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		tokens.put("PASSIVEENROLLMENTDATE", "");
		tokens.put("PASSIVEENROLLMENTDEADLINE", "");
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put("ssapApplicationId", ssapApplication.getId());
		return tokens;
	}

	private String getName(SsapApplication ssapApplication) {
		return ssapApplication.getEsignFirstName() + " " + ssapApplication.getEsignLastName();
	}


	private  void generateActivationLink(Household household) {

		Map<String,String> consumerActivationDetails = new HashMap<String, String>();
		consumerActivationDetails.put("consumerName", household.getFirstName()+" " +household.getLastName());
		consumerActivationDetails.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		consumerActivationDetails.put("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		consumerActivationDetails.put("exchangeURL",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		consumerActivationDetails.put("emailType","consumerAccountActivationEmail");
		consumerActivationDetails.put("expirationDays",String.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.EMPLOYEE)));

		CreatedObject createdObject = new CreatedObject();
		createdObject.setObjectId(household.getId());
		createdObject.setEmailId(household.getEmail());
		createdObject.setRoleName(GhixRole.INDIVIDUAL.toString());
		createdObject.setPhoneNumbers(Arrays.asList(household.getPhoneNumber()));
		createdObject.setFullName(household.getFirstName() + " " + household.getLastName());
		createdObject.setFirstName(household.getFirstName());
		createdObject.setLastName(household.getLastName());
		createdObject.setCustomeFields(consumerActivationDetails);

		CreatorObject creatorObject = new CreatorObject();
		creatorObject.setObjectId(0);
		creatorObject.setFullName("");
		creatorObject.setRoleName(GhixRole.ADMIN.toString());

		//accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject, Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.EMPLOYEE)));


	}

}
