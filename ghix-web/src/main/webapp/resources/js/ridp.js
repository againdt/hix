var ridpApp = angular.module('ridpApp', ['ngRoute', 'spring-security-csrf-token-interceptor']);

var currentUserInfoUrl = "/hix/ridp/currentUserInfo?ts=" + (new Date()).getTime();

ridpApp.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'verifyidentity.temp.html',
			controller  : 'getStartedController',
			resolve: {
				check: function($q, $timeout, $location, apiGetFactory, dataShareService){
					 var defer = $q.defer();
					 
					 apiGetFactory.apiResponse(currentUserInfoUrl).success(function(data){
						    if(data.ridpVerified == "Y"){
						    	$location.path('verificationcomplete');
						    } else if(data.ridpSource == "MANUAL"){
						    	if(data.documentsUploaded.length == 0){
						    		$location.path('uploaddocument');
						    	}else{
						    		dataShareService.setMyData(data.documentsUploaded);
						    		for(var i=0;i<data.documentsUploaded.length;i++){
						    			if(data.documentsUploaded[i].status == "SUBMITTED"){						    				
						    				$location.path('uploaddocumentprocess');
						    			}else{
						    				$location.path('uploaddocumentreject');
						    			}
						    		}
						    	}
							} else if(data.ridpSource == "FARS"){
								FARSInfo = {
										verificationResponse:{
											"serviceContactName": data.serviceContactName,
											"serviceContactCallCenterNumber":data.serviceContactCallCenterNumber,
											"dhsReferenceNumber":data.dhsReferenceNumber
										}
								};
								
								if(data.allowManualVerification) {
									FARSInfo.verificationResponse.allowManualVerification = data.allowManualVerification
								}
								
								dataShareService.setMyData(FARSInfo);
								$location.path('identitynotverify');
							}else{
								data.personSurName = data.personSurName.replace(" ","-");
								dataShareService.setMyData(data);
								defer.resolve();
								return defer.promise;
							}	
						});
					 return defer.promise;
				}
			} 
		})
		.when('/contactinfo', {
			templateUrl : 'contactinfo.temp.html',
			controller  : 'contactController'
		})
		.when('/identityquestions', {
			templateUrl : 'identityquestions.temp.html',
			controller  : 'questionsController'
		})
		.when('/identitynotverify', {
			templateUrl : 'identitynotverify.temp.html',
			controller  : 'identitynotverifyController'
		})
		.when('/verificationcomplete', {
			templateUrl : 'verificationcomplete.temp.html',
			controller  : 'verificationcompleteController'
		})
		.when('/uploaddocument', {
			templateUrl : 'uploaddocument.temp.html',
			controller  : 'uploaddocumentController'
		})
		.when('/uploaddocumentprocess', {
			templateUrl : 'uploaddocument-process.temp.html',
			controller  : 'uploaddocumentprocessController'
		})
		.when('/uploaddocumentreject', {
			templateUrl : 'uploaddocument-reject.temp.html',
			controller  : 'uploaddocumentrejectController'
		})
		.when('/technicalerror', {
			templateUrl : 'technicalerror.temp.html',
			controller  : 'technicalerrorController'
		})
		.when('/maximumattempts', {
			templateUrl : 'maximumattempts.temp.html',
			controller  : 'maximumattemptsController'
		})
		.otherwise({
			redirectTo: '/'
		});

});

ridpApp.controller('mainController', ['$scope','$http','$location', function($scope, $http, $location) {	

	$scope.statecode = $('#stateCode').val();
}]);


ridpApp.controller('getStartedController', ['$scope','$location',function($scope, $location) {
	updateNav("ridp-nav-start");

	$scope.getStarted = function(){		
		$location.path('contactinfo');
	};
	
}]);


ridpApp.controller('contactController', ['$scope','$http','$location',  'statesFactory', 'apiGetFactory', 'apiPostFactory', 'ageValidation', 'dataShareService', function($scope, $http, $location, statesFactory, apiGetFactory, apiPostFactory, ageValidation, dataShareService) {
	
	updateNav("ridp-nav-contact");
	$('#ssn').inputmask({"mask": "999-99-9999"});
	$('#phone').inputmask({"mask": "999-999-9999"});
	$('#dob').inputmask();
		
	$scope.errorMessages = {};
	$scope.states = statesFactory;
	
	
	$scope.userInfo = dataShareService.getMyData();
	if(!$scope.userInfo){
		$location.path('/');
	}else if($scope.userInfo.verifyStatus == "completed"){
		/*HIX-39207*/
		$location.path('verificationcomplete');
	}else{
		if($scope.userInfo.fullTelephoneNumber){
			$scope.userInfo.fullTelephoneNumber = $scope.userInfo.fullTelephoneNumber.slice(0,3)+"-"+$scope.userInfo.fullTelephoneNumber.slice(3,6)+"-"+$scope.userInfo.fullTelephoneNumber.slice(6,10);
		}
		
		if($scope.userInfo.errorMsg){
			$scope.errorMessages.errorMsg = $scope.userInfo.errorMsg;
		}
		
		$scope.userInfo.address1 = $scope.userInfo.streetName;
	}
	
	$scope.doPaperVerification = function(){
		
		$('#paperVerificationModal').modal();
		/*var response = confirm("Are you sure you want to mark the applicant as verified based on the paper application?");
	    if (response == true) {
			apiPostFactory.apiResponse("/hix/ridp/csrOverrideForPaperIdentification?csrftoken=" + csrfToken).success(function(data){
				window.location.href="/hix/ssap";
			});
	    }*/
	};
	
	
	$scope.confirmPaperVerification = function(){
		apiPostFactory.apiResponse("/hix/ridp/csrOverrideForPaperIdentification").success(function(data){
			window.location.href="/hix/ssap";
		});
	};
	
	$scope.stateSelect = function stateSelect() {
		var sCode = $scope.userInfo.locationStateUSPostalServiceCode;
		var res = 0;
		$.each($scope.states, function (index, s) {
			if(s.value === sCode) {
				res = index;
			}
		});
		
		$('#state option[value='+res+']').prop("selected", true);
	}; 
	
	//contact form
	$scope.submitContactForm = function(contactForm,userInfo) {	
		$scope.loading = true;
			$scope.userInfoReq = {
					"personGivenName":userInfo.personGivenName,
					"personMiddleName":userInfo.personMiddleName,
					"personSurName":userInfo.personSurName.replace(" ","-"),
					"personBirthDate":userInfo.personBirthDate,
					"personNameSuffixText":userInfo.personNameSuffixText,
					"locationCityName":userInfo.locationCityName,
					"locationStateUSPostalServiceCode":userInfo.locationStateUSPostalServiceCode,
					"locationPostalCode":userInfo.locationPostalCode,
					"streetName":userInfo.address1
				};
			
			/*if(userInfo.address2){
				$scope.userInfoReq.streetName = userInfo.address1 + " " + userInfo.address2;
			}else{
				$scope.userInfoReq.streetName = userInfo.address1;
			}*/
			
			if(userInfo.personSSNIdentification){	
				if(userInfo.personSSNIdentification.indexOf("-") != -1){
					$scope.userInfoReq.personSSNIdentification = userInfo.personSSNIdentification;
				}else{
					$scope.userInfoReq.personSSNIdentification = userInfo.personSSNIdentification.slice(0,9);
				}									
			}
			
			if(userInfo.fullTelephoneNumber){	
				if(userInfo.fullTelephoneNumber.indexOf("-") != -1){
					var phone = userInfo.fullTelephoneNumber.split("-");
					$scope.userInfoReq.fullTelephoneNumber = phone[0]+phone[1]+phone[2];
				}else{
					$scope.userInfoReq.fullTelephoneNumber = userInfo.fullTelephoneNumber.slice(0,10);
				}
									
			}

			
			apiPostFactory.apiResponse("/hix/ridp/validateRIDPPrimary", $scope.userInfoReq)
				.success(function(data){
					$scope.userInfoRes = data;
						
					dataShareService.setMyData($scope.userInfoRes);
					
					
					//REF
					if($scope.userInfoRes.responseCode == 'HE000050'){
						if(['RF1', 'RF2'].indexOf($scope.userInfoRes.verificationResponse.finalDecision) !== -1){
							$location.path('identitynotverify');
						}else if($scope.userInfoRes.verificationResponse.finalDecision === 'RF3'){
							$location.path('technicalerror').search({param: 'RF3'}); //'Usage limit page' error
						}else{
							//RF4
							$location.path('uploaddocument');
						}
						
					}else if($scope.userInfoRes.responseCode == "HS000000"){
						$location.path('identityquestions');
					}else if(['HE200001', 'HE200002', 'HE200003', 'HE200029', 'HE200030', 'HE200031', 'HE200038', 'HE200039'].indexOf($scope.userInfoRes.responseCode) !== -1){
						$location.path('uploaddocument').search({param: $scope.userInfoRes.responseCode});
					}else if(['HE200013', 'HE200037'].indexOf($scope.userInfoRes.responseCode) !== -1){
						$scope.errorMessages.errorMsg = "SSN is typically optional but in this case please enter your SSN to continue further.";
					}else if($scope.userInfoRes.responseCode == "HE200014"){
						$scope.errorMessages.errorMsg = "We were unable to standardize your address. Please use residential address and verify the address against USPS or DMV records.";
					}else if($scope.userInfoRes.responseCode == "HE200015"){
						$scope.errorMessages.errorMsg = "Your street address exceeds maximum allowable limit of 68 characters. Please review the information and try again.";
					}else if($scope.userInfoRes.responseCode == 'HE200024'){
						$scope.errorMessages.errorMsg = 'State legislation requires match on more identification information';
					}else if($scope.userInfoRes.responseCode == 'HE200025'){
						$scope.errorMessages.errorMsg = 'Suffix is required to access consumer file, have consumer resubmit with name suffix.';
					}else if($scope.userInfoRes.responseCode == 'HE200026'){
						$scope.errorMessages.errorMsg = 'Date of birth required to access consumer, have consumer resubmit with DOB';
					}else if($scope.userInfoRes.responseCode == 'HE200027'){
						$scope.errorMessages.errorMsg = 'Middle name is required to access consumer file, have consumer resubmit with middle name.';
					}else{
						// This is a catch-all error block which redirects the user to the technical error screen
						// HX009000 =  Unexpected System Exception (Failed at Gateway)
						// HE200004 = One or more requested reports unavailable at this time. Please resubmit later
						// HE200005 = Components of checkpoint system temporarily unavailable. Please resubmit 
						// HE200008 = Not all data available for Experian Detect evaluation 
						// HE200009 = Experian Detect temporarily unavailable
						// HE200010 = Precise ID system temporarily unavailable 
						// HE200016 = Input validation error
						// HE200020 = Other Precise ID system error
						$location.path('technicalerror').search({param: $scope.userInfoRes.responseCode});
					}
					
					if($scope.errorMessages.errorMsg){
						$scope.loading = false;	
					}
					//scroll to top if error message shows 
					$("html, body").animate({ scrollTop: 0 }, "slow");
			})
			.error(function(data, status) {
				$scope.loading = false;
				$scope.errorMessages.errorMsg = status === 504 ? "Failure to connect, please try again later" : "An error has occurred. Please try again later.";
				console.error('validateRIDPPrimary error', status, data);
			});
	};
	
	/*$('.date-picker').datepicker();*/
}]);


ridpApp.controller('questionsController', ['$scope','$http','$location', 'dataShareService', 'apiPostFactory', 'apiGetFactory', function($scope, $http, $location, dataShareService, apiPostFactory, apiGetFactory) {
	updateNav("ridp-nav-question");
	$scope.answer={};
	$scope.errorMessages = {};
	
	$scope.questionsData = dataShareService.getMyData();
	
	if(!$scope.questionsData){
		$location.path('/');
	}else if($scope.questionsData.verifyStatus == "completed"){
		/*HIX-37442*/
		$location.path('verificationcomplete');
	}
	
    $scope.submitQuestionForm = function(questionForm) {
    	
    	if(questionForm.$valid){
    		$scope.loading = true;
    		
    		$scope.answers = {
	    			"sessionIdentifier"	: $scope.questionsData.verificationResponse.sessionIdentifier
	    	};
	    	
	    	$scope.verificationResponses = [];
	    	
	    	for(var i=1;i<=$scope.questionsData.verificationResponse.questions.length;i++){
	    		$scope.verificationResponses[i-1] = {
	    				"questionNumber": i,
	    				"choiceIndex": $scope.answer[i]
	    		};
	    	}
		
	    	$scope.answers.verificationResponses = $scope.verificationResponses;
	    	
	    	apiPostFactory.apiResponse("/hix/ridp/validateRIDPSecondary", $scope.answers).success(function(data){
	    		
	    		if(data.responseCode == 'HE000050'){
	    			if(['RF1', 'RF2'].indexOf(data.verificationResponse.finalDecision) !== -1){
	    				$scope.FARSInfo = {
					 			verificationResponse:data.verificationResponse
						};
						dataShareService.setMyData($scope.FARSInfo);
						$location.path('identitynotverify');
	    			}else if(data.verificationResponse.finalDecision === 'RF3'){
	    				$location.path('technicalerror').search({param: 'RF3'}); //'Usage limit page' error
	    			}else{
						//RF4
						$location.path('uploaddocument');
					}
	    			
	    		}else if(data.responseCode == "HS000000"){	  
	    			if(['RF1', 'RF2'].indexOf(data.verificationResponse.finalDecision) !== -1){
	    				$scope.FARSInfo = {
					 			verificationResponse:data.verificationResponse
						};
						dataShareService.setMyData($scope.FARSInfo);
						$location.path('identitynotverify');
	    			}else if(data.verificationResponse.finalDecision === 'RF3'){
	    				$location.path('technicalerror').search({param: 'RF3'}); //'Usage limit page' error
	    			}else if(data.verificationResponse.finalDecision === 'RF4'){
	    				$location.path('uploaddocument');
	    			}else{
	    				dataShareService.setMyData({"verifyStatus":"completed"});
	    				$location.path('verificationcomplete');
	    			}
	    		}else if(data.responseCode == "HE000050"){
	    			$location.path('uploaddocument');
	    		}else if(['HE200016', 'HE200040'].indexOf(data.responseCode) !== -1){
	    			apiGetFactory.apiResponse(currentUserInfoUrl).success(function(data){
	    				data.errorMsg = "You response for one of more questions was incorrect. Please review the contact information and try again.";
						dataShareService.setMyData(data);
						$location.path('contactinfo');
					});
	    		}else if(['HE200018', 'HE200041'].indexOf(data.responseCode) !== -1){
	    			apiGetFactory.apiResponse(currentUserInfoUrl).success(function(data){
	    				data.errorMsg = "Your session timed out. Please review the contact information and try again.";
						dataShareService.setMyData(data);
						$location.path('contactinfo');
					});
	    		}else{
	    			$scope.errorMessages.errorMsg = "We encountered technical error. Please review the information and try again."; 
	    		}
	    		
	    		if($scope.errorMessages.errorMsg){
					$scope.loading = false;	
				}
	    		$("html, body").animate({ scrollTop: 0 }, "slow");
	    	});
    	}else{
    		$scope.errorMessages.errorMsg = "Please answer all the questions to proceed further.";
    		//scroll to top if error message shows 
			$("html, body").animate({ scrollTop: 0 }, "slow");
    	}
    	
    };
	
	
}]);


ridpApp.controller('identitynotverifyController', ['$scope','$http','$location', 'dataShareService', 'apiPostFactory', 'apiGetFactory', function($scope, $http, $location, dataShareService, apiPostFactory, apiGetFactory) {
	updateNav("ridp-nav-experian");
	$scope.errorMessages = {};
	$scope.FARSRes = dataShareService.getMyData();
	
	
	if(!$scope.FARSRes){
		//go back to start page if refresh
		 $location.path('/');
	}else{
		$scope.codeNum = {
				dshReferenceNumber:$scope.FARSRes.verificationResponse.dhsReferenceNumber
		};
		
		$scope.identityVerify = function (){
			$scope.loading = true;
			apiPostFactory.apiResponse("/hix/ridp/validateFARS", $scope.codeNum).success(function(data){
				$scope.verifyResponse = data;
				
				
				if(['RF1', 'RF2'].indexOf($scope.verifyResponse.FinalDecisionCode) !== -1){
					$scope.errorMessages.HE200045 = true;
    			}else if($scope.verifyResponse.FinalDecisionCode === 'RF3'){
    				$location.path('technicalerror').search({param: 'RF3'}); //'Usage limit page' error
    			}else if($scope.verifyResponse.FinalDecisionCode === 'RF4'){
					//RF4
					$location.path('uploaddocument');
				}else if($scope.verifyResponse.ResponseMetadata){
					if($scope.verifyResponse.ResponseMetadata.ResponseCode == "HS000000"){
			   			 $location.path('verificationcomplete');
			   		}else if($scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200006"){
			   			$scope.errorMessages.HE200006 = true;
			   		}else if($scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200045"){
			   			$scope.errorMessages.HE200045 = true;
			   		}else if($scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200004" || $scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200005" || $scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200008" || $scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200009" || $scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200010" || $scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200016" || $scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200018" || $scope.verifyResponse.ResponseMetadata.ResponseCode == "HE200020"){
			   			$scope.errorMessages.errorMsg = "We encountered technical error. Please review the information and try again.";
			   		}else{
			   			$scope.errorMessages.errorMsg = "We encountered technical error. Please review the information and try again."; 
			   		}
				}else{
					$location.path('technicalerror');
		   		 }
				
				if($scope.errorMessages.errorMsg || $scope.errorMessages.HE200006 || $scope.errorMessages.HE200045){
					$scope.loading = false;	
				}
			});
		};
		
		$scope.clearDHSReferenceNumber = function(){
			$scope.loading = true;
			apiPostFactory.apiResponse("/hix/ridp/clearDHSReferenceNumber").success(function(data){
				if(data.responseCode == "HS000000"){
					apiGetFactory.apiResponse(currentUserInfoUrl).success(function(data){
						dataShareService.setMyData(data);
						$location.path('contactinfo');
					});
				}else{
		   			 $scope.errorMessages.errorMsg = "We encountered technical error. Please review the information and try again."; 
		   		 }
				
				if($scope.errorMessages.errorMsg){
					$scope.loading = false;	
				}
			});
		};
		
		$scope.gotoManualVerification = function() {
			apiPostFactory.apiResponse("/hix/ridp/pushToManualVerification").success(function(data) {
				$location.path('uploaddocument');
			});
		};
	}
}]);


ridpApp.controller('verificationcompleteController', ['$scope','$http','$location', function($scope, $http, $location) {	
	updateNav("ridp-nav-finish");
}]);

ridpApp.controller('uploaddocumentController', ['$scope','$http','$location', 'documentsFactory', 'fileUpload', 'apiGetFactory', 'dataShareService', function($scope, $http, $location, documentsFactory, fileUpload, apiGetFactory, dataShareService) {	
	updateNav("ridp-nav-manual");
	
	$("#manualVerificationFile").change(function () {
		var fileNameArray = $(this).val().split('\\');		
	    $("#uploadFile").val(fileNameArray[fileNameArray.length - 1]);
	});
	
	$scope.documents = documentsFactory;
	$scope.document = {};
	$scope.errorMessages = {};
	
	var param = $location.search().param;
	
//	if(param === 'HE200038'){
//		$scope.errorMessages.errorMsg = 'Unable to standardize current address, have consumer verify address with official documentation.';
//	}else if(param === 'HE200039'){
//		$scope.errorMessages.errorMsg = 'Current address exceeds maximun length, have consumer verify address with official documentation';
//	}
	
	$scope.upload = function(){	
		$scope.loading = true;
		
		//for upload file
		var file = $scope.verificationFile;
		var fileName = "manualVerificationFile"; 
		
		//for text input
        var fields = [{"name": "doctype", "data": $scope.document.doctype}];
        
        var uploadUrl = "manualVerificationFileUpload";
        fileUpload.uploadFileAndFieldsToUrl(fileName, file, fields, uploadUrl).success(function(data){
        	if(data.responseCode == "HS000000"){
        			dataShareService.setMyData(data.documentsUploaded);
        			$location.path('uploaddocumentprocess');
        	}else{
        		 $scope.errorMessages.errorMsg = "Your file could not be uploaded. Use one of these file types: BMP, GIF, JPG, JPEG, PNG, or PDF."; 
        		//scroll to top if error message shows 
     			$("html, body").animate({ scrollTop: 0 }, "slow");
            	$scope.loading = false;
        	}
        });
	};
}]);


ridpApp.controller('uploaddocumentprocessController', ['$scope','$http','$location', 'dataShareService', function($scope, $http, $location, dataShareService) {	
	updateNav("ridp-nav-manual");
	$scope.documentsUploaded = dataShareService.getMyData();
	/*if(!$scope.documentsUploaded){
		//go back to start page if refresh
		 $location.path('/');
	}*/
	
}]);

ridpApp.controller('uploaddocumentrejectController', ['$scope','$http','$location', 'documentsFactory', 'fileUpload', 'apiGetFactory', 'dataShareService', function($scope, $http, $location, documentsFactory, fileUpload, apiGetFactory, dataShareService) {
	updateNav("ridp-nav-manual");
	
	$("#manualVerificationFile").change(function () {
		var fileNameArray = $(this).val().split('\\');		
	    $("#uploadFile").val(fileNameArray[fileNameArray.length - 1]);
	});
	
	$scope.documents = documentsFactory;
	$scope.document = {};
	$scope.errorMessages = {};
	
	$scope.documentsUploaded = dataShareService.getMyData();
	/*if(!$scope.documentsUploaded){
		//go back to start page if refresh
		 $location.path('/');
	}*/
	$scope.upload = function(){	
		$scope.loading = true;
		
		//for upload file
		var file = $scope.verificationFile;
		var fileName = "manualVerificationFile"; 
		
		//for text input
        var fields = [{"name": "doctype", "data": $scope.document.doctype}];
        
        var uploadUrl = "manualVerificationFileUpload";
        fileUpload.uploadFileAndFieldsToUrl(fileName, file, fields, uploadUrl).success(function(data){
        	if(data.responseCode == "HS000000"){
        			dataShareService.setMyData(data.documentsUploaded);
        			$location.path('uploaddocumentprocess');
        	}else{
				$scope.errorMessages.errorMsg = "Your file could not be uploaded. Use one of these file types: BMP, GIF, JPG, JPEG, PNG, or PDF.";
        		//scroll to top if error message shows 
     			$("html, body").animate({ scrollTop: 0 }, "slow");
            	$scope.loading = false;
        	}
        });
	};
	
}]);

ridpApp.controller('technicalerrorController', ['$scope','$http','$location', 'apiPostFactory', function($scope, $http, $location, apiPostFactory) {	
	updateNav("ridp-nav-finish");
	
	var param = $location.search().param;
	
	if(param === 'RF3'){
		$scope.errorMessage = 'Your attempts to identify proof electronically has exceeded the maximum limit. You must now wait for 16 hours before attempting again.';
	}else{
		$scope.errorMessage = 'Unexpected Technical Error Occurred';
	}
	
	$scope.gotoManualVerification = function() {
		apiPostFactory.apiResponse("/hix/ridp/pushToManualVerification").success(function(data) {
			$location.path('uploaddocument');
		});
	};
}]);

ridpApp.controller('maximumattemptsController', ['$scope','$http','$location',function($scope, $http, $location) {	
	updateNav("ridp-nav-finish");
}]);





ridpApp.factory("apiGetFactory",['$http',function($http){
	
	var apiRequest = function(url) {
		return $http({
			method: "GET",
			url: url
		});
	};
	
	return {
		apiResponse: function(url) {
			return apiRequest(url);
		}
	};
}]);


ridpApp.factory("apiPostFactory",['$http',function($http){
	
	var apiRequest = function(url, data) {
		return $http({
			method: "POST",
			url: url,
			data: data,
			timeout: 1 * 30 * 1000
		});
	};
	
	return {
		apiResponse: function(url, data) {
			return apiRequest(url, data);
		}
	};
}]);



ridpApp.factory('dataShareService', ['$http',function($http) {
	var myData;
	return {
		getMyData: function() {
			return myData;
		},
		setMyData: function(data) {
			myData = data;
		}
	};
	
}]);




ridpApp.factory("ageValidation", function() {

	return {
        validate: function(dateOfBirth) {
        	//check whether it's  mm/dd/yyyy format
        	/*var DOBRegex=/^\d{2}\/\d{2}\/\d{4}$/;
        	var isValidDOB = DOBRegex.test(dateOfBirth);
        	if(dateOfBirth && !isValidDOB){
        		return { valid: false, invalidMsg: "Please use the MM/DD/YYYY format."};
        	};*/
        	
        	var today = new Date();
			today.setHours(0);
			today.setMinutes(0);
			today.setSeconds(0);
			today.setMilliseconds(0);
			var effective = new Date();

			//Current Date is on or before 15 of a month
			if(today.getDate() <= 15){
				effective.setMonth(today.getMonth() + 1);
			}
			//Current date is after 15th of the month
			else{
				effective.setMonth(today.getMonth() + 2);
			}
			effective.setDate(1);
			effective.setHours(0);
			effective.setMinutes(0);
			effective.setSeconds(0);
			effective.setMilliseconds(0);

			dob_arr = dateOfBirth.split('/');
			var dob_mm = dob_arr[0];
			var dob_dd = dob_arr[1];
			var dob_yy = dob_arr[2];
			var birthDate = new Date();
			birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

			var age = effective.getFullYear() - birthDate.getFullYear();
			var m = effective.getMonth() - birthDate.getMonth();
			if (m < 0 || (m === 0 && effective.getDate() < birthDate.getDate())){
				age--;
			}
			
			/*if(age < 18 || age > 120 || !age) {
				if(age < 18) {
					return { valid: false, invalidMsg: "You must at 18 or older."};
				}
				else if(age > 120) {
					return { valid: false, invalidMsg: "Please enter an age below 120."};
				}
			}*/
			
			//no future date, no >120 
			if(birthDate.getTime() > today.getTime() || age > 120  ){
				return { valid: false };
			}else{
				return { valid: true };
			}

			
				
	
        }
	};
});




ridpApp.directive("requiredErrDirective",function(){
	return {
		scope: {
			  errMsg: '@',
	          labelFor: '@'
	        },
		template: '<label for="{{labelFor}}" class="error"><span> <em class="excl">!</em>{{errMsg}}</span></label>'
	};
});

ridpApp.directive("patternErrDirective",function(){
	return {
		scope: {
	          errMsg: '@',
	          labelFor: '@'
	        },
		template: '<label for="{{labelFor}}" class="error"><span> <em class="excl">!</em>{{errMsg}}</span></label>'
	};
});

ridpApp.directive("dobErrDirective",function(ageValidation){
	return{		
		scope: {
			  errMsg: '@',	
	          labelFor: '@'
	        },
		template: '<label for="{{labelFor}}" class="error"><span> <em class="excl">!</em>{{errMsg}}</span></label>'
	};
});


//custom error directive
ridpApp.directive("dobErr",function(ageValidation){
	return{
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel){
			ngModel.$parsers.unshift(function (value) {
              ngModel.$setValidity('dobErr', ageValidation.validate(value).valid);
              return value;
            });
		}
	};
});


function updateNav(activeNav){
	$("#ridp-nav li").removeClass("active");
	$("#"+activeNav).show().addClass("active");
}


//ng-focus validation
ridpApp.directive('ngFocus', [function() {
	var FOCUS_CLASS = "ng-focused";
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$focused = false;
			element.bind('focus', function(evt) {
				element.addClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = true;
				});
			}).bind('blur', function(evt) {
				element.removeClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = false;
				});
			});
		}
	};
}]);


ridpApp.directive('displayInvalid', ['$parse', '$filter', function($parse, $filter) {   
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elm, attrs, model) {
        var displayed = false;
        scope.$watch(attrs.ngModel, function(newValue, oldValue, scope) {
          // only set once... on initial load
          if(displayed == false && oldValue != undefined){
            displayed = true;
            elm.val(model.$modelValue);
          }
        });
      }
    };
  }]);


ridpApp.directive('fileDirective', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileDirective);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


ridpApp.directive('validFile',function(){
  return {
    require:'ngModel',
    link:function(scope,el,attrs,ngModel){
      //change event is fired when file is selected
      el.bind('change',function(){
        scope.$apply(function(){
          ngModel.$setViewValue(el.val());
          ngModel.$render();
        });
      });
    }
  };
});




ridpApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileAndFieldsToUrl = function(fileName, file, fields, uploadUrl){
        var fd = new FormData();
        fd.append("csrftoken", $('#tokid').val());
        fd.append(fileName, file);
        if(fields != undefined){
        	for(var i = 0; i < fields.length; i++){
                fd.append(fields[i].name, fields[i].data);
            };
        }
        
       return  $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
        
    };
}]);