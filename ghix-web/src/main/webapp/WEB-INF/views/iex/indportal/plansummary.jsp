<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>
  $(document).ready(function() {
    var scope = angular.element(document.getElementById("plansummary")).scope();
    if (scope) {
      scope.$apply(function() {
        scope['plansummary'] = {title: '<spring:message code="indportal.plansummary.myplandetails" javaScriptEscape="true"/>'};
      });
    }
  });
</script>
<script type="text/ng-template" id="plansummary">


<div class="header row-fluid ng-scope">
  <h4><spring:message code="indportal.plansummary.myplandetails" javaScriptEscape="true"/></h4>
</div>
<div class="" ng-init="initplansummary()">
<div spinning-loader="loader"></div>
  <div class="margin20-lr accordion-wrap">

        <!-- MEDICAL PLAN -->
    <div ng-if="showhealthplan==true">
      <div class="ps-detail__header ps-detail__header--group" style="background-color: rgb(240, 240, 240); border-bottom: none;">
        <span><b><spring:message code='indportal.plansummary.medicalhealth' javaScriptEscape='true'/></b></span>
        <i id="plan_details_medical_toggle_icon" class="toggle icon-chevron-down" ng-keyup="checkPlanDetailsSlideDivEvents($event, 'medical')" ng-click="slidePlanDetailsDiv('medical')" tabindex="0"></i>
      </div>
      <div id="plan_details_medical_plan" class="toggle-div">
        <div  class="well-step row-fluid enrollment collapse in plansummary-content" id="myHealthPaln" ng-if="planDetails.activeHealthPlanDetails.length > 0">
			<div class="activeEnrollment" ng-repeat="activePlanDetails in planDetails.activeHealthPlanDetails" >
				<div ng-include="'planSummaryTemplate'"></div></br>
			</div>
		</div>
      </div>
    </div>

        <!-- DENTAL PLAN -->
    <div ng-if="showdentalplan==true">
      <div class="ps-detail__header ps-detail__header--group" style="background-color: rgb(240, 240, 240); border-bottom: none;">
        <span><b><spring:message code='indportal.plansummary.dentalhealth' javaScriptEscape='true'/></b></span>
        <i id="plan_details_dental_toggle_icon" class="toggle icon-chevron-down" ng-keyup="checkPlanDetailsSlideDivEvents($event, 'dental')" ng-click="slidePlanDetailsDiv('dental')" tabindex="0"></i>
      </div>
      <div id="plan_details_dental_plan" class="toggle-div">
		<div class="well-step row-fluid enrollment collapse in plansummary-content" id="myHealthPaln" ng-if="planDetails.activeDentalPlanDetails.length > 0">
			<div class="pastEnrollment" ng-repeat="activePlanDetails in planDetails.activeDentalPlanDetails" >
            <div ng-include="'planSummaryTemplate'"></div></br>
          </div>
        </div>
			</div>
		</div>
        
   </div>
    <div class="pull-left">
      <a href="javascript:void(0)" id="goToDashboard" ng-click="goDash()" class="btn btn-primary"><spring:message code="indportal.eligibilitysummary.gobacktobashboard" javaScriptEscape="true"/></a>
      <a id="disenrollBoth" href="javascript:void(0)" ng-if="isAllPlansDisEnroll" ng-click="disenrollAlert('planSummary','both', activePlanDetails.enrollmentId, true)" class="btn btn-primary">
        <spring:message code="indportal.plansummary.disenrollfromhealthanddental" javaScriptEscape="true"/>
      </a>
      <a id="disenrollBoth" href="javascript:void(0)" ng-if="!isAllPlansDisEnroll" ng-click="disenrollAlert('planSummary','both', activePlanDetails.enrollmentId, false)" class="btn btn-primary">
        <spring:message code="indportal.plansummary.cancelfromhealthanddental" javaScriptEscape="true"/>
      </a>
    </div>
  </div>


<!-- Coming Start Date Modal-->
<div ng-show="comingStartDayDialog">
 <div modal-show="comingStartDayDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="comingStartDateModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="comingStartDateModal"><spring:message code="indportal.plansummary.comingstartdate" javaScriptEscape="true"/></h3>
        </div>
        <div class="modal-body gutter20">
          <p><spring:message code="indportal.plansummary.comingstartdatecontent" javaScriptEscape="true"/></p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="continueDisenrollment()" class=""><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
</div>
<!-- Coming Start Date Modal-->

<!-- Disenroll Modal-->
<div ng-show="disenrollDialog">
 <div modal-show="disenrollDialog" id="disenrollDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelDisenroll()">&times;</button>
          <h3 id="disenrollModal">
            <span ng-if="enrollmentAction"><spring:message code="indportal.plansummary.voluntarydisenrollmentreasons" javaScriptEscape="true"/></span>
            <span ng-if="!enrollmentAction"><spring:message code="indportal.plansummary.cancelcoverage" javaScriptEscape="true"/></span>
          </h3>
        </div>
        <div class="modal-body ">
          <form class="form-horizontal">
            <h5 ng-if="enrollmentAction" class="margin10-l" id="subtitle"><strong><spring:message code="indportal.plansummary.whyareyoudisenrolling" javaScriptEscape="true"/></strong></h5>
            <h5 ng-if="!enrollmentAction" class="margin10-l" id="subtitle"><strong><spring:message code="indportal.plansummary.cancel.whyareyoudisenrolling" javaScriptEscape="true"/></strong></h5>
  
            <div class="margin30-l">

              <label class="radio">
                  <input type="radio" name="disenrollmentreason" id="disaffordability" value="CANNOT_AFFORD_PAYMENT" checked="" required="" class="" ng-model="checked">
                  <spring:message code="indportal.plansummary.icantafford" javaScriptEscape="true"/>
              </label>
              <br>
			  <label class="radio" >
                  <input type="radio" name="disenrollmentreason" id="disnothappywithplan" value="NO_PROPER_SERVICE" required="" class="" ng-model="checked">
                  <spring:message code="indportal.plansummary.imnothappywith" javaScriptEscape="true"/>
                  <span ng-if="stateCode !== 'CA' && stateCode !== 'MN'">
										<span ng-if="typeOfPlan == 'Dental'"><spring:message code="indportal.plansummary.dental" javaScriptEscape="true"/></span>
										<span ng-if="typeOfPlan == 'Health'"><spring:message code="indportal.plansummary.health" javaScriptEscape="true"/></span>
										<span ng-if="typeOfPlan == 'both'"><spring:message code="indportal.plansummary.health" javaScriptEscape="true"/> and <spring:message code="indportal.plansummary.dental" javaScriptEscape="true"/></span>
										<spring:message code="indportal.plansummary.plan" javaScriptEscape="true"/>
								  </span>
              </label>
			  <br>
			  <label class="radio">
                  <input type="radio" name="disenrollmentreason" id="outOfnetwork" value="PHY_OUT_OF_NETWORK" required="" class="" ng-model="checked">
                  <spring:message code="indportal.plansummary.phyOutOfNetwork" javaScriptEscape="true"/>
              </label>
			  <br>
              <label class="radio">
                  <input type="radio" name="disenrollmentreason" id="agentError" value="AGENT_ERROR" class="" ng-model="checked">
                  <spring:message code="indportal.plansummary.agentError"/>
              </label>
			  <br>
              <label class="radio">
                  <input type="radio" name="disenrollmentreason" id="other" value="OTHER" class="" ng-model="checked">
                  <spring:message code="indportal.plansummary.other"/>
              </label>

            </div>
           <div id="otherDisenrollReason" ng-if="checked=='OTHER'">
              <div class="margin30-l">
                <input type="text" name="tellusmore" id="tellusmore" class="span11" ng-model="$parent.otherReason"/>
              </div>
           </div>
          </form>
			  <div ng-if="stateCode !== 'MN'" class="margin5-l margin5-r margin20-t alert alert-info margin0-b">
              <p><spring:message code="indportal.plansummary.otherReasonText"/></p>
              </div>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal" ng-click="cancelDisenroll()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
          <a href="javascript:void(0);" class="btn btn-primary"  ng-if="!checked" ng-click="disenrollmentReason()" class=""><spring:message code="indportal.customGrouping.skipAndContinue" javaScriptEscape="true"/></a>
          <a href="javascript:void(0);" class="btn btn-primary"  ng-if="checked" ng-click="disenrollmentReason()" class=""><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Disenroll Modal-->

<!-- HIX-59752 Prevent Disenroll Action on an enrolled application if there is any another application in Eligibility Received status-->
<!-- pendingAppDialog Modal-->
<div ng-show="pendingAppDialog">
 <div modal-show="pendingAppDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="pendingAppModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="pendingAppModal"><spring:message code="indportal.pendingAppModal.header" javaScriptEscape="true"/></h3>
        </div>
        <div class="modal-body">
			<spring:message code="indportal.pendingAppModal.body"/>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- pendingAppDialog Modal-->

<!-- Are You Sure Modal-->
<div ng-show="areYourSureDialog">
 <div modal-show="areYourSureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollCheckModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        
          <div id="disenrollCheckModal" class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <span ng-if="typeOfPlan == 'Dental' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinue.dental" javaScriptEscape="true"/></h3></span>
            <span ng-if="typeOfPlan == 'Health' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinue" javaScriptEscape="true"/></h3></span>
            <span ng-if="stateCode !== 'MN' && typeOfPlan == 'both' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinueboth" javaScriptEscape="true"/></h3></span>
            <span ng-if="stateCode === 'MN' && typeOfPlan == 'both' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinue" javaScriptEscape="true"/></h3></span>
            <span ng-if="typeOfPlan == 'Dental' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.cancel.doyouwanttocontinue.dental" javaScriptEscape="true"/></h3></span>
            <span ng-if="typeOfPlan == 'Health' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.cancel.doyouwanttocontinue.health" javaScriptEscape="true"/></h3></span>
            <span ng-if="stateCode !== 'MN' && typeOfPlan == 'both' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinueboth" javaScriptEscape="true"/></h3></span>
            <span ng-if="stateCode === 'MN' && typeOfPlan == 'both' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.cancel.doyouwanttocontinue.dental" javaScriptEscape="true"/></h3></span>
          </div>

          <div class="modal-body gutter20">
              <span ng-if="typeOfPlan == 'Health'">
                    <p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll" /></p>
                  </span>
                  <span ng-if="stateCode !== 'MN' && typeOfPlan == 'Dental'">
                    <p><spring:message code="indportal.plansummary.youhaveseelctedtodisenrolldental" /></p>
               <p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2dental" /></p>
                  </span>
                  <span ng-if="stateCode === 'MN' && typeOfPlan == 'Dental'">
                    <p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll" javaScriptEscape="true"/></p>
                    <p><spring:message code="indportal.plansummary.ifyoucanaffordbut" javaScriptEscape="true"/></p>
                  </span>
                  <span ng-if="stateCode === 'MN' && typeOfPlan == 'both'">
                      <p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll" javaScriptEscape="true"/></p>
                    </span>
              <span ng-if="stateCode !== 'MN' && typeOfPlan == 'both'">
                    <p><spring:message code="indportal.plansummary.youhaveseelctedtodisenrollboth" /></p>
               <p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2both" /> <a href="https://www.coveredca.com/individuals-and-families/getting-covered/special-enrollment/qualifying-life-events/" target="_blank"><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2_1" /></a></p>
                <p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll3both" /></p>
                  </span>
              <span ng-if="typeOfPlan == 'Health' && dentaldetails && dentaldetails.disEnroll && dentaldetails.electedAptc > 0">
              <p><spring:message code="indportal.plansummary.aptcwarning" /></p>
              </span>
                  <div class="margin20-t alert alert-info margin0-b" ng-if="typeOfPlan == 'Health' || typeOfPlan == 'both'">
                    <spring:message code="indportal.plansummary.ifyoucanaffordbut" />
                    <br><br>
                    <span ng-if="stateCode !== 'MN'">
              <spring:message code="indportal.plansummary.thefeecontent4" /> <a href="<spring:message code='indportal.plansummary.yhiExemptionsUrl'/>" target="_blank"> <spring:message code="indportal.plansummary.yhiExemptionsUrl"/></a>
                    </span>
                  </div>
                </div>
                <div ng-if="stateCode !== 'MN'" class="modal-footer">
                  <a href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.no" /></a>
                  <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goToDisenrollDateModal()" class=""><spring:message code="indportal.portalhome.yes" /></a>
                </div>
                <div ng-if="stateCode === 'MN'" class="modal-footer">
                  <a href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
                  <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goToDisenrollDateModal()" class=""><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
                </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Are You Sure Modal-->

<!-- Choosing Disenrollment Date Modal-->
<div ng-show="disenrollDateDialog.step">
 <div modal-show="disenrollDateDialog.step" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollDateDialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div id="disenrollDateDialog" class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="disenrollDateModal" ng-if="disenrollDateDialog.step == 1"><spring:message code="indportal.plansummary.selectTerminationDate"/></h3>
		  <h3 id="ConfirmDisenrollDateModal" ng-if="disenrollDateDialog.step == 2"><spring:message code="indportal.plansummary.confirmEndDate"/></h3>
		  <h3 id="ConfirmSubmitDisenrollDateModal" ng-if="disenrollDateDialog.step == 3"><spring:message code="indportal.plansummary.confirmation"/></h3>
        </div>
        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 1">
		  <form class="form-horizontal">
            <div class="margin30-l">
              <label class="radio margin5-b">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="CURRENT_MONTH" ng-click="saveSelectedDate('CURRENT_MONTH')">
                  <spring:message code="indportal.plansummary.disenrollmentDates.lastDayCurrentMonth"/>
              </label>
              <label class="radio margin5-b">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="NEXT_MONTH" ng-click="saveSelectedDate('NEXT_MONTH')">
                  <spring:message code="indportal.plansummary.disenrollmentDates.lastDayNextMonth"/>
              </label>
              <label class="radio margin5-b">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="MONTH_AFTER_NEXT_MONTH" ng-click="saveSelectedDate('MONTH_AFTER_NEXT_MONTH')">
                  <spring:message code="indportal.plansummary.disenrollmentDates.lastDayAfterMonth"/>
              </label>
            </div>

		  </form>
		  <div class="margin20-t alert alert-info margin0-b"><spring:message code="indportal.plansummary.needSpecificTermDate"/></div>
        </div>

        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 2">
            <p>
              <span ng-if="typeOfPlan === 'Health'"><spring:message code="indportal.plansummary.chosenEndDate.health"/></span>
              <span ng-if="typeOfPlan === 'Dental'"><spring:message code="indportal.plansummary.chosenEndDate.dental"/></span>
              <span ng-if="typeOfPlan === 'both'"><spring:message code="indportal.plansummary.chosenEndDate.both"/></span>
            </p>

		  <div class="margin20-t margin0-b disenroll-confirm-box">
			<!--<p><spring:message code="indportal.plansummary.from"/></p>-->
			<div ng-if="typeOfPlan == 'Health'"><img class="pull-left" ng-src="{{healthdetails.planLogoUrl}}" alt="{{healthdetails.issuerName}} logo"></div>
			<div ng-if="typeOfPlan == 'Dental'"><img class="pull-left" ng-src="{{dentaldetails.planLogoUrl}}" alt="{{dentaldetails.issuerName}} logo"></div>
			<div ng-if="typeOfPlan == 'both'"><img class="pull-left" ng-src="{{healthdetails.planLogoUrl}}" alt="{{healthdetails.issuerName}} logo">
			<img class="pull-right" ng-src="{{dentaldetails.planLogoUrl}}" alt="{{dentaldetails.issuerName}} logo"></div>
          </div>
        </div>
        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 3">
		  <div class="margin20-t alert alert-info margin0-b disenroll-confirm-box">
        <p ng-if="typeOfPlan === 'Health'">
          <spring:message code="indportal.plansummary.disenrollConfirmContent.health"/>
        </p>
        <p ng-if="typeOfPlan === 'Dental'">
          <spring:message code="indportal.plansummary.disenrollConfirmContent.dental"/>
        </p>
        <p ng-if="typeOfPlan === 'both'">
          <spring:message code="indportal.plansummary.disenrollConfirmContent.both"/>
        </p>
          </div>
		  <div class="margin20-t alert alert-info margin0-b">
			<spring:message code="indportal.plansummary.note"/>:
			<ul>
        <li><spring:message code="indportal.plansummary.disenrollConfirmPt1"/></li>
        <li ng-if="stateCode === 'MN'"><spring:message code="indportal.plansummary.disenrollConfirmPt2"/></li>
			</ul>
		  </div>
        </div>

        <div class="modal-footer" ng-if="disenrollDateDialog.step == 1">
          <button class="btn btn-primary" ng-click="disenrollDateDialog.step = 2" ng-if="stateCode !== 'MN'" ng-disabled="!disenrollDateDialog.date"><spring:message code="indportal.portalhome.ok"/></button>
          <a ng-if="stateCode === 'MN'" class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
          <button class="btn btn-primary" ng-click="disenrollDateDialog.step = 2" ng-if="stateCode === 'MN'" ng-disabled="!disenrollDateDialog.date"><spring:message code="indportal.portalhome.continue"/></button>
        </div>
        <div class="modal-footer" ng-if="disenrollDateDialog.step == 2">
          <button class="btn btn-secondary" ng-click="disenrollDateDialog.step = 1"><spring:message code="indportal.plansummary.updateTermDate"/></button>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-if="stateCode !== 'MN'" ng-click="disenrollDateDialog.step = 3"><spring:message code="indportal.portalhome.ok"/></a>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-if="stateCode === 'MN'" ng-click="disenrollDateDialog.step = 3"><spring:message code="indportal.portalhome.continue"/></a>
        </div>
        <div class="modal-footer" ng-if="disenrollDateDialog.step == 3">
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-if="stateCode !== 'MN'" ng-click="submitDisenrollment()"><spring:message code="indportal.portalhome.ok"/></a>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-if="stateCode === 'MN'" ng-click="submitDisenrollment()"><spring:message code="indportal.portalhome.submit"/></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Choosing Disenrollment Date Modal-->

<!-- SubmitDisenroll-->
<div ng-show="submitDisenrollmentDialog">
 <div modal-show="submitDisenrollmentDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="submitDisenrollModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="submitDisenrollModal">
            <span ng-if="enrollmentAction"> <spring:message code="indportal.plansummary.voluntarydisenrollmentrequestsubmitted" javaScriptEscape="true"/></span>
            <span ng-if="!enrollmentAction"> <spring:message code="indportal.plansummary.cancelcoveragerequest" javaScriptEscape="true"/></span>
          </h3>
        </div>
        <div class="modal-body gutter20">
            <p>
              <span ng-if="stateCode !== 'MN' && enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewed" javaScriptEscape="true"/></span>
              <span ng-if="stateCode === 'MN' && enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewedcancelcoverage" javaScriptEscape="true"/></span>
              <span ng-if="!enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewedcancelcoverage" javaScriptEscape="true"/></span>
            </p>
            <!--<p><spring:message code="indportal.plansummary.youwillbecovered" javaScriptEscape="true"/></p>-->
            <p ng-if="stateCode !== 'MN'"><spring:message code="indportal.plansummary.ifyouhaveanyquestions" javaScriptEscape="true"/></p>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goHome()"><spring:message code="indportal.portalhome.godashboard" javaScriptEscape="true"/></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
</div>
<!-- SubmitDisenroll-->

<!-- Failure Modal-->
<div ng-show="submitDisenrollmentFailureDialog">
 <div modal-show="submitDisenrollmentFailureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedSumissionModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="failedSumissionModal"><spring:message code="indportal.plansummary.technicalissues" javaScriptEscape="true"/></h3>
        </div>
        <div class="modal-body gutter20">
          <p><spring:message code="indportal.plansummary.cannotsubmitrequest" javaScriptEscape="true"/></p>
          <p><spring:message code="indportal.plansummary.ifyouhaveanyquestions" javaScriptEscape="true"/></p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
  </div>
</div>
<!-- Failure Modal-->

<!-- Failure Modal 2-->
<div ng-show="payFHFail">
 <div modal-show="payFHFail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedFHModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="failedFHModal"><spring:message code="indportal.plansummary.technicalissues" javaScriptEscape="true"/></h3>
        </div>
        <div class="modal-body gutter20">
          <p><spring:message code="indportal.plansummary.cannotsubmitrequest" javaScriptEscape="true"/></p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
</div>
<!-- Failure Modal 2-->
<!-- Payment success Modal-->
<div ng-show="paySuccess">
	<div modal-show="paySuccess" class="modal fade" tabindex="-1"
		role="dialog" aria-labelledby="paySuccessModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h3 id="paySuccessModal">Complete your Payment</h3>
				</div>
				<div class="modal-body gutter20">
					<p>Click 'Proceed To Payment' to complete your payment on the payment portal.</p>
				</div>
				<div class="modal-footer">
					<form id="postform" name="postform" method="get" action="finance/paymentRedirect" target="_blank">
						<a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message
							code="indportal.portalhome.cancel" javaScriptEscape="true" /></a>
						<input type="hidden" id="finEnrlDataKey" name="finEnrlDataKey"/>
						<input type="submit" value="Proceed To Payment" class="btn btn-primary" ng-click="payHealthExternal()">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Payment success Modal -->
<!-- Excessive Aptc Modal-->
<div ng-show="aptcOverMax">
 <div modal-show="aptcOverMax" class="modal fade ex-aptc" tabindex="-1" role="dialog" aria-labelledby="aptcOverMaxModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="aptcOverMaxModal">APTC Over Amount Allowed</h3>
        </div>
        <div class="modal-body gutter20">
          <p>Your input of APTC exceeds the amount allowed. You can press "Max" to set your APTC amount to the maximum allowed; or</p>
		  <p>You can press "Cancel" to set your APTC before the change.</p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal" href="/#" ng-click="changeSelectedAptc()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
		  <a class="btn btn-primary" data-dismiss="modal" href="/#" ng-click="changeSelectedAptc('max')">Max</a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
</div>
<!-- Excessive Aptc Modal-->
<!-- aptc slider -->
<div ng-show="showAptcSlider">
		<div modal-show="showAptcSlider" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="changeElectedAptcModal" aria-hidden="true">

			<div class="modal-dialog">
				<div class="modal-content">
				<!-- header -->
				<div class="modal-header">
					<button id="aptcClosebtn" type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h3 id="changeElectedAptcModal"><spring:message	code="indportal.atpcModal.header" javaScriptEscape="true" /></h3>
				</div>

				<div class="margin10 modal-body">
					<div class="carousel">
						<!-- Carousel items -->
						<div class="carousel-inner">
							<!-- item 1 -->
							<div class="item" ng-class="{'active' : showItem1}">
								<h5><spring:message	code="indportal.atpcModal.aptcExplanation"/> </h5>

                <div ng-if="stateCode !== 'MN'">
                  <hr>
                  <p><spring:message code="indportal.atpcModal.usageAptc"/></p>

                  <div class="alert alert-info">
                    <p>
                      <strong><spring:message code="indportal.atpcModal.aptcCredit"/></strong>
                    </p>
                    <p><spring:message code="indportal.atpcModal.aptcCreditContent"/></p>
                  </div>

                  <div class="alert alert-info">
                    <p>
                      <strong><spring:message code="indportal.atpcModal.aptcmonthlyAdv"/></strong>
                    </p>

                    <p><spring:message code="indportal.atpcModal.aptcmonthlyAdvContent"/></p>
                  </div>
                </div>
                <!-- MN only -->
                <div ng-if="stateCode === 'MN'">
                  <div>
                    <p><spring:message code="indportal.atpcModal.usageAptc"/></p>
                  </div>
                  <table class="table" style="border:none;">
                    <tbody>
                      <tr style="border:none;background-color:#f8f8f8;">
                        <td style="border:none;color:#027070;"><spring:message code="indportal.atpcModal.aptcmonthlyAdv"/></td>
                        <td style="border:none;vertical-align:top;color:#027070;"><spring:message code="indportal.atpcModal.aptcCredit"/></td>
                      </tr>
                      <tr style="border:none;">
                        <td style="border:none;"><spring:message code="indportal.atpcModal.aptcmonthlyAdvContent"/></td>
                        <td style="border:none;vertical-align:top;"><spring:message code="indportal.atpcModal.aptcCreditContent"/></td>
                      </tr>
                      <tr style="border:none;">
                        <td style="border:none;vertical-align:top;"><b><spring:message code="indportal.atpcModal.aptcmonthlyAdvContent.pro"/></b> <spring:message code="indportal.atpcModal.aptcmonthlyAdvContent.pro.content"/></td>
                        <td style="border:none;"><b><spring:message code="indportal.atpcModal.aptcCreditContent.pro" javaScriptEscape="true"/></b> <spring:message code="indportal.atpcModal.aptcCreditContent.pro.content"/></td>
                      </tr>
                      <tr style="border:none;">
                        <td style="border:none;"><b><spring:message code="indportal.atpcModal.aptcmonthlyAdvContent.con"/></b> <spring:message code="indportal.atpcModal.aptcmonthlyAdvContent.con.content"/></td>
                        <td style="border:none;vertical-align:top;"><b><spring:message code="indportal.atpcModal.aptcCreditContent.con"/></b> <spring:message code="indportal.atpcModal.aptcCreditContent.con.content"/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <div ng-if="stateCode !== 'MN'">
                  <p>
                    <strong><spring:message code="indportal.atpcModal.instruction1"/></strong>
                  </p>
                </div>
							</div>
							<!-- item 1 ends -->

							<!-- item 2 -->
							<div class="item" ng-class="{'active' : showItem2}">

								<!-- SLIDER -->
								<div class="well well-small">
								<h5><spring:message code="indportal.atpcModal.sliderInstruction"/></h5>
									<div class="row-fluid aptc-container">
                    <div class="span12">
                      <span><label for="montax"><spring:message code="indportal.atpcModal.sliderCredit"/></label></span>
                    </div>
										<div class="span12 margin0">
                      <div class="slider-container">
                        <span class="val pull-left">$ <input type="text" max="{{planSummaryDetails.maxAptc}}" id="aptcText" ng-model="newAptc" class="input-mini txt-center margin0" aria-label="Monthly Tax Credit  Sent Directly to Insurance Company" ng-change="getSliderWidth()" ng-blur="formatAptcCredit()" num-only check-max="newAptc"></span>

                        <div class="span8 margin0">
                          <input id="aptcSliderValue" type="range" min="0" max="{{planSummaryDetails.maxAptc}}"
                              class="aptc" step="0.01" ng-model="newAptc" ng-change="getSliderWidth()" value="{{newAptc}}"
                              ng-blur="formatAptcCredit()" aria-label="Monthly Tax Credit  Sent Directly to Insurance Company"/>
                            <div class="range-container">
                              <div ng-style="{'width':width}" ></div>
                            </div>
                        </div>
  
                        <!--<span class="val">$	<input type="text" id="anntax" class="input-mini txt-center margin0" value="" readonly="" aria-disabled="true" aria-label="Amount per month credited towards Next Year's Tax Return $ ">{{planSummaryDetails.maxAptc - newAptc}}</span>-->
                        <span class="val" ng-show="planSummaryDetails.maxAptc - newAptc >= 0">	{{planSummaryDetails.maxAptc - newAptc | currency }}</span>
                      </div>
										</div>
									</div>

									<div class="margin20-t alert alert-info" ng-if="isAptcGetUpdated === true">
										<spring:message code="indportal.atpcModal.effectiveDate"/>: {{sliderEffectiveDate}}
									</div>

								</div>
								<p>
									<strong><spring:message code="indportal.atpcModal.amountTitle"/></strong>
                </p>
                <ul style="list-style-type:disc;">
									<li><spring:message code="indportal.atpcModal.amountContent1"/></li>
									<li><spring:message code="indportal.atpcModal.amountContent2_1"/></li>
                  <li><spring:message code="indportal.atpcModal.amountContent3_1"/></li>
                </ul>
							</div>
							<!-- item 2 ends -->
						</div>
					</div>
				</div>
				<div class="modal-footer">
          <div id="carousel-nav" class="pull-left">
            <a id="slide1" href="javascript:void(0);" data-to="1" title="<spring:message code='pd.label.showcart.button.1'/>" ng-class="{'active': showItem1}" ng-click="showSlide1()"><spring:message code="pd.label.showcart.button.1"/></a>
            <a id="slide2" href="javascript:void(0);" data-to="2" title="<spring:message code='pd.label.showcart.button.2'/>" ng-class="{'active': showItem2}" ng-click="showSlide2()"><spring:message code="pd.label.showcart.button.2"/></a>
          </div>
					<div class="pull-right">
						<a id="aptcCloseTaxbtn" href="javascript:void(0);" data-dismiss="modal" class="btn"><spring:message code="indportal.portalhome.close"/></a>
						<a id="aptcAdjustTaxbtn" href="javascript:void(0);" class="btn btn-primary" ng-click="showSlide2()" ng-show="showAdjustButton"><spring:message code="indportal.atpcModal.adjustAptc"/></a>
						<a id="aptcConfirmTaxbtn" href="javascript:void(0);" class="btn btn-primary" ng-click="newAptc == planSummaryDetails.electedAptc || planSummaryDetails.maxAptc - newAptc < 0 || saveAptc()" ng-show="showConfirmButton" ng-disabled="newAptc == electedAptc || planSummaryDetails.maxAptc - newAptc < 0 || newAptc[newAptc.length -1] ==='.' || aptcOverMax "><spring:message code="indportal.portalhome.confirm"/></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- modal -->



<div ng-show="showAptcSliderError">
	<div modal-show="showAptcSliderError" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="aptcErrorModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 id="aptcErrorModal"><spring:message code="indportal.atpcErrorModal.header" javaScriptEscape="true" /></h3>
		</div>
		<div class="modal-body">
			<p class="alert alert-error">
				<spring:message code="indportal.atpcErrorModal.error"/>
			</p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.atpcErrorModal.close"/></button>
		</div>
	</div>
</div>
</script>
<script type="text/javascript">

function css_browser_selector(u) {
    var ua = u.toLowerCase(),
        is = function(t) {
            return ua.indexOf(t) > -1
        },
        g = 'gecko',
        w = 'webkit',
        s = 'safari',
        o = 'opera',
        m = 'mobile',
        h = document.documentElement,
        b = [(!(/opera|webtv/i.test(ua)) && /trident/.test(ua)) ? ('ie ie1') : is('firefox/2') ? g + ' ff2' : is('firefox/3.5') ? g + ' ff3 ff3_5' : is('firefox/3.6') ? g + ' ff3 ff3_6' : is('firefox/3') ? g + ' ff3' : is('gecko/') ? g : is('opera') ? o + (/version\/(\d+)/.test(ua) ? ' ' + o + RegExp.$1 : (/opera(\s|\/)(\d+)/.test(ua) ? ' ' + o + RegExp.$2 : '')) : is('konqueror') ? 'konqueror' : is('blackberry') ? m + ' blackberry' : is('android') ? m + ' android' : is('chrome') ? w + ' chrome' : is('iron') ? w + ' iron' : is('applewebkit/') ? w + ' ' + s + (/version\/(\d+)/.test(ua) ? ' ' + s + RegExp.$1 : '') : is('mozilla/') ? g : '',
        is('j2me') ? m + ' j2me' : is('iphone') ? m + ' iphone' : is('ipod') ? m + ' ipod' : is('ipad') ? m + ' ipad' : is('mac') ? 'mac' : is('darwin') ? 'mac' : is('webtv') ? 'webtv' : is('win') ? 'win' + (is('windows nt 6.0') ? ' vista' : '') : is('freebsd') ? 'freebsd' : (is('x11') || is('linux')) ? 'linux' : '', 'js'];
c = b.join(' ');
h.className += ' ' + c;
return c;
};
css_browser_selector(navigator.userAgent);
</script>

<script type="text/ng-template" id="planSummaryTemplate">
<div class="medicalPlan">
          <div class="row-fluid">
            <div class="span6 planBody">
              <div class="row-fluid info-header txt-center pull-left planHeader">
                <img ng-src="{{activePlanDetails.planLogoUrl}}" alt="{{activePlanDetails.issuerName}} logo">
                <p><a href="javascript:void(0)">{{activePlanDetails.planName}}</a></p>
              </div>
              <div class="pull-left">
                <table class="dl-horizontal planInfo">
									<caption class="aria-hidden">{{activePlanDetails.planName}}</caption>
                	<tr>
                		<th scope="row" class="plan-summary"><spring:message code="indportal.plansummary.plantype" javaScriptEscape="true"/>:</th>
                 		<td id="plan-type" class="plan-summary">{{activePlanDetails.planType}}</td>
                 	</tr>
                 	<tr>
                    <th scope="row" class="plan-summary"><div ng-if="activePlanDetails.typeOfPlan === 'Health'"><spring:message code="indportal.plansummary.officevisit" javaScriptEscape="true"/>:</div>
                      <div ng-if="activePlanDetails.typeOfPlan === 'Dental'" style="white-space:pre-wrap;"><spring:message code="indportal.plansummary.dentalOfficevisit" javaScriptEscape="true"/>:</div>
                    </th>
	                  <td id="off-visit" class="plan-summary">
						<span ng-if="activePlanDetails.officeVisit === null">"N/A"</span>
						<span ng-if="activePlanDetails.officeVisit !== null"></span><span ng-bind-html="activePlanDetails.officeVisit"></span></td>
                 	</tr>
                 	<tr ng-if="activePlanDetails.typeOfPlan === 'Health'">
	                  <th scope="row" class="plan-summary"><spring:message code="indportal.plansummary.genericmedications" javaScriptEscape="true"/>:</th>
	                  <td id="generic-medic" class="plan-summary"><span ng-bind-html="activePlanDetails.genericMedications"></span></td>
                 	</tr>
                 	<tr>
                    <th scope="row" class="plan-summary"><div ng-if="activePlanDetails.typeOfPlan === 'Health'"><spring:message code="indportal.plansummary.deductible" javaScriptEscape="true"/>:</div>
                      <div ng-if="activePlanDetails.typeOfPlan === 'Dental'"><spring:message code="indportal.plansummary.dentalDeductible" javaScriptEscape="true"/>:</div></th>
	                  <td id="deductible" class="plan-summary">{{activePlanDetails.deductible}}</td>
                 	</tr>
                 	<tr>
                    <th scope="row" class="plan-summary"><div ng-if="activePlanDetails.typeOfPlan === 'Health'"><spring:message code="indportal.plansummary.maxoutofpocket" javaScriptEscape="true"/>:</div>
                      <div ng-if="activePlanDetails.typeOfPlan === 'Dental'"><spring:message code="indportal.plansummary.dentalMaxoutofpocket" javaScriptEscape="true"/>:</div></th>
	                  <td id="m-o-p" class="plan-summary">{{activePlanDetails.oopMax}}</td>
	                </tr>
                </table>
              </div>
              <div class="center viewBenefitButton"><a id="view-benefit-details" href="<c:url value="/private/planinfo/{{activePlanDetails.coverageStartDate | removeForwardSlashes}}/{{activePlanDetails.planId}}" />" target="_blank" class="btn btn-primary btn-small"><spring:message code="indportal.plansummary.viewbenefitdetails" javaScriptEscape="true"/></a></div>
            </div>

            <div class="span6 planBody">
              <div class="row-fluid info-header planHeader">
                <h5 class="margin10-l"><spring:message code="indportal.plansummary.policyinformation" javaScriptEscape="true"/></h5>
              </div>
              <div class="">
                <div class="dl-horizontal planTable">
                  <dl>
				        <dt><spring:message code="indportal.plansummary.policyid" javaScriptEscape="true"/>:</dt>
                  		<dd id="plan-type" ng-if="activePlanDetails.policyId">{{activePlanDetails.policyId}}</dd>
						    <dd id="plan-type" ng-if="!activePlanDetails.policyId">N/A</dd>

				        <dt><spring:message code="indportal.plansummary.coverageperiod" javaScriptEscape="true"/>:</dt>
                  <dd id="off-visit">{{activePlanDetails.coverageStartDate}} - {{activePlanDetails.coverageEndDate}}</dd>

                  <dt><spring:message code="indportal.plansummary.status" javaScriptEscape="true"/>:</dt>
                  <dd id="generic-medic">{{activePlanDetails.enrollmentStatus}}</dd>
                  </dl>
                      <dl ng-show="{{activePlanDetails.disEnroll && activePlanDetails.paymentUrl && activePlanDetails.enrollmentStatus != null && activePlanDetails.enrollmentStatus.toUpperCase() === 'PENDING'}}"
                      ng-if="stateCode !== 'MN'">
                      <dt><spring:message code="indportal.plansummary.paymenturl" javaScriptEscape="true"/>:</dt>
                      <dd id="generic-medic" class="ng-binding">
                      <div ><a id="pay-health-insurance" href="javascript:void(0)" ng-click="payForHealth(activePlanDetails.caseNumber, activePlanDetails.typeOfPlan,activePlanDetails.enrollmentId)" class="btn btn-primary btn-small">Pay for {{activePlanDetails.typeOfPlan}} Insurance</a></div>
                      </dd> </dl>
                    </div>
              </div>
              <div class="row-fluid info-header planHeader">
                <h5 class="margin10-l">
                  <div ng-if="stateCode !== 'MN'"><spring:message code="indportal.plansummary.paymentinformation" javaScriptEscape="true"/></div>
                  <div ng-if="stateCode === 'MN'"><spring:message code="indportal.plansummary.dentalPaymentinformation" javaScriptEscape="true"/></div>
                </h5>
              </div>

              <div class="">
                <dl class="dl-horizontal planTable">
                  <dt><spring:message code="indportal.plansummary.monthlypremuim" javaScriptEscape="true"/>:</dt>
                  <dd>{{activePlanDetails.premium | currency}}</dd>
				  <dt ng-if="activePlanDetails.electedAptc != 'None'"><spring:message code="indportal.plansummary.electedAptc" javaScriptEscape="true"/>:</dt>
                  <dd ng-if="activePlanDetails.electedAptc != 'None'">{{activePlanDetails.electedAptc | currency}}</dd>
                    <dt ng-if="activePlanDetails.stateSubsidyAmount != null"><spring:message code="indportal.plansummary.stateSubsidyAmount" javaScriptEscape="true"/>:</dt>
                    <dd ng-if="activePlanDetails.stateSubsidyAmount != null">{{activePlanDetails.stateSubsidyAmount | currency}}</dd>
 				  <dt><spring:message code="indportal.plansummary.netPremium" javaScriptEscape="true"/>:</dt>
                  <dd>{{activePlanDetails.netPremium | currency}}</dd>

				  <dt ng-if="activePlanDetails.premiumEffDate !== null">
            <spring:message code="indportal.plansummary.premiumEffectiveDate"/>
            <a class="ttclass" rel="tooltip" href="javascript:void(0)" aria-hidden="<spring:message code="indportal.plansummary.premiumEffectiveDateTooltip"/>"
            data-original-title="<spring:message code="indportal.plansummary.premiumEffectiveDateTooltip"/>"><i class="icon-question-sign"></i><span class="aria-hidden">More Information</span></a>:
				  </dt>
                  <dd ng-if="activePlanDetails.premiumEffDate !== null">{{activePlanDetails.premiumEffDate}}</dd>
            </dl>

				  <div class="center margin20-b" ng-if="activePlanDetails.allowAptcAdjustment">
					<a id="adjust-premium-tax" href="javascript:void(0)" class="btn btn-primary btn-small" ng-click="changeAptc(activePlanDetails)"><spring:message code="indportal.plansummary.adjustAptc" javaScriptEscape="true"/></a>
				  </div>
                
              </div>
              <div class="row-fluid info-header planHeader">
                <h5 class="margin10-l"><spring:message code="indportal.plansummary.contactinformation" javaScriptEscape="true"/></h5>
              </div>
              <div class="">
                <dl class="dl-horizontal planTable">
                  <dt><spring:message code="indportal.plansummary.customerservice" javaScriptEscape="true"/>:</dt>
                  <dd id="plan-type">{{activePlanDetails.issuerContactPhone}}</dd>
                  <dt><spring:message code="indportal.plansummary.web" javaScriptEscape="true"/>:</dt>
					<dd id="off-visit" class="ng-binding"><a href="{{activePlanDetails.issuerContactUrl}}"  target="_blank"><u>Click Here.</u></a></dd>
                </dl>
              </div>

              <div class="row-fluid info-header planHeader"  ng-if="activePlanDetails.coveredMembers.length>0">
                <h5 class="margin10-l"><spring:message code="indportal.plansummary.coveredfamilymembers" javaScriptEscape="true"/></h5>
              </div>

              <p class="center margin0-b margin10-t">{{member.name}}</p>
              <dl class="dl-horizontal planTable familyCovered" ng-repeat="member in activePlanDetails.coveredMembers track by $index">
                <dt>Coverage Start Date:</dt>
                <dd>{{member.coverageStartDate | date:'mediumDate'}}</dd>
                <dt>Coverage End Date:</dt>
                <dd>{{member.coverageEndDate | date:'mediumDate'}}</dd>
              </dl>
            </div>
          </div>
         </div>


		<div class="row-fluid" ng-show="{{activePlanDetails.disEnroll}}">
            <div class="span6"></div>
            <div class="span6">
		    <sec:accesscontrollist hasPermission="IND_PORTAL_DISENROLL" domainObject="user">
              <div class="center gutter20">
                <a id="disenrollPlan" href="javascript:void(0)" class="btn btn-primary btn-small">
                  <span ng-if="activePlanDetails.typeOfPlan === 'Health'" ng-click="disenrollAlert('planSummary', activePlanDetails.typeOfPlan, activePlanDetails.enrollmentId, activePlanDetails.validCoverageHealth)">
                    <span ng-if="!activePlanDetails.validCoverageHealth"><spring:message code="indportal.plansummary.cancelCoverage" javaScriptEscape="true"/></span>
                    <span ng-if="activePlanDetails.validCoverageHealth"><spring:message code="indportal.plansummary.disenrollfromhealthplan" javaScriptEscape="true"/></span>
                  </span>
                  <span ng-if="activePlanDetails.typeOfPlan === 'Dental'" ng-click="disenrollAlert('planSummary', activePlanDetails.typeOfPlan, activePlanDetails.enrollmentId, activePlanDetails.validCoverageDental)">
                    <span ng-if="!activePlanDetails.validCoverageDental"><spring:message code="indportal.plansummary.cancelCoverage" javaScriptEscape="true"/></span>
                    <span ng-if="activePlanDetails.validCoverageDental"><spring:message code="indportal.plansummary.disenrollfromdentalplan" javaScriptEscape="true"/></span>
                  </span>
                </a>
              </div>
			</sec:accesscontrollist>
            </div>
          </div>
</script>




