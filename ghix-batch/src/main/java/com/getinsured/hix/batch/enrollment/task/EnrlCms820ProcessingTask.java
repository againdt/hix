package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrlCms820Service;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;

public class EnrlCms820ProcessingTask  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlCms820ProcessingTask.class);
	private EnrlCms820Service enrlCms820Service;
	private JobService jobService;
	long jobExecutionId = -1;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		LOGGER.info("Enrollment CMS 820 processing step intiated");
		if(null != enrlCms820Service){
			jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			String batchJobStatus=null;
			
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
										batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			LOGGER.trace("EnrlCms820ProcessingTask calling EnrlCms820Service.processCms820");
			StepExecution stepExecution = chunkContext.getStepContext().getStepExecution();
			enrlCms820Service.processCms820(jobService, stepExecution);
			
		}else{
			LOGGER.error(" EnrlCms820Service is null ");
			throw new RuntimeException("EnrlCms820Service is null") ;
		}
		return RepeatStatus.FINISHED;
	}

	public EnrlCms820Service getEnrlCms820Service() {
		return enrlCms820Service;
	}

	public void setEnrlCms820Service(EnrlCms820Service enrlCms820Service) {
		this.enrlCms820Service = enrlCms820Service;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
}
