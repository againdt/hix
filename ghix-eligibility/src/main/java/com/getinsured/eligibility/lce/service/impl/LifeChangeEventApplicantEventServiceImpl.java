package com.getinsured.eligibility.lce.service.impl;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateMidnight;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.sep.repository.SepEventsRepository;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicantEventService;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.model.SepEvents.Financial;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepTransientDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.EventPrecedenceIndicator;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Service
public class LifeChangeEventApplicantEventServiceImpl implements
		LifeChangeEventApplicantEventService {
	
	private final String REPORTSTARTDATE = "REPORTSTARTDATE";	
	private final String REPORTENDDATE = "REPORTENDDATE";	
	private final String ENROLLMENTSTARTDATE = "ENROLLMENTSTARTDATE";	
	private final String ENROLLMENTENDDATE = "ENROLLMENTENDDATE";	
	private final String CREATEDTIMESTAMP = "CREATEDTIMESTAMP";	
	private final String COVERAGEENDDATE = "COVERAGEENDDATE";	
	private final String COVERAGESTARTDATE = "COVERAGESTARTDATE";
	private final String EVENTDATE = "EVENTDATE";
	private static int numberOfenrollmentDays = 60;
	private final int MAX_FUTURE_DAYS_ALLOWED=60;
	private final int MIN_FUTURE_DAYS_ALLOWED= 0;
	public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventApplicantEventServiceImpl.class);

	
	@Autowired private CommentTargetService commentTargetService;
	private static final String SUCCESS = "success";
	
	@Autowired
	private SepEventsRepository sepEventsRepository;
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;
	
	@Autowired
	private ReferralOEService referralOEService;
	

	@Override
	public List<SsapApplicantEvent> cloneSsapApplicantEvents(SsapApplication currentApplication, SsapApplicationEvent ssapApplicationEvent, List<SsapApplicant> ssapApplicants, List<RequestParamDTO> events, SepTransientDTO sepTransientDTO) throws Exception {
		//init();
		List<SsapApplicantEvent> ssapApplicantEvents = new ArrayList<SsapApplicantEvent>();
		Map<Long, List<RequestParamDTO>> changedApplicantsWithEvent = getChangedApplicantsWithEvents(events);
		JsonArray householdMembers = LifeChangeEventUtil.getHouseholdMembers(currentApplication.getApplicationData());
		Map<Long, List<SepEvents>> multiEventData = new HashMap<Long, List<SepEvents>>();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			List<SsapApplicantEvent> ssapApplicantEventList = new ArrayList<SsapApplicantEvent>();
			if(changedApplicantsWithEvent.containsKey(ssapApplicant.getPersonId())) {
				List<RequestParamDTO> eventList = changedApplicantsWithEvent.get(ssapApplicant.getPersonId());
				for (RequestParamDTO requestParamDTO : eventList) {
					for (ChangedApplicant changedApplicant : requestParamDTO.getChangedApplicants()) {
						if(changedApplicant.getPersonId().equals(ssapApplicant.getPersonId())) {
							SsapApplicantEvent sSapApplicantEvent = createSsapApplicantEvent(null, currentApplication, ssapApplicationEvent, LifeChangeEventUtil.getDate(changedApplicant.getEventDate()), requestParamDTO.getEventCategory(), requestParamDTO.getEventSubCategory(), changedApplicant.getPersonId(), householdMembers, changedApplicant.getIsChangeInZipCodeOrCounty(), ssapApplicant, sepTransientDTO.getUserId(), multiEventData, sepTransientDTO.getEnrolledPersons());
							ssapApplicantEventList.add(sSapApplicantEvent);
						}
					}
				}
			}
			else {
				SsapApplicantEvent sSapApplicantEvent = createSsapApplicantEvent(null, currentApplication, ssapApplicationEvent, new TSDate(), LifeChangeEventConstant.OTHER, LifeChangeEventConstant.OTHER, ssapApplicant.getPersonId(), householdMembers, false, ssapApplicant, sepTransientDTO.getUserId(), multiEventData, sepTransientDTO.getEnrolledPersons());
				ssapApplicantEventList.add(sSapApplicantEvent);
			}
			int sepEventId = checkEventPrecedence(multiEventData.get(ssapApplicant.getPersonId()));
			for (SsapApplicantEvent ssapApplicantEvent : ssapApplicantEventList) {
				if(ssapApplicantEvent.getSepEvents().getId() == sepEventId) {
					ssapApplicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				}
			}
			ssapApplicantEvents.addAll(ssapApplicantEventList);
		}
		updateEnrollmentStartAndEndDates(currentApplication, ssapApplicationEvent, sepTransientDTO.getDemographicFlagMap(), sepTransientDTO.getDemographicEventsData(), sepTransientDTO.getHomeAddressChangeDemoFlags(), sepTransientDTO.getDemographicEventDate(), sepTransientDTO.getDobEventDate(),ssapApplicantEvents);
		
		return ssapApplicantEvents;
	}
	
	private SsapApplicantEvent createSsapApplicantEvent(SsapApplicantEvent ssapApplicantEvent, SsapApplication currentApplication, SsapApplicationEvent ssapApplicationEvent, Date eventDate, String lookUpType, String lookUpCode, Long personId, JsonArray householdMembers, Boolean isChangeInZipCodeOrCounty, SsapApplicant ssapApplicant, Long userId, Map<Long, List<SepEvents>> multiEventData, Set<Long> enrolledPerson) throws Exception{
		
		if(ssapApplicantEvent==null){
			ssapApplicantEvent = new SsapApplicantEvent();
		}
		ssapApplicantEvent.setId(0);
		ssapApplicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
		
		ssapApplicantEvent.setSsapApplicant(ssapApplicant);		
		ssapApplicantEvent.setCreatedBy(userId.intValue());
		ssapApplicantEvent.setCreatedTimeStamp(new Timestamp(new TSDate().getTime()));		
		setSSAPApplicantEvent(ssapApplicantEvent, currentApplication, eventDate, lookUpType, lookUpCode, personId, householdMembers, isChangeInZipCodeOrCounty,multiEventData, enrolledPerson);
		ssapApplicantEvent.setEventPrecedenceIndicator(null);

		return ssapApplicantEvent;	
	}
	
	private SsapApplicantEvent setSSAPApplicantEvent(SsapApplicantEvent ssapApplicantEvent, SsapApplication currentApplication, Date eventDate, String lookUpType, String lookUpCode, Long personId, JsonArray householdMembers, Boolean isChangeInZipCodeOrCounty, Map<Long, List<SepEvents>> multiEventData, Set<Long> enrolledPerson) throws Exception{
		SepEvents changeEventType;
		
		//LOGGER.info("Setting Applicant Event : Starts");
		
		try {
			changeEventType = getSepEnrollmentEvent(personId, householdMembers, lookUpType, lookUpCode, currentApplication.getParentSsapApplicationId().longValue(), enrolledPerson,eventDate);
			boolean isNextYearsOE = referralOEService.isOpenEnrollment(currentApplication.getCoverageYear()) ;

			if(!LifeChangeEventConstant.OTHER.equals(changeEventType.getName()) && eventDate!=null){
				Map<String, Timestamp> lifeChangeEventRules = processLifeEventRules(eventDate, changeEventType, lookUpType, personId, householdMembers,isNextYearsOE);
				ssapApplicantEvent.setCoverageStartDate(lifeChangeEventRules.get(COVERAGESTARTDATE));
				ssapApplicantEvent.setCoverageEndDate(lifeChangeEventRules.get(COVERAGEENDDATE));
				ssapApplicantEvent.setEnrollmentEndDate(lifeChangeEventRules.get(ENROLLMENTENDDATE));
				ssapApplicantEvent.setEnrollmentStartDate(lifeChangeEventRules.get(ENROLLMENTSTARTDATE));
				ssapApplicantEvent.setReportEndDate(lifeChangeEventRules.get(REPORTENDDATE));
				ssapApplicantEvent.setReportStartDate(lifeChangeEventRules.get(REPORTSTARTDATE));
				ssapApplicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			}
			else if(LifeChangeEventConstant.OTHER.equals(changeEventType.getName()) && eventDate != null) {
				ssapApplicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			}
			
			ssapApplicantEvent.setSepEvents(changeEventType);
			
			initMultipleEventData(personId, changeEventType, multiEventData);
			//initDemographicChange(changeEventType, personId, eventDate, isChangeInZipCodeOrCounty);
			
		} catch (Exception exception) {
			throw exception;
		}
		
		//LOGGER.info("Setting Applicant Event : Ends");
		
		return ssapApplicantEvent;
	}
	
	private Map<String, Timestamp> processLifeEventRules(Date eventDate, SepEvents changeEventType, String lookupType, Long personId, JsonArray householdMembers, boolean isNextYearsOE) throws Exception {
		Map<String, Timestamp> result = new HashMap<String, Timestamp>();
		
		try {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(eventDate);
			result.put(EVENTDATE, new Timestamp(eventDate.getTime()));
			result.put(REPORTSTARTDATE, getReportStartDate(changeEventType));
			result.put(REPORTENDDATE, getReportEndDate(changeEventType));
			result.put(ENROLLMENTSTARTDATE, getEnrollmentStartDate(eventDate, changeEventType, lookupType));
			result.put(ENROLLMENTENDDATE, getEnrollmentEndDate(eventDate, changeEventType, lookupType,isNextYearsOE));
			result.put(CREATEDTIMESTAMP, getCreatedTimeStamp(eventDate, changeEventType));
			result.put(COVERAGESTARTDATE, getCoverageStartDate(eventDate, changeEventType, lookupType));
			result.put(COVERAGEENDDATE, getCoverageEndDate(eventDate, changeEventType, lookupType));
		} catch(Exception exception){
			throw exception;
		}
		
		return result;
	}
	
	private SepEvents getSepEnrollmentEvent(Long personId, JsonArray householdMembers, String lookUpType, String lookUpCode,long oldSsapApplicationId, Set<Long> enrolledPerson, Date eventDate) throws Exception{
		SepEvents changeEventType;
		
		try {
			if(lookUpType.equalsIgnoreCase(LifeChangeEventConstant.UPDATE_DEPENDENTS_DEATH)){
				if(personId==LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID){
					changeEventType = sepEventsRepository.findByEventNameAndFinancial(LifeChangeEventConstant.UPDATE_DEPENDENTS_DEATH_OF_PRIMARY_APPLICANT, LifeChangeEventConstant.UPDATE_DEPENDENTS, Financial.N);
				} else {
					changeEventType = sepEventsRepository.findByEventNameAndFinancial(LifeChangeEventConstant.UPDATE_DEPENDENTS_DEATH_OF_DEPENDENTS, LifeChangeEventConstant.UPDATE_DEPENDENTS, Financial.N);
				}
			} else if(lookUpType.equalsIgnoreCase(LifeChangeEventConstant.INCARCERATION_CHANGE)){
				changeEventType = getIncarcerationEventDetails(personId, householdMembers,oldSsapApplicationId, enrolledPerson);
			} else if(lookUpType.equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS) && lookUpCode.equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS_RELATIONSHIP_CHANGE_REMOVE) && !(enrolledPerson != null && enrolledPerson.contains(personId)) ){
				changeEventType = sepEventsRepository.findByEventNameAndFinancial(LifeChangeEventConstant.DEMOGRAPHICS_RELATIONSHIP_CHANGE, lookUpType, Financial.N);
			} else if((lookUpType.equalsIgnoreCase(LifeChangeEventConstant.LOSS_OF_MEC)||lookUpType.equalsIgnoreCase(LifeChangeEventConstant.GAIN_MEC)) && Days.daysBetween( new DateMidnight(),new DateMidnight(eventDate)).getDays() < MAX_FUTURE_DAYS_ALLOWED && Days.daysBetween( new DateMidnight(),new DateMidnight(eventDate)).getDays() >  MIN_FUTURE_DAYS_ALLOWED ){
				changeEventType = sepEventsRepository.findByEventNameAndFinancial(lookUpCode+"_TYPE4", lookUpType, Financial.N);
				if(changeEventType == null){
					changeEventType = sepEventsRepository.findByEventNameAndFinancial(lookUpCode, lookUpType, Financial.N);
				}
			} else {
				changeEventType = sepEventsRepository.findByEventNameAndFinancial(lookUpCode, lookUpType, Financial.N);
			}
			
			//Override event if change type of event is "REMOVE" and fetch the New event with change type "OTHER"
			if((changeEventType.getChangeType().equals(LifeChangeEventConstant.CHANGE_TYPE_REMOVE) && !(enrolledPerson != null && enrolledPerson.contains(personId))) || (changeEventType.getChangeType().equals(LifeChangeEventConstant.CHANGE_TYPE_ADD) && (enrolledPerson != null && enrolledPerson.contains(personId))) ){
				SepEvents tempChangeEventType  = sepEventsRepository.findByEventNameAndFinancial(lookUpCode+LifeChangeEventConstant.CHANGE_TYPE_OTHER_CONCAT_STRING, lookUpType, Financial.N);
				if(tempChangeEventType != null){
					changeEventType = tempChangeEventType;
				}
			}
			
		} catch (Exception exception) {
			throw exception;
		}
		
		return changeEventType;
	}
	
	private SepEvents getIncarcerationEventDetails(Long personId, JsonArray householdMembers,long oldSsapApplicationId, Set<Long> enrolledPerson) throws Exception{
		JsonObject householdMember = null;
		JsonObject incarcerationStatus = null;
		SepEvents sepEventsResource	= null;	
		
		try {
			householdMember = LifeChangeEventUtil.getHouseholdMember(householdMembers, String.valueOf(personId));
			incarcerationStatus = householdMember.get(LifeChangeEventConstant.INCARCERATION_STATUS).getAsJsonObject();
			String incarcerationStatusIndicator = incarcerationStatus.get(LifeChangeEventConstant.INCARCERATION_STATUS_INDICATOR).getAsString();
			
			if(!LifeChangeEventUtil.isEmpty(incarcerationStatusIndicator)){
				if(LifeChangeEventConstant.TRUE.equals(incarcerationStatusIndicator)){
					if(personId==LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID){
						sepEventsResource = sepEventsRepository.findByEventNameAndFinancial(LifeChangeEventConstant.INCARCERATED_PRIMARY, LifeChangeEventConstant.INCARCERATION_CHANGE, Financial.N);
					} else {
						sepEventsResource = sepEventsRepository.findByEventNameAndFinancial(LifeChangeEventConstant.INCARCERATED_DEPENDENTS, LifeChangeEventConstant.INCARCERATION_CHANGE, Financial.N);
					}
				} else {
					//check if person is already enrolled
					if((enrolledPerson != null && enrolledPerson.contains(personId))){
						//person already in enrollment - get the Not Incarcerted with change type other.
						sepEventsResource = sepEventsRepository.findByEventNameAndFinancial(LifeChangeEventConstant.NOT_INCARCERATED_OTHER, LifeChangeEventConstant.INCARCERATION_CHANGE, Financial.N);
					}else{
					   //person is not enrolled - get Not Incarcerate with change type Add
						sepEventsResource = sepEventsRepository.findByEventNameAndFinancial(LifeChangeEventConstant.NOT_INCARCERATED, LifeChangeEventConstant.INCARCERATION_CHANGE,Financial.N); 
					}
					
				}
			}
		} catch(Exception exception){
			throw exception;
		}
		
		return sepEventsResource;
	}

	
	private Map<Long, List<RequestParamDTO>> getChangedApplicantsWithEvents(List<RequestParamDTO> events) throws Exception{
		Map<Long, List<RequestParamDTO>> setOfModifiedApplicants = new  HashMap<>();
		List<RequestParamDTO> personEvents = null;
		
		for(RequestParamDTO requestParamDTO:events){
			List<ChangedApplicant> changedApplicants = requestParamDTO.getChangedApplicants();
			if(changedApplicants != null){
				for (ChangedApplicant changedApplicant : changedApplicants) {
					if(setOfModifiedApplicants.get(changedApplicant.getPersonId())!=null){
						personEvents = setOfModifiedApplicants.get(changedApplicant.getPersonId());
						personEvents.add(requestParamDTO);
					} else {
						personEvents = new ArrayList<>();
						personEvents.add(requestParamDTO);
					}
					setOfModifiedApplicants.put(changedApplicant.getPersonId(), personEvents);
				}
			}
		}
		
		return setOfModifiedApplicants;
	}
	
	private Timestamp getReportStartDate(SepEvents changeEventType) {
		if(changeEventType.getName().equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS)) {
			return null;
		}
		return new Timestamp(new TSDate().getTime());
	}

	private Timestamp getReportEndDate(SepEvents changeEventType) {
		if(changeEventType.getName().equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS)) {
			return null;
		}
		return new Timestamp(new TSDate().getTime());
	}

	private Timestamp getEnrollmentStartDate(Date eventDate, SepEvents changeEventType, String lookupType) throws ParseException {
		Timestamp  timestamp;
		if(changeEventType.getCategory().equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS) && !LifeChangeEventUtil.isDobEvent(changeEventType.getName()) && !changeEventType.getName().equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS_RELATIONSHIP_CHANGE)) {
			timestamp = null;
		} else if(changeEventType.getCategory().equalsIgnoreCase("SSAP_RENEWAL_EVENT")) {
			timestamp = new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
		    		parse(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.
		    				OPEN_ENROLLMENT_START_DATE) + " 00:00:00").getTime());
		}
		else {
			
			timestamp = new Timestamp(eventDate.getTime());
		}
		
		return timestamp;
	}

	private Timestamp getEnrollmentEndDate(Date eventDate, SepEvents changeEventType, String lookupType, boolean isNextYearsOE) throws ParseException {
		Timestamp  timestamp;
		if((changeEventType.getCategory().equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS) && !LifeChangeEventUtil.isDobEvent(changeEventType.getName()) && !changeEventType.getName().equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS_RELATIONSHIP_CHANGE))) {
			timestamp = null;
		} else if(changeEventType.getCategory().equalsIgnoreCase("SSAP_RENEWAL_EVENT")) {
			timestamp = new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
		    		parse(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.
		    				OPEN_ENROLLMENT_END_DATE) + " 00:00:00").getTime());
		} 
		else if (isNextYearsOE)
		{
			try {
				return new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
						parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) + " 23:59:59").getTime());
			} catch (ParseException e) {
				LOGGER.error("Error while calculating the SsapApplicantEvent Enrollment End Date",e);
				return null;
			}
		}
		else {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(eventDate);
			cal.add(Calendar.DATE, getEnrollmentGracePeriod());
			timestamp = checkForPastDate(new Timestamp(cal.getTime().getTime()));
		}
			
		return timestamp;
	}

	private int getEnrollmentGracePeriod() {
		int enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (!StringUtils.isEmpty(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
		
		return numberOfenrollmentDays + enrollmentGracePeriod; 
	}

	private Timestamp checkForPastDate(Timestamp timestamp) {
		if(timestamp != null && timestamp.before(new Timestamp(new TSDate().getTime()))) {
			return new Timestamp(new TSDate().getTime());
		}
		return timestamp;
	}

	private Timestamp getCreatedTimeStamp(Date eventDate, SepEvents changeEventType) {
		return new Timestamp(new TSDate().getTime());
	}

	private Timestamp getCoverageStartDate(Date eventDate, SepEvents changeEventType, String lookupType) {
		Timestamp  timestamp = null;
		
		if(changeEventType.getCategory().equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS)) {
		} else if(changeEventType.getCategory().equalsIgnoreCase(LifeChangeEventConstant.OTHER)){
			timestamp = new Timestamp(new TSDate().getTime());
		} else {
			if(changeEventType.getType()==LifeChangeEventConstant.ONE){
				timestamp = getCoverageStartDateForType1(eventDate);
			} else if(changeEventType.getType()==LifeChangeEventConstant.TWO){
				timestamp = getCoverageStartDateForType2();
			} else if(changeEventType.getType()==LifeChangeEventConstant.THREE){
				timestamp = getCoverageStartDateForType3();
			}
		}
		
		return timestamp;
	}
	
	private Timestamp getCoverageStartDateForRenewalYear(int renewalYear) {
		Calendar coverageCal = TSCalendar.getInstance();
	    coverageCal.set(Calendar.DAY_OF_MONTH, 1);
	    coverageCal.set(Calendar.YEAR, renewalYear);
		coverageCal.set(Calendar.MONTH, 0);
		
		return new Timestamp(coverageCal.getTime().getTime());
	}

	private Timestamp getCoverageEndDate(Date eventDate, SepEvents changeEventType, String lookupType) {
		if(changeEventType.getCategory().equalsIgnoreCase(LifeChangeEventConstant.DEMOGRAPHICS)) {
			return null;
		}
		
		Calendar calendar = TSCalendar.getInstance();  
        calendar.set(Calendar.DAY_OF_MONTH, 31);  
        calendar.set(Calendar.MONTH, 11);
                        
        Date lastDayOfMonth = calendar.getTime(); 


	    return new Timestamp(lastDayOfMonth.getTime());
	}
	
	private Timestamp getCoverageStartDateForType1(Date eventDate) {
		return new Timestamp(eventDate.getTime());
	}
	
	private Timestamp getCoverageStartDateForType2() {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(new TSDate());
	
		cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		 return new Timestamp(cal.getTime().getTime());
	}
	
	private Timestamp getCoverageStartDateForType3() {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(new TSDate());
		int month = 1;
		int year = 1;
		Calendar coverageCal = TSCalendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		
		if(dayOfMonth < 15) {
			 month = cal.get(Calendar.MONTH) + 1;
		} else {
			 month = cal.get(Calendar.MONTH) + 2;
		}
		
		year = cal.get(Calendar.YEAR);
		 
		if(month > 12) {
			month = 1;
			year = year + 1;
		}
		
	    coverageCal.set(Calendar.DAY_OF_MONTH, 1);
	    coverageCal.set(Calendar.YEAR, year);
		coverageCal.set(Calendar.MONTH, month);
	    
	    return new Timestamp(coverageCal.getTime().getTime());
	}
		
/*	private boolean isPersonAlreadyEnrolled(long oldSsapApplicationId,long personId) throws Exception{
		
		LifeChangeEventDTO lifeChangeEventDTO = null;
		String responseDTO = null;
		boolean isPersonAlreadyEnrolled = false;
		try{
			lifeChangeEventDTO = new LifeChangeEventDTO();
			lifeChangeEventDTO.setOldApplicationId(oldSsapApplicationId);
			lifeChangeEventDTO.setUserName("exadmin@ghix.com");
			responseDTO = ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO, 
					SsapEndPoints.LCE_GET_ENROLLED_APPLICANTS, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
			lifeChangeEventDTO = (LifeChangeEventDTO) GhixUtils.getXStreamStaxObject().fromXML(responseDTO);
			if(lifeChangeEventDTO != null && lifeChangeEventDTO.getPersonIds() != null && lifeChangeEventDTO.getPersonIds().contains(personId)){
				isPersonAlreadyEnrolled = true;
			}
		}catch(Exception ex){
			//LOGGER.error("Exception occurred while retriving enrollment details for old application.", ex);
			throw ex;
		}
		return isPersonAlreadyEnrolled;
	}*/
	
	private Map<Long, List<SepEvents>> initMultipleEventData(Long personId, SepEvents changeEventType, Map<Long, List<SepEvents>> eventsMap){
		List<SepEvents> events = eventsMap.get(personId);
		if(null == events){
			events = new ArrayList<SepEvents>();
		}
		events.add(changeEventType);
		eventsMap.put(personId, events);
		return eventsMap;
	}
			
	private Integer checkEventPrecedence(List<SepEvents> events) throws Exception{
		SepEvents highestPrecedenceEvent = null;
		int prevPrecedenceOrder = 0;
		int nextPrecedenceOrder;
		
		for(SepEvents event: events){
			if(event.getEventPrecedenceOrder()==null){
				nextPrecedenceOrder = 0;
			} else {
				nextPrecedenceOrder = event.getEventPrecedenceOrder();
			}
			
			if(prevPrecedenceOrder==0){
				prevPrecedenceOrder = nextPrecedenceOrder;
				highestPrecedenceEvent = event;
			} else if(nextPrecedenceOrder < prevPrecedenceOrder){
				prevPrecedenceOrder = nextPrecedenceOrder;
				highestPrecedenceEvent = event;
			}
		}
		
		return highestPrecedenceEvent.getId();
	}
	
	private SsapApplicationEvent updateEnrollmentStartAndEndDates(SsapApplication currentApplication, SsapApplicationEvent ssapApplicationEventResource, Map<String, Boolean> demographicFlagMap, Map<Long, List<String>> demographicEventsData, List<Boolean> homeAddressChangeDemoFlags, Date demographicEventDate, Date dobEventDate,List<SsapApplicantEvent> ssapApplicantEvents) throws Exception{
		//long applicationEventId = currentapplicationEvent.get().get(0);
		
		//LOGGER.info("Updating Enrollment Dates : Starts");
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(currentApplication.getCoverageYear()) ;
		try {
			//SsapApplicationEvent ssapApplicationEventResource = ssapApplicationEventLink.get(0);
			
			if(!demographicFlagMap.get(LifeChangeEventConstant.IS_OTHER_CHANGE) && 
					(demographicFlagMap.get(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE))){
				SsapApplicationEvent parentApplicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(currentApplication.getParentSsapApplicationId().longValue());
				
				if(parentApplicationEvent != null && parentApplicationEvent.getEnrollmentStartDate()!=null){
					ssapApplicationEventResource.setEnrollmentStartDate(parentApplicationEvent.getEnrollmentStartDate());
				} else {
					if(demographicFlagMap.get(LifeChangeEventConstant.IS_DOB_CHANGE)){
						ssapApplicationEventResource.setEnrollmentStartDate(getMinDate(dobEventDate));
					} else {
						ssapApplicationEventResource.setEnrollmentStartDate(getMinDate(demographicEventDate));
					}
				}
				
				if(parentApplicationEvent != null && parentApplicationEvent.getEnrollmentEndDate()!=null){
					ssapApplicationEventResource.setEnrollmentEndDate(checkForPastDate(parentApplicationEvent.getEnrollmentEndDate()));
				} else {
					Date eventDate;
					if(demographicFlagMap.get(LifeChangeEventConstant.IS_DOB_CHANGE)){
						eventDate = dobEventDate;
					} else {
						eventDate = demographicEventDate;
					}
					
					ssapApplicationEventResource.setEnrollmentEndDate(checkForPastDate(getMaxDate(getEnrollmentEndDate(eventDate,isNextYearsOE))));
				}
			} else {
			
					Timestamp minEnrollmentStartDate = getMinEnrollmentStartDate(ssapApplicantEvents);
					if(minEnrollmentStartDate!=null){
						minEnrollmentStartDate = getMinDate(minEnrollmentStartDate);
						ssapApplicationEventResource.setEnrollmentStartDate(minEnrollmentStartDate);
					}
				
					Timestamp maxEnrollmentEndDate =getMaxEnrollmentEndDate(ssapApplicantEvents);
					if(maxEnrollmentEndDate!=null){
						maxEnrollmentEndDate = getMaxDate(maxEnrollmentEndDate);
						ssapApplicationEventResource.setEnrollmentEndDate(checkForPastDate(maxEnrollmentEndDate));
					}
			}
			
			if(checkEnrollmentDatesForNull(ssapApplicationEventResource)){
				Date reportingDate = new TSDate();
				ssapApplicationEventResource.setEnrollmentStartDate(getMinDate(reportingDate));
				ssapApplicationEventResource.setEnrollmentEndDate(getMaxDate(getEnrollmentEndDate(reportingDate,isNextYearsOE)));
			}
			
			return ssapApplicationEventResource;
		} catch(Exception exception){
			throw exception;
		}
	}
	
	private Timestamp getMinEnrollmentStartDate(List<SsapApplicantEvent> ssapApplicantEvents) throws Exception{
		Timestamp prevDate = null;
		for(SsapApplicantEvent ssapApplicantEvent: ssapApplicantEvents){
			if(ssapApplicantEvent.getEnrollmentStartDate() !=null && (prevDate==null || ssapApplicantEvent.getEnrollmentStartDate().getTime() < prevDate.getTime()) ){
				prevDate =ssapApplicantEvent.getEnrollmentStartDate();
			} 
		}
		return prevDate;
	}
	
	private Timestamp getMaxEnrollmentEndDate(List<SsapApplicantEvent> ssapApplicantEvents) throws Exception{
		Timestamp nextDate= null;
		for(SsapApplicantEvent ssapApplicantEvent: ssapApplicantEvents){
			if(ssapApplicantEvent.getEnrollmentEndDate()!=null  && (nextDate == null || ssapApplicantEvent.getEnrollmentEndDate().getTime() > nextDate.getTime()  ) ){
				nextDate =ssapApplicantEvent.getEnrollmentEndDate();
			}
		}
		return nextDate;
	}
	
	private boolean checkEnrollmentDatesForNull(SsapApplicationEvent ssapApplicationEventResource){
		if(ssapApplicationEventResource.getEnrollmentStartDate()!=null && ssapApplicationEventResource.getEnrollmentEndDate()!=null){
			return false;
		} 
		
		return true;
	}
	
	private Date getEnrollmentEndDate(Date eventDate, boolean isNextYearsOE){
		if (isNextYearsOE)
		{
			try {
				return new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
						parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) + " 23:59:59").getTime());
			} catch (ParseException e) {
				LOGGER.error("Error while calculating the SsapApplicantEvent Enrollment End Date",e);
				return null;
			}
		}
		else {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(eventDate);
			cal.add(Calendar.DATE, getEnrollmentGracePeriod());
			
			return cal.getTime();
		}
	}
	
	private Timestamp getMinDate(Date date){
		Calendar calendar = TSCalendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 00);
	    calendar.set(Calendar.MINUTE, 00);
	    calendar.set(Calendar.SECOND, 00);
	    calendar.set(Calendar.MILLISECOND, 000);
		
		return new Timestamp(calendar.getTime().getTime());
	}
	
	private Timestamp getMaxDate(Date date){
		Calendar calendar = TSCalendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	    
	    return new Timestamp(calendar.getTime().getTime());
	}

	@Override
	public List<SsapApplicantEvent> getApplicantEventByApplicationCaseNumber(String caseNumber) {
		return ssapApplicantEventRepository.getApplicantEventByApplicationCaseNumber(caseNumber);
	}
	
	@Override
	public List<SsapApplicantEvent> getSsapApplicantEvents(long ssapApplicationId,String sepEventName) {
		
		return ssapApplicantEventRepository.findByApplicationIdAndSepEvent(ssapApplicationId,sepEventName);
	}
	
	
	/**
	 * 
	 * 
	 * Utitlity method to store a comment in COMMENTS table
	 * 
	 * @param ssapApplicantEventList List of ssap applicants for whom comment to be stored.
	 * @param target_name 
	 * @param reasonText
	 * @param commenterName
	 *  @param commentedBy
	 * @return success/failure
	 */
	@Override
	public String saveAddTaxDepReason(List<SsapApplicantEvent> ssapApplicantEventList, String targetName, String reasonText,String commenterName,long commentedBy)throws Exception{
		String targetId=null;
		List<Comment> comments = null;
		CommentTarget commentTarget =null;
		
		try{
			
			if(!ssapApplicantEventList.isEmpty()){
				for(SsapApplicantEvent ssapApplicantEvent:ssapApplicantEventList){
					targetId=ssapApplicantEvent.getId()+"";
					commentTarget =  new CommentTarget();
					commentTarget.setTargetId(new Long(targetId));
					commentTarget.setTargetName(TargetName.valueOf(targetName));
					
					
					comments = new ArrayList<Comment>();
					Comment comment = new Comment();
					comment.setComentedBy((int)commentedBy);
					comment.setCommenterName(commenterName);
					comment.setComment(reasonText);
					comment.setCommentTarget(commentTarget);
					
					comments.add(comment);
					commentTarget.setComments(comments);
					commentTargetService.saveCommentTarget(commentTarget);
				}
			}
			
			return SUCCESS;
		}catch(Exception e){
			LOGGER.error("Error while saving add tax dependent reason",e);
			throw e;
		}
	}

	
}
