/**
 * 
 */
package com.getinsured.hix.account.web;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;

/**
 * @author Biswakesh
 *
 */
@Component
public class Loginemailchangeconfirm extends NotificationAgent {
	public static final String RESET_CONTROLLER_URL = "account/user/resetemail/";
	
	
	/**
	 * overrides the implementation of {@link NotificationAgent} agent 
	 * with customized implementation as required for Password Recovery
	 * @return {@link Map}
	 */

	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		AccountUser userObj = (AccountUser) notificationContext.get("USER_OBJ");
		String randomToken = (String) notificationContext.get("RANDOM_TOKEN");
		String newEmailAddress =  (String) notificationContext.get("NEW_EMAIL_ADDRESS");		
		
		try {
			randomToken = URLEncoder.encode(randomToken, "UTF-8");
		}catch(Exception ex) {
			
		}
		Map<String,String> bean = new HashMap<String, String>();
		String resetUrl = new StringBuilder(GhixEndPoints.GHIXWEB_SERVICE_URL).append(RESET_CONTROLLER_URL).append(randomToken).toString();
		bean.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		bean.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		long currentTime = TSCalendar.getInstance().getTimeInMillis();
		String notificationDate = sdf.format(currentTime);
		bean.put("notificationDate", notificationDate);
		bean.put("userFirstName", userObj.getFirstName());
		bean.put("userLastName", userObj.getLastName());
		bean.put("prevEmail", userObj.getUserName());
		bean.put("newEmail", newEmailAddress);
		bean.put("id",Integer.toString(userObj.getId()));
		bean.put("EmailConfirmationURL", resetUrl );

		return bean; 
	}

	@Override
	public Map<String, String> getEmailData(
			Map<String, Object> notificationContext) {
		String newEmailAddress =  (String) notificationContext.get("NEW_EMAIL_ADDRESS");
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", newEmailAddress);

		String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		data.put("Subject", "Changes to your secure account at "+exchangeName);
		return data;
	}
	
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}

}
