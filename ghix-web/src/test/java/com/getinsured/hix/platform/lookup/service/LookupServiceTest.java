/**
 * File 'LookupServiceTest' for testing Lookup service methods.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jul 22, 2013
 *
 */
package com.getinsured.hix.platform.lookup.service;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.getinsured.hix.BaseTest;

/**
 * Class 'LookupServiceTest' for testing Lookup service methods.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jul 22, 2013
 *
 */
public class LookupServiceTest extends BaseTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(LookupServiceTest.class);
	
	/**
	 * Default constructor.
	 */
	public LookupServiceTest() {
	}

	@Autowired
	private LookupService lookupService;
	
	/**
	 * Method 'testPopulateLanguageNames' to unit test populateLanguageNames method.
	 *
	 */
	@Test
	public void testPopulateLanguageNames() {
		LOGGER.info("Beginning executing testPopulateLanguageNames method");

        List<String> languageList = lookupService.populateLanguageNames("");
        
        Assert.notNull(languageList, "Languages for Agent/Brokers was not populated.");
        Assert.notEmpty(languageList, "No languages found for Agent/Brokers.");
	}
}
