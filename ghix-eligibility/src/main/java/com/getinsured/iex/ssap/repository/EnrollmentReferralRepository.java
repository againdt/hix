package com.getinsured.iex.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.enrollment.Enrollment;

public interface EnrollmentReferralRepository extends JpaRepository<Enrollment, Integer> {

	@Query("SELECT en.id, en.enrollmentStatusLkp.lookupValueCode, en.planId, en.CMSPlanID, en.benefitEffectiveDate, en.benefitEndDate, en.createdOn,en.aptcAmt "
	        + " FROM Enrollment as en WHERE en.ssapApplicationid= :ssapApplicationid AND en.insuranceTypeLkp.lookupValueCode = 'HLT'")
	List<Object[]> getEnrollmentDataBySsapApplicationId(@Param("ssapApplicationid") Long ssapApplicationid);
	
	Enrollment findById(Integer id);
}