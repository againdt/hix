package com.getinsured.iex.hub.nmec.wrapper;

import java.text.ParseException;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.nmec.niem.niem.niem_core._2.InsuranceType;
import com.getinsured.iex.hub.nmec.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public class InsuranceTypeWrapper implements JSONAware{

	private String insuranceEffectiveDate;
    private String insuranceEndDate;
    private static final Logger LOGGER = LoggerFactory.getLogger(InsuranceTypeWrapper.class);
    
    @SuppressWarnings("unchecked")
	public InsuranceTypeWrapper( InsuranceType insuranceType)
    {
    	if(insuranceType != null)
    	{
    		Date  date = null;
    		
    		if(insuranceType.getInsuranceEffectiveDate() != null)
    		{
    			date = ((JAXBElement<Date>) insuranceType.getInsuranceEffectiveDate().getDateRepresentation()).getValue();
    		}
			
    		if(date != null)
			{
				try 
				{
					XMLGregorianCalendar cal = date.getValue();
					if(cal != null){
    					insuranceEffectiveDate = PlatformServiceUtil.xmlDateToString(cal,"yyyy-MM-dd");
    					
    					if(insuranceType.getInsuranceEndDate() != null)
    					{
    						date = ((JAXBElement<Date>) insuranceType.getInsuranceEndDate().getDateRepresentation()).getValue();
    						cal = date.getValue();
    						insuranceEndDate = PlatformServiceUtil.xmlDateToString(cal,"yyyy-MM-dd");
    					}
					}
					
				} catch (IllegalArgumentException e) {
					LOGGER.info(e.getStackTrace().toString());
				} catch (DatatypeConfigurationException e) {
					LOGGER.info(e.getStackTrace().toString());
				} catch (ParseException e) {
					LOGGER.info(e.getStackTrace().toString());
				}
			}
    	}
    }
    
    @Override
    @SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("InsuranceEffectiveDate", this.insuranceEffectiveDate);
		obj.put("InsuranceEndDate", this.insuranceEndDate);
		return obj.toJSONString();
	}
}
