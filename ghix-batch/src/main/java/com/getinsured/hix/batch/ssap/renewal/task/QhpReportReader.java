package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.ssap.renewal.service.QhpReportService;
import com.getinsured.hix.batch.ssap.renewal.util.QhpReportPartitionerParams;

/**
 * QHP Report Reader class is used to get read Enrollment IDs from Partitioner.
 * 
 * @since June 14, 2019
 */
public class QhpReportReader implements ItemReader<Integer> {

	private static final Logger LOGGER = LoggerFactory.getLogger(QhpReportReader.class);
	private List<Integer> readerEnrollmentIdList;
	private int partition;
	private int loopCount;
	private int startIndex;
	private int endIndex;

	private QhpReportService qhpReportService;
	private QhpReportPartitionerParams qhpReportPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			partition = executionContext.getInt("partition");
			startIndex = executionContext.getInt("startIndex");
			endIndex = executionContext.getInt("endIndex");
			LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);

			if (null != qhpReportPartitionerParams && CollectionUtils.isNotEmpty(qhpReportPartitionerParams.getEnrollmentIdList())) {

				if (startIndex < qhpReportPartitionerParams.getEnrollmentIdList().size()
						&& endIndex <= qhpReportPartitionerParams.getEnrollmentIdList().size()) {
					readerEnrollmentIdList = new CopyOnWriteArrayList<Integer>(qhpReportPartitionerParams.getEnrollmentIdList().subList(startIndex, endIndex));
				}
			}
			
		}
	}

	@Override
	public Integer read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		boolean readerFlag = false;
		Integer readerEnrollmentId = null;

		if (readerEnrollmentIdList != null && !readerEnrollmentIdList.isEmpty()) {

			if (loopCount < readerEnrollmentIdList.size()) {
				loopCount++;
				readerFlag = true;
				readerEnrollmentId = readerEnrollmentIdList.get(loopCount - 1);
			}
		}

		if (!readerFlag) {
			LOGGER.warn("Unable to find Enrollment IDs.");
		}
		return readerEnrollmentId;
	}

	public QhpReportService getQhpReportService() {
		return qhpReportService;
	}

	public void setQhpReportService(QhpReportService qhpReportService) {
		this.qhpReportService = qhpReportService;
	}

	public QhpReportPartitionerParams getQhpReportPartitionerParams() {
		return qhpReportPartitionerParams;
	}

	public void setQhpReportPartitionerParams(QhpReportPartitionerParams qhpReportPartitionerParams) {
		this.qhpReportPartitionerParams = qhpReportPartitionerParams;
	}
}
