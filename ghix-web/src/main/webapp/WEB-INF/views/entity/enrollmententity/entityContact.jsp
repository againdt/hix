<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
  <link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<div class="gutter10">
<%-- <div class="row-fluid">
	<div class="page-header">
		<c:choose>
			<c:when test="${loggedUser == 'entityAdmin' && enrollmentEntity.registrationStatus =='Pending'}">
				<h1><spring:message code="label.entity.contactInformation"/></h1>
			</c:when>
			<c:otherwise>
				<h1><spring:message code="label.entity.step"/> 4: <spring:message code="label.entity.contactInformation"/></h1>
			</c:otherwise> 
		</c:choose>
	</div>
</div> --%>

      <div class="row-fluid margin20-t">    
        <jsp:include page="../leftNavigationMenu.jsp" />
    <!-- #sidebar ENDS -->
			<!-- .span3 -->
			<!-- #sidebar ENDS -->
			<div class="span9" id="rightpanel">
				<div id="section">
				<div class="header">
					<h4><spring:message code="label.entity.step"/> 4: <spring:message code="label.entity.contactInformation"/></h4>
          		</div>
          		<div class="gutter10">
          		<p><spring:message code="label.entity.tellUsAboutTheContactPeopleInYourOrganization"/></p>
                
				<form class="form-horizontal gutter10" id="frmentityContact" name="frmentityContact" action=<c:url value="/entity/enrollmententity/entitycontactinfo"/>	method="POST">
				<df:csrfToken/>
					<fieldset>
					<input type="hidden" name="entityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>">
					<h4>
					     <spring:message code="label.entity.primaryContact"/>
					</h4>
					<div class="control-group">
						<label for="priContactName" class="required control-label"	for="priContactName"><spring:message code="label.entity.name"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="priContactName" id="priContactName" class="input-xlarge" size="30" value="${fn:escapeXml(enrollmentEntity.priContactName)}" />
							<!-- <input type="hidden" name="priContactName" id="priContactName" /> -->
							<div id="priContactName_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="priContactEmailAddress" class="required control-label"><spring:message code="label.entity.email"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<!-- end of label -->
						<div class="controls">
							<input type="email" placeholder="<spring:message code="label.entity.entCompany@email.com"/>" name="priContactEmailAddress" id="priContactEmailAddress"
								value="${enrollmentEntity.priContactEmailAddress}" class="xlarge"	size="30" />
							<div id="priContactEmailAddress_error" class="help-inline"></div>
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->

					<div class="control-group phone-group">
						<%-- <legend for="priContactPrimaryPhoneNumber1" class="required control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></legend>
						<label for="priContactPrimaryPhoneNumber2" class="aria-hidden control-label">Primary Phone Number Second 3 digits</label>
						<label for="priContactPrimaryPhoneNumber3" class="aria-hidden control-label">Primary Phone Number Second 3 digits</label>
						<label for="priContactPrimaryPhoneNumber" class="aria-hidden hidden">&nbsp;</label> --%>
						<!-- <label for="phone2" class="required hidden">Primary Phone Number</label>
                    <label for="phone3" class="required hidden">Primary Phone Number</label> -->
                    	<label for="priContactPrimaryPhoneNumber1" class="control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber"/><span class="aria-hidden"> <spring:message code="label.entity.first3digits"/></span><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                    	<label for="priContactPrimaryPhoneNumber2" class="aria-hidden control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber2"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                    	<label for="priContactPrimaryPhoneNumber3" class="aria-hidden control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber3"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="priContactPrimaryPhoneNumber1" id="priContactPrimaryPhoneNumber1"	value="${priContactPrimaryPhoneNumber1}" maxlength="3" 	placeholder="xxx" class="area-code input-mini" /> 
							<input	type="text" name="priContactPrimaryPhoneNumber2" id="priContactPrimaryPhoneNumber2"	value="${priContactPrimaryPhoneNumber2}" maxlength="3"	placeholder="xxx" class="input-mini"  /> 
							<input type="text" name="priContactPrimaryPhoneNumber3" id="priContactPrimaryPhoneNumber3" value="${priContactPrimaryPhoneNumber3}" maxlength="4"	placeholder="xxxx" class="input-small" /> 
							<input type="hidden" name="priContactPrimaryPhoneNumber" id="priContactPrimaryPhoneNumber" value="" />
							<div id="priContactPrimaryPhoneNumber1_error"></div>
							<div id="priContactPrimaryPhoneNumber3_error"></div>
						</div>
					</div>

					<div class="control-group phone-group">
						<%-- <label for="priContactSecondaryPhoneNumber1" class="control-label"><spring:message code="label.entity.enterSecondaryPhoneNumber"/><img id="secReq" src="<c:url value="/resources/img/requiredAsterisk.png"/>"  alt="Required!" /></label>
						<label for="priContactSecondaryPhoneNumber2" class="hide control-label">Secondary Phone Number Second 3 digits</label>
						<label for="priContactSecondaryPhoneNumber3" class="hide control-label">Secondary Phone Number Last 4 digits</label>
						<label for="priContactSecondaryPhoneNumber" class="hide hidden">&nbsp;</label> --%>
						<!-- <label for="phone2" class="required hidden">Secondary Phone Number</label>
                 		<label for="phone3" class="required hidden">Secondary Phone Number</label> -->
						<label for="priContactSecondaryPhoneNumber1" class="control-label"><spring:message code="label.entity.enterSecondaryPhoneNumber"/><img id="secReq" src="<c:url value="/resources/img/requiredAsterisk.png"/>"  alt="Required!" /> <span class="aria-hidden"><spring:message code="label.entity.first3digits"/></span></label>
						<label for="priContactSecondaryPhoneNumber2" class="aria-hidden control-label"><spring:message code="label.entity.enterSecondaryPhoneNumber2"/></label>
						<label for="priContactSecondaryPhoneNumber3" class="aria-hidden control-label"><spring:message code="label.entity.enterSecondaryPhoneNumber3"/></label>			
						<div class="controls">
							<input type="text" name="priContactSecondaryPhoneNumber1"  id="priContactSecondaryPhoneNumber1" value="${priContactSecondaryPhoneNumber1}" maxlength="3" placeholder="xxx" class="area-code input-mini"/> 
								<input	type="text" name="priContactSecondaryPhoneNumber2" id="priContactSecondaryPhoneNumber2" value="${priContactSecondaryPhoneNumber2}" maxlength="3" placeholder="xxx" class="input-mini" /> 
								<input type="text" name="priContactSecondaryPhoneNumber3" id="priContactSecondaryPhoneNumber3" value="${priContactSecondaryPhoneNumber3}" maxlength="4" placeholder="xxxx" class="input-small" /> 
								<input type="hidden" name="priContactSecondaryPhoneNumber" id="priContactSecondaryPhoneNumber" value="" />
							<div id="priContactSecondaryPhoneNumber1_error"></div>
							<div id="priContactSecondaryPhoneNumber3_error"></div>
						</div>
					</div>
					</fieldset>
					
					<div class="control-group">
					<fieldset>
                <legend class="control-label"><spring:message code="label.entity.howWouldthisPersonLiketobeContacted"/></legend>
                <div class="controls">
                  <label class="radio" for="email">                    
                    <c:choose>
						<c:when test="${enrollmentEntity.priCommunicationPreference  == 'email'}">
							<input type="radio" name="priCommunicationPreference" id="email" value="email" checked='checked'/>
						</c:when>
						<c:otherwise>
							 <input type="radio" name="priCommunicationPreference" id="email" value="email" checked="checked"/>
						</c:otherwise>
					</c:choose>          
                    <spring:message code="label.entity.email"/>
                  </label>
                  <label class="radio " for="primaryphone">
                  	<c:choose>
						<c:when test="${enrollmentEntity.priCommunicationPreference  == 'Primary Phone'}">
							<input type="radio" name="priCommunicationPreference" id="primaryphone" value="Primary Phone" checked='checked'/>
						</c:when>
						<c:otherwise>
							<input type="radio" name="priCommunicationPreference" id="primaryphone" value="Primary Phone"/>
						</c:otherwise>
					</c:choose>                          
                    <spring:message code="label.entity.selectPrimaryPhone"/> 
                  </label>
                  <label class="radio " for="secondaryphone">                    
                    <c:choose>
						<c:when test="${enrollmentEntity.priCommunicationPreference  == 'Secondary Phone'}">
							<input type="radio" name="priCommunicationPreference" id="secondaryphone" value="Secondary Phone" checked='checked'/>
						</c:when>
						<c:otherwise>
							<input type="radio" name="priCommunicationPreference" id="secondaryphone" value="Secondary Phone"/>
						</c:otherwise>
					</c:choose>     
                    <spring:message code="label.entity.selectSecondaryPhone"/>
                  </label>
                  <label class="radio " for="mail">                   
                    <c:choose>
						<c:when test="${enrollmentEntity.priCommunicationPreference  == 'mail'}">
							<input type="radio" name="priCommunicationPreference" id="mail" value="mail" checked='checked'/>
						</c:when>
						<c:otherwise>
							 <input type="radio" name="priCommunicationPreference" id="mail" value="mail"/>
						</c:otherwise>
					</c:choose>
                    <spring:message code="label.entity.mail"/>
                  </label>
                  <div id="communicationPreference_error"></div>
                </div><!-- .controls -->
                </fieldset>
              </div><!-- .control-group -->
					<fieldset>
					<h4>
						<spring:message code="label.entity.financialContact"/>
					</h4>
					<div class="control-group">
						<label for="finContactName" class="required control-label"
							id="finContactName"><spring:message code="label.entity.name"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="finContactName" id="finContactName"
								class="input-xlarge" size="30" value="${fn:escapeXml(enrollmentEntity.finContactName)}" />
							<!-- <input type="hidden" name="finContactName" id="finContactName" /> -->
							<div id="finContactName_error"></div>
						</div>
					</div>


					<div class="control-group">
						<label for="finEmailAddress" class="required control-label"><spring:message code="label.entity.email"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<!-- end of label -->
						<div class="controls">
							<input type="email" placeholder="<spring:message code="label.entity.entCompany@email.com"/>" name="finEmailAddress" id="finEmailAddress" value="${enrollmentEntity.finEmailAddress}" class="xlarge" size="30" />
							<div id="finEmailAddress_error" class="help-inline"></div>
						</div>
						<!-- end of controls-->
					</div>
					<!-- end of control-group -->

					<div class="control-group phone-group">
						<label for="finPrimaryPhoneNumber1" class="required control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber"/><span class="aria-hidden"><spring:message code="label.entity.first3digits"/></span><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<label for="finPrimaryPhoneNumber2" class="hide control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber2"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<label for="finPrimaryPhoneNumber3" class="hide control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber3"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<label for="finPrimaryPhoneNumber" class="hide control-label">&nbsp;</label>
						<!--  <label for="phone2" class="required hidden">Primary Phone Number</label>
                    <label for="phone3" class="required hidden">Primary Phone Number</label> -->
						<div class="controls">
							<input type="text" name="finPrimaryPhoneNumber1" id="finPrimaryPhoneNumber1" value="${finPrimaryPhoneNumber1}" maxlength="3" placeholder="xxx" class="area-code input-mini" />
							<input type="text" name="finPrimaryPhoneNumber2" id="finPrimaryPhoneNumber2" value="${finPrimaryPhoneNumber2}" maxlength="3" placeholder="xxx" class="input-mini" /> 
							<input type="text" name="finPrimaryPhoneNumber3" id="finPrimaryPhoneNumber3" value="${finPrimaryPhoneNumber3}" maxlength="4" placeholder="xxxx" class="input-small" /> 
							<input type="hidden" name="finPrimaryPhoneNumber" id="finPrimaryPhoneNumber" value="" />
							<div id="finPrimaryPhoneNumber1_error"></div>
							<div id="finPrimaryPhoneNumber3_error"></div>
						</div>
					</div>

					<div class="control-group phone-group">
						<label for="finFaxNumber1" class="control-label"><spring:message code="label.entity.enterSecondaryPhoneNumber"/><img id="faxReq" src="<c:url value="/resources/img/requiredAsterisk.png"/>"  alt="Required!" /><span class="aria-hidden"><spring:message code="label.entity.first3digits"/></span></label>
						
						<!-- <label for="finFaxNumber1" class="aria-hidden control-label">Secondary Phone Number First 3 digits</label> -->
						<label for="finFaxNumber2" class="aria-hidden control-label"><spring:message code="label.entity.enterSecondaryPhoneNumber2"/></label>
						<label for="finFaxNumber3" class="aria-hidden control-label"><spring:message code="label.entity.enterSecondaryPhoneNumber3"/></label>
						<label for="finFaxNumber" class="hide hidden">&nbsp;</label>
                   <!--  <label for="finFaxNumber" class="required hidden">Fax Number</label> -->
						<div class="controls">
							<input type="text" name="finFaxNumber1" id="finFaxNumber1" value="${finFaxNumber1}" maxlength="3" placeholder="xxx" class="area-code input-mini" /> 								
								<input type="text" name="finFaxNumber2" id="finFaxNumber2" value="${finFaxNumber2}" maxlength="3" placeholder="xxx" class="input-mini" /> 
								<input type="text" name="finFaxNumber3" id="finFaxNumber3" value="${finFaxNumber3}" maxlength="4" placeholder="xxxx" class="input-small" /> 
								<input type="hidden" name="finFaxNumber" id="finFaxNumber" value="" />
							<div id="finFaxNumber1_error"></div>
							<div id="finFaxNumber3_error"></div>
						</div>
					</div>

</fieldset>
			<div class="control-group">
			<fieldset>
                <legend class="control-label" for="finCommunicationPreference"><spring:message code="label.entity.howWouldthisPersonLiketobeContacted"/></legend>
                <div class="controls">
                  <label class="radio" for="finemail">                    
                    <c:choose>
						<c:when test="${enrollmentEntity.finCommunicationPreference  == 'email'}">
							<input type="radio" name="finCommunicationPreference" id="finemail" value="email" checked='checked'/>
						</c:when>
						<c:otherwise>
							 <input type="radio" name="finCommunicationPreference" id="finemail" value="email" checked='checked' />
						</c:otherwise>
					</c:choose>          
                   <spring:message code="label.entity.email"/>
                  </label>
                  <label class="radio" for="finprimaryphone">
                  	<c:choose>
						<c:when test="${enrollmentEntity.finCommunicationPreference  == 'Primary Phone'}">
							<input type="radio" name="finCommunicationPreference" id="finprimaryphone" value="Primary Phone" checked='checked'/>
						</c:when>
						<c:otherwise>
							<input type="radio" name="finCommunicationPreference" id="finprimaryphone" value="Primary Phone"/>
						</c:otherwise>
					</c:choose>                          
                    <spring:message code="label.entity.selectPrimaryPhone"/>  
                  </label>
                  <label class="radio" for="finsecondaryphone">                    
                    <c:choose>
						<c:when test="${enrollmentEntity.finCommunicationPreference  == 'Secondary Phone'}">
							<input type="radio" name="finCommunicationPreference" id="finsecondaryphone" value="Secondary Phone" checked='checked'/>
						</c:when>
						<c:otherwise>
							<input type="radio" name="finCommunicationPreference" id="finsecondaryphone" value="Secondary Phone"/>
						</c:otherwise>
					</c:choose>     
                     <spring:message code="label.entity.selectSecondaryPhone"/>
                  </label>
                  <label class="radio" for="finmail">                   
                    <c:choose>
						<c:when test="${enrollmentEntity.finCommunicationPreference  == 'mail'}">
							<input type="radio" name="finCommunicationPreference" id="finmail" value="mail" checked='checked'/>
						</c:when>
						<c:otherwise>
							 <input type="radio" name="finCommunicationPreference" id="finmail" value="mail"/>
						</c:otherwise>
					</c:choose>
                    <spring:message code="label.entity.mail"/> 
                  </label>
                  <div id="communicationPreference_error"></div>
                </div><!-- .controls -->
                </fieldset>
              </div><!-- .control-group -->
              
             <%--  <div class="control-group">  
		     	<table class="table table-border-none">
					<tr>		
						<c:choose>
							<c:when test="${isEdit  == false}">
								<div class="form-actions">
									 <td><input class="btn" type="button" value="<spring:message code="label.entity.back"/>" onclick="window.location.href='/hix/entity/enrollmententity/subsite'"></td>
									 <td><input type="submit" name="SaveEntityInfo" id="SaveEntityInfo" value="<spring:message code="label.entity.next"/>" class="btn btn-primary  offset8" /></td>
								</div>
							</c:when>
							<c:otherwise>
								 <div class="form-actions">
									 <td><input class="btn" type="button" value="<spring:message code="label.entity.cancel"/>" onClick="window.location.href='/hix/entity/enrollmententity/viewentitycontactinfo'"></td>
									 <td><input type="submit" name="SaveEntityInfo" id="SaveEntityInfo" value="<spring:message code="label.entity.save"/>" class="btn btn-primary  offset8" /></td>
								</div>
							</c:otherwise>
						</c:choose>		                                       	                           
					 </tr>	
			    </table>         
        	 </div> --%>
        	 
        	 <div class="control-group">
        	 	<div class="clear">
        	 		<c:choose>
						<c:when test="${isEdit  == false}">
							<div class="form-actions">
								 <input class="btn back-btn" type="button" value="<spring:message code="label.entity.back"/>" onclick="window.location.href='/hix/entity/enrollmententity/subsite'">
								 <input type="submit" name="SaveEntityInfo" id="SaveEntityInfo" value="<spring:message code="label.entity.next"/>" class="btn btn-primary" />
							</div>
						</c:when>
						<c:otherwise>
							 <div class="form-actions">
							 <c:set var="enrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/> </c:set>
								 <input class="btn cancel-btn" type="button" value="<spring:message code="label.entity.cancel"/>" onClick="window.location.href='/hix/entity/enrollmententity/viewentitycontactinfo?entityId=${enrollmentEntityId}'">
								 <input type="submit" name="SaveEntityInfo" id="SaveEntityInfo" value="<spring:message code="label.entity.save"/>" class="btn btn-primary" />
							</div>
						</c:otherwise>
					</c:choose>	
				</div> 
        	 </div>
				
<%-- 					<c:choose> --%>
<%-- 						<c:when test="${isEdit  == false}"> --%>
<!-- 							<div class="form-actions"> -->
<!-- 								<input class="btn btn-primary" type="button" value="Back" onclick="window.location.href='/hix/entity/enrollmententity/subsite'">&nbsp; &nbsp;  -->
<!-- 								<input type="submit" name="SaveEntityInfo" id="SaveEntityInfo" value="Next" class="btn btn-primary" /> -->
<!-- 							</div> -->
<%-- 						</c:when> --%>
<%-- 						<c:otherwise> --%>
<!-- 							 <div class="form-actions"> -->
<!-- 								<input class="btn btn-primary" type="button" value="Cancel" onClick="window.location.href='/hix/entity/enrollmententity/viewentitycontactinfo'">&nbsp; &nbsp;  -->
<!-- 								<input type="submit" name="SaveEntityInfo" id="SaveEntityInfo" value="Save" class="btn btn-primary" /> -->
<!-- 							</div> -->
<%-- 						</c:otherwise> --%>
<%-- 					</c:choose> --%>
				</form>

			
			<input type="hidden" name="emailCheck" id="emailCheck" value="" />
			</div>
			</div>
			</div>
	</div>
</div>
<script type="text/javascript">
	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
            }
		}
	}
$("#entityContacts").removeClass("link");
$("#entityContacts").addClass("active");

var validator = $("#frmentityContact").validate(
					{	onkeyup : false,
						onclick : false,
						rules : {
							priContactName : { required : true},
							finContactName : { required : true},
							priContactEmailAddress : { required : true,email: true},
							finEmailAddress : { required : true,email: true},
							priCommunicationPreference : { required : true},
							finCommunicationPreference : { required : true},
							priContactSecondaryPhoneNumber1 : {numberStartsWithZeroCheck: false, digits:true},
							priContactSecondaryPhoneNumber2:{digits:true},
							priContactSecondaryPhoneNumber3 : { priContactSecondaryPhoneNumberCheck : true,priConSecPhone:true,digits:true},
							priContactPrimaryPhoneNumber1 : {numberStartsWithZeroCheck: false,digits:true},
							priContactPrimaryPhoneNumber2:{digits:true},
							priContactPrimaryPhoneNumber3 : { priContactPrimaryPhoneNumberCheck : true,digits:true},
							finPrimaryPhoneNumber1 : {numberStartsWithZeroCheck: false,digits:true},
							finPrimaryPhoneNumber2:{digits:true},
							finPrimaryPhoneNumber3 : {finPrimaryPhoneNumberCheck : true,digits:true},
							finFaxNumber1 :{numberStartsWithZeroCheck: false,digits:true},
							finFaxNumber2:{digits:true},
							finFaxNumber3 : {finFaxNumberCheck : true,finConSecPhone:true,digits:true}	
							
						},
						groups: {
							priContactSecondaryPhoneNumber: "priContactSecondaryPhoneNumber1 priContactSecondaryPhoneNumber2 priContactSecondaryPhoneNumber3",
							priContactPrimaryPhoneNumber:"priContactPrimaryPhoneNumber1 priContactPrimaryPhoneNumber2 priContactPrimaryPhoneNumber3",
							finPrimaryPhoneNumber:"finPrimaryPhoneNumber1 finPrimaryPhoneNumber2 finPrimaryPhoneNumber3",
							finFaxNumber:"finFaxNumber1 finFaxNumber2 finFaxNumber3"
							
						},
						messages : {
							priContactPrimaryPhoneNumber3 : {priContactPrimaryPhoneNumberCheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>",
								priContactPrimaryPhoneNumberCheck:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidPhoneNumber' javaScriptEscape='true'/></span>",
								digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidPhoneNumber' javaScriptEscape='true'/></span>"},
							priContactPrimaryPhoneNumber1 :{numberStartsWithZeroCheck :"<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>",
								digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidPhoneNumber' javaScriptEscape='true'/></span>"},
							priContactPrimaryPhoneNumber2:{digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidPhoneNumber' javaScriptEscape='true'/></span>"},
							priContactSecondaryPhoneNumber1:{digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidSecondaryPhoneNumber' javaScriptEscape='true'/></span>"},
							priContactSecondaryPhoneNumber2:{digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidSecondaryPhoneNumber' javaScriptEscape='true'/></span>"},
							priContactSecondaryPhoneNumber3:{priContactSecondaryPhoneNumberCheck :  "<span> <em class='excl'>!</em><spring:message code='label.valuidatePleaseEnterSecondaryPhoneNumber' javaScriptEscape='true'/></span>",
								priContactSecondaryPhoneNumberCheck:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidSecondaryPhoneNumber' javaScriptEscape='true'/></span>",
								priConSecPhone:"<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNo' javaScriptEscape='true'/></span>",
								digits:"<span><em class='excl'>!</em><spring:message  code='label.validatePleaseEnterValidSecondaryPhoneNumber' javaScriptEscape='true'/></span>"},
							finContactName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFinContactName' javaScriptEscape='true'/></span>"},
							priContactEmailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePriContactEmailAddress' javaScriptEscape='true'/></span>",
							    	email : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidEmail' javaScriptEscape='true'/></span>"},
							finEmailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFinEmailAddress' javaScriptEscape='true'/></span>",
							 	    	email : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidEmail' javaScriptEscape='true'/></span>"},
							priCommunicationPreference: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePriCommunicationPreference' javaScriptEscape='true'/></span>"
							},
							finCommunicationPreference: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFinCommunicationPreference' javaScriptEscape='true'/></span>"
							},
							priContactName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePriContactName' javaScriptEscape='true'/></span>"
							},							
							finFaxNumber3 : {finFaxNumberCheck :  "<span> <em class='excl'>!</em><spring:message code='label.valuidatePleaseEnterSecondaryPhoneNumber' javaScriptEscape='true'/></span>",
								finFaxNumberCheck:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidFinancialSecondaryPhoneNumber' javaScriptEscape='true'/></span>",
								finConSecPhone:"<span> <em class='excl'>!</em><spring:message code='label.validateEnterFinancialSecondaryPhoneNumber' javaScriptEscape='true'/></span>",
								digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidFinancialSecondaryPhoneNumber' javaScriptEscape='true'/></span>"},
							finFaxNumber2:{digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidFinancialSecondaryPhoneNumber' javaScriptEscape='true'/></span>"},
							finFaxNumber1:{digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidFinancialSecondaryPhoneNumber' javaScriptEscape='true'/></span>"},
									
							finPrimaryPhoneNumber1 : {numberStartsWithZeroCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>",
								digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidFinancialPrimaryPhoneNumber' javaScriptEscape='true'/></span>"},
							finPrimaryPhoneNumber2:{digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidFinancialPrimaryPhoneNumber' javaScriptEscape='true'/></span>"},
							finPrimaryPhoneNumber3 : {finPrimaryPhoneNumberCheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>",
								finPrimaryPhoneNumberCheck:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidFinancialPrimaryPhoneNumber' javaScriptEscape='true'/></span>",
								digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidFinancialPrimaryPhoneNumber' javaScriptEscape='true'/></span>"
							}
							
						/* email : {required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>",
								emailCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailCheck' javaScriptEscape='true'/></span>"
							} */
						},

						errorClass: "error",
						errorPlacement: function(error, element) {
							var elementId = element.attr('id');
							if(elementId == "priContactSecondaryPhoneNumber1" || elementId == "priContactSecondaryPhoneNumber2" || elementId == "priContactSecondaryPhoneNumber3" )
							{
								error.appendTo( $("#" + "priContactSecondaryPhoneNumber3" + "_error"));
							}
							if(elementId == "priContactPrimaryPhoneNumber1" || elementId == "priContactPrimaryPhoneNumber2" || elementId == "priContactPrimaryPhoneNumber3" )
							{
								error.appendTo( $("#" + "priContactPrimaryPhoneNumber3" + "_error"));
							}
							if(elementId == "finFaxNumber2" || elementId == "finFaxNumber2" || elementId == "finFaxNumber3" )
							{
								error.appendTo( $("#" + "finFaxNumber3" + "_error"));
							}
							if(elementId == "finPrimaryPhoneNumber1" || elementId == "finPrimaryPhoneNumber2" || elementId == "finPrimaryPhoneNumber3" )
							{
								error.appendTo( $("#" + "finPrimaryPhoneNumber3" + "_error"));
							}
							else{	
								error.appendTo( $("#" + elementId + "_error"));
							}
							$("#" + elementId + "_error").attr('class','error help-inline');
						} 
					});




	jQuery.validator.addMethod("priConSecPhone",function(value, element, param) {
		var tmpOne = $("#priContactSecondaryPhoneNumber1").val().trim();
		var tmpTwo = $("#priContactSecondaryPhoneNumber2").val().trim();
		var tmpThree = $("#priContactSecondaryPhoneNumber3").val().trim();
		
		if($('input[name=priCommunicationPreference]:radio:checked').val()=='Secondary Phone'){

			if((tmpOne.length == 0) && (tmpTwo.length == 0)  && (tmpThree.length == 0)) {
				return false;
			}
			if((tmpOne.length < 3 ) || (tmpTwo.length < 3 )  || (tmpThree.length < 4 ) || (isNaN(tmpOne)) || (isNaN(tmpTwo)) || (isNaN(tmpThree)) || (tmpOne == '000'))
			{
				return false;
			} else {
				$("#priContactSecondaryPhoneNumber").val(tmpOne + tmpTwo + tmpThree);		
				return true;
			}
			
		}else{
			if((tmpOne.length == 0) && (tmpTwo.length == 0)  && (tmpThree.length == 0)) {
				return true;
			}
			if((tmpOne.length < 3 ) || (tmpTwo.length < 3 )  || (tmpThree.length < 4 ) || (isNaN(tmpOne)) || (isNaN(tmpTwo)) || (isNaN(tmpThree)) || (tmpOne == '000'))
			{
				return false;
			} else {
				$("#priContactSecondaryPhoneNumber").val(tmpOne + tmpTwo + tmpThree);		
				return true;
			}
		}
	
	});
	
	
	
    
    $('input[type=radio][name=priCommunicationPreference]').change(function () {
        if ($(this).val() == 'Secondary Phone') {
            $('#secReq').show();
        } else {
        	 $('#secReq').hide();
        }
        
    });
	
    if ($('input:radio[name=priCommunicationPreference]:checked').val() == 'Secondary Phone' ) {
        $('#secReq').show();
    } else {
   	 $('#secReq').hide();
    }


jQuery.validator.addMethod("priContactPrimaryPhoneNumberCheck",	function(value, element, param) {
						ie8Trim();
						priContactPrimaryPhoneNumber1 = $("#priContactPrimaryPhoneNumber1").val().trim();
						priContactPrimaryPhoneNumber2 = $("#priContactPrimaryPhoneNumber2").val().trim();
						priContactPrimaryPhoneNumber3 = $("#priContactPrimaryPhoneNumber3").val().trim();
						var a=(/^[0-9]*$/.test(priContactPrimaryPhoneNumber1)&& /^[0-9]*$/.test(priContactPrimaryPhoneNumber2)&&/^[0-9]*$/.test(priContactPrimaryPhoneNumber3));
						
						var firstChar = priContactPrimaryPhoneNumber1.charAt(0);
						if(firstChar == 0) {
								return false;
						}
						if(!a){
							return false;
						}
						if ((priContactPrimaryPhoneNumber1 == "" || priContactPrimaryPhoneNumber2 == "" || priContactPrimaryPhoneNumber3 == "")	|| (isNaN(priContactPrimaryPhoneNumber1)) || (priContactPrimaryPhoneNumber1.length < 3) || (isNaN(priContactPrimaryPhoneNumber2)) || (priContactPrimaryPhoneNumber2.length < 3) || (isNaN(priContactPrimaryPhoneNumber3)) || (priContactPrimaryPhoneNumber3.length < 4)) 
						{
							return false;
						} else {
							$("#priContactPrimaryPhoneNumber").val(priContactPrimaryPhoneNumber1+priContactPrimaryPhoneNumber2+priContactPrimaryPhoneNumber3);
							return true;
						}
					});
					
jQuery.validator.addMethod("numberStartsWithZeroCheck", function(value, element, param) {
	ie8Trim();
	// This is added to check if user has not entered anything into the textbox
	if((value.length == 0)) {
		return true;
	}
	
	// If user has entered anything then test, if entered value is valid
	var firstChar = value.charAt(0);
	if(firstChar == 0) {
			return false;
	} else{
	    return true;
	}
});
jQuery.validator.addMethod("priContactSecondaryPhoneNumberCheck",function(value, element, param) {
						ie8Trim();					
						priContactSecondaryPhoneNumber1 = $("#priContactSecondaryPhoneNumber1").val().trim();
						priContactSecondaryPhoneNumber2 = $("#priContactSecondaryPhoneNumber2").val().trim();
						priContactSecondaryPhoneNumber3 = $("#priContactSecondaryPhoneNumber3").val().trim();
                        
						var a=(/^[0-9]*$/.test(priContactSecondaryPhoneNumber1)&& /^[0-9]*$/.test(priContactSecondaryPhoneNumber2)&& /^[0-9]*$/.test(priContactSecondaryPhoneNumber3));
						var firstChar = priContactSecondaryPhoneNumber1.charAt(0);
						if((priContactSecondaryPhoneNumber1.length == 0) && (priContactSecondaryPhoneNumber2.length == 0)  && (priContactSecondaryPhoneNumber3.length == 0)) {
							return true;
						}
						if(firstChar == 0) {
								return false;
						}
						if(!a){
							return false;
						}
						
						if((priContactSecondaryPhoneNumber1.length < 3 ) || (priContactSecondaryPhoneNumber2.length < 3 )  || (priContactSecondaryPhoneNumber3.length < 4 ) || (isNaN(priContactSecondaryPhoneNumber1)) || (isNaN(priContactSecondaryPhoneNumber2)) || (isNaN(priContactSecondaryPhoneNumber3)) || (priContactSecondaryPhoneNumber1 == '000'))
						{
							return false;
						} else {
							$("#priContactSecondaryPhoneNumber").val(priContactSecondaryPhoneNumber1 + priContactSecondaryPhoneNumber2 + priContactSecondaryPhoneNumber3);		
							return true;
						}
					});
jQuery.validator.addMethod(	"finPrimaryPhoneNumberCheck",function(value, element, param) {
						finPrimaryPhoneNumber1 = $("#finPrimaryPhoneNumber1").val().trim();
						finPrimaryPhoneNumber2 = $("#finPrimaryPhoneNumber2").val().trim();
						finPrimaryPhoneNumber3 = $("#finPrimaryPhoneNumber3").val().trim();
						var a=(/^[0-9]*$/.test(finPrimaryPhoneNumber1)&& /^[0-9]*$/.test(finPrimaryPhoneNumber2)&& /^[0-9]*$/.test(finPrimaryPhoneNumber3));
						var firstChar = finPrimaryPhoneNumber1.charAt(0);
						if(firstChar == 0) {
								return false;
						}
						if(!a){
							return false;
						}
						if ((finPrimaryPhoneNumber1 == "" || finPrimaryPhoneNumber2 == "" || finPrimaryPhoneNumber3 == "") || (isNaN(finPrimaryPhoneNumber1)) || (finPrimaryPhoneNumber1.length < 3) || (isNaN(finPrimaryPhoneNumber2))	|| (finPrimaryPhoneNumber2.length < 3)	|| (isNaN(finPrimaryPhoneNumber3))|| (finPrimaryPhoneNumber3.length < 4)) 
						{
							return false;
						} else {
							$("#finPrimaryPhoneNumber").val(finPrimaryPhoneNumber1+ finPrimaryPhoneNumber2+ finPrimaryPhoneNumber3);
							return true;
						}
					});
jQuery.validator.addMethod("finFaxNumberCheck", function(value, element,param) {
		ie8Trim();
		finFaxNumber1 = $("#finFaxNumber1").val().trim();
		finFaxNumber2 = $("#finFaxNumber2").val().trim();
		finFaxNumber3 = $("#finFaxNumber3").val().trim();
		var a=(/^[0-9]*$/.test(finFaxNumber1) && /^[0-9]*$/.test(finFaxNumber2) && /^[0-9]*$/.test(finFaxNumber3));
		var firstChar = finFaxNumber1.charAt(0);
		if((finFaxNumber1.length == 0) && (finFaxNumber2.length == 0)  && (finFaxNumber3.length == 0)) {
			return true;
		}
		if(firstChar == 0) {
				return false;
		}
		if(!a){
			return false;
		}
		
		
		if((finFaxNumber1.length < 3 ) || (finFaxNumber2.length < 3 )  || (finFaxNumber3.length < 4 ) || (isNaN(finFaxNumber1)) || (isNaN(finFaxNumber2)) || (isNaN(finFaxNumber3)) || (finFaxNumber1 == '000'))
		{
			return false;
		} else {
			$("#finFaxNumber").val(finFaxNumber1 + finFaxNumber2 + finFaxNumber3);		
			return true;
		}
	});
jQuery.validator.addMethod("finConSecPhone",function(value, element, param) {
	var tmpOneFin = $("#finFaxNumber1").val().trim();
	var tmpTwoFin = $("#finFaxNumber2").val().trim();
	var tmpThreeFin = $("#finFaxNumber3").val().trim();
	
	if($('input[name=finCommunicationPreference]:radio:checked').val()=='Secondary Phone'){

		if((tmpOneFin.length == 0) && (tmpTwoFin.length == 0)  && (tmpThreeFin.length == 0)) {
			return false;
		}
		if((tmpOneFin.length < 3 ) || (tmpTwoFin.length < 3 )  || (tmpThreeFin.length < 4 ) || (isNaN(tmpOneFin)) || (isNaN(tmpTwoFin)) || (isNaN(tmpThreeFin)) || (tmpOneFin == '000'))
		{
			return false;
		} else {
			$("#finFaxNumber").val(tmpOneFin + tmpTwoFin + tmpThreeFin);		
			return true;
		}
		
	}else{
		if((tmpOneFin.length == 0) && (tmpTwoFin.length == 0)  && (tmpThreeFin.length == 0)) {
			return true;
		}
		if((tmpOneFin.length < 3 ) || (tmpTwoFin.length < 3 )  || (tmpThreeFin.length < 4 ) || (isNaN(tmpOneFin)) || (isNaN(tmpTwoFin)) || (isNaN(tmpThreeFin)) || (tmpOneFin == '000'))
		{
			return false;
		} else {
			$("#finFaxNumber").val(tmpOneFin + tmpTwoFin + tmpThreeFin);		
			return true;
		}
	}

});

 $('input[type=radio][name=finCommunicationPreference]').change(function () {
    if ($(this).val() == 'Secondary Phone') {
        $('#faxReq').show();
    } else {
    	 $('#faxReq').hide();
    }
    
});

if ($('input:radio[name=finCommunicationPreference]:checked').val() == 'Secondary Phone' ) {
    $('#faxReq').show();
} else {
	 $('#faxReq').hide();
}

// 	function shiftbox(element, nextelement) {
// 		maxlength = parseInt(element.getAttribute('maxlength'));
// 		if (element.value.length == maxlength) {
// 			nextelement = document.getElementById(nextelement);
// 			nextelement.focus();
// 		}
// 	}
	$(function() {
		$("#SaveEntityInfo").click(function(e) {
			/*to check the text for address line 2 for IE browsers
			$("input[id^=address2_]").each(function(){
			                if($(this).val() === "Apt, Suite, Unit, Bldg, Floor, etc"){
			                                $(this).val("");
			                }
			});                           */
			if ($("#frmentityContact").validate().form()) {
				$("#frmentityContact").submit();
			}
		});
	});
</script>


