package com.getinsured.hix.consumer.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Consumer Portal Enrollment Service for fetching Enrollments for a CMRHouseHold
 * @author bhatia_s
 *
 */
public interface ConsumerPortalEnrollementService {

	List< Map<String,String>> getHouseholdPlanMetaData(Integer cmrHouseHoldId) throws GIException;
	
}
