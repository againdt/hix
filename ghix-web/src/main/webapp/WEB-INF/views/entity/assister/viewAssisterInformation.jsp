<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link href="<gi:cdnurl value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/phoneUtils.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/preserveImgAspectRatio.js" />"></script> 

<style type="text/css">
.table tr th{
               background: none !important;
               height: 14px;
               font-weight: normal;
               color: #5A5A5A;
       }
</style>

<div class="gutter10-lr">
	<div class="row-fluid">	
	  <c:set var="encAssisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
	    <c:set var="encAssisterEntityId" ><encryptor:enc value="${assister.entity.id}" isurl="true"/> </c:set>
			 <div class="row-fluid">
				<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
				<!-- <div id="sidebar" class="span3 hide">
					<div class="header hide"></div>			
				</div> -->
			<div class="span9" id="rightpanel">
	<div class="header">
	<h4 class="pull-left"><spring:message code="label.assister.assisterInformation"/></h4>
			<c:choose>
				<c:when test="${CA_STATE_CODE}">
					<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus=='Incomplete'}">
						<a class="btn btn-small pull-right" href="<c:url value="/entity/assister/editassisterinformation?assisterId=${encAssisterId}"/>"><spring:message  code="label.edit"/> <span class="aria-hidden"><spring:message code="label.assister.assisterInformation"/></span></a>
					</c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${loggedUser!='entityAdmin' && enrollmentEntity.registrationStatus!='Pending' }">
					      <a class="btn btn-small pull-right" href="<c:url value="/entity/assister/editassisterinformation?assisterId=${encAssisterId}"/>"><spring:message  code="label.edit"/> <span class="aria-hidden"><spring:message code="label.assister.assisterInformation"/></span></a>
		             </c:if>
				</c:otherwise>
			</c:choose> 
			
             <c:if test="${loggedUser=='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete' }">
             	<a class="btn btn-small pull-right" href="<c:url value="/entity/assister/editassisterinformation?assisterId=${encAssisterId}&entityId=${encAssisterEntityId}"/>"><spring:message  code="label.edit"/> <span class="aria-hidden"><spring:message code="label.assister.assisterInformation"/></span></a>
            </c:if>
	</div>
             	<form class="form-horizontal" id="frmviewAssisterInformation" name="frmviewAssisterInformation" action="viewassisterinformation" method="POST">
             	<df:csrfToken/>
             	<input type="hidden" id="entityId" name="entityId" value="<encryptor:enc value="${assister.entity.id}"/>">

				<div class="row-fluid">					
					<div class="span">
						<table class="table table-border-none">
							
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.name"/></td>
									<td><strong>${assister.firstName} ${assister.lastName}</strong></td>
								</tr>
								
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.emailAddress"/></td>
									<td><strong>${assister.emailAddress}</strong></td>
								</tr>
								
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.PhoneNumber"/></td>
									<td>
										<strong> 
										<c:choose>
											<c:when test="${assister.primaryPhoneNumber!=null}">
												<c:set var="primaryPhoneNumber" value="${assister.primaryPhoneNumber}"></c:set>
												<c:set var="formattedPrimaryPhoneNumber" value="(${fn:substring(primaryPhoneNumber,0,3)})${fn:substring(primaryPhoneNumber,3,6)}-${fn:substring(primaryPhoneNumber,6,10)} "></c:set>
												${formattedPrimaryPhoneNumber}
											</c:when>
											<c:otherwise>
												${assister.primaryPhoneNumber}
											</c:otherwise>											
										</c:choose>																				
										</strong>
									</td>
								</tr>
								
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.secondaryPhoneNumber"/></td>
									<td>
									<strong> 
										<c:choose>
											<c:when test="${assister.secondaryPhoneNumber!=null && not empty assister.secondaryPhoneNumber}">
												<c:set var="secondaryPhoneNumber" value="${assister.secondaryPhoneNumber}"></c:set>
												<c:set var="formattedSecondaryPhoneNumber" value="(${fn:substring(secondaryPhoneNumber,0,3)})${fn:substring(secondaryPhoneNumber,3,6)}-${fn:substring(secondaryPhoneNumber,6,10)} "></c:set>
												${formattedSecondaryPhoneNumber}
											</c:when>
											<c:otherwise>
												${assister.secondaryPhoneNumber}
											</c:otherwise>
										</c:choose>
									</strong>
									</td>
								</tr>
	
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.preferredMethodofCommunication"/></td>
									<td><strong>${assister.communicationPreference}</strong></td>
								</tr>
								
								<c:choose>
						 						<c:when test="${ showPostalMailOption == 'true'  }">
						 								<tr>
															<td class="span4 txt-right" scope="row"><spring:message code="label.assister.receiveNotices" /> </td> 
															<td>
																<strong>
																	<c:if test="${assister.postalMail!=null && assister.postalMail == 'Y' }" > <spring:message code="label.assister.Yes" /></c:if>
																	<c:if test="${assister.postalMail == null || assister.postalMail == 'N' ||  assister.postalMail == 'n'}" > <spring:message code="label.assister.No" /> </c:if>
																</strong>
															</td>
														</tr>
						 						</c:when>
						 		</c:choose>
								
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.isCertifiedAssister"/></td>
									<c:if test="${assister.certificationNumber==null || assister.certificationNumber == ''}">
										<td><strong><spring:message code="label.assister.No"/></strong></td>
									</c:if>
									<c:if test="${assister.certificationNumber!=null && assister.certificationNumber != ''}">
										<td><strong><spring:message code="label.assister.Yes"/></strong></td>
									</c:if>
								</tr>
								
								<c:if test="${assister.certificationNumber!=null && assister.certificationNumber != ''}">
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.assisterCertification"/> #</td>
									<td><strong><fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/></strong></td>
								</c:if>
	
								<c:if test="${assister.primaryAssisterSite!=null}">
									<tr>
										<td class="span4 txt-right" scope="row"><spring:message code="label.assister.priAssisterSite"/></td>
										<td><strong>${assister.primaryAssisterSite.siteLocationName}</strong></td>
									</tr>
								</c:if>	
								<c:if test="${assister.secondaryAssisterSite!=null}">
									<tr>
										<td class="span4 txt-right" scope="row"><spring:message code="label.assister.secAssisterSite"/></td>
										<td><strong>${assister.secondaryAssisterSite.siteLocationName}</strong></td>
									</tr>
								</c:if>
						</table>
					</div>
				</div>
				
				<div class="gutter10">
				<div class="header">
				<h4><spring:message code="label.assister.mailingAddress"/></h4>
				</div>
					<table class="table table-border-none">
							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.assister.streetAddress"/></td>
								<td><strong>${assister.mailingLocation.address1}</strong></td>
							</tr>
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.suite"/></td>
									<td><strong>${assister.mailingLocation.address2}</strong></td>
								</tr>
							
							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.assister.city"/></td>
								<td><strong>${assister.mailingLocation.city}</strong></td>
							</tr>
							
							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.assister.state"/></td>
								<td><strong>${assister.mailingLocation.state}</strong></td>
							</tr>

							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.assister.zipCode"/></td>
								<td><strong>${assister.mailingLocation.zip}</strong></td>
							</tr>
					</table>
				</div>
				
				<div class="gutter10">
				<div class="header">
					<h4><spring:message code="label.assister.profileInformation"/></h4>
				</div>
					
					<table class="table table-border-none">
						<tr>
							<td class="span4 txt-right" scope="row"><spring:message code="label.assister.education"/></td>
							<td><strong>${assister.education}</strong></td>
						</tr>
						<tr>
							<td class="span4 txt-right" scope="row"><spring:message code="label.assister.spokenLanguagesSupport"/></td>
							<td><strong>${assisterLanguages.spokenLanguages}</strong></td>
						</tr>
						<tr>
							<td class="span4 txt-right" scope="row"><spring:message code="label.assister.writtenLanguagesSupport"/></td>
							<td><strong>${assisterLanguages.writtenLanguages}</strong></td>
						</tr>
						<tr>
							<td class="span4 txt-right"><spring:message code="label.assister.photo"/></td><td><spring:url value="/entity/assisteradmin/photo/${encAssisterId}"
											var="photoUrl" /> <img width="100px" height="100px" src="${photoUrl}" alt="Profile image of Counselor" class="profilephoto" />
									</td>
						</tr>
					</table>
				</div>
         </form>
      </div>
   </div>
</div> 
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#assisters").removeClass("link");
	$("#assisters").addClass("active");
});
</script>
