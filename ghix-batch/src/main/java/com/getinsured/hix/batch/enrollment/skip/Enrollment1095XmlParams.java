/**
 * 
 */
package com.getinsured.hix.batch.enrollment.skip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.util.Enrollment1095XmlWrapper;

/**
 * @author negi_s
 *
 */
@Component("enrollment1095XmlParams")
public class Enrollment1095XmlParams {
	List<Integer> enrollmentIdList=null;
	Map<String,String> skippedEnrollmentMap=null;
	private boolean isFreshRun;
	Map<Integer, Map<String,Object>> partitionListMap = null;
	Map<Integer, Enrollment1095XmlWrapper> modifiedPartitonListMap = null;
	private Integer totalRecordCount;
	private Integer totalUpdateCount;
	private Integer totalFailedCount;
	private Integer totalOverWrittenCount;
	
	public static final String MAP_DATA_KEY = "data";
	public static final String MAP_RUN_KEY = "isFreshRun";

	public Enrollment1095XmlParams() {
		this.enrollmentIdList = new ArrayList<Integer>();
		this.skippedEnrollmentMap = new HashMap<String,String>();
		this.partitionListMap = new HashMap<Integer, Map<String,Object>>();
		this.modifiedPartitonListMap = new HashMap<Integer, Enrollment1095XmlWrapper>();
		this.isFreshRun = false;
		this.totalRecordCount = 0;
		this.totalUpdateCount = 0;
		this.totalFailedCount = 0;
		this.totalOverWrittenCount = 0;
	}

	public synchronized void putToPartitionListMap(Integer key, Map<String,Object> partitionList){
		partitionListMap.put(key, partitionList);
	}
	
	public Map<Integer, Map<String,Object>> getPartitionListMap(){
		return partitionListMap;
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> getPartitionFromId(Integer partitionId){
		Map<String,Object> partitionMap = partitionListMap.get(partitionId);
		return (List<Integer>) partitionMap.get(MAP_DATA_KEY);
	}
	
	public boolean getRunInfoFromId(Integer partitionId){
		Map<String,Object> partitionMap = partitionListMap.get(partitionId);
		return (boolean) partitionMap.get(MAP_RUN_KEY);
	}
	
	public synchronized void putToModifiedPartitionListMap(Integer key, Enrollment1095XmlWrapper enrollment1095XmlWrapper){
		modifiedPartitonListMap.put(key, enrollment1095XmlWrapper);
	}
	
	public Map<Integer, Enrollment1095XmlWrapper> getModifiedPartitionListMap(){
		return modifiedPartitonListMap;
	}
	
	public List<Integer> getModifiedPartitionFromId(Integer partitionId){
		if(modifiedPartitonListMap.get(partitionId) != null){
			return modifiedPartitonListMap.get(partitionId).getPartitionList();
		}
		return new ArrayList<Integer>(); 
	}
	
	public boolean getModifiedRunInfoFromId(Integer partitionId){
		if(modifiedPartitonListMap.get(partitionId) != null){
			return modifiedPartitonListMap.get(partitionId).isFreshRun();
		}
		return true;
	}
	
	public Enrollment1095XmlWrapper getWrapperFromId(Integer partitionId){
		return modifiedPartitonListMap.get(partitionId)	;
	}
	
	public boolean isFreshRun() {
		return isFreshRun;
	}

	public synchronized void setFreshRun(boolean isFreshRun) {
		this.isFreshRun = isFreshRun;
	}

	public Integer getTotalRecordCount() {
		return totalRecordCount;
	}

	public synchronized void setTotalRecordCount(Integer totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public Integer getTotalUpdateCount() {
		return totalUpdateCount;
	}

	public synchronized void incrementTotalUpdateCount(Integer incrementBy) {
		this.totalUpdateCount += incrementBy;
	}

	public Integer getTotalOverWrittenCount() {
		return totalOverWrittenCount;
	}

	public synchronized void incrementTotalOverWrittenCount(Integer incrementBy) {
		this.totalOverWrittenCount += incrementBy;
	}
	
	public synchronized void addAllToEnrollmentIdList(List<Integer> enrollmentIdList){
		this.enrollmentIdList.addAll(enrollmentIdList);
	}
	
	public synchronized void addToEnrollmentIdList(Integer enrollmentId){
		this.enrollmentIdList.add(enrollmentId);
	}
	
	public List<Integer> getEnrollmentIdList(){
		return enrollmentIdList;
	}
	public synchronized void clearEnrollmentIdList(){
		this.enrollmentIdList.clear();
	}

	public synchronized void putToSkippedEnrollmentMap(String key, String value){
		skippedEnrollmentMap.put(key, value);
	}
	public synchronized void putAllToSkippedEnrollmentMap(Map<String,String> failedHouseholdMap){
		this.skippedEnrollmentMap.putAll(failedHouseholdMap);
	}	
	
	public Map<String,String> getSkippedEnrollmentMap(){
		return skippedEnrollmentMap;
	}
	
	public synchronized void clearSkippedEnrollmentMap(){
		this.skippedEnrollmentMap.clear();
	}	
	
	public Integer getTotalFailedCount() {
		return totalFailedCount;
	}
	
	public synchronized void incrementTotalFailedCount(Integer incrementBy) {
		this.totalFailedCount += incrementBy;
	}
	
	public void resetMap(){
		this.partitionListMap = new HashMap<Integer, Map<String,Object>>();
		this.modifiedPartitonListMap = new HashMap<Integer, Enrollment1095XmlWrapper>();
	}
	public synchronized void resetCounters(){
		this.totalRecordCount = 0;
		this.totalUpdateCount = 0;
		this.totalOverWrittenCount = 0;
		this.totalFailedCount = 0;
	}
}
