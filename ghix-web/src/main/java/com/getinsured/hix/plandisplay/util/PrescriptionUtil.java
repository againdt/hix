package com.getinsured.hix.plandisplay.util;

import java.lang.reflect.Type;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.plandisplay.DrugDosageDTO;
import com.getinsured.hix.dto.plandisplay.DrugSearchDTO;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component
public class PrescriptionUtil {
	
	@Autowired private RestTemplate restTemplate;
	@Autowired private Gson platformGson;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PrescriptionUtil.class);

	public List<DrugSearchDTO> drugSearch(String name) {
		List<DrugSearchDTO> result = null;
		try {
			String response = restTemplate.getForObject(GhixEndPoints.PlandisplayEndpoints.SEARCH_DRUG_BY_NAME + "?name="+name, String.class);
			Type type = new TypeToken<List<DrugSearchDTO>>() {}.getType();
			result = platformGson.fromJson(response, type);			
			
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		return result;
	}
	
	public List<DrugDosageDTO> drugSearchById(String drugId) {
		List<DrugDosageDTO> result = null;
		try {
			String response = restTemplate.getForObject(GhixEndPoints.PlandisplayEndpoints.SEARCH_DRUG_BY_ID +"?drugId="+drugId, String.class);
			Type type = new TypeToken<List<DrugDosageDTO>>() {}.getType();
			result = platformGson.fromJson(response, type);			
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		return result;
	}
	
	public String getFairPriceResponse(String ndc) {
		String response = null;
		try {
			if(StringUtils.isNotBlank(ndc)){
				response = restTemplate.getForObject(GhixEndPoints.PlandisplayEndpoints.GET_FAIR_PRICE_OF_DRUG+"/"+ndc, String.class);
			}
		} catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		return response;
	}
	public String getGenericRxCuiResponse(String name, String strength) {
		String response = null;
		try {
			if(StringUtils.isNotBlank(name) && StringUtils.isNotBlank(strength)){
				response = restTemplate.getForObject(GhixEndPoints.PlandisplayEndpoints.GET_GENERIC_RXCUI_OF_DRUG+"?name="+name+"&strength="+strength, String.class);
			}
		}
		catch (RestClientException re) {
				PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.WARN, "Unable to Retrieve Generic Rx Info for drug name: "+name, re, false);
		} 
		catch (Exception e) {
			PlanDisplayUtil.putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
		
		return response;
	}
}
