<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/tablesorter.css" />" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script>
<%@ page isELIgnored="false"%>

<%
Integer pageSizeInt = (Integer)request.getAttribute("pageSize");
String reqURI = (String)request.getAttribute("reqURI");
Integer totalResultCount = (Integer)request.getAttribute("resultSize");
if(totalResultCount == null){
	totalResultCount = 0;
}
Integer currentPage = (Integer)request.getAttribute("currentPage");
int noOfPage = 0;
if(totalResultCount>0){
	if((totalResultCount%pageSizeInt)>0){
		noOfPage = (totalResultCount/pageSizeInt)+1;
	}
	else{
		noOfPage = (totalResultCount/pageSizeInt);
	}
}

%>
<style>
    #sidebar .header h4 { padding-right: 10px !important;}
</style>
<%-- <form class="form-vertical" id="frmmanagebroker" name="frmmanagebroker"
	action="<c:url value="/admin/broker/manage/list" />" method="POST"> --%>
	<!-- start of secondary navbar -->

	<%-- <ul class="nav nav-pills">
				  <li class="active"><a href="<c:url value="/admin/broker/manage"/>">Manage brokers</a></li>
			</ul> --%>
	<!-- end of secondary navbar -->

	<div class="gutter10">
		<div class="l-page-breadcrumb">
			<!--start page-breadcrumb -->
			<div class="row-fluid">
			<!--
				<ul class="page-breadcrumb">
					<li><a href="javascript:history.back()">&lt; <spring:message
								code="label.back" /></a></li>
					<li><a href="<c:url value="/admin/broker/manage"/>"><spring:message
								code="label.brokers" /></a></li>
					<li>Manage</li>
				</ul> -->
				<!--page-breadcrumb ends-->
			</div>
			<!-- end of .row-fluid -->
		</div><!--l-page-breadcrumb ends-->
		<div class="row-fluid">
<!-- 			<div class="span8"> -->
				<h1 id="skip"><spring:message code="label.brkAgents"/>
<%-- 					<font size="4%"><small>${resultSize} Total Agents </small></font> --%>
					<font><small>${resultSize}  <c:choose><c:when test="${resultSize > 1 }"><spring:message code="label.brkTotalAgents"/></c:when><c:otherwise><spring:message code="label.brkTotalAgent"/></c:otherwise></c:choose>  </small>
					<c:if test="${CA_STATE_CODE}">
						<a href="#disclaimerPopup" role="button" data-toggle="modal" style=" font-size: 13px;" class="pull-right"><spring:message code="label.agent.exportList"/></a>
					</c:if>
					</font>
				</h1>
<!-- 			</div> -->
		</div><!-- end of .row-fluid -->
		<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4><span id="rrb"><spring:message code="label.brkRefineResultsBy"/></span><a class="pull-right" href="<c:url value="/admin/broker/manage"/>"><spring:message code="label.brkResetAll"/></a></h4>
			</div>
			<div class="lightgray">
<%-- 				<form method="POST" action="brokers" name="brokersearch" id="brokersearchsearch" class="form-horizontal gutter10" novalidate="novalidate" > --%>
				<form method="POST" action="<c:url value="/admin/broker/manage/list" />" id="brokersearchsearch" name="brokersearchsearch" class="form-vertical gutter10" novalidate="novalidate" aria-describedby="rrb">
					<df:csrfToken/>
					<input type="hidden" value="Active" id="desigStatus" name="desigStatus">
					<input type="hidden" id="sortBy" name="sortBy" >
					<input type="hidden" id="sortOrder" name="sortOrder" >
					<input type="hidden" id="pageNumber" name="pageNumber" value="1">
					<input type="hidden" id="pageNumber" name="pageSize" value="${pageSize + 0}">
					<input type="hidden" id="changeOrder" name="changeOrder" >
					<!-- <input type="hidden" value="brokerss" id="fromModule" name="fromModule"> -->
					<div class="control-group">
						<label for="user.firstName" class="control-label"><spring:message code="label.brkFirstNames"/> </label>
						<div class="controls">
							<input type="text" name="user.firstName" id="user.firstName" value="${sessionScope.brokersearchparam.user.firstName}" class="span12" maxlength="100" />
						</div><!-- end of controls -->
					</div><!-- end of control-group -->

		            <div class="control-group">
		                <label for="user.lastName" class="control-label"><spring:message code="label.brkLastNames"/></label>
		                <div class="controls">
		                    <input type="text" name="user.lastName" id="user.lastName" value="${sessionScope.brokersearchparam.user.lastName}"
		                        class="span12" maxlength="100" />
		                </div><!-- end of controls -->
		            </div><!-- end of control-group -->
		            
		             <div class="control-group">
		                <label for="user.phoneNumber" class="control-label"><spring:message code="label.brkPhoneNumber"/></label>
		                <div class="controls">
		                    <input type="text" name="contactNumber" id="contactNumber" value="${sessionScope.brokersearchparam.contactNumber}"
		                        class="span12" maxlength="10" oninput="forceNumeric(this)" />
		                </div><!-- end of controls -->
		            </div><!-- end of control-group -->
		            
		            <c:if test="${!CA_STATE_CODE}">
			            <div class="control-group">
				                <label for="licenseNumberOrNPN" class="control-label"><spring:message code="label.licenseNumberOrNPN"/></label>
				                <div class="controls">
				                    <input type="text" name="licenseNumber" id="licenseNumber" value="${sessionScope.brokersearchparam.licenseNumber}"
				                        class="span12" size="30" maxlength="10" />
				                </div><!-- end of controls -->
				          </div><!-- end of control-group -->
			         </c:if>

		             <div class="control-group">
		                <label for="companyName" class="control-label"><spring:message code="label.brkCompanyName"/></label>
		                <div class="controls">
		                    <input type="text" name="companyName" id="companyName" value="${sessionScope.brokersearchparam.companyName}"
		                        class="span12" maxlength="100" />
		                </div><!-- end of controls -->
		            </div><!-- end of control-group -->

		            <c:if test="${CA_STATE_CODE}">
		            	<div class="control-group">
			                <label for="licenseNumber" class="control-label"><spring:message code="label.licenseNumber"/></label>
			                <div class="controls">
			                    <input type="text" name="licenseNumber" id="licenseNumber" value="${sessionScope.brokersearchparam.licenseNumber}"
			                        class="span12" size="30" maxlength="10" />
			                </div><!-- end of controls -->
			            </div><!-- end of control-group -->
		            </c:if>

					<div class="control-group">
		                <label for="certificationStatus" class="control-label"><spring:message code="label.brkCertificationStatus"/></label>
						<select name="certificationStatus" id="certificationStatus"  class="span12">
							<option value="" selected="selected"><spring:message code="label.brkSelect"/></option>

							<option value="Pending" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Pending'}"> SELECTED </c:if>><spring:message code="label.brkPending"/></option>
							<option value="Incomplete" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Incomplete'}"> SELECTED </c:if>><spring:message code="label.brkInComplete"/></option>
							<option value="Withdrawn" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Withdrawn'}"> SELECTED </c:if>><spring:message code="label.brkWithdrawn"/></option>
							<option value="Eligible" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Eligible'}"> SELECTED </c:if>><spring:message code="label.brkEligible"/></option>
							<option value="Certified" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Certified'}"> SELECTED </c:if>><spring:message code="label.brkCertified"/></option>
							<option value="Denied" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Denied'}"> SELECTED </c:if>><spring:message code="label.brkDenied"/></option>
							<option value="Terminated-Vested" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Terminated-Vested'}"> SELECTED </c:if>><spring:message code="label.brkTerminatedVested"/></option>
							<option value="Terminated-For-Cause" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Terminated-For-Cause'}"> SELECTED </c:if>><spring:message code="label.brkTerminatedForCause"/></option>
							<option value="Deceased" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Deceased'}"> SELECTED </c:if>><spring:message code="label.brkDeceased"/></option>
							<c:if test="${CA_STATE_CODE}">
							<option value="Suspended" <c:if test="${sessionScope.brokersearchparam.certificationStatus == 'Suspended'}"> SELECTED </c:if>><spring:message code="label.brkSuspended"/></option>
							</c:if>
						</select>
					</div>
		            <div class="txt-center">
		                <input type="submit" value="<spring:message code="label.brkGo"/>" class="btn btn-primary input-small" id="submit" name="submit">
		            </div>
				</form>
			</div><!-- end of .lightgray -->
		</div><!-- end of .span3 -->
		<div id="rightpanel" class="span9">
		<div id="brokerlistdiv">
			<form class="form-vertical" id="frmmanagebroker" name="frmmanagebroker"
	action="<c:url value="/admin/broker/manage/list" />" method="POST">
					<df:csrfToken/>
					<input type="hidden" name="id" id="id" value="">
					<c:choose>
						<c:when test="${fn:length(brokerslist) > 0}">
							<table class="table" id="brokerlist" name="brokerlist">
								<thead>
									<tr class="header">
										<spring:message code="label.brkBrokerName" var="brkName"/>
										<spring:message code="label.brkCompanyName" var="brkBusinessName"/>
										<spring:message code="label.brkContactNumber" var="brkPhoneNumber"/>
										<c:if test="${CA_STATE_CODE}">
											<spring:message code="label.licenseNumber" var="brkLicenseNumber"/>
										</c:if>
										<spring:message code="label.brkSubmittedOn" var="brkSubmittedOn"/>
										<spring:message code="label.brkCertifiedOn" var="brkCertifiedOn"/>
										<spring:message code="label.brkCertificationStatus" var="brkCertificationStatus"/>
										<c:set var="orderBy" value="${sortOrder}" />
										<th class="sortable" scope="col" class="header" style="padding: 0 0 0 8px;"><dl:sort customFunctionName="traverse" title="${brkName}" sortBy="CONTACT_NAME" sortOrder="${orderBy}"></dl:sort></th>

										<c:if test="${CA_STATE_CODE}">
											<th   scope="col"   style="padding: 0 0 0 8px;"><spring:message code="label.role"/></th>
										</c:if>

										<th class="sortable" scope="col" class="header" style="padding: 0;"><dl:sort customFunctionName="traverse" title="${brkBusinessName}" sortBy="companyName" sortOrder="${orderBy}"></dl:sort></th>
										<th class="sortable" scope="col" class="header" style="padding: 0;"><dl:sort customFunctionName="traverse" title="${brkPhoneNumber}" sortBy="contactNumber" sortOrder="${orderBy}"></dl:sort></th>
										<c:if test="${CA_STATE_CODE}">
											<th class="sortable" scope="col" class="header" style="padding: 0;"><dl:sort customFunctionName="traverse" title="${brkLicenseNumber}" sortBy="licenseNumber" sortOrder="${orderBy}"></dl:sort></th>
										</c:if>
										<c:if test="${not CA_STATE_CODE}">
											<th class="sortable" scope="col" class="header" style="padding: 0;"><dl:sort customFunctionName="traverse" title="${brkSubmittedOn}" sortBy="applicationDate" sortOrder="${orderBy}"></dl:sort></th>
										</c:if>

										<th class="sortable" scope="col" class="header" style="padding: 0;"><dl:sort customFunctionName="traverse" title="${brkCertifiedOn}" sortBy="certificationDate" sortOrder="${orderBy}"></dl:sort></th>
										<c:choose>
											<c:when test="${CA_STATE_CODE}">
												<th class="sortable" scope="col" class="header" style="padding: 0;white-space: nowrap;"><dl:sort customFunctionName="traverse" title="${brkCertificationStatus}" sortBy="certificationStatus" sortOrder="${orderBy}"></dl:sort></th>
											</c:when>
											<c:otherwise>
												<th class="sortable" scope="col" class="header" style="padding: 0;white-space: nowrap;"><dl:sort customFunctionName="traverse" title="${brkCertificationStatus}" sortBy="certificationStatus" sortOrder="${orderBy}"></dl:sort></th>
											</c:otherwise>
										</c:choose>
										<th scope="col" class="sortable" style="padding: 0;"><spring:message code="label.brkactions"/></th>
									</tr>
								</thead>
								<c:set var="counter" value="0" />
								<c:forEach items="${brokerslist}" var="broker">
									<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>

									<%-- <c:set var="counter" value="${fn:length(brokerslist)}" />  --%>
									<tr>
										<td class="span2">
												<a
												href="<c:url value="/admin/broker/viewcertificationinformation/${encBrokerId}"/>">
												<c:choose>
													<c:when test="${broker.userfirstName !=null && broker.userlastName != null}">
														${broker.userfirstName}&nbsp;${broker.userlastName}
													</c:when>
													<c:otherwise>
														${broker.firstName}&nbsp;${broker.lastName}
													</c:otherwise>
												</c:choose>
												</a>
										</td>
										<c:if test="${CA_STATE_CODE}">
											<c:choose>
												<c:when test="${broker.userid == null or broker.userid  == 0}">
													<td  varStatus="loop"  class="span2" id="roleRow${loop.index}" encUserId="0"  scope="col"   style="padding: 0 0 0 8px;">-</td>
												</c:when>
												<c:otherwise>
													<c:set var="encUserId" ><encryptor:enc value="${broker.userid}" isurl="true"/> </c:set>
													<td  varStatus="loop" class="span2" id="roleRow${loop.index}" encUserId="${encUserId}"  scope="col"   style="padding: 0 0 0 8px;">-</td>
												</c:otherwise>
											</c:choose>
										</c:if>
										<td class="span2">${broker.companyName}</td>
										<td class="span2">${broker.contactNumber}</td>
										<c:if test="${CA_STATE_CODE}">
											<td class="span2">${broker.licenseNumber}</td>
										</c:if>
										<c:if test="${not CA_STATE_CODE}">
											<td class="span2"><fmt:formatDate value="${broker.applicationDate}"
												pattern="MM-dd-yyyy" /></td>
										 </c:if>

										<td class="span2"><fmt:formatDate value="${broker.certificationDate}"
												pattern="MM-dd-yyyy" /></td>
										<%-- <td><fmt:formatDate value="${broker.deCertificationDate}"
												pattern="MM-dd-yyyy" /></td>  Commented Terminated On Column for HIX-41588 --%>
										<td class="span1"><c:out value="${broker.certificationStatus}" /></td>
										<!-- 	<td><button type="button" class="btn"  onClick="location.href='updatecertificationstatus'"><i class="icon-pencil"></i> Update</button></td> -->
										<td class="span1"><div class="dropdown">
													 <a class="dropdown-toggle" data-toggle="dropdown" href="#" alt="dropdown"><i
													 class="icon-cog"></i><i class="caret"></i><span class="hide">Action</span></a>
													 <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">
													  <li><a href="<c:url value="/admin/broker/editcertificationstatus/${encBrokerId}"/>"><i class="icon-pencil"></i>
													   		<spring:message code='label.edit' /></a></li>
													  </ul>
										</div></td>
									</tr>
									<c:set var="counter" value="${counter+1}" />
								</c:forEach>
							</table>
							<div class="pagination center">
							<c:choose>
								<c:when test="${CA_STATE_CODE}">
									<dl:paginate resultSize="${resultSize + 0}"
									pageSize="${pageSize + 0}" hideTotal="true" showDynamicPageSize="true" />
								</c:when>
								<c:otherwise>
							<dl:paginate resultSize="${resultSize + 0}"
								pageSize="${pageSize + 0}" hideTotal="true" />
								</c:otherwise>
							</c:choose>

						</div>
						</c:when>
						<c:otherwise>
							<div class="alert alert-info"><spring:message code='label.brknomatching' /></div>
						</c:otherwise>
					</c:choose>
				</form>
				<c:if test="${CA_STATE_CODE}">
				<div id="disclaimerPopup" class="modal hide fade" tabindex="-1" data-backdrop="static" modal="true" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
					<form id="exportAgentListAsExcel" name="exportAgentListAsExcel" action="exportAgentList" method="GET" novalidate>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h3><spring:message code='label.agent.exportList.disclaimerTitle' javaScriptEscape='true'/></h3>
						</div>
						<div class="modal-body">
							<p><spring:message  code="label.agent.exportList.disclaimerMesg1"/> ${exchangeName} <spring:message  code="label.agent.exportList.disclaimerMesg2"/></p>
							<p><spring:message  code="label.agent.exportList.disclaimerMesg3"/></p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label.brk.no"/></button>
		  					<input id="submitbtn" type="submit" class="btn btn-primary" value='<spring:message code="label.brk.yes"/>'/>
						</div>
					</form>
				</div>
				</c:if>
			</div>
			</div><!-- end of .span9 -->
		</div><!-- end of .row-fluid -->
		</div> <!-- end of .gutter10-lr -->

	<script>
		$(document).ready(function() {


			$('input[name="check_allActive"]').click(function() {

				  if( $('input[name="check_allActive"]').is(":checked") )
				  {
					  $('input[name="indCheckboxActive"]').each( function() {
						  	$(this).attr("checked",true);
		          			$('input[name="indCheckboxActive"]').prop('checked', true);
					  });

				  } else {
					  $('input[name="indCheckboxActive"]').each( function() {
		          			$('input[name="indCheckboxActive"]').prop('checked', false);
						  	$(this).prop('checked', false);
					  });
				  }
			  });
			$(":checkbox").click(countChecked);

			$('[id^="roleRow"]').each(function( index ) {
				var tdElement= $( this );
				
				if($( this ).attr('encUserId') == "0"){
					 tdElement.html("Agent");
				}else{
					$.get('<c:url value="/admin/broker/role/" />'+$( this ).attr('encUserId')  , function(data, status){
						 if('BROKER' == data){
							 tdElement.html("Agent");
						 }else if('AGENCY_MANAGER' == data){
							 tdElement.html("Agency Manager");
						 }else{
							 tdElement.html(data);
						 }

				    });
				}
				 
			});


		});


		/* $(document).ready(function() {
			 var val, tmp;
				var elements = $("#brokerlist th a");

				if(null != elements && elements.length > 0) {
					for(i=0; i<elements.length; i++) {
						val = elements[i].href;

						if(null != val && -1 != val.indexOf("csrftoken")) {
							tmp = val.substring(val.indexOf("csrftoken"), val.length);
            				tmp = tmp.substring(0,tmp.indexOf("&"));

							val = val.replace(tmp, "");
							val = val.replace("&&", "&");
							elements[i].href = val;
						}
					}
				}
		}); */


		/* $(document).ready(function() {
		    $('table[name="brokerlist"]').tablesorter({
		        // pass the headers argument and assing a object
		        headers: {
		            // assign the secound column (we start counting zero)
		            0: {
		                // disable it by setting the property sorter to false
		                sorter: false
		            },
		            6: {
		                // disable it by setting the property sorter to false
		                sorter: false
		            }
		        }

		    });
		}); */

		//If the Master checkbox is checked or unchchecked, process the following code



		//For each of the broker checkboxes, process the following code when they are changed
		 /* $('input[name="indCheckboxActive"]').bind('change', function() {

			  if ( $('input[name="indCheckboxActive"]').filter(':not(:checked)').length == 0 ) {
			 	$('input[name="check_allActive"]').attr("checked",true);
			  } else {
			 	$('input[name="check_allActive"]').attr("checked",false);
			  }

		  }); */
			$(function() {
				$('.datepick').each(function() {
					$(this).datepicker({
						showOn : "button",
						buttonImage : "../resources/images/calendar.gif",
						buttonImageOnly : true
					});
				});
			});
		  
		  function forceNumeric(input){
			  let value = input.value;
			  let numbers = value.replace(/[^0-9]/g, "");
			  input.value = numbers;
			}

		  function countChecked() {
				var n = $("input[name='indCheckboxActive']:checked").length;
				var itemsSelected = "<small>&#40; "+ n + (n === 1 ? " Item" : " Items") +" Selected &#41;&nbsp;</small>";
				//$(this).find(".counter").text(itemsSelected);
				$("#counter").html(itemsSelected);
				if(n > 0) {
					$("#counter").show();

				} else {
					$("#counter").show();

				}
			}
			countChecked();

			function traverse(url){
				var queryString = {};
				url.replace(
					    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
					    function($0, $1, $2, $3) { queryString[$1] = $3; }
					);
				 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
				 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
				 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
				 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
				$("#brokersearchsearch input[type=submit]").click();
			}

			$("#exportAgentListAsExcel").submit(function(){
				$("#disclaimerPopup").modal('hide');
			});

	</script>
<%-- </form> --%>
