package com.getinsured.eligibility.at.ref.service.migration;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerBaseService;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;

@Component("lceCitizenshipHandlerMigrationService")
public class LceCitizenshipHandlerMigrationService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceCitizenshipHandlerMigrationService.class);

	@Autowired
	@Qualifier("lceAppExtensionEventMigration")
	private LceAppExtensionEventMigration lceAppExtensionEventMigration;
	
	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceCitizenshipHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		createSepEvent(currentApplication, lceProcessRequestDTO);
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);//HIX-108047
		
		updateAllowEnrollment(currentApplication, Y);
		
		LOGGER.info("LceCitizenshipHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	private Map<String, ApplicantEvent> createSepEvent(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Create Citizenship event - " + currentApplication.getId());
		return lceAppExtensionEventMigration.createCitizenshipEvent(currentApplication, lceProcessRequestDTO.getApplicationExtension());
	}
}
