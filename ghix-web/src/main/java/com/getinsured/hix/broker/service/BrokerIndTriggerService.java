package com.getinsured.hix.broker.service;

import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.PaymentMethods;

public interface BrokerIndTriggerService {
	
	public void triggerInd35(Broker broker, PaymentMethods paymentMethods);

	EntityResponseDTO triggerInd47(DesignateBroker designateBroker);

	BrokerBobResponseDTO triggerInd72(BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO);

	BrokerBobResponseDTO triggerInd73(BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO);

	void triggerInd54(Broker broker);
	
}
