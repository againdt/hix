package com.getinsured.hix.broker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.AccountUser;

public interface IUserRepository extends JpaRepository<AccountUser, Integer> {
}
