
package com.getinsured.ahbx.platform.ecm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.getinsured.ahbx.platform.ecm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckOutFile_QNAME = new QName("http://ecm.ahbx.accenture.com/", "checkOutFile");
    private final static QName _DoSearchResponse_QNAME = new QName("http://ecm.ahbx.accenture.com/", "doSearchResponse");
    private final static QName _GetFileResponse_QNAME = new QName("http://ecm.ahbx.accenture.com/", "getFileResponse");
    private final static QName _CheckinFile_QNAME = new QName("http://ecm.ahbx.accenture.com/", "checkinFile");
    private final static QName _Entry_QNAME = new QName("http://ecm.ahbx.accenture.com/", "entry");
    private final static QName _CheckOutFileResponse_QNAME = new QName("http://ecm.ahbx.accenture.com/", "checkOutFileResponse");
    private final static QName _Content_QNAME = new QName("http://ecm.ahbx.accenture.com/", "content");
    private final static QName _CheckinAttachedFile_QNAME = new QName("http://ecm.ahbx.accenture.com/", "checkinAttachedFile");
    private final static QName _MapWrapper_QNAME = new QName("http://ecm.ahbx.accenture.com/", "MapWrapper");
    private final static QName _CheckinFileResponse_QNAME = new QName("http://ecm.ahbx.accenture.com/", "checkinFileResponse");
    private final static QName _DoSearch_QNAME = new QName("http://ecm.ahbx.accenture.com/", "doSearch");
    private final static QName _CheckinAttachedFileResponse_QNAME = new QName("http://ecm.ahbx.accenture.com/", "checkinAttachedFileResponse");
    private final static QName _GetFile_QNAME = new QName("http://ecm.ahbx.accenture.com/", "getFile");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.getinsured.ahbx.platform.ecm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckinAttachedFileResponse }
     * 
     */
    public CheckinAttachedFileResponse createCheckinAttachedFileResponse() {
        return new CheckinAttachedFileResponse();
    }

    /**
     * Create an instance of {@link AnyTypeArray }
     * 
     */
    public AnyTypeArray createAnyTypeArray() {
        return new AnyTypeArray();
    }

    /**
     * Create an instance of {@link KeyValue }
     * 
     */
    public KeyValue createKeyValue() {
        return new KeyValue();
    }

    /**
     * Create an instance of {@link SearchResult }
     * 
     */
    public SearchResult createSearchResult() {
        return new SearchResult();
    }

    /**
     * Create an instance of {@link GetFileResponse }
     * 
     */
    public GetFileResponse createGetFileResponse() {
        return new GetFileResponse();
    }

    /**
     * Create an instance of {@link CheckinFile }
     * 
     */
    public CheckinFile createCheckinFile() {
        return new CheckinFile();
    }

    /**
     * Create an instance of {@link Datasource }
     * 
     */
    public Datasource createDatasource() {
        return new Datasource();
    }

    /**
     * Create an instance of {@link CheckinFileResponse }
     * 
     */
    public CheckinFileResponse createCheckinFileResponse() {
        return new CheckinFileResponse();
    }

    /**
     * Create an instance of {@link GetFile }
     * 
     */
    public GetFile createGetFile() {
        return new GetFile();
    }

    /**
     * Create an instance of {@link CheckinAttachedFile }
     * 
     */
    public CheckinAttachedFile createCheckinAttachedFile() {
        return new CheckinAttachedFile();
    }

    /**
     * Create an instance of {@link DoSearchResponse }
     * 
     */
    public DoSearchResponse createDoSearchResponse() {
        return new DoSearchResponse();
    }

    /**
     * Create an instance of {@link DoSearch }
     * 
     */
    public DoSearch createDoSearch() {
        return new DoSearch();
    }

    /**
     * Create an instance of {@link Folder }
     * 
     */
    public Folder createFolder() {
        return new Folder();
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link DocumentStream }
     * 
     */
    public DocumentStream createDocumentStream() {
        return new DocumentStream();
    }

    /**
     * Create an instance of {@link CheckOutFile }
     * 
     */
    public CheckOutFile createCheckOutFile() {
        return new CheckOutFile();
    }

    /**
     * Create an instance of {@link Image }
     * 
     */
    public Image createImage() {
        return new Image();
    }

    /**
     * Create an instance of {@link CheckOutFileResponse }
     * 
     */
    public CheckOutFileResponse createCheckOutFileResponse() {
        return new CheckOutFileResponse();
    }

    /**
     * Create an instance of {@link MapWrapper }
     * 
     */
    public MapWrapper createMapWrapper() {
        return new MapWrapper();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckOutFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "checkOutFile")
    public JAXBElement<CheckOutFile> createCheckOutFile(CheckOutFile value) {
        return new JAXBElement<CheckOutFile>(_CheckOutFile_QNAME, CheckOutFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "doSearchResponse")
    public JAXBElement<DoSearchResponse> createDoSearchResponse(DoSearchResponse value) {
        return new JAXBElement<DoSearchResponse>(_DoSearchResponse_QNAME, DoSearchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "getFileResponse")
    public JAXBElement<GetFileResponse> createGetFileResponse(GetFileResponse value) {
        return new JAXBElement<GetFileResponse>(_GetFileResponse_QNAME, GetFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckinFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "checkinFile")
    public JAXBElement<CheckinFile> createCheckinFile(CheckinFile value) {
        return new JAXBElement<CheckinFile>(_CheckinFile_QNAME, CheckinFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link KeyValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "entry")
    public JAXBElement<KeyValue> createEntry(KeyValue value) {
        return new JAXBElement<KeyValue>(_Entry_QNAME, KeyValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckOutFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "checkOutFileResponse")
    public JAXBElement<CheckOutFileResponse> createCheckOutFileResponse(CheckOutFileResponse value) {
        return new JAXBElement<CheckOutFileResponse>(_CheckOutFileResponse_QNAME, CheckOutFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Content }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "content")
    public JAXBElement<Content> createContent(Content value) {
        return new JAXBElement<Content>(_Content_QNAME, Content.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckinAttachedFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "checkinAttachedFile")
    public JAXBElement<CheckinAttachedFile> createCheckinAttachedFile(CheckinAttachedFile value) {
        return new JAXBElement<CheckinAttachedFile>(_CheckinAttachedFile_QNAME, CheckinAttachedFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MapWrapper }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "MapWrapper")
    public JAXBElement<MapWrapper> createMapWrapper(MapWrapper value) {
        return new JAXBElement<MapWrapper>(_MapWrapper_QNAME, MapWrapper.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckinFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "checkinFileResponse")
    public JAXBElement<CheckinFileResponse> createCheckinFileResponse(CheckinFileResponse value) {
        return new JAXBElement<CheckinFileResponse>(_CheckinFileResponse_QNAME, CheckinFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoSearch }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "doSearch")
    public JAXBElement<DoSearch> createDoSearch(DoSearch value) {
        return new JAXBElement<DoSearch>(_DoSearch_QNAME, DoSearch.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckinAttachedFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "checkinAttachedFileResponse")
    public JAXBElement<CheckinAttachedFileResponse> createCheckinAttachedFileResponse(CheckinAttachedFileResponse value) {
        return new JAXBElement<CheckinAttachedFileResponse>(_CheckinAttachedFileResponse_QNAME, CheckinAttachedFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ecm.ahbx.accenture.com/", name = "getFile")
    public JAXBElement<GetFile> createGetFile(GetFile value) {
        return new JAXBElement<GetFile>(_GetFile_QNAME, GetFile.class, null, value);
    }

}
