package com.getinsured.hix.indportal.dto;

import java.math.BigDecimal;
import java.util.List;

import com.getinsured.hix.indportal.dto.AppGroupResponse.GroupType;
import com.getinsured.hix.indportal.dto.AppGroupResponse.ProgramType;

public class Group{
	private Integer id;
	private String groupCsr;
	private String coverageDate;
	private String groupEnrollmentId;
	private BigDecimal maxAptc;
	private BigDecimal maxStateSubsidy;
	private List<Member> memberList;
	private GroupType groupType;
	private ProgramType programType;
	private boolean hasAPTCEligibleMembers;
	private Boolean hasStateSubsidyEligibleMembers=false;
	private String encData;
	private String hiosIssuerId;
	private String issuerName;
	private String planName;
	private Float netPremium;
	private BigDecimal electedAptc;
	private BigDecimal electedStateSubsidy;
	private Long enrollmentPriorApplicationId;
	private Long enrollmentApplicationId;
	private String encryptedGroupEnrollmentId;
	private String allowChangePlan;
	private String enrollmentCreationDate;
	private String enrollmentStatus;
	private String enrollmentEndDate;
	private Boolean allowDisenroll;
	private Boolean isRenewal = false;
	private Boolean isNonStandardRelationGroup = false;
	
	public Boolean getIsNonStandardRelationGroup() {
		return isNonStandardRelationGroup;
	}
	public void setIsNonStandardRelationGroup(Boolean isNonStandardRelationGroup) {
		this.isNonStandardRelationGroup = isNonStandardRelationGroup;
	}
	public Boolean getIsRenewal() {
		return isRenewal;
	}
	public void setIsRenewal(Boolean isRenewal) {
		this.isRenewal = isRenewal;
	}
	public String getGroupCsr() {
		return groupCsr;
	}
	public void setGroupCsr(String groupCsr) {
		this.groupCsr = groupCsr;
	}
	public String getCoverageDate() {
		return coverageDate;
	}
	public void setCoverageDate(String coverageDate) {
		this.coverageDate = coverageDate;
	}
	
	public String getGroupEnrollmentId() {
		return groupEnrollmentId;
	}
	public void setGroupEnrollmentId(String groupEnrollmentId) {
		this.groupEnrollmentId = groupEnrollmentId;
	}
	public BigDecimal getMaxAptc() {
		return maxAptc;
	}
	public void setMaxAptc(BigDecimal maxAptc) {
		this.maxAptc = maxAptc;
	}
	public List<Member> getMemberList() {
		return memberList;
	}
	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}
	public GroupType getGroupType() {
		return groupType;
	}
	public void setGroupType(GroupType groupType) {
		this.groupType = groupType;
	}
	public ProgramType getProgramType() {
		return programType;
	}
	public void setProgramType(ProgramType programType) {
		this.programType = programType;
	}
	public boolean isHasAPTCEligibleMembers() {
		return hasAPTCEligibleMembers;
	}
	public void setHasAPTCEligibleMembers(boolean hasAPTCEligibleMembers) {
		this.hasAPTCEligibleMembers = hasAPTCEligibleMembers;
	}
	public String getEncData() {
		return encData;
	}
	public void setEncData(String encData) {
		this.encData = encData;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getNetPremium() {
		return netPremium;
	}
	public void setNetPremium(Float netPremium) {
		this.netPremium = netPremium;
	}
	public BigDecimal getElectedAptc() {
		return electedAptc;
	}
	public void setElectedAptc(BigDecimal electedAptc) {
		this.electedAptc = electedAptc;
	}
	public Long getEnrollmentPriorApplicationId() {
		return enrollmentPriorApplicationId;
	}
	public void setEnrollmentPriorApplicationId(Long enrollmentPriorApplicationId) {
		this.enrollmentPriorApplicationId = enrollmentPriorApplicationId;
	}
	public Long getEnrollmentApplicationId() {
		return enrollmentApplicationId;
	}
	public void setEnrollmentApplicationId(Long enrollmentApplicationId) {
		this.enrollmentApplicationId = enrollmentApplicationId;
	}
	public String getEncryptedGroupEnrollmentId() {
		return encryptedGroupEnrollmentId;
	}
	public void setEncryptedGroupEnrollmentId(String encryptedGroupEnrollmentId) {
		this.encryptedGroupEnrollmentId = encryptedGroupEnrollmentId;
	}
	public String getAllowChangePlan() {
		return allowChangePlan;
	}
	public void setAllowChangePlan(String allowChangePlan) {
		this.allowChangePlan = allowChangePlan;
	}
	public String getEnrollmentCreationDate() {
		return enrollmentCreationDate;
	}
	public void setEnrollmentCreationDate(String enrollmentCreationDate) {
		this.enrollmentCreationDate = enrollmentCreationDate;
	}
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public String getEnrollmentEndDate() {
		return enrollmentEndDate;
	}
	public void setEnrollmentEndDate(String enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}

	public BigDecimal getMaxStateSubsidy() {
		return maxStateSubsidy;
	}

	public void setMaxStateSubsidy(BigDecimal maxStateSubsidy) {
		this.maxStateSubsidy = maxStateSubsidy;
	}

	public BigDecimal getElectedStateSubsidy() {
		return electedStateSubsidy;
	}

	public void setElectedStateSubsidy(BigDecimal electedStateSubsidy) {
		this.electedStateSubsidy = electedStateSubsidy;
	}

	public Boolean isHasStateSubsidyEligibleMembers() {
		return hasStateSubsidyEligibleMembers;
	}

	public void setHasStateSubsidyEligibleMembers(Boolean hasStateSubsidyEligibleMembers) {
		this.hasStateSubsidyEligibleMembers = hasStateSubsidyEligibleMembers;
	}
	
	public Boolean getAllowDisenroll() {
		return allowDisenroll;
	}
	
	public void setAllowDisenroll(Boolean allowDisenroll) {
		this.allowDisenroll = allowDisenroll;
	}

}
