package com.getinsured.eligibility.at.dto;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.timeshift.TimeshiftSupport;

@XmlRootElement(name = "AccountTransferRequestDTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountTransferRequestDTO implements TimeshiftSupport{

	@XmlElement(name = "AccountTransferRequestPayloadType", required = true)
	private AccountTransferRequestPayloadType accountTransferRequestPayloadType;

	@XmlElement(name = "user", required = true)
	private String user="na";
	
	@XmlElement(name = "tsoffset", required = true)
	private String tsOffset="na";
	
	@XmlElement(name = "giwsPayloadId", required = true)
	private Integer giwsPayloadId;

	@XmlElement(name = "caseNumber", required = false)
	private String caseNumber;

	@XmlElement(name = "isFullDetermination", required = false)
	private boolean isFullDetermination;

	@XmlElement(name = "atResponseType", required = false)
	private String atResponseType;

	@XmlElement(name = "isLCE", required = false)
	private boolean isLCE;

	@XmlElement(name = "isQE", required = false)
	private boolean isQE;

	@XmlElement(name = "isRenewal", required = false)
	private boolean isRenewal;

	@XmlElement(name = "notificationType", required = false)
	private String notificationType;

	@XmlElement(name = "enrolledApplicationId", required = false)
	private long enrolledApplicationId;

	@XmlElement(name = "compareToApplicationId", required = false)
	private long compareToApplicationId;

	@XmlElement(name = "renewalCompareToApplicationId", required = false)
	private long renewalCompareToApplicationId;

	@XmlElement(name = "applicationsWithSameId", required = false)
	private List<ApplicationsWithSameIdDTO> applicationsWithSameId;

	@XmlElement(name = "coverageYear", required = false)
	private long coverageYear;

	@XmlElement(name = "processToBeExecuted", required = false)
	private String processToBeExecuted;

	@XmlElement(name = "accountTransferCategory", required = false)
	private String accountTransferCategory;

	@XmlElement(name = "cmrAutoLinking", required = false)
	private boolean cmrAutoLinking = false;

	@XmlElement(name = "multipleCmr", required = false)
	private boolean multipleCmr = false;

	@XmlElement(name = "nonFinancialCmr", required = false)
	private boolean nonFinancialCmr = false;
	
	@XmlElement(name = "nonFinancialCmrId", required = false)
	private int nonFinancialCmrId;
	
	@XmlElement(name = "requester", required = false)
	private String requester;

	@XmlElement(name = "additionalFieldsMapXmlString", required = false)
	private String additionalFieldsMapXmlString;
	
	@XmlElement(name = "currentSpanApplicationId", required = false)
	private long currentSpanApplicationId;
	
	@XmlElement(name = "atSpanInfoId", required = false)
	private long atSpanInfoId;

	@XmlElement(name = "accountMigration", required = false)
	private boolean accountMigration;
	
	public void setRenewalCompareToApplicationId(long renewalCompareToApplicationId) {
		this.renewalCompareToApplicationId = renewalCompareToApplicationId;
	}

	public int getNonFinancialCmrId() {
		return nonFinancialCmrId;
	}

	public void setNonFinancialCmrId(int nonFinancialCmrId) {
		this.nonFinancialCmrId = nonFinancialCmrId;
	}

	public boolean isMultipleCmr() {
		return multipleCmr;
	}

	public void setMultipleCmr(boolean multipleCmr) {
		this.multipleCmr = multipleCmr;
	}

	public boolean isNonFinancialCmr() {
		return nonFinancialCmr;
	}

	public void setNonFinancialCmr(boolean nonFinancialCmr) {
		this.nonFinancialCmr = nonFinancialCmr;
	}

	public boolean isCmrAutoLinking() {
		return cmrAutoLinking;
	}

	public void setCmrAutoLinking(boolean cmrAutoLinking) {
		this.cmrAutoLinking = cmrAutoLinking;
	}

	public String getAccountTransferCategory() {
		return accountTransferCategory;
	}

	public void setAccountTransferCategory(String accountTransferCategory) {
		this.accountTransferCategory = accountTransferCategory;
	}

	public boolean isRenewal() {
		return isRenewal;
	}

	public void setRenewal(boolean isRenewal) {
		this.isRenewal = isRenewal;
	}

	public String getProcessToBeExecuted() {
		return processToBeExecuted;
	}

	public void setProcessToBeExecuted(String processToBeExecuted) {
		this.processToBeExecuted = processToBeExecuted;
	}

	public long getCompareToApplicationId() {
		return compareToApplicationId;
	}

	public void setCompareToApplicationId(long compareToApplicationId) {
		this.compareToApplicationId = compareToApplicationId;
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}

	public List<ApplicationsWithSameIdDTO> getApplicationsWithSameId() {
		return applicationsWithSameId;
	}

	public void setApplicationsWithSameId(List<ApplicationsWithSameIdDTO> applicationsWithSameId) {
		this.applicationsWithSameId = applicationsWithSameId;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public AccountTransferRequestPayloadType getAccountTransferRequestPayloadType() {
		return accountTransferRequestPayloadType;
	}

	public void setAccountTransferRequestPayloadType(AccountTransferRequestPayloadType accountTransferRequestPayloadType) {
		this.accountTransferRequestPayloadType = accountTransferRequestPayloadType;
	}

	public Integer getGiwsPayloadId() {
		return giwsPayloadId;
	}

	public void setGiwsPayloadId(Integer giwsPayloadId) {
		this.giwsPayloadId = giwsPayloadId;
	}

	public boolean isFullDetermination() {
		return isFullDetermination;
	}

	public void setFullDetermination(boolean isFullDetermination) {
		this.isFullDetermination = isFullDetermination;
	}

	public boolean isLCE() {
		return isLCE;
	}

	public void setLCE(boolean isLCE) {
		this.isLCE = isLCE;
	}

	public boolean isQE() {
		return isQE;
	}

	public void setQE(boolean isQE) {
		this.isQE = isQE;
	}

	public String getAtResponseType() {
		return atResponseType;
	}

	public void setAtResponseType(String atResponseType) {
		this.atResponseType = atResponseType;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public long getEnrolledApplicationId() {
		return enrolledApplicationId;
	}

	public void setEnrolledApplicationId(long enrolledApplicationId) {
		this.enrolledApplicationId = enrolledApplicationId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getTsOffset() {
		return tsOffset;
	}

	public void setTsOffset(String tsOffset) {
		this.tsOffset = tsOffset;
	}

	public String getRequester() {
		return requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	public String getAdditionalFieldsMapXmlString() {
		return additionalFieldsMapXmlString;
	}

	public void setAdditionalFieldsMapXmlString(String additionalFieldsMapXmlString) {
		this.additionalFieldsMapXmlString = additionalFieldsMapXmlString;
	}

	public long getCurrentSpanApplicationId() {
		return currentSpanApplicationId;
	}

	public void setCurrentSpanApplicationId(long currentSpanApplicationId) {
		this.currentSpanApplicationId = currentSpanApplicationId;
	}
	
	public long getAtSpanInfoId() {
		return atSpanInfoId;
	}

	public void setAtSpanInfoId(long atSpanInfoId) {
		this.atSpanInfoId = atSpanInfoId;
	}

	public long getRenewalCompareToApplicationId() {
		return renewalCompareToApplicationId;
	}

	public boolean isAccountMigration() {
		return accountMigration;
	}

	public void setAccountMigration(boolean accountMigration) {
		this.accountMigration = accountMigration;
	}
}
