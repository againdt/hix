
package com.getinsured.eligibility.prescreen.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.GhixEndPoints;


/**
 * @author Nikhil Talreja
 * @since 17 May, 2013
 * void
 */
@Component
public class PrescreenInfo implements ApplicationListener<ContextRefreshedEvent>{
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PrescreenInfo.class);
	
	private List<PrescreenInfoRec> records; 
	
	private static final String CALCULATOR = "Calculator";
	
	/**
	 * @author Nikhil Talreja
	 * @since
	 * void
	 */
	public static void main(String[] args) {
		PrescreenInfo prescreenInfo = new PrescreenInfo();
		prescreenInfo.loadPrescreenInfo();
		
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		LOGGER.info(""+getPrescreenInfo());
	}
	
	public void loadPrescreenConfigDetails(){
		
		if (records != null && records.size() > 0) {
			return;
		}
		else{
			records = new ArrayList<PrescreenInfoRec>();
		}

		
		
		//Calculator URLs
		records.add(new PrescreenInfoRec(CALCULATOR,"APTC (Marketing)",PrescreenConstants.PRESCREEN_SVC_URL+PrescreenConstants.CALCULATE_APTC_URL));
		records.add(new PrescreenInfoRec(CALCULATOR,"APTC (Product)",PrescreenConstants.PRESCREEN_SVC_URL+PrescreenConstants.CALCULATE_APTC_PRODUCT_URL));
		records.add(new PrescreenInfoRec(CALCULATOR,"CSR",PrescreenConstants.PRESCREEN_SVC_URL+PrescreenConstants.CALCULATE_CSR_URL));
		records.add(new PrescreenInfoRec(CALCULATOR,"MAGI",PrescreenConstants.PRESCREEN_SVC_URL+PrescreenConstants.CALCULATE_MAGI_URL));
		records.add(new PrescreenInfoRec("Calculator","FPL",PrescreenConstants.PRESCREEN_SVC_URL+PrescreenConstants.CALCULATE_FPL_URL));
		
		//Prescreen REST Services
		records.add(new PrescreenInfoRec("REST service","Prescreen Service",PrescreenConstants.PRESCREEN_SVC_URL));
		records.add(new PrescreenInfoRec("REST service","Get PRESCREEN_DATA Record",PrescreenConstants.PRESCREEN_SVC_URL+PrescreenConstants.GET_PRESCREEN_RECORD));
		records.add(new PrescreenInfoRec("REST service","Save PRESCREEN_DATA Record",PrescreenConstants.PRESCREEN_SVC_URL+PrescreenConstants.SAVE_PRESCREEN_RECORD));
		
		//Social Media URLs
		records.add(new PrescreenInfoRec("Social Media","Facebook share",PrescreenConstants.FACEBOOK_SHARE_URL));
		records.add(new PrescreenInfoRec("Social Media","Twitter share",PrescreenConstants.TWITTER_SHARE_URL));
		records.add(new PrescreenInfoRec("Social Media","Google Plus share",PrescreenConstants.GOOGLEPLUS_SHARE_URL));
		
		//Plan Management URLs
		records.add(new PrescreenInfoRec("Plan Management","Benchmark Premium URL",PrescreenConstants.BENCHMARCK_PREMIUM_URL));
		records.add(new PrescreenInfoRec("Plan Management","Teaser Plan URL",GhixEndPoints.PlanMgmtEndPoints.TEASER_PLAN_API_URL));
		records.add(new PrescreenInfoRec("Plan Management","Teaser Plan URL for Mini Estimator",GhixEndPoints.PlanMgmtEndPoints.TEASER_ELIGIBILITY_PLANS_API_URL));
		
		//Affiliate Properties
		records.add(new PrescreenInfoRec("Affiliate Services","URL to verify API Key",GhixEndPoints.AffiliateServiceEndPoints.VALIDATE_APIKEY));
		records.add(new PrescreenInfoRec("Affiliate Services","Partners for auto registeration",PrescreenConstants.AUTO_REG_PARTNERS));
	}
	
	/**
	 * <p>
	 * This method calls all the individual methods. Every method loads specific
	 * details as indicated in each method.
	 * </p>
	 * 
	 */
	public void loadPrescreenInfo() {
		
		LOGGER.info("Loading Prescreen info");
		loadPrescreenConfigDetails();
	}
	
	public List<PrescreenInfoRec> getPrescreenInfo() {
		
		loadPrescreenInfo();
		return records;
	}

}
