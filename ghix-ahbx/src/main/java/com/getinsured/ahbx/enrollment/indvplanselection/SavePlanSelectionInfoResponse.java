//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.20 at 10:29:02 PM PDT 
//


package com.getinsured.ahbx.enrollment.indvplanselection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for savePlanSelectionInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="savePlanSelectionInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="savePlanSelectionInfoResponse" type="{http://impl.webservice.ahbx.accenture.com/}savePlanSelectionInfoResponseDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "savePlanSelectionInfoResponse", propOrder = {
    "savePlanSelectionInfoResponse"
})
public class SavePlanSelectionInfoResponse {

    protected SavePlanSelectionInfoResponseDTO savePlanSelectionInfoResponse;

    /**
     * Gets the value of the savePlanSelectionInfoResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SavePlanSelectionInfoResponseDTO }
     *     
     */
    public SavePlanSelectionInfoResponseDTO getSavePlanSelectionInfoResponse() {
        return savePlanSelectionInfoResponse;
    }

    /**
     * Sets the value of the savePlanSelectionInfoResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SavePlanSelectionInfoResponseDTO }
     *     
     */
    public void setSavePlanSelectionInfoResponse(SavePlanSelectionInfoResponseDTO value) {
        this.savePlanSelectionInfoResponse = value;
    }

}
