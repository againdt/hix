import React from 'react';
import { connect } from 'react-redux';


// Components

import NavBar from '../components/navBar/NavBar';


// Constants

import EXCEPTIONS from '../constants/exceptionList';



const NavBarContainer = props => {

    const {
        exception
    } = props;

    const getContent = () => {
        if(exception !== null && exception.exceptionType === EXCEPTIONS.REVIEW_AFTER_SIGN) {
            return null;
        } else {
            return <NavBar/>;
        }
    };

    return (
        <div className="navbar-container">
            {
                getContent()
            }
        </div>
    );
};

const mapStateToProps = state => ({
    exception: state.exception
});

export default connect(mapStateToProps)(NavBarContainer);