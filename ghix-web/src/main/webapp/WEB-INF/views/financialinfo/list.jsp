<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<jsp:include page="/WEB-INF/views/layouts/untiletopbar.jsp" />

<%@ page isELIgnored="false"%>

<div id="maincontainer" class="container">
<div class="row">
	<div class="span4">
		<h2>Financial Info</h2>
	</div>
	<div>
		<br><br/>
		<p><a href="createfinancialinfo">Create Financial Info</a></p>
	</div>
	<div>
		<form class="form-stacked" id="frmlist" name="frmlist" action="list" method="POST">
			<fieldset>
				<legend></legend>
				<p></p>
				<br />
				<div class="profile">
					<div class="clearfix">
						  <display:table pagesize="5" export="true" name="financialInfos" sort="list" id="data" requestURI="list">
							  <display:setProperty name="paging.banner.placement">bottom</display:setProperty>
							  
							  <display:setProperty name="export.excel" value="true" />
							  <display:setProperty name="export.csv" value="true" />
							  <display:setProperty name="export.xml" value="true" />
							  
							  <display:setProperty name="export.excel.filename" value="fininfo.xls"/>
							  <display:setProperty name="export.csv.filename" value="fininfo.csv"/>
							  <display:setProperty name="export.xml.filename" value="fininfo.xml"/>
							 
							  <display:column property="id" title="Id" sortable="true"/>
							  <display:column property="firstName" title="First Name" sortable="true"/>
							  <display:column property="lastName" title="Last Name" sortable="true"/>
							  <display:column property="email" title="Email" sortable="true"/>
							  <display:column property="paymentType" title="Payment Type" sortable="true"/>
							  <display:column value ="${fn:substring(data.bankInfo.accountNumber, fn:length(data.bankInfo.accountNumber) - 4, fn:length(data.bankInfo.accountNumber))}"  title="Account Number" sortable="true"/>
							  <display:column value="${fn:substring(data.creditCardInfo.cardNumber, fn:length(data.creditCardInfo.cardNumber) - 4, fn:length(data.creditCardInfo.cardNumber))}" title="CreditCard Number" sortable="true"/>
							  <display:column property="created" title="Created" format="{0,date,MM/dd/yyyy}" sortable="true"/>
						</display:table>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>This information is required to determine initial eligibility to
					use the SHOP Exchange. All fields are subjected to a basic format
					validation of the input (e.g. a zip code must be a 5 digit number).
					The question mark icon which opens a light-box provides helpful
					information. The EIN number can be validated with an external data
					source if an adequate web API is available.</p>
			</div>
		</div>
	</div>
</div>
</div>
<%@ include file="/WEB-INF/views/layouts/untilefooter.jsp"%>
