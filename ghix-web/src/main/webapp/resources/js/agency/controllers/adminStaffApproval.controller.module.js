(function() {
'use strict';

angular.module('adminStaffApprovalCtrlModule', ['translateApp'])
.controller('AdminStaffApprovalCtrl', AdminStaffApprovalCtrl);

AdminStaffApprovalCtrl.$inject = ['$http', '$stateParams', '$scope', '$state', 'dataStorageService', '$translate', 'StatusService'];

    function AdminStaffApprovalCtrl($http, $stateParams, $scope, $state, dataStorageService, $translate, StatusService) {
      var vm = this;

      vm.init = init;
      vm.save = save;
      vm.cancel = cancel;
      vm.edit = edit;
      vm.save = save;
      vm.callViewCommentsModal = callViewCommentsModal;
      vm.assistantId =  $stateParams.assistantId || dataStorageService.getItem('assistantId');
      vm.registrationComplete = $stateParams.registrationComplete || dataStorageService.getItem('registrationComplete');
      vm.currentUserRole = StatusService.CurrentUserRole;
      dataStorageService.setItem('assistantId', vm.assistantId);
      dataStorageService.setItem('registrationComplete', vm.registrationComplete);
      vm.viewAdminStaffFlow = dataStorageService.getItem('viewAdminStaffFlow');

      function init(){
          vm.flowFlag = {};
          $scope.$parent.vm.sideNavListFour[0].showOKicon = true;
          $scope.$parent.vm.sideNavListFour[1].disableLink = false;
          $scope.$parent.vm.sideNavListFour[2].disableLink = false;

          $scope.viewCommentsModalPath = "/hix/resources/html/agency/templates/viewCommentsModal.html";
          updateFlowFlag();
          var promise = $http.get('/hix/agency/staff/approvalhistory/' + vm.assistantId);
          promise.success(function(response){
              vm.loader = false;
              vm.staffApprovalData = response;
          })
          promise.error(function(response){
              vm.loader = false;
          })
      }

      function edit() {
      	vm.editMode = !vm.editMode;
      }

      function save() {
          vm.loader = false;
          vm.editMode = !vm.editMode;
          if(!vm.approvalStatus) {
            vm.approvalStatus = vm.staffApprovalData.approvalStatus;
          }
          var data = {
              id : vm.assistantId || vm.staffApprovalData.id,
              approvalStatus: vm.approvalStatus,
              status: vm.staffApprovalData.status,
              comment: vm.staffApprovalData.comment
          }
          var promise = $http.post('/hix/agency/staff/approvalstatus/', data);
          promise.success(function(response){
              vm.loader = false;
              vm.editMode = false;
              vm.staffApprovalData = response;
          })
          promise.error(function(response){
              vm.loader = false;
          })
      }

      function cancel() {
      	vm.editMode = !vm.editMode;
      }

      function callViewCommentsModal(comments) {
          $scope.content = comments;
      }
      function updateFlowFlag(){

          //for admin flow
          var isAdminRole = $("#isAdminRole").val();

          //for agency flow
          var certificationStatus = $('#agencyCertificationStatus').val();

          if (isAdminRole === 'true') {
              vm.flowFlag.adminFlow = true;
              vm.editMode = false;
              $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
          }
          if(vm.viewAdminStaffFlow) {
              $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
          }
          if (vm.currentUserRole === 'APPROVEDADMINSTAFFL1' || vm.currentUserRole === 'APPROVEDADMINSTAFFL2') {
               $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
          }
      }

    }

})();
