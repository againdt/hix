 <%@ include file="../include.jsp" %>
          
<div id="appscr61" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages"><strong class="nameOfHouseHold"></strong><spring:message code="ssap.page11.personalInfo" text="- Personal Information"/></h4>
		</div>
		
		<form id="appscr61Form" class="form-horizontal"  data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.pageHeader.familyAndHousehold" text="Family and Household" /></h4>
			</div>
			<div class="gutter10">
			<div class="control-group">
				<p>
					<spring:message code="ssap.page11.sex" text="Sex"/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p>
				<div class="margin5-l">
					<label for="appscr61GenderMaleID" class="radio">
						<input type="radio" name="appscr61_gender" onchange="apppscr60SetGenderInSSAP()" id="appscr61GenderMaleID" value="male" data-parsley-errors-container="#appscr61GenderMaleIDErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/>
						<spring:message code="ssap.page11.male" text="Male"/>
					</label>
					<label for="appscr61GenderFemaleID" class="radio">
						<input type="radio"  name="appscr61_gender" onchange="apppscr60SetGenderInSSAP()" id="appscr61GenderFemaleID" value="female" />
						<spring:message code="ssap.page11.female" text="Female"/>
					</label>
					<div id="appscr61GenderMaleIDErrorDiv"></div>
				</div>
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page11.does" text="Does"/>
					<strong class="nameOfHouseHold"></strong>&nbsp;
					<spring:message code="ssap.page11.haveASSN" text=" have a Social Security Number ?"/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p>
				<div class="margin5-l">
					<label for="socialSecurityCardHolderIndicatorYes" class="radio">
						<input type="radio" name="socialSecurityCardHolderIndicator" id="socialSecurityCardHolderIndicatorYes" value="yes" onclick="askForSSNNoPage61();" data-parsley-errors-container="#socialSecurityCardHolderIndicatorYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />" />
						<spring:message code="ssap.page11.yes" text="Yes"/>
					</label>
					<label for="socialSecurityCardHolderIndicatorNo" class="radio">
						<input type="radio" name="socialSecurityCardHolderIndicator" id="socialSecurityCardHolderIndicatorNo" value="no" onclick="askForSSNNoPage61();" />
						<spring:message code="ssap.page11.no" text="No"/>
					</label>
					<div id="socialSecurityCardHolderIndicatorYesErrorDiv"></div>
				</div>
			</div><!-- control-group ends -->
			
			
			<div class="alert alert-info">
				<p class="margin10-t">
					<spring:message code="ssap.page11.if" text="If"/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page11.isApplyingForHealthInsuranceYouMustProvideSSN" text="is applying for health insurance, you must provide a Social Security number (SSN) if available. If "/><span class="nameOfHouseHold"></span> <spring:message code="ssap.page11.dontHaveSSNWeCanHelp" text="doesn't have a SSN we can help"/>
					<span id="appscr61HimOrHer"></span>
					<spring:message code="ssap.page11.applyForone" text="apply for one."/>
					<spring:message code="ssap.page11.visit" text="Visit"/>&nbsp;<a href="http://www.ssa.gov/ssnumber" target="_blank">
					<spring:message code="ssap.page11.ssnumber" text="www.ssa.gov/ssnumber"/></a>.&nbsp;
					
					<span class="non_financial_hide">
						<spring:message code="ssap.page11.onlyUseSSNForIncomeAndOtherInfo" text=" We only use SSNs to check income and other information to see if"/>
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page11.canGetHelpForHealthInsuranceSSNCanHelpThis" text="can get help paying for health insurance."/>
					</span>
					
					<spring:message code="ssap.page11.aSSNCanHelp" text="A SSN can also help with enrolling in a health plan if "/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page11.isEligibleForOne" text="is eligible for one. "/>
				</p>
			</div>
			<%-- <p>
				<spring:message code="ssap.page11.because" text="Because"/>
				<strong class="nameOfHouseHold"></strong>
				<spring:message code="ssap.page11.notApplingForHealthInsuranceMayProvideSSN" text="isn't applying for health insurance, you may provide a Social Security number (SSN) for "/>
				<strong class="nameOfHouseHold"></strong>&nbsp;
				<spring:message code="ssap.page11.smallIf" text="if"/>
				<strong id="appscr63ChildHeShe"></strong>
				<spring:message code="ssap.page11.hasOneOptionalUsethisSSNToCheck" text="has one. It's optional. We&#39;ll use this SSN to check"/>
				<strong class="nameOfHouseHold"></strong>
				<spring:message code="ssap.page11.incomeCanSpeedUpDecisionWhetherHouseholdGetHelpForInsurance" text="'s income. This can speed up the decision about whether household members get help paying for insurance."/>
			</p> --%>
			
			<div id="askAboutSSNOnPage61" class="hide">
				<div class="control-group">
					<label class="required control-label radio" for="ssn">
						<spring:message code="ssap.page11.SSNText" text="Social Security Number"/>
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<%-- <div class="pull-left margin5-r span2">
							<input  data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"  data-parsley-minlength="3" data-parsley-minlength-message="<spring:message code="ssap.required.please" text="Please Enter"/> <spring:message code="ssap.required.first.three.digit" text="first three digits"/>" placeHolder="<spring:message code="ssap.page11.placeholder.3x" text="xxx"/>" class="input-mini" type="text" id="ssn1" name="ssn1">
						</div>
						<div class="pull-left margin5-r span2">
							<input data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"  data-parsley-minlength="2" data-parsley-minlength-message="<spring:message code="ssap.required.please" text="Please Enter"/> <spring:message code="ssap.required.second.two.digit" text="second two digits"/>" placeHolder="<spring:message code="ssap.page11.placeholder.2x" text="xx"/>"  class="input-mini" type="text" id="ssn2" name="ssn2">
						</div>
						<div class="pull-left margin5-r span2">
							<input  data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />"  data-parsley-minlength="4" data-parsley-minlength-message="<spring:message code="ssap.required.please" text="Please Enter"/> <spring:message code="ssap.required.last.four.digit" text="last four digits"/>" placeHolder="<spring:message code="ssap.page11.placeholder.4x" text="xxxx"/>" class="input-small" type="text" id="ssn3" name="ssn3">
						</div> --%>
						<input id="ssn" type="text" placeholder="xxx-xx-xxxx" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.ssnRequired' />"  data-parsley-pattern="^\*{3}-\*{2}-\d{4}$" data-parsley-pattern-message="<spring:message code="ssap.error.ssnValid" />" onblur="maskSSN()">
						
						<input class="input61" type="hidden" id="socialSecurityNumber" name="socialSecurityNumber">
						</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<p>
						<spring:message code="ssap.page11.capIs" text="Is"/>
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page11.sameNameAppearsOn" text="the same name that appears on"/>
						<strong id="appscr61SSNHisHer"></strong>
						<spring:message code="ssap.page11.SSCard" text="Social Security card?"/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</p>
					<div class="margin5-l">
						<label for="fnlnsSameIndicatorYes" class="radio">
							<input type="radio" id="fnlnsSameIndicatorYes" name="fnlnsSame" value="yes" onchange="hasSameSSNNumberP61();" data-parsley-errors-container="#fnlnsSameIndicatorYesErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" />
							<spring:message code="ssap.page11.yes" text="Yes"/>
						</label>
						<label for="fnlnsSameIndicatorNo" class="radio">
							<input type="radio" id="fnlnsSameIndicatorNo" name="fnlnsSame"  value="no"  onchange="hasSameSSNNumberP61();" />
							<spring:message code="ssap.page11.no" text="No"/>
						</label>
						<div id="fnlnsSameIndicatorYesErrorDiv"></div>
					</div>
				</div><!-- control-group ends -->
		
				<div id="sameSSNPage61Info" class="hide">
					<h4>
						<spring:message code="ssap.page11.enterSameName" text="Enter the same name as shown on  "/>
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page11.personSSNCard" text="'s Social Security Card:"/>
					</h4>
					<div class="control-group margin20-t">
						<label class="required control-label radio" for="firstNameOnSSNCard">
							<spring:message code="ssap.page11.placeholder.firstName" text="First Name" />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							<span aria-label="Required!"></span>
						</label><!-- end of label -->
						<div class="controls">
							<input placeHolder="<spring:message code="ssap.page11.placeholder.firstName" text="First Name"/>" class="input-large" type="text" name="firstNameOnSSNCard" id="firstNameOnSSNCard" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.firstNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.firstNameRequired'/>" />
							<span class="errorMessage"></span>
							<!-- <ul id="parsley-sameName-fn" class="parsley-error-list hide">
								<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
								</li>
							</ul> -->
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label radio" for="middleNameOnSSNCard">
							<spring:message code="ssap.page11.placeholder.middleName" text="Middle Name" />
						</label><!-- end of label -->
						<div class="controls">
							<input placeHolder="<spring:message code="ssap.page11.placeholder.middleName" text="Middle Name"/>"  class="input-large" type="text" name="middleNameOnSSNCard" id="middleNameOnSSNCard" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.middleNameValid'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="required control-label radio" for="lastNameOnSSNCard">
							<spring:message code="ssap.lastName" text="Last Name" />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							<span aria-label="Required!"></span>
						</label><!-- end of label -->
						<div class="controls">
							<input placeHolder="<spring:message code="ssap.page11.placeholder.lastName" text="Last Name"/>" class="input-large" type="text"  id="lastNameOnSSNCard" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$"  data-parsley-pattern-message="<spring:message code='ssap.error.lastNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.lastNameRequired'/>" />
							<span class="errorMessage"></span>
							<!-- <ul id="parsley-sameName-ln" class="parsley-error-list hide">
								<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
								</li>
							</ul> -->
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label radio" for="suffixOnSSNCard">
							<spring:message code="ssap.suffix" text="Suffix" />
						</label><!-- end of label -->
						<div class="controls">
							<select class="input-large" id="suffixOnSSNCard" name="suffix">
								<option value="" selected="selected"><spring:message code="ssap.suffix" text="Suffix" /></option>
								<option value="Jr."><spring:message code="ssap.JR" text="Jr." /></option>
								<option value="Sr."><spring:message code="ssap.SR" text="Sr." /></option>
								<option value="III"><spring:message code="ssap.III" text="III" /></option>
								<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
							</select>
							<div class="help-inline" id="suffix3_error"></div>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
				</div><!-- sameSSNPage61Info ends -->
			</div><!-- askAboutSSNOnPage61 ends -->
			
			<div id="DontaskAboutSSNOnPage61" class="hide">
				<h4>
					<spring:message code="ssap.page11.noSSNExplaination" text="If no Social Security Number is available please select from the following explanations:"/>
				</h4>
				<label for="reasonableExplanationForNoSSN" aria-hidden="true"> <spring:message code="ssap.page11.noSSNExplaination" text="If no Social Security Number is available please select from the following explanations:"/></label>
				<div class="control-group">
					<select  class="input-large" name="reasonableExplanationForNoSSN" id="reasonableExplanationForNoSSN" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" >
			             <option value=""><spring:message code="ssap.page11.explainationText" text="Select Explanation"/></option>
			             <option value="RELIGIOUS_EXCEPTION"><spring:message code="ssap.page11.explanationType.religious" text="Religious Exception"/></option>
			      		 <option value="JUST_APPLIED"><spring:message code="ssap.page11.explanationType.justApplied" text="Just Applied"/></option>
				         <option value="ILLNESS_EXCEPTION"><spring:message code="ssap.page11.explanationType.illnessException" text="Illness Exception"/></option>
				         <option value="CITIZEN_EXCEPTION"><spring:message code="ssap.page11.explanationType.citizenExceptio" text="Citizen Exception"/></option>
					</select>
					<span class="errorMessage"></span>
				</div>
				<%-- <div class="control-group">
					<p>
						<spring:message code="ssap.page11.does" text="Does"/>&nbsp;
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page11.individualMandateExemption" text=" have an individual mandate exemption ? "/>
					</p>
					<div class="margin5-l">
						<label>
							<input type="radio" name="fnlnsExemption" id="fnlnsExemptionIndicatorYes" value="yes" onchange="hasAnIdividualMandateExemptionP61();">
							<spring:message code="ssap.page11.yes" text="Yes"/>
						</label>
						<label>
							<input type="radio" name="fnlnsExemption" id="fnlnsExemptionIndicatorNo" value="no" onchange="hasAnIdividualMandateExemptionP61();">
							<spring:message code="ssap.page10.planToFileNo" text="No" />
						</label>
					</div>
				</div> --%><!-- control-group -->
				
				<%-- <div class="control-group">
					<label class="required control-label">
						<spring:message code="ssap.page11.exemptionID" text="Exemption ID:"/>
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						<span aria-label="Required!"></span>
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" class="input-large" id="exemptionId" name="exemptionId" placeHolder="<spring:message code="ssap.page11.placeholder.exemptionId" text="Exemption Id"/>" parsley-error-message="Please Enter Exemption Id"/>
						<ul id="parsley-exception-id" class="parsley-error-list hide">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul>
					</div><!-- end of controls-->
				</div> --%><!-- control-group ends -->
			</div>
			</div>
		</form>
	</div>
</div>
      