import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitSetComponent } from './submit-set.component';

describe('SubmitSetComponent', () => {
    let component: SubmitSetComponent;
    let fixture: ComponentFixture<SubmitSetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SubmitSetComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SubmitSetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
