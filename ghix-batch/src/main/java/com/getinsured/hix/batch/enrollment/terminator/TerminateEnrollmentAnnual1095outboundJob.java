package com.getinsured.hix.batch.enrollment.terminator;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.Enrollment1095PdfNotificationService;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095Out;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.util.DateUtil;
//import com.getinsured.hix.batch.util.GhixBatchConstants;

public class TerminateEnrollmentAnnual1095outboundJob  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollmentAnnual1095outboundJob.class);
	private Enrollment1095Out enrollment1095Out;
	private Enrollment1095PdfNotificationService enrollment1095PdfNotificationService;
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		
		Map<String, String> emailDataMap=new HashMap<>();  
		
		if(enrollment1095Out!=null){
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_PDF_BatchEmail.GENERATED_DATE, DateUtil.dateToString(new Date(), EnrollmentConstants.FINANCE_DATE_FORMAT));
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_PDF_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_1095_OUT_PDF_JOB);
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_PDF_BatchEmail.INTIAL_PDF_COUNT, enrollment1095Out.getInitialPdfCount()+"");
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_PDF_BatchEmail.CORRECTED_PDF_COUNT, enrollment1095Out.getCorrectedPdfCount()+"");
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_PDF_BatchEmail.FAILED_RECORD_COUNT, enrollment1095Out.getSkippedPdfCount()+"");
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_PDF_BatchEmail.RESEND_PDF, enrollment1095Out.getResendPdfCount()+"");
			emailDataMap.put(EnrollmentConstants.Enrollment_1095_PDF_BatchEmail.VOID_PDF_COUNT, enrollment1095Out.getVoidPdfCount()+"");
			
		}
		try{
			enrollment1095PdfNotificationService.sendPdfGenerationEmail(emailDataMap);
		}catch(Exception e){
			LOGGER.error("Error while sending status email for "+EnrollmentConstants.ENROLLMENT_1095_OUT_PDF_JOB + " :: "+e.toString());
		}finally{
			if(enrollment1095Out!=null){
				enrollment1095Out.resetCorrectedPdfCount();
				enrollment1095Out.resetInitialPdfCount();
				enrollment1095Out.resetResendPdfCount();
				enrollment1095Out.resetSkippedPdfCount();
				enrollment1095Out.resetVoidPdfCount();
				enrollment1095Out.clearEnrollmentIdList();
			}
		}
		LOGGER.info("Terminate");
		return RepeatStatus.FINISHED;

}
	public Enrollment1095Out getEnrollment1095Out() {
		return enrollment1095Out;
	}
	public void setEnrollment1095Out(Enrollment1095Out enrollment1095Out) {
		this.enrollment1095Out = enrollment1095Out;
	}
	public Enrollment1095PdfNotificationService getEnrollment1095PdfNotificationService() {
		return enrollment1095PdfNotificationService;
	}
	public void setEnrollment1095PdfNotificationService(
			Enrollment1095PdfNotificationService enrollment1095PdfNotificationService) {
		this.enrollment1095PdfNotificationService = enrollment1095PdfNotificationService;
	}


	
	
}
