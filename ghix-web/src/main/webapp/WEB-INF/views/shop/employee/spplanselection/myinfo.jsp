<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util.tld" prefix="util"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />

<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${employeeApplication.name}<span></h1>
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <util:myStuff employeeApplication="${employeeApplication}"></util:myStuff>
      <util:quickLinks employeeApplication="${employeeApplication}"></util:quickLinks>
    </div><!--/span3-->
    <div class="span9" id="rightpanel">      
      <div class="header">
      	<h4 class="pull-left">My Information</h4> 
        	<c:choose>
			    <c:when test="${onlyViewAllowed == 'Y'}">
			       <a href="#" id="myinfo-edit" class="btn btn-small pull-right" alt="Edit" aria-describedby="description-edit">Edit</a>
			    </c:when>
			    <c:otherwise>
			       <a href="<c:url value='/shop/employee/spplanselection/editmyinfo/${employeeApplication.id}'/>" id="myinfo-edit" class="btn btn-small pull-right" alt="Edit" aria-describedby="description-edit">Edit</a>
			    </c:otherwise>
			</c:choose>
        	    
      </div>   	
        <div class="gutter10">
          <div class="row-fluid">
            <p>The following information was entered by your employer and verified by you.</p>
			<div id="myinfo" class="row-fluid">
				<dl  class="dl-horizontal">
					<dt>Name</dt>
					<dd id="name">
						${empDetailsApplication.firstName} ${empDetailsApplication.lastName}
						<c:if test="${empDetailsApplication.suffix != null && empDetailsApplication.suffix != ''}">
							${empDetailsApplication.suffix}
						</c:if>
					</dd>
					<dt>Home Address</dt>
					<dd id="address"><address>
			                 ${empDetailsApplication.location.address1} <br>
			                 ${empDetailsApplication.location.address2} ${(empty empDetailsApplication.location.address2) ? '' : '<br>'}
			                 ${empDetailsApplication.location.city}, 
			                 ${empDetailsApplication.location.state} &nbsp;
			                 ${empDetailsApplication.location.zip}<br>
			                 ${fn:toUpperCase(fn:substring(empDetailsApplication.location.county, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.location.county, 1, -1))}</address></dd>
					<dt>Gender</dt>
					<dd id="gender">
					<c:choose>
					    <c:when test="${empDetailsApplication.gender == null}">
					       <weak>N/A</weak>
					    </c:when>
					    <c:otherwise>
					       <strong>${fn:toUpperCase(fn:substring(empDetailsApplication.gender, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.gender, 1, -1))}</strong>
					    </c:otherwise>
					</c:choose>
					</dd>
					<dt>Marital Status</dt>
				    <dd>${martialStatus}</dd>
					<dt>Date of Birth</dt>
					<dd id="d-o-b"><fmt:formatDate pattern="MM/dd/yyyy" value="${empDetailsApplication.dob}" var="dob" /> ${dob}</dd>
					<dt>Tax ID Number</dt>
					<dd id="tax-id">
						<c:if test="${empDetailsApplication.ssn != null && empDetailsApplication.ssn != ''}">
						<c:set var="ssnParts" value="${fn:split(empDetailsApplication.ssn,'-')}" />
						***-**-${ssnParts[2]}
						</c:if>	
					</dd>
					<dt>Email Address</dt>
					<dd id="email-id">${empDetailsApplication.email}</dd>
					<dt>Phone Number</dt>
					<dd id="phone">${empDetailsApplication.contactNumber}</dd>
					<dt>Tobacco User</dt>
					<dd id="Tobacco-user">
						<c:choose>
		               	  	<c:when test="${empDetailsApplication.smoker == null}">
								<weak>N/A</weak>
							</c:when>
							<c:otherwise>
		               			<strong>${fn:toUpperCase(fn:substring(empDetailsApplication.smoker, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.smoker, 1, -1))}</strong>
		               		</c:otherwise>
						</c:choose>
					</dd>
					<dt style="word-wrap:break-word;text-align:left;"><spring:message  code="label.tobaccoCessationPrg"/></dt>
					<dd>
					<c:choose>
					    <c:when test="${empDetailsApplication.tobaccoCessPrg == null || empDetailsApplication.tobaccoCessPrg == ''}">
					       <weak>N/A</weak>
					    </c:when>
					    <c:otherwise>
					       <strong>${fn:toUpperCase(fn:substring(empDetailsApplication.tobaccoCessPrg, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.tobaccoCessPrg, 1, -1))}</strong>
					    </c:otherwise>
					</c:choose>
					</dd>
					<dt>Member of Federally Recognized Tribe</dt>
					<dd id="fedral-tribe">
						<c:choose>
						    <c:when test="${empDetailsApplication.nativeAmr == null}">
						       <weak>N/A</weak>
						    </c:when>
						    <c:otherwise>
						       <strong>${fn:toUpperCase(fn:substring(empDetailsApplication.nativeAmr, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.nativeAmr, 1, -1))}</strong>
						    </c:otherwise>
						</c:choose>
					</dd>
					<c:if test="${frtDocId !='none'}">
						<dt><spring:message  code="label.uploadsupportingdocument"/></dt>
		       			<dd><strong>${frtDocName} </strong> <a href="javascript:downloadFile('${frtDocId}')">download</a></dd>
					</c:if>
				</dl>
			</div>
          </div>
      </div>
  </div><!--/rightpanel--> 
  </div><!--/row-fluid-->        
</div><!--/main .gutter10-->

<c:if test="${isEnrollmentEnded }">
<jsp:include page="../terminateemployercoverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplicationId.id}" />
	<jsp:param name="nativeAmr" value="${nativeAmr}" />
</jsp:include>  
</c:if>
<c:if test="${!isEnrollmentEnded }">
<jsp:include page="../waive_employer_coverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
</jsp:include>
</c:if>

<script>
$(".inactive").click(function(e) {
	e.preventDefault();
});

function downloadFile(fileId){
	var  contextPath =  "<%=request.getContextPath()%>";

	var url = contextPath+"/shop/employee/spplanselection/downloadDocument?udId="+fileId;
	var subUrl =  '<c:url value="'+url+'"/>';
	window.open(subUrl,"Supported Document");
}
</script>