<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<div class="gutter10">
       <div class="row-fluid">
            <ul class="page-breadcrumb">
                <li><a href="<c:url value="/planmgmt/issuer/indvmembers/" />">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="<c:url value="/planmgmt/issuer/indvmembers/" />">Members</a></li>
                <li>Manage Individual</li>
            </ul><!--page-breadcrumb ends-->
            <h1><issuerLogo:view issuerId="${issuerid}" />${enrollee.firstName} <c:if test="${enrollee.middleName != ''}">${enrollee.middleName} </c:if>${enrollee.lastName} <small>Active member since <fmt:formatDate value="${enrollee.effectiveStartDate}" pattern=" MMM dd, yyyy"/></small></h1>
        </div>

<div class="row-fluid">
    <div class="span3" id="sidebar">
		<div class="header">
            	<h4 class="margin0">About this Member</h4>
         </div> 
                <ul class="nav nav-tabs nav-stacked">
     				<li class="active"><a href="#">Member Details</a></li>
                </ul>                
            
    </div><!--end of .span3  -->
	<div class="span9" id="rightpanel">
    	<div class="header">
            <h4 class="span10">Member Details</h4>
        </div>
		<table class="table table-condensed table-border-none">
			<tr>
				<td class="txt-right span3"><spring:message code='label.memberName'/></td>
				<td><strong>${enrollee.firstName} <c:if test="${enrollee.middleName != ''}">${enrollee.middleName} </c:if>${enrollee.lastName}</strong></td> 
			</tr>			
			<tr>
				<td class="txt-right">Member Id</td>
				<td><strong>${enrollee.exchgIndivIdentifier}</strong></td>
			</tr>
			<tr>
				<td class="txt-right"><spring:message  code='label.planName'/></td>
				<td><strong>${enrollee.enrollment.planName}</strong></td>
			</tr>
			<tr>
				<td class="txt-right"><spring:message  code='label.planNumber'/></td>
				<td><strong>${enrollee.enrollment.CMSPlanID}</strong></td>
			</tr>
			<tr>
				<td class="txt-right"><spring:message  code='label.planLevel'/></td>
				<td><strong>${enrollee.enrollment.planLevel}</strong></td>
			</tr>
			<tr>
				<td class="txt-right"><spring:message code='label.effectiveDate'/></td>
				<td><strong><fmt:formatDate value="${enrollee.effectiveStartDate}" pattern=" MMM dd, yyyy"/></strong></td>
			</tr> 
			 <tr>
				<td class="txt-right"><spring:message code='label.policyNumber'/></td>
				<td><strong>${enrollee.healthCoveragePolicyNo}</strong></td>
			</tr>	 		
			<tr>
				<td class="txt-right"><spring:message code='label.subscriberName'/></td>
				<td><strong>${enrollee.enrollment.subscriberName}</strong></td>
			</tr>
			 <tr>
				<td class="txt-right"><spring:message code='label.relationshipToSubscriber'/></td>						
				<td><strong>${enrollee.relationshipToSubscriberLkp.lookupValueLabel}</strong></td>				
			</tr> 
			 <tr>
				<td class="txt-right"><spring:message code='label.enrollmentStatus'/></td>
				<td><strong>${enrollee.enrollment.enrollmentStatusLkp.lookupValueLabel}</strong></td>
			</tr> 
			<tr>
				<td class="txt-right"><a class="btn btn-primary btn-small" href="<c:url value="/planmgmt/issuer/indvmembers" />"><spring:message code='label.back'/></a></td>
				<td></td>
			</tr>
		</table>
				
	</div>
   	</div>
</div>
<!-- end of .row-fluid -->
