package com.getinsured.hix.planmgmt.service.email;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.util.PlanMgmtConstants;

@Component
public class IssuerPlanCertificationEmail {
	
	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(IssuerPlanCertificationEmail.class);

	private Plan planObj;
	private Issuer issuerObj;
	private AccountUser user;
	private IssuerRepresentative issuerRepObj;
	

	@Value("#{configProp['appUrl']}")
	private String appUrl;

	public void setIssuerObj(final Issuer issuerObj){
		this.issuerObj = issuerObj;
	}

	public void setPlanObj(final Plan planObj){
		this.planObj = planObj;
	}

	public void setUser(final AccountUser user){
		this.user = user;
	}
	
	public void setIssuerRepObj(IssuerRepresentative issuerRepObj) {
		this.issuerRepObj = issuerRepObj;
	}

	public Map<String, Object> getSingleData(){
		
		final Map<String, Object> data = new HashMap<String, Object>();
		final DateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");

		data.put(PlanMgmtConstants.INSURANCE_EXCHANGE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.EXCHANGE_ADDRESS, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		data.put(PlanMgmtConstants.EXCHANGE_WEBSITE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_URL));
		data.put("primaryContact", user.getFullName());
		data.put(PlanMgmtConstants.PLAN_NUMBER, planObj.getIssuerPlanNumber() == null ? "": planObj.getIssuerPlanNumber().trim());
		data.put(PlanMgmtConstants.HEALTHEXCHANGE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_URL, appUrl == null ? "" : appUrl);
		data.put("verifyEndDate", dateFormat.format(DateUtils.addDays(new TSDate(), 30)));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_PHONE));
		data.put(PlanMgmtConstants.HEALTHEXCHANGE_TEAM, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		
		data.put(PlanMgmtConstants.ISSUER_MARKETING_NAME, issuerObj.getName() == null ? "": issuerObj.getName().trim());
		data.put(PlanMgmtConstants.CITY, issuerObj.getCity());
		data.put(PlanMgmtConstants.STATE, issuerObj.getState());
		data.put(PlanMgmtConstants.ZIPCODE, issuerObj.getZip());
		data.put(PlanMgmtConstants.SYSTEM_DATE, dateFormat.format(new TSDate()));
		
		String firstName = issuerRepObj.getFirstName() == null ? "" : issuerRepObj.getFirstName().trim();
		String lastName = issuerRepObj.getLastName() == null ? "" : issuerRepObj.getLastName().trim();
		
		String address1 = issuerObj.getAddressLine1() == null ? "" : issuerObj.getAddressLine1().trim();
		String address2 = issuerObj.getAddressLine2() == null ? "" : issuerObj.getAddressLine2().trim();
		String address ="";
		if(StringUtils.isNotBlank(address2)){
			address = address1 + PlanMgmtConstants.BREAK_LINE + address2;
		}else{
			address = address1;
		}
		data.put(PlanMgmtConstants.ISSUER_ADDRESS, address);
		
		data.put(PlanMgmtConstants.ISSUER_REPRESENTATIVE, firstName + " " + lastName);

		final Map<String, Object> emailHeaders = new HashMap<String, Object>();

		emailHeaders.put("IssuerId", String.valueOf(issuerObj.getId()));
		emailHeaders.put("UserId", String.valueOf(user.getId()));
		emailHeaders.put("To", user.getEmail());
		emailHeaders.put("id", Integer.toString(planObj.getId()));
		emailHeaders.putAll(data);
		LOGGER.debug("Notification Template Data :-" + SecurityUtil.sanitizeForLogging(String.valueOf(data.toString())));
		return emailHeaders;
	}


}
