<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>

<%  
String relyingParty = CharacterEncoder.getSafeText(request.getParameter("relyingParty")); 
if (relyingParty.equalsIgnoreCase("YHI_QA") ||
	relyingParty.equalsIgnoreCase("idssodev") ||
	relyingParty.equalsIgnoreCase("YHIUAT") ||
	relyingParty.equalsIgnoreCase("YHISIT") ||
	relyingParty.equalsIgnoreCase("YHIE2E1") ||
	relyingParty.equalsIgnoreCase("IDSTAGE") ||
	relyingParty.equalsIgnoreCase("YHISSODEV") ||
	relyingParty.equalsIgnoreCase("YHI")) {
 RequestDispatcher dispatcher = request.getRequestDispatcher("login_yhi.jsp");
 dispatcher.forward(request, response);
}
else if (relyingParty.startsWith("idalink") ||
		 relyingParty.startsWith("IDALINK")) {
	RequestDispatcher dispatcher = request.getRequestDispatcher("login_idalink.jsp");
	dispatcher.forward(request, response);
}
%>
