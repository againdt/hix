import { DataKeepService } from './../../../services/data-keep.service';
import { FetchService } from './../../../services/fetch.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { ElementRef, HostListener } from '@angular/core';

import { ProviderInfo, ProviderFullData } from "../../../models/data.model";
import { DocSearchTypeaheadComponent } from "./../../doc-search-typeahead/doc-search-typeahead.component";

@Component({
    selector: "gi-doc-who",
    templateUrl: './doc-who.component.html',
    styleUrls: ["./doc-who.component.scss"]
})
export class DocWhoComponent {
    @ViewChild(DocSearchTypeaheadComponent) private docSearch: DocSearchTypeaheadComponent;
    _invalidZip: boolean = false;
    public alerts: any = [];
    public newAlert: any;

    @Input() listedDoctors: ProviderInfo[];
    @Input() zipcode: string;
    @Output() notifyZipcode: EventEmitter<string> = new EventEmitter<string>();
    public docSearchError$ = new Subject<string>();
    public docSays$ = new Subject<{ msg: string; index?: number }>();
    public providerType = "doctor";

    @HostListener("document:click", ["$event"])
    clickout(event) {
        this.alerts = [];
        this.newAlert = null;
    }

    constructor(
        public datakeep: DataKeepService,
        public fetchdata: FetchService,
        private eRef: ElementRef
    ) {}

    public ngOnInit() {

        this.docSays$.subscribe(val => {
            if (val.msg) {
                this.newAlert = { type: "danger", msg: val.msg };
            }
        });
    }

    public receiveDoctor(doctor: ProviderFullData) {
        // console.log(doctor)
        const providerinfo: ProviderInfo = {
            id: doctor.strenuus_id,
            name: doctor.name,
            specialty: doctor.specialty,
            address: doctor.providerAddress[0].addline1,
            city: doctor.providerAddress[0].city,
            state: doctor.providerAddress[0].state,
            zip: doctor.providerAddress[0].zip,
            providerType: doctor.providerType,
            locationId: doctor.locationId
        };
        
        if(doctor.providerAddress[0].addLine2) {
        	providerinfo.address += ' ' + doctor.providerAddress[0].addLine2;
        }
        
        const rsp = this.datakeep.updatePersonalizedState(
            "DOCTOR",
            "ADD",
            providerinfo
        );
        if (rsp.status === "failure") {
            // temp implementation. need to handle multiple error pop
            this.docSays$.next({ msg: rsp.msg });
        } else if (rsp.status === "success" && rsp.msg !== "") {
            let me = this;
            setTimeout(function() {
                me.docSays$.next({ msg: rsp.msg });
            }, 0);
        }
		
		const ariaLabel = 'You added ' + providerinfo.providerType + ' ' + providerinfo.name + ' ' 
			+ providerinfo.specialty  + ' in ' + providerinfo.address + ' ' + providerinfo.city + ' ' + providerinfo.state + ' ' + providerinfo.zip;
			
		this.datakeep.updateAriaLabel('provider-search-field', ariaLabel);
    }

    public handleCardDestroy(doctor: ProviderInfo) {
        this.datakeep.updatePersonalizedState("DOCTOR", "REMOVE", doctor);
        
        const ariaLabel = 'You removed ' + doctor.providerType + ' ' + doctor.name + ' ' 
			+ doctor.specialty  + ' in ' + doctor.address + ' ' + doctor.city + ' ' + doctor.state + ' ' + doctor.zip;
        this.datakeep.updateAriaLabel('provider-search-field', ariaLabel);
    }

    public updateZipcode(zipcode: string): void {
        //2. check if the zipcode has letters in it
        if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipcode)) {
            this.notifyZipcode.emit(zipcode);
        } else {
            this._invalidZip = true;
        }
    }

    public validateZip(zipcode: string): void {
        //1 check if the zipcode has 5 digits
        if (zipcode.length < 5) {
            this._invalidZip = false;
            return;
        }
        //2. check if the zipcode has letters in it
        if (/(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipcode)) {
            //3. check if the zipcode is a valid WA zip
            this.datakeep.validateZip(zipcode).subscribe(
                response => {
                    if (response.status === 200 && (<any>response)._body) {
                        this._invalidZip = false;
                    } else {
                        this._invalidZip = true;
                    }
                },
                err => {
                    console.error(err);
                    this._invalidZip = true;
                }
            );
        } else {
            this._invalidZip = true;
        }
    }
}
