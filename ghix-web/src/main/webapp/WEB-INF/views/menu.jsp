<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.getinsured.hix.platform.util.ui.MenuViewPreparer"%>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@page import="com.getinsured.hix.model.DesignateBroker"%>
<%@page import="org.springframework.web.context.request.RequestScope"%>
<%@page import="com.getinsured.hix.model.entity.Assister"%>
<%@page import="com.getinsured.hix.model.entity.DesignateAssister"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<link rel="stylesheet" type="text/css" href="<gi:cdnurl value="/resources/css/general.css" />" media="screen" />

<%
	List<String> roleNamesList = (List<String>) request.getAttribute("roleNames");
	Map<String, List<MenuViewPreparer.MenuItem>> menuItemMap = null;
	List<MenuViewPreparer.MenuItem> menuItemList = null;
	String currRoleName=null;
	String myRole = null;
	if (roleNamesList.size() > 0){
		myRole = roleNamesList.get(0).replaceAll("_menu", "");
		myRole = Character.toUpperCase(myRole.charAt(0)) + myRole.substring(1);
		// this is done for multiple roles
		menuItemMap = new LinkedHashMap<String, List<MenuViewPreparer.MenuItem>>();

		for (int i=0 ; i<roleNamesList.size(); i++){
			List<MenuViewPreparer.MenuItem> menuItemLists = (List<MenuViewPreparer.MenuItem>) request.getAttribute(roleNamesList.get(i));
			menuItemMap.put(roleNamesList.get(i), menuItemLists);
		}

		// as of now this will read only 0th element as we are assuming only one role. Later moduleName check will be included to land based on module name
		// If user has multiple roles, he/she will be asked to pick a role. In most cases, we would pass the module parameter which would determine the role for the user.
		menuItemList = menuItemMap.get(roleNamesList.get(0));

		currRoleName = roleNamesList.get(0);
		if (menuItemList == null){
			throw new ServletException ("No menuList declared for " + roleNamesList.get(0));
		}
	}

	String currPageNew = (String) request.getAttribute("javax.servlet.forward.request_uri");
	Broker designatedBrokerProfile = (Broker) (request.getAttribute("designatedBrokerProfile"));
	DesignateBroker db = (DesignateBroker) (request.getAttribute("designateBroker"));
	String isBrokerSwitchToEmplView =  (String) request.getSession().getAttribute("isBrokerSwitchToEmplView");
	String isBrokerSwitchToIndvView =  (String) request.getSession().getAttribute("isBrokerSwitchToIndvView");
	String isUserSwitchToOtherView =  (String) request.getSession().getAttribute("isUserSwitchToOtherView");
	Assister designatedAssisterProfile = (Assister) (request.getAttribute("designatedAssisterProfile"));
	DesignateAssister da = (DesignateAssister) (request.getSession().getAttribute("designateAssister"));
	Boolean brokerConnectEnabled = Boolean.valueOf(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.IEX_BROKER_CONNECT_ENABLED));

	if(StringUtils.isBlank(isUserSwitchToOtherView)){
		isUserSwitchToOtherView="N";
	}else if(StringUtils.isBlank(isBrokerSwitchToIndvView)){
		isBrokerSwitchToIndvView="N";
	} else if(StringUtils.isBlank(isBrokerSwitchToEmplView)){
		isBrokerSwitchToEmplView="N";
	}

	String switchToResourceName  =  (String) request.getSession().getAttribute("switchToResourceName");
	String switchToEmplName =  (String) request.getSession().getAttribute("switchToEmplName");
	String switchToIndvName =  (String) request.getSession().getAttribute("switchToIndvName");
	String switchToModuleId =  request.getSession().getAttribute("switchToModuleId")+"";
	String switchToModuleName  =  (String) request.getSession().getAttribute("switchToModuleName");

	if(!StringUtils.isBlank(switchToResourceName)){
		 if(switchToResourceName.equalsIgnoreCase("SettingUpNewEmployer") && switchToModuleId.equalsIgnoreCase("-1")){
			switchToResourceName = "Setting up new Employer";
			request.getSession().setAttribute("switchToResourceName", "SettingUpNewEmployer");
			request.getSession().setAttribute("switchToModuleId", switchToModuleId);
		}else if(switchToModuleName.equalsIgnoreCase("agency_manager")){
			switchToResourceName="Viewing Agency Account ("+switchToResourceName+")";
		}else{
			switchToResourceName="Viewing "+(myRole.equals("Broker")?"Agent":myRole)+" Account ("+switchToResourceName+")";
		}


	}else {
		switchToResourceName="";
	}

	if(!StringUtils.isBlank(switchToEmplName)){
		switchToEmplName="Viewing "+switchToEmplName;
	}else {
		switchToEmplName="";
	}

	if(!StringUtils.isBlank(switchToIndvName)){
		switchToIndvName="Viewing "+switchToIndvName;
	}else {
		switchToIndvName="";
	}

	/* as per requirement in JIRA no HIX-1245
	   Hide this menu item if the employer status is Approved or Conditionally Approved.
	*/
	String employerEligibilityStatus = (String) request.getSession().getAttribute("employerEligibilityStatus");
	String employerEnrollmentStatus = (String) request.getSession().getAttribute("employerEnrollmentStatus");
	String EmployerCoverageRenewalStarted = (String) request.getSession().getAttribute("EmployerCoverageRenewalStarted");
	String upcomingEnrollment = (String) request.getSession().getAttribute("upcomingEnrollment");
	String EmployerRenewalDone = (String) request.getSession().getAttribute("EmployerRenewalDone");
	String enableConsumerSignUp = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ENABLE_CONSUMER_SIGNUP);
    String stateCode=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
    String isConsentPageEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_IS_CONSENT_PAGE_ENABLED);
%>

<script type="text/javascript">
$(function(){
	$(".validateDedesignate").click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        if (href.indexOf('#') != 0) {
           $('<div id="modal" class="modal jsValidateDedesignateModal"><div class="modal-header" id="header" style="border-bottom:0;"><button type="button" class="modalClose close" aria-hidden="true">x</button></div><div class="modal-body"><iframe   id="dedesignatePopup" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:120px;"></iframe></div></div>').modal({backdrop:"static",keyboard:"false"});

           document.getElementById('dedesignatePopup').onload = function() {
               if( $('#dedesignatePopup').contents().find('.application-error').length > 0){
              	 var errorText = $($( $( $('#dedesignatePopup').contents().find('.application-error')[0]).next( )[0]).find('span')[0]).text().trim() ;
            	 var errorHeading = $($( $('#dedesignatePopup').contents().find('.application-error')[0]).prev()[0]).text().trim();
            	 var parentElement = $('#dedesignatePopup').parent();
            	 parentElement.empty();
            	 parentElement.append('<h3>'+errorHeading+' </h3>');
            	 parentElement.append('<div> <span>'+ errorText +' </span> </div>');
               }
          };

		}
	});
});

$('.modalClose').live('click',function(){
	closeLightboxOnCancel();
});

function closeLightbox(){
	$("#modal, .modal-backdrop").remove();
	window.location.reload();
}

function closeLightboxOnCancel(){
	$("#modal, .modal-backdrop").remove();
}
</script>

<c:set var="currPageNew" value="<%=currPageNew%>" />
<c:set var="currRoleName" value="<%=currRoleName%>" />
<c:set var="menuItemList" value="<%=menuItemList%>" />
<c:set var="menuItemMap" value="<%=menuItemMap%>" />
<c:set var="roleNamesList" value="<%=roleNamesList%>" />
<c:set var="designatedBrokerProfile" value="<%=designatedBrokerProfile%>" />
<c:set var="isUserSwitchToOtherView" value="<%=isUserSwitchToOtherView%>" />
<c:set var="switchToResourceName" value="<%=switchToResourceName%>" />

<c:set var="isBrokerSwitchToEmplView" value="<%=isBrokerSwitchToEmplView%>" />
<c:set var="switchToEmplName" value="<%=switchToEmplName%>" />
<c:set var="isBrokerSwitchToIndvView" value="<%=isBrokerSwitchToIndvView%>" />
<c:set var="switchToIndvName" value="<%=switchToIndvName%>" />
<c:set var="db" value="<%=db%>" />
<c:set var="da" value="<%=da%>" />
<c:set var="employerEligibilityStatus" value="<%=employerEligibilityStatus%>" />
<c:set var="employerEnrollmentStatus" value="<%=employerEnrollmentStatus%>" />
<c:set var="EmployerCoverageRenewalStarted" value="<%=EmployerCoverageRenewalStarted%>" />
<c:set var="upcomingEnrollment" value="<%=upcomingEnrollment%>" />
<c:set var="EmployerRenewalDone" value="<%=EmployerRenewalDone%>" />
<c:set var="enableConsumerSignUp" value="<%=enableConsumerSignUp%>" />
<c:set var="switchToModuleNameVar" value="<%=switchToModuleName%>" />
<c:set var="stateCode" value="<%=stateCode%>" />
<c:set var="brokerConnectEnabled" value="<%=brokerConnectEnabled%>" />
<c:set var="isConsentPageEnabled" value="<%=isConsentPageEnabled%>" />

<c:if test="${currPageNew ne '/hix/'}">

		<c:if test="${isUserSwitchToOtherView =='Y'}">
			<div id="navtopview">
				<div>
					<p><strong>${switchToResourceName}</strong></p>
					<a href="<c:url value="/account/user/switchNonDefRole/${userActiveRoleName}"/>" class="btn btn-mini btn-primary">My Account</a>
				</div>
			</div>
		</c:if>
		<c:if test="${isConsentPageEnabled != 'Y'}">
		<c:if test="${designatedBrokerProfile != null && db!=null && db.status != 'InActive'}">
			<div class="view-broker pull-right">
				<div class="dropdown">
					<c:set var="encBrokerId" ><encryptor:enc value="${designatedBrokerProfile.id}" isurl="true"/> </c:set>
				   <c:if test="${currRoleName eq 'individual_menu'}">
				   		<a data-toggle="dropdown" class="dropdown-toggle"><spring:message code="label.brk.YourAgent"/> <i class="icon-cog"></i><span class="caret"></span> </a>
				   </c:if>
					   <ul class="dropdown-menu pull-right">
						 <li><a href="#"><i class="icon-user"></i> ${designatedBrokerProfile.user.firstName} ${designatedBrokerProfile.user.lastName}</a></li>

						 <c:if test="${designatedBrokerProfile.contactNumber != null}">
						 	<li><a href="javascript:void(0)"><i class="icon-phone"></i> (${fn:substring(designatedBrokerProfile.contactNumber,0,3)}) ${fn:substring(designatedBrokerProfile.contactNumber,3,12)}</a></li>
						 </c:if>

						 <c:if test="${db.status == 'Pending'}">
							<li><a href="#" class="alert alert-info margin0"><spring:message code="label.brk.DesingationStatus"/> ${db.status}</a></li>
						 </c:if>

						 <li><a href="/hix/broker/viewbrokerprofile/${encBrokerId}"><spring:message code="label.brk.Profile"/></a></li>

						 <c:if test="${db.status == 'Pending' || db.status == 'Active'}">
						 	<c:if test="${currRoleName ne 'employee_menu'}">
									<li><a id="dedesignateLink" class="validateDedesignate" href="<c:url value="/broker/dedesignatePopup?fromLightbox=false&brokerId=${encBrokerId}"/>"><spring:message code="label.brk.DeDesignateBrk"/></a></li>
							</c:if>
						 </c:if>
						 <!-- <li><a href="#">Send a Message</a></li> -->

						 </ul>
				</div>
			</div>
            <div class="clearfix"></div>
		</c:if>
		
		<c:if test="${designatedAssisterProfile != null && da!=null && da.status != 'InActive'}">
			<div class="view-broker pull-right">
				<div class="dropdown">
					<c:set var="encAssisterId" ><encryptor:enc value="${designatedAssisterProfile.id}" isurl="true"/> </c:set>
				   <c:if test="${currRoleName eq 'individual_menu'}">
				   		<a data-toggle="dropdown" class="dropdown-toggle"><spring:message code="label.counselor.yourcounselor"/> <i class="icon-cog"></i><span class="caret"></span> </a>
				   </c:if>
					   <ul class="dropdown-menu pull-right">
						 <li><a href="#"><i class="icon-user"></i> ${designatedAssisterProfile.firstName} ${designatedAssisterProfile.lastName}</a></li>

						 <c:if test="${designatedAssisterProfile.primaryPhoneNumber != null}">
						 	<li><a href="javascript:void(0)"><i class="icon-phone"></i> (${fn:substring(designatedAssisterProfile.primaryPhoneNumber,0,3)}) ${fn:substring(designatedAssisterProfile.primaryPhoneNumber,3,12)}</a></li>
						 </c:if>

						 <c:if test="${da.status == 'Pending'}">
							<li><a href="#" class="alert alert-info margin0"><spring:message code="label.counselor.designationstatus"/> ${da.status}</a></li>
						 </c:if>

						 <li><a href="/hix/entity/assister/viewAssisterProfile/${encAssisterId}"><spring:message code="label.counselor.profile"/></a></li>

						 <c:if test="${da.status == 'Pending' || da.status == 'Active'}">
						 	<c:if test="${currRoleName ne 'employee_menu'}">
									<li><a id="dedesignateAssisterLink" class="validateDedesignate" href="<c:url value="/entity/assister/dedesignateAssisterPopup?firstName=${designatedAssisterProfile.user.firstName}&lastName=${designatedAssisterProfile.user.lastName}&fromLightbox=false&assisterId=${encAssisterId}"/>"><spring:message code="label.counselor.dedesignate"/></a></li>
							</c:if>
						 </c:if>
						 <!-- <li><a href="#">Send a Message</a></li> -->

						 </ul>
				</div>
			</div>
            <div class="clearfix"></div>
		</c:if>


		<c:if test="${sessionScope.showAgentPromotion != null && stateCode != 'MN'}">
			<c:if test="${stateCode != 'CA'}">
				<div class="agent-promotion-circle pull-right">
					<a id="myModalLabel" class="button" href="#agentPromotion" data-toggle="modal">
						<img src="<c:url value="/resources/images/headset-white.png"/>" alt="Agent Promotion icon">
						<span><spring:message code="indportal.agentPromotion.workToget"/></span>
					</a>
				</div>
			</c:if>
		</c:if>
		</c:if>
<c:choose>
	<c:when test="${currRoleName eq 'broker_menu'}">
		<c:choose>
			<c:when test="${designatedBrokerProfile != null && designatedBrokerProfile.certificationStatus == 'Certified'}">
				<c:set var="isMenuVisible" value="true" />
			</c:when>
			<c:otherwise>
				<c:set var="isMenuVisible" value="false" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${currRoleName eq 'assisterenrollmententity_menu'}">
		<c:choose>
			<c:when test="${assisterEnrollmentEntity != null && assisterEnrollmentEntity.registrationStatus != 'Pending' && assisterEnrollmentEntity.registrationStatus != 'Incomplete'}">
				<c:set var="isMenuVisible" value="true" />
			</c:when>
			<c:otherwise>
				<c:set var="isMenuVisible" value="false" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${currRoleName eq 'assister_menu'}">
		<c:choose>
			<c:when test="${designatedAssisterProfile != null && designatedAssisterProfile.certificationStatus == 'Certified'}">
				<c:set var="isMenuVisible" value="true" />
			</c:when>
			<c:otherwise>
				<c:set var="isMenuVisible" value="false" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${employerEligibilityStatus eq 'NEW' && currRoleName eq 'employer_menu' }">
		<c:set var="isMenuVisible" value="false" />
	</c:when>
	<c:when test="${currRoleName eq 'issuer_representative' }">
		<c:set var="isMenuVisible" value="false" />
	</c:when>
	<c:when test="${currRoleName eq 'csr_menu'}">
		<c:choose>
			<c:when test="${isUserFullyAuthorized != null && isUserFullyAuthorized=='false' }">
				<c:set var="isMenuVisible" value="false" />
			</c:when>
			<c:otherwise>
				<c:set var="isMenuVisible" value="true" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${currRoleName eq 'agency_manager_menu'}">
		<c:choose>
			<c:when test="${agencyCertificationStatus != null && agencyCertificationStatus != 'INCOMPLETE' }">
				<c:set var="isMenuVisible" value="true" />
			</c:when>
			<c:otherwise>
				<c:set var="isMenuVisible" value="false" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${currRoleName eq 'approvedadminstaffl1_menu'}">
		<c:choose>
			<c:when test="${adminStaffApprovalStatus != null}">
				<c:set var="isMenuVisible" value="true" />
			</c:when>
			<c:otherwise>
				<c:set var="isMenuVisible" value="false" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${currRoleName eq 'approvedadminstaffl2_menu'}">
		<c:choose>
			<c:when test="${adminStaffApprovalStatus != null}">
				<c:set var="isMenuVisible" value="true" />
			</c:when>
			<c:otherwise>
				<c:set var="isMenuVisible" value="false" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<c:set var="isMenuVisible" value="${showMenu == false ? 'false' : 'true'}" />
	</c:otherwise>
</c:choose>

<c:if test="${isMenuVisible == 'true'}">
<div class="gutter10">
	<div class="navbar hide" id="menu">
		<div class="navbar-inner">
				<ul class="nav navPhixbar" style="float:none;">
					<c:if test="${fn:contains(currPageNew ,'shop/employer')}">
						<li class="dropdown nmhide mshide"><a href="<c:url value="/shop/employer/dashboard"/>"  aria-haspopup="true" aria-expanded="false" title="Home"><i class="icon-home"></i><span class="aria-hidden">Dashboard</span></a></li>
					</c:if>
					<!-- build menus...  -->
					<c:forEach var="menuItem" items="${menuItemList}">
						<c:set var="currPage" value="${currPageNew}" />
						<c:if test="${not empty menuItem.subMenus}">
							<c:forEach items="${menuItem.subMenus}" var="subMenu">
								<c:if test="${subMenu.redirecturl eq currPageNew}">
									<c:set var="currPage" value="${menuItem.redirecturl}" />
								</c:if>
							</c:forEach>
						</c:if>
						<li class="<c:if test="${not empty menuItem.subMenus}">dropdown</c:if>
									<c:if test="${currPage eq menuItem.redirecturl}"> active</c:if>
									<c:if test="${(isUserSwitchToOtherView =='Y') && (switchToModuleNameVar eq 'agency_manager') && (menuItem.title eq 'My Delegations' || menuItem.title eq 'My Agent Profile')}">hide</c:if>"
>
							<c:choose>
								<c:when test="${(currPage eq menuItem.redirecturl)}">
									<c:choose>
										<c:when test="${(fn:contains(menuItem.url , '/hix/broker/dashboard')) && (currRoleName eq 'broker_menu' || currRoleName eq 'agency_manager_menu')}">
											<a href="${menuItem.url}"
												title="${menuItem.title}" <c:if test="${not empty menuItem.subMenus}">
												aria-haspopup="<c:if test="${not empty menuItem.subMenus}">true</c:if>" aria-haspopup="<c:if test="${empty menuItem.subMenus}">false</c:if>"
												aria-expanded="<c:if test="${not empty menuItem.subMenus}">false</c:if>" aria-expanded="<c:if test="${empty menuItem.subMenus}">false</c:if>"
												class="dropdown-toggle" data-toggle="dropdown"</c:if>
												onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Home', 1]);"> ${menuItem.caption}<c:if test="${not empty menuItem.subMenus}"> <i class="caret"></i></c:if></a>
										</c:when>
										<c:when test="${(fn:contains(menuItem.url , '/hix/broker/individuals')) && (currRoleName eq 'broker_menu')}">
											<a href="${menuItem.url}"
												title="${menuItem.title}" <c:if test="${not empty menuItem.subMenus}">
												class="dropdown-toggle" data-toggle="dropdown" </c:if>
												aria-haspopup="<c:if test="${not empty menuItem.subMenus}">true</c:if>" aria-haspopup="<c:if test="${empty menuItem.subMenus}">false</c:if>"
												aria-expanded="<c:if test="${not empty menuItem.subMenus}">false</c:if>" aria-expanded="<c:if test="${empty menuItem.subMenus}">false</c:if>"
												onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Delegations', 1]);">${menuItem.caption}<c:if test="${not empty menuItem.subMenus}" > <i class="caret"></i></c:if></a>
										</c:when>
										<c:otherwise>
											<a href="${menuItem.url}"
												title="${menuItem.title}"<c:if test="${not empty menuItem.subMenus}">
												class="dropdown-toggle"
												aria-haspopup="<c:if test="${not empty menuItem.subMenus}">true</c:if>" aria-haspopup="<c:if test="${empty menuItem.subMenus}">false</c:if>"
												aria-expanded="<c:if test="${not empty menuItem.subMenus}">false</c:if>" aria-expanded="<c:if test="${empty menuItem.subMenus}">false</c:if>"
												data-toggle="dropdown"</c:if>>${menuItem.caption}<c:if test="${not empty menuItem.subMenus}"> <i class="caret"></i></c:if></a>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<a href="${menuItem.url}"
										title="${menuItem.title}"<c:if test="${not empty menuItem.subMenus}">
										aria-haspopup="<c:if test="${not empty menuItem.subMenus}">true</c:if>" aria-haspopup="<c:if test="${empty menuItem.subMenus}">false</c:if>"
										aria-expanded="<c:if test="${not empty menuItem.subMenus}">false</c:if>" aria-expanded="<c:if test="${empty menuItem.subMenus}">false</c:if>"
										class="dropdown-toggle" data-toggle="dropdown"</c:if>>${menuItem.caption}<c:if test="${not empty menuItem.subMenus}"> <i class="caret"></i></c:if></a>
								</c:otherwise>
							</c:choose>
							<!-- build sub menus...  -->
							<c:if test="${not empty menuItem.subMenus}">

									<ul class="dropdown-menu">
										<c:forEach items="${menuItem.subMenus}" var="subMenu">
											<c:choose>
												<c:when test="${subMenu.url == '#'}">
													<li>
														<a data-content="Feature unavailable in this version of the product." data-placement="top" rel="popover" class="submenu" href="#" data-original-title="Note:">${subMenu.caption}</a>
													</li>
												</c:when>
												<%-- HIX-52382 - Hide Add New Individual Link in Enable Counsumer Sign Up is Not Y for Agent and Enrollment Counselor --%>
												<c:when test="${(subMenu.caption == 'Add New Individual') && (enableConsumerSignUp != 'Y') && ( currRoleName == 'broker_menu' || currRoleName == 'assister_menu' ) }">
													<%-- Blank here it will skip creating menu --%>
												</c:when>
												<%-- HIX-52382 - Ends here--%>
												<%-- HIX-106241 -Admin Staff drop downs of agency will be enabled only when agency is certified --%>
												<c:when test="${(fn:contains(subMenu.url , '/hix/agency#/viewadminstafflist') || fn:contains(subMenu.url , '/hix/agency#/addadminstaff')) && (currRoleName eq 'agency_manager_menu' && agencyCertificationStatus != 'CERTIFIED')}">
													<li><a href="javascript:void();" class="disabled" title="${subMenu.title}">${subMenu.caption}</a></li>
												</c:when>
												<%-- HIX-100565 -Last 2 drop downs of agency will behave same as how agent works --%>
												<c:when test="${((subMenu.url == '/hix/agency#/agencyDelegation') || (subMenu.url == '/hix/agency#/agencyBOB') || (subMenu.caption == 'Transfer Consumers')) && (currRoleName eq 'agency_manager_menu' && agencyCertificationStatus != 'CERTIFIED')}">
													<li><a href="javascript:void();" class="disabled" title="${subMenu.title}">${subMenu.caption}</a></li>
												</c:when>

												<c:when test="${((fn:contains(subMenu.url , '/hix/broker/individuals')) || (fn:contains(subMenu.url , '/hix/broker/bookofbusiness')) || (fn:contains(subMenu.url , '/hix/broker/dashboard'))) && (currRoleName eq 'agency_manager_menu')}">
														<c:choose>
															<c:when test="${designatedBrokerProfile.certificationStatus == 'Certified' && designatedBrokerProfile.status == 'Active'}">

																<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
															</c:when>
															<c:otherwise>
																<li><a href="javascript:void();" class="disabled" title="${subMenu.title}">${subMenu.caption}</a></li>
															</c:otherwise>
														</c:choose>
												</c:when>

												<c:when test="${(currRoleName eq 'broker_menu')}">
													<c:choose>
														<c:when test="${(designatedBrokerProfile.status == 'InActive') && (fn:contains(subMenu.url , '/hix/broker/individuals') || fn:contains(subMenu.url , '/hix/broker/bookofbusiness'))}">
															<li><a href="javascript:void();" class="disabled" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${(fn:contains(subMenu.url , '/hix/broker/individuals?desigStatus=Pending')) && (designatedBrokerProfile.certificationStatus == 'Certified')}">
																	<li><a href="${subMenu.url}" title="${subMenu.title}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Pending Delegation Requests', 1]);">${subMenu.caption}</a></li>
																</c:when>
																<c:when test="${(fn:contains(subMenu.url , '/hix/broker/bookofbusiness')) && (designatedBrokerProfile.certificationStatus == 'Certified')}">
																	<li><a href="${subMenu.url}" title="${subMenu.title}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Active Consumers', 1]);">${subMenu.caption}</a></li>
																</c:when>
																<c:when test="${(fn:contains(subMenu.url , '/hix/broker/individuals?desigStatus=InActive')) && (designatedBrokerProfile.certificationStatus == 'Certified')}">
																	<li><a href="${subMenu.url}" title="${subMenu.title}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Inactive Consumers', 1]);">${subMenu.caption}</a></li>
																</c:when>
																<c:when test="${(fn:contains(subMenu.url , '/hix/broker/viewcertificationinformation')) && (designatedBrokerProfile.certificationStatus == 'Certified')}">
																	<li><a href="${subMenu.url}" title="${subMenu.title}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Agent Information', 1]);">${subMenu.caption}</a></li>
																</c:when>
																<c:when test="${(fn:contains(subMenu.url , '/hix/broker/viewprofile/')) && (designatedBrokerProfile.certificationStatus == 'Certified')}">
																	<li><a href="${subMenu.url}" title="${subMenu.title}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Profile', 1]);">${subMenu.caption}</a></li>
																</c:when>
																<c:when test="${(fn:contains(subMenu.url , '/hix/broker/certificationstatus/')) && (designatedBrokerProfile.certificationStatus == 'Certified')}">
																	<li><a href="${subMenu.url}" title="${subMenu.title}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Certification Status', 1]);">${subMenu.caption}</a></li>
																</c:when>
																<c:when test="${(fn:contains(subMenu.url , '/hix/broker/helpondemand#participation')) && (designatedBrokerProfile.certificationStatus == 'Certified')}">
																	<c:if test="${brokerConnectEnabled}">
																		<li><a href="${subMenu.url}" title="${subMenu.title}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Profile', 1]);">${subMenu.caption}</a></li>
																	</c:if>
																</c:when>
																<c:when test="${(fn:contains(subMenu.url , '/hix/admin/broker/activitystatus')) && (designatedBrokerProfile.certificationStatus == 'Certified')}">
																	<li><a href="${subMenu.url}" title="${subMenu.title}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Top Nav Clicks', 'Click', 'Status', 1]);">${subMenu.caption}</a></li>
																</c:when>
																<c:otherwise>
																	<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
																</c:otherwise>
															</c:choose>
														</c:otherwise>

													</c:choose>
												</c:when>
												<c:when test="${(currRoleName eq 'approvedadminstaffl1_menu') && (fn:contains(subMenu.url , '/hix/agency#/viewagentlist') || fn:contains(subMenu.url , '/hix/agency#/agencyDelegation') || fn:contains(subMenu.url , '/hix/agency#/agencyBOB') || fn:contains(subMenu.url , '/hix/agency#/transferConsumerDelegations'))}">
													<c:choose>
														<c:when test="${agencyCertificationStatus == 'CERTIFIED' && adminStaffApprovalStatus == 'APPROVED' && adminStaffActivityStatus == 'Active'}">
															<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:when>
														<c:otherwise>
															<li><a href="javascript:void();" class="disabled" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:otherwise>
													</c:choose>
												</c:when>
												<c:when test="${(currRoleName eq 'approvedadminstaffl2_menu') && (fn:contains(subMenu.url, '/hix/agency#/viewagentlist') || fn:contains(subMenu.url, '/hix/broker/certificationapplication') || fn:contains(subMenu.url, '/hix/agency#/agencyDelegation') || fn:contains(subMenu.url , '/hix/agency#/transferConsumerDelegations')
													|| fn:contains(subMenu.url, '/hix/agency#/agencyBOB') || fn:contains(subMenu.url, '/hix/agency#/agencyInformation') || fn:contains(subMenu.url, '/hix/agency#/locationHour') || fn:contains(subMenu.url, '/hix/agency#/documentupload') || fn:contains(subMenu.url, '/hix/agency#/certificationstatus'))}">
													<c:choose>
														<c:when test="${agencyCertificationStatus == 'CERTIFIED' && adminStaffApprovalStatus == 'APPROVED' && adminStaffActivityStatus == 'Active'}">
															<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:when>
														<c:otherwise>
															<li><a href="javascript:void();" class="disabled" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:otherwise>
													</c:choose>
												</c:when>

												<c:when test="${(fn:contains(subMenu.url , '/hix/broker/certificationapplication')) && (currRoleName eq 'agency_manager_menu')}">
													<c:choose>
														<c:when test="${agencyCertificationStatus == 'CERTIFIED'}">
														<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:when>
														<c:otherwise>
															<li><a href="javascript:void();" class="disabled" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:otherwise>
													</c:choose>
												</c:when>
												<c:otherwise>

													<c:if test="${!(fn:contains(subMenu.url , '/hix/shop/employer/application/attestation') && (employerEligibilityStatus == 'CONDITIONALLY_APPROVED' || employerEligibilityStatus == 'APPROVED'))}">
														<c:if test="${(fn:contains(subMenu.url , '/hix/shop/employer/planselection/home')) && ( (employerEnrollmentStatus != 'PENDING' && employerEnrollmentStatus != 'ACTIVE') || (EmployerRenewalDone == 'N') )}">
															<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:if>
														<c:if test="${(fn:contains(subMenu.url , '/hix/shop/employer/planselection/viewselections') && (employerEnrollmentStatus == 'PENDING' || employerEnrollmentStatus == 'ACTIVE'))}">
															<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:if>
														<c:if test="${(fn:contains(subMenu.url , '/hix/shop/employer/setupcoverage/home')) && (EmployerCoverageRenewalStarted == 'Y') && (upcomingEnrollment=='N')}">
															<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:if>
														<c:if test="${(fn:contains(subMenu.url , '/hix/shop/employer/coverage/cancelupcomingcoverage')) && (upcomingEnrollment=='Y')}">
															<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:if>
														<c:if test="${!(fn:contains(subMenu.url , '/hix/shop/employer/planselection/viewselections')) && !(fn:contains(subMenu.url , '/hix/shop/employer/planselection/home')) && !(fn:contains(subMenu.url , '/hix/shop/employer/setupcoverage/home')) && !(fn:contains(subMenu.url , '/hix/shop/employer/coverage/cancelupcomingcoverage'))}">
															<li><a href="${subMenu.url}" title="${subMenu.title}">${subMenu.caption}</a></li>
														</c:if>
													</c:if>

												</c:otherwise>
											</c:choose>
										</c:forEach>
									</ul>
							</c:if>
						</li>
					</c:forEach>
				</ul>
		</div>
	</div>
</div>
</c:if>
</c:if>


<!-- Agent Promotion Modal-->
<c:if test="${sessionScope.showAgentPromotion != null && stateCode != 'CA'}">
    <div id="agentPromotion" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div id="myModalLabel" class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3><spring:message code="indportal.agentPromotion.didYouFindAgent" /></h3>
      </div>
      <div class="modal-body">
		<c:if test="${stateCode != 'CA'}">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 agent-logo">
				<img src="<c:url value="/resources/images/agent_promotion.jpg"/>" alt="Agent Promotion Logo">
			</div>
		</c:if>

		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 margin10-t">
			<p><spring:message code="indportal.agentPromotion.whyImportant"/></p>
			<ul>
				<li><spring:message code="indportal.agentPromotion.licensedagentsbrokers" /></li>
				<li><spring:message code="indportal.agentPromotion.certifiedcounselors" /></li>
			</ul>
        	<p><spring:message code="indportal.agentPromotion.findBestAndSiutYou" /></p>
			<button id="pop_AgentPromotion" data-dismiss="modal" aria-hidden="true" class="btn btn-primary btn-block margin10-b margin20-t"><spring:message code="indportal.agentPromotion.connectToday" /></button>
			<a href="javascript:void(0);" onclick="dismissAgentPromo()" data-dismiss="modal" aria-hidden="true" class="pull-right"><small><spring:message code="indportal.agentPromotion.dontAsk" /></small></a>
		</div>
      </div>
   	</div>
</c:if>
<!-- Agent Promotion Modal-->

<script type="text/javascript">
	$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });
	$(document).ready(function() {

		$('.ttclass').tooltip();
		//Added for ADA JAWS -->
		$('.dropdown').on('click', function() {
			if($(this).hasClass('open')) {
				$(this).find('a.dropdown-toggle').attr('aria-expanded', 'false');
			} else {
				$( this ).find('a.dropdown-toggle').attr('aria-expanded', 'true');
			}
		});
	});
	/*
		HIX-18131  :  If the current url is one of registration flow, then
		              it required to hide the menu;
	*/
	var registrationUrls=new Array("/securityquestions","/account/phoneverification/sendcode","/user/activation");
	var currDocUrl = document.URL;
	var urlIdx=0;
	var noOfRegUrls = registrationUrls.length;

	while(urlIdx <noOfRegUrls ){
		if(currDocUrl.indexOf(registrationUrls[urlIdx]) >0){
			$('#menu').hide();
		}
		if(currDocUrl.indexOf('admin/broker/securityquestions') >0){
			$('#menu').show();//HIX-19628 Menus should be visible in this case
		}
		if(currDocUrl.indexOf('crm/consumer/securityquestions') >0){
			$('#menu').show();//HIX-21621 Menus should be visible in this case
		}
		urlIdx++;
	}

	$(document).ready(function(){
		var navPhixbar = $(".navPhixbar");
		if(navPhixbar.find("li").length > 0 ){

			//for orange navigation
			if($('#navtopview').length > 0){
				navPhixbar.closest("#menu").css("margin-top","30px");
			}
			navPhixbar.closest("#menu").show();
		}

		<%-- HIX-102893- Higlighting correct menu --%>
		<c:if test="${isUserSwitchToOtherView =='Y'}">
            $(".customize-plans").css("padding-top","30px");
        </c:if>
        <c:if test="${viewOtherBrokerInfo}">
       		$("a[title='My Agent Profile']").parent().removeClass('active');
       		$("a[title='Agents']").parent().addClass('active');
        </c:if>
        <c:if test="${viewSelfBrokerInfo}">
	   		$("a[title='My Agent Profile']").parent().addClass('active');
	   		$("a[title='Agents']").parent().removeClass('active');
    	</c:if>
	});

	//Add agent promotion cookie when user dismissed modal
	function dismissAgentPromo(){
		var d = new Date();
		d.setFullYear(d.getFullYear() + 100);
	    var expires = "expires="+d.toUTCString();
	    document.cookie = 'dismissAgentPromo=true' + '; ' + expires;
	};
</script>
