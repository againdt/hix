package com.getinsured.hix.broker.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class CustomListner implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		HibernatePersistenceProviderResolver.register();
	}

}
