package com.getinsured.iex.lce;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

public class SepTransientDTO {
	SsapApplication currentApplication;
	SsapApplication parentApplication;
	Map<Long, SsapApplicant> currentApplicants;
	Map<Long, SsapApplicant> parentApplicants;
	Long userId;
	Long cmrHouseholdId;
	Map<Long, List<SepEvents>> multiEventData;
	Map<String, Boolean> demographicFlagMap;
	Map<Long, List<String>> demographicEventsData;
	List<Boolean> homeAddressChangeDemoFlags;
	Date demographicEventDate;
	Date dobEventDate;
	Set<Long> enrolledPersons;
	String userName;
	
	public SsapApplication getCurrentApplication() {
		return currentApplication;
	}
	public void setCurrentApplication(SsapApplication currentApplication) {
		this.currentApplication = currentApplication;
	}
	public SsapApplication getParentApplication() {
		return parentApplication;
	}
	public void setParentApplication(SsapApplication parentApplication) {
		this.parentApplication = parentApplication;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Map<Long, SsapApplicant> getCurrentApplicants() {
		return currentApplicants;
	}
	public void setCurrentApplicants(Map<Long, SsapApplicant> currentApplicants) {
		this.currentApplicants = currentApplicants;
	}
	public Map<Long, SsapApplicant> getParentApplicants() {
		return parentApplicants;
	}
	public void setParentApplicants(Map<Long, SsapApplicant> parentApplicants) {
		this.parentApplicants = parentApplicants;
	}
	public Long getCmrHouseholdId() {
		return cmrHouseholdId;
	}
	public void setCmrHouseholdId(Long cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}
	public Map<Long, List<SepEvents>> getMultiEventData() {
		return multiEventData;
	}
	public void setMultiEventData(Map<Long, List<SepEvents>> multiEventData) {
		this.multiEventData = multiEventData;
	}
	public Map<String, Boolean> getDemographicFlagMap() {
		return demographicFlagMap;
	}
	public void setDemographicFlagMap(Map<String, Boolean> demographicFlagMap) {
		this.demographicFlagMap = demographicFlagMap;
	}
	public Map<Long, List<String>> getDemographicEventsData() {
		return demographicEventsData;
	}
	public void setDemographicEventsData(
			Map<Long, List<String>> demographicEventsData) {
		this.demographicEventsData = demographicEventsData;
	}
	public List<Boolean> getHomeAddressChangeDemoFlags() {
		return homeAddressChangeDemoFlags;
	}
	public void setHomeAddressChangeDemoFlags(
			List<Boolean> homeAddressChangeDemoFlags) {
		this.homeAddressChangeDemoFlags = homeAddressChangeDemoFlags;
	}
	public Date getDemographicEventDate() {
		return demographicEventDate;
	}
	public void setDemographicEventDate(Date demographicEventDate) {
		this.demographicEventDate = demographicEventDate;
	}
	public Date getDobEventDate() {
		return dobEventDate;
	}
	public void setDobEventDate(Date dobEventDate) {
		this.dobEventDate = dobEventDate;
	}
	public Set<Long> getEnrolledPersons() {
		return enrolledPersons;
	}
	public void setEnrolledPersons(Set<Long> enrolledPersons) {
		this.enrolledPersons = enrolledPersons;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
