package com.getinsured.iex.hub.hhs.cms.dsh.services.ee.vlp.wsdl;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.7.5
 * 2014-02-14T10:21:57.079+05:30
 * Generated source version: 2.7.5
 * 
 */
@WebService(targetNamespace = "http://vlp.ee.services.dsh.cms.hhs.gov/wsdl", name = "VerifyLawfulPresencePortType")
@XmlSeeAlso({com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ObjectFactory.class, com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory.class, com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface VerifyLawfulPresencePortType {

    @WebResult(name = "ReverifyAgency3InitVerifResponse", targetNamespace = "http://vilpsav.ee.sim.dsh.cms.hhs.gov", partName = "body")
    @WebMethod(operationName = "ReverifyAgency3InitVerif")
    public com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ReverifyAgency3InitVerifResponseType reverifyAgency3InitVerif(
        @WebParam(partName = "body", name = "ReverifyAgency3InitVerifRequest", targetNamespace = "http://vilpsav.ee.sim.dsh.cms.hhs.gov")
        com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ReverifyAgency3InitVerifRequestType body
    );

    @WebResult(name = "Agency3InitVerifResponse", targetNamespace = "http://vlp.ee.sim.dsh.cms.hhs.gov", partName = "body")
    @WebMethod(operationName = "VerifyLawfulPresence")
    public com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifResponseType verifyLawfulPresence(
        @WebParam(partName = "body", name = "Agency3InitVerifRequest", targetNamespace = "http://vlp.ee.sim.dsh.cms.hhs.gov")
        com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifRequestType body
    );

    @WebResult(name = "SubmitAgency3DHSResubResponse", targetNamespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov", partName = "body")
    @WebMethod(operationName = "SubmitAgency3DHSResub")
    public com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav.SubmitAgency3DHSResubResponseType submitAgency3DHSResub(
        @WebParam(partName = "body", name = "SubmitAgency3DHSResubRequest", targetNamespace = "http://vclpsav.ee.sim.dsh.cms.hhs.gov")
        com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav.SubmitAgency3DHSResubRequestType body
    );
}
