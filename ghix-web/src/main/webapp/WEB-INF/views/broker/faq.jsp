<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<form  class="form-horizontal" id="faqInfo" name="faqInfo" action="faq" method="get">
 <h3>This is FAQ Page</h3>
 <div class="row-fluid">
    	
		<p><spring:message code="label.agentInfoMgmtMessage" /></p>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3 gray">
		      <h4 class="margin0"><spring:message code="label.agentLearn.HowTo"/></h4>
		      
						<ul>
							<li><a href="#registerAsAgent"><spring:message code="label.agentLearn.HowToApply"/></a></li>
							<li><a href="#manageYourProfile"><spring:message code="label.agentLearn.Profile" /></a></li>
							<li><a href="#manageEmployerAccts"><spring:message code="label.agentLearn.EmployerAccounts" /></a></li>
							<li><a href="#manageIndAccts"><spring:message code="label.agentLearn.IndividualAccounts" /></a></li>
							<li><a href="#manageExistingAccts"><spring:message code="label.agentLearn.ExistingAccounts" /></a></li>
						</ul>
	     </div>
        
        <div class="span9">		
			
				<div class="header" id="registerAsAgent"><spring:message code="label.agentApplyAsAgent" /></div>
        <div class="gutter10">
				<p><spring:message code="label.agentCreateAccount" /></p>
				<ul>
					<li><spring:message code="label.agentCreateAccountEmail" /></li>	
					<li><spring:message code="label.agentCreateAccountLicense" /></li>
				</ul>
				<p><spring:message code="label.agentCreateAccountLicense" /></p>
        <p><strong><spring:message code="label.agentMarketplace.Apply" /></strong></p>
            <ol>
              <li><spring:message code="label.agentMarketplace.Navigate" /></li>
              <li><spring:message code="label.agentMarketplace.CreateAcct" /></li>
              <li><spring:message code="label.agentMarketplace.CertifiedCounselor" /></li>
              <li><spring:message code="label.agentMarketplace.CompleteCreation" /></li>
              <li><spring:message code="label.agentMarketplace.AgentInfo" /></li>
              <li><spring:message code="label.agentMarketplace.BuildProfile" />
                  <ul>
                      <li><spring:message code="label.agentMarketplace.ClientsServed" /></li>
                      <li><spring:message code="label.agentMarketplace.Languages" /></li>
                      <li><spring:message code="label.agentMarketplace.ProductExpertise" /></li>
                      <li><spring:message code="label.agentMarketplace.Education" /></li>
                      <li><spring:message code="label.agentMarketplace.AboutYourself" /></li>
                      <li><spring:message code="label.agentMarketplace.UploadPhoto" /></li>
                  </ul>
              </li>
              <li><spring:message code="label.agentMarketplace.PaymentInfo" /></li>
              <li><spring:message code="label.agentMarketplace.Finish" /></li>
              <li><spring:message code="label.agentMarketplace.Review" />
              	<ol>
              		<li><spring:message code="label.agentMarketplace.ReviewEdit" /></li>
              		<li><spring:message code="label.agentMarketplace.ReviewSave" /></li>
              	</ol>
              </li>
            </ol>   
            <p><spring:message code="label.agentMarketplace.Notification" /></p>
        </div>
        <!--**************************** END OF REGISTER AS AGENT**************************** -->
         
				<div class="header" id="manageYourProfile"><spring:message code="label.agentManageProfile"/></div>		
    
        <div class="gutter10">    
        <p><spring:message code="label.agentManageProfileMessage"/></p>    
        
        <ul>
            <li><a href="#createProfile"><spring:message code="label.agentManage.Create"/></a></li>
            <li><a href="#viewProfile"><spring:message code="label.agentManage.ViewProfile"/></a></li>
            <li><a href="#editProfile"><spring:message code="label.agentManage.EditProfile"/></a></li>
            <li><a href="#renewCert"><spring:message code="label.agentManage.RenewCert"/></a></li>
            <li><a href="#deActivateProfile"><spring:message code="label.agentManage.Deactivate"/></a></li>
        </ul>
        
        <h4 id="create"><spring:message code="label.agentManage.Create"/></h4>
        <p><spring:message code="label.agentManage.CreateDetails"/></p>
        
        <h4 id="viewProfile"><spring:message code="label.agentManage.ViewProfile"/></h4>
        <p><spring:message code="label.agentManage.ViewProfileDetails"/></p>
        
        <ol>
          <li><spring:message code="label.agentManage.ViewProfileDetails1"/></li>   
          <li><spring:message code="label.agentManage.ViewProfileDetails2"/></li>
          <li><spring:message code="label.agentManage.ViewProfileDetails3"/></li>   
        </ol>
        
        <h4 id="editProfile"><spring:message code="label.agentManage.EditProfile"/></h4>
        <p><spring:message code="label.agentManage.EditProfileDetails"/></p>
        
        <ol>
            <li><spring:message code="label.agentManage.EditProfileDetails1"/></li>
            <li><spring:message code="label.agentManage.EditProfileDetails2"/></li>
            <li><spring:message code="label.agentManage.EditProfileDetails3"/></li>
            <li><spring:message code="label.agentManage.EditProfileDetails4"/></li>
            <li><spring:message code="label.agentManage.EditProfileDetails5"/></li>
        </ol>
        
        <h4 id="renewCert"><spring:message code="label.agentManage.RenewCert"/></h4>
        <p><spring:message code="label.agentManage.RenewCertDetails"/></p>
        
        <h4 id="deActivateProfile"><spring:message code="label.agentManage.Deactivate"/></h4>
        <p><spring:message code="label.agentManage.DeactivateDetails"/></p>
        </div>
        <!--**************************** END OF MANAGE PROFILE **************************** -->
        
        <div class="header" id="manageEmployerAccts"><spring:message code="label.agentManage.EmployerAccts"/></div>
        <div class="gutter10">
        <p><spring:message code="label.agentManage.EmployerAcctsMessage"/></p>
        
        <ul>
            <li><a href="#viewEmpList"><spring:message code="label.agentManageEmpAccts.ViewList"/></a></li>
            <li><a href="#findEmp"><spring:message code="label.agentManageEmpAccts.Find"/></a></li>
            <li><a href="#changeEmpStatus"><spring:message code="label.agentManageEmpAccts.Status"/></a></li>
            <li><a href="#viewEmpSummary"><spring:message code="label.agentManageEmpAccts.Summary"/></a></li>
            <li><a href="#viewEmpContactInfo"><spring:message code="label.agentManageEmpAccts.ContactInfo"/></a></li>
            <li><a href="#editEmp"><spring:message code="label.agentManageEmpAccts.EditComments"/></a></li>
            <li><a href="#actAsEmployer"><spring:message code="label.agentManageEmpAccts.ActEmployer"/></a></li>
        </ul>
        
        <h4 id="viewEmpList"><spring:message code="label.agentManageEmpAccts.ViewList"/></h4>
        <p><spring:message code="label.agentManageEmpAccts.ViewListDetails"/></p>
        
        <ol>
          <li><spring:message code="label.agentManageEmpAccts.ViewListDetails1"/></li>
          <li><spring:message code="label.agentManageEmpAccts.ViewListDetails2"/></li>
          <li><spring:message code="label.agentManageEmpAccts.ViewListDetails3"/></li>
          <li><spring:message code="label.agentManageEmpAccts.ViewListDetails4"/></li>
        </ol>

        
        <h4 id="findEmp"><%--spring:message code=""/--%></h4>
        <p><spring:message code="label.agentManageEmpAccts.FindDetails"/></p>
        <ol>
            <li><spring:message code="label.agentManageEmpAccts.FindDetails1"/></li>
            <li><spring:message code="label.agentManageEmpAccts.FindDetails2"/></li>
            <li><spring:message code="label.agentManageEmpAccts.FindDetails3"/></li>
            <li><spring:message code="label.agentManageEmpAccts.FindDetails4"/></li>
        </ol>
        
        <h4 id="changeEmpStatus"><%--spring:message code=""/--%>Change emp Status</h4>
        <p><spring:message code="label.agentManageEmpAccts.StatusDetailsA"/></p>
        <p><spring:message code="label.agentManageEmpAccts.StatusDetailsB"/></p>
        <ol>
            <li><spring:message code="label.agentManageEmpAccts.StatusDetails1"/></li>
            <li><spring:message code="label.agentManageEmpAccts.StatusDetails2"/><br>
            <em><spring:message code="label.agentManageEmpAccts.StatusDetails2a"/></em></li>
            <li><spring:message code="label.agentManageEmpAccts.StatusDetails3"/>
                <p><spring:message code="label.agentManageEmpAccts.StatusDetails3a"/></p>
            </li>
        </ol>
        
        
        
        <h4 id="viewEmpSummary"><spring:message code="label.agentManageEmpAccts.Summary"/></h4>
        
        <p><spring:message code="label.agentManageEmpAccts.SummaryDetails"/></p>
          <ol>
            <li><spring:message code="label.agentManageEmpAccts.SummaryDetails1"/></li>
            <li><spring:message code="label.agentManageEmpAccts.SummaryDetails2"/></li>
            <li><spring:message code="label.agentManageEmpAccts.SummaryDetails3"/><br>
            	<em><spring:message code="label.agentManageEmpAccts.SummaryDetails3a"/></em>
            </li>
          </ol>        
        
        <h4 id="viewEmpContactInfo"><spring:message code="label.agentManageEmpAccts.ContactInfo"/></h4>
        <p><spring:message code="label.agentManageEmpAccts.ContactInfoDetails"/></p>
            <ol>

            <li><spring:message code="label.agentManageEmpAccts.ContactInfoDetails1"/></li>
            <li><spring:message code="label.agentManageEmpAccts.ContactInfoDetails2"/></li>
            <li><spring:message code="label.agentManageEmpAccts.ContactInfoDetails3"/><br>
            	<em><spring:message code="label.agentManageEmpAccts.ContactInfoDetails3a"/></em>
            </li>
            <li><spring:message code="label.agentManageEmpAccts.ContactInfoDetails4"/></li>
        
            </ol>
            
            
          <h4 id="editEmp"><spring:message code="label.agentManageEmpAccts.EditComments"/></h4>
            <p><spring:message code="label.agentManageEmpAccts.EditCommentsDetails"/></p>
              <ol>
                <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails1"/></li>
                <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails2"/><br>
                <em><spring:message code="label.agentManageEmpAccts.EditCommentsDetails2a"/></em>
                </li>
                <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails3"/></li>
                <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails4"/>
                      <ol>
                         <li> <spring:message code="label.agentManageEmpAccts.EditCommentsDetails4a"/></li>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails4b"/></li>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails4c"/></li>
                      </ol>
                </li>
                <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails5"/></li>
                <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails6"/>
                      <ol>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails6a"/></li>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails6b"/></li>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails6c"/></li>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails6d"/></li>
                      </ol>
                </li>
                <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails7"/>
                      <ol>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails7a"/></li>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails7b"/></li>
                          <li><spring:message code="label.agentManageEmpAccts.EditCommentsDetails7c"/></li>
                      </ol>
                </li>
              </ol>
          
          
          <h4 id="actAsEmployer"><spring:message code="label.agentManageEmpAccts.ActEmployer"/></h4>
          <p><spring:message code="label.agentManageEmpAccts.ActEmployerDetails"/></p>

           <p> <spring:message code="label.agentManageEmpAccts.ActEmployerDetailsA"/></p>
              <ol>
                  <li><spring:message code="label.agentManageEmpAccts.ActEmployerDetails1"/></li>
                  <li><spring:message code="label.agentManageEmpAccts.ActEmployerDetails2"/><br>
                  <em><spring:message code="label.agentManageEmpAccts.ActEmployerDetails2a"/></em>
                  </li>
                  <li><spring:message code="label.agentManageEmpAccts.ActEmployerDetails3"/></li>
                  <li><spring:message code="label.agentManageEmpAccts.ActEmployerDetails4"/></li>
              </ol>
        </div>
        <!-- **************************** END OF EMPLOYER ACCOUNTS**************************** -->
        
        <div class="header" id="manageIndAccts"><spring:message code="label.agentManageIndAccts"/></div>
        <div class="gutter10">
          
         <P> <spring:message code="label.agentManageIndAccts.Details"/></P>
          <ul>
           <li><a href="#viewIndList"> <spring:message code="label.agentManageIndAccts.ViewIndividual"/></a></li>
           <li><a href="#findInd"><spring:message code="label.agentManageIndAccts.FindIndividual"/></a></li>
            <li><a href="#changeIndStatus"> <spring:message code="label.agentManageIndAccts.ChangeIndStatus"/></a></li>
           <li><a href="viewIndIncome"> <spring:message code="label.agentManageIndAccts.ViewSummary"/></a></li>
           <li><a href="#editIndComments"> <spring:message code="label.agentManageIndAccts.EditComments"/></a></li>
           <li><a href="#actAsInd">  <spring:message code="label.agentManageIndAccts.ActIndividual"/></a></li>
          </ul>
          
          <h4 id="viewIndList"><spring:message code="label.agentManageIndAccts.ViewIndividual"/></h4>
          <p><spring:message code="label.agentManageIndAccts.ViewIndividualDetails"/></p>
            <ol>
              <li><spring:message code="label.agentManageIndAccts.ViewIndividualDetails1"/></li>
              <li><spring:message code="label.agentManageIndAccts.ViewIndividualDetails2"/></li>
              <li><spring:message code="label.agentManageIndAccts.ViewIndividualDetails3"/></li>
              <li><spring:message code="label.agentManageIndAccts.ViewIndividualDetails4"/></li>
            </ol>
            
            
          <h4 id="findInd"><spring:message code="label.agentManageIndAccts.FindIndividual"/></h4>
          <p><spring:message code="label.agentManageIndAccts.FindIndividualDetails"/></p>
            <ol>
              <li><spring:message code="label.agentManageIndAccts.FindIndividualDetails1"/></li>
              <li><spring:message code="label.agentManageIndAccts.FindIndividualDetails2"/></li>
              <li><spring:message code="label.agentManageIndAccts.FindIndividualDetails3"/></li>
              <li><spring:message code="label.agentManageIndAccts.FindIndividualDetails4"/></li>
            </ol>
            
            
          <h4 id="changeIndStatus"><spring:message code="label.agentManageIndAccts.ChangeIndStatus"/></h4>
          <p><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetailsA"/></p>
          <p><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetailsB"/></p>
            <ol>
              <li><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails1"/></li>
              <li><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails2"/><br>
              <em><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails2a"/></em></li>
              <li><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails3"/>
              	<p><spring:message code="label.agentManageIndAccts.ChangeIndStatusDetails3a"/></p>
              </li>
            </ol>
            
            
          <h4 id="viewIndIncome"><spring:message code="label.agentManageIndAccts.ViewSummary"/></h4>
          <p><spring:message code="label.agentManageIndAccts.ViewSummaryDetails"/></p>
            <ol>
              <li><spring:message code="label.agentManageIndAccts.ViewSummaryDetails1"/></li>
              <li><spring:message code="label.agentManageIndAccts.ViewSummaryDetails2"/></li>
             <li> <spring:message code="label.agentManageIndAccts.ViewSummaryDetails3"/><br>
              <em><spring:message code="label.agentManageIndAccts.ViewSummaryDetails3a"/></em></li>
            </ol>
            
            
          
          <h4 id="editIndComments"><spring:message code="label.agentManageIndAccts.EditComments"/></h4>
          <p><spring:message code="label.agentManageIndAccts.EditCommentsDetails"/></p>
              <ol>
                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails1"/></li>
                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails2"/><em>
                <spring:message code="label.agentManageIndAccts.EditCommentsDetails2a"/></em></li>
                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails3"/></li>
                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails4"/>
                    <ol>
                      <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails4a"/></li>
                      <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails4b"/></li>
                      <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails4c"/></li>
                    </ol>
                </li>
                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails5"/></li>
                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails6"/>
                    <ol>
                      <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails6a"/></li>
                      <li> <spring:message code="label.agentManageIndAccts.EditCommentsDetails6b"/></li>
                       <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails6c"/></li>
                       <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails6d"/></li>
                    </ol>
                </li>
                <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails7"/>
                    <ol>
                        <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails7a"/></li>
                        <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails7b"/></li>
                        <li><spring:message code="label.agentManageIndAccts.EditCommentsDetails7c"/></li>
                    </ol>
                </li>
              </ol>
          <h4 id="actAsInd"> <spring:message code="label.agentManageIndAccts.ActIndividual"/></h4>
          <p><spring:message code="label.agentManageIndAccts.ActIndividualDetailsA"/></p>

          <p><spring:message code="label.agentManageIndAccts.ActIndividualDetailsB"/></p>
        
              <ol>

                  <li><spring:message code="label.agentManageIndAccts.ActIndividualDetails1"/></li>
                  <li><spring:message code="label.agentManageIndAccts.ActIndividualDetails2"/><br>
                  <em><spring:message code="label.agentManageIndAccts.ActIndividualDetails2a"/></em></li>
                  <li><spring:message code="label.agentManageIndAccts.ActIndividualDetails3"/></li>
                  <li><spring:message code="label.agentManageIndAccts.ActIndividualDetails4"/></li>
          
             </ol>

          
        </div>
         <!-- **************************** END OF INDIVIDUAL ACCOUNTS**************************** -->
        
        <div class="header" id="manageExistingAccts"><spring:message code="label.agentManageExistingAccts"/></div>
        <div class="gutter10">
            <ul>
              <li><a href="#addExistingClient"><spring:message code="label.agentManageExistingAccts.AddExistingClient"/></a></li>
              <li><a href="#setupNewClient"><spring:message code="label.agentManageExistingAccts.SetupNewClient"/></a></li>
              <li><a href="#newClientNote"><spring:message code="label.agentManageExistingAccts.NewClientNotification"/></a></li>
              <li><a href="#newClientNoteToDo"><spring:message code="label.agentManageExistingAccts.NewNotificationToDo"/></a></li>
              <li><a href="#editClientInfo"><spring:message code="label.agentManageExistingAccts.EditClientInfo"/></a></li>
            </ul>
            
            <h4 id="addExistingClient"><spring:message code="label.agentManageExistingAccts.AddExistingClient"/></h4>
           <p><spring:message code="label.agentManageExistingAccts.AddExistingClientDetails"/></p>
            
            <h4 id="setupNewClient"><spring:message code="label.agentManageExistingAccts.SetupNewClient"/></h4>
            <p><spring:message code="label.agentManageExistingAccts.SetupNewClientDetails"/></p>
            
            <h4 id="setupNewClient"><spring:message code="label.agentManageExistingAccts.NewClientNotification"/></h4>
            <p><spring:message code="label.agentManageExistingAccts.NewClientNotificationDetails"/></p>
            
            <h4 id="newClientNoteToDo"><spring:message code="label.agentManageExistingAccts.NewNotificationToDo"/></h4>
            <p><spring:message code="label.agentManageExistingAccts.NewNotificationToDoDetails"/></p>
            
            <h4 id="editClientInfo"><spring:message code="label.agentManageExistingAccts.EditClientInfo"/></h4>
            <p><spring:message code="label.agentManageExistingAccts.EditClientInfoDetails"/></p>

                        
        </div>
        <!--**************************** END OF INDIVIDUAL ACCOUNTS**************************** -->
					
		</div>
		</div>
 
 </form>
