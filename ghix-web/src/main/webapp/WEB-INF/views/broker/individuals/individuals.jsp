<%@ page import="com.getinsured.hix.platform.security.service.ModuleUserService"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page isELIgnored="false"%>



<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script> --%>
<link rel="stylesheet" type="text/css" href="<gi:cdnurl value="/resources/css/tablesorter.css" />" />

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<gi:cdnurl value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<gi:cdnurl value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<gi:cdnurl value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>


<%
	Integer pageSizeInt = (Integer) request.getAttribute("pageSize");
	String reqURI = (String) request.getAttribute("reqURI");
	Integer totalResultCount = (Integer) request
			.getAttribute("resultSize");
	if (totalResultCount == null) {
		totalResultCount = 0;
	}
	Integer currentPage = (Integer) request.getAttribute("currentPage");
	int noOfPage = 0;

	if (totalResultCount > 0) {
		if ((totalResultCount % pageSizeInt) > 0) {
			noOfPage = (totalResultCount / pageSizeInt) + 1;
		} else {
			noOfPage = (totalResultCount / pageSizeInt);
		}
	}
%>

<%-- <%
System.out.println(" INDIV IDS ************************************************************");
request.setAttribute("individualsId", request.getAttribute("individualsId"));

%> --%>

<!--  -->
<style>
#rightpanel tbody tr td:first-child{padding-left: 11px;}
.page-breadcrumb {margin-top: 0 !important;}
.page-breadcrumb li i.icon-circle-arrow-left {top: 0px;}
#rightpanel tbody tr td:first-child {
padding-left: 9px !important;
}

</style>
<!--start page-breadcrumb -->
<div class="gutter10-lr">
<div class="l-page-breadcrumb">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
			<li><a href="<c:url value="/broker/individuals"/>"><spring:message
						code="label.individuals" /></a></li>
			<li>${desigStatus}</li>
		</ul>
	</div>
	<!--page-breadcrumb ends-->
</div>
	<div class="row-fluid">

		<c:if test="${message != null}">
			<div class="errorblock alert alert-info">
				<p>${message}</p>
			</div>
		</c:if>


			<h1><a name="skip"></a>
				<c:choose>
						<c:when test="${CA_STATE_CODE}">
							<c:if 	test="${(desigStatus == 'Pending'  )}">
								<spring:message code="label.pending.delegation.requests"/>
							</c:if>
							<c:if 	test="${(desigStatus == 'InActive'  )}">
							 	<spring:message code="label.Inactive.delegation.requests"/>
							</c:if>
						</c:when>
						<c:otherwise>
							 <spring:message code="label.individuals"/>
						</c:otherwise>
				</c:choose>
				 <small>${resultSize}
						${tableTitle}</small>
			</h1>

		<div class="pull-right gutter10">
<%-- 			<div class="controls">
				<div class="dropdown" id="dropdown">
					<span id="counter" class="gray"></span> <span id="bulkActions">
						<a href="#" data-target="#" data-toggle="dropdown" role="button"
						id="inactiveLabel"
						class="dropdown-toggle btn btn-primary btn-small">Bulk Action <i class="icon-cog icon-white"></i><i class="caret"></i></a>
						<ul aria-labelledby="inactiveLabel" role="menu"
							class="dropdown-menu pull-right">
							<li><c:if test="${(desigStatus == 'Pending')}">
									<a class="" onclick="submitRequest('Active');">Accept</a></li>
							<li><a id="declinelink" class="validateDecline"
								onclick="markInactive();" href="#mark-inactive">Decline</a> </c:if> <c:if
									test="${(desigStatus == 'Active')}">
									<li><a id="declinelink" class="validateDecline"
										onclick="markInactive();" href="#mark-inactive">Mark as
										Inactive</a></li>
								</c:if> <c:if test="${(desigStatus == 'InActive')}">
									<!-- <a class="" href="#new-msg" data-toggle="modal"
										onclick="resetForm();saveDraftOfMessage();">Send a Message</a> -->
									<a class="" href="#" data-toggle="modal">Send Designation
										Request</a>
								</c:if></li>
						</ul>
					</span>
onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Pending Delegation Requests', 1]);"
				</div>
			</div> --%>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="gutter10-lr">
		<div id="sidebar" class="span3">
		<c:set var="encFromModule" ><encryptor:enc value="<%=ModuleUserService.INDIVIDUAL_MODULE%>" isurl="true"/> </c:set>
			<div class="header clearfix">
			       <h4 class="pull-left"><spring:message code="label.brkRefineResultsBy"/></h4> <a class="pull-right margin5-r" href="#" onclick="resetAll(); triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Reset All', 1]);">(<spring:message code="label.brkResetAll"/>)</a>
			</div>
			<div class="graybg">
				<form class="form-vertical gutter10" id="individualsearch"
					name="individualsearch" action="individuals?desigStatus=${desigStatus}" method="POST">
					<df:csrfToken/>
						<input type="hidden" name="fromModule" id="fromModule" value="<%=ModuleUserService.INDIVIDUAL_MODULE%>" />
						<%-- <input type="hidden" name="individualsId" id="individualsId" value="<%=request.getAttribute("individualsId")%>" /> --%>

					<input type="hidden" name="totalCountAtClient" value="${resultSize}" />
					<input type="hidden" id="sortBy" name="sortBy" >
					<input type="hidden" id="sortOrder" name="sortOrder" >
					<input type="hidden" id="pageNumber" name="pageNumber" value="1">
					<input type="hidden" id="changeOrder" name="changeOrder" >
					<input type="hidden" id="previousSortBy" name="previousSortBy" value="${previousSortBy}">
					<input type="hidden" id="previousSortOrder" name="previousSortOrder" value="${previousSortOrder}">
					<input type="hidden" id="currentPageNumber" name="currentPageNumber" value="<%=request.getAttribute("currentPage")%>" />
					<!-- <div class="control-group">
						<label class="control-label" for="firstName"><spring:message code='label.brkFirstName'/></label>
						<div class="controls">
							<input class="span" type="text" id="firstName" name="firstName"
								value="${searchCriteria.firstName}" placeholder="" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Filter - First Name', 1]);"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="lastName"><spring:message code='label.brkLastNames'/></label>
						<div class="controls">
							<input class="span" type="text" id="lastName" name="lastName"
								value="${searchCriteria.lastName}" placeholder="" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Filter - Last Name', 1]);"/>
						</div>
					</div> -->

					<c:if test="${(desigStatus == 'Pending' || desigStatus == '' || desigStatus == null)}">
					<div class="control-group">
						<label class="control-label" for="firstName"><spring:message code='label.brkFirstName'/></label>
						<div class="controls">
							<input class="span" type="text" id="firstName" name="firstName"
								value="${searchCriteria.firstName}" placeholder="" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Filter - First Name', 1]);"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="lastName"><spring:message code='label.brkLastNames'/></label>
						<div class="controls">
							<input class="span" type="text" id="lastName" name="lastName"
								value="${searchCriteria.lastName}" placeholder="" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Filter - Last Name', 1]);"/>
						</div>
					</div>
					<fieldset>
					<legend><spring:message code='label.brkrequestsent'/></legend>
						<div class="box-tight margin0">
							<div class="control-group">
								<label class="required control-label" for="requestSentFrom"><spring:message code='label.search.from'/></label>
								<%-- <div class="controls">
									<input type="text" id="requestSentFrom" name="requestSentFrom" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.requestSentFrom}" />
									<div class="help-inline" id="requestSentFrom_error"></div>
								</div> --%>

								<div class="input-append date date-picker" id="date">
									<input class="input-small" type="text" name="requestSentFrom" id="requestSentFrom" value="${searchCriteria.requestSentFrom}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Request Sent From Date', 1]);"/>
									<span class="add-on" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Request Sent From Date', 1]);"><i class="icon-calendar"></i></span>
									<div class="help-inline" id="requestSentFrom_error"></div>
								</div>

							</div>

							<div class="control-group">
								<label class="required control-label" for="requestSentTo"><spring:message code='label.search.to'/></label>
								<%-- <div class="controls">
									<input type="text" id="requestSentTo" name="requestSentTo"
										class="datepick input-small" title="MM/dd/yyyy"
										value="${searchCriteria.requestSentTo}" />
									<div class="help-inline" id="requestSentTo_error"></div>
								</div> --%>
								<div class="input-append date date-picker" id="date">
									<input class="input-small" type="text" name="requestSentTo" id="requestSentTo" value="${searchCriteria.requestSentTo}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Request Sent To Date', 1]);"/>
									<span class="add-on" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Request Sent To Date', 1]);"><i class="icon-calendar"></i></span>
									<div class="help-inline" id="requestSentTo_error"></div>
								</div>
								<!-- end of controls-->
							</div>
						</div>
						</fieldset>
					</c:if>

					<c:if test="${(desigStatus == 'InActive')}">
					<div class="control-group">
						<label class="control-label" for="firstName"><spring:message code='label.brkFirstName'/></label>
						<div class="controls">
							<input class="span" type="text" id="firstName" name="firstName"
								value="${searchCriteria.firstName}" placeholder="" onfocus="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Inactive Delegations', 'OnFocus', 'Filter - First Name', 1]);"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="lastName"><spring:message code='label.brkLastNames'/></label>
						<div class="controls">
							<input class="span" type="text" id="lastName" name="lastName"
								value="${searchCriteria.lastName}" placeholder="" onfocus="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Inactive Delegations', 'OnFocus', 'Filter - Last Name', 1]);"/>
						</div>
					</div>
					<fieldset>
					<legend><spring:message code="label.search.inactiveSince"/></legend>
						<div class="box-tight margin0">
							<div class="control-group">
								<label class="required control-label" for="inactiveDateFrom"><spring:message code='label.search.from'/></label>
								<%-- <div class="controls">
									<input type="text" id="inactiveDateFrom" name="inactiveDateFrom" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.inactiveDateFrom}" />
									<div class="help-inline" id="inactiveDateFrom_error"></div>

								</div> --%>
								<!-- end of controls-->

								<div class="input-append date date-picker" id="date">
									<input class="input-small" type="text" name="inactiveDateFrom" id="inactiveDateFrom" value="${searchCriteria.inactiveDateFrom}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Inactive ', 'Click', 'Inactive Since From Date', 1]);"/>
									<span class="add-on" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Inactive ', 'Click', 'Inactive Since From Date', 1]);"><i class="icon-calendar"></i></span>
									<div class="help-inline" id="inactiveDateFrom_error"></div>
								</div>
							</div>

							<div class="control-group">
								<label class="required control-label" for="inactiveDateTo"><spring:message code='label.search.to'/></label>
								<%-- <div class="controls">
									<input type="text" id="inactiveDateTo" name="inactiveDateTo" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.inactiveDateTo}" />
									<div class="help-inline" id="inactiveDateTo_error"></div>
								</div> --%>
								<!-- end of controls-->

								<div class="input-append date date-picker" id="date">
									<input class="input-small" type="text" name="inactiveDateTo" id="inactiveDateTo" value="${searchCriteria.inactiveDateTo}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Inactive ', 'Click', 'Inactive Since To Date', 1]);"/>
									<span class="add-on" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Inactive ', 'Click', 'Inactive Since To Date', 1]);"><i class="icon-calendar"></i></span>
									<div class="help-inline" id="inactiveDateTo_error"></div>
								</div>
							</div>
						</div>
						</fieldset>
					</c:if>

					<c:if test="${(desigStatus == 'Active')}">
					<div class="control-group">
						<label for="eligibilityStatus" class="control-label"><spring:message code="label.agent.employers.EligibilityStatus"/></label>
						<div class="controls">
							<select size="1" id="eligibilityStatus" name="eligibilityStatus" class="span12">
								<option value=""><spring:message code='label.search.dropdown.option.select'/></option>
								<c:forEach var="eligibilityStatus" items="${eligibilityStatuslist}">
									<option <c:if test="${eligibilityStatus == searchCriteria.eligibilityStatus}"> SELECTED </c:if> value="${eligibilityStatus}">${eligibilityStatus.description2}</option>
								</c:forEach>
							</select>
							</div>
					</div>
					<div class="control-group">
							<label for="applicationStatus" class="control-label"><spring:message code="label.indApplicationStatus"/></label>
							<div class="controls">
							<select size="1" id="applicationStatus" name="applicationStatus" class="span12">
								<option value=""><spring:message code='label.search.dropdown.option.select'/></option>
								<c:forEach var="applicationStatus" items="${applicationStatusList}">
									<option <c:if test="${applicationStatus == searchCriteria.applicationStatus}"> SELECTED </c:if> value="${applicationStatus}">${applicationStatus.description}</option>
								</c:forEach>
							</select>
							</div>
						</div>

					</c:if>
					<div class="gutter10">
						<input type="submit" name="submit" id="submit"
							class="btn input-small btn-primary offset3" value="<spring:message code='label.brkGo'/>" title="<spring:message code='label.brkGo'/>" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Go', 1]);"/>
					</div>
				</form>
			</div>


		</div>
		<!-- end of span3 -->

		<div id="rightpanel" class="span9">
			<form class="form-vertical" id="brokerindividuals"
				name="brokerindividuals" action="individuals" method="POST">
				<df:csrfToken/>
				<input type="hidden" name="prevStatus" id="prevStatus" value="${desigStatus}">
				<input type="hidden" name="ids" id="ids" value="">
				<input type="hidden" name="fromModule" id="fromModule" value="${encFromModule}">
				<div id="brokerlist">

					<input type="hidden" name="id" id="id" value="">

					<c:set var="firstNameSorter" value="${sortOrder}" />
					<c:set var="familySizeSorter" value="${sortOrder}" />
					<c:set var="requestSentSorter" value="${sortOrder}" />
					<c:set var="eligibilityStatusSort" value="${sortOrder}" />
					<c:set var="applicationStatusSort" value="${sortOrder}" />
					<c:set var="inactiveSinceSort" value="${sortOrder}" />

					<c:choose>

<%-- 						<c:when test="${fn:length(approvedbrokerslist) > 0}"> --%>
						<c:when test="${resultSize > 0 and desigStatus == 'Active'}">
							<table class="table table-striped tablesorter" id="indTable${desigStatus}"
								name="indTable${desigStatus}">
								<thead>
									<tr class="header">
										<!-- Please do not change checkbox column header from <td> to <th> -->
<%-- 										<td class="txt-center header" scope="col"><input type="checkbox" value="${broker.individualId}"
												id="check_all${desigStatus}" name="check_all${desigStatus}"></td> --%>
										<spring:message code="label.brkList.columnHeader.Name" var="nameVal"/>
										<spring:message code="label.assiter.indFamilySize" var="familySizeVal"/>
										<spring:message code="label.assister.indHouseholdIncome" var="houseHoldIncomeVal"/>
										<spring:message code="label.assister.indELIGIBILITYSTATUS" var="eligibilityStatusVal"/>
										<spring:message code="label.ApplicationStatus" var="applicationStatusVal"/>
										<spring:message code="label.brkactions" var="bulkActionVal"/>
										<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${nameVal}" sortBy="FIRST_NAME" sortOrder="${firstNameSorter}"></dl:sort></th>
										<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${familySizeVal}" sortBy="FAMILY_SIZE" sortOrder="${familySizeSorter}"></dl:sort></th>
										<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${houseHoldIncomeVal}" sortBy="HOUSEHOLD_INCOME" sortOrder="${sortOrder}"></dl:sort></th>
										<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${eligibilityStatusVal}" sortBy="ELIGIBILITY_STATUS" sortOrder="${sortOrder}"></dl:sort></th>
										<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${applicationStatusVal}" sortBy="APPLICATION_STATUS" sortOrder="${sortOrder}"></dl:sort></th>
										<th class="sortable" scope="col" style="width:45px"><spring:message code="label.brkAction"/></th>
									</tr>
								</thead>
								<c:forEach items="${houseHoldList}" var="household">
									<c:set var="encryptedId" ><encryptor:enc value="${household.id}" isurl="true"/> </c:set>
									<tr>
										<%-- <td class="txt-center"><input type="checkbox" value="${household.id}"
											class="indCheckbox${desigStatus}"
											name="indCheckbox${desigStatus}"></td> --%>
										<td><a href="../individual/individualcase/${encryptedId}">${household.firstName} ${household.lastName}</a></td>
										<td>
												<c:choose >
													<c:when  test="${household.familySize==0}">N/A</c:when>
													<c:when  test="${household.familySize!=0}">${household.familySize}</c:when>
												</c:choose>
										</td>
								        <td>${household.houseHoldIncome}</td>
								        <td>${household.eligibilityStatus}</td>
								      	<td>${household.applicationStatus}</td>

										<td class="txt-right">
											<div class="controls">
												<div class="dropdown">
													<a href="/page.html"
														aria-haspopup="true"
														aria-expanded="false"
														data-target="#" data-toggle="dropdown"
														role="button" id="dLabel" class="dropdown-toggle"> <i
														class="icon-cog"></i><b class="caret"></b>
													</a>

													<!-- <li><a href="#new-msg" data-toggle="modal"
														onclick="resetForm();saveDraftOfMessage();">Send a
															Message</a></li> -->


													<ul aria-labelledby="dLabel" role="menu"
														class="dropdown-menu pull-right txt-left">
														<li><a href="<c:url value="/individual/individualcase/${encryptedId}"/>"><spring:message code="label.brkdetails"/></a></li>
														<li><a id="declinelink" class="validateDecline"	href="<c:url value="/broker/declinePopup?desigId=${encryptedId}&prevStatus=${desigStatus}&fromModule=${encFromModule}"/>"><spring:message code="label.brkMarkAsInactive"/></a>
														<sec:accesscontrollist hasPermission="BROKER_ADD_TICKET" domainObject="user">
															<li><a encryptedval="${encryptedId}" class="contactExchangeLink" href="javascript:;">Contact ${exchangeName}</a></li>
														</sec:accesscontrollist>
													</ul>


												</div>
											</div>
										</td>
									</tr>
								</c:forEach>
							</table>
						</c:when>
						<c:when test="${resultSize > 0 and (desigStatus == 'Pending' || desigStatus == 'InActive')}">
							<table class="table table-striped tablesorter" id="indTable${desigStatus}"
								name="indTable${desigStatus}">
								<thead>
									<tr>
										<!-- Please do not change checkbox column header from <td> to <th> -->
										<%-- <td class="txt-center header" scope="col"><input type="checkbox" value="${broker.individualId}"
											id="check_all${desigStatus}" name="check_all${desigStatus}"></td> --%>
											<spring:message code="label.brkList.columnHeader.Name" var="nameVal"/>
											<spring:message code="label.assiter.indFamilySize" var="familySizeVal"/>
											<spring:message code="label.brkrequestsent" var="requestSentVal"/>
											<spring:message code="label.brkinactivesince" var="inactiveSinceVal"/>
											<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse" title="${nameVal}" sortBy="FIRST_NAME" sortOrder="${firstNameSorter}"></dl:sort></th>
										<c:choose>
											<c:when	test="${(desigStatus == 'Pending' || desigStatus == '' || desigStatus == null)}">
												<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse"  title="${familySizeVal}" sortBy="FAMILY_SIZE" sortOrder="${familySizeSorter}"></dl:sort></th>
												<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse" title="${requestSentVal}" sortBy="REQUESTSENT" sortOrder="${requestSentSorter}"></dl:sort></th>
												<th><spring:message code="label.brkactions"/></th>
											</c:when>
											<c:otherwise>
												<th class="sortable" scope="col" style="width: 175px;"><dl:sort  customFunctionName="traverse" title="${inactiveSinceVal}" sortBy="INACTIVESINCE" sortOrder="${inactiveSinceSort}"></dl:sort></th>
											</c:otherwise>
										</c:choose>
										<!-- <th class="txt-right"></th> -->
									</tr>
								</thead>
								<c:forEach items="${houseHoldList}" var="household">
									<c:set var="encryptedId" ><encryptor:enc value="${household.id}" isurl="true"/> </c:set>
									<tr>
										<%-- <td class="txt-center" scope="col"><input type="checkbox" value="${household.id}"
										name="indCheckbox${desigStatus}"
										class="indCheckbox${desigStatus}"></td> --%>
										<td>

										<c:choose>
												<c:when test="${CA_STATE_CODE}">
													<a name="indlink"
													href="<c:url value="/individual/viewindividualdetail/${encryptedId}?desigStatus=${desigStatus}&phoneNumber=${household.phoneNumber}&emailAddress=${household.emailAddress }&firstName=${household.firstName}&lastName=${household.lastName}"/>"
													id="ind${household.id}" class="inddetail" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Individual Name', 1]);">${household.firstName} ${household.lastName}</a>
												</c:when>
												<c:otherwise>
													<a name="indlink"
													href="<c:url value="/individual/viewindividualdetail/${encryptedId}?desigStatus=${desigStatus}"/>"
													id="ind${household.id}" class="inddetail">${household.firstName} ${household.lastName} </a>

												</c:otherwise>
										</c:choose>
										</td>

										<c:choose>
											<c:when test="${(desigStatus == 'Pending')}">
												<td>
													<c:choose >
														<c:when  test="${household.familySize==0}">N/A</c:when>
														<c:when  test="${household.familySize!=0}">${household.familySize}</c:when>
													</c:choose>
												</td>
<%-- 												<td class="txt-center"><fmt:formatDate --%>
<%-- 														value="${dateMap[household.id]}" type="date" /></td> --%>
												<td><fmt:formatDate
 														value="${household.requestSent}" type="date" pattern="MM/dd/yyyy"/></td>
											</c:when>
											<c:otherwise>
												<td><fmt:formatDate
														value="${household.inActiveSince}" type="date" pattern="MM/dd/yyyy"/></td>
											</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${(desigStatus == 'Pending')}">
												<td>
														<div class="dropdown">
															<a href="/page.html" data-target="#"
																aria-haspopup="true"
																aria-expanded="false"
																data-toggle="dropdown" role="button" id="dLabel"
																class="dropdown-toggle"> <i class="icon-cog"></i><b class="caret"></b>
															</a>
															<ul aria-labelledby="dLabel" role="menu"
																class="dropdown-menu">
																<li><a
																	href="<c:url value="/broker/approve/${encryptedId}/${encFromModule}?prevStatus=${desigStatus}"/>" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Action - Accept', 1]);"><spring:message code="label.brkaccept"/></a>
																</li>
																<li><a href="<c:url value="/broker/declinePopup?desigId=${encryptedId}&prevStatus=${desigStatus}&fromModule=${encFromModule}&firstName=${household.firstName}&lastName=${household.lastName}"/>"
																id="declinelink" class="validateDecline" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'Click', 'Action - Decline', 1]);"><spring:message code="label.brkdecline"/></a>
																</li>
																<!--  <li><a href="#new-msg" data-toggle="modal"
																	onclick="resetForm();saveDraftOfMessage();">Send a
																		Message</a></li>-->
															</ul>
														</div>
												</td>
											</c:when>
											<c:otherwise>

											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
							</table>
						</c:when>
					</c:choose>
					<c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
						<c:if test="${resultSize > 0 and (desigStatus == 'Pending' || desigStatus == 'Active')}">
							<div  style="text-align: left;font-size:9px;">N/A - Not Available</div>
						</c:if>
					</c:if>

					<c:choose>
						<c:when
							test="${fn:length(brokerslist) > 0 || resultSize > 0}">
							<div class="center"  id="paginationContainer">
							 <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
							 </div>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="$(\"#status option:selected\").val() == 'Pending'">
									<div class="alert alert-info"><spring:message code="label.brkapprovalalert"/></div>
								</c:when>
								<c:otherwise>
									<div class="alert alert-info"><spring:message code="label.brknomatching"/></div>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>

				</div>
			</form>
		</div>
	</div>
</div>
<!-- end of .row-fluid -->

<%-- Secure Inbox Start--%>
<%-- <jsp:include page="../../inbox/includeInboxCompose.jsp"></jsp:include> --%>
<%-- Secure Inbox End--%>

<script type="text/javascript">

$(document).ready(function() {
	validateContactExchangeForm();
	$('.date-picker').datepicker({
	    autoclose: true,
	    format: 'mm/dd/yyyy'
	});

	//Added for ADA JAWS -->
	$('.dropdown').on('click', function() {
		if($(this).hasClass('open')) {
			$('this').find('a.dropdown-toggle').attr('aria-expanded', 'false');
		} else {
			$('this').find('a.dropdown-toggle').attr('aria-expanded', 'true');
		}
	});

    $(".reset-form").click(function() {
    	$('#priority').html('');
    	document.getElementById("saveIndTicketForm").reset();

    });

    //Code to add onlick event for pagination links
    if(${CA_STATE_CODE} == true){


        $('#paginationContainer').find('a').click(function(e) {
        	//prevent Default functionality
            e.preventDefault();

            var individualIdArray = new Array();
            var currentPageNum = $("#currentPageNumber").val();
			triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal - Pending Delegations', 'click', 'Page Number - ' + currentPageNum]);
            <c:forEach items="${individualsId}" var="oneId" varStatus="status">
            	individualIdArray.push(${oneId});
        	</c:forEach>

            var newForm = document.createElement("form");
			var existURL = $(this).attr('href');

			if (existURL.indexOf('individualsIdAtClient' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('individualsIdAtClient'));
		        var suffix = existURL.substring(existURL.indexOf('individualsIdAtClient'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }
			if (existURL.indexOf('totalCountAtClient' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('totalCountAtClient'));
		        var suffix = existURL.substring(existURL.indexOf('totalCountAtClient'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }

			if (existURL.indexOf('landingFromPrev' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('landingFromPrev'));
		        var suffix = existURL.substring(existURL.indexOf('landingFromPrev'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }

            $(newForm).attr("action", existURL)
                   .attr("method", "post");

            $(individualIdArray).each(function( index ) {
            	 $(newForm).append('<input type="hidden" name="individualsIdAtClient" value="' + individualIdArray[index] + '" />');
            });
            $(newForm).append('<input type="hidden" name="totalCountAtClient" value="' + ${resultSize}  + '" />');

            $(newForm).append('<input type="hidden" name="csrftoken" value="' + $("#csrftoken").attr('value') + '" />');

            //If Landing from Prev click
            var textOfPageLink =  $(this).text();
            textOfPageLink = textOfPageLink.trim();
            var textOfPageLinkArray = textOfPageLink.split( /\s+/);
            var isnum = /^\d+$/.test(textOfPageLinkArray[textOfPageLinkArray.length-1]);
            if(!isnum){
            	var localTempPageNumStr = getParameterByName('pageNumber',existURL);
            	isnum = /^\d+$/.test(localTempPageNumStr);
            	if(isnum && ((parseInt(localTempPageNumStr)%10)==0) ){
            		$(newForm).append('<input type="hidden" name="landingFromPrev" value="true" />');
            	}

            }


            document.body.appendChild(newForm);
            $(newForm).submit();
            document.body.removeChild(newForm);
        	return false;
    	});
    }


	$(".inddetail").click(
			function(e) {
				e.preventDefault();
				var href = $(this).attr('href');
				if (href.indexOf('#') != 0) {
					$(
							'<div id="inddetail" class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body"><iframe src="'
									+ href
									+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;min-height:293px;"></iframe> <button class="btn offset2" data-dismiss="modal" aria-hidden="true"><spring:message code="label.brkCancel"/></button></div></div>')
							.modal();
				}
	});


	var desigStatus = '${param.desigStatus}';
	if($(null != 'table[name="indTable${desigStatus}"]').tablesorter) {
		if(desigStatus =="Active"){
			$('table[name="indTable${desigStatus}"]').tablesorter({
				sortList : [ [ 1, 0 ]]
			});
		}
		else if(desigStatus =="Pending"){
			$('table[name="indTable${desigStatus}"]').tablesorter({
				sortList : [ [ 3, 0 ]]
			});
		}else{
			$('table[name="indTable${desigStatus}"]').tablesorter({
				sortList : [ [2, 0 ]]
			});
		}
	}


});
	var validator = $("#individualsearch")
			.validate(
					{
						onkeyup : false,
						onclick : false,
						rules : {
							requestSentFrom : {
								chkDateFormat : true, chkDateFromAndTo:true
							},
							requestSentTo : {
								chkDateFormat : true, chkDateFromAndTo:true
							},
							inactiveDateFrom : {
								chkDateFormat : true, chkDateFromAndTo:true
							},
							inactiveDateTo : {
								chkDateFormat : true, chkDateFromAndTo:true
							}
						},
						messages : {
							requestSentFrom : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							},
							requestSentTo : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							},
							inactiveDateFrom : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							},
							inactiveDateTo : {
								chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
								chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
							}
						}
					});
	jQuery.validator.addMethod("chkDateFormat",
			function(value, element, param) {
				if(value == "") {
					return true;
				}

				var isValid = false;
				var reg = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
				if (reg.test(value)) {
					var splittedDate = value.split('/');
					var mm = parseInt(splittedDate[0], 10);
					var dd = parseInt(splittedDate[1], 10);
					var yyyy = parseInt(splittedDate[2], 10);
					var newDate = new Date(yyyy, mm - 1, dd);
					if ((newDate.getFullYear() == yyyy)
							&& (newDate.getMonth() == mm - 1)
							&& (newDate.getDate() == dd))
						isValid = true;
					else
						isValid = false;
				} else
					isValid = false;

				return isValid;

			});

	jQuery.validator.addMethod("chkDateFromAndTo", function(value, element, param) {
 		var requestSentFrom = $('#requestSentFrom').val();
		var requestSentTo = $('#requestSentTo').val();

		var inactiveDateFrom = $('#inactiveDateFrom').val();
		var inactiveDateTo = $('#inactiveDateTo').val();
		var fromDate;
		var toDate;

		if(requestSentFrom!=null && requestSentFrom!=''){
			fromDate = requestSentFrom;
		}
		if(requestSentTo!=null && requestSentTo!=''){
			toDate = requestSentTo;
		}
		if(inactiveDateFrom!=null && inactiveDateFrom!=''){
			fromDate = inactiveDateFrom;
		}
		if(inactiveDateTo!=null && inactiveDateTo!=''){
			toDate = inactiveDateTo;
		}
		if((fromDate != null) &&(toDate !=null)){
			if((new Date(fromDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3")))  > (new Date(toDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3")))){

				return false;
			}
		}
		return true;
});

	/* $(function() {
		$('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx + '/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true
			});
		});
	}); */

	function getParameterByName(name, url) {
	    if (!url) '10';
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	function validate(todo) {
		var confirmMessage = "Are you sure to " + todo + " ?";

		if (confirm(confirmMessage) == false) {
			return false;
		}
	};

	function closeMe(desigId, desigStatus, desigName) {
		if (desigStatus == '') {
			desigStatus = 'Pending';
		}

		var href = "/hix/broker/declinePopup?desigId=" + desigId
				+ "&prevStatus=" + desigStatus + "&fromModule=${encFromModule}";
		$(
				'<div id="declinepopup" class="modal"><div class="modal-header"><h3><spring:message code="label.indAreYouSure"/></h3></div><div class="modal-body"><iframe id="declinepopup" src="'
						+ href
						+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:240px;"></iframe></div></div>')
				.modal();
		$('#inddetail.modal').remove();
	}

	function closeIFrame() {
		$("#declinepopup").remove();
	}

	$(".validateDecline").on('click', function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		if (href.indexOf('#') != 0) {
			$('<div id="declinepopup" class="modal"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3><spring:message code="label.indAreYouSure"/></h3></div><div class="modal-body"><iframe id="declinepopup" src="'
							+ href
							+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:145px;"></iframe></div></div>')
					.modal();
		}
	});


	//If the Master checkbox is checked or unchchecked, process the following code
	$('input[name="check_all${desigStatus}"]').click(function() {
		if ($('input[name="check_all${desigStatus}"]').is(":checked")) {
			$('input[name="indCheckbox${desigStatus}"]').each(function() {
				$(this).attr("checked", true);
			});
		} else {
			$('input[name="indCheckbox${desigStatus}"]').each(function() {
				$(this).attr("checked", false);
			});
		}
	});
	//For each of the ind checkboxes, process the following code when they are changed
	$('input[name="indCheckbox${desigStatus}"]').bind(
			'change',
			function() {
				if ($('input[name="indCheckbox${desigStatus}"]').filter(
						':not(:checked)').length == 0) {
					$('input[name="check_all${desigStatus}"]').attr("checked",
							true);
				} else {
					$('input[name="check_all${desigStatus}"]').attr("checked",
							false);
				}
			});

	function countChecked() {
		var n = $("input[name='indCheckbox${desigStatus}']:checked").length;
		var itemsSelected = "<small>&#40; " + n
				+ (n === 1 ? " Item" : " Items")
				+ " Selected &#41;&nbsp;</small>";
		//$(this).find(".counter").text(itemsSelected);
		$("#counter").html(itemsSelected);
		if (n > 0) {
			$("#counter").show();
			$("#bulkActions").show();
		} else {
			$("#counter").show();
			$("#bulkActions").hide();
		}
	}
	countChecked();
	$(":checkbox").click(countChecked);

	function markInactive() {
		var ids = '';
		$.each($("input[name='indCheckbox${desigStatus}']:checked"),
				function() {
					ids += (ids ? ',' : '') + $(this).attr('value');
				});
		ids += ',';

		var desigStatus = '${param.desigStatus}';
		var href = '/hix/broker/declinePopup?desigId=' + ids + '&prevStatus='
				+ desigStatus + '&fromModule=${encFromModule}';
		$(
				'<div id="declinepopup" class="modal"><div class="modal-body"><iframe id="bulkdeclinepopup" src="'
						+ href
						+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:170px;"></iframe></div></div>')
				.modal();
	}

	function markInactive() {
		var ids = '';
		$.each($("input[name='indCheckbox${desigStatus}']:checked"),
				function() {
					ids += (ids ? ',' : '') + $(this).attr('value');
				});
		ids += ',';

		var desigStatus = '${param.desigStatus}';
		var href = '/hix/broker/declinePopup?desigId=' + ids + '&prevStatus='
				+ desigStatus + '&fromModule=${encFromModule}';
		$(
				'<div id="declinepopup" class="modal"><div class="modal-body"><iframe id="bulkdeclinepopup" src="'
						+ href
						+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:170px;"></iframe></div></div>')
				.modal();
	}

	function markInactiveIndividual(externalIndividualId) {

		var desigStatus = '${param.desigStatus}';
		var href = '/hix/broker/declinePopup?desigId=' + externalIndividualId + '&prevStatus='
				+ desigStatus + '&fromModule=${encFromModule}';
		$(
				'<div id="declinepopup" class="modal"><div class="modal-body"><iframe id="bulkdeclinepopup" src="'
						+ href
						+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:170px;"></iframe></div></div>')
				.modal();
	}


	function submitRequest(requestType) {
		var ids = '';
		$.each($("input[name='indCheckbox${desigStatus}']:checked"),
				function() {
					ids += (ids ? ',' : '') + $(this).attr('value');
				});
		ids += ',';

		var prevStatus = "${desigStatus}";
		var validUrl = "";
		if (requestType == 'InActive') {
			validUrl = "declineRequests/${encFromModule}";
		} else if (requestType == 'Active') {
			validUrl = "approveRequests/individuals";
		}
		$.ajax({
			url : validUrl,
			data : {
				ids : ids,
				desigStatus : prevStatus
			},
			success : function(data) {
				window.location.reload();
			}
		});
	}

	function resetAll() {
		 var  contextPath =  "<%=request.getContextPath()%>";
       var documentUrl = contextPath + "/broker/individuals/resetall?desigStatus="+"${desigStatus}";
      window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
      }

/*     $(function() {
    	$("#saveIndTicketData").click(function(e){

    		if($("#saveIndTicket").validate().form()) {
    			$("#saveIndTicket").submit();
    		}
    	});
    }); */

    //$(document).ready(function(){
    	 $(".contactExchangeLink").on('click', function(e) {
    			e.preventDefault();
    			$("#subject").val("");
    			$("#requestSubType").val("");
    			//var html = $("#contactExchnagePopup").modal();
    			var pathURL = "/hix/broker/ticket/create/"+ e.target.getAttribute("encryptedval")+"/${encFromModule}";
    			$.ajax({
    				url : pathURL,
    				type : "GET",
    				success: function(response){
    					var html = $("#contactExchnagePopup").modal();
    					loadData(response);

    	    		},
    	   			error: function(e){
    	    			alert("Failed to load Ticket page");
    	    		},
    			});


    		});


    		var requestTypeArray = {};

    		function loadData(response){
    			var responseData = response.split('|');
    			if((responseData != null) && (responseData.length == 4)){
    				var firstName = responseData[0];
    				var lastName = responseData[1];
    				var requestType = responseData[2];
    				requestTypeArray =  $.parseJSON(requestType);
    				var priorityType = responseData[3];

    				$(".indFirstName").html(firstName);
    				$(".indLastName").html(lastName);
    				$('#requestType').html('<option value="">Select</option>');
    			 	for ( var key in requestTypeArray) {
    				 	$('#requestType').append("<option value='"+key+"' >"+key + "</option>");
    			 	}
    			 	if(null != priorityType && undefined != priorityType){

    				 	var priorityTypeArray = priorityType.split(',');
    				 	$('#priority').html('');
    				 	for ( var i=0;i<priorityTypeArray.length;i++) {
        				 	var value = priorityTypeArray[i].replace(/\W/g,"")
    					   $('#priority').append("<option value='"+ value +"' >"+ priorityTypeArray[i].replace(/\W/g,"") + "</option>");

    				 	}
    				 	$('#priority').find('[value="Medium"]').attr({'selected':'selected'});
    			 	}
    			}
    		}

    		function loadRequestSubType(){
    			var subType = $('#requestType').val();
    			$('#requestSubType').html('<option value="">Select</option>');
    			var requestSubTypeArray = requestTypeArray[subType];
    			if(null != requestSubTypeArray && undefined != requestSubTypeArray){
    				 for ( var i=0;i<requestSubTypeArray.length;i++) {
    					 $('#requestSubType').append("<option value='"+requestSubTypeArray[i]+"' >"+requestSubTypeArray[i] + "</option>");
    			 	 }
    			}
    		}
    		var validateContactExchangeForm = function(){
    			$("#saveIndTicketForm").validate({
        			rules : {
        				    subject : { required : true },
        				    description : { required : true, maxlength : 2000  },
        				    priority : { required : true },
        				    type : { required : true },
        				    category : { required : true }

        			},
        			messages : {
        				subject : { required : '<span><em class="excl">!</em>Please enter Subject.</span>'},
        				description : { required : '<span><em class="excl">!</em>Please enter Description.</span>'},
        				priority : { required : '<span><em class="excl">!</em>Please enter Priority.</span>'},
        				type : { required : '<span><em class="excl">!</em>Please enter Request-Sub Type.</span>'},
        				category : { required : '<span><em class="excl">!</em>Please enter Request Type.</span>'},

        			},
        			errorClass: "error",
        			errorPlacement : function(error, element) {

        				var elementId = element.attr('id');

        				error.appendTo($("#" + elementId + "_error"));

        			},
        			submitHandler: function() {
                        saveTicket()
                    }
        		});

        		}

    	    function saveTicket() {
	    	    	var pathURL = "/hix/broker/ticket/create/${encryptedId}/${encFromModule}";
	    	    	$.ajax({
	    	    		url : pathURL,
	    	    		type : "POST",
	    	    		data :  $("#saveIndTicketForm").serialize(),
	    	    		success: function(response,xhr){
	    	    			var ticketNumber = response;
	    	    			$("#ticketNumber").html(ticketNumber);
	    	    			$('#ticketNum').val(response);
	    	    			//var ticketNumber = response.val;

	    	    			if(isInvalidCSRFToken(xhr))
	    						return;

	    	    			$(".modal").modal("hide");
	    	    			$("#ticketSuccessPopup").modal();
	    	    		},
	    	    		error: function(e){
	    	    				$('#ticketNum').val("");
	    	    				console.log('Fail to load Ticket Info');
	    	    		}
	    	    	});
    	    };

    	    function isInvalidCSRFToken(xhr){
    			var rv = false;
    			if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {
    			  alert($('Session is invalid').text());
    			  rv = true;
    			 }
    			return rv;
    		}

    	    function traverse(url){
    			var queryString = {};
    			url.replace(
    				    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
    				    function($0, $1, $2, $3) { queryString[$1] = $3; }
    				);
    			 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
    			 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
    			 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
    			 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
    			$("#individualsearch input[type=submit]").click();
    		}

   // });



</script>
<style>

#contactExchnagePopup {
	width:800px;
	display:none;
}
.row-fluid select.span12{width:99%;}
</style>

<c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
<div id="contactExchnagePopup" class="modal">
	<form action="" id="saveIndTicketForm" name="saveIndTicketForm">
		<div class="modal-header">
			<button type="button" class="close reset-form" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Contact ${exchangeName}</h3>
		</div>
		<div class="modal-body">
			<div class="contactExchangeContainer">
				<h2>Support Request for <span id="indFirstName" class="indFirstName"></span> <span id="indLastName" class="indLastName"></span></h2>
			    <div class="row-fluid ">
			    	<div class="span3 gray">

			    					<div class="control-group clearfix">
										<label for="requestType" class="control-label">Request Type <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
										<div class="controls">
											<select size="1" id="requestType" name="category" class="span12" aria-label="Request Type" onchange="loadRequestSubType();">
												<option value="">Select</option>
											</select>
										</div>
										<div id="requestType_error"></div>
									</div>
									<div class="control-group clearfix">
										<label for="requestType" class="control-label">Request Sub-Type <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
										<div class="controls">
											<select size="1" id="requestSubType" name="type" class="span12" aria-label="Request Type">
												<option value="">Select</option>
											</select>
										</div>
										<div id="requestSubType_error"></div>
									</div>
									<div class="control-group clearfix">
										<label for="priority" class="control-label">Priority <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
										<div class="controls">
									<select size="1" id="priority" name="priority" class="span12" aria-label="Request Type">
												<option value="">Select</option>
											</select>
										</div>
										<div id="priority_error"></div>
									</div>
			    		    	</div>
			    	<div class="span9 gray">
			    		<div class="control-group clearfix">
							<label for="subject" class="control-label">Subject <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>

							<div class="controls">
								<input type="text" name="subject" class="span12" maxlength="100"/>
								<div id="subject_error"></div>
							</div>
						</div>
						<div class="control-group clearfix">
							<label for="description"  class="control-label">Description <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<div id="description_error"></div>
								<textarea rows="20" cols="20" class="span12" name="description" maxlength="2000" id="description">
								Note: Please provide the following details to the best of your knowledge.

								Please enter the following primary consumer information (if available).
								Application ID:
								Consumer Last Name:
								DOB:
								Last 4 SSN:

								Please enter the following Carrier and Plan information (if available)
								Carrier Name:
								Consumer's Plan:

								Please enter as detailed as possible description of the issue.

								Please enter the steps that you have taken so far to assist the consumer with the above issue (if any)
								</textarea>
							</div>
						</div>
			    	</div>

			    </div>
			 </div><!-- contactExchangeContainer -->
		</div>
		<div class="modal-footer">
	        <button type="button" class="btn reset-form" data-dismiss="modal">Cancel</button>
		 	<input type="submit" class="btn btn-primary pull-right"  id="saveIndTicketData" name="saveIndTicketData" value="Submit" />
	    </div>
	</form>
</div>

<div id="ticketSuccessPopup" class="modal hide">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h3>Success!</h3></div><div class="modal-body">
	<p>A support request has been created for <span class="indFirstName"></span> <span class="indLastName"></span>. Please note down the following ticket number for your reference <span id="ticketNumber" class="display_block txt-center gutter10-t"></span></p>
	<p class="txt-center"><button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></p>
	</div>
</div>
</c:if>
