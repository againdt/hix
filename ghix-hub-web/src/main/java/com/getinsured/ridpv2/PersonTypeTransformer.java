package com.getinsured.ridpv2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.AbstractTransformer;
import com.getinsured.iex.hub.platform.FileBasedConfiguration;
import com.getinsured.iex.hub.platform.ObjectTransformationException;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ObjectFactory;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.PersonMiddleNameType;
import com.getinsured.ridpv2.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.PersonNameType;

public class PersonTypeTransformer extends AbstractTransformer {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private final Set<Character> acceptedChars = new HashSet<>(Arrays.asList('D', 'L', 'O'));

    private static ObjectFactory factory = new ObjectFactory();

    @Override
    public Object transform(Object obj) throws ObjectTransformationException {
        boolean pTypeTransEnabled = Boolean.valueOf(FileBasedConfiguration.getConfiguration().getProperty("fdh.ridp.personType.transformerEnabled.v1"));
        if(!pTypeTransEnabled) {
            logger.warn("Transformation for PersonType is disabled for RIDP");
            return obj;
        }
        if(!(obj instanceof PersonType)) {
            throw new ObjectTransformationException("Can not transform "+obj.getClass().getName()+" Expected "+PersonType.class.getName());
        }
        PersonType p = (PersonType)obj;
        PersonNameType pName = p.getPersonName();
        if(pName != null) {
            String givenName = pName.getPersonGivenName().getValue();
            PersonMiddleNameType middleNameType = pName.getPersonMiddleName();
            String surName = pName.getPersonSurName().getValue();

            String middleName = null;
            if(middleNameType != null) {
                middleName = middleNameType.getValue();
            }
            String[] fNm = this.handleFirstAndMiddleName(givenName, middleName);

            switch(fNm.length) {
                case 1:
                    givenName = fNm[0];
                    break;
                case 2:
                    givenName = fNm[0];
                    middleName = fNm[1];
                    break;
            }
            pName.getPersonGivenName().setValue(givenName);
            if(middleName != null) {
                middleName = processMiddleName(middleName);
                if(pName.getPersonMiddleName() != null) {
                    pName.getPersonMiddleName().setValue(middleName);
                } else{
                    PersonMiddleNameType personMiddleNameType = factory.createPersonMiddleNameType();
                    personMiddleNameType.setValue(middleName);
                    pName.setPersonMiddleName(personMiddleNameType);
                }
            }
            surName = this.handleSurname(surName);
            pName.getPersonSurName().setValue(surName);
            return p;
        }
        return null;
    }

    private String processMiddleName(String middleName) {
        String strippedAndReplacedString = StringUtils.stripAccents(middleName).replace("-", " ").replace(".", " ").replace(",", " ");
        String[] str = strippedAndReplacedString.split("\\s+");
        if(str.length > 1) {
            return str[0].trim();
        }
        return strippedAndReplacedString.trim();

    }

    private String[] handleFirstAndMiddleName(String firstName, String middleName) throws ObjectTransformationException {

        String[] splitStr;

        String strippedAccentsTrimmedString = StringUtils.stripAccents(firstName).trim();

        splitStr = strippedAccentsTrimmedString.split("\\s+", 2);

        for(int i = 0; i < splitStr.length; i++){
            splitStr[i] = splitStr[i].replaceAll("[^A-Za-z]", "");
        }

        switch(splitStr.length){
            case 0:
                throw new ObjectTransformationException("Transformation failed for " + firstName + " " + middleName);
            case 1:
                if(middleName != null && middleName.length() > 0){
                    return new String[]{splitStr[0], middleName};
                }else {
                    if(logger.isDebugEnabled()) {
                        logger.debug("Received furst name {}, changed to {}", firstName, splitStr);
                    }
                    return splitStr;
                }
            case 2:
                if(logger.isDebugEnabled()) {
                    logger.debug("Received first name {} changed to {}, Received middle name {} changed to {}",
                            firstName, splitStr[0], middleName, splitStr[1]);
                }
                return splitStr; //First name split into first and middle
        }
        return splitStr;
    }

    private String handleSurname(String surName) throws ObjectTransformationException {
        String strippedAndReplacedString = StringUtils.stripAccents(surName).trim().replace(" ", "-")
                .replace(".", "")
                .replace(",", "-");
        int indexOfApostrophe = strippedAndReplacedString.indexOf("'");
        if (indexOfApostrophe > 0) {
            logger.debug("Found apostraophe");
            char previousChar = strippedAndReplacedString.charAt(indexOfApostrophe - 1);
            if (!acceptedChars.contains(previousChar)) {
                logger.debug("Apostraophe not allowed for {} processing surname {}", previousChar, surName);
                throw new ObjectTransformationException("Invalid apostraophe presence");
            }
        }
        return strippedAndReplacedString;
    }


}
