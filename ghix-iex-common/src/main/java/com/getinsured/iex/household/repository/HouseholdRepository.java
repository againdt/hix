/**
 * 
 */
package com.getinsured.iex.household.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.consumer.Household;

/**
 * @author vishwanath_s
 *
 */
@Repository
@Transactional
public interface HouseholdRepository extends JpaRepository<Household, Integer> {

	List<Household> findByEmailIgnoreCaseOrderByIdDesc(String email);

	@Query("Select hh.id from Household as hh where hh.householdCaseId= :caseId Order by hh.id desc")
	List<Integer> findByCaseIdOrderByIdDesc(@Param("caseId") String caseId);
}
