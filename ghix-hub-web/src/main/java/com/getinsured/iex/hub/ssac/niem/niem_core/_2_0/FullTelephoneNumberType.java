
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for a full telephone number.
 * 
 * <p>Java class for FullTelephoneNumberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FullTelephoneNumberType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FullTelephoneNumberType")
public class FullTelephoneNumberType
    extends ComplexObjectType
{


}
