import { FetchService } from './../../services/fetch.service';
import { DataKeepService } from './../../services/data-keep.service';
import { ProviderFullData } from './../../models/data.model';
import { Component, EventEmitter, Output, Input, OnInit, OnChanges, ViewChild, ElementRef} from '@angular/core';
import 'rxjs/Rx';
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { Observer } from "rxjs/Observer";

import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'doc-search-typeahead',
    templateUrl: "./doc-search-typeahead.component.html",
    styleUrls: ["./doc-search-typeahead.component.scss"]
})
export class DocSearchTypeaheadComponent implements OnInit {
    @Input() public zipcode: string;
    @Input() public invalidZip: boolean;
    @Input() public searchtype: "doctor" | "facility";
    @Output() public selectedDoctor = new EventEmitter<ProviderFullData>();
    @Input() public over5: boolean;
    @Input() public selectedType: string;
    @Output() public inputFocused = new EventEmitter<boolean>();
    public provider: any;
    public typeStr: string;
    public typeaheadLoading: boolean;
    public typeaheadNoResults: boolean;
    public statesComplex: ProviderFullData[] = [];
    public inFocus = false;

    public invalid_flag = false;
    @ViewChild("docInput") private docInput: ElementRef;
    @ViewChild('ngbTypeahead') ngbTypeahead: NgbTypeahead;

    searching = false;
    searchFailed = false;
    hideSearchingWhenUnsubscribed = new Observable(() => () => {this.searching = false; });

    public constructor(
        private fetch: FetchService,
        private datakeep: DataKeepService
    ) { }

    search = (text$: Observable<string>) =>
    text$
      .debounceTime(300)
      .do((queryString: string)=>{
          if( queryString.length <3){
              this.searchFailed = false;
          }
      })
      .filter((queryString: string) => queryString.length >= 3 && this.zipcode && !this.over5)
      .distinctUntilChanged()
      .do(() => {
          this.searching = true;
          this.searchFailed = false;
        })
        .switchMap((queryString: string) => this.getDocsAsObservable(queryString)
            .do((val) =>{
                this.searchFailed = false;
                if(val.length === 0 || val === undefined)
                {
                    this.searchFailed = true;

                }
            })
            .catch(() => {
                this.searchFailed = true;
                return Observable.of([]);
            }))
        .do(() => {
            this.searching = false;
        })
        .merge(this.hideSearchingWhenUnsubscribed);

    formatter = (x: {name: string}) => x.name;

    public ngOnInit() {
    
    	this.typeStr = `Type ${this.searchtype} name to search near zip ${this.zipcode}. After you type, use arrow key to navigate through the options and press enter to select. You can add up to 5 doctors or facilities`;
    	
    	const isQueried$ = new Subject<boolean>();
    }

	public ngOnChanges(){
        this.typeStr = `Type ${this.searchtype} name to search near zip ${this.zipcode}. After you type, use arrow key to navigate through the options and press enter to select. You can add up to 5 doctors or facilities`;
    }
    public getDocsAsObservable(queryString: string): Observable<any> {
        const query = new RegExp(queryString, 'ig');
        const isQueried$ = new Subject<boolean>();

        return Observable.of(queryString)
            .delayWhen((queryString: string) => {
                this.fetch.doctorSearch(queryString, this.zipcode, this.searchtype).subscribe(
                    val => {
                        if (val.status === "FAILURE") {
                            console.error(val.error);
                        } else {
                            // success
                            if (val.providers.length !== 0) {
                                this.statesComplex = val.providers;
                            } else {
                                this.statesComplex = []
                            }
                        }
                    },
                    err => {
                        //console.log(err);
                    },
                    () => {
                        isQueried$.next(true);
                    }
                )

                return isQueried$;
            })
            .switchMap((queryString: string) => {
                return Observable.of(this.statesComplex);
            });


    }

    public getStatesAsObservable(queryString: string): Observable<any> {
        const query = new RegExp(queryString, 'ig');
        const isQueried$ = new Subject<boolean>();

        return Observable.of(queryString)
            .delayWhen((queryString: string) => {
                this.fetch.doctorSearch(queryString, this.zipcode, this.searchtype).subscribe(
                    val => {
                        if (val.status === "FAILURE") {
                            console.error(val.error);
                        } else {
                            // success
                            if (val.providers.length !== 0) {
                                this.statesComplex = val.providers;
                            } else {
                                this.statesComplex = []
                            }
                        }
                    },
                    err => {
                       // console.log(err);
                    },
                    () => {
                        isQueried$.next(true);
                    }
                )

                return isQueried$;
            })
            .switchMap((queryString: string) => {
                return Observable.of(this.statesComplex);
            });
    }


    public changeTypeaheadLoading(e: boolean): void {

        this.typeaheadLoading = this.provider && this.provider.length >= 3 && e && !this.over5;
        this.invalid_flag = false;
    }

    public changeTypeaheadNoResults(e: boolean): void {
        this.typeaheadNoResults = e;
        this.invalid_flag = true;
    }


    //select doctor from the dropdown list
    public onSelect(e: any): void {
        //console.log("provider: "+ this.provider);
        this.provider = "";
        //console.log('Selected value: ', e.item.name);
        let type
        if(this.searchtype === "facility"){
            type = "hospital";
        }else if(this.searchtype ==="doctor"){
            type = "doctor";
        }
        this.selectedDoctor.emit(Object.assign(e.item, { providerType: type }));
        //this.docInput.nativeElement.setAttribute("aria-label", "added " + e.item.name);
        //this.docInput.nativeElement.blur();
        //console.log(document.activeElement);
    }
    public focusEvent() {
        this.inFocus = true;
        this.inputFocused.emit(this.inFocus);
        //console.log("focus in input: " + this.inFocus);
    }
    public blurEvent() {
        this.inFocus = false;
        this.inputFocused.emit(this.inFocus);
        //this.searchFailed = false;
    }

    public clearInput() {
        this.docInput.nativeElement.value = "";
        this.provider = "";
        //console.log("docinput: " + this.docInput.nativeElement.value);
        //console.log("docinput model: " + this.provider);
    }
    
    public onProviderChange(){
    	if(this.provider.length < 3){
    		this.ngbTypeahead.dismissPopup();
    	}
    }


}



