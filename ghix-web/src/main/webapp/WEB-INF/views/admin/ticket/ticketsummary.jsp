<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<style>
    #sidebar form#sendSecureTicketMessage,
    #sidebar form#uploadDocumentModal {
        border: none;
        margin: 0;
    }
</style>
<c:set var="encTicketid" scope="request"><encryptor:enc value="${param.ticketId}" isurl="true"/> </c:set>
<jsp:include page="addComments.jsp">
    <jsp:param value="${encTicketid}" name="ticketId"/>
</jsp:include>
<jsp:include page="cancelTicket.jsp">
    <jsp:param value="${encTicketid}" name="ticketId"/>
</jsp:include>
<jsp:include page="reassignTicket.jsp">
    <jsp:param value="${encTicketid}" name="ticketId"/>
</jsp:include>
<jsp:include page="completeTicket.jsp">
    <jsp:param value="${encTicketid}" name="ticketId"/>
</jsp:include>
<jsp:include page="completeVerifyTicket.jsp">
    <jsp:param value="${encTicketid}" name="ticketId"/>
</jsp:include>
<jsp:include page="sendSecureMsg.jsp">
    <jsp:param value="${encTicketid}" name="ticketId"/>
</jsp:include>
<jsp:include page="reclassifyTicket.jsp">
    <jsp:param value="${encTicketid}" name="ticketId"/>
</jsp:include>
<jsp:include page="attachToTicket.jsp">
    <jsp:param value="${encTicketid}" name="ticketId"/>
</jsp:include>
<jsp:include page="reopenTicket.jsp">
    <jsp:param value="" name=""/>
</jsp:include>
<jsp:include page="restartTicket.jsp">
    <jsp:param value="" name=""/>
</jsp:include>

<div class="header">
    <h4><spring:message code="header_tkt.abtThisTkt"/></h4>
</div>
<ul class="nav nav-list">
    <%--   <input type="hidden" value="<encryptor:enc value="${param.ticketId}" isurl="true"/>" name="encTicketId" id="encTicketId"> --%>

    <c:set var="encTicketid"><encryptor:enc value="${param.ticketId}" isurl="true"/> </c:set>
    <%--  <input type="text" value="${encTicketid}" >  --%>
    <li id="ticketdetail" class="navmenu"><a href="../ticketdetail/${encTicketid}"><spring:message
            code="lbl_tk.summary"/></a></li>
    <li id="tickethistory" class="navmenu"><a href="../tickethistory/${encTicketid}"><spring:message
            code="link_tkt.tktHistory"/></a></li>
    <li id="showcomments" class="navmenu"><a href="../showcomments/${encTicketid}"><spring:message
            code="lbl.comments"/></a></li>
    <li id="doclist" class="navmenu"><a href="../getTicketDocuments/${encTicketid}"><spring:message
            code="link_tkt.attachments"/></a></li>
    <li id="tasklist" class="navmenu"><a href="../getticketTaskList/${encTicketid}"><spring:message
            code="link_tkt.tasks"/></a></li>
</ul>
<br/>
<div class="header">
    <h4><i class="icon-cog"></i> <spring:message code="header_tkt.actions"/></h4>
</div>

<ul class="nav nav-list">
    <c:choose>
        <c:when test="${(TICKET_STATUS=='Completed')}">
            <li id="cancelTicket" class="navmenu disabled"><a href="#"><i class="icon-remove"></i><spring:message
                    code="lbl.cancelTicket"/></a></li>
            <li id="sendMessage" class="navmenu disabled"><a href="#"><i
                    class="icon-envelope icon-white"></i><spring:message code='link_tkt.sendMsg'/></a></li>
            <li id="reassign" class="navmenu disabled"><a href="#"><i class="icon-exchange"></i><spring:message
                    code="option_tktTask.reAssign"/></a></li>
            <li id="upload" class="navmenu disabled"><a href="#"><i class="icon-upload-alt"></i><spring:message
                    code='link_tkt.addAttachment'/></a></li>
            <li id="addcomments" class="navmenu disabled"><a href="#"><i class="icon-upload"></i><spring:message
                    code='link_tkt.addComment'/></a></li>
            <li id="cancelTicket" class="navmenu" hidden=true><a href="#"><i
                    class="icon-comments-alt"></i><spring:message code="option_tktTask.archiveTkt"/></a></li>
            <c:if test="${quickAction.taskStatus=='Claimed'}">
                <li class="navmenu" id="completeTicket-${ticket.id}">
                    <a href="javascript:completeTicket('${encTicketid}', '${tkmTicketsObj.tkmWorkflows.category}')"><i
                            class="icon-refresh"></i> <spring:message code="option_tktTask.markAsComplete"/> </a></li>
            </c:if>
            <c:if test="${quickAction.taskStatus=='UnClaimed'}">
                <li class="navmenu" id="completeTicket-${ticket.id}">
                    <a href="javascript:completeTicket('${encTicketid}', '${tkmTicketsObj.tkmWorkflows.category}')"><i class="icon-refresh"></i><spring:message
                        code="option_tktTask.claim"/> </a></li>
            </c:if>
        </c:when>
        <c:when test="${(TICKET_STATUS=='Canceled')}">
            <li id="cancelTicket" class="navmenu disabled"><a href="#"><i class="icon-remove"></i><spring:message
                    code="lbl.cancelTicket"/></a></li>
            <li id="sendMessage" class="navmenu disabled"><a href="#"><i
                    class="icon-envelope icon-white"></i><spring:message code='link_tkt.sendMsg'/></a></li>
            <li id="reassign" class="navmenu disabled"><a href="#"><i class="icon-exchange"></i><spring:message
                    code="option_tktTask.reAssign"/></a></li>
            <li id="upload" class="navmenu disabled"><a href="#"><i class="icon-upload-alt"></i><spring:message
                    code='link_tkt.addAttachment'/></a></li>
            <li id="addcomments" class="navmenu disabled"><a href="#"><i class="icon-upload"></i><spring:message
                    code='link_tkt.addComment'/></a></li>

            <c:choose>
                <c:when test="${(tkmTicketsObj.isArchived == 'Y' )}">
                    <li class="navmenu disabled" id="reopen"><a href="#"><i class="icon-folder-open"></i> Reopen</a>
                    </li>
                    <li class="navmenu disabled" id="restart"><a href="#"><i class="icon-refresh"></i> Restart</a></li>
                </c:when>
                <c:otherwise>
                    <li class="navmenu" id="reopen"><a href="javascript:reopenTicketFunction('${encTicketid}')"><i
                            class="icon-folder-open"></i> Reopen</a></li>
                    <li class="navmenu" id="restart"><a href="javascript:restartTicketFunction('${encTicketid}')"><i
                            class="icon-refresh"></i> Restart</a></li>
                </c:otherwise>
            </c:choose>

            <li id="cancelTicket" class="navmenu" hidden=true><a href="#"><i
                    class="icon-comments-alt"></i><spring:message code="option_tktTask.archiveTkt"/></a></li>
            <li id="reclassify" class="navmenu" hidden=true><a href="#"><i class="icon-comments-alt"></i></a></li>

        </c:when>
        <c:when test="${(TICKET_STATUS=='Resolved')}">
            <li id="cancelTicket" class="navmenu disabled"><a href="#"><i class="icon-remove"></i><spring:message
                    code="lbl.cancelTicket"/></a></li>
            <li id="sendMessage" class="navmenu disabled"><a href="#"><i
                    class="icon-envelope icon-white"></i><spring:message code='link_tkt.sendMsg'/></a></li>
            <li id="reassign" class="navmenu disabled"><a href="#"><i class="icon-exchange"></i><spring:message
                    code="option_tktTask.reAssign"/></a></li>
            <li id="upload" class="navmenu disabled"><a href="#"><i class="icon-upload-alt"></i><spring:message
                    code='link_tkt.addAttachment'/></a></li>
            <li id="addcomments" class="navmenu disabled"><a href="#"><i class="icon-upload"></i><spring:message
                    code='link_tkt.addComment'/></a></li>

            <c:choose>
                <c:when test="${(tkmTicketsObj.isArchived == 'Y' )}">
                    <li class="navmenu disabled" id="reopen"><a href="#"><i class="icon-folder-open"></i> Reopen</a>
                    </li>
                    <li class="navmenu disabled" id="restart"><a href="#"><i class="icon-refresh"></i> Restart</a></li>
                </c:when>
                <c:otherwise>
                    <li class="navmenu" id="reopen"><a href="javascript:reopenTicketFunction('${encTicketid}')"><i
                            class="icon-folder-open"></i> Reopen</a></li>
                    <li class="navmenu" id="restart"><a href="javascript:restartTicketFunction('${encTicketid}')"><i
                            class="icon-refresh"></i> Restart</a></li>
                </c:otherwise>
            </c:choose>

            <li id="cancelTicket" class="navmenu" hidden=true><a href="#"><i
                    class="icon-comments-alt"></i><spring:message code="option_tktTask.archiveTkt"/></a></li>
            <li id="reclassify" class="navmenu" hidden=true><a href="#"><i class="icon-comments-alt"></i></a></li>
        </c:when>
        <%--   <c:when test="${(tkmTicketsObj.tkmWorkflows.category == 'Document Verification' )}">
                <li id="reclassify" class="navmenu" hidden=true><a href="#"><i class="icon-comments-alt"></i></a></li>
           </c:when> --%>
        <c:otherwise>
            <li id="cancelTicket" class="navmenu"><a href="#" onclick="javascript:showCancelDiv()" role="button"
                                                     data-toggle="modal"><i class="icon-ok"></i><spring:message
                    code="lbl.cancelTicket"/></a></li>
            <c:if test="${quickAction.taskStatus=='Claimed'}">
                <li class="navmenu" id="completeTicket-${ticket.id}">
                    <a href="javascript:completeTicket('${encTicketid}','${tkmTicketsObj.tkmWorkflows.category}')"><i
                        class="icon-refresh"></i> <spring:message code="option_tktTask.markAsComplete"/> </a></li>
            </c:if>
            <c:if test="${quickAction.taskStatus=='UnClaimed'}">
                <li class="navmenu" id="completeTicket-${ticket.id}">
                    <a href="javascript:completeTicket('${encTicketid}', '${tkmTicketsObj.tkmWorkflows.category}')"><i class="icon-refresh"></i><spring:message
                        code="option_tktTask.claim"/> </a></li>
            </c:if>
            <li id="sendMessage" class="navmenu"><a href="#" onclick="javascript:showSendMessageDlg();" role="button"
                                                    data-toggle="modal"><i
                    class="icon-envelope icon-white"></i><spring:message code='link_tkt.sendMsg'/></a></li>

            <c:if test="${quickAction.taskStatus=='Claimed'}">
                <li id="reassign" class="navmenu"><a href="#" onclick="javascript:divpopulate()" role="button"
                                                     data-toggle="modal"><i class="icon-exchange"></i> <spring:message
                        code="option_tktTask.reAssign"/></a></li>
            </c:if>
            <c:if test="${quickAction.taskStatus=='UnClaimed'}">
                <c:if test="${isAssignAllowed == true }">
                    <li id="assignTicket" class="navmenu"><a href="#" onclick="javascript:divpopulate('Assign')"
                                                             role="button" data-toggle="modal"><i
                            class="icon-exchange"></i> Assign Ticket</a></li>
                </c:if>
            </c:if>


            <li id="reassign" class="navmenu" style="display: none;"><a href="#reassignDialog"
                                                                        id="displayReassignDialog" role="button"
                                                                        data-toggle="modal"><i
                    class="icon-exchange"></i> <spring:message code="option_tktTask.reAssign"/></a></li>
            <li id="upload" class="navmenu"><a href="#uploadDocumentDialog" id="displayuploadDialog" role="button"
                                               data-toggle="modal"><i class="icon-upload-alt"></i><spring:message
                    code='link_tkt.addAttachment'/></a></li>
            <li id="addcomments" class="navmenu"><a href="#addCommentsDlg" role="button" data-toggle="modal"><i
                    class="icon-comments-alt"></i><spring:message code='link_tkt.addComment'/></a></li>
            <li id="archiveTicket" class="navmenu disabled hide"><a href="#" role="button" data-toggle="modal"><i
                    class="icon-comments-alt"></i><spring:message code="option_tktTask.archiveTkt"/></a></li>

            <c:choose>
                <c:when test="${(tkmTicketsObj.tkmWorkflows.category == 'Document Verification' )}">
                    <li id="reclassify" class="navmenu" hidden=true><a href="#"><i class="icon-comments-alt"></i></a>
                    </li>
                </c:when>

                <c:otherwise>
                    <li id="reclassify"><a href="#" onclick="javascript:populateTicketType()" role="button"
                                           data-toggle="modal"><i class="icon-comments-alt"></i>Re-Classify Ticket</a>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:otherwise>
    </c:choose>
    <c:if test="${tkmTicketsObj.moduleName == 'HOUSEHOLD' }">
        <c:if test="${permissionMap.memberViewViewMemAcct == 'true' }">
            <li class="navmenu"><a href="javascript:void(0)" data-target="#markCompleteDialog" role="button"
                                   data-toggle="modal"><i class="icon-eye-open"></i>View Member Account</a></li>
        </c:if>
    </c:if>
    <li id="back"><a href="<c:url value="${param.backButtonUrl}" />" onclick="" role="button"><i
            class="icon-step-backward"></i>Back</a></li>
    <!-- <li class="navmenu"><a href="#"><i class="icon-ok"></i>Reassign</a></li> -->
</ul>

<form name="dialogForm" id="dialogForm" method="POST" action="<c:url value="/crm/consumer/dashboard" />"
      novalidate="novalidate">
    <df:csrfToken/>
    <div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog"
         aria-labelledby="markCompleteDialog" aria-hidden="true">
        <div class="markCompleteHeader">
            <div class="header">
                <h4 class="margin0 pull-left">View Member Account</h4>
                <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x"
                        type="button">x
                </button>
            </div>
        </div>

        <div class="modal-body clearfix gutter10-lr">
            <div class="control-group">
                <div class="controls">
                    Clicking "Member View" will take you to the Member's portal
                    for ${tkmTicketsObj.role.firstName} ${tkmTicketsObj.role.lastName}.<br/>
                    Through this portal you will be able to take actions on behalf of the member.<br/>
                    Proceed to Member view?
                </div>
            </div>
        </div>
        <div class="modal-footer clearfix">
            <input class="pull-left" type="checkbox" id="checkConsumerView" name="checkConsumerView">
            <div class="pull-left">&nbsp; Don't show this message again.</div>
            <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
            <button class="btn btn-primary" type="submit">Member View</button>
            <input type="hidden" name="switchToModuleName" id="switchToModuleName"
                   value="<encryptor:enc value="individual"/>"/>
            <input type="hidden" name="switchToModuleId" id="switchToModuleId"
                   value="<encryptor:enc value="${householdId}"/>"/>
            <input type="hidden" name="switchToResourceName" id="switchToResourceName"
                   value="${tkmTicketsObj.role.firstName} ${tkmTicketsObj.role.lastName}"/>
        </div>
    </div>
</form>

<div id="reassignDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="reassignDialog"
     aria-hidden="true">
    <div class="markCompleteHeader">
        <div class="header">
            <h4 class="pull-left"><spring:message code="option_tktTask.reAssign"/></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="close" title="x" type="button">
                <spring:message code="btn.crossClose"/></button>
        </div>
    </div>

    <div class="modal-body">
        <div class="control-group">
            <input type="hidden" id="tkmTaskId" name="">
            <div class="controls">
                <div>
                    <spring:message code="text_tkt.queue"/> <img
                        src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10"
                        alt="Required!"/>
                    <select class="input-medium"
                            id="activitiTaskQueues" onchange="javascript:loadQueueUsers();">
                        <option value=""><spring:message code="label.select"/></option>

                    </select>

                </div>
                <div style="display: none;" id="selectTaskUser">
                    <spring:message code="text_tkt.assignee"/> <img
                        src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10"
                        alt="Required!"/>
                    <select class="input-medium" id="activitiTaskUser" style="margin-left: 10px;">
                    </select>
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer clearfix">
        <button class="btn pull-left" data-dismiss="modal" aria-hidden="true"
                onclick="javascript:denyCancelDialogue();"><spring:message code="button.cancel"/></button>
        <button class="btn btn-primary" onclick="javascript:reassignSubmit();"><spring:message
                code="button_tkt.submit"/></button>
    </div>
</div>

<script type="text/javascript">
    function showCancelDiv() {
        $('#tkmErrorDiv').html("");
        $('#confirmCancelDlg').modal('show');
    }


    function reassignSubmit() {
        //Check if group is selected
        if ($('#activitiTaskQueues').val() == '' || $('#activitiTaskQueues').val() == 'Select') {
            alert("Please select a Queue Name");
            return;
        }

        //If task is claimed, check the users selection
        if (taskClaimed) {
            //If the user is not selected
            if ($('#activitiTaskUser').length > 0 && ($('#activitiTaskUser').val() == '' || $('#activitiTaskUser').val() == 'Select')) {
                alert("Please select Assignee");
                return;
            }
        }

        performReassign();
    }


    function notifyCancelResponse(status, ticketid) {
        if (status == true) {
            $('#editBtnDiv').hide();
            $('#cancelTicket a').attr("href", "#");
            $('#cancelTicket').attr('disabled', 'disabled');
            window.location.reload(true);
        } else {
            alert("Ticket cancellation failed");
        }
    }

    function denyCancelDialogue() {
        $('#tkmErrorDiv').html("");
    }

    function notifyReassignResponse(status, ticketId, assignee, queueName, action) {
        if (action == true) {
            var redirectUrl = '<c:url value="/ticketmgmt/ticket/ticketdetail/'+'${encTicketId}'+'"> </c:url>';
            window.location.href = redirectUrl;
        } else {
            if (status == true) {
                window.location.reload(true);
            } else {
                alert("Ticket Reassign failed");
            }
        }
    }

    function notifyAddCommentsResponse(status, ticketid) {
        if (status == true) {
            window.location.reload(true);
        } else {
            alert("comments were not added");
        }
    }

    function notifyClaimResponse(ticketId) {
        window.location.reload(true);
    }

    function notifyCompleteResponse(status) {
        if (status == true) {
            window.location.reload(true);
        } else {
            var redirectUrl = '<c:url value="/ticketmgmt/ticket/ticketlist"> </c:url>';
            window.location.href = redirectUrl;
        }
    }

    function completeTicket(ticketid, ticketCategory) {
        // Any Verify Document Ticket Type should go to getTicketActiveTaskDataVerifyDoc
        if (ticketCategory && ticketCategory === "Document Verification") {
            document.getElementById("ticketIdVerify").value = ticketid;
            document.getElementById("ticketId").value = ticketid;
            $('#validateVerifyComments').val('Y');
            getTicketActiveTaskDataVerifyDoc();
        } else {
            document.getElementById("ticketId").value = ticketid;
            $('#validateComments').val('N');
            getTicketActiveTaskData();
        }
    }

    function notifyReClassifyResponse(status) {
        //window.location.reload(true);
        window.location = window.location;
        alert("Reclassification done successfully");
    }
</script>
