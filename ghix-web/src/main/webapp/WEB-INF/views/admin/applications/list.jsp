<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<div class="row-fluid">

	<div class="page-header gutter10">
		<h1><a class="skip">Manage Applications</a></h1>
	</div><!-- end of page-header -->
	
	<form class="form-vertical" id="frmApplication" name="frmApplication" action="<c:url value="/admin/applications/list" />"  method="POST">
		<div class="form-actions form-actions-top">
			<div class="row-fluid">
				<div class="span2">
					<label>Policy Number</label>
					<div class="controls">
						<input type="text" name="policynumber" id="policynumber" value="${searchCriteria.policynumber}"  class="span12" size="30" />
					</div><!-- end of controls -->
				</div>
		
				<div class="span2">
					<label>Status</label>
					<div class="controls">
						<select  name="status" id="status" class="input-small">
		 					<option value="">SELECT</option>
		 					<c:forEach items="${statusList}" var="status">
						    	<option ${searchCriteria.status == status    ? 'selected="selected"' : ''} value="${status}"> ${status} </option>
						    </c:forEach>
						</select>
					</div><!-- end of controls -->
				</div>
				
				<div class="span2">
					<label>Plan Type</label>
					<div class="controls">
						<select  name="plantype" id="plantype" class="input-small">
		 					<option value="">SELECT</option>
		 					<c:forEach items="${plantypes}" var="plantype">
						  	  <option ${searchCriteria.plantype == plantype  ? 'selected="selected"' : ''} value="${plantype}">${plantype}</option>
						    </c:forEach>
						</select>
					</div><!-- end of controls -->
				</div>
				
				<div class="span3">
					<label>Issuer</label>
					<div class="controls">
						<select  name="issuer" id="issuer" class="input-small">
		 					<option value="">SELECT</option>
		 					<c:forEach items="${issuers}" var="issuer">
						  	  <option ${searchCriteria.issuer == issuer.id  ? 'selected="selected"' : ''} value="${issuer.id}">${issuer.name}</option>
						    </c:forEach>
						</select>
					</div><!-- end of controls -->
				</div>
				
				<div class="span3">
					<label>&nbsp;<!-- intentionally left blank --></label>
					<input type="submit" name="submitbtn" id="submitbtn" value="Search" class="btn btn-primary" title="Search"/> 
				</div><!-- end of control-group -->
			</div>
		</div><!-- end of form-actions -->
	</form>
	
	<div id="applicationlist">
		<c:choose>
			<c:when test="${fn:length(applicationlist) > 0}">
				<table class="table">
					<thead>
						<tr> <th><dl:sort title="ID" sortBy="id" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th><dl:sort title="Number" sortBy="policyNumber" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th><dl:sort title="Status" sortBy="status" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th><dl:sort title="Type" sortBy="planType" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th><dl:sort title="Plan Name" sortBy="plan.name" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th><dl:sort title="Issuer Name" sortBy="issuer.name" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th><dl:sort title="Effective Date" sortBy="effectiveDate" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th><dl:sort title="Termination Date" sortBy="terminationDate" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						</tr>
					</thead>
					<c:forEach items="${applicationlist}" var="application">
							<tr>
								<td> ${application.id} </td>
								<td> ${application.policyNumber} </td>
								<td> ${application.status} </td>
								<td> ${application.planType} </td>
								<td> ${application['plan:name']} </td>
								<td> ${application['issuer:name']} </td>
								<td> <fmt:formatDate value="${application.effectiveDate}" pattern="MM/dd/yyyy"/></td>
								<td> <fmt:formatDate value="${application.terminationDate}" pattern="MM/dd/yyyy"/></td>
							</tr>
						</c:forEach>
				</table>
				
				<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
				
			</c:when>
			<c:otherwise>
				<hr />
				<div class="alert alert-info">No matching records found.</div>
			</c:otherwise>
		</c:choose>
	</div> <!-- end of application list -->
</div><!-- end of .row-fluid -->
	