<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="<c:url value="/planmgmt/issuer/representatives/add"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.newIssuerApplication"/></a></li>
            <li><spring:message  code="pgheader.step"/> 2</li>
        </ul><!--page-breadcrumb ends-->
		<h1><issuerLogo:view issuerName="" /><spring:message  code="pgheader.newIssuerApplication"/></h1>
</div>
		<div class="row-fluid">
        	<div class="gutter10">
                <div class="span3" id="sidebar">
                    <div class="header">
                        <h4 class="margin0"><spring:message  code="pgheader.steps"/></h4>
                    </div>
                    <!--  beginning of side bar -->
                    <ol class="nav nav-list">
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.addRepresentative"/></a></li>
                        <li class="active"><a href="#"><spring:message  code="pgheader.issuerApplication"/></a></li>
                        <li><a href="#"><spring:message  code="pgheader.financialInformation"/></a></li>
                        <li><a href="#"><spring:message  code="pgheader.updateProfile"/></a></li>
                        <li><a href="#"><spring:message  code="pgheader.signApplication"/></a></li>
                    </ol>
                    <!-- end of side bar -->
                </div>
                <!-- end of span3 -->
                <div class="span9" id="rightpanel">
                    <div class="header">
                        <h4 class="span10"><spring:message  code="pgheader.step"/> 2: <spring:message  code="pgheader.issuerApplication"/></h4>
                    </div>
                    <div class="gutter10">
                        <p><spring:message  code="pgheader.uploadDocumentsInfo"/></p>
                
                    <form class="form-horizontal" id="frmIssuerApp" name="frmIssuerApp" action="<c:url value="/planmgmt/issuer/application/documents/save"/>" method="POST">
                        <div class="header">
                            <h4><spring:message  code="label.issuerUploadAuthority"/></h4>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="certificate_auth"><spring:message  code="label.uploadFile"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="file" id="authority" name="authority"  class="input-file" />
                                <button type="submit" id="btn_upload_authority" name="btn_upload_authority" class="btn"><spring:message  code="label.upload"/></button>
                                <div id="authority_display"></div>
                                <input type="hidden" id="hdnAuthority" name="hdnAuthority" value=""/>
                                <div id="authority_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="header">
                            <h4><spring:message  code="label.issuerUploadAccreditation"/></h4>
                        </div>
                        <div class="gutter10-t">
                        <div class="control-group">
                            <label class="control-label" for="certificate_auth"><spring:message  code="label.issuerUploadAccreditation"/></label>
                            <div class="pull-left marginL20">
                                <select class="input-small" name="issuerAccreditation" id="issuerAccreditation">
                                    <option value="yes"><spring:message  code="label.yes"/></option>
                                    <option value="no"><spring:message  code="label.no"/></option>
                                </select>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="accrediting-entity"><spring:message  code="label.accreditingEntity"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="accreditingEntity" id="accreditingEntity" class="input-large" />
                                <div id="accreditingEntity_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="accrediting"><spring:message  code="label.uploadFile"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="file" id="accrediting" name="accrediting"  class="input-file" />
                                &nbsp; <button id="btn_upload_accrediting" name="btn_upload_accrediting" class="btn"><spring:message  code="label.upload"/></button>
                                <div id="accrediting_error" class=""></div>
                                <div id="accrediting_display" class=""></div>
                                <input type="hidden" id="hdnAccrediting" name="hdnAccrediting" value=""/>
                         
                            </div>
                        </div><!-- end of control-group-->
                        </div>
                        <div class="header">
                            <h4><spring:message  code="label.organizationData"/></h4>
                        </div>
                        <div class="gutter10-t">
                        <div class="control-group">
                            <label class="control-label"><spring:message  code="label.templateText"/></label>
                            <div class="pull-left marginL20">
                                <a href="<c:url value="/resources/sampleTemplates/organization_data.csv"/>" class="btn"><spring:message  code="label.download"/></a>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="org-data"><spring:message  code="label.uploadFile"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="file" id="orgData" name="orgData"  class="input-file" />
                                &nbsp; <button id="btn_upload_orgData" name="btn_upload_orgData" class="btn"><spring:message  code="label.upload"/></button>
                                <div id="orgData_error" class=""></div>
                                <div id="orgData_display"></div>							
                                <input type="hidden" id="hdnOrgData" name="hdnOrgData" value=""/>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="header">
                            <h4><spring:message  code="label.issuerUploadMarketingReq"/></h4>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="marketing-org"><spring:message  code="label.uploadFile"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="file" id="marketing" name="marketing"  class="input-file" />
                                &nbsp; <button id="btn_upload_marketing" name="btn_upload_marketing" class="btn"><spring:message  code="label.upload"/></button>
                                <div id="marketing_display"></div>
                                <div id="marketing_error" class=""></div>
                                <input type="hidden" id="hdnMarketing" name="hdnMarketing" value=""/>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="header">
                            <h4><spring:message  code="label.financialDisclosures"/></h4>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="financial-disclosures"><spring:message  code="label.uploadFile"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="file" id="finDisclosures" name="finDisclosures"  class="input-file" />
                                &nbsp; <button id="btn_upload_finDisclosures" name="btn_upload_finDisclosures" class="btn"><spring:message  code="label.upload"/></button>
                                <div id="finDisclosures_error" class=""></div>
                                <div id="finDisclosures_display"></div>
                                <input type="hidden" id="hdnFinDisclosures" name="hdnFinDisclosures" value=""/>
                         
                            </div>
                        </div><!-- end of control-group-->
                        </div>
                                            
                        <div class="header">
                            <h4><spring:message  code="label.issuerUploadAdditionalInfo"/></h4>
                        </div>
                        <div class="gutter10-t">
                        <div class="control-group">
                            <label class="control-label" for="add-support-doc"><spring:message  code="label.uploadFile"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="file" id="addSupportDoc" name="addSupportDoc"  class="input-file" />
                                &nbsp; <button id="btn_upload_addSupportDoc" name="btn_upload_addSupportDoc" class="btn"><spring:message  code="label.upload"/></button>
                                <div id="addSupportDoc_error" class=""></div>
                                <div id="addSupportDoc_display"></div>
                                <input type="hidden" id="hdnAddSupportDoc" name="hdnAddSupportDoc" value=""/>
                            </div>
                        </div><!-- end of control-group-->
                       </div> 
                        <input type="hidden" id="fileToUpload" name="fileToUpload" value=""/>
                        <div>
                            <a href='<c:url value="/planmgmt/issuer/representatives/add"/>' class="btn pull-left"><spring:message  code="label.back"/></a>
                            <button type="submit" class="btn btn-primary pull-right"><spring:message  code="label.next"/></button>
                        </div>
                    </form>
                </div>
            </div><!--  end of span9 -->
        	</div>
		</div><!-- end row-fluid -->
<script type="text/javascript">
$(document).ready(function() {
	var button_id_names='#btn_upload_authority,'+
						'#btn_upload_accrediting,'+
						'#btn_upload_orgData,'+
						'#btn_upload_marketing,'+
						'#btn_upload_finDisclosures,'+
						'#btn_upload_addSupportDoc';
						
		$(button_id_names).click(function(){
		var idName = this.id;
		var upload = idName.split("_");
		$('#fileToUpload').val(upload[2]);
		$('#frmIssuerApp').ajaxSubmit({
			url: "<c:url value='/planmgmt/issuer/application/documents/upload'/>",
			success: function(responseText){
	        	 
				var val = responseText.split("|");    	 
	        	 if(val[0]=='authority'){
		        	 $("#authority_display").text(val[1]);
					 $("#hdnAuthority").val(val[2]);
	        	 }
	        	 else if(val[0]=='accrediting'){
		        	 $("#accrediting_display").text(val[1]);
					 $("#hdnAccrediting").val(val[2]);
	        	 }
	        	 else if(val[0]=='orgData'){
		        	 $("#orgData_display").text(val[1]);
					 $("#hdnOrgData").val(val[2]);
	        	 }
	        	 if(val[0]=='marketing'){
		        	 $("#marketing_display").text(val[1]);
					 $("#hdnMarketing").val(val[2]);
	        	 }
	        	 else if(val[0]=='finDisclosures'){
		        	 $("#finDisclosures_display").text(val[1]);
					 $("#hdnFinDisclosures").val(val[2]);
	        	 }
	        	 else if(val[0]=='addSupportDoc'){
		        	 $("#addSupportDoc_display").text(val[1]);
					 $("#hdnAddSupportDoc").val(val[2]);
	        	 }
	        	 
	   		 }
		});
	  return false; 
	});	// button click close
	}); // ready method close
	
	var validator = $("#frmIssuerApp").validate({ 
		rules : {
			authority : {required : true},
			accreditingEntity : {required : true},
			accrediting : { required : true},
			orgData : { required : true},
			marketing : { required : true},
			finDisclosures : { required : true},		
			addSupportDoc : {required : true}
		},
		messages : {
			authority : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadAuthority' javaScriptEscape='true'/></span>"},
			accreditingEntity: { required : "<span><em class='excl'>!</em><spring:message  code='err.validateAccreditingEntity' javaScriptEscape='true'/></span>"},
			accrediting : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadAccreditation' javaScriptEscape='true'/></span>"},
			orgData : { required : "<span><em class='excl'>!</em><spring:message  code='err.validateUploadOrgData' javaScriptEscape='true'/></span>"},
			marketing: { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadMarketingReq' javaScriptEscape='true'/></span>"},
			finDisclosures : { required : "<span><em class='excl'>!</em><spring:message  code='err.validateUploadFinDisclosures' javaScriptEscape='true'/></span>"},		
			addSupportDoc : {required : "<span><em class='excl'>!</em><spring:message  code='err.validateAddSupportDoc' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});

</script>
