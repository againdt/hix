package com.getinsured.consumer.service;

import java.util.List;
import java.util.Map;

import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.hix.consumer.history.ConsumerEventHistory;
import com.getinsured.hix.consumer.history.ConsumerHistoryView;
import com.getinsured.hix.dto.crm.ConsumerHistoryRequest;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.consumer.CmrEventsLog;
import com.getinsured.hix.model.consumer.ConsumerCallLog;
import com.getinsured.hix.platform.eventlog.AppEventDto;
import com.getinsured.hix.platform.util.exception.GIException;


public interface ConsumerHistoryService {
	
	public List<Object[]> getConsumerCallLogHistory(String houdeholdId);
	
	public List<Comment> getConsumerCommentsHistory(String houdeholdId );
	public ConsumerHistoryView getConsumerHistory(String houdeholdId) throws ConsumerServiceException;
	public ConsumerCallLog populateCallLogObject(ConsumerHistoryRequest historyRequest) throws Exception;

	public List<Comment> saveCommentLogObject(
			ConsumerHistoryRequest historyRequest) throws Exception;
	
	public List<CmrEventsLog> getConsumerEventsHistory(String houdeholdId);

	public String saveConsumerEverntLog(ConsumerEventHistory consumerEventHistory);
	
	List<AppEventDto> searchAppEvents(Map<String, Object> searchCriteria);

}
