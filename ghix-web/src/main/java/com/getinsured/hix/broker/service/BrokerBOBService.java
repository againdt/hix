package com.getinsured.hix.broker.service;

import java.text.ParseException;
import java.util.List;

import com.getinsured.hix.dto.broker.BrokerBOBDetailsDTO;
import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.broker.HouseholdEligibilityInformationDTO;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface BrokerBOBService {

	public BrokerBobResponseDTO getBrokerBookOfBusiness(BrokerBOBDetailsDTO requestDTO, BrokerBOBSearchParamsDTO bobSearchParamsDTO, boolean skipPagination) throws Exception;

	public HouseholdEligibilityInformationDTO getHouseholdEligibilityInformation(BrokerBOBDetailsDTO requestDTO) throws Exception;

	public HouseholdEligibilityInformationDTO getHouseholdMemberInformation(BrokerBOBDetailsDTO requestDTO) throws Exception;

	public BrokerBobResponseDTO loadIssuerDetails(String userName);
	
	public List<BrokerBOBDetailsDTO> processHouseholdStatus(List<BrokerBOBDetailsDTO> listOfBrokerBOBDetailsDTO) throws ParseException;
	
	public List<BrokerBOBDetailsDTO> loadPlanDetails(List<BrokerBOBDetailsDTO> listOfBrokerBOBDetailsDTO, String userName);

	public SsapApplication getSsapApplication(Long applicationId);
}
