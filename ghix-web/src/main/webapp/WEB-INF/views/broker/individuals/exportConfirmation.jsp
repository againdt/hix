<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<style>
#exportExcelPopupModal {
	display:none;
}
.row-fluid select.span12{width:99%;}
</style>


<script id="exportExcelConfirmationForm" type="text/ng-template">

		<div id="exportExcelPopupModal" class="modal hide fade" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
			<form id="exportExcel" name="exportExcel" action="exportcsv" method="GET" novalidate>
			<div class="modal-header">
				<h3><spring:message code="label.bobExportDisclaimer"/></h3>
			</div>
			<div class="modal-body exportExcelConfirmation">
				<p><spring:message code="label.bobExportCSVConfirm_1"/> <%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)%> <spring:message code="label.bobExportCSVConfirm_2"/></p>
			</div><!-- exportExcelConfirmationContainer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label.brk.no"/></button>
		  		<input type="submit" class="btn btn-primary" value='<spring:message code="label.brk.yes"/>'/>
			</div>
		</form>
	</div>
	</script>