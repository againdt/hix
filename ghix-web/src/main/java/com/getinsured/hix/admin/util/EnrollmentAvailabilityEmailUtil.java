/**
 * 
 */
package com.getinsured.hix.admin.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.email.IssuerPlanEnrollmentAvailabilityChangeEmail;
import com.getinsured.hix.platform.account.repository.IInboxMsgClobRepository;
import com.getinsured.hix.platform.account.repository.IInboxMsgRepository;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.timeshift.TimeShifterUtil;

/**
 * @author gorai_k
 *
 */
@Component
public class EnrollmentAvailabilityEmailUtil {

	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(EnrollmentAvailabilityEmailUtil.class);
	
	@Autowired private IssuerService issuerService;
	@Autowired private IIssuerRepresentativeRepository representativeRepository;
	@Autowired private UserService userService;
	@Autowired private IssuerPlanEnrollmentAvailabilityChangeEmail issuerPlanEnrollmentAvailabilityChangeEmail;
	@Autowired private IInboxMsgRepository iInboxMsgRepository;
	@Autowired private IInboxMsgClobRepository iInboxMsgClobRepository;
	@Autowired private NoticeService noticeService;
	
	private static final String FILE_NAME = "IssuerPlanEnrollmentAvailabilityChange";
	
	
	public void sendNotificationEmailToIssuerRepresentativeForEnrollment(Plan plan) throws NotificationTypeNotFound, InvalidUserException, NoticeServiceException {
		Issuer issuerObj = issuerService.getIssuerById(plan.getIssuer().getId());
		List<IssuerRepresentative> listIssuerRep = representativeRepository.findByIssuerId(issuerObj.getId());
		Map<String,Object> data = new HashMap<String,Object>();
		for (int i = 0; i < listIssuerRep.size(); i++) {
			if (listIssuerRep.get(i).getUserRecord() != null && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(listIssuerRep.get(i).getUserRecord().getStatus())) {
				Integer userId = listIssuerRep.get(i).getUserRecord().getId();
				AccountUser accountUser = new AccountUser();
				if (null != userId && userId != 0) {
					accountUser = userService.findById(userId);
				}
				if (accountUser != null && !StringUtils.isBlank(accountUser.getEmail())) {
					issuerPlanEnrollmentAvailabilityChangeEmail.setPlanObj(plan);
					issuerPlanEnrollmentAvailabilityChangeEmail.setIssuerObj(issuerObj);
					issuerPlanEnrollmentAvailabilityChangeEmail.setUser(accountUser);
					issuerPlanEnrollmentAvailabilityChangeEmail.setIssuerRepObj(listIssuerRep.get(i));
					
					String fileName = FILE_NAME + TimeShifterUtil.currentTimeMillis()+PlanMgmtConstants.PDF_EXT;
					data = issuerPlanEnrollmentAvailabilityChangeEmail.getSingleData();
					noticeService.createNotice(PlanMgmtConstants.PLAN_ENROLLMENT_UPDATE_NOTIFICATION,  GhixLanguage.US_EN, data, PlanMgmtConstants.PLAN_ECM_PDF_FILE_PATH, fileName, accountUser, null, GhixNoticeCommunicationMethod.Email);
					LOGGER.info("Message stored in inbox successfully");

				} else {
					LOGGER.info("Issuer Representative email address is blank or null");
				}
			} else {
				LOGGER.debug("No User Found for Issuer Representative : "+ listIssuerRep.get(i).getId());
			}
		}	
	}
}
