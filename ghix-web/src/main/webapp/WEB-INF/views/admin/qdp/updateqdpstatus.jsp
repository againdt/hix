<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<style>
.accordion-border-none .accordion-group {
	border: 0 none;
}
</style>

<form class="form-horizontal" id="frmplaninfo" name="frmplaninfo" action='<c:url value="/admin/planmgmt/updateqdpplanstatus"/>' method="post">
	<div class="row-fluid">
	 <div class="span12">
	   <div class="accordion accordion-border-none" id="accordion">
		 <div class="accordion-group">
		    <div id="planDocuments" class="accordion-body collapse">
		    	<div class="gutter10">
					<div class="page-header">
						<h3><a class="accordion-toggle skip" data-toggle="collapse" data-parent="#accordion" href="#noPlanDocuments"><i class="icon-minus"></i> ${plan.name}: <small><spring:message code='label.linkEditCertificationStatus'/></small></a></h3>
					</div><!-- end of page-header -->	    
		    	</div>
	               <df:csrfToken/>
	               <input type="hidden" id="planId" name="planId" value="<encryptor:enc value="${plan.id}"></encryptor:enc>"/>	       
	                <div class="row-fluid">
	                   <table class="table table-border-none table-condensed">
	                        <tbody>
	                            <tr>
	                                <td class="span4 txt-right"><spring:message code='label.issuer'/></td>
	                                <td><strong>${plan.issuer.name}</strong></td>
	                            </tr>
	                            
                            	<tr>
	                                <td class="txt-right"><spring:message code='label.planMarket'/></td>
									<td><strong>${plan.market}</strong></td>
	                            </tr>
								<tr>
									<td class="txt-right"><spring:message code='label.linkProviderNetwork'/></td>
									<td><strong>${network.name}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code='label.providerNetworkType'/></td>
									<c:if test="${network.type == fn:toUpperCase('hmo')}">
										<td><strong><spring:message code='label.hmo' javaScriptEscape='true'/></strong></td>
									</c:if>	
									<c:if test="${network.type == fn:toUpperCase('ppo')}">
										<td><strong><spring:message code='label.ppo' javaScriptEscape='true'/></strong></td>
									</c:if>	
								</tr>
								<tr>
									<td class="span4 txt-right"><spring:message code='label.planStartDate'/></td>
									<td><strong>${plan.startDate}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code='label.planEndDate'/></td>
									<td><strong>${plan.endDate}</strong></td>
								</tr>
								<tr>
									<td class="span4 txt-right"><spring:message code='label.actuarialVC'/></td>
									<td><strong>${actCerti}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code='label.planBenefitData'/></td>
									<td><strong>${benefitFile}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code='label.planRateData'/></td>
									<td><strong>${rateFile}</strong></td>
								</tr>
	                        </tbody>
	                    </table>
	                  </div>
	               </div>
			    </div>
				<hr/>
				<div class="accordion-group">
					<div id="noPlanDocuments" class="accordion-body collapse in">	
						<div class="page-header">
							<h3><a class="accordion-toggle skip" data-toggle="collapse" data-parent="#accordion" href="#planDocuments"><i class="icon-plus"></i> ${plan.name}: <small><spring:message code='label.linkEditCertificationStatus'/></small></a></h3>
						</div>
					</div>
				</div>
			 </div> 
                
                 <div class="row-fluid">
                  	<div class="box-gutter10">
                    <h4 class="alert alert-info"><spring:message code='label.currentStatus'/> : ${plan.status}</h4>
	                    
	               <div class="row-fluid">
	                    <div class="control-group">
	                        <label for="select01" class="control-label"><spring:message code='label.updateStatus'/></label>
	                        <div class="controls">
	                            <select id="status" name="status">
	                                 <option value=""><spring:message code='label.selectOption'/></option>
	                                <c:forEach var="qdpcertiStatus" items="${qdpcertiStatusList}">
							    		<option value="${qdpcertiStatus.key}">${qdpcertiStatus.value}</option>
									</c:forEach>
	                            </select>
	                             <div id="status_error" class="help-inline"></div>
	                        </div> <!-- end of controls-->
	                    </div>
	                    <!-- end of control-group --> 
		
			            <div class="control-group">
				            <label for="textarea" class="control-label"><spring:message code='label.addComment'/></label>
				            <div class="controls">
				              <textarea rows="3" id="textarea" class="input-xlarge"></textarea>
				            </div>
			         	</div>
						<!-- end of control-group --> 
						<div class="control-group">
				            <label for="fileInput" class="control-label"><spring:message code='label.uploadFile'/></label>
				            <div class="controls">
				              <input type="file" id="fileInput" class="input-file">&nbsp;
				              <button class="btn"><spring:message code='label.upload'/></button>
				            </div>
			          	</div>
						<!-- end of control-group --> 
					</div>
					
	                <div class="form-actions">
	                    <div class="control-group span5">
	                           <button type="submit" name="submit" id="submit" class="btn btn-primary"><spring:message  code='label.update'/></button>
	                    </div>  
	                </div> 
	                 <!-- end of form actions -->
					</div>
				</div> <!-- row fluid forms --> 
			<!-- end of profile -->
		</div>
		<!-- end of span9 -->
	</div>
</form>      
	<!-- end of row-fluid -->
<script type="text/javascript">
var validator = $("#frmplaninfo").validate({ 
	rules : {
		status : {required : true}
	},
	messages : {
		status : { required : "<span><em class='excl'>!</em><spring:message  code='err.selectCertStatus' javaScriptEscape='true'/></span>"}
		},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	}
});
</script>
