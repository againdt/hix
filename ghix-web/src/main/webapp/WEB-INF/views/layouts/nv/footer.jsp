<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.platform.util.*"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<jsp:useBean id="date" class="java.util.Date" />
<fmt:formatDate var="now" value="${date}" pattern="yyyy" />  
		 
<footer class="container" role="footer">
 	<div class="row-fluid">

		<div class="disclaimers">
			<a href="#" class="seal">
				<img src="https://d1q4hslcl8rmbx.cloudfront.net/assets/themes/ssh-en/assets/img/state-seal.png" width="86" alt="State of Nevada">
			</a>
			<p><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%> is brought to you by the <a href="http://exchange.nv.gov/" target="_blank">State of Nevada Silver State Health Insurance Exchange</a> </p>
			<p>The Official Site of the Silver State Health Insurance Exchange | Copyright &copy;${now} State of Nevada - All
			Rights Reserved |
			<a target="_blank" href="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL)%>" title="Link open in new window or tab">Privacy Policy</a>
			<%--https://www.nevadahealthlink.com/privacy-policy/--%>
		</div>


			<c:set var="assisterRoleName" ><encryptor:enc value="assisterenrollmententity" isurl="true"/> </c:set>
			<%if(GhixConstants.GHIX_ENVIRONMENT.equals("PROD")){%>
				<ul class="nav nav-list pull-right" id="build">
					<li style="white-space: nowrap;"><a href="#">Build Number: <%= GhixPlatformConstants.BUILD_NUMBER %> </a></li>
             		<li style="white-space: nowrap;"><a href="#">Build Date: <%= GhixPlatformConstants.BUILD_DATE %></a></li>
 				</ul>
 			<%}%>
 				
			<%if(GhixConstants.GHIX_ENVIRONMENT.equals("DEV")){%>
				    <c:set var="brokerRoleName">
                        <encryptor:enc value="broker" isurl="true" />
                    </c:set>
                    <div class="txt-center">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <a href="<c:url value='/account/signup/${brokerRoleName}' />">Health Insurance Agent/Broker</a>
                        </div>
                        <c:set var="assisterRoleName">
                            <encryptor:enc value="assisterenrollmententity" isurl="true" />
                        </c:set>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                             <a href="<c:url value="/account/signup/${assisterRoleName}"/>">Enrollment Entities</a>
                        </div>
                        <ul class="nav nav-list pull-right hide" id="buildshow">
                            <li style="white-space: nowrap;"><a href="#">Branch Name: <%= GhixPlatformConstants.BUILD_BRANCH_NAME %></a></li>
                            <li style="white-space: nowrap;"><a href="#">Build Number: <%= GhixPlatformConstants.BUILD_NUMBER %></a></li>
                            <li style="white-space: nowrap;"><a href="#">Build Date: <%= GhixPlatformConstants.BUILD_DATE %></a></li>
                        </ul>
                    </div>
			<%}%>
	</div>
</footer><!-- footer -->