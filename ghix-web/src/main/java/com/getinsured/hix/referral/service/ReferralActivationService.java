package com.getinsured.hix.referral.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;
import com.getinsured.eligibility.referral.ui.dto.ReferralVerification;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.iex.referral.ReferralLinkingOverride;

/**
 * @author chopra_s
 * 
 */
public interface ReferralActivationService {
	int bindUserCmrToReferralActivation(int ssap, int cmr, int user);

	ReferralActivation fetchReferralActivationObject(final int id);

	ReferralVerification populateReferralVerification(final ReferralActivation referralActivation);

	int validateSecurityQuestions(final ReferralVerification referralVerification);

	int uploadDocumentForVerification(CommonsMultipartFile documentFile, String parameter, int id) throws Exception;

	List<ConsumerDocument> fetchConsumerDocuments(long referralActivation);

	List<Integer> fetchCoverageYear();

	int persistCoverageYear(int referralId, int coverageYear);
	
	int processRidpCheck(int id, int userId);
	
	Map<String, String> processRidpLinkCheck(int id, int userId, int householdId);

	ReferralActivation fetchReferralActivationForSsap(final int ssapApplication);

	void sendAdditionalVerificationRequiredNotification(long referralActivationId);
	
	LceActivityDTO fetchApplicantsforLceEvents(String caseNumber);

	Map<String, String> updateSepEventsforApplicantandApplications(LceActivityDTO lceActivityDTO);

	Map<String, List<SepEvents>> fetchSepAndQEPEvents();
	
	long getActiveApplicationCountForCMR(final int cmrHouseHoldId, Integer ssapApplicationId); 
	
	Map<Integer, String> getSsapApplicant(String accessCode, String isCsr);
	
	String getSsapApplicantGuid(String ssapApplicationId);
	
	int checkForExistingHouseholdFromSsnDoB(String ssapApplicationId);
	
	ReferralLinkingOverride compareHouseholdAndPrimary(String ssapApplicationId, String householdId);
	
	Integer updateReferralActivationCount(String referralId);
	
	Integer updateReferralActivationSuccessCount(String referralId);

	Map<String, String> checkforEnrolledApplication(int cmrId, int ssapApplicationId);

	void triggerDesignation(int householdId);

	Map<Integer, String> getSsapApplicantfromHome(String accessCode,String csrDefaultRole);
}