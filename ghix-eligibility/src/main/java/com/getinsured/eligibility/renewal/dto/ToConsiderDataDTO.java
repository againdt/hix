package com.getinsured.eligibility.renewal.dto;

import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentWithMemberDataDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;

public class ToConsiderDataDTO {

	String applicationId;
	String memberId;
	boolean isToConsider;
	String groupReasonCode;
	Map<String,SsapApplicant> dentalATMembers;
	Map<String,Object> dentalEnrolledMembersMap;
	EnrollmentWithMemberDataDTO enrolledMemberDto;
	long previousYearEnrolledApplicationId;
	SsapApplicant ssapApplicant;
	boolean isAllMembersNotToConsider;
	String planId;
	boolean isNewAddedMember;
	
	
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public boolean isAllMembersNotToConsider() {
		return isAllMembersNotToConsider;
	}
	public void setAllMembersNotToConsider(boolean isAllMembersNotToConsider) {
		this.isAllMembersNotToConsider = isAllMembersNotToConsider;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public boolean isToConsider() {
		return isToConsider;
	}
	public void setToConsider(boolean isToConsider) {
		this.isToConsider = isToConsider;
	}
	public String getGroupReasonCode() {
		return groupReasonCode;
	}
	public void setGroupReasonCode(String groupReasonCode) {
		this.groupReasonCode = groupReasonCode;
	}
	public Map<String, SsapApplicant> getDentalATMembers() {
		return dentalATMembers;
	}
	public void setDentalATMembers(Map<String, SsapApplicant> dentalATMembers) {
		this.dentalATMembers = dentalATMembers;
	}
	public Map<String, Object> getDentalEnrolledMembersMap() {
		return dentalEnrolledMembersMap;
	}
	public void setDentalEnrolledMembersMap(
			Map<String, Object> dentalEnrolledMembersMap) {
		this.dentalEnrolledMembersMap = dentalEnrolledMembersMap;
	}
	public EnrollmentWithMemberDataDTO getEnrolledMemberDto() {
		return enrolledMemberDto;
	}
	public void setEnrolledMemberDto(EnrollmentWithMemberDataDTO enrolledMemberDto) {
		this.enrolledMemberDto = enrolledMemberDto;
	}
	public long getPreviousYearEnrolledApplicationId() {
		return previousYearEnrolledApplicationId;
	}
	public void setPreviousYearEnrolledApplicationId(
			long previousYearEnrolledApplicationId) {
		this.previousYearEnrolledApplicationId = previousYearEnrolledApplicationId;
	}
	public SsapApplicant getSsapApplicant() {
		return ssapApplicant;
	}
	public void setSsapApplicant(SsapApplicant ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}
	public boolean isNewAddedMember() {
		return isNewAddedMember;
	}
	public void setNewAddedMember(boolean isNewAddedMember) {
		this.isNewAddedMember = isNewAddedMember;
	}
	
	
	@Override
	public String toString() {
		return "ToConsiderDataDTO [applicationId=" + applicationId
				+ ", applicantGuid=" + memberId + ", isToConsider="
				+ isToConsider + ", groupReasonCode=" + groupReasonCode
				+ ", dentalATMembers=" + dentalATMembers
				+ ", dentalEnrolledMembersMap=" + dentalEnrolledMembersMap
				+ ", enrolledMemberDto=" + enrolledMemberDto
				+ ", previousYearEnrolledApplicationId="
				+ previousYearEnrolledApplicationId + ", ssapApplicant="
				+ ssapApplicant + ", isAllMembersNotToConsider="
				+ isAllMembersNotToConsider + ", planId=" + planId
				+ ", isNewAddedMember=" + isNewAddedMember + "]";
	}
}
