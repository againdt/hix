(function (views) {
	views.CompareView = Backbone.View.extend({
		
		events : {
			'click .comparePlans' : 'comparePlans',
			'click .backToAll' : 'backToAll'
		},
		el : '#compare',
		initialize : function (options) {
			_.bindAll (this, 'render','comparePlans');
			var self = this;
			self.tmpl = _.template($('#tmpCompare').html());
			self.collection.bind('reset', this.render);
		},
		render : function () {
            var self;
            self = this;

			var html = this.tmpl(self.collection.info());
			$(this.el).html(html);
		},
		
		comparePlans: function () {
			
			/*compare view should have atleast 2 plans to compare*/
			if(this.collection.compareModels.length > 0){
				this.collection.page=1;
				this.collection.resetFieldFilter();
				this.collection.loadIssuers();
				this.collection.showCompare=true;
				this.collection.pager();
				$('#Nplans').hide();
				$('#compareHead').show();
				this.collection.updateCompareSummary();	
				this.collection.preserveOtherOptions();
			}
			else{ alert('Please add atleast one plan to favorites');}
		},
		backToAll : function(){
			/*set compare view to false*/
			this.collection.showCompare=false;
			$('#Nplans').show();
			$('#compareHead').hide();
			this.collection.resetFieldFilter();
			this.collection.loadIssuers();
			this.collection.preserveOtherOptions();
		}
		
	});
})(App.views);