<style>
	#container .highcharts-container{height: auto !important; overflow-x: visible!important; left:5px!important; } 
	.pie-Descrip{margin:-75px 0 0 0}
</style>
<script type="text/javascript" src="/hix/resources/js/highcharts.js"></script>
<script type="text/javascript" src="/hix/resources/js/shop/piechart.js"></script>



		<div class="clearfix">
			<h4 class="margin0">What&#039;s Next?</h4>
			<div class="gutter10">
				<div id="complete-oep" class="alert">
					<p>Open enrollment ends $enrollEndDate$. If you meet the $participation_required$% participation requirement, you will receive an invoice on $invoicedate$.</p>
					 $showWaivedText$                
				</div>	
			</div><!-- End -->
		</div> <!-- End /.blurb --> 
		
		<div class="txt-center">
			<h5 class="header-secondary"><b>Participation Summary</b></h5>
			<p>Participation Required: $participation_required$%</p>
			<p>Employer Participation: $participationrate$%</p>
		</div> <!-- End /.blurb -->
		<div  id="step-progress">        
			<div id="progress-bar">
				
				<div id="container"></div>
			</div> <!--/End Progress-bar-->
		</div><!--/End step-progress-->
		
		  


<script type="text/javascript">
 showPieChart($enrolled$, $waived_qualified$, $waived_unqualified$, $noresponse$);
</script>