package com.getinsured.eligibility.ssap.integration.household.service;

import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

@Component(value="incomeServiceActivator")
public class IncomeServiceActivator {
	
	@SuppressWarnings("rawtypes")
	@ServiceActivator
	public String receive(Message message) {
		return message.getHeaders().get("HOUSEHOLD_REQUEST").toString();
	}

}
