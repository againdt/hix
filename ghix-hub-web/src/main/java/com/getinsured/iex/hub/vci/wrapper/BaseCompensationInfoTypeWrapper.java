package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.BaseCompensationInfoType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.BaseCompensationInfoType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class BaseCompensationInfoTypeWrapper implements JSONAware {
	
	private IncomeTypeWrapper payRate;
	private String payFrequencyCode;
	private String payFrequencyMessage;
	private String payPeriodFrequencyCode;
	private String payPeriodFrequencyMessage;
	private String annualizedIncome;
	
    private static final String PAY_RATE_KEY = "PayRate";
    private static final String FREQ_CODE_KEY = "PayFrequencyCode";
    private static final String FREQ_MSG_RATE_KEY = "PayFrequencyMessage";
    private static final String PAY_PERIOD_FREQ_CODE_KEY = "PayPeriodFrequencyCode";
    private static final String PAY_PERIOD_FREQ_MSG_KEY = "PayPeriodFrequencyMessage";
    private static final String INCOME_KEY = "AnnualizedIncome";
    
    
	public BaseCompensationInfoTypeWrapper(BaseCompensationInfoType baseCompensationInfoType){
		
		if(baseCompensationInfoType == null){
			return;
		}
		
		this.payRate = new IncomeTypeWrapper(baseCompensationInfoType.getPayRate());
		this.payFrequencyCode = baseCompensationInfoType.getPayFrequencyCode() != null ? baseCompensationInfoType.getPayFrequencyCode().getValue() : null;
		this.payFrequencyMessage = baseCompensationInfoType.getPayFrequencyMessage() != null ? baseCompensationInfoType.getPayFrequencyMessage().getValue() : null;
		this.payPeriodFrequencyCode = baseCompensationInfoType.getPayPeriodFrequencyCode() != null ? baseCompensationInfoType.getPayPeriodFrequencyCode().getValue() : null;
		this.payPeriodFrequencyMessage = baseCompensationInfoType.getPayPeriodFrequencyMessage() != null ? baseCompensationInfoType.getPayPeriodFrequencyMessage().getValue().value() : null;
		this.annualizedIncome = baseCompensationInfoType.getAnnualizedIncome() != null ? baseCompensationInfoType.getAnnualizedIncome().getValue().toString() : null;
		
	}

	public IncomeTypeWrapper getPayRate() {
		return payRate;
	}

	public String getPayFrequencyCode() {
		return payFrequencyCode;
	}

	public String getPayFrequencyMessage() {
		return payFrequencyMessage;
	}

	public String getPayPeriodFrequencyCode() {
		return payPeriodFrequencyCode;
	}

	public String getPayPeriodFrequencyMessage() {
		return payPeriodFrequencyMessage;
	}

	public String getAnnualizedIncome() {
		return annualizedIncome;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(PAY_RATE_KEY, this.payRate);
		obj.put(FREQ_CODE_KEY, this.payFrequencyCode);
		obj.put(FREQ_MSG_RATE_KEY, this.payFrequencyMessage);
		obj.put(PAY_PERIOD_FREQ_CODE_KEY, this.payPeriodFrequencyCode);
		obj.put(PAY_PERIOD_FREQ_MSG_KEY, this.payPeriodFrequencyMessage);
		obj.put(INCOME_KEY, this.annualizedIncome);
		return obj.toJSONString();
	}
	
}
