//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.08.09 at 07:57:21 PM PDT 
//


package com.getinsured.fars2.niem.niem.niem_core._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.fars2.cms.dsh.ridp.extension._1.CityNameRestrictionType;
import com.getinsured.fars2.cms.dsh.ridp.extension._1.StreetNameRestrictionType;


/**
 * A data type for a word or phrase by which a person or thing is known, referred, or addressed.
 * 
 * <p>Java class for ProperNameTextType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProperNameTextType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://niem.gov/niem/niem-core/2.0>TextType">
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProperNameTextType")
@XmlSeeAlso({
    StreetNameRestrictionType.class,
    CityNameRestrictionType.class,
    PersonNameTextType.class
})
public class ProperNameTextType
    extends TextType
{


}
