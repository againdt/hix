package com.getinsured.hix.crm.web;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.eligibility.enums.ApplicationSource;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.cap.application.ApplicationService;
import com.getinsured.hix.cap.consumer.dto.CheckPreLinkDataDto;
import com.getinsured.hix.cap.util.CapPortalConfigurationUtil;
import com.getinsured.hix.cap.util.CapRequestValidatorUtil;
import com.getinsured.hix.consumer.repository.ISSAPApplicationRepository;
import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.crm.util.ConsumerEventsUtil;
import com.getinsured.hix.dto.cap.ProgramEligibilityDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeDataDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeDataUpdateDTO;
import com.getinsured.hix.dto.enrollment.Enrollment834ResendDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentEventHistoryDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateDTO;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.indportal.CSROverrideUtility;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.indportal.IndividualPortalUtility;
import com.getinsured.hix.indportal.dto.EnrollmentStatusUpdate;
import com.getinsured.hix.indportal.dto.IndPortalEligibilityDateDetails;
import com.getinsured.hix.indportal.dto.IndPortalValidationDTO;
import com.getinsured.hix.indportal.dto.MyApplications;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AppEvent;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.GIErrorCode;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmTicketsResponse;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.consumer.ManageApplicantsDto;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.CustomPermissionEvaluator;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.TicketMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.RestfulResponseException;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.ridp.util.RIDPConstants;
import com.getinsured.hix.screener.util.ScreenerUtil;
import com.getinsured.hix.util.ConsumerResetPasswordUtil;
import com.getinsured.hix.util.TicketMgmtUtils;
import com.getinsured.iex.client.SsapApplicationAccessor;
import com.getinsured.iex.dto.PortalResponse;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.indportal.dto.ReinstateEnrollmentDTO;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@Api(value = "AdminConsumerController operations", basePath = "", description = "All operations for Consumer")
public class AdminConsumerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminConsumerController.class);
	private static final String ERROR_MESSAGE = "errorMsg";
	private static final String APPLICATION_ERROR = "Application Error ::Please contact system admin";
	private static final int LISTING_NUMBER = 10;
	private static final String FILTER_SORTBY = "sortBy";
	private static final String FILTER_SORTORDER = "sortOrder";
	private static final String SORT_ASC = "ASC";
	private static final String SORT_DESC = "DESC";
	private static final String FILTER_CHANGEORDER = "changeOrder";
	private static final String FILTER_PAGE_SIZE = "pageSize";
	private static final String PAGE_ON_LOAD_FLAG = "onPageLoad";
	private static final String SUBMIT = "submit";
	public static final String CONTENT_TYPE = "application/json";
	private static final String PAGE_TITLE = "page_title";
	private static final String CAUSE = "Cause : ";
	private static final String ELIG_LEAD_ID = "eligLeadId";
	private static final String FIRST_NAME = "firstName";
	private static final String HOUSEHOLD = "household";
	private static final int THREE = 3;
	private static final int SIX = 6;
	private static final int TEN = 10;
	private static final int TWELVE = 12;
	private static final int FOURTEEN = 14;
	private static final String APPEAL_HISTORY = "appealhistory";
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final Object SUCCESS_RESPONSE_CODE = "HS000000";
	private static final Object ERROR_RESPONSE_CODE = "HE999999";
	private static final int MEMBER_LISTING_NUMBER = 25;
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static final String EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER = "Exception occurred while getting logged in user";
	//private static final String INDIVIDUAL_ROLE = "INDIVIDUAL";
	private static final String HOUSEHOLD_ID = "householdId";
	private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	private static final String ADD_NEW_VIEW_CASE_URL = "addNewViewCaseUrl";
	private static final String ADD_SPECIAL_ENROLLMENT_INFO_URL = "specialEnrollmentInfoUrl";

	@Autowired
	private HIXHTTPClient hIXHTTPClient;
	/*FIXME
	@Autowired private ConsumerService consumerService;*/
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	@Qualifier("screenerUtil")
	private ScreenerUtil phixUtil;

	@Autowired
	private ZipCodeService zipCodeService;
	@Autowired
	private AccountActivationService accountActivationService;
	@Autowired
	private UserService userService;
	@Autowired
	private ConsumerPortalUtil consumerPortalUtil;
	@Autowired
	private CommentTargetService commentTargetService;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private GIMonitorService giMonitorService;
	@Autowired
	private BrokerService brokerService;
	//@Autowired private RoleService roleService;
	@Autowired
	private ConsumerEventsUtil consumerEventsUtil;
	@Autowired
	private ISSAPApplicationRepository applicationRepository;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private CapRequestValidatorUtil capRequestValidatorUtil;
	@Autowired
	private ConsumerResetPasswordUtil consumerResetPasswordUtil;
	@Autowired
	@Qualifier("individualPortalUtility")
	private IndividualPortalUtility util;
	@Autowired
	private CapPortalConfigurationUtil capPortalConfigurationUtil;
	@Autowired
	private CustomPermissionEvaluator perEvaluator;
	@Autowired
	private CSROverrideUtility csrOverrideUtility;
	@Autowired
	private IndividualPortalUtility individualPortalUtility;
	@Autowired
	ApplicationService applicationService;
	@Autowired
	private SsapApplicationAccessor ssapApplicationAccessor;
	@Autowired
	private Gson platformGson;
	public static String VIEW_CASE_URL;

	@Value("#{configProp['cap.basicInfo.viewCaseAhbxUrl']}")
	public void setADD_VIEW_CASE_URL(String vIEW_CASE_URL) {
		VIEW_CASE_URL = vIEW_CASE_URL;
	}

	public static String ENROLLMENT_INFO_URL;

	@Value("#{configProp['cap.enrollment.specialenrollmentInfo']}")
	public void setADD_SPECIAL_ENROLLMENT_INFO_URL(String sPECIAL_ENROLLMENT_INFO_URL) {
		ENROLLMENT_INFO_URL = sPECIAL_ENROLLMENT_INFO_URL;
	}

	private static final List<String> SEARCH_CRITERIA_PARAMS_AUDIT = Arrays.asList("firstName", "lastName", "contactNumber", "ssn", "appId", "extAppId");

	/**
	 * @author Sneha
	 * @param model
	 * @param request
	 * @return
	 * below request mapping serves for  creating new Consumer
	 */
	@ApiOperation(value = "Create new Consumer", notes = "This method will check the eligibility for creating new consumer.")
	@RequestMapping(value = "crm/consumer/newconsumer", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'ADD_NEW_CONSUMER')")

	public String newConsumer(Model model, HttpServletRequest request) {
		try {
			/* @PreAuthorize annotation need to be added for this mapping
			 * TBD
			 */
			EligLead eligLead = null;
			Household household = new Household();

			int id = (request.getParameter(ELIG_LEAD_ID) != null) ? Integer.valueOf(request.getParameter(ELIG_LEAD_ID)) : 0;

			if (0 != id) {
				eligLead = phixUtil.fetchEligLeadRecord(id);

				if (null != eligLead) {
					household.setFirstName(StringUtils.substringBefore(eligLead.getName(), " "));
					household.setLastName(StringUtils.substringAfter(eligLead.getName(), " "));
					household.setEmail(eligLead.getEmailAddress());
					household.setZipCode(eligLead.getZipCode());
					household.setEligLead(eligLead);
					if (null != eligLead.getMemberData()) {
						model.addAttribute("dob", getDOB(eligLead.getMemberData()));
						model.addAttribute("isTobaccoUser", getIsTobaccoUser(eligLead.getMemberData()));
					}
					if (null != eligLead.getPhoneNumber() && eligLead.getPhoneNumber().length() == TEN) {
						model.addAttribute("phone1", StringUtils.substring(eligLead.getPhoneNumber(), 0, THREE));
						model.addAttribute("phone2", StringUtils.substring(eligLead.getPhoneNumber(), THREE, SIX));
						model.addAttribute("phone3", StringUtils.substring(eligLead.getPhoneNumber(), SIX, TEN));

					}
					Map<String, String> addressFields = getAddressFields(eligLead.getZipCode());
					model.addAttribute("defaultStateCode", addressFields.get("stateCode"));
				}
				model.addAttribute(HOUSEHOLD, household);
				model.addAttribute(ELIG_LEAD_ID, id);
			}

			//populate state and Affiliate list
			model.addAttribute("statelist", new StateHelper().getAllStates());
			//model.addAttribute("affiliateList", getAffiliateList());
		} catch (Exception e) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in newConsumer()" + e.getMessage() + CAUSE + e.getCause());
		}
		return "crm/consumer/newconsumer";
	}

	/**
	 * @author Sneha
	 * @param model
	 * @param request
	 * @return
	 * below request mapping serves for  save Consumer
	 */
	@ApiOperation(value = "Save new Consumer", notes = "This method will save a new consumer to database.")
	@RequestMapping(value = "crm/consumer/newconsumer/addconsumerSubmit", method = { RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ADD_NEW_CONSUMER')")
	public String newConsumerSubmit(Model model, @ModelAttribute(HOUSEHOLD) Household household, HttpServletRequest request) {
		try {

			/* @PreAuthorize annotation need to be added for this mapping
			 * TBD
			 */
			EligLead eligLead = null;
			//populate state and Affiliate list
			model.addAttribute("statelist", new StateHelper().getAllStates());

			String eligLeadIdStr = request.getParameter(ELIG_LEAD_ID);
			if (null != eligLeadIdStr && StringUtils.trim(eligLeadIdStr).length() > 0) {
				long eligLeadId = Long.parseLong(eligLeadIdStr);
				eligLead = phixUtil.fetchEligLeadRecord(eligLeadId);
				household.setEligLead(eligLead);
			} else {
				eligLead = household.getEligLead();
			}
			eligLead.setName(household.getFirstName() + " " + household.getLastName());
			eligLead.setEmailAddress(household.getEmail());
			String phoneNumber = household.getPhoneNumber().replace("-", "");
			eligLead.setPhoneNumber(phoneNumber);
			//TBD set Date of Birth and Tobacco user
			ObjectMapper mapper = new ObjectMapper();
			Member member = new Member();
			member.setDateOfBirth(request.getParameter("birthDate"));
			member.setIsTobaccoUser(request.getParameter("smoker"));
			eligLead.setMemberData(mapper.writeValueAsString(member));
			if (0 == eligLead.getId()) {
				//save ElegeLead
				EligLead eleigLeadAfterSave = phixUtil.createEligLeadRecord(eligLead);
				household.setEligLead(eleigLeadAfterSave);
			}

			//save household
			String response = hIXHTTPClient.getPOSTData(GhixEndPoints.ConsumerServiceEndPoints.SAVE_HOUSEHOLD_RECORD, mapper.writeValueAsString(household), CONTENT_TYPE);
			Household householdAfterSave = mapper.readValue(response, Household.class);
			model.addAttribute(HOUSEHOLD, householdAfterSave);
			request.setAttribute("afterSave", "afterSave");
			generateActivationLink(householdAfterSave);
			consumerEventsUtil.generateHouseholdCeateEvent(household);

		} catch (RestfulResponseException ex) {
			switch (ex.getHttpStatus()) {
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in newConsumerSubmit().", ex.getMessage() + CAUSE);
				break;

			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
		} catch (Exception e) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in newConsumerSubmit()" + e.getMessage() + CAUSE + e.getCause());
		}

		return "crm/consumer/newconsumer";
	}

	/**
	 * @author Sneha
	 * @param model
	 * @param request
	 * @return
	 * below request mapping serves for  search member
	 */
	@GiAudit(transactionName = "Search Members", eventType = EventTypeEnum.CAP_MANAGE_MEMBERS, eventName = EventNameEnum.PII_READ)
	@ApiOperation(value = "Search Members", notes = "This method is to search member.")
	@RequestMapping(value = "crm/member/managemembers", method = { RequestMethod.POST, RequestMethod.GET })
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PreAuthorize("hasPermission(#model, 'MEMBER_VIEW_LIST')")
	public String searchMember(Model model, HttpServletRequest request) {
		capRequestValidatorUtil.validateSearchMember(request);

		validateLoggedInAgent(request, userService, brokerService);
		int iResultCount = 0;
		int totalConsumer = 0;
		Map<String, Object> householdListAndRecordCount = new HashMap<String, Object>();

		Map<String, Object> searchCriteria = null;
		try {
			searchCriteria = this.createMemSearchCriteria(model, request);
			if (null != searchCriteria) {
				String strPageNum = request.getParameter("pageNumber");
				int iPageNum = StringUtils.isEmpty(strPageNum) ? 0 : (Integer.parseInt(strPageNum) - 1);
				int startRecord = iPageNum * MEMBER_LISTING_NUMBER;
				searchCriteria.put("startRecord", startRecord);
				searchCriteria.put(FILTER_PAGE_SIZE, MEMBER_LISTING_NUMBER);
				model.addAttribute("searchCriteria", searchCriteria);
				model.addAttribute(FILTER_SORTORDER, searchCriteria.get(FILTER_SORTORDER));
			}

			if (model.containsAttribute(PAGE_ON_LOAD_FLAG)) {
				//ResponseEntity<Map> consumerResponseEntity =restTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_CONSUMER, searchCriteria, Map.class);
				ResponseEntity<Map> consumerResponseEntity = ghixRestTemplate.exchange(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_CONSUMER, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Map.class, searchCriteria);
				householdListAndRecordCount = consumerResponseEntity.getBody();

				// Get household list and totalConsumers
				List<Map<String, Object>> householdList = (List<Map<String, Object>>) householdListAndRecordCount.get("householdList");
				Integer iHouseholdCnt = (Integer) householdListAndRecordCount.get("recordCount");
				iResultCount = iHouseholdCnt.intValue();
				if (totalConsumer == 0) {
					model.addAttribute("totalConsumer", iResultCount);
				}
				model.addAttribute("householdList", householdList);
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter("Number of records found=", iResultCount));
		} catch (RestfulResponseException ex) {
			switch (ex.getHttpStatus()) {
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in searchConsumer().", ex.getMessage() + CAUSE);
				break;

			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
		} catch (Exception e) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in searchConsumer()" + e.getMessage() + CAUSE + e.getCause());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, APPLICATION_ERROR));
		}

		model.addAttribute("isRelinkAllowed", this.perEvaluator.hasPermission(ConsumerPortalUtil.CAP_MEMBER_RELINKALLOWED));

		model.addAttribute("resultSize", iResultCount);
		model.addAttribute(FILTER_PAGE_SIZE, MEMBER_LISTING_NUMBER);
		model.addAttribute(SUBMIT, SUBMIT);

		return "crm/member/searchmember";
	}

	@GiAudit(transactionName = "Search Applicants", eventType = EventTypeEnum.CAP_MANAGE_APPLICANT, eventName = EventNameEnum.PII_READ)
	@ApiOperation(value = "Search Applicants", notes = "This method is to search applicants.")
	@RequestMapping(value = "crm/member/searchapplicants", method = { RequestMethod.POST, RequestMethod.GET })
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PreAuthorize("hasPermission(#model, 'MEMBER_SEARCH_APPLICANTS')")
	public String searchApplicants(Model model, HttpServletRequest request) {
		capRequestValidatorUtil.validateSearchApplicants(request);

		int iResultCount = 0;
		int totalConsumer = 0;
		List<String> effectiveYears = null;
		String coverageYear = null;
		Map<String, Object> applicantListAndRecordCount = new HashMap<String, Object>();
		List<ApplicationSource> applicationSourceList = new ArrayList<ApplicationSource>(Arrays.asList(ApplicationSource.values()));
		Map<String, Object> searchCriteria = null;
		try {

			searchCriteria = this.createMemSearchCriteria(model, request);

			if (searchCriteria != null) {
				List<String> searchParams = new ArrayList<String>();
				for (String searchParam : searchCriteria.keySet()) {
					if (SEARCH_CRITERIA_PARAMS_AUDIT.contains(searchParam)) {
						searchParams.add(searchParam);
					}
				}
				model.addAttribute("searchCriteria", searchCriteria);
				model.addAttribute(FILTER_SORTORDER, searchCriteria.get(FILTER_SORTORDER));

				searchParams.add(searchCriteria.get("appSource") != null ? "appSource=" + (String) searchCriteria.get("appSource") : "appSource=ALL");
				GiAuditParameterUtil.add(new GiAuditParameter("Search Criteria Parameters ", searchParams.toString()));
			}

			if (model.containsAttribute(PAGE_ON_LOAD_FLAG)) {
				//ResponseEntity<Map> consumerResponseEntity =restTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_APPLICANTS, searchCriteria, Map.class);
				ResponseEntity<Map> consumerResponseEntity = ghixRestTemplate.exchange(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_APPLICANTS, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Map.class, searchCriteria);
				applicantListAndRecordCount = consumerResponseEntity.getBody();

				// Get applicants from SSAP_APPLICATIONS and SSAP_APPLICANTS
				List<ManageApplicantsDto> applicantList = (List<ManageApplicantsDto>) applicantListAndRecordCount.get("applicantList");
				Integer iResultCt = (Integer) applicantListAndRecordCount.get("recordCount");
				iResultCount = iResultCt.intValue();
				if (totalConsumer == 0) {
					model.addAttribute("totalApplicants", iResultCount);
				}
				model.addAttribute("exchangeType", applicantListAndRecordCount.get("exchangeType"));
				model.addAttribute("applicantList", applicantList);

				model.addAttribute("applicationStatusMap", getApplicationStatusAsMap());
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter("Number of records found=", iResultCount));
			effectiveYears = applicationRepository.findAllEffectiveYears();
			coverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		} catch (RestfulResponseException ex) {
			switch (ex.getHttpStatus()) {
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in searchConsumer().", ex.getMessage() + CAUSE);
				break;

			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
		} catch (Exception e) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, APPLICATION_ERROR));
			LOGGER.error("Error in searchConsumer()" + e.getMessage() + CAUSE + e.getCause());
		}

		model.addAttribute("effectiveYearSource", effectiveYears);
		model.addAttribute("resultSize", iResultCount);
		model.addAttribute(FILTER_PAGE_SIZE, LISTING_NUMBER);
		model.addAttribute(SUBMIT, SUBMIT);
		model.addAttribute("applicationSourceList", applicationSourceList);
		model.addAttribute("effectiveYear", coverageYear);
		model.addAttribute("enrollmentStatusList", getEnrollmentStatusList());

		return "crm/member/searchapplicants";
	}

	@GiAudit(transactionName = "Search Applicants", eventType = EventTypeEnum.CAP_MANAGE_APPLICANT, eventName = EventNameEnum.PII_READ)
	@ApiOperation(value = "Search Applicants", notes = "This method is to search applicants.")
	@RequestMapping(value = "crm/member/searchapplicants/screenpop", method = { RequestMethod.POST, RequestMethod.GET })
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PreAuthorize("hasPermission(#model, 'MEMBER_SEARCH_APPLICANTS')")
	public String searchApplicantsV2(Model model, HttpServletRequest request) {
		capRequestValidatorUtil.validateSearchApplicants(request);

		int iResultCount = 0;
		int totalConsumer = 0;
		List<String> effectiveYears = null;
		String coverageYear = null;
		Map<String, Object> applicantListAndRecordCount = new HashMap<String, Object>();
		List<ApplicationSource> applicationSourceList = new ArrayList<ApplicationSource>(Arrays.asList(ApplicationSource.values()));
		Map<String, Object> searchCriteria = null;
		try {

			searchCriteria = this.createMemSearchCriteria(model, request);

			if (searchCriteria != null) {
				List<String> searchParams = new ArrayList<String>();
				for (String searchParam : searchCriteria.keySet()) {
					if (SEARCH_CRITERIA_PARAMS_AUDIT.contains(searchParam)) {
						searchParams.add(searchParam);
					}
				}
				model.addAttribute("searchCriteria", searchCriteria);
				model.addAttribute(FILTER_SORTORDER, searchCriteria.get(FILTER_SORTORDER));

				searchParams.add(searchCriteria.get("appSource") != null ? "appSource=" + (String) searchCriteria.get("appSource") : "appSource=ALL");
				GiAuditParameterUtil.add(new GiAuditParameter("Search Criteria Parameters ", searchParams.toString()));
			}
			model.addAttribute("ispost", "0");
			if (model.containsAttribute(PAGE_ON_LOAD_FLAG)) {
				//ResponseEntity<Map> consumerResponseEntity =restTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_APPLICANTS, searchCriteria, Map.class);
				ResponseEntity<Map> consumerResponseEntity = ghixRestTemplate.exchange(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_APPLICANTS, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Map.class, searchCriteria);
				applicantListAndRecordCount = consumerResponseEntity.getBody();

				// Get applicants from SSAP_APPLICATIONS and SSAP_APPLICANTS
				List<ManageApplicantsDto> applicantList = (List<ManageApplicantsDto>) applicantListAndRecordCount.get("applicantList");
				Integer iResultCt = (Integer) applicantListAndRecordCount.get("recordCount");
				iResultCount = iResultCt.intValue();
				if (totalConsumer == 0) {
					model.addAttribute("totalApplicants", iResultCount);
				}
				model.addAttribute("exchangeType", applicantListAndRecordCount.get("exchangeType"));
				model.addAttribute("applicantList", applicantList);

				model.addAttribute("ispost", "submit");
				model.addAttribute("applicationStatusMap", getApplicationStatusAsMap());
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter("Number of records found=", iResultCount));
			effectiveYears = applicationRepository.findAllEffectiveYears();
			coverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		} catch (RestfulResponseException ex) {
			switch (ex.getHttpStatus()) {
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in searchConsumer().", ex.getMessage() + CAUSE);
				break;

			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
		} catch (Exception e) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, APPLICATION_ERROR));
			LOGGER.error("Error in searchConsumer()" + e.getMessage() + CAUSE + e.getCause());
		}

		model.addAttribute("effectiveYearSource", effectiveYears);
		model.addAttribute("resultSize", iResultCount);
		model.addAttribute(FILTER_PAGE_SIZE, LISTING_NUMBER);
		model.addAttribute(SUBMIT, SUBMIT);
		model.addAttribute("applicationSourceList", applicationSourceList);
		model.addAttribute("effectiveYear", coverageYear);
		model.addAttribute("enrollmentStatusList", getEnrollmentStatusList());

		return "crm/member/searchapplicants/screenpop";
	}

	private List<LookupValue> getEnrollmentStatusList() {
		List<String> lookupValueCode = new ArrayList<String>();

		lookupValueCode.add(Enrollment.ENROLLMENT_STATUS_CANCEL);
		lookupValueCode.add(Enrollment.ENROLLMENT_STATUS_TERM);
		lookupValueCode.add(Enrollment.ENROLLMENT_STATUS_CONFIRM);
		lookupValueCode.add(Enrollment.ENROLLMENT_STATUS_PENDING);

		List<LookupValue> enrollmentStatusList = lookupService.getLookupValueListForBOBFeed("ENROLLMENT_STATUS", lookupValueCode);

		Comparator<LookupValue> cmp = new Comparator<LookupValue>() {
			public int compare(LookupValue o1, LookupValue o2) {
				return o1.getLookupValueCode().compareTo(o2.getLookupValueCode());
			}
		};
		Collections.sort(enrollmentStatusList, cmp);
		return enrollmentStatusList;
	}

	private HashMap<String, String> getApplicationStatusAsMap() {
		HashMap<String, String> appStatusMap = new HashMap<String, String>();
		for (ApplicationStatus AppStatus : ApplicationStatus.values()) {
			appStatusMap.put(AppStatus.getApplicationStatusCode(), AppStatus.toString());
		}

		return appStatusMap;
	}
	//	@ApiOperation(value = "Search Members", notes = "This method is to search member.")
	//	@RequestMapping(value="crm/member/managemembers", method = {RequestMethod.POST, RequestMethod.GET})
	//	@SuppressWarnings({ "unchecked", "rawtypes" })
	//	// @PreAuthorize("hasPermission(#model, 'CSR_ROLE')")
	//	public String searchMember(Model model,HttpServletRequest request)
	//	{   try{
	//		int iResultCount =0;
	//		int totalConsumer=0;
	//		Map<String, Object> householdListAndRecordCount = new HashMap<String, Object>();
	//
	//		/* @PreAuthorize annotation need to be added for this mapping
	//		 * TBD
	//		 */
	//
	//		Map<String, Object> searchCriteria = this.createMemSearchCriteria(model,request);
	//		if(model.containsAttribute(PAGE_ON_LOAD_FLAG)){
	//		ResponseEntity<Map> consumerResponseEntity =restTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_CONSUMER, searchCriteria, Map.class);
	//		householdListAndRecordCount=consumerResponseEntity.getBody();
	//
	//		List<Map<String,Object>> householdList = (List<Map<String,Object>>) householdListAndRecordCount.get("householdList");
	//		Integer iResultCt = (Integer) householdListAndRecordCount.get("recordCount");
	//		iResultCount = iResultCt.intValue();
	//		if(totalConsumer==0){
	//			model.addAttribute("totalConsumer", iResultCount);
	//		}
	//		model.addAttribute("householdList", householdList);
	//		}
	//		model.addAttribute("searchCriteria", searchCriteria);
	//		model.addAttribute(FILTER_SORTORDER, searchCriteria.get(FILTER_SORTORDER));
	//		model.addAttribute("resultSize", iResultCount);
	//		model.addAttribute(FILTER_PAGE_SIZE, LISTING_NUMBER);
	//		model.addAttribute(SUBMIT, SUBMIT);
	//
	//
	//
	//	}
	//	catch(Exception e){
	//		model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
	//		LOGGER.error("Error in searchConsumer()"+e.getMessage()+CAUSE+e.getCause());
	//	}
	//		return "crm/member/searchmember";
	//	}

	/**
	 * @author Sneha
	 * @param model
	 * @param request
	 * @return
	 * below request mapping serves for  search consumer
	 */

	@RequestMapping(value = "crm/consumer/capturePhoneNumber", method = { RequestMethod.POST, RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String capturePhoneAndDNIS(Model model, HttpServletRequest request) {
		try {

			return "forward:/crm/consumer/manageConsumers";

		} catch (Exception e) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in capturePhoneAndDNIS()" + e.getMessage() + CAUSE + e.getCause());
		}
		return "forward:/crm/consumer/manageConsumers";
	}

	/**
	 * @author Sneha
	 * @param model
	 * @param request
	 * @return
	 * below request mapping serves for  search consumer
	 */
	/*
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private  List<Map<String, Object>> getAffiliateList(){
			HashMap<String, Object> searchCriteria = new HashMap<String, Object>();
			searchCriteria.put(START_RECORD_KEY, START_RECORD_WITH);
			searchCriteria.put(FILTER_PAGE_SIZE, PAGE_SIZE);
			searchCriteria.put(FILTER_SORTORDER, SORT_ASC);
			searchCriteria.put("sortBy", "accountExecutive");
			Map<String, Object> affiliateListAndRecordCount = null;
			List<Map<String, Object>> affiliateList =null;
			ResponseEntity<Map> affiliateResponseEntity = restTemplate.postForEntity(GhixEndPoints.AffiliateServiceEndPoints.SEARCH_AFFILIATE, searchCriteria, Map.class);
			affiliateListAndRecordCount = affiliateResponseEntity.getBody();
			affiliateList= (List<Map<String, Object>>) affiliateListAndRecordCount.get("affiliateList");
			return affiliateList;
		}
	*/
	/**
	 * @author Kuldeep
	 * @param model
	 * @param request
	 * @return
	 * creating the search criteria for searching employer
	 */
	//

	private Map<String, Object> createMemSearchCriteria(Model model, HttpServletRequest request) throws Exception {
		HashMap<String, Object> searchCriteria = new HashMap<String, Object>();
		boolean flagSearchCreteria = false;
		String phonepopup = "";
		String comingFromPopup = request.getParameter("comingFromPopUp");
		if (!StringUtils.isEmpty(comingFromPopup) && !comingFromPopup.trim().equals("")) {
			phonepopup = comingFromPopup;
		}
		try {
			String sortBy = (request.getParameter(FILTER_SORTBY) == null) ? FIRST_NAME : request.getParameter(FILTER_SORTBY);
			searchCriteria.put(FILTER_SORTBY, ((sortBy.equals("")) ? FIRST_NAME : sortBy));

			String sortOrder = (request.getParameter(FILTER_SORTORDER) == null) ? SORT_ASC : request.getParameter(FILTER_SORTORDER);
			sortOrder = (sortOrder.equals("")) ? SORT_ASC : sortOrder;
			String changeOrder = (request.getParameter(FILTER_CHANGEORDER) == null) ? "false" : request.getParameter(FILTER_CHANGEORDER);
			sortOrder = (changeOrder.equals("true")) ? ((sortOrder.equals(SORT_DESC)) ? SORT_ASC : SORT_DESC) : sortOrder;
			request.removeAttribute(FILTER_CHANGEORDER);
			request.setAttribute(FILTER_SORTORDER, sortOrder);

			searchCriteria.put(FILTER_SORTORDER, sortOrder);

			String strPageNum = request.getParameter("pageNumber");
			int iPageNum = StringUtils.isEmpty(strPageNum) ? 0 : (Integer.parseInt(strPageNum) - 1);
			int startRecord = iPageNum * LISTING_NUMBER;
			searchCriteria.put("startRecord", startRecord);
			searchCriteria.put(FILTER_PAGE_SIZE, LISTING_NUMBER);

			String firstName = request.getParameter(FIRST_NAME);
			if (!StringUtils.isEmpty(firstName) && !firstName.trim().equals("")) {
				searchCriteria.put(FIRST_NAME, firstName);
				flagSearchCreteria = true;
			}
			String lastName = request.getParameter("lastName");
			if (!StringUtils.isEmpty(lastName) && !lastName.trim().equals("")) {
				searchCriteria.put("lastName", lastName);
				flagSearchCreteria = true;
			}
			String householdEmail = request.getParameter("householdEmail");
			if (!StringUtils.isEmpty(householdEmail) && !householdEmail.trim().equals("")) {
				searchCriteria.put("householdEmail", householdEmail);
				flagSearchCreteria = true;
			}
			String contactNumber = "";
			if (!phonepopup.equals("") && phonepopup.equals("popup")) {
				contactNumber = request.getParameter("popupNumber");
			} else if (org.apache.commons.lang3.StringUtils.isNotBlank(request.getParameter("ANI"))) {
				contactNumber = request.getParameter("ANI");
			} else {
				contactNumber = request.getParameter("contactNumber");
			}

			if (!StringUtils.isEmpty(contactNumber) && !contactNumber.trim().equals("")) {
				searchCriteria.put("contactNumber", contactNumber);
				flagSearchCreteria = true;
			}

			String zipCode = request.getParameter("zipCode");
			if (!StringUtils.isEmpty(zipCode) && !zipCode.trim().equals("")) {
				searchCriteria.put("zipCode", zipCode);
				flagSearchCreteria = true;
			}

			String state = request.getParameter("state");
			if (!StringUtils.isEmpty(state) && !state.trim().equals("")) {
				searchCriteria.put("state", state);
				flagSearchCreteria = true;
			}

			String ssn = request.getParameter("ssn");
			if (!StringUtils.isEmpty(ssn) && !ssn.trim().equals("")) {
				searchCriteria.put("ssn", ssn);
				flagSearchCreteria = true;
			}

			String appId = request.getParameter("appId");
			if (!StringUtils.isEmpty(appId) && !appId.trim().equals("")) {
				searchCriteria.put("appId", appId);
				flagSearchCreteria = true;
			}

			String extAppId = request.getParameter("extAppId");
			if (!StringUtils.isEmpty(extAppId) && !extAppId.trim().equals("")) {
				searchCriteria.put("extAppId", extAppId);
				flagSearchCreteria = true;
			}

			String externalApplicantId = request.getParameter("externalApplicantId");
			if (!StringUtils.isEmpty(externalApplicantId) && !externalApplicantId.trim().equals("")) {
				searchCriteria.put("externalApplicantId", externalApplicantId);
				flagSearchCreteria = true;
			}

			String appSource = request.getParameter("appSource");
			if ((!StringUtils.isEmpty(appSource)) && (!"All".equalsIgnoreCase(appSource))) {
				searchCriteria.put("appSource", appSource.trim());
				flagSearchCreteria = true;
			}

			String isReferralFlow = request.getParameter("isReferralFlow");
			if (!StringUtils.isEmpty(isReferralFlow)) {
				searchCriteria.put("isReferralFlow", isReferralFlow);
			}

			String accessCode = request.getParameter("accessCode");
			if (!StringUtils.isEmpty(accessCode)) {
				searchCriteria.put("accessCode", accessCode.trim());
				flagSearchCreteria = true;
			}

			String effectiveYear = request.getParameter("effectiveYear");
			if (!StringUtils.isEmpty(effectiveYear)) {
				searchCriteria.put("effectiveYear", effectiveYear.trim());
				flagSearchCreteria = true;
			}

			String dateOfBirth = request.getParameter("dateOfBirth");
			if (!StringUtils.isEmpty(dateOfBirth)) {
				searchCriteria.put("dateOfBirth", XssHelper.stripXSS(dateOfBirth));
				flagSearchCreteria = true;
			}
			String enrollmentId = request.getParameter("enrollmentId");
			if (!StringUtils.isEmpty(enrollmentId)) {
				searchCriteria.put("enrollmentId", enrollmentId);
				flagSearchCreteria = true;
			}
			String enrollmentStatus = request.getParameter("enrollmentStatus");
			if (!StringUtils.isEmpty(enrollmentStatus)) {
				searchCriteria.put("enrollmentStatus", enrollmentStatus);
				flagSearchCreteria = true;
			}
			String cmrHouseholdId = request.getParameter("cmrHouseholdId");
			if (!StringUtils.isEmpty(cmrHouseholdId) && !cmrHouseholdId.trim().equals("")) {
				searchCriteria.put("cmrHouseholdId", cmrHouseholdId);
				flagSearchCreteria = true;
			}

			String externalCaseId = request.getParameter("externalCaseId");
			if (!StringUtils.isEmpty(externalCaseId) && !externalCaseId.trim().equals("")) {
				searchCriteria.put("externalCaseId", externalCaseId);
				flagSearchCreteria = true;
			}

		} catch (Exception e) {
			LOGGER.error("Error in createMemSearchCriteria()");
			throw e;
		}
		if (flagSearchCreteria) {
			model.addAttribute(PAGE_ON_LOAD_FLAG, "onSearchTrue");
		}

		return searchCriteria;
	}

	@ApiOperation(value = "View Member", notes = "This method is to view member.")
	@RequestMapping(value = "crm/member/viewmember/{encHouseholdid}", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String viewMember(Model model, HttpServletRequest request, @PathVariable("encHouseholdid") String encHouseholdid) {
		LOGGER.info("View Member Summary -- start ");
		capRequestValidatorUtil.validateViewMember(encHouseholdid);
		try {
			String StrEncTicketId = (StringUtils.isNumeric(encHouseholdid)) ? encHouseholdid : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdid);
			model.addAttribute(HOUSEHOLD_ID, StrEncTicketId);
			Integer id = Integer.parseInt(StrEncTicketId);
			LOGGER.info("Calling the RestAPI");
			String postParams = "\"" + id + "\"";
			LOGGER.info("Fetching pldHousehold  : " + SecurityUtil.sanitizeForLogging(postParams));
			Household household = consumerPortalUtil.getHouseholdRecord(id);
			String accessCode = getApplicantAccessCode(id);
			model.addAttribute("accessCode", accessCode);
			model.addAttribute(HOUSEHOLD, household);
			if (StringUtils.isNotEmpty(household.getSsn()) && household.getSsn().trim().length() == 9) {
				String consumerssn = "*****" + household.getSsn().substring(5, 9);
				model.addAttribute("consumerssn", consumerssn);
			}
			model.addAttribute(HOUSEHOLD, household);
			capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
			capPortalConfigurationUtil.setMemberInfoPermissions(model, request);
			this.clearSessionVariable(request.getSession());

			setViewMemberLink(model);
		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in viewMember()", ex);
		}
		LOGGER.info("viewMember Summary -- end ");
		return "crm/member/viewmember";
	}

	@ApiOperation(value = "View Applications", notes = "This method is to view applications.")
	@RequestMapping(value = "crm/newapplications/{encHouseholdid}", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'VIEW_APPLICATIONS_PAGE')")
	public String viewApplications(Model model, HttpServletRequest request, @PathVariable("encHouseholdid") String encHouseholdid) {
		LOGGER.info("View Applications -- start ");
		try {
			String StrEncTicketId = (StringUtils.isNumeric(encHouseholdid)) ? encHouseholdid : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdid);
			model.addAttribute(HOUSEHOLD_ID, StrEncTicketId);
			Integer id = Integer.parseInt(StrEncTicketId);
			LOGGER.info("Calling the RestAPI");
			String postParams = "\"" + id + "\"";
			LOGGER.info("Fetching pldHousehold  : " + SecurityUtil.sanitizeForLogging(postParams));
			Household household = consumerPortalUtil.getHouseholdRecord(id);
			String accessCode = getApplicantAccessCode(id);
			model.addAttribute("accessCode", accessCode);
			model.addAttribute(HOUSEHOLD, household);

			if (StringUtils.isNotEmpty(household.getSsn()) && household.getSsn().trim().length() == 9) {
				String consumerssn = "*****" + household.getSsn().substring(5, 9);
				model.addAttribute("consumerssn", consumerssn);
			}
			model.addAttribute(HOUSEHOLD, household);
			capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
			capPortalConfigurationUtil.setMemberInfoPermissions(model, request);
			this.clearSessionVariable(request.getSession());

			setSpecialEnrollmentInfoURL(model);

			setViewMemberLink(model);
		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in viewApplications()", ex);
		}
		LOGGER.info("viewApplications -- end ");
		return "crm/newapplications";
	}

	@RequestMapping(value = "/crm/getApplications", method = RequestMethod.GET)
	@GiAudit(transactionName = "Show all applications for a household", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
	@PreAuthorize("hasPermission(#model, 'VIEW_APPLICATIONS_PAGE')")
	@ResponseBody
	public MyApplications viewMyApplications(@RequestParam(value = "coverageYear", required = true) String coverageYear, @RequestParam(value = "encHouseholdId", required = true) String encHouseholdId) {
		MyApplications myApplications = new MyApplications();
		boolean isIndividual = true;
		try {
			Long householdId = Long.parseLong((StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId));
			IndPortalValidationDTO indPortalValidationDTO = util.validateCoverageYrForApplications(coverageYear);
			if (!indPortalValidationDTO.getStatus()) {
				LOGGER.debug(indPortalValidationDTO.getErrors().toString());
			} else {
				myApplications = util.getApplicationsForHousehold(householdId, isIndividual, coverageYear);
			}
		} catch (Exception ex) {
			LOGGER.error("Error retrieving applications", ex);
		}
		return myApplications;
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Mark household RIDP verified", notes = "This method is to mark household RIDP verified.")
	@RequestMapping(value = "crm/member/markRidpVerified", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public @ResponseBody String markRidpVerified(Integer householdId, String comments) {
		LOGGER.info("Mark RIDP Verified Summary -- start");
		capRequestValidatorUtil.validateMarkRidpVerified(Integer.toString(householdId));
		JSONObject response = new JSONObject();
		try {
			Household household = consumerPortalUtil.getHouseholdRecord(householdId);
			household.setRidpVerified(RIDPConstants.HOUSEHOLD_RIDP_VERIFIED);
			household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_CSR_OVERRIDE);
			household.setRidpDate(new TSDate());
			consumerPortalUtil.saveHouseholdRecord(household);
			saveCommentsInDB(household, comments);
			response.put(RIDPConstants.RESPONSE_CODE_FIELD, SUCCESS_RESPONSE_CODE);
		} catch (Exception e) {
			response.put(RIDPConstants.RESPONSE_CODE_FIELD, ERROR_RESPONSE_CODE);
			LOGGER.error("Error marking household ridp verified", e);
		}
		LOGGER.info("Mark RIDP Verified Summary -- end");
		return response.toJSONString();
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Convert to Manual RIDP verification", notes = "This method is to convert to Manual RIDP verification.")
	@RequestMapping(value = "crm/member/convertManualRIDP", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public @ResponseBody String convertManualRIDP(Integer householdId) {
		LOGGER.info("Convert to manual verification -- start");
		capRequestValidatorUtil.validateConvertManualRIDP(Integer.toString(householdId));
		JSONObject response = new JSONObject();
		try {
			Household household = consumerPortalUtil.getHouseholdRecord(householdId);
			household.setRidpSource(RIDPConstants.RIDP_VERIFY_SOURCE_MANUAL);
			household.setRidpDate(new TSDate());
			consumerPortalUtil.saveHouseholdRecord(household);
			response.put(RIDPConstants.RESPONSE_CODE_FIELD, SUCCESS_RESPONSE_CODE);
		} catch (Exception e) {
			response.put(RIDPConstants.RESPONSE_CODE_FIELD, ERROR_RESPONSE_CODE);
			LOGGER.error("Error converting RIDP source to Paper", e);
		}
		LOGGER.info("Convert to manual verification -- end");
		return response.toJSONString();
	}

	/**
	 * Handles the GET request for loading comments screen for broker.
	 *
	 * @param id
	 * @param request
	 * @param model
	 * @return
	 * @throws InvalidUserException
	 */
	@ApiOperation(value = "Get Consumer Comments from Consumer Id", notes = "This method takes Consumer Id as an input parameter and returns Consumer comments.")
	@RequestMapping(value = "/crm/member/comments/{enchouseholdid}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String getComments(@PathVariable("enchouseholdid") String encHouseholdId, HttpServletRequest request, Model model) throws InvalidUserException {
		LOGGER.info("consumer - getComments : START ");
		capRequestValidatorUtil.validateGetComments(encHouseholdId);
		try {
			String StrEncHouseholdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			model.addAttribute(HOUSEHOLD_ID, StrEncHouseholdId);
			Integer id = Integer.parseInt(StrEncHouseholdId);
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Comments");

			Household household = consumerPortalUtil.getHouseholdRecord(id);
			model.addAttribute(HOUSEHOLD, household);
			capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
			setViewMemberLink(model);

		} catch (Exception exception) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in consumer - getComments: ", exception);
		}

		LOGGER.info("consumer - getComments : END ");
		return "crm/member/comments";
	}

	/**
	 * Handles the GET/POST request for saving comments screen for broker.
	 *
	 * @param target_id
	 * @param target_name
	 * @param employerName
	 * @param model
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Add new Comment", notes = "This method will add a new Consumer Comment to database.")
	@RequestMapping(value = "/crm/consumer/newcomment", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String addNewComment(@RequestParam(value = "target_id", required = false) Integer target_id, @RequestParam(value = "target_name", required = false) String target_name, @RequestParam(value = "brokerName", required = false) String brokerName, Model model, HttpServletRequest request) {
		LOGGER.info("crm/consumer - addNewComment : START");
		try {
			model.addAttribute("page_title", "Getinsured Health Exchange: Comments");
			model.addAttribute("target_id", target_id);
			model.addAttribute("target_name", target_name);
		} catch (Exception exception) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in crm/consumer - addNewComment: ", exception);
		}

		LOGGER.info("crm/consumer - addNewComment : END");
		return "/crm/consumer/addnewcomment";
	}

	@ApiOperation(value = "Consumer Home", notes = "This method is used to switch role to Employee.")
	@RequestMapping(value = "/crm/consumer/dashboard", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String switchToConsumerView(Model model, HttpServletRequest request) {
		if (request.getSession().getAttribute("checkDilog") == null) {
			String checkDilog = request.getParameter("checkConsumerView");
			request.getSession().setAttribute("checkDilog", checkDilog);
		}

		String baseUrl = "/account/user/switchUserRole?ref=".intern();
		HashMap<String, String> paramMap = new HashMap<String, String>(3);
		paramMap.put("switchToModuleName", request.getParameter("switchToModuleName"));
		paramMap.put("switchToModuleId", request.getParameter("switchToModuleId"));
		paramMap.put("switchToResourceName", request.getParameter("switchToResourceName"));
		baseUrl = baseUrl + GhixAESCipherPool.encryptParameterMap(paramMap);

		return "forward:" + baseUrl;
	}

	/**
	 * Handles GET/POST request for displaying ticket history
	 *
	 * @param id
	 * @param model
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Get List of Ticket History for Consumer", notes = "This method takes Consumer Id as an input and returns List of Ticket History for Consumer.")
	@RequestMapping(value = "/crm/member/tickethistory/{enchouseholdid}", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String getConsumerTicketHistory(@PathVariable("enchouseholdid") String encHouseholdId, Model model, HttpServletRequest request) {

		LOGGER.info("consumer - getConsumerTicketHistory : START");
		capRequestValidatorUtil.validateGetConsumerTicketHistory(encHouseholdId);

		try {
			List<TkmTickets> ticketHistoryList = null;
			String StrEncHouseholdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			model.addAttribute(HOUSEHOLD_ID, StrEncHouseholdId);
			Integer id = Integer.parseInt(StrEncHouseholdId);
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Ticket History");

			Household household = consumerPortalUtil.getHouseholdRecord(id);

			model.addAttribute(HOUSEHOLD, household);

			ticketHistoryList = getTkmTicketsForConsumer(id);

			if (request.getAttribute(APPEAL_HISTORY) != null && ticketHistoryList != null) {
				List<TkmTickets> appealHistoryList = new ArrayList<TkmTickets>();
				for (TkmTickets tkmTickets : ticketHistoryList) {
					if ((tkmTickets.getTkmWorkflows().getType()).equals("Individual Appeal")) {
						appealHistoryList.add(tkmTickets);
					}
				}
				ticketHistoryList = appealHistoryList;
				model.addAttribute(APPEAL_HISTORY, "true");
			}

			String timezone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);

			if (ticketHistoryList != null) {
				for (TkmTickets ticket : ticketHistoryList) {
					Date convCreatedDate = TicketMgmtUtils.convertDateByTimezone(ticket.getCreated(), timezone);
					Date convUpdatedDate = TicketMgmtUtils.convertDateByTimezone(ticket.getUpdated(), timezone);
					ticket.setCreated(convCreatedDate);
					ticket.setUpdated(convUpdatedDate);
				}
			}

			model.addAttribute("ticketHistoryList", ticketHistoryList);
			model.addAttribute("pageSize", GhixConstants.PAGE_SIZE);
			capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
			setViewMemberLink(model);
		} catch (Exception exception) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in consumer - getConsumerTicketHistory: ", exception);
		}
		LOGGER.info("consumer - getConsumerTicketHistory : END");
		return "crm/member/tickethistory";
	}

	@RequestMapping(value = "/crm/member/appealhistory/{encHouseholdId}", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String getConsumerAppealHistory(@PathVariable("encHouseholdId") String encHouseholdId, Model model, HttpServletRequest request) {

		capRequestValidatorUtil.validateGetConsumerAppealHistory(encHouseholdId);

		String id = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
		//Integer id = Integer.parseInt(StrEncHouseholdId);
		model.addAttribute(HOUSEHOLD_ID, id);
		LOGGER.info("consumer - getConsumerAppealHistory : START");
		request.setAttribute(APPEAL_HISTORY, "true");
		LOGGER.info("consumer - getConsumerAppealHistory - Forwarding to tickethistory : END");
		setViewMemberLink(model);
		return "forward:/crm/member/tickethistory/" + id;
	}

	/**
	 * Retrieves all the tickets for the user from the ticket mgmt module
	 *
	 * @param userObj
	 * @return ticketsList
	 * @throws Exception
	 */
	private List<TkmTickets> getTkmTicketsForConsumer(Integer householdId) throws Exception {

		LOGGER.info("consumer - getTkmTicketsForConsumer : START");
		List<TkmTickets> ticketsList = null;

		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();

		try {
			tkmTicketsRequest.setModuleName("HOUSEHOLD");
			tkmTicketsRequest.setModuleId(householdId);
			String postResp = ghixRestTemplate.postForObject(GhixEndPoints.TicketMgmtEndPoints.GETCONSUMERTICKETLIST, tkmTicketsRequest, String.class);
			TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(postResp);
			ticketsList = tkmTicketsResponse.getTkmTicketsList();
		} catch (Exception exception) {
			LOGGER.error("Error in fetching the ticket list for the consumer:", exception);
			throw exception;
		}

		LOGGER.info("consumer - getTkmTicketsForConsumer : END");

		return ticketsList;

	}

	/**
	 * Handles GET/POST request for displaying the ticket details.
	 *
	 * @param model
	 * @param request
	 * @param id
	 * @param brokerId
	 * @return
	 */
	@ApiOperation(value = "Get Ticket Detail for Consumer", notes = "This method takes Ticket Id and Consumer Id as input parameters and returns Ticket Details for Consumer.")
	@RequestMapping(value = "/crm/member/ticketdetail/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String getConsumerTicketDetail(Model model, HttpServletRequest request, @PathVariable("id") String id, @RequestParam("memberId") String consumerId) {

		LOGGER.info("consumer - getConsumerTicketDetail : START");

		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();

		try {
			String strEncTicketId = (StringUtils.isNumeric(id)) ? id : ghixJasyptEncrytorUtil.decryptStringByJasypt(id);
			String strEncConsumerId = (StringUtils.isNumeric(consumerId)) ? consumerId : ghixJasyptEncrytorUtil.decryptStringByJasypt(consumerId);
			model.addAttribute(HOUSEHOLD_ID, strEncConsumerId);
			tkmTicketsRequest.setId(Integer.parseInt(strEncTicketId));
			Household household = consumerPortalUtil.getHouseholdRecord(Integer.parseInt(strEncConsumerId));

			model.addAttribute(HOUSEHOLD, household);

			// fetch the ticket data by ticketId from the ticket management
			// module.
			String postResp = ghixRestTemplate.getForObject(TicketMgmtEndPoints.SEARCH_TKMTICKETS_ID + tkmTicketsRequest.getId(), String.class);
			TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(postResp);
			TkmTickets tkmTicketsObj = tkmTicketsResponse.getTkmTickets();

			model.addAttribute("tkmTicketsObj", tkmTicketsObj);

		} catch (Exception exception) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in consumer - getConsumerTicketDetail: ", exception);
		}
		LOGGER.info("consumer - getConsumerTicketDetail : END");

		return "crm/member/ticketdetail";
	}

	/**
	 * Gets DOB of the consumer
	 * @param memberData
	 * @return
	 */
	private String getDOB(String memberData) {
		String dob = "";
		String memberDataLocal = memberData.replaceAll("\"", "");
		Pattern p1 = Pattern.compile("(dateOfBirth:[\\d]{2}/[\\d]{2}/[\\d]{4})");
		Matcher m1 = p1.matcher(memberDataLocal);
		while (m1.find()) {
			dob = m1.group().substring(TWELVE);
			dob = (!dob.equalsIgnoreCase("null")) ? dob : "";
		}
		return dob;
	}

	/**
	 * Gets the tobacco user flag of the consumer
	 * @param memberData
	 * @return
	 */
	private String getIsTobaccoUser(String memberData) {
		String isTobaccoUser = "";
		String memberDataLocal = memberData.replaceAll("\"", "");
		Pattern p = Pattern.compile("(isTobaccoUser:)(\\w+)");
		Matcher m = p.matcher(memberDataLocal);
		while (m.find()) {
			isTobaccoUser = m.group().substring(FOURTEEN);
			isTobaccoUser = (!isTobaccoUser.equalsIgnoreCase("null")) ? isTobaccoUser : "";
		}
		return isTobaccoUser;

	}

	/**
	 * Gets the address fields from the zipcode
	 * @param zip
	 * @return
	 */
	private Map<String, String> getAddressFields(String zip) {

		Map<String, String> retMap = new HashMap<String, String>();
		ZipCode zipCode = zipCodeService.findByZipCode(zip);

		if (zipCode != null) {
			LOGGER.info("Zip " + SecurityUtil.sanitizeForLogging(zipCode.getZip()));
			LOGGER.info("State " + SecurityUtil.sanitizeForLogging(zipCode.getState()));
			LOGGER.info("State Name " + SecurityUtil.sanitizeForLogging(zipCode.getStateName()));
			LOGGER.info("County " + SecurityUtil.sanitizeForLogging(zipCode.getCounty()));

			retMap.put("stateCode", zipCode.getState());
			retMap.put("county", zipCode.getCounty());
			retMap.put("stateName", zipCode.getStateName());

		}
		return retMap;
	}

	private void generateActivationLink(Household household) {
		try {
			//to be removed
			AccountUser user = null;
			try {
				user = userService.getLoggedInUser();
			} catch (InvalidUserException e) {

			}

			Map<String, String> consumerActivationDetails = new HashMap<String, String>();
			consumerActivationDetails.put("consumerName", household.getFirstName() + " " + household.getLastName());
			consumerActivationDetails.put("exchangeName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			consumerActivationDetails.put("exchangePhone", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			consumerActivationDetails.put("exchangeURL", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
			consumerActivationDetails.put("emailType", "consumerAccountActivationEmail");
			consumerActivationDetails.put("expirationDays", String.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.EMPLOYEE)));
			CreatedObject createdObject = new CreatedObject();
			createdObject.setObjectId(household.getId());
			createdObject.setEmailId(household.getEmail());
			createdObject.setRoleName(GhixRole.INDIVIDUAL.toString());
			createdObject.setPhoneNumbers(Arrays.asList(household.getPhoneNumber()));
			createdObject.setFullName(household.getFirstName() + " " + household.getLastName());
			createdObject.setFirstName(household.getFirstName());
			createdObject.setLastName(household.getLastName());
			createdObject.setCustomeFields(consumerActivationDetails);
			CreatorObject creatorObject = new CreatorObject();
			if (user != null) {
				creatorObject.setObjectId(user.getId());
				creatorObject.setFullName(user.getFullName() + " " + user.getLastName());
			}
			creatorObject.setRoleName(GhixRole.ADMIN.toString());

			accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject, Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.EMPLOYEE)));

		} catch (Exception e) {

		}

	}

	@GiAudit(transactionName = "Show Application Details", eventType = EventTypeEnum.CAP_APPLICATION_SEARCH, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = "crm/application/linkDetails/{ssapAppId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public String showDetails(Model model, HttpServletRequest request, @PathVariable("ssapAppId") String encSsapAppId) {
		LOGGER.info("show details of the application : " + SecurityUtil.sanitizeForLogging(encSsapAppId));
		List<Map<String, Object>> houseHoldList = null;
		Map<String, Object> application = null;
		String error = StringUtils.EMPTY;
		int recordCount = 0;

		String strSsapAppId = (StringUtils.isNumeric(encSsapAppId)) ? encSsapAppId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encSsapAppId);
		Long ssapAppId = Long.parseLong(strSsapAppId);
		GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
		if (ssapAppId != null && ssapAppId > 0) {
			Map<String, Object> searchCriteria = this.createReffAppSearchCriteria(String.valueOf(ssapAppId), request);
			try {
				//ResponseEntity<Map> appDetailsEntity = restTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.GET_LINKAPPDATA, searchCriteria, Map.class);
				ResponseEntity<Map> appDetailsEntity = ghixRestTemplate.exchange(GhixEndPoints.ConsumerServiceEndPoints.GET_LINKAPPDATA, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Map.class, searchCriteria);
				Map<String, Object> applDetailsMap = appDetailsEntity.getBody();

				houseHoldList = (List<Map<String, Object>>) applDetailsMap.get("houseHoldList");
				application = (Map<String, Object>) applDetailsMap.get("application");
				recordCount = (Integer) applDetailsMap.get("recordCount");
				//recordCount = new BigDecimal(lRecCnt).intValueExact();

				if (application != null) {
					model.addAttribute("applicationJson", JacksonUtils.getJacksonObjectWriterForHashMap(String.class, String.class).writeValueAsString(application));
				}

			} catch (RestfulResponseException ex) {
				LOGGER.error("Unable to get the link data", ex);
				application = null;
				houseHoldList = new ArrayList<>();
				error = "Unable to get the application link data.";
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Unable to get the application link data"));
				switch (ex.getHttpStatus()) {
				case INTERNAL_SERVER_ERROR:
					LOGGER.error("Error in searchConsumer().", ex);
					break;

				default:
					LOGGER.error("Unknown error occurred. ", ex);
					break;
				}
			}

			catch (Exception e) {
				LOGGER.error("Unable to get the link data", e);
			}
		} else {
			LOGGER.error("Invalid parameters to get the application link data");
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Invalid parameters to get the application link data"));
			error = "Invalid application id";
		}

		model.addAttribute("recordCount", recordCount);
		model.addAttribute("application", application);
		model.addAttribute("houseHoldList", houseHoldList);
		model.addAttribute("errorMsg", error);
		model.addAttribute("resultSize", recordCount);
		model.addAttribute(FILTER_PAGE_SIZE, LISTING_NUMBER);
		return "crm/application/linkDetails";
	}

	private Map<String, Object> createReffAppSearchCriteria(String ssapId, HttpServletRequest request) {
		Map<String, Object> searchCriteria = new HashMap<>();

		String strSsapId = request.getParameter("ssapAppId");
		if (StringUtils.isEmpty(strSsapId)) {
			strSsapId = ssapId;
		}
		searchCriteria.put("ssapAppId", strSsapId);

		String strPageNum = request.getParameter("pageNumber");
		int iPageNum = StringUtils.isEmpty(strPageNum) ? 0 : (Integer.parseInt(strPageNum) - 1);
		int startRecord = iPageNum * LISTING_NUMBER;

		searchCriteria.put("startRecord", startRecord);
		searchCriteria.put("pageSize", LISTING_NUMBER);

		return searchCriteria;
	}

	@GiAudit(transactionName = "Get Application Details", eventType = EventTypeEnum.CAP_APPLICATION_SEARCH, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = "crm/application/checkprelinkdata/{ssapAppId}/{cmrId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	@ResponseBody
	public String checkPrelinkData(@PathVariable("ssapAppId") String ssapAppId, @PathVariable("cmrId") String cmrId, HttpServletResponse response) throws InvalidUserException {

		response.setContentType("application/json");
		String restResponse = StringUtils.EMPTY;
		String householdId = "0";

		try {
			String ssapId = ghixJasyptEncrytorUtil.decryptStringByJasypt(ssapAppId);
			if (!cmrId.equals("0")) {
				householdId = ghixJasyptEncrytorUtil.decryptStringByJasypt(cmrId);
			}

			restResponse = ghixRestTemplate.getForObject(GhixEndPoints.EligibilityEndPoints.LINKING_STATUS_SSAP_CMRHOUSEHOLD + "/" + ssapId + "/" + householdId, String.class);
			CheckPreLinkDataDto checkPreLinkDataDTO = JacksonUtils.getJacksonObjectReaderForJavaType(CheckPreLinkDataDto.class).readValue(restResponse);
			if (!(checkPreLinkDataDTO.getREF_LCE_CMR_ID()).isEmpty()) {

				String encryptCmrId = ghixJasyptEncrytorUtil.encryptStringByJasypt(checkPreLinkDataDTO.getREF_LCE_CMR_ID());
				checkPreLinkDataDTO.setREF_LCE_CMR_ID(encryptCmrId);
				restResponse = JacksonUtils.getJacksonObjectWriterForJavaType(CheckPreLinkDataDto.class).writeValueAsString(checkPreLinkDataDTO);
				GiAuditParameterUtil.add(new GiAuditParameter("SSAP Application Id", ssapId), new GiAuditParameter("REF_LCE_CMR_ID", checkPreLinkDataDTO.getREF_LCE_CMR_ID()));

			}

		} catch (RestClientException | IOException e) {
			LOGGER.error("Unable to get the link data", e);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Unable to get the link data"));
		}
		GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
		return restResponse;
	}

	private void saveCommentsInDB(Household household, String commentText) throws InvalidUserException {

		CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(new Long(household.getId()), TargetName.CONSUMER);
		if (commentTarget == null) {
			commentTarget = new CommentTarget();
			commentTarget.setTargetId(new Long(household.getId()));
			commentTarget.setTargetName(TargetName.CONSUMER);
		}

		AccountUser accountUser = userService.getLoggedInUser();
		Comment comment = new Comment();
		String commenterName = "";

		if (accountUser != null) {
			comment.setComentedBy(accountUser.getId());

			commenterName += (accountUser.getFirstName() == null || accountUser.getFirstName().length() == 0) ? "" : accountUser.getFirstName() + " ";
			commenterName += (accountUser.getLastName() == null || accountUser.getLastName().length() == 0) ? "" : accountUser.getLastName();
		}

		//Checking if user's name is blank
		if (StringUtils.isBlank(commenterName)) {
			commenterName = "Anonymous User";
		}

		comment.setCommenterName(commenterName);

		//Fetching first 4000 char for comment text;
		commentText = (StringUtils.defaultString(commentText)).trim();//Fixing probable cause of NPE found in HIX-101733
		int beginIndex = 0;
		int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH : commentText.length();
		commentText = commentText.substring(beginIndex, endIndex);
		comment.setComment(commentText);
		comment.setCommentTarget(commentTarget);

		List<Comment> comments = null;

		if (commentTarget.getComments() == null || commentTarget.getComments().size() == 0) {
			comments = new ArrayList<Comment>();
		} else {
			comments = commentTarget.getComments();
		}

		comments.add(comment);
		commentTarget.setComments(comments);
		commentTargetService.saveCommentTarget(commentTarget);
	}

	@RequestMapping(value = "crm/member/editmember/{encHouseholdid}", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String editMember(final Model model, final HttpServletRequest request, @PathVariable("encHouseholdid") String encHouseholdid, final RedirectAttributes redirectAttrs) {
		LOGGER.info("View Member Summary -- start ");
		capRequestValidatorUtil.validateEditMember(encHouseholdid);
		try {
			String StrEncTicketId = (StringUtils.isNumeric(encHouseholdid)) ? encHouseholdid : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdid);
			Integer id = Integer.parseInt(StrEncTicketId);
			LOGGER.info("Calling the RestAPI");
			String postParams = "\"" + id + "\"";
			LOGGER.info("Fetching pldHousehold  : " + SecurityUtil.sanitizeForLogging(postParams));
			Household household = consumerPortalUtil.getHouseholdRecord(id);
			String accessCode = getApplicantAccessCode(id);
			model.addAttribute("accessCode", accessCode);
			String consumerssn = StringUtils.EMPTY;
			if (StringUtils.isNotEmpty(household.getSsn()) && household.getSsn().trim().length() == 9) {
				consumerssn = "*****" + household.getSsn().trim().substring(5, 9);
			}
			model.addAttribute("consumerssn", consumerssn);
			model.addAttribute(HOUSEHOLD_ID, household.getId());
			model.addAttribute(HOUSEHOLD, household);
			capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
			capPortalConfigurationUtil.setMemberInfoPermissions(model, request);
			setViewMemberLink(model);
		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in editMember()", ex);
		}
		LOGGER.info("viewMember Summary -- end ");
		return "crm/member/editmember";
	}

	@RequestMapping(value = "crm/member/editmemberSubmit", method = { RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String editMemberSubmit(final Model model, final HttpServletRequest request, @ModelAttribute(value = "Household") final Household householdObj, final BindingResult result, final RedirectAttributes redirectAttrs) {
		LOGGER.info("Edit Member Submit -- start ");
		capRequestValidatorUtil.validateEditMemberSubmit(householdObj);
		String encHouseholdid = request.getParameter("encHouseholdid");
		try {

			//if(StringUtils.isEmpty(displaySSN) && )
			String StrEncTicketId = (StringUtils.isNumeric(encHouseholdid)) ? encHouseholdid : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdid);
			Integer id = Integer.parseInt(StrEncTicketId);
			LOGGER.info("Updating edit member details" + id);
			householdObj.setId(id);
			householdObj.setSsn(StringUtils.defaultString(request.getParameter("displaySSN")));
			householdObj.setSsnNotProvidedReason("NO".equalsIgnoreCase(request.getParameter("ssnRequired")) ? StringUtils.defaultString(request.getParameter("ssnNotReqdReasons")).trim() : "");// store reason only in case SSN has not been provided.
			consumerPortalUtil.updateEditMemberDetails(householdObj);
			model.addAttribute(HOUSEHOLD, householdObj);
		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in editMember()", ex);
		}
		LOGGER.info("viewMember Summary -- end ");
		return "redirect:viewmember/" + encHouseholdid;
	}

	@SuppressWarnings("unchecked")
	@GiAudit(transactionName = "CAP Relinking Transaction", eventType = EventTypeEnum.CAP_RELINKING_TOOL, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = "crm/member/relinkMember/{linkEncHouseholdid}/{unlinkEncHouseholdid}/{userId}", method = { RequestMethod.POST, RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String reLinkMember(Model model, HttpServletRequest request, @PathVariable("linkEncHouseholdid") String linkEncHouseholdid, @PathVariable("unlinkEncHouseholdid") String unlinkEncHouseholdid, @PathVariable("userId") String userId) {
		LOGGER.info("Inside reLinkMember of AdminConsumerController ");
		try {

			linkEncHouseholdid = (StringUtils.isNumeric(linkEncHouseholdid)) ? linkEncHouseholdid : ghixJasyptEncrytorUtil.decryptStringByJasypt(linkEncHouseholdid);
			unlinkEncHouseholdid = (StringUtils.isNumeric(unlinkEncHouseholdid)) ? unlinkEncHouseholdid : ghixJasyptEncrytorUtil.decryptStringByJasypt(unlinkEncHouseholdid);

			LOGGER.info("Posting consumer service for linking household with id: " + SecurityUtil.sanitizeForLogging(linkEncHouseholdid));

			Integer linkhouseholdId = Integer.parseInt(linkEncHouseholdid);
			Integer delinkhouseholdId = Integer.parseInt(unlinkEncHouseholdid);
			Integer userid = Integer.parseInt(userId);
			Map<String, Integer> reuestMap = new HashMap<String, Integer>();
			reuestMap.put("linkhouseholdId", linkhouseholdId);
			reuestMap.put("delinkhouseholdId", delinkhouseholdId);
			reuestMap.put("userid", userid);
			AccountUser user = userService.getLoggedInUser();
			if (null != user) {
				reuestMap.put("loggedInUser", user.getId());
			}

			//ResponseEntity<Map> consumerResponseEntity = restTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.RELINK_CMRHOUSEHOLD_TO_USER,reuestMap,Map.class);
			ResponseEntity<Map> consumerResponseEntity = ghixRestTemplate.exchange(GhixEndPoints.ConsumerServiceEndPoints.RELINK_CMRHOUSEHOLD_TO_USER, getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, Map.class, reuestMap);
			if (null != consumerResponseEntity && null != consumerResponseEntity.getBody() && null != consumerResponseEntity.getBody().get("household")) {
				LOGGER.info("The house hold is linked to new module user: ");
				request.getSession().setAttribute(ERROR_MESSAGE, "Relink successful");
				consumerEventsUtil.generateHouseholdRelinkedEvent(consumerResponseEntity.getBody());
			}
			GiAuditParameterUtil.add(new GiAuditParameter("Relinking Parameters", "Linking Household is " + linkhouseholdId + " and Unlinking Household is " + delinkhouseholdId));

			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));

		} catch (RestfulResponseException ex) {
			request.getSession().setAttribute(ERROR_MESSAGE, "Relink failed.");
			switch (ex.getHttpStatus()) {
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in Relink.", ex.getMessage() + CAUSE);
				break;

			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
		} catch (Exception ex) {
			request.getSession().setAttribute(ERROR_MESSAGE, "Relink failed.");
			GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog("RELINK", new TSDate(), Exception.class.getName(), ExceptionUtils.getStackTrace(ex), null);
			LOGGER.error("Error in viewMember()", ex);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Relink failed"));
		}
		LOGGER.info("viewMember Summary -- end ");
		return "redirect:/crm/member/managemembers";

	}

	private String getUserName() throws Exception {
		String userName = "";

		try {
			AccountUser user = userService.getLoggedInUser();
			userName = user.getUsername();
		} catch (Exception exception) {
			String errorMsg = "GhixRestCallInvoker InvalidUserException";
			;
			LOGGER.warn(errorMsg);
			return null;
		}
		return userName;
	}

	private void validateLoggedInAgent(HttpServletRequest request, UserService userService, BrokerService brokerService) throws AccessDeniedException, AccessDeniedException {
		try {
			AccountUser user = null;

			user = userService.getLoggedInUser();

			Role userRole = userService.getDefaultRole(user);
			if (userRole != null && "BROKER".equalsIgnoreCase(userRole.getName())) {

				Broker brokerObj = new Broker();
				String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
				if (isUserSwitchToOtherView != null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)) {
					brokerObj = brokerService.getBrokerDetail(userService.getLoggedInUser().getActiveModuleId());
				} else {
					brokerObj = brokerService.findBrokerByUserId(user.getId());
				}
				if (BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(brokerObj)) {
					throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
				}

			}
		} catch (AccessDeniedException accessDeniedException) {
			LOGGER.error("Exception occured while accepting designation request for Employer / Individual : ", accessDeniedException);
			throw accessDeniedException;
		} catch (InvalidUserException ex) {
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER, ex);
		}

		catch (Exception e) {
			LOGGER.error("Error in newConsumer()", e);
		}
	}

	@ApiOperation(value = "View Enrollment", notes = "This method is the landing page for enrollment details")
	@RequestMapping(value = "crm/member/enrollment/{encHouseholdid}", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String viewEnrollment(Model model, HttpServletRequest request, @PathVariable("encHouseholdid") String encHouseholdId) {
		LOGGER.info("View Enrollment -- start ");
		capRequestValidatorUtil.validateViewEnrollment(encHouseholdId);
		Map<String, Boolean> overridePermsForUser = this.perEvaluator.getUserPermissions(ConsumerPortalUtil.OVERRIDE_PERMS);
		try {
			String householdIdStr = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			Integer householdId = Integer.parseInt(householdIdStr);
			model.addAttribute(HOUSEHOLD_ID, householdId);
			Household household = consumerPortalUtil.getHouseholdRecord(householdId);
			model.addAttribute(HOUSEHOLD, household);
			model.addAttribute("householdExists", true);
			model.addAttribute("isResendAllowed", (!overridePermsForUser.isEmpty() && overridePermsForUser.containsKey(ConsumerPortalUtil.CAP_ALLOW_RESEND834)) ? overridePermsForUser.containsKey(ConsumerPortalUtil.CAP_ALLOW_RESEND834) : "N");
			model.addAttribute("isEnrollmentEditAllowed", (!overridePermsForUser.isEmpty() && overridePermsForUser.containsKey(ConsumerPortalUtil.ENROLLMENT_DATA_EDIT)) ? overridePermsForUser.containsKey(ConsumerPortalUtil.CAP_ALLOW_RESEND834) : "N");
			model.addAttribute("isEnrollmentReadOnlyAllowed", (!overridePermsForUser.isEmpty() && overridePermsForUser.containsKey(ConsumerPortalUtil.ENROLLMENT_DATA_READ_ONLY)) ? overridePermsForUser.containsKey(ConsumerPortalUtil.CAP_ALLOW_RESEND834) : "N");

			capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
			capPortalConfigurationUtil.setEnrollmentInfoPermissions(model, request);
			setViewMemberLink(model);
		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in viewEnrollment() : ", ex);
		}
		LOGGER.info("View Enrollment -- End ");
		return "crm/member/enrollment";
	}

	@ApiOperation(value = "View Premium History Enrollment", notes = "This method is the landing page for enrollment premium details")
	@RequestMapping(value = "crm/member/enrollment/premiumhistory/{encEnrollmentId}", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String viewEnrollmentPremium(Model model, HttpServletRequest request, @PathVariable("encEnrollmentId") String encEnrollmentId) {

		capRequestValidatorUtil.validateViewEnrollment(encEnrollmentId);
		Map<String, Boolean> overridePermsForUser = this.perEvaluator.getUserPermissions(ConsumerPortalUtil.OVERRIDE_PERMS);
		try {
			String enrollmentIdString = (StringUtils.isNumeric(encEnrollmentId)) ? encEnrollmentId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encEnrollmentId);
			Integer enrollmentId = Integer.parseInt(enrollmentIdString);
			Integer householdCaseId = 0;
			EnrollmentDataDTO newEnrollmentDataDTO = null;
			List<EnrollmentDataDTO> enrollmentDataDTOList = fetchEnrollmentDetails(enrollmentId);
			if (enrollmentDataDTOList != null && !enrollmentDataDTOList.isEmpty()) {
				for (EnrollmentDataDTO enrollmentDataDTO : enrollmentDataDTOList) {
					if (enrollmentId.equals(enrollmentDataDTO.getEnrollmentId())) {
						householdCaseId = enrollmentDataDTO.getHouseHoldCaseId() != null ? Integer.parseInt(enrollmentDataDTO.getHouseHoldCaseId()) : 0;
						newEnrollmentDataDTO = enrollmentDataDTO;
						break;
					}
				}
			}
			model.addAttribute("enrollmentDetail", newEnrollmentDataDTO != null ? new Gson().toJson(newEnrollmentDataDTO) : null);
			model.addAttribute(HOUSEHOLD_ID, householdCaseId);
			Household household = consumerPortalUtil.getHouseholdRecord(householdCaseId);
			model.addAttribute(HOUSEHOLD, household);
			model.addAttribute("isResendAllowed", overridePermsForUser.get(ConsumerPortalUtil.CAP_ALLOW_RESEND834) ? "Y" : "N");
			model.addAttribute("isEnrollmentEditAllowed", overridePermsForUser.get(ConsumerPortalUtil.ENROLLMENT_DATA_EDIT) ? "Y" : "N");
			model.addAttribute("isEnrollmentReadOnlyAllowed", overridePermsForUser.get(ConsumerPortalUtil.ENROLLMENT_DATA_READ_ONLY) ? "Y" : "N");
			model.addAttribute("encodedEnrollmentID", encEnrollmentId);
			model.addAttribute("redirectPremiumHistory", "true");

		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in viewEnrollment() : ", ex);
		}
		return "crm/member/enrollment";
	}

	@ApiOperation(value = "View Enrollment and Enrollee Details", notes = "This method is to view Enrollment and Enrollee details for the household")
	@RequestMapping(value = "crm/member/enrollmentandenrolleedetails", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	@ResponseBody
	public String viewEnrollmentAndEnrolleeDetails(@RequestParam String encHouseholdId, HttpServletResponse response) throws InvalidUserException {

		EnrollmentResponse enrollmentResponse = null;
		response.setContentType(CONTENT_TYPE);
		Gson gson = new Gson();
		AccountUser user = null;
		//String responseJSON = StringUtils.EMPTY;
		try {
			String householdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			user = userService.getLoggedInUser();

			List<String> householdIds = new ArrayList<String>();
			householdIds.add(householdId);
			List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
			enrollmentStatusList.add(EnrollmentStatus.TERM);
			enrollmentStatusList.add(EnrollmentStatus.CANCEL);
			enrollmentStatusList.add(EnrollmentStatus.PENDING);
			enrollmentStatusList.add(EnrollmentStatus.CONFIRM);

			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setHouseHoldCaseIdList(householdIds);
			enrollmentRequest.setPremiumDataRequired(true);
			enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);

			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, gson.toJson(enrollmentRequest));

			//HIX-116390
			List<String> userPermissionList = new ArrayList<String>();
			userPermissionList.addAll(capPortalConfigurationUtil.getUserPermissions(null, user));

			if (res != null) {
				enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
				enrollmentResponse.setUserPermissionList(userPermissionList);
				Map<String, List<EnrollmentDataDTO>> map = enrollmentResponse.getEnrollmentDataDtoListMap();
				if (map != null && !map.isEmpty()) {
					List<EnrollmentDataDTO> enrollmentList = map.get(householdId);
					for (EnrollmentDataDTO enrollmentDataDTO : enrollmentList) {
						enrollmentDataDTO.setIssuerId(ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollmentDataDTO.getIssuerId()));
						enrollmentDataDTO.setEncEnrollmentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(enrollmentDataDTO.getEnrollmentId())));
					}
				}
				//responseJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentResponse.class).writeValueAsString(enrollmentResponse);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
			GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), Exception.class.getName(), e.getStackTrace().toString(), user);
			GIErrorCode errorCodeObject = gobj.getErrorCode();
			return errorCodeObject.getMessage();
		}

		return gson.toJson(enrollmentResponse);
	}

	@ApiOperation(value = "View Enrollment History", notes = "This method is to view enrollment history for the enrollment id")
	@RequestMapping(value = "crm/member/enrollmenthistory", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	@ResponseBody
	public String viewEnrollmentHistoryDetails(@RequestBody HashMap<String, String> inputParams, HttpServletResponse response) throws InvalidUserException {

		List<AppEvent> appEventList = null;
		response.setContentType(CONTENT_TYPE);
		String responseJson = StringUtils.EMPTY;

		try {
			String encHouseholdId = inputParams.get("encHouseholdId");
			String householdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			String eventType = StringUtils.EMPTY;
			Map<String, Object> searchCriteria = new HashMap<String, Object>();
			searchCriteria.put("moduleId", householdId);

			List<LookupValue> lookupValueList = lookupService.getLookupValueListForAppEvents("APPEVENT_ENROLLMENT");
			if (lookupValueList == null || lookupValueList.isEmpty()) {
				return JacksonUtils.getJacksonObjectWriterForJavaTypeList(List.class, LookupValue.class).writeValueAsString(appEventList);
			}

			LookupValue lookupValue = lookupValueList.get(0);
			eventType = lookupValue.getLookupType().getDescription();
			searchCriteria.put("type", eventType);

			responseJson = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.SEARCH_APP_EVENTS, searchCriteria, String.class);

		} catch (RestfulResponseException ex) {
			switch (ex.getHttpStatus()) {
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in getting enrollment event history.", ex);
				break;

			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
		} catch (Exception exception) {
			LOGGER.error("Error in getting enrollment event history: " + exception.getMessage() + CAUSE + exception.getCause());
		}

		return responseJson;
	}

	private String getApplicantAccessCode(int id) {
		LOGGER.info("Calling the RestAPI getApplicantAccessCode");
		String postParams = "\"" + id + "\"";
		String latestAccessCode = "No Financial Applications found.";
		LOGGER.info("Fetching pldHousehold  : " + SecurityUtil.sanitizeForLogging(postParams));
		XStream xstream = GhixUtils.getXStreamStaxObject();
		try {
			String postResp = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GETHOUSEHOLDACCESSCODES, postParams, String.class);
			@SuppressWarnings("unchecked")
			List<String> consumerResponse = (List<String>) xstream.fromXML(postResp);
			if (null != consumerResponse && consumerResponse.size() > 0) {
				latestAccessCode = consumerResponse.get(0);
			}
		} catch (RestfulResponseException ex) {
			switch (ex.getHttpStatus()) {
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Error in getting household codes.", ex.getMessage() + CAUSE);
				break;

			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
		}
		return latestAccessCode;
	}

	@RequestMapping(value = "crm/member/resend834", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	@ResponseBody
	public Map<String, Boolean> resend834(@RequestParam String encEnrollmentId, @RequestParam String eventType, @RequestParam String comment, HttpServletResponse response) throws InvalidUserException {

		Map<String, Boolean> mapResponse = new HashMap<String, Boolean>();
		mapResponse.put("status", Boolean.FALSE);

		response.setContentType(CONTENT_TYPE);
		AccountUser user = userService.getLoggedInUser();
		try {
			String enrollmentId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encEnrollmentId);

			List<Integer> enrollmentIds = new ArrayList<Integer>();
			enrollmentIds.add(Integer.parseInt(enrollmentId));
			Enrollment834ResendDTO resendDto = new Enrollment834ResendDTO();
			resendDto.setEnrollmentIds(enrollmentIds);

			Enrollment834ResendDTO.EVENT_TYPE resendEventType = (("ALL").compareToIgnoreCase(eventType) == 0) ? Enrollment834ResendDTO.EVENT_TYPE.ALL : Enrollment834ResendDTO.EVENT_TYPE.LAST;
			resendDto.setEventType(resendEventType);
			resendDto.setComments(comment);

			ObjectMapper mapper = new ObjectMapper();
			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.SET_RESEND_834_ENROLLMENT_URL, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, mapper.writeValueAsString(resendDto));
			if (res != null) {
				Gson gson = new Gson();
				EnrollmentResponse enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
				Map<Integer, String> map = enrollmentResponse.getFailedEnrollmentMap();
				mapResponse.put("status", (map != null && map.size() == 0));
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
			GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), Exception.class.getName(), e.getStackTrace().toString(), user);
			LOGGER.error(gobj.toString());
		}

		return mapResponse;
	}

	@RequestMapping(value = "crm/member/appeventcomments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<String> getAppEventComments(@RequestParam String encAppEventId, HttpServletResponse response) {
		LOGGER.info("================== In getAppEventComments ================");
		List<String> commentsList = new ArrayList<String>();

		try {
			String appEventId = (StringUtils.isNumeric(encAppEventId)) ? encAppEventId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encAppEventId);
			LOGGER.info("Get the application event: ", appEventId);

			CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(new Long(appEventId), TargetName.APP_EVENT);
			List<Comment> comments = (commentTarget == null) ? new ArrayList<Comment>() : commentTarget.getComments();

			if (comments != null && !comments.isEmpty()) {
				for (Comment comment : comments) {
					commentsList.add(StringEscapeUtils.unescapeHtml4(StringEscapeUtils.unescapeHtml4(comment.getComment())));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
		}

		return commentsList;
	}

	@GiAudit(transactionName = "Update Enrollment Details", eventType = EventTypeEnum.MEMBER_ENROLLMENT_UPDATE, eventName = EventNameEnum.EDIT_ENROLLMENT)
	@RequestMapping(value = "crm/member/updateEnrollmentdetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public EnrollmentResponse updateEnrollmentdetails(@RequestBody String entollmentJson, HttpServletResponse response) {
		LOGGER.info("================== In getAppEventComments ================");
		EnrollmentResponse enrollmentResponse = null;
		Gson gson = new Gson();
		try {
			AccountUser user = userService.getLoggedInUser();
			EnrollmentUpdateDTO enrollmentUpdateDTO = gson.fromJson(entollmentJson, EnrollmentUpdateDTO.class);
			//if (isValidEnrollmentUpdateDTO(enrollmentUpdateDTO)) {
			enrollmentUpdateDTO.setLoggedInUserEmail(user.getEmail());
			entollmentJson = gson.toJson(enrollmentUpdateDTO);
			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_UPDATE, user.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, entollmentJson);
			if (res != null) {
				enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
			}
			//}
			GiAuditParameterUtil.add(new GiAuditParameter("Enrollment ID", enrollmentUpdateDTO.getId()), new GiAuditParameter("Household ID", enrollmentUpdateDTO.getHouseholdId()), new GiAuditParameter("User Name", user.getUserName()), new GiAuditParameter("User Role", user.getCurrentUserRole()), new GiAuditParameter("Date Updated", new TSDate()));
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
		}
		return enrollmentResponse;
	}

	@SuppressWarnings("unused")
	private boolean isValidEnrollmentUpdateDTO(EnrollmentUpdateDTO enrollmentUpdateDTO) {
		if (null != enrollmentUpdateDTO && null != enrollmentUpdateDTO.getAction()) {
			switch (enrollmentUpdateDTO.getAction()) {
			case ENROLLMENT_START_DATE_CHANGE:
				return isValidEnrollmentStartDateChange(enrollmentUpdateDTO);
			case MEMBER_START_DATE_CHANGE:
				return isValidMemberStartDateChange(enrollmentUpdateDTO);
			case ENROLLMENT_END_DATE_CHANGE:
				return isValidEnrollmentEndDateChange(enrollmentUpdateDTO);
			case MEMBER_END_DATE_CHANGE:
				return isValidMemberEndDateChange(enrollmentUpdateDTO);
			case MEMBER_DATA_CHANGE:
				return isValidMemberDataChange(enrollmentUpdateDTO);
			default:
				return false;
			}
		}
		return false;
	}

	private boolean isValidMemberDataChange(EnrollmentUpdateDTO enrollmentUpdateDTO) {
		boolean valid = false;
		if (null != enrollmentUpdateDTO && 0 < enrollmentUpdateDTO.getId() && null != enrollmentUpdateDTO.getAction() && StringUtils.isNotEmpty(enrollmentUpdateDTO.getComment()) && (null != enrollmentUpdateDTO.getMembers() && enrollmentUpdateDTO.getMembers().size() > 0)) {
			for (EnrolleeDataUpdateDTO member : enrollmentUpdateDTO.getMembers()) {
				if (StringUtils.isNotEmpty(member.getExchgIndivIdentifier())) {
					valid = true;
				} else {
					valid = false;
					break;
				}
			}
		}
		return valid;

	}

	private boolean isValidMemberEndDateChange(EnrollmentUpdateDTO enrollmentUpdateDTO) {
		boolean valid = false;
		if (null != enrollmentUpdateDTO && 0 < enrollmentUpdateDTO.getId() && null != enrollmentUpdateDTO.getAction() && StringUtils.isNotEmpty(enrollmentUpdateDTO.getComment()) && (null != enrollmentUpdateDTO.getMembers() && enrollmentUpdateDTO.getMembers().size() > 0)) {
			for (EnrolleeDataUpdateDTO member : enrollmentUpdateDTO.getMembers()) {
				if (!StringUtils.isEmpty(member.getEffectiveEndDateStr()) && !StringUtils.isEmpty(member.getExchgIndivIdentifier())) {
					valid = true;
				} else {
					valid = false;
					break;
				}
			}
		}
		return valid;
	}

	private boolean isValidEnrollmentEndDateChange(EnrollmentUpdateDTO enrollmentUpdateDTO) {
		boolean valid = false;
		if (null != enrollmentUpdateDTO && !StringUtils.isEmpty(enrollmentUpdateDTO.getBenefitEndDateStr()) && 0 < enrollmentUpdateDTO.getId() && null != enrollmentUpdateDTO.getAction() && StringUtils.isNotEmpty(enrollmentUpdateDTO.getComment())) {
			valid = true;
		}
		return valid;

	}

	private boolean isValidMemberStartDateChange(EnrollmentUpdateDTO enrollmentUpdateDTO) {
		boolean valid = false;
		if (null != enrollmentUpdateDTO && 0 < enrollmentUpdateDTO.getId() && null != enrollmentUpdateDTO.getAction() && StringUtils.isNotEmpty(enrollmentUpdateDTO.getComment()) && (null != enrollmentUpdateDTO.getMembers() && enrollmentUpdateDTO.getMembers().size() > 0)) {
			for (EnrolleeDataUpdateDTO member : enrollmentUpdateDTO.getMembers()) {
				if (!StringUtils.isEmpty(member.getEffectiveStartDateStr()) && !StringUtils.isEmpty(member.getExchgIndivIdentifier())) {
					valid = true;
				} else {
					valid = false;
					break;
				}
			}
		}
		return valid;

	}

	private boolean isValidEnrollmentStartDateChange(EnrollmentUpdateDTO enrollmentUpdateDTO) {
		boolean valid = false;
		if (null != enrollmentUpdateDTO && !StringUtils.isEmpty(enrollmentUpdateDTO.getBenefitEffectiveDateStr()) && 0 < enrollmentUpdateDTO.getId() && null != enrollmentUpdateDTO.getAction() && StringUtils.isNotEmpty(enrollmentUpdateDTO.getComment())) {
			valid = true;
		}
		return valid;
	}

	@ApiOperation(value = "Get Consumer Password Reset Questions", notes = "This method takes Consumer Id as an input parameter and returns Password Reset Questions of Consumer.")
	@RequestMapping(value = "/crm/consumer/pwresetquestions/{encHouseholdId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'CAP_RESET_INDIVIDUAL_PASSWORD')")
	@GiAudit(transactionName = "CSR Clicked Reset Password", eventType = EventTypeEnum.CLICK_RESET_PASSWORD, eventName = EventNameEnum.PII_READ)
	public String getPasswordResetQuestions(Model model, @PathVariable("encHouseholdId") String encHouseholdId, HttpServletRequest request) {

		LOGGER.info("consumer - getPasswordResetQuestions : START");
		String jsonResponse = null;
		try {
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Password Reset Questions");
			String StrEncHouseholdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			model.addAttribute(HOUSEHOLD_ID, StrEncHouseholdId);
			Integer id = Integer.parseInt(StrEncHouseholdId);
			Household household = consumerPortalUtil.getHouseholdRecord(id);
			GiAuditParameterUtil.add(new GiAuditParameter("target_user", household.getUser().getUsername()));
			model.addAttribute(HOUSEHOLD, household);

			Map<String, Object> responseMap = consumerResetPasswordUtil.fetchResetPasswordQuestions(household);
			request.getSession().setAttribute("CONSUMER_RESET_PASSWORD_QA", responseMap.get(ConsumerResetPasswordUtil.QUESTIONSANSWERS));
			responseMap.remove(ConsumerResetPasswordUtil.QUESTIONSANSWERS);

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForHashMap(String.class, Object.class);
			jsonResponse = writer.writeValueAsString(responseMap);
			model.addAttribute("responseJson", jsonResponse);
			capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
			setViewMemberLink(model);
		} catch (Exception exception) {
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Failed to get Identity Proofing Questions"));
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in consumer - getPasswordResetQuestions: ", exception);
		}

		LOGGER.info("consumer - getPasswordResetQuestions : END");
		return "crm/consumer/pwresetquestions";
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Validate Consumer Password Reset Questions", notes = "This method takes Consumer Id as an input parameter and validates Password Reset Questions of Consumer.")
	@RequestMapping(value = "crm/consumer/validatepwdresetquestions/{encHouseholdId}", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'CAP_RESET_INDIVIDUAL_PASSWORD')")
	@GiAudit(transactionName = "CSR Filled in Authentication Information", eventType = EventTypeEnum.AUTHENTICATION_INFORMATION_FILLED, eventName = EventNameEnum.PII_WRITE)
	@ResponseBody
	public String validatePasswordResetQuestions(Model model, HttpServletRequest request, @PathVariable("encHouseholdId") String encHouseholdId, @RequestBody Map<String, Object> requestParams) {
		LOGGER.info("consumer - validatePasswordResetQuestions : START");
		String jsonResponse = null;
		try {
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Password Reset Questions");
			String StrEncHouseholdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			Integer id = Integer.parseInt(StrEncHouseholdId);
			Household household = consumerPortalUtil.getHouseholdRecord(id);
			model.addAttribute(HOUSEHOLD, household);

			Map<String, String> consumerResetPasswordQA = new HashMap<String, String>();
			if (request.getSession().getAttribute("CONSUMER_RESET_PASSWORD_QA") != null) {
				consumerResetPasswordQA = (Map<String, String>) (request.getSession().getAttribute("CONSUMER_RESET_PASSWORD_QA"));
			}
			Map<String, Object> responseMap = consumerResetPasswordUtil.validateResetPasswordQuestions(household, requestParams, consumerResetPasswordQA);
			GiAuditParameterUtil.add(new GiAuditParameter("target_user", household.getUser().getUsername()));
			if (responseMap != null && responseMap.get("STATUS") != null) {
				GiAuditParameterUtil.add(new GiAuditParameter("IdentityProofing", responseMap.get("STATUS").equals(ConsumerResetPasswordUtil.OK) ? "SUCCESSFUL" : "FAILED"));
				if (responseMap.get("STATUS").equals(ConsumerResetPasswordUtil.ACCOUNT_LOCKED)) {
					GiAuditParameterUtil.add(new GiAuditParameter("MaxRetriesAttempted", "True"));
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Max Retry Reached"));
				}
			}

			//HIX-102117: if override email is allowed do not set NULL question map in session. 
			if (responseMap != null && (responseMap.get("OVERRIDE_EMAIL") == null || Boolean.FALSE.equals((Boolean) responseMap.get("OVERRIDE_EMAIL")))) {
				request.getSession().setAttribute("CONSUMER_RESET_PASSWORD_QA", responseMap.get(ConsumerResetPasswordUtil.QUESTIONSANSWERS));
			}

			responseMap.remove(ConsumerResetPasswordUtil.QUESTIONSANSWERS);

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForHashMap(String.class, Object.class);
			jsonResponse = writer.writeValueAsString(responseMap);

		} catch (Exception exception) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in consumer - getComments: ", exception);
		}

		LOGGER.info("consumer - getPasswordResetQuestions : END");
		return jsonResponse;
	}

	@ApiOperation(value = "Validate Consumer Password Reset Questions", notes = "This method takes Consumer Id as an input parameter and validates Password Reset Questions of Consumer.")
	@RequestMapping(value = "crm/consumer/sendresetpasswordemail/{encHouseholdId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#model, 'CAP_RESET_INDIVIDUAL_PASSWORD_OVERRIDE')")
	@ResponseBody
	@GiAudit(transactionName = "Password Reset Email Send", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_WRITE)
	public String sendResetPasswordEmail(Model model, HttpServletRequest request, @PathVariable("encHouseholdId") String encHouseholdId, @RequestBody Map<String, Object> requestParams) {
		LOGGER.info("consumer - sendResetPasswordEmail : START");
		String jsonResponse = null;
		try {
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Password Reset Questions");
			String StrEncHouseholdId = (StringUtils.isNumeric(encHouseholdId)) ? encHouseholdId : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdId);
			Integer id = Integer.parseInt(StrEncHouseholdId);
			Household household = consumerPortalUtil.getHouseholdRecord(id);

			model.addAttribute(HOUSEHOLD, household);
			GiAuditParameterUtil.add(new GiAuditParameter("target_user", household.getUser().getUsername()));
			Map<String, Object> responseMap = consumerResetPasswordUtil.handleResetPasswordToAlternateEmail(household, requestParams);
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForHashMap(String.class, Object.class);
			jsonResponse = writer.writeValueAsString(responseMap);

		} catch (Exception exception) {
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error Sending Password Reset Email"));
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in consumer - getComments: ", exception);
		}

		LOGGER.info("consumer - sendResetPasswordEmail : END");
		return jsonResponse;
	}

	private void clearSessionVariable(HttpSession session) {
		session.removeAttribute(("CONSUMER_RESET_PASSWORD_QA"));
	}

	/**
	 * To Invoke CancelEnrollment API, and mark Active enrollment to CANCEL
	 * @param entollmentJson
	 * @param response
	 * @return
	 */
	@GiAudit(transactionName = "Cancel Active Enrollment", eventType = EventTypeEnum.MEMBER_ENROLLMENT_UPDATE, eventName = EventNameEnum.EDIT_ENROLLMENT)
	@RequestMapping(value = "crm/member/cancelenrollment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public EnrollmentResponse cancelEnrollment(@RequestBody String entollmentJson, HttpServletResponse response) {
		LOGGER.info("================== In getAppEventComments ================");
		EnrollmentResponse enrollmentResponse = null;
		Gson gson = new Gson();
		try {
			AccountUser user = userService.getLoggedInUser();
			EnrollmentUpdateDTO enrollmentUpdateDTO = gson.fromJson(entollmentJson, EnrollmentUpdateDTO.class);
			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_CANCEL, user.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, entollmentJson);
			if (res != null) {
				enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
			}
			GiAuditParameterUtil.add(new GiAuditParameter("Enrollment ID", enrollmentUpdateDTO.getId()), new GiAuditParameter("Household ID", enrollmentUpdateDTO.getHouseholdId()), new GiAuditParameter("User Name", user.getUserName()), new GiAuditParameter("User Role", user.getCurrentUserRole()), new GiAuditParameter("Date Updated", new TSDate()));
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
		}
		return enrollmentResponse;
	}

	/**
	 * Send as add Enrollment request.
	 * @param entollmentJson
	 * @param response
	 * @return
	 */
	@GiAudit(transactionName = "Send As Add Enrollment Request", eventType = EventTypeEnum.MEMBER_ENROLLMENT_UPDATE, eventName = EventNameEnum.EDIT_ENROLLMENT)
	@RequestMapping(value = "crm/member/sendasaddenrollment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public EnrollmentResponse sendAsAddEnrollmentRequest(@RequestBody String entollmentJson, HttpServletResponse response) {
		LOGGER.info("================== In getAppEventComments ================");
		EnrollmentResponse enrollmentResponse = null;
		Gson gson = new Gson();
		try {
			AccountUser user = userService.getLoggedInUser();
			EnrollmentUpdateDTO enrollmentUpdateDTO = gson.fromJson(entollmentJson, EnrollmentUpdateDTO.class);
			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.ENROLLMENT_UPDATE_ADD_TXN, user.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, entollmentJson);
			if (res != null) {
				enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
			}
			GiAuditParameterUtil.add(new GiAuditParameter("Enrollment ID", enrollmentUpdateDTO.getId()), new GiAuditParameter("Household ID", enrollmentUpdateDTO.getHouseholdId()), new GiAuditParameter("User Name", user.getUserName()), new GiAuditParameter("User Role", user.getCurrentUserRole()), new GiAuditParameter("Date Updated", new TSDate()));
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
		}
		return enrollmentResponse;
	}

	@ApiOperation(value = "View Enrollment and Enrollee Details", notes = "This method is to view Enrollment and Enrollee details for the household")
	@RequestMapping(value = "crm/member/getenrollmentandenrolleedetailsbyenrollmentid", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	@ResponseBody
	public String getEnrollmentAndEnrolleeDetailsByEnrollmentId(@RequestParam String requestEnrollmentId, HttpServletResponse response) throws InvalidUserException {

		EnrollmentResponse enrollmentResponse = null;
		response.setContentType(CONTENT_TYPE);
		Gson gson = new Gson();
		AccountUser user = null;

		try {
			Integer enrollmentId = Integer.parseInt(StringUtils.isNumeric(requestEnrollmentId) ? requestEnrollmentId : ghixJasyptEncrytorUtil.decryptStringByJasypt(requestEnrollmentId));
			user = userService.getLoggedInUser();
			List<Integer> enrollmentIdList = new ArrayList<Integer>();
			enrollmentIdList.add(enrollmentId);
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setIdList(enrollmentIdList);
			enrollmentRequest.setPremiumDataRequired(true);

			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, gson.toJson(enrollmentRequest));
			if (res != null) {
				enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
			GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), Exception.class.getName(), e.getStackTrace().toString(), user);
			GIErrorCode errorCodeObject = gobj.getErrorCode();
			return errorCodeObject.getMessage();
		}
		return gson.toJson(enrollmentResponse);
	}

	/**
	 * 
	 * @param enrollmentId
	 * @return
	 */
	private List<EnrollmentDataDTO> fetchEnrollmentDetails(Integer enrollmentId) {
		List<EnrollmentDataDTO> enrollmentDataDtoList = new ArrayList<>();
		Gson gson = new Gson();
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			List<Integer> enrollmentIdList = new ArrayList<Integer>();
			enrollmentIdList.add(enrollmentId);
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setIdList(enrollmentIdList);
			enrollmentRequest.setPremiumDataRequired(true);
			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, gson.toJson(enrollmentRequest));
			if (res != null) {
				EnrollmentResponse enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
				if (enrollmentResponse.getEnrollmentDataDtoListMap() != null && !enrollmentResponse.getEnrollmentDataDtoListMap().isEmpty()) {
					for (String householdCaseIdStr : enrollmentResponse.getEnrollmentDataDtoListMap().keySet()) {
						enrollmentDataDtoList = enrollmentResponse.getEnrollmentDataDtoListMap().get(householdCaseIdStr);
						break;
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
			giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), Exception.class.getName(), e.getStackTrace().toString(), user);
		}
		return enrollmentDataDtoList;
	}

	@ApiOperation(value = "External URL For Loading View Member", notes = "This method is to view member.")
	@RequestMapping(value = "external/member/view", method = { RequestMethod.GET })
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String viewExternalMember(Model model, HttpServletRequest request) {
		int householdId;
		String encHouseholdid = StringUtils.EMPTY;
		try {
			if (null != request.getSession().getAttribute("householdId")) {
				householdId = (int) request.getSession().getAttribute("householdId");
				encHouseholdid = ghixJasyptEncrytorUtil.encryptStringByJasypt(new Integer(householdId).toString());
			} else {
				model.addAttribute("errorMessage", "No Household Information Found. Please select appropriate Household to view the information.");
				return "member/view/errorPage";
			}
			capRequestValidatorUtil.validateViewMember(encHouseholdid);
			String StrEncTicketId = (StringUtils.isNumeric(encHouseholdid)) ? encHouseholdid : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdid);
			model.addAttribute(HOUSEHOLD_ID, StrEncTicketId);
			Integer id = Integer.parseInt(StrEncTicketId);
			Household household = consumerPortalUtil.getHouseholdRecord(id);
			String accessCode = getApplicantAccessCode(id);
			model.addAttribute("accessCode", accessCode);
			model.addAttribute(HOUSEHOLD, household);
			if (StringUtils.isNotEmpty(household.getSsn()) && household.getSsn().trim().length() == 9) {
				String consumerssn = "*****" + household.getSsn().substring(5, 9);
				model.addAttribute("consumerssn", consumerssn);
			}
			model.addAttribute(HOUSEHOLD, household);

			LOGGER.info("accessCode = " + accessCode);
			if (null != this.capPortalConfigurationUtil) {
				LOGGER.info("setting permmssions for  " + household);
				capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
				capPortalConfigurationUtil.setMemberInfoPermissions(model, request);
				setViewMemberLink(model);
			} else {
				LOGGER.info("pemrission setting is not possible because capPortalConfigurationUtil is null");
			}
			this.clearSessionVariable(request.getSession());
			setViewMemberLink(model);
		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in external viewMember()", ex);
		}
		LOGGER.info("external viewMember Summary -- end ");
		return "crm/member/viewmember";

	}

	@ApiOperation(value = "View Enrollment history Details", notes = "This method is to view Enrollment history details for the household")
	@RequestMapping(value = "crm/member/enrollmentdetails", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	@ResponseBody
	public String getEnrollmentHistory(@RequestParam String enrollmentId, HttpServletResponse response) throws InvalidUserException {
		EnrollmentResponse enrollmentResponse = null;
		response.setContentType(CONTENT_TYPE);
		Gson gson = new Gson();
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setEnrollmentId(Integer.valueOf(enrollmentId));
			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_SUBSCRIBER_EVENTS, enrollmentId, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, gson.toJson(enrollmentRequest));
			if (res != null) {
				enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
				getISOFormattedDate(enrollmentResponse);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
			GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), Exception.class.getName(), e.getStackTrace().toString(), user);
			GIErrorCode errorCodeObject = gobj.getErrorCode();
			return errorCodeObject.getMessage();
		}

		return gson.toJson(enrollmentResponse);
	}

	@ApiOperation(value = "View Enrollment Snapshot", notes = "This method is to view Enrollment snapshot details for the household")
	@RequestMapping(value = "crm/member/enrollmentsnapshot", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	@ResponseBody
	public String getEnrollmentSnapshot(@RequestParam String enrollmentId, @RequestParam String subscriberEventId, HttpServletResponse response) throws InvalidUserException {
		EnrollmentResponse enrollmentResponse = null;
		response.setContentType(CONTENT_TYPE);
		Gson gson = new Gson();
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setEnrollmentId(Integer.valueOf(enrollmentId));
			enrollmentRequest.setSubscriberEventId(Integer.valueOf(subscriberEventId));
			ResponseEntity<String> res = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_SNAPSHOT, enrollmentId, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, gson.toJson(enrollmentRequest));
			if (res != null) {
				enrollmentResponse = gson.fromJson(res.getBody(), EnrollmentResponse.class);
				getISOFormattedDate(enrollmentResponse);
			}
		} catch (Exception e) {
			LOGGER.error("Unable to get the valid data: ", e);
			GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(null, new TSDate(), Exception.class.getName(), e.getStackTrace().toString(), user);
			GIErrorCode errorCodeObject = gobj.getErrorCode();
			return errorCodeObject.getMessage();
		}

		return gson.toJson(enrollmentResponse);
	}

	//HIX-112219 : Coverting "09/12/2018 12:00:00.0" to ISO format date
	private void getISOFormattedDate(EnrollmentResponse enrollmentResponse) {
		try {
			if (enrollmentResponse != null) {
				List<EnrollmentEventHistoryDTO> enrollmentEventHistoryDTOList = enrollmentResponse.getEnrollmentEventHistoryDTOList();
				if (enrollmentEventHistoryDTOList != null) {
					List<EnrollmentEventHistoryDTO> newList = new ArrayList<>();
					for (EnrollmentEventHistoryDTO dto : enrollmentEventHistoryDTOList) {
						dto.setEventCreationDate(convertToISODate(dto.getEventCreationDate()));
						newList.add(dto);
					}

					enrollmentResponse.setEnrollmentEventHistoryDTOList(newList);
				}

				List<EnrollmentDataDTO> enrollmentDataDTOList = enrollmentResponse.getEnrollmentDataDTOList();
				if (enrollmentDataDTOList != null) {
					List<EnrollmentDataDTO> newEnrollmentDataDTOList = new ArrayList<>();
					List<EnrolleeDataDTO> newEnrolleeDataDtoList = new ArrayList<>();
					for (EnrollmentDataDTO enrollmentDataDTO : enrollmentDataDTOList) {
						List<EnrolleeDataDTO> enrolleeDataDtoList = enrollmentDataDTO.getEnrolleeDataDtoList();
						if (enrolleeDataDtoList != null) {
							for (EnrolleeDataDTO dto : enrolleeDataDtoList) {
								dto.setEffectiveStartDate(convertToISODate(dto.getEffectiveStartDate()));
								dto.setEffectiveEndDate(convertToISODate(dto.getEffectiveEndDate()));
								newEnrolleeDataDtoList.add(dto);
							}
							enrollmentDataDTO.setEnrolleeDataDtoList(newEnrolleeDataDtoList);
						}
						newEnrollmentDataDTOList.add(enrollmentDataDTO);
					}
					enrollmentResponse.setEnrollmentDataDTOList(newEnrollmentDataDTOList);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in getISOFormattedDate() : ", e);
		}
	}

	private String convertToISODate(String date) {
		String isoDate = "";
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			String currentDatePattern = "MM/dd/yyyyHH:mm:ss.SSS";
			isoDate = df.format(DateUtils.parseDate(date, currentDatePattern));
		} catch (Exception e) {
			LOGGER.error("Error while coverting date to ISO format", e);
		}

		return isoDate;
	}

	@RequestMapping(value = "crm/updateEnrollmentStatus", method = RequestMethod.POST)
	@GiAudit(transactionName = "CSR over ride to update enrollment status", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
	@ResponseBody
	@PreAuthorize("hasPermission(#model, 'MYAPP_ENRLMNT_OVRRD_ENRL_TO_CONFIRM')")
	public String updateEnrollmentStatusDetails(HttpServletRequest request, @RequestBody EnrollmentStatusUpdate enrollmentStatusUpdate) {

		LOGGER.info("Update the enrollment status");
		String response = IndividualPortalConstants.FAILURE;
		if (null != enrollmentStatusUpdate) {
			if (enrollmentStatusUpdate.getLastPremiumPaidDate() != null) {
				try {
					DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					formatter.parse(enrollmentStatusUpdate.getLastPremiumPaidDate());
				} catch (ParseException e) {
					LOGGER.debug("Invalid Last Premium Paid Date : " + enrollmentStatusUpdate.getLastPremiumPaidDate());
					return IndividualPortalConstants.FAILURE;
				}
			}

			if (StringUtils.isNotBlank(enrollmentStatusUpdate.getOverrideCommentText())) {
				response = csrOverrideUtility.invokeEnrollmentStatusUpdate(enrollmentStatusUpdate);
				try {
					capPortalConfigurationUtil.setActiveModuleId(ghixJasyptEncrytorUtil.decryptStringByJasypt(enrollmentStatusUpdate.getHouseHoldId()));
				} catch (InvalidUserException e) {
					LOGGER.debug("Invalid User Exception");
					return IndividualPortalConstants.FAILURE;
				}
				request.getSession().setAttribute(IndividualPortalConstants.OVERRIDE_COMMENT, enrollmentStatusUpdate.getOverrideCommentText());
				individualPortalUtility.logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_ENROLLMENT_STATUS_UPDATE, null, request);
			}
		}
		return response;
	}

	/**
	 * @param request
	 * @param caseNumber
	 * @param enrollmentId
	 * @param planType
	 * @return responseVal
	 */
	@RequestMapping(value = "/crm/reinstateenrollment", method = RequestMethod.POST)
	@GiAudit(transactionName = "Re-instate cancelled/terminated enrollments", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_WRITE)
	@PreAuthorize("hasPermission(#model, 'MYAPP_REINST_ENROLLMENT')")
	@ResponseBody
	public String reinstate(HttpServletRequest request, @RequestParam(value = "caseNumber", required = true) String caseNumber, @RequestParam(value = "enrollmentId", required = true) String enrollmentId, @RequestParam(value = "planType", required = true) String planType, @RequestParam(value = "overrideCommentText", required = true) String overrideCommentText, @RequestParam(value = "houseHoldId", required = true) String houseHoldId) {

		ReinstateEnrollmentDTO reinstateEnrollmentDTO = new ReinstateEnrollmentDTO();
		reinstateEnrollmentDTO.setCaseNumber(caseNumber);
		if (planType != null && planType.trim().length() > 0) {
			if (planType.equalsIgnoreCase("Health")) {
				reinstateEnrollmentDTO.setReinstateHealth(true);
				reinstateEnrollmentDTO.setHealthEnrollmentId(enrollmentId);
			} else if (planType.equalsIgnoreCase("Dental")) {
				reinstateEnrollmentDTO.setReinstateDental(true);
				reinstateEnrollmentDTO.setDentalEnrollmentId(enrollmentId);
			}
		}

		String responseVal = IndividualPortalConstants.FAILURE;
		AccountUser user = null;
		Long ssapOrEnrollmentId = null;
		try {
			user = userService.getLoggedInUser();

			boolean reinstateBothDentalAndHealth = false;
			if (null != reinstateEnrollmentDTO.getReinstateHealth() && reinstateEnrollmentDTO.getReinstateHealth() && null != reinstateEnrollmentDTO.getReinstateDental() && reinstateEnrollmentDTO.getReinstateDental()) {
				reinstateBothDentalAndHealth = true;
				ssapOrEnrollmentId = Long.valueOf(individualPortalUtility.getIdFromCaseNumber(caseNumber).toString());
			} else {
				if (enrollmentId != null) {
					ssapOrEnrollmentId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(enrollmentId));
				}
			}
			if (ssapOrEnrollmentId != null) {
				responseVal = csrOverrideUtility.reinstateEnrollment(ssapOrEnrollmentId, reinstateBothDentalAndHealth, user.getUserName());

				Map<String, String> logEventParams = new HashMap<String, String>();
				logEventParams.put(IndividualPortalConstants.OVERRIDE_COMMENT, overrideCommentText);
				logEventParams.put(IndividualPortalConstants.CMR_HOUSEHOLD_ID, ghixJasyptEncrytorUtil.decryptStringByJasypt(houseHoldId));

				capPortalConfigurationUtil.logAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_REINSTATE_ENROLLMENTS, logEventParams, request);

				//HIX-69290 :: call to update Dental APTC amount
				if (!"CA".equalsIgnoreCase(stateCode) && StringUtils.equalsIgnoreCase(responseVal, IndividualPortalConstants.SUCCESS) && null != reinstateEnrollmentDTO.getReinstateHealth() && reinstateEnrollmentDTO.getReinstateHealth()) {
					individualPortalUtility.updateAptcOnReinstate(reinstateEnrollmentDTO, user, caseNumber);
				}
			}

		} catch (InvalidUserException e) {
			LOGGER.error("Unable to get logged in user during reinstating enrollment", e);
		} catch (Exception gir) {
			LOGGER.error("Reinstatement is not successful. ", gir);
		}
		return responseVal;
	}

	/**
	 * @param model
	 * @param request
	 * @param redirectAttributes
	 * @param caseNumber
	 * @return URL
	 */
	@GiAudit(transactionName = "Goto SSAP", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.SSAP_APP)
	@RequestMapping(value = "/cap/gotossap", method = RequestMethod.GET)
	public String goToSsap(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		//HIX-57896
		String caseNumberFromRequest = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
		if (caseNumberFromRequest != null && caseNumberFromRequest.trim().length() > 0) {
			redirectAttributes.addFlashAttribute(IndividualPortalConstants.CASE_NUMBER, caseNumberFromRequest.trim());
		}
		return "redirect:/ssap";
	}

	@RequestMapping(value = { "/cap/applicant/events/{caseNumber}/{gated}/{cmrHouseholdId}" }, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	@ResponseBody
	public String getEventDataForApplication(@PathVariable("caseNumber") String caseNumber, @PathVariable("gated") Optional<String> gated, @PathVariable("cmrHouseholdId") Optional<String> cmrHouseholdId, HttpServletResponse response) {
		String result = "500";
		String eventType = null;
		try {
			if (gated.isPresent()) {
				if (!getValidGatedValues().contains(gated.get())) {
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return null;
				}
				eventType = gated.get();
			} else {
				eventType = "y";
			}

			String cmrHouseholdIdPlainText = ghixJasyptEncrytorUtil.decryptStringByJasypt(cmrHouseholdId.orElse(null));
			result = ghixRestTemplate.getForObject(GhixEndPoints.ELIGIBILITY_URL + "ssap/retrieveEventsByApplication/" + cmrHouseholdIdPlainText + "/" + caseNumber + "/" + eventType, String.class);
		} catch (Exception ex) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			LOGGER.error("Exception occured while fetching event details: " + caseNumber, ex);
		}
		return result;
	}

	private Set<String> getValidGatedValues() {
		Set<String> validGatedValues = new HashSet<String>();
		validGatedValues.add("y");
		validGatedValues.add("n");
		validGatedValues.add("all");
		return validGatedValues;
	}

	@ResponseBody
	//	@PreAuthorize(IndividualPortalConstants.PORTAL_OVERRIDE_SEP_DETAILS_PERMISSION)
	@RequestMapping(value = "/cap/getEligibilityDateDetails", method = RequestMethod.GET)
	@GiAudit(transactionName = "Fetch Eligibility Start date details for CSR Override", eventType = EventTypeEnum.INDIVIDUAL_PORTAL, eventName = EventNameEnum.PII_READ)
	public IndPortalEligibilityDateDetails getEligibilityStartDateDetails(HttpServletRequest request) {

		LOGGER.debug("Calling cap API to get existing Enrollment and APTC Effective date details for application");

		IndPortalEligibilityDateDetails indPortalEligibilityDateDetails = null;
		try {

			String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
			String encHouseholdid = request.getParameter("encHouseholdid");
			Integer cmrHouseholdid = Integer.parseInt((StringUtils.isNumeric(encHouseholdid)) ? encHouseholdid : ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseholdid));
			if (StringUtils.isNotBlank(caseNumber)) {

				int year = TSCalendar.getInstance().get(Calendar.YEAR);
				String coverageYear = request.getParameter(IndividualPortalConstants.COVERAGE_YEAR);
				if (StringUtils.isNotBlank(coverageYear) && StringUtils.isNumeric(coverageYear)) {
					year = Integer.parseInt(coverageYear);
				}

				indPortalEligibilityDateDetails = capPortalConfigurationUtil.fetchEligibilityDetailsForAppln(caseNumber, year, cmrHouseholdid);
			}

		} catch (Exception e) {
			LOGGER.error("Error calling API to get existing Enrollment and APTC Effective date details for application", e);
		}
		return indPortalEligibilityDateDetails;
	}

	private void setViewMemberLink(Model model) {
		model.addAttribute(ADD_NEW_VIEW_CASE_URL, VIEW_CASE_URL);
	}

	private void setSpecialEnrollmentInfoURL(Model model) {
		model.addAttribute(ADD_SPECIAL_ENROLLMENT_INFO_URL, ENROLLMENT_INFO_URL);
	}

	/**
	 * @param model
	 * @param request
	 * @param reinstateEnrollmentDTO
	 * @return responseVal
	 */
	@RequestMapping(value = "/cap/sendobat", method = RequestMethod.POST)
	@GiAudit(transactionName = "CSR over ride to send outbound account transfer", eventType = EventTypeEnum.CAP_APPLICATION_PAGE, eventName = EventNameEnum.PII_WRITE)
	@PreAuthorize("hasPermission(#model, 'MYAPP_SEND_OUTBOUND_AT')")
	@ResponseBody
	public String sendOutboundAt(Model model, HttpServletRequest request) {
		String restResponse = IndividualPortalConstants.FAILURE;
		try {
			String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
			long applicationId = Long.parseLong(individualPortalUtility.getIdFromCaseNumber(caseNumber).toString());
			restResponse = ghixRestTemplate.getForObject(GhixEndPoints.EligibilityEndPoints.GENERATE_PUSH_AT + "?applicationId=" + applicationId, String.class);
			AccountTransferResponsePayloadType atresponse = null;
			JAXBContext jaxbContext = JAXBContext.newInstance(AccountTransferResponsePayloadType.class);
			StringReader stringReaderAtResponse = new StringReader(restResponse);

			atresponse = (AccountTransferResponsePayloadType) jaxbContext.createUnmarshaller().unmarshal(stringReaderAtResponse);

			if ("Success".equalsIgnoreCase(atresponse.getResponseMetadata().getResponseDescriptionText().getValue())) {
				return "SUCCESS";
			}

			individualPortalUtility.logIndividualAppEvent(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES, IndividualPortalConstants.CSR_OVERRIDES_SEND_OUTBOUND_AT, null, request);
		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Send Outbound Account Transfer is not successful. ", ex);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, APPLICATION_ERROR));
		}
		return IndividualPortalConstants.FAILURE;
	}

	@RequestMapping(value = "/cap/getOutBoundButtonStatus", method = RequestMethod.GET)
	@ResponseBody
	public String getOutBoundButtonStatus(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
		String restResponse = IndividualPortalConstants.FAILURE;
		if (caseNumber != null && caseNumber.trim().length() > 0) {
			long applicationId = Long.parseLong(individualPortalUtility.getIdFromCaseNumber(caseNumber).toString());
			if (applicationId > 0) {
				try {
					restResponse = ghixRestTemplate.getForObject(GhixEndPoints.EligibilityEndPoints.IS_MEDICAID_CHIP_ELIGIBLE_MEMBER + "?applicationId=" + applicationId, String.class);
				} catch (Exception ex) {
					LOGGER.error("Exception occured while invoking outbound AT for application id  - " + applicationId, ex);
				}
			} else {
				LOGGER.error("Application id is not valid : " + applicationId);
			}
		}
		return restResponse;
	}

	/**
	 * @param model
	 * @param request
	 * @param reinstateEnrollmentDTO
	 * @return responseVal
	 */
	@RequestMapping(value = "/cap/getProgrmeEligibility", method = RequestMethod.GET)
	@GiAudit(transactionName = "CSR over ride to send outbound account transfer", eventType = EventTypeEnum.CAP_APPLICATION_PAGE, eventName = EventNameEnum.PII_WRITE)
	@PreAuthorize("hasPermission(#model, 'MYAPP_OVERRIDE_PROG_ELIG')")
	@ResponseBody
	public ProgramEligibilityDTO getProgrmeEligibility(Model model, HttpServletRequest request) {
		ProgramEligibilityDTO application = null;
		try {
			String caseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
			SsapApplicationResource resource = ssapApplicationAccessor.getSsapApplicationsByCaseNumberId(caseNumber);
			application = populateToApplication(resource);
			application = applicationService.getApplication(application);
		} catch (Exception ex) {
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Could not get program eligibility details. ", ex);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, APPLICATION_ERROR));
		}
		return application;
	}

	private ProgramEligibilityDTO populateToApplication(SsapApplicationResource resource) {
		ProgramEligibilityDTO application = new ProgramEligibilityDTO();
		application.setSsapApplicationId(resource.getApplicationId());
		application.setMaximumAPTC(resource.getMaximumAPTC());
		application.setCsrLevel(resource.getCsrLevel());
		application.setEligibilityStatus(resource.getEligibilityStatus());
		application.setFinancialAssistanceFlag(resource.getFinancialAssistanceFlag());
		if (null != resource.getEligibilityStartDate())
			application.setEligibilityStartDate(new SimpleDateFormat("MM/dd/yy").format(resource.getEligibilityStartDate()));
		return application;
	}

	/**
	 * @param model
	 * @param request
	 * @param reinstateEnrollmentDTO
	 * @return responseVal
	 */
	@RequestMapping(value = "/cap/overrideProgramElig", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'MYAPP_OVERRIDE_PROG_ELIG')")
	@ResponseBody
	public String overrideProgramElig(Model model, HttpServletRequest request, @RequestBody ProgramEligibilityDTO programEligibilityDTO) {
		String response = "failure";
		try {
			String oldApplicationCaseNumber = request.getParameter(IndividualPortalConstants.CASE_NUMBER);
			String coverageYear = request.getParameter(IndividualPortalConstants.COVERAGE_YEAR);

			final Map<String, String> requestMap = new HashMap<String, String>();
			long applicationId = programEligibilityDTO.getSsapApplicationId();
			String newApplicationCaseNumber = null;

			//Step1 - Clone Application
			if (applicationId > 0) {
				response = ghixRestTemplate.getForObject(GhixEndPoints.ELIGIBILITY_URL + "/ssapapplication/cloneApplication?applicationId=" + applicationId, String.class, requestMap);
				if (response != null) {
					LOGGER.info("Creating object ");
					PortalResponse responseObj = platformGson.fromJson(response, PortalResponse.class);
					if (responseObj != null) {
						newApplicationCaseNumber = responseObj.getClonedAppCaseNumber();
						LOGGER.info("caseId to be returned " + newApplicationCaseNumber);
					}
				}
				LOGGER.info("response from eligibility cloning API : " + response);
			}

			response = applicationService.overrideProgramEligibility(programEligibilityDTO, newApplicationCaseNumber, oldApplicationCaseNumber, coverageYear, request);

		} catch (Exception ex) {
			response = "failure";
			model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Could not override program eligibility ", ex);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, APPLICATION_ERROR));
		}
		return response;
	}
}
