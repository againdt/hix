package com.getinsured.hix.batch.ssap.integration.task;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapIntegrationLogRepository;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplication;

@DependsOn({"ghixPlatformEndPoints", "dynamicPropertiesUtil"})
public class RetrySsapIntegrationTask implements Tasklet {
	
	private static final Logger LOGGER = Logger.getLogger(RetrySsapIntegrationTask.class);

	private static final String ESIGN_DELAY_MINUTES = "iex.ssap.integration.retry.esign.delay.minutes";
	
	private static final String ESIGN_DAYS_INTERVAL = "iex.ssap.integration.esign.interval.days";

	private static final String VLP_SERVICE = "vlpService";

	private static final String SSAC_SERVICE = "ssacService";

	private static final String MAX_RETRY_ATTEMPTS = "iex.ssap.integration.retry.max.attempts";
	
    private final List<String> openApplicationStatuses = Arrays.asList(
            ApplicationStatus.SIGNED.getApplicationStatusCode(),
            ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),
            ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode(),
            ApplicationStatus.SUBMITTED.getApplicationStatusCode());
    
    private RestTemplate restTemplate;
    
	private SsapJsonBuilder ssapJsonBuilder;
    
	private SsapApplicationRepository ssapApplicationRepository;
	
	private SsapIntegrationLogRepository ssapIntegrationLogRepository;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		LOGGER.info("RetrySsapIntegrationTask started on " + new Timestamp(System.currentTimeMillis()));
		Integer esignDelayInMinutes = new Integer(DynamicPropertiesUtil.getPropertyValue(ESIGN_DELAY_MINUTES));
		Integer esignDaysInterval = new Integer(DynamicPropertiesUtil.getPropertyValue(ESIGN_DAYS_INTERVAL));
		
		DateTime startDate = DateTime.now().minusDays(esignDaysInterval);
		DateTime endDate = DateTime.now().minusMinutes(esignDelayInMinutes);
		try {
			List<SsapApplication> unverifiedNonFinancialApplications = ssapApplicationRepository.findApplicationsToRetry(startDate.toDate(), endDate.toDate(), openApplicationStatuses);
			for (SsapApplication application : unverifiedNonFinancialApplications) {
				if(!isMaxRetryCountReached(application)) {
					restTemplate.postForObject(GhixEndPoints.SsapIntegrationEndpoints.SSAP_INTEGRATION_URL, application.getId(), String.class);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in retry Ssap Integration task", e);
		}
		LOGGER.info("RetrySsapIntegrationTask finished on " + new Timestamp(System.currentTimeMillis()));
		return RepeatStatus.FINISHED;
	}
	
	private boolean isMaxRetryCountReached(SsapApplication ssapApplication) {
		boolean maxRetryCountReached;
		Integer ssapIntegrationRetryMaxAttempts = new Integer(DynamicPropertiesUtil.getPropertyValue(MAX_RETRY_ATTEMPTS));
		// Non-Financial Application Flow - get count of SSAC and VLP tries
		SingleStreamlinedApplication ssapJSON = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
		int vlpRequiredMembersCount = getNonCitizenMembers(ssapJSON);
		int ssacRequiredMembersCount = getMembersWithSSN(ssapJSON);
		Long currentVLPServiceAttempts = ssapIntegrationLogRepository.getRetryCountForService(ssapApplication.getId(), VLP_SERVICE);
		Long currentSSACServiceAttempts = ssapIntegrationLogRepository.getRetryCountForService(ssapApplication.getId(), SSAC_SERVICE);
		
		if((currentVLPServiceAttempts < (vlpRequiredMembersCount * ssapIntegrationRetryMaxAttempts)) || 
				(ssacRequiredMembersCount > 0 && currentSSACServiceAttempts < ssapIntegrationRetryMaxAttempts)) {
			LOGGER.debug("Retrying SSAC and VLP for ssap application Id: " + ssapApplication.getId());
			maxRetryCountReached = false;
		} else {
			LOGGER.info("Aborting retries as max retries for SSAC and VLP exceeded for ssap application Id: " + ssapApplication.getId());
			maxRetryCountReached = true;
		}
		return maxRetryCountReached;
	}

	private int getMembersWithSSN(SingleStreamlinedApplication ssapJSON) {
		int membersWithSSN = 0;
		List<HouseholdMember> applicants = ssapJSON.getTaxHousehold().get(0).getHouseholdMember();
		for (HouseholdMember applicant : applicants) {
			if(applicant.getApplyingForCoverageIndicator() && null != applicant.getSocialSecurityCard() && 
        		  StringUtils.isNotBlank(applicant.getSocialSecurityCard().getSocialSecurityNumber())) {
				membersWithSSN++;
			}
		}
		return membersWithSSN;	
	}

	private int getNonCitizenMembers(SingleStreamlinedApplication ssapJSON) {
		int nonCitizenCount = 0;
		List<HouseholdMember> applicants = ssapJSON.getTaxHousehold().get(0).getHouseholdMember();
		for (HouseholdMember applicant : applicants) {
			boolean citizenshipAttestedIndicator = applicant.getCitizenshipImmigrationStatus().getCitizenshipAsAttestedIndicator() != null ? applicant.getCitizenshipImmigrationStatus().getCitizenshipAsAttestedIndicator() : false;
			boolean citizenshipStatusIndicator = applicant.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null ? applicant.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() : false;

			if(applicant.getApplyingForCoverageIndicator() && 
                  applicant.getCitizenshipImmigrationStatus() != null &&
					(!citizenshipAttestedIndicator || !citizenshipStatusIndicator))
			{
				nonCitizenCount++;
			}
		}
		return nonCitizenCount;
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}
	
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}

	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}

	public SsapIntegrationLogRepository getSsapIntegrationLogRepository() {
		return ssapIntegrationLogRepository;
	}

	public void setSsapIntegrationLogRepository(
			SsapIntegrationLogRepository ssapIntegrationLogRepository) {
		this.ssapIntegrationLogRepository = ssapIntegrationLogRepository;
	}
}
