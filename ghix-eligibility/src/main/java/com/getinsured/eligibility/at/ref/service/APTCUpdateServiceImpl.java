package com.getinsured.eligibility.at.ref.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.eligibility.at.ref.dto.APTCUpdateRequest;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.hix.dto.enrollment.EnrollmentAptcUpdateDto;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.indportal.dto.Group;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;

/**
 * @author chopra_s
 * 
 */
@Component("aptcUpdateService")
@Scope("singleton")
public class APTCUpdateServiceImpl implements APTCUpdateService {
	private static final Logger LOGGER = LoggerFactory.getLogger(APTCUpdateServiceImpl.class);

	private static final String AT_APTC_AMOUNT_UPDATE = "AT_APTC_AMOUNT_UPDATE";

	@Autowired
	private IntegrationLogService integrationLogService;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private CoverageStartDateService coverageStartDateService;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;

	private String HEALTH = "Health";

	private String DENTAL = "Dental";
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired 
	private Gson platformGson;

	@Override
	public String process(APTCUpdateRequest request, AptcUpdate aptcUpdate) {
		LOGGER.info("Process APTCUpdateRequest for  request - " + request);
		String responseValue = GhixConstants.RESPONSE_FAILURE;
		String aptcUpdateJson = StringUtils.EMPTY;
		StringBuffer messageString = new StringBuffer(StringUtils.EMPTY);
		try {
			boolean convFlag = false;
			List<EnrollmentAptcUpdateDto> enrollmentAptcUpdateDtoList =	null;
			//set the isConvertedToNonFinancialFlag = true if IsNonFinancialConversion(Financial App->Non-Financial App) comes true from reffral 
			if(aptcUpdate!=null && null!= aptcUpdate.getIsNonFinancialConversion() && aptcUpdate.getIsNonFinancialConversion()) {
				convFlag = true;
			}
			List<EnrollmentShopDTO> healthList = aptcUpdate.getEnrollmentList();
			
			Map<String, BigDecimal> enrollmentMaxAptc = new HashMap <String, BigDecimal>();
			if(aptcUpdate!= null && aptcUpdate.getAppGroupResponse()!=null && !CollectionUtils.isEmpty(aptcUpdate.getAppGroupResponse().getGroupList())){
				for(Group group:aptcUpdate.getAppGroupResponse().getGroupList()) {
					if(StringUtils.isNotEmpty(group.getGroupEnrollmentId())) {
						enrollmentMaxAptc.put(group.getGroupEnrollmentId(), group.getMaxAptc());
					}
				}
			}
			List<String> skipEnrollmentId = new ArrayList<String>();
			if(healthList!=null) {
				for(EnrollmentShopDTO health : healthList) {
					Float newAptc = null;
					if(enrollmentMaxAptc.get(health.getEnrollmentId()+"")!=null){
						newAptc = enrollmentMaxAptc.get(health.getEnrollmentId()+"").floatValue();
					}
					if(isEqual(health.getAptcAmt(), newAptc)) {
						skipEnrollmentId.add(health.getEnrollmentId()+"");
					}
				}
			}
			
			if(StringUtils.isBlank(messageString.toString()) && aptcUpdate!= null && aptcUpdate.getAppGroupResponse()!=null && !CollectionUtils.isEmpty(aptcUpdate.getAppGroupResponse().getGroupList())){
				enrollmentAptcUpdateDtoList = new ArrayList<EnrollmentAptcUpdateDto>();
				for(Group group:aptcUpdate.getAppGroupResponse().getGroupList()) {
					if(StringUtils.isNotEmpty(group.getGroupEnrollmentId()) && !skipEnrollmentId.contains(group.getGroupEnrollmentId())) {
						Float groupAPTC = null;
						if(group.getMaxAptc()!=null) {
							groupAPTC = group.getMaxAptc().floatValue();
						}
						EnrollmentAptcUpdateDto enrollmentDto =  createEnrollAptcUpdDTO(
								group.getGroupEnrollmentId(), aptcUpdate.getAptcEffectiveDate(), groupAPTC, convFlag);
						enrollmentAptcUpdateDtoList.add(enrollmentDto);
					}
				}
			}
			if(enrollmentAptcUpdateDtoList != null 
					&& !enrollmentAptcUpdateDtoList.isEmpty()){
				//Call enrollment API to change elected APTC
				EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
				enrollmentRequest.setEnrollmentAptcUpdateDtoList(enrollmentAptcUpdateDtoList);
				ObjectMapper mapper = new ObjectMapper();
				aptcUpdateJson = mapper.writeValueAsString(aptcUpdate);
				String jsonResponse = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_APTC, 
						aptcUpdate.getUserName(), HttpMethod.POST, 
						MediaType.APPLICATION_JSON, 
						String.class, platformGson.toJson(enrollmentRequest))).getBody();
				EnrollmentResponse enrollmentResponse = platformGson.fromJson(jsonResponse, EnrollmentResponse.class);
				if (null != enrollmentResponse 
						&& StringUtils.equalsIgnoreCase(enrollmentResponse.getStatus(),GhixConstants.RESPONSE_SUCCESS)) {
					messageString.append("APTC update successful for aptcUpdateJson - " + aptcUpdateJson);
					if(!MapUtils.isEmpty(enrollmentResponse.getSkippedEnrollmentMap())) {
						messageString.append("Skipped Enrollments due to new financial effective date is after enrollment end date : ");
						for (Integer enrollmentId : enrollmentResponse.getSkippedEnrollmentMap().keySet()) {
							String skipReason = enrollmentResponse.getSkippedEnrollmentMap().get(enrollmentId);
							messageString.append("Enrollment Id: "+enrollmentId+" , SkipReason: "+skipReason+" ");
						}
					}
					responseValue = GhixConstants.RESPONSE_SUCCESS;
				}else {
					messageString.append("Unable to update APTC amount for aptcUpdateJson - " + aptcUpdateJson);
				}
			}else {
				messageString.append("No change in APTC amount, so no call made to enrollment for update");
				responseValue = GhixConstants.RESPONSE_SUCCESS;
			}
		}
		catch (Exception e) {
			messageString.append("Exception occurred while invoking APTC amount update for aptcUpdateJson - " + aptcUpdate + " - " + ExceptionUtils.getFullStackTrace(e));
		} finally {
			String logMessage = messageString.toString();
			logData(request.getCurrentApplicationId(), logMessage, responseValue);
		}
		
		return responseValue;
	}
	
	
	private boolean isEqual(Float f1, Float f2){
		boolean isEqual = false;
		
		if(f1 == null && f2 == null){
			isEqual = true;
		}else if(f1!=null && f2!=null && f1.floatValue()==f2.floatValue()){
			isEqual=true;
		}
		return isEqual;
	}
	
	/**
	 * Create EnrollmentAptcUpdateDto for given enrollment
	 * @param enrollmentId
	 * @param aptcEffectiveDate
	 * @param aptcAmt
	 * @param convFlag
	 * @return
	 */
	private EnrollmentAptcUpdateDto createEnrollAptcUpdDTO(String enrollmentId, Date aptcEffectiveDate, Float aptcAmt, boolean convFlag) {
		
		EnrollmentAptcUpdateDto plan = new EnrollmentAptcUpdateDto(); 
		
		plan.setEnrollmentId(Integer.valueOf(enrollmentId));
		plan.setAptcEffectiveDate(aptcEffectiveDate);
		plan.setConvertedToNonFinancialFlag(convFlag);
		if(convFlag) {
			aptcAmt=null;
		}
		plan.setAptcAmt(aptcAmt);
		 
		return plan;
	}

	@Override
	public AptcUpdate populateAptcUpdate(APTCUpdateRequest request) {
		LOGGER.info("Populate APTCUpdate request Data");
		final SsapApplication enrolledApplication = fetchEnrolledApp(request.getEnrolledApplicationId());
		final Map<String, Object> enrollmentDetails = ssapEnrolleeHandler.fetchEnrollmentID(request.getEnrolledApplicationId());
		LOGGER.info("Enrollment Details Received - " + enrollmentDetails);
		AptcUpdate aptcUpdate = null;
		if ((enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO) != null && enrollmentDetails.get(HEALTH) != null) || (enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO) != null && enrollmentDetails.get(DENTAL) != null)) {
			aptcUpdate = new AptcUpdate();
			aptcUpdate.setCaseNumber(enrolledApplication.getCaseNumber());
			aptcUpdate.setAptcAmt(convertAmountToFloat(request.getMaximumAPTC()));
			aptcUpdate.setAptcEffectiveDate(calculateDate(enrollmentDetails, request));
			aptcUpdate.setHealthEnrollmentId((String) enrollmentDetails.get(HEALTH));
			aptcUpdate.setDentalEnrollmentId((String) enrollmentDetails.get(DENTAL));
			if ((enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO) != null && enrollmentDetails.get(HEALTH) != null)) {
				aptcUpdate.setHealthEnrollmentEndDate(((EnrollmentShopDTO) enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO)).getCoverageEndDate());
			}
		}
		return aptcUpdate;
	}

	private Date calculateDate(Map<String, Object> enrollmentDetails, APTCUpdateRequest request) {
		Date coverageStartDate;

		try {
			Map<String, EnrollmentShopDTO> enrollments = new HashMap<String, EnrollmentShopDTO>();
			enrollments.put("health", (EnrollmentShopDTO) enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO));
			enrollments.put("dental", (EnrollmentShopDTO) enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO));
			coverageStartDate = coverageStartDateService.computeCoverageStartDate(BigDecimal.valueOf(request.getCurrentApplicationId()), enrollments, false, false);
			LOGGER.info("coverage StartDate for " + request.getCurrentApplicationId() + " - " + coverageStartDate);
		} catch (GIException e) {
			throw new GIRuntimeException("Error in calculating coverage start date");
		}

		return coverageStartDate;

	}

	private float convertAmountToFloat(BigDecimal maximumAPTC) {
		if (maximumAPTC == null) {
			return 0;
		} else {
			return maximumAPTC.floatValue();
		}
	}

	private SsapApplication fetchEnrolledApp(long id) {
		SsapApplication app = ssapApplicationRepository.findOne(id);
		if (app == null) {
			String errorMsg = "No Application found for Id - " + id;
			throw new GIRuntimeException(errorMsg);
		}
		return app;
	}

	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, AT_APTC_AMOUNT_UPDATE, status, ssapApplicationId, null);
	}
}
