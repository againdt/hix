package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.ssap.renewal.service.VerificationService;
import com.getinsured.hix.batch.ssap.renewal.util.VerificationPartitionerParams;

/**
 * Verification Reader class is used to get read SSAP Application ID from Partitioner.
 * 
 * @since August 12, 2019
 */
public class VerificationReader implements ItemReader<Long> {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationReader.class);
	List<Long> readSsapApplicationIdList;
	private int partition;
	private int loopCount;
	private int startIndex;
	private int endIndex;

	private VerificationService verificationService;
	private VerificationPartitionerParams verificationPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			partition = executionContext.getInt("partition");
			startIndex = executionContext.getInt("startIndex");
			endIndex = executionContext.getInt("endIndex");
			LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);

			if (null != verificationPartitionerParams && CollectionUtils.isNotEmpty(verificationPartitionerParams.getSsapApplicationIdList())) {

				if (startIndex < verificationPartitionerParams.getSsapApplicationIdList().size()
						&& endIndex <= verificationPartitionerParams.getSsapApplicationIdList().size()) {
					readSsapApplicationIdList = new CopyOnWriteArrayList<Long>(verificationPartitionerParams.getSsapApplicationIdList().subList(startIndex, endIndex));
				}
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("readSsapApplicationList is null: {}", CollectionUtils.isEmpty(readSsapApplicationIdList));
			}
		}
	}

	@Override
	public Long read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		boolean readerFlag = false;
		Long readerSsapApplicationID = null;

		if (CollectionUtils.isNotEmpty(readSsapApplicationIdList)) {

			if (loopCount < readSsapApplicationIdList.size()) {
				loopCount++;
				readerFlag = true;
				readerSsapApplicationID = readSsapApplicationIdList.get(loopCount - 1);
			}
		}

		if (!readerFlag) {
			LOGGER.warn("Unable to find SSAP Application.");
		}
		return readerSsapApplicationID;
	}

	public VerificationService getVerificationService() {
		return verificationService;
	}

	public void setVerificationService(VerificationService verificationService) {
		this.verificationService = verificationService;
	}

	public VerificationPartitionerParams getVerificationPartitionerParams() {
		return verificationPartitionerParams;
	}

	public void setVerificationPartitionerParams(VerificationPartitionerParams verificationPartitionerParams) {
		this.verificationPartitionerParams = verificationPartitionerParams;
	}
}
