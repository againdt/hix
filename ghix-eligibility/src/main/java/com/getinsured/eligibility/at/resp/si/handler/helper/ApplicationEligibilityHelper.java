package com.getinsured.eligibility.at.resp.si.handler.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.repository.EligibilityProgramRepository;
import com.getinsured.eligibility.slcsp.service.BenchMarkPremiumService;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component
public class ApplicationEligibilityHelper {

	private static final Logger LOGGER = Logger.getLogger(ApplicationEligibilityHelper.class);

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private ApplicationExchangeEligibilityHelper applicationExchangeEligibilityHelper;
	
	@Autowired BenchMarkPremiumService benchMarkPremiumService;
	
	@Autowired private AptcHistoryRepository aptcHistoryRepository;
	@Autowired
	@Qualifier("enrollmentUpdateAdapter")
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;
	
	@Autowired private EligibilityProgramRepository eligibilityProgramRepository;


	public void processEligibility(	Map<Long, List<EligibilityProgram>> eligibilityProgramMap,
			boolean fullAssessment, String applicationID, Map<String, Object> hhLevelEliDetails) {

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(applicationID);
		if (!ssapApplicationList.isEmpty()){
			SsapApplication ssapApplication = ssapApplicationList.get(0);

			setApplicationEligibilityStatus(eligibilityProgramMap, ssapApplication, fullAssessment);
			setApplicationExchangeEligibilityStatus(eligibilityProgramMap, ssapApplication);
			setHHLevelEliDetails(ssapApplication, hhLevelEliDetails);
			ssapApplicationRepository.save(ssapApplication);
		} else {
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + applicationID);
		}

	}

	private void setApplicationExchangeEligibilityStatus(Map<Long, List<EligibilityProgram>> eligibilityProgramMap, SsapApplication ssapApplication) {
		ExchangeEligibilityStatus exchangeEligibilityStatus = applicationExchangeEligibilityHelper.processEligibility(eligibilityProgramMap);
		ssapApplication.setExchangeEligibilityStatus(exchangeEligibilityStatus);
	}

	private void setApplicationEligibilityStatus(Map<Long, List<com.getinsured.eligibility.model.EligibilityProgram>> eligibilityProgramMap, SsapApplication ssapApplication, boolean isFullAssessment) {

		// Populate applicationEligibility Set for all applicants..
		Set<String> applicationEligibility = EligibilityStatusHelper.prepareApplicationEligibilitySet(eligibilityProgramMap);

		// Determine Application Eligibility Status & Exchange Eligibility Status
		boolean isAssessment = isFullAssessment;

		if (isAssessment){
			EligibilityStatusHelper.assessmentStatus(applicationEligibility, ssapApplication);
		} else {
			EligibilityStatusHelper.fullDeterminationStatus(applicationEligibility, ssapApplication);
		}

	}

	private void setHHLevelEliDetails(SsapApplication ssapApplication, Map<String, Object> hhLevelEliDetails) {

		BigDecimal maxAPTCAmount = null;
		BigDecimal maxStateSubsidyAmount = null;
		String csrLevel = null;

		if (!hhLevelEliDetails.isEmpty()){
			Boolean isApplicantionAPTCElig = (Boolean) hhLevelEliDetails.get("APTCElig");
			if(isApplicantionAPTCElig!=null && isApplicantionAPTCElig.equals(Boolean.TRUE) && hhLevelEliDetails.containsKey(EligibilityConstants.MAX_APTC_AMT)) {
				maxAPTCAmount = (BigDecimal) hhLevelEliDetails.get(EligibilityConstants.MAX_APTC_AMT);
			}
			
			if (hhLevelEliDetails.containsKey(EligibilityConstants.CSR_LEVEL)){
				csrLevel = (String) hhLevelEliDetails.get(EligibilityConstants.CSR_LEVEL);
			}

			Boolean isApplicantionStateSubsidyElig = (Boolean) hhLevelEliDetails.get("StateSubsidyElig");
			if(isApplicantionStateSubsidyElig!=null && isApplicantionStateSubsidyElig.equals(Boolean.TRUE) && hhLevelEliDetails.containsKey(EligibilityConstants.MAX_STATE_SUBSIDY_AMT)) {
				maxStateSubsidyAmount = (BigDecimal) hhLevelEliDetails.get(EligibilityConstants.MAX_STATE_SUBSIDY_AMT);
			}
		}

		if(maxAPTCAmount != null){
			ssapApplication.setMaximumAPTC(maxAPTCAmount.setScale(2, BigDecimal.ROUND_HALF_UP));	
		}
		else{
			ssapApplication.setMaximumAPTC(maxAPTCAmount);
		}

		String isStateSubsidyEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);
		if("Y".equalsIgnoreCase(isStateSubsidyEnabled)) {
			if (maxStateSubsidyAmount != null) {
				ssapApplication.setMaximumStateSubsidy(maxStateSubsidyAmount.setScale(2, BigDecimal.ROUND_HALF_UP));
			} else {
				ssapApplication.setMaximumStateSubsidy(maxStateSubsidyAmount);
			}
		}

		ssapApplication.setCsrLevel(csrLevel);
		
		if(ssapApplication.getExchangeEligibilityStatus()!=null && ssapApplication.getExchangeEligibilityStatus().name().contains("CSR")) {
			List<SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();
			for(SsapApplicant ssapApplicant : ssapApplicants) {
				if("QHP".equalsIgnoreCase(ssapApplicant.getEligibilityStatus()) && ssapApplicant.getCsrLevel()==null) {
					ssapApplication.setCsrLevel(null);
					break;
				}
			}
		}
		
		/**
		 * HIX-49705 - Multiple CSR level. If case comes out to be APTC with some CSR Level, reset the CSR Value.
		 */
		if (ExchangeEligibilityStatus.APTC.equals(ssapApplication.getExchangeEligibilityStatus()) 
				|| ExchangeEligibilityStatus.QHP.equals(ssapApplication.getExchangeEligibilityStatus())){
			ssapApplication.setCsrLevel(null);
		}

		compareAndModifyElectedAPTCAmount(ssapApplication, maxAPTCAmount);

	}

	private void compareAndModifyElectedAPTCAmount(SsapApplication ssapApplication, BigDecimal maxAPTCAmount) {

		BigDecimal electedAPTCAmount = ssapApplication.getElectedAPTC();

		if (maxAPTCAmount != null && electedAPTCAmount != null && electedAPTCAmount.compareTo(maxAPTCAmount) > 0) {
			LOGGER.info("Elected APTC Amount is greater than Maximum APTC Amount. Set Elected APTC Amount to Maximum APTC Amount.");
			ssapApplication.setElectedAPTC(maxAPTCAmount);
		}
	}

	/**
	 * Row inserted in APTCHistory table
	 * when "aptc is y in program eligibilities"
	 * or
	 * "state subsidy is y in program eligibilities"
	 * @param aptcProgram
	 * @param stateSubsidyProgram
	 * @param applicationID
	 * @param maxAPTCAmount
	 * @param maxStateSubsidyAmount
	 * @param slcspAmount
	 */
	public void insertHHAptc(com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram aptcProgram,
							 com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram stateSubsidyProgram,
							 String applicationID,
							 BigDecimal maxAPTCAmount,
							 BigDecimal maxStateSubsidyAmount,
							 BigDecimal slcspAmount ) {

		boolean isStateSubsidyEnabled = "Y".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(
				GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY));

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(applicationID);
		if (ssapApplicationList.isEmpty()) {
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + applicationID);
		}


		SsapApplication ssapApplication = ssapApplicationList.get(0);
		BigDecimal cmrHouseholdId = ssapApplication.getCmrHouseoldId();

		Float slcspAmountt = new Float(slcspAmount.toString());
		//HIX-117679 Do not call benchmark premium API for CA
		if(!EligibilityUtils.isCaCall() && (slcspAmount == null || slcspAmountt == 0.0)) {
			slcspAmountt = benchMarkPremiumService.getSLCSPAmount(ssapApplication);
			slcspAmount = slcspAmountt != null ? BigDecimal.valueOf(slcspAmountt).setScale(2, BigDecimal.ROUND_HALF_UP) : BigDecimal.valueOf(0.0);
		}

		AptcHistory aptcHistory = new AptcHistory();

		aptcHistory.setSsapApplicationId(ssapApplication.getId());
		aptcHistory.setCmrHouseholdId(cmrHouseholdId);
		aptcHistory.setCoverageYear(ssapApplication.getCoverageYear());
		aptcHistory.setSlcspAmount(slcspAmount);
		aptcHistory.setIsRecalc("N");

		if (aptcProgram != null) {
			aptcHistory.setAptcAmount(maxAPTCAmount);
			aptcHistory.setEligStartDate(aptcProgram.getEligibilityStartDate());
			aptcHistory.setEligEndDate(aptcProgram.getEligibilityEndDate());
		} else {
			List<EligibilityProgram> eligibilityProgramListForApplication = eligibilityProgramRepository.getEligibilitiesForApplication(ssapApplication.getId());
			if (eligibilityProgramListForApplication != null && !eligibilityProgramListForApplication.isEmpty()) {
				for (EligibilityProgram eligibilityProgram : eligibilityProgramListForApplication) {
					if (eligibilityProgram.getEligibilityStartDate() != null
							&& eligibilityProgram.getEligibilityEndDate() != null) {
						aptcHistory.setEligStartDate(eligibilityProgram.getEligibilityStartDate());
						aptcHistory.setEligEndDate(eligibilityProgram.getEligibilityEndDate());
						break;
					}
				}
			}
		}

		if (isStateSubsidyEnabled && stateSubsidyProgram != null) {
			aptcHistory.setStateSubsidyAmt(maxStateSubsidyAmount);
			aptcHistory.setStateSubsidyStartDate(stateSubsidyProgram.getEligibilityStartDate());
			aptcHistory.setStateSubsidyEndDate(stateSubsidyProgram.getEligibilityEndDate());
		}

		if("OE".equalsIgnoreCase(ssapApplication.getApplicationType())) {
			Date coverageStartDate = enrollmentUpdateAdapter.getCoverageStartDate(ssapApplication, null);
			aptcHistory.setEffectiveDate(coverageStartDate);
		}

		aptcHistoryRepository.save(aptcHistory);

	}

}
