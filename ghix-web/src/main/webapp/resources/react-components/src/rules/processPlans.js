/**
 * Created by sellathurai_s on 8/31/18.
 */

import moment from 'moment';

class ProcessPlans {

    constructor(tabData, config) {
        this.tabData = tabData;
        this.config = config;
        this.currentDate = this.config.serverDate ? moment(this.config.serverDate, 'MM-DD-YYYY') : moment();
    }

    isTrue(field) {
        return (field && (field.toLowerCase() === "y" || field.toLowerCase() === "yes" || field === true));
    }

    getPeriod( startDate, endDate){
        const OE_DATE = (this.currentDate).isBetween(moment(startDate, 'MM-DD-YYYY'), moment(endDate, 'MM-DD-YYYY'), 'days', true);
        return OE_DATE && this.tabData.applicationType === 'OE' ? 'OE' : 'OUTSIDE OE';
    }

    isFavPlanAvailable(planType) {
        if (this.tabData.preEligibility && this.tabData.preEligibility.plans) {
            let favPlans = this.tabData.preEligibility.plans.filter(x => x.type.toLowerCase() === planType.toLowerCase());
            return favPlans && favPlans.length > 0 ? "Y" : "NO";
        } else {
            return "NO";
        }
    }

    getEnrollments(planType) {
        if(!this.tabData.enrollments || this.tabData.enrollments.length <= 0)
            return [];

        let planTypeEnrollments = this.tabData.enrollments.filter(x => x.type.toLowerCase() === planType.toLowerCase());

        let enrollments = planTypeEnrollments.map(function (enrollment) {
            return {
                'plan': enrollment.plan,
                'memberCount': enrollment.enrolles && enrollment.enrolles.length > 0 ? enrollment.enrolles.length : 0
            }
        });

        return enrollments;
    }

    getPlanData(appObject, planType) {
        const enrollments = this.getEnrollments(planType);
        const isEnrollmentsPresent = enrollments && enrollments.length > 0;
        const { applicationStatus } = appObject;
        const appStatuses = ['ER', 'EN', 'PN', 'OP'];

        // Id 5, 6, 7, 8 APTC distribution spreadsheet
        if (isEnrollmentsPresent && appStatuses.includes(applicationStatus)) {
            appObject.favoritePlanAvailable = "N/A";
            appObject.enrollmentStatus = 'Y';
            return appObject;

        } else {
            appObject.favoritePlanAvailable = this.isFavPlanAvailable(planType);
        }

        if (appObject.applicationStatus === 'ER') { // Id 3, 4
            appObject.period = this.tabData.isInsideOEEnrollmentWindow ? 'OE' : 'OUTSIDE OE';
        }

        // DEFAULT CASE if no health enrollments and no favorite health enrollments. No need to check any application status
        if (!isEnrollmentsPresent && appObject.favoritePlanAvailable === 'NO') {
            appObject.applicationStatus = 'N/A';
        }

        return appObject;
    }

    getPlans(planType) {
        let appObject =  {
            "applicationStatus": this.tabData.applicationStatus  ? this.tabData.applicationStatus : "NO",
            "period": "N/A",
            "enrollmentStatus": "NO",
            "favoritePlanAvailable":"NO",
        };

        return this.getPlanData(appObject, planType);
    }

    getHealthPlanInfo() {
        return this.getPlans("health");
    }
}

export { ProcessPlans as default};