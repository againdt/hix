package com.getinsured.eligibility.prescreen.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.prescreen.repository.IPrescreenPremiumRepository;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.prescreen.MarketPlacePremium;
import com.getinsured.hix.model.prescreen.PremiumMultiplierData;
import com.getinsured.hix.model.prescreen.PrescreenPremiumData;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * 
 * @author bhatia_s
 *
 */
@Service("prescreenPremiumService")
public class PrescreenPremiumServiceImpl implements PrescreenPremiumService {
	private static final Logger LOGGER = Logger.getLogger(PrescreenPremiumServiceImpl.class);

	public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	private static final int FOUR = 4;
	private static final int SIXTY = 60;
	private static final int THOUSAND = 1000;
	private static final int NO_OF_HOURS_IN_DAY = 24;
	private static final int LEAP_YEAR_DAYS = 366;
	private static final int NON_LEAP_YEAR_DAYS = 365;
	private static final int FIFTEEN = 15;
	
	private static final int ADULT_AGE = 26;
	private static final String NY_STATE_CODE = "NY";
	private static final String VT_STATE_CODE = "VT";
	
	private static final double NY_MULTIPLIER_ONE_ADULT = 0.7;
	private static final double NY_MULTIPLIER_TWO_ADULTS = 0.85;
	private static final double VT_MULTIPLIER_ONE_ADULT = 0.93;
	private static final double VT_MULTIPLIER_TWO_ADULTS = 0.81;
	
	private static Map<String,Map<Integer,Double>> multipliersForNYandVT = new HashMap<String, Map<Integer,Double>>();
	
	static{
		Map<Integer,Double> values = new HashMap<Integer, Double>();
		values.put(1, NY_MULTIPLIER_ONE_ADULT);
		values.put(2, NY_MULTIPLIER_TWO_ADULTS);
		multipliersForNYandVT.put(NY_STATE_CODE, values);
		
		values = new HashMap<Integer, Double>();
		values.put(1, VT_MULTIPLIER_ONE_ADULT);
		values.put(2, VT_MULTIPLIER_TWO_ADULTS);
		multipliersForNYandVT.put(VT_STATE_CODE, values);
	}
	
	
	@Autowired private IPrescreenPremiumRepository iPrescreenPremiumRepository;
	
	/**
	 * 
	 */
	@Override
	public PrescreenPremiumData calculatePremium(
			List<Member> memberList, String countyCode, String zipCode,
			String stateName) {

		PrescreenPremiumData prescreenPremiumData = new PrescreenPremiumData();
		
		MarketPlacePremium marketPlacePremium = iPrescreenPremiumRepository.getRatingAreaByZipAndCounty(zipCode, countyCode);
		if(marketPlacePremium!=null){
			
			LOGGER.debug("Match found for the zipcode and county from marketplace premium table");
			prescreenPremiumData.setPremiumFound(true);			
			double lowestCatPremium = 0.0;
			double lowestGoldPremium = 0.0;
			double lowestSilverPremium = 0.0;
			double lowestBronzePremium = 0.0;
			double secLowestSilverPremium = 0.0;
			
			//Premium calculation for NY and VT
			if(StringUtils.equalsIgnoreCase(stateName, NY_STATE_CODE)
					|| StringUtils.equalsIgnoreCase(stateName, VT_STATE_CODE)){
				getPremiumForNYandVT(memberList,marketPlacePremium,prescreenPremiumData,stateName);
			}
			else{
				// Loop through each member DOB
				for(Member member : memberList){
					String dob = member.getDateOfBirth();
					int age = this.getAgeFromDob(dob);
					
					// Fetch the Multiplier factor based on age to calculate the premium
					Double multiplier = PremiumMultiplierData.getPremiumMultiplier(stateName, age);
					LOGGER.debug("Age: "+age+" Multiplier:"+multiplier);		
					if(multiplier != null){
						lowestCatPremium += marketPlacePremium.getCatQhpCost() * multiplier;
						lowestGoldPremium += marketPlacePremium.getGoldQhpCost() * multiplier;
						lowestSilverPremium += marketPlacePremium.getSilverQhpCost() * multiplier;
						lowestBronzePremium += marketPlacePremium.getBronzeQhpCost() * multiplier;
						secLowestSilverPremium += marketPlacePremium.getSecondSilverQhpCost() * multiplier;
					}
				}
				
				// Prepare the final Premium data for Eligibility Module
				prescreenPremiumData.setLowestCatPremium(lowestCatPremium);
				prescreenPremiumData.setLowestGoldPremium(lowestGoldPremium);
				prescreenPremiumData.setLowestSilverPremium(lowestSilverPremium);
				prescreenPremiumData.setLowestBronzePremium(lowestBronzePremium);
				prescreenPremiumData.setSecLowestSilverPremium(secLowestSilverPremium);
			}
			
			// Add count of QHPs to response
			prescreenPremiumData.setNumberOfBronzeQHPs(marketPlacePremium.getNumOfBronzeQhps());
			prescreenPremiumData.setNumberOfCatQHPs(marketPlacePremium.getNumOfCatQhps());
			prescreenPremiumData.setNumberOfGoldQHPs(marketPlacePremium.getNumOfGoldQhps());
			prescreenPremiumData.setNumberOfIssuers(marketPlacePremium.getNumOfIssuers());
			prescreenPremiumData.setNumberOfQHPs(marketPlacePremium.getNumOfQhps());
			prescreenPremiumData.setNumberOfSilverQHPs(marketPlacePremium.getNumOfSilverQhps());
		}
		else{
			LOGGER.debug("No match found for the zipcode and county from marketplace premium table");			
			prescreenPremiumData.setPremiumFound(false);
		}
		
		
		return prescreenPremiumData;
	}
	
	/**
	 * HIX-26268 - New York and Vermont SLSP
	 * 
	 * Method calculates premium values for NY and VT
	 * 
	 */
	public void getPremiumForNYandVT(List<Member> memberList, MarketPlacePremium marketPlacePremium, 
			PrescreenPremiumData prescreenPremiumData, String stateName){
		
		LOGGER.debug("Calculating premium for " + stateName);
		int noOfAdults = 0;
		int noOfChildren = 0;
		
		double multiplier = 1.0;
		
		for(Member member : memberList){
			int age = this.getAgeFromDob(member.getDateOfBirth());
			if(age >= ADULT_AGE){
				noOfAdults++;
			}
			else{
				noOfChildren++;
			}
		}
		
		/*
		 * 1. For every adult multiplier will be incremented by 1
		 * 2. 1 adult + any no. of children, 
		 * 		2.1 NY - multiplier to be incremented by 0.7
		 * 		2.2 VT - multiplier to be incremented by 0.93
		 * 3. >= 2 adults + any no. of children
		 * 		3.1 NY - multiplier to be incremented by 0.85
		 * 		3.2 VT - multiplier to be incremented by 0.81
		 */
		if(noOfAdults > 0){
			multiplier = noOfAdults;
			if(noOfAdults == 1 && noOfChildren > 0){
				multiplier += multipliersForNYandVT.get(stateName).get(1);
			}
			else if(noOfAdults >= 2 && noOfChildren > 0){
				multiplier += multipliersForNYandVT.get(stateName).get(2);
			}
		}
		else{
			multiplier = noOfChildren;
		}
		
		LOGGER.debug("Multiplier : " + multiplier);
		prescreenPremiumData.setLowestCatPremium(marketPlacePremium.getCatQhpCost() * multiplier);
		prescreenPremiumData.setLowestGoldPremium(marketPlacePremium.getGoldQhpCost() * multiplier);
		prescreenPremiumData.setLowestSilverPremium(marketPlacePremium.getSilverQhpCost() * multiplier);
		prescreenPremiumData.setLowestBronzePremium(marketPlacePremium.getBronzeQhpCost() * multiplier);
		prescreenPremiumData.setSecLowestSilverPremium(marketPlacePremium.getSecondSilverQhpCost() * multiplier);
		 
	}
	
	/**
	 * Utility method to get age of a member
	 *
	 * @param String - The DOB in MM/dd/yyyy format
	 * @return double - age of the member
	 */
	public int getAgeFromDob(String dob){

		if(StringUtils.isBlank(dob)){
			return 0;
		}

		Calendar cal = computeEffectiveDate();
		long today = cal.getTimeInMillis();
	    Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
	    long birthDate = dobDate.getTime();
	    
	    long diff = today - birthDate;
	    double diffYears = 0;
	    long millisinYear = 0;
	    
	    if(cal.get(Calendar.YEAR) % FOUR == 0){
	    	millisinYear = (long)THOUSAND * SIXTY * SIXTY
	    	        * NO_OF_HOURS_IN_DAY * LEAP_YEAR_DAYS;
	    }
	    else{
	    	millisinYear = (long)THOUSAND * SIXTY * SIXTY
	    	        * NO_OF_HOURS_IN_DAY * NON_LEAP_YEAR_DAYS;
	    	
	    }
	    
	    diffYears =  (double)diff / millisinYear;
	    BigDecimal age = new BigDecimal(diffYears);
	    return age.intValue();

	}
	
	/**
	 * Computes effective date for coverage start
	 * 
	 * Following logic is implemented as part of HIX-24255
	 * 
	 * 1. If current date is on or before 15th of a month, effective date is 1st of next month
	 * 2. If current date is after 15th of a month, effective date is 1st of next to next month
	 * 
	 * Exmaples:
	 * 1. Current Date - 12th July 2014 , Effective Date - 1st August 2014
	 * 2. Current Date - 19th July 2014, Effective Date - 1st September 2014
	 * 3. Current Date - 28th November 2013, Effective Date - 1st January 2014
	 * 4. Current Date - 25th December 2013, Effective Date - 1st February 2014
	 * 
	 * @author - Sunil Desu, Nikhil Talreja
	 * @since 28 November, 2013
	 */
	
	private Calendar computeEffectiveDate(){
		
		Calendar effectiveDate = TSCalendar.getInstance();
		Calendar currentDate = TSCalendar.getInstance();
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);	

		//Current Date is on or before 15 of month
		if(currentDate.get(Calendar.DAY_OF_MONTH) <= FIFTEEN){	
			effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 1);	
		}
		//Current Date is after 15 of month
		else{
			effectiveDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH) + 2);
		}
		
		effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
		effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
		effectiveDate.set(Calendar.MINUTE, 0);
		effectiveDate.set(Calendar.SECOND, 0);
		effectiveDate.set(Calendar.MILLISECOND, 0);
		return effectiveDate;
	}
	
	public static void main(String args[]) throws ParseException {
		
		PrescreenPremiumServiceImpl obj = new PrescreenPremiumServiceImpl();
		double age = obj.getAgeFromDob("9/28/2003");
		BigDecimal d = new BigDecimal(age);
		LOGGER.debug("age :"+d.intValue());
		
		Double multiplier = PremiumMultiplierData.getPremiumMultiplier("CA", d.intValue());
		LOGGER.debug("multiplier :"+multiplier);
	}
}
