<%@page import="java.util.ArrayList"%>
<%@page import="antlr.collections.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.util.*"   %>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>


<script type="text/javascript" src="<:url value="/resources/js/affiliate/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/highlight.min.js" />"></script>

<div class="row-fluid">
	<div class="gutter10">
        <div id="skip" class="hide"></div>
        <ul class="page-breadcrumb">
            <li><a href="/hix/admin/affiliate/manage">&lt;  Back</a></li>
            <li><a href="/hix/admin/affiliate/manage">Manage Affiliate</a></li>
            <li><a href="/hix/admin/affiliate/viewaffiliate/${affiliate.affiliateId}">View Affiliate</a></li>
            <li><a href="#">Edit Flow</a></li>
        </ul><!--page-breadcrumb ends-->
        <h1>Edit Flow Information</h1>
	</div>
</div>

<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
</div>
 <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:forEach items="${FieldError}" var="element"> 
			  <tr>
			    <td><p><c:out value="${element}"></c:out><p/></td>
			  </tr>
			</c:forEach>
			<br>
		</div>
</div> 
<div class="">
<input type="hidden" name="affiliateId" id="affiliateId" value="${affiliateId}">
<input type="hidden" name="affiliateflowId" id="affiliateflowId" value="${affiliateflowId}">
<input type="hidden" name="pageName" id="pageName" value="${pageName}">




	<form id="editflow-form" data-validate="parsley" class="form-horizontal"  enctype="multipart/form-data" action="<c:url value="/admin/affiliate/editflow/${pageName}/${affiliateId}/${affiliateflow.affiliateflowId}"/>"   method="post" >
		
		<spring:url value="/affiliate/logo/${affiliateId}/${affiliateflow.affiliateflowId}"  var="photoUrl"/>
				<img src="${photoUrl}" alt="flow photo thumbnail" class="profilephoto thumbnail" /><br>
				<div class="control-group">
					<label for="brkChangephoto" class="control-label required"><spring:message code="label.brkChangephoto"/></label>
					<div class="controls">
						<input type="file" class="input-file" id="fileInputPhoto" name="fileInputPhoto">
						<input type="hidden"  id="fileInputPhoto_Size" name="fileInputPhoto_Size" value="1">               	
					</div>	<!-- end of controls -->
					<div id="fileInputPhoto_error"></div>
				</div><!-- end of  control-group -->
		
		
		<fieldset id="flow_0">
		<div class="left-col-w" style="width:470px; float:left;">
		
		    <div class="control-group">
				<label for="flowName" class="control-label">Flow Name<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="flowName" name="flowName" value="${affiliateflow.flowName}" data-required="true" class="parsley-validated parsley-error width247">
			</div>
		
		
			<div class="control-group">
				<label class="control-label">Status <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
				<select id="flowStatus" name="flowStatus" data-required="true" class="parsley-validated parsley-error width260">
					<option value="">Select from a list</option>
						<c:forEach var="flowStatus" items="${AffiliateFlowStatusList}">
									<option <c:if test="${flowStatus == affiliateflow.flowStatus}"> SELECTED </c:if> value="${flowStatus}">${flowStatus.description}</option>
						</c:forEach>
						
				</select>
			</div>
				<div class="control-group">
				<label class="control-label">Type of traffic </label>
				<select id="trafficType" name="trafficType"  class="parsley-validated parsley-error width260">
					<option value="">Select from a list</option>
					<c:forEach var="trafficType" items="${AffiliateTrafficTypeList}">
									<option <c:if test="${trafficType == affiliateflow.trafficType}"> SELECTED </c:if> value="${trafficType}">${trafficType.description}</option>
					</c:forEach>
					
				</select>
			</div>
			
			<div class="control-group">
				<label for="ivrNumber" class="control-label">IVR Number</label>
				<input type="text" id="ivrNumber" name="ivrNumber" value="${affiliateflow.ivrNumber}" data-type="number" maxlength="10" data-trigger="change" data-rangelength="[10,10]" data-rangelength-message="This value should be 10 digits" class="parsley-validated parsley-error width247">
				<div id="ivrNumber_error"></div>
			</div>
			
			<div class="control-group">
				<label for="Website" class="control-label">Redirection Url <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="redirectionUrl" name="redirectionUrl"  value="${affiliateflow.redirectionUrl}" data-required="true" data-trigger="change" class="parsley-validated"  class="xlarge" size="60">
			</div>
			
			<c:choose>
			<c:when test="${flowActiveFlag == true}">
				<div class="control-group">
				<label class="control-label">Web Pricing Model<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<select id="webPricingModel" name="webPricingModel" data-required="true" class="parsley-validated parsley-error width260" disabled="disabled">
					<option value="">Select from a list</option>
					<c:forEach var="webPricingModel" items="${WebPricingModelList}">
									<option <c:if test="${webPricingModel == affiliateflow.webPricingModel}"> SELECTED </c:if> value="${webPricingModel}">${webPricingModel.description}</option>
					</c:forEach>
				</select>
			</div>
			
			<div class="control-group">
			<label for="webpayout" class="control-label">Web Payout</label>
			<div class="controls">
              <c:choose>
			<c:when test="${affiliateflow.webPayoutType=='FIXED'}">
            <label class="radio inline width85">
            	<input type="radio" name="webPayoutType" id="fixedWebPayout" value="FIXED" checked='checked' data-required="true" class="parsley-validated" disabled="disabled"> Fixed($)
            </label>
           <!--  <input type="text" class="span5"> -->
            <input type="text" id="webPayoutvalue" name="webPayoutvalue" readonly="readonly" value="${affiliateflow.webPayoutvalue}" data-required="true" data-type="number" class="span5 parsley-validated parsley-error width247">
            
			<label class="radio inline width85">
				<input type="radio" name="webPayoutType" id="percentWebPayout" value="PERCENTAGE" disabled="disabled"> Percentage(%) 
			
			</label>
         </c:when>
         <c:otherwise>
         
              <label class="radio inline width85">
            	<input type="radio" name="webPayoutType" id="fixedWebPayout" value="FIXED" checked data-required="true" class="parsley-validated" disabled="disabled"> Fixed($)
            </label>
           <!--  <input type="text" class="span5"> -->
            <input type="text" id="webPayoutvalue" name="webPayoutvalue" readonly="readonly" value="${affiliateflow.webPayoutvalue}" data-required="true" data-type="number" class="span5 parsley-validated parsley-error width247">
            
			<label class="radio inline width85">
				<input type="radio" name="webPayoutType" id="percentWebPayout" value="PERCENTAGE" checked='checked' disabled="disabled"> Percentage(%) 
			
			</label>
         </c:otherwise>
          </c:choose>
            </div>
            </div>
            
            <div class="control-group">
				<label class="control-label">Call Pricing Model <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<select  id="callPricingModel" name="callPricingModel" data-required="true" class="parsley-validated parsley-error width260" disabled="disabled">
					<option value="">Select from a list</option>
					<c:forEach var="callPricingModel" items="${CallPricingModelList}">
						<option <c:if test="${callPricingModel == affiliateflow.callPricingModel}"> SELECTED </c:if> value="${callPricingModel}">${callPricingModel.description}</option>
					</c:forEach>
				</select>
			</div>
			

				<div class="control-group">
					<label for="callpayout" class="control-label">Call Payout</label>
					<div class="controls">
					   <c:choose>
						<c:when test="${affiliateflow.callPayoutType =='FIXED_PAYOUT'}">
						<label class="radio inline width85">
							<input type="radio" name="callPayoutType" id="callPayoutFixed" value="FIXED_PAYOUT" disabled="disabled" checked='checked' data-required="true" class="parsley-validated"> Fixed($)
						</label>
						<!-- <input type="text" class="span5"> -->
						 <input type="text" id="callPayoutvalue" readonly="readonly" name="callPayoutvalue" value="${affiliateflow.callPayoutvalue}" data-required="true" data-type="number" class="span5 parsley-validated parsley-error width247">
						
						<label class="radio inline width85">
							<input type="radio"name="callPayoutType" id="callPayoutPercent" disabled="disabled" value="PERCENTAGE" >Percentage(%) 
						</label>
						</c:when> 
						<c:otherwise>
						<label class="radio inline width85">
							<input type="radio" name="callPayoutType" id="callPayoutFixed" disabled="disabled" value="FIXED_PAYOUT" data-required="true" class="parsley-validated"> Fixed($)
						</label>
						<!-- <input type="text" class="span5"> -->
						 <input type="text" id="callPayoutvalue" name="callPayoutvalue" readonly="readonly" value="${affiliateflow.callPayoutvalue}" data-required="true" data-type="number" class="span5 parsley-validated parsley-error width247">
						
						<label class="radio inline width85">
							<input type="radio"name="callPayoutType" id="callPayoutPercent" disabled="disabled" checked = 'checked' value="PERCENTAGE" >Percentage(%) 
						</label>
						</c:otherwise>
						
						</c:choose>

				</div>
				</div>
            </c:when>
			<c:otherwise>
				<div class="control-group">
				<label class="control-label">Web Pricing Model<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<select id="webPricingModel" name="webPricingModel" data-required="true" class="parsley-validated parsley-error width260">
					<option value="">Select from a list</option>
					<c:forEach var="webPricingModel" items="${WebPricingModelList}">
									<option <c:if test="${webPricingModel == affiliateflow.webPricingModel}"> SELECTED </c:if> value="${webPricingModel}">${webPricingModel.description}</option>
					</c:forEach>
				</select>
			</div>
			
			<div class="control-group">
			<label for="webpayout" class="control-label">Web Payout</label>
			<div class="controls">
              <c:choose>
			<c:when test="${affiliateflow.webPayoutType =='FIXED'}">
            <label class="radio inline width85">
            	<input type="radio" name="webPayoutType" id="fixedWebPayout" value="FIXED" checked='checked' data-required="true" class="parsley-validated"> Fixed($)
            </label>
           <!--  <input type="text" class="span5"> -->
            <input type="text" id="webPayoutvalue" name="webPayoutvalue" value="${affiliateflow.webPayoutvalue}" data-required="true" data-type="number" class="span5 parsley-validated parsley-error width247">
            
			<label class="radio inline width85">
				<input type="radio" name="webPayoutType" id="percentWebPayout" value="PERCENTAGE" > Percentage(%) 
			
			</label>
         </c:when>
         <c:otherwise>
         
              <label class="radio inline width85">
            	<input type="radio" name="webPayoutType" id="fixedWebPayout" value="FIXED" checked data-required="true" class="parsley-validated"> Fixed($)
            </label>
           <!--  <input type="text" class="span5"> -->
            <input type="text" id="webPayoutvalue" name="webPayoutvalue" value="${affiliateflow.webPayoutvalue}" data-required="true" data-type="number" class="span5 parsley-validated parsley-error width247">
            
			<label class="radio inline width85">
				<input type="radio" name="webPayoutType" id="percentWebPayout" value="PERCENTAGE" checked='checked'> Percentage(%) 
			
			</label>
         </c:otherwise>
          </c:choose>
            </div>
            </div>
            
            <div class="control-group">
				<label class="control-label">Call Pricing Model <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<select  id="callPricingModel" name="callPricingModel" data-required="true" class="parsley-validated parsley-error width260">
					<option value="">Select from a list</option>
					<c:forEach var="callPricingModel" items="${CallPricingModelList}">
						<option <c:if test="${callPricingModel == affiliateflow.callPricingModel}"> SELECTED </c:if> value="${callPricingModel}">${callPricingModel.description}</option>
					</c:forEach>
				</select>
			</div>
			

				<div class="control-group">
					<label for="callpayout" class="control-label">Call Payout</label>
					<div class="controls">
					   <c:choose>
						<c:when test="${affiliateflow.callPayoutType =='FIXED_PAYOUT'}">
						<label class="radio inline width85">
							<input type="radio" name="callPayoutType" id="callPayoutFixed" value="FIXED_PAYOUT" checked='checked' data-required="true" class="parsley-validated"> Fixed($)
						</label>
						<!-- <input type="text" class="span5"> -->
						 <input type="text" id="callPayoutvalue" name="callPayoutvalue" value="${affiliateflow.callPayoutvalue}" data-required="true" data-type="number" class="span5 parsley-validated parsley-error width247">
						
						<label class="radio inline width85">
							<input type="radio"name="callPayoutType" id="callPayoutPercent" value="PERCENTAGE" >Percentage(%) 
						</label>
						</c:when> 
						<c:otherwise>
						<label class="radio inline width85">
							<input type="radio" name="callPayoutType" id="callPayoutFixed" value="FIXED_PAYOUT" data-required="true" class="parsley-validated"> Fixed($)
						</label>
						<!-- <input type="text" class="span5"> -->
						 <input type="text" id="callPayoutvalue" name="callPayoutvalue" value="${affiliateflow.callPayoutvalue}" data-required="true" data-type="number" class="span5 parsley-validated parsley-error width247">
						
						<label class="radio inline width85">
							<input type="radio"name="callPayoutType" id="callPayoutPercent" checked = 'checked' value="PERCENTAGE" >Percentage(%) 
						</label>
						</c:otherwise>
						
						</c:choose>

				</div>
				</div>
            </c:otherwise>
		   </c:choose>
		   <div class="control-group">
			<label for="inboundGreeting" class="control-label">Inbound Greetings </label> 
		   <textarea rows="4" cols="45" id="inboundGreeting" name="inboundGreeting" value="${affiliateflow.inboundGreeting}" data-trigger="keyup" data-rangelength="[0,200]" class="parsley-validated"></textarea>
		   </div>
		</div>
		<div class="right-col-width"style="width:470px; float:right;">
		
				<div class="control-group">
				<label for="customField1" class="control-label">Custom Field 1 </label>
				<input type="text" id="customField1" name="customField1" value="${affiliateflow.customField1}"  class="parsley-validated parsley-error">
			</div>
			<div class="control-group">
				<label for="customField2" class="control-label">Custom Field 2 </label>
				<input type="text" id="customField2" name="customField2" value="${affiliateflow.customField2}"  class="parsley-validated parsley-error">
			</div>
			<div class="control-group">
				<label for="customField3" class="control-label">Custom Field 3 </label>
				<input type="text" id="customField3" name="customField3" value="${affiliateflow.customField3}"  class="parsley-validated parsley-error">
			</div>
			<div class="control-group">
				<label for="customField4" class="control-label">Custom Field 4 </label>
				<input type="text" id="customField4" name="customField4" value="${affiliateflow.customField4}"  class="parsley-validated parsley-error">
			</div>
			
			<div class="control-group">
			<label for="businessTermsConditons" class="control-label">Business Comments </label> 
		   <textarea rows="8" cols="45" id="businessTermsConditons" name="businessTermsConditons"  value="${affiliateflow.businessTermsConditons}"  data-trigger="keyup"  data-rangelength="[0,200]" class="parsley-validated textareaH233"></textarea>
		   </div>
			
			<div class="control-group">
				<label for="outboundGreeting" class="control-label">Outbound Greetings</label> 
			   <textarea rows="4" cols="45" id="outboundGreeting" name="outboundGreeting" value="${affiliateflow.outboundGreeting}" data-trigger="keyup" data-rangelength="[0,200]" class="parsley-validated"></textarea>
		   </div>
		</div>
		
		</fieldset>
		<div class="control-group pull-right marginR20">
			<input type="submit" class="btn btn-primary btn-small" id="save-flow" value="Save Flow">
			<input type="button" class="btn btn-primary btn-small" id="cancel-flow" value="Cancel Flow">
			
		</div>
	
	</form>
	<input type="hidden" name="IvrNumberCheck" id="IvrNumberCheck" value="" />
</div>

<script type="text/javascript">

$(document).ready(function(){
	$('#outboundGreeting').text('${affiliateflow.outboundGreeting}');
	$('#businessTermsConditons').text('${affiliateflow.businessTermsConditons}');
	$('#inboundGreeting').text('${affiliateflow.inboundGreeting}');
	
});
	  
	 
	  $('#cancel-flow').click(function (){
		  
		  if($("#pageName").val()=="viewaffiliate")
			  {
			  window.location='<c:url value="/admin/affiliate/viewaffiliate/'+ ${affiliateId} +'"/>'
			 
			  }
		  else
			  {
			  window.location='<c:url value="/admin/affiliate/manageflow"/>'
			  }
		  
	  });
	  
	  
	  $('#webPayoutvalue, #callPayoutvalue').bind('focusout change ',function(){
			if (($('input:radio[name="webPayoutType"]:checked').attr('id') === "percentWebPayout")){
				percentCheck($(this));
			}
			
			if($('input:radio[name="callPayoutType"]:checked').attr('id') === "callPayoutPercent"){
				percentCheck($(this));
			}
			
			if (($('input:radio[name="webPayoutType"]:checked').attr('id') === "fixedWebPayout")){
				positiveCheck($(this));
			}
			
			if($('input:radio[name="callPayoutType"]:checked').attr('id') === "callPayoutFixed"){
				positiveCheck($(this));
			}
		  }); 
		  
		$('input[name="webPayoutType"]').change(function(){
			var textboxVal = parseInt($('#webPayoutvalue').val());
			if((textboxVal!="NaN") && ($(this).attr('id') === "percentWebPayout")){
				if((textboxVal >100) || (textboxVal <0)){
					$(this).parents('.controls').find('.requiredError').remove();
					$(this).parents('.controls').append('<p class="requiredError">* This value should be between 0 and 100 .</p>')
				}
				else{
					$('.controls').find('.requiredError').remove();
				}
			}
			else if((textboxVal!="NaN") && ($(this).attr('id') === "fixedWebPayout")){
				if(textboxVal <0){
					$(this).parents('.controls').find('.requiredError').remove();
					$(this).parents('.controls').append('<p class="requiredError">* This value should be positive.</p>')
				}
				else{
					$('.controls').find('.requiredError').remove();
				}
			}
		});
		
		$('input[name="callPayoutType"]').change(function(){
			var textboxVal1 = parseInt($('#callPayoutvalue').val());
			if((textboxVal1!="NaN") && ($(this).attr('id') === "callPayoutPercent")){
				if((textboxVal1 >100) || (textboxVal1 <0)){
					$(this).parents('.controls').find('.requiredError').remove();
					$(this).parents('.controls').append('<p class="requiredError">* This value should be between 0 and 100 .</p>')
				}
				else{
					$('.controls').find('.requiredError').remove();
				}
			}
			else if((textboxVal1!="NaN") && ($(this).attr('id') === "callPayoutFixed")){
				if(textboxVal1 <0){
					$(this).parents('.controls').find('.requiredError').remove();
					$(this).parents('.controls').append('<p class="requiredError">* This value should be positive.</p>')
				}
				else{
					$('.controls').find('.requiredError').remove();
				}
			}
		});
		
			  
			  function percentCheck(thisVal){
					if((thisVal.val() >100) || (thisVal.val() <0)){
						thisVal.parents('.controls').find('.requiredError').remove();
						thisVal.parents('.controls').append('<p class="requiredError">* This value should be between 0 and 100 .</p>')
					}
					else{
						$('.controls').find('.requiredError').remove();
					}
				}

				function positiveCheck(thisVal){
					if(thisVal.val() <0){
						thisVal.parents('.controls').find('.requiredError').remove();
						thisVal.parents('.controls').append('<p class="requiredError">* This value should be positive.</p>')
					}
					else{
						$('.controls').find('.requiredError').remove();
					}
				}
				
				
				$('#ivrNumber').blur(function(){				
					if( ($("#ivrNumber").val()!="") )
					{
						$.get('checkIfExistIVRNumber',
							{
								ivrNumber: $("#ivrNumber").val()
						    },
							function(response){
								if(response === true){
									$('#ivrNumber_error .error').remove();
									$("#ivrNumber_error").append("<label class='error'><span> <em class='excl'>!</em>IVRNumber already exist.</span></label>");		
									return false;
								}
								else{
									$('#ivrNumber_error .error').remove();
								}
							});
					}
					else{
						$('#ivrNumber_error .error').remove();
					}
				});

				function checkIVRNumber(){
					
				}		


				$('#editflow-form').submit(function(){
					if($('#ivrNumber_error .error').is(':visible') === true)
						{
						return false;
						}
					else
						{
						$.post('<c:url value="/admin/affiliate/addflow/'+ ${affiliateId} +'"/>')
						
						}
						
					 $('select').removeAttr('disabled');
					 $('input[type="radio"]').removeAttr('disabled');
				})
	  
		
</script>

    