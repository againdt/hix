<%@ include file="../include.jsp" %>
<div id="appscrBloodRel" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.bloodRel.reldef" text="Define Your Household Relationships "/>
			</h4>
		</div>
		<form id="appscrBloodRelForm" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.bloodRel.header" /></h4>
			</div>
			<div class="gutter10">
				<div class="control-group alert alert-info" >
					<span><spring:message code="ssap.bloodRel.hereAre" /> <span class="householdMemberCount"></span> <spring:message code="ssap.bloodRel.membersRelationship" /></span>
					<ol class="margin10-t" id="relationshipMembersList">
						
					</ol>
					
					<div class="margin10-t hide_nm" ><spring:message code="ssap.bloodRel.25Ward" /></div>
				</div><!-- control-group ends -->
				
				<p class="hide alert alert-error" id="multiSpouse_error">
					<spring:message code="ssap.bloodRel.multiSpouseError" />
				</p>
				
				<div class="control-group">
					<ol id="relationshipSelection">
						
					</ol>
				</div>
				
				
			</div><!-- .gutter10 -->
		</form>
	</div>
</div>

<select id="relationshipOptions_all" class="hide" aria-label="Relationship options">
	<option value=""><spring:message code="ssap.page13.selectRelationship" text="Select Relationship"/></option>
	<option value="01"><spring:message code="ssap.page13.spouse" text="Spouse"/></option>
	<option value="03"><spring:message code="ssap.page13.parentOfChild"/></option>	
	<option value="03a"><spring:message code="ssap.page13.parentOfAdoptedChild"/></option>	
	<option value="03f"><spring:message code="ssap.page13.parentOfFosterChild"/></option>
	<option value="04"><spring:message code="ssap.page13.grandParent" text="Grandparent"/></option>
	<option value="05"><spring:message code="ssap.page13.grandChild" text="Grandchild"/></option>
	<option value="06"><spring:message code="ssap.page13.uncleAunt" text="Uncle/aunt"/></option>
	<option value="07"><spring:message code="ssap.page13.nephewNiece" text="Nephew/niece"/></option>
	<option value="08"><spring:message code="ssap.page13.firstCousin" text="First cousin"/></option>
	<option value="09"><spring:message code="ssap.page13.adoptedChild" text="Adopted child (son or daughter)"/></option>
	<option value="10"><spring:message code="ssap.page13.fosterChild" text="Foster child (foster son or foster daughter)"/></option>
	<option value="11"><spring:message code="ssap.page13.sonDaughterInLaw" text="Son-in-law or daughter-in-law"/></option>
	<option value="12"><spring:message code="ssap.page13.brotherSisterInLaw" text="Brother-in-law or sister-in-law"/></option>
	<option value="13"><spring:message code="ssap.page13.motherFatherInLaw" text="Mother-in-law or father-in law"/></option>
	<option value="14"><spring:message code="ssap.page13.sibling" text="Sibling (brother or sister)"/></option>
	<option value="15"><spring:message code="ssap.page13.wardOfGuardian"/></option>
	<option value="15c"><spring:message code="ssap.page13.wardOfCGuardian"/></option>
	<option value="15p"><spring:message code="ssap.page13.wardOfParentCaretaker"/></option>
	<option value="16"><spring:message code="ssap.page13.stepParent" text="Stepparent (stepfather or stepmother)"/></option>
	<option value="17"><spring:message code="ssap.page13.stepChild" text="Stepchild (stepson or stepdaughter)"/></option>
	<option value="19"><spring:message code="ssap.page13.childSonDaughter" text="Child (son or daughter)"/></option>
	<%-- <option value="23"><spring:message code="ssap.page13.sponsoredDependent" text="Sponsored dependent"/></option>
	<option value="24"><spring:message code="ssap.page13.minorDependent" text="Dependent of a minor dependent"/></option> --%>
	<option value="25"><spring:message code="ssap.page13.formerSpouse" text="Former spouse"/></option>
	<option value="26"><spring:message code="ssap.page13.guardian" text="Guardian"/></option>
	<option value="31"><spring:message code="ssap.page13.courtAppointedGuardian" text="Court-appointed guardian"/></option>
	<%-- <option value="38"><spring:message code="ssap.page13.collateralDependent" text="Collateral dependent"/></option> --%>
	<option value="53"><spring:message code="ssap.page13.domesticPartner" text="Domestic partner"/></option>
	<%-- <option value="60"><spring:message code="ssap.page13.annuitant" text="Annuitant"/></option>
	<option value="D2"><spring:message code="ssap.page13.trustee" text="Trustee"/></option> --%>
	<option value="G8"><spring:message code="ssap.page13.unspecifiedRelationship" text="Unspecified relationship"/></option>
	<option value="G9"><spring:message code="ssap.page13.unspecifiedRelative" text="Unspecified relative"/></option>
	<option value="03-53"><spring:message code="ssap.page13.parentDomesticPartner" text="Parent's domestic partner"/></option>
	<option value="53-19"><spring:message code="ssap.page13.childOfDomesticPartner" text="Child of domestic partner"/></option>
	<option value="03w"><spring:message code="ssap.page13.parentCaretakerOfWard"/></option>	
</select>


<select id="relationshipOptions_id" class="hide" aria-label="Relationship options">
	<option value=""><spring:message code="ssap.page13.selectRelationship" text="Select Relationship"/></option>
	<option value="01"><spring:message code="ssap.page13.spouse" text="Spouse"/></option>
	<option value="03"><spring:message code="ssap.page13.parentOfChild"/></option>	
	<option value="03a"><spring:message code="ssap.page13.parentOfAdoptedChild"/></option>	
	<option value="03w"><spring:message code="ssap.page13.parentCaretakerOfWard"/></option>	
	<option value="09"><spring:message code="ssap.page13.adoptedChild" text="Adopted child (son or daughter)"/></option>
	<option value="15c"><spring:message code="ssap.page13.wardOfCGuardian"/></option>
	<option value="15p"><spring:message code="ssap.page13.wardOfParentCaretaker"/></option>
	<option value="16"><spring:message code="ssap.page13.stepParent" text="Stepparent (stepfather or stepmother)"/></option>
	<option value="17"><spring:message code="ssap.page13.stepChild" text="Stepchild (stepson or stepdaughter)"/></option>
	<option value="19"><spring:message code="ssap.page13.childSonDaughter" text="Child (son or daughter)"/></option>
	<option value="31"><spring:message code="ssap.page13.courtAppointedGuardian" text="Court-appointed guardian"/></option>
</select>

<select id="parentRelationshipOptions" class="hide" aria-label="Relationship options">
	<option value=""><spring:message code="ssap.page13.selectRelationship" text="Select Relationship"/></option>
	<option value="09"><spring:message code="ssap.page13.adoptedChild" text="Adopted child (son or daughter)"/></option>
	<option value="10"><spring:message code="ssap.page13.fosterChild" text="Foster child (foster son or foster daughter)"/></option>
	<option value="19"><spring:message code="ssap.page13.childSonDaughter" text="Child (son or daughter)"/></option>
</select>
	
					
<div id="over26DependentAlert" class="modal hide fade" tabindex="-1" role="dialog">
  <div class="modal-header">
  </div>
  <div class="modal-body">
    <p><spring:message code="ssap.bloodRel.over26"/> <strong class="primaryContactName"></strong>.<p>  	
	<p><spring:message code="ssap.bloodRel.notList"/></p>
	<p>(<spring:message code="ssap.bloodRel.25Ward"/>)</p>
	<div id="over26DependentList"></div>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" onclick="showHouseHoldContactSummary();next();">Continue</button>
  </div>
</div>					