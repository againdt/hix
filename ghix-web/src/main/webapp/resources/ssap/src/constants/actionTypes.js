
// CommonAction type for State clearing

const CLEAR_STATE = 'CLEAR_STATE';
export const commonActionTypes = {
    CLEAR_STATE
};


// ActivePage action types

const CHANGE_PAGE = 'CHANGE_PAGE';

export const activePageActionTypes = {
    CHANGE_PAGE,
    CLEAR_STATE
};


// Adjusted flow action types

const ADJUST_FLOW = 'ADJUST_FLOW';

export const adjustedFlowActionTypes = {
    ADJUST_FLOW,
    CLEAR_STATE
};


// Blood relations action types

const SET_RELATIONS = 'SET_RELATIONS';
const SET_RELATIONS_WHOLE_LIST = 'SET_RELATIONS_WHOLE_LIST';

export const bloodRelationshipsTypes = {
    SET_RELATIONS,
    SET_RELATIONS_WHOLE_LIST,
    CLEAR_STATE
};


// Context action types

const ADD_CONTEXT = 'ADD_CONTEXT';
const REMOVE_CONTEXT = 'REMOVE_CONTEXT';

export const contextActionTypes = {
    ADD_CONTEXT,
    REMOVE_CONTEXT,
    CLEAR_STATE
};


// Console action types

const SWITCH_CONSOLE_MODE = 'SWITCH_CONSOLE_MODE';

export const consoleActionTypes = {
    SWITCH_CONSOLE_MODE,
    CLEAR_STATE
};


// Counties action types

const REMOVE_COUNTY = 'REMOVE_COUNTY';
const SET_COUNTIES = 'SET_COUNTIES';
const SET_COUNTY_FOUND_BY_STATE_AND_ZIPCODE = 'SET_COUNTY_FOUND_BY_STATE_AND_ZIPCODE';

export const countiesActionTypes = {
    REMOVE_COUNTY,
    SET_COUNTIES,
    SET_COUNTY_FOUND_BY_STATE_AND_ZIPCODE,
    CLEAR_STATE
};


// Countries action types

const SET_COUNTRIES = 'SET_COUNTRIES';

export const countriesActionTypes = {
    SET_COUNTRIES,
    CLEAR_STATE
};


// Data (JSON) action types

const CHANGE_DATA = 'CHANGE_DATA';
const SET_DATA = 'SET_DATA';

export const dataActionTypes = {
    CHANGE_DATA,
    SET_DATA,
    CLEAR_STATE
};


// Member Affordability (JSON) action types

const SET_MEMBER_AFFORDABILITY = 'SET_MEMBER_AFFORDABILITY';

export const memberAffordabilityTypes = {
    SET_MEMBER_AFFORDABILITY,
    CLEAR_STATE
};


// Error action types

const SET_ERROR = 'SET_ERROR';
const REMOVE_ERROR = 'REMOVE_ERROR';

export const errorActionTypes = {
    SET_ERROR,
    REMOVE_ERROR,
    CLEAR_STATE
};


// Exception action types

const SET_EXCEPTION = 'SET_EXCEPTION';
const REMOVE_EXCEPTION = 'REMOVE_EXCEPTION';
const UPDATE_EXCEPTION = 'UPDATE_EXCEPTION';

export const exceptionActionTypes = {
    SET_EXCEPTION,
    REMOVE_EXCEPTION,
    UPDATE_EXCEPTION,
    CLEAR_STATE
};


// Instructions action types

const SET_INSTRUCTIONS = 'SET_INSTRUCTIONS';

export const instructionsActionTypes = {
    SET_INSTRUCTIONS,
    CLEAR_STATE
};


// Form action types

const CHANGE_INPUT_STATUS = 'CHANGE_INPUT_STATUS';
const CLEAR_SUBMISSION = 'CLEAR_SUBMISSION';
const REGISTER_INPUT = 'REGISTER_INPUT';
const RE_REGISTER_INPUT = 'RE_REGISTER_INPUT';
const REMOVE_FORM = 'REMOVE_FORM';
const REMOVE_INPUT = 'REMOVE_INPUT';
const SUBMIT_FORM = 'SUBMIT_FORM';
const VALIDATE_FORM = 'VALIDATE_FORM';

export const formActionTypes = {
    CHANGE_INPUT_STATUS,
    CLEAR_SUBMISSION,
    REGISTER_INPUT,
    RE_REGISTER_INPUT,
    REMOVE_FORM,
    REMOVE_INPUT,
    SUBMIT_FORM,
    VALIDATE_FORM,
    CLEAR_STATE
};


// Flow action types

const SET_FLOW = 'SET_FLOW';

export const flowActionTypes = {
    SET_FLOW,
    CLEAR_STATE
};


// Header Data (JSON) action types

const SET_ENVIRONMENTS = 'SET_ENVIRONMENTS';

export const headerFooterLinkTypes = {
    SET_ENVIRONMENTS,
    CLEAR_STATE
};


// Language action types

const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';

export const languageActionTypes = {
    CHANGE_LANGUAGE,
    CLEAR_STATE
};


// Methods action types

const SET_METHODS = 'SET_METHODS';
const CLEAR_METHODS = 'CLEAR_METHODS';

export const methodsActionTypes = {
    SET_METHODS,
    CLEAR_METHODS,
    CLEAR_STATE
};


// Messages action types

const SET_MESSAGES = 'SET_MESSAGES';

export const messagesActionTypes = {
    SET_MESSAGES,
    CLEAR_STATE
};


// Modal action types

const SHOW_MODAL = 'SHOW_MODAL';
const HIDE_MODAL = 'HIDE_MODAL';
const HIDE_ALL_MODALS = 'HIDE_ALL_MODALS';

export const modalActionTypes = {
    SHOW_MODAL,
    HIDE_MODAL,
    HIDE_ALL_MODALS,
    CLEAR_STATE
};


// Scroll action types

const DEFINE_ELEMENT_FOR_SCROLLING = 'DEFINE_ELEMENT_FOR_SCROLLING';
const CLEAR_ELEMENT_FOR_SCROLLING = 'CLEAR_ELEMENT_FOR_SCROLLING';

export const scrollActionTypes = {
    DEFINE_ELEMENT_FOR_SCROLLING,
    CLEAR_ELEMENT_FOR_SCROLLING,
    CLEAR_STATE
};


// Session action types

const REFRESH_SESSION = 'REFRESH_SESSION';

export const sessionActionTypes = {
    REFRESH_SESSION,
    CLEAR_STATE
};


// Spinner action types

const START_SPINNER = 'START_SPINNER';
const STOP_SPINNER = 'STOP_SPINNER';

export const spinnerActionTypes = {
    START_SPINNER,
    STOP_SPINNER,
    CLEAR_STATE
};


// Tribe action types

const SET_TRIBES = 'SET_TRIBES';
const CLEAR_TRIBES = 'CLEAR_TRIBES';

export const tribesActionTypes = {
    SET_TRIBES,
    CLEAR_TRIBES,
    CLEAR_STATE
};


// Switch directions action types

const CHANGE_DIRECTION = 'CHANGE_DIRECTION';

export const switchDirectionsActionTypes = {
    CHANGE_DIRECTION,
    CLEAR_STATE
};

