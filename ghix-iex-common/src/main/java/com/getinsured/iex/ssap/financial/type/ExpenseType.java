package com.getinsured.iex.ssap.financial.type;

/**
 * Describes expense types.
 *
 * @author Yevgen Golubenko
 * @since 4/1/19
 */
public enum ExpenseType {
  /**
   * Alimony expense.
   */
  ALIMONY,

  /**
   * Other expense.
   */
  OTHER,

  /**
   * Student Loan Interest expense.
   */
  STUDENT_LOAN_INTEREST,

  /**
   * Not specified expense type,
   * default.
   */
  UNSPECIFIED
}
