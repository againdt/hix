import React, { Fragment } from 'react';

// Components

import AddressItem from './AddressItem';
import SummaryItem from './SummaryItem';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';


const AddressSummaryOfPerson = props => {

    const {
        ButtonContainer,
        data,
        householdMembers
    } = props;

    const {
        otherAddresses
    } = data.singleStreamlinedApplication.taxHousehold[0];

    return householdMembers.map((member, i) => {
        const {
            addressId,
            name,
            householdContact: {
                homeAddress,
                mailingAddress,
                mailingAddressSameAsHomeAddressIndicator
            },
            livesAtOtherAddressIndicator
        } = member;

        const getHomeAddress = () => {

            return i === 0 ?
                <AddressItem
                    label='Home Address'
                    {...homeAddress}
                /> :
                <AddressItem
                    label={livesAtOtherAddressIndicator ? 'Other Address' : 'Home Address'}
                    {...(otherAddresses.find(a => a.addressId === addressId))}
                />;
        };

        const getMailingAddress = () => {
            return i !== 0 ? null : mailingAddressSameAsHomeAddressIndicator ?

                <SummaryItem
                    category='Mailing Address'
                    value='Same as home address'
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                /> :
                <AddressItem
                    label='Mailing Address'
                    {...mailingAddress}
                />;
        };

        return (
            <div className='summary__block' key={`${member.name.firstName}${i}`}>
                <div className="usa-grid usa-width-one-whole summary__header">
                    <h4 className="usa-width-two-thirds">
                        <Fragment>
                            {normalizeNameOfPerson(name)}
                            {i === 0 ? <small> (Primary Contact) </small> : ''}
                        </Fragment>
                    </h4>
                    <div className="usa-width-one-third" style={{textAlign: 'right'}}>
                        {
                            <ButtonContainer
                                customButtonClassName="btn-small"
                                type='editButton_secondary'
                                args={['editAddress', i]}
                            />
                        }
                    </div>

                </div>
                { getHomeAddress() }
                { getMailingAddress() }
            </div>

        );
    });
};

export default AddressSummaryOfPerson;
