<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/ng-template" id="qepeligible">
  <div class="row-fluid">
    <div id="rightpanel">
      <div class="header">
        <h4><spring:message code="lce.qepeligible.qualifyingEvents"/></h4>
      </div><!-- header -->

      <section class="gutter20">
        <h3><spring:message code="lce.qepeligible.qualifiedForSEP"/></h3>
        <div class="margin20 gutter10 alert alert-info">
          <p><spring:message code="lce.qepeligible.qualifiedForSEPContent1"/></p>
          <p><spring:message code="lce.qepeligible.qualifiedForSEPContent2"/></p>
		  <p><spring:message code="lce.qepeligible.qualifiedForSEPContent3"/></p>
        </div>
        <div class="margin20 gutter10">
          <p><strong><spring:message code="lce.qepeligible.SEPAdviceTitle"/></strong></p>
          <ul>
            <li><spring:message code="lce.qepeligible.SEPAdviceBPt1"/></li>
            <li><spring:message code="lce.qepeligible.SEPAdviceBPt2"/></li>
            <li><spring:message code="lce.qepeligible.SEPAdviceBPt3"/></li>
          </ul>
        </div>
      </section>



      <!-- NEXT BUTTON -->
      <dl class="dl-horizontal pull-right">
        <a class="btn btn-primary" ng-click="startYourAppEvent()">
          <spring:message code="lce.qepeligible.applyBtn"/>
        </a>
      </dl><!-- NEXT BUTTON -->



  <!-- Modal-->
    <div ng-show= "showDialog" id="financialHelp" modal-show="showDialog" class="modal fade bigger-modal" tabindex="-1" role="dialog" aria-labelledby="financialHelpModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="financialHelpModal"><spring:message code="indportal.portalhome.financialhelp" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
			<sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user" var="showMessage"/>
			  <c:choose>
				<c:when test="${not showMessage}">
					<p><spring:message code="indportal.portalhome.restrictusertoredirectidaholink" javaScriptEscape="true"/><a href="javascript:void(0)" onclick="openInNewTab('<spring:message code="indportal.portalhome.idalinkUrl"/>')"> <spring:message code="indportal.portalhome.idalinkUrl"/></a>. <spring:message code="indportal.portalhome.restrictusertoredirectidaholinknext" javaScriptEscape="true"/></p>
				</c:when>
			    <c:otherwise>
				 <p><spring:message code="indportal.portalhome.doyouwanttoapplyforfinancialhelp" javaScriptEscape="true"/></p>
			    </c:otherwise>
			  </c:choose>
           <!-- <p><spring:message code="indportal.portalhome.doyouwanttoapplyforfinancialhelp" javaScriptEscape="true"  htmlEscape="false"/></p>
			<sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user" var="showMessage"/>
			<c:if test="${not showMessage}">
			<p><spring:message code="indportal.portalhome.restrictusertoredirectidaholink" javaScriptEscape="true"/><a href="javascript:void(0)" onclick="openInNewTab('<spring:message code="indportal.portalhome.idalinkUrl"/>')"> <spring:message code="indportal.portalhome.idalinkUrl"/></a>. <spring:message code="indportal.portalhome.restrictusertoredirectidaholinknext" javaScriptEscape="true"/></p>
			</c:if>-->
            <!-- <p><spring:message code="indportal.portalhome.ifyouselecttoapplyforfinancialhelp" javaScriptEscape="true"/></p>-->
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></button>
			<sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user">
            	<a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="openInSameTab('<%=GhixConstants.APP_URL%>individual/saml/idalinkredirect')"><spring:message code="indportal.portalhome.yestakemetoidalink" javaScriptEscape="true"/></a>
            </sec:accesscontrollist>
            <button class="btn btn-primary" data-dismiss="modal" ng-click="goToSsapQEP()"><spring:message code="indportal.portalhome.stayon" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
	<!-- Modal-->
    </div><!-- rightside -->
  </div> <!-- row-fluid -->
</script>
