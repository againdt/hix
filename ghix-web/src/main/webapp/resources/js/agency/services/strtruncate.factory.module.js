(function(){
    'use strict';

    angular.module('truncateFactoryModule', [])
    .filter('trunc', truncFilter);

	
	function truncFilter(){
		return function (input, wordwise, max, tail) {
			if (!input) return '';
			if (!max) return input;
			if (input.length <= max) return input;
			input = input.substr(0, max);
			if (wordwise) {
                var lastspace = input.lastIndexOf(' ');
                if (lastspace !== -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (input.charAt(lastspace-1) === '.' || input.charAt(lastspace-1) === ',') {
                    lastspace = lastspace - 1;
                  }
                  input = input.substr(0, lastspace);
                }
            }
            return input + (tail || ' …');
		};
	}

	
})();