//#################################################################################
//#################################################################################
//#################################################################################
//#################################################################################
//
//
//Please replace highlight.js in node module with this file
//Complete path: /ghix-web/src/main/webapp/resources/angular/dst/node_modules/@ng-bootstrap/ng-bootstrap/typeahead/highlight.js
//
//#################################################################################
//#################################################################################
//#################################################################################


import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { regExpEscape, toString } from '../util/util';
var NgbHighlight = (function () {
    function NgbHighlight() {
        this.highlightClass = 'ngb-highlight';
    }
    NgbHighlight.prototype.ngOnChanges = function (changes) {
        var resultStr = toString(this.result);
        var resultLC = resultStr.toLowerCase().split(' ');
        var termLC = toString(this.term).toLowerCase().split(' ');
        
        var currentIdx = 0;
        if (termLC.length > 0) {
        	this.parts = [];
        	for(var i = 0; i < resultLC.length; i ++) {
        		var isMatched = false;
        		
        		for(var j = 0; j < termLC.length; j ++) {
        			if(resultLC[i].indexOf(termLC[j]) === 0) {
        				isMatched = true;
        				var arr = resultLC[i].split(new RegExp("(" + regExpEscape(termLC[j]) + ")")).map(function (part) {
    						var originalPart = resultStr.substr(currentIdx, part.length);
    						currentIdx += part.length;
    						return originalPart;
    					});
            			
            			
            			this.parts = this.parts.concat(arr);
            			break;
        			}
					
        		}
        		
        		if(isMatched === false) {
        			this.parts.push(resultLC[i].charAt(0).toUpperCase() + resultLC[i].slice(1));
        			currentIdx += resultLC[i].length;
        		}
        		
        		this.parts.push(' ');
        		currentIdx ++;
        		
        	}           
        }
        else {
            this.parts = [resultStr];
        }
    };
    NgbHighlight.decorators = [
        { type: Component, args: [{
                    selector: 'ngb-highlight',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    template: "<ng-template ngFor [ngForOf]=\"parts\" let-part let-isOdd=\"odd\">" +
                        "<span *ngIf=\"isOdd\" class=\"{{highlightClass}}\">{{part}}</span><ng-template [ngIf]=\"!isOdd\">{{part}}</ng-template>" +
                        "</ng-template>",
                    // template needs to be formatted in a certain way so we don't add empty text nodes
                    styles: ["\n    .ngb-highlight {\n      font-weight: bold;\n    }\n  "]
                },] },
    ];
    /** @nocollapse */
    NgbHighlight.ctorParameters = function () { return []; };
    NgbHighlight.propDecorators = {
        "highlightClass": [{ type: Input },],
        "result": [{ type: Input },],
        "term": [{ type: Input },],
    };
    return NgbHighlight;
}());
export { NgbHighlight };
//# sourceMappingURL=highlight.js.map