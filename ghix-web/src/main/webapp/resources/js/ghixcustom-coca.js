// JavaScript Document

$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });


// CoCA Theme 
$(document).ready(function(){
	if ($('.row-fluid > .span3').length===0){
		$('#main .container').css({
			'background-image':'none',
			'background-color':'#ffffff',
		});
	}
	else {}
	
	$('h1').addClass('offset3');
	$('.span9 h1').removeClass('offset3');
	$('.span3 h1').removeClass('offset3');
	$('.span9 h4.span10').removeClass('span10').addClass('span8');
	
	// Remove Breadcrumbs - HIX-10721
	$(".breadcrumb, .page-breadcrumb, #breadcrumbs, .l-page-breadcrumb").remove();
	
	//Hightlight <tr> on check
  	$('.checkbox input').change(function(){
  	    var c = this.checked  ? '#F2CE40' : '#fff';
  	    $(this).parents('tr').css('background', c);
  	}); 
  	  	
  	$('input[name="check_allActive"]').click(function() {
		  
		  if( $('input[name="check_allActive"]').is(":checked") )
		  {
			  $('input[name="indCheckboxActive"]').each( function() {
				  $(this).attr("checked",true);
				  $(this).parents('tr').css('background', '#F2CE40');
			  });
			  
		  } else {
			  $('input[name="indCheckboxActive"]').each( function() {
				  $(this).attr("checked",false);
				  $(this).parents('tr').css('background', 'none');
			  });
		  }
	  });
  //Hightlight <tr> on check ENDS
  	
  	/*code for when single checkbox event is changed from the listing page. Refer HIX-18962*/
  	$('input[name="indCheckboxActive"]').change(function() {
        if (!$(this).is(':checked')) {
            $(this).attr('checked',false);
            $(this).parents('tr').css('background', 'none');
        }
        else{
        	$(this).attr('checked',true);
        	$(this).parents('tr').css('background', '#F2CE40');
        }
        
    });
  	
  	$('li#locationAndHours ul li').hide();
  	
  	if ($('#sidebar').length > 0){
		if($('#rightpanel').length > 0){
			$('#sidebar').closest('.row-fluid').before('<a href="#rightpanel" name="skip" class="skip-side-nav" accesskey="s" >Skip To Main Content</a>'); 
		}else if($('.span9').length > 0){
			$('.span9').attr('id','rightpanel');
			$('#sidebar').closest('.row-fluid').before('<a href="#rightpanel" name="skip" class="skip-side-nav" accesskey="s" >Skip To Main Content</a>'); 
		} 	
	}
	//skip sidebar - set tabindex to be -1 when click 
	$(".skip-side-nav").bind('click', function() {
		if ($('#sidebar').length > 0){
			$('#sidebar *').attr('tabindex',-1);
		}
	});
	//skip sidebar - set sidebar tabindex back to 0
	$('*').bind('focusin', function() {
		if ($('#sidebar').length > 0){
			$('#sidebar a').attr('tabindex',0);
		}
	});
  	if ($('#sidebar').length > 0){
		if($('#rightpanel').length > 0){
			$('#sidebar').closest('.row-fluid').before('<a href="#rightpanel" name="skip" class="skip-side-nav" accesskey="s" >Skip To Main Content</a>'); 
		}else if($('.span9').length > 0){
			$('.span9').attr('id','rightpanel');
			$('#sidebar').closest('.row-fluid').before('<a href="#rightpanel" name="skip" class="skip-side-nav" accesskey="s" >Skip To Main Content</a>'); 
		} 	
	}
	//skip sidebar - set tabindex to be -1 when click 
	$(".skip-side-nav").bind('click', function() {
		if ($('#sidebar').length > 0){
			$('#sidebar *').attr('tabindex',-1);
		}
	});
	//skip sidebar - set sidebar tabindex back to 0
	$('*').bind('focusin', function() {
		if ($('#sidebar').length > 0){
			$('#sidebar a').attr('tabindex',0);
		}
	});
	
	//replacing fontawesome with client's icons
	$('i.icon-print').each(function(){
		/*commented this line as it was creating a BLOCKER issue in IE8. Refer HIX-18633, HIX-18630*/
		/*$(this).wrap('<img class="margin5-r" src="../resources/img/CA-Print.png" alt="print file" />').hide();*/
	});
	
	$('i.icon-star').each(function(){
		$(this).wrap('<img class="margin5-r" src="../resources/img/CA-Star-On.png" alt="star" />').hide();
	});
	
	$('i.icon-star-empty').each(function(){
		$(this).wrap('<img class="margin5-r" src="../resources/img/CA-Star-Off.png"  alt="empty star" />').hide();
	});
	
	$('i.icon-shopping-cart').each(function(){
		$(this).wrap('<img class="margin5-r" src="../resources/img/CA-Cart.png"alt="shopping cart" />').hide();
	});
});




