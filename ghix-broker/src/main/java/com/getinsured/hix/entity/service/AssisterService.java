package com.getinsured.hix.entity.service;

import com.getinsured.hix.model.entity.Assister;

/**
 * Encapsulates service layer method calls for Assister
 */
public interface AssisterService {

	/**
	 * Generates partial record for the given Assister from third party and
	 * return the generated sequence id
	 * 
	 * @param assister
	 *            object with partial Assister information like business legal
	 *            name,state ein etc.
	 * @return the partially generated Assister id
	 */
	Integer generateAssisterRecordId(Assister assister);
	
	/**
	 * Finds the assister by given id
	 * 
	 * @param assisterId
	 *            the given assister id
	 * @return the existing assister if found
	 */
	Assister findById(int assisterId);
}