package com.getinsured.hix.batch.enrollment.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;

@Service("enrollmentCaAutoTermService")
@Transactional
public class EnrollmentCaAutoTermServiceImpl implements EnrollmentCaAutoTermService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCaAutoTermServiceImpl.class);
	
	@Autowired private UserService userService;
	@Autowired private LookupService lookupService;
	@Autowired	private IEnrolleeRepository enrolleeRepository;
	@Autowired	private IEnrollmentRepository enrollmentRepository;
	@Autowired 	private JobService jobService;
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void processAutoTermEnrollment(Integer dateYear, long jobExecutionId, String hiosIssuerId, Boolean autoTermRenewals, Boolean generate834) throws Exception{
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, dateYear);
		autoTermRenewals = autoTermRenewals == null ? true : autoTermRenewals;
		generate834 = generate834 == null ? true : generate834;		
		
		Date effectiveEndDate = DateUtil.getYearStartDate(calendar.getTime(),DateUtil.StartEndYearEnum.END);
		
		List<Integer> enrollmentIdList = new ArrayList<Integer>();
		List<Long> priorEnrollmentIdList = new ArrayList<Long>();
		
		if (hiosIssuerId != null) {
			enrollmentIdList = enrollmentRepository.getEnrollmentIdsForAutoTerminationUsingHiosId(dateYear.toString(), hiosIssuerId);
			priorEnrollmentIdList = enrollmentRepository.getPriorEnrollmentIdUsingHiosId(Integer.valueOf(dateYear + 1).toString(), hiosIssuerId);
		} else {
			enrollmentIdList = enrollmentRepository.getEnrollmentIdsForAutoTerminationCA(dateYear.toString());
			priorEnrollmentIdList = enrollmentRepository.getPriorEnrollmentId(Integer.valueOf(dateYear + 1).toString());
		}
		
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_EXCHANGE);
		for (Integer enrollmentId : enrollmentIdList){
			
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			try{
				String sendTocarrieFlag = "true";
				if(priorEnrollmentIdList.contains(Long.valueOf(enrollmentId.longValue())) && !autoTermRenewals){
					continue;
				}
				if(priorEnrollmentIdList.contains(Long.valueOf(enrollmentId.longValue())) && !generate834){
					sendTocarrieFlag="false";
				}else{
					/**
					 * For Enrollment get Household case Id
					 * Check if HouseHold has enrollment in Year + 1 in Non-cancel status
					 * Check if same Insurance type
					 * Check if same issuer
					 * 
					 * If all true then SET => sendTocarrieFlag="false";
					 */
					Enrollment enrollment = enrollmentRepository.findById(enrollmentId);
					String hoisIssuerId = null;
					if(enrollment!=null && enrollment.getCMSPlanID()!=null){
						hoisIssuerId = enrollment.getCMSPlanID().substring(0, 5);
						Long newEnrollmentCount = 
								enrollmentRepository.getEnrollmentCountByHouseHoldIdTypeAndIssuer(enrollment.getHouseHoldCaseId(), 
								                                                                  enrollment.getInsuranceTypeLkp().getLookupValueCode(), 
								                                                                  hoisIssuerId, 
								                                                                  Integer.valueOf(dateYear + 1).toString() );
						if(newEnrollmentCount > 0 ){
							sendTocarrieFlag="false";
						}
					}
				}
				
				/**Call disenrollment */
				disEnrollForAutoTermination(enrollmentId,effectiveEndDate,sendTocarrieFlag,user);
			}
			catch (Exception exception){
				LOGGER.error("Auto Termination Failed for Enrollment :: " + enrollmentId);
			}
		}
	}
	
	private void disEnrollForAutoTermination(Integer enrollmentId,Date effectiveEndDate,String sendTocarrieFlag,AccountUser user){
		Enrollment enrollment = enrollmentRepository.findOne(enrollmentId);
		
		List<EnrollmentEvent> enrollmentEventList = new ArrayList<EnrollmentEvent>();
		List<Enrollee> enrolleeList = new ArrayList<Enrollee>();
		EnrollmentEvent enrollmentEvent = null;
		enrollment.setUpdatedBy(user);
		enrollment.setEnrollmentStatusLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
		enrollment.setBenefitEndDate(effectiveEndDate);//set to end date of the year of effective start date
		for (Enrollee enrollee :  enrolleeRepository.getEnrolledEnrolleesForEnrollmentID(enrollment.getId())){
			if(enrollee!=null){
				enrollee.setEffectiveEndDate(effectiveEndDate);//set to end date of the year of effective start date
				enrollee.setEnrolleeLkpValue(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.ENROLLMENT_STATUS,EnrollmentConstants.ENROLLMENT_STATUS_TERM));
				enrollee.setUpdatedBy(user);
				enrollmentEvent = new EnrollmentEvent();
				enrollmentEvent.setTxnIdentifier(EnrollmentEvent.TRANSACTION_IDENTIFIER.AUTO_DISENROLL_BATCH.toString());
				enrollmentEvent.setEnrollee(enrollee);
				enrollmentEvent.setEnrollment(enrollee.getEnrollment());
				enrollmentEvent.setEventTypeLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_TYPE,EnrollmentConstants.EVENT_TYPE_CANCELLATION));
				enrollmentEvent.setEventReasonLkp(lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.EVENT_REASON, EnrollmentConstants.TERMINATION_REASON_TERMINATION_OF_BENIFITS));
				enrollmentEvent.setSendToCarrierFlag(sendTocarrieFlag);
				if(user!=null){
					enrollmentEvent.setCreatedBy(user);
				}
				enrollee.setLastEventId(enrollmentEvent);
				enrollmentEventList.add(enrollmentEvent);
				enrolleeList.add(enrollee);
			}
		}
		enrollment.setAbortTimestamp(new Date());
		enrollment.setEnrollmentEvents(enrollmentEventList);
		enrollment.setEnrollees(enrolleeList);
		enrollmentRepository.save(enrollment);
	}

}
