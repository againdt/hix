package com.getinsured.hix.indportal.dto.dm.application;

public class AmericanIndianAlaskanNative {
	 private String federallyRecognizedTribeName;
	 private boolean personRecognizedTribeIndicator;


	 // Getter Methods 

	 public String getFederallyRecognizedTribeName() {
	  return federallyRecognizedTribeName;
	 }

	 public boolean getPersonRecognizedTribeIndicator() {
	  return personRecognizedTribeIndicator;
	 }

	 // Setter Methods 

	 public void setFederallyRecognizedTribeName(String federallyRecognizedTribeName) {
	  this.federallyRecognizedTribeName = federallyRecognizedTribeName;
	 }

	 public void setPersonRecognizedTribeIndicator(boolean personRecognizedTribeIndicator) {
	  this.personRecognizedTribeIndicator = personRecognizedTribeIndicator;
	 }
	}
