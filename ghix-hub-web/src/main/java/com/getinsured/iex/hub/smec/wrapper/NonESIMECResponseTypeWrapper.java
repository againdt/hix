package com.getinsured.iex.hub.smec.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.NonESIMECIndividualResponseType;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.NonESIMECResponseType;


public class NonESIMECResponseTypeWrapper implements JSONAware{
	
	  private String responseCode;
	  private String responseDescription;
	  private NonESIMECIndividualResponseTypeWrapper nonESIMECIndividualResponseTypeWrapper;
	    
	public NonESIMECResponseTypeWrapper(NonESIMECResponseType nonESIMECResponseType) throws HubServiceException{
		if(nonESIMECResponseType != null)
		{
			NonESIMECIndividualResponseType nonESIMECIndividualResponseType = nonESIMECResponseType.getNonESIMECIndividualResponse();
			
			if(nonESIMECIndividualResponseType != null){
				nonESIMECIndividualResponseTypeWrapper = new NonESIMECIndividualResponseTypeWrapper(nonESIMECIndividualResponseType);
			}
			responseCode = nonESIMECResponseType.getResponseCode();
			responseDescription = nonESIMECResponseType.getResponseDescription();
		}
	}
	
	public String getResponseCode() {
		return responseCode;
	}
	
	public String getResponseDescription() {
		return responseDescription;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseCode", this.responseCode);
		obj.put("ResponseDescription", this.responseDescription);
		obj.put("NonESIMECIndividualResponseType", this.nonESIMECIndividualResponseTypeWrapper);
		return obj.toJSONString();
	}
}
