package com.getinsured.iex.hub.ridp.fars.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ridp.fars.cms.hix._0_1.hix_core.ResponseMetadataType;

/**
 * Wrapper class for com.getinsured.iex.hub.ridp.fars.cms.hix._0_1.hix_core.ResponseMetadataType
 * 
 * @author Nikhil Talreja
 * @since 20-Mar-2014
 * 
 */
public class ResponseMetadataWrapper implements JSONAware{
	
	private String responseCode;
	private String responseDescriptionText;
	private String tdsResponseDescriptionText;
    
    public ResponseMetadataWrapper(ResponseMetadataType responseMetadataType){
    	
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	if(responseMetadataType.getResponseCode() != null){
    		this.responseCode = responseMetadataType.getResponseCode().getValue();
    	}
    	if(responseMetadataType.getResponseDescriptionText() != null){
    		this.responseDescriptionText = responseMetadataType.getResponseDescriptionText().getValue();
    	}
    	if(responseMetadataType.getTDSResponseDescriptionText() != null){
    		this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText().getValue();
    	}
    }
    
	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescriptionText() {
		return responseDescriptionText;
	}

	public void setResponseDescriptionText(String responseDescriptionText) {
		this.responseDescriptionText = responseDescriptionText;
	}

	public String getTdsResponseDescriptionText() {
		return tdsResponseDescriptionText;
	}

	public void setTdsResponseDescriptionText(String tdsResponseDescriptionText) {
		this.tdsResponseDescriptionText = tdsResponseDescriptionText;
	}
    
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseCode", this.responseCode);
		obj.put("ResponseDescriptionText", this.responseDescriptionText);
		obj.put("TDSResponseDescriptionText", this.tdsResponseDescriptionText);
		return obj.toJSONString();
	}
	
}
