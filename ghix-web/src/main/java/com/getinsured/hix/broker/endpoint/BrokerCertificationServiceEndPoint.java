package com.getinsured.hix.broker.endpoint;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.XPath;
import org.dom4j.xpath.DefaultXPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.AbstractDom4jPayloadEndpoint;

import com.getinsured.hix.broker.service.BrokerCertificationService;

/**
 * {@link BrokerCertificationServiceEndPoint} endpoint handles the request for Certification Status
 * based upon license number.
 * This class extends {@link AbstractDom4jPayloadEndpoint} and overrides the
 * abstract method {@link AbstractDom4jPayloadEndpoint#invokeInternal(Element, Document)} to handle 
 * the message payload as dom4j elements. 
 * 
 */
public class BrokerCertificationServiceEndPoint extends AbstractDom4jPayloadEndpoint {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerCertificationServiceEndPoint.class);

	public static final String NAMESPACE = "http://localhost:8080/hixws/schemas";
	
	private XPath licenseNumberPath;
	
    private BrokerCertificationService brokerCertificationService;
    public BrokerCertificationServiceEndPoint()
    {
    	super();
    	Map<String,String> nameSpaceUris = new HashMap<String, String>();
    	nameSpaceUris.put("hixws",NAMESPACE);
    	licenseNumberPath =  new DefaultXPath("/hixws:GetAssisterCertificationRequest/hixws:licenseNumber");
    	licenseNumberPath.setNamespaceURIs(nameSpaceUris);
    	    	
    }
    
    public void setBrokerCertificationService(BrokerCertificationService brokerCertificationService) {
        this.brokerCertificationService = brokerCertificationService;
    }
    
    /**
     * Overrides the abstract method {@link AbstractDom4jPayloadEndpoint#invokeInternal(Element, Document)}
     * and handles the request for certification status.
     */
    protected Element invokeInternal(Element requestElement, Document document){
	    	LOGGER.info("invoke called");
	        Map<String,String> appData = new HashMap<String, String>();
	        appData.put("licenseNumber", licenseNumberPath.valueOf(requestElement));
	        LOGGER.info("test=============================" + licenseNumberPath.valueOf(requestElement) + ">> " + appData.toString());
	          
	        
	    	Map<String,String> responseData = brokerCertificationService.brokerService(appData);
	    	
	    	Element responseElement = document.addElement("GetAssisterCertificationResponse",NAMESPACE);
	       responseElement.addElement("licenseNumber").setText(responseData.get("licenseNumber"));
	       responseElement.addElement("certification_status").setText(responseData.get("certification_status"));
    	  
        return responseElement; 
    }

}