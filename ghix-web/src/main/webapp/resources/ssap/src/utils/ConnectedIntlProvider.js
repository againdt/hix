import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';

// Localization for English and Spanish

import { addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

addLocaleData([...en, ...es]);

/**
 *
 * @param {Object} props
 * @returns {React.Component}
 *
 * Here we inject in pre-built component <IntlProvider/> messages
 * (they go from Redux Store on successful download from server)
 * and make <IntlProvider/> to update every time when we switch languages
 */
const ConnectedIntlProvider = props => {
    const {
        children,
        language,
        messages
    } = props;

    return (
        <IntlProvider
            locale={language}
            messages={messages}
        >
            { children }
        </IntlProvider>
    );
};

const mapStateToProps = state => ({
    key: state.language, // Changing of language causes changing of the Key. It invokes update for <ConnectedIntlProvider/>
    language: state.language,
    messages: state.messages && state.messages[state.language]
});

export default connect(mapStateToProps)(ConnectedIntlProvider);
