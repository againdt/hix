<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<script type="text/javascript">
	
</script>
<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/employer/searchemployer"/>">Employers</a></li>
			<li>Summary</li>
		</ul>
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p>
					<c:out value="${errorMsg}"></c:out>
				<p />
			</c:if>
			<br>
		</div>
		<h1>
			<span class="span3">${employer.name}</span> <small class="span9">Contact
				Name: ${employer.contactFirstName} ${employer.contactLastName}</small>
		</h1>
	</div>
	<!--page-breadcrumb ends-->

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4>About this Employer</h4>
			</div>
			<ul class="nav nav-list">
				<li><a href="/hix/crm/crmemployer/details/${employerId}">Summary</a></li>
				<li class="navmenu"><a
					href="/hix/crm/crmemployer/employercontactinfo/${employerId}">Contact
						Info</a></li>
				<!-- <li class="navmenu disabled"><a href="#">Messages</a></li> -->
				<li>
				<a
					href="/hix/crm/crmemployer/employercomments/${employerId}">Comments</a></li>
				<li><a
					href="/hix/crm/crmemployer/ticket/history/${employerId}">Ticket
						History</a></li>
				<li class="active"><a href="#">Security Questions</a></li>
			</ul>
			<br>
			<div class="header">
				<h4><i class="icon-cog icon-white"></i> Actions</h4>
			</div>
			<ul class="nav nav-list">
				<c:if test="${checkDilog == null}">  
					<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employer Account</a></li>
				</c:if>
				<c:if test="${checkDilog != null}">  
					<li class="navmenu"><a href="/hix/account/user/switchUserRole?switchToModuleName=employer&switchToModuleId=${employerId}&switchToResourceName=${employer.name}"  role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Employer Account</a></li>
				</c:if>
				<li class="navmenu"><a href="<c:url value="/crm/crmemployer/passwordreset"/>/${employerId}?sourcePage=securityQuestions" role="button" data-toggle="modal"><i class="icon-envelope"></i><spring:message  code='label.sendPasswordResetLink'/></a></li>
				<!-- <li class="navmenu disabled"><a href="#"><i
						class="icon-envelope-unread"></i> Compose Message</a></li>

				<li class="navmenu disabled"><a href="#"><i
						class="icon-comment"></i> New Comment </a></li> -->
			</ul>
		</div>


		<div id="rightpanel" class="span9">
			<div class="header">
				<h4 class="span10">Security Questions</h4>
			</div>
			<div class="gutter10">
				<form class="form-horizontal" id="" name="" action="" method="POST">
					<table class="table table-border-none verticalThead">
						<!-- <thead>
						<tr class="graydrkbg">
							<th class="span3"><strong>Contact Info</strong></th>
							<th></th>
						</tr>
					</thead> -->
						<tbody>
							<tr>
								<td class="txt-left span4"><strong> Security
										Question 1:</strong></td>
							</tr>
							<tr>
								<td>${accountUser.securityQuestion1}</td>
							</tr>
							<tr>
								<td class="txt-left span4">Answer:</td>
							</tr>
							<tr>
								<td>${accountUser.securityAnswer1}</td>
							</tr>

<%-- 							
							<tr>
								<td class="txt-left span4"><strong> Security
										Question 3:</strong></td>
							</tr>
							<tr>
								<td>${accountUser.securityQuestion3}</td>
							</tr>
							<tr>
								<td class="txt-left span4">Answer:</td>
							</tr>
							<tr>
								<td>${accountUser.securityAnswer3}</td>
							</tr>
 --%>							
						</tbody>
					</table>
					<input type="hidden" name="employerName" id="employerName"
						value="${employer.name}" />
				</form>
			</div>
		</div>
	</div>
</div>

<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employer/home" />" novalidate="novalidate">
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Employer Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Employer View" will take you to the Employer's portal for ${employer.name}.<br/>
					Through this portal you will be able to take actions on behalf of the employer, such as view <br/>
					and edit employee list, fill out employer eligibility etc.<br/>
					Proceed to Employer view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkEmployerView" name="checkEmployerView"  > 
    <div class="pull-left">&nbsp; Don't show this message again.</div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
   <button class="btn btn-primary" type="submit">Employer View</button>
      <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employer" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${employerId}" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${employer.name}" />
    <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>

<!--  Latest UI -->