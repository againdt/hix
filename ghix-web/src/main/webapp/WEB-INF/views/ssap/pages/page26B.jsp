<%@ include file="../include.jsp" %>

<div id="appscr76P2"  style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page26.healthInsuranceFor" text="Health Insurance Information for" />
				<strong class="nameOfHouseHold"></strong>
			</h4>
		</div>
		<form id="appscr76P2Form" class="form-horizontal" data-parsley-excluded=":hidden">
		<div class="header">
			<h4><spring:message code="ssap.pageHeader.additionalQuestions" text="Additional Questions" /></h4>
		</div>
		<div class="gutter10">
			<div class="control-group">
				<p>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page26B.currentlyEnrolledIn" text="currently enrolled in health coverage from any of the following" />?
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="appscr76P2_cobra">
						<input type="radio" id="appscr76P2_cobra" name="appscr76p2radiosgroup" value="corba" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#appscr76P2_cobraErrorDiv"/>
						<span><spring:message code="ssap.page26B.cobra" text="COBRA" /></span>
					</label>
					
					<label class="radio" for="appscr76P2_retireHealthPlan">
						<input type="radio" id="appscr76P2_retireHealthPlan" name="appscr76p2radiosgroup" value="retire health plan" />
						<span><spring:message code="ssap.page26B.retireeHealthPlan" text="Retiree health plan" /></span>
					</label>
					
					<label class="radio" for="appscr76P2_veteransProgram">
						<input type="radio" id="appscr76P2_veteransProgram" name="appscr76p2radiosgroup" value="veterans health program" />
						<span><spring:message code="ssap.page26B.veteransHealthPlan" text="Veterans health program" /></span>
					</label>
					
					<label class="radio" for="appscr76P2_noneOfAbove">
						<input type="radio" id="appscr76P2_noneOfAbove" name="appscr76p2radiosgroup" value="none" />
						<span><spring:message code="ssap.noneOfTheAbove" text="None of the above" /></span>
					</label>
					<div id="appscr76P2_cobraErrorDiv"></div>
				</div>
			</div><!-- control-group ends -->
			
			<%-- <div class="control-group hide">
				<p>
					<spring:message code="ssap.does" text="Does" />
					<strong class="nameOfHouseHold"></strong>&nbsp;
					<spring:message code="ssap.page26B.haveAPrimaryCAre" text="have a primary care physician" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="checkbox">
						<input	type="radio" id="appscr76P2_physicianCareIndicatorYes" name="haveApPrimaryCarePhysician" value="yes"  /><spring:message code="ssap.yes" text="Yes" />
					</label>
					
					<label class="checkbox">
						<input type="radio" id="appscr76P2_physicianCareIndicatorNo" name="haveApPrimaryCarePhysician" value="no" checked="checked"  /><spring:message code="ssap.no" text="No" />
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends --> --%>
			
			<%-- <div id="haveApPrimaryCarePhysicianOnPageAppscr76Part2" class="hide">
				<div class="control-group">
					<label class="control-label">
						<spring:message code="ssap.primaryCarePhysician" text="Primary Care Physician Name" />
					</label><!-- end of label -->
					<div class="controls">
						<input id="appscr76P2_physicianName" name="" type="text" placeholder="<spring:message code='ssap.page26B.name' text='Name' />" value="" class="input-medium">
						<ul id="parsley-physician" class="parsley-error-list">
							<li class="required" style="display: list-item;">
								<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
							</li>
						</ul>
						<label class="checkbox">
							<input id="appscr76P2IDN" type="checkbox" /> 
							<span><spring:message code="ssap.iDontKnow" text="I don't know" /></span>
						</label>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
			</div> --%><!-- haveApPrimaryCarePhysicianOnPageAppscr76Part2 -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page26.will" text="Will" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code='ssap.page26B.beEnrolledIn' text='be enrolled in health coverage from any of the following in' />
					<span class='coverageYear'></span>?
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="appscr76P2_cobraForCoverage">
						<input type="radio" id="appscr76P2_cobraForCoverage" name="willBeEnrolledInHealthCoverageInFollowingYear" value="corba" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#appscr76P2_cobraForCoverageErrorDiv"/>
						<span><spring:message code="ssap.page26B.cobra" text="COBRA" /></span>
					</label>
					
					<label class="radio" for="appscr76P2_RHPForCoverage">
						<input type="radio" id="appscr76P2_RHPForCoverage" name="willBeEnrolledInHealthCoverageInFollowingYear" value="retiree health plan" />
						<span><spring:message code="ssap.page26B.retireeHealthPlan" text="Retiree health plan" /></span>
					</label>
					
					<label class="radio" for="appscr76P2_VHPForCoverage">
						<input type="radio" id="appscr76P2_VHPForCoverage" name="willBeEnrolledInHealthCoverageInFollowingYear" value="veterans health plans" />
						<span><spring:message code="ssap.page26B.veteransHealthPlan" text="Veterans health program" /></span>
					</label>
					
					<label class="radio" for="appscr76P2_NOTAForCoverage">
						<input type="radio" id="appscr76P2_NOTAForCoverage" name="willBeEnrolledInHealthCoverageInFollowingYear" value="none" />
						<span><spring:message code="ssap.noneOfTheAbove" text="None of the above" /></span>
					</label>
					<div id="appscr76P2_cobraForCoverageErrorDiv"></div>
				</div>
			</div><!-- control-group ends -->
			</div><!-- .gutter10 -->
		</form>
	</div>
	
	
	
	<div class="Main_ui_design hide">
		<form>
			<div class="input_tetx_area" style="display:none;">
				<br> <p class="cobaappDarkWithoutMarginTop"><spring:message code="ssap.page26B.tellUsAboutYourFormer" text="Tell us about your former employer" /></p>
			</div>

		<!-- starts here -->
		<div class="memberNameDataDiv1"  style="display:none;">
			<div class="input_tetx_area">
				<table class='lineHeight15'>
					<tr>
						<td><input type="checkbox" class='marginLeft28' id="nameOfHouseholdOnPage76P2"
							checked="checked" aria-label="Name of Household" /></td>
						<td><div class="cobaappLabel"> 
							<span class="houseHoldNameHere2" aria-label="Name of Household">

							</span></div></td>
					</tr>
				</table>
			</div>


<div id="tellUsaboutUrFormerEmployerAapscr76P2" style="display:none;">

			<div class="input_tetx_area">
				<br> <p class="cobaappDarkWithoutMarginTop noTopMargin marginLeft47"><spring:message code="ssap.page26.whoCanWeContact" text="Who can we contact at this employer?" /></p> <p class="cobaappLabel noLeftMargin">
					<spring:message code="ssap.page26.ifYouAreNot" text="If you are not sure, ask your employer (optional)" />  </p>
			</div>

			<div class="input_tetx_area">
				<table class='marginLeft47 lineHeight15'>
					<tr>
						<td colspan="2">
						<label for="employerContactName" class="aria-hidden"><spring:message code='ssap.page26B.employerContactName' text='Employer Contact Name' /></label>
						<input id="employerContactName"
							class="input_row_area_left noMargin width300" type="text"
							value="" placeholder="<spring:message code='ssap.page26B.employerContactName' text='Employer Contact Name' />" name=""></td>
						<td></td>
					<tr>
					<tr>
						<td colspan='2'><label
							class="cobaappDarkWithoutMarginTop noMargin"><spring:message code="ssap.phoneNo" text="Phone No" />
						</label></td>
						<td><label for="appscr76P2_firstPhoneNumber" class="cobaappDarkWithoutMarginTop noMargin"><spring:message code="ssap.phoneType" text="Phone Type" /></label></td>
					<tr>
					<tr>
						<td><input class="input_row_area_left noMargin" type="text"
							value="" placeholder="xxxxxxxxxx" id="appscr76P2_firstPhoneNumber" name=""></td>
						<td>
						<label for="appscr76P2_firstExt" class="aria-hidden"><spring:message code="ssap.phoneType" text="Phone Type" /></label>
						<input class="input_row_area_left negativeLeftMargin61Percent width55"
							type="text" value="" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" id="appscr76P2_firstExt" name=""></td>
						<td>
						
						<label for="appscr76P2_firstCell" class="aria-hidden"><spring:message code="ssap.phoneType" text="Phone Type" /></label>
						<select id="appscr76P2_firstCell">
										<option value="0"><spring:message code="ssap.phoneType" text="Phone Type" /></option>
										<option value="Cell"><spring:message code="ssap.cellPhone" text="Cell" /></option>
										<option value="Home"><spring:message code="ssap.homePhone" text="Home" /></option>
										<option value="Work"><spring:message code="ssap.workPhone" text="Work" /></option>
						</select>
						</td>
					<tr>
					<tr>
						<td colspan='3'><label
							class="cobaappDarkWithoutMarginTop noMargin"><spring:message code="ssap.email" text="Email" /> </label></td>
					<tr>
					<tr>
						<td colspan='3'><input
							class="input_row_area_left noMargin width300" type="text" value="" placeholder="<spring:message code='ssap.page26.emailAddress' text='Email Address' />" name=""></td>
					<tr>
				</table>

			</div>

</div>
</div>

			<div class="input_tetx_area"  style="display:none;">
				<table class='lineHeight15'>
					<tr>
						<td><input type="checkbox" class='marginLeft28' id="otherEmployeeOnPage76P2"
							checked="checked" /></td>
						<td><label class="cobaappLabel checkbox" for="otherEmployeeOnPage76P2"><spring:message code="ssap.page26.otherEmployer" text="Other Employer" /> </label></td>
					</tr>
				</table>
			</div>


<div id="secondDivToBeHideAndShowOnAppsce76P2"  style="display:none;">
			<div class="input_tetx_area">
				<table class='lineHeight15 marginLeft47'>
					<tr>
						<td><label class="cobaappDarkWithoutMarginTop noMargin" for="employerName"><spring:message code="ssap.page26.employerName" text="Employer Name" /> </label></td>
						<td><label class="cobaappDarkWithoutMarginTop noMargin" for="employerIdentificatiuonNumber"><spring:message code="ssap.page26.employerIdentificatiuonNumber" text="Employer Identification Number" /> </label></td>
					<tr>
					<tr>
						<td><input class="input_row_area_left noMargin width300"
							type="text" value="" placeholder="Employer Name " name="" id="employerName">
						</td>
						<td><input class="input_row_area_left noMargin" type="text"
							value="" placeholder="Employer EIN" name="" id="employerIdentificatiuonNumber"></td>
					<tr>
					<tr>
						<td colspan='2'><label
							class="cobaappDarkWithoutMarginTop noMargin" for="employerAddress"><spring:message code="ssap.page26A.employerAddress" text="Employer Address" /> </label></td>
					<tr>
					<tr>
						<td colspan='2'><input class="input_row_area_left noMargin width100" type="text" id="employerAddress"
							value="" placeholder="<spring:message code='ssap.page26.employerName' text='Employer Name' /> " name=""></td>
					<tr>
					<tr>
						<td colspan='2'>
						<label for="address1" class="aria-hidden"><spring:message code='ssap.address1' text='Address1' /></label>
						<input class="input_row_area_left noMargin width100" type="text"
							value="" placeholder="<spring:message code='ssap.address1' text='Address1' />" name=""></td>
					<tr>
					<tr>
						<td colspan='2'>
							<label for="city1" class="aria-hidden"><spring:message code='ssap.city' text='City' /></label>
						<input class="input_row_area_left  width150" type="text"
							value="" placeholder="<spring:message code='ssap.city' text='City' />" name=""> 
						<label for="zip1" class="aria-hidden"><spring:message code='ssap.zip' text='Zip' /></label>
							
							<input class="input_row_area_left  width150" type="text"
							value="" placeholder="<spring:message code='ssap.zip' text='Zip' />" name=""> 
							<label for="state1" class="aria-hidden"><spring:message code="ssap.State" text="State" /></label>
							<select class="input_row_area_left height24 width150" id="state1">
							<option value='0'><spring:message code="ssap.State" text="State" /></option>
								<option value='AL'><spring:message code="ssap.state.Albama" text="Alabama" /></option>
								<option value='AK'><spring:message code="ssap.state.Alaska" text="Alaska" /></option>
								<option value='AS'><spring:message code="ssap.state.AmericanSamoa" text="American Samoa" /></option>
								<option value='AZ'><spring:message code="ssap.state.Arizona" text="Arizona" /></option>
								<option value='AR'><spring:message code="ssap.state.Arkansas" text="Arkansas" /></option>
								<option value='CA'><spring:message code="ssap.state.California" text="California" /></option>
								<option value='CO'><spring:message code="ssap.state.Colorado" text="Colorado" /></option>
								<option value='CT'><spring:message code="ssap.state.Connecticut" text="Connecticut" /></option>
								<option value='DE'><spring:message code="ssap.state.Delaware" text="Delaware" /></option>
								<option value='DC'><spring:message code="ssap.state.DistrictOfColumbia" text="District of Columbia" /></option>
								<option value='FL'><spring:message code="ssap.state.Florida" text="Florida" /></option>
								<option value='GA'><spring:message code="ssap.state.Georgia" text="Georgia" /></option>
								<option value='GU'><spring:message code="ssap.state.Guam" text="Guam" /></option>
								<option value='HI'><spring:message code="ssap.state.Hawaii" text="Hawaii" /></option>
								<option value='ID'><spring:message code="ssap.state.Idaho" text="Idaho" /></option>
								<option value='IL'><spring:message code="ssap.state.Illinois" text="Illinois" /></option>
								<option value='IN'><spring:message code="ssap.state.Indiana" text="Indiana" /></option>
								<option value='IA'><spring:message code="ssap.state.Iowa" text="Iowa" /></option>
								<option value='KS'><spring:message code="ssap.state.Kansas" text="Kansas" /></option>
								<option value='KY'><spring:message code="ssap.state.Kentucky" text="Kentucky" /> </option>
								<option value='LA'><spring:message code="ssap.state.Louisiana" text="Louisiana" /></option>
								<option value='ME'><spring:message code="ssap.state.Maine" text="Maine" /></option>
								<option value='MD'><spring:message code="ssap.state.Maryland" text="Maryland" /></option>
								<option value='MA'><spring:message code="ssap.state.Massachusetts" text="Massachusetts" /></option>
								<option value='MI'><spring:message code="ssap.state.Michigan" text="Michigan" /></option>
								<option value='MN'><spring:message code="ssap.state.Minnesota" text="Minnesota" /></option>
								<option value='MS'><spring:message code="ssap.state.Mississippi" text="Mississippi" /></option>
								<option value='MO'><spring:message code="ssap.state.Missouri" text="Missouri" /></option>
								<option value='MT'><spring:message code="ssap.state.Montana" text="Montana" /></option>
								<option value='NE'><spring:message code="ssap.state.Nebraska" text="Nebraska" /></option>
								<option value='NV'><spring:message code="ssap.state.Nevada" text="Nevada" /></option>
								<option value='NH'><spring:message code="ssap.state.NewHampshire" text="New Hampshire" /></option>
								<option value='NJ'><spring:message code="ssap.state.NewJersey" text="New Jersey" /></option>
								<option value='NM'><spring:message code="ssap.state.NewMexico" text="New Mexico" /></option>
								<option value='NY'><spring:message code="ssap.state.NewYork" text="New York" /></option>
								<option value='NC'><spring:message code="ssap.state.NorthCarolina" text="North Carolina" /></option>
								<option value='ND'><spring:message code="ssap.state.NorthDakota" text="North Dakota" /></option>
								<option value='MP'><spring:message code="ssap.state.NorthernMarianasIslands" text="Northern Marianas Islands" /></option>
								<option value='OH'><spring:message code="ssap.state.Ohio" text="Ohio" /></option>
								<option value='OK'><spring:message code="ssap.state.Oklahoma" text="Oklahoma" /></option>
								<option value='OR'><spring:message code="ssap.state.Oregon" text="Oregon" /></option>
								<option value='PA'><spring:message code="ssap.state.Pennsylvania" text="Pennsylvania" /></option>
								<option value='PR'><spring:message code="ssap.state.PuertoRico" text="Puerto Rico" /></option>
								<option value='RI'><spring:message code="ssap.state.RhodeIsland" text="Rhode Island" /></option>
								<option value='SC'><spring:message code="ssap.state.SouthCarolina" text="South Carolina" /></option>
								<option value='SD'><spring:message code="ssap.state.SouthDakota" text="South Dakota" /></option>
								<option value='TN'><spring:message code="ssap.state.Tennessee" text="Tennessee" /></option>
								<option value='TX'><spring:message code="ssap.state.Texas" text="Texas" /></option>
								<option value='UT'><spring:message code="ssap.state.Utah" text="Utah" /></option>
								<option value='VT'><spring:message code="ssap.state.Vermont" text="Vermont" /></option>
								<option value='VA'><spring:message code="ssap.state.Virginia" text="Virginia" /></option>
								<option value='VI'><spring:message code="ssap.state.VirginIslands" text="Virgin Islands" /></option>
								<option value='WA'><spring:message code="ssap.state.Washington" text="Washington" /></option>
								<option value='WV'><spring:message code="ssap.state.WestVirginia" text="West Virginia" /></option>
								<option value='WI'><spring:message code="ssap.state.Wisconsin" text="Wisconsin" /></option>
								<option value='WY'><spring:message code="ssap.state.Wyoming" text="Wyoming" /></option>
							
							</select> <!-- It should be select box - harsh -->
						</td>
					<tr>
					<tr>
						<td colspan='2'><label
							class="cobaappDarkWithoutMarginTop noMargin" for="appscr76P2_secondPhoneNumber"><spring:message code="ssap.page26.employerPhoneNumber" text="Employer Phone Number" /> </label></td>
					<tr>
					<tr>
						<td colspan='2'><input class="input_row_area_left noMargin"
							type="text" value="" placeholder="xxxxxxxxxx" id="appscr76P2_secondPhoneNumber" name="">
							<input class="input_row_area_left marginLeft5 noTopMargin width55" type="text"
							value="" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" id="appscr76P2_secondExt" name=""></td>
					<tr>
					<tr>
						<td colspan='2'><label
							class="cobaappDarkWithoutMarginTop noMargin"><spring:message code="ssap.page26.whoCanWeContact" text="Who can we contact at this employer?" /></label> <label
							class="cobaappLabel noMargin"> <spring:message code="ssap.page26.ifYouAreNot" text="If you are not sure, ask your employer (optional)" /> </label></td>
					<tr>
					<tr>
						<td colspan='2'><input
							class="input_row_area_left noMargin width300" type="text"
							value="" placeholder="Employer Contact Name " name=""></td>
					<tr>
					<tr>
						<td><label class="cobaappDarkWithoutMarginTop noMargin"><spring:message code="ssap.phoneNo" text="Phone No" /> </label></td>
						<td><label class="cobaappDarkWithoutMarginTop noMargin"><spring:message code="ssap.phoneType" text="Phone Type" /> </label></td>
					<tr>
					<tr>
						<td><input class="input_row_area_left noMargin" type="text"
							value="" placeholder="xxxxxxxxxx" id="appscr76P2_thirdPhoneNumber" name=""> <input
							class="input_row_area_left marginLeft5 noTopMargin width55" type="text" value=""
							placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" id="appscr76P2_thirdExt" name=""></td>
						<td><select aria-label="Phone type options">
								<option value="0"><spring:message code="ssap.phoneType" text="Phone Type" /></option>
								<option value="Cell"><spring:message code="ssap.cellPhone" text="Cell" /></option>
								<option value="Home"><spring:message code="ssap.homePhone" text="Home" /></option>
								<option value="Work"><spring:message code="ssap.workPhone" text="Work" /></option>
						</select></td>
					<tr>
					<tr>
						<td colspan='2'><label
							class="cobaappDarkWithoutMarginTop noMargin"><spring:message code="ssap.email" text="Email" /> </label></td>
					<tr>
					<tr>
						<td colspan='2'><input
							class="input_row_area_left noMargin width300" type="text"
							value="" placeholder="Email Address" name=""></td>
					<tr>
				</table>
			</div>
	</div>

		<!-- ends here -->
			<div class="input_tetx_area">
				<div class='horzLine'></div>
			</div>



		</form>

	</div>
</div>







