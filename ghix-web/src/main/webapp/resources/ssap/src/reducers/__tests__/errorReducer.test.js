
import errorReducer, { initialState } from '../errorReducer';
import { errorActionTypes } from '../../constants/actionTypes';

describe('Test fro errorReducer', () => {

    const anotherAction = {
        type: 'ANOTHER_ACTION',
        payload: []
    };

    const setErrorAction = {
        type: errorActionTypes.SET_ERROR,
        errorData: {
            errorMessage: 'New error message',
            errorType: 'New error type'
        }
    };

    const wrongValues = [
        [23],
        23,
        '43346',
        null,
        () => {}
    ];

    const previousState = {
        errorStatus: true,
        errorMessage: 'Previous error message',
        errorType: 'Previous error type'
    };

    const removeErrorAction = {
        type: errorActionTypes.REMOVE_ERROR
    };

    const clearStateAction = {
        type: errorActionTypes.CLEAR_STATE
    };

    describe('Test for errorReducer', () => {

        it('This reducer hasn\'t been invoked', () => {
            expect(errorReducer(initialState, anotherAction)).toEqual(initialState);
        });

        wrongValues.forEach(v => {
            it('Should throw an error', () => {
                expect(() => {
                    errorReducer(initialState, {
                        ...setErrorAction,
                        errorData: v
                    });
                }).toThrow('Invalid value (not an object) has been passed in errorReducer');
            });
        });

        it('Should set error data', () => {
            expect(errorReducer(initialState, setErrorAction)).toEqual({
                errorStatus: true,
                errorMessage: setErrorAction.errorData.errorMessage,
                errorType: setErrorAction.errorData.errorType
            });
        });

        it('Should remove error data', () => {
            expect(errorReducer(previousState, removeErrorAction)).toEqual(initialState);
        });

        it('Should clear state', () => {
            expect(errorReducer({ welcomeText: 'Hi there!' }, clearStateAction)).toEqual(initialState);
        });
    });


});