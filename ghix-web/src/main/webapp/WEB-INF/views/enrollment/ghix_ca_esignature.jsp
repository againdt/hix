<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<jsp:useBean id="now" class="java.util.Date" />

<style>
#container-wrap::after {top: 0px !important}
.agreement {
	background: #ebebeb
}

#eSignature {
	border-bottom: 1px solid #bbb;
}

.esign {
	background: #f2f2f2
}

.agree::-webkit-scrollbar {
	width: 10px;
	height: 10px;
}

.agree::-webkit-scrollbar-track {
	background: #fff;
}

.agree::-webkit-scrollbar-thumb {
	background: #bbb;
}
i.icon-print {
	color: inherit;
}
</style>
<script type="text/javascript">

    window.history.forward();
    function noBack() { window.history.forward();
    $("#submitButton").removeAttr("disabled");
    }
</script>
<input type="hidden" name="healthPlanIssuer" id="healthPlanIssuer" value="${healthPlanIssuer}" />
<input type="hidden" name="healthPlanName" id="healthPlanName" value="${healthPlanName}" />
<input type="hidden" name="healthPlanLevel" id="healthPlanLevel" value="${healthPlanLevel}" />

<input type="hidden" name="dentalPlanIssuer" id="dentalPlanIssuer" value="${dentalPlanIssuer}" />
<input type="hidden" name="dentalPlanName" id="dentalPlanName" value="${dentalPlanName}" />
<input type="hidden" name="dentalPlanLevel" id="dentalPlanLevel" value="${dentalPlanLevel}" />

<body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload=""></body>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript">

	// Variables for dataLayer
	var healthPlanIssuer = '${healthPlanIssuer}';
	var healthPlanName = '${healthPlanName}';
	var healthPlanLevel = '${healthPlanLevel}';

	var dentalPlanIssuer = '${dentalPlanIssuer}';
	var dentalPlanName = '${dentalPlanName}';
	var dentalPlanLevel = '${dentalPlanLevel}';

	var healthPlanInfo = healthPlanIssuer + ' | ' +  healthPlanName + ' | ' +  healthPlanLevel;
    var dentalPlanInfo = dentalPlanIssuer + ' | ' +  dentalPlanName + ' | ' +  dentalPlanLevel;

	
	if(healthPlanName != '' && dentalPlanName != '' ) {
		planInfo = healthPlanInfo + ' :: ' + dentalPlanInfo;
	}
	else if(healthPlanName != '' && dentalPlanName == '' ) {
		planInfo = healthPlanInfo;
	}
	else if(healthPlanName == '' && dentalPlanName != '' ) {
		planInfo = dentalPlanInfo;
	}

	
	function validateForm() {
		if ($("#frmesign").validate().form()) {

			// Checkout Step 4
			window.dataLayer.push({
				'event': 'checkout',
				'eventCategory': 'Plan Selection - ecommerce',
				'eventAction': 'Enroll Button Click',
				'eventLabel': planInfo,
				'ecommerce': {
					'checkout': {
						'actionField': {'step': 4}
					}
				}
			});

			$("#frmesign").submit();
			$("#submitButton").attr("disabled", true);
		}
	}

</script>

<div class="gutter10">

<div class="row-fluid">
	<div style="font-size: 14px; color: red">
		<c:if test="${errorMsg != ''}">
			<p>
				<c:out value="${errorMsg}"></c:out>
			<p />
		</c:if>
	</div>
</div>
<c:if test="${errorMsg == ''}">

	<div class="row-fluid" id="titlebar">
		<h1 class="custom-margin"><spring:message code="enroll.eSignature"/></h1>
	</div>
	<div class="row-fluid">
		<form method="post" action="esignature" name="frmesign" id="frmesign">
		<df:csrfToken/>
			<input type="hidden" name="cart_id" id="cart_id" value="${cartId}" />
			<input type="hidden" name="aptc" id="aptc" value="${aptc}" />
			<input type="hidden" name="hasStateSubsidy" id="hasStateSubsidy" value="${hasStateSubsidy}" />
			<input type="hidden" name="fininfoId" id="fininfoId" value="${fininfoId}" />
			<input type="hidden" name="employeeFullName" id="employeeFullName" value="${employeeFullName}" />
			<input type="hidden" name="enrollmentType" id="enrollmentType" value="${enrollment_type}" />
			<div class="span10 offset1" id="rightpanel">
				<p>
					<c:choose>
						<c:when test="${enrollment_type == enrollment_type_medical}">
							<h2><spring:message code="medical.esignature.enterYourEsignature.label"/></h2>
							<p><spring:message code="medical.esignature.enterYourEsignature.value"/></p>
						</c:when>
						<c:otherwise>
							<p><spring:message code="enroll.esignature.header"/></p>
						</c:otherwise>
					</c:choose>
					<%-- <c:if test="${showFileTaxReturn==true && enrollment_type == enrollment_type_individual}">
						<label class="checkbox" for="agreement"> <input type="checkbox" name="agreement" id="agreement" value="1">
							<spring:message code="enroll.esignature.aptcAgreement" htmlEscape="false"/>
						</label>
					</c:if> --%>
				</p>

			   <div class="row-fluid agree-list" >

					<!-- Tax Filers Signature -->
					<c:if test="${enrollment_type == enrollment_type_individual && (aptc !='' || hasStateSubsidy)}">
						<br/>
						<div class="control-group">
							<div class="controls">
								<input type="checkbox" name="taxFiler_esign" id="taxFiler_esign" value="1">
								<label class="checkbox" for="taxFiler_esign">
									<spring:message code="enroll.esignature.aptcAgreement1" htmlEscape="false"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
									<ul>
											<li><spring:message code="enroll.esignature.aptcAgreement2" htmlEscape="false"/></li>
											<c:if test="${hasStateSubsidy}">
											    <li><spring:message code="enroll.esignature.stateSubsidyAgreement1" htmlEscape="false"/></li>
											</c:if>

											<li><spring:message code="enroll.esignature.aptcAgreement3" htmlEscape="false"/></li>
											<li><spring:message code="enroll.esignature.aptcAgreement4" htmlEscape="false"/></li>
									</ul>
								</label>

							<div class="" id="taxFiler_esign_error"></div>
							</div>
						</div>
					</c:if>
				</div>

				<div class="agreement-title">
						<c:choose>
							<c:when test="${enrollment_type == enrollment_type_medical}">
								<span aria-label="medical.agreement.coveredCaliforniaAgreement.label"><spring:message code="medical.agreement.coveredCaliforniaAgreement.label"/></span>
							</c:when>
							<c:otherwise>
								<span aria-label="label.binding.arbitration.agreement"><spring:message code="label.binding.arbitration.agreement"/></span>
							</c:otherwise>
						</c:choose>
					<%-- <a rel="tooltip" title="<spring:message code='enroll.print'/>" href="#"class="btn btn-primary btn-small pull-right" onclick="printContent('user-agreement');"><i class="icon-print"></i>&nbsp;<spring:message code="enroll.print"/></a> --%>
					<button id="aid-print" rel="tooltip" class="btn btn-primary btn-small pull-right" title="<spring:message code='enroll.print'/>" type="button"><i class="icon-print"></i>&nbsp;<spring:message code="enroll.print"/></button>
				</div>
				<div class="agreement">
					<div class="gutter10 clearfix">
						<div class="control-group">

								<c:choose>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<textarea rows="4" class="span12 agree" id="user-agreement" readonly="readonly"><spring:message code="label.new.agreement"/></textarea>
									</c:when>
									<c:when test="${enrollment_type == enrollment_type_medical}">
										<%--Different Text For Individual --%>
										<textarea rows="4" class="span12 agree" id="user-agreement" readonly="readonly"><spring:message code="medical.agreement.coveredCaliforniaAgreement.value"/></textarea>
									</c:when>
									<c:otherwise>
										<div class="controls">
										<%--Different Text For Employees --%>
										<textarea rows="4" class="span12 agree" id="user-agreement" readonly="readonly"><spring:message code="label.new.agreement"/></textarea>
										</div>
									</c:otherwise>
								</c:choose>
						</div>
						<!--Alignment for e-signature and provide -->
						<div class="control-group">
								<c:choose>
									<c:when test="${enrollment_type == enrollment_type_medical}">
										<label for="applicant_esig" class="required control-label"><spring:message code="medical.esignature.signature.label"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									</c:when>
									<c:otherwise>
										<label for="applicant_esig" class="required control-label"><spring:message code="enroll.esignature.signature"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									</c:otherwise>
								</c:choose>

								<div class="controls">
									<input
										type="text"
										maxlength="50"
										class="gtm_applicant_esig"
										id="applicant_esig"
										name="applicant_esig"
										value="${applicant_esig }"
										aria-label= "<spring:message code="enroll.esignature.signature"/>"/>
									<div class="" id="applicant_esig_error"></div>
								</div>


								<c:if test="${enrErrorMsg != null && enrErrorMsg != ''}">
									<div class="error" >
										<label class="error">
											<span>
												<em class="excl">!</em>
												<spring:message code="enroll.esignature.errorMessage1"/>
												<c:if test="${enrErrorMsg2 != ''}"><spring:message code="enroll.esignature.errorMessage2"/> <c:out value="${enrErrorMsg2}"></c:out>)</c:if>.
											</span>
										</label>
									</div>
								</c:if>
						</div>




						<div class="control-group">
							<div class="controls">
								<c:choose>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<label class="checkbox" for="ind-terms">
											<input type="checkbox" name="terms" id="ind-terms" value="1">
											<spring:message code="label.read.agreement"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
										</label>

									</c:when>
									<c:when test="${enrollment_type == enrollment_type_medical}">
									<label class="checkbox" for="ind-terms-label">
										  <input type="checkbox" name="terms" id="ind-terms-label" value="1" aria-label="<spring:message code="medical.agreement.read.label"/>">
										  <spring:message code="medical.agreement.read.label"/>
									</label>
									</c:when>
									<c:otherwise>
										<label class="checkbox" for="emp-terms">
											<input type="checkbox" name="terms" id="emp-terms" value="1">
											<spring:message code="label.read.agreement"/>
										</label>
									</c:otherwise>
								</c:choose>
								<div class="" id="ind-terms_error"></div>
								<div class="" id="emp-terms_error"></div>
							</div>
						</div>

						<c:if test="${showpin==true}">
										<br/>
										<div class="control-group clearfix">

												<label for="pin_esig"><spring:message code="enroll.esignature.PIN"/>
													<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
													<a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message code="enroll.esignature.pinTooltip"/>"
													class="info"><i class="icon-question-sign"></i>
													</a>
												</label>
											<div class="controls">
												<input type="password" class="input-small" id="pin_esig" name="pin_esig" maxlength="4" value="${pin_esig }" />
												<div class="" id="pin_esig_error"></div>
											</div>
										</div>
						</c:if>



						<!-- gutter10 -->
					</div>

					<div class="control-group esign gutter10-b">
						<div class="gutter10">
							<span class="span3"><spring:message code="enroll.esignature.provideSignature"/>:</span>
							<div id="eSignature" class="controls span6"></div>
							<span class="pull-left marginL20"><spring:message code="enroll.date"/>: &nbsp; </span>
							<fmt:formatDate value="${currentDate}" type="both" pattern="MM/dd/yyyy" />
						</div>
						<!-- gutter10 -->
					</div>
				</div>
				<div class="form-actions">
					<c:choose>
							<%-- Different Text For Individual --%>
							<c:when test="${enrollment_type == enrollment_type_individual}">
								<button
									type="button"
									id="submitButton"
									class="btn btn-primary pull-right gtm_esign_enroll"
									title="<spring:message code="enroll.esignature.submitButtonValue"/>">
										<spring:message code="enroll.esignature.submitButtonValue"/>
								</button>
								<button
									type="button"
									id="aid-backBtn"
									class="btn gtm_esign_back"
									title="<spring:message code='enroll.back' />"
									name="button"
									onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="enroll.back"/> Button Click', 'eventLabel': '<spring:message code="enroll.back"/>'});">
									<spring:message code='enroll.back' />
								</button>
							</c:when>
							<%--Different Text For Employees --%>
							<c:otherwise>
									<button
										type="button"
										id="submitButton"
										class="btn btn-primary pull-right gtm_esign_enroll"
										title="<spring:message code="enroll.esignature.submitButtonValue"/>">
										<spring:message code="enroll.esignature.submitButtonValue"/>
									</button>
									<button
										type="button"
										id="aid-backBtn"
										class="btn gtm_esign_back"
										title="<spring:message code='enroll.back' />"
										name="button"
										onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="enroll.back"/> Button Click', 'eventLabel': '<spring:message code="enroll.back"/>'});">
											<spring:message code='enroll.back' />
										</button>
							</c:otherwise>
				  </c:choose>
				</div>
			</div>
			<!--rightpanel-->
		</form>
		<!-- End Form -->
	</div>
	<!--row-fluid-->
	</div>
</c:if>
<iframe name=print_frame width=400 height=400 frameborder=0 src=about:blank class="hide" aria-hidden="true"></iframe>
<script type="text/javascript">
	$(document).ready(function() {
	    document.title = 'E-Signature Page';

		var esign_org;
		$('.info').tooltip();
		$('span.icon-ok').parents('li').css('list-style', 'none');
		$("#aid-print").click(function(){
			printContent('user-agreement');
		});
		$("#submitButton").click(function(){
			validateForm();
		});
		$("#aid-backBtn").click(function(){
			window.location = "/hix/private/showCart";
		});

		$('#applicant_esig').bind('keyup focusout', function(event) {
			$('#eSignature').text($('#applicant_esig').val());
		});


		// Data layer variable to be pushed at the end of shell.jsp
    	dl_pageCategory = 'Checkout';

		// Checkout Step 3
		$('#applicant_esig').bind('focusout', function(event) {
			window.dataLayer.push({
				'event': 'checkout',
				'eventCategory': 'Plan Selection - ecommerce',
				'eventAction': 'E-Signature',
				'eventLabel': planInfo,
				'ecommerce': {
					'checkout': {
						'actionField': {'step': 3}
					}
				}
			});
		});

	});
</script>

<script type="text/javascript">

	setTimeout(function(){
		$('#applicant_esig').focusin(function(event){
			esign_org = $('#applicant_esig').val();
		});
	});

	// $('#a').click(function(event){
	// 	var esign_latest =  $('#applicant_esig').val();
	// 	if (esign_latest && (esign_org !== esign_latest)) {
	// 		window.dataLayer.push({
	// 			'event': 'checkout',
	// 			'eventCategory': 'Plan Selection - ecommerce',
	// 			'eventAction': 'E-Signature',
	// 			'eventLabel': planInfo,
	// 			'ecommerce': {
	// 				'checkout': {
	// 					'actionField': {'step': 3}
	// 				}
	// 			}
	// 		});
	// 	}
	// });


	jQuery.validator.addMethod("alphabetsOnly", function(value, element) {
		return /^[a-zA-Z-.\s\']+$/i.test(value);
	});
	jQuery.validator.addMethod("fullNameOnly", function(value, element) {
	    if(/\w+\s+\w+/.test(value)) {
	       return true;
	    } else {
	       return false;
	    }
	});
	jQuery.validator.addMethod("employeeFullNameOnly", function(value, element) {
		var employeeFullName=$('#employeeFullName').val();
		var enrollmentType= $('#enrollmentType').val();
		if(enrollmentType=='24'){
		 if(value.toUpperCase()==employeeFullName.toUpperCase()){
	            return true;
	        }
	        else{
	            return false;
	        }
	    }
		return true;
	});
	var validator = $("#frmesign")
	.validate({
		rules : {
			'applicant_esig' : {
				required : true,
				alphabetsOnly : true,
				fullNameOnly : true,
				employeeFullNameOnly : true
			},
			'pin_esig' : {
				required : true,
				number : true,
				maxlength : 4,
				minlength : 4
			},
			'taxFiler_esign' : {
				required : true
			},
			'terms' : {
				required : true
			}
		},
		messages : {
			'pin_esig' : {
				required : "<span ><em class='excl'>!<span class='aria-hidden'>Error: Invalid Input </span></em><spring:message code='label.validatePinEsig' javaScriptEscape='true'/></span>"
			},
			'applicant_esig' : {
				required : "<span><em class='excl'>!<span class='aria-hidden'>Error: Invalid Input </span></em><spring:message code='label.validateApplicantEsig' javaScriptEscape='true'/></span>",
				alphabetsOnly : "<span><em class='excl'>!<span class='aria-hidden'>Error: Invalid Input </span></em><spring:message code='label.validateSignature'/></span>",
				fullNameOnly : "<span><em class='excl'>!<span class='aria-hidden'>Error: Invalid Input </span></em><spring:message code='label.validateApplicantEsig'/></span>",
				employeeFullNameOnly : "<span><em class='excl'>!</em><spring:message code='label.validateApplicantEsig'/></span>"
			},
			'taxFiler_esign' : {
				required : "<span><em class='excl'>!<span class='aria-hidden'>Error: Invalid Input </span></em><spring:message code='label.validateTerms' javaScriptEscape='true'/></span>"
			},
			'terms' : {
				required : "<span><em class='excl'>!<span class='aria-hidden'>Error: Invalid Input </span></em><spring:message code='label.validateTerms' javaScriptEscape='true'/></span>"
			}
		},
		errorClass : "error",
		errorPlacement : function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error")
					.attr('class', 'error');
		}
	});

	function printContent(id){
	 	var str ="<html>";
		 	str=str+"<head></head>";
		 	str=str+"<body>";
		 	str=str+"<table align='center'>";
		 	str=str+"<tr><td align='center'>Exchange Agreement</td></tr>";
		 	str=str+"<tr><td>"+document.getElementById(id).innerHTML+"</td></tr>";
		 	str=str+"</table>";
		 	str=str+"</body>";
		 	str=str+"</html>";

	 		window.frames["print_frame"].document.body.innerHTML=str;
	 		window.frames["print_frame"].window.focus();
	 		window.frames["print_frame"].window.print();
	}
</script>
