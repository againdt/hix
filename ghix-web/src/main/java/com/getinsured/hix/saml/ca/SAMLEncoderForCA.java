package com.getinsured.hix.saml.ca;

import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBElement;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixPlatformKeystore;

import gov.ca.calheers.saml2encode.Saml2EncodeInput;
import gov.ca.calheers.saml2encode.Saml2EncodeManager;
import gov.ca.calheers.saml2encode.Saml2EncodingException;
import oasis.names.tc.saml._2_0.assertion.AssertionType;
import oasis.names.tc.saml._2_0.assertion.AttributeStatementType;
import oasis.names.tc.saml._2_0.assertion.AttributeType;
import oasis.names.tc.saml._2_0.assertion.AudienceRestrictionType;
import oasis.names.tc.saml._2_0.assertion.AuthnContextType;
import oasis.names.tc.saml._2_0.assertion.AuthnStatementType;
import oasis.names.tc.saml._2_0.assertion.ConditionsType;
import oasis.names.tc.saml._2_0.assertion.NameIDType;
import oasis.names.tc.saml._2_0.assertion.SubjectConfirmationDataType;
import oasis.names.tc.saml._2_0.assertion.SubjectConfirmationType;
import oasis.names.tc.saml._2_0.assertion.SubjectLocalityType;
import oasis.names.tc.saml._2_0.assertion.SubjectType;
import oasis.names.tc.saml._2_0.protocol.ResponseType;
import oasis.names.tc.saml._2_0.protocol.StatusCodeType;
import oasis.names.tc.saml._2_0.protocol.StatusType;

/**
 * This method encodes the Saml2EncodeInput as a SAML2 Post Binding protocol,
 * using an enveloped digital signature.
 * 
 * @author aaron.m.spence
 */
@Component
public class SAMLEncoderForCA implements Saml2EncodeManager {

    private class SAML2DigitalSignatureBuilder {
        
        public String signDocument(String assertionIdKey, PrivateKeyEntry keyEntry, StringWriter documentToSign)
                throws Exception {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");
            List<Transform> transforms = new ArrayList<Transform>(2);
            transforms.add(fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));
            transforms.add(fac.newTransform("http://www.w3.org/2001/10/xml-exc-c14n#", (TransformParameterSpec) null));

            Reference ref = fac.newReference("#" + assertionIdKey, fac.newDigestMethod(
            		DIGEST_ALGO_URI, null), transforms,
                    null, null);
            
            
            SignedInfo si = fac.newSignedInfo(
                    fac.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE, (C14NMethodParameterSpec) null),
                    fac.newSignatureMethod(
                    		SIGNATURE_ALGO_URI, null),
                    Collections.singletonList(ref));
            

            
			X509Certificate x509Certificate = (X509Certificate) keyEntry.getCertificate();

            // Create the KeyInfo containing the X509Data.
            KeyInfoFactory kif = fac.getKeyInfoFactory();
            List<Object> x509Content = new ArrayList<>();
            x509Content.add(x509Certificate.getSubjectX500Principal().getName());
            x509Content.add(x509Certificate);
            X509Data xd = kif.newX509Data(x509Content);
            KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));

            dbf.setNamespaceAware(true);

            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            StringReader stringReader = new StringReader(documentToSign.toString());
            InputSource inputSource = new InputSource(stringReader);
            Document actualDocument = documentBuilder.parse(inputSource);

            Node assertionNode = actualDocument.getElementsByTagNameNS(

                    "urn:oasis:names:tc:SAML:2.0:assertion", "Assertion").item(0);
            
            Node subjectNode = actualDocument.getElementsByTagNameNS(

                    "urn:oasis:names:tc:SAML:2.0:assertion", "Subject").item(0);
           
            DOMSignContext dsc = new DOMSignContext(keyEntry.getPrivateKey(), assertionNode, subjectNode);
            Element element=(Element)assertionNode;
            try{
                dsc.setIdAttributeNS(element, null, "ID");
            }
            catch(Exception e){
                element.setAttributeNS(null, "ID", assertionIdKey);
                dsc.setIdAttributeNS(element, null, "ID");
            }
            
            XMLSignature signature = fac.newXMLSignature(si, ki);

            signature.sign(dsc);
            StringWriter transformStringWriter = SAMLEncoderForCA.this.getStringWriter();
            StreamResult transformResult = new StreamResult(transformStringWriter);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(actualDocument), transformResult);

            return transformStringWriter.getBuffer().toString();
        }
    }

    private static final String SAML2_BEARER_SUBJECT = "urn:oasis:names:tc:SAML:2.0:cm:bearer";
    private static final String SAML2_PASSWORD_AUTH_TYPE = "urn:oasis:names:tc:SAML:2.0:ac:classes:Password";
    private static final String CAL_HEERS_SAML2_ASSERTION = "CalHEERS_SAML2_Assertion_";
    private static final String UNSPECIFIED_SAML_FORMAT = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";
    private static final String ENTITY_SAML_FORMAT = "urn:oasis:names:tc:SAML:2.0:nameid-format:entity";
    private static final String SAML_VERSION = "2.0";
    private static final String SAML2_SUCCESS = "urn:oasis:names:tc:SAML:2.0:status:Success";
    private static String SAML2_RESPONSE = "CalHEERS_SAML2_Response_";
    private static final String CARRIER_PAYMENT_KEY_TYPE= "SHA256withRSA";
    private static final String DIGEST_ALGO_URI= "http://www.w3.org/2001/04/xmlenc#sha256";
    private static final String SIGNATURE_ALGO_URI="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

    

    /**
     * This method creates the default response status. The response will always
     * be success.
     * 
     * @return The default response status.
     */
    private StatusType buildStatusCode() {
        StatusType status = new StatusType();
        StatusCodeType statusCode = new StatusCodeType();
        statusCode.setValue(SAML2_SUCCESS);
        status.setStatusCode(statusCode);
        return status;
    }

    /**
     * This method builds the subject on the assertion using the input and the
     * digital signature certificate.
     * 
     * @param assertion
     *            The assertion to set the subject on.
     * @param x509Certificate
     *            The digital signature certificate.
     * @param input
     *            The input.
     * @throws DatatypeConfigurationException
     *             If subject objects are not create-able by the object factory.
     */
    private void buildSubject(AssertionType assertion, X509Certificate x509Certificate, Saml2EncodeInput input)
            throws DatatypeConfigurationException {
        oasis.names.tc.saml._2_0.assertion.ObjectFactory objectFactory = new oasis.names.tc.saml._2_0.assertion.ObjectFactory();
        SubjectType subject = new SubjectType();

        JAXBElement<NameIDType> subjectIdActual = this.createSubjectNameID(input, objectFactory);
        subject.getContent().add(subjectIdActual);
        NameIDType subjectCN = this.createSubjectName(x509Certificate, input);
        JAXBElement<SubjectConfirmationType> subjectConfirmationActual = this.createSubjectConfirmation(input,
                objectFactory, subjectCN);
        subject.getContent().add(subjectConfirmationActual);
        assertion.setSubject(subject);
    }

    /**
     * This method creates the authentication type
     * 
     * @param input
     *            The input.
     * @return A SAML2 authentication statement type.
     */
    private AuthnStatementType createAuthenticationType(Saml2EncodeInput input) {
        AuthnStatementType authnStatementType = new AuthnStatementType();
        authnStatementType.setAuthnInstant(input.getAuthorizationDateTime());
        authnStatementType.setSessionIndex(input.getSessionIndex());
        authnStatementType.setSessionNotOnOrAfter(input.getValidBeforeDate());
        SubjectLocalityType subjectLocalityType = this.createSubjectLocalityType(input);
        authnStatementType.setSubjectLocality(subjectLocalityType);
        return authnStatementType;
    }

    /**
     * This method creates the subject confirmation. It defaults to 'Sender
     * vouches for subject' confirmation.
     * 
     * @param input
     *            The input.
     * @param objectFactory
     *            The object factory.
     * @param subjectCN
     *            The name of the subject.
     * @return the subject confirmation.
     */
    private JAXBElement<SubjectConfirmationType> createSubjectConfirmation(Saml2EncodeInput input,
            oasis.names.tc.saml._2_0.assertion.ObjectFactory objectFactory, NameIDType subjectCN) {
        SubjectConfirmationType subjectConfirmation = new SubjectConfirmationType();
        subjectConfirmation.setMethod(SAML2_BEARER_SUBJECT);
        subjectConfirmation.setNameID(subjectCN);
        SubjectConfirmationDataType subjectConfirmationData = new SubjectConfirmationDataType();
        subjectConfirmationData.setNotOnOrAfter(input.getValidBeforeDate());
        subjectConfirmationData.setRecipient(input.getAssertionDestination());
        JAXBElement<SubjectConfirmationType> subjectConfirmationActual = objectFactory
                .createSubjectConfirmation(subjectConfirmation);
        subjectConfirmation.setSubjectConfirmationData(subjectConfirmationData);
        return subjectConfirmationActual;
    }

    /**
     * This method sets up the location of the responsible party.
     * 
     * @param input
     *            The input.
     * @return A Subject locality type used to specify the location of the
     *         responsible party.
     */
    private SubjectLocalityType createSubjectLocalityType(Saml2EncodeInput input) {
        SubjectLocalityType subjectLocalityType = new SubjectLocalityType();
        subjectLocalityType.setAddress(input.getSubjectAddress());
        subjectLocalityType.setDNSName(input.getSubjectDNS());
        return subjectLocalityType;
    }

    /**
     * This method creates the subject name.
     * 
     * @param x509Certificate
     *            The signature certificate.
     * @return the subject name.
     */
    private NameIDType createSubjectName(X509Certificate x509Certificate, Saml2EncodeInput input) {
        NameIDType subjectCN = new NameIDType();
        subjectCN.setFormat("urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName");
        subjectCN.setValue(x509Certificate.getIssuerDN().getName());
        return subjectCN;
    }

    /**
     * This method creates the subject name property.
     * 
     * @param input
     *            The input.
     * @param objectFactory
     *            The object factory for creating the name field.
     * @return The subject name property.
     */
    private JAXBElement<NameIDType> createSubjectNameID(Saml2EncodeInput input,
            oasis.names.tc.saml._2_0.assertion.ObjectFactory objectFactory) {
        NameIDType subjectId = new NameIDType();
        subjectId.setFormat(UNSPECIFIED_SAML_FORMAT);
        subjectId.setValue(input.getSubjectId());
        subjectId.setNameQualifier(input.getNameQualifier());
        
        JAXBElement<NameIDType> subjectIdActual = objectFactory.createNameID(subjectId);
        return subjectIdActual;
    }

    /**
     * This method encodes the Saml2EncodeInput as a SAML2 Post Binging
     * protocol, using an enveloped digital signature.
     * 
     * @param input
     *            The input to be encoded.
     * @return The encoded result.
     * @throws Exception
     *             If any errors occur.
     */
    private String doEncoding(Saml2EncodeInput input) throws Exception {
        String encoded = null;
        ResponseType response = new ResponseType();
        response.setDestination(input.getAssertionDestination());
        UUID responseId = input.getResponseUUID();
        response.setID(SAML2_RESPONSE + responseId.toString());
        response.setVersion(SAML_VERSION);
        response.setIssueInstant(input.getAuthorizationDateTime());

        StatusType status = this.buildStatusCode();

        response.setStatus(status);

        AssertionType assertion = new AssertionType();
        assertion.setVersion(SAML_VERSION);

        assertion.setIssueInstant(input.getAuthorizationDateTime());
        assertion.setVersion(SAML_VERSION);

        NameIDType nameId = new NameIDType();
        nameId.setFormat(ENTITY_SAML_FORMAT);
        nameId.setValue(input.getIssuerName());
        assertion.setIssuer(nameId);

        PrivateKeyEntry pKeyEntry = GhixPlatformKeystore.getPrivateKeyEntry(GhixPlatformConstants.PAYNOW_SIGNING_KEY_ALIAS, null);
        Signature signature = Signature.getInstance(CARRIER_PAYMENT_KEY_TYPE);
        signature.initSign((PrivateKey) pKeyEntry.getPrivateKey());
        X509Certificate x509Certificate = (X509Certificate) pKeyEntry.getCertificate();

        oasis.names.tc.saml._2_0.assertion.ObjectFactory assertionObjectFactory = new oasis.names.tc.saml._2_0.assertion.ObjectFactory();
        this.buildSubject(assertion, x509Certificate, input);

        
        AuthnStatementType authnStatementType = this.createAuthenticationType(input);

        AuthnContextType authnContextType = new AuthnContextType();
        JAXBElement<String> authnContextClassRef = assertionObjectFactory
                .createAuthnContextClassRef(SAML2_PASSWORD_AUTH_TYPE);
        authnContextType.getContent().add(authnContextClassRef);
        authnStatementType.setAuthnContext(authnContextType);
        assertion.getStatementOrAuthnStatementOrAuthzDecisionStatement().add(authnStatementType);
        
        StringWriter paymentWriter = this.getStringWriter();
        JAXB.marshal(input.getItemToEncode(), paymentWriter);
        AttributeStatementType attributeStatement = new AttributeStatementType();
        AttributeType carrierPaymentAttribute = new AttributeType();
        carrierPaymentAttribute.setName(input.getResponseItemName());
        carrierPaymentAttribute.getAttributeValue().add(paymentWriter.toString().getBytes(Charset.forName("UTF-8")));
        attributeStatement.getAttributeOrEncryptedAttribute().add(carrierPaymentAttribute);
        assertion.getStatementOrAuthnStatementOrAuthzDecisionStatement().add(attributeStatement);

        
        JAXBElement<AssertionType> actualAssertion = assertionObjectFactory.createAssertion(assertion);
        StringWriter assertionStringWriter = this.getStringWriter();
        JAXB.marshal(actualAssertion, assertionStringWriter);

        UUID assertionId = input.getAssertionUUID();
        String assertionIdKey = CAL_HEERS_SAML2_ASSERTION + assertionId.toString();
        assertion.setID(assertionIdKey);
        ConditionsType conditions = new ConditionsType();
        conditions.setNotBefore(input.getValidAfterDate());
        conditions.setNotOnOrAfter(input.getValidBeforeDate());
        AudienceRestrictionType audienceRestriction = new AudienceRestrictionType();
        audienceRestriction.getAudience().add(input.getAudienceRestriction());
        conditions.getConditionOrAudienceRestrictionOrOneTimeUse().add(audienceRestriction);
        assertion.setConditions(conditions);
        oasis.names.tc.saml._2_0.protocol.ObjectFactory objectFactory = new oasis.names.tc.saml._2_0.protocol.ObjectFactory();

        response.getAssertionOrEncryptedAssertion().add(assertion);
        
        JAXBElement<ResponseType> responseActual = objectFactory.createResponse(response);
        StringWriter requestWriter = this.getStringWriter();
        JAXB.marshal(responseActual, requestWriter);

        String signedDocument = new SAML2DigitalSignatureBuilder().signDocument(assertionIdKey, pKeyEntry, requestWriter);

        encoded = DatatypeConverter.printBase64Binary(signedDocument.getBytes());

        return encoded;
    }

    /**
     * This method encodes the Saml2EncodeInput as a SAML2 Post Binding
     * protocol, using an enveloped digital signature. A Saml2EncodingException
     * will be thrown if any errors are encountered.
     * 
     * @see gov.ca.calheers.saml2encode.Saml2EncodeManager#encode(gov.ca.calheers.saml2encode.Saml2EncodeInput)
     */
    @Override
    public void encode(Saml2EncodeInput input) {

        try {

            input.setEncodedResult(this.doEncoding(input));

        } catch (Exception e) {
            throw new Saml2EncodingException("Exception encoding message:", e);
        }

    }

    /**
     * This method gets a StringWriter for marshalling XML.
     * 
     * @return
     */
    StringWriter getStringWriter() {

        return new StringWriter(2048);

    }

}
