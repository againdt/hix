<%@page session="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<style>
.table th,.table th a {
	background: #666;
	color: #fff
}

table th {
	height: 22px
}

/*START css*/
#sidebar h4,#sidebar .header,.gray h4,.header {
	max-height: 10%;
}

h4 small.headerSmall {
	font-size: 12px;
	color: #666;
}

.grayBgTable {
	background-color: #F8F8F8;
}

.page-breadcrumb {
	display: inline;
}

h4.rule,.span9 .rule {
	border-bottom: 1px solid #999;
}

h4.rule {
	padding-bottom: 0;
	margin-bottom: 10px;
}

.top30 {
	margin-top: 30px;
}

.bgBox {
	padding: 20px 0;
	background-color: #F5F5F5;
	border-radius: 4px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border: 1px solid #eee;
	vertical-align: top;
	text-align: center;
}

a.bgBox:hover {
	background-color: #ECECEC;
}

a.bgBox img {
	display: block;
	margin-left: auto;
	margin-right: auto;
}

h1.rule,.span4 h3.rule {
	border-bottom: 1px solid #DADADA;
}

#sidebar ul li {
	line-height: 24px;
}

#sidebar ul li a {
	line-height: 24px;
}

h1.title{
	color:#1BB5E0;
}

</style>




<div class="gutter10">
	<h1 class="gutter10 rule"><a name="skip"></a><i class=" icon-home"></i> Help Center</h1>
	<div class="gutter10">
		<div class="row-fluid">
			<div class="span4">
				<h3 class="">
					<a class="bgBox span12" href="<c:url value='/faq/individual'/>"><img src="<gi:cdnurl value="/resources/img/faq-individualhelp.png" />" alt="Consumer" />Individual</a>
				</h3>
			</div>
			
			<div class="span4">
				<h3 class="">
					<a class="bgBox span12" href="<c:url value='/faq/assister'/>"><img src="<gi:cdnurl value="/resources/img/faq-entityadminhelp.png" />" alt="Consumer Connectors" />Consumer Connectors</a>
				</h3>
			</div>
			
			<div class="span4">
				<h3 class="">
					<a class="bgBox span12" href="<c:url value='/faq/employer'/>"><img src="<gi:cdnurl value="/resources/img/faq-employerhelp.png" />" alt="Employer/Employee" />Employer/Employee</a>
				</h3>
			</div>
		</div>
	</div>
</div>


<script>
$(document).ready(function(){
	document.title="Help for YHI";
});

</script>
