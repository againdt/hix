//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.29 at 08:34:31 AM IST 
//


package com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.String;
import com.getinsured.iex.hub.vci.gov.niem.niem.structures._2.ComplexObjectType;
import com.getinsured.iex.hub.vci.gov.niem.niem.usps_states._2.USStateCodeType;


/**
 * A data type for an address.
 * 
 * <p>Java class for StructuredAddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StructuredAddressType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationStreet" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationCityName" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationStateUSPostalServiceCode" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationPostalCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StructuredAddressType", propOrder = {
    "locationStreet",
    "locationCityName",
    "locationStateUSPostalServiceCode",
    "locationPostalCode"
})
public class StructuredAddressType
    extends ComplexObjectType
{

    @XmlElement(name = "LocationStreet")
    protected StreetType locationStreet;
    @XmlElement(name = "LocationCityName")
    protected ProperNameTextType locationCityName;
    @XmlElement(name = "LocationStateUSPostalServiceCode")
    protected USStateCodeType locationStateUSPostalServiceCode;
    @XmlElement(name = "LocationPostalCode")
    protected String locationPostalCode;

    /**
     * Gets the value of the locationStreet property.
     * 
     * @return
     *     possible object is
     *     {@link StreetType }
     *     
     */
    public StreetType getLocationStreet() {
        return locationStreet;
    }

    /**
     * Sets the value of the locationStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link StreetType }
     *     
     */
    public void setLocationStreet(StreetType value) {
        this.locationStreet = value;
    }

    /**
     * Gets the value of the locationCityName property.
     * 
     * @return
     *     possible object is
     *     {@link ProperNameTextType }
     *     
     */
    public ProperNameTextType getLocationCityName() {
        return locationCityName;
    }

    /**
     * Sets the value of the locationCityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProperNameTextType }
     *     
     */
    public void setLocationCityName(ProperNameTextType value) {
        this.locationCityName = value;
    }

    /**
     * Gets the value of the locationStateUSPostalServiceCode property.
     * 
     * @return
     *     possible object is
     *     {@link USStateCodeType }
     *     
     */
    public USStateCodeType getLocationStateUSPostalServiceCode() {
        return locationStateUSPostalServiceCode;
    }

    /**
     * Sets the value of the locationStateUSPostalServiceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link USStateCodeType }
     *     
     */
    public void setLocationStateUSPostalServiceCode(USStateCodeType value) {
        this.locationStateUSPostalServiceCode = value;
    }

    /**
     * Gets the value of the locationPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationPostalCode() {
        return locationPostalCode;
    }

    /**
     * Sets the value of the locationPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationPostalCode(String value) {
        this.locationPostalCode = value;
    }

}
