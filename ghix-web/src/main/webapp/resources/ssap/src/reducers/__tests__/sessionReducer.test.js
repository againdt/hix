import sessionReducer, { initialState } from '../sessionReducer';
import { sessionActionTypes } from '../../constants/actionTypes';

const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const newSessionAction = {
    type: sessionActionTypes.REFRESH_SESSION,
    session: {
        lastAccessedTime: 156,
        inactiveIntervalMin: 345,
        creationTime: 689,
        now: 122,
        nowTimeShifted: 234
    }
};

const clearAction = {
    type: sessionActionTypes.CLEAR_STATE
};

describe('Test for sessionReducer', () => {

    it('This reducer hasn\'t been invoked', () => {
        expect(
            sessionReducer(
                initialState,
                anotherAction
            )
        ).toEqual(initialState);
    });

    it('Should set new session values', () => {
        expect(
            sessionReducer(
                initialState,
                newSessionAction
            )
        ).toEqual(newSessionAction.session);
    });

    it('Should clear state', () => {
        expect(sessionReducer(
            {
                lastAccessedTime: 156,
                inactiveIntervalMin: 345,
                creationTime: 689,
                now: 122,
                nowTimeShifted: 234
            },
            clearAction
        )).toEqual(initialState);
    });
});