<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration.IEXConfigurationEnum"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.joda.time.DateTime"%>
<%@page import="org.joda.time.LocalDate"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.timeshift.util.TSDate"%>
<%@page import="org.apache.commons.lang3.time.DateUtils"%>
<%@page import="java.util.Date"%>
<% String userActiveRoleLabel =  (String) session.getAttribute("userActiveRoleLabel"); %>
<c:set var="userActiveRoleLabel" value="<%=userActiveRoleLabel%>" />
<%
	String ehCurrentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
	String ehCoverageYearOptionCutOffDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_CURRENT_YEAR_TAB);
	String ehCoverageYearOptionCutOffDatePriviledgeUser = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_CURRENT_YEAR_TAB_PRIVILEDGE_USER);
	try{
			if(ehCoverageYearOptionCutOffDatePriviledgeUser !=null && ehCoverageYearOptionCutOffDatePriviledgeUser.trim().length()!=0 && !"Individual".equalsIgnoreCase(userActiveRoleLabel)){
				ehCoverageYearOptionCutOffDate=ehCoverageYearOptionCutOffDatePriviledgeUser;
			}
		    if(ehCoverageYearOptionCutOffDate != null && ehCoverageYearOptionCutOffDate.trim().length() != 0){
		    	Date todaysDate = new TSDate();
		    	Date cutOffDate = DateUtils.parseDateStrictly(ehCoverageYearOptionCutOffDate, new String[]{"MM/dd/yyyy"});
				if (todaysDate.before(cutOffDate)) {
					ehCurrentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
				}
		    }
		}
		catch(Exception ex){}
	String ehRenewalCoverageYear = ehCurrentCoverageYear;
	String ehtRenewalCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_RENEWAL_COVERAGE_YEAR);
	if(null!=ehtRenewalCoverageYear && ehtRenewalCoverageYear.trim().length()>0){
		ehRenewalCoverageYear = ehtRenewalCoverageYear;
	}
	String exchangeStartYear = "2015";
	String exchangeStartYearProperty = DynamicPropertiesUtil.getPropertyValue("global.exchangeStartYear");
	if(null!=exchangeStartYearProperty && exchangeStartYearProperty.trim().length()>0){
	    exchangeStartYear = exchangeStartYearProperty;
	}

%>

<input id="ehCurrentCoverageYear" name="ehCurrentCoverageYear" type="hidden" value="<%=ehCurrentCoverageYear%>"/>

<!-- <script>
// data retriving from spring source has to be in jsp files
var csrStorage = {
		edit:{header: '<spring:message code="indportal.portalhome.editapplication2"/>', content: '<spring:message code="indportal.portalhome.editapplicationcontent" javaScriptEscape="true"/> '},
		initiate:{header: '<spring:message code="indportal.portalhome.initiateverifications2" />', content: '<spring:message code="indportal.portalhome.initiateverificationscontent" javaScriptEscape="true"/> '},
		rerun:{header: '<spring:message code="indportal.portalhome.reruneligibility2"/>', content: '<spring:message code="indportal.portalhome.reruneligibilitycontent" javaScriptEscape="true"/> '},
		cancelTerm:{header: '<spring:message code="indportal.portalhome.cancelorterminatemyplan2" />', content: '<spring:message code="indportal.portalhome.cancelorterminatemyplancontent" javaScriptEscape="true"/>'},
		update:{header: '<spring:message code="indportal.portalhome.updatecarrier2" />', content: '<spring:message code="indportal.portalhome.updatecarriercontent" javaScriptEscape="true"/>'},
		view:{header: 'view', content: 'csrStorage.view.content'},
		specialEnroll:{header: '<spring:message code="indportal.portalhome.openspecialenorollment2"/>', content: '<spring:message code="indportal.portalhome.openspecialenorollmentcontent" javaScriptEscape="true"/>'},
		coverageDate:{header: '<spring:message code="indportal.portalhome.changecoveragedate2" />', content: '<spring:message code="indportal.portalhome.changecoveragedatecontent" javaScriptEscape="true"/>'},
		reinstate:{header: '<spring:message code="indportal.portalhome.reinstateenrollment2" />', content: '<spring:message code="indportal.portalhome.reinstateenrollmentcontent" javaScriptEscape="true"/> '},
		overrideSEPDenial:{header: '<spring:message code="indportal.portalhome.overridesepdenial2"/>', content: '<spring:message code="indportal.portalhome.overridesepdenialcontent" javaScriptEscape="true"/>'}
};
</script> -->

<script>
	$(document).ready(function() {
		var scope = angular.element(document.getElementById("enrollmenthistory")).scope();
		if (scope) {
			scope.$apply(function() {
				scope['enrollmenthistory'] = {title: '<spring:message code="indportal.enrollments.myEnrollments" javaScriptEscape="true"/>'};
			});
		}
	});
	function payHealthExternalDlEvent() {
		// Checkout Step 6
		window.dataLayer.push({
			'event': 'checkout',
			'eventCategory': 'CalHEERS - ecommerce',
			'eventAction': 'Pay Now Button Click',
			'eventLabel': 'Enrollment History Page',
			'ecommerce': {
				'checkout': {
					'actionField': {'step': 6}
				}
			}
		});
	}
</script>

<script type="text/ng-template" id="enrollmenthistory">

	<style>
		.no-pb {
			padding-bottom: 0 !important;
		}
		.pt-10 {
			padding-top: 10px !important;
		}
	</style>

	<div id="rightpanel" class="multi-year" ng-init="getEnrollmentsHistory()">
		<div spinning-loader="loader"></div>

		<div>
			<!--#########################################################Current Enrollments##############################################################-->
			<div>
				<div class="tab-pane active">
					<div ng-if="myEnrollments.activeHealthEnrollments.length == 0 && myEnrollments.activeDentalEnrollments.length == 0" class="enrollmentWrap">
						<div class="row-fluid header" data-automation-id="enrollmentHeader" id="enrollmentHeader_01">
							<h4 class="col-xs-12 col-sm-7 col-md-8 col-lg-8 no-pb pt-10"><spring:message code="indportal.enrollments.myEnrollments" /></h4>
                            <span class="col-xs-12 col-sm-5 col-md-4 col-lg-4 dropdown coverage-year coverage-year_dropdown coverage-year_right">
                                <label for="activeEhCoverageYear_01" class="bs3-control-label-inline"><spring:message code="indportal.enrollments.enrollmentYear"/></label>
                                <select class="input-small" data-automation-id="activeEhCoverageYear" id="activeEhCoverageYear_01" ng-model="$parent.activeEhCoverageYear" ng-options="coverageYear for coverageYear in ehCoverageYearArray" ng-change="getEnrollmentsHistory()"></select>
                            </span>
						</div>
						<div class="noEnrollment">
							<p><spring:message code="indportal.portalhome.noenrollmentsfound" javaScriptEscape="true"/></p>
						</div>
					</div>

					<div ng-if="myEnrollments.activeHealthEnrollments.length >0 || myEnrollments.activeDentalEnrollments.length > 0" class="enrollmentWrap">
						<div class="row-fluid header collapse-header" data-automation-id="enrollmentHeader" id="enrollmentHeader_02">
                            <h4 class="col-xs-12 col-sm-7 col-md-8 col-lg-8 no-pb pt-10"><spring:message code="indportal.enrollments.currentEnrollments" /></h4>
								<span class="col-xs-12 col-sm-5 col-md-4 col-lg-4 dropdown coverage-year coverage-year_dropdown coverage-year_right">
									<span class="dropdown margin10-r">
                                        <label for="activeEhCoverageYear_02" class="bs3-control-label-inline"><spring:message code="indportal.enrollments.enrollmentYear"/></label>
										<select class="input-small margin0-b" data-automation-id="activeEhCoverageYear" id="activeEhCoverageYear_02" ng-model="$parent.activeEhCoverageYear" ng-options="coverageYear for coverageYear in ehCoverageYearArray" ng-change="getEnrollmentsHistory()"></select>
									</span>
								<i id="my_enrollments_toggle_icon" class="toggle icon-chevron-sign-up" ng-keyup="checkEnrollmentsSlideDiv($event, 'myEnrollments')" ng-click="enrollmentsSlideDiv('myEnrollments')" style="font-size: 17px;" tabindex="0"></i>
								</span>
						</div>
						<div id="myEnrollments" class="gutter10-lr toggle-div">
							<!--health plan title-->
							<div class="plan-title collapse-header" ng-if="myEnrollments.activeHealthEnrollments.length > 0">
								<img src="<c:url value='/resources/img/health-icon.png'/>" alt="Health Plan Image">
								<spring:message code="indportal.enrollments.healthPlan" />
								<i id="my_health_plan_toggle_icon" class="toggle icon-chevron-sign-up pull-right margin10-r margin10-t" ng-keyup="checkEnrollmentsSlideDiv($event, 'myHealthPaln')" ng-click="enrollmentsSlideDiv('myHealthPaln')" style="font-size: 17px;" tabindex="0"></i>
							</div>
							<!--health plan title end-->

							<!--health plan-->
							<div class="well-step row-fluid enrollment toggle-div" id="myHealthPaln" ng-if="myEnrollments.activeHealthEnrollments.length > 0">
								<div class="activeEnrollment" data-automation-id="activeEnrollment" id="activeEnrollment_{{outterIndex}}" ng-repeat="myEnrollment in myEnrollments.activeHealthEnrollments" ng-init="activeEnrollment = true; outterIndex = $index">
									<div ng-include="'myEnrollmentTemplate'"></div>
								</div>
							</div>
							<!--health plan end-->

							<!--dental plan title-->
							<div class="plan-title collapse-header" ng-if="myEnrollments.activeDentalEnrollments.length > 0">
								<img src="<c:url value="/resources/img/tooth-icon.png"/>" alt="Dental Plan Image">
								<spring:message code="indportal.enrollments.dentalPlan" />
								<i id="my_dental_plan_toggle_icon" class="toggle icon-chevron-sign-up pull-right margin10-r margin10-t" ng-keyup="checkEnrollmentsSlideDiv($event, 'myDentalPlan')" ng-click="enrollmentsSlideDiv('myDentalPlan')" style="font-size: 17px;" tabindex="0"></i>
							</div>
							<!--dental plan title end-->

							<!--dental plan-->
							<div class="well-step row-fluid enrollment toggle-div" id="myDentalPlan" ng-if="myEnrollments.activeDentalEnrollments.length > 0">
								<div class="activeEnrollment" id="aid_dentalActiveEnrollment_{{outterIndex}}" ng-repeat="myEnrollment in myEnrollments.activeDentalEnrollments" ng-init="activeEnrollment = true; outterIndex=$index;">
									<div ng-include="'myEnrollmentTemplate'"></div>
								</div>
							</div>
							<!--dental plan end-->
						</div>
						<!--my enrollments end-->
					</div>


					<!--#########################################################Past Enrollments##############################################################-->

					<div ng-if="myEnrollments.inactiveHealthEnrollments.length == 0 && myEnrollments.inactiveDentalEnrollments.length == 0" class="enrollmentWrap">
						<div class="header" data-automation-id="enrollmentHeader">
							<h4>
								<spring:message code="indportal.enrollments.pastEnrollments" />
								<span class="pull-right coverage-year" data-automation-id="enrollmentHeaderCoverageYear" id="enrollmentHeaderCoverageYear_01">
									<spring:message code="indportal.enrollments.enrollmentYear"/> {{activeEhCoverageYear}}
								</span>
							</h4>
						</div>

						<div class="noEnrollment">
							<p><spring:message code="indportal.portalhome.noenrollmentsfound" javaScriptEscape="true"/></p>
						</div>
					</div>

					<div ng-if="myEnrollments.inactiveHealthEnrollments.length > 0 || myEnrollments.inactiveDentalEnrollments.length > 0">
						<div class="header row-fluid margin20-t collapse-header"  data-automation-id="enrollmentHeader">
							<h4 class="col-xs-12 col-sm-7 col-md-8 col-lg-8"><spring:message code="indportal.enrollments.pastEnrollments" /></h4>
                            <span class="col-xs-12 col-sm-5 col-md-4 col-lg-4 margin10-t coverage-year_right coverage-year" data-automation-id="enrollmentHeaderCoverageYear" id="enrollmentHeaderCoverageYear_02">
								<span><spring:message code="indportal.enrollments.enrollmentYear"/> {{activeEhCoverageYear}}</span>
								<i id="my_past_enrollments_toggle_icon" class="toggle icon-chevron-sign-up" ng-keyup="checkEnrollmentsSlideDiv($event, 'pastEnrollments')" ng-click="enrollmentsSlideDiv('pastEnrollments')" style="font-size: 17px;" tabindex="0"></i>
                            </span>
						</div>

						<!--past enrollments starts-->
						<div id="pastEnrollments" class="toggle-div">
							<ul class="nav nav-tabs" id="pastEnrollmentTab">
								<li class="active" ng-if="myEnrollments.inactiveHealthEnrollments.length > 0">
									<a href="#pastHealthEnrollments" data-automation-id="pastEnrollmentTabLink" id="pastEnrollmentTabLink_01" data-toggle="tab" onclick="event.preventDefault();">
										<img src="<c:url value="/resources/img/health-icon.png"/>" alt="Health Plan Image">
										<spring:message code="indportal.enrollments.healthPlan" />
									</a>
								</li>
	  							<li ng-if="myEnrollments.inactiveDentalEnrollments.length > 0" ng-class="myEnrollments.inactiveHealthEnrollments.length == 0? 'active' : ''" >
	  								<a href="#pastDentalEnrollments" data-automation-id="pastEnrollmentTabLink" id="pastEnrollmentTabLink_02"data-toggle="tab" onclick="event.preventDefault();">
	  									<img src="<c:url value="/resources/img/tooth-icon.png"/>" alt="Dental Plan Image">
										<spring:message code="indportal.enrollments.dentalPlan" />
									</a>
								</li>
							</ul>

							<div class="tab-content">
								<div class="tab-pane active" id="pastHealthEnrollments">
									<!--health plan-->
									<div class="well-step row-fluid enrollment collapse in" ng-if="myEnrollments.inactiveHealthEnrollments.length > 0">
										<div class="pastEnrollment" data-automation-id="pastEnrollmentHealth" id="pastEnrollmentHealth_{{outterIndex}}"ng-repeat="myEnrollment in myEnrollments.inactiveHealthEnrollments" ng-init="outterIndex=$index;">
											<div ng-include="'myEnrollmentTemplate'"></div>
										</div>
									</div>
									<!--health plan end-->
								</div>

								<div class="tab-pane" ng-class="myEnrollments.inactiveHealthEnrollments.length == 0? 'active' : ''" id="pastDentalEnrollments">
									<!--health plan-->
									<div class="well-step row-fluid enrollment collapse in" ng-if="myEnrollments.inactiveDentalEnrollments.length > 0">
										<div class="pastEnrollment" data-automation-id="pastEnrollmentDental" id="pastEnrollmentDental_{{outterIndex}}" ng-repeat="myEnrollment in myEnrollments.inactiveDentalEnrollments" ng-init="outterIndex=$index;">
											<div ng-include="'myEnrollmentTemplate'"></div>
										</div>
									</div>
									<!--health plan end-->
								</div>
								<!--tab-pane end-->
							</div>
							<!--tab-content end-->
						</div>
						<!--past enrollments ends-->
					</div>

				</div>
				<!--tab-pane end-->
			</div>
			<!--tab-content end-->
		</div>
		<!--loading end-->
	</div>
	<!--rightpanel end-->
<!-- CSR Edit Termination Date Modal-->
<div ng-show="cendDateOptions.currentMode" ng-cloak>
	<div modal-show="cendDateOptions.currentMode" class="modal fade" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
			<div class="modal-content">
            	<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelCsrAction()">&times;</button>
              		<h3><spring:message code="indportal.enrollments.changeCoverageEndDate" /></h3>
				</div>
				<form name="terminationDateForm" novalidate>
				<div class="modal-body">
					  	<div class="control-group">
							<label for="change_your_termination_date" class="control-label"><spring:message code="indportal.enrollments.changeYourTerminationDate" /></label>
							<select id="change_your_termination_date" class="input-xlarge" ng-model="newCoverageEndDate" ng-options="value as value for value in terminationDates" required>
								<option value=""><spring:message code="indportal.enrollments.selectTerminationDate" /></option>
							</select>
						</div>
				</div>
				<div class="modal-footer">
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">
							<spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/>
						</a>
						<button class="btn btn-primary pull-right" ng-disabled="terminationDateForm.$invalid" ng-click="updateTerminationDate()">
							<spring:message code="indportal.contactus.submit"/>
						</button>
                	</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Modal for Successfully updateing New CoverageTerminationDate -->
<div ng-if= "cendDateOptions.openSuccessCTDModal">
      <div modal-show="cendDateOptions.openSuccessCTDModal" id="CoverageTerminationDate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="CoverageTerminationDateModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <!-- <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="CoverageTerminationDate">Successfully updated <spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/></h3>
          </div> -->
          <div class="modal-body">
          <p><spring:message code="indportal.enrollments.successful.termination.date" /></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-right" data-dismiss="modal"><spring:message code="indportal.portalhome.close" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>

<!-- Modal for Failing to update New CoverageTerminationDate -->
<div ng-if = "cendDateOptions.openFailCTDModal">
      <div modal-show="cendDateOptions.openFailCTDModal" id="CoverageTerminationDate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="CoverageTerminationDateModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="fCoverageTerminationDate"><spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-right" data-dismiss="modal"><spring:message code="indportal.portalhome.close" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>


<!-- CSR override enrollment Modal-->
<div ng-if="modalForm.csr" ng-cloak>
	<div modal-show="modalForm.csr" class="modal fade" data-keyboard="false" data-backdrop="static" id="overrideEnrollmentModal">
		<div class="modal-dialog">
			<div class="modal-content">
            	<div class="modal-header">
              		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelCsrAction()">&times;</button>
              		<h3 id="myModalLabel">
						<span>{{currentCsr.header}}</span>
			  		</h3>
            	</div>

			 <div ng-hide ="modalForm.overrideEnrollmentDetails || modalForm.overrideEnrollmentSuccessful || csrInputOverride || csrInputTermDate || SPEopened || ReinstateenrollmentData.showReinstateEnrollmentOptions || changeCovStart || modalForm.subResult || overrideNewSEPDate || activefailuremsg">
				<div class="modal-body">
					<p ng-class="{fadeFont: fadeFont}">{{currentCsr.content}}</p>
        		</div>


        		<div class="modal-body" >
					<label for="overrideText_csr" class="oRReason"><spring:message code="indportal.portalhome.viewoverridehistorycontent" javaScriptEscape="true"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></label>
            		<br>
            			<textarea id="overrideText_csr" class="overrideText" ng-model="modalForm.overrideComment" placeholder="Please explain the change you are making and why"></textarea>
            		<br>
            		<div>
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()" ng-disabled = "csrInputTermDate"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
                		<a class="btn btn-primary pull-right" ng-click="modalForm.overrideComment.length > 0 && reinstateEnrollment(modalForm.overrideComment)" class="" ng-disabled = "csrInputTermDate || modalForm.overrideComment.length < 1"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
                		<span class="pull-left">{{maxLength - modalForm.overrideComment.length}} <spring:message code="indportal.portalhome.charactersleft" javaScriptEscape="true"/></span>
             		</div>
				</div>
    		</div>

			<div ng-if="ReinstateenrollmentData.showReinstateEnrollmentOptions && !modalForm.subResult">
				<div class="modal-body">
					<div ng-if="ReinstateenrollmentData.dentalEnrollmentId || ReinstateenrollmentData.healthEnrollmentId">
						<span><spring:message code="indportal.portalhome.reinstateenrollmentMessage" javaScriptEscape="true"/></span><br><br>
						<form name="modalForm.reinstateEnrollmentOptions">
							<div>
								<input type="hidden" name="healthEnrollmentId" id="healthEnrollmentId" value="{{ReinstateenrollmentData.healthEnrollmentId}}" ng-model="ReinstateenrollmentData.healthEnrollmentId">
								<label class="checkbox" for="reinstateHealth">
									<input type="checkbox" name="reinstateHealth" id="reinstateHealth" ng-model="ReinstateenrollmentData.reinstateHealth" ng-disabled="!ReinstateenrollmentData.healthEnrollmentId">
									<spring:message code="indportal.portalhome.reinstateHealth" javaScriptEscape="true"/>
								</label>
      						</div>
      						<div>
								<input type="hidden" name="dentalEnrollmentId" id="dentalEnrollmentId" value="{{ReinstateenrollmentData.dentalEnrollmentId}}" ng-model="ReinstateenrollmentData.dentalEnrollmentId">
      							<label class="checkbox" for="reinstateDental">
									<input type="checkbox" name="reinstateDental" id="reinstateDental" ng-model="ReinstateenrollmentData.reinstateDental" ng-disabled="!ReinstateenrollmentData.dentalEnrollmentId">
									<spring:message code="indportal.portalhome.reinstateDental" javaScriptEscape="true"/>
								</label>
      						</div>
						</form>
					</div>
					<div ng-if="ReinstateenrollmentData.noenrollmentsPresent">
						<spring:message code="indportal.portalhome.noenrollmentsMessage" javaScriptEscape="true"/></span>
					</div>
					<div spinning-loader="loader"></div>
				</div>

			    <div class="modal-footer">
					<a class="btn btn-primary" ng-click="getSubsq()" ng-show="!ReinstateenrollmentData.noenrollmentsPresent" ng-disabled="!ReinstateenrollmentData.reinstateDental && !ReinstateenrollmentData.reinstateHealth"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
		            <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
	    	    </div>
			</div>

				<!-- first modal body-->
				<div ng-show="modalForm.overrideEnrollmentDetails">
            		<div class="modal-body">
						<p ng-class="{fadeFont: fadeFont}">{{currentCsr.content}}</p>

						<div class="margin10-t">
							<label for="overrideEnrollmentDetails" class="oRReason"><spring:message code="indportal.portalhome.viewoverridehistorycontent" javaScriptEscape="true"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></label>
							<span class="pull-right">{{maxLength - modalForm.overrideComment.length}} <spring:message code="indportal.portalhome.charactersleft" javaScriptEscape="true"/></span>
						</div>
                		<textarea id="overrideEnrollmentDetails" class="overrideText" ng-model="modalForm.overrideComment" placeholder="<spring:message code="indportal.enrollments.explainWhy" javaScriptEscape="true"/>"></textarea>

						<form class="form-horizontal margin10-t">

							<p class="alert alert-info">
								<spring:message code="indportal.enrollments.note" javaScriptEscape="true"/>
							</p>

							<div class="control-group">
								<label class="control-label" for="issuerAssignedPolicy"><spring:message code="indportal.enrollments.issuerPolicyID" javaScriptEscape="true"/></label>
								<div class="controls">
									<input type="text" id="issuerAssignedPolicy" class="input-medium" ng-model="overrideEnrollmentData.issuerAssignedPolicyId" ng-trim="true">
								</div>
							</div>

							<sec:accesscontrollist hasPermission="MYAPP_ENRLMNT_OVERRD_ENROL_TO_CONFIRM_LASTPRMPAIDDT" domainObject="user">
							<div class="control-group">
								<label class="control-label" for="lastPremiumPaidDate"><spring:message code="indportal.enrollments.lastPremiumPaidDate" javaScriptEscape="true"/></label>
								<div class="controls">
									<div class="input-append date datePicker">
										<input type="text" id="lastPremiumPaidDate" ui-mask="99/99/9999" placeholder="mm/dd/yyyy" model-view-value="true" class="input-small" ng-model="overrideEnrollmentData.lastPremiumPaidDate" ng-blur="validateLastPremiumPaidDate($event, 'LastPremiumPaidDateErr')" num-only>
										<span class="add-on"><i class="icon-calendar"></i></span>

									</div>
									<span ng-show="LastPremiumPaidDateErr" class="popover-content ng-binding zip-warning redBorder">{{LastPremiumPaidDateErrMsg}}</span>
								</div>
							</div>
							</sec:accesscontrollist>
							<table class="table table-condensed csrOverrideMemberTable">
								<caption class="table-caption"><spring:message code="indportal.enrollments.coveredMembers" javaScriptEscape="true"/></caption>
								<thead>
									<tr>
										<th>Member</th><th style="width: 80px"><spring:message code="indportal.enrollments.subscriber" javaScriptEscape="true"/></th><th><spring:message code="indportal.enrollments.issuerAssignedMemberID" javaScriptEscape="true"/></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="familymember in overrideEnrollmentData.familyMembers">
										<td>
											{{familymember.name}}
										</td>
										<td class="text-center">
											<i class="icon-ok" ng-if="familymember.relationship=='Self'">
										</td>
										<td>
											<input type="text" ng-model="familymember.issuerAssignedMemberId" class="input-medium" ng-trim="true">
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>

					<div class="modal-footer">
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">
							<spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/>
						</a>
						<button class="btn btn-primary pull-right" ng-click="getOverrideEnrollmentResult(modalForm.overrideComment,overrideEnrollmentData)" ng-disabled = "LastPremiumPaidDateErr || modalForm.overrideComment === undefined || modalForm.overrideComment.length < 1">
							<spring:message code="indportal.enrollments.overrideStatusConfirmed" javaScriptEscape="true"/>
						</button>

                	</div>
				</div>
				<!-- end first modal body-->

				<!-- second modal body-->
				<div ng-show="modalForm.overrideEnrollmentSuccessful">
            		<div class="modal-body">
						<h3><spring:message code="indportal.portalhome.csrsubmissionsucceed" javaScriptEscape="true"/></h3>
			    		<p> <spring:message code="indportal.portalhome.csroktogoback" javaScriptEscape="true"/></p>
                	</div>

					<div class="modal-footer">
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="refreshEnrollmentHistory()">
							<spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/>
						</a>
                	</div>
				</div>
				<!-- end third modal body-->

				<!-- fail modal body-->
				<div ng-show="modalForm.subResult === 'failure'">
					<div class="modal-body">
						<h3><spring:message code="indportal.portalhome.csrsubmissionfailtitle" javaScriptEscape="true"/></h3>
						<p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
						<p><spring:message code="indportal.portalhome.csrsubmissionfailcontent2" javaScriptEscape="true"/></p>
					</div>
					<div class="modal-footer">
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">
							<spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/>
						</a>
                	</div>
				</div>
				<!--end fail modal body-->
            </div>
		</div>
	</div>
</div>
<!-- CSR Modal-->


<!-- CSR override History Modal-->
<div ng-show="modalForm.csrOverrideHistory" ng-cloak>
	<div modal-show="modalForm.csrOverrideHistory" class="modal fade" data-keyboard="false" data-backdrop="static" id="csrOverrideHistoryModal">
		<div class="modal-dialog">
			<div class="modal-content">
            	<div class="modal-header">
              		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelCsrAction()">&times;</button>
              		<h3>
						<spring:message code="indportal.enrollments.overrideHistory" javaScriptEscape="true"/>
			  		</h3>
            	</div>

				<!-- success modal body-->
				<div class="modal-body" ng-show="modalForm.subResult === true">
					<p ng-if="overrideHistoryData.length === 0">No Override History found.</p>
					<table class="table" ng-if="overrideHistoryData.length > 0">
						<tr ng-repeat="history in overrideHistoryData">
							<td>
								<div class="override-historyData-header">
									<strong>{{history.commenterName}}</strong> <spring:message code="indportal.enrollments.addedComment" javaScriptEscape="true"/> - {{history.commentedDate}}
								</div>
								<div class="gutter5-l">{{history.overrideComment}}</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="modal-footer">
					<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">
						<spring:message code="indportal.portalhome.close" javaScriptEscape="true"/>
					</a>
                </div>
				<!-- success modal body-->

				<!-- fail modal body-->
				<div ng-show="modalForm.subResult === 'failure'">
					<div class="modal-body">
						<h3><spring:message code="indportal.portalhome.csrsubmissionfailtitle" javaScriptEscape="true"/></h3>
						<p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
						<p><spring:message code="indportal.portalhome.csrsubmissionfailcontent2" javaScriptEscape="true"/></p>
					</div>
					<div class="modal-footer">
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">
							<spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/>
						</a>
                	</div>
				</div>
				<!--end fail modal body-->
			</div>
		</div>
	</div>
</div>

<!-- Failure Modal 2-->
<div ng-show="payFHFail">
 <div modal-show="payFHFail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedFHModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="failedFHModal"><spring:message code="indportal.plansummary.technicalissues" javaScriptEscape="true"/></h3>
        </div>
        <div class="modal-body gutter20">
          <p><spring:message code="indportal.plansummary.cannotsubmitrequest" javaScriptEscape="true"/></p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
</div>
<!-- Failure Modal 2-->
<!-- Payment success Modal-->
<div ng-show="paySuccess">
	<div modal-show="paySuccess" class="modal fade" tabindex="-1"
		role="dialog" aria-labelledby="paySuccessModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h3 id="paySuccessModal">Complete your Payment</h3>
				</div>
				<div class="modal-body gutter20">
					<p>Click 'Proceed To Payment' to complete your payment on the payment portal.</p>
				</div>
				<div class="modal-footer">
					<form id="postform" name="postform" method="get" action="finance/paymentRedirect" target="_blank">
						<a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message
							code="indportal.portalhome.cancel" javaScriptEscape="true" /></a>
						<input type="hidden" id="finEnrlDataKey" name="finEnrlDataKey"/>
						<input type="submit" value="Proceed To Payment" class="btn btn-primary" ng-click="payHealthExternal($event)" onclick="payHealthExternalDlEvent()">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Payment success Modal -->

<!-- Disenroll modals Start -->
<div spinning-loader="loader"></div>
<!-- Pending app dialog -->
<div ng-show="pendingAppDialog">
 <div modal-show="pendingAppDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="pendingAppModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="pendingAppModal"><spring:message code="indportal.pendingAppModal.header" javaScriptEscape="true"/></h3>
        </div>
        <div class="modal-body">
			<spring:message code="indportal.pendingAppModal.body"/>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Disenroll Modal-->
<div ng-show="disenrollDialog">
 <div modal-show="disenrollDialog" id="disenrollDialog" class="modal fade" tabindex="0" role="dialog" aria-labelledby="disenrollModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" id="cancelDisenroll_01" data-dismiss="modal" aria-hidden="true" ng-click="cancelDisenroll()">&times;</button>
          <h3 id="disenrollModal">
						<span ng-if="enrollmentAction"><spring:message code="indportal.plansummary.voluntarydisenrollmentreasons" javaScriptEscape="true"/></span>
						<span ng-if="!enrollmentAction"><spring:message code="indportal.plansummary.cancelcoverage" javaScriptEscape="true"/></span>
					</h3>
        </div>
        <div class="modal-body ">
          <form class="form-horizontal">
						<h5 ng-if="enrollmentAction" class="margin10-l" id="subtitle"><strong><spring:message code="indportal.plansummary.whyareyoudisenrolling" javaScriptEscape="true"/></strong></h5>
						<h5 ng-if="!enrollmentAction" class="margin10-l" id="subtitle"><strong><spring:message code="indportal.plansummary.cancel.whyareyoudisenrolling" javaScriptEscape="true"/></strong></h5>

            <div class="margin30-l">
              <label class="radio">
                  <input type="radio" name="disenrollmentreason" id="disaffordability" value="CANNOT_AFFORD_PAYMENT" checked="" required="" class="" ng-model="checked">
                  <spring:message code="indportal.plansummary.icantafford" javaScriptEscape="true"/>
              </label>
              <br>
			  			<label class="radio" >
								<input type="radio" name="disenrollmentreason" id="disnothappywithplan" value="NO_PROPER_SERVICE" required="" class="" ng-model="checked">
								<spring:message code="indportal.plansummary.imnothappywith" javaScriptEscape="true"/>
								<span ng-if="stateCode !== 'CA' && stateCode !== 'MN'">
										<span ng-if="typeOfPlan == 'Dental'"><spring:message code="indportal.plansummary.dental" javaScriptEscape="true"/></span>
										<span ng-if="typeOfPlan == 'Health'"><spring:message code="indportal.plansummary.health" javaScriptEscape="true"/></span>
										<span ng-if="typeOfPlan == 'both'"><spring:message code="indportal.plansummary.health" javaScriptEscape="true"/> and <spring:message code="indportal.plansummary.dental" javaScriptEscape="true"/></span>
										<spring:message code="indportal.plansummary.plan" javaScriptEscape="true"/>
								</span>
              </label>
			  			<br>
			  			<label class="radio">
								<input type="radio" name="disenrollmentreason" id="outOfnetwork" value="PHY_OUT_OF_NETWORK" required="" class="" ng-model="checked">
								<spring:message code="indportal.plansummary.phyOutOfNetwork" javaScriptEscape="true"/>
              </label>
							<br>
							<label class="radio">
								<input type="radio" name="disenrollmentreason" id="agentError" value="AGENT_ERROR" class="" ng-model="checked">
								<spring:message code="indportal.plansummary.agentError"/>
							</label>
							<br>
              <label class="radio">
                  <input type="radio" name="disenrollmentreason" id="other" value="OTHER" class="" ng-model="checked">
                  <spring:message code="indportal.plansummary.other"/>
              </label>
            </div>
            <div id="otherDisenrollReason" ng-if="checked=='OTHER'">
              <div class="margin30-l">
                <input type="text" name="tellusmore" id="tellusmore" class="span11" ng-model="$parent.otherReason"/>
              </div>
            </div>
          </form>
			  <div ng-if="stateCode !== 'MN'" class="margin5-l margin5-r margin20-t alert alert-info margin0-b">
              <p><spring:message code="indportal.plansummary.otherReasonText"/></p>
              </div>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0);" class="btn btn-secondary" id="cancelDisenroll__02" data-dismiss="modal" ng-click="cancelDisenroll()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
          <a href="javascript:void(0);" class="btn btn-primary" id="aid_skip" ng-if="!checked" ng-click="disenrollmentReason()" class=""><spring:message code="indportal.customGrouping.skipAndContinue" javaScriptEscape="true"/></a>
          <a href="javascript:void(0);" class="btn btn-primary" id="aid_continue" ng-if="checked" ng-click="disenrollmentReason()" class=""><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Disenroll Modal-->
<!-- Are You Sure Modal-->
<div ng-show="areYourSureDialog">
 <div modal-show="areYourSureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollCheckModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div  id="disenrollCheckModal" class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<span ng-if="typeOfPlan == 'Dental' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinue.dental" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'Health' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinue" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'both' && enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinueboth" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'Dental' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.cancel.doyouwanttocontinue.dental" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'Health' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.cancel.doyouwanttocontinue.health" javaScriptEscape="true"/></h3></span>
					<span ng-if="typeOfPlan == 'both' && !enrollmentAction"><h3><spring:message code="indportal.plansummary.doyouwanttocontinueboth" javaScriptEscape="true"/></h3></span>
        </div>
        <div class="modal-body gutter20">
		  <span ng-if="typeOfPlan == 'Health'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll" javaScriptEscape="true"/></p>
					</span>
			<span ng-if="stateCode === 'MN' && typeOfPlan == 'Dental'">
				<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll" javaScriptEscape="true"/></p>
				<p><spring:message code="indportal.plansummary.ifyoucanaffordbut" javaScriptEscape="true"/></p>
			</span>
		  <span ng-if="stateCode !== 'MN' && typeOfPlan == 'Dental'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenrolldental" javaScriptEscape="true"/></p>
		 	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2dental" javaScriptEscape="true"/></p>
          </span>
		  <span ng-if="stateCode !== 'MN' && typeOfPlan == 'both'">
          	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenrollboth" javaScriptEscape="true"/></p>
		 	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2both" javaScriptEscape="true"/> <a href="https://www.coveredca.com/individuals-and-families/getting-covered/special-enrollment/qualifying-life-events/" target="_blank"><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll2_1" javaScriptEscape="true"/></a></p>
		  	<p><spring:message code="indportal.plansummary.youhaveseelctedtodisenroll3both" javaScriptEscape="true"/></p>
          </span>
		  <span ng-if="typeOfPlan == 'Health' && dentaldetails && dentaldetails.disEnroll && dentaldetails.electedAptc > 0">
			<p><spring:message code="indportal.plansummary.aptcwarning" javaScriptEscape="true"/></p>
		  </span>
          <div class="margin20-t alert alert-info margin0-b" ng-if="typeOfPlan == 'Health' || typeOfPlan == 'both'">
            <spring:message code="indportal.plansummary.ifyoucanaffordbut" javaScriptEscape="true"/>
            <br><br>
			<div ng-if="stateCode !== 'MN'"><spring:message code="indportal.plansummary.thefeecontent4" javaScriptEscape="true"/> <a href="<spring:message code='indportal.plansummary.yhiExemptionsUrl'/>" target="_blank"> <spring:message code="indportal.plansummary.yhiExemptionsUrl"/></a></div>
          </div>
        </div>
        <div ng-if="stateCode !== 'MN'" class="modal-footer">
          <a href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.no" javaScriptEscape="true"/></a>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goToDisenrollDateModal()" class=""><spring:message code="indportal.portalhome.yes" javaScriptEscape="true"/></a>
				</div>
				<div ng-if="stateCode === 'MN'" class="modal-footer">
						<a href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
						<a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goToDisenrollDateModal()" class=""><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
					</div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Are You Sure Modal-->
<!-- Choosing Disenrollment Date Modal-->
<div ng-show="disenrollDateDialog.step">
 <div modal-show="disenrollDateDialog.step" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disenrollCheckModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="disenrollDateModal" ng-if="disenrollDateDialog.step == 1"><spring:message code="indportal.plansummary.selectTerminationDate"/></h3>
		  <h3 id="ConfirmDisenrollDateModal" ng-if="disenrollDateDialog.step == 2"><spring:message code="indportal.plansummary.confirmEndDate"/></h3>
		  <h3 id="ConfirmSubmitDisenrollDateModal" ng-if="disenrollDateDialog.step == 3"><spring:message code="indportal.plansummary.confirmation"/></h3>
        </div>
        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 1">
		  <form class="form-horizontal">
            <div class="margin30-l">
							<label class="radio margin5-b">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="CURRENT_MONTH" ng-click="saveSelectedDate('CURRENT_MONTH')">
									<spring:message code="indportal.plansummary.disenrollmentDates.lastDayCurrentMonth"/>
							</label>
							<label class="radio margin5-b">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="NEXT_MONTH" ng-click="saveSelectedDate('NEXT_MONTH')">
									<spring:message code="indportal.plansummary.disenrollmentDates.lastDayNextMonth"/>
							</label>
							<label class="radio margin5-b" ng-if="actualDatesEndOfMonthAfterNext">
                  <input type="radio" name="disenrollmentdate" checked="" required="" class="" ng-model="disenrollDateDialog.date" value="MONTH_AFTER_NEXT_MONTH" ng-click="saveSelectedDate('MONTH_AFTER_NEXT_MONTH')">
									<spring:message code="indportal.plansummary.disenrollmentDates.lastDayAfterMonth"/>
              </label>
            </div>

		  </form>
		  <div class="margin20-t alert alert-info margin0-b"><spring:message code="indportal.plansummary.needSpecificTermDate"/></div>
        </div>

        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 2">
					<p>
						<span ng-if="typeOfPlan === 'Health'"><spring:message code="indportal.plansummary.chosenEndDate.health"/></span>
						<span ng-if="typeOfPlan === 'Dental'"><spring:message code="indportal.plansummary.chosenEndDate.dental"/></span>
						<span ng-if="typeOfPlan === 'both'"><spring:message code="indportal.plansummary.chosenEndDate.both"/></span>
					</p>

		  <div class="margin20-t margin0-b disenroll-confirm-box">
			<!--<p><spring:message code="indportal.plansummary.from"/></p>-->
			<div ng-if="typeOfPlan == 'Health'"><img class="pull-left" ng-src="{{healthdetails.planLogoUrl}}" alt="{{healthdetails.issuerName}} logo"></div>
			<div ng-if="typeOfPlan == 'Dental'"><img class="pull-left" ng-src="{{dentaldetails.planLogoUrl}}" alt="{{dentaldetails.issuerName}} logo"></div>
			<div ng-if="typeOfPlan == 'both'"><img class="pull-left" ng-src="{{healthdetails.planLogoUrl}}" alt="{{healthdetails.issuerName}} logo">
			<img class="pull-right" ng-src="{{dentaldetails.planLogoUrl}}" alt="{{dentaldetails.issuerName}} logo"></div>
          </div>
        </div>
        <div class="modal-body gutter20" ng-if="disenrollDateDialog.step == 3">
		  	<div class="margin20-t alert alert-info margin0-b disenroll-confirm-box">
					<p ng-if="typeOfPlan === 'Health'">
						<spring:message code="indportal.plansummary.disenrollConfirmContent.health"/>
					</p>
					<p ng-if="typeOfPlan === 'Dental'">
						<spring:message code="indportal.plansummary.disenrollConfirmContent.dental"/>
					</p>
					<p ng-if="typeOfPlan === 'both'">
						<spring:message code="indportal.plansummary.disenrollConfirmContent.both"/>
					</p>
				</div>
		  <div class="margin20-t alert alert-info margin0-b">
			<spring:message code="indportal.plansummary.note"/>:
			<ul>
				<li><spring:message code="indportal.plansummary.disenrollConfirmPt1"/></li>
				<li ng-if="stateCode === 'MN'"><spring:message code="indportal.plansummary.disenrollConfirmPt2"/></li>
			</ul>
		  </div>
        </div>

        <div class="modal-footer" ng-if="disenrollDateDialog.step == 1">
					<a ng-if="stateCode === 'MN'" href="#" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
          <button class="btn btn-primary" ng-click="disenrollDateDialog.step = 2" ng-disabled="!disenrollDateDialog.date"><spring:message code="indportal.portalhome.continue"/></button>
        </div>
        <div class="modal-footer" ng-if="disenrollDateDialog.step == 2">
          <button class="btn btn-secondary" ng-click="disenrollDateDialog.step = 1"><spring:message code="indportal.plansummary.updateTermDate"/></button>
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-click="disenrollDateDialog.step = 3"><spring:message code="indportal.portalhome.continue"/></a>
        </div>
        <div class="modal-footer" ng-if="disenrollDateDialog.step == 3">
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" class="" ng-click="submitDisenrollment()"><spring:message code="indportal.contactus.submit"/></a>
        </div>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
<!-- Choosing Disenrollment Date Modal-->
<!-- SubmitDisenroll-->
<div ng-show="submitDisenrollmentDialog">
 <div modal-show="submitDisenrollmentDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="submitDisenrollModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="submitDisenrollModal">
						<span ng-if="enrollmentAction"> <spring:message code="indportal.plansummary.voluntarydisenrollmentrequestsubmitted" javaScriptEscape="true"/></span>
						<span ng-if="!enrollmentAction"> <spring:message code="indportal.plansummary.cancelcoveragerequest" javaScriptEscape="true"/></span>
					</h3>
        </div>
        <div class="modal-body gutter20">
					<p>
						<span ng-if="stateCode !== 'MN' && enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewed" javaScriptEscape="true"/></span>
						<span ng-if="stateCode === 'MN' && enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewedcancelcoverage" javaScriptEscape="true"/></span>
						<span ng-if="!enrollmentAction"> <spring:message code="indportal.plansummary.wehavereviewedcancelcoverage" javaScriptEscape="true"/></span>
					</p>
          <!--<p><spring:message code="indportal.plansummary.youwillbecovered" javaScriptEscape="true"/></p>-->
          <p ng-if="stateCode !== 'MN'"><spring:message code="indportal.plansummary.ifyouhaveanyquestions" javaScriptEscape="true"/></p>
        </div>
        <div class="modal-footer">
          <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goHome()"><spring:message code="indportal.portalhome.godashboard" javaScriptEscape="true"/></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
</div>
</div>
<!-- SubmitDisenroll-->
<!-- SubmitDisenroll Failure Modal-->
<div ng-show="submitDisenrollmentFailureDialog">
 <div modal-show="submitDisenrollmentFailureDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedSumissionModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="failedSumissionModal"><spring:message code="indportal.plansummary.technicalissues" javaScriptEscape="true"/></h3>
        </div>
        <div class="modal-body gutter20">
          <p><spring:message code="indportal.plansummary.cannotsubmitrequest" javaScriptEscape="true"/></p>
          <p><spring:message code="indportal.plansummary.ifyouhaveanyquestions" javaScriptEscape="true"/></p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-secondary" data-dismiss="modal" href="/#"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
      </div><!-- /.modal-content-->
    </div> <!-- /.modal-dialog-->
   </div>
  </div>
</div>
<!-- SubmitDisenroll Failure Modal-->
<!-- Disenroll modals End -->
</script>


<script type="text/ng-template" id="myEnrollmentTemplate">
<div class="enrollmentTemplate" data-automation-id="enrollmentTemplate">

	<!--plan-benefit-->
	<div class="row-fluid plan-benefit">
		<!--info-header-->
		<div class="txt-center col-xs-12 col-md-6 col-lg-6">
			<img ng-src="{{myEnrollment.planLogoUrl}}" alt="{{myEnrollment.issuerName}} logo">
			<p>
				<a href="javascript:void(0)">{{myEnrollment.planName}}</a>
			</p>

			<div class="margin20-t">
				<a class="btn btn-block margin10-b" data-automation-id="viewBenefitDetailsBtn" id="viewBenefitDetailsBtn" target="_blank" href="<c:url value="/private/planinfo/{{myEnrollment.coverageStartDate | removeForwardSlashes}}/{{myEnrollment.planId}}" />"><spring:message code="indportal.enrollments.viewBenefitDetails" /></a>
			</div>
		</div>
		<!--info-header end-->

		<!--benefit-summary-->
		<div class="col-xs-12 col-md-5 col-lg-5" data-automation-id="benefitSummary" id="benefitSummary">
			<table class="table table-condensed">
				<caption class="table-caption" data-automation-id="benefitSummaryHeader" id="benefitSummaryHeader"><spring:message code="indportal.enrollments.benefitSummary" /></caption>
				<tr>
					<th>
						<spring:message code="indportal.enrollments.planType"/>
					</th>
					<td>
						{{myEnrollment.productType}}
					</td>
				</tr>

				<tr>
					<th>
						<div ng-if="myEnrollment.planType === 'Health' && stateCode !== 'MN'"><spring:message code="indportal.enrollments.officeVisit" /></div>
						<div ng-if="myEnrollment.planType === 'Health' && stateCode === 'MN'"><spring:message code="indportal.enrollments.primaryCareVisit" /></div>
						<div ng-if="myEnrollment.planType === 'Dental' && stateCode !== 'MN'"><spring:message code="indportal.enrollments.routineDental" /></div>
						<div ng-if="myEnrollment.planType === 'Dental' && stateCode === 'MN'"><spring:message code="indportal.enrollments.dentalOfficevisit" /></div>
					</th>
					<td><span ng-bind-html="myEnrollment.officeVisit"></span></td>
				</tr>

				<tr ng-if="myEnrollment.planType === 'Health'">
					<th><spring:message code="indportal.enrollments.genericMedications"/></th>
					<td><span ng-bind-html="myEnrollment.genericMedications"></span></td>
				</tr>

				<tr>
					<th>
						<div ng-if="myEnrollment.planType === 'Health'"><spring:message  code="indportal.enrollments.deductible" /></div>
						<div ng-if="myEnrollment.planType === 'Dental'"><spring:message  code="indportal.enrollments.dentalDeductible" /></div>
					</th>
					<td>{{myEnrollment.deductible}}</td>
				</tr>

				<tr>
					<th>
						<spring:message code="indportal.enrollments.outOfPocketMaximum" />
					</th>
					<td>{{myEnrollment.oopMax}}</td>
				</tr>
			</table>
		</div>
		<!--benefit-summary end-->
	</div>
	<!--plan-benefit, rowfluid end-->

	<div class="row-fluid margin10-b">
		<!--plan-summary-->
		<div class="col-xs-12 col-md-5 col-lg-5" data-automation-id="planSummary" id="planSummary">
			<table class="table table-condensed">
				<caption class="table-caption" data-automation-id="planSummaryHeader" id="planSummaryHeader"><spring:message code="indportal.enrollments.PlanSummary" /></caption>
				<tr ng-if="myEnrollment.policyId !== null">
					<th>
						<spring:message code="indportal.enrollments.policyId" />
					</th>
					<td>
						<span>{{myEnrollment.policyId}}</span>
					</td>
				</tr>

				<tr>
					<th><spring:message code="indportal.enrollments.coverageStartDate" /></th>
					<td>{{myEnrollment.coverageStartDate}}</td>
				</tr>

				<tr>
					<th><spring:message code="indportal.enrollments.coverageEndDate" /></th>
					<td>{{myEnrollment.coverageEndDate}}</td>
				</tr>

				<tr>
					<th><spring:message code="indportal.enrollments.enrollmentStatus" /></th>
					<td>
						<a href="#" ng-if="myEnrollment.enrollmentStatus.toUpperCase() === 'PENDING'" class="ttclass" rel="tooltip" aria-tooltip data-original-title="<spring:message code="indportal.plansummary.enrollmentCompletePendingTooltip"/>">
							{{myEnrollment.enrollmentStatus}}
						</a>
						<span ng-if="myEnrollment.enrollmentStatus.toUpperCase() !== 'PENDING'">
							{{myEnrollment.enrollmentStatus}}
						</span>
					</td>

				</tr>

				<tr>
					<th><spring:message code="indportal.enrollments.monthlyPremium" /></th>
					<td>{{myEnrollment.monthlypremium | currency}}</td>
				</tr>

				<tr ng-if="myEnrollment.electedAptc !== null && myEnrollment.electedAptc !== 'null'">
					<th>
						<spring:message code="indportal.enrollments.electedAPTC" />
					</th>
					<td>{{myEnrollment.electedAptc | currency}}</td>
				</tr>
				<tr ng-if="stateCode=='CA' && myEnrollment.electedStateSubsidy !== undefined && myEnrollment.electedStateSubsidy !== null && myEnrollment.electedStateSubsidy !== 'null'">
                    <th>
                        <spring:message code="indportal.enrollments.electedStateSubsidy" />
                    </th>
                    <td>{{myEnrollment.electedStateSubsidy | currency}}</td>
                </tr>

				<tr>
					<th><spring:message code="indportal.enrollments.netPremium" /></th>
					<td>{{myEnrollment.netPremium | currency}}</td>
				</tr>

				<tr ng-if="myEnrollment.premiumEffDate !== null">
					<th>
						<spring:message code="indportal.plansummary.premiumEffectiveDate"/><a class="ttclass" rel="tooltip" aria-tooltip href="javascript:void(0)"
						aria-hidden="<spring:message code="indportal.plansummary.premiumEffectiveDateTooltip"/>" data-original-title="<spring:message code="indportal.plansummary.premiumEffectiveDateTooltip"/>"><span class="hidden"><spring:message code="indportal.plansummary.premiumEffectiveDate"/> Help Text</span><i class="icon-question-sign" style="color: #D8790D;"></i></a>:
					</th>
					<td>{{myEnrollment.premiumEffDate}}</td>
				</tr>
			</table>
		</div>
		<!--plan-summary end-->

		<!--covered-family-members-->
		<div class="col-xs-12 col-md-7 col-lg-7 covered-family-members">


			<table class="table table-condensed" data-automation-id="contactYourCarrier" id="contactYourCarrier">
				<caption class="table-caption">
					<spring:message code="indportal.enrollments.contactYourCarrier"/>
				</caption>
				<tr data-automation-id="customerService" id="customerService">
					<th>
						<spring:message code="indportal.enrollments.customerService"/>
					</th>
					<td>{{myEnrollment.issuerPhone}}</td>
				</tr>

				<tr data-automation-id="contactYourCarrierWeb" id="contactYourCarrierWeb">
					<th>
						<spring:message code="indportal.enrollments.web"/>
					</th>
					<td ng-if="myEnrollment.issuerSite !== null && myEnrollment.issuerSite !== 'null'">
						<a href="{{myEnrollment.issuerSite}}" data-automation-id="issuerSite" target="_blank"><spring:message code="indportal.enrollments.clickHere"/></a>
					</td>
				</tr>

			</table>

			<table class="table table-condensed" data-automation-id="coveredFamilyMembers" id="coveredFamilyMembers">
				<caption class="table-caption">
					<spring:message code="indportal.enrollments.coveredFamilyMembers" />
				</caption>
				<tr ng-repeat="member in myEnrollment.enrolleeHistoryEntries" data-automation-id="enrolleeHistoryEntries" id="enrolleeHistoryEntries_{{outterIndex}}" ng-init="outterIndex=$index;">
					<td data-automation-id="enrolleeRelationship" id="enrolleeRelationship_{{outterIndex}}">{{member.relationship}}</td>
					<td data-automation-id="enrolleeName" id="enrolleeName_{{outterIndex}}">{{member.name}}</td>
					<td data-automation-id="coverageDate" id="coverageDate_{{outterIndex}}">{{member.startDate | date:"MM/dd/yyyy"}} - {{member.endDate | date:"MM/dd/yyyy"}}</td>
				</tr>
			</table>
		</div>
		<!--covered-family-members end-->
	</div>

	<div class="enrollment-btn_wrap" ng-if="activeEnrollment === true">
		<a class="btn btn-primary" data-automation-id="terminationDate" id="terminationDate" ng-if="myEnrollment.terminationDateChangeAllowed === true" ng-click="editTerminationDate(myEnrollment)"><spring:message code="indportal.enrollments.terminationDate"/></a>

		<c:if test="${stateCode != 'MN'}">
			<a class="btn btn_res enrollment-btn" data-automation-id="payForPlan" id="payForPlan_01" ng-if="myEnrollment.allowPaymentRedirect && myEnrollment.enrollmentStatus != null && myEnrollment.enrollmentStatus.toUpperCase() === 'PENDING'" href="javascript:void(0)" ng-click="payForHealth(myEnrollment.caseNumber, myEnrollment.planType, myEnrollment.enrollmentId)">
				<span data-automation-id="payForPlan_Health" id="payForPlan_Health_01" ng-if="myEnrollment.planType == 'Health'"><spring:message code="indportal.enrollments.payForHealthPlan"/></span>
				<span data-automation-id="payForPlan_Dental" id="payForPlan_Dental_01" ng-if="myEnrollment.planType == 'Dental'"><spring:message code="indportal.enrollments.payForDentalPlan"/></span>
			</a>
		</c:if>
	<span ng-if="myEnrollment.allowDisenroll" data-automation-id="disenrollFromPlan" id="disenrollFromPlan">
		<sec:accesscontrollist hasPermission="IND_PORTAL_DISENROLL" domainObject="user">
			<a href="javascript:void(0)" class="btn btn-primary btn_save" ng-if="myEnrollment.planType == 'Health'" ng-click="disenrollAlert('enrollmentHistory', myEnrollment.planType, myEnrollment.enrollmentId, myEnrollment.validCoverageHealth)">
				<span ng-show="myEnrollment.validCoverageHealth" data-automation-id="disenrollFromPlan_Health" id="disenrollFromPlan_Health_01"><spring:message code="indportal.enrollments.disenrollFromHealthPlan"/></span>
				<span ng-show="!myEnrollment.validCoverageHealth" data-automation-id="disenrollFromPlan_Health" id="disenrollFromPlan_Health_02"><spring:message code="indportal.plansummary.cancelCoverage"/></span>

			</a>
			<a href="javascript:void(0)" class="btn btn-primary btn_save" ng-if="myEnrollment.planType == 'Dental'" ng-click="disenrollAlert('enrollmentHistory', myEnrollment.planType, myEnrollment.enrollmentId, myEnrollment.validCoverageDental)">
				<span ng-show="myEnrollment.validCoverageDental" data-automation-id="disenrollFromPlan_Dental" id="disenrollFromPlan_Dental_01"><spring:message code="indportal.enrollments.disenrollFromDentalPlan"/></span>
				<span ng-show="!myEnrollment.validCoverageDental" data-automation-id="disenrollFromPlan_Dental" id="disenrollFromPlan_Dental_02"><spring:message code="indportal.plansummary.cancelCoverage"/></span>
			</a>
		</sec:accesscontrollist>
	</span>
	</div>
</div>
<!--end row-fluid-->

	<%-- <sec:accesscontrollist hasPermission="MYAPP_ENRLMNT_ACTIONS" domainObject="user">
	<div class="enrollmentCsrActionDiv">
		<!-- CSR MENU -->
		<div class="csrActionBorderDiv">
			<div class="csrActionBtnDiv" ng-click="enrollmentCsrAction($event)" data-toggle="tooltip" rel="tooltip" data-placement="top" data-original-title="<strong><spring:message code="indportal.enrollments.overrideFunctions" javaScriptEscape="true"/></strong>" data-html="true" tabindex="-1">
				<div class="gutter5">
					<i class="icon-gear csrActionGear"></i>
					<span><spring:message code="indportal.portalhome.actions" javaScriptEscape="true"/></span>
				</div>
			</div>
		</div>
		<!-- CSR MENU -->

		<!-- CSR OPTIONS -->
		<div class="csrButtonDiv" style="display:none">
			<div class="center csrBtns">
			<sec:accesscontrollist hasPermission="MYAPP_ENRLMNT_OVRRD_ENRL_TO_CONFIRM" domainObject="user">
				<!-- <a ng-click="getOverrideHistory(myEnrollment.caseNumber,myEnrollment.enrollmentId)" class="btn"><spring:message code="indportal.enrollments.viewOverrideHistory" javaScriptEscape="true"/></a>  -->
				<a ng-if ="myEnrollment.enrollmentStatus == 'Pending'" ng-click="openOverrideEnrollStatus(myEnrollment.caseNumber,'overrideEnrollment', myEnrollment)" id="OverrideEnrollStatus" class="btn"><spring:message code="indportal.enrollments.overrideEnrollmentStatus" javaScriptEscape="true"/></a>
			</sec:accesscontrollist>
			<sec:accesscontrollist hasPermission="IND_PORTAL_REINSTATE_ENROLLMENT,MYAPP_REINST_ENROLLMENT" domainObject="user">
				<a ng-if ="myEnrollment.enrollmentStatus == 'Cancelled'  || myEnrollment.enrollmentStatus == 'Terminated'" ng-click="openCSR(myEnrollment, 'reinstate')" id="reinstateenrollment" data-autamation-id="reinstateenrollment" class="btn csractions"><spring:message code="indportal.portalhome.reinstateenrollment" javaScriptEscape="true"/></a>
			</sec:accesscontrollist>
			</div>

		</div>
		<!-- CSR OPTIONS -->
	</div>
	</sec:accesscontrollist> --%>

</script>
