<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>


<link type="text/css" rel="stylesheet"
	href="<c:url value="/resources/css/qdp.css" />" />

<div class="gutter10">
	<div class="row-fluid">
            <ul class="page-breadcrumb">
                <li><a href="#">&lt; <spring:message code="label.back"/></a></li>
                <li><a href="#"><spring:message code="label.plans"/></a></li>
                <li><a href="#"><spring:message code="label.linkManageQDP"/></a></li>
                <li><spring:message code="label.reviewandsubmit"/></li>
            </ul><!--page-breadcrumb ends-->
			<h1><a name="skip"></a><spring:message code="label.addNewQDP"/></h1>
		</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
				<div class="header">
					<!--  beginning of side bar -->
					<h4 class="margin0"><spring:message code='label.steps'/></h4>
				</div>
					<ol class="nav nav-list">
						<li class="done"><a href="#"><spring:message code='label.linkDentalPlanDetails'/></a></li>
						<li class="done"><a href="#"><spring:message code='label.linkDentalPlanBenefits'/></a></li>
						<li class="done"><a href="#"><spring:message code='label.linkDentalPlanRates'/></a></li>
						<li class="done"><a href="#"><spring:message code='label.linkProviderNetwork'/></a></li>
						<li class="active"><a href="#"><spring:message code='label.reviewandconfirm'/></a></li>
					</ol>
					<!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9 gray" id="rightpanel">
			<div class="header">
					<h4 class="margin0"><spring:message code='label.step5'/> <spring:message code='label.reviewandconfirm'/></h4>
				</div>
                 
				<table class="table table-border-none marginL5">
					<tr>
						<td class="span4 txt-right"><spring:message code='label.issuer'/></td>
						<td><strong>${issuerName}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.planName'/></td>
						<td><strong>${planName}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.planNumber'/></td>
						<td><strong>${planNumber}</strong></td>
					</tr>
					<!-- repeating area -->
					<tr>
						<td class="txt-right"><spring:message code='label.planMarket'/></td>
						<td>${planMarket}</td>
					</tr>
                    <tr>
						<td class="txt-right"><spring:message code='label.planLevel'/></td>
						<td>${planLevel}</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.planStartDate'/></td>
						<td>${startDate}</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.planEndDate'/></td>
						<td>${endDate}</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.actuarialVC'/></td>
						<td>${actCerti}</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.planBenefitData'/></td>
						<td>${benefitFile}</td>
					</tr>
                    <tr>
						<td class="txt-right"><spring:message code='label.planBrochure'/></td>
						<td>${brochureFile}</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.planRateData'/></td>
						<td>${rateFile}</td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.linkProviderNetwork'/></td>
						<td>${netName}</td>
					</tr>
				</table>
				<div class="form-actions paddingL225">
					<img id="loadingImg1" align="right" src="<c:url value="/resources/images/loader.white.gif" />" alt="Loader"/>
					<button type="submit" id="submit1" name="submit1" class="btn btn-primary" onClick="changeState()"><spring:message code='label.btnSubmit'/></button>
				</div>
				<!-- end of form-actions -->
				
				<form class="form-stacked" id="frmChangeState" name="frmChangeState" action="<c:url value="/admin/planmgmt/submitqdpstatus" />" method="post" enctype="multipart/form-data">
					<input type="hidden" name="planId" id="planId" value="${planId}" />
				</form>
			</div>
		</div>
	</div>

	<div id="confirmBox">
		<div class="message"></div>
	</div>

	<script type="text/javascript">
function savePageAsPDF() { 
	  var pURL = "http://savepageaspdf.pdfonline.com/pdfonline/pdfonline.asp?cURL=" + escape(document.location.href); 
	  window.open(pURL, "PDFOnline", "scrollbars=yes, resizable=yes,width=640, height=480,menubar,toolbar,location");
} 

function changeState() {
	$("#frmChangeState").submit();
}

$(document).ready(function(){
	$('#loadingImg1').hide();
    var changeStatus = { 
	        beforeSubmit:  submitChangeStateRequest,  // pre-submit callback 
	        success:       submitChangeStateResponse  // post-submit callback 
	    };
    $('#frmChangeState').ajaxForm(changeStatus);
});

function submitChangeStateResponse(responseText, statusText, xhr, $form)  {	
	doSubmit("<spring:message code='label.info'/>", "");
}

function submitChangeStateRequest(formData, jqForm, options) {
	$('#loadingImg1').show();
	return true;
}

function doSubmit(msg, section)
{
    var confirmBox = $("#confirmBox");
    confirmBox.find(".message").text(msg);
    confirmBox.dialog
    ({
      autoOpen: true,
      modal: true,
      buttons:
        {
          'OK': function () {
            $(this).dialog('close');
            var ctx = "${pageContext.request.contextPath}/admin/planmgmt/manageqdpplans";
            window.location.replace(ctx);
          }
        }
    });
}

$('.complete').each(function(){
	var completeStep = $(this).html();
	var replaceExpr = /[0-9]*\./gi;
	$(this).html(completeStep.replace(replaceExpr,'<i class="icon icon-ok"></i>'));
})
</script>
