package com.getinsured.hix.batch.bulkusers.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.identity.provision.CreateUserRequest;
import com.getinsured.identity.provision.UserRequest;

public class BulkUsersProcessorListener<T, S> implements ItemProcessListener<UserRequest, CreateUserRequest>, StepExecutionListener {

	private static final Logger logger = LoggerFactory.getLogger(BulkUsersProcessorListener.class);
	private StepExecution stepExecution;
	
	@Override
	public void beforeProcess(UserRequest item) {
	}

	@Override
	public void afterProcess(UserRequest item, CreateUserRequest result) {
	}

	@Override
	public void onProcessError(UserRequest user, Exception e) {
        String exceptionAsString = CommonUtil.convertExceptionToString(e);
        String jobProcessId = CommonUtil.getJobProcessId(stepExecution.getJobExecution());
		logger.error("onProcessError() jobProcessId : "+jobProcessId+" -> ["+user+"] ExceptionDetails : "+exceptionAsString);
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		return null;
	}

}
