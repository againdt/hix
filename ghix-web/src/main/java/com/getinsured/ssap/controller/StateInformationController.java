package com.getinsured.ssap.controller;

import java.io.IOException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.UIConfiguration;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.ssap.models.AmericanAlaskaTribe;
import com.getinsured.hix.ssap.util.SsapUtility;
import com.getinsured.ssap.model.tribe.TribeResponse;
import com.getinsured.ssap.service.SingleStreamlinedApplicationService;
import com.getinsured.ssap.service.TaxFilingRequirementService;
import com.getinsured.ssap.util.JsonUtil;

/**
 * Provides access to various state configurations, such
 * as
 * <ul>
 *  <li>Relationship mappings</li>
 *  <li>IRS tax filing thresholds</li>
 *  <li>Financial flow settings</li>
 *  <li>County information for state, zip, etc.</li>
 *  <li>Tribe information for states</li>
 *  <li>Localized message resources</li>
 * </ul>
 *
 * @author Yevgen Golubenko
 * @since 2019-06-10
 */
@RestController
public class StateInformationController {
  private static final Logger log = LoggerFactory.getLogger(StateInformationController.class);

  private final SingleStreamlinedApplicationService singleStreamlinedApplicationService;
  private final TaxFilingRequirementService taxFilingRequirementService;

  @Autowired
  public StateInformationController(final SingleStreamlinedApplicationService singleStreamlinedApplicationService,
                                    final TaxFilingRequirementService taxFilingRequirementService) {
    this.singleStreamlinedApplicationService = singleStreamlinedApplicationService;
    this.taxFilingRequirementService = taxFilingRequirementService;
  }

  @RequestMapping(value = "/newssap/configuration")
  public ResponseEntity<Map<String, Object>> isFinancialFlowEnabled() {
    final String value = DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.SSAP_FINANCIAL_ENABLED);
    final Map<String, Object> ret = new HashMap<>(1);
    ret.put("financialFlow", "Y".equals(value));

    return ResponseEntity.ok(ret);
  }

  @RequestMapping(value = "/newssap/taxFilingThresholds", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getTaxFilingThresholds(@RequestParam(value = "year", required = false, defaultValue = "0") int year) {
    if(year > 0) {
      return ResponseEntity.ok(taxFilingRequirementService.getTaxFilingRequirementsByYear(year));
    }

    return ResponseEntity.ok(taxFilingRequirementService.getTaxFilingRequirements());
  }

  /**
   * UI needs to ask users to select relationships between 2+ people in household,
   * this relationship list may differ from state to state, so request is made to this endpoint
   * in order to fetch correct list for current state exchange.
   *
   * @return JSON list of relationship needed for the UI.
   */
  @RequestMapping(value = "/newssap/relationships", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getRelationships() {
    final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
    log.debug("Fetching blood relationships for state: {}", stateCode);

    final String fileName = String.format("/configuration/%s/relationships.json", stateCode.toLowerCase());
    final ClassPathResource resource = new ClassPathResource(fileName);

    if (!resource.exists() || !resource.isReadable()) {
      log.warn("Relationship list for given state {} not found/can't be read: {}", stateCode, fileName);
      return ResponseEntity.notFound().build();
    }

    return getFileResponse(resource);
  }

  /**
   * Returns common relationships list.
   * <p>
   * Account Transfers (AT) use relationship codes that are not part of our SSAP,
   * we have these codes and their corresponding labels defined in common/relationships.json
   * file.
   * </p>
   *
   * @return {@link ResponseEntity} common relationships in json format.
   */
  @RequestMapping(value = "/newssap/commonRelationships", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getCommonRelationships() {
    final String fileName = "/configuration/common/relationships.json";
    final ClassPathResource resource = new ClassPathResource(fileName);

    if (!resource.exists() || !resource.isReadable()) {
      log.warn("Relationship (common) list not found/can't be read: {}", fileName);
      return ResponseEntity.notFound().build();
    }

    return getFileResponse(resource);
  }

  /**
   * Reads file and returns {@link ResponseEntity} from it.
   *
   * @param resource {@link ClassPathResource} classpath resource.
   * @return {@link ResponseEntity}
   */
  private ResponseEntity<?> getFileResponse(final ClassPathResource resource) {
    final String json = JsonUtil.readFile(resource);

    if (json == null) {
      log.error("Error occurred while reading file: {}, returning NO CONTENT response.", resource.getPath());
      return ResponseEntity.noContent().build();
    }

    return ResponseEntity.ok(json);
  }

  /**
   * Returns list of states that have Federally Recognized Alaska Native/Indian Tribes
   * in them.
   *
   * @return List of states that have Alaska Native/Indian Tribes.
   */
  @RequestMapping(value = "/newssap/stateswithtribes")
  public ResponseEntity<List<TribeResponse>> getAllStatesWithTribes() {
    final Map<String, List<AmericanAlaskaTribe>> stateTribes = SsapUtility.getAlaskaNativeAndTribeList();
    final StateHelper stateHelper = new StateHelper();
    final List<TribeResponse> statesAndTribes = new ArrayList<>();

    stateTribes.forEach((state, tribes) -> {
      final TribeResponse tr = new TribeResponse();
      tr.setCode(state);
      tr.setName(stateHelper.getStateName(state));
      tribes.sort(Comparator.comparing(AmericanAlaskaTribe::getTribeName));
      tr.setTribes(tribes);
      statesAndTribes.add(tr);
    });

    statesAndTribes.sort(Comparator.comparing(TribeResponse::getName));
    return ResponseEntity.ok(statesAndTribes);
  }

  @RequestMapping(value = "/newssap/tribesforstate/{stateCode}")
  public ResponseEntity<?> getTribesForState(@PathVariable String stateCode) {
    List<AmericanAlaskaTribe> tribes = SsapUtility.getAmericanAlaskaStateTribeName(stateCode);

    if (tribes != null) {
      return ResponseEntity.ok(tribes);
    } else {
      return ResponseEntity.ok(new ArrayList<>());
    }
  }

  @RequestMapping(value = "/newssap/tribesforstates/{stateCodesWithComma}")
  public ResponseEntity<?> getTribesForStates(@PathVariable String stateCodesWithComma) {
    Map<String, List<AmericanAlaskaTribe>> tribes = new HashMap<>();

    String[] stateCodes = stateCodesWithComma.split(",");

    for (String s : stateCodes) {
      List<AmericanAlaskaTribe> tribe = SsapUtility.getAmericanAlaskaStateTribeName(s);
      if (tribe == null) {
        tribe = new ArrayList<>();
      }

      tribes.put(s, tribe);
    }

    return ResponseEntity.ok(tribes);
  }

  @RequestMapping(value = "/newssap/messages/{locale}")
  public ResponseEntity<?> readProps(@PathVariable String locale) {
    String fileName = String.format("i18n/ssap_%s.properties", locale);
    ClassPathResource resource = new ClassPathResource(fileName);

    // If given resource with locale doesn't exist, try base then with 'en' locale.
    if (!resource.exists()) {
      log.warn("Resource doesn't exist for given locale: i18n/ssap_{}.properties", locale);
      fileName = "i18n/ssap.properties";
      resource = new ClassPathResource(fileName);

      if (!resource.exists()) {
        log.warn("Resource doesn't exist: i18n/ssap.properties, falling back to en");
        fileName = "i18n/ssap_en.properties";
        resource = new ClassPathResource(fileName);
      }
    }

    if (resource.exists()) {
      final Properties properties = new Properties();

      try {
        properties.load(resource.getInputStream());
        Map<Object, Object> map = new HashMap<>();

        for (Object k : properties.keySet()) {
          if (!String.valueOf(k).startsWith("/")) {
            map.put(k, properties.getProperty(String.valueOf(k)));
          }
        }

        return ResponseEntity.ok(map);
      } catch (IOException e) {
        log.error("IOException while reading {}", fileName);
        log.error("Exception occurred while reading file", e);
      }
    }

    return ResponseEntity.notFound().build();
  }

  @RequestMapping(value = "/newssap/countiesforstate/{stateCode}/{zipCode}")
  public ResponseEntity<List<Map<String, String>>> getCountriesForStateAndZipCode(@PathVariable String stateCode, @PathVariable String zipCode) {
    log.debug("Creating county list for given state: {} and zip code: {}", stateCode, zipCode);
    return ResponseEntity.ok(singleStreamlinedApplicationService.populateCountiesForStateAndZipCode(stateCode, zipCode));
  }

  @RequestMapping(value = "/newssap/countiesforstate/{stateCode}")
  public ResponseEntity<List<Map<String, String>>> getCountiesForState(@PathVariable String stateCode) {
    log.debug("Creating county list for given state: {}", stateCode);
    return ResponseEntity.ok(singleStreamlinedApplicationService.populateCountiesForState(stateCode));
  }

  @RequestMapping(value = "/newssap/countiesforstates/{stateCodes}")
  public ResponseEntity<?> getCountiesForStates(@PathVariable String stateCodes) {
    log.debug("Creating county list for given states: {}", stateCodes);
    Map<String, List<Map<String, String>>> stateCounties = new HashMap<>();

    for (String s : stateCodes.split(",")) {
      stateCounties.put(s, singleStreamlinedApplicationService.populateCountiesForState(s));
    }

    return ResponseEntity.ok(stateCounties);
  }
}
