package com.getinsured.hix.cap.consumer;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.security.CustomPermissionEvaluator;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@Api(value = "CapMemberController operations", basePath = "", description = "CAP portal for CA state. Direct URL for accessing enrollments,applications and info based on houseHoldCaseId.")
@RequestMapping(value="cap/member")
public class CapMemberController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CapMemberController.class);
	
	private static final String HOUSEHOLD_ID = "householdId";
	private static final String HOUSEHOLD = "household";
	@Autowired private CustomPermissionEvaluator perEvaluator;
	
	@Autowired
	GhixRestTemplate ghixRestTemplate;
	
	@Autowired private ConsumerPortalUtil consumerPortalUtil;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@ApiOperation(value = "View Enrollment", notes = "This method is the landing page for enrollment details")
	@RequestMapping(value = "/info/{encHouseHoldCaseId}", method = {RequestMethod.GET})
	@PreAuthorize("hasPermission(#model, 'CRM_VIEW_MEMBER')")
	public String viewMember(Model model,
			HttpServletRequest request, @PathVariable("encHouseHoldCaseId") String encHouseHoldCaseId) {
		LOGGER.info("View Enrollment -- start ");
		Map<String, Boolean> overridePerms = perEvaluator.getUserPermissions(ConsumerPortalUtil.OVERRIDE_PERMS);
		try {
	        
	        String householdIdStr = (StringUtils.isNumeric(encHouseHoldCaseId)) ? encHouseHoldCaseId: ghixJasyptEncrytorUtil.decryptStringByJasypt(encHouseHoldCaseId);
	        Integer householdId = null;
	        Household household = null;
	        try {
	        	householdId = Integer.parseInt(householdIdStr);
	        	model.addAttribute(HOUSEHOLD_ID, householdId);
	        	household = consumerPortalUtil.getHouseholdRecord(householdId);	        	
	        }catch(NumberFormatException ne) {
	        	LOGGER.error("Error in viewEnrollment() : "+ne.getMessage()+ne.getCause());
	        }
	        if(null == household) {
	        	model.addAttribute("householdExists",false);
	        	model.addAttribute(HOUSEHOLD_ID,householdIdStr);
	        }
	        model.addAttribute(HOUSEHOLD,household);
	        model.addAttribute("isResendAllowed", overridePerms.get(ConsumerPortalUtil.CAP_ALLOW_RESEND834));
	        model.addAttribute("isEnrollmentEditAllowed", overridePerms.get(ConsumerPortalUtil.ENROLLMENT_DATA_EDIT));
	        model.addAttribute("isEnrollmentReadOnlyAllowed", overridePerms.get(ConsumerPortalUtil.ENROLLMENT_DATA_READ_ONLY));
	        
	        
		} catch (Exception ex) {
		//	model.addAttribute(ERROR_MESSAGE, APPLICATION_ERROR);
			LOGGER.error("Error in viewEnrollment() : "+ex.getMessage()+ex.getCause());
		}
		LOGGER.info("View Enrollment -- End ");
		return "crm/member/enrollment";
	}
	
}
