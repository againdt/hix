package com.getinsured.ssap.service;

import com.getinsured.ssap.model.hub.vlp.VlpRequest;
import com.getinsured.ssap.model.hub.vlp.response.CaseStatusResponse;
import com.getinsured.ssap.model.hub.vlp.response.VlpResponse;

/**
 * Interface for VLP (verify lawful presence) integration.
 * <p>
 *   This contains various methods to invoke and process calls to
 *   Government Hub Integration (ghix-hub) that is residing on AWS Government Cloud.
 * </p>
 * @author Yevgen Golubenko
 * @since 2/7/19
 */
public interface VlpIntegration {

  /**
   * Invokes initial lawful presence verification.
   *
   * @param vlpRequest {@link VlpRequest} object.
   * @return returns {@link VlpResponse} object.
   */
  VlpResponse verify(VlpRequest vlpRequest);

  /**
   * Invokes re-verification process, after initial verification.
   * @param vlpRequest {@link VlpRequest} object.
   * @return returns {@link VlpResponse} object.
   */
  VlpResponse reverify(VlpRequest vlpRequest);

  /**
   * Invokes re-verification with SAVIS id.
   * @param vlpRequest {@link VlpRequest} object.
   * @return returns {@link VlpResponse} object.
   */
  VlpResponse resubmitWithSevisId(VlpRequest vlpRequest);

  /**
   * Invokes second verification.
   * @param vlpRequest {@link VlpRequest} object.
   * @return returns {@link VlpResponse} object.
   */
  VlpResponse secondVerification(VlpRequest vlpRequest);

  /**
   * Invokes third verification.
   * @param vlpRequest {@link VlpRequest} object.
   * @return returns {@link VlpResponse} object.
   */
  VlpResponse thirdVerification(VlpRequest vlpRequest);

  /**
   * Checks case status.
   *
   * @param caseNumber case number.
   * @return {@link CaseStatusResponse}.
   */
  CaseStatusResponse caseStatus(final String caseNumber);

  /**
   * Invokes close case endpoint.
   * @param vlpRequest vlp request object.
   * @return {@link VlpResponse}
   */
  VlpResponse closeCase(final VlpRequest vlpRequest);

  /**
   * Retrieves step two case resolution.
   * @param vlpRequest {@link VlpRequest} object.
   * @return returns {@link VlpResponse} object.
   */
  VlpResponse getStepTwoCaseResolution(final VlpRequest vlpRequest);

  /**
   * Retrieves step three case resolution.
   * @param vlpRequest {@link VlpRequest} object.
   * @return returns {@link VlpResponse} object.
   */
  VlpResponse getStepThreeCaseResolution(final VlpRequest vlpRequest);
}
