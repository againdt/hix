package com.getinsured.eligibility;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.ssap.integration.notices.AgedOutDisenrollmentNoticeService;
import com.getinsured.eligibility.ssap.integration.notices.QEPNonFinancialNotificationService;
import com.getinsured.eligibility.ssap.integration.notices.SsapIntegrationNotificationService;

/**
 *
 * @author raguram_p
 *
 */

@Controller
@RequestMapping("/ssapintegration")
public class SsapIntegrationNoticeController {

	private static final Logger log = LoggerFactory.getLogger(SsapIntegrationNoticeController.class);

	@Autowired
	private SsapIntegrationNotificationService ssapIntegrationNotificationService;

	@Autowired
	private QEPNonFinancialNotificationService qepNonFinancialNotificationService;
	
	@Autowired
	private AgedOutDisenrollmentNoticeService agedOutDisenrollmentNoticeService;

	@RequestMapping(value = "/notice/addInfoDoc/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> generateAdditionalDocumentNoticeForNonFinApp(@PathVariable("caseNumber") String caseNumber) {
		String result="";
		try {
			 String ecmId=ssapIntegrationNotificationService.generateAdditionalInformationDocument(caseNumber);
			 result="Additional Information for SSAP required notification generated for the case number " +caseNumber +" at " +ecmId;
		} catch (Exception e) {
			log.error("Unknown error while updating ssap application data", e);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * Called by the React SSAP in case if VLP verification requires more information from the user.
	 * @param caseNumber SSAP case number.
	 * @return {@link ResponseEntity} with {@code code} field that indicates success (200) or failure (non 200).
	 */
	@RequestMapping(value = "/notice/vlp/additionalinforequired/{caseNumber}", method = RequestMethod.GET)
	public ResponseEntity<?> sendAdditionalInformationRequiredForSsap(@PathVariable("caseNumber") String caseNumber) {
		final Map<String, Object> response = new HashMap<>();

		try {
			final String ecmId = ssapIntegrationNotificationService.generateAdditionalInformationDocument(caseNumber);
			response.put("code", HttpStatus.OK.value());
			response.put("message", "notice generated successfully for case number " + caseNumber);
			response.put("ecmId", ecmId);
		} catch (Exception e) {
			log.error("Exception occurred while generating notification for 'additional info required', case number: {}, error: {}", caseNumber, e.getMessage());
			log.error("Exception details", e);
			response.put("code", HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.put("message", e.getMessage());
			response.put("ecmId", null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

		log.info("Generated notice for case number: {}", caseNumber);
		return ResponseEntity.ok(response);
	}

	@RequestMapping(value = "/notice/qepgrant/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> generateQEPGrantNoticeForNonFinancial(@PathVariable("caseNumber") String caseNumber) {
		String result="";
		try {
			 String ecmId=qepNonFinancialNotificationService.generateQEPGrantNoticeForNonFinancial(caseNumber);
			 result="QEP Grant Non Financial Application notification generated for the case number " +caseNumber +" at " +ecmId;
		} catch (Exception e) {
			log.error("Unknown error while generating QEP Grant Non Financial Application notification - ", e);
		}
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/notice/ageoutnotice/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> generateAgeoutNotice(@PathVariable("caseNumber") String caseNumber) {
		String result="";
		try {
			 String ecmId=agedOutDisenrollmentNoticeService.processAgeOutDependents(caseNumber);
			 result="Age out Non Financial Application notification generated for the case number " +caseNumber +" at " +ecmId;
		} catch (Exception e) {
			log.error("Unknown error while generating Age Out Grant Non Financial Application notification - ", e);
		}
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}
}
