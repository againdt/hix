package com.getinsured.hix.errors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.security.service.jpa.UserServiceImpl;

@Controller
public class ErrorController {
	private Logger logger = LoggerFactory.getLogger(ErrorController.class);
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/error/pageExpired",  method = RequestMethod.GET)
	public String handlePageEXpiredException(HttpServletRequest request, HttpServletResponse response) {
		String landingPage = "/hix/account/user/login";
		try {
			AccountUser user = userService.getLoggedInUser();
			
			if(user != null) {
				if(logger.isInfoEnabled()) {
					logger.info("Expired page : {}, Invoked by {}",request.getRequestURI(), user.getUserName());
				}
			
				Role defaultRole  = userService.getDefaultRole(user);
				
				if(defaultRole != null) {
					landingPage = defaultRole.getLandingPage();
					if(!landingPage.startsWith("/hix")) {
						landingPage = "/hix" + landingPage;
					}
				}
			}
		} catch (InvalidUserException e) {
			logger.error("Error finding the logged in user",e);
		}
		
		if(logger.isDebugEnabled()) {
			logger.debug("landingPageUrl: {}", landingPage );
		}
		
		request.setAttribute("landingPageUrl", landingPage);

		return "error/pageExpired";
	}
}
