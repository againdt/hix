import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { HashRouter as Router, Route } from 'react-router-dom';
import ConnectedIntlProvider from '../utils/ConnectedIntlProvider';

import { setMessages } from '../actions/actionCreators';
import BrokerParticipation from '../pages/BrokerParticipation';
import BrokerAvailability from '../pages/BrokerAvailability';
import Spinner from '../components/spinner/Spinner';
import Alert from '../components/alert/Alert';

import '../../node_modules/@cmsgov/design-system-core/dist/index.css';
import '../../node_modules/@cmsgov/design-system-layout/dist/index.css';

import '../App.css';

// Source for internationalized messages. Until the API is ready
import internationalizedMessages from '../i18n/locales/data.json';

const App = props => {
  const { spinner, alert } = props;
  useEffect(() => {
    props.actions.setMessages(internationalizedMessages);
  }, []);

  return (
    <Router>
      <ConnectedIntlProvider>
        <div className="App">
          <div className="ds-l-container">
            {alert &&
            alert.status &&
            alert.alertTitle &&
            alert.alertMessage &&
            alert.errorType ? (
              <Alert
                className="ds-l-row ds-u-radius ds-u-margin-bottom--1"
                heading={alert.alertTitle}
                hideIcon={false}
                variation={alert.errorType}
                role="alert"
              >
                {alert.alertMessage}
              </Alert>
            ) : (
              ''
            )}

            <Route path="/participation" component={BrokerParticipation} />
            <Route path="/availability" component={BrokerAvailability} />
            {spinner.isSpinnerActive ? (
              <Spinner role="progressbar" size="large" ariaText="Loading" />
            ) : (
              ''
            )}
          </div>
        </div>
      </ConnectedIntlProvider>
    </Router>
  );
};

const mapStateToProps = state => ({
  data: state.data,
  spinner: state.spinner,
  alert: state.alert
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      setMessages
    },
    dispatch
  )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
