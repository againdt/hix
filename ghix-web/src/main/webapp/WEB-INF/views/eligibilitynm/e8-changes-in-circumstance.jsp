
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Changes in Circumstance</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">


<div class="row-fluid" id="main">
    <div class="row-fluid" id="titlebar">
      <div class="span3">
        <a class="btn btn-link" href="e1-1-start-app">Back </a>
      </div>
      <div class="span9 pull-left">
        <h1>Your Account Dashboard</h1>
      </div>
    </div>
    
    <!--titlebar-->
    <div class="row-fluid">
      <div class="span3 gutter10" id="sidebar">
      <div class="gutter10">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li><a href="e7-view-account">Overview <i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="e7-enrollment-history">Enrollment History <i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="#">Household Profile<i class="icon-chevron-right pull-right"></i></a></li>
          <li class="active"><a href="#">Change in Circumstance <i class="icon-chevron-right"></i></a></li>
          <li><a href="#">Inbox <i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="#">Account Settings <i class="icon-chevron-right pull-right"></i></a></li>
        </ul>
        </div><!-- gutter10 -->
      </div>
      <!--sidebar-->
      
      <div class="span9" id="rightpanel">
        <div class="gutter10">
          <div class="changes">
            <h3>Changes in Circumstance</h3>
            <p>Changes in your coverage, household or your income may affect your benefits or trigger a special enrollment period. It is important to update your account within 30 days of when the change occurs.</p>
            <p><a href="#">Learn how changes affect your coverage</a></p>
            <hr>
            <div class="span5">
              <label class="radio">
                <input type="radio" class="moving" name="#" value="Moving">
                Moving </label>
              <label class="radio">
                <input type="radio" class="adoption" name="#" value="Adoption">
                Adoption </label>
              <label class="radio">
                <input type="radio" class="married" name="#" value="Getting married">
                Getting married</label>
            </div>
            <div class="span5">
              <label class="radio">
                <input type="radio" class="immigration" name="#" value="Eligible immigration status">
                Eligible immigration status</label>
              <label class="radio">
                <input type="radio" class="loss" name="#" value="Loss of coverage">
                <!--name="loss"-->
                Loss of coverage </label>
              <a href="#">See more changes in circumstance</a> </div>
            <div class="well clear">
              <button class="pull-right btn">Report changes</button>
            </div>
          </div>
          <!-- changes -->

          <!-- MOVING -->
          <div id="moving-part1">
            <h3>Moving</h3>
            <p>Did any of the following people move in the past 30 days?</p>
            <hr>
            <label class="checkbox">
              <input type="checkbox" value="#">
              Stan Jacobs </label>
          
            <label class="checkbox">
              <input type="checkbox" value="#">
              Janet Jacobs </label>
            <label class="checkbox">
              <input type="checkbox" value="#">
              Joe Jacobs </label>
            <div class="well clear">
              <button class="pull-right btn next-moving-1">Next</button>
            </div>
          </div>
          <div id="moving-part2">
            <p>Provide the following information about this person's new address:</p>
            <hr>
            <div class="span6">
              <label class="control-label" for="inputStreet">Street address</label>
              <div class="controls">
                <input type="text" name="inputStreet" class="span12" placeholder="1234 Main St.">
              </div>
            </div>
            <div class="span3">
              <label class="control-label" for="inputApt">Apt.</label>
              <div class="controls">
                <input type="text" name="inputApt" class="span9" placeholder="">
              </div>
            </div>
            <div class="span3">
              <label class="control-label" for="inputCity">City</label>
              <div class="controls">
                <input type="text" name="inputCity"  class="span12" placeholder="Santa Fe">
              </div>
            </div>
            <div class="span3">
              <label class="control-label" for="inputState">State</label>
              <div class="controls">
                <select name="inputState" class="span12" id="inputState">
                  <option value="">Select a State</option>
                  <option value="AL">Alabama</option>
                  <option value="AK">Alaska</option>
                  <option value="AZ">Arizona</option>
                  <option value="AR">Arkansas</option>
                  <option value="CA">California</option>
                  <option value="CO">Colorado</option>
                  <option value="CT">Connecticut</option>
                  <option value="DE">Delaware</option>
                  <option value="DC">District Of Columbia</option>
                  <option value="FL">Florida</option>
                  <option value="GA">Georgia</option>
                  <option value="HI">Hawaii</option>
                  <option value="ID">Idaho</option>
                  <option value="IL">Illinois</option>
                  <option value="IN">Indiana</option>
                  <option value="IA">Iowa</option>
                  <option value="KS">Kansas</option>
                  <option value="KY">Kentucky</option>
                  <option value="LA">Louisiana</option>
                  <option value="ME">Maine</option>
                  <option value="MD">Maryland</option>
                  <option value="MA">Massachusetts</option>
                  <option value="MI">Michigan</option>
                  <option value="MN">Minnesota</option>
                  <option value="MS">Mississippi</option>
                  <option value="MO">Missouri</option>
                  <option value="MT">Montana</option>
                  <option value="NE">Nebraska</option>
                  <option value="NV">Nevada</option>
                  <option value="NH">New Hampshire</option>
                  <option value="NJ">New Jersey</option>
                  <option value="NM" selected>New Mexico</option>
                  <option value="NY">New York</option>
                  <option value="NC">North Carolina</option>
                  <option value="ND">North Dakota</option>
                  <option value="OH">Ohio</option>
                  <option value="OK">Oklahoma</option>
                  <option value="OR">Oregon</option>
                  <option value="PA">Pennsylvania</option>
                  <option value="RI">Rhode Island</option>
                  <option value="SC">South Carolina</option>
                  <option value="SD">South Dakota</option>
                  <option value="TN">Tennessee</option>
                  <option value="TX">Texas</option>
                  <option value="UT">Utah</option>
                  <option value="VT">Vermont</option>
                  <option value="VA">Virginia</option>
                  <option value="WA">Washington</option>
                  <option value="WV">West Virginia</option>
                  <option value="WI">Wisconsin</option>
                  <option value="WY">Wyoming</option>
                </select>
              </div>
            </div>
            <div class="span3">
              <label class="control-label" for="inputZip">Zip</label>
              <div class="controls">
                <input type="text" name="inputZip" class="span9" placeholder="87502">
              </div>
            </div>
            <div class="well clear">
              <button class="pull-right btn next-moving-2">Next</button>
            </div>
          </div>
          <div id="moving-part3">
            <p>When was the date of the move?</p>
            <hr>
            <div class="span5">
              <div class="controls">
                <div class="input-append date" data-date="12-28-2012" data-date-format="mm-dd-yyyy">
                  <input class="span8" type="text" value="12-28-2012">
                  <span class="add-on"><i class="icon-calendar"></i></span></div>
              </div>
            </div>
            <div class="well clear"> <a href="#">
              <button class="pull-right btn PageRefresh">Report to Exchange</button>
              </a> </div>
          </div>
          <!-- MOVING ENDS -->

          <!-- ADOPTION -->
          <div id="adoption-part1">
            <h3>Adoption</h3>
            <p>Have any of the following people been adopted or placed for adoption in the last 30 days?</p>
            <hr>
            <label class="checkbox">
              <input type="checkbox" value="#">
              Joe Jacobs </label>
            <div class="well clear">
              <button class="pull-right btn next-adoption-1">Next</button>
            </div>
          </div>
          <div id="adoption-part2">
            <p>When was this person adopted or placed for adoption?</p>
            <hr>
            <div class="span5">
              <div class="controls">
                <div class="input-append date" data-date="12-28-2012" data-date-format="mm-dd-yyyy">
                  <input class="span8" type="text" value="12-28-2012">
                  <span class="add-on"><i class="icon-calendar"></i></span></div>
              </div>
            </div>
            <div class="well clear"> <a href="#">
              <button class="pull-right btn next-adoption-2 PageRefresh">Report to Exchange</button>
              </a> </div>
          </div>
          <!-- ADOPTION ENDS -->

          <!-- GETTIING MARRIED -->
          <div id="gettingMarried-part1">
            <h3>Getting Married</h3>
            <p>Did any of these people get married in the past 30 days?</p>
            <hr>
            <label class="checkbox">
              <input type="checkbox" value="#">
              Joe Jacobs </label>
            <div class="well clear">
              <button class="pull-right btn next-married-1">Next</button>
            </div>
          </div>
          <div id="gettingMarried-part2">
            <p>When did this person get married?</p>
            <hr>
            <div class="span5">
              <div class="controls">
                <div class="input-append date" data-date="12-28-2012" data-date-format="mm-dd-yyyy">
                  <input class="span8" type="text" value="12-28-2012">
                  <span class="add-on"><i class="icon-calendar"></i></span></div>
              </div>
            </div>
            <div class="well clear"> <a href="#">
              <button class="pull-right btn next-married-2 PageRefresh">Report to Exchange</button>
              </a> </div>
          </div>
          <!-- GETTIING MARRIED ENDS -->

          <!-- IMMIGRATION -->
          <div id="immigration-part1">
            <h3>Eligible immigration status</h3>
            <p>Did Janet Jacobs gain eligible immigration status in the past 30 days?</p>
            <hr>
            <label class="radio inline">
              <input type="radio" name="#" value="Yes">
              Yes </label>
            <label class="radio inline">
              <input type="radio" name="#" value="No">
              No </label>
            <div class="well clear">
              <button class="pull-right btn next-immigration-1">Next</button>
            </div>
          </div>
          <div id="immigration-part2">
            <p>When did Janet Jacobs gain eligible immigration status?</p>
            <hr>
            <div class="span5">
              <div class="controls">
                <div class="input-append date" data-date="12-28-2012" data-date-format="mm-dd-yyyy">
                  <input class="span8" type="text" value="12-28-2012">
                  <span class="add-on"><i class="icon-calendar"></i></span></div>
              </div>
            </div>
            <div class="well clear"> <a href="#">
              <button class="pull-right btn PageRefresh">Report to Exchange</button>
              </a> </div>
          </div>
          <!-- IMMIGRATION ENDS -->

          <!-- LOSS OF COVERAGE -->
          <div id="lossOfCoverage-part1">
            <h3>Loss of Coverage</h3>
            <p>Did any of the following people health insurance in the past 30 days?</p>
            <p><small>Check all that apply</small></p>
            <hr>
            <label class="checkbox">
              <input type="checkbox" value="#">
              Stan Jacobs </label>
            <label class="checkbox">
              <input type="checkbox" value="#">
              Janet Jacobs </label>
            <label class="checkbox">
              <input type="checkbox" value="#">
              Joe Jacobs </label>
            <div class="well clear">
              <button class="pull-right btn next1">Next</button>
            </div>
          </div>
          <div id="lossOfCoverage-part2">
            <p>Was termination of the coverage due to failure to pay premiums?</p>
            <hr>
            <label class="radio inline">
              <input type="radio" name="#" value="Yes">
              Yes </label>
            <label class="radio inline">
              <input type="radio" name="#" value="No">
              No </label>
            <div class="well clear">
              <button class="pull-right btn next2">Next</button>
            </div>
          </div>
          <div id="lossOfCoverage-part3">
            <p>What kind of healthcare coverage was lost?</p>
            <hr>
            <div class="span5">
              <label class="checkbox">
                <input type="checkbox" value="#">
                Medicaid </label>
              <label class="checkbox">
                <input type="checkbox" value="#">
                Medicare </label>
              <label class="checkbox">
                <input type="checkbox" value="#">
                CHIP </label>
              <label class="checkbox">
                <input type="checkbox" value="#">
                TRICARE </label>
            </div>
            <div class="span5">
              <label class="checkbox">
                <input type="checkbox" value="#">
                Employer Sponsored Insurance </label>
              <label class="checkbox">
                <input type="checkbox" value="#">
                VA </label>
              <label class="checkbox">
                <input type="checkbox" value="#">
                Peace Corps </label>
              <label class="checkbox">
                <input type="checkbox" value="#">
                Coverage from the Individual Market </label>
            </div>
            <div class="well clear"> <a href="#">
              <button class="pull-right btn next3 PageRefresh">Report to Exchange</button>
              </a> </div>
          </div>
          <!-- LOSS OF COVERAGE ENDS -->
          <!--gutter10-->
        </div>
        <!--rightpanel-->
      </div>
      <!--row-fluid-->
  </div>
  <!--main-->
</div>

<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/jquery-1.7.2.min.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/jquery-ui-1.8.22.custom.min.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/jquery.multiselect.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-modal.js"></script> -->
<script>
    $(document).ready(function () {
  $('#rightpanel .gutter10 > :not(.changes)').hide();
        $('#qualify-coverage').modal('hide');
        $('#sidebar').affix();

    //moving
        $("input[class='moving']").change(function () {
            $('#moving-part1').show(this.value == "Loss of coverage");
        $('#rightpanel .gutter10 > :not(#moving-part1, .changes,.next-moving-1,#moving-part2, #moving-part3)').hide();
        });
        $('.next-moving-1').click(function () {
            $('#moving-part2').fadeToggle();
        });
        $('.next-moving-2').click(function () {
            $('#moving-part3').fadeToggle();
        });
    //adoption
        $("input[class='adoption']").change(function () {
            $('#adoption-part1').fadeToggle(this.value == "Loss of coverage");
      $('#rightpanel .gutter10 > :not(#adoption-part1, .changes,.next-adoption-1,#adoption-part2)').hide();
        });
        $('.next-adoption-1').click(function () {
            $('#adoption-part2').fadeToggle();
        });
    //married
        $('#gettingMarried-part1, #gettingMarried-part2').hide();
        $("input[class='married']").change(function () {
            $('#gettingMarried-part1').fadeToggle(this.value == "Loss of coverage");
      $('#rightpanel .gutter10 > :not(#gettingMarried-part1, .changes,#gettingMarried-part2,.next-married-1)').hide();
        });
        $('.next-married-1').click(function () {
            $('#gettingMarried-part2').fadeToggle();
        });
    //loss of coverage
        $("input[class='loss']").change(function () {
            $('#lossOfCoverage-part1').fadeToggle(this.value == "Loss of coverage");
        $('#rightpanel .gutter10 > :not(#lossOfCoverage-part1, .changes,.next1,#lossOfCoverage-part2,.next2,#lossOfCoverage-part3)').hide();

        });
        $('.next1').click(function () {
            $('#lossOfCoverage-part2').fadeToggle();
        });
        $('.next2').click(function () {
            $('#lossOfCoverage-part3').fadeToggle();
        });
    //immigration
        $("input[class='immigration']").change(function () {
            $('#immigration-part1').fadeToggle(this.value == "Loss of coverage");
        $('#rightpanel .gutter10 > :not(#immigration-part1, .changes,#immigration-part2)').hide();

        });
        $('.next-immigration-1').click(function () {
            $('#immigration-part2').fadeToggle();
        });
        $('.PageRefresh').click(function () {
            location.reload();
        });

        $('.date').datepicker();
    });
</script>

