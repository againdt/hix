		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
		<%@ taglib uri="/WEB-INF/tld/comments-view" prefix="comment" %>
		<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
		<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
		<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />

					<c:set var="assisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
					<c:set var="assisterEntityId" ><encryptor:enc value="${assister.entity.id}"  isurl="true"/> </c:set>
					<div class="row-fluid">
						<h1><a name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
					</div>
					<div class="row-fluid">
						<div id="sidebar" class="span3">
							<div class="header"></div>
							<ul class="nav nav-list">
								<li><a href="<c:url value="/entity/entityadmin/assisterinformation?assisterId=${assisterId}&entityId=${assisterEntityId}" />"><spring:message code="label.assister.certifiedCounselor"/></a></li>
								<li><a href="<c:url value="/entity/entityadmin/assisterprofile?assisterId=${assisterId}&entityId=${assisterEntityId}" />"><spring:message code="label.assister.profile"/></a></li>
								<c:choose>	
									<c:when test="${loggedUser=='entityAdmin' }">
										<li><a href="<c:url value="/entity/assisteradmin/certificationstatus?assisterId=${assisterId}&entityId=${assisterEntityId}" />"><spring:message code="label.assister.certificationStatus"/></a></li>
									</c:when>
									</c:choose>
								<li class="active"><a href="#"><spring:message code="label.assister.status"/> </a></li>
							</ul>
						</div><!-- end of span3 -->
						<div class="span9">
							<form class="form-horizontal" id="frmupdatecertification" enctype="multipart/form-data" name="frmupdatecertification" method="post" action="editassisterstatus">
							<df:csrfToken/>
								<input type="hidden" id="assisterId" name="assisterId" value="<encryptor:enc value="${assister.id}"/>">
								<input type="hidden" id="entityId" name="entityId" value="<encryptor:enc value="${assister.entity.id}"/>">
								<div class="row-fluid">
									<div class="header">
										<h4 class="pull-left"><spring:message code="label.assister.status"/> </h4>
									</div>
									<div class="gutter10">
										<table class="table table-border-none table-condensed table-auto">
											<tbody>
												<tr>
													<td class="span4 txt-right"><spring:message code="label.assister.status"/> </td>
													<td><strong>${assister.status }</strong></td>
												</tr>
												
											
												<tr>
													<td class="span4 txt-right"><label for="status"><spring:message code="label.assister.newStatus"/></label></td>
													<td>
														<select size="1" id="status" name="status" class="input-medium">
															<option value=""><spring:message code="label.assister.select"/></option>
															<option id="active" value="Active"><spring:message code="label.assister.active"/></option>
															<option id="inactive"value="InActive"><spring:message code="label.assister.assisterInActive"/></option>															
														</select>
														<input type="hidden" name="assisterStatus" id="assisterStatus" val=""/>
														<div id="status_error"></div>
													</td>
												</tr>
												<tr>
													<td class="span4 txt-right"><label for="activityComments"><spring:message code="label.assister.comment"/></label></td>												
													<td><textarea name="activityComments" id="activityComments" class="input-xlarge" rows="3" maxlength="1000"></textarea></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="form-actions">
									<input type="hidden" id="fileToUpload" name="fileToUpload" value="fileInput">
                                    <a class="btn" href="<c:url value="/entity/entityadmin/assisterstatus?assisterId=${assisterId}&entityId=${assisterEntityId}" />"><spring:message code="label.assister.cancel"/></a>
                                    <input type="submit" class="btn btn-primary" name="save" id="save" value="<spring:message code="label.entity.save"/>">
									<span id="processing" class="margin10-l hide"> <spring:message code="ssap.footer.processing" text="Processing please wait"/><img class="margin10-l" src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/></span>
								</div>
							</form><!-- Showing comments -->
							<div class="gutter10">     
                                  	   <display:table id="assisterActivityStatusHistory" name="assisterActivityStatusHistory" list="assisterActivityStatusHistory" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
                                             <display:column property="updatedOn" titleKey="label.entity.date"  format="{0,date,MM/dd/yyyy}" sortable="false"  />
                                             <display:column property="previousActivityStatus" titleKey="label.entity.previousStatus" sortable="false" />
                                             <display:column property="newActivityStatus" titleKey="label.NewStatus" sortable="false" />
                                                   <display:column property="activityComments" titleKey="label.entity.vieComment" sortable="false" />
                                          </display:table>                                
                           		</div><!--- end of .gutter10 -->
						</div><!-- end of span9 -->
					</div>
				<div id="modalBox" class="modal hide fade">
					<div class="modal-header">
						<a class="close" data-dismiss="modal" data-original-title=""><span aria-hidden="true">x</span><span class="aria-hidden">close</span></a>
						<h3 id="myModalLabel">
							<spring:message code="label.assister.viewComment"/>
						</h3>
					</div>
					<div id="commentDet" class="modal-body">
						<label id="commentlbl" ></label>						
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.assister.close"/></a>
					</div>
				</div>
		
		<script type="text/javascript">
		
		    var validator = $("#frmupdatecertification").validate({ 
			onkeyup: true,
			onclick: true,
			 ignore: "",
			rules : {
				status : { required : true}	
			},
			messages : {
				status: { required :"<span> <em class='excl'>!</em><spring:message code='label.validateNewStatus' javaScriptEscape='true'/></span>"}
			
			},
			errorClass: "error",
			errorPlacement: function(error, element) {
				var elementId = element.attr('id');
				error.appendTo( $("#" + elementId + "_error"));
				$("#" + elementId + "_error").attr('class','error help-inline');
			} 
		});		 
		
//if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,92,new Date().getTime(),"","","","",""]);
		</script>
	<script type="text/javascript">
	$("#save").click(function(e){
		var selectedValue = $('#status option:selected').val();		
		$("#assisterStatus").val(selectedValue);
		if($("#frmupdatecertification").valid()){
			$("#save").addClass("hide");
			$("#processing").show();
		}else{
			$("#save").removeClass("hide");
			$("#processing").hide();
		};
	});
	
	function getComment(comments)
		{	
			$('#commentlbl').html("<p> Loading Comment...</p>");
			comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
			comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
			$('#commentlbl').html("<p> "+ comments + "</p>");
			
		}
	
	</script>
