package com.getinsured.hix.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * Handle finance request and verify the
 * authorization of the user to perform the action/activity
 * @author Sahoo_s
 * @since 06-June-2014
 */

@Component
public class FinanceSecureUtil {
	
	@Autowired private UserService userService;	
	@Autowired private FinanceRestApiUtils financeRestApiUtils;
	private static final String ROLE_EMPLOYER = "EMPLOYER";
	private static final String ROLE_ISSUER = "ISSUER";
	public static final int NUMERICAL_0 				= 0;
	public static final int NUMERICAL_1 				= 1;
	
	/**
	 * Check the logged in user role
	 * 		<li>True - If the Logged in user role is Employer</li>
	 * 		<li>False - If the Logged in user role is other than Employer</li>
	 * @param AccountUser
	 * @return boolean isAdmin
	 */
	public boolean isEmployerRole(AccountUser user){
		boolean isAdmin = false;
		if (user.getDefRole().getName().equalsIgnoreCase(ROLE_EMPLOYER)) {
			isAdmin = true;
		}
		return isAdmin;
	}
	
	/**
	 * This method check the employer details & payment details data 
	 * with logged in user module id & payment detail id 
	 * @param AccountUser
	 * @param paymentMethodId
	 * @param employerId
	 * @return boolean employerPaymentFlag
	 */
	public boolean findPaymentDetailsForEmployer(AccountUser user, int paymentMethodId, int employerId){
		PaymentMethods paymentMethods = null;
		boolean employerPaymentFlag = false;
		ModuleUser moduleUser = userService.getUserDefModule(user.getActiveModuleName(), user.getId());
		
		
		if (moduleUser.getModuleName().equalsIgnoreCase(ROLE_EMPLOYER)) {
			//paymentMethods = financeRestApiUtils.findPaymentMethodsByIdModuleIdNModuleName(paymentMethodId, moduleUser.getModuleId(), PaymentMethods.ModuleName.EMPLOYER);
			paymentMethods = this.searchPaymentMethodByParameter(paymentMethodId, moduleUser.getModuleId(), PaymentMethods.ModuleName.EMPLOYER);
		}else if (moduleUser.getModuleName().equalsIgnoreCase(ROLE_ISSUER)) {
			//paymentMethods = financeRestApiUtils.findPaymentMethodsByIdModuleIdNModuleName(paymentMethodId, moduleUser.getModuleId(), PaymentMethods.ModuleName.ISSUER);
			paymentMethods = this.searchPaymentMethodByParameter(paymentMethodId, moduleUser.getModuleId(), PaymentMethods.ModuleName.ISSUER);
		}
		if (paymentMethods==null) {
			employerPaymentFlag = true;
		}
		return employerPaymentFlag;
	}
	
	/**
	 * 
	 * @param moduleID
	 * @param moduleName	 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private PaymentMethods searchPaymentMethodByParameter(Integer paymentMethodID, Integer moduleID,  PaymentMethods.ModuleName moduleName)
	{
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
		if(paymentMethodID != null)
		{
			paymentMethodRequestDTO.setId(paymentMethodID);
		}
		if(moduleID != null)
		{
			paymentMethodRequestDTO.setModuleID(moduleID);
		}
		if(moduleName != null)
		{
			paymentMethodRequestDTO.setModuleName(moduleName);
		}		
		Map<String, Object> financeResponseMap = financeRestApiUtils.searchPaymentMethod(paymentMethodRequestDTO);
		if(financeResponseMap != null && !financeResponseMap.isEmpty() && financeResponseMap.containsKey(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY))
		{
			List<PaymentMethods> paymentMethodList = (List<PaymentMethods>) financeResponseMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
			if(paymentMethodList != null && !paymentMethodList.isEmpty() && paymentMethodList.size() == NUMERICAL_1)
			{
				return paymentMethodList.get(NUMERICAL_0);
			}
		}
		return null;
	}
	/**
	 * This method check the employer details & employer invoice details data 
	 * @param employerId
	 * @param invoiceId
	 * @return boolean employerInvoiceFlag
	 */
	public boolean findInvoiceDetailsForEmployer(int employerId, int invoiceId){
		boolean employerInvoiceFlag = false;
		EmployerInvoices employerInvoices = financeRestApiUtils.findEmployerInvoicesById(invoiceId);
		if(employerInvoices==null){
			employerInvoiceFlag = true;
		}else if (employerInvoices!=null && employerInvoices.getEmployer().getId() != employerId) {
			employerInvoiceFlag = true;
		}
		return employerInvoiceFlag;
	}
	
	/**
	 * This method check the employer details & logged in user 
	 * @param AccountUser
	 * @param employerId
	 * @return boolean securityFlag
	 */
	public boolean findEmployerForLoggedinUser(AccountUser user, int employerId){
		boolean securityFlag = false;
		ModuleUser moduleUser = userService.getUserDefModule(user.getActiveModuleName(), user.getId());
		if (moduleUser.getModuleId() != employerId) {
			securityFlag = true;
		}
		return securityFlag;
	}
	
}
