<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util" prefix="util"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo" %>
<%@ taglib prefix="spnav" uri="/WEB-INF/tld/employee-spenroll-nav" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<!-- <link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" /> -->
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>
<style type="text/css">
.dialogCSS{background-color: white}
#emp-contribution.well {
	padding: 10px;
}
.dl-horizontal dt {overflow:visible;}
#plan-summary .dl-horizontal dt, #emp-contribution .dl-horizontal dt { width: 196px!important;}
#plan-summary .dl-horizontal dd, #emp-contribution .dl-horizontal dd { margin-left: 195px !important;}
/* .datepicker-dropdown {
    position: fixed;
} */


.tooltip{
	
	width:200px;
}

.tooltip-inner{
	border:1px solid #bf6d00;
}

.tooltip.left .tooltip-arrow {
top: 50%;
right: 0;
margin-top: -5px;
border-width: 5px 0 5px 5px;
border-left-color: #bf6d00;
}
label {
	line-height: 13px;
}
</style>


<div class="gutter10">
	<div class="row-fluid" id="titlebar">
  		<h1>Welcome &nbsp;<span id="user-name">${employeeApplication.name}</span></h1>
  	</div>
  	<div class="row-fluid">
	    <div class="span3" id="sidebar">
		     <util:myStuff employeeApplication="${employeeApplication}" hidesettings="true" hideinbox="true"></util:myStuff>
	    </div><!--/span3-->
    <div class="span9" id="rightpanel">
	      <div class="header">
	      	<h4>Report Change in Circumstances        
	      </div>
	      <div  class="row-fluid">
  
			  <form class="form=horizontal" id="reportChange-form" action="<c:url value='/shop/employee/spplanselection/addemployeeevent'/>" method="post">     
              <df:csrfToken/>
			  	<fmt:formatDate pattern="MM/dd/yyyy" value="${sysDate}" var="systemDate" />
			  	<input type="hidden" name="applicationId" id="applicationId" value="${employeeApplication.id}"/>        
			      <div >
			          <h3>Please provide information about qualifying event you wish to report.</h3>
			          <p>Based on the information you provide, you may be eligible for Special Enrollment in which you can add/remove family members from coverage or change your selected plan.</p>
			        <div class="control-group" id="report-change" >
			          <div class="controls" id="select-event">
			            <fieldset>
			              <legend>Event Type</legend>
			                  <div class="row-fluid height30">
			                      <input type="hidden" name="employeeQualifyingEvents[0].employeeApplicationId" value="${employeeApplication.id}"/>
			                      <input type="hidden" name="employeeQualifyingEvents[0].createdBy" value="${userid}"/>
			                  	  <input type="hidden" name="employeeQualifyingEvents[0].eventCode" id="marriage-code" value="32"/>
			                  	  <input type="hidden" name="employeeQualifyingEvents[0].spEventCode" value="32"/>
			                      <label class="radio inline" for="marriage">
			                        <input type="radio" name="employeeQualifyingEvents[0].eventType" id="marriage" value="Marriage" class="eventClass">
			                            Marriage
			                      </label>
			                      <span  data-date-format="mm/dd/yyyy" data-date=""  class="input-append date pull-right hide">
			                          <input type="text" readonly="readonly" id="marriage-date" name="employeeQualifyingEvents[0].eventDate" value="${systemDate}"  class="span7 datePicker1" >
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                      </span>
			                     
			                  </div>
			                  <div class="row-fluid height30">
			                    <input type="hidden" name="employeeQualifyingEvents[1].employeeApplicationId" value="${employeeApplication.id}"/>
			                    <input type="hidden" name="employeeQualifyingEvents[1].createdBy" value="${userid}"/>
			                  	<input type="hidden" name="employeeQualifyingEvents[1].eventCode" id="birth-code" value="02"/>
			                  	<input type="hidden" name="employeeQualifyingEvents[1].spEventCode" value="02"/>
			                    <label  class="radio inline" for="birth">
			                      <input type="radio" name="employeeQualifyingEvents[1].eventType" id="birth" value="Birth of Child"  class="eventClass">
			                      Birth of Child
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="birth-date" readonly="readonly"  name="employeeQualifyingEvents[1].eventDate" value="${systemDate}"  class="span7 datePicker1" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid height30">
			                  	 <input type="hidden" name="employeeQualifyingEvents[2].employeeApplicationId" value="${employeeApplication.id}"/>
			                     <input type="hidden" name="employeeQualifyingEvents[2].createdBy" value="${userid}"/>
			                  	 <input type="hidden" name="employeeQualifyingEvents[2].eventCode" id="adoption-code" value="05"/>
			                  	 <input type="hidden" name="employeeQualifyingEvents[2].spEventCode" value="05"/>
			                    <label class="radio inline" for="adoption">
			                      <input type="radio" name="employeeQualifyingEvents[2].eventType" id="adoption" value="Adoption" class="eventClass">
			                      Adoption
			                    </label>
			                    <span  data-date-format="mm/dd/yyyy" data-date=""  class="input-append date  pull-right hide">
			                          <input type="text" id="adoption-date" readonly="readonly"  name="employeeQualifyingEvents[2].eventDate"  value="${systemDate}"   class="span7 datePicker1" aria-label="Adoption" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid height30">
			                   <input type="hidden" name="employeeQualifyingEvents[3].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[3].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[3].eventCode" id="divorce-code" value="01"/>
			                   <input type="hidden" name="employeeQualifyingEvents[3].spEventCode" value="07"/>
			                    <label class="radio inline info-message" for="divorce">
			                      <input type="radio" name="employeeQualifyingEvents[3].eventType" id="divorce" value="Divorce"  class="eventClass">
			                      Divorce
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="divorce-date" readonly="readonly"  name="employeeQualifyingEvents[3].eventDate"  value="${systemDate}"   class="span7 datePicker1" aria-label="Divorce" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid height30">
			                    <input type="hidden" name="employeeQualifyingEvents[4].employeeApplicationId" value="${employeeApplication.id}"/>
			                    <input type="hidden" name="employeeQualifyingEvents[4].createdBy" value="${userid}"/>
			                  	<input type="hidden" name="employeeQualifyingEvents[4].eventCode" id="seperation-code" value="31"/>
			                  	<input type="hidden" name="employeeQualifyingEvents[4].spEventCode" value="07"/>
			                    <label class="radio inline info-message" for="seperation">
			                      <input type="radio" name="employeeQualifyingEvents[4].eventType" id="seperation" value="Legal Seperation" class="eventClass">
			                      Legal Separation
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="seperation-date" readonly="readonly" name="employeeQualifyingEvents[4].eventDate"  value="${systemDate}"  class="span7 datePicker1" aria-label="Seperation" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid height30">
			                   <input type="hidden" name="employeeQualifyingEvents[5].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[5].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[5].eventCode" id="death-code" value="03"/>
			                   <input type="hidden" name="employeeQualifyingEvents[5].spEventCode" value="07"/>
			                    <label class="radio inline info-message" for="death">
			                      <input type="radio" name="employeeQualifyingEvents[5].eventType" id="death" value="Death of a Family Member" class="eventClass">
			                      Death of a Family Member
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="death-date" readonly="readonly"  name="employeeQualifyingEvents[5].eventDate"  value="${systemDate}"   class="span7 datePicker1" aria-label="Death" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid height30">
			                   <input type="hidden" name="employeeQualifyingEvents[6].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[6].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[6].eventCode" id="gained-eligibility-code" value="07"/>
			                   <input type="hidden" name="employeeQualifyingEvents[6].spEventCode" value="07"/>
			                  <label for="gained-eligibility" class="radio inline">
			                    <input type="radio" name="employeeQualifyingEvents[6].eventType" id="gained-eligibility" value="Gained Eligibility for other" class="eventClass">
			                    Gained Eligibility for other Coverage
			                  </label>
			                  <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                    <input type="text" id="gained-eligibility-date" readonly="readonly"  name="employeeQualifyingEvents[6].eventDate"  value="${systemDate}"  class="span7 datePicker1" aria-label="Gained Eligibility for other" tabindex="0">
			                    <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid height30">
			                   <input type="hidden" name="employeeQualifyingEvents[7].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[7].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[7].eventCode" id="loss-chip-code" value="07"/>
			                   <input type="hidden" name="employeeQualifyingEvents[7].spEventCode" value="07"/>
			                    <label class="radio inline" for="loss-chip">
			                   <input type="radio" name="employeeQualifyingEvents[7].eventType" id="loss-chip" value="Loss of Medicaid and CHIP" class="eventClass">
			                      Loss of Medicaid and CHIP
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="loss-chip-date" readonly="readonly"  name="employeeQualifyingEvents[7].eventDate"  value="${systemDate}"  class="span7 datePicker1" aria-label="Loss of Medicaid and CHIP" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid height30">
			                   <input type="hidden" name="employeeQualifyingEvents[8].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[8].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[8].eventCode" id="loss-mec-code" value="07"/>
			                   <input type="hidden" name="employeeQualifyingEvents[8].spEventCode" value="07"/>
			                    <label class="radio inline loss-mec-radio" for="loss-mec" title="Members of federaly recognized tribes can enroll in health plan any time of year. You can change plans as often as once a month.">
			                   <input type="radio" name="employeeQualifyingEvents[8].eventType" id="loss-mec" value="Loss of MEC (Minimum Essential Coverage)" class="eventClass">
			                      Loss of MEC (Minimum Essential Coverage)
			                    </label>   
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="loss-mec-date" readonly="readonly"  name="employeeQualifyingEvents[8].eventDate"  value="${systemDate}"  class="span7 datePicker1" aria-label="Loss of MEC (Minimum Essential Coverage)" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid height30" id="divfrt" style="display:none">
			                   <input type="hidden" name="employeeQualifyingEvents[9].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[9].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[9].eventCode" id="member-frt-code" value="AI"/>
			                    <label class="radio inline info-message-tribe" for="member-frt">
			                   <input type="radio" name="employeeQualifyingEvents[9].eventType" id="member-frt" value="Member of Federally Recognized Tribe" class="eventClass">
			                      Member of Federally Recognized Tribe
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="hidden" id="member-frt-date"  name="employeeQualifyingEvents[9].eventDate"   aria-label="Member of Federally Recognized Tribe" tabindex="0">
			                          
			                    </span>
			                  </div>
			              <!--FOR ADMIN ONLY  -->
			            <c:if test="${isEmployee=='N' }">
			            	  <div class="row-fluid"></div>
			                  <div class="row-fluid  height30">
			                   <input type="hidden" name="employeeQualifyingEvents[10].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[10].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[10].eventCode" id="newly-eligible-code" value="AI"/>
			                   <input type="hidden" name="employeeQualifyingEvents[10].spEventCode" value="NE"/>
			                    <label class="radio inline" for="newly-eligible">
			                   <input type="radio" name="employeeQualifyingEvents[10].eventType" id="newly-eligible" value="Newly Eligible" class="eventClass">
			                      Newly Eligible
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="newly-eligible-date" readonly="readonly"  name="employeeQualifyingEvents[10].eventDate"   value="${systemDate}"  class="span7 datePicker1" aria-label="Newly Eligible" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid  height30">
			                   <input type="hidden" name="employeeQualifyingEvents[11].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[11].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[11].eventCode" id="exchange-error-code" value="AI"/>
			                   <input type="hidden" name="employeeQualifyingEvents[11].spEventCode" value="ER"/>
			                    <label class="radio inline"  for="exchange-error">
			                   <input type="radio" name="employeeQualifyingEvents[11].eventType" id="exchange-error" value="Exchange Error" class="eventClass">
			                      Exchange Error
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="exchange-error-date" readonly="readonly"  name="employeeQualifyingEvents[11].eventDate"  value="${systemDate}"  class="span7 datePicker1" aria-label="Exchange Error" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid  height30">
			                   <input type="hidden" name="employeeQualifyingEvents[12].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[12].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[12].eventCode" id="financial-change-code" value="AI"/>
			                   <input type="hidden" name="employeeQualifyingEvents[12].spEventCode" value="FC"/>
			                    <label class="radio inline"  for="financial-change">
			                   <input type="radio" name="employeeQualifyingEvents[12].eventType" id="financial-change" value="Financial Change" class="eventClass">
			                      Financial Change
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="financial-change-date" readonly="readonly"  name="employeeQualifyingEvents[12].eventDate"  value="${systemDate}"  class="span7 datePicker1" aria-label="Financial Change" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid  height30">
			                   <input type="hidden" name="employeeQualifyingEvents[13].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[13].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[13].eventCode" id="dissatisfied-services-code" value="AI"/>
			                   <input type="hidden" name="employeeQualifyingEvents[13].spEventCode" value="AB"/>
			                    <label class="radio inline" for="dissatisfied-services">
			                   <input type="radio" name="employeeQualifyingEvents[13].eventType" id="dissatisfied-services" value="Dissatisfied with Medical Care/Services Rendered" class="eventClass">
			                      Dissatisfied with Medical Care/Services Rendered
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="dissatisfied-services-date" readonly="readonly"  name="employeeQualifyingEvents[13].eventDate"  value="${systemDate}"  class="span7 datePicker1" aria-label="Exceptional Circumstances" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			                  <div class="row-fluid  height30">
			                   <input type="hidden" name="employeeQualifyingEvents[14].employeeApplicationId" value="${employeeApplication.id}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[14].createdBy" value="${userid}"/>
			                   <input type="hidden" name="employeeQualifyingEvents[14].eventCode" id="exceptional-circumstances-code" value="AI"/>
			                   <input type="hidden" name="employeeQualifyingEvents[14].spEventCode" value="EX"/>
			                    <label class="radio inline" for="exceptional-circumstances">
			                   <input type="radio" name="employeeQualifyingEvents[14].eventType" id="exceptional-circumstances" value="Exceptional Circumstances" class="eventClass">
			                      Exceptional Circumstances
			                    </label>
			                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
			                          <input type="text" id="exceptional-circumstances-date" readonly="readonly"  name="employeeQualifyingEvents[14].eventDate"  value="${systemDate}"   class="span7 datePicker1" aria-label="Exceptional Circumstances" tabindex="0">
			                          <span class="add-on"><i class="icon-calendar"></i></span>
			                    </span>
			                  </div>
			            </c:if>    
			              </fieldset>
			          </div>
			        </div> <!-- /waiving-reason -->           
			        
			      </div><!-- /modal-body -->
			      <div class="modal-footer">
			          <button id="cancelBtnModal" type="button" class="btn pull-left" onclick="javascript:history.back();" aria-hidden="true">Cancel</button>           
			          <button id="qualifyingeventbtn" type="button" class="btn btn-primary pull-right" aria-hidden="true" alt="Submit">Submit</button>
			      </div>
			  </form>
			</div>
  	</div>
  </div><!--/row-fluid-->        
</div><!--/main .gutter10-->
	     

<!-- <div id="qualified-event" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="reportChangeModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" > -->
<!--/.modal -->
			<!--Qualifying Event LIGHTBOX Success  -->
			<div id="qeventsuccess" class="modal hide fade" role="dialog" aria-labelledby="qeventsuccess" aria-hidden="true" data-keyboard="false" data-backdrop="static">
				<div class="modal-header">
					<h3 id="qeventsuccessLabel">Report Change in Circumstances</h3>
				</div>
				<div class="modal-body" id="qeventsuccessBody">
					<p>Thanks for reporting this change!</p>
					<p>Based on the information you provided, you are eligible for Special Enrollment in which you can add/remove family members from coverage or change
					your selected plan.</p>
					<p>By closing this window you will be returned to your dashboard where you can begin the Special Enrollment Process.</p>
				</div>
				<div class="modal-footer">					
					<button class="btn btn-primary" id="qeventsuccessButton">Close</button>
				</div>
			</div>
			<!--Qualifying Event LIGHTBOX error  -->
			<div id="qeventerror" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="qeventerror" aria-hidden="true">
				<div class="modal-header">
					<button id="qeventerrorCrossClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
					<h3 id="qeventerrorLabel">Report Change in Circumstances</h3>
				</div>
				<div class="modal-body" id="qeventerrorBody">
					<p>Thanks for reporting this change!</p>
					<p>Based on the date of your qualifying event <span id="datelist"></span>. you are not eligible for Special Enrollment at this time. </p>
					<p>If you wish to add/remove family members from coverage or change your selected plan, you will be able to do so during your employer's next Open
					Enrollment period which begins on <fmt:formatDate pattern="MM/dd/yyyy" value="${nextOpenEnrollmentStart}" /> .</p>
				</div>
				<div class="modal-footer">					
					<button class="btn btn-primary" id="qeventerrorButton" >Close</button>
				</div>
			</div>			
			<!--federally recognized tribe LIGHTBOX Success  -->
			<div id="nativeamrerror" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="nativeamrerror" aria-hidden="true">
				<div class="modal-header">
					<button id="nativeamrerrorCrossClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
					<h3 id="nativeamrerrorLabel">Report Change in Circumstances</h3>
				</div>
				<div class="modal-body" id="nativeamrerrorBody">
					<p>You have made the changes please come again on next month.</p>
				</div>
				<div class="modal-footer">					
					<button class="btn btn-primary" id="nativeamrerrorClose">Close</button>
				</div>
			</div>		
			
	<!-- temporary Modal for notifying Sp enrollment is not yet ready. Starts here -->
	<div class="modal fade hide" id="blockSPEnrollment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	      </div>
	      <div class="modal-body">
	        <div class="row-fluid">
	        	<p>Special enrollment functionality is not available now. Please check back next week for reporting your qualifying event.</p>
	        	<p>It is very important that you report a qualifying event like birth, marriage etc within 30 days of the event to qualify for special enrollment in your employer coverage.</p>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary"  data-dismiss="modal">Ok</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->		
	<!-- temporary Modal for notifying Sp enrollment is not yet ready. ends here -->
						
							
<script type="text/javascript">
var systemDate = '${systemDate}';
$(document).ready(function(){
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			return false;
		}
	 });
	//qualified-event-box
	var onlyViewAllowed = '${onlyViewAllowed}';
	if(onlyViewAllowed == 'Y'){
		$('#qualified-event-box').unbind('click');
		$('#qualified-event-box').attr('disabled','disabled');
	}
	else{
		$('#qualified-event-box').bind('click');
		$('#qualified-event-box').removeAttr('disabled')
	}
	resetEventForm();
   	//$('.span7').val('');
    $('div[rel=popover]').popover({trigger:'hover'});
    $('div[rel=popover]').popover({trigger:'focus'}); 

   
    
    $('.datePicker1').datepicker({
    	endDate: '+'+ systemDate + 'd',
		format:'mm/dd/yyyy',
		autoclose:true,
		forceParse: false,
		showOn : "button",
		//buttonImage : imgpath,
		//buttonImageOnly : true
	});
    
    var isnativeAmr = '${nativeAmr}';
    
    if(isnativeAmr == 'true'){
    	$('#divfrt').css('display', 'block');
    }else{
    	$('#divfrt').css('display', 'none');
    }
	
	$('.add-on').click(function(){
		$(this).parent('.input-append').find('.datePicker1').focus();
	});

	$('.info').tooltip();
	$('i.icon-ok').parents('li').css('list-style','none');
    
});

$('input[type=radio]').click(function(){ // adds and removes date field
   var clickedradioID = $(this).attr('id');
  
	//$.each($('.datepicker1'), function() {
    //   alert($(this).attr('id'));
  //});
	
	
	// $('input:radio').each(function(i, item) {
	 //  alert("check val == "  + $("input[name='employeeEvents["+i+"].eventDate'").val() );
    //   alert($('#employeeEvents['+i+'].eventDate').val());
   // });
   
   $('input:radio').each(function() {
	   $(this).parent().parent().find("input[type=text]").val('');  
	   if(clickedradioID != $(this).attr('id') ) {
		   $(this).parent().removeClass("checked");
	       $(this).parent().siblings('span').addClass("hide");
	       $(this).prop('checked', false);
	      
	   } 
	   else {
		   $(this).parent().addClass("checked"); //added for pseudo-element to work. Don't remove
           $(this).parent().siblings('span').removeClass("hide");
	   }
	 });
});


$('#qualified-event-box').click(function() {
	resetEventForm();
	var isnativeAmr = '${nativeAmr}'; 
	var employeeStatus='${employee.status}';
	
	if(isnativeAmr == 'true' && employeeStatus == 'NOT_ENROLLED'){
		if(isAddEventAllowed == 'true'){
			window.location.href = "<c:url value='/shop/employee/spplanselection/addnativeamremployeeevent/${employeeApplication.id}' />";
		}else{
			$('#nativeamrerror').modal('show');
		}
	}else{
	   	//$('#blockSPEnrollment').modal('show');
		$('#qualified-event').modal('show'); 
		// $('body').css('overflow','hidden');
	}
     
});



function validateForm(){
	var isValid=true;
	var checked = $("#reportChange-form input:radio").length > 0;
	if (!$("input[type=radio]:checked").val()) {
    	isValid=false;
        alert("Please select at least one event");
    }
    else{
    		var isEmployee = '${isEmployee}';
	    	var loopLimit = (isEmployee=='Y') ? 8 : 12;
    		for(var i=0;i<loopLimit;i++){
        		
        		var checkEvent=document.getElementsByName('employeeQualifyingEvents['+i+'].eventType');
        		var dateVal=document.getElementsByName('employeeQualifyingEvents['+i+'].eventDate');
        		
        		if(checkEvent[0].checked){
        			if(dateVal[0].value == '' || dateVal[0].value == 'undefined'){
        				alert("Please select event date for the event selected.");	
        				isValid=false;
        				break;
        			}
        		} 
        	}    	
        }
	return isValid;
}
    
    
$('#qualifyingeventbtn').click(function() {
	 
	if($('#member-frt')[0].checked){
		$('#member-frt-date').val(systemDate);
	}
   	if(validateForm()) {
   		var employeeAppId = '${employeeApplication.id}';
   		
   		var selEventName = $(".eventClass:checked").attr('id'); 
   		var selEventDate = $("#"+selEventName+"-date").val();
   		var selEventCode = $("#"+selEventName+"-code").val();
   		
   		
	var subUrl = '<c:url value="/shop/employee/spplanselection/validatequalifyingevent"> 
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
   	$.ajax(
   			{
           		type: "POST",
          		url: subUrl,
          		data : {employeeAppId : employeeAppId, eventCode: selEventCode, eventDate: selEventDate}, 
           		dataType:'json',
           		success: function(response,xhr){
	              if(isInvalidCSRFToken(xhr))
	                return;
           			handleResponse(response);
           		},
          			error: function(e){
          				alert('Failed to validate Event Date.');

           		}
           });
   	}
   	
   });
   
    function handleResponse(response){
    	$('#qualified-event').modal('hide');
			var successflag = false;
			var dateliststr = '';
			  $.each(response, function( key, val ) {
				  dateliststr += key + ",";
			      if(key == "success"){
			    	  successflag = true;
			      }
			  });
			if(successflag){
   				$('#qeventsuccess').modal('show');	
			}else{
				$('#datelist').text( dateliststr.slice(0, -1) );
				$('#qeventerror').modal('show');
			}
    }
    
   $('#qeventsuccessButton').click(function() {
   	$('#qeventsuccess').modal('hide');
   	$("#reportChange-form").submit();
   	$('.span7').val('');
   });
   
   $('#qeventerrorButton').click(function() {
   	$('#qeventerror').modal('hide');
   	$('.span7').val('');
   	return false;
   });
   $('#nativeamrerrorClose').click(function() {
	   	$('#nativeamrerror').modal('hide'); 
  });  
   /* $('.datePicker').datepicker({
		endDate:'-1d',
		format:'mm/dd/yyyy',
		autoclose:true
   }); */ 

function resetEventForm(){
    $('#reportChange-form').find(':input').each(function() {
    	switch(this.type) {
    	case 'text':
        case 'radio':
       	var eleId=this.id;
        $(this).parent().removeClass("checked");
        $('#'+eleId).prop('checked','');
        $(this).parent().siblings('span').addClass("hide");
   	        }
        });
}

   function gotoNext(pageNum){
		if(pageNum=='1'){
			location.href="<c:url value='/shop/employee/editemployee/${employeeApplication.id}' />";
		}
		else if(pageNum=='2'){
			location.href="<c:url value='/shop/employee/spplanselection/spenrollverifydependent/${employeeApplication.id}' />";
		}
		else if(pageNum=='3'){
			location.href="<c:url value='/shop/employee/employeeselectplan/${employeeApplication.id}' />";
		}
		else if(pageNum=='5'){
			location.href="<c:url value='/shop/employee/spplanselection/myplans/${employeeApplication.id}' />";
		}
		/* else if(pageNum=='4'){
			location.href="<c:url value='/shop/employee/verifyemployee' />";
		}
		 */
	}

$( '#crossClose, #reportChangeModalCrossClose, #qeventerrorCrossClose, #nativeamrerrorCrossClose').click(function(){
	resetEventForm();
   	$('.span7').val('');
	 $('body').css('overflow','auto');
});

/* $('.add-on').click(function(){
	$('.datePicker').focus();
}); */
function imgSize(){
    $(".carrierlogo").each(function(){
        var width = $(this).width();
        var height = $(this).height();
        var maxWidth = 160;
		var maxHeight = 60;
		
		// Check if the current width is larger than the max
	    if(width > maxWidth){
	        ratio = maxWidth / width; // get ratio for scaling image
	        $(this).width(maxWidth); // Set new width
	        $(this).height((height * ratio)); // Scale height based on ratio
	        height = height * ratio; // Reset height to match scaled image
	    	width = width * ratio; // Reset width to match scaled image
	    }
		
		// Check if current height is larger than max
		if(height > maxHeight){                                     
		  ratio = maxHeight / height; // get ratio for scaling image
		  $(this).height(maxHeight); // Set new height
		  $(this).width((width * ratio)); // Scale width based on ratio    
		}
	    
		$(this).css('display','block');
    });
}

</script>
