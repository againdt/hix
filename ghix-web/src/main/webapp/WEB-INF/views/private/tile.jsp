<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div id="mainSummary" class="ps-plans__tiles">
    <%-- cp-sample__container --%>
    <div class="cp-sample__container">
        <c:choose>
            <c:when test="${gpsVersion == 'V1'}">
                <h2 class="cp-sample__header"><spring:message code="pd.label.title.calculatingGIPlanScores"/></h2>
                <div class="cp-tile poor dummyTileDisplay">
                    <div class="score">
                        <div class="circle-wrapper">
                            <div class="center-circle"></div>
                            <div class="slice">
                                <div class="pie fill pie-color"></div>
                                <div class="pie pie-color"></div>
                            </div>
                            <div class="circle-core center-circle">
                                <div class="circle-num"><spring:message code="pd.label.dummyTileDisplay.circleNum"/></div>
                            </div>
                        </div>
                    </div> <!-- .score -->
            </c:when>
            <c:otherwise>
                <h2 class="cp-sample__header"><spring:message code="pd.label.title.findingHealthPlans"/></h2>
                <div class="cp-loading--outter"></div>
                <div class="cp-loading--inner"></div>
                <div class="cp-tile cp-sample__tile">
            </c:otherwise>
        </c:choose>

            <!-- tile body -->
            <div class="cp-tile__body">
                <!-- sample logo img -->
                <a href="#detail" id="detail_<@=id@>" class="cp-tile__img-link cp-sample__img-link detail">
                    <img src="<c:url value="/resources/img/sampleinsurance.png" />" alt="sample Plan Logo" class="cp-tile__img">
                </a>
                <!-- sample logo img end-->

                <!-- sample plan name -->
                <div class="cp-tile__plan-name">
                    <spring:message code="pd.label.title.sampleInsurance"/>
                </div>
                <!-- sample plan name end-->

                <!-- sample premium -->
                <div class="cp-tile__premium">
                    <span class="cp-tile__premium-amount"><spring:message code="pd.label.title.dollarSigns"/></span>
                    <span class="cp-tile__premium-month"><spring:message code="pd.label.title.premiumPerMonth"/></span>
                </div>
                <!-- sample premium end-->

                <table class="u-margin-top-20">
                    <!-- sample primary care visits -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <spring:message code="pd.label.title.officeVisits"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.dollarSigns2"/>
                        </td>
                    </tr>
                    <!-- sample primary care visits end-->


                    <!-- sample Drug Tier -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <spring:message code="pd.label.title.genericDrugs"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.noCharge"/>
                        </td>
                    </tr>
                    <!-- sample Drug Tier end-->

                    <!-- sample Deductible -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <spring:message code="pd.label.commontext.deductible"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.dollarSigns4"/>
                        </td>
                    </tr>
                    <!-- sample Deductible end-->

                    <!-- sample Out-Of-Pocket Maximum -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <spring:message code="pd.label.oopmax"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.dollarSigns4"/>
                        </td>
                    </tr>
                    <!-- sample Out-Of-Pocket Maximum end-->


                    <!-- sample Expense Estimate -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <spring:message code="pd.label.title.expenseEstimate"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.dollarSigns4"/>
                        </td>
                    </tr>
                    <!-- sample Deductible end-->
                </table>
            </div>

            <!-- Sample tile footer -->
            <div class="cp-tile__footer">

                    <label for="sampleCompare" class="cp-tile__item">
                        <input type="checkbox" class="ps-form-check-input u-margin-top-0 u-margin-right-5 compare" id="sampleCompare">
                        <spring:message code="pd.label.title.compare" htmlEscape="true"/>
                    </label>

                    <a href="javascript:void(0)" class="detail cp-tile__item">
                        <spring:message code="pd.label.title.details"/>
                    </a>
            </div>
            <!-- Sample tile footer ends -->
        </div>
        <!-- Sample tile -->
    </div>
    <!-- cp-sample__container end-->
</div> <!-- #mainSummary -->

<div id="mainSummary_err" class="alert alert-block" style="display:none">
    <p><spring:message code="pd.label.info.plans.notAvailable"/></p>
</div>


<script type="text/html" id="resultItemTemplateMainSummary">
    <@ if($("#gpsVersion").val() == 'V1') { @>
        <@ if(smartScore >=80){ @>
            <div class="cp-tile detailPlanTile best details">
                <a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.green" htmlEscape="true"/>" aria-label="<spring:message code="pd.label.tooltip.summary.planDetail.green" htmlEscape="true"/>"   data-html="true" rel="tooltip" role="tooltip" data-placement="bottom" href="#">
        <@ } else if(smartScore >=60){ @>
            <div class="cp-tile detailPlanTile ok details">
                <a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.yellow" htmlEscape="true"/>" data-html="true" rel="tooltip" role="tooltip" aria-label="<spring:message code="pd.label.tooltip.summary.planDetail.yellow" htmlEscape="true"/>"data-placement="bottom" href="#">
        <@ } else { @>
            <div class="cp-tile detailPlanTile poor details">
                <a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.red" htmlEscape="true"/>" data-html="true" rel="tooltip" role="tooltip" aria-label="<spring:message code="pd.label.tooltip.summary.planDetail.red" htmlEscape="true"/>"data-placement="bottom" href="#">
        <@ } @>

            <div class="score">
                <div class="circle-wrapper">
                    <div class="center-circle"></div>
                    <div class="slice">
                        <div class="pie score_<@=smartScore@> pie-color"></div>
                    </div>
                    <div class="circle-core center-circle">
                        <div class="circle-num"><@=smartScore@></div>
                    </div>
                </div>
            </div>
        </a>
    <@ } else { @>
        <div class="cp-tile" tabindex="0" aria-label="You are on plan <@=name@>">
    <@ } @>

        <!-- tile header -->
		<@ if($('#stateCode').val() != 'CA') { @>
			<@ if(expenseEstimate == "Lower Cost"){ @>
    	        <div class="cp-tile__header cp-tile__header--lower">
        	<@ } else if(expenseEstimate == "Average"){ @>
            	<div class="cp-tile__header cp-tile__header--average">
 	     	<@ } else if(expenseEstimate == "Pricey"){ @>
  	         	<div class="cp-tile__header cp-tile__header--higher">
			<@ } else { @>
        		<div class="cp-tile__header">
       	 	<@ } @>

			<a
				href="#"
				onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover', 'eventAction': '<spring:message code="pd.label.title.expenseEstimate"/> - Hover', 'eventLabel': '<@=name@>'});"
                class="gtm_tile_total_expense"
				rel="tooltip"
				role="tooltip"
				data-placement="bottom"
				data-html="true"
				data-original-title="<spring:message code="pd.tooltip.summary.estimatedCostDetail" htmlEscape="true"/>"
				aria-label="<spring:message code="pd.label.title.expenseEstimate"/>help text:<spring:message code="pd.tooltip.summary.estimatedCostDetail" htmlEscape="true"/> help text finished">

			<@ if(expenseEstimate == "Lower Cost"){ @>
	           <spring:message code="pd.label.commontext.lowerExpense"/>
	        <@ } else if(expenseEstimate == "Average"){ @>
    	       <spring:message code="pd.label.commontext.mediumExpense"/>
        	<@ } else if(expenseEstimate == "Pricey"){ @>
           	   <spring:message code="pd.label.commontext.higherExpense"/>
        	<@ } else { @>
        		<@=expenseEstimate@>
       	 	<@ } @>

			</a>
        </div>
		<@ } @>

        <!-- tile header end-->

        <!-- tile body -->
        <div class="cp-tile__body">
            <!-- logo img -->
            <a href="#detail" id="detail_<@=id@>" class="cp-tile__img-link detail">
                <@ if ($('#stateCode').val() == 'CT' && issuer.length > 75) {@>
                    <img id="detail_<@=id@>" src="<@=issuerLogo@>" alt="<@=issuer.substr(0,75)@>..." class="cp-tile__img">
                    <!-- <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer.substr(0,75)@>..." onload="resizeimage(this)" /></div> -->
                <@ } else if($('#stateCode').val() != 'CT' && issuer.length > 24) { @>
                    <img id="detail_<@=id@>" src="<@=issuerLogo@>" alt="<@=issuer.substr(0,24)@>..." class="cp-tile__img">
                    <!-- <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer.substr(0,24)@>..." onload="resizeimage(this)" /></div> -->
                <@ } else { @>
                    <img id="detail_<@=id@>" src="<@=issuerLogo@>" alt="<@=issuer@>" class="cp-tile__img">
                    <!-- <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="planimg carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer@>" onload="resizeimage(this)" /></div> -->
                <@ } @>
            </a>
            <!-- logo img end-->

            <!-- plan name -->
            <div class="cp-tile__plan-name">
                <a class="detail" href="#detail" id="detail_<@=id@>" data-placement="bottom" rel="tooltip" role="tooltip" data-original-title="<@=name@>" aria-label="<@=name@>">
                    <@ if ($('#stateCode').val() == 'CT' && name.length > 75) {@>
                        <@=name.substr(0,75)@>...
                    <@ } else if($('#stateCode').val() != 'CT' && name.length > 24) { @>
                        <@=name.substr(0,24)@>...
                    <@ } else {@>
                        <@=name@>
                    <@ } @>
                </a>
            </div>
            <!-- plan name end-->

            <!-- metal tier -->
            <div>
                <@ if(level === 'PLATINUM') {@>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--platinum"><spring:message code="pd.label.filterby.platinum"/>
                <@ } else if(level === 'GOLD') { @>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--gold"><spring:message code="pd.label.filterby.gold"/>
                <@ } else if(level === 'SILVER') { @>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--silver"><spring:message code="pd.label.filterby.silver"/>
                <@ } else if(level === 'BRONZE') {@>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--bronze"><spring:message code="pd.label.filterby.bronze"/>
                <@ } else if(level === 'CATASTROPHIC') {@>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--catastrophic"><spring:message code="pd.label.filterby.catastropic"/>
                <@ } else {@>
                    <span class="cp-tile__metal-tier cp-tile__metal-tier--bronze"><@=level@>
                <@ } @>

				<@if(hsa=="Yes"){ @> &nbsp;HSA <@} @>

				&nbsp;<@=networkType@></span>

				<@ var planNum = issuerPlanNumber.substr(issuerPlanNumber.length - 2, issuerPlanNumber.length) @>
				<@ if(planNum !== '00' && planNum !== '01') {@>
					<span class="cp-tile__metal-tier cp-tile__metal-tier--csr cp-tile__metal-tier--csr-right ps-csr-show hide"><spring:message code="pd.label.title.csr"/></span>
				<@ } @>
            </div>
            <!-- metal tier end-->

			<!-- current plan -->
			<@ if ($("#enrollmentType").val() == 'A' && $("#initialHealthPlanId").val() == planId){ @>
				<div>
					<span class="cp-tile__current-plan">
						<spring:message code="pd.label.title.yourCurrentPlan"/>
					</span>
				</div>
			<@ } @>
			<!-- current plan end -->

            <!-- premium -->
            <div class="cp-tile__premium">
				<span class="cp-tile__premium-amount">$<@=premiumAfterCredit.toFixed(2) @></span>
				<span class="cp-tile__premium-month"><spring:message code="pd.label.title.premiumPerMonth"/></span>
			</div>
            <!-- premium end-->

            <!-- tax credit -->
            <c:if test="${exchangeType != 'OFF' && (maxAptc > 0 || maxStateSubsidy > 0)}">
                <@ var totalCredit = 0; @>
            	<@ if (aptc > 0 || $('#stateCode').val() === 'CA') {@>
                <@ totalCredit = totalCredit + (+aptc.toFixed(2)); @>
                <@ } @>
                <@ if(stateSubsidy){ @>
                <@    totalCredit = totalCredit + (+stateSubsidy.toFixed(2)); @>
                <@ } @>
                <div class="cp-tile__tax-credit">
                <@ if (totalCredit !== 0 && stateSubsidy !== null) { @>
                        <a
                            href="#"
                            rel="tooltip"
                            role="tooltip"
                            data-html="true"
                            data-placement="bottom"
                            data-original-title="<table>
                                                    <tr>
                                                        <td><spring:message code="pd.label.commontext.federal" htmlEscape="false"/>
                                                        <td>=</td>
                                                        <td>$<@=aptc && aptc.toFixed(2)@><td/>
                                                    </tr>
                                                    <tr>
                                                        <td><spring:message code="pd.label.commontext.state" htmlEscape="false"/>
                                                        <td>=</td>
                                                        <td>$<@=stateSubsidy && stateSubsidy.toFixed(2)@><td/>
                                                    <tr>
                                                        <td><spring:message code="pd.label.commontext.total" htmlEscape="false"/>
                                                        <td>=</td>
                                                        <td>$<@=totalCredit && totalCredit.toFixed(2)@><td/>
                                                    </tr>
                                                    </table>">
                                <spring:message arguments="<@=totalCredit.toFixed(2)@>" code="pd.label.commontext.afterTaxCredit" htmlEscape="false"/>
                        </a>

                <@ } else { @>
                <spring:message arguments="<@=totalCredit.toFixed(2)@>" code="pd.label.commontext.afterTaxCredit" htmlEscape="false"/>
                <@ } @>
                </div>
            </c:if>
            <!-- tax credit end -->

            <table class="cp-tile__table">
                <!-- primary care visits -->
                <tr>
					<th class="cp-tile__label" scope="row">

                        <c:if test="${stateCode == 'CT' || stateCode == 'MN'}">
                            <a
                                href="#"
                                rel="tooltip"
                                role="tooltip"
                                data-html="true"
                                data-placement="right"
                                data-original-title="<spring:message code="pd.label.title.officeVisits.tooltip" htmlEscape="false"/>"
                                aria-label="<spring:message code="pd.label.title.officeVisits"/> help text: <spring:message code="pd.label.title.officeVisits.tooltip" htmlEscape="false"/> help text finished">
                                <spring:message code="pd.label.title.officeVisits"/>
                            </a>
                        </c:if>
                        <c:if test="${stateCode != 'CT' && stateCode != 'MN'}">
                            <spring:message code="pd.label.title.officeVisits"/>
                        </c:if>
					</th>
					<td class="cp-tile__value">
						<@ if($('#stateCode').val() == "CA") { @>
							<spring:message code="pd.label.title.youpay"/>&nbsp;<@=planTier1.PRIMARY_VISIT.tileDisplayVal@>
						<@ } else if($('#stateCode').val() == "MN") { @>
							<a
								href="#"
								rel="tooltip"
								role="tooltip"
								data-html="true"
								data-placement="top"
								data-original-title="<@=planTier1.PRIMARY_VISIT.displayVal@>"
								aria-label="<@=planTier1.PRIMARY_VISIT.displayVal@>">
								<@=planTier1.PRIMARY_VISIT.tileDisplayVal@>
								<@ if(planTier1.PRIMARY_VISIT.tileDisplayVal.indexOf('/') === -1) { @>
									<@ if( planTier1.PRIMARY_VISIT.tileDisplayVal.indexOf('%') !== -1 ) { @>
										<spring:message code="pd.label.coinsurance"/>
									<@ } else { @>
										<spring:message code="pd.label.copay"/>
									<@ } @>
								<@ } @>
							</a>
						<@ } else { @>
							<@=planTier1.PRIMARY_VISIT.tileDisplayVal@>
						<@ } @>
					</td>
				</tr>
                <!-- primary care visits end-->

                <!-- generic drugs -->
                <tr>
					<th class="cp-tile__label" scope="row">
                        <c:if test="${stateCode == 'CT' || stateCode == 'MN'}">
                            <a
                                href="#"
                                rel="tooltip"
                                data-html="true"
                                role="tooltip"
                                data-placement="right"
                                data-original-title="<spring:message code="pd.label.title.genericDrugs.tooltip" htmlEscape="false"/>"
                                aria-label="<spring:message code="pd.label.title.genericDrugs"/> help text: <spring:message code="pd.label.title.genericDrugs.tooltip" htmlEscape="false"/> help text finished">
                                <spring:message code="pd.label.title.genericDrugs"/>
                            </a>
                        </c:if>
                        <c:if test="${stateCode != 'CT' && stateCode != 'MN'}">
                            <spring:message code="pd.label.title.genericDrugs"/>
                        </c:if>
					</th>
					<td class="cp-tile__value">
                        <@ if($('#stateCode').val() == "CA") { @>
                            <spring:message code="pd.label.title.youpay"/>&nbsp;<@=planTier1.GENERIC.tileDisplayVal @>
                        <@ } else if($('#stateCode').val() == "MN"){ @>
							<a
                                href="#"
                                rel="tooltip"
                                data-html="true"
                                role="tooltip"
                                data-placement="top"
                                data-original-title="<@=planTier1.GENERIC.displayVal @>"
                                aria-label="<@=planTier1.GENERIC.displayVal @>">
                                <@=planTier1.GENERIC.tileDisplayVal @>
                            </a>
						<@ } else { @>
                            <@=planTier1.GENERIC.tileDisplayVal @>
                        <@ } @>
					</td>
				</tr>
                <!-- generic drugs end-->

                <!-- deductible -->
                <tr>
					<th class="cp-tile__label" scope="row">
                    	<a
							href="#"
							rel="tooltip"
                            class="gtm_tile_yearly_deductible"
                            onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover', 'eventAction': '<spring:message code="pd.label.Deductible"/> - Hover', 'eventLabel': '<@=name@>'});"
                            role="tooltip"
							data-placement="right"
							data-html="true"
                            data-original-title="<spring:message code="pd.label.tooltip.deductible"/>"
                            aria-label="<spring:message code="pd.label.Deductible"/>  help text: <spring:message code="pd.label.tooltip.deductible"/> help text finished">
                            <spring:message code="pd.label.Deductible"/>
                        </a>
					</th>
					<td class="cp-tile__value">
                        <@ if(intgMediDrugDeductible !=null ){@>
                            <a
                                href="#"
                                rel="tooltip"
                                role="tooltip"
                                data-placement="top"
                                data-html="true"
                                data-original-title="<spring:message arguments="<@=intgMediDrugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>"
                                aria-label="$<@=intgMediDrugDeductible.toFixed()@> help text:<spring:message arguments="<@=intgMediDrugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/> help text finished">
                                $<@=intgMediDrugDeductible.toFixed()@>
                            </a>
                        <@ } else if((medicalDeductible != null) || (drugDeductible!= null)){ @>
                            <@  if(medicalDeductible !=null && drugDeductible != null){ @>
                                <a
                                    href="#"
                                    rel="tooltip"
                                    role="tooltip"
                                    data-placement="top"
                                    data-html="true"
                                    data-original-title="<spring:message arguments="<@=medicalDeductible.toFixed()@>,<@=drugDeductible.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/>"
                                    aria-label="$<@=medicalDeductible.toFixed()@> / $<@=drugDeductible.toFixed()@> help text <spring:message arguments="<@=medicalDeductible.toFixed()@>,<@=drugDeductible.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/> help text finished">
                                    $<@=medicalDeductible.toFixed()@> / $<@=drugDeductible.toFixed() @>
                                </a>
                            <@ }else if(medicalDeductible !=null  ){ @>
                                <a
                                    href="#"
                                    rel="tooltip"
                                    role="tooltip"
                                    data-placement="top"
                                    data-html="true"
                                    data-original-title="<spring:message arguments="<@=medicalDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>"
                                    aria-label="$<@=medicalDeductible.toFixed()@> help text <spring:message arguments="<@=medicalDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>help text finished">
                                    $<@=medicalDeductible.toFixed() @>
                                </a>
                            <@ }else if(drugDeductible != null ){ @>
                                <a
                                    href="#"
                                    rel="tooltip"
                                    role="tooltip"
                                    data-placement="top"
                                    data-html="true"
                                    data-original-title="<spring:message arguments="<@=drugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>"
                                    aria-label="$<@=drugDeductible.toFixed()@>help text<spring:message arguments="<@=drugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>help text finished">
                                    $<@=drugDeductible.toFixed() @>
                                </a>
                            <@ } @>
                        <@ } @>

						<@ if($('#stateCode').val() == "CA") { @>
							<a
								href="#"
                                rel="tooltip"
                                role="tooltip"
                                data-placement="top"
                                data-html="true"
                                data-original-title="<spring:message code="pd.label.title.deductibledesc.tooltip"/>"
                                aria-label="<spring:message code="pd.label.title.deductibledesc.tooltip"/> help text finished">
                                <spring:message code="pd.label.title.deductibledesc"/>
                            </a>
                        <@ } @>
					</td>
				</tr>
                <!-- deductible end -->

				<!-- total expense estimate -->
				<@ if($('#stateCode').val() == "CA") { @>
					<tr>
						<th class="cp-tile__label" scope="row">
							<a
								href="#"
                                class="gtm_tile_total_expense"
                                onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover', 'eventAction': '<spring:message code="pd.label.title.expenseEstimate" /> - Hover', 'eventLabel': '<@=name@>'});"
								rel="tooltip"
								role="tooltip"
								data-placement="right"
								data-html="true"
								data-original-title="<spring:message code="pd.tooltip.summary.estimatedCostDetail" htmlEscape="true"/>"
								aria-label="<spring:message code="pd.label.title.expenseEstimate"/>help text:<spring:message code="pd.tooltip.summary.estimatedCostDetail" htmlEscape="true"/> help text finished">

								<spring:message code="pd.label.title.expenseEstimate" />

							</a>
						</th>
						<td class="cp-tile__value">
							<a
								href="#"
								rel="tooltip"
								role="tooltip"
								data-placement="top"
								data-html="true"
								data-original-title="<spring:message arguments="<@=parseFloat(annualPremiumAfterCredit).toFixed(2)@>,<@=parseFloat(estimatedTotalHealthCareCost - annualPremiumAfterCredit).toFixed(2)@>, <@=parseFloat(estimatedTotalHealthCareCost).toFixed(2)@>" code="pd.tooltip.summary.dynamicCostDetail"/>"
								aria-label="<spring:message code="pd.label.title.expenseEstimate"/>help text:<spring:message code="pd.tooltip.summary.dynamicCostDetail" htmlEscape="true"/> help text finished">

							<@ if(expenseEstimate == "Lower Cost"){ @>
	      				    	<spring:message code="pd.label.commontext.lowerExpense"/> <span class="best">
	     				  	<@ } else if(expenseEstimate == "Average"){ @>
    	      					<spring:message code="pd.label.commontext.mediumExpense"/> <span class="ok">
        					<@ } else if(expenseEstimate == "Pricey"){ @>
           	  					<spring:message code="pd.label.commontext.higherExpense"/> <span class="poor">
        					<@ } @>

							<i class="icon-flag"></i></span>
							</a>
						</td>
					</tr>
				<@ } @>
				<!-- total expense estimate end -->


                <!-- out-of-pocket maximum -->
                <@ if($("#showOopOnOff").val() == 'ON') { @>
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <a
                                href="#"
                                class="gtm_tile_oop_max"
                                onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover', 'eventAction': '<spring:message code="pd.tooltip.oopmax" /> - Hover', 'eventLabel': '<@=name@>'});"
                                rel="tooltip"
                                role="tooltip"
                                data-placement="right"
                                data-html="true"
                                data-original-title="<spring:message code="pd.tooltip.oopmax"/>" aria-label="<spring:message code="pd.label.oopmax"/> help text:<spring:message code="pd.tooltip.oopmax"/> help text finished">
                                <spring:message code="pd.label.oopmax"/>
                            </a>
                        </th>

                        <td class="cp-tile__value">
                            <@ if(intgMediDrugOopMax != null){@>
                                <a
                                    href="#"
                                    rel="tooltip"
                                    role="tooltip"
                                    data-placement="top"
                                    data-html="true"
                                    data-original-title="<spring:message arguments="<@=intgMediDrugOopMax.toFixed()@>" code="pd.tooltip.intgMediDrugOopMax" htmlEscape="false"/>"
                                    aria-label="$<@=intgMediDrugOopMax.toFixed()@>help text<spring:message arguments="<@=intgMediDrugOopMax.toFixed()@>" code="pd.tooltip.intgMediDrugOopMax" htmlEscape="false"/>help text finished">
                                    $<@=intgMediDrugOopMax.toFixed()@>
                                </a>
                            <@ } else if((medicalOopMax != null ) || (drugOopMax != null)){ @>
                                <@ if(medicalOopMax !=null && drugOopMax != null){ @>
                                    <a
                                        href="#"
                                        rel="tooltip"
                                        role="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message arguments="<@=medicalOopMax.toFixed()@>,<@=drugOopMax.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/>"
                                        aria-label="$<@=medicalOopMax.toFixed()@> / $<@=drugOopMax.toFixed()@>help text<spring:message arguments="<@=medicalOopMax.toFixed()@>,<@=drugOopMax.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/>help text finished">
                                        $<@=medicalOopMax.toFixed()@> / $<@=drugOopMax.toFixed()@>
                                    </a>
                                <@ } else if(medicalOopMax !=null){@>
                                    <a
                                        href="#"
                                        rel="tooltip"
                                        role="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message arguments="<@=medicalOopMax.toFixed()@>" code="pd.tooltip.medicalOopMax" htmlEscape="false"/>"
                                        aria-label="$<@=medicalOopMax.toFixed()@>help text<spring:message arguments="<@=medicalOopMax.toFixed()@>" code="pd.tooltip.medicalOopMax" htmlEscape="false"/>help text finished">
                                        $<@=medicalOopMax.toFixed()@>
                                    </a>
                                <@ } else if(drugOopMax != null){ @>
                                    <a
                                        href="#"
                                        rel="tooltip"
                                        role="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message arguments="<@=drugOopMax.toFixed()@>" code="pd.tooltip.drugOOPMax" htmlEscape="false"/>"
                                        aria-label="$<@=drugOopMax.toFixed()@>help text<spring:message arguments="<@=drugOopMax.toFixed()@>" code="pd.tooltip.drugOOPMax" htmlEscape="false"/>help text finished">
                                        $<@=drugOopMax.toFixed()@>
                                    </a>
                                <@ } @>
                            <@ } else if($('#stateCode').val() == "CA"){ @>
                                <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
                            <@ } @>
                        </td>
                    </tr>
                <@ } @>
                <!-- out-of-pocket maximum end-->

                <!-- network -->
                <@ if($("#showNetworkTransparencyRating").val() == 'ON') { @>
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <a
                                href="#"
                                rel="tooltip"
                                role="tooltip"
                                data-placement="right"
                                data-html="true"
                                data-original-title="<spring:message code="pd.tooltip.network" htmlEscape="true"/>"
                                aria-label="<spring:message code="pd.label.network" htmlEscape="true"/>help text:<spring:message code="pd.tooltip.network" htmlEscape="true"/>help text finished">
                                <spring:message code="pd.label.network" htmlEscape="true"/>
                            </a>
                        </th>

                        <td class="cp-tile__value">
                            <@ if(networkTransparencyRating === null){@>
                                <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
                            <@ } else {@>
                                <@ if(networkTransparencyRating.toUpperCase() === 'BASIC'){@>
                                    <a
                                        href="#"
                                        rel="tooltip"
                                        role="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message code="pd.tooltip.network.basic" htmlEscape="true"/>"
                                        aria-label="<spring:message code="pd.network.basic" htmlEscape="true"/>help text<spring:message code="pd.tooltip.network.basic" htmlEscape="true"/>help text finished">
                                        <spring:message code="pd.network.basic" htmlEscape="true"/>
                                    </a>
                                <@ } else if(networkTransparencyRating.toUpperCase() === 'STANDARD') { @>

                                    <a
                                        href="#"
                                        rel="tooltip"
                                        role="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message code="pd.tooltip.network.standard" htmlEscape="true"/>"
                                        aria-label="<spring:message code="pd.network.standard" htmlEscape="true"/>help text<spring:message code="pd.tooltip.network.standard" htmlEscape="true"/>help text finished">
                                        <spring:message code="pd.network.standard" htmlEscape="true"/>
                                    </a>
                                <@ } else if(networkTransparencyRating.toUpperCase() === 'BROAD') { @>

                                    <a
                                        href="#"
                                        rel="tooltip"
                                        role="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message code="pd.tooltip.network.broad" htmlEscape="true"/>"
                                        aria-label="<spring:message code="pd.network.broad" htmlEscape="true"/>help text<spring:message code="pd.tooltip.network.broad" htmlEscape="true"/>help text finished">
                                        <spring:message code="pd.network.broad" htmlEscape="true"/>
                                    </a>
                                <@ } @>
                            <@ } @>
                        </td>
                    </tr>
                <@ } @>
                <!-- network end -->


                <!-- quality rating -->
                <@ if($("#showQualityRating").val() == 'YES') { @>
                    <tr>
                        <th class="cp-tile__label" scope="row">

                            <c:if test="${stateCode != 'ID'}">
                                <a
                                    href="#"
                                    class="gtm_tile_quality_rating"
                                    onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover', 'eventAction': '<spring:message code="pd.label.tooltip.qualityRatings.1"/> - Hover', 'eventLabel': '<@=name@>'});"
                                    rel="tooltip"
                                    role="tooltip"
                                    data-html="true"
                                    data-placement="right"
                                    data-original-title="<spring:message code="pd.label.qualityRating.tooltip" htmlEscape="false"/>" aria-label="<spring:message code="pd.label.tooltip.qualityRatings.1"/> help text:<spring:message code="pd.label.qualityRating.tooltip" htmlEscape="false"/> help text finished">
                                    <spring:message code="pd.label.tooltip.qualityRatings.1"/>
                                </a>
                            </c:if>

                            <c:if test="${stateCode == 'ID'}">
                                <spring:message code="pd.label.tooltip.qualityRatings.1"/>
                            </c:if>
                        </th>
                        <td class="cp-tile__value">
                            <!-- qualityRating values -->
                            <@ if (issuerQualityRating != null) { @>
                                <@= populateQualityRatingsHtml(issuerQualityRating)@>
                            <@ } else { @>
                                <c:if test="${stateCode == 'CA'}">
                                    <a
                                        data-placement="top"
                                        rel="tooltip"
                                        role="tooltip"
                                        href="#"
                                        data-original-title="<spring:message code="pd.label.tooltip.qualityRatings.9"/>"
                                        aria-label="<spring:message code="pd.label.tooltip.qualityRatings.9"/>">
                                        <spring:message code="pd.label.tooltip.qualityRatings.7"/>
                                    </a>
                                </c:if>

                                <c:if test="${stateCode != 'CA'}">
                                    <spring:message code="pd.label.tooltip.qualityRatings.7"/>
                                </c:if>

                                <@ } @>
                            <!-- qualityRating values ends -->
                        </td>
                    </tr>
                <@ } @>
                <!-- quality rating end -->


                <!-- benefits -->
                <c:forEach var="benefit" items="${benefitsArray}">
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <@ if(['${benefit}'] == "Pediatric dental"){ @>
                                <@ if($('#stateCode').val() == "MN"){ @>
                                <a
                                    href="#"
                                    data-original-title="<spring:message code="pd.label.commontext.benefits.pediatricDental.description" htmlEscape="true"/>"
                                    rel="tooltip"
                                    role="tooltip"
									data-placement="right"
                                    aria-label="<spring:message code="pd.label.commontext.benefits.pediatricDental.description" htmlEscape="true"/>">
                                    <spring:message code="pd.label.title.childrensDental" htmlEscape="true"/>
                                </a>
                                <@ } else {@>
                                    <spring:message code="pd.label.title.childrensDental" htmlEscape="true"/>
                                <@ } @>
                            <@ } else if(['${benefit}'] == "HSA Eligible"){ @>
						   		 <a
                                    href="#"
                                    data-original-title="<spring:message code="pd.label.title.hsaQualifiedtooltip" htmlEscape="true"/>"
                                    rel="tooltip"
                                    role="tooltip"
									data-placement="right"
                                    aria-label="<spring:message code="pd.label.title.hsaQualifiedtooltip" htmlEscape="true"/>">
									<spring:message code="pd.label.title.hsaEligible" htmlEscape="true"/>
                                </a>
						   <@ } else { @>
                                ${benefit}
                            <@ } @>
                        </th>
                        <td class="cp-tile__value">
                            <@ if(benefitsCoverage['${benefit}'] == "GOOD"){ @>
                                <a
                                    href="#"
                                    data-placement="top"
                                    data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.good" htmlEscape="true"/>"
                                    rel="tooltip"
                                    role="tooltip"
                                    aria-label="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.good" htmlEscape="true"/>">
                                    <i class="icon-circle icon-circle--good"></i>
                                </a>
                            <@ } else if(benefitsCoverage['${benefit}'] == "OK"){ @>
                                <a
                                    href="#"
                                    data-placement="top"
                                    data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.ok" htmlEscape="true"/>"
                                    rel="tooltip"
                                    role="tooltip"
                                    aria-label="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.ok" htmlEscape="true"/>">
                                    <i class="icon-circle icon-circle--ok"></i>
                                </a>
                            <@ } else if(benefitsCoverage['${benefit}'] == "POOR"){ @>
                                <a
                                    href="#"
                                    data-placement="top"
                                    data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.poor" htmlEscape="true"/>"
                                    rel="tooltip"
                                    role="tooltip"
                                    aria-label="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.poor" htmlEscape="true"/>">
                                    <i class="icon-circle icon-circle--poor"></i>
                                </a>
                            <@ } else { @>
                                <a
                                    href="#"
                                    data-placement="top"
                                    data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.na" htmlEscape="true"/>"
                                    rel="tooltip"
                                    role="tooltip"
                                    aria-label="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.na" htmlEscape="true"/>">
                                    <i class="icon-ban-circle"></i>
                                </a>
                            <@ } @>
                        </td>
                    </tr>
                </c:forEach>
                <!-- benefits end -->

                <!-- prescriptions -->
                <c:if test="${prescriptionSearchConfig=='ON'}">
                    <@ _.each (prescriptionResponseList, function (prescription) { @>
                        <tr>
                            <th class="cp-tile__label" scope="row">
                                <@ var drugName = prescription.drugName; @>
                                <@ var drugInfo = prescription.drugDosage; @>

                                <@ if(prescription.isGenericCovered === "Y" && (prescription.isDrugCovered === "N" || prescription.isDrugCovered === "U")){@>
                                    <@ drugInfo = prescription.genericDosage; @>
                                <@ } @>

                                <a
                                    href="#"
                                    data-placement="right"
                                    rel="tooltip"
                                    data-html="true"
                                    data-original-title="<@=drugInfo@>">
                                    <@ if (drugName.length > 19) {@>
                                        <@=drugName.substr(0, 19)@>...
                                    <@ } else { @>
                                        <@=drugName@>
                                    <@ } @>
                                </a>
                            </th>

                            <td class="cp-tile__value">
                                <@ if(prescription.isDrugCovered === "Y" || prescription.isGenericCovered === "Y"){ @>
									<@ var authRequired = prescription.authRequired; @>
									<@ var stepTherapyRequired = prescription.stepTherapyRequired; @>
									<@ if(prescription.isGenericCovered === "Y" && (prescription.isDrugCovered === "N" || prescription.isDrugCovered === "U")){ @>
										<@ authRequired = prescription.genericAuthRequired; @>
									 	<@ stepTherapyRequired = prescription.genericStepTherapyRequired; @>
									<@ } @>
                                    <a
                                        href="#"
                                        data-placement="top"
                                        data-original-title="<spring:message arguments="<@=authRequired@>,<@=stepTherapyRequired@>" code="pd.label.drugCovered"/>"
                                        rel="tooltip"
                                        role="tooltip"
                                        aria-label="<spring:message arguments="<@=authRequired@>,<@=stepTherapyRequired@>" code="pd.label.drugCovered"/>">
                                        <i class="icon-ok-sign"></i>
                                    </a>

                                    <@ if(prescription.isGenericCovered === "Y" && (prescription.isDrugCovered === "N" || prescription.isDrugCovered === "U")){ @>
                                        <a
                                            href="#"
                                            data-placement="top"
                                            data-original-title="<spring:message code="pd.label.generic.tooltip"/>"
                                            rel="tooltip"
                                            role="tooltip"
                                            aria-label="<spring:message code="pd.label.generic.tooltip"/>">
                                            <spring:message code="pd.label.generic"/>
                                        </a>
                                    <@ } @>
                                <@ } else { @>
                                    <@if(prescription.isDrugCovered === "U"){ @>
                                        <a
                                            href="#"
                                            data-placement="top"
                                            data-original-title="<spring:message code="pd.label.unknown"/>"
                                            rel="tooltip"
                                            role="tooltip"
                                            aria-label="<spring:message code="pd.label.unknown"/>">
                                            <i class='UNKNOWN'></i>
                                        </a>
                                    <@} else if(prescription.isDrugCovered === "N"){ @>
                                        <a
                                            href="#"
                                            data-placement="top"
                                            data-original-title="<spring:message code="pd.label.notCovered"/>"
                                            rel="tooltip"
                                            role="tooltip"
                                            aria-label="<spring:message code="pd.label.notCovered"/>">
                                            <i class='icon-ban-circle'></i>
                                        </a>
                                    <@}@>
                                <@}@>
                            </td>
                        </tr>
                    <@ }); @>
                </c:if>
                <!-- prescriptions end -->

                <!-- providers -->
                <c:if test="${providerSearchConfig=='ON'}">
                    <@ _.each (doctors, function (doctor) { @>
                        <tr>
                            <th class="cp-tile__label" scope="row">
                                <@ var providerType = doctor.providerType @>
                                <@ var docName = doctor.providerName; @>
                                <@ if (providerType == "doctor") { @>
                                    <@ if (docName.indexOf('Dr.') != 0 && $('#stateCode').val() == 'CA') { @>
                                        <@ docName = "<spring:message code="pd.label.doctor.title"/>" + " "+ docName @>
                                    <@ } @>

                                    <a
                                        href="#"
                                        data-placement="right"
                                        rel="tooltip" role="tooltip"
                                        data-original-title="<@=docName@>"
                                        aria-label="<@=docName@>">
                                        <@ if (docName.length > 15) { @>
                                            <@=docName.substr(0, 15)@>...
                                        <@ } else { @>
                                            <@=docName@>
                                        <@ } @>
                                    </a>
                                <@ } else if (providerType == "dentist") { @>
                                    <@ docName =  " " + docName + " "@>

                                    <a
                                        href="#"
                                        class="cp-tooltip__icon-link"
                                        data-placement="right"
                                        rel="tooltip" role="tooltip"
                                        data-original-title="<@=docName@>"
                                        aria-label="<@=docName@>">
                                        <@ if (docName.length > 15) {@>
                                            <@=docName.substr(0, 15)@>...
                                        <@ } else { @>
                                            <@=docName@>
                                        <@ } @>
                                    </a>
                                <@ } else { @>
                                    <a
                                        href="#"
                                        class="cp-tooltip__icon-link"
                                        data-placement="right"
                                        rel="tooltip" role="tooltip"
                                        data-original-title="<@=docName@>">
                                        <@ if (docName.length > 15) { @>
                                            <@=docName.substr(0, 15)@>...
                                        <@ } else { @>
                                            <@=docName@>
                                        <@ } @>
                                    </a>
                                <@ } @>

                            </th>

                            <td class="cp-tile__value">
                                <@ if(doctor.networkStatus== "outNetWork"){ @>
                                    <a
                                        href="#"
										data-delay='{"hide": "2000"}'
                                        data-placement="top"
										data-html="true"
                                        data-original-title="<spring:message code="pd.label.outNetwork"/>"
                                        rel="tooltip"
                                        role="tooltip"
                                        aria-label="<spring:message code="pd.label.outNetwork"/>">
                                        <i class="icon-ban-circle"></i>
                                    </a>
                                <@ } else if(doctor.networkStatus == "inNetWork"){ @>
                                    <a
                                        href="#"
                                        data-placement="top"
                                        data-original-title="<spring:message code="pd.label.availableInNetwork"/>"
                                        rel="tooltip"
                                        role="tooltip"
                                        aria-label="<spring:message code="pd.label.availableInNetwork"/>">
                                        <i class="icon-ok-sign"></i>
                                    </a>
                                <@ } else { @>
                                    <a
                                        href="#"
                                        data-placement="top"
                                        data-original-title="<spring:message code="pd.label.availablilityUnknown"/>"
                                        rel="tooltip"
                                        role="tooltip"
                                        aria-label="<spring:message code="pd.label.availablilityUnknown"/>">
                                        <i class="icon-question-sign"></i>
                                    </a>
                                <@ } @>
                            </td>
                        </tr>
                    <@ }); @>
                </c:if>
                <!-- providers end -->

            </table>
        </div>
        <!-- tile body ends -->

        <!-- tile footer -->
        <div class="cp-tile__footer">

                <label for="compareChk_<@=id@>" class="cp-tile__item u-hide-xs" title="<spring:message code="pd.label.title.compare"/>">
                    <input
                    type="checkbox"
                    class="ps-form-check-input u-margin-top-0 u-margin-right-5 compare gtm_tile_compare"
                    id="compareChk_<@=id@>" data-plan-id="<@=planId@>">
                    <spring:message code="pd.label.title.compare" htmlEscape="true"/>
                </label>

                <a href="#" id="detail_<@=id @>" data-plan-id="<@=planId@>" class="detail cp-tile__item gtm_detail" title="<spring:message code="pd.label.title.details"/>">
                    <spring:message code="pd.label.title.details"/>
                </a>

            <@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
				<a href="#" id="cart_<@=planId@>" class="cp-tile__item cp-tile__add addToCart gtm_add_to_cart" title="<spring:message code="pd.label.title.add"/>">
                    <spring:message code="pd.label.title.add"/>
                    <i class="icon-shopping-cart"></i>
                </a>
            <@ } @>
		</div>
        <!-- tile footer ends -->
  <!-- .tile-header-->

  <!-- .plan-options -->
  <!-- <div class="plan-actions">
    <div class="pull-left input-append">
      <label class="addToCompareLbl checkbox inline btn btn-small" for="compareChk_<@=id@>" id="compareLbl_<@=id@>">
        <input type="checkbox" class="addToCompare addtofav comparechk ie10" id="compareChk_<@=id@>">
		<a href="#" title="<spring:message code='pd.label.title.compare'/>" class="addToCompare addtofav btn btn-small" id="compare_<@=id@>"><spring:message code="pd.label.title.compare" htmlEscape="true"/></a>
      </label>
      <label class="checkbox inline btn btn-small btn-primary comparing" for="removeChk_<@=id@>" style="display:none;" id="removeLbl_<@=id@>">
        <input type="checkbox" class="removeCompare addtofav addedToFav comparechk ie10" id="removeChk_<@=id@>">
  <a class="removeCompare addtofav addedToFav btn btn-small ie10" href="#" title="<spring:message code='pd.label.title.compare'/>"id="remove_<@=id@>" style="display:none;" ><spring:message code="pd.label.title.compare" htmlEscape="true"/></a>
      </label>

    </div>
	<div class="pull-right input-append btn-small">
   		 <a class="detail btn btn-small ie10 view-detail" href="#" title="<spring:message code='pd.label.title.viewDetail'/>" id="detail_<@=id@>"><spring:message code="pd.label.title.viewDetail" htmlEscape="true"/></a>
	</div>
  </div> -->
</div>
<!-- tile ends -->
</script>
<%-- <script>
// make dynamic tooltip text readable by JAWS
	$(document).ready(function () {
		$('body').on('change', '.plantype_filter_google_tracking', function(){
			if($(this).prop('checked')){
				triggerGoogleTrackingEvent(['_trackEvent', 'Left Nav Filters', 'check', 'Plan Type ga- ' + $(this).val()]);
			}
		});

	});
</script> --%>
