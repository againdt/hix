<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>

<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/issuer/market/individual/profile/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
       		<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
            <li><spring:message  code="pgheader.update.shopMarketProfile"/></li>
        </ul><!--page-breadcrumb ends-->

        <h1><img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img"/>${issuerObj.name}</h1>
    </div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>"><spring:message  code="pgheader.issuerDetails"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>"><spring:message  code="pgheader.issuerRepresentative"/></a></li>
                    <c:if test="${exchangeState != 'ID' && exchangeState != 'MN'}">
                    	<li><a href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>"><spring:message  code="pgheader.financialInformation"/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>"><spring:message  code="pgheader.update.companyProfile"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/market/individual/profile/${encIssuerId}"/>"><spring:message  code="pgheader.individualMarketProfile"/></a></li>
                    <c:if test="${exchangeState != 'ID'}">
                    	<li class="active"><a href="#"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/admin/issuer/accreditationdocument/view/${encIssuerId}" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/certification/status/${encIssuerId}"/>"><spring:message  code="pgheader.certificationStatus"/></a></li>
    				<%-- <li><a href="<c:url value="/admin/issuer/partner/list/${issuerObj.id}"/>"><spring:message  code="pgheader.partners"/></a></li> --%>
                    <li><a href="<c:url value="/admin/issuer/history/${encIssuerId}"/>"><spring:message  code="pgheader.issuerHistory"/></a></li>
                       <li><a href="<c:url value="/admin/issuer/crossWalkStatus/${encIssuerId}"/>"><spring:message  code="pgheader.crossWalkStatus"/></a></li>
                   	<c:if test="${exchangeState == 'ID'}">
                       <li><a href="<c:url value="/admin/issuer/displayNetworkTransparencyData/${encIssuerId}"/>"><spring:message code="pgheader.networkTransparencyStatus" /></a></li>
                   	</c:if>
        		    <c:if test="${activeRoleName == 'OPERATIONS'}">
						<li><a href="<c:url value="/admin/issuer/issuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="pgheader.IssuerPaymentInfo"/></a></li>
					</c:if>
                      
                </ul>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.update.shopMarketProfile"/></h4>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/market/shop/profile/edit/${encIssuerId}"/>"><spring:message  code="label.edit"/></a>
                </div>
				<div class="gutter10">
		
				<form class="form-horizontal" id="frmIssuerShopMarktProfInfo" name="frmIssuerShopMarktProfInfo" action="" method="POST">
				
				<table class="table table-border-none">
					<tbody>
						<tr>
							<th class="txt-right span4">
								<spring:message  code="label.customerServicePhone"/>
							</th>
							<td>
								<c:set var="custServPhone" value="${issuerObj.shopCustServicePhone}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServPhone"))%></strong>  
								&nbsp; &nbsp;
								<spring:message  code="label.phone.ext"/>&nbsp;
								<strong>${issuerObj.shopCustServicePhoneExt}</strong>
							</td>
						</tr>
						<tr>
							<th class="txt-right span4">
								<spring:message  code="label.customerServiceTollFreeNumber"/>
							</th>
							<td>
								<c:set var="custTollFree" value="${issuerObj.shopCustServiceTollFree}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custTollFree"))%></strong>
							</td>
						</tr>
						<tr>
							<th class="txt-right span4">
								<spring:message  code="label.customerServiceTTY"/>
							</th>
							<td>
								<c:set var="custServTTY" value="${issuerObj.shopCustServiceTTY}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServTTY"))%></strong>
							</td>
						</tr>
						<tr>
							<th class="txt-right span4">
								<spring:message  code="label.consumerFacingWebSiteURL"/>
							</th>
							<td>
								<a href="${issuerObj.shopSiteUrl}" target="_blank"><strong>${issuerObj.shopSiteUrl}</strong></a>
							</td>
						</tr>
					</tbody>
				</table>			
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>

<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script> 
