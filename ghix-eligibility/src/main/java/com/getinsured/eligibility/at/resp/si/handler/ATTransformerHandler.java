package com.getinsured.eligibility.at.resp.si.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.transform.ERPTransformer;
import com.getinsured.eligibility.at.resp.si.transform.FetchData;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeType;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

/**
 * MemberMatchHandler to compare stored ssapApplicants with the received state's response.
 *
 * @author Ekram
 *
 */
@Component("aTTransformerHandler")
public class ATTransformerHandler {

	private static final Logger LOGGER = Logger.getLogger(ATTransformerHandler.class);

	@Autowired private ERPTransformer eRPTransformer;
	@Autowired private FetchData fetchData;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;


	public String processERP(AccountTransferRequestDTO request){

		Map<String, Object> resultMap = new HashMap<>();
		ERPResponse erpResponse = new ERPResponse();	/* safety for parsing json later.. */
		try {
			erpResponse = eRPTransformer.transform(request);
			erpResponse = fetchData.fetchData(erpResponse);
			resultMap.put(EligibilityConstants.AT_TRANSFORMER_RESULT, EligibilityConstants.AUTO);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID).
					append("erpResponse.getApplicationID()").append(EligibilityConstants.REASON).append(e.getMessage());

			LOGGER.error(errorReason.toString());
			resultMap.put(EligibilityConstants.AT_TRANSFORMER_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERP_RESPONSE, null);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());

		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);
	}


}
