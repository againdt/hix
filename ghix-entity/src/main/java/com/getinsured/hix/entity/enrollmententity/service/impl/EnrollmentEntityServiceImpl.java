package com.getinsured.hix.entity.enrollmententity.service.impl;

import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.COMMENTS;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.COMMENT_COL;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.DATE;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.DOCUMENTID_EE;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.FILE_NAME;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.MODEL_NAME;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.NEW_STATUS;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.PREVIOUS_STATUS;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.REMOVE;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.REPOSITORY_NAME;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.STATUS_COL;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.UPDATED_DATE_COL;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.UPDATE_DATE;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.entity.EnrollmentEntityAssisterSearchResult;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.entity.admin.repository.IEntityDocumentRepository;
import com.getinsured.hix.entity.admin.service.EntityIndTriggerService;
import com.getinsured.hix.entity.assister.repository.IAssisterRepository;
import com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl;
import com.getinsured.hix.entity.enrollmententity.repository.IEnrollmentEntityRepository;
import com.getinsured.hix.entity.enrollmententity.repository.IEntityNumberRepository;
import com.getinsured.hix.entity.enrollmententity.service.EnrollmentEntityService;
import com.getinsured.hix.entity.enrollmententity.service.SiteLocationHoursService;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.EntityLocation;
import com.getinsured.hix.model.entity.EntityNumber;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;
import com.getinsured.hix.model.entity.SiteLocationHours;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.thoughtworks.xstream.XStream;

/**
 * Implements {@link EnrollmentEntityService} to perform Enrollment Entity
 * related operations like find,save,update etc.
 * @param <E>
 * @param <E>
 * 
 */
@Service("enrollmentEntityService")
@Transactional
@Repository
public class EnrollmentEntityServiceImpl implements EnrollmentEntityService, ApplicationContextAware {
	private static final String MM_DD_YY_1 = ",'mm-dd-yy') + 1";

	private static final String FROM = "FROM";

	private static final String STATUS = "status";

	private static final String WHERE = "WHERE";

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEntityServiceImpl.class);
	
	@Autowired
	EntityIndTriggerService entityIndTriggerService;

	@Autowired
	private IAssisterRepository assisterRepository;

	@Autowired
	private IEnrollmentEntityRepository enrollmentEntityRepository;

	@Autowired
	private ObjectFactory<DisplayAuditService> objectFactory;

	@Autowired
	private EnrollmentEntityHistoryRendererImpl enrollmentEntityHistoryRendererImpl;

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private IEntityNumberRepository entityNumberRepository;

	@Autowired
	private IEntityDocumentRepository documentRepository;

	@Autowired
	private ContentManagementService ecmService;

	@Autowired
	private SiteLocationHoursService siteLocationHoursService;
	
	@Autowired
	private ZipCodeService zipCodeService;
	
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;

	private static final String DATE_STRING = "Date";
	private static final String NEW_CERTIFICATION_STATUS = "New Status";
	private static final String VIEW_COMMENT = "View Comment";
	private static final String DOCUMENT_ID_ENTITY = "documentIdEE";
	private static final int ONE = 1;
	private static final int TWO = 2;
	private static final int THREE = 3;
	private static final int FOUR = 4;
	private static final int FIVE = 5;
	private static final int SIX = 6;
	private static final int SEVEN = 7;
	private static final int EIGHT = 8;
	private static final int NINE = 9;
	private static final int TEN = 10;
	private static final int ELEVEN = 11;
	private static final int TWELVE = 12;
	private static final int THIRTEEN = 13;
	private static final int FORTEEN = 14;
	private static final int FIFTEEN = 15;
	private static final int SIXTEEN = 16;
	private static final int SEVENTEEN = 17;
	private static final int EIGHTEEN = 18;
	private static final String ASSISTERS = "assisters";
	private static final int DISTANCE_FOR_OPTION_ANY = 4000;
	public static final String LANGUAGES_SPOKEN = "languageSpoken_";
	public static final String REGISTRATION_STATUS = "registrationStatus";
	public static final String DISTANCE = "distance";
	public static final String ZIP = "zip";
	public static final String ZIP_SELECT = "zip1";
	private static final String LATTITUDE_CONST = "lattitude";
	private static final String LONGITUDE_CONST = "longitude";
	private static final String PERCENTAGE = "%";
	private static final String ENTITY_NAME = "entityName";
	private static final String ENTITY_NUMBER = "entityNumber";
	private static final String FROM_DATE = "fromDate";
	private static final String TO_DATE = "toDate";
	private static final String START_LINE = "startLine";
	private static final String END_RECORD= "endRecord";
	private static final String TOTAL_RECORDS = "totalRecords";
	private static final String ENTITYID = "entityId";
	private static final String SITEID = "siteId";
	private static final String CECEMAILADDRESS = "cecEmailAddress";
	private static final String CECFIRSTORLASTNAME = "cecFirstOrLastName";
	private static final String STARTPAGE = "startPage";
	private static final String PRIMARYSITE = "primarySite";
	private static final String MODULEID = "moduleId";
	private static final double STATE_WIDTH_FACTOR = 4;
	private static final double MILES_FACTOR = 69;


	private static final String ORACLE = "ORACLE";

	private static final String LIKE = "LIKE";
	@Value("#{configProp['ecm.type']}")
	private String ecmType;
	@Autowired
	private ObjectFactory<QueryBuilder<EnrollmentEntity>> delegateFactory;

	private ApplicationContext appContext;
	
	

	/**
	 * @see EnrollmentEntityService#findEnrollmentEntityByUserId(int)
	 */
	@Override
	@Transactional(readOnly = true)
	public EnrollmentEntity findEnrollmentEntityByUserId(int userid) {
		LOGGER.info("findEnrollmentEntityByUserId: START");
		return enrollmentEntityRepository.findEnrollmentEntityByUserId(userid);
	}

	@Override
	public EnrollmentEntity saveEnrollmentEntity(EnrollmentEntity enrollmentEntity) {

		LOGGER.info("saveEnrollmentEntity: START");

		// Generate Entity Number if does not exist
		if (enrollmentEntity != null && enrollmentEntity.getId() != 0 && (enrollmentEntity.getEntityNumber() == null || enrollmentEntity.getEntityNumber() == 0)) {
			generateEntityNumber(enrollmentEntity);
		}
		EnrollmentEntity existingEnrollmentEntity= enrollmentEntityRepository.saveAndFlush(enrollmentEntity);
		
		if (!EntityConstants.STATUS_INCOMPLETE.equalsIgnoreCase(existingEnrollmentEntity.getRegistrationStatus())) {
			entityIndTriggerService.triggerInd35(existingEnrollmentEntity);
		}
		
		
		return existingEnrollmentEntity;
	}

	/**
	 * Retrieves {@link EntityNumber} if exists, else generate entity number and
	 * set into {@link EnrollmentEntity} object
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 */
	private void generateEntityNumber(EnrollmentEntity enrollmentEntity) {

		LOGGER.info("generateEntityNumber: START");

		EntityNumber entityNumberObj = entityNumberRepository.findByEntityId(enrollmentEntity.getId());
		if (entityNumberObj == null) {
			LOGGER.info("Generating unique entity number for Enrollment Entity");
			EntityNumber entityNumber = new EntityNumber();
			entityNumber.setEntityId(enrollmentEntity.getId());
			entityNumberObj = entityNumberRepository.saveAndFlush(entityNumber);
		}
		enrollmentEntity.setEntityNumber(new Long(entityNumberObj.getId()));

		LOGGER.info("generateEntityNumber: END");
	}

	@Override
	public EnrollmentEntity getEnrollmentEntityByRecordID(int id) {

		LOGGER.info("generateEntityNumber: START");
		LOGGER.info("generateEntityNumber: END");

		return enrollmentEntityRepository.findById(id);
	}

	/**
	 * @see EntityrService#findPaymentMethodDetailsForEntity(int, String)
	 */
	@Override
	@Transactional(readOnly = true)
	public PaymentMethods findPaymentMethodDetailsForEntity(int moduleId, String entityModule) {

		LOGGER.info("findPaymentMethodDetailsForEntity: START");

		LOGGER.info("Get PaymentMethodDetails For Entity");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
		paymentMethodRequestDTO.setModuleID(moduleId);
		paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ASSISTERENROLLMENTENTITY);
		paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.Active);
		paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentIsDefault.Y);
		LOGGER.info("Retrieving Payment Detail REST Call Starts.");
		String paymentMethodsStr = ghixRestServiceInvoker.invokeRestService(paymentMethodRequestDTO, FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
		LOGGER.info("Retrieving Payment Detail REST Call Ends.");

		PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse) xstream
				.fromXML(paymentMethodsStr);
		PaymentMethods paymentMethods = null;
		Map<String, Object> paymentMethodMap = paymentMethodResponse
				.getPaymentMethodMap();
		if (paymentMethodMap != null
				&& paymentMethodMap
						.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY) != null) {
			List<PaymentMethods> paymentMethodList = (List<PaymentMethods>) paymentMethodMap
					.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
			paymentMethods = paymentMethodList != null ? paymentMethodList
					.get(0) : null;
		}

		LOGGER.info("findPaymentMethodDetailsForEntity: END");

		return paymentMethods;
	}

	/**
	 * @see EntityService#loadEntityrStatusHistory(entityId)
	 */

	@Override
	public List<Map<String, Object>> loadEntityrStatusHistory(Integer entityId) {

		LOGGER.info("loadEntityrStatusHistory: START");

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(STATUS_COL, NEW_CERTIFICATION_STATUS);
		requiredFieldsMap.put(COMMENT_COL, VIEW_COMMENT);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(STATUS_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(UPDATE_DATE);
		displayCols.add(PREVIOUS_STATUS);
		displayCols.add(NEW_STATUS);
		displayCols.add(COMMENTS);

		List<Map<String, Object>> data = null;
		try {
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(this.appContext);
			List<Map<String, String>> entityHistoryData = service.findRevisions(REPOSITORY_NAME, MODEL_NAME,
					requiredFieldsMap, entityId);
			if (entityHistoryData != null && !entityHistoryData.isEmpty()) {
				data = enrollmentEntityHistoryRendererImpl.processData(entityHistoryData, compareCols, displayCols);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred while loading Entity Status History", e);
		}

		LOGGER.info("loadEntityrStatusHistory: END");

		return data;
	}

	/**
	 * @see EnrollmentEntityService#findEnrollmentEntityById(int)
	 */
	@Override
	public EnrollmentEntity findEnrollmentEntityById(int entityId) {

		LOGGER.info("findEnrollmentEntityById: START");
		LOGGER.info("findEnrollmentEntityById: END");

		return enrollmentEntityRepository.findById(entityId);
	}

	/**
	 * @see EnrollmentEntityService#findEnrollmentEntity(EnrollmentEntity,
	 *      Integer, Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> findEnrollmentEntity(EnrollmentEntity enrollmentEntity, Integer startRecord,
			Integer pageSize) {

		LOGGER.info("findEnrollmentEntity: START");

		QueryBuilder<EnrollmentEntity> enrollmentEntityQuery = delegateFactory.getObject();

		enrollmentEntityQuery.buildObjectQuery(EnrollmentEntity.class);
				
		if(enrollmentEntity != null) {
			applyWhereToQuery(enrollmentEntityQuery, ENTITY_NAME, enrollmentEntity.getEntityName(), DataType.STRING, ComparisonType.LIKE);
			applyWhereToQuery(enrollmentEntityQuery, REGISTRATION_STATUS, enrollmentEntity.getRegistrationStatus(), DataType.STRING, ComparisonType.STARTWITH);
			applyWhereToQuery(enrollmentEntityQuery, "organizationType", enrollmentEntity.getOrgType(), DataType.STRING, ComparisonType.LIKE);
			applyWhereToQuery(enrollmentEntityQuery, "receivedPayments", enrollmentEntity.getReceivedPayments(), DataType.STRING, ComparisonType.LIKE);
		}
				
		List<EnrollmentEntity> enrollmentEntitys = enrollmentEntityQuery.getRecords(startRecord, pageSize);
		Map<EnrollmentEntity, Integer> eentityWithAssisterCount = new HashMap<EnrollmentEntity, Integer>();
		int assisterCount = 0;
		for (EnrollmentEntity enEntity : enrollmentEntitys) {
			assisterCount = assisterRepository.findByEntity(enEntity).size();
			eentityWithAssisterCount.put(enEntity, assisterCount);
		}
		Map<String, Object> eentityListAndRecordCount = new HashMap<String, Object>();
		eentityListAndRecordCount.put("enrollmententitymap", eentityWithAssisterCount);
		eentityListAndRecordCount.put("recordCount", enrollmentEntityQuery.getRecordCount());

		LOGGER.info("findEnrollmentEntity: END");

		return eentityListAndRecordCount;
	}

	/**
	 * @param enrollmentEntityQuery
	 * @param value
	 */
	private void applyWhereToQuery(QueryBuilder<EnrollmentEntity> enrollmentEntityQuery, String fieldName, String fieldValue, DataType dataType, ComparisonType comparisonType) {
		if (fieldValue != null && fieldValue.length() > 0) {
			enrollmentEntityQuery.applyWhere(fieldName, fieldValue.toUpperCase(), dataType, comparisonType);
		}
	}

	/**
	 * @see EnrollmentEntityService#searchEnrollmentEntity(EnrollmentEntityRequestDTO)
	 * 
	 */
	@Override
	public Map<String, Object> searchEnrollmentEntity(EnrollmentEntityRequestDTO entityRequestDTO, boolean totalFlag) {

		LOGGER.info("searchEnrollmentEntity: START");

		StringBuilder query = new StringBuilder();
		EntityManager em = null;
		Map<EnrollmentEntity, Integer> eentityWithAssisterCount = new LinkedHashMap<EnrollmentEntity, Integer>();
		Map<String, Object> eentityListAndRecordCount = null;
		try{
			eentityListAndRecordCount = new HashMap<String, Object>();

			em = entityManagerFactory.createEntityManager(); 

			//EnrollmentEntity enrollmentEntity = entityRequestDTO.getEnrollmentEntity();

			String zip = entityRequestDTO.getZipCode();
			String language = entityRequestDTO.getLanguage();
			Integer startRecord = entityRequestDTO.getStartRecord();
			Integer startLine = startRecord + 1;
			//Integer endRecord = startRecord + entityRequestDTO.getPageSize();
			String sortBy = entityRequestDTO.getSortBy();
			String sortOrder = entityRequestDTO.getSortOrder();

//			query.append("SELECT ID,USER_ID,ENTITY_NAME,REGISTRATION_STATUS,REGISTRATION_RENEWAL_DATE,LINE_NUMBER "
//					+ "FROM (SELECT E.ID,E.USER_ID,E.ENTITY_NAME,E.REGISTRATION_STATUS,E.REGISTRATION_RENEWAL_DATE,"
//					+ "ROW_NUMBER() OVER (ORDER BY E.ID ASC) LINE_NUMBER "
//					+ "FROM EE_ENTITIES E,USERS U WHERE E.USER_ID = U.ID ");
			
			query.append("SELECT ID,USER_ID,ENTITY_NAME,ENTITY_NUMBER,REGISTRATION_STATUS,REGISTRATION_RENEWAL_DATE, assisters, LINE_NUMBER ");
			
			query.append( " FROM ( SELECT ID,USER_ID,ENTITY_NAME,ENTITY_NUMBER,REGISTRATION_STATUS,REGISTRATION_RENEWAL_DATE, assisters, ROW_NUMBER() OVER (ORDER BY " + sortBy + " " + sortOrder + ") as LINE_NUMBER ");
			
			query.append( " FROM (SELECT E.ID,E.USER_ID,E.ENTITY_NAME,E.ENTITY_NUMBER,E.REGISTRATION_STATUS,E.REGISTRATION_RENEWAL_DATE, ");
			
			if (sortBy.equalsIgnoreCase(ASSISTERS))
			{
				query.append("(SELECT COUNT(*) FROM EE_ASSISTERS A WHERE A.EE_ENTITY_ID = E.ID) ");
			}
			else
			{
				query.append(" 0 ");
			}
			query.append(" as assisters ");
			query.append(" FROM EE_ENTITIES E,USERS U WHERE E.USER_ID = U.ID ");

			addConditionsToEntityQuery(entityRequestDTO, query, zip, language);
			
			query.append(" ) SUBQ8 ");
			
			query.append(" ) SUBQ9 WHERE LINE_NUMBER BETWEEN :" + START_LINE + " AND :" + END_RECORD);
			Query pQuery = em.createNativeQuery(query.toString());
			setParameterizedQuery(entityRequestDTO,pQuery);
			List list = pQuery.getResultList();
			if (sortBy.equalsIgnoreCase(ASSISTERS))
			{
				createEnrollmentEntityWithAssisterCountMap(list, eentityWithAssisterCount);
			}
			else
			{
				List<EnrollmentEntity> entities = createEnrollmentEntityList(list);

				for (EnrollmentEntity entity : entities) {
					eentityWithAssisterCount.put(entity, findAssistersForEnrollmentEntity(entity.getId()));
				}
			}

			eentityListAndRecordCount.put("enrollmententitymap", eentityWithAssisterCount);
			eentityListAndRecordCount.put("recordCount", (startLine == 1 && eentityWithAssisterCount.size() < entityRequestDTO.getPageSize()) ?  eentityWithAssisterCount.size() : getEnrollmentEntityTotalCount(entityRequestDTO));
			
		}catch(Exception e){
			LOGGER.error("Exception occurred while loading enrollment entities", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}
		
		LOGGER.info("searchEnrollmentEntity: END");

		return eentityListAndRecordCount;
	}
	
	private Query setParameterizedQuery(
			EnrollmentEntityRequestDTO entityRequestDTO, Query pQuery) {
		LOGGER.info("setParameterizedQuery: START");
		Integer startRecord = entityRequestDTO.getStartRecord();
		Integer startLine = startRecord + 1;
		Integer endRecord = startRecord + entityRequestDTO.getPageSize();
		setParameterizedQueryForSearchEnrollmentEntity(entityRequestDTO,pQuery);
		
		pQuery.setParameter(START_LINE,startLine);
		pQuery.setParameter(END_RECORD, endRecord);
		LOGGER.info("setParameterizedQuery: END");
		return pQuery;
		
	}

	private Query setParameterizedQueryForSearchEnrollmentEntity(EnrollmentEntityRequestDTO entityRequestDTO,Query pQuery) {
	
		LOGGER.info("setParameterizedQueryForSearchEnrollmentEntity: START");

		EntityUtils.setQueryParameter(pQuery, ENTITY_NAME, entityRequestDTO.getEntityName(), true);
		EntityUtils.setQueryParameter(pQuery, ENTITY_NUMBER, entityRequestDTO.getEntityNumber(), false);
		EntityUtils.setQueryParameter(pQuery, "language", entityRequestDTO.getLanguage(), true);
		EntityUtils.setQueryParameter(pQuery, REGISTRATION_STATUS, entityRequestDTO.getStatus(), false);
		EntityUtils.setQueryParameter(pQuery, "organisationType", entityRequestDTO.getOrganizationType(), true);
		EntityUtils.setQueryParameter(pQuery, "receivedPayments", entityRequestDTO.getReceivedPayments(), false);
		EntityUtils.setQueryParameter(pQuery, "countyServed", entityRequestDTO.getCountyServed(), true);
		EntityUtils.setQueryParameter(pQuery, FROM_DATE, entityRequestDTO.getFromDate(), false);
		EntityUtils.setQueryParameter(pQuery, TO_DATE, entityRequestDTO.getToDate(), false);
		String zip = entityRequestDTO.getZipCode();
		
		if (checkZipCode(zip)) {
			pQuery.setParameter("zip", zip);
		}
		
		LOGGER.info("setParameterizedQueryForSearchEnrollmentEntity: END");
		return pQuery;
	}
	
	/**
	 * method to get Enrollment Entity Total Count based on search criteria
	 * @param entityRequestDTO
	 * @return
	 */
	private int getEnrollmentEntityTotalCount(EnrollmentEntityRequestDTO entityRequestDTO) {

		LOGGER.info("getEnrollmentEntityTotalCount: START");

		StringBuilder query = new StringBuilder();
		List<?> totalCount = null;
		EntityManager em = null; 
		try{
		em = entityManagerFactory.createEntityManager(); 
		String zip = entityRequestDTO.getZipCode();
		String language = entityRequestDTO.getLanguage();

		query.append(" SELECT count(*)");
		query.append(" FROM EE_ENTITIES E,USERS U WHERE E.USER_ID = U.ID ");

		addConditionsToEntityQuery(entityRequestDTO, query, zip, language);
		
		Query pQuery =  em.createNativeQuery(query.toString());
		setParameterizedQueryForSearchEnrollmentEntity(entityRequestDTO,pQuery);
		totalCount = pQuery.getResultList();
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		LOGGER.info("getEnrollmentEntityTotalCount: END");

		return totalCount != null && (!totalCount.isEmpty())  ? Integer.parseInt(totalCount.get(0) + "") : 0;
	}

	/**
	 * @param entityRequestDTO
	 * @param query
	 * @param zip
	 * @param language
	 */
	private void addConditionsToEntityQuery(
			EnrollmentEntityRequestDTO entityRequestDTO, StringBuilder query,
			String zip, String language) {
		EntityUtils.appendToSearchAssisterQuery(query, "E.ENTITY_NAME", entityRequestDTO.getEntityName(), ENTITY_NAME, LIKE, true, false);
		EntityUtils.appendToSearchAssisterQuery(query, "E.ENTITY_NUMBER", entityRequestDTO.getEntityNumber(), ENTITY_NUMBER, LIKE, true, false);
		
		if (language != null && !"".equals(language)) {
			query.insert(query.lastIndexOf(FROM) + FROM.length(), " EE_POPULATION_LANGUAGES P, ");
			query.insert(query.indexOf(WHERE) + WHERE.length(), " E.ID=P.EE_ENTITY_ID AND ");
			query.append(" AND UPPER(P.GI_LANGUAGE) LIKE UPPER(:language)");
		}

		EntityUtils.appendToSearchAssisterQuery(query, "E.REGISTRATION_STATUS", entityRequestDTO.getStatus(), "registrationStatus", "=", false, false);
		EntityUtils.appendToSearchAssisterQuery(query, "E.ORG_TYPE", entityRequestDTO.getOrganizationType(), "organisationType", LIKE, true, false);

		if (checkZipCode(zip)) {
			// Added table EE_SITES and updated where condition as part of issue HIX-15045
			query.insert(query.lastIndexOf(FROM) + FROM.length(), " EE_LOCATIONS L,  EE_SITES S, ");
			// query.insert(query.indexOf("WHERE") + "WHERE".length(),
			// " E.MAILING_LOCATION_ID = L.ID AND ");
			query.append(" AND E.Id = S.EE_ENTITY_ID AND S.SITE_TYPE = '" + Site.site_type.PRIMARY.toString() + "' AND S.MAILING_LOCATION_ID = L.ID AND L.ZIP LIKE (:zip) ");
		}

		EntityUtils.appendToSearchAssisterQuery(query, "E.REGISTRATION_RENEWAL_DATE", entityRequestDTO.getFromDate(), FROM_DATE, ">=", false, true);
		EntityUtils.appendToSearchAssisterQuery(query, "E.REGISTRATION_RENEWAL_DATE", entityRequestDTO.getToDate(), TO_DATE, "<=", false, true,true);
		EntityUtils.appendToSearchAssisterQuery(query, "E.RECEIVED_PAYMENTS", entityRequestDTO.getReceivedPayments(), "receivedPayments", "IN", false, false);
		EntityUtils.appendToSearchAssisterQuery(query, "E.COUNTY_SERVED", entityRequestDTO.getCountyServed(), "countyServed", LIKE, true, false);
	}
	
	/**
	 * 
	 * method to get Enrollment Entity map with assister count
	 * @param list
	 * @param eentityWithAssisterCount
	 */
	private void createEnrollmentEntityWithAssisterCountMap(List list, Map<EnrollmentEntity, Integer> eentityWithAssisterCount) {

		LOGGER.info("createEnrollmentEntityWithAssisterCountMap: START");

		//List<EnrollmentEntity> enrollmentEntities = new ArrayList<EnrollmentEntity>();
		EnrollmentEntity entity = null;

		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			entity = new EnrollmentEntity();
			Object[] objArray = (Object[]) iterator.next();

			if (objArray[0] != null) {
				entity.setId(Integer.valueOf(objArray[0].toString()));
			}

			if (objArray[1] != null) {
				if (entity.getUser() != null) {
					entity.getUser().setId(Integer.valueOf(objArray[1].toString()));
				} else {
					AccountUser user = new AccountUser();
					user.setId(Integer.valueOf(objArray[1].toString()));
					entity.setUser(user);
				}
			}

			if (objArray[2] != null) {
				entity.setEntityName(objArray[2].toString());
			}
			String entityNum = objArray[THREE] != null ? objArray[THREE].toString() : "0";
			Long entityNumber = (entityNum!=null || !entityNum.isEmpty())? Long.valueOf(entityNum) : 0;
			entity.setEntityNumber(entityNumber);
			if (objArray[FOUR] != null) {
				entity.setRegistrationStatus(objArray[FOUR].toString());
			}
			if (objArray[FIVE] != null) {
				try {
					entity.setRegistrationRenewalDate(EntityUtils.formatDate(new SimpleDateFormat(
							EntityConstants.DATE_FORMAT).format(new SimpleDateFormat(EntityConstants.DATE_TIME_FORMAT)
							.parse(objArray[FIVE].toString()))));
				} catch (Exception exception) {
					LOGGER.error("Error while formatting registration renewal date : ", exception);
				}
			}
			eentityWithAssisterCount.put(entity, objArray[SIX] != null ? Integer.valueOf(objArray[SIX].toString()) : 0);
		}

		LOGGER.info("createEnrollmentEntityWithAssisterCountMap: END");
		
	}

	/**
	 * @see EnrollmentEntityService#searchEnrollmentEntity(EnrollmentEntityRequestDTO)
	 * 
	 */
	@Override
	public int searchEnrollmentEntity(EnrollmentEntityRequestDTO entityRequestDTO) {

		LOGGER.info("searchEnrollmentEntity: START");

		StringBuilder query = new StringBuilder();

		EntityManager em = null;
		List list = null;
		try{
			em = entityManagerFactory.createEntityManager();

			String zip = entityRequestDTO.getZipCode();
			String language = entityRequestDTO.getLanguage();


			query.append("SELECT ID,USER_ID,ENTITY_NAME,ENTITY_NUMBER,REGISTRATION_STATUS,REGISTRATION_RENEWAL_DATE,LINE_NUMBER "
					+ "FROM (SELECT E.ID,E.USER_ID,E.ENTITY_NAME,E.ENTITY_NUMBER,E.REGISTRATION_STATUS,E.REGISTRATION_RENEWAL_DATE,"
					+ "ROW_NUMBER() OVER (ORDER BY E.ID ASC) LINE_NUMBER "
					+ "FROM EE_ENTITIES E,USERS U WHERE E.USER_ID = U.ID ");

			addConditionsToEntityQuery(entityRequestDTO, query, zip, language);
			
			
			query.append(") SUBQ10 ");

			Query pQuery = em.createNativeQuery(query.toString());
			setParameterizedQueryForSearchEnrollmentEntity(entityRequestDTO,pQuery);
			//setParameterizedQueryForSearchEnrollmentEntity(pQuery,entityName,registrationStatus,zip,language,fromDate,toDate,receivedPayments,countyServed,organisationType);
			list = pQuery.getResultList();
		}catch(Exception e) {
			LOGGER.error("Exception occurred while loading enrollment entities", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}

		LOGGER.info("searchEnrollmentEntity: END");

		return (list == null ? 0 : list.size());
	}

	private List<EnrollmentEntity> createEnrollmentEntityList(List list) {

		LOGGER.info("createEnrollmentEntityList: START");

		List<EnrollmentEntity> enrollmentEntities = new ArrayList<EnrollmentEntity>();
		EnrollmentEntity entity = null;

		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			entity = new EnrollmentEntity();
			Object[] objArray = (Object[]) iterator.next();

			if (objArray[0] != null) {
				entity.setId(Integer.valueOf(objArray[0].toString()));
			}

			if (objArray[1] != null) {
				if (entity.getUser() != null) {
					entity.getUser().setId(Integer.valueOf(objArray[1].toString()));
				} else {
					AccountUser user = new AccountUser();
					user.setId(Integer.valueOf(objArray[1].toString()));
					entity.setUser(user);
				}
			}

			if (objArray[2] != null) {
				entity.setEntityName(objArray[2].toString());
			}
			String entityNum = objArray[THREE] != null ? objArray[THREE].toString() : "0";
			Long entityNumber = (entityNum!=null || !entityNum.isEmpty())? Long.valueOf(entityNum) : 0;
			entity.setEntityNumber(entityNumber);
			if (objArray[FOUR] != null) {
				entity.setRegistrationStatus(objArray[FOUR].toString());
			}
			if (objArray[FIVE] != null) {
				try {
					entity.setRegistrationRenewalDate(EntityUtils.formatDate(new SimpleDateFormat(
							EntityConstants.DATE_FORMAT).format(new SimpleDateFormat(EntityConstants.DATE_TIME_FORMAT)
							.parse(objArray[FIVE].toString()))));
				} catch (Exception exception) {
					LOGGER.error("Error while formatting registration renewal date : ", exception);
				}
			}
			enrollmentEntities.add(entity);
		}

		LOGGER.info("createEnrollmentEntityList: END");

		return enrollmentEntities;
	}

	@Override
	public int findEnrollmentEntityByRegistrationRenewalDate() {

		LOGGER.info("findEnrollmentEntityByRegistrationRenewalDate: START");

		int recordCount = 0;
		EntityManager em = null;
		try{
		em = entityManagerFactory.createEntityManager();
		StringBuilder query = new StringBuilder();
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			query.append("SELECT COUNT(*) FROM EE_ENTITIES E WHERE date_trunc('day',E.REGISTRATION_RENEWAL_DATE) = date_trunc('day', now())");
		}else{
			query.append("SELECT COUNT(*) FROM EE_ENTITIES E WHERE trunc(E.REGISTRATION_RENEWAL_DATE) = trunc(sysdate)");
		}
		 
		List list = em.createNativeQuery(query.toString()).getResultList();

		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			Object obj = iterator.next();
			if (obj != null) {
				recordCount = Integer.valueOf(obj.toString());
			}
			break;
		}
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}

		LOGGER.info("findEnrollmentEntityByRegistrationRenewalDate: END");

		return recordCount;
	}

	@Override
	public int findEnrollmentEntityByRegistrationStatus(String registrationStatus) {

		LOGGER.info("findEnrollmentEntityByRegistrationStatus: START");

		int recordCount = 0;
		EntityManager em = null;
		try{
			em = entityManagerFactory.createEntityManager();
			StringBuilder query = new StringBuilder();
			query.append("SELECT COUNT(*) FROM EE_ENTITIES E WHERE E.REGISTRATION_STATUS = : " + REGISTRATION_STATUS );
			Query pQuery=em.createNativeQuery(query.toString());
			pQuery.setParameter(REGISTRATION_STATUS, registrationStatus);
			List list = pQuery.getResultList();
	
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object obj = iterator.next();
				if (obj != null) {
					recordCount = Integer.valueOf(obj.toString());
				}
				break;
			}
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		LOGGER.info("findEnrollmentEntityByRegistrationStatus: END");

		return recordCount;
	}

	@Override
	public int findTotalNoOfEnrollmentEntities() {

		LOGGER.info("findTotalNoOfEnrollmentEntities: START");

		int recordCount = 0;
		EntityManager em = null;
		try{
			em = entityManagerFactory.createEntityManager();
			StringBuilder query = new StringBuilder();
			query.append("SELECT COUNT(*) FROM EE_ENTITIES");

			List list = em.createNativeQuery(query.toString()).getResultList();

			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object obj = iterator.next();
				if (obj != null) {
					recordCount = Integer.valueOf(obj.toString());
				}
				break;
			}
		}catch(Exception e){
			LOGGER.error("Exception occurred while loading total no of enrollment entities", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}

		LOGGER.info("findTotalNoOfEnrollmentEntities: END");

		return recordCount;
	}

	@Override
	public int findAssistersForEnrollmentEntity(int enrollmentEntityId) {

		LOGGER.info("findAssistersForEnrollmentEntity: START");

		int recordCount = 0;
		EntityManager em = null;
		try{
			em = entityManagerFactory.createEntityManager();
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT COUNT(*) FROM EE_ASSISTERS A WHERE A.EE_ENTITY_ID = :enrollmentEntityId");
			
			Query pQuery = em.createNativeQuery(query.toString());
			pQuery.setParameter("enrollmentEntityId", enrollmentEntityId);
			List list= pQuery.getResultList();
			
			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object obj = iterator.next();
				if (obj != null) {
					recordCount = Integer.valueOf(obj.toString());
				}
				break;
			}
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}

		LOGGER.info("findAssistersForEnrollmentEntity: END");

		return recordCount;
	}

	/**
	 * @see EntityService#loadEntityrStatusHistory(entityId)
	 */

	@Override
	public List<Map<String, Object>> loadEntityrDocumentHistory(Integer entityId) {

		LOGGER.info("loadEntityrDocumentHistory: START");

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(DOCUMENTID_EE, DOCUMENT_ID_ENTITY);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(DOCUMENTID_EE);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(DATE);
		displayCols.add(FILE_NAME);
		displayCols.add(REMOVE);

		List<Map<String, Object>> data = null;
		try {
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> entityHistoryData = service.findRevisions(REPOSITORY_NAME, MODEL_NAME,
					requiredFieldsMap, entityId);
			List<Map<String, String>> orderedData = new ArrayList<Map<String, String>>(entityHistoryData);
			List<Map<String, String>> entityHistoryUniqueData = new ArrayList<Map<String, String>>();
			Collections.reverse(orderedData);
			int size = orderedData.size();
			Map<String, String> firstElement = null;
			Map<String, String> secondElement = null;
			firstElement = orderedData.get(0);
			for (int i = 0; i < size - 1; i++) {
				firstElement = orderedData.get(i);
				secondElement = orderedData.get(i + 1);
				if ((!firstElement.get(DOCUMENTID_EE).isEmpty())
						&& ((!firstElement.get(DOCUMENTID_EE).equals(secondElement.get(DOCUMENTID_EE))))) {
					entityHistoryUniqueData.add(firstElement);
				}
			}
			List<Map<String, String>> entityHistoryDocData = getEntityHistoryDocData(entityHistoryUniqueData);
			if (entityHistoryDocData != null && !entityHistoryDocData.isEmpty()) {
				data = enrollmentEntityHistoryRendererImpl.processDataForDocument(entityHistoryDocData, compareCols,
						displayCols);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occurred while loading Entity Status History", e);
		}

		LOGGER.info("loadEntityrDocumentHistory: END");

		return data;
	}

	/**
	 * @param entityHistoryUniqueData
	 * @return
	 */
	private List<Map<String, String>> getEntityHistoryDocData(
			List<Map<String, String>> entityHistoryUniqueData) {
		List<Map<String, String>> entityHistoryDocData = new LinkedList<Map<String, String>>();
		for (Map<String, String> map : entityHistoryUniqueData) {
			if (!EntityUtils.isEmpty(map.get(DOCUMENTID_EE))) {
				String docId = map.get(DOCUMENTID_EE);
				if (Integer.valueOf(docId) != 0) {
					EntityDocuments documentObj = documentRepository.findOne(Integer.valueOf(docId));
					if (documentObj != null) {
						Map<String, String> documentMap = new LinkedHashMap<String, String>();
						documentMap.put("Date", documentObj.getCreatedDate().toString());
						documentMap.put("FileName", documentObj.getOrgDocumentName());
						Integer id = documentObj.getID();
						documentMap.put("remove", id.toString());
						entityHistoryDocData.add(documentMap);
					}
				}
			}
		}
		return entityHistoryDocData;
	}

	/**
	 * @throws ContentManagementServiceException 
	 * @see EntityService#deleteDocument(integer)
	 */
	@Override
	public void deleteDocument(Integer id) throws ContentManagementServiceException {

		LOGGER.info("deleteDocument: START");

			EntityDocuments entityDocuments = documentRepository.findOne(id);
			if (!(ORACLE.equals(ecmType)) && entityDocuments != null) {
				ecmService.deleteContent(entityDocuments.getDocumentName());
			}
			documentRepository.delete(id);

		LOGGER.info("deleteDocument: END");
	}

	@Override
	public EnrollmentEntity findEnrollmentEntityByEntityName(String entityName) {

		LOGGER.info("findEnrollmentEntityByEntityName: START");
		LOGGER.info("findEnrollmentEntityByEntityName: END");

		return enrollmentEntityRepository.findByEntityName(entityName);
	}

	@Override
	public EnrollmentEntityResponseDTO searchEnrollmentEntityForAssisters(
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("searchEnrollmentEntityForAssisters: START");
		Query query = null;
		String queryStr = null;
		
		List<EnrollmentEntityAssisterSearchResult> entities = new ArrayList<EnrollmentEntityAssisterSearchResult>();
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();
		EntityManager em = null;
		try{
		em = entityManagerFactory.createEntityManager();

		String zip = enrollmentEntityRequestDTO.getZipCode();
		String language = enrollmentEntityRequestDTO.getLanguage();
		Integer distance = enrollmentEntityRequestDTO.getDistance() != null && enrollmentEntityRequestDTO.getDistance() > 0 ? enrollmentEntityRequestDTO.getDistance() : EntityConstants.DEFAULT_DISTANCE;
		String entityName = enrollmentEntityRequestDTO.getEntityName();
		Integer startRecord = enrollmentEntityRequestDTO.getStartRecord();
		Integer pageSize =  enrollmentEntityRequestDTO.getPageSize();
		ZipCode zipCode = null;
		int totalCount = 0;
		
		if (checkZipCode(zip)) {
			zipCode = zipCodeService.findByZipCode(zip+"");
			 if (zipCode == null || (zipCode.getLat() == 0 && zipCode.getLat() == 0)) {
	 	 	 	         enrollmentEntityResponseDTO.setListOfEnrollEntityAssisterSearchResult(entities);
 	                     enrollmentEntityResponseDTO.setTotalEnrollmentEntities(0);
 	 	                 return enrollmentEntityResponseDTO;
		            }
		}

		queryStr = createEntitySearchQuery(entityName, language, zip, distance, zipCode, false); 

		query = em.createNativeQuery(queryStr);
		
		setParametersInEntitySearchQuery(query, entityName, language, zip, distance, zipCode);
		
		query.setFirstResult(startRecord);
		query.setMaxResults(pageSize);
		
		List<?> list = query.getResultList();

		createEnrollmentEntityList(entities, list);
		
		
		if ((startRecord.intValue() == 0 || startRecord.intValue() == 1) && (list != null && list.size() < pageSize) ) {
			totalCount = list.size();
		}
		else {
			totalCount = getTotalCountForEntitySearch(em, zip, language,
					distance, entityName, zipCode);
		}

		enrollmentEntityResponseDTO.setListOfEnrollEntityAssisterSearchResult(entities);
		enrollmentEntityResponseDTO.setTotalEnrollmentEntities(totalCount);
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		LOGGER.info("searchEnrollmentEntityForAssisters: END");

		return enrollmentEntityResponseDTO;
	}

	/**
	 * @param em
	 * @param zip
	 * @param language
	 * @param distance
	 * @param entityName
	 * @param zipCode
	 * @param totalCount
	 * @return
	 */
	private int getTotalCountForEntitySearch(EntityManager em, String zip,
			String language, Integer distance, String entityName,
			ZipCode zipCode) {
		Query query;
		String queryStr;
		int totalCount = 0;
		queryStr = createEntitySearchQuery(entityName, language, zip, distance, zipCode, true); 

		query = em.createNativeQuery(queryStr);
		
		setParametersInEntitySearchQuery(query, entityName, language, zip, distance, zipCode);
		
		List<?> totalCountList = query.getResultList();
		
		if (totalCountList != null && (!totalCountList.isEmpty())){
			totalCount = Integer.parseInt(totalCountList.get(0) + "");
		}
		return totalCount;
	}

	/**
	 * 
	 * Method to create locate assistance entity search query 
	 * 
	 * @param entityName
	 * @param language
	 * @param zip
	 * @param distance
	 * @param zipCode
	 * @param isTotalCountQuery
	 * @return
	 */
	private String createEntitySearchQuery(String entityName, String language, String zip, Integer distance, ZipCode zipCode, boolean isTotalCountQuery) {
		
		LOGGER.info("createEntitySearchQuery: start");
		
		StringBuilder query = new StringBuilder();
		
		double longitude = 0.0;
		double lattitude = 0.0;
		
		if (checkZipCode(zip) && zipCode != null)
		{
			longitude = zipCode.getLon();
			lattitude = zipCode.getLat();
		} 
		getEntitySearchQuery(zipCode, query, longitude, lattitude,isTotalCountQuery);
		query.append(" FROM EE_ENTITIES E, USERS U, EE_SITES S, EE_LOCATIONS L, EE_SITE_LANGUAGES SL ");		
		query.append(" WHERE E.ID = S.EE_ENTITY_ID AND E.USER_ID = U.ID AND L.ID = S.PHYSICAL_LOCATION_ID AND SL.EE_SITE_ID = S.ID ");		
		query.append(" AND E.REGISTRATION_STATUS = :" + REGISTRATION_STATUS );

		EntityUtils.appendToSearchAssisterQuery(query, "E.ENTITY_NAME", entityName, ENTITY_NAME, LIKE, true, false);		
		addLanguageConditionToQuery(language, query);
		if(longitude != 0 && lattitude != 0) {
			double dis = (distance * STATE_WIDTH_FACTOR)/MILES_FACTOR;
			query.append(" AND L.LAT between ("+lattitude+ " - " + dis + ") and ("+lattitude+" + " + dis + ") ");
			query.append(" AND L.LON between ( " +longitude +" - " + dis + ") and (" +longitude +" + " + dis + ")");
		}
		query.append(" ) SUBQ1 ");
		
		if (checkZipCode(zip) && distance!= DISTANCE_FOR_OPTION_ANY) {
			query.append(" WHERE (ZIP = :" + ZIP + ") OR (DISTANCE > 0 AND DISTANCE <= :" + DISTANCE + ") ");
		}
		
		query.append(" ) SUBQ2 WHERE SUBQ2.ID IN (SELECT EE_ENTITY_ID FROM EE_ASSISTERS "
				+ "WHERE CERTIFICATION_STATUS = 'Certified' AND STATUS = 'Active' "
				+ "AND (PRIMARY_ASSISTER_SITE_ID = SUBQ2.SITE_ID OR SECONDARY_ASSISTER_SITE_ID = SUBQ2.SITE_ID)) ");
		
		if(!isTotalCountQuery)
		{
			query.append(" ORDER BY ");
			
			if (checkZipCode(zip) || distance == DISTANCE_FOR_OPTION_ANY) {
				query.append(" DISTANCE, ENTITY_NAME, ID ");
			}
			else {
				query.append(" ENTITY_NAME, ID ");
			}
		}
		LOGGER.info("createEntitySearchQuery: END");
		
		return query.toString();
	}

	/**
	 * @param longitude
	 * @param lattitude
	 * @return
	 */
	private boolean checkCoordinates(double longitude, double lattitude) {
		return longitude != 0 && lattitude != 0;
	}

	/**
	 * @param zip
	 * @return
	 */
	private boolean checkZipCode(String zip) {
		return zip != null;
	}

	/**
	 * @param zipCode
	 * @param query
	 * @param longitude
	 * @param lattitude
	 */
	private void getEntitySearchQuery(ZipCode zipCode, StringBuilder query,
			double longitude, double lattitude,boolean isCountQuery) {
		if(isCountQuery){
			query.append(" SELECT COUNT(*) FROM ( SELECT * FROM ( ");
		}else{
			query.append(" SELECT * FROM ( SELECT * FROM ( ");
		}
		query.append( " SELECT E.ID,E.ENTITY_NAME,E.REGISTRATION_STATUS,S.PRIMARY_PHONE_NUMBER,S.PRIMARY_EMAIL_ADDRESS,L.ADDRESS1,L.ADDRESS2,L.CITY,L.STATE,L.ZIP,L.COUNTY,SL.SPOKEN_LANGUAGES," +
						"SL.WRITTEN_LANGUAGES,U.FIRST_NAME,U.LAST_NAME,S.ID AS SITE_ID, L.LON, L.LAT, ");
		
		if ( zipCode !=  null  && longitude != 0 && lattitude != 0 ) {
			//query.append(" CASE WHEN l.zip = :zip1 THEN 0 ");
			//query.append(" ELSE ");
			if("ORACLE".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
				query.append(" TRUNC( CAST(3959 * acos( TRUNC( CAST( cos( TRUNC(  CAST( (:" + LATTITUDE_CONST + "/ 180.0 * 3.14 ) AS NUMBER) ,4) ) * cos( TRUNC( CAST ( (L.LAT / 180.0 * 3.14) AS NUMBER ) ,4)) * cos( (TRUNC(  L.LON / 180.0 * 3.14 ,4)) - TRUNC( CAST(  (:" + LONGITUDE_CONST +"/ 180.0 * 3.14 ) AS NUMBER) ,4) ) + sin( TRUNC(   CAST ( (:" + LATTITUDE_CONST + "/ 180.0 * 3.14 ) AS NUMBER) ,4) ) * sin( TRUNC(  L.LAT / 180.0 * 3.14 ,4) ) AS NUMBER ), 15) ) AS NUMBER ),4)");
			}
			else if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
				query.append(" TRUNC( CAST(3959 * acos( TRUNC( CAST( cos( TRUNC(  CAST( (:" + LATTITUDE_CONST + "/ 180.0 * 3.14 ) AS NUMERIC) ,4) ) * cos( TRUNC( CAST ( (L.LAT / 180.0 * 3.14) AS NUMERIC ) ,4)) * cos( (TRUNC(  L.LON / 180.0 * 3.14 ,4)) - TRUNC( CAST(  (:" + LONGITUDE_CONST +"/ 180.0 * 3.14 ) AS NUMERIC) ,4) ) + sin( TRUNC(   CAST ( (:" + LATTITUDE_CONST + "/ 180.0 * 3.14 ) AS NUMERIC) ,4) ) * sin( TRUNC(  L.LAT / 180.0 * 3.14 ,4) ) AS NUMERIC ), 15) ) AS NUMERIC ),4)");
			}
			//query.append(" END ");
		}
		else {
			query.append(" -1 ");
		}
		query.append(" as DISTANCE");
	}

	/**
	 * @param language
	 * @param query
	 */
	private void addLanguageConditionToQuery(String language,
			StringBuilder query) {
		String languageArr[] = {};
		if (language != null && !"".equals(language)) {
			languageArr = language.split(",");
			for(int i=0; i<languageArr.length;i++)
			{
				if(i == 0){
					query.append(" AND ( SL.SPOKEN_LANGUAGES LIKE :" + LANGUAGES_SPOKEN + i );
				}
				else{
					query.append(" OR SL.SPOKEN_LANGUAGES LIKE :" + LANGUAGES_SPOKEN + i);
				}
			}
			query.append(" ) ");
		}
	}
	
	/**
	 * method to set parameters in entity search query
	 * 
	 * @param query
	 * @param entityName
	 * @param language
	 * @param zip
	 * @param distance
	 * @param zipCode
	 */
	private void setParametersInEntitySearchQuery(Query query, String entityName, String language, String zip, Integer distance, ZipCode zipCode) {
		
		double longitude = 0.0;
		double lattitude = 0.0;
		
		if (zipCode !=  null)
		{
			longitude = zipCode.getLon();
			lattitude = zipCode.getLat();
			
			if (checkCoordinates(longitude, lattitude) ) {
				query.setParameter(LATTITUDE_CONST, lattitude);
				query.setParameter(LONGITUDE_CONST, longitude);
				//query.setParameter(ZIP_SELECT, zip); 
			}
		}
		
		if(checkZipCode(zip) && distance!= DISTANCE_FOR_OPTION_ANY) {
			query.setParameter(DISTANCE, distance);
			query.setParameter(ZIP, zip);
		}
				
		query.setParameter(REGISTRATION_STATUS, EntityConstants.STATUS_ACTIVE);
		EntityUtils.setQueryParameter(query, ENTITY_NAME, entityName, true);
		
		String languageArr[] = {};
		if (language != null && !"".equals(language)) {
			languageArr = language.split(",");
			for(int i=0; i<languageArr.length;i++)
			{
				query.setParameter(LANGUAGES_SPOKEN+i, PERCENTAGE+languageArr[i]+PERCENTAGE);
			}
		}
		

		
	}
	

	@Override
	public EnrollmentEntityAssisterSearchResult getEnrollmentEntityDetails(Integer enrollmentEntityId, Integer siteId) {

		LOGGER.info("getEnrollmentEntityDetails: START");

		StringBuilder query = new StringBuilder();
		EntityManager em = null;
		EnrollmentEntityAssisterSearchResult result = null;
		try {
			em = entityManagerFactory.createEntityManager();
	
			query.append("SELECT ID,ENTITY_NAME,REGISTRATION_STATUS,PRIMARY_PHONE_NUMBER,PRIMARY_EMAIL_ADDRESS,"
					+ "ADDRESS1,ADDRESS2,CITY,STATE,ZIP,COUNTY,SPOKEN_LANGUAGES,WRITTEN_LANGUAGES,FIRST_NAME,LAST_NAME,SITE_ID,LAT,LON,LINE_NUMBER FROM ("
					+ "SELECT E.ID,E.ENTITY_NAME,E.REGISTRATION_STATUS,S.PRIMARY_PHONE_NUMBER,S.PRIMARY_EMAIL_ADDRESS,L.ADDRESS1,L.ADDRESS2,"
					+ "L.CITY,L.STATE,L.ZIP,L.COUNTY,SL.SPOKEN_LANGUAGES,SL.WRITTEN_LANGUAGES,U.FIRST_NAME,U.LAST_NAME,S.ID AS SITE_ID,L.LAT,L.LON, ROW_NUMBER() OVER (ORDER BY E.ID ASC) LINE_NUMBER "
					+ "FROM EE_ENTITIES E, USERS U, EE_SITES S, EE_LOCATIONS L, EE_SITE_LANGUAGES SL "
					+ "WHERE E.ID = S.EE_ENTITY_ID AND E.USER_ID = U.ID AND L.ID = S.PHYSICAL_LOCATION_ID AND SL.EE_SITE_ID = S.ID AND E.ID =  ");
	
			query.append(enrollmentEntityId).append(" AND S.ID =:siteId");
			query.append(") SUBQ11 ");
			Query pQuery = em.createNativeQuery(query.toString());
			pQuery.setParameter("siteId", siteId);
			List<?> list = pQuery.getResultList();
			
			if(CollectionUtils.isNotEmpty(list)){
				result = getEnrollmentEntityDetail(list);
				List<SiteLocationHours> siteLocationHours = siteLocationHoursService.findSiteLocationHoursBySite(result
						.getSite());
				/* Bug Fix- HIX-33553 Start
				   Sorted siteLocationHours so that the Day should appear as per standard sorted order
				 * like Monday ,Tuesday....Sunday
				 */
				Collections.sort(siteLocationHours);
				//Bug Fix- HIX-33553 End
				result.setListOfSiteLocationHours(siteLocationHours);
			}
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		LOGGER.info("getEnrollmentEntityDetails: END");

		return result;
	}

	private <E> void createEnrollmentEntityList(List<EnrollmentEntityAssisterSearchResult> entities, List<E> rsList) {

		LOGGER.info("createEnrollmentEntityList: START");

		Iterator<E> rsIterator = rsList.iterator();

		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			EnrollmentEntityAssisterSearchResult searchDTO = new EnrollmentEntityAssisterSearchResult();
			EnrollmentEntity enrollmentEntity = new EnrollmentEntity();
			EntityLocation entityLocation = new EntityLocation();
			SiteLanguages siteLanguages = new SiteLanguages();
			Site site = new Site();
			AccountUser user = new AccountUser();

			setEnrollmentEntityObject(objArray, enrollmentEntity,
					entityLocation);

			if (objArray[TEN] != null) {
				entityLocation.setCounty(objArray[TEN].toString());
			}
			if (objArray[ELEVEN] != null) {
				siteLanguages.setSpokenLanguages(objArray[ELEVEN].toString());
			}
			if (objArray[TWELVE] != null) {
				siteLanguages.setWrittenLanguages(objArray[TWELVE].toString());
			}
			if (objArray[THIRTEEN] != null) {
				user.setFirstName(objArray[THIRTEEN].toString());
			}
			if (objArray[FORTEEN] != null) {
				user.setLastName(objArray[FORTEEN].toString());
			}
			if (objArray[FIFTEEN] != null) {
				site.setId(Integer.valueOf(objArray[FIFTEEN].toString()));
			}
			
			if (objArray.length >= EIGHTEEN && objArray[EIGHTEEN] != null) {
				searchDTO.setDistance(Double.parseDouble(objArray[EIGHTEEN] +""));
			}
			if(siteLanguages!=null){
				String writtenLanguages=siteLanguages.getWrittenLanguages();
				writtenLanguages=languageWordWrap(writtenLanguages);
				String spokenLanguages=siteLanguages.getSpokenLanguages();
				spokenLanguages=languageWordWrap(spokenLanguages);
				siteLanguages.setSpokenLanguages(spokenLanguages);
				siteLanguages.setWrittenLanguages(writtenLanguages);
			}
			searchDTO.setEnrollmentEntity(enrollmentEntity);
			searchDTO.setEntityLocation(entityLocation);
			searchDTO.setSite(site);
			searchDTO.setSiteLanguages(siteLanguages);
			searchDTO.setUser(user);
			entities.add(searchDTO);
		}

		LOGGER.info("createEnrollmentEntityList: END");
	}
	
	private String languageWordWrap(String languageString){
		  if(languageString != null){
		   String[] splittedString = languageString.split(",");
		   String newString = "";
		   for(int i= 0; i<splittedString.length;i++){
		    if(!splittedString[i].equals("null")){
		     if(i == 0){
		      newString += splittedString[i];
		     }else{
		      newString += "," + splittedString[i];
		     }
		    }
		    
		   }
		   languageString = newString.replaceAll(",", ", ");
		  }
		  return languageString;
		   
		 }

	/**
	 * @param objArray
	 * @param enrollmentEntity
	 * @param entityLocation
	 */
	private void setEnrollmentEntityObject(Object[] objArray,
			EnrollmentEntity enrollmentEntity, EntityLocation entityLocation) {
		if (objArray[0] != null) {
			enrollmentEntity.setId(Integer.valueOf(objArray[0].toString()));
		}
		if (objArray[ONE] != null) {
			enrollmentEntity.setEntityName(objArray[ONE].toString());
		}
		if (objArray[TWO] != null) {
			enrollmentEntity.setRegistrationStatus(objArray[TWO].toString());
		}
		if (objArray[THREE] != null) {
			enrollmentEntity.setPrimaryPhoneNumber(objArray[THREE].toString());
		}
		if (objArray[FOUR] != null) {
			enrollmentEntity.setPrimaryEmailAddress(objArray[FOUR].toString());
		}
		setEntityLocation(objArray, entityLocation);
	}

	/**
	 * @param objArray
	 * @param entityLocation
	 */
	private void setEntityLocation(Object[] objArray,
			EntityLocation entityLocation) {
		if (objArray[FIVE] != null) {
			entityLocation.setAddress1(objArray[FIVE].toString());
		}
		if (objArray[SIX] != null) {
			entityLocation.setAddress2(objArray[SIX].toString());
		}
		if (objArray[SEVEN] != null) {
			entityLocation.setCity(objArray[SEVEN].toString());
		}
		if (objArray[EIGHT] != null) {
			entityLocation.setState(objArray[EIGHT].toString());
		}
		if (objArray[NINE] != null) {
			entityLocation.setZip(objArray[NINE].toString());
		}
	}

	private <E> EnrollmentEntityAssisterSearchResult getEnrollmentEntityDetail(List<E> rsList) {

		LOGGER.info("getEnrollmentEntityDetail: START");

		Iterator<E> rsIterator = rsList.iterator();
		EnrollmentEntityAssisterSearchResult searchDTO = null;
		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			searchDTO = new EnrollmentEntityAssisterSearchResult();

			EnrollmentEntity enrollmentEntity = new EnrollmentEntity();
			EntityLocation entityLocation = new EntityLocation();
			SiteLanguages siteLanguages = new SiteLanguages();
			Site site = new Site();
			AccountUser user = new AccountUser();

			setEnrollmentEntityObject(objArray, enrollmentEntity,
					entityLocation);

			if (objArray[TEN] != null) {
				entityLocation.setCounty(objArray[TEN].toString());
			}
			if (objArray[ELEVEN] != null) {
				siteLanguages.setSpokenLanguages(objArray[ELEVEN].toString());
			}
			if (objArray[TWELVE] != null) {
				siteLanguages.setWrittenLanguages(objArray[TWELVE].toString());
			}
			if (objArray[THIRTEEN] != null) {
				user.setFirstName(objArray[THIRTEEN].toString());
			}
			if (objArray[FORTEEN] != null) {
				user.setLastName(objArray[FORTEEN].toString());
			}
			if (objArray[FIFTEEN] != null) {
				site.setId(Integer.valueOf(objArray[FIFTEEN].toString()));
			}
			if (objArray[SIXTEEN] != null) {
				entityLocation.setLat(Double.valueOf(objArray[SIXTEEN].toString()));
			}
			if (objArray[SEVENTEEN] != null) {
				entityLocation.setLat(Double.valueOf(objArray[SEVENTEEN].toString()));
			}
			searchDTO.setEnrollmentEntity(enrollmentEntity);
			searchDTO.setEntityLocation(entityLocation);
			searchDTO.setSite(site);
			searchDTO.setSiteLanguages(siteLanguages);
			searchDTO.setUser(user);
		}

		LOGGER.info("getEnrollmentEntityDetail: END");

		return searchDTO;
	}

	/**
	 * @param <E>
	 * @param <E>
	 * @see EntityService#searchIndividualForEnrollmentEntity
	 */
	@Override
	public EnrollmentEntityResponseDTO searchIndividualForEnrollmentEntity(Map<String, Object> searchCriteria,
			String desigStatus, Integer startRecord, Integer pageSize) {

		LOGGER.info("searchIndividualForEnrollmentEntity: START");

		StringBuilder query = new StringBuilder();
		EntityManager em =null;
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		try{
			em = entityManagerFactory.createEntityManager();
			// Get search criteria
			String individualFirstName = (String) searchCriteria.get("individualFirstName");
			String individualLastName = (String) searchCriteria.get("individualLastName");
			String enrollmentCounselorFirstName = (String) searchCriteria.get("enrollmentCounselorFirstName");
			String enrollmentCounselorLastName = (String) searchCriteria.get("enrollmentCounselorLastName");
			String status = (String) searchCriteria.get(STATUS);
			String entityIdStr = (String) searchCriteria.get("entityId");
			Integer entityId = Integer.valueOf(entityIdStr);
			String fromDate = (String) searchCriteria.get("fromDate");
			String toDate = (String) searchCriteria.get("toDate");
			Integer startLine = startRecord + 1;
			Integer endRecord = startRecord + pageSize;
			List<Map<String, Object>> individualList = new ArrayList<Map<String, Object>>();
			// building query
			query.append("SELECT FIRST_NAME,LAST_NAME,FIRSTNAME,LASTNAME,LAST_UPDATE_TIMESTAMP,ENROLLMENT_STATUS, LINE_NUMBER FROM ( "
					+ "SELECT A.FIRST_NAME,A.LAST_NAME,I.FIRSTNAME,I.LASTNAME,D.LAST_UPDATE_TIMESTAMP,I.ENROLLMENT_STATUS,ROW_NUMBER() OVER (ORDER BY D.ID ASC)LINE_NUMBER FROM "
					+ "EE_ASSISTERS A, EXTERNAL_INDIVIDUAL I, EE_DESIGNATE_ASSISTERS D WHERE A.ID = D.ASSISTER_ID AND I.INDIVIDUAL_CASE_ID = D.INDIVIDUAL_ID AND D.STATUS =:desigStatus");

			appendFullNameToQuery(query, "I.FIRSTNAME", individualFirstName, "I.LASTNAME", individualLastName, "name");
			/*if (individualLastName != null && !"".equals(individualLastName)) {
				query.append(" AND UPPER(I.LASTNAME) LIKE UPPER('%" + individualLastName.trim() + "%')");
			}*/
			if (entityId != null) {
				query.append(" AND D.ENTITY_ID=:entityId ");
			}
			appendFullNameToQuery(query, " A.FIRST_NAME ", enrollmentCounselorFirstName, " A.LAST_NAME ", enrollmentCounselorLastName, "counselorName");
			
			if (status != null && !"".equals(status)) {
				query.append(" AND TRIM(I.ENROLLMENT_STATUS) = TRIM(:status)");
			}
			if (fromDate != null && !"".equals(fromDate)) {
				query.append(" AND D.LAST_UPDATE_TIMESTAMP >=to_date(:" + FROM_DATE + ",'mm-dd-yyyy')");
			}
			if (toDate != null && !"".equals(toDate)) {
				query.append("AND D.LAST_UPDATE_TIMESTAMP <= to_date(:" + TO_DATE + MM_DD_YY_1);
			}
			// For Total no of Individuals
			query.append(") SUBQ12 ");
			Query pQuery=em.createNativeQuery(query.toString());
			setParameterssearchIndividualForEntity(pQuery,searchCriteria,desigStatus,entityId);
			List<?> listSize = pQuery .getResultList();
		
			query.append("WHERE LINE_NUMBER BETWEEN :" + START_LINE + " AND :" + END_RECORD);
			
			Query pQueryForList=em.createNativeQuery(query.toString());
			setParameterssearchIndividualForEntityList(pQueryForList,startLine,endRecord,searchCriteria,desigStatus,entityId);
			List<?> list = pQueryForList.getResultList();
					
			createIndividualListForEnrollmentEntity(individualList, list);
			
			enrollmentEntityDTO.setEnrollmentEntityHistory(individualList);
			enrollmentEntityDTO.setNoOfIndividualsForEnrollmentEntity(listSize.size());
		}finally{
			if(em != null && em.isOpen()){
				 em.close();
			}
		}
		LOGGER.info("searchIndividualForEnrollmentEntity: END");

		return enrollmentEntityDTO;
	}

	/**
	 * @param query
	 * @param firstName
	 * @param lastName
	 */
	private void appendFullNameToQuery(StringBuilder query, String firstNameField, String firstName, String lastNameField, String lastName, String fieldKey) {
		if ((firstName != null && !"".equals(firstName)) ||(lastName != null && !"".equals(lastName))) {
			query.append(" AND UPPER(").append(firstNameField).append("||").append(lastNameField).append(") LIKE UPPER(:").append(fieldKey).append(")");
		}
	}

	private Query setParameterssearchIndividualForEntityList(Query pQueryForList, Integer startLine, Integer endRecord,
			Map<String, Object> searchCriteria,String desigStatus,Integer entityId) {
		
		LOGGER.info("setParameterssearchIndividualForEnrollmentEntityList: START");
		
		setParameterssearchIndividualForEntity(pQueryForList,searchCriteria,desigStatus,entityId);
		pQueryForList.setParameter(START_LINE,startLine);
		pQueryForList.setParameter(END_RECORD, endRecord);
		
		LOGGER.info("setParameterssearchIndividualForEnrollmentEntityList: START");
		return pQueryForList;
	}

	private Query setParameterssearchIndividualForEntity(Query pQuery,Map<String, Object> searchCriteria,String desigStatus,Integer entityId) {
		LOGGER.info("setParameterssearchIndividualForEnrollmentEntity: START");
		
		String individualFirstName = (String) searchCriteria.get("individualFirstName");
		String individualLastName = (String) searchCriteria.get("individualLastName");
		String enrollmentCounselorFirstName = (String) searchCriteria.get("enrollmentCounselorFirstName");
		String enrollmentCounselorLastName = (String) searchCriteria.get("enrollmentCounselorLastName");
		String status = (String) searchCriteria.get(STATUS);
		String fromDate = (String) searchCriteria.get("fromDate");
		String toDate = (String) searchCriteria.get("toDate");
		
		pQuery.setParameter("desigStatus", desigStatus);
		pQuery.setParameter("entityId", entityId);
		
		setFullNameQueryParameter(pQuery, individualFirstName, individualLastName, "name");
		setFullNameQueryParameter(pQuery, enrollmentCounselorFirstName, enrollmentCounselorLastName, "counselorName");
		
		if (status != null && !"".equals(status)) {
			pQuery.setParameter(STATUS,status);
		}
		if (fromDate != null && !"".equals(fromDate)) {
			pQuery.setParameter(FROM_DATE,fromDate);
		}

		if (toDate != null && !"".equals(toDate)) {
			pQuery.setParameter(TO_DATE,toDate);
		}
		
		LOGGER.info("setParameterssearchIndividualForEnrollmentEntity: END");
		return pQuery;
	}

	/**
	 * @param pQuery
	 * @param firstName
	 * @param lastName
	 */
	private void setFullNameQueryParameter(Query pQuery, String firstName,
			String lastName, String parameterName) {
		if ((firstName != null && !"".equals(firstName)) ||(lastName != null && !"".equals(lastName))) {
			String name = "%" + (firstName==null?"":(firstName.trim().replaceAll(" ", "%")))+"%"+(lastName==null?"":(lastName.trim()+"%"));
			pQuery.setParameter(parameterName, "%" + name.trim() + "%");
		}
	}

/*	*//**
	 * Retrieves Individual details from DB and sends the indidivual Ids to AHBX
	 * 
	 * @param entityId
	 *            the entity id
	 * @param individualStatus
	 *            the individual status
	 *//*
	private void sendIndividualsListToAHBX(Integer entityId, String individualStatus) {

		LOGGER.info("sendIndividualsListToAHBX: START");

		List<Long> indvIdList = new ArrayList<Long>();
		List<DesignateAssister> designateAssisterList = null;

		try {
			designateAssisterList = designateAssister.findDesignateAssisterForExternalIndbyEntityId(entityId,
					DesignateAssister.Status.valueOf(individualStatus));
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching individual details from DB: ", exception);
		}

		// Retrieving external individual Ids associated with broker Id
		for (DesignateAssister desigAssister : designateAssisterList) {
			indvIdList.add(Long.valueOf(desigAssister.getIndividualId()));
		}

		if (indvIdList != null && indvIdList.size() != 0) {
			LOGGER.info("External Individual BOB Status AHBX service call Starts : ");

			// IND51 Trigger - Call AHBX to retrieve/save external individual
			// status
			ahbxRestCallInvoker.sendIndividualList(indvIdList);
		}

		LOGGER.info("sendIndividualsListToAHBX: END");
	}*/

	/**
	 * @param <E>
	 * @ to set Individual List
	 */
	private <E> void createIndividualListForEnrollmentEntity(List<Map<String, Object>> individualList, List<E> list) {

		LOGGER.info("createIndividualListForEnrollmentEntity: START");

		Iterator<E> rsIterator = list.iterator();
		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			Map<String, Object> individual = new HashMap<String, Object>();
			// set enrollmentCounselor name
			if ((objArray[0] != null) && (objArray[1] != null)) {
				individual
						.put("enrollmentCounselor", objArray[0].toString().concat(" ").concat(objArray[1].toString()));
			}
			// set individual name
			if ((objArray[2] != null) && (objArray[THREE] != null)) {
				individual.put("individualName", objArray[2].toString().concat(" ").concat(objArray[THREE].toString()));
			}
			// set date
			if (objArray[FOUR] != null) {
				String dateStr = objArray[FOUR].toString();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date convertedDate = null;
				try {
					convertedDate = dateFormat.parse(dateStr);
				} catch (ParseException e) {
					LOGGER.error("Error while parsing date  : ", e);
				}
				individual.put("updated", convertedDate);
			}
			if ((objArray[FIVE] != null)) {
				individual.put(STATUS, objArray[FIVE].toString());
			}
			individualList.add(individual);
		}

		LOGGER.info("createIndividualListForEnrollmentEntity: END");
	}
	
	/**
	 * @see EnrollmentEntityService#searchCECToReassign(enrollmentEntityRequestDTO)
	 * 
	 * @param EnrollmentEntityRequestDTO
	 *            contains Assister search parameters
	 * @return {@link List<Assister>}
	 */
	@Override
	public Map<String,Object> searchCECToReassign(EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("searchAssistersByEntityAndSite: START");
		
		Map<String,Object> output = new HashMap<String,Object>();
		StringBuilder sb = new StringBuilder();
		EntityManager em = null;
		try{
		em = entityManagerFactory.createEntityManager();

		List<Assister> assisters = new ArrayList<Assister>();

		Map<String, Object> searchCriteria = enrollmentEntityRequestDTO.getSearchCriteria();

		Integer startRecord = (Integer) searchCriteria.get(STARTPAGE);	
		Integer endRecord = startRecord + enrollmentEntityRequestDTO.getPageSize();

		Integer siteId = (Integer)searchCriteria.get(PRIMARYSITE);
		String cecFirstOrLastName = (String)searchCriteria.get(CECFIRSTORLASTNAME);
		String cecEmailAddress = (String)searchCriteria.get(CECEMAILADDRESS);
		Integer entityId= (Integer)searchCriteria.get(MODULEID);
	
		sb.append("SELECT A.ID,A.FIRST_NAME,A.LAST_NAME,A.EMAIL_ADDRESS,SITE.ID as SITE_ID,SITE.SITE_LOCATION_NAME FROM EE_ASSISTERS A,EE_ENTITIES E,EE_SITES SITE WHERE A.EE_ENTITY_ID = E.ID " +
				"and A.PRIMARY_ASSISTER_SITE_ID=SITE.ID AND  A.EE_ENTITY_ID = :entityId and A.STATUS = 'Active' and A.CERTIFICATION_STATUS = 'Certified'" );	
		
		if (StringUtils.isNotEmpty(cecFirstOrLastName)) {
			cecFirstOrLastName = cecFirstOrLastName.trim().replace(" ", PERCENTAGE);
			sb.append(" and upper(A.first_name) || upper(A.last_name) like upper(:cecFirstOrLastName) ");
		}
		if (StringUtils.isNotEmpty(cecEmailAddress)) {
			cecEmailAddress = cecEmailAddress.trim().replace(" ", PERCENTAGE);
			sb.append(" and upper(A.email_address) like upper(:cecEmailAddress) ");
		}
		if(siteId != null) {
			sb.append(" and A.PRIMARY_ASSISTER_SITE_ID= :siteId ");
		}						
		sb.append(" order by A.first_name,A.last_name ");
				
		Query pQuery= em.createNativeQuery(sb.toString());	
		
		pQuery.setParameter(ENTITYID, entityId);
		if(siteId != null)
		{
			pQuery.setParameter(SITEID, siteId);
		}
		if (StringUtils.isNotEmpty(cecEmailAddress)) {
			if(StringUtils.contains(cecEmailAddress, " ")) {
				cecEmailAddress = StringUtils.replace(cecEmailAddress, " ", PERCENTAGE);
			}
			pQuery.setParameter(CECEMAILADDRESS,  PERCENTAGE + searchCriteria.get(CECEMAILADDRESS) + PERCENTAGE);
		}				
		if (StringUtils.isNotEmpty(cecFirstOrLastName)) {
			if(StringUtils.contains(cecFirstOrLastName, " ")) {
				cecFirstOrLastName = StringUtils.replace(cecFirstOrLastName, " ", PERCENTAGE);
			}
			pQuery.setParameter(CECFIRSTORLASTNAME,  PERCENTAGE + cecFirstOrLastName  + PERCENTAGE);
		}
		
		pQuery.setMaxResults(endRecord); 
		pQuery.setFirstResult(startRecord);     	
		
		List<?> list = pQuery.getResultList();
		setAssisterList(assisters, list);
		output.put(ASSISTERS, assisters);
		int totalNumberOfRecords = getCECRecordCount(siteId,cecFirstOrLastName,cecEmailAddress,entityId,searchCriteria);
		output.put(TOTAL_RECORDS, new Integer(totalNumberOfRecords));
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		LOGGER.info("searchAssistersByEntityAndSite: END");

		return output;
	}
	
	private void setAssisterList(List<Assister> assisters, List rsList) {

		LOGGER.info("setAssisterList: START");
		int idColumnIndex=0;
		int firstNameColumnIndex=1;
		int lastNameColumnIndex=2;
		int emailColumnIndex=3;
		int siteIdColumnIndex=4;
		int siteNameColumnIndex=5;
		Iterator rsIterator = rsList.iterator();

		while (rsIterator.hasNext()) {
			
			Object[] objArray = (Object[]) rsIterator.next();
			Assister a = new Assister();
			Site s = new Site();
			if (objArray[idColumnIndex] != null) {
				a.setId(Integer.valueOf(objArray[idColumnIndex].toString()));
			}			
			if (objArray[firstNameColumnIndex] != null) {
				a.setFirstName(objArray[firstNameColumnIndex].toString());
			}
			if (objArray[lastNameColumnIndex] != null) {
				a.setLastName(objArray[lastNameColumnIndex].toString());
			}
			if (objArray[emailColumnIndex] != null) {
				a.setEmailAddress(objArray[emailColumnIndex].toString());
			}
			if (objArray[siteIdColumnIndex] != null) {				
				s.setId(Integer.valueOf(objArray[siteIdColumnIndex].toString()));
				s.setSiteLocationName(objArray[siteNameColumnIndex].toString());
				a.setPrimaryAssisterSite(s);				
			}
			assisters.add(a);
		}
		LOGGER.info("setAssisterList: END");
	}
	
	private int getCECRecordCount(Integer siteId,String cecFirstOrLastName,String cecEmailAddress,Integer entityId,Map<String, Object> searchCriteria) {

		LOGGER.info("getCECTotalCount: START");

		int recordCount = 0;
		EntityManager em = null;
		try{
		em = entityManagerFactory.createEntityManager();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT count(*) FROM EE_ASSISTERS A,EE_ENTITIES E,EE_SITES SITE WHERE A.EE_ENTITY_ID = E.ID " +
				"and A.PRIMARY_ASSISTER_SITE_ID=SITE.ID AND  A.EE_ENTITY_ID = :entityId and A.STATUS = 'Active' and A.CERTIFICATION_STATUS = 'Certified'" );
		
		if (StringUtils.isNotEmpty(cecFirstOrLastName)) {
			cecFirstOrLastName = cecFirstOrLastName.trim().replace(" ", "%");
			sb.append(" and upper(A.first_name) || upper(A.last_name) like upper(:cecFirstOrLastName) ");
		}
		if (StringUtils.isNotEmpty(cecEmailAddress)) {
			cecEmailAddress = cecEmailAddress.trim().replace(" ", PERCENTAGE);
			sb.append(" and upper(A.email_address) like upper(:cecEmailAddress) ");
		}
		if(siteId != null) {
			sb.append(" and A.PRIMARY_ASSISTER_SITE_ID= :siteId ");
		}	
		
		Query query= em.createNativeQuery(sb.toString());	
		
		query.setParameter(ENTITYID, entityId);
		if(siteId != null)
		{
			query.setParameter(SITEID, siteId);
		}
		if (StringUtils.isNotEmpty(cecEmailAddress)) {
			if(StringUtils.contains(cecEmailAddress, " ")) {
				cecEmailAddress = StringUtils.replace(cecEmailAddress, " ", PERCENTAGE);
			}
			query.setParameter(CECEMAILADDRESS,  PERCENTAGE + searchCriteria.get(CECEMAILADDRESS) + PERCENTAGE);
		}				
		if (StringUtils.isNotEmpty(cecFirstOrLastName)) {
			if(StringUtils.contains(cecFirstOrLastName, " ")) {
				cecFirstOrLastName = StringUtils.replace(cecFirstOrLastName, " ", PERCENTAGE);
			}
			query.setParameter(CECFIRSTORLASTNAME,  PERCENTAGE + cecFirstOrLastName + PERCENTAGE);
		}
		
		Object result = query.getSingleResult();
    	if(result != null){
        	Number count = (Number) result;
        	recordCount = count.intValue();
    	}
		}finally{
			if(em != null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		return recordCount;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		appContext = applicationContext;
		
	}
	
	private Object loadRepository() {
		return this.appContext.getBean(REPOSITORY_NAME);
	}
	
	
}