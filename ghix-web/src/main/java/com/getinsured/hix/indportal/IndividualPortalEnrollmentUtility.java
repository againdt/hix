package com.getinsured.hix.indportal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.dto.enrollment.EnrolleeDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.indportal.dto.ActiveEnrollmentHistoryComparator;
import com.getinsured.hix.indportal.dto.EnrollmentHistory;
import com.getinsured.hix.indportal.dto.EnrollmentHistoryEntry;
import com.getinsured.hix.indportal.dto.EnrollmentHistoryEntryComparator;
import com.getinsured.hix.indportal.dto.OTR;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.indportal.dto.IndDisenroll.TerminationDateChoice;
import com.getinsured.iex.util.IndividualPortalUtil;
import com.getinsured.timeshift.TSLocalTime;
import com.google.gson.Gson;


/**
 * HIX-59164
 * Display Enrollment History for the Household
 * 
 * @author sahoo_s
 */
@Component
public class IndividualPortalEnrollmentUtility extends IndividualPortalUtility {

	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualPortalEnrollmentUtility.class);
	
	private static final String ENROLLMENT_EXCEPTION_MESSAGE = "Exception occured while fetching enrollment details :";
	
	private static final String DENTAL_PLAN = "dental";
	public static final String HEALTH_PLAN = "health";
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
    @Autowired private Gson platformGson;
	
	@Value("#{configProp['appUrl']}")
	private String appUrl;
	
	@Autowired
	private UserService userService;
	
	@Autowired 
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired private IndividualPortalUtil individualPortalUtil;
	
	private static List<String> enrollmentStatus = new ArrayList<String>();
	private static List<String> ACTIVE_ENROLLMENT_STATUS = new ArrayList<String>();
	private static final String TERM_ENROLLMENT = "TERM";
	
	static{
		enrollmentStatus.add("CONFIRM");
		enrollmentStatus.add("PENDING");
		ACTIVE_ENROLLMENT_STATUS.add("CONFIRM");
		ACTIVE_ENROLLMENT_STATUS.add("PENDING");
	}
	
	public EnrollmentHistory getEnrollmentHistory(int coverageYear) {
		
		EnrollmentResponse enrollmentResponse = null;  
		EnrollmentHistory enrollmentHistory = null;		
		try {
			if(coverageYear != 0){
				
				AccountUser user = this.userService.getLoggedInUser();
				String activeModuleId = String.valueOf(user.getActiveModuleId());
				
				List<String> houseHoldCaseIdList = new ArrayList<String>();
				houseHoldCaseIdList.add(activeModuleId);
				
				List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
				enrollmentStatusList.add(EnrollmentStatus.TERM);
				enrollmentStatusList.add(EnrollmentStatus.CANCEL);
				enrollmentStatusList.add(EnrollmentStatus.PENDING);
				enrollmentStatusList.add(EnrollmentStatus.CONFIRM);
				
				EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
				enrollmentRequest.setCoverageYear(String.valueOf(coverageYear));
				enrollmentRequest.setHouseHoldCaseIdList(houseHoldCaseIdList);
				enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
				
				
				String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS, 
						user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
				
				if (null != response){ 
					enrollmentResponse = platformGson.fromJson(response, EnrollmentResponse.class);
					if(enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						List<EnrollmentDataDTO> enrollmentDataDTOList = enrollmentResponse.getEnrollmentDataDtoListMap().get(activeModuleId);
						enrollmentHistory = transformEnrollmentHistoryForDisplay(enrollmentDataDTOList, coverageYear);
					}
					else if(enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE) && enrollmentResponse.getErrMsg().equalsIgnoreCase("No Enrollment found for given search criteria.")){
						enrollmentHistory = new EnrollmentHistory();
					}else{
						throw new GIException("Unable to get Enrollment History Details.");
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : ", e);
			throw new GIRuntimeException(ENROLLMENT_EXCEPTION_MESSAGE);
		}
		
		return enrollmentHistory;
	}

	private EnrollmentHistory transformEnrollmentHistoryForDisplay(List<EnrollmentDataDTO> enrollmentDataDTOList, int coverageYear) throws GIException {
		
		EnrollmentHistory enrollmentHistory = new EnrollmentHistory();
		List<EnrollmentHistoryEntry> activeHealthEnrollmentHistory = new ArrayList<>();
		List<EnrollmentHistoryEntry> activeDentalEnrollmentHistory = new ArrayList<>();
		List<EnrollmentHistoryEntry> inActiveHealthEnrollmentHistory = new ArrayList<>();
		List<EnrollmentHistoryEntry> inActiveDentalEnrollmentHistory = new ArrayList<>();

		if (enrollmentDataDTOList != null) {

			for (EnrollmentDataDTO enrollmentDataDTO : enrollmentDataDTOList) {
				
				List<EnrolleeDataDTO> enrolleeDataDTOList = enrollmentDataDTO.getEnrolleeDataDtoList();
				PlanResponse planResponse = getPlanDetails(String.valueOf(enrollmentDataDTO.getPlanId()));
				String planLogoURL = planResponse.getIssuerLogo();
				String enrollmentId  = ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollmentDataDTO.getEnrollmentId().toString());
				EnrollmentHistoryEntry enrollmentHistoryEntry = 
						new EnrollmentHistoryEntry(enrollmentDataDTO,enrolleeDataDTOList,planResponse,planLogoURL,enrollmentId);
				
				Date endOfCurrentMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.CURRENT_MONTH, null);
				Date endOfNextMonth = null;
				Date endOfMonthAfterNextMonth = null;
				
				int currentMonth = new LocalDate(endOfCurrentMonth.getTime()).getMonthOfYear();
				
				//If user terminates plan in November, show only two options
				if(currentMonth == DateTimeConstants.NOVEMBER){
					endOfNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.NEXT_MONTH, null);
				}
				//If user terminates plan in December, show only one option	
				else if(currentMonth !=	DateTimeConstants.DECEMBER){
					endOfNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.NEXT_MONTH, null);
					endOfMonthAfterNextMonth = getTerminationDateOnDisenrollment(TerminationDateChoice.MONTH_AFTER_NEXT_MONTH, null);
				}
				
				enrollmentHistoryEntry.setEndOfCurrentMonth(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfCurrentMonth));
				enrollmentHistoryEntry.setEndOfNextMonth(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfNextMonth));
				enrollmentHistoryEntry.setEndOfMonthAfterNext(dateToString(IndividualPortalConstants.UI_DATE_FORMAT, endOfMonthAfterNextMonth));
				
				if (enrollmentStatus.contains(enrollmentDataDTO.getEnrollmentStatus())) {
					//Added check to hide the dis-enrollment button while application Type=SEP and eligibility Status=DE
					boolean hideDisenroll = isActiveApplicationDeniedEligibility(Long.valueOf(enrollmentDataDTO.getHouseHoldCaseId()), coverageYear);
					if (hideDisenroll) {
						enrollmentHistoryEntry.setAllowDisenroll(false);
					}else {
						enrollmentHistoryEntry.setAllowDisenroll(true);
					}
				}	
				
				//HIX-87477
				enrollmentHistoryEntry.setTerminationDateChangeAllowed(
						isTerminateDateChangeAllowed(enrollmentHistoryEntry.getEnrollmentStatus(), 
								enrollmentHistoryEntry.getCoverageEndDate()));
				
				boolean isOE = individualPortalUtil.isInsideOEEnrollmentWindow(coverageYear);
				enrollmentHistoryEntry.setInsideOEEnrollmentWindow(isOE);
				
				if(isEnrollmentActive(enrollmentDataDTO)){
					if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(HEALTH_PLAN)){
						activeHealthEnrollmentHistory.add(enrollmentHistoryEntry);
					}else if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(DENTAL_PLAN)){
						activeDentalEnrollmentHistory.add(enrollmentHistoryEntry);
					}
				} else {
					if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(HEALTH_PLAN)){
						inActiveHealthEnrollmentHistory.add(enrollmentHistoryEntry);
					}else if(enrollmentDataDTO.getPlanType().equalsIgnoreCase(DENTAL_PLAN)){
						inActiveDentalEnrollmentHistory.add(enrollmentHistoryEntry);
					}
				}
			}
		}else {
			throw new GIException("No Enrollments found for the Household.");
		}
		enrollmentHistory.setActiveHealthEnrollments(activeHealthEnrollmentHistory);
		enrollmentHistory.setActiveDentalEnrollments(activeDentalEnrollmentHistory);
		enrollmentHistory.setInactiveHealthEnrollments(inActiveHealthEnrollmentHistory);
		enrollmentHistory.setInactiveDentalEnrollments(inActiveDentalEnrollmentHistory);
		Collections.sort(enrollmentHistory.getActiveHealthEnrollments(),new ActiveEnrollmentHistoryComparator());
		Collections.sort(enrollmentHistory.getActiveDentalEnrollments(),new ActiveEnrollmentHistoryComparator());
		Collections.sort(enrollmentHistory.getInactiveHealthEnrollments(),new EnrollmentHistoryEntryComparator());
		Collections.sort(enrollmentHistory.getInactiveDentalEnrollments(),new EnrollmentHistoryEntryComparator());
		
		return enrollmentHistory;
	}
	
	private boolean isTerminateDateChangeAllowed(String enrollmentStatus, String coverageEndDate) {
		boolean isTerminateDateChangeAllowed = false;
		
		
		if("Terminated".equalsIgnoreCase(enrollmentStatus)) {
			
			SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			try {
				LocalDate currentDate = TSLocalTime.getLocalDateInstance();
				LocalDate enrollCovEndDate = new DateTime(formatter.parseObject(coverageEndDate)).toLocalDate();
				
				if(enrollCovEndDate.isAfter(currentDate.withDayOfMonth(currentDate.dayOfMonth().getMaximumValue()))) {
					isTerminateDateChangeAllowed = true;
				}

			} catch (ParseException e) {
				LOGGER.error("Parsing error occurred for Enrollment Coverage End date");
			}
		}
		
		return isTerminateDateChangeAllowed;
	}
	
	public boolean isEnrollmentActive(EnrollmentDataDTO enrollmentDataDTO){
		boolean isActive = false;
		
		boolean isBenefitEndDateOnOrAfterToday = false;
		LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
		LocalDate dateToCompare = new DateTime((enrollmentDataDTO.getEnrollmentBenefitEffectiveEndDate())).toLocalDate();
		if(dateToCompare.isAfter(todaysDate) || dateToCompare.isEqual(todaysDate)){
			isBenefitEndDateOnOrAfterToday = true;
		}
		if(ACTIVE_ENROLLMENT_STATUS.contains(enrollmentDataDTO.getEnrollmentStatus()) || (TERM_ENROLLMENT.equalsIgnoreCase(enrollmentDataDTO.getEnrollmentStatus()) && isBenefitEndDateOnOrAfterToday)){
			isActive = true;
		}
		return isActive;
	}
	
	public void determineOtrEligibility(String coverageYear, OTR otr, AccountUser accountUser){
		
		if(!StringUtils.isNumeric(coverageYear) || null==accountUser){
			if(!StringUtils.isNumeric(coverageYear) ){
				LOGGER.error("Coverage year cannot be empty or non-numeric");
				return;
			}
			LOGGER.error("Account user cannot be null");
			return;
		}
		try{
			int householdId = accountUser.getActiveModuleId();
			
			//OTR End date is in the future
			if(isBeforeOrAfterOTRDates(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ONE_TOUCH_RENEWAL_START_DATE), 
					DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ONE_TOUCH_RENEWAL_END_DATE))){				
				return;
			}
			
			//OTR flag is set to the year the tab is clicked on
			if(!coverageYear.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ONE_TOUCH_RENEWAL_YEAR))){
				return;
			}
			int appYear = Integer.parseInt(coverageYear);
			SsapApplicationResource ssapApplicationResource = getApplicationForCoverageYearForPortalHome(new Long(householdId), appYear);
			
			//Check if ssap belongs to the individual
			if(householdId!=ssapApplicationResource.getCmrHouseoldId().intValue()){
				return;
			}
			
			//Check if the app status is EN
			if(!IndividualPortalConstants.SSAPStatus.EN.toString().equalsIgnoreCase(ssapApplicationResource.getApplicationStatus())){
				return;
			}
			
			//Application is not renewed already
			if(RenewalStatus.OTR.toString().equalsIgnoreCase(ssapApplicationResource.getRenewalStatus())){
				otr.setRenewed(true);
				return;				
			}
			
			
			int enrollmentYear = Integer.parseInt(coverageYear);
			
			EnrollmentHistory enrollmentHistory = getEnrollmentHistory(enrollmentYear);
			
			//Check if QHP or QDP exists with 12/31 end date
			if(null!=enrollmentHistory && null!=enrollmentHistory.fullList() && enrollmentHistory.fullList().size()>0){
				SortedSet<LocalDate> coverageEndDates = new TreeSet<LocalDate>();
				coverageEndDates = getSortedCoverageEndDates(enrollmentHistory.fullList());
				LocalDate coverageEndDate = coverageEndDates.last();
				if(null==coverageEndDate || coverageEndDate.getYear()!=Integer.parseInt(coverageYear) || coverageEndDate.getDayOfMonth()!=31 || coverageEndDate.getMonthOfYear() != 12){
					return;
				}
			} else {
				// No QHP or QDP found for current coverage year
				return;
			}
			
			SsapApplicationResource renewalYearApp = getApplicationForCoverageYearForPortalHome(new Long(householdId), appYear+1);
			if(renewalYearApp!=null && IndividualPortalConstants.ACTIVE_STATUS_SSAP.contains(renewalYearApp.getApplicationStatus())){
				otr.setPendingApp(true);
				return;
			}
			
			//Check if no enrollments exist for next year
			EnrollmentHistory enrollmentHistoryForNextYear = getEnrollmentHistory(enrollmentYear+1);
			if(enrollmentHistoryForNextYear.fullList().size()==0){
				otr.setShowOTR(true);
				otr.setCaseNumber(ssapApplicationResource.getCaseNumber());
				otr.setCoverageYear(coverageYear);
			}
		}catch(Exception ex){
			LOGGER.error("Cannot determine the OTR eligibility",ex);
		}
		
	}

		private SortedSet<LocalDate>  getSortedCoverageEndDates(List<EnrollmentHistoryEntry> enrollments) {
			SortedSet<LocalDate> coverageEndDates = new TreeSet<LocalDate>();

			SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			if(CollectionUtils.isNotEmpty(enrollments)) {
				for(EnrollmentHistoryEntry enrollmentHistory : enrollments) {
					if(StringUtils.isNotBlank(enrollmentHistory.getCoverageEndDate())) {
						LocalDate enrollmentEndDate = null;
						try {
							enrollmentEndDate = new DateTime(formatter.parseObject(enrollmentHistory.getCoverageEndDate())).toLocalDate();
						} catch (ParseException ex) {
							LOGGER.error("Enrollment Coverage End date cannot be processed", ex);
							continue;
						}
						coverageEndDates.add(enrollmentEndDate);
					}
				}
			}
			return coverageEndDates;
		}

}
