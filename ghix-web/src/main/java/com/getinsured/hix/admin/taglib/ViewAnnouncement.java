package com.getinsured.hix.admin.taglib;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.admin.service.AnnouncementService;



public class ViewAnnouncement extends TagSupport {
	private AnnouncementService announcementService;
	
	private ApplicationContext applicationContext;
	 
	
	
	private static final long serialVersionUID = 1L;
	private static final String template = "<div class='announcement gray'>" +
											"<h4 class='announcement'>Announcements</h4>" +
											"<label class='error' style=''> " +
											" <MESSAGE> " +
											"</label></div>";
	private static final Logger LOGGER = LoggerFactory.getLogger(ViewAnnouncement.class);
	@Override
    public int doStartTag() throws JspException {
		applicationContext = RequestContextUtils.getWebApplicationContext(
				pageContext.getRequest(),
				pageContext.getServletContext()
			);
		
		announcementService = (AnnouncementService)applicationContext.getBean("announcementService"); 
		
        try {
            JspWriter out = pageContext.getOut();
            List<String> announcementDescList = announcementService.viewAnnouncement();
            int announcementDescListSize = announcementDescList.size();
            String announcement = "";
            announcement += "<ul class='nav nav-list'>";
            for(int i=0; i< announcementDescListSize; i++){
            	announcement += "<li>" + announcementDescList.get(i) + "</li>";
            }
            announcement += "</ul>";
            
            if(announcement != "" ){
            	out.println(ViewAnnouncement.template.replaceAll("<MESSAGE>", announcement));
            }
            
 
        } catch (IOException e) {
           // e.printStackTrace();
        	LOGGER.error("", e);
        }
        return SKIP_BODY;
    }
}
