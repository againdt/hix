package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * I-94 Stamp in Passport Document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class I94InPassportDocument extends DhsDocument
{
  public I94InPassportDocument()
  {
    super.setDocumentType(DocumentType.I94_IN_PASSPORT);
  }

  @JsonProperty("I94Number")
  private String i94Number;

  @JsonProperty("VisaNumber")
  private String visaNumber;

  @JsonProperty("PassportNumber")
  private String passportNumber;

  @JsonProperty("CountryOfIssuance")
  private String countryOfIssuance;

  @JsonProperty("SEVISID")
  private String sevisId;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
    return notBlank(this.i94Number) &&
            notBlank(this.passportNumber) &&
              notBlank(this.countryOfIssuance) &&
                this.docExpirationDate != null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final I94InPassportDocument document = new I94InPassportDocument();

    document.setI94Number(validValue(citizenshipDocument.getI94Number()));
    document.setSevisId(getSevisId(citizenshipDocument));
    document.setVisaNumber(validValue(citizenshipDocument.getVisaNumber()));
    document.setPassportNumber(validValue(citizenshipDocument.getForeignPassportOrDocumentNumber()));
    document.setCountryOfIssuance(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getI94Number()
  {
    return i94Number;
  }

  public void setI94Number(final String i94Number)
  {
    this.i94Number = i94Number;
  }

  public String getVisaNumber()
  {
    return visaNumber;
  }

  public void setVisaNumber(final String visaNumber)
  {
    this.visaNumber = visaNumber;
  }

  public String getPassportNumber()
  {
    return passportNumber;
  }

  public void setPassportNumber(final String passportNumber)
  {
    this.passportNumber = passportNumber;
  }

  public String getCountryOfIssuance()
  {
    return countryOfIssuance;
  }

  public void setCountryOfIssuance(final String countryOfIssuance)
  {
    this.countryOfIssuance = countryOfIssuance;
  }

  public String getSevisId()
  {
    return sevisId;
  }

  public void setSevisId(final String sevisId)
  {
    this.sevisId = sevisId;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", I94InPassportDocument.class.getSimpleName() + "[", "]")
        .add("i94Number='" + i94Number + "'")
        .add("visaNumber='" + visaNumber + "'")
        .add("passportNumber='" + passportNumber + "'")
        .add("countryOfIssuance='" + countryOfIssuance + "'")
        .add("sevisId='" + sevisId + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
