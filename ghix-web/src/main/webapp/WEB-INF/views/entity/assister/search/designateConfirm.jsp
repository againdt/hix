<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<script src="<c:url value="/resources/js/jquery.validate.min.js" />" type="text/javascript"></script>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<style>
body {background: #fff !important;}
</style>

<div id="broker-password-confirm">
	<div class="gutter10">    
		<div class="row-fluid">
			<div class="span12">
				<h1><spring:message code="label.counselor.success"/></h1>
				<div class="row-fluid">
					<div class="alert alert-info span12">
						<p class="pull-left"><spring:message code="label.counselor.successfullyselected"/>&nbsp;${assisterName}.</p>			
					</div>
				</div>
			</div>
		</div>	
		
		<div>		
			<a href="<c:url value="/entity/locateassister/searchentities" />" class="btn btn-primary pull-left"><spring:message code="label.assister.designatemessage.btn"/></a>
			<c:choose>
				<c:when test="${CA_STATE_CODE}">
					<input name="close" type="button" onClick="window.close();" value="<spring:message code='label.entity.close'/>" class="btn btn-primary pull-right" />
				</c:when>
				<c:otherwise>
					<input name="close" type="button" onClick="window.parent.location.reload();window.parent.closeSearchLightBox();" value="<spring:message code='label.entity.close'/>" class="btn btn-primary pull-right" />			
				</c:otherwise>
			</c:choose>
 		</div>
	</div>
</div>
