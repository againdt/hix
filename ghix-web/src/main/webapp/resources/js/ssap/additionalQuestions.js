var addQuetionsHouseHoldDone = 1;
function addAdditionalQuetionDataToTable()
{	
	var willHealthCoverageThroughJob;
	var houseHoldHealthCoverage;
	var otherHealthCoverage;
	var appscr76P2PhysicianIndicator;
	
	var appendColumn;
	
	for(var i=1; i<=noOfHouseHold; i++)
	{	
		if($('#appscr76P1UninsuredLast6MonthTD'+i).html() != undefined)
		{
		
			updateAdditionalQuetionDataToTable(i);
			continue;
		}
		
		willHealthCoverageThroughJob = $('input[name=willHealthCoverageThroughJob]:radio:checked').val();
		houseHoldHealthCoverage = $('#appscr76P1CheckBoxForEmployeeName').prop("checked");
		otherHealthCoverage = $('#appscr76P1CheckBoxForOtherEmployee').prop("checked");
		
		appendColumn = "";
		
		appendColumn+="<td class='76P1_uninsuredLast6Month' id='appscr76P1UninsuredLast6MonthTD"+i+"'>"+$('input[name=wasUninsuredFromLast6Month]:radio:checked').val()+"</td>";
		appendColumn+="<td class='76P1_isHealthCoverage' id='appscr76P1HealthCoverageTD"+i+"'>"+$('input[name=isHealthCoverageThroughJob]:radio:checked').val()+"</td>";
		appendColumn+="<td class='76P1_willHealthCoverage' id='appscr76P1WillHealthCoverageTD"+i+"'>"+willHealthCoverageThroughJob+"</td>";
		
		if(willHealthCoverageThroughJob == "yes" || willHealthCoverageThroughJob == "Yes" || willHealthCoverageThroughJob == "YES")
			appendColumn+="<td class='76P1_healthCoverageDate' id='appscr76P1HealthCoverageDateTD"+i+"'>"+$('#appscr76P1HealthCoverageDate').val()+"</td>";
		else
			appendColumn+="<td class='76P1_healthCoverageDate' id='appscr76P1HealthCoverageDateTD"+i+"'></td>";
		
		if(houseHoldHealthCoverage == true || houseHoldHealthCoverage == "true")
		{
			appendColumn+="<td class='76P1_houseHoldEmployer' id='appscr76P1HouseHoldEmployerTD"+i+"'>"+houseHoldHealthCoverage+"</td>";
			
			appendColumn+="<td class='76P1_employerContactName' id='appscr76P1EmployerContactNameTD"+i+"'>"+$('#appscr76P1EmployerContactName').val()+"</td>";
			appendColumn+="<td class='76P1_firstPhoneNumber' id='appscr76P1FirstPhoneNumberTD"+i+"'>"+$('#appscr76P1_firstPhoneNumber').val()+"</td>";
			appendColumn+="<td class='76P1_firstExt' id='appscr76P1FirstExtTD"+i+"'>"+$('#appscr76P1_firstExt').val()+"</td>";
			appendColumn+="<td class='76P1_firstPhoneType' id='appscr76P1FirstPhoneTypeTD"+i+"'>"+$('#appscr76P1_firstPhoneType').val()+"</td>";
			appendColumn+="<td class='76P1_emailAddress' id='appscr76P1EmailAddressTD"+i+"'>"+$('#appscr76P1_emailAddress').val()+"</td>";
		}
		else
		{
			appendColumn+="<td class='76P1_houseHoldEmployer' id='appscr76P1HouseHoldEmployerTD"+i+"'>false</td>";
			
			appendColumn+="<td class='76P1_employerContactName' id='appscr76P1EmployerContactNameTD"+i+"'></td>";
			appendColumn+="<td class='76P1_firstPhoneNumber' id='appscr76P1FirstPhoneNumberTD"+i+"'></td>";
			appendColumn+="<td class='76P1_firstExt' id='appscr76P1FirstExtTD"+i+"'></td>";
			appendColumn+="<td class='76P1_firstPhoneType' id='appscr76P1FirstPhoneTypeTD"+i+"'>0</td>";
			appendColumn+="<td class='76P1_emailAddress' id='appscr76P1EmailAddressTD"+i+"'></td>";
		}
		
		if(otherHealthCoverage == true || otherHealthCoverage == "true")
		{
			appendColumn+="<td class='76P1_checkBoxForOtherEmployee' id='appscr76P1CheckBoxForOtherEmployeeTD"+i+"'>"+otherHealthCoverage+"</td>";
			
			appendColumn+="<td class='76P1_otherEmployerName' id='appscr76P1OtherEmployerNameTD"+i+"'>"+$('#appscr76P1_otherEmployerName').val()+"</td>";
			appendColumn+="<td class='76P1_employerEIN' id='appscr76P1EmployerEINTD"+i+"'>"+$('#appscr76P1_employerEIN').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerAddress1' id='appscr76P1OtherEmployerAddress1TD"+i+"'>"+$('#appscr76P1_otherEmployerAddress1').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerAddress2' id='appscr76P1OtherEmployerAddress2TD"+i+"'>"+$('#appscr76P1_otherEmployerAddress2').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerCity' id='appscr76P1OtherEmployerCityTD"+i+"'>"+$('#appscr76P1_otherEmployerCity').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerZip' id='appscr76P1OtherEmployerZipTD"+i+"'>"+$('#appscr76P1_otherEmployerZip').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerState' id='appscr76P1OtherEmployerStateTD"+i+"'>"+$('#appscr76P1_otherEmployerState').val()+"</td>";
			appendColumn+="<td class='76P1_secondPhoneNumber' id='appscr76P1SecondPhoneNumberTD"+i+"'>"+$('#appscr76P1_secondPhoneNumber').val()+"</td>";
			appendColumn+="<td class='76P1_secondExt' id='appscr76P1SecondExtTD"+i+"'>"+$('#appscr76P1_secondExt').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerContactName' id='appscr76P1OtherEmployerContactNameTD"+i+"'>"+$('#appscr76P1_otherEmployerContactName').val()+"</td>";
			appendColumn+="<td class='76P1_thirdPhoneNumber' id='appscr76P1ThirdPhoneNumberTD"+i+"'>"+$('#appscr76P1_thirdPhoneNumber').val()+"</td>";
			appendColumn+="<td class='76P1_thirdExt' id='appscr76P1ThirdExtTD"+i+"'>"+$('#appscr76P1_thirdExt').val()+"</td>";
			appendColumn+="<td class='76P1_thirdPhoneType' id='appscr76P1ThirdPhoneTypeTD"+i+"'>"+$('#appscr76P1_thirdPhoneType').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerEmailAddress' id='appscr76P1OtherEmployerEmailAddressTD"+i+"'>"+$('#appscr76P1_otherEmployer_emailAddress').val()+"</td>";
		}
		else
		{
			appendColumn+="<td class='76P1_checkBoxForOtherEmployee' id='appscr76P1CheckBoxForOtherEmployeeTD"+i+"'>false</td>";
			
			appendColumn+="<td class='76P1_otherEmployerName' id='appscr76P1OtherEmployerNameTD"+i+"'></td>";
			appendColumn+="<td class='76P1_employerEIN' id='appscr76P1EmployerEINTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerAddress1' id='appscr76P1OtherEmployerAddress1TD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerAddress2' id='appscr76P1OtherEmployerAddress2TD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerCity' id='appscr76P1OtherEmployerCityTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerZip' id='appscr76P1OtherEmployerZipTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerState' id='appscr76P1OtherEmployerStateTD"+i+"'></td>";
			appendColumn+="<td class='76P1_secondPhoneNumber' id='appscr76P1SecondPhoneNumberTD"+i+"'></td>";
			appendColumn+="<td class='76P1_secondExt' id='appscr76P1SecondExtTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerContactName' id='appscr76P1OtherEmployerContactNameTD"+i+"'></td>";
			appendColumn+="<td class='76P1_thirdPhoneNumber' id='appscr76P1ThirdPhoneNumberTD"+i+"'></td>";
			appendColumn+="<td class='76P1_thirdExt' id='appscr76P1ThirdExtTD"+i+"'></td>";
			appendColumn+="<td class='76P1_thirdPhoneType' id='appscr76P1ThirdPhoneTypeTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerEmailAddress' id='appscr76P1OtherEmployerEmailAddressTD"+i+"'></td>";
		}
		
		/*	PageId 76Part1 ended	 */
		
		/*	PageId 76Part2 started	 */
		
		appscr76P2PhysicianIndicator = $('input[name=haveApPrimaryCarePhysician]:radio:checked').val();
		
		appendColumn+="<td class='76P2_enrolledFollowing' id='appscr76P2EnrolledFollowingTD"+i+"'>"+$('input[name=appscr76p2radiosgroup]:radio:checked').val()+"</td>";		
		/*appendColumn+="<td class='76P2_physicianIndicator' id='appscr76P2PhysicianIndicatorTD"+i+"'>"+appscr76P2PhysicianIndicator+"</td>";*/
		if(appscr76P2PhysicianIndicator == 'yes' || appscr76P2PhysicianIndicator == 'Yes')
			appendColumn+="<td class='76P2_physicianName' id='appscr76P2PhysicianNameTD"+i+"'>"+$('#appscr76P2_physicianName').val()+"</td>";
		else
			appendColumn+="<td class='76P2_physicianName' id='appscr76P2PhysicianNameTD"+i+"'></td>";		
		appendColumn+="<td class='76P2_enrolledCoverageForCoverage' id='appscr76P2EnrollEdCoverageForCoverageTD"+i+"'>"+$('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val()+"</td>";
		
		/*	PageId 76Part2 ended	 */
		
		
		/*	PageId 77Part1 Started   */
		
		
		appendColumn+="<td class='77P1CurrentlyEnrolledInHealthPlan' id='appscr77P1CurrentlyEnrolledInHealthPlanTD"+i+"'>"+$('input[name=77P1CurrentlyEnrolledInHealthPlan]:radio:checked').val()+"</td>";
		
		var temp = $('input[name=77P1WillBeEnrolledInHealthPlan]:radio:checked').val();
		appendColumn+="<td class='77P1WillBeEnrolledInHealthPlan' id='appscr77P1WillBeEnrolledInHealthPlanTD"+i+"'>"+temp+"</td>";
		if(temp == "yes")
			appendColumn+="<td class='77P1WillBeEnrolledInHealthPlanDate' id='appscr77P1WillBeEnrolledInHealthPlanDateTD"+i+"'>"+$('#77P1WillBeEnrolledInHealthPlanDate').val()+"</td>";
		else
			appendColumn+="<td class='77P1WillBeEnrolledInHealthPlanDate' id='appscr77P1WillBeEnrolledInHealthPlanDateTD"+i+"'></td>";
		
		temp = $('input[name=77P1ExpectChangesToHealthCoverage]:radio:checked').val();
		appendColumn+="<td class='77P1ExpectChangesToHealthCoverage' id='appscr77P1ExpectChangesToHealthCoverageTD"+i+"'>"+temp+"</td>";
		if(temp == "no")
		{
			appendColumn+="<td class='77P1WillNoLongerHealthCoverage' id='appscr77P1WillNoLongerHealthCoverageTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1NoLongerHealthCoverageDate' id='appscr77P1NoLongerHealthCoverageDateTD"+i+"'></td>";
			
			appendColumn+="<td class='77P1PlanToDropHealthCoverage' id='appscr77P1PlanToDropHealthCoverageTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1PlanToDropHealthCoverageDate' id='appscr77P1PlanToDropHealthCoverageDateTD"+i+"'></td>";
			
			appendColumn+="<td class='77P1WillOfferCoverage' id='appscr77P1WillOfferCoverageTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1WillOfferCoverageDate' id='appscr77P1WillOfferCoverageDateTD"+i+"'></td>";
			
			appendColumn+="<td class='77P1PlaningToEnrollInHC' id='appscr77P1PlaningToEnrollInHCTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1PlaningToEnrollInHCDate' id='appscr77P1PlaningToEnrollInHCDateTD"+i+"'></td>";
			
			appendColumn+="<td class='77P1HealthPlanOptionsGoingToChange' id='appscr77P1HealthPlanOptionsGoingToChangeTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1HealthPlanOptionsGoingToChangeDate' id='appscr77P1HealthPlanOptionsGoingToChangeDateTD"+i+"'></td>";			
		}
		else
		{
			temp = $('#77P1WillNoLongerHealthCoverage').prop("checked");
			appendColumn+="<td class='77P1WillNoLongerHealthCoverage' id='appscr77P1WillNoLongerHealthCoverageTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1NoLongerHealthCoverageDate' id='appscr77P1NoLongerHealthCoverageDateTD"+i+"'>"+$('#77P1NoLongerHealthCoverageDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1NoLongerHealthCoverageDate' id='appscr77P1NoLongerHealthCoverageDateTD"+i+"'></td>";
			
			temp = $('#77P1PlanToDropHealthCoverage').prop("checked");
			appendColumn+="<td class='77P1PlanToDropHealthCoverage' id='appscr77P1PlanToDropHealthCoverageTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1PlanToDropHealthCoverageDate' id='appscr77P1PlanToDropHealthCoverageDateTD"+i+"'>"+$('#77P1PlanToDropHealthCoverageDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1PlanToDropHealthCoverageDate' id='appscr77P1PlanToDropHealthCoverageDateTD"+i+"'></td>";
			
			temp = $('#77P1WillOfferCoverage').prop("checked");
			appendColumn+="<td class='77P1WillOfferCoverage' id='appscr77P1WillOfferCoverageTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1WillOfferCoverageDate' id='appscr77P1WillOfferCoverageDateTD"+i+"'>"+$('#77P1WillOfferCoverageDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1WillOfferCoverageDate' id='appscr77P1WillOfferCoverageDateTD"+i+"'></td>";
			
			temp = $('#77P1PlaningToEnrollInHC').prop("checked");
			appendColumn+="<td class='77P1PlaningToEnrollInHC' id='appscr77P1PlaningToEnrollInHCTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1PlaningToEnrollInHCDate' id='appscr77P1PlaningToEnrollInHCDateTD"+i+"'>"+$('#77P1PlaningToEnrollInHCDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1PlaningToEnrollInHCDate' id='appscr77P1PlaningToEnrollInHCDateTD"+i+"'></td>";
			
			temp = $('#77P1HealthPlanOptionsGoingToChange').prop("checked");
			appendColumn+="<td class='77P1HealthPlanOptionsGoingToChange' id='appscr77P1HealthPlanOptionsGoingToChangeTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1HealthPlanOptionsGoingToChangeDate' id='appscr77P1HealthPlanOptionsGoingToChangeDateTD"+i+"'>"+$('#77P1HealthPlanOptionsGoingToChangeDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1HealthPlanOptionsGoingToChangeDate' id='appscr77P1HealthPlanOptionsGoingToChangeDateTD"+i+"'></td>";
		}
		
		
		/* PageId 77Part1 ended		 */
		
		/* PageId 77Part2 Started 	 */
		
		
		appendColumn+="<td class='77P2CurrentLowestCostSelfOnlyPlanName' id='appscr77P2CurrentLowestCostSelfOnlyPlanNameTD"+i+"'>"+$('#currentLowestCostSelfOnlyPlanName').val()+"</td>";
		appendColumn+="<td class='77P2CurrentPlanMeetsMinimumStandard' id='appscr77P2CurrentPlanMeetsMinimumStandardTD"+i+"'>"+$('#currentPlanMeetsMinimumStandard').prop("checked")+"</td>";
		
		appendColumn+="<td class='77P2ComingLowestCostSelfOnlyPlanName' id='appscr77P2ComingLowestCostSelfOnlyPlanNameTD"+i+"'>"+$('#comingLowestCostSelfOnlyPlanName').val()+"</td>";
		appendColumn+="<td class='77P2ComingPlanMeetsMinimumStandard' id='appscr77P2ComingPlanMeetsMinimumStandardTD"+i+"'>"+$('#comingPlanMeetsMinimumStandard').prop("checked")+"</td>";
		
		appendColumn+="<td class='77P2LowestCostSelfOnlyPremiumAmount' id='appscr77P2LowestCostSelfOnlyPremiumAmountTD"+i+"'>"+$('#lowestCostSelfOnlyPremiumAmount').val()+"</td>";
		appendColumn+="<td class='77P2LowestCostSelfOnlyPremiumFrequency' id='appscr77P2LowestCostSelfOnlyPremiumFrequencyTD"+i+"'>"+$('#lowestCostSelfOnlyPremiumFrequency').val()+"</td>";
		
		
		/* PageId 77Part2 ended		 */
		
		
		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendColumn);	
		
	}
	
	//next();
}

function updateAdditionalQuetionDataToTable(i)
{
		/*	PageId 76Part1 started	 */
		
		$('#appscr76P1UninsuredLast6MonthTD'+i).text($('input[name=wasUninsuredFromLast6Month]:radio:checked').val());
		$('#appscr76P1HealthCoverageTD'+i).text($('input[name=isHealthCoverageThroughJob]:radio:checked').val());
		$('#appscr76P1WillHealthCoverageTD'+i).text($('input[name=willHealthCoverageThroughJob]:radio:checked').val());
		$('#appscr76P1HealthCoverageDateTD'+i).text($('#appscr76P1HealthCoverageDate').val());
		$('#appscr76P1HouseHoldEmployerTD'+i).text($('#appscr76P1CheckBoxForEmployeeName').prop("checked"));
		$('#appscr76P1EmployerContactNameTD'+i).text($('#appscr76P1EmployerContactName').val());
		$('#appscr76P1FirstPhoneNumberTD'+i).text($('#appscr76P1_firstPhoneNumber').val());
		$('#appscr76P1FirstExtTD'+i).text($('#appscr76P1_firstExt').val());
		$('#appscr76P1FirstPhoneTypeTD'+i).text($('#appscr76P1_firstPhoneType').val());
		$('#appscr76P1EmailAddressTD'+i).text($('#appscr76P1_emailAddress').val());
		$('#appscr76P1CheckBoxForOtherEmployeeTD'+i).text($('#appscr76P1CheckBoxForOtherEmployee').prop("checked"));
		$('#appscr76P1OtherEmployerNameTD'+i).text($('#appscr76P1_otherEmployerName').val());
		$('#appscr76P1EmployerEINTD'+i).text($('#appscr76P1_employerEIN').val());
		$('#appscr76P1OtherEmployerAddress1TD'+i).text($('#appscr76P1_otherEmployerAddress1').val());
		$('#appscr76P1OtherEmployerAddress2TD'+i).text($('#appscr76P1_otherEmployerAddress2').val());
		$('#appscr76P1OtherEmployerCityTD'+i).text($('#appscr76P1_otherEmployerCity').val());
		$('#appscr76P1OtherEmployerZipTD'+i).text($('#appscr76P1_otherEmployerZip').val());
		$('#appscr76P1OtherEmployerStateTD'+i).text($('#appscr76P1_otherEmployerState').val());
		$('#appscr76P1SecondPhoneNumberTD'+i).text($('#appscr76P1_secondPhoneNumber').val());
		$('#appscr76P1SecondExtTD'+i).text($('#appscr76P1_secondExt').val());
		$('#appscr76P1OtherEmployerContactNameTD'+i).text($('#appscr76P1_otherEmployerContactName').val());
		$('#appscr76P1ThirdPhoneNumberTD'+i).text($('#appscr76P1_thirdPhoneNumber').val());
		$('#appscr76P1ThirdExtTD'+i).text($('#appscr76P1_thirdExt').val());
		$('#appscr76P1ThirdPhoneTypeTD'+i).text($('#appscr76P1_thirdPhoneType').val());
		$('#appscr76P1OtherEmployerEmailAddressTD'+i).text($('#appscr76P1_otherEmployer_emailAddress').val());
		
		/*	PageId 76Part1 ended	 */
		
		/*	PageId 76Part2 started	 */
		
		$('#appscr76P2EnrolledFollowingTD'+i).text($('input[name=appscr76p2radiosgroup]:radio:checked').val());
		/*$('#appscr76P2PhysicianIndicatorTD'+i).text($('input[name=haveApPrimaryCarePhysician]:radio:checked').val());*/
		$('#appscr76P2PhysicianNameTD'+i).text($('#appscr76P2_physicianName').val());
		$('#appscr76P2EnrollEdCoverageForCoverageTD'+i).text($('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val());
		
		/*	PageId 76Part2 ended	 */
	
}

function appscr76FromMongodb()
{
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();	
	if(useJSON == true){
		appscr76FromMongodbJSON(currentID);
		return;
	}
	
	/*	PageId 76Part1 started	 
	var temp = $('#appscr76P1UninsuredLast6MonthTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#wasUninsuredFromLast6MonthYes').prop("checked", true);
	else
		$('#wasUninsuredFromLast6MonthNo').prop("checked", true);
	
	
	temp = $('#appscr76P1HealthCoverageTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#isHealthCoverageThroughJobYes').prop("checked", true);
	else
		$('#isHealthCoverageThroughJobNo').prop("checked", true);
	
	
	temp = $('#appscr76P1WillHealthCoverageTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#willHealthCoverageThroughJobYes').prop("checked", true);
	else
		$('#willHealthCoverageThroughJobNo').prop("checked", true);
	
	
	$('#appscr76P1HealthCoverageDate').val($('#appscr76P1HealthCoverageDateTD1').text());
	
	
	temp = $('#appscr76P1HouseHoldEmployerTD1').text();
	if(temp == "true")
	{
		document.getElementById('appscr76P1CheckBoxForEmployeeName').checked = true;
		$("#memberNameDataDivPart1").show();
	}
	else
	{
		document.getElementById('appscr76P1CheckBoxForEmployeeName').checked = false;
		$("#memberNameDataDivPart1").hide();
	}
	
	
	$('#appscr76P1EmployerContactName').val($('#appscr76P1EmployerContactNameTD1').text());
	$('#appscr76P1_firstPhoneNumber').val($('#appscr76P1FirstPhoneNumberTD1').text());
	$('#appscr76P1_firstExt').val($('#appscr76P1FirstExtTD1').text());
	$('#appscr76P1_firstPhoneType').val($('#appscr76P1FirstPhoneTypeTD1').text());
	$('#appscr76P1_emailAddress').val($('#appscr76P1EmailAddressTD1').text());
	
	temp = $('#appscr76P1CheckBoxForOtherEmployeeTD1').text();
	if(temp == "true")
	{
		document.getElementById('appscr76P1CheckBoxForOtherEmployee').checked = true;
		$("#memberNameDataDivPart2").show();
	}
	else
	{
		document.getElementById('appscr76P1CheckBoxForOtherEmployee').checked = false;
		$("#memberNameDataDivPart2").hide();
	}
		
	$('#appscr76P1_otherEmployerName').val($('#appscr76P1OtherEmployerNameTD1').text());
	$('#appscr76P1_employerEIN').val($('#appscr76P1EmployerEINTD1').text());
	$('#appscr76P1_otherEmployerAddress1').val($('#appscr76P1OtherEmployerAddress1TD1').text());
	$('#appscr76P1_otherEmployerAddress2').val($('#appscr76P1OtherEmployerAddress2TD1').text());
	$('#appscr76P1_otherEmployerCity').val($('#appscr76P1OtherEmployerCityTD1').text());
	$('#appscr76P1_otherEmployerZip').val($('#appscr76P1OtherEmployerZipTD1').text());
	$('#appscr76P1_otherEmployerState').val($('#appscr76P1OtherEmployerStateTD1').text());
	$('#appscr76P1_secondPhoneNumber').val($('#appscr76P1SecondPhoneNumberTD1').text());
	$('#appscr76P1_secondExt').val($('#appscr76P1SecondExtTD1').text());
	$('#appscr76P1_otherEmployerContactName').val($('#appscr76P1OtherEmployerContactNameTD1').text());
	$('#appscr76P1_thirdPhoneNumber').val($('#appscr76P1ThirdPhoneNumberTD1').text());
	$('#appscr76P1_thirdExt').val($('#appscr76P1ThirdExtTD1').text());
	$('#appscr76P1_thirdPhoneType').val($('#appscr76P1ThirdPhoneTypeTD1').text());
	$('#appscr76P1_otherEmployer_emailAddress').val($('#appscr76P1OtherEmployerEmailAddressTD1').text());
	
		PageId 76Part1 ended	 
	
		PageId 76Part2 started	 
	
	temp = $('#appscr76P2EnrolledFollowingTD1').text();
	if(temp == "corba")
		$('#appscr76P2_cobra').prop("checked", true);
	if(temp == "retire health plan")
		$('#appscr76P2_retireHealthPlan').prop("checked", true);
	if(temp == "veterans health program")
		$('#appscr76P2_veteransProgram').prop("checked", true);
	else if(temp == "none")
		$('#appscr76P2_noneOfAbove').prop("checked", true);
	
	
	temp = $('#appscr76P2PhysicianIndicatorTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#appscr76P2_physicianCareIndicatorYes').prop("checked", true);
	else
		$('#appscr76P2_physicianCareIndicatorNo').prop("checked", true);
	
	$('#appscr76P2_physicianName').val($('#appscr76P2PhysicianNameTD1').text());
	
	
	temp = $('#appscr76P2EnrollEdCoverageForCoverageTD1').text();
	if(temp == "corba")
		$('#appscr76P2_cobraForCoverage').prop("checked", true);
	if(temp == "retire health plan")
		$('#appscr76P2_RHPForCoverage').prop("checked", true);
	if(temp == "veterans health program")
		$('#appscr76P2_VHPForCoverage').prop("checked", true);
	else if(temp == "none")
		$('#appscr76P2_NOTAForCoverage').prop("checked", true);
		
		PageId 76Part2 ended	 
	
		PageId 77Part1 started	 
	
	temp = $('#appscr77P1CurrentlyEnrolledInHealthPlanTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#77P1CurrentlyEnrolledInHealthPlanYes').prop("checked", true);
	else
		$('#77P1CurrentlyEnrolledInHealthPlanNo').prop("checked", true);
	
	
	temp = $('#appscr77P1WillBeEnrolledInHealthPlanTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#77P1WillBeEnrolledInHealthPlanYes').prop("checked", true);
	else
		$('#77P1WillBeEnrolledInHealthPlanNo').prop("checked", true);
	
	temp = $('#appscr77P1WillBeEnrolledInHealthPlanDateTD1').text();
	$('#77P1WillBeEnrolledInHealthPlanDate').val(temp);
	
	
	temp = $('#appscr77P1ExpectChangesToHealthCoverageTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#77P1ExpectChangesToHealthCoverageYes').prop("checked", true);
	else
		$('#77P1ExpectChangesToHealthCoverageNo').prop("checked", true);
	
	
	temp = $('#appscr77P1WillNoLongerHealthCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1WillNoLongerHealthCoverage').checked = true;
	else
		document.getElementById('77P1WillNoLongerHealthCoverage').checked = false;
	
	temp = $('#appscr77P1NoLongerHealthCoverageDateTD1').text();
	$('#77P1NoLongerHealthCoverageDate').val(temp);
	
	
	temp = $('#appscr77P1PlanToDropHealthCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1PlanToDropHealthCoverage').checked = true;
	else
		document.getElementById('77P1PlanToDropHealthCoverage').checked = false;
	
	temp = $('#appscr77P1PlanToDropHealthCoverageDateTD1').text();
	$('#77P1PlanToDropHealthCoverageDate').val(temp);
	
	
	temp = $('#appscr77P1WillOfferCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1WillOfferCoverage').checked = true;
	else
		document.getElementById('77P1WillOfferCoverage').checked = false;
	
	temp = $('#appscr77P1WillOfferCoverageDateTD1').text();
	$('#77P1WillOfferCoverageDate').val(temp);
	
	
	temp = $('#appscr77P1PlaningToEnrollInHCTD1').text();
	if(temp == "true")
		document.getElementById('77P1PlaningToEnrollInHC').checked = true;
	else
		document.getElementById('77P1PlaningToEnrollInHC').checked = false;
	
	temp = $('#appscr77P1PlaningToEnrollInHCDateTD1').text();
	$('#77P1PlaningToEnrollInHCDate').val(temp);
	
	
	temp = $('#appscr77P1HealthPlanOptionsGoingToChangeTD1').text();
	if(temp == "true")
		document.getElementById('77P1HealthPlanOptionsGoingToChange').checked = true;
	else
		document.getElementById('77P1HealthPlanOptionsGoingToChange').checked = false;
	
	temp = $('#appscr77P1HealthPlanOptionsGoingToChangeDateTD1').text();
	$('#77P1HealthPlanOptionsGoingToChangeDate').val(temp);*/
	
	
	/*	PageId 77Part1 ended	 */
	
	/*	PageId 77Part2 started	 */
	
	
	/*temp = $('#appscr77P2CurrentLowestCostSelfOnlyPlanNameTD1').text();
	//$('#currentLowestCostSelfOnlyPlanName').val(temp);
	
	temp = $('#appscr77P2CurrentPlanMeetsMinimumStandardTD1').text();
	if(temp == "true")
		document.getElementById('currentPlanMeetsMinimumStandard').checked = true;
	else
		document.getElementById('currentPlanMeetsMinimumStandard').checked = false;
	
	
	temp = $('#appscr77P2ComingLowestCostSelfOnlyPlanNameTD1').text();
	//$('#comingLowestCostSelfOnlyPlanName').val(temp);
	
	temp = $('#appscr77P2ComingPlanMeetsMinimumStandardTD1').text();
	if(temp == "tr" +
			"ue")
		document.getElementById('comingPlanMeetsMinimumStandard').checked = true;
	else
		document.getElementById('comingPlanMeetsMinimumStandard').checked = false;
	
	
	temp = $('#appscr77P2LowestCostSelfOnlyPremiumAmountTD1').text();
	$('#lowestCostSelfOnlyPremiumAmount').val(temp);
	
	
	temp = $('#appscr77P2LowestCostSelfOnlyPremiumFrequencyTD1').text();
	$('#lowestCostSelfOnlyPremiumFrequency').val(temp);*/
	
	/*	PageId 77Part2 ended	 */
}


function addDataAppscr78ToTable()
{	
	var appendColumn;
	
	for(var i=1; i<=noOfHouseHold; i++)
	{	
		if($('#appscr78MedicareTD'+i).html() != undefined)
		{
			updateDataAppscr78ToTable(i);
			continue;
		}
		
		appendColumn = "";
		
		if(document.getElementById("appscr78Medicare").checked == true)
			appendColumn+="<td class='P78Medicare' id='appscr78MedicareTD"+i+"'>"+"yes"+"</td>";
		else
			appendColumn+="<td class='P78Medicare' id='appscr78MedicareTD"+i+"'>"+"no"+"</td>";
		
		
		if(document.getElementById("appscr78Tricare").checked == true)
		appendColumn+="<td class='P78Tricare' id='appscr78TricareTD"+i+"'>"+"yes"+"</td>";
		else
			appendColumn+="<td class='P78Tricare' id='appscr78TricareTD"+i+"'>"+"no"+"</td>";
		
		if(document.getElementById("appscr78PeaceCorps").checked == true)
			appendColumn+="<td class='P78PeaceCorps' id='appscr78PeaceCorpsTD"+i+"'>"+"yes"+"</td>";
		else
			appendColumn+="<td class='P78PeaceCorps' id='appscr78PeaceCorpsTD"+i+"'>"+"no"+"</td>";
		
		
		if(document.getElementById("appscr78OtherStateFHBP").checked == true)
		{
		appendColumn+="<td class='P78OtherStateFHBP' id='appscr78OtherStateFHBPTD"+i+"'>"+"yes"+"</td>";
		appendColumn+="<td class='P78TypeOfProgram' id='appscr78TypeOfProgramTD"+i+"'>"+$('#appscr78TypeOfProgram').val()+"</td>";
		appendColumn+="<td class='P78NameOfProgram' id='appscr78NameOfProgramTD"+i+"'>"+$('#appscr78NameOfProgram').val()+"</td>";
		}
		else
		{
			appendColumn+="<td class='P78OtherStateFHBP' id='appscr78OtherStateFHBPTD"+i+"'>"+"no"+"</td>";
			appendColumn+="<td class='P78TypeOfProgram' id='appscr78TypeOfProgramTD"+i+"'>"+"N/A"+"</td>";
			appendColumn+="<td class='P78NameOfProgram' id='appscr78NameOfProgramTD"+i+"'>"+"N/A"+"</td>";
		}
	
		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendColumn);
	}
	
	next();
}

function updateDataAppscr78ToTable(i)
{
		if(document.getElementById("appscr78Medicare").checked == true)
			$('#HouseHold'+i).find(".P78Medicare").html("yes");
		else
			$('#HouseHold'+i).find(".P78Medicare").html("no");
		
		
		if(document.getElementById("appscr78Tricare").checked == true)
			$('#HouseHold'+i).find(".P78Tricare").html("yes");
		else
			$('#HouseHold'+i).find(".P78Tricare").html("no"); 
		
		if(document.getElementById("appscr78PeaceCorps").checked == true)
			$('#HouseHold'+i).find(".P78PeaceCorps").html("yes");
		else
			$('#HouseHold'+i).find(".P78PeaceCorps").html("no");  
		
		
		if(document.getElementById("appscr78OtherStateFHBP").checked == true)
		{
			$('#HouseHold'+i).find(".P78OtherStateFHBP").html("yes"); 
					
			$('#HouseHold'+i).find(".P78TypeOfProgram").html($('#appscr78TypeOfProgram').val());
			
			$('#HouseHold'+i).find(".P78NameOfProgram").html($('#appscr78NameOfProgram').val());
				
		}
		else
		{
			$('#HouseHold'+i).find(".P78OtherStateFHBP").html("no"); 
			
			$('#HouseHold'+i).find(".P78TypeOfProgram").html("N/A");
			
			$('#HouseHold'+i).find(".P78NameOfProgram").html("N/A");
		
		}
}

function fillDataAppscr78ToTable()
{
	var temp = "";
	
	temp = $('#appscr78MedicareTD1').text();
	if(temp == "yes")
		document.getElementById('appscr78Medicare').checked = true;
	else
		document.getElementById('appscr78Medicare').checked = false;
	
	
	temp = $('#appscr78TricareTD1').text();
	if(temp == "yes")
		document.getElementById('appscr78Tricare').checked = true;
	else
		document.getElementById('appscr78Tricare').checked = false;
	
	
	temp = $('#appscr78PeaceCorpsTD1').text();
	if(temp == "yes")
		document.getElementById('appscr78PeaceCorps').checked = true;
	else
		document.getElementById('appscr78PeaceCorps').checked = false;
	
	
	temp = $('#appscr78OtherStateFHBPTD1').text();
	if(temp == "yes")
		document.getElementById('appscr78OtherStateFHBP').checked = true;
	else
		document.getElementById('appscr78OtherStateFHBP').checked = false;
	
	
	temp = $('#appscr78TypeOfProgramTD1').text();
	$('#appscr78TypeOfProgram').val(temp);
	
	
	temp = $('#appscr78NameOfProgramTD1').text();
	$('#appscr78NameOfProgram').val(temp);
}

function addDataAppscr79ToTable()
{	
	var appendColumn;
	var isMemberFRTribeIndicator = "";
	
	for(var i=1; i<=noOfHouseHold; i++)
	{	
		if($('#appscr79IsMemberFRTribeTD'+i).html() != undefined)
		{
			updateDataAppscr79ToTable(i);
			continue;
		}
		
		appendColumn = "";
		
		isMemberFRTribeIndicator = $('input[name=AmericonIndianQuestionRadio]:radio:checked').val();
		appendColumn+="<td class='P79IsMemberFRTribe' id='appscr79IsMemberFRTribeTD"+i+"'>"+isMemberFRTribeIndicator+"</td>";
		
		if(isMemberFRTribeIndicator == "yes")
		{
			appendColumn+="<td class='P79State' id='appscr79StateTD"+i+"'>"+$('#appscr79State').val()+"</td>";
			appendColumn+="<td class='P79TribeName' id='appscr79TribeNameTD"+i+"'>"+$('#appscr79TribeName').val()+"</td>";
		}
		else
		{
			appendColumn+="<td class='P79State' id='appscr79StateTD"+i+"'></td>";
			appendColumn+="<td class='P79TribeName' id='appscr79TribeNameTD"+i+"'>TribeName</td>";
		}
		
		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendColumn);
	}
	
	skipOrKeepPage80();
	next();
}

function updateDataAppscr79ToTable(i)
{
	var isMemberFRTribeIndicator = "";
	
	isMemberFRTribeIndicator = $('input[name=AmericonIndianQuestionRadio]:radio:checked').val();
	
		$('#appscr79IsMemberFRTribeTD'+i).html(isMemberFRTribeIndicator);
		$('#appscr79StateTD'+i).html($('#appscr79State').val());
		$('#appscr79TribeNameTD'+i).html($('#appscr79TribeName').val());
	
}

function fillValuesAppscr79FromMongodb()
{
	var temp = $('#appscr79IsMemberFRTribeTD1').text();
	if(temp == "yes")
		$('#AmericonIndianQuestionRadioYes').prop("checked", true);
	else
		$('#AmericonIndianQuestionRadioNo').prop("checked", true);
	
	checkAmericonIndianQuestion();
	
	temp = $('#appscr79StateTD1').text();
	$('#appscr79State').val(temp);
	
	temp = $('#appscr79TribeNameTD1').text();
	$('#appscr79TribeName').val(temp);
}

function addDataAppscr81ToTable()
{	
	var appendColumn;
	
	var doesWantHelpForPayingBIll = "";
	var doesAnyHaveHealthInsure = "";
	var noneAnyHaveHealthInsure = "";
	
	var didHaveAHealthInsuranceFromLastJob = "";
	var whyDidThatInsuranceEnd = "";
	var isHealthPlanFromSelfJob = "";
	
	for(var i=1; i<=noOfHouseHold; i++)
	{	
		if($('#P81BdoesWantHelpForPayingBIllTD'+i).html() != undefined)
		{
			updateDataAppscr81ToTable(i);
			continue;
		}
		
		appendColumn = "";
		
		/*	appscrPage81B started	*/
		doesWantHelpForPayingBIll = $('input[name=doesWantHelpForPayingBIllP81B]:radio:checked').val();
		doesAnyHaveHealthInsure = $('#peopleHealthInsurDependChild').prop("checked");
		noneAnyHaveHealthInsure = $('#peopleHealthInsurNone').prop("checked");
		
		appendColumn+= "<td class='P81BdoesWantHelpForPayingBIll' id='P81BdoesWantHelpForPayingBIllTD"+i+"'>"+doesWantHelpForPayingBIll+"</td>";
		
		if(noneAnyHaveHealthInsure)
		{
			appendColumn+= "<td class='P81BdoesAnyHaveHealthInsure' id='P81BdoesAnyHaveHealthInsureTD"+i+"'>false</td>";
			appendColumn+= "<td class='P81BnoneAnyHaveHealthInsure' id='P81BnoneAnyHaveHealthInsureTD"+i+"'>"+noneAnyHaveHealthInsure+"</td>";
		}
		else
		{
			appendColumn+= "<td class='P81BdoesAnyHaveHealthInsure' id='P81BdoesAnyHaveHealthInsureTD"+i+"'>"+doesAnyHaveHealthInsure+"</td>";
			appendColumn+= "<td class='P81BnoneAnyHaveHealthInsure' id='P81BnoneAnyHaveHealthInsureTD"+i+"'>false</td>";
		}
		/*	appscrPage81B ended	*/
		
		/*	appscrPage81C started	*/
		
		didHaveAHealthInsuranceFromLastJob = $('input[name=didHaveAHealthInsuranceFromLastJob]:radio:checked').val();
		whyDidThatInsuranceEnd = $('input[name=whyDidThatInsuranceEnd]:radio:checked').val();
		isHealthPlanFromSelfJob = $('input[name=isHealthPlanFromSelfJob]:radio:checked').val();
		
		appendColumn+= "<td class='P81CdidHaveAHealthInsuranceFromLastJob' id='P81CdidHaveAHealthInsuranceFromLastJobTD"+i+"'>"+didHaveAHealthInsuranceFromLastJob+"</td>";
		appendColumn+= "<td class='P81CwhyDidThatInsuranceEnd' id='P81CwhyDidThatInsuranceEndTD"+i+"'>"+whyDidThatInsuranceEnd+"</td>";
		if(whyDidThatInsuranceEnd == "OTHER")
			appendColumn+= "<td class='P81CExplanation' id='P81CExplanationTD"+i+"'>"+$('#descriptionTextArea').val()+"</td>";
		else
			appendColumn+= "<td class='P81CExplanation' id='P81CExplanationTD"+i+"'></td>";
		appendColumn+= "<td class='P81CisHealthPlanFromSelfJob' id='P81CisHealthPlanFromSelfJobTD"+i+"'>"+isHealthPlanFromSelfJob+"</td>";
		
		/*	appscrPage81C ended	*/
		
		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendColumn);
	}
	
}

function updateDataAppscr81ToTable(i)
{
	var doesWantHelpForPayingBIll = "";
	var doesAnyHaveHealthInsure = "";
	var noneAnyHaveHealthInsure = "";
	
	var didHaveAHealthInsuranceFromLastJob = "";
	var whyDidThatInsuranceEnd = "";
	var isHealthPlanFromSelfJob = "";
	
	doesWantHelpForPayingBIll = $('input[name=doesWantHelpForPayingBIllP81B]:radio:checked').val();
	doesAnyHaveHealthInsure = $('#peopleHealthInsurDependChild').prop("checked");
	noneAnyHaveHealthInsure = $('#peopleHealthInsurNone').prop("checked");
	
	didHaveAHealthInsuranceFromLastJob = $('input[name=didHaveAHealthInsuranceFromLastJob]:radio:checked').val();
	whyDidThatInsuranceEnd = $('input[name=whyDidThatInsuranceEnd]:radio:checked').val();
	isHealthPlanFromSelfJob = $('input[name=isHealthPlanFromSelfJob]:radio:checked').val();
	
	
		$('#P81BdoesWantHelpForPayingBIllTD'+i).html(doesWantHelpForPayingBIll);
		$('#P81BdoesAnyHaveHealthInsureTD'+i).html(doesAnyHaveHealthInsure);
		$('#P81BnoneAnyHaveHealthInsureTD'+i).html(noneAnyHaveHealthInsure);
		
		$('#P81BdidHaveAHealthInsuranceFromLastJobTD'+i).html(didHaveAHealthInsuranceFromLastJob);
		$('#P81BwhyDidThatInsuranceEndTD'+i).html(whyDidThatInsuranceEnd);
		$('#P81CExplanationTD'+i).html($('#descriptionTextArea').val());
		$('#P81BisHealthPlanFromSelfJobTD'+i).html(isHealthPlanFromSelfJob);
	
	
	
}

function fillValuesAppscr81FromMongodb()
{
	var temp = $('#P81BdoesWantHelpForPayingBIllTD1').text();
	if(temp == "yes")
		$('#doesWantHelpForPayingBIllP81BYes').prop("checked", true);
	else
		$('#doesWantHelpForPayingBIllP81BNo').prop("checked", true);
	
	
	temp = $('#P81BdoesAnyHaveHealthInsureTD1').text();
	if(temp == "true")
		document.getElementById('peopleHealthInsurDependChild').checked = true;
	else
		document.getElementById('peopleHealthInsurDependChild').checked = false;
	
	temp = $('#P81BnoneAnyHaveHealthInsureTD1').text();
	if(temp == "true")
		document.getElementById('peopleHealthInsurNone').checked = true;
	else
		document.getElementById('peopleHealthInsurNone').checked = false;
	
	showOrHide81BInfo();
	
	
	
	
	temp = $('#P81CdidHaveAHealthInsuranceFromLastJobTD1').text();
	if(temp == "yes")
		$('#didHaveAHealthInsuranceFromLastJobYes').prop("checked", true);
	else
		$('#didHaveAHealthInsuranceFromLastJobNo').prop("checked", true);
	
	
	temp = $('#P81CwhyDidThatInsuranceEndTD1').text();
	if(temp == "PARENT_NO_LONGER_EMPLOYED")
		$('#P81CParentNoLongerEmployed').prop("checked", true);
	else if(temp == "EMPLOYER_STOPPED_COVERAGE")
		$('#P81CEmployerStoppedCoverage').prop("checked", true);
	else if(temp == "EMPLOYER_STOPPED_DEPENDANT_COVERAGE")
		$('#P81CEmployerStoppedDependant').prop("checked", true);
	else if(temp == "INSURANCE_TOO_EXPENSIVE")
		$('#P81CInsuranceTooExpensive').prop("checked", true);
	else if(temp == "UNINSURED_MEDICAL_NEEDS")
		$('#P81CUninsuredMedicalNeeds').prop("checked", true);
	else
		$('#P81COther').prop("checked", true);
	
	
	temp = $('#P81CExplanationTD1').text();
	$('#descriptionTextArea').val(temp);
	
	
	temp = $('#P81CisHealthPlanFromSelfJobTD1').text();
	if(temp == "yes")
		$('#isHealthPlanFromSelfJobYes').prop("checked", true);
	else
		$('#isHealthPlanFromSelfJobNo').prop("checked", true);
}

function appscr76FromMongodbJSON(currentMember){
	
	//cleanappscr76FromMongodb();
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentMember-1];
	
	/*if(householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes == null || householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes == ""){
		goToPageById("appscr81B"); //appscr81B
		return;
	}*/
	
	/*	PageId 76Part1 started	 */
	
	var job_income_name = householdMemberObj.detailedIncome.jobIncome[0].employerName;
	$('.EmployerNameHere').html(job_income_name);
	
	if(householdMemberObj.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator == true){
		$('#wasUninsuredFromLast6MonthYes').prop("checked", true);
		$("#appscr76P1FirstHideAndShowDiv").show();
		
	} else if (householdMemberObj.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator == false) {
		$('#wasUninsuredFromLast6MonthNo').prop("checked", true);
		$("#appscr76P1FirstHideAndShowDiv").hide();
	} else if (householdMemberObj.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator == null){
		$('#wasUninsuredFromLast6MonthYes').prop("checked", false);
		$('#wasUninsuredFromLast6MonthNo').prop("checked", false);
	}
	$("input[name=isHealthCoverageThroughJob]").prop("checked",false);
	if(householdMemberObj.healthCoverage.isOfferedHealthCoverageThroughJobIndicator == true){
		$('#isHealthCoverageThroughJobYes').prop("checked", true);
		displayWillBeOfferedFromJob();
	}
	if(householdMemberObj.healthCoverage.isOfferedHealthCoverageThroughJobIndicator == false){
		$('#isHealthCoverageThroughJobNo').prop("checked", true);
		displayWillBeOfferedFromJob();
	} else if(householdMemberObj.healthCoverage.isOfferedHealthCoverageThroughJobIndicator == null){
		$('#isHealthCoverageThroughJobYes').prop("checked", false);
		$('#isHealthCoverageThroughJobNo').prop("checked", false);
	}
	$("input[name=willHealthCoverageThroughJob]").prop("checked",false);
	$("#appscr76P1FirstHideAndShowDiv").hide();
	if(householdMemberObj.healthCoverage.employerWillOfferInsuranceIndicator == true){
		$('#willHealthCoverageThroughJobYes').prop("checked", true);
		$("#appscr76P1FirstHideAndShowDiv").show();
	} else if(householdMemberObj.healthCoverage.employerWillOfferInsuranceIndicator == false) {
		$('#willHealthCoverageThroughJobNo').prop("checked", true);
	} else if(householdMemberObj.healthCoverage.employerWillOfferInsuranceIndicator == null){
		$('#willHealthCoverageThroughJobYes').prop("checked", false);
		$('#willHealthCoverageThroughJobNo').prop("checked", false);
	}
	
	$('#appscr76P1HealthCoverageDate').val(UIDateformat(householdMemberObj.healthCoverage.employerWillOfferInsuranceStartDate));
	
	$("#memberNameDataDivPart1").hide();	
	$('#appscr76P1CheckBoxForEmployeeName').prop("checked", false);
	if(trimSpaces(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactPersonName) != ""){		
		$('#appscr76P1CheckBoxForEmployeeName').prop("checked", true);
		$("#memberNameDataDivPart1").show();		
	}
	
	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactPersonName ===""){
		$('#appscr76P1EmployerContactName').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactPersonName);
	}
	
	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneNumber!=" "){
		$('#appscr76P1_firstPhoneNumber').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneNumber);
	}
	
	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneExtension===""){
		$('#appscr76P1_firstExt').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneExtension);
	}
	
	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneType ===""){
		$('#appscr76P1_firstPhoneType').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneType);
	}
	
	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactEmailAddress !=""){
		$('#appscr76P1_emailAddress').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactEmailAddress);
	}
	
	$('#appscr76P1CheckBoxForOtherEmployee').prop("checked", false);
	$("#memberNameDataDivPart2").hide();	
	
	if(trimSpaces(householdMemberObj.healthCoverage.formerEmployer[0].employerName) != "") {
		document.getElementById('appscr76P1CheckBoxForOtherEmployee').checked = true;
		$("#memberNameDataDivPart2").show();
	}	
	
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerName ===""){
		$('#appscr76P1_otherEmployerName').val(householdMemberObj.healthCoverage.formerEmployer[0].employerName);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerIdentificationNumber ===""){
		$('#appscr76P1_employerEIN').val(householdMemberObj.healthCoverage.formerEmployer[0].employerIdentificationNumber);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerIdentificationNumber ===""){
		$('#appscr76P1_otherEmployerAddress1').val(householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress1);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress2 ===""){
		$('#appscr76P1_otherEmployerAddress2').val(householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress2);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].address.city ===""){
		$('#appscr76P1_otherEmployerCity').val(householdMemberObj.healthCoverage.formerEmployer[0].address.city);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].address.zipCode ===" "){
		$('#appscr76P1_otherEmployerZip').val(householdMemberObj.healthCoverage.formerEmployer[0].address.zipCode);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].address.state ===" "){
		$('#appscr76P1_otherEmployerState').val(householdMemberObj.healthCoverage.formerEmployer[0].address.state);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneNumber !=" "){
		$('#appscr76P1_secondPhoneNumber').val(householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneNumber);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneExtension ===""){
		$('#appscr76P1_secondExt').val(householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneExtension);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPersonName ===""){
		$('#appscr76P1_otherEmployerContactName').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPersonName);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneNumber ===" "){
		$('#appscr76P1_thirdPhoneNumber').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneNumber);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneExtension ===""){
		$('#appscr76P1_thirdExt').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneExtension);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneType ===" "){
		$('#appscr76P1_thirdPhoneType').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneType);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactEmailAddress ===""){
		$('#appscr76P1_otherEmployer_emailAddress').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactEmailAddress);
	//}
	/*	PageId 76Part1 ended	 */
	
	/*	PageId 76Part2 started	 */
	
	$("input[name=appscr76p2radiosgroup]").prop("checked",false);
	if(householdMemberObj.healthCoverage.currentlyEnrolledInCobraIndicator == true){
		$('#appscr76P2_cobra').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.currentlyEnrolledInRetireePlanIndicator == true){
		$('#appscr76P2_retireHealthPlan').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.currentlyEnrolledInVeteransProgramIndicator == true){
		$('#appscr76P2_veteransProgram').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.currentlyEnrolledInNoneIndicator == true) { 
		$('#appscr76P2_noneOfAbove').prop("checked", true);
	}
	
	
	/*temp = $('#appscr76P2PhysicianIndicatorTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#appscr76P2_physicianCareIndicatorYes').prop("checked", true);
	else
		$('#appscr76P2_physicianCareIndicatorNo').prop("checked", true);
	
	$('#appscr76P2_physicianName').val($('#appscr76P2PhysicianNameTD1').text());*/
	
	
	$("input[name=willBeEnrolledInHealthCoverageInFollowingYear]").prop("checked",false);
	if(householdMemberObj.healthCoverage.willBeEnrolledInCobraIndicator == true){
		$('#appscr76P2_cobraForCoverage').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.willBeEnrolledInRetireePlanIndicator == true){
		$('#appscr76P2_RHPForCoverage').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.willBeEnrolledInVeteransProgramIndicator == true){
		$('#appscr76P2_VHPForCoverage').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.willBeEnrolledInNoneIndicator == true){
		$('#appscr76P2_NOTAForCoverage').prop("checked", true);
	}
		
	/*	PageId 76Part2 ended	 */
	
	/*	PageId 77Part1 started	 */
	
	$("input[name=77P1CurrentlyEnrolledInHealthPlan]").prop("checked",false);	
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.isCurrentlyEnrolledInEmployerPlanIndicator == true){
		$('#77P1CurrentlyEnrolledInHealthPlanYes').prop("checked", true);
	}else if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.isCurrentlyEnrolledInEmployerPlanIndicator == false){
		$('#77P1CurrentlyEnrolledInHealthPlanNo').prop("checked", true);
	}
	
	$("input[name=77P1WillBeEnrolledInHealthPlan]").prop("checked",false);
	$('#DateToCovered').hide();
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.willBeEnrolledInEmployerPlanIndicator == true){
		$('#77P1WillBeEnrolledInHealthPlanYes').prop("checked", true);
		$('#DateToCovered').show();
	}else if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.willBeEnrolledInEmployerPlanIndicator == false){
		$('#77P1WillBeEnrolledInHealthPlanNo').prop("checked", true);
	}
	
	//This is not available in json
	//temp = $('#appscr77P1WillBeEnrolledInHealthPlanDateTD1').text();
	//$('#77P1WillBeEnrolledInHealthPlanDate').val(temp);
	
	/*
	 * These data are not available in json 
	temp = $('#appscr77P1ExpectChangesToHealthCoverageTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#77P1ExpectChangesToHealthCoverageYes').prop("checked", true);
	else
		$('#77P1ExpectChangesToHealthCoverageNo').prop("checked", true);
	
	
	temp = $('#appscr77P1WillNoLongerHealthCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1WillNoLongerHealthCoverage').checked = true;
	else
		document.getElementById('77P1WillNoLongerHealthCoverage').checked = false;
	
	temp = $('#appscr77P1NoLongerHealthCoverageDateTD1').text();
	$('#77P1NoLongerHealthCoverageDate').val(temp);
	
	
	temp = $('#appscr77P1PlanToDropHealthCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1PlanToDropHealthCoverage').checked = true;
	else
		document.getElementById('77P1PlanToDropHealthCoverage').checked = false;
	
	temp = $('#appscr77P1PlanToDropHealthCoverageDateTD1').text();
	$('#77P1PlanToDropHealthCoverageDate').val(temp);
	
	
	temp = $('#appscr77P1WillOfferCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1WillOfferCoverage').checked = true;
	else
		document.getElementById('77P1WillOfferCoverage').checked = false;
	
	temp = $('#appscr77P1WillOfferCoverageDateTD1').text();
	$('#77P1WillOfferCoverageDate').val(temp);
	
	
	temp = $('#appscr77P1PlaningToEnrollInHCTD1').text();
	if(temp == "true")
		document.getElementById('77P1PlaningToEnrollInHC').checked = true;
	else
		document.getElementById('77P1PlaningToEnrollInHC').checked = false;
	
	temp = $('#appscr77P1PlaningToEnrollInHCDateTD1').text();
	$('#77P1PlaningToEnrollInHCDate').val(temp);
	
	
	temp = $('#appscr77P1HealthPlanOptionsGoingToChangeTD1').text();
	if(temp == "true")
		document.getElementById('77P1HealthPlanOptionsGoingToChange').checked = true;
	else
		document.getElementById('77P1HealthPlanOptionsGoingToChange').checked = false;
	
	temp = $('#appscr77P1HealthPlanOptionsGoingToChangeDateTD1').text();
	$('#77P1HealthPlanOptionsGoingToChangeDate').val(temp);
	
	*/
	
	/*	PageId 77Part1 ended	 */
	
	/*	PageId 77Part2 started	 */
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentLowestCostSelfOnlyPlanName === ""){
		$('#currentLowestCostSelfOnlyPlanName').val(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentLowestCostSelfOnlyPlanName);
	}
	
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentPlanMeetsMinimumStandardIndicator == true)
		document.getElementById('currentPlanMeetsMinimumStandard').checked = false;
	else
		document.getElementById('currentPlanMeetsMinimumStandard').checked = true;
	
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingLowestCostSelfOnlyPlanName === ""){
		$('#comingLowestCostSelfOnlyPlanName').val(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingLowestCostSelfOnlyPlanName);
	}
	
	temp = $('#appscr77P2ComingPlanMeetsMinimumStandardTD1').text();
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingPlanMeetsMinimumStandardIndicator == true)
		document.getElementById('comingPlanMeetsMinimumStandard').checked = true;
	else
		document.getElementById('comingPlanMeetsMinimumStandard').checked = false;
	
	/*
	 * Following data are not available in json	
	temp = $('#appscr77P2LowestCostSelfOnlyPremiumAmountTD1').text();
	$('#lowestCostSelfOnlyPremiumAmount').val(temp);
	
	
	temp = $('#appscr77P2LowestCostSelfOnlyPremiumFrequencyTD1').text();
	$('#lowestCostSelfOnlyPremiumFrequency').val(temp);
	*/
	
	/*	PageId 77Part2 ended	 */

	
}

/*$(document).ready(function(){
	$("input[name=isHealthCoverageThroughJob],input[name=willHealthCoverageThroughJob]").bind("click", function(){
		if($(this).attr("value") == "yes"){
			$("#memberNameDataDivHide, #memberNameDataDiv").show();
		}else if($("#isHealthCoverageThroughJobNo").is(":checked") && $("#willHealthCoverageThroughJobNo").is(":checked")){
			$("#memberNameDataDivHide, #memberNameDataDiv").hide();
		}
	});
});*/