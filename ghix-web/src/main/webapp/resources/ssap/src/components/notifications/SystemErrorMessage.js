import React from 'react';

const SystemErrorMessage = props => {

    const url = window.location.href;

    const {
        data: {
            singleStreamlinedApplication: {
                mockData = undefined,
                ssapApplicationId = 'not received',
                manuallyCreated = undefined,
                taxHousehold
            }
        },
        DASHBOARD,
        error: {
            errorMessage,
            time,
            code,
            endpoint
        },
        PHONE
    } = props;

    const {
        householdMember
    } = taxHousehold[0];

    const {
        bloodRelationship
    } = householdMember[0];

    return (
        <div className="application-error">
            <div className="usa-grid">
                <div className="usa-width-one-whole usa-alert usa-alert-info alert--info">
                    <h4 className="margin-tb-10">
                        { errorMessage } &nbsp;
                        <a href="#">{ PHONE }</a>.
                    </h4>
                    <a className="usa-width-one-whole margin-b-10" href={ DASHBOARD }> Go back to your Dashboard</a>
                </div>
                <div className='additional-error-info'>
                    <div>
                        <span>SSAP ID: { ssapApplicationId }</span>
                        <span>Code: { code }</span>
                        <span>Time: { time }</span>
                    </div>
                    <div>
                        <span>Endpoint: { endpoint }</span>
                        <span>Url: { url }</span>
                    </div>
                    <div>
                        <span>M: { manuallyCreated ? 'TRUE' : 'FALSE' }</span>
                        <span>BR: { bloodRelationship.length }</span>
                        <span>HHMs: { mockData ? 0 : householdMember.length }</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SystemErrorMessage;