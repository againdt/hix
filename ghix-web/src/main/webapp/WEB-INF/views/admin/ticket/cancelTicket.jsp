<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<style type="text/css">
.modal .header{
	border-radius: 6px 6px 0 0 !important;
    padding: 10px !important;
}
.modal .dialogClose{
	margin: -4px !important;
    padding: 4px;
}
</style>
<input type="hidden" id="ticketId" name="ticketId" value="${encTicketid}"> 

<!-- Cancel confirmation dialog  -->
<div id="confirmCancelDlg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markConfirmCancelDlg" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="markCancelHeader">
    	<div class="header">
            <h4 class="pull-left"><spring:message code="header.cancelTicket" /></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="close" title="x" type="button"><spring:message code="btn.crossClose" /></button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					<h4 class="margin0 pull-left"><spring:message code="msg.confirmCancel" /></h4>
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
    <button class="btn pull-left" data-dismiss="modal" aria-hidden="true" onclick="javascript:denyCancelDialogue()"><spring:message code="optionCancelNo" /></button>
    <button class="btn btn-primary" onclick="javascript:showCancelTicketDlg()"><spring:message code="optionCancelYes" /></button>
  </div>
</div>
<!-- Cancel confirmation dialog ends -->
<!-- Cancel ticket dialog -->
<div id="cancelTicketDlg" class="modal hide fade" tabindex="-1"	role="dialog" aria-labelledby="cancelTicketDlg" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="cancelTicketHeader">
		<div class="header">
	    
			<div id="cancelerr" style="font-size: 14px; color: red"> </div>				
			<h4 class="pull-left">Cancel ticket</h4>
			<button aria-hidden="true" data-dismiss="modal" id="crossCloseCancel"	class="close" title="x" type="button"><spring:message code="btn.crossClose" /></button>
		</div>
	</div>

	<div class="modal-body">
		<div class="control-group">
			<div class="controls">
				<label><spring:message code="lbl.cancelReason" /> </label>
				<textarea class="span" name="comment_text" id="comment_text" rows="4" cols="40" style="resize: none;" maxlength="100"
					spellcheck="true"></textarea>
				<div id="comment_text_error"></div>
			</div>
		</div>
	</div>
	<div class="modal-footer clearfix">
		<button class="btn pull-left" aria-hidden="true" data-dismiss="modal" id="CancelTktBtn"><spring:message code="button.cancel" /></button>
		<button class="btn btn-primary"	onclick="javascript:cancelTicket();"  id="submitBtn"><spring:message code="button_tkt.submit" /></button>
	</div>
	<div id="errorMsg"><label id="errorLabel"></label></div>
	<div id="ajaxloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
    </div>
</div>
<!-- Cancel ticket dialog ends -->
<script>
function showCancelTicketDlg(){
	$("#crossCloseCancel").attr("disabled",false); 	
	$("#CancelTktBtn").attr("disabled",false); 	
	$("#submitBtn").attr("disabled",false); 	
	$('#tkmErrorDiv').html("");
	$('#confirmCancelDlg').modal('hide');
	
	$("#comment_text").val("");

	$('#cancelTicketDlg').modal('show');
}



function cancelTicket()
{   
	var ticketId = $("#ticketId").val();
	$("#crossCloseCancel").attr("disabled",true); 	
	$("#CancelTktBtn").attr("disabled",true); 	
	$("#submitBtn").attr("disabled",true); 	
	var commentData = $("#comment_text").val();
	var commentLength =  $("#comment_text").val().length;
	
	var validateUrl ='<c:url value="/ticketmgmt/ticket/cancelticket"></c:url>';
	$("#ajaxloader").show();
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
			ticketId : ticketId,
			comment : commentData,
			commentLength : commentLength
		},
		success : function(response) {
			if (response !=null && response != "") {
				$('#cancelTicketDlg').modal('hide');
				$("#ajaxloader").hide();
                window.location.reload();
				$('#tkmErrorDiv').html(response);
				$("#completeTicket-"+ticketId).hide();
				$("#reassignTicket-"+ticketId).hide();
				
			  
			} else {
				$('#cancelTicketDlg').modal('hide');
				$("#ajaxloader").hide();
				notifyCancelResponse(true, ticketId);
			}
			
			
		},
		error : function(error) {
			$("#ajaxloader").hide();
			notifyCancelResponse(false, ticketId);
		}
	});
}

	
function denyCancelDialogue()
{

	$('#tkmErrorDiv').html("");	
}

</script>
