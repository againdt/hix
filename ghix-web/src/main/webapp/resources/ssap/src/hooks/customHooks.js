import { useEffect, useState } from 'react';
import _ from 'lodash';

export function useFetchEffect(...args) {

    const [
        statesForFetching,
        [
            oneStateEndpoint,
            manyStatesEndpoint
        ],
        watchedData,
        actionCreator
    ] = args;

    const [ call, setCall ] = useState({});

    useEffect(() => {

        _.remove(statesForFetching, el => el === null);

        const states = _.difference(
            _.uniq(statesForFetching),
            Object.entries(watchedData).map(([ countyName ]) => countyName)
        );

        const statesToString = states.toString();

        let selectedCall;

        switch(states.length){
        case 0:
            selectedCall = null;
            break;
        case 1:
            selectedCall = () => actionCreator(`${window.location.origin}${oneStateEndpoint}${statesToString}`, statesToString);
            break;
        default:
            selectedCall = () => actionCreator(`${window.location.origin}${manyStatesEndpoint}${statesToString}`);
            break;
        }

        setCall({ selectedCall });

    }, [statesForFetching.toString()]);

    return call;
}

