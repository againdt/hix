<script type="text/ng-template" id="shared-relationship.jsp">
  <div class="gutter10" ng-controller="bloodRelations">
  	<form class="form-horizontal" name="bloodRelationship">
      <h3>
        <spring:message code="ssap.bloodRel.header" />
      </h3>
      <div class="householdMemberRelationship gutter10" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
        <div class="control-group alert alert-info" >
          <span><spring:message code="ssap.bloodRel.hereAre" /> {{activeHouseholdCount}} <spring:message code="label.lce.establishRelation" /></span>
          <ol ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold" class="margin40-l">
            <li ng-repeat="householdMember in taxHousehold.householdMember | filter: activeDeathFilter track by $index ">
              <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
            </li>
          </ol>
          <div class="margin10-t"><strong><spring:message code="label.lce.dependentOverAge3"/></strong></div>
        </div><!-- control-group ends -->
        <div class="eachMember gutter20">
          <ol>
            <li ng-repeat="householdMember in taxHousehold.householdMember" ng-if="(taxHousehold.householdMember.length-1) > $index && activeDeathFilter(householdMember)">
              How is <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong> related to the other household members?
              <div ng-repeat="relhouseholdMember in taxHousehold.householdMember" ng-if="$index > $parent.$index && activeDeathFilter(relhouseholdMember)">
                <form class="form-horizontal">
                  <div class="control-group">
                    <label class="control-label" for="ddlRelationship{{$parent.$parent.$index+$index}}">
                      <strong> {{householdMember.name.firstName | capitalize:true}}</strong> is <strong>{{relhouseholdMember.name.firstName | capitalize:true}}</strong>&apos;s
                    </label>
  				  <ng-form name="realtionshipForm">
                    <div class="controls">

        			   <select class="span6" name="relationship" id="ddlRelationship{{$parent.$parent.$index+$index}}"  ng-model="bloodRelationshipModel[$parent.$parent.$index][$parent.$index]" ng-change="updateRelationshipDetails(householdMember,relhouseholdMember,bloodRelationshipModel[$parent.$parent.$index][$parent.$index])" ng-disabled="(newlyAddedApplicantPersonId != relhouseholdMember.personId) || (disablePrimaryToNewHouseholdRelationship && newlyAddedApplicantPersonId == relhouseholdMember.personId && householdMember.personId == 1)" required age-over16="false" from-Household="householdMember" to-Household="relhouseholdMember" is-relationship-dropdown="Y" multiple-spouse-check="Y">
                          <option ng-disabled=" (householdMember.personId == 1) && (!disablePrimaryToNewHouseholdRelationship) && (relationship.code == '01')" ng-repeat="relationship in relationships " value="{{relationship.code}}" ng-selected="{{relationship.code == bloodRelationshipModel[$parent.$parent.$parent.$index][$parent.$index] }}" label="{{relationship.type}}">{{relationship.type}} </option>
                       </select>

                    </div>
                    <!-- Controls -->
                    <label class="control-label" for="hidDummyField{{$parent.$parent.$index+$index}}"><span class="aria-hidden">hidDummyField {{$parent.$parent.$index+$index}}</span> <input type="hidden" id="hidDummyField{{$parent.$parent.$index+$index}}" /></label>
                    <span class="validation-error-text-container span7" style="display:block" ng-if="submitted && (realtionshipForm.relationship.$error.required)">Please select relationship between {{householdMember.name.firstName | capitalize:true}} and {{relhouseholdMember.name.firstName | capitalize:true}}.</span>
					<span class="validation-error-text-container span7" style="display:block" ng-if="submitted && (realtionshipForm.relationship.$error.singleSpouse && !realtionshipForm.relationship.$error.ageOver16)"><spring:message code="label.multiple.spouses.not.allowed"/></span>
  				 </ng-form>
                  </div>
                </form>
              </div>
            </li>
          </ol>
        </div>
      </div>

      <!-- BACK & NEXT BUTTON -->
       <dl class="dl-horizontal pull-right">
        <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()">
          <spring:message code="label.lce.back"/>
        </a>
        <a role="button" tabindex="0" class="btn btn-primary" ng-if="bloodRelationship.$invalid" ng-click="$parent.submitted=true">
          <spring:message code="label.lce.next"/>
        </a>
  	    <a role="button" tabindex="0" class="btn btn-primary" ng-if="bloodRelationship.$valid" ng-click="processAgeOver26();$root.isBackFromAdressPage=false;">
          <spring:message code="label.lce.next"/>
        </a>
      </dl>

      <!-- MODAL THAT POPS UP WHEN USER WANT TO SAVE  -->
      <modal-small show='modalShown'>
          
              <div>
                <p>
                  <spring:message code="label.lce.dependentOverAge4"/> <strong>{{primaryHousehold.name.firstName}} {{primaryHousehold.name.middleName}} {{primaryHousehold.name.lastName}}</strong>.
                </p>
                <p>
                  <spring:message code="label.lce.dependentOverAge2"/>
                </p>
                <p class="alert alert-info">
                  <spring:message code="label.lce.dependentOverAge3"/>
                </p>
                <ul >
					<li ng-repeat="houseHoldeMemberFromArray in ageOver26Array">{{houseHoldeMemberFromArray.name.firstName}} {{houseHoldeMemberFromArray.name.middleName}} {{houseHoldeMemberFromArray.name.lastName}}</li>
                </ul>
                <dl class="dl-horizontal pull-right">
                  <a role="button" tabindex="0" class="btn btn-primary" ng-click="updateCoverageFlag(ageOver26Array);toggleModalForAgeOver26(); nextHouseholdDependent();$root.isBackFromAdressPage=false;"><spring:message code="label.lce.next"/></a>
                </dl>
              </div>
          
      </modal-small>
    </form>
  </div>
</script>
