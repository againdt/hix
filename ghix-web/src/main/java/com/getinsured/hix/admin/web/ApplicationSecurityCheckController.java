package com.getinsured.hix.admin.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.getinsured.hix.endpoints.EndpointsService;
import com.getinsured.hix.endpoints.dto.Endpoint;
import com.getinsured.hix.model.RolePermission;
import com.getinsured.hix.platform.security.repository.IRolePermissionRepository;

/**
 * Displays current application security.
 * 
 * @author Yevgen Golubenko
 */
@Controller
public class ApplicationSecurityCheckController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationSecurityCheckController.class);

	@Autowired
	private RequestMappingHandlerMapping requestMappingHandlerMapping;

	@Autowired
	private EndpointsService endpointsService;

	@Autowired
	private IRolePermissionRepository rolePermissionsRepo;

	/**
	 * @author desu_s
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/admin/appsecurity", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_URL_PERMISSIONS')")
	public String getProperty(final Model model, final HttpServletRequest request) {
		return "admin/appsecurity";
	}

	/**
	 * @author desu_s
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "endpoints", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_URL_PERMISSIONS')")
	@ResponseBody
	public List<Endpoint> getEndPointsInView(Model model) {
		model.addAttribute("endpoints", endpointsService.transform(requestMappingHandlerMapping));
		List<Endpoint> endpoints = new ArrayList<Endpoint>(0);
		try {
			endpoints = endpointsService.transform(requestMappingHandlerMapping);
		} catch (Exception ex) {
			LOGGER.error("Unable to create the endpoint mappings.", ex);
		}
		return endpoints;
	}

	@RequestMapping(value = "rolesperms", method = RequestMethod.GET, produces = "text/csv")
	@PreAuthorize("hasPermission(#model, 'VIEW_URL_PERMISSIONS')")
	@ResponseBody
	public String downloadCSV() {
		HashMap<String, Set<Endpoint>> roleEndPoints = new HashMap<>();
		List<Endpoint> endpoints = new ArrayList<Endpoint>(0);
		endpoints = endpointsService.transform(requestMappingHandlerMapping);
		for (Endpoint ep : endpoints) {
			String permission = ep.getPermission();
			List<RolePermission> rolesPerms = this.rolePermissionsRepo.getRolesByPermission(permission);
			for (RolePermission rp : rolesPerms) {
				String roleName = rp.getRole().getName();
				Set<Endpoint> existingEndPoints = roleEndPoints.get(roleName);
				if (existingEndPoints == null) {
					existingEndPoints = new HashSet<>();
					existingEndPoints.add(ep);
					roleEndPoints.put(roleName, existingEndPoints);
				} else {
					existingEndPoints.add(ep);
				}
			}
		}
		//Now return the CSV
		StringBuilder sb = new StringBuilder();
		sb.append("\"ROLE NAME\",");
		sb.append("\"URL\",");
		sb.append("\"PERMISSION\"\n");
		Iterator<Entry<String, Set<Endpoint>>> cursor = roleEndPoints.entrySet().iterator();
		Entry<String, Set<Endpoint>> entry = null;
		String role = null;
		Set<Endpoint>  role_endpoints = null;
		
		while(cursor.hasNext()) {
			entry = cursor.next();
			role = entry.getKey();
			role_endpoints = entry.getValue();
			for(Endpoint ep: role_endpoints) {
				sb.append("\"");
				sb.append(role);
				sb.append("\",");
				
				sb.append("\"");
				sb.append(ep.getUrl());
				sb.append("\",");
				
				sb.append("\"");
				sb.append(ep.getPermission());
				sb.append("\"\n");
			}
		}
		sb.trimToSize();
		return sb.toString();
	}
	
	@RequestMapping(value = "rolespermsAll", method = RequestMethod.GET, produces = "text/csv")
	@PreAuthorize("hasPermission(#model, 'VIEW_URL_PERMISSIONS')")
	@ResponseBody
	public String downloadCSVAll() {
		HashMap<String, Set<Endpoint>> roleEndPoints = new HashMap<>();
		List<Endpoint> endpoints = new ArrayList<Endpoint>(0);
		endpoints = endpointsService.transform(requestMappingHandlerMapping);
		for (Endpoint ep : endpoints) {
			String permission = ep.getPermission();
			List<RolePermission> rolesPerms = this.rolePermissionsRepo.getRolesByPermission(permission);
			for (RolePermission rp : rolesPerms) {
				String roleName = rp.getRole().getName();
				Set<Endpoint> existingEndPoints = roleEndPoints.get(roleName);
				if (existingEndPoints == null) {
					existingEndPoints = new HashSet<>();
					existingEndPoints.add(ep);
					roleEndPoints.put(roleName, existingEndPoints);
				} else {
					existingEndPoints.add(ep);
				}
			}
		}
		//Now return the CSV
		StringBuilder sb = new StringBuilder();
		sb.append("\"ROLE NAME\",");
		sb.append("\"URL\",");
		sb.append("\"HTTP METHOD\",");
		sb.append("\"PERMISSION\"\n");
		sb.append("\"Method\"\n");
		sb.append("\"Location\"\n");
		Iterator<Entry<String, Set<Endpoint>>> cursor = roleEndPoints.entrySet().iterator();
		Entry<String, Set<Endpoint>> entry = null;
		String role = null;
		Set<Endpoint>  role_endpoints = null;
		
		while(cursor.hasNext()) {
			entry = cursor.next();
			role = entry.getKey();
			role_endpoints = entry.getValue();
			for(Endpoint ep: role_endpoints) {
				sb.append("\"");
				sb.append(role);
				sb.append("\",");
				
				sb.append("\"");
				sb.append(ep.getUrl());
				sb.append("\",");
				
				sb.append("\"");
				sb.append(ep.getHttpMethod());
				sb.append("\",");
				
				sb.append("\"");
				sb.append(ep.getPermission());
				sb.append("\",");
				
				sb.append("\"");
				sb.append(ep.getMethodName());
				sb.append("\",");
				
				sb.append("\"");
				sb.append(ep.getClassName());
				sb.append("\"\n");
			}
		}
		sb.trimToSize();
		return sb.toString();
	}
}
