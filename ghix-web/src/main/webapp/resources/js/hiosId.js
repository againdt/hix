function isDuplicateHios(){
	if($("#hiosIssuerId").val() != ""){  // if hiosId is not empty
		$.ajax({
			type : "POST",
			url: "<c:url value='/admin/issuer/checkDuplicateHios'/>",
			data: { "hiosId": $("#hiosIssuerId").val()},
			success: function(responseText){
				if(responseText!=""){
					if(responseText=="multiple"){
						$('#hiosIssuerId_error').html("<label class='error' for='hiosIssuerId' generated='true'><span><em class='excl'>!</em> <spring:message  code='err.multipleHios' javaScriptEscape='true'/></span></label>");
					}else{
						$('#hiosIssuerId_error').html("<label class='error' for='hiosIssuerId' generated='true'><span><a href='/hix/admin/issuer/details/"+responseText+"'><em class='excl'>!</em> <spring:message  code='err.duplicateHios' javaScriptEscape='true'/></a></span></label>");	
					}
					returnVal =  false;
				} 
				else{
					$('#hiosIssuerId_error').html("");
					returnVal = true;
				}
	       	}
		});
	}
	return returnVal;
}