package com.getinsured.referraltask;

/**
 * @author chopra_s
 * 
 */
public interface ReferralEligibilityTask {
	public void runEligibility(final long ssapApplicationId);
}
