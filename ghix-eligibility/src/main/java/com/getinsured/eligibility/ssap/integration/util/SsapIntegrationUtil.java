package com.getinsured.eligibility.ssap.integration.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * Utility class containing common utility methods. 
 */
public class SsapIntegrationUtil {
    
	private static final Object SUCCESS_RESPONSE_CODE = "HS000000";
	
	private static final String NON_RETRYABLE_ERROR_RESPONSE_CODE_PREFIX = "HE";
	
	private static final String HUB_UNKNOWN_ERROR_CODE = "HE009999";
	
    @SuppressWarnings("serial")
    private static final Map<String, String> SSAP_TO_HUB_IMMI_DOCTYPE_MAPPING = new HashMap<String, String>() {
        {
            put("I327", "I327");
            put("I551", "I551");
            put("I571", "I571");
            put("I766", "I766");
            put("I797", "I797");
            put("MacReadI551", "MacReadI551");
            put("TempI551", "TempI551");
            put("I94", "I94Document");
            put("I94UnexpForeignPassport", "I94UnexpForeignPassport");
            put("UnexpForeignPassport", "UnexpForeignPassport");
            put("I20", "I20");
            put("DS2019", "DS2019");
            put("NaturalizationCertificate", "NatrOfCert");
            put("CitizenshipCertificate", "CertOfCit");
        }
    };

    
    public static String getImmigrationDocument(String document) {
        return SSAP_TO_HUB_IMMI_DOCTYPE_MAPPING.get(document);
    }
    
    /**
     * Method to check if the object containing a string is not null or blank
     * @param obj
     * @return boolean
     */
    public static boolean isNotNullOrBlank(Object obj) {
        return obj != null && !"".equals(obj.toString().trim());
    }
    
    /**
     * Method to check if the response code from hub service is success or non-retryable error code 
     * @param obj
     * @return boolean
     */
    public static boolean isSuccessOrNonRetryableResponseCode(String responseCode) {
    	if(StringUtils.isNotBlank(responseCode) && ((responseCode.equals(SUCCESS_RESPONSE_CODE) || responseCode.startsWith(NON_RETRYABLE_ERROR_RESPONSE_CODE_PREFIX)) && 
    			!responseCode.equals(HUB_UNKNOWN_ERROR_CODE))) {
    		return true;
    	}
    	return false;
    }
    
    
    

}
