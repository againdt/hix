<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page isELIgnored="false"%>

<script src="//maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    var locations = ${mapString};
    
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(locations[0][1],locations[0][2]),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker;

    for (i = 0,j=1; i <= locations.length; i++,j++) {  
     	marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld='+j+'|FF776B|000000'
      
      });
      marker.set("type", "point");
      marker.set("id", 1);
    };
});
  </script>
<style type="text/css">
body {background: #fff;}
#provider-list {padding-top: 0 !important;}
.border-b {border-bottom: 1px solid #E5E5E5;}
.container {
    margin-top: -40px;
    padding: 0 13px;
    width: 97%;
}

#list {border: 1px solid #E5E5E5; height: 218px; width: 400px; margin-left: 0; }
#list .result-block {margin-left: 0;}
#list i {margin-right: 3px;}
#list dl, #list .doc-info {margin: 0 0 0 18px;}
#list dl {font-size: 9px; text-transform: uppercase;}
#list dl dt{margin-right: 3px; width: auto;}
#list .dl-horizontal dd {margin-left: 55px;}
#list .doc-info {font-size: 12px;}
address {margin-bottom: 0; line-height: 16px;}
</style>
<body id="provider-list">
	<div class="gutter10-lr">
    	<div class="gutter10-b">
			<a class="btn btn-primary" href="javascript:history.back(1)"><spring:message code='label.back'/></a>
        </div>	
     	<%int count=0; %>
		<form id="result-list" name="result-list" action="detail" method="post">
               <div id="list" class="span5" style="max-height: 293px;overflow-y:scroll;border-bottom: none;">
  					<c:choose>
						<c:when test="${fn:length(physicianObj) > 0}">
							<c:forEach var="physician" items="${physicianObj}" >
								<div class="result-block span5 border-b">
		                             <h4 class="span5 margin0"><i class="icon-map-marker"></i><a href="<c:url value='/provider/physiciandetails/${physician.id}?phytype=${physicianType}&zipcode=${defaultZip}'/>"><span class="count"><%=++count%>.</span>${physician.first_name }  ${physician.last_name }</a></h4>
		                             <dl class="dl-horizontal">
		                             <dt><spring:message code='label.specialities'/></dt>
		                             <c:forEach var="physicianSpeciality" items="${physician.physicianSpecialRelation}">
		 								<dd>${physicianSpeciality.specialty.name} ,</dd>
		 							 </c:forEach>
		                             </dl>
		                             <div class="doc-info">
		                                 <address>carrierlogo
			                                 <c:forEach var="physicianAddresses" items="${physician.physicianAddress}">
			                                 	${physicianAddresses.address1}, ${physicianAddresses.state} ${physicianAddresses.zip}<br>
			                                 	${physicianAddresses.phoneNumber}
			                                  </c:forEach>
			                                  ${physician.emailAddress}
		                                 </address>
		                             </div>
		                         </div> <!--end result-block-->
							</c:forEach>
							<div class="center"><dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/></div>
						</c:when>
					<c:otherwise>
						<div class="alert alert-info"><spring:message code='label.searchResult'/></div>
					</c:otherwise>
				</c:choose>                  
                </div><!--end list-->
                <div class="span2">
					<div id="map" style="width: 220px; height: 205px;"></div>
				</div>
            	<div class="center clearfix"></div>
		</form>
	</div>						
</body>