 <div class="span6 info-block">                    
              <div class="gutter10">
                <div class="header"><h4>Employer Contribution</h4></div>
                  <div class="gutter10 tile">
                    <p>To make your health coverage more affordable, your employer <span id="employer-name"><strong>$employerName$</strong></span> has chosen to offer the following benefits:</p>
                    <div class="data-display well" id="emp-contribution">
                    	<dl class="dl-horizontal">
                        	<dt>Monthly Employer Contributions:</dt>
                        	<dd>&nbsp;</dd>
                        	<dl class="dl-horizontal">
                        		<dt>Towards your health coverage:</dt>
                        			<dd id="your-health-coverage">&#36;$EmployerContributionIndv$</dd>
                        		<dt>Towards dependent health coverage:</dt>
                        			<dd id="dependent-health-coverage">&#36;$EmployerContributionDepe$</dd>
                        		<dt>Towards your dental coverage:</dt>
                        			<dd id="your-dental-coverage">&#36;$employerDentalContributionIndv$</dd>
                        		<dt>Towards dependent dental coverage:</dt>
                        			<dd id="dependent-dental-coverage">&#36;$employerDentalContributionDepe$</dd>
                        	</dl>	
                        	<dt>Available plans to choose from:</dt>
                        		<dd id="plan-choice">$planCount$</dd>
                        	<dt id="c-s-d">Coverage start date:</dt>
                       		 	<dd id="coverage-s-date">$coverageEffDt$</dd>
                       		 $if(coverageEndDt)$	
                       		 	<dt id="c-e-d">Coverage end date:</dt>
                       		 		<dd id="coverage-e-date">$coverageEndDt$</dd>
                       		 $endif$		
                    	</dl>
                    </div>
                </div>
              </div>
            </div>