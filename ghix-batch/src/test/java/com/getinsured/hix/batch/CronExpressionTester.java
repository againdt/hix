package com.getinsured.hix.batch;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.scheduling.support.CronSequenceGenerator;

/**
 * Class is used for CRON expression tester in Spring.
 */
public class CronExpressionTester {

	private static final TimeZone PST = TimeZone.getTimeZone("PST");

	public static void main(String[] args) {

		printNextFireTime("0 0/1 * 1/1 * ?");
		printNextFireTime("0 0/10 * 1/1 * ?");
		
		printNextFireTime("0 0 2 ? * SAT");

		printNextFireTime("0 0 3 * * *");
		printNextFireTime("0 0/5 * * * *");
		printNextFireTime("0 40 15 * * *");

		printNextFireTime("0 5 2 * * THU,FRI");
		printNextFireTime("0 15 8,12,18 * * *");
	}

	/**
	 * CRON Expression Maker: http://www.cronmaker.com/
	 */
	private static void printNextFireTime(String cronExp) {

		CronSequenceGenerator csg = new CronSequenceGenerator(cronExp, PST);
		Date nextDate = csg.next(Calendar.getInstance().getTime());

		System.out.println("Next Fire Time==>" + cronExp + " ==>" + DateFormat.getDateTimeInstance().format(nextDate));
	}
}
