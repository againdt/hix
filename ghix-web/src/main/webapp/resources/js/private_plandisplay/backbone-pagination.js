(function (backbone) {
	/**
	 * @class
	 * Pagination
	 */
	backbone.Pagination = {
		/**  how many items to show per page */
		perPage : 12,

		/** page to start off on */
		page : 1,

		/**
		 *
		 */
		initialize: function(){

			this.sortColumn = "";
			this.sortColumnList = [];
			var sortDirectionList = [];
			this.sortDirection = "desc";
			this.lastSortColumn = "";
			this._sorted = false;

			this.fieldFilterRules = [];
			this.lastFieldFilterRules = [];

			this.filterFields = "";
			this.filterExpression = "";
			this.lastFilterExpression = "";

			this.showCompare = false;
			this.showDetail = false;
			this.compareModels=[];
			this.detailModels= [];
			this.drawermodels;
			this.cartSummaryfetched = false;

			this.issuerList = [];
			this.issuerCount = [];

			this.totalPlanCount = 0;
			this.filteredPlanCount = 0;

			this.prevItem = 0;
			this.nextItem = 0;
			this.start = 0;
			this.stop = 0;

			this.cartItems= [];
		},

		nextPage : function () {
			var self = this;

			if(self.page >= self.information.totalPages) {
				return;
			}

			self.page = ++self.page;
			window.dataLayer.push({'event': 'displayPaginationEvent', 'eventCategory': 'Plan Selection - Plan Display Pagination', 'eventAction': 'Pagination Button Click', 'eventLabel': 'Pagination - ' + self.page});
			//triggerGoogleTrackingEvent(['_trackEvent', 'Plan Display Pagination', 'click', 'Pagination - ' + self.page]);

			self.pager();
		},

		previousPage : function () {
			var self = this;

			self.page = --self.page || 1;
			window.dataLayer.push({'event': 'displayPaginationEvent', 'eventCategory': 'Plan Selection - Plan Display Pagination', 'eventAction': 'Pagination Button Click', 'eventLabel': 'Pagination - ' + self.page});
			//triggerGoogleTrackingEvent(['_trackEvent', 'Plan Display Pagination', 'click', 'Pagination - ' + self.page]);

			self.pager();
		},

		goTo : function (page) {
			var self = this;

			self.page = parseInt(page,10);
			self.pager();
			self.findMaxTileHeight();
		},
		findMaxTileHeight : function() {
			var highestTile = null;
			var optionBlock = null;
			var ht = 0;
			$('.tile').each(function(){
					var h = $(this).find(".plan-options").height();
		  		if(h > ht){
		    		ht = h;
		    		highestTile = $(this);
		  		}
			});
			$('.tile .plan-options').css('height',ht);
		},

		howManyPer : function (perPage) {
			var self = this;
			var lastPerPage = self.perPage;
			self.perPage = parseInt(perPage, 10);
			self.perPage = Math.ceil( ( lastPerPage * ( self.page - 1 ) + 1 ) / perPage);;
			self.pager();
		},

		setSort : function (column, direction) {
			var self = this;
			if(column !==undefined && direction !==undefined){
				self.lastSortColumn = this.sortColumn;
				self.sortColumn = column;
				self.sortDirection = direction;
				self.page = 1;
				self.pager();
				self.info();

				$("#"+column).attr('selected', 'selected');
			}
		},

		addToCompare: function (planId) {
			if($.inArray(planId, this.compareModels)== -1)
				this.compareModels.push(planId);
		},

		addToDetail: function (planId) {
			this.detailModels = [];
			this.detailModels.push(planId);
		},

		removeCompare: function (planId,viewType) {
			var self = this;
			var i=0;
			_.each(self.compareModels, function(remId){
				if(remId == planId){
					self.compareModels.splice(i,1);
				}
					i++;
			});
			/*in compare view*/
			if(viewType == 'cview'){
				if(self.compareModels.length==0)
					self.showCompare=false;
				else
					self.showCompare=true;

				self.pager();
			}
		},

		// setFieldFilter is used to filter each value of each model
		// according to `rules` that you pass as argument.
		// Example: You have a collection of books with 'release year' and 'author'.
		// You can filter only the books that were released between 1999 and 2003
		// And then you can add another `rule` that will filter those books only to
		// authors who's name start with 'A'.
		setFieldFilter: function ( fieldFilterRules ) {
			if( !_.isEmpty( fieldFilterRules ) ) {
				this.lastFieldFilterRules = this.fieldFilterRules;
				this.fieldFilterRules = fieldFilterRules;
				this.page = 1;
				this.pager();
				this.info();
			}else{
				this.resetFieldFilter();
			}
		},

		resetFieldFilter: function () {
			this.fieldFilterRules = [];
			this.pager();
			this.info();
		},


		// setFilter is used to filter the current model. After
		// passing 'fields', which can be a string referring to
		// the model's field or an array of strings representing
		// all of the model's fields you wish to filter by and
		// 'filter', which is the word or words you wish to
		// filter by, pager() and info() will be called automatically.
		setFilter: function ( fields, filter ) {
			if( fields !== undefined && filter !== undefined ){
				this.filterFields = fields;
				this.lastFilterExpression = this.filterExpression;
				this.filterExpression = filter;
				this.pager();
				this.info();
			}
		},

		loadIssuerList: function (models) {
			var self = this;
			self.issuerList = [];
			self.issuerCount = [];
			_.each(models, function(model){
				if( jQuery.inArray(model.get("issuer"),self.issuerList) === -1 ) {
					self.issuerList.push(model.get("issuer"));
					self.issuerCount[model.get("issuer")+"Count"] = 1;
				}else{
					self.issuerCount[model.get("issuer")+"Count"]++;
				}
			});
		},

		searchByPlanType: function (models,planType) {
			var planPresent = false;
			_.each(models, function(model){
				if( planType == model.get("planType")) {
					planPresent = true;
					return planPresent;
				}
			});
			return planPresent;
		},

		searchPlan: function (models,planId) {
			var planModel = "";
			_.each(models, function(model){
				if( model.get("planId") == planId) {
					planModel = model;
				}
			});
			return planModel;
		},

		updateCartSummary : function (cartModels) {
			var self = this;
			self.cartItems = [];

			_.each(cartModels, function(cartModel){
				self.cartItems.push(cartModel.get("planId"));
			});
			this.updateCartCount(cartModels.length);
		},

		updateCartView : function () {
			var self = this;
			_.each(self.cartItems, function(planId){
				self.updateCartButtonToRemove(planId);
			});
		},

		updateCartButtonToRemove : function (planId) {
//			$("#cart_"+planId).addClass("addedItem select btn-primary");
//			$("#cart_"+planId).attr("title",plan_display['pd.label.text.remove']);
//			$("#cart_"+planId + " span").text(plan_display['pd.label.text.remove']);
//          $("#cart_"+planId + " i").removeClass("icon-shopping-cart").addClass("icon-remove");
//			$("#cart_"+planId).parents('.tile').addClass('selected');

			$("#cart_"+planId).addClass("cp-tile__item--selected");
			$("#cart_"+planId).attr("title",plan_display['pd.label.text.remove']);
			$("#cart_"+planId).text(plan_display['pd.label.text.remove']);
		},

		updateCartButtonToAdd : function (planId) {
//			$("#cart_"+planId).attr("class","addToCart btn btn-small ie10");
//			$("#cart_"+planId + " span").text(plan_display['pd.label.title.addToCart']);
//			$("#cart_"+planId + " i").removeClass("icon-remove").addClass("icon-shopping-cart");
//			$("#cart_"+planId).attr("title",plan_display['pd.label.text.addThisPlan']);
//			$("#cart_"+planId).parents('.tile').removeClass('selected');
//			$("#cart_"+planId).removeClass('select');

			$("#cart_"+planId).removeClass("cp-tile__item--selected");
			$("#cart_"+planId).attr("title", plan_display['pd.label.title.add']);
			$("#cart_"+planId).text(plan_display['pd.label.title.add']);
			$("#cart_"+planId).append('<i class="icon-shopping-cart"></i>');
		},

		updateCartCount : function (numPlanInCart) {
			$('#planNumber').text(numPlanInCart);
//	 	    cartDiv = $('.secondary-cart');
//	 	    var indicatedNum = this.setDomForPlanNumber(numPlanInCart);
//	        cartDiv.find('.plan-number').remove();
//	        indicatedNum.appendTo(cartDiv);
//	        indicatedNum.find('.cart-num').addClass('animated fadeInDown');
		},

//		setDomForPlanNumber : function (num) {
// 	        return $('<span class="plan-number"><span class="cart-num">' + num + '</span></span>');
// 	    },

		comparePager : function () {
			var self = this;
			if (self.drawermodels === undefined) {
				self.drawermodels = self.orgmodels;
				self.drawermodels = self._compare(self.drawermodels, this.compareModels);
			}else {
				self.drawermodels = self.orgmodels;
				self.drawermodels = self._compare(self.drawermodels, this.compareModels);
			}
		},

		pager : function () {
			var self = this;

			if(self.nextItem){
				self.start = self.start+1,
				self.stop  = self.stop+1;
				self.nextItem = 0;
			} else if(self.prevItem){
				self.start = self.start-1,
				self.stop  = self.stop-1;
				self.prevItem = 0;
			} else {
				self.start = (self.page-1)*this.perPage;
				self.stop  = self.start+self.perPage;
			}

			if (self.orgmodels === undefined) {
				self.orgmodels = self.models;
				self.totalPlanCount = self.orgmodels.length;
			}

			self.models = self.orgmodels;

			if(self.stop >= self.models.length && self.models.length > this.perPage){
				self.stop = self.models.length;
			}

			if (!_.isEmpty( this.sortColumnList)) {
				self.models = self._sortBy(self.models, this.sortColumnList);
				this._sorted = true;
			}

			if (self.sortColumn) {
				self.models = self._sort(self.models, self.sortColumn, self.sortDirection);
			}

			// Check if field-filtering was set using setFieldFilter
			if ( !_.isEmpty( this.fieldFilterRules ) ) {
				self.models = self._fieldFilter(self.models, this.fieldFilterRules);
			}

			// Check if filtering was set using setFilter.
			if ( this.filterExpression !== "" ) {
				self.models = self._filter(self.models, this.filterFields, this.filterExpression);
			}

			//check whether compare items called or not.
			if ( this.showCompare == true ) {
				self.models = self.orgmodels;
				var sortCol = new Array();
				sortCol.push('planId');
				self.models = self._sortBy(self._compare(self.models, this.compareModels), sortCol);
			}

			//check whether compare items called or not.
			if ( this.showDetail == true ) {
				self.models = self.orgmodels;
				self.models = self._detail(self.models, this.detailModels);
			}

			// If the sorting or the filtering was changed go to the first page
			if ( this.lastSortColumn !== this.sortColumn || this.lastFilterExpression !== this.filterExpression || !_.isEqual(this.fieldFilterRules, this.lastFieldFilterRules) ) {
				self.stop = self.start + this.perPage;

				this.lastSortColumn = this.sortColumn;
				this.lastFieldFilterRules = this.fieldFilterRules;
				this.lastFilterExpression = this.filterExpression;
			}

			// We need to save the sorted and filtered models collection
			// because we'll use that sorted and filtered collection in info().
			self.sortedAndFilteredModels = self.models;
			self.filteredPlanCount = self.sortedAndFilteredModels.length;

			self.reset(
				self.models.slice(self.start,self.stop)
			);
			//Invoke Tile layout styling method:
			this.addingStyleToMiddleTile();
		},

		_sortList:function(models){
			var self = this;
			for(var j=0;j<this.sortColumnList.length;j++){

				models = self._sort(models, this.sortColumnList[j], this.sortDirectionList[j]);
			}
			return models;
		},
		_sort : function (models, sort, direction) {

			var costShareEligible = false;
			if($('#showSilverFirst').val() == 'ON' && sort == 'estimatedTotalHealthCareCost'){
				_.each(models, function(model){
					if(model.get('costSharing') == 'CS2' ||  model.get('costSharing') == 'CS3' ||  model.get('costSharing') == 'CS4' ||  model.get('costSharing') == 'CS5' ||  model.get('costSharing') == 'CS6'){
						costShareEligible = true;
					}
				});
			}

			models = models.sort(function(modela,modelb) {
				var a = modela.get(sort),
					b = modelb.get(sort);

				var direction1 = direction;

				if(a!=0 && b!=0 && a==b){
					if($("#gpsVersion").val() == 'V1' && sort!='smartScore'){
						a = modela.get('smartScore');
						b = modelb.get('smartScore');
						direction1 = 'desc';
					}else if($("#gpsVersion").val() != 'V1' && sort=='premium'){
						a = modela.get('estimatedTotalHealthCareCost');
						b = modelb.get('estimatedTotalHealthCareCost');
						direction1 = 'asc';
					}else{
						a = modela.get('premium');
						b = modelb.get('premium');
						direction1 = 'asc';
					}
				}

				if(costShareEligible == true && (modela.get('level') == 'SILVER' || modelb.get('level') == 'SILVER')){
					a = modela.get('level');
					b = modelb.get('level');
					direction1 = 'desc';
					if(a==b){
						a = modela.get('premium');
						b = modelb.get('premium');
						direction1 = 'asc';
					}
				}

				if (direction1 === 'desc') {
					if (a > b) {
						return -1;
					}

					if (a < b) {
						return 1;
					}
				} else {
					if (a < b) {
						return -1;
					}

					if (a > b) {
						return 1;
					}
				}

				return 0;
			});

			var planIdToMatch = null;
			if($('#showCurrentPlanFirst').val() == 'ON' && sort == $('#defaultSort').val() && $('#enrollmentType').val() == 'A' && ($('#initialHealthPlanId').val() != null || $('#initialDentalPlanId').val() != null)){
				if($("#insuranceType").val() == 'HEALTH'){
					planIdToMatch = $('#initialHealthPlanId').val();
				}
				if($("#insuranceType").val() == 'DENTAL'){
					planIdToMatch = $('#initialDentalPlanId').val();
				}
				var i=0;
				_.each(models, function(model){
					if(planIdToMatch != null && model.get('planId') == planIdToMatch){
						temp = model;
						models.splice(i,1);
						models.unshift(temp);
					}
					i++;
				});
			}

			return models;

		},

		sortBy : function(sortAttributes){
			if (arguments.length) {
				this._sortAttributes = arguments;
			}
			this.models = this._sortBy(this.models, this._sortAttributes);
			this._sorted = true;
		},
		_sortBy : function(models,attributes){
			var attr, that = this;

			if (!attributes.length) {
				return this.models;
			}
			attr = attributes[0];
			// Base case
			if (attributes.length === 1) {
				return _(models).sortBy(function(model) {
					return model.get(attr);
				});
			}
			// Splits up models by sort attribute,
			// and then does a recursive sort on each group
			else {
				models = _(models).chain().sortBy(function(model) {
					return model.get(attr);
				}).groupBy(function(model) {
					return model.get(attr);
				}).toArray().value();

				attributes = _.last(attributes, attributes.length - 1);
				_(models).each(function(modelSet, index) {
					models[index] = that._sortBy(models[index], attributes);
				});
				return _(models).flatten();
			}

			//Invoke Tile layout styling method:
			this.addingStyleToMiddleTile();
		},
			// The actual place where the collection is field-filtered.
		// Check setFieldFilter for arguments explicacion.
		_fieldFilter: function( models, rules ) {

			// Check if there are any rules
			if ( _.isEmpty(rules) ) {
				return models;
			}

			var filteredModels = [];

			// Iterate over each rule
			_.each(models, function(model){

				var should_push = true;

				// Apply each rule to each model in the collection
				_.each(rules, function(rule){

					// Don't go inside the switch if we're already sure that the model won't be included in the results
					if( !should_push ){
						return false;
					}

					should_push = false;

					// The field's value will be passed to a custom function, which should
					// return true (if model should be included) or false (model should be ignored)
					if(rule.type === "function"){
						var f = _.wrap(rule.value, function(func){
							return func( model.get(rule.field) );
						});
						if( f() ){
							should_push = true;
						}

					// The field's value is required to be non-empty
					}else if(rule.type === "required"){
						if( !_.isEmpty( model.get(rule.field).toString() ) ) {
							should_push = true;
						}

					// The field's value is required to be greater tan N (numbers only)
					}else if(rule.type === "min"){
						if( !_.isNaN( Number( model.get(rule.field) ) ) &&
							!_.isNaN( Number( rule.value ) ) &&
							Number( model.get(rule.field) ) >= Number( rule.value ) ) {
							should_push = true;
						}

					// The field's value is required to be smaller tan N (numbers only)
					}else if(rule.type === "max"){
						if( !_.isNaN( Number( model.get(rule.field) ) ) &&
							!_.isNaN( Number( rule.value ) ) &&
							Number( model.get(rule.field) ) <= Number( rule.value ) ) {
							should_push = true;
						}

					// The field's value is required to be between N and M (numbers only)
					}else if(rule.type === "range"){
						for(var i=0;i<rule.value.length;i++){
						  if( !_.isNaN( Number( model.get(rule.field) ) ) &&
							_.isObject( rule.value[i] ) &&
							!_.isNaN( Number( rule.value[i].min ) ) &&
							!_.isNaN( Number( rule.value[i].max ) ) &&
							Number( model.get(rule.field) ) >= Number( rule.value[i].min ) &&
							Number( model.get(rule.field) ) < Number( rule.value[i].max ) ) {
							should_push = true;
						  }
					    }

					// The field's value is required to be more than N chars long
					}else if(rule.type === "minLength"){
						if( model.get(rule.field).toString().length >= rule.value ) {
							should_push = true;
						}

					// The field's value is required to be no more than N chars long
					}else if(rule.type === "maxLength"){
						if( model.get(rule.field).toString().length <= rule.value ) {
							should_push = true;
						}

					// The field's value is required to be more than N chars long and no more than M chars long
					}else if(rule.type === "rangeLength"){
						if( _.isObject( rule.value ) &&
							!_.isNaN( Number( rule.value.min ) ) &&
							!_.isNaN( Number( rule.value.max ) ) &&
							model.get(rule.field).toString().length >= rule.value.min &&
							model.get(rule.field).toString().length <= rule.value.max ) {
							should_push = true;
						}

					// The field's value is required to be equal to one of the values in rules.value
					}else if(rule.type === "oneOf"){
						if( _.isArray( rule.value ) &&
							_.include( rule.value, model.get(rule.field) ) ) {
							should_push = true;
						}

					// The field's value is required to be equal to the value in rules.value
					}else if(rule.type === "equalTo"){
						for(var i=0;i<rule.value.length;i++){
						  if( rule.value[i] === model.get(rule.field) ) {
							should_push = true;
						  }
						}

					// The field's value is required to match the regular expression
					}else if(rule.type === "pattern"){
						if( rule.value.toString().match(model.get(rule.field).toString()) ) {
							should_push = true;
						}

					//csr filter
					}
					else if(rule.type === "jsonObject"){
						if(rule.field === "doctors"){
							var hasDoctor = true;
							for(var k=0;k<rule.value.length;k++){
								var hasPlan = false;
								var doctorsListArr = model.get(rule.field);
								_.each(doctorsListArr, function(doctorName){
									if(rule.value[k].toString().match(doctorName.providerId.toString()) && doctorName.networkStatus == "inNetWork" ){
										hasPlan =  true;
										return false;
									}
								});
								if(hasPlan == false){
									hasDoctor = false ;
								}

							}
							if(hasDoctor == true){
								should_push = true;
							}
						}else{
							should_push = false;
						}
					}
					else if(rule.type === "compare"){
						for(var i=0;i<rule.value.length;i++){
							if( rule.field == 'costSharing'){
							  if(model.get(rule.field).toString() != 'CS1') {
								should_push = true;
							  }
							}
							}
					}// Unknown type
					else{
						should_push = false;
					}

				});

				if( should_push ){
					filteredModels.push(model);
				}

			});
			self.filteredPlanCount = filteredModels.length;
			return filteredModels;
		},

		// The actual place where the collection is filtered.
		// Check setFilter for arguments explicacion.
		_filter: function ( models, fields, filter ) {

			//  For example, if you had a data model containing cars like { color: '', description: '', hp: '' },
			//  your fields was set to ['color', 'description', 'hp'] and your filter was set
			//  to "Black Mustang 300", the word "Black" will match all the cars that have black color, then
			//  "Mustang" in the description and then the HP in the 'hp' field.
			//  NOTE: "Black Musta 300" will return the same as "Black Mustang 300"

			// We accept fields to be a string or an array,
			// but if string is passed we need to convert it
			// to an array.

			var self = this;

			if( _.isString( fields ) ) {
				var tmp_s = fields;
				fields = [];
				fields.push(tmp_s);
			}



			// 'filter' can be only a string.
			// If 'filter' is string we need to convert it to
			// a regular expression.
			// For example, if 'filter' is 'black dog' we need
			// to find every single word, remove duplicated ones (if any)
			// and transform the result to '(black|dog)'
			if( filter === '' || !_.isString(filter) ) {
				return models;
			} else {
				filter = filter.match(/\w+/ig);
				filter = _.uniq(filter);
				var pattern = "(" + filter.join("|") + ")";
				var regexp = new RegExp(pattern, "igm");
			}

			var filteredModels = [];

			// We need to iterate over each model
			_.each( models, function( model ) {

				var matchesPerModel = [];

				// and over each field of each model
				_.each( fields, function( field ) {

					var value = model.get( field );

					if( value ) {

						// The regular expression we created earlier let's us to detect if a
						// given string contains each and all of the words in the regular expression
						// or not, but in both cases match() will return an array containing all
						// the words it matched.
						var matchesPerField;

						matchesPerField = value.toString().match( regexp );

						matchesPerField = _.map(matchesPerField, function(match) {
							return match.toString().toLowerCase();
						});

						_.each(matchesPerField, function(match){
							matchesPerModel.push(match);
						});

					}

				});

				// We just need to check if the returned array contains all the words in our
				// regex, and if it does, it means that we have a match, so we should save it.
				matchesPerModel = _.uniq( _.without(matchesPerModel, "") );

				if(  _.isEmpty( _.difference(filter, matchesPerModel) ) ) {
					filteredModels.push(model);
				}

			});

			return filteredModels;
		},

		//multiple items Compare Functionality
		// arguments
		// models: initial collection of items
		//selPlans : array of selected items
		_compare: function( models, selPlans ) {

			// Check if there are any rules
			if ( _.isEmpty(selPlans) ) {
				return models;
			}

			var comparedModels = [];

			// Iterate over each rule
			_.each(models, function(model){
				// Apply each rule to each model in the collection
				_.each(selPlans, function(cmp){
					//if selected id matches with id from model, then push to comparedModels
					if(model.id == cmp.replace("compare_","")){
						comparedModels.push(model);

					}
				});
			});
			return comparedModels;
		},

		_detail: function( models, selPlans ) {
			var detailModels = [];

			// Iterate over each rule
			_.each(models, function(model){
				_.each(selPlans, function(detail){
					//if selected id matches with id from model, then push to comparedModels
					if(model.id == detail.replace("detail_","")){
						detailModels.push(model);

					}
				});
			});

			return detailModels;
		},

		info : function () {

			var self = this,
				info = {},
				totalRecords = (self.sortedAndFilteredModels) ? self.sortedAndFilteredModels.length : self.length,
				totalPages = Math.ceil(totalRecords/self.perPage);

			info = {
				totalRecords  : totalRecords,
				page          : self.page,
				perPage       : self.perPage,
				totalPages    : totalPages,
				lastPage      : totalPages,
				lastPagem1    : totalPages-1,
				previous      : false,
				next          : false,
				page_set      : [],
				startRecord   : (self.page - 1) * self.perPage + 1,
				endRecord     : Math.min(totalRecords, self.page * self.perPage)
			};

			if (self.page > 1) {
				info.prev = self.page - 1;
			}

			if (self.page < info.totalPages) {
				info.next = self.page + 1;
			}

			info.pageSet = self.setPagination(info);

			self.information = info;
			return info;
		},


		updateCompareSummary : function () {
			var self = this;
			/*if compare view*/
			if(this.showCompare===true || this.showDetail===true){
				$('.pagination').hide();
				$('.ps-sidebar').hide();
				$('.back a').hide();
				$('.tiles').find('#rightpanel').addClass('tileRightPanel benefit-details').removeClass('span9');
				$('.choose-plan').hide();
				$('.comparePlans').hide();
				$('.tooltip').hide();
				$('.js-plan-compare-widget').hide();
			}

			if(this.showCompare===true){
				$('.backToAll').show();
				$('.detailBackToAll').hide();
				$('.comparing-plans').show();
				$('#detail').hide();
				$('#compare').show();
				$('.addToCompare').hide();
				$('#totalCompareCount').html(this.compareModels.length);
				$('#filteredPlanCount').html(this.compareModels.length);

				$('.plan-display-show, .plan-detail-show').hide();
				$('.plan-compare-show').show();

				//temporary fix
				$('body').css('min-width', '576px');
			}else if(this.showDetail===true){
				$('[rel=tooltip]').tooltip('destroy');
				$('.backToAll').hide();
				$('.detailBackToAll').show();
				$('#detail').show();
				$('.detail-plan').show();
				$('.comparing-plans').hide();
				$('#compare').hide();
				$('.choose-plan').hide();

				$('.plan-display-show, .plan-compare-show').hide();
				$('.plan-detail-show').show();
				$('body').css('min-width', '0');

			}else{/*if no compare view*/
				$('.comparePlans').show();
				$('.compareNow').hide();
				$('.backToAll').hide();
				$('.detailBackToAll').hide();
				$('.addToCompare').show();
				$('.addToCompareLbl').show();
				$('#planTR').show();
				$('#compareTR').hide();
				$('.ps-sidebar').show();
				$('.tiles').find('#rightpanel').removeClass('tileRightPanel benefit-details').addClass('span9');
				$('.comparing-plans').hide();
				$('.choose-plan').show();
				$('.backToAll').css('style', '');
				$('.back a').show();
				$('#detail, .detail-plan').hide();
				$('#compare').show();

				$('#filteredPlanCount').html(this.filteredPlanCount);
				$('#compareCount').html(this.compareModels.length);
				$('#totalPlanCount').html(this.totalPlanCount);

				$('.plan-detail-show, .plan-compare-show').hide();
				$('.plan-display-show').show();
				$('body').css('min-width', '0');


			}

			var csr = $('#csr').val();
			if(csr === 'CS0' || csr === 'CS1'|| csr === ''){
				$('.ps-non-csr-show').css('display', 'inline-block');
			}else{
				$('.ps-csr-show').css('display', 'inline-block');
			}

			/*if plans in compare then show remove button*/
			_.each(this.compareModels, function(remId){
//				$('#compare_'+remId).hide();
//				$('#remove_'+remId).show();
//				$('#compareLbl_'+remId).hide();
//				$('#removeLbl_'+remId).show();
//				$('#compareChk_'+remId).removeAttr('checked');
//				$('#removeChk_'+remId).attr('checked','checked');
//				$('#removeLbl_'+remId).parents('.tile').addClass('compared-selected');
//				$('#removeLbl_'+remId).parents('.input-append').addClass('compared');

				$('label[for=compareChk_' + remId + ']').addClass('cp-tile__item--selected');
				$('#compareChk_'+remId).prop('checked', true);


			});
		},

		drawPie : function (scorenum) {
            var val = scorenum;
            if (val >= 50) {
	           $('.score_'+scorenum).parent('div').addClass('score50').prepend('<div class="pie fill pie-color"></div>');
	         }
	         if (val < 50) {
	        	 $('.score_'+scorenum).parent('div').addClass('lessfifty');
	         }

	         var deg = 360/100*scorenum;
	         $('.score_'+scorenum).css({
	                  '-moz-transform':'rotate('+deg+'deg)',
	                  '-webkit-transform':'rotate('+deg+'deg)',
	                  '-o-transform':'rotate('+deg+'deg)',
	                  'transform':'rotate('+deg+'deg)'
	            });
	     },

		addingStyleToMiddleTile: function(){
        	/*
        		Clear the tiles after every 3rd Tile and add after new clearfix element to clear the float layout
        		and css class to every second tile:
        	*/
			//console.log('fired from backbone js');
//			$('#mainSummary').find('.jsTilesClearFix').remove();
//			$('#mainSummary .tile').css({marginLeft: 0, marginRight:0}); //Required to overwrite .tile:nth-child(3n-1) from css as it is using on other pages
//			$('#mainSummary .tile:nth-of-type(2n)').removeClass('middleTile'); //First remove clearfix and middleTile class as event fires twice so needed these code
//        	$('#mainSummary .tile:nth-of-type(3n)').after('<div class="clearfix jsTilesClearFix"></div>');
//        	$('#mainSummary .tile:nth-of-type(2n)').addClass('middleTile');
		},

		preserveOtherOptions: function () {
			var self = this;
			if(this.fieldFilterRules.planType){
				$('#planType').val(this.fieldFilterRules.planType.value);
			}

			$("#"+this.sortColumn).prop('checked', true);
			//$('.sort').dropkick();

			//scrolls page to the top
			$('.tile-header a.detail').on('click', function(){
    	   		$('html, body').animate({scrollTop: 0}, 'slow');
    	   	});

			this.updateCompareSummary();
			this.updateCartView();

			$('.circle-num').each(function(){
            	var scorenum = $(this).html();
            	self.drawPie(scorenum);
           	});

			if(+$('#compareCount').text() > 0){
				$('.js-plan-compare-widget').show();
			}

		},

		loadIssuers : function () {
			var self = this;
			$('#issuerList').html("");
			_.each(self.issuerList, function(issuerFilter){
				var html = '<input type="checkbox" id="issuer_'+issuerFilter.replace(/[\s\.?]/g,"_")+'" class="issuers" value="'+issuerFilter+'" /> '+issuerFilter+' ('+self.issuerCount[issuerFilter+'Count']+')<br/>';
				$('#issuerList').append(html);
			});
		},

		setPagination : function (info) {
			var pages = [];
			// How many adjacent pages should be shown on each side?
			var ADJACENT = 3;
			var ADJACENTx2 = ADJACENT*2;
			var LASTPAGE = Math.ceil(info.totalRecords/info.perPage);
			var LPM1 = -1;

			if (LASTPAGE > 1) {
				//not enough pages to bother breaking it up
				if (LASTPAGE < (7 + ADJACENTx2)) {
					for (var i=1,l=LASTPAGE; i <= l; i++) {
						pages.push(i);
					}
				}
				// enough pages to hide some
				else if (LASTPAGE > (5 + ADJACENTx2)) {

					//close to beginning; only hide later pages
					if (info.page < (1 + ADJACENTx2)) {
						for (var i=1, l=4+ADJACENTx2; i < l; i++) {
							pages.push(i);
						}
					}

					//in middle; hide some front and some back
					else if(LASTPAGE - ADJACENTx2 > info.page && info.page > ADJACENTx2) {
						for (var i = info.page - ADJACENT; i <= info.page + ADJACENT; i++) {
							pages.push(i);
						}
					}
					//close to end; only hide early pages
					else{
						for (var i = LASTPAGE - (2 + ADJACENTx2); i <= LASTPAGE; i++) {
							pages.push(i);
						}
					}
				}
			}

			return pages;
		}
	};

})(App.backbone);
