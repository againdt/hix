﻿define(['textbox'], function (txtbox) {
    var email = new baseComponent();    
    email.type = "email";
    email.getHTML = function (cgfObj) {
        var defCfg = {
            etype:"email"
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;
        var vm = this;
        vm.context.registerComponent(txtbox);        
        var textbox = vm.context.comp["textbox"].getHTML(compData);
        var $email = $(textbox);
        return $email[0].outerHTML;
    };
    return email;
});