/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.batch.enrollment.util.Enrollment1095XmlWrapper;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public interface Enrollment1095XmlBatchService {
	
	/**
	 * Generate Annual IRS XML for given set of enrollment 1095s
	 * @param enrollment1095Ids
	 * @param docSeqId
	 * @param wipFolder 
	 * @param recordSequenceIdMap 
	 * @param batchCategoryCode 
	 * @param enrollment1095XmlWrapper 
	 * @return
	 * @throws GIException 
	 */
	boolean processXmlForEnrollment1095(List<Integer> enrollment1095Ids, Integer docSeqId, String wipFolder, Map<Integer, String> recordSequenceIdMap, String batchCategoryCode, Enrollment1095XmlWrapper enrollment1095XmlWrapper)
					throws GIException;
}
