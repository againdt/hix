import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocVisitComponent } from './doc-visit.component';

describe('DocVisitComponent', () => {
  let component: DocVisitComponent;
  let fixture: ComponentFixture<DocVisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocVisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
