
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;



/**
 * A data type for the incarceration information. 
 *             
 * 
 * <p>Java class for SSAIncarcerationInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSAIncarcerationInformationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PrisonerIdentification"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PrisonerConfinementDate"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}ReportingPersonText"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SupervisionFacility"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}InmateStatusIndicator"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSAIncarcerationInformationType", propOrder = {
    "prisonerIdentification",
    "prisonerConfinementDate",
    "reportingPersonText",
    "supervisionFacility",
    "inmateStatusIndicator"
})
public class SSAIncarcerationInformationType
    extends ComplexObjectType
{

    @XmlElement(name = "PrisonerIdentification", required = true)
    protected String prisonerIdentification;
    @XmlElement(name = "PrisonerConfinementDate", required = true)
    protected String prisonerConfinementDate;
    @XmlElement(name = "ReportingPersonText", required = true)
    protected String reportingPersonText;
    @XmlElement(name = "SupervisionFacility", required = true)
    protected SupervisionFacilityType supervisionFacility;
    @XmlElement(name = "InmateStatusIndicator", required = true)
    protected Boolean inmateStatusIndicator;

    /**
     * Gets the value of the prisonerIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrisonerIdentification() {
        return prisonerIdentification;
    }

    /**
     * Sets the value of the prisonerIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrisonerIdentification(String value) {
        this.prisonerIdentification = value;
    }

    /**
     * Gets the value of the prisonerConfinementDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrisonerConfinementDate() {
        return prisonerConfinementDate;
    }

    /**
     * Sets the value of the prisonerConfinementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrisonerConfinementDate(String value) {
        this.prisonerConfinementDate = value;
    }

    /**
     * Gets the value of the reportingPersonText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingPersonText() {
        return reportingPersonText;
    }

    /**
     * Sets the value of the reportingPersonText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingPersonText(String value) {
        this.reportingPersonText = value;
    }

    /**
     * Gets the value of the supervisionFacility property.
     * 
     * @return
     *     possible object is
     *     {@link SupervisionFacilityType }
     *     
     */
    public SupervisionFacilityType getSupervisionFacility() {
        return supervisionFacility;
    }

    /**
     * Sets the value of the supervisionFacility property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupervisionFacilityType }
     *     
     */
    public void setSupervisionFacility(SupervisionFacilityType value) {
        this.supervisionFacility = value;
    }

    /**
     * Gets the value of the inmateStatusIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getInmateStatusIndicator() {
        return inmateStatusIndicator;
    }

    /**
     * Sets the value of the inmateStatusIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInmateStatusIndicator(Boolean value) {
        this.inmateStatusIndicator = value;
    }

}
