package com.getinsured.hix.broker.history;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.audit.service.HistoryRendererService;

/**
 * This class is used to populate the List<Map<String, Object>> base on the data
 * got from REV_history or AUD tables. Populated List<Map<String, Object>> can
 * be used to display the corresponding data as History pages in UI : The
 * data currently taken is whole data.This should have been paginated to give
 * optimum results.
 */
@Service
public class BrokerHistoryRendererImpl implements HistoryRendererService {

	private static final String PROCESS_DOCUMENT_VALUES_END = "processDocumentValues : END";

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerHistoryRendererImpl.class);

	// private static final String COMMENT_TEXT_PRE =
	// "<a onclick=\"getComment('";
	// private static final String COMMENT_TEXT_POST =
	// "');\" href='#modalBox' data-toggle='modal'>View Comment</a>";
	private static final String COMMENT_TEXT_PRE = "<a onclick=\"getComment('";
	private static final String COMMENT_TEXT_POST = "');\" href='#modalBox' data-toggle='modal'>View Comment</a>";
	public static final String TIMESTAMP_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String REPOSITORY_NAME = "iBrokerRepository";
	public static final String MODEL_NAME = "com.getinsured.hix.model.Broker";
	public static final int UPDATE_DATE = 1;
	public static final int PREVIOUS_STATUS = 2;
	public static final int NEW_STATUS = 3;
	public static final int COMMENT = 4;
	public static final int ATTACHMENT = 5;
	public static final String UPDATED_DATE_COL = "updated";
	public static final String COMMENT_COL = "comments";
	public static final String ATTACHMENT_COL_E_O_DECL_PAGE = "eoDeclarationDocumentId";
	public static final String ATTACHMENT_COL_CONTRACT = "contractDocumentId";
	public static final String ATTACHMENT_COL_SUPPORTING = "supportingDocumentId";
	public static final String STATUS_COL = "certificationStatus";
	public static final String PREVIOUS_STATUS_COL = "previousCertificationStatus";
	public static final String NEW_STATUS_COL = "newCertificationStatus";
	public static final String STATUS_FILE_COL = "supportFile";
	public static final String LINE_SEP = "line.separator";
	public static final String HASH_SEP = "#";
	public static final String APOSTROPHE_STRING = "apostrophes";
	public static final String APOSTROPHE = "'";
	public static final String NO_COMMENTS = "No Comments";

	/**
	 * Populates the List<Map<String, Object>> which can be used to display the
	 * corresponding data as History pages in UI
	 * 
	 * @param data
	 *            List<Map<String, String>> input of the entire revision history
	 *            data.
	 * @param compareColumns
	 *            List<String> of column names based on which the filteration of @param
	 *            data should be done.
	 * @param columnsToDisplay
	 *            List<Integer> of column ids specified in corresponding
	 *            HistoryService, which will indicate newly prepared map should
	 *            contain what all keys.
	 * @return List<Map<String, Object>> Filtered data based on @param data, @param
	 *         compareColumns and @param columnsToDisplay.
	 */
	@Override
	public List<Map<String, Object>> processData(List<Map<String, String>> data, List<String> compareColumns,
			List<Integer> columnsToDisplay) {
		LOGGER.info("processData : START");
		List<Map<String, String>> orderedData = new ArrayList<Map<String, String>>(data);
		Collections.reverse(orderedData);
		int size = orderedData.size();
		Map<String, String> firstElement = null;
		Map<String, String> secondElement = null;
		boolean allSameCertificationStatusRecords = true;
		List<Map<String, Object>> processedData = new ArrayList<Map<String, Object>>();
		{
			firstElement = orderedData.get(0);
		}
		for (int i = 0; i < size - 1; i++) {
			firstElement = orderedData.get(i);
			secondElement = orderedData.get(i + 1);
			for (String keyColumn : compareColumns) {
				/*
				 * to incorporate same status record in Certification history
				 * when comments added
				 */
				if (!isEqual(firstElement, secondElement, keyColumn)) {

					processedData.add(formMap(firstElement, secondElement, columnsToDisplay));
					allSameCertificationStatusRecords = false;

				} else {
					if (checkCommentAndAttachmentDecl(firstElement, secondElement)) {
						processedData.add(formMap(firstElement, secondElement, columnsToDisplay));
						allSameCertificationStatusRecords = false;
					}
					
					if(checkContractAndAttchmentSup(firstElement, secondElement)) {
						processedData.add(formMap(firstElement, secondElement, columnsToDisplay));
						allSameCertificationStatusRecords = false;
					}

				}

			}

		}

		// Following Code is removed to avoid duplicate records from being
		// displayed for a new Broker
		// This comparison is done to get the last element in the list.
		// Also if there is only one element in the list.

		if (size == 1 || allSameCertificationStatusRecords) {
			for (String keyColumn : compareColumns) {
				processedData.add(formMap(firstElement, firstElement, columnsToDisplay));
			}
		}
		LOGGER.info("processData : END");
		return processedData;
	}

	private boolean checkContractAndAttchmentSup(
			Map<String, String> firstElement, Map<String, String> secondElement) {
		return checkIfElementIsEmpty(firstElement, secondElement, ATTACHMENT_COL_CONTRACT)
				|| checkIfElementIsEmpty(firstElement, secondElement, ATTACHMENT_COL_SUPPORTING);
	}

	private boolean checkCommentAndAttachmentDecl(
			Map<String, String> firstElement, Map<String, String> secondElement) {
		return checkIfElementIsEmpty(firstElement, secondElement, COMMENT_COL)
				|| checkIfElementIsEmpty(firstElement, secondElement, ATTACHMENT_COL_E_O_DECL_PAGE);
	}

	private boolean checkIfElementIsEmpty(Map<String, String> firstElement,
			Map<String, String> secondElement, String attachmentColContract) {
		return (!firstElement.get(attachmentColContract).isEmpty()) && (!secondElement.get(attachmentColContract).equalsIgnoreCase(firstElement.get(attachmentColContract)));
	}

	/**
	 * Compares values of maps firstElement and secondElement for key keyColumn.
	 */
	private boolean isEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		return firstElement.get(keyColumn).equals(secondElement.get(keyColumn));
	}

	/**
	 * Forms map for given key. The newly formed map contains key-value pairs
	 * based on the list provided as columnsToDisplay.
	 */
	private Map<String, Object> formMap(Map<String, String> firstMap, Map<String, String> secondMap,
			List<Integer> columnsToDisplay) {
		LOGGER.info("formMap : START");
		Map<String, Object> values = new HashMap<String, Object>();
		for (int colId : columnsToDisplay) {
			switch (colId) {
			case UPDATE_DATE:
				String dt = firstMap.get(UPDATED_DATE_COL);
				SimpleDateFormat sdf = new SimpleDateFormat();
				sdf.applyPattern(TIMESTAMP_PATTERN1);
				try {
					values.put(UPDATED_DATE_COL, sdf.parse(dt));
				} catch (ParseException e) {
					LOGGER.error("Unable to parse Updated Date " + dt + " ", e);
				}
				break;

			case COMMENT:
				processComments(firstMap, secondMap, values);
				break;

			case PREVIOUS_STATUS:
				values.put(PREVIOUS_STATUS_COL, secondMap.get(STATUS_COL));
				break;

			case NEW_STATUS:
				values.put(NEW_STATUS_COL, firstMap.get(STATUS_COL));
				break;

			case ATTACHMENT:
				processAttachment(firstMap, secondMap, values);
				
				break;
			}
		}
		LOGGER.info("formMap : END");
		return values;
	}

	private void processAttachment(Map<String, String> firstMap,
			Map<String, String> secondMap, Map<String, Object> values) {
		String valueString = null;
		
		valueString = this.processDocumentValues(firstMap, secondMap, ATTACHMENT_COL_E_O_DECL_PAGE);
		
		if(null == valueString) {
			values.put(ATTACHMENT_COL_E_O_DECL_PAGE, null);
		} 
		else {
			values.put(ATTACHMENT_COL_E_O_DECL_PAGE, valueString);
		}
		
		valueString = null;
		valueString = this.processDocumentValues(firstMap, secondMap, ATTACHMENT_COL_CONTRACT);
		
		if(null == valueString) {
			values.put(ATTACHMENT_COL_CONTRACT, null);
		} 
		else {
			values.put(ATTACHMENT_COL_CONTRACT, valueString);
		}

		valueString = null;
		valueString = this.processDocumentValues(firstMap, secondMap, ATTACHMENT_COL_SUPPORTING);
		
		if(null == valueString) {
			values.put(ATTACHMENT_COL_SUPPORTING, null);
		} 
		else {
			values.put(ATTACHMENT_COL_SUPPORTING, valueString);
		}
	}

	private void processComments(Map<String, String> firstMap,
			Map<String, String> secondMap, Map<String, Object> values) {
		String comments = null;
		String firstComment = firstMap.get(COMMENT_COL);
		String secondComment = secondMap.get(COMMENT_COL);
		
		if(null == firstComment)  {
			values.put(COMMENT_COL, NO_COMMENTS);					
		}
		else {
			if(null == secondComment) {
				comments = firstComment.trim();

				comments = this.processComments(comments);

				if (comments == null) {
					values.put(COMMENT_COL, NO_COMMENTS);
				} 
				else {
					values.put(COMMENT_COL, comments);
				}
			}
			else {
				firstComment = firstComment.trim();
				secondComment = secondComment.trim();
				
				if(firstComment.isEmpty()) {
					values.put(COMMENT_COL, NO_COMMENTS);
				}
				else {
					if(secondComment.isEmpty()) {
						comments = firstComment.trim();
						
						comments = this.processComments(comments);

						if (comments == null) {
							values.put(COMMENT_COL, NO_COMMENTS);
						} 
						else {
							values.put(COMMENT_COL, comments);
						}
					}
					else {
						if(firstComment.equals(secondComment)) {
							values.put(COMMENT_COL, NO_COMMENTS);
						}
						else {
							comments = firstComment.trim();
							
							comments = this.processComments(comments);

							if (comments == null) {
								values.put(COMMENT_COL, NO_COMMENTS);
							} 
							else {
								values.put(COMMENT_COL, comments);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Method 'processComments' to process comments for display .
	 *
	 * @author chalse_v 
	 * @param rawComment The raw comment.
	 * @return processedComment The processed comments.
	 */
	private String processComments(String rawComment) {
		LOGGER.info("processComments : START");
		String processedComment = null;
		
		if (rawComment != null && !rawComment.trim().isEmpty()) { 
			/* To replace newline break with # so that later on <br> can be inserted in its place.*/
			/* To replace Single Quote with predefined string which will be replaced later on. */
			processedComment = rawComment.trim().replace(System.getProperty(LINE_SEP), HASH_SEP).replace(APOSTROPHE, APOSTROPHE_STRING);
			processedComment = StringEscapeUtils.escapeHtml(processedComment);
			processedComment = COMMENT_TEXT_PRE + processedComment + COMMENT_TEXT_POST;
		} 
		LOGGER.info("processComments : END");
		return processedComment;
	}
	
	/**
	 * Method 'processDocumentValues' to process document entry value for display.
	 *
	 * @author chalse_v 
	 * 
	 * @param firstElementMap The first entry collection.
	 * @param secondElementMap The second entry collection.
	 * @param key THe lookup key for value in collection.
	 * @return String <code>null</code> for no entry to display, <code>String</code> The entry string to display.
	 */
	private String processDocumentValues(final Map<String, String> firstElementMap, final Map<String, String> secondElementMap, final String key) {
		LOGGER.info("processDocumentValues : START");
		String firstDoc = firstElementMap.get(key);
		String secondDoc = secondElementMap.get(key);
		
		if(null == firstDoc) {
			LOGGER.info(PROCESS_DOCUMENT_VALUES_END);
			return null;
		}
		else {
			if(null == secondDoc) {
				LOGGER.info(PROCESS_DOCUMENT_VALUES_END);
				return firstDoc;
			}
			else {
				firstDoc = firstDoc.trim();
				secondDoc = secondDoc.trim();
				
				if(firstDoc.isEmpty()) {
					LOGGER.info(PROCESS_DOCUMENT_VALUES_END);
					return null;
				}
				else {
					if(secondDoc.isEmpty()) {
						LOGGER.info(PROCESS_DOCUMENT_VALUES_END);
						return firstDoc;
					}
					else {
						if(firstDoc.equals(secondDoc)) {
							LOGGER.info(PROCESS_DOCUMENT_VALUES_END);
							return null;
						}
						else {
							LOGGER.info(PROCESS_DOCUMENT_VALUES_END);
							return firstDoc;
						}
					}
				}
			}
			
		}
	}
}
