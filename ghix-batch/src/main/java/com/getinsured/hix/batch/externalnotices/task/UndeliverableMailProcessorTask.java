package com.getinsured.hix.batch.externalnotices.task;

import com.getinsured.hix.batch.externalnotices.service.NoticeBatchService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.timeshift.sql.TSTimestamp;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class UndeliverableMailProcessorTask extends StepExecutionListenerSupport implements Tasklet {
    private static final Logger LOGGER = Logger.getLogger(UndeliverableMailProcessorTask.class);
    private static volatile Boolean isBatchRunning = false;

    private String filePathName;
    private String startDate;
    private String endDate;
    private NoticeBatchService noticeBatchService;

    public String getFilePathName() {
        return filePathName;
    }

    public void setFilePathName(String filePathName) {
        this.filePathName = filePathName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public NoticeBatchService getNoticeBatchService() {
        return noticeBatchService;
    }
    public void setNoticeBatchService(NoticeBatchService noticeBatchService) {
        this.noticeBatchService = noticeBatchService;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        synchronized (isBatchRunning) {
            if(isBatchRunning) {
                return RepeatStatus.FINISHED;
            } else {
                isBatchRunning = true;
            }
        }

        try {
            if (!StringUtils.isNotBlank(filePathName)) {
                filePathName = DynamicPropertiesUtil.getPropertyValue("iex.notifications.undeliverable.cron.filePath");
            }

            if (StringUtils.isNotBlank(filePathName)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(this.getClass().getName() + " started at " + new Timestamp(System.currentTimeMillis()));
                }
                noticeBatchService.processUndeliverableMail(filePathName);
            } else {
                LOGGER.error("execute::error filepath not present in config or params");
            }
        } catch(Exception ex) {
            LOGGER.error("execute::error while executing", ex);
        } finally{
            isBatchRunning = false;
        }

        LOGGER.debug(this.getClass().getName() + " finishing at " + new Timestamp(System.currentTimeMillis()));
        return RepeatStatus.FINISHED;
    }

}
