package com.getinsured.eligibility.at.mes.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.mes.common.MESProcessingConstants;
import com.getinsured.eligibility.at.mes.queue.dto.ATSpanInfoRequestDTO;
import com.getinsured.eligibility.at.mes.queue.dto.ATQueueInfoResponseDTO;
import com.getinsured.eligibility.at.mes.repository.ATSpanProcessingRepository;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.enums.AccountTransferProcessStatus;
import com.getinsured.eligibility.enums.AccountTransferQueueStatus;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.iex.ssap.model.AccountTransferSpanInfo;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.sql.TSTimestamp;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * @author Aarti Deorukhkar
 *
 */
@Service("atSpanProcessingService")
public class AtSpanProcessingServiceImpl implements AtSpanProcessingService{

	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(AtSpanProcessingServiceImpl.class);
	private static final int FIRST_SPAN = 1;
	private static final int MAX_ERROR_MESSAGE_SIZE = 99;
	
	@Autowired private Gson platformGson;	
	
	@Autowired
	ATSpanProcessingRepository atSpanProcessingRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private QueueProcessingService queueProcessingService;

	
	
	/**
	 * This method creates span in at_span_info table.
	 * It updates corresponding application status for queued and closed spans.
	 * 
	 * @param application
	 * @param atQueueInfoRequestDTO
	 * @param aTQueueInfoResponseDTO
	 * @param referralResponseMap
	 * @return
	 */
	private String updateSpanAndApplicationStatus(SsapApplication application,ATSpanInfoRequestDTO atQueueInfoRequestDTO,
			ATQueueInfoResponseDTO  aTQueueInfoResponseDTO,Map<String,Object> referralResponseMap) {
		
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("SSAP APPLICATION ID = {} , AT SPAN INFO ID = {} ",application.getId(),atQueueInfoRequestDTO.getAtSpanInfoId());
		}
		// Save Span Info
		
		AccountTransferSpanInfo atSpanInfo = null;
		if(atQueueInfoRequestDTO.getAtSpanInfoId() > 0){
			atSpanInfo = atSpanProcessingRepository.findById(atQueueInfoRequestDTO.getAtSpanInfoId());
			if(atSpanInfo == null){
				LOGGER.error("AT SPAN RECORD NOT FOUND FOR AT SPAN ID {}",atQueueInfoRequestDTO.getAtSpanInfoId());
				atSpanInfo = new AccountTransferSpanInfo();
			}
		}
		else{
			LOGGER.error("AT SPAN ID IS NOT SET IN REQUEST.");
			atSpanInfo = new AccountTransferSpanInfo();
		}
		atSpanInfo.setCmrHouseholdId(application.getCmrHouseoldId());
		atSpanInfo.setCoverageYear(application.getCoverageYear());
		atSpanInfo.setCurrentSpan(new BigDecimal(atQueueInfoRequestDTO.getCurrentSpan()));
		atSpanInfo.setTotalSpan(new BigDecimal(atQueueInfoRequestDTO.getTotalSpans()));
		if(aTQueueInfoResponseDTO.getDueDate() != null){
			atSpanInfo.setDueDate(aTQueueInfoResponseDTO.getDueDate().toDate());	
		}
		
		atSpanInfo.setQueueStatus(this.determineQueueStatusFromAtProcessingStatus(aTQueueInfoResponseDTO));
		referralResponseMap.remove(ReferralProcessingConstants.SPAN_INFO_DTO);// Remove span DTO as it is not needed for further processing. Reduce size of map
		referralResponseMap.put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID,null);// Set KEY_APPLICATIONS_WITH_SAME_ID map to null to reduce size of map.
		
		
		
		if(atSpanInfo.getQueueStatus().equals(AccountTransferQueueStatus.QUEUED)){
			atSpanInfo.setProcessStatus(null);
			try{
				XStream xStream = GhixUtils.getXStreamStaxObject();
				atSpanInfo.setReferralResponseMap(xStream.toXML(referralResponseMap));
			}catch (Exception e) {
				LOGGER.error("EXCEPTION IN SERIALIZING REFERRAL RESPONSE MAP TO XML WHILE CREATING SPAN");
			}	
		}
		
		atSpanInfo.setSsapApplicationId(application.getId());
		atSpanInfo.setEligibilityStartDate(atQueueInfoRequestDTO.getEligibilityStartDate() != null ? atQueueInfoRequestDTO.getEligibilityStartDate().toDate() : null);
		atSpanInfo.setEligibilityEndDate(atQueueInfoRequestDTO.getEligibilityEndDate() != null ? atQueueInfoRequestDTO.getEligibilityEndDate().toDate() : null);
		atSpanInfo = atSpanProcessingRepository.save(atSpanInfo);
		

		this.setEffectiveDateFromPrevApplication(application, referralResponseMap);
		this.updateApplicationStatusBasedOnSpanStatus(application, atSpanInfo);
		
		if(atSpanInfo.getQueueStatus().equals(AccountTransferQueueStatus.PROCESSED_REAL_TIME)){
			this.closePreviousERApplications(application);
			
		}
		
		return atSpanInfo.getQueueStatus().toString();
	}


	private void setEffectiveDateFromPrevApplication(SsapApplication application,
			Map<String, Object> referralResponseMap) {
		long enrolledApplicationId = 0;
		if(referralResponseMap != null && referralResponseMap.get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID) != null){
			enrolledApplicationId = (long)referralResponseMap.get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID);	
		}
		if(enrolledApplicationId > 0){
			SsapApplication enrolledApplication = ssapApplicationRepository.findById(enrolledApplicationId);
			application.setEffectiveDate(enrolledApplication.getEffectiveDate());
		}
	}

	
	/**
	 * 
	 * Since current span has resulted into  realtime processing, mark any previous ER applications to CL status. 
	 * 6/3/2019 - Dont't close previous ER app if dental enrollment is present for it.i.e application dental status = EN.
	 * This change is made to support only dental enrollment flow.
	 * 
	 * @param application
	 */
	public void closePreviousERApplications(SsapApplication application) {
		List<SsapApplication> ERApplicationList = ssapApplicationRepository.findSsapByCmrHouseholdIdAppstatus(application.getCmrHouseoldId(),ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),application.getCoverageYear());
		if(ERApplicationList != null && ERApplicationList.size() > 0){
			for (SsapApplication eachERApplication : ERApplicationList) {

				if(eachERApplication.getId() != application.getId() && !ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(eachERApplication.getApplicationDentalStatus())){
					if(LOGGER.isInfoEnabled()){
						LOGGER.info("Set application status from ER to CL for application id - {} ",application.getId());	
					}
					eachERApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
					ssapApplicationRepository.save(eachERApplication);	
				}
				
			}	
		}
		else{
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("NO PREVIOUS ER APPLICATION FOUND FOR CMR HH ID - {}",application.getCmrHouseoldId());	
			}
		}
	}


	/**
	 * 
	 * This method will update application status based on span status.
	 * if span status = Queued, application status = Queued
	 * if span status = Closed, application status = Closed
	 * 
	 * @param application
	 * @param atSpanInfo
	 */
	private void updateApplicationStatusBasedOnSpanStatus(SsapApplication application,AccountTransferSpanInfo atSpanInfo) {
		if(atSpanInfo.getQueueStatus().equals(AccountTransferQueueStatus.QUEUED)){
			application.setApplicationStatus(ApplicationStatus.QUEUED.getApplicationStatusCode());
		}
		else if(atSpanInfo.getQueueStatus().equals(AccountTransferQueueStatus.CLOSED)){
			application.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
		}	
		ssapApplicationRepository.save(application);
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("ssap application id {} marked as {} ",application.getId(),application.getApplicationStatus());	
		}
	}

	
	/**
	 * 
	 * If received current span == 1, dequeue all the spans for that HH.
	 * First find spans for HH, if found DQ all spans and corresponding applications.
	 * If not found, no action taken.
	 * 
	 * @param cmrHouseholdId
	 * @param coverageYear
	 */
	private void dequeueAllQueuedSpansForHH(BigDecimal cmrHouseholdId,long coverageYear) {
		List<AccountTransferSpanInfo> atSpanList = atSpanProcessingRepository.findByCmrHouseholdIdAndCoverageYearAndStatus(cmrHouseholdId, coverageYear, AccountTransferQueueStatus.QUEUED);
		if(atSpanList != null && atSpanList.size()>0){
			for (AccountTransferSpanInfo accountTransferSpanInfo : atSpanList) {
				accountTransferSpanInfo.setQueueStatus(AccountTransferQueueStatus.DE_QUEUED);
				
				// Dequeue Span
				atSpanProcessingRepository.save(accountTransferSpanInfo);
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Dequeued Span with Id = {} , HH Id = {} , ssap application id = {} ",accountTransferSpanInfo.getId(),cmrHouseholdId,accountTransferSpanInfo.getSsapApplicationId());	
				}
				// Dequeue corresponding application
				SsapApplication ssapApplication = ssapApplicationRepository.findById(accountTransferSpanInfo.getSsapApplicationId());
				ssapApplication.setApplicationStatus(ApplicationStatus.DEQUEUED.toString());
				ssapApplicationRepository.save(ssapApplication);
			}
		}
		else{
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("No Queued spans found for HH id - {}",cmrHouseholdId);	
			}
		}
	}

	
	
	@Override
	public String processCurrentSpan(Map<String,Object> referralResponseMap) {
		long ssapApplicationId = (long) referralResponseMap.get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
		ATSpanInfoRequestDTO atQueueInfoRequestDTO = (ATSpanInfoRequestDTO)referralResponseMap.get(ReferralProcessingConstants.SPAN_INFO_DTO);
		// TODO Auto-generated method stub
		if(atQueueInfoRequestDTO != null){
			final SsapApplication currentSsapApplication = ssapApplicationRepository.findById(ssapApplicationId);
			
			if(atQueueInfoRequestDTO.getCurrentSpan() == FIRST_SPAN){
				this.dequeueAllQueuedSpansForHH(currentSsapApplication.getCmrHouseoldId(),currentSsapApplication.getCoverageYear());
			}
			atQueueInfoRequestDTO.setCoverageYear(currentSsapApplication.getCoverageYear());
			atQueueInfoRequestDTO.setCmrHouseoldId(currentSsapApplication.getCmrHouseoldId().intValue());
			
			ATQueueInfoResponseDTO  aTQueueInfoResponseDTO = queueProcessingService.determineATProcessingType(atQueueInfoRequestDTO);
			return this.updateSpanAndApplicationStatus(currentSsapApplication,atQueueInfoRequestDTO,aTQueueInfoResponseDTO,referralResponseMap);	
		}
		else{
			// throw exception
			return null;
		}
		
		
	}
	
	/**
	 * Find span status from queuing algorithm response. 
	 * @param aTQueueInfoResponseDTO
	 * @return
	 */
	private AccountTransferQueueStatus determineQueueStatusFromAtProcessingStatus(ATQueueInfoResponseDTO aTQueueInfoResponseDTO){
		AccountTransferQueueStatus queueStatus=null;
		switch (aTQueueInfoResponseDTO.getAtProcessingType()) {
		case MESProcessingConstants.AT_PROCESSING_TYPE_QUEUE:
			queueStatus = AccountTransferQueueStatus.QUEUED;
			break;
		case MESProcessingConstants.AT_PROCESSING_TYPE_CLOSED:
			queueStatus = AccountTransferQueueStatus.CLOSED;
			break;
		case MESProcessingConstants.AT_PROCESSING_TYPE_REALTIME:
			queueStatus = AccountTransferQueueStatus.PROCESSED_REAL_TIME;
			break;
		default:
			break;
		}
		
		return queueStatus;
	}


	@Override
	public void updateSpanStatus(String status,String errorMessage, long ssapApplicationId) {
		if(ssapApplicationId > 0 && StringUtils.isNotEmpty(status)){
			AccountTransferSpanInfo accountTransferSpanInfo = atSpanProcessingRepository.findBySsapApplicationId(ssapApplicationId);
			if(accountTransferSpanInfo != null){
				if(status.equalsIgnoreCase(ReferralProcessingConstants.BATCH_SUCCESS)){
					accountTransferSpanInfo.setProcessStatus(AccountTransferProcessStatus.SUCCESS);
					accountTransferSpanInfo.setQueueStatus(AccountTransferQueueStatus.PROCESSED_BATCH);
				}
				else{
					accountTransferSpanInfo.setProcessStatus(AccountTransferProcessStatus.FAILED);
					errorMessage = StringUtils.trimToEmpty(errorMessage);
					if(errorMessage.length() > MAX_ERROR_MESSAGE_SIZE){
						errorMessage = errorMessage.substring(0, MAX_ERROR_MESSAGE_SIZE);
					}
					accountTransferSpanInfo.setFailureMessage(errorMessage);
				}
				accountTransferSpanInfo.setProcessedTimestamp(new TSTimestamp());
				atSpanProcessingRepository.save(accountTransferSpanInfo);	
			}
			else{
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Span not found for ssap application id - {}.",ssapApplicationId);	
				}	
			}
		}
		else{
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Received invalid parameters. ssap_application_id = {} , status = {}",ssapApplicationId,status);	
			}
		}
	}
	
	@Override
	public AccountTransferSpanInfo updateCurrentAndFetchNextSpan(ATSpanInfoRequestDTO atSpanInfo) {
		if(atSpanInfo.getSsapApplicationId() > 0 && atSpanInfo.getProcessStatus() != null){
			AccountTransferSpanInfo currentATSpanInfo = this.updateCurrentSpanStatus(atSpanInfo);
			return findNextSpanToBeProcessed(currentATSpanInfo);
		}
		else{
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Received invalid parameters. ssap_application_id = {} , status = {}",atSpanInfo.getSsapApplicationId(),atSpanInfo.getProcessStatus());	
			}
		}
		return null;
	}


	private AccountTransferSpanInfo findNextSpanToBeProcessed(AccountTransferSpanInfo currentATSpanInfo) {
		AccountTransferSpanInfo nextAtSpanForProcessing = null;

		if(currentATSpanInfo == null) {
			return nextAtSpanForProcessing;
		}

		List<AccountTransferSpanInfo> atSpanInfoList = atSpanProcessingRepository.findSpansByExtMemberIdAndProcessStatus(currentATSpanInfo.getExternalApplicantId(), currentATSpanInfo.getCoverageYear(), AccountTransferProcessStatus.ATRECEIVED);
		if(atSpanInfoList != null && atSpanInfoList.size()>0){
			nextAtSpanForProcessing = atSpanInfoList.get(atSpanInfoList.size() - 1);
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("FOUND NEXT SPAN FOR PROCESSING. SPAN ID = {} , SSAP_APPLICATION_ID = {}" , nextAtSpanForProcessing.getId(),nextAtSpanForProcessing.getSsapApplicationId());	
			}
		}
		return nextAtSpanForProcessing;
	}


	private AccountTransferSpanInfo updateCurrentSpanStatus(ATSpanInfoRequestDTO atSpanInfo) {
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("UPDATE SPAN STATUS FOR SSAP APPLICATION ID - {} TO PROCESS STATUS = {}.",atSpanInfo.getSsapApplicationId(),atSpanInfo.getProcessStatus());	
		}
		AccountTransferSpanInfo currentATSpanInfo = atSpanProcessingRepository.findBySsapApplicationId(atSpanInfo.getSsapApplicationId());
		if(currentATSpanInfo != null){
			currentATSpanInfo.setProcessStatus(atSpanInfo.getProcessStatus());
			if(atSpanInfo.getQueueStatus() != null){
				currentATSpanInfo.setQueueStatus(atSpanInfo.getQueueStatus());
			}
			
			String errorMessage = StringUtils.trimToEmpty(atSpanInfo.getErrorMessage());
			if(errorMessage.length() > MAX_ERROR_MESSAGE_SIZE){
				errorMessage = errorMessage.substring(0, MAX_ERROR_MESSAGE_SIZE);
			}
			currentATSpanInfo.setFailureMessage(errorMessage);
			
			currentATSpanInfo.setProcessedTimestamp(new TSTimestamp());
			atSpanProcessingRepository.save(currentATSpanInfo);	
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("UPDATED SPAN STATUS OF SSAP APPLICATION ID - {} TO PROCESS STATUS = {}.",atSpanInfo.getSsapApplicationId(),atSpanInfo.getProcessStatus());	
			}
		}
		else{
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Span not found for ssap application id - {}.",atSpanInfo.getSsapApplicationId());	
			}	
		}
		return currentATSpanInfo;
	}

	@Override
	public long createAtSpanInfo(ATSpanInfoRequestDTO atSpanInfoRequestDTO) {
		AccountTransferSpanInfo atSpanInfo = new AccountTransferSpanInfo();
		atSpanInfo.setCurrentSpan(new BigDecimal(atSpanInfoRequestDTO.getCurrentSpan()));
		atSpanInfo.setTotalSpan(new BigDecimal(atSpanInfoRequestDTO.getTotalSpans()));
		atSpanInfo.setGiWsPayloadId(atSpanInfoRequestDTO.getGiWsPayloadId());
		atSpanInfo.setSsapApplicationId(null);
		atSpanInfo.setCoverageYear(atSpanInfoRequestDTO.getCoverageYear());
		atSpanInfo.setProcessStatus(atSpanInfoRequestDTO.getProcessStatus());
		atSpanInfo.setExternalApplicantId(atSpanInfoRequestDTO.getExternalApplicantId());
		atSpanInfo = atSpanProcessingRepository.save(atSpanInfo);
		return atSpanInfo.getId();
	}

}
