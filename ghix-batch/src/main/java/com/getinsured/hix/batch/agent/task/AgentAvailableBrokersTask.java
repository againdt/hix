package com.getinsured.hix.batch.agent.task;

import com.getinsured.hix.batch.agent.service.AgentBatchService;
import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.sql.Timestamp;

public class AgentAvailableBrokersTask extends StepExecutionListenerSupport implements Tasklet {
    private static final Logger LOGGER = Logger.getLogger(AgentDailySummaryNoticesTask.class);
    private static volatile Boolean isBatchRunning = false;
    private AgentBatchService agentBatchService;

    public AgentBatchService getAgentBatchService() {
        return agentBatchService;
    }

    public void setAgentBatchService(AgentBatchService agentBatchService) {
        this.agentBatchService = agentBatchService;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        synchronized (isBatchRunning) {
            if(isBatchRunning) {
                return RepeatStatus.FINISHED;
            } else {
                isBatchRunning = true;
            }
        }

        try {
            LOGGER.debug(this.getClass().getName() + " started at " + new Timestamp(System.currentTimeMillis()));
            agentBatchService.setAgentBrokerAvailability();
        } catch(Exception ex) {
            LOGGER.error("execute::error while executing", ex);
        } finally{
            isBatchRunning = false;
        }

        LOGGER.debug(this.getClass().getName() + " finishing at " + new Timestamp(System.currentTimeMillis()));
        return RepeatStatus.FINISHED;
    }
}
