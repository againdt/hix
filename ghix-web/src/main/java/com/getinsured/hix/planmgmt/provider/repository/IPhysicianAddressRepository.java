package com.getinsured.hix.planmgmt.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PhysicianAddress;

public interface IPhysicianAddressRepository extends JpaRepository<PhysicianAddress, Integer> {

	@Query("SELECT p FROM PhysicianAddress p where LOWER(p.county) = LOWER(:county) AND LOWER(p.address1) = LOWER(:address) AND LOWER(p.physician) = LOWER(:physician)")
    PhysicianAddress getPhysicianAddress(@Param("county") String county,@Param("address") String address,@Param("physician") int physician);
}
