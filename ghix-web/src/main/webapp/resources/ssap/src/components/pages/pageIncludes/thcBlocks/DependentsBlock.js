import React, { Fragment } from 'react';

// Components

import UniversalInput from '../../../../lib/UniversalInput';

// Constants

import { THC_TYPES } from '../../../../constants/masksConstants';
import INPUT_TYPES from '../../../../constants/inputTypes';

const DependentsBlock = props => {
    const {
        analyzeFamily,
        family,
        onFieldChange,
        ...restProps
    } = props;

    const getDependents = () => {

        return family.combinedFilers
            .filter(filer => filer.isFilingTaxes)
            .map(filer => {

                const {
                    filerPersonId,
                    filerFullName
                } = filer;

                const checkDependent = dependent => {
                    const {
                        dependentPersonId,
                        spouse
                    } = dependent;

                    if(spouse && spouse[0] === filerPersonId) {
                        return false;
                    }

                    if(Array.isArray(filerPersonId)) {
                        return filerPersonId.every(id => id !== dependentPersonId);
                    } else {
                        return dependentPersonId !== filerPersonId;
                    }
                };

                return (
                    <Fragment key={filerPersonId}>
                        <p>
                            Dependents of &nbsp;
                            <strong>
                                { filerFullName }
                            </strong>
                            :
                        </p>
                        {
                            family.dependents
                                .filter(dependent => checkDependent(dependent))
                                .map(dependent => {

                                    const {
                                        claimerIds,
                                        dependentIndex,
                                        dependentPersonId,
                                        dependentFullName,
                                        isDependent,
                                        isAbleToBeDependent
                                    } = dependent;

                                    const isChecked = () => {
                                        if(Array.isArray(filerPersonId)) {
                                            return filerPersonId.some(id => claimerIds.find(claimerId => claimerId === id));
                                        } else {
                                            return claimerIds.some(claimerId => claimerId === filerPersonId);
                                        }
                                    };

                                    const isFilerClaimed = (() => Array.isArray(filerPersonId) ? false : analyzeFamily.getIndicatorsValues(filerPersonId).taxRelationship === THC_TYPES.FILER_DEPENDENT)();

                                    const isDisabled = () => {
                                        if(isFilerClaimed) {
                                            return true;
                                        }

                                        if(Array.isArray(filerPersonId)) {
                                            return !isAbleToBeDependent && !filerPersonId.some(id => claimerIds.find(claimerId => claimerId === id));
                                        } else {
                                            return !isAbleToBeDependent && !claimerIds.some(claimerId => claimerId === filerPersonId);
                                        }
                                    };

                                    return (
                                        <UniversalInput
                                            key={dependentPersonId}
                                            label={{
                                                text: dependentFullName
                                            }}
                                            inputData={{
                                                isInputRequired: false,
                                                type: INPUT_TYPES.CHECKBOX,
                                                name: 'isDependent',
                                                classes: [''],
                                                checked: isChecked(),
                                                value: isDependent,
                                                currentData: { },
                                                disabled: isDisabled(),
                                                fields: [],
                                            }}
                                            inputActions={{
                                                onFieldChange
                                            }}
                                            indexInArray={[dependentIndex, filerPersonId]}
                                            {...restProps}
                                        />
                                    );
                                })
                        }
                    </Fragment>
                );
            }).filter(f => f);
    };

    const preparedDependents = getDependents();

    const noAnyDependent = preparedDependents.every(d => d.props.children[1].length === 0);

    const isSomethingForRender = family.pairs.every(pair => pair.isAnyOfSpousesFiler ? pair.isPairMadeChoice : true) && family.filersQty > 0 && family.possibleDependents > 0 && !noAnyDependent;

    return  isSomethingForRender ?

        <div className="subsection">
            <p>
                Who are the dependents that will be claimed by  the tax filer(s) on his/her/their income tax return?
            </p>
            {
                preparedDependents
            }
        </div> : null;
};

export default DependentsBlock;