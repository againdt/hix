package com.getinsured.hix.batch.agency.service;

public interface AgencyMigrationService {

	public void process(String sourceFile,String delimeter,int agencyColumnIndex,int agencyRowIndex) throws Exception;
}
