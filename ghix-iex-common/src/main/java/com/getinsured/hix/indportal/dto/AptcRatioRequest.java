package com.getinsured.hix.indportal.dto;

import java.math.BigDecimal;
import java.util.List;

public class AptcRatioRequest {
	private Integer coverageYear;
	private BigDecimal maxAptc;
	private List<Member> memberList;
	
	public Integer getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}
	public BigDecimal getMaxAptc() {
		return maxAptc;
	}
	public void setMaxAptc(BigDecimal maxAptc) {
		this.maxAptc = maxAptc;
	}
	public List<Member> getMemberList() {
		return memberList;
	}
	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}

	public class Member{
		private String id;
		private Integer age;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public Integer getAge() {
			return age;
		}
		public void setAge(Integer age) {
			this.age = age;
		}
	}
}
