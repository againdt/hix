<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ui-lightness/jquery-ui-1.8.22.custom.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui-1.9.2.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/shopstyles.css"/>" />
<style>
.line {
border-right: 1px solid #e3e3e3 !important;
}
#contribution_error label{margin: -12px 0 30px 142px;}

.display{display: block !important;}
</style>
<script type="text/javascript">
function validateForm(){
	$("#frmcontribution").submit();
}

function clearNavigation(){
	$('li.navmenu').each(function(index, element) {
		var index = index+1;
		if(index > 5){
			var link = $(this).find($('a')).attr('href');
			var text = $(this).html().replace('<i class="icon-ok"></i>', '<span>' + index +'. </span>');
			var finalHtml = text.replace(link, '#');
	        $(this).html(finalHtml);
		}
	});
}

function populateEmployeeFinalData(){
	if( $("#frmcontribution").validate().form() ) {
		var validateUrl = '<c:url value="/shop/employer/plandisplay/getEmployerFinalContributionData">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
	       $.ajax({
			url: validateUrl, 
			type: "POST", 
			dataType:'json',
			data: {coverageEmployee: $("#employeeContribution").val(),coverageDependent: $("#dependentContribution").val()}, 
			success: function(response,xhr)
			{
		        if(isInvalidCSRFToken(xhr))
		          return;
				 clearNavigation();
				 var obj = response;
				 var empContString = obj['employerBenchmarkIndvPremium'] +' / <spring:message code = "label.month"/></td>';
				 var deptContString = obj['employeeBenchmarkDeptPremium'] +' / <spring:message code = "label.month"/></td>';
				 var totalString =  '<strong>'+ obj['totalPremium'] +' / <spring:message code = "label.month"/></strong></td>';
						                
				 $("#employeesCont").html(empContString);
				 $("#dependentCont").html(deptContString);
				 $("#totalValue").html(totalString);
				 $("#taxtitle").html('<strong><spring:message code = "label.estTaxCredit"/> <a href="javascript:void(0)" rel="tooltip" data-placement="top" data-original-title="<spring:message code = "label.taxAdvice"/>" class="info"><i class="icon-question-sign"></i></a></strong>');
				 $("#taxCredit").html('<strong>'+ obj['taxCredit'] + ' / <spring:message code = "label.month"/></strong>');	 
				 $("#employeeCost").html('<strong>' + obj['employeeCost'] + ' / <spring:message code = "label.month"/></strong>');
				 $('.info').tooltip();
			},
       });
    }
}
</script>
<div class="gutter10">
<div class="row-fluid">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<util:nav pageName="${page_name}"></util:nav>
			</div>
			<util:selection pageName="${page_name}"></util:selection>
		</div>
		<!--sidebar-->
		<div class="span9" id="rightpanel">
				<h4 class="graydrkbg gutter10">
			 		<a href="<c:url value="/shop/employer/plandisplay/benchmarkplan"/>" class="btn"> <spring:message code = "label.back"/> </a>
            		<span class="offset1"><spring:message code = "label.finalizeCosts"/></span>
<%--             		<input type="button" name="submitButton" id="submitButton" onClick="javascript:validateForm();" class="btn btn-primary btn-small pull-right" value="<spring:message code = "label.next"/>" /> --%>
					<a href="#" class="btn btn-primary btn-small pull-right" id="submitButton" onClick="javascript:validateForm();"> <spring:message  code="label.next"/> </a>
				</h4>				
			<!--titlebar-->
			<form id="frmcontribution" name="frmcontribution" action="finalcontribution" method="post">
				<df:csrfToken/>
				<input type="hidden" id="employeeContribution" name="employeeContribution" />
				<input type="hidden" id="dependentContribution" name="dependentContribution" />
				<div class="gutter10">
					<p><spring:message code = "label.adjustContri"/></p>
					<div class="well">
						<div style="margin: 15px 0;">
			              <p class="pull-left paddingT5" style="width: 105px"><strong><spring:message code = "label.employees"/></strong> (${totalEmp})</p>
			              <div class="pull-left paddingT5 gutter10-lr">0%</div>
			              <div id="employeeSlider" class="slider" style="width:70%;float:left; height:30px"></div>
			              <div class="pull-left paddingT5 marginL10">100%</div>
			              <div class="clearfix"></div>
		                </div>
		                <div id="contribution_error"></div>
		                <div style="margin:30px 0; clear:both;">
			              <p class="pull-left paddingT5" style="width: 105px"><strong><spring:message code="label.dependent"/></strong> (${totalDependents})</p>
			              <div class="pull-left paddingT5 gutter10-lr">0%</div>
			              <div id="dependentSlider" class="slider" style="width:70%;float:left; height:30px"></div>
			              <div class="pull-left paddingT5 marginL10">100%</div>
			              <div class="clearfix"></div>
		                </div>
						
						<table class="table table-border-none table-condensed">
							<tr>
								<td style="width: 37%"><strong><spring:message code = "label.emplCostsCredits"/></strong> <a href="javascript:void(0)"
									rel="tooltip" data-placement="bottom"
									data-original-title="<spring:message code = "label.costCaldesc3"/> " class="info">
									<i class="icon-question-sign"></i></a>
								</td>
								<td class="line">&nbsp;</td>
								<td>&nbsp;</td>
								<td><strong><spring:message code = "label.empCosts"/></strong>
									<a href="javascript:void(0)" rel="tooltip" data-placement="top" data-original-title="<spring:message code = "label.empOnlyCoverage"/>" class="info">
									<i class="icon-question-sign"></i></a>
								</td>
							</tr>
							<tr>
								<td><spring:message code="label.contriEmpPart"/></td>
								<td  style="width: 24%;" id="employeesCont"  class="line">${employerBenchmarkIndvPremium} / <spring:message code = "label.month"/></td>
								<td>&nbsp;</td>
								<td id="employeeCost"><strong>${employeeCost} /<spring:message code = "label.month"/></strong></td>
							</tr>
							<tr>
								<td><spring:message code = "label.contriForDependents"/></td>
								<td  class="line" id="dependentCont">${employeeBenchmarkDeptPremium} /<spring:message code = "label.month"/></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><strong><spring:message code = "label.total"/></strong></td>
								<td  id="totalValue"><strong>${totalPremium} /<spring:message code = "label.month"/></strong></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td></td>
							</tr>
							<tr>
							    <td id="taxtitle"><strong><spring:message code = "label.estTaxCredit"/> <a href="javascript:void(0)" rel="tooltip" data-placement="top" data-original-title="<spring:message code= "label.taxAdvice"/>" class="info"><i class="icon-question-sign"></i></a></strong></td>
								<td id="taxCredit"><strong>${taxCredit} /<spring:message code = "label.month"/></strong></td>
							</tr>
						</table>
					</div>
				</div><!--gutter10-->
			</form>
		</div> <!--rightpanel-->
	</div> <!--row-fluid-->
</div> <!--main-->
</div>


<script type="text/javascript">
$('.info').tooltip();
$(document).ready(function(){
	empMinContribution = ${empMinContribution};
	stateName = '${stateName}';
	var employeeTooltip = function(event, ui) {
		if(ui.value == undefined){ 
			ui.value = ${employerIndvContribution};
		}
		if(ui.value < empMinContribution){
        	ui.value = ${employerIndvContribution};
        	//$('#contribution_error').html("<label class='error' generated='true'><span><em class='excl'>!</em>Employer contribution should be more than 50%.</span></label>");
            return false;
	    }
		$('#employee').remove();
		$(this).find(".ui-slider-range-min").html("<span id='employer'><spring:message code = "label.emplPays"/></span>");
		$(this).find("a.ui-slider-handle").html("<span style='float: left; display: block; margin-top: -20px; margin-left: -8px; color:#555; font-size:0.7em;  width:35px;'>"+ui.value + "% </span>");
		$(this).find("a.ui-slider-handle").after("<span id='employee'><spring:message code = "label.employeePays"/></span>");
		if(ui.value == 100){
			//$(this).find("a.ui-slider-handle").find('span').remove();
			$('#employee').remove();
		}else if(ui.value < 1){
			$(this).find(".ui-slider-range-min").find('span').remove();	
		}
		$( "#sliderdiv" ).html( ui.value + "%" );
        $( "#employeeContribution").val(ui.value);
	};
	
	var dependentTooltip = function(event, ui) {
		
		if(ui.value == undefined){ 
			ui.value = ${employerDepeContribution};
		}
		$('#dependent').remove();
		$(this).find(".ui-slider-range-min").html("<span id='employer'><spring:message code = "label.emplPays"/></span>");
		$(this).find("a.ui-slider-handle").html("<span style='float: left; display: block; margin-top: -20px; margin-left: -8px; color:#555; font-size:0.7em; width:35px;'>"+ui.value + "% </span>");
		$(this).find("a.ui-slider-handle").after("<span id='dependent'><spring:message code = "label.employeePays"/></span>");
		if(ui.value == 100){
			//$(this).find("a.ui-slider-handle").find('span').remove();
			$('#dependent').remove();
		}else if(ui.value < 1){
			$(this).find(".ui-slider-range-min").find('span').remove();	
		}
		$( "#sliderdiv" ).html( ui.value + "%" );
        $( "#dependentContribution").val(ui.value);
	};
	
	var stopSlider = function(event, ui){
		if(ui.value <= empMinContribution){
        	ui.value = ${employerIndvContribution};
        	$('#contribution_error').html("<label class='error' generated='true'><span><em class='excl'>!</em>In "+stateName+ ", <spring:message code = 'label.mincontribution'/> "+empMinContribution+"% .</span></label>");
        	$('#contribution_error').find('label.error').addClass('display').fadeOut(3000, "linear", complete);
        }
		else{
			$('#contribution_error label').html("").hide();
		}
		
		function complete() {
			$('#contribution_error label').removeClass('display')
		  }
	};
	
	$( "#employeeSlider" ).slider({ orientation: "horizontal",
		range: "min",
		min: 0,
		max: 100,
		step: 5,
		value: ${employerIndvContribution},
	    create: employeeTooltip,
		slide:  employeeTooltip,
		change: populateEmployeeFinalData,
		stop: stopSlider
	});
	$( "#dependentSlider" ).slider({ orientation: "horizontal",
		range: "min",
		min: 0,
		max: 100,
		step: 5,
		value: ${employerDepeContribution},
	    create: dependentTooltip,
		slide:  dependentTooltip,
		change: populateEmployeeFinalData
	});
});

</script>
