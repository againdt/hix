package com.getinsured.iex.hub.ssac;


import java.util.Iterator;

import javax.xml.bind.JAXBElement;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ssap.iex.HouseholdMember;
import com.getinsured.iex.hub.platform.ssap.iex.SSAP;
import com.getinsured.iex.hub.platform.ssap.iex.SSAPTaxHouseHold;
import com.getinsured.iex.hub.ssac.exchange.ObjectFactory;
import com.getinsured.iex.hub.ssac.extension.SSACompositeResponseType;
import com.getinsured.iex.hub.ssac.wrapper.PersonWrapper;
import com.getinsured.iex.hub.ssac.wrapper.SSACompositeIndividualRequestWrapper;
import com.getinsured.iex.hub.ssac.wrapper.SSACompositeRequestWrapper;
import com.getinsured.iex.hub.ssac.wrapper.SSACompositeResponseWrapper;

public class SSACServiceHandler extends AbstractServiceHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SSACServiceHandler.class);
	
	private static ObjectFactory factory;
	private Jaxb2Marshaller marshaller;
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
	}
	
	@Override
	public Object getRequestpayLoad() throws HubServiceException {
		try {
			return getPayLoadFromJSONInput();
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while creating payload",e);
			throw new HubServiceException("Failed to generate the payload:",e);
		}
		
	}
	
	public Object getPayLoadFromJSONInput()
			throws HubServiceException, HubMappingException {
		
		SSAP ssap = null;
		try{
			ssap = new SSAP(getJsonInput());
		}catch(ParseException ex){
			listJSONKeys();
			throw new HubMappingException("Failed to process input data ["+ex.getMessage()+"]", ex);
		}
		
		HouseholdMember houseHoldMember = null;
		SSAPTaxHouseHold taxHousehold = null;
		
		Iterator<SSAPTaxHouseHold> householdCursor = ssap.houseHoldIterator();
		
		SSACompositeRequestWrapper payLoad = new SSACompositeRequestWrapper();
		
		if(householdCursor == null){
			throw new HubServiceException("No household information provided");
		}
		
		while(householdCursor.hasNext()){
			taxHousehold = householdCursor.next();
			Iterator<HouseholdMember> memberCursor = taxHousehold.iterator();
			
			if(memberCursor == null){
				throw new HubServiceException("No member information provided");
			}
			
			while(memberCursor.hasNext()){
				houseHoldMember = memberCursor.next();
				PersonWrapper person = new PersonWrapper();
				this.extractSSAPData(houseHoldMember.getSsnInfo(), person);
				this.extractSSAPData(houseHoldMember, person);
				SSACompositeIndividualRequestWrapper applicant = new SSACompositeIndividualRequestWrapper();
				this.extractSSAPData(houseHoldMember.getCitizenshipInfo(), applicant);
				this.extractSSAPData(houseHoldMember.getIncarcerationInfo(), applicant);
				applicant.setPerson(person);
				this.extractSSAPData(houseHoldMember, applicant);
				
				/**
				 * Hardcoding to be removed once we get these fields from
				 * SSAP
				 */
				applicant.setTitleIIAnnualIncomeIndicator(false);
				//applicant.setRequestTitleIIAnnualIncomeDate("2012");
				applicant.setTitleIIMonthlyIncomeIndicator(false);
				//applicant.setRequestTitleIIMonthlyIncomeDate("201212");
				applicant.setQtrCoverageIndicator(false);
				
				payLoad.addIndividualRequest(applicant);
			}
		}
		return factory.createSSACompositeRequest(payLoad.getSystemRepresentation().getValue());
	}
	
	/*@SuppressWarnings("unchecked")
	public Object getPayLoadFromJSONInput(String jsonSectionKey, String jsonStr) throws HubServiceException, HubMappingException{
		
		logger.info("received JSON String: "+jsonStr);
		JAXBElement<SSACompositeRequestType> payloadElement = null;
		HubServiceException hse;
		
		
		Object obj = null;
		JSONArray requests = null;
		try {
			GIJson mainJson = GIJson.getGIJson(jsonStr);
			obj = mainJson.getValueForKey("singleStreamlinedApplication#taxHousehold", jsonSectionKey);
			if(obj == null){
				throw new HubServiceException("failed to find the house hold information from input json");
			}
			requests = (JSONArray)obj;
		} catch (ParseException e) {
			logger.error("Invalid JSON:"+jsonStr);
			hse = new HubServiceException("Failed to parse JSON input", e);
			throw hse;
		}
		
		
		try{
			
			SSACompositeRequestWrapper payLoad = new SSACompositeRequestWrapper();
			SSACompositeIndividualRequestWrapper individualRequest = null;
			
			if(requests == null || requests.size() == 0){
				throw new HubServiceException("Mimimum one request information expected for SSAC Service");
			}
			
			JSONObject requestData = null;
			Set<Map.Entry<String, Object>> individualRequestData = null;
			PersonWrapper person;
			String birthDate = null;
			JSONObject ssnObj;
			Parameter target = null;
			for(int i = 0; i < requests.size(); i ++){
				requestData = (JSONObject)requests.get(i);
				individualRequestData = ((JSONObject)requestData.get("name")).entrySet();
				if(individualRequestData.size() == 0){
					logger.error("Invalid JSON: "+jsonStr);
					throw new HubServiceException("Invalid request data input, no applicant attributes found");
				}
				person = handleNameKey(individualRequestData);
				birthDate = (String) requestData.get("dateOfBirth");
				target = this.getTargetParameter("dateOfBirth", PersonWrapper.class.getName());
				if(target != null){
					target.setValue(person, birthDate);
				}
				else{
					throw new HubServiceException("Date of birth is required for every applicant");
				}
				ssnObj = (JSONObject) requestData.get("socialSecurityCard");
				if(ssnObj != null){
					String ssnVerificationFlag = (String) ssnObj.get("socialSecurityNumber");
					target = this.getTargetParameter("socialSecurityNumber", PersonWrapper.class.getName());
					if(target != null){
						target.setValue(person, ssnVerificationFlag);
					}
					else{
						throw new HubServiceException("SSN is required for every applicant");
					}
				}
				else{
					throw new HubServiceException("SSN is required for every applicant");
				}
				individualRequest = getIndividualVerificationFlags(requestData);
				individualRequest.setPerson(person);
				payLoad.addIndividualRequest(individualRequest);

			}
			
			payloadElement = factory.createSSACompositeRequest(payLoad.getSystemRepresentation().getValue());
			return payloadElement;
		}
		catch(Exception e){
			logger.error("Failed to get Payload for SSAC service",e);
			throw new HubServiceException("Failed to generate the payload:",e);
		}
		
	}
	
	private SSACompositeIndividualRequestWrapper getIndividualVerificationFlags(
			JSONObject requestData) throws HubMappingException {
		String namespace =SSACompositeIndividualRequestWrapper.class.getName(); 
		SSACompositeIndividualRequestWrapper individualRequest = new SSACompositeIndividualRequestWrapper();
		JSONObject obj = (JSONObject) requestData.get("citizenshipImmigrationStatus");
		Boolean citizenShipFlag = (Boolean) obj.get("citizenshipAsAttestedIndicator");
		Parameter target = this.getTargetParameter("citizenshipAsAttestedIndicator",namespace);
		if(target != null){
			target.setValue(individualRequest, citizenShipFlag);
		}
		obj = null;
		target = null;
		String ssnVerificationFlag = null;
		obj = (JSONObject) requestData.get("socialSecurityCard");
		if(obj != null){
			ssnVerificationFlag = (String) obj.get("socialSecurityNumber");
			target = this.getTargetParameter("socialSecurityNumber", namespace);
			if(target != null){
				target.setValue(individualRequest, ssnVerificationFlag);
			}
		}
		obj = null;
		target  = null;
		obj = (JSONObject) requestData.get("incarcerationStatus");
		if(obj != null){
			Boolean incarcerationStatus = (Boolean) obj.get("incarcerationAsAttestedIndicator");
			target = this.getTargetParameter("incarcerationAsAttestedIndicator", namespace);
			if(target != null){
				target.setValue(individualRequest, incarcerationStatus);
			}
		}
		// Not available in input JSON we receive from SSAP, set to default true for now
		individualRequest.setTitleIIAnnualIncomeIndicator(true);
		if(individualRequest.isTitleIIAnnualIncomeIndicator()){
			target = this.getTargetParameter("requestTitleIIAnnualIncomeDate", namespace);
			if(target != null){
				target.setValue(individualRequest, "2012");
			}
		}
		individualRequest.setTitleIIMonthlyIncomeIndicator(true);
		if(individualRequest.isTitleIIMonthlyIncomeIndicator()){
			target = this.getTargetParameter("requestTitleIIMonthlyIncomeDate", namespace);
			if(target != null){
				target.setValue(individualRequest, "201212");
			}
		}
		individualRequest.setQtrCoverageIndicator(true);
		return individualRequest;
		
	}

	private PersonWrapper handleNameKey(Set<Entry<String, Object>> individualRequestData) throws HubMappingException{
		String namespace = PersonWrapper.class.getName();
		PersonWrapper person = new PersonWrapper();
		Iterator<Map.Entry<String, Object>> cursor = individualRequestData.iterator();
		Map.Entry<String, Object> me = null;
		String paramName = null;
		Parameter applicantParameter = null;
		while(cursor.hasNext()){
			me = cursor.next();
			paramName = me.getKey();
			applicantParameter = this.getTargetParameter(paramName, namespace);
			if(applicantParameter == null){
				logger.warn("Failed to find parameter mapping for input:"+paramName+" for namespace:"+namespace);
				continue;
			}
			//This will insure the validation as per service XML declaration
			applicantParameter.setValue(person, me.getValue());
		}
		return person;
		
	}*/
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public String handleResponse(Object responseObject) 
			throws HubServiceException {
		String message = "";
		try {
			JAXBElement<SSACompositeResponseType> test = (JAXBElement<SSACompositeResponseType>)responseObject;
			SSACompositeResponseType response = test.getValue();
			SSACompositeResponseWrapper responseWrapper = new SSACompositeResponseWrapper(response);
			this.setResponseCode(responseWrapper.getResponseCode());
			if(getContext() != null){
				responseWrapper.getContext().putAll(getContext());
			}
			this.requestStatus = RequestStatus.SUCCESS;
			message = responseWrapper.toJSONString();
		}
		catch(ClassCastException ce) {
			message = "Help! I do know not any thing about "+responseObject.getClass().getName()+" Expecting:"+SSACompositeResponseType.class.getName();
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			throw new HubServiceException(message, ce);
		}
		
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}
	

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return "H03";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.ssac.exchange",new SSACNamespacePrefixMatcher());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.ssac.exchange",new SSACNamespacePrefixMatcher());
		}
		return this.marshaller;
	}
}
