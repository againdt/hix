
	$if(navDataList)$
		    <div class="row-fluid">
				<div id="progress-bar">
                  <div id="bar">
                  	<ul>
                  	$navDataList: {navData|
					        
							<li id="one"><span id="step-one" class="steps $navData.STEP_CLASS$" alt="$navData.ALT_TEXT$" aria-label="$navData.ALT_TEXT$"><span class="step-number" style="$navData.STEP_NUMBER_STYLE$" id="no-one">$i$</span></span><span id="s$i$" class="step-details">$navData.ALT_TEXT$</span></li>
					    }$
                  	</ul>
                  </div>
              </div>   
			</div>
			
		    <div class="row-fluid">
	            <div class="gutter10">
		              <div class="span9" id="complete-oep">
			                $if(errorMsg)$
				                <p>
				                	<span class="alert-error" id="errorMsg"><small>$errorMsg$</small></span>
				                </p>
			                $else$
			                	<p id="enroll-s1">$currentNavData.STEP_DESC$</p>
			                $endif$
		              </div>
		              $if(employeeAppStatus)$
			               <div class="span3" id="make-changes">
			                	<button class="btn btn-primary makeChanges" alt="Make Changes"  onclick="">Make Changes</button> 
			               </div>
			          $else$
			          	   $if(!errorMsg)$	
			          	   		$if(currentNavData.ALT_ID)$
			               		<div class="span3" id="$currentNavData.ALT_ID$">
		                			<button class="btn btn-primary" alt="$currentNavData.ALT_TEXT$"  onClick="gotoNext('$navStep$')" >$currentNavData.ALT_TEXT$</button> 
		              			</div>
		              			$endif$
		              	   $endif$
		              $endif$
		              $if(openEnrollmentPeriod)$
			              <div class="row-fluid">
					            <div class="gutter10 pull-left">
					              <p id="w-e-c" class="text-warning"><small>You may <a href="#waive-emp-coverage" data-toggle="modal">waive employer coverage</a> if you are already covered elsewhere.</small></p>
					            </div>
			              </div>  
		              $endif$
	            </div>
            </div>
			
	$else$
		<div class="row-fluid">	
			<div id="complete-oep" class="span9">
				<p id="waive-coverage">
					You have waived your <span class="employer-name"><strong>$employerName$</strong></span>-sponsored health coverage. You can undo this selection any time prior to the end of Open Enrollment on $openEnrollmentEndDate$.<br/><br/>
	                <span class="alert-error"><small>Not having health insurance may subject you to tax penalties due to non-compliance with the individual mandate of the Affordable Care Act.</small></span>
                </p>
			</div>
			<div id="report-Changes" class="span3">
				<button class="btn btn-primary undoWaiver">Undo Waiver</button> 
			</div>				
		</div>	
	$endif$	
