import React, {Fragment} from 'react';
import normalizeStringToBooleanValue from '../utils/normalizeStringToBooleanValue';

// Utils


const RadioCheckBoxes = props => {

    const handleValueChange = e => {
        const { dataset: { value } } = e.target;
        props.inputActions.handleFieldChange(!normalizeStringToBooleanValue(value));
    };

    const {
        inputData: {
            checked,
            id,
            classes,
            isInputRequired,
            name,
            type
        },
        label: {
            text,
            labelClass
        },
        legend: {
            legendText
        }
    } = props;

    return (
        <Fragment>
            <div className={`usa-width-one-whole gi-component gi-${type}`}>
                {legendText &&
                    <legend className={`${labelClass && labelClass.join(' ')} required-${isInputRequired}`}>
                        {legendText}
                    </legend>
                }
                <input
                    id={id}
                    name={name}
                    className={`gi-${type}-input ${classes && classes.join(' ')} required-${isInputRequired}`}
                    type="radio"
                    checked={checked}
                    onChange={() => {}}
                    value={checked}
                />
                <label
                    htmlFor={id}
                    className={`gi-${type}-label`}
                    data-value={checked}
                    onClick={handleValueChange}
                >
                    {text}
                </label>
            </div>
        </Fragment>
    );
};

export default RadioCheckBoxes;
