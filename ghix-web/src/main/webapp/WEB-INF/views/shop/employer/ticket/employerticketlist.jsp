<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ page isELIgnored="false"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>
<script type="text/javascript">
var showAndHideRowNum = 0;
</script>
<div class="gutter10">
<div class="row-fluid">
    	<ul class="page-breadcrumb">
                    <li>View Tickets</li>
                </ul><!--page-breadcrumb ends-->
		<h1>Tickets <font size="4%"><small>${resultSize} Total Tickets </small></font></h1>
    </div><!--  end of row-fluid -->
<div class="span9">		
            <form  method="POST">
				<df:csrfToken/>
            <c:choose>
				<c:when test="${resultSize >= 1}">
					<c:choose>
						<c:when test="${fn:length(ticketList) > 0}">
								<table class="table table-striped">
									<thead>
										<tr class="graydrkbg ">
											<!--  span9 header -->
											<th><input type="checkbox"> </th>
											<th></th>
											<%-- <th class="sortable" scope="col"><dl:sort
													title="Task" sortBy="id"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
											<th class="sortable" scope="col"><dl:sort
													title="Subject" sortBy="subject"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Ticket Type" sortBy="tkmWorkflows"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Requester"
													sortBy="role"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Requested" sortBy="created"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Assignee" sortBy="updator"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
									<c:forEach items="${ticketList}" var="ticket" varStatus="vs">
									<tr>
											<td><input type="checkbox"> </td>
											<td> <button type="button" id ="dropdown" onclick="javascript:getTicketTaskData(${ticket.id});"><b>v</b></button></td>
											<%-- <span onClick="getTicketTaskData(${ticket.id});"><b>v</b></span> --%>
											<td>${ticket.subject}</td>											
											<td> ${ticket.tkmWorkflows.type}</td>
											<td> ${ticket.role.firstName}</td>
											<td><fmt:formatDate
													value="${ticket.created}"
													pattern="MM-dd-yyyy" /></td>
											<td> ${ticket.updator.firstName}</td>
											<td style="display:none;" id="userId"> ${USER_ID}</td>
											<td style="display:none;" id="queueId"> ${QUEUE_ID}</td>
										</tr>
										<tr style="display:none;" id="hide${ticket.id}">
										<td></td>
										<td colspan="7">
										<table  id="excelDataTable${ticket.id}">
													<!-- <thead>
													class="table table-striped"
														
														<tr class="graydrkbg ">
															<th><input type="checkbox"> </th>
															<th>TaskName</th>
															<th>CreatedBy</th>
															<th>Asignee</th>
															<th>Creation</th>
															<th>LastUpdate</th>
															<th>Status</th>
														</tr>
														
													</thead> -->
													
														<%-- <c:forEach items="${ticket.tkmTicketsTaksList}" var="ticketTask" varStatus="vs">
														<tr>
															<td><input type="checkbox"> </td>
															<td>${ticketTask.taskName}</td>											
															<td>${ticketTask.creator}</td>
															<td>${ticketTask.assignee.firstName}</td>
															<td><fmt:formatDate value="${ticketTask.created}" pattern="MM-dd-yyyy" /></td>
															<td><fmt:formatDate value="${ticketTask.updated}" pattern="MM-dd-yyyy" /></td>
															<td>${ticketTask.status}</td>
														</tr>
													</c:forEach> --%>
												</table>
												</td>
										</tr>				
									</c:forEach>
									</tbody>
								</table>
                                <div class="pagination">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
								</div>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
			</c:choose>
        </form>
		</div>
		</div>
<script type="text/javascript">
function getTicketTaskData(id)
{
	var hideId = "hide"+ id;
	//alert("displaying"+hideId);
	
	var validateUrl = '<c:url value="userTickettasklist">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';

	if(showAndHideRowNum != id)
	{
		//alert("Right"+id);
		hideRow("hide"+showAndHideRowNum);
		showAndHideRowNum = id;
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			ticketId : id
		},
			success: function(response,xhr)
			{
		        if(isInvalidCSRFToken(xhr))
		          return;
				$('#hideId').attr('style', 'display:block');
				$("#excelDataTable"+id).empty();
				buildHtmlTable(response, id);
				showAndHideRow(hideId);
   			},
  			error: function(e)
  			{
   				alert("Failed to Fetch Data");
   			}
	});
	}
	else
	{
		hideRow(hideId);
		showAndHideRowNum = 0;
	}
}

function buildHtmlTable(response, id) 
{
	var newresponse = JSON.parse(response); 
	var myList = newresponse;
	 //var myList = response;
    if(myList != '')
    {
    	//alert("MyList :"+myList);
	 var columns = addAllColumnHeaders(myList, id);
    	//alert(columns);
        for (var i = 0 ; i < myList.length ; i++) {
            var row$ = $('<tr/>');
            for (var colIndex = 0 ; colIndex < columns.length ; colIndex++) {
            	if(columns[colIndex]=="QueueId"||columns[colIndex]=="UserId"||columns[colIndex]=="TaskId"||
            			columns[colIndex]=="ActivitiTaskId"||columns[colIndex]=="ProcessInstanceId"||columns[colIndex]=="Comments"||columns[colIndex]=="FormProperties"){
            	continue;	
            	}
                var cellValue = myList[i][columns[colIndex]];
          //      alert(columns);
                if (cellValue == null) { cellValue = ""; }
    
              
                	row$.append($('<td/>').html(cellValue));	
                
                
                
                
            }
            var taskMap='';
            //var abcd = myList[i]
            var ticketid = id
            var taskMapString = '';
            var quId = $("#queueId").html();
            var parsedArray= JSON.parse(quId)
            var usrQueueId=parseInt($.trim(myList[i].QueueId));
            var usrId=parseInt($.trim($("#userId").html()))
            if((myList[i].Status=="Claimed")&&myList[i].UserId==usrId){
            	taskMap = myList[i];
            	row$.append($('<td>'));
        		row$.append($('<input/>').attr({ type: 'button', name:'btn1', value:'Complete', class: 'btn btn-primary btn-small'})
        				.unbind().bind("click",{taskMap:taskMap,ticketid:ticketid},function(event){
						completeTask(event.data.taskMap, event.data.ticketid);}
					));
			  row$.append($('</td>'));
	        }
          else {
        	  if((myList[i].Status=="UnClaimed")&&(myList[i].UserId==usrId||$.inArray(usrQueueId, parsedArray)!=-1)){
        		  taskMapString=JSON.stringify(myList[i]);
   
        		  		row$.append($('<td>'));
        			    row$.append($('<input/>').attr({ type: 'button', name:'btn1', value:'Claim', class: 'btn btn-primary btn-small'})
        			    		.unbind().bind("click",{taskMapString:taskMapString,ticketid:ticketid},function(event){
        			    			claimtask(event.data.taskMapString, event.data.ticketid);}
        				
        			  ));
        			  row$.append($('</td>'));

        	  }
        	  
          }
            
            $("#excelDataTable"+id).append(row$);
        }
    }
    else
    {
    	//alert(response);
    	var row$ = $('<tr/>');
    	var message = '<h4 class="alert alert-info">Currently there are no task available for this ticket </h4>';
    	
    	row$.append($('<td/>').html(message));
    	$("#excelDataTable"+id).append(row$);
    }
}


function claimtask(taskMapString,ticketid)
{

	var claimTaskUrl = '<c:url value="claimUserTask">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';

	var task = taskMapString;
	$.ajax(
	{
	url : claimTaskUrl,
	type : "POST",
	data : {
		taskObject : task
		   },
		success: function(response,xhr)
		{
	        if(isInvalidCSRFToken(xhr))
	          return;
			getTicketTaskData(ticketid)
			
			},
		error: function(e)
		{
				alert("Failed to Fetch Data");
			}
	});
}

function completeTask(taskMap, ticketid)
  {

	var taskId=taskMap.ActivitiTaskId
	var taskFormPropertiesUrl = '<c:url value="getTaskFormProperties">
									<c:param name="${df:csrfTokenParameter()}"> 
										<df:csrfToken plainToken="true" />
									</c:param> 
								</c:url>';
	var taskPropertiesResponse ;
	$.ajax(
			{
				url : taskFormPropertiesUrl,
				type : "POST",
				data : 
				{
					taskId : taskId
				},
				success: function(response,xhr)
				{
			        if(isInvalidCSRFToken(xhr))
			          return;
					taskPropertiesResponse = response;
					buildPopUp(taskPropertiesResponse,taskMap,ticketid);
				},
				error: function(e)
				{
					alert("Failed to Fetch Data");
				}
			});

}
function buildPopUp(taskPropertiesResponse,taskMap,ticketid){
	
	var newresponse = JSON.parse(taskPropertiesResponse);
	var id=ticketid;
	var popup = open("", "Popup", "width=300,height=200");
	popup.id='popupId';
	var ticketDocumentsUrl = '<c:url value="getTicketDocuments">
								<c:param name="${df:csrfTokenParameter()}"> 
									<df:csrfToken plainToken="true" />
								</c:param> 
							</c:url>';

	var documentResponse=null;
	$.ajax(
			{
			url : ticketDocumentsUrl,
			type : "POST",
			data : {
				ticketId : id
				   },
				success: function(response,xhr)
				{
			        if(isInvalidCSRFToken(xhr))
			          return;
					documentResponse = response;
				},
				error: function(e)
				{
					alert("Failed to Fetch Data");
				},
				async:   false
			});
	
	var newSelect=null;
	var txtField =null;
	var td;
	if(popup.document.getElementById("txtid")==null){
	for (var i = 0 ; i < newresponse.length ; i++){
		
			var propertyLabel = popup.document.createElement("label");
			propertyLabel.innerHTML=newresponse[i].name
			popup.document.body.appendChild(propertyLabel);
			if(newresponse[i].type=="enum"){
				newSelect = popup.document.createElement("SELECT");
				$(newSelect).data("selectKey",newresponse[i].id);
				newSelect.name="selectField"
					for(j=0 ; j<newresponse[i].values.length;j++)
					{
					   var opt = document.createElement("option");
					   opt.value= j;
					   opt.innerHTML = newresponse[i].values[j]; // whatever property it has
					   newSelect.appendChild(opt);
					}
				//propertyLabel.htmlFor="selectid"
				popup.document.body.appendChild(newSelect);
				td=popup.document.createElement("br");
				popup.document.body.appendChild(td);
				
			}
			else{
				txtField = popup.document.createElement("input");
				txtField.name="textField"
				$(txtField).data("txtKey",newresponse[i].id);
				popup.document.body.appendChild(txtField);
				td=popup.document.createElement("br");
				popup.document.body.appendChild(td);
			}
		}
		var txtOk = popup.document.createElement("TEXTAREA");
		var aOk = popup.document.createElement("button")
		aOk.innerHTML = "OK";
		txtOk.id='txtid'
		aOk.onclick=function(){func(popup,taskMap,ticketid);};
		td=popup.document.createElement("br");
		popup.document.body.appendChild(td);
		popup.document.body.appendChild(txtOk);
		popup.document.body.appendChild(aOk);
		if(documentResponse!=null){
			var documentPathList = JSON.parse(documentResponse);
			var selecetDocument = popup.document.createElement("label");
			td=popup.document.createElement("br");
  			popup.document.body.appendChild(td);
			selecetDocument.innerHTML="Ticket Documents: "
			popup.document.body.appendChild(selecetDocument);
			td=popup.document.createElement("br");
  			popup.document.body.appendChild(td);
			for(i=0 ; i<documentPathList.length;i++){
				
				var docPath  = documentPathList[i];
	  			
	  			$("<a style='cursor:pointer;' href='#'>"+docPath+ "</a>")
	  	    	.unbind().bind("click",{docPath:docPath},function(event){
	  	    	 showdetail(event.data.docPath);}       				
        			  )
	  	      	.appendTo(popup.document.body);
	  			td=popup.document.createElement("br");
	  			popup.document.body.appendChild(td);
					
			}
			
		}
	
	}
	popup.focus();
}

function showdetail(docid) {
	//var  contextPath =  "http://localhost:8007/hix/";
	
	var  contextPath =  "<%=request.getContextPath()%>";
	var  serverName =  "<%=request.getServerName()%>";
	var  serverPort =  "<%=request.getServerPort()%>";
	

    var documentUrl = "http://"+serverName+":"+serverPort+contextPath + "/shop/employer/ticket/viewDocument?documentId="+docid;
    
	open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
}


function func(popup,taskMapString,ticketid){
	var comments = popup.document.getElementById('txtid').value
	var dropdownfields =[]
	var textfields =[]
	var formPropertiesMap = {};
	
	
	dropdownfields = popup.document.getElementsByName("selectField");
	textfields = popup.document.getElementsByName("textField");
	var selectedValue = "";
	
	for(var i=0 ; i<dropdownfields.length; i++) {
	    selectedValue = dropdownfields[i].options[dropdownfields[i].selectedIndex].text;
	    formPropertiesMap[$(dropdownfields[i]).data("selectKey")] = selectedValue;
	}
	
	for(var i=0 ; i<textfields.length; i++) {
	    selectedValue = textfields[i].value;
	    formPropertiesMap[$(textfields[i]).data("txtKey")] = selectedValue;
	}

	
	taskMapString.Comments= comments
	
	var myStringJSON = JSON.stringify(taskMapString);
	var temp=myStringJSON;
	actualObj = JSON.parse(temp);
	
	actualObj.formPropertiesMap = formPropertiesMap;
	
	myStringJSON = JSON.stringify(actualObj);
	
	var completeTaskUrl = '<c:url value="completeUserTask">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';

	if(comments==''){
		alert("Please provide comments");
	}
	
		else{
			$.ajax(
					{
					url : completeTaskUrl,
					type : "POST",
					data : {
						taskObject : myStringJSON
						   },
						success: function(response,xhr)
						{
					        if(isInvalidCSRFToken(xhr))
					          return;
							getTicketTaskData(ticketid)
							},
							error: function(e)
							{
	
								alert("Failed to Fetch Data");
							}
					});
	
			popup.close();		
		}
	}

function showAndHideRow(id) 
{
	
	var hideId = id;
	/* var showHideBool; */
	if( document.getElementById(hideId).style.display=='none')
	{
		document.getElementById(hideId).style.display = '';
	}
	else
	{
		document.getElementById(hideId).style.display = 'none';
	}
}
function hideRow(id)
{
	//alert($('#id').size()+"      "+id);
	if(id != 'hide0')
	{
		document.getElementById(id).style.display = 'none';
	}
}


</script>
<script type="text/javascript">
function addAllColumnHeaders(myList, id)
{
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    for (var i = 0 ; i < myList.length ; i++) {
        var rowHash = myList[i];
        for (var key in rowHash) {
        	if(key=="QueueId"||key=="UserId"||key=="TaskId"||key=="ActivitiTaskId"||key=="ProcessInstanceId"||key=="Comments"||key=="FormProperties"){
            	continue;	
            	}
            if ($.inArray(key, columnSet) == -1){
                columnSet.push(key);
                headerTr$.append($('<th/>').html(key));
            }
        }
    }
    $("#excelDataTable"+id).append(headerTr$);

    return columnSet;
}
</script>