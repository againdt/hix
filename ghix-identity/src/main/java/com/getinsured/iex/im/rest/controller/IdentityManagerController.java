package com.getinsured.iex.im.rest.controller;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Locale;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.im.wso2.SetMultipleUserAttributesHandler;


@Controller
public class IdentityManagerController extends MessageProcessor {
	
	@Autowired
	private WebServiceTemplate wso2ServiceTemplate;
	
	@Autowired
	private WebServiceTemplate wso2EchoTemplate;
	
	private static final SecureRandom random = new SecureRandom();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(IdentityManagerController.class);
	
	private ResponseEntity<String> isUserExistingOnWSO2(String clientIp, String jsonInput){
	
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge authBridge = HubServiceBridge.getHubServiceBridge("IsExistingUserRequest");
			handler = authBridge.getServiceHandler();
			
			handler.setJsonInput(jsonInput);
			
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("WSO2_USERMGMT");
			requestRecord.setEndpointOperationName("WSO2_EXIST_USER");
			requestRecord.setAccessIp(clientIp);
			requestRecord.setStatus("PENDING");
			response = this.executeRequest(requestRecord, handler, wso2ServiceTemplate);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	@RequestMapping(value = "/isExistingUser", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> isExistingUser(@RequestBody String userNamePayload){
	
		ResponseEntity<String> response = null;
		try{
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(userNamePayload);
			String inputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			
			response = isUserExistingOnWSO2((String) objInputJson.get("clientIp"), inputJSON);
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value = "/setMultipleUserAttributes", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> setMultipleUserAttributes(@RequestBody String userNamePayload){
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			handler = new SetMultipleUserAttributesHandler();
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(userNamePayload);
			String inputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			handler.setJsonInput(inputJSON);
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("WSO2_USERMGMT");
			requestRecord.setEndpointOperationName("WSO2_SET_MULTIPLE_USER_ATTRIBUTES");
			requestRecord.setAccessIp((String) objInputJson.get("clientIp"));
			requestRecord.setStatus("PENDING");
			response = this.executeRequest(requestRecord, handler, wso2ServiceTemplate);
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> authenticate(@RequestBody String authRequest){
	
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge authBridge = HubServiceBridge.getHubServiceBridge("AuthRequest");
			handler = authBridge.getServiceHandler();
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(authRequest);
			String apcInputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			
			handler.setJsonInput(apcInputJSON);
			
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("WSO2_USERMGMT");
			requestRecord.setEndpointOperationName("WSO2_AUTH");
			requestRecord.setAccessIp((String) objInputJson.get("clientIp"));
			requestRecord.setStatus("PENDING");
			response = this.executeRequest(requestRecord, handler, wso2ServiceTemplate);
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value = "/changepswd", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> changePassword(@RequestBody String authRequest){
	
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge authBridge = HubServiceBridge.getHubServiceBridge("UpdatePasswordRequest");
			handler = authBridge.getServiceHandler();
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(authRequest);
			String apcInputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			
			handler.setJsonInput(apcInputJSON);
			
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("WSO2_USERMGMT");
			requestRecord.setEndpointOperationName("WSO2_CHPASSWD");
			requestRecord.setAccessIp((String) objInputJson.get("clientIp"));
			requestRecord.setStatus("PENDING");
			response = this.executeRequest(requestRecord, handler, wso2ServiceTemplate);
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value = "/changepswdByAdmin", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> changePasswordByAdmin(@RequestBody String authRequest){
	
       ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		HttpStatus status = HttpStatus.OK;
		try{
			HubServiceBridge authBridge = HubServiceBridge.getHubServiceBridge("UpdatePasswordByAdminRequest");
			handler = authBridge.getServiceHandler();
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(authRequest);
			String apcInputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			
			handler.setJsonInput(apcInputJSON);
			
			response = isUserExistingOnWSO2((String) objInputJson.get("clientIp"), apcInputJSON);
			if(response != null){
				status = response.getStatusCode();
			}else{
				throw new GIRuntimeException("Check for existing user failed with no reponse from server, can not change the password (by admin)");
			}

		    String responseBody = response.getBody();
			if(status.is2xxSuccessful() && responseBody != null && responseBody.trim().compareToIgnoreCase("true") == 0){
				requestRecord = new GIWSPayload();
				requestRecord.setEndpointFunction("WSO2_USERMGMT");
				requestRecord.setEndpointOperationName("WSO2_CHPASSWD_ADMIN");
				requestRecord.setAccessIp((String) objInputJson.get("clientIp"));
				requestRecord.setStatus("PENDING");
				response = this.executeRequest(requestRecord, handler, wso2ServiceTemplate);
			}else{
				LOGGER.info("Check for existing user (By Admin) on WSO2 failed with Status:"+status);
				response = new ResponseEntity<String>("User existence check failed on WSO2", HttpStatus.BAD_REQUEST);
			}
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> updateUser(@RequestBody String claimUpdateRequest){
	
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge authBridge = HubServiceBridge.getHubServiceBridge("SetUserClaimRequest");
			handler = authBridge.getServiceHandler();
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(claimUpdateRequest);
			String updateClaimInputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			
			handler.setJsonInput(updateClaimInputJSON);
			
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("WSO2_USERMGMT");
			requestRecord.setEndpointOperationName("WSO2_SET_USER_CLAIM");
			requestRecord.setAccessIp((String) objInputJson.get("clientIp"));
			requestRecord.setStatus("PENDING");
			response = this.executeRequest(requestRecord, handler, wso2ServiceTemplate);
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value = "/checkAvailability", method = RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> checkAvailability(){
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge authBridge = HubServiceBridge.getHubServiceBridge("EchoRequest");
			handler = authBridge.getServiceHandler();
			handler.setJsonInput("{\"echo\":"+random.nextInt()+"}");
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("WSO2_AVAILABILITY");
			requestRecord.setEndpointOperationName("WSO2_ECHO_INT");
			requestRecord.setAccessIp("0.0.0.0.");
			requestRecord.setStatus("PENDING");
			LOGGER.info("Initiating WSO2 Availability Check "+wso2EchoTemplate.getDefaultUri());
			response = this.executeRequest(requestRecord, handler, wso2EchoTemplate);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		Date date = new TSDate();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate );
		return "home";
	}
}
