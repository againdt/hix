package com.getinsured.hix.batch.hub.writer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.hub.exceptions.HubServiceException;
import com.getinsured.hix.batch.hub.model.HubResponse;

@Component("HubResponseWriter")
public class HubResponseWriter implements ItemWriter<HubResponse> {
	
	private static final Logger LOGGER =LoggerFactory.getLogger(HubResponse.class);
	
	private static final String UPDATE_QUERY = "UPDATE HUB_RESPONSES SET STATUS = ?, RETRY_COUNT = ? WHERE ID = ?";
	
	@Autowired private DataSource dataSource;
	
	public void write(List<? extends HubResponse> hubResponses)	throws HubServiceException {
		
		boolean success = false;
		Connection conn = null;
		Exception ex = null;
		int updated = 0;
		PreparedStatement preparedStatement = null;
		try {
			conn = dataSource.getConnection();
			conn.setAutoCommit(false);
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			for (HubResponse response : hubResponses) {
				preparedStatement.setString(1, response.getStatus());
				preparedStatement.setLong(2, response.getRetryCount());
				preparedStatement.setLong(3, response.getId());
				preparedStatement.addBatch();
			}
			
			int[] x = preparedStatement.executeBatch();
			conn.commit();
			success = true;
			updated += x.length;
			LOGGER.info("Updated "+updated +" Records");
			
		} catch (SQLException se) {
			ex = se;
			if(!success && conn != null){
				try {
					conn.rollback();
				} catch (SQLException e) {
					LOGGER.error("Error rolling back a failed transaction updating HUB_RESPONSES records",e);
				}
			}
		} finally{
			
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (Exception sqlException) {
					LOGGER.error(sqlException.getMessage(),sqlException);
				}
			}
			if(conn != null){
				try{
					conn.close();
				}catch(SQLException se){
					LOGGER.error("Error closing DB connection",se);
				}
			}
		}
		if(ex != null){
			throw new HubServiceException("HUB_RESPONSE update failed",ex);
		}
		
	}
		
}
