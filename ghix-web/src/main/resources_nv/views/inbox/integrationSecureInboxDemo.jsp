<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />

<%-- File upload scripts --%>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>

<link href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" rel="stylesheet"  type="text/css" />
<link href="<c:url value="/resources/js/upload/css/style.css" />" rel="stylesheet"  type="text/css" />

<div class="gutter10">

	<!-- TODO: Need to add notification alert message division at top of integrator jsp -->
	<c:if test="${message != null}">
	<div class="errorblock alert alert-info">
		<p>${message}</p>
	</div>
	</c:if>

	<div class="row-fluid">
		<div class="span3">
			<div class="gray">
				<h4 class="margin0">Actions</h4>
				<ul class="nav nav-list">
					<!-- TODO: Integrations of search link -->
					<li>
						<c:url value="/inbox/secureInboxSearch" var="searchURL">
						   <c:param name="searchText" value="${searchText}"/>
						</c:url>
						<a href="${searchURL}">Search Message Integration</a>
						<%-- <a href="${pageContext.request.contextPath}/inbox/secureInboxSearch?searchText=test">Search Message Integration</a> --%>
					</li>
					<!-- TODO: Integrations of compose message link -->
					<li><a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();"><i class="icon-envelope-unread"></i> Compose Integration</a></li>
				</ul>
			</div>
		</div>
		<div id="rightpanel" class="span9">
			<h3>Integration Secure-Inbox Demo</h3>
		</div>
	</div>
	<!-- row-fluid -->
</div>
<!-- gutter10 -->