package com.getinsured.hix.batch.planmgmt.task;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.planmgmt.service.PlanMgmtService;

public class UpdatePlanStatusTask implements Tasklet{
	
	private static final Logger LOGGER = Logger.getLogger(UpdatePlanStatusTask.class);

	private PlanMgmtService planMgmtService;
	
	public PlanMgmtService getPlanMgmtService() {
		return planMgmtService;
	}

	public void setPlanMgmtService(PlanMgmtService planMgmtService) {
		this.planMgmtService = planMgmtService;
	}
	
	
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws Exception {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Update plan status job started on :- " +new Timestamp(System.currentTimeMillis()));
		} 
		try {
			 String response = planMgmtService.updatePlanStatus();
			 if(LOGGER.isDebugEnabled()){
				 LOGGER.debug("Update plan status response " + response);
			 }	 
         } catch (Exception e) {
        	 LOGGER.error("Exception occur while trigger update plan status job : " + e);
         }
		 if(LOGGER.isDebugEnabled()){
			 LOGGER.debug("Update plan status job finished on :- " +new Timestamp(System.currentTimeMillis()));
		 }
		 return RepeatStatus.FINISHED;
	}
	
}
