package com.getinsured.eligibility.move.enrollment.service;

public class MoveEnrollmentRequest {

	private String userName;
	private Long enrolledSsapApplicationId;
	private Long newSsapApplicationId;

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getEnrolledSsapApplicationId() {
		return enrolledSsapApplicationId;
	}
	public void setEnrolledSsapApplicationId(Long enrolledSsapApplicationId) {
		this.enrolledSsapApplicationId = enrolledSsapApplicationId;
	}
	public Long getNewSsapApplicationId() {
		return newSsapApplicationId;
	}
	public void setNewSsapApplicationId(Long newSsapApplicationId) {
		this.newSsapApplicationId = newSsapApplicationId;
	}

}
