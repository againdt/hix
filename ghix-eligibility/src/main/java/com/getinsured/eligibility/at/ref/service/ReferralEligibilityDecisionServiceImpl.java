package com.getinsured.eligibility.at.ref.service;

import java.util.Date;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.resp.service.AptcHistoryService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

/**
 * @author chopra_s
 * 
 */
@Component("referralEligibilityDecisionService")
@Scope("singleton")
public class ReferralEligibilityDecisionServiceImpl implements ReferralEligibilityDecisionService {
	private static final Logger LOGGER = Logger.getLogger(ReferralEligibilityDecisionServiceImpl.class);

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("referralActivationNotificationService")
	private ReferralActivationNotificationService referralActivationNotificationService;

	@Autowired
	@Qualifier("referralSEPService")
	private ReferralSEPService referralSEPService;

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;
	
	@Autowired
	private IUserRepository userRepository;
	
	@Autowired private QLEValidationService qleValidationService;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired
	@Qualifier("enrollmentUpdateAdapter")
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;
	
	@Autowired
	private AptcHistoryService aptcHistoryService;
	
	@Autowired
	private  ReferralOEService referralOEService;
	
	private static final String MN_STATE_CODE = "MN";

	@Override
	@ReferralTransactionAnno
	public boolean execute(long applicationId, boolean cmrAutoLinking) {
		boolean blnComplete = false;
		try {
			LOGGER.info("ReferralEligibilityDecision for Referral application " + applicationId);

			final SsapApplication application = ssapApplicationRepository.findOne(applicationId);

			if (application == null) {
				LOGGER.error(ReferralConstants.NO_SSAP_FOUND + applicationId);
				throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + applicationId);
			}

			final boolean checkEligibility = isApplicationDenied(application);

			if (checkEligibility) {
				closeSsapApplication(application);
				blnComplete = true;
			} else {
				if (!cmrAutoLinking) {
					// set status back to UC to work activation flow...
					setUnclaimedStatus(application);
				}
				triggerOEEmail(application, cmrAutoLinking);
				blnComplete = cmrAutoLinking;
			}

			LOGGER.info("ReferralEligibilityDecision for Referral application Ends");
		} catch (Exception e) {
			throw e;
		}
		return blnComplete;
	}

	@Override
	@ReferralTransactionAnno
	public boolean execute(long applicationId, ApplicationExtension applicationExtension, boolean cmrAutoLinking) {
		boolean blnComplete = false;
		try {
			LOGGER.info("ReferralEligibilityDecision for Referral application " + applicationId);

			final SsapApplication application = ssapApplicationRepository.findOne(applicationId);

			if (application == null) {
				LOGGER.error(ReferralConstants.NO_SSAP_FOUND + applicationId);
				throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + applicationId);
			}
			final boolean checkEligibility = isApplicationDenied(application);

			if (checkEligibility) {
				closeSsapApplication(application);
				blnComplete = true;
			} else {
				if (!cmrAutoLinking) {
					// set status back to UC to work activation flow...
					setUnclaimedStatus(application);
				}
				blnComplete = processQEAutomation(application, applicationExtension, cmrAutoLinking);
			}
			LOGGER.info("ReferralEligibilityDecision for Referral application Ends, blnComplete - " + blnComplete);
		} catch (Exception e) {
			throw e;
		}
		return blnComplete;
	}

	private boolean processQEAutomation(SsapApplication application, ApplicationExtension applicationExtension, boolean cmrAutoLinking) {
		boolean blnComplete = false;
		// check for Automation
		final SsapApplicant primaryApplicant = retrievePrimaryApplicant(application);
		ApplicantEvent applicantEvent = ApplicationExtensionEventUtil.applicantEventForAutoQEP(primaryApplicant, applicationExtension);
		
		
		if(applicantEvent == null && referralOEService.isQEPDuringOE(application.getCoverageYear()))
		{
			applicantEvent = new ApplicantEvent();
			applicantEvent.setCode(ReferralConstants.QEP_WITHOUT_QLE);
			applicantEvent.setEventDate(new TSDate());
		}
		
		if (applicantEvent != null) {
			// create Application Event
			LOGGER.info("processQEAutomation event creation starts");
			createAutoQEPEvent(application, applicationExtension, applicantEvent);
			Date coverageStartDate = enrollmentUpdateAdapter.getCoverageStartDate(application, ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(application.getId()));
			aptcHistoryService.updateEffectiveDate(coverageStartDate, new Long(application.getId()));
			AccountUser updateUser = userRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
			if (!StringUtils.isEmpty(application.getApplicationStatus())
					&& !application.getApplicationStatus().equals(ApplicationStatus.CLOSED.getApplicationStatusCode())) {
			    qleValidationService.runValidationEngine(application.getCaseNumber(),  Source.EXTERNAL, updateUser.getId());
			}
			// if denial
			if (referralSEPService.checkForDenial(applicantEvent.getEventDate()) || ApplicationExtensionEventUtil.isFutureEventDenied(applicantEvent)) {
				qleValidationService.denySep(application.getCaseNumber(),updateUser.getId());
				blnComplete = true;
			} else {
				triggerQEEmail(application, cmrAutoLinking); // TODO Enrollment Date
				blnComplete = cmrAutoLinking;
			}
		} else { // manual
			triggerQEEmail(application, cmrAutoLinking);
			blnComplete = cmrAutoLinking;
		}
		return blnComplete;
	}
	
	private void updateSepQepDeniedFlag(final SsapApplication application)
	{
		application.setSepQepDenied(ReferralConstants.Y);
	}
	
	private void createAutoQEPEvent(SsapApplication application, ApplicationExtension applicationExtension, ApplicantEvent applicantEvent) {
		final String eventCode = ApplicationExtensionEventUtil.QEP_EVENT_NAME_MAP.get(applicantEvent.getCode());
		LOGGER.info("QEP Event code "+applicantEvent.getCode()+" "+eventCode);
		referralSEPService.persistQEPEventforApplicantandApplication(application, applicantEvent.getEventDate(), eventCode);
	}

	private void triggerQEEmail(SsapApplication application, boolean cmrAutoLinking) {
			if (cmrAutoLinking) {
				try {
					LOGGER.info("trigger QE AutoLinking AccountActivation for " + application.getId());
					referralActivationNotificationService.triggerQEAutoLinkingAccountActivation(application.getId());
				} catch (Exception e) {
					StringBuilder errorReason = new StringBuilder().append("- Error sending QE AutoLinking AccountActivation notice after AT is received ")
							.append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
					LOGGER.error(errorReason.toString());
				}
				try {
					// TODO : the below notice will be triggered in case of MN - we need to remove this once content is finalized
					String stateCode = DynamicPropertiesUtil
							.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					if (stateCode.equalsIgnoreCase(MN_STATE_CODE)) {
						LOGGER.info("trigger LC002 FinancialLCE after AT is received " + application.getId());
						referralLceNotificationService.generateFinancialLCENotice(application.getCaseNumber());
					}
				} catch (Exception e) {
					StringBuilder errorReason = new StringBuilder().append("- Error sending LC002 FinancialLCE notice after AT is received ")
							.append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
					LOGGER.error(errorReason.toString());
				}
			}
	}

	private void triggerOEEmail(SsapApplication application, boolean cmrAutoLinking) {
		try {
			if (cmrAutoLinking) {
				LOGGER.info("trigger OE AutoLinking AccountActivation for " + application.getId());
				referralActivationNotificationService.triggerOEAutoLinkingAccountActivation(application.getId()); // need auto linking mail for OE
			}
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append("- Error sending OE Activation/Auto Linking email ").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());	
		}
	}

	private boolean isApplicationDenied(SsapApplication application) {
		return EligibilityStatus.DE.equals(application.getEligibilityStatus());
	}

	private void closeSsapApplication(SsapApplication ssapApplication) {
		LOGGER.info("Close Application id " + ssapApplication.getId());
		ssapApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
		ssapApplicationRepository.save(ssapApplication);
	}

	private void setUnclaimedStatus(SsapApplication ssapApplication) {
		ssapApplication.setApplicationStatus(ApplicationStatus.UNCLAIMED.getApplicationStatusCode());
		ssapApplicationRepository.save(ssapApplication);
	}

	private SsapApplicant retrievePrimaryApplicant(SsapApplication currentApplication) {
		for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {
			if (ssapApplicant.getPersonId() == ReferralConstants.PRIMARY) {
				return ssapApplicant;
			}
		}
		return null;
	}

	private void triggerDenialNotice(String caseNumber, boolean cmrAutoLinking) {
		try {
			if (cmrAutoLinking) {
				LOGGER.info("triggerDenialNotice for caseNumber - " + caseNumber + " , cmrAutoLinking - " + cmrAutoLinking);
				referralLceNotificationService.generateSEPDenialNotice(caseNumber);
			} else {
				LOGGER.info("triggerDenialNotice for caseNumber - " + caseNumber + " , cmrAutoLinking - " + cmrAutoLinking);
				referralLceNotificationService.generateDenialNotice(caseNumber);
			}
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append("ERROR_LCE_NOTIFICATION  - Notice SEP Denial").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}
}
