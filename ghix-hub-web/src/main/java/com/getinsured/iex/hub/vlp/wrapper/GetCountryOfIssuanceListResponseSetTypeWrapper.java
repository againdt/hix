package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.GetCountryOfIssuanceListResponseSetType;


/**
 * This class is the wrapper for com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.model.GetCountryOfIssuanceListResponseSetType
 * 
 * HIX-34220
 * 
 * @author Nikhil Talreja
 * @since 09-Apr-2014
 *
 */
public class GetCountryOfIssuanceListResponseSetTypeWrapper implements JSONAware{
	
	 private int numberOfCountryCodes;
	 private CountryCodeArrayTypeWrapper countryCodeArray;
	 
	 private static final String NO_OF_COUNTRY_CODES_KEY = "NumberOfCountryCodes";
	 private static final String COUNTRY_CODE_ARRAY_KEY = "CountryCodeArray";
	 
	 public GetCountryOfIssuanceListResponseSetTypeWrapper(GetCountryOfIssuanceListResponseSetType getCountryOfIssuanceListResponseSetType){
		 
		 if(getCountryOfIssuanceListResponseSetType == null){
			 return;
		 }
		 
		 this.numberOfCountryCodes = getCountryOfIssuanceListResponseSetType.getNumberOfCountryCodes();
		 this.countryCodeArray = new CountryCodeArrayTypeWrapper(getCountryOfIssuanceListResponseSetType.getCountryCodeArray());
		 
	 }

	public int getNumberOfCountryCodes() {
		return numberOfCountryCodes;
	}

	public CountryCodeArrayTypeWrapper getCountryCodeArray() {
		return countryCodeArray;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();

		obj.put(NO_OF_COUNTRY_CODES_KEY, this.numberOfCountryCodes);
		obj.put(COUNTRY_CODE_ARRAY_KEY, this.countryCodeArray);
		
		return obj.toJSONString();
	}
	
}
