package com.getinsured.ssap.util;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * TODO: Missing JavaDoc.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-19
 */
public class BloodRelationshipCodeTest {

  @Test
  public void getBloodRelationForCode() {
    BloodRelationshipCode bloodRelationshipCode = BloodRelationshipCode.getBloodRelationForCode(BloodRelationshipCode.SPOUSE.getCode(), true);
    Assert.assertEquals("Blood relationship not correct", BloodRelationshipCode.SPOUSE, bloodRelationshipCode);
  }

  @Test
  public void getBloodRelationForCode2() {
    BloodRelationshipCode bloodRelationshipCode = BloodRelationshipCode.getBloodRelationForCode(BloodRelationshipCode.GRANDCHILD.getCode(), true);
    Assert.assertEquals("Blood relationship not correct", BloodRelationshipCode.OTHER, bloodRelationshipCode);
  }

  @Test
  public void getBloodRelationForCode3() {
    BloodRelationshipCode bloodRelationshipCode = BloodRelationshipCode.getBloodRelationForCode(BloodRelationshipCode.CHILD.getCode(), true);
    Assert.assertEquals("Blood relationship not correct", BloodRelationshipCode.CHILD, bloodRelationshipCode);
  }

  @Test
  public void getBloodRelationForCode4() {
    BloodRelationshipCode bloodRelationshipCode = BloodRelationshipCode.getBloodRelationForCode(BloodRelationshipCode.SELF.getCode(), true);
    Assert.assertEquals("Blood relationship not correct", BloodRelationshipCode.SELF, bloodRelationshipCode);
  }

  @Test
  public void getBloodRelationForCode5() {
    BloodRelationshipCode bloodRelationshipCode = BloodRelationshipCode.getBloodRelationForCode(BloodRelationshipCode.PARENT_OF_CHILD.getCode(), true);
    Assert.assertEquals("Blood relationship not correct", BloodRelationshipCode.OTHER, bloodRelationshipCode);
  }
}
