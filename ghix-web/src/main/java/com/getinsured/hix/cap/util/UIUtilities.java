package com.getinsured.hix.cap.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.cap.consumer.ConsumerCRUDController;
import com.getinsured.timeshift.TimeShifterUtil;

public class UIUtilities {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerCRUDController.class);

	public static long getElapsedTimeInMillis(long startTimeInMillis) {
		return TimeShifterUtil.currentTimeMillis() - startTimeInMillis;
	}

	public static void printPerf(String functionName, long startTime) {
		try{
		LOGGER.info( String.format("[PERF] %s: %s ms", functionName, UIUtilities.getElapsedTimeInMillis(startTime)));
		}catch(Exception ex)
		{
			LOGGER.info("Exception while doing Perf logging. Ignoring.");
		}
 	}

}
