package com.getinsured.eligibility.at.resp.si.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.PersonInfo;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.at.resp.si.handler.helper.MatchHandler;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;

/**
 * MemberMatchHandler to compare stored ssapApplicants with the received state's response.
 *
 * @author Ekram
 *
 */
@Component("memberMatchHandler")
public class MemberHandler {

	private static final String UNCHECKED = "unchecked";
	private static final Logger LOGGER = Logger.getLogger(MemberHandler.class);

	public String processERP(Message<ERPResponse> message){

		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, erpResponse.getApplicationID());

		try {
			Map<String, Object> result = process(erpResponse);

			String resultReason = (String) result.get("reason");

			if (!StringUtils.equalsIgnoreCase(resultReason, EligibilityConstants.OK)){
				LOGGER.info(EligibilityConstants.HOUSEHOLD_NOT_MATCHED);

				@SuppressWarnings(UNCHECKED)
				List<PersonInfo> unmatchedMemberList = (List<PersonInfo>) result.get("unmatchedPersonList");

				resultMap.put(EligibilityConstants.MEMBER_MATCH_RESULT, EligibilityConstants.MANUAL);
				resultMap.put("REASON", resultReason);
				resultMap.put(EligibilityConstants.UNMATCHED_PERSON, unmatchedMemberList);

			} else {
				LOGGER.info(EligibilityConstants.HOUSEHOLD_MATCHED);
				resultMap.put(EligibilityConstants.MEMBER_MATCH_RESULT, EligibilityConstants.AUTO);
			}

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID).
					append(erpResponse.getApplicationID()).append(EligibilityConstants.REASON).append(e);

			LOGGER.error(errorReason.toString());
			resultMap.put(EligibilityConstants.MEMBER_MATCH_RESULT, "ERROR");
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());

		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);
	}

	private Map<String, Object> process(ERPResponse erpResponse){
		//1. Fetch all incoming applicants from AT Response
		List<TaxHouseholdMember> taxHHList =  erpResponse.getTaxHouseholdMemberList();

		//2. Fetch all existing applicants from SSAP_APPLICANT table
		List<TaxHouseholdMember> ssapHHList =  erpResponse.getSsapApplicantList();

		//3. Compare every AT Response applicant with DB..
		/*for (TaxHouseholdMember atTaxPerson : taxHHList) {

			TaxHouseholdMember ssapApplicant = MatchHandler.findMatchingMember(atTaxPerson, ssapHHList);

			if (ssapApplicant == null){
				return false;
			}
		}*/

		Map<String, Object> result = MatchHandler.matchMembers(taxHHList, ssapHHList);
		List<PersonInfo> unmatchedPersonList = new ArrayList<>();
		if (!StringUtils.equalsIgnoreCase((String) result.get("reason"), EligibilityConstants.OK)){
			@SuppressWarnings(UNCHECKED)
			List<TaxHouseholdMember> unmatchedMemberList = (List<TaxHouseholdMember>) result.get(EligibilityConstants.UNMATCHED_MEMBER_LIST);

			for (TaxHouseholdMember taxHouseholdMember : unmatchedMemberList) {
				unmatchedPersonList.add(taxHouseholdMember.getPersonInfo());
			}

			result.put("unmatchedPersonList", unmatchedPersonList);
		}
		return result;
	}

}
