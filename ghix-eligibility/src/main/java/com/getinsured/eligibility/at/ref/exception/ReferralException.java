package com.getinsured.eligibility.at.ref.exception;

/**
 * @author chopra_s
 * 
 */
public final class ReferralException extends Exception {
	private String message;
	private Exception baseException;
	private static final long serialVersionUID = 1L;

	public ReferralException(String message) {
		super();
		this.message = message;
	}

	public ReferralException(String message, Exception baseException) {
		super();
		this.message = message;
		this.baseException = baseException;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Exception getBaseException() {
		return baseException;
	}

	public void setBaseException(Exception baseException) {
		this.baseException = baseException;
	}

}
