package com.getinsured.eligibility.estimator.mini.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.getinsured.affiliate.model.Affiliate;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorRequest;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;

/**
 * Validator class for Mini Estimator
 * JIRA: HIX-12785
 * 
 * @author Nikhil Talreja, Sunil Desu
 * @since 10 July, 2013
 *
 */

@Component
public class MiniEstimatorValidator {
	
	private static final Logger LOGGER = Logger.getLogger(MiniEstimatorValidator.class);
	
	@Autowired private ZipCodeService zipCodeService;  
	@Autowired private RestTemplate restTemplate;
	
	//Error Codes
	protected static final int EMPTY_REQUEST_ERROR_CODE = 10000;
	protected static final int NULL_REQUEST_ERROR_CODE = 10001;
	protected static final int MISSING_STATE_CODE_ERROR_CODE = 10002;
	protected static final int INVALID_STATE_CODE_ERROR_CODE = 10003;
	
	protected static final int INVALID_ZIP_ERROR_CODE = 90001;
	protected static final int INVALID_FAMILY_SIZE_ERROR_CODE = 90002;
	protected static final int INVALID_INCOME_ERROR_CODE = 90003;
	protected static final int INVALID_DOB_ERROR_CODE = 90004;
	protected static final int INVALID_AFFILIATE_ERROR_CODE = 90005;
	protected static final int EMAIL_MISSING_ERROR_CODE = 90006;
	protected static final int INVALID_EMAIL_ERROR_CODE = 90007;
	protected static final int INVALID_API_KEY_ERROR_CODE = 90008;
	protected static final int INVALID_PHONE_ERROR_CODE = 90009;
	protected static final int TECHNICAL_ISSUES_ERROR_CODE = 90010;
	protected static final int PHONE_NUMBER_MISSING_ERROR_CODE = 90011;
	//generic error message code for missing value
	protected static final int MISSING_PARAMETER_ERROR_CODE = 90012;
	protected static final int DB_ERROR_CODE = 90013;
	protected static final int INVALID_VALUE_CODE = 90014;
	
	//Error Messages
	protected static final String EMPTY_REQUEST_ERROR_MESSAGE = "Empty request received";
	protected static final String NULL_REQUEST_ERROR_MESSAGE = "Request object is null";
	protected static final String MISSING_STATE_CODE_ERROR_MESSAGE = "State Code is blank";
	protected static final String INVALID_STATE_CODE_ERROR_MESSAGEE = "Invalid State or State information not available";
	
	protected static final String INVALID_ZIP_ERROR_MESSAGE = "Invalid zip code";
	protected static final String INVALID_FAMILY_SIZE_ERROR_MESSAGE = "Family size must be between 0 and 19";
	protected static final String INVALID_INCOME_ERROR_MESSAGE = "HH Income should be between 0 and 99999999";
	protected static final String INVALID_DOB_ERROR_MESSAGE = "Invalid date of birth";
	protected static final String INVALID_AFFILIATE_ERROR_MESSAGE = "Invalid Affiliate ID";
	protected static final String EMAIL_MISSING_ERROR_MESSAGE = "Email address required";
	protected static final String INVALID_EMAIL_ERROR_MESSAGE = "Invalid email address";
	protected static final String INVALID_API_KEY_ERROR_MESSAGE = "Invalid API key";
	protected static final String INVALID_PHONE_ERROR_MESSAGE = "Invalid phone number";
	protected static final String TECHNICAL_ISSUES_ERROR_MESSAGE = "Technical Issues";
	protected static final String PHONE_NUMBER_MISSING_ERROR_MESSAGE = "Phone number is required";
	protected static final String EMPTY_FIRST_NAME="First name is mandatory";
	protected static final String EMPTY_LAST_NAME="Last name is mandatory";
	protected static final String FLOW_ID_MISSING="Flow id missing";
	protected static final String ERROR_SAVING_TO_DB = "Error saving to Database";
	

	//Constants
	public static final int ZIP_COUNTY_LENGTH = 5;
	public static final int MIN_FAMILY_SIZE = 0;
	public static final int MAX_FAMILY_SIZE = 19;
	private static final double MAX_HH_INCOME = 99999999.0;
	public static final String PATTERN_EMAIL = "^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})";
	public static final int PHONE_NUMBER_LENGTH = 10;
	public static final int DOB_LENGTH = 10;

	
	/**
	 * Method to validate API key for an affiliate partner
	 *
	 * @author Nikhil Talreja
	 * @since 10 July, 2013
	 * 
	 * @return Affiliate - Affiliate object
	 */
	protected Affiliate validateAPIKey(long affiliateId, String apiKey,MiniEstimatorResponse response) {
		
		ResponseEntity<Affiliate> affiliateResponseEntity = null;
		
		try{
			
			if(apiKey == null){
				LOGGER.info("API key is null");
				response.getErrors().put(MiniEstimatorUtil.INVALID_API_KEY_ERROR_CODE,MiniEstimatorUtil.INVALID_API_KEY_ERROR_MESSAGE);
				return null;
			}
			
			String url = String.format(GhixEndPoints.AffiliateServiceEndPoints.VALIDATE_APIKEY, affiliateId,apiKey);
			LOGGER.debug("Affiliate validation URL : "+url);
			
			affiliateResponseEntity = restTemplate.getForEntity(url, Affiliate.class);
			if(affiliateResponseEntity.getBody() == null){
				LOGGER.info("Affiliate Id/API key is invalid");
				response.getErrors().put(MiniEstimatorUtil.INVALID_API_KEY_ERROR_CODE,"Invalid Affiliate Id/Api Key");
			}
			else{
				LOGGER.debug("Affiliate : " + affiliateResponseEntity.getBody().getCompanyName());
			}
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			throw new RuntimeException("Unable to validate API key "+e.fillInStackTrace(), e);
		}
	
		return affiliateResponseEntity.getBody();
	}
	
	/**
	 * @author Nikhil Talreja, Sunil Desu
	 * @since 11 July 2013
	 * 
	 * Validates the zip code for Mini-Estimator request. 
	 * If zipcode and county code combination is valid, state code is returned.
	 * 
	 * @param MiniEstimatorRequest
	 * @param MiniEstimatorResponse
	 * @return stateCode
	 */
	public ZipCode validateZipCode(MiniEstimatorRequest request, MiniEstimatorResponse response){
		
		// Zip Code
		LOGGER.info("Validating zip code");	
		List<ZipCode> zipCodes = zipCodeService.findByZip(request.getZipCode());
		
		//Checking if zip code is valid
		if(CollectionUtils.isEmpty(zipCodes)){
			LOGGER.error("Zip Code is invalid");
			response.getErrors().put(INVALID_ZIP_ERROR_CODE,INVALID_ZIP_ERROR_MESSAGE);			
			return null;
		}

		/*
		 * Check if county code is valid
		 * County code is a concatenation of state FIPs + county FIPs
		 */			
		for(ZipCode record : zipCodes){
			
			//TODO - Check for state+county fips once county fips is split
			
			if(record.getStateFips() != null && record.getCountyFips() != null && StringUtils.equalsIgnoreCase(request.getCountyCode(),
					record.getStateFips().concat(record.getCountyFips()))){
				return record;
			}
			
//			else if(StringUtils.equalsIgnoreCase(request.getCountyCode(),record.getCountyFips())){
//				stateCode =  record.getState();
//				break;
//			}
		}
	
		
		//LOGGER.error("Zip Code and county code combination is invalid.");
		response.getErrors().put(INVALID_ZIP_ERROR_CODE,"Invalid County Code");
	
		return null;
	}
	
	/**
	 * Method to validate DOB for all household members
	 *
	 * @author Nikhil Talreja
	 * @since 10 July, 2013
	 */
	
	public boolean validateDob(List<Member> members){
		
		LOGGER.info("Validating DOB for all members");
		
		if(members == null){
			return false;
		}
		else{
			
			String dob = null;
			
			for(Member record : members){
				
				dob = record.getDateOfBirth();
				
				if(StringUtils.isBlank(dob)){
					continue;
				}
				else if(StringUtils.length(dob) != DOB_LENGTH){
					return false;
				}
				try{
					Date dateOfBirth = stringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
					if(dateOfBirth.compareTo(TSCalendar.getInstance().getTime()) > 0){
						return false;
					}
				}
				catch (Exception e){
					LOGGER.error("DOB is invalid"+e);
					return false;
				}

			}
		}
		
		return true;
	}
	
	public boolean validateTobaccoUsage(List<Member> members){
		if(members!=null){
			for(Member record : members){
				if(record.getIsTobaccoUser()!=null && !StringUtils.isBlank(record.getIsTobaccoUser())){
					if(!(record.getIsTobaccoUser().equalsIgnoreCase("Y")||record.getIsTobaccoUser().equalsIgnoreCase("N"))){
						return false;
					}
				}
			}
		}
		
		return true;
	}

	
	
	/**
	 * Validates the Mini-Estimator request
	 * 
	 * @author - Nikhil Talreja
	 * @param response 
	 * @since 10 July, 2013
	 */
	protected boolean validateMiniEstimatorRequest(MiniEstimatorRequest request, MiniEstimatorResponse response) {
		
		LOGGER.debug("Request \n " + request.toString());
		response.setMembers(request.getMembers());
		
		// Family Size
		LOGGER.info("Validating family size");
		if(request.getFamilySize() <= MIN_FAMILY_SIZE || request.getFamilySize() > MAX_FAMILY_SIZE
				|| CollectionUtils.isEmpty(request.getMembers())){
			response.getErrors().put(INVALID_FAMILY_SIZE_ERROR_CODE,INVALID_FAMILY_SIZE_ERROR_MESSAGE);
		}
		
		// HH Income
		LOGGER.info("Validating HH Income");
		if (request.getHhIncome() < 0 || request.getHhIncome() > MAX_HH_INCOME) {
			response.getErrors().put(INVALID_INCOME_ERROR_CODE,INVALID_INCOME_ERROR_MESSAGE);
		}
		
		//DOB for all members
		if(!validateDob(request.getMembers())){
			response.getErrors().put(INVALID_DOB_ERROR_CODE,INVALID_DOB_ERROR_MESSAGE);
		}
		
		/*
		 * Email address - Verify pattern if present in request
		 */
		if(StringUtils.isNotBlank(request.getEmail()) && !request.getEmail().matches(PATTERN_EMAIL)){
			response.getErrors().put(INVALID_EMAIL_ERROR_CODE,INVALID_EMAIL_ERROR_MESSAGE);
		}
		
		/*
		 * Phone Number - Validate only if present in request
		 */
		if(StringUtils.isNotBlank(request.getPhone()) && 
					(StringUtils.length(request.getPhone()) != PHONE_NUMBER_LENGTH || 
					!StringUtils.isNumeric(request.getPhone()) ||
					StringUtils.startsWithIgnoreCase(request.getPhone(), "0")) ){
			response.getErrors().put(INVALID_PHONE_ERROR_CODE,INVALID_PHONE_ERROR_MESSAGE);
		}
		
		return true;
	}
	
	public Date stringToDate(String dateStr, String format) throws ParseException {
	    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	    dateFormat.setLenient(false);
	    Date convertedDate = new TSDate();
		convertedDate = dateFormat.parse(dateStr);
	    return convertedDate;
	}
}
