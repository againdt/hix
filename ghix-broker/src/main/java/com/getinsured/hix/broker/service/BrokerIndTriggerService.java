package com.getinsured.hix.broker.service;

import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant;

public interface BrokerIndTriggerService {
	
	public void triggerInd35(Broker broker, PaymentMethods paymentMethods);

	EntityResponseDTO triggerInd47(DesignateBroker designateBroker);

	void triggerInd54(AgencyAssistant agencyAssistant);

	void triggerInd35(AgencyAssistant agencyAssistant);

	
}
