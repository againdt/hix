package com.getinsured.eligibility.at.mes.queue.dto;

import org.joda.time.DateTime;

public class ATQueueInfoResponseDTO {

	String atProcessingType;
	DateTime dueDate;
	
	public String getAtProcessingType() {
		return atProcessingType;
	}
	public void setAtProcessingType(String atProcessingType) {
		this.atProcessingType = atProcessingType;
	}
	public DateTime getDueDate() {
		return dueDate;
	}
	public void setDueDate(DateTime dueDate) {
		this.dueDate = dueDate;
	}
	
}
