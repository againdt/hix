package com.getinsured.hix.ssap;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.eligibility.enums.ApplicationSource;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.iex.hub.Countries;
import com.getinsured.hix.iex.hub.repository.ICountriesRepository;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.ridp.exception.RIDPServiceException;
import com.getinsured.hix.ridp.service.RIDPService;
import com.getinsured.hix.ssap.models.AmericanAlaskaTribe;
import com.getinsured.hix.ssap.models.SsapPrimaryApplicantInfo;
import com.getinsured.hix.ssap.util.SsapUtility;
import com.getinsured.iex.client.ResourceCreatorImpl;
import com.getinsured.iex.client.SsapApplicationRequest;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapApplication;
import com.getinsured.iex.dto.SsapApplicationEventResource;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.SsapPreferencesDTO;
import com.getinsured.iex.dto.qep.QepEventRequest;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.ContactPreferences;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.util.IexEnumAdapterFactory;
import com.getinsured.iex.util.SsapUtil;
import com.getinsured.ssap.model.eligibility.EligibilityResponse;
import com.getinsured.ssap.model.eligibility.ResponseStatus;
import com.getinsured.ssap.model.eligibility.RunMode;
import com.getinsured.ssap.model.financial.IfsvResponse;
import com.getinsured.ssap.service.EligibilityEngineIntegrationService;
import com.getinsured.ssap.service.IfsvService;
import com.getinsured.ssap.service.SingleStreamlinedApplicationService;
import com.getinsured.timeshift.util.TSDate;
import com.getinsured.web.configuration.StateConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


@Controller
public class SSAPController extends Util{
	private static final String CONTACT_INFORMATION_UPDATE = "CONTACT_INFORMATION_UPDATE";
	private static final String APPEVENT_PREFERENCES = "APPEVENT_PREFERENCES";
	private static final String UPDATE_PREFERENCES = "/application/updatePreferences";
	private static final String SSAP_APPLICANT_DISPLAY_ID = "SSAP_APPLICANT_DISPLAY_ID";
	private static final int SERVER_DATE_NO_1 = 5;
	private static final String SSAP_VISITED = "ssapVisited";
	private static final String SSAP_APPLICATION_EVENT_LINK = "SSAP_APPLICATION_EVENT_LINK";
	private static final String CASE_NUMBER_IN_SESSION = "caseNumberInSession";
	private static final String SINGLE_STREAMLINED_APPLICATION = "singleStreamlinedApplication";
	private static final String SSAP_APPLICATION_ID = "SSAP_APPLICATION_ID";
	private static final String SSAP_APPLICANTS = "applicants";
	private static final String APPEVENT_APPLICATION = "APPEVENT_APPLICATION";
	private static final String APPLICATION_CREATED = "APPLICATION_CREATED";
	private static final String APPLICATION_SUBMITTED = "APPLICATION_SUBMITTED";
	private static final String APPLICATION_ID  = "Application ID";
	private static final String APPLICATION_TYPE  = "Application Type";
	private static final String NON_FINANCIAL_APPLICATION  = "Non Financial Application";
	//private static final String CSR_OVER_RIDE= "csroverride";
	private static Logger ssapControllerLogger = LoggerFactory.getLogger(SSAPController.class);

	@Autowired private Gson platformGson;
	@Autowired private ResourceCreatorImpl resourceCreatorImpl;
	@Autowired private UserService userService;
	@Autowired private LookupService lookUpService;
	@Autowired private ConsumerPortalUtil consumerPortalUtil;
	@Autowired private ZipCodeService zipCodeService;
	@Autowired private RestTemplate restTemplate;
	@Autowired private LocationService locationService;
	@Autowired private ICountriesRepository iCountriesRepository;
	@Autowired private SsapUtil ssapUtil;
	@Autowired private RIDPService ridpService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private HIXHTTPClient hIXHTTPClient;
	@Autowired private AppEventService appEventService;
	@Autowired private LookupService lookupService;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired
	private PreferencesService preferencesService;

	@Autowired
	private SingleStreamlinedApplicationService singleStreamlinedApplicationService;

	@Autowired
	private StateConfiguration stateConfiguration;

	@Autowired
	private EligibilityEngineIntegrationService eligibilityEngineIntegrationService;

	@Autowired
	private IfsvService ifsvService;

	@GiAudit(transactionName = "Read default ssap-application data for creating new ssap..", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.NON_FINANCIAL_APP )
	@RequestMapping(value="/ssapnew", method = RequestMethod.GET)
	public String startSSAPNew(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) throws GIException {
		
		if(request.getParameter("caseNumber")!=null && request.getParameter("caseNumber").trim().length()>0){
    		redirectAttributes.addFlashAttribute("caseNumber", request.getParameter("caseNumber").trim());
    	}

		return "redirect:/ssap";
	}

	private String startSSAPOld(String caseNumberInFlash,
															String applicationType,
															String coverageYear, Model model,
															HttpServletRequest request,HttpServletResponse response) throws InvalidUserException, GIException
	{
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0); // Proxies.
		HttpSession session = request.getSession();
		Date date = new Date();
		String dateOfServer = ((date.getMonth()+1)+"".length()==2?(date.getMonth()+1):"0"+(date.getMonth()+1)).toString();
		dateOfServer+="/"+(date.getDate()+"".length()==2?date.getDate():"0"+date.getDate()).toString();
		dateOfServer+= "/"+date.toString().split(" ")[SERVER_DATE_NO_1];
		String ssapApplicationJson = "";
		String currentPageId = "";
		String applicationStatus = ApplicationStatus.OPEN.getApplicationStatusCode();
		String applicationMode = "";
		SsapApplicationResource ssapApplicationResource = null;
		SsapApplicationEventResource ssapApplicationEventResource = null;
		Household household=null;
		String designatedBrokerLicenseNumber = "";
		String designatedBrokerName = "";
		String brokerFirstName = "";
		String brokerLastName = "";
		String internalBrokerId = "0";
		String internalAssisterId = "0";
		String designatedAssisterLicenseNumber = "";
		String designatedAssisterName = "";
		String assisterFirstName = "";
		String assisterLastName = "";
		String helpingEntityFlag = "NONE";
		String applicationSource = "";
		long ssapId = 0;

		boolean isRidpVerified = false;
		int houseHoldId = 0;

		ssapControllerLogger.debug("Before RIDP ***");
		String ssapVisited = request.getSession().getAttribute(SSAP_VISITED)==null?"N":request.getSession().getAttribute(SSAP_VISITED).toString();
		session.setAttribute(SSAP_APPLICATION_EVENT_LINK,null);
		session.setAttribute(SSAP_APPLICATION_ID,null);

		//----------
		SsapPrimaryApplicantInfo ssapPrimaryApplicantInfo = new SsapPrimaryApplicantInfo();
		try {
			houseHoldId = userService.getLoggedInUser().getActiveModuleId();
			household = consumerPortalUtil.getHouseholdRecord(houseHoldId);
			if(household!=null){
				ssapPrimaryApplicantInfo.setFirstName(household.getFirstName());
				ssapPrimaryApplicantInfo.setMiddleName(household.getMiddleName());
				ssapPrimaryApplicantInfo.setLastName(household.getLastName());
				ssapPrimaryApplicantInfo.setPhoneNumber(household.getPhoneNumber());
				ssapPrimaryApplicantInfo.setEmailAddress(household.getEmail());
				ssapPrimaryApplicantInfo.setNameSuffix(getMatchingSuffixValue(household.getNameSuffix()));
				ssapPrimaryApplicantInfo.setDateOfBirth(household.getBirthDate()); 

			}
		}
		catch (InvalidUserException e1) {
			ssapControllerLogger.error("ERROR LOADING HOUSEHOLD INFORMATION>>>>>>>>>>>>>>>>>", e1);
			throw new GIRuntimeException(e1);
		}
		if(StringUtils.isEmpty(caseNumberInFlash) && (StringUtils.isEmpty(coverageYear) || !isNewApplicationCreationApplicable(houseHoldId, new Long(coverageYear)))){
			return "redirect:/indportal";
		}

		String ridpverified;
		try {
			ridpverified = ridpService.isRidpRequired().booleanValue() ? "N" : "Y";
		} catch (RIDPServiceException ridpException) {
			ssapControllerLogger.error("Error getting the ridp verification information", ridpException);
			throw new GIRuntimeException(ridpException);
		} // y or n
		String csrOverRide = (session.getAttribute(SsapConstants.CSR_OVER_RIDE)==null)?"N":"Y";
		if("Y".equalsIgnoreCase(csrOverRide) || (StringUtils.isNotEmpty(ridpverified) &&  ridpverified.equalsIgnoreCase("Y"))) {
			isRidpVerified = true;
		}


		if(!isRidpVerified && "N".equals(ssapVisited)) {
			request.getSession().setAttribute(SSAP_VISITED, "Y");
			return "redirect:ridp/verification";
		}
		//ssapControllerLogger.info("caseNumber:" + caseNumberInFlash);
		ssapApplicationResource = getApplicationByCaseNumber(caseNumberInFlash);
		if(ssapApplicationResource != null) {

			if(houseHoldId != ssapApplicationResource.getCmrHouseoldId().intValueExact()) {
				ssapControllerLogger.error("Permission denied to access the caseNumber:: ");
				throw new GIRuntimeException("Permission denied to access the application");
			}

			// Prevent editing the LCE applications to maintain the data consistency
			if("Y".equalsIgnoreCase(csrOverRide)){
				if(SsapApplicationEventTypeEnum.SEP.name().equalsIgnoreCase(ssapApplicationResource.getApplicationType())){
					ssapControllerLogger.error("Permission denied to edit SEP application with the caseNumber:: ");
					return "redirect:/indportal";
				}

				if(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplicationResource.getApplicationStatus())) {
					ssapControllerLogger.error("Permission denied to edit EN status application with the caseNumber:: " );
					return "redirect:/indportal";
				}
			}
			ssapApplicationJson = ssapApplicationResource.getApplicationData();

			currentPageId = ssapApplicationResource.getCurrentPageId();
			applicationStatus = ssapApplicationResource.getApplicationStatus();
			applicationMode = "editmode";
			ssapId = getApplicationId(ssapApplicationResource);
			session.setAttribute(SSAP_APPLICATION_ID,ssapId);
			applicationSource = ssapApplicationResource.getSource();
			if(StringUtils.isEmpty(applicationType)) {
				applicationType = ssapApplicationResource.getApplicationType();
			}
			if(StringUtils.isEmpty(coverageYear)) {
				coverageYear = ssapApplicationResource.getCoverageYear() + "";
			}
		}

		/*
		if(ssapId!=0){
			try {
				ssapApplicationEventResource = reseourceCreatorImpl.findSsapApplicationEventBySsapApplicationId(ssapId);
			} catch (Exception e) {
				ssapControllerLogger.error("ERROR FINDING SSAP_APPLICATION_EVENT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getLocalizedMessage(),e);
				throw new GIRuntimeException(e);
			}
			if(ssapApplicationEventResource != null){
				session.setAttribute(SSAP_APPLICATION_EVENT_LINK,    GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "/SsapApplicationEvent/"+Util.getIDFromLink(ssapApplicationEventResource.get_links()));
			}
			session.setAttribute(SSAP_APPLICATION_ID,ssapId);
		}*/

		try {
			Broker broker = getAssignedBroker();
			if(broker!=null){
				designatedBrokerLicenseNumber = broker==null?"":broker.getLicenseNumber();
				designatedBrokerName = broker==null?"":broker.getUser().getFullName();
				brokerFirstName = broker!=null?broker.getUser().getFirstName():"";
				brokerLastName = broker!=null?broker.getUser().getLastName():"";
				internalBrokerId = broker!=null ?String.valueOf(broker.getId()):"0";
				helpingEntityFlag = "BROKER";
			}else{
				Assister assister = getAssignedAssister();
				if(assister!=null && "Active".equalsIgnoreCase(assister.getStatus())){
					designatedAssisterLicenseNumber = assister!=null && assister.getCertificationNumber()!=null?String.valueOf(assister.getCertificationNumber()):"";
					designatedAssisterName = assister!=null?assister.getUser().getFullName():"";
					assisterFirstName = assister!=null?assister.getUser().getFirstName():"";
					assisterLastName = assister!=null?assister.getUser().getLastName():"";
					internalAssisterId = assister!=null ?String.valueOf(assister.getId()):"0";
					helpingEntityFlag = "ASSISTER";
				}
			}
		} catch (Exception e) {
			ssapControllerLogger.error("ERROR LOADING BROKER/ASSISTER INFORMATION>>>>>>>>>>>>>>>>>", e);
			throw new GIRuntimeException(e);
		}


		Map<String,String> countriesMap =  new HashMap<String, String>();

		List<Countries> alcountriesList  = (ArrayList<Countries>)iCountriesRepository.findAll();
		//ssapControllerLogger.debug("alcountriesList size ====" +alcountriesList.size());
		for (Countries countriesObj : alcountriesList) {
			if(StringUtils.isNotEmpty(countriesObj.getCountryCode())){
				countriesMap.put(countriesObj.getCountryCode(), countriesObj.getCountryName());
			}
		}

		ssapControllerLogger.debug(" Country of Issuance found");




		model.addAttribute("ssapApplicationJson",ssapApplicationJson);
		model.addAttribute("ssapCurrentPageId",currentPageId);
		model.addAttribute("ssapApplicationStatus",applicationStatus);
		model.addAttribute("applicationMode",applicationMode);
		model.addAttribute("dateOfServer",dateOfServer);
		model.addAttribute("designatedBrokerLicenseNumber",designatedBrokerLicenseNumber);
		model.addAttribute("designatedBrokerName",designatedBrokerName);
		model.addAttribute("brokerFirstName",brokerFirstName);
		model.addAttribute("brokerLastName",brokerLastName);
		model.addAttribute("internalBrokerId",internalBrokerId);
		model.addAttribute("internalAssisterId",internalAssisterId);
		model.addAttribute("designatedAssisterLicenseNumber",designatedAssisterLicenseNumber);
		model.addAttribute("designatedAssisterName",designatedAssisterName);
		model.addAttribute("assisterFirstName",assisterFirstName);
		model.addAttribute("assisterLastName",assisterLastName);
		model.addAttribute("helpingEntity",helpingEntityFlag);
		model.addAttribute("alCountryOfIssuances",platformGson.toJson(SsapUtility.simpleMapValueSort(countriesMap)));
		model.addAttribute("cmrHouseHold",platformGson.toJson(ssapPrimaryApplicantInfo));
		model.addAttribute("applicationSource",applicationSource);
		model.addAttribute("applicantCounter", getMaxpersonId(caseNumberInFlash));
		model.addAttribute("applicationType", applicationType);
		model.addAttribute("coverageYear", coverageYear);
		
		String stateExchangeCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		model.addAttribute("stateExchangeCode", stateExchangeCode);

		//set pre-populattion data
		PreferencesDTO preferencesDTO= preferencesService.getPreferences(household.getId(), false);
		if(preferencesDTO !=null){
			model.addAttribute("writtenLang", StringUtils.isBlank(preferencesDTO.getPrefWrittenLang())?"English":preferencesDTO.getPrefWrittenLang());
			model.addAttribute("spokenLang", StringUtils.isBlank(preferencesDTO.getPrefSpokenLang())?"English":preferencesDTO.getPrefSpokenLang());
			model.addAttribute("prefCommunication", preferencesDTO.getPrefCommunication() ==null ?"Email":preferencesDTO.getPrefCommunication().getMethod());
		}
		model.addAttribute("mailingAddress", household.getPrefContactLocation());

		model.addAttribute("userEmailAddress", household.getEmail());
		// Get the QEP grace period + 60 days
		int enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (StringUtils.isNumeric(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
		model.addAttribute("qepAllowedInterval", enrollmentGracePeriod + 60);

		if(!"INDIVIDUAL".equalsIgnoreCase(userService.getLoggedInUser().getDefRole().getName()) && "N".equalsIgnoreCase(csrOverRide)) {
			model.addAttribute(SsapConstants.TRACK_APPLICATION_SOURCE,"Y");
		}
		else {
			model.addAttribute(SsapConstants.TRACK_APPLICATION_SOURCE,"N");
		}

		if(session.getAttribute(SsapConstants.CSR_OVER_RIDE) != null) {
			model.addAttribute(SsapConstants.CSR_OVER_RIDE,session.getAttribute(SsapConstants.CSR_OVER_RIDE).toString());
		}
		request.getSession().setAttribute(SSAP_VISITED, "N");

		GiAuditParameterUtil.add(new GiAuditParameter("caseNumber", caseNumberInFlash));
		GiAuditParameterUtil.add(new GiAuditParameter("coverageYear", coverageYear));
		GiAuditParameterUtil.add(new GiAuditParameter("ssapID", ssapId));

		return  "ssapmainold";
	}

	private String startSSAPReact(String caseNumberInFlash,
															String applicationType,
															String coverageYear, Model model,
															HttpServletRequest request,HttpServletResponse response) throws InvalidUserException, GIException {

		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0); // Proxies.
		HttpSession session = request.getSession();
		String dateOfServer = getServerDate();
		String ssapApplicationJson = "";
		String currentPageId = "";
		String applicationStatus = ApplicationStatus.OPEN.getApplicationStatusCode();
		String applicationMode = "";
		SsapApplicationResource ssapApplicationResource = null;
		Household household=null;
		String designatedBrokerLicenseNumber = "";
		String designatedBrokerName = "";
		String brokerFirstName = "";
		String brokerLastName = "";
		String internalBrokerId = "0";
		String internalAssisterId = "0";
		String designatedAssisterLicenseNumber = "";
		String designatedAssisterName = "";
		String assisterFirstName = "";
		String assisterLastName = "";
		String helpingEntityFlag = "NONE";
		String applicationSource = "";
		long ssapId = 0;

		boolean isRidpVerified = false;
		int houseHoldId = 0;

		ssapControllerLogger.debug("Before RIDP ***");
		//String ssapVisited = request.getSession().getAttribute(SSAP_VISITED)==null?"N":request.getSession().getAttribute(SSAP_VISITED).toString();
		session.setAttribute(SSAP_APPLICATION_EVENT_LINK,null);
		session.setAttribute(SSAP_APPLICATION_ID,null);

		//----------
		SsapPrimaryApplicantInfo ssapPrimaryApplicantInfo = new SsapPrimaryApplicantInfo();
		try {
			houseHoldId = userService.getLoggedInUser().getActiveModuleId();
			household = consumerPortalUtil.getHouseholdRecord(houseHoldId);
			if(household!=null){
				ssapPrimaryApplicantInfo.setFirstName(household.getFirstName());
				ssapPrimaryApplicantInfo.setMiddleName(household.getMiddleName());
				ssapPrimaryApplicantInfo.setLastName(household.getLastName());
				ssapPrimaryApplicantInfo.setPhoneNumber(household.getPhoneNumber());
				ssapPrimaryApplicantInfo.setEmailAddress(household.getEmail());
				ssapPrimaryApplicantInfo.setNameSuffix(getMatchingSuffixValue(household.getNameSuffix()));

			}
		}
		catch (InvalidUserException e1) {
			ssapControllerLogger.error("ERROR LOADING HOUSEHOLD INFORMATION>>>>>>>>>>>>>>>>>", e1);
			throw new GIRuntimeException(e1);
		}
		if(StringUtils.isEmpty(caseNumberInFlash) && (StringUtils.isEmpty(coverageYear) ||
				!isNewApplicationCreationApplicable(houseHoldId, new Long(coverageYear))))
		{
			ssapControllerLogger.error("SSAP: Redirecting to individual portal. caseNumberInfFlash: {}, coverageYear: {}, householdId: {} (if first two empty or new app creation not available for hh)",
					caseNumberInFlash, coverageYear, houseHoldId);
			return "redirect:/indportal";
		}

		String ridpverified;
		try {
			ridpverified = ridpService.isRidpRequired() ? "N" : "Y";
		} catch (RIDPServiceException ridpException) {
			ssapControllerLogger.error("Error getting the ridp verification information", ridpException);
			throw new GIRuntimeException(ridpException);
		} // y or n
		String csrOverRide = (session.getAttribute(SsapConstants.CSR_OVER_RIDE)==null)?"N":"Y";
		if("Y".equalsIgnoreCase(csrOverRide) || (StringUtils.isNotEmpty(ridpverified) &&  ridpverified.equalsIgnoreCase("Y"))) {
			isRidpVerified = true;
		}


		/*if(!isRidpVerified && "N".equals(ssapVisited)) {
			request.getSession().setAttribute(SSAP_VISITED, "Y");
			return "redirect:ridp/verification";
		}*/

		//ssapControllerLogger.info("caseNumber:" + caseNumberInFlash);
		ssapApplicationResource = getApplicationByCaseNumber(caseNumberInFlash);
		if(ssapApplicationResource != null) {

			if(houseHoldId != ssapApplicationResource.getCmrHouseoldId().intValueExact()) {
				ssapControllerLogger.error("Permission denied to access the caseNumber:: ");
				throw new GIRuntimeException("Permission denied to access the application");
			}

			// Prevent editing the LCE applications to maintain the data consistency
			if("Y".equalsIgnoreCase(csrOverRide)){
				if(SsapApplicationEventTypeEnum.SEP.name().equalsIgnoreCase(ssapApplicationResource.getApplicationType())){
					ssapControllerLogger.error("Permission denied to edit SEP application with the caseNumber:: ");
					return "redirect:/indportal";
				}

				if(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplicationResource.getApplicationStatus())) {
					ssapControllerLogger.error("Permission denied to edit EN status application with the caseNumber:: " );
					return "redirect:/indportal";
				}
			}

			ssapApplicationJson = ssapApplicationResource.getApplicationData();
			currentPageId = ssapApplicationResource.getCurrentPageId();
			applicationStatus = ssapApplicationResource.getApplicationStatus();
			applicationMode = "editmode";
			ssapId = getApplicationId(ssapApplicationResource);
			session.setAttribute(SSAP_APPLICATION_ID,ssapId);
			applicationSource = ssapApplicationResource.getSource();
			if(StringUtils.isEmpty(applicationType)) {
				applicationType = ssapApplicationResource.getApplicationType();
			}
			if(StringUtils.isEmpty(coverageYear)) {
				coverageYear = ssapApplicationResource.getCoverageYear() + "";
			}
		}

		try {
			Broker broker = getAssignedBroker();
			if(broker!=null){
				designatedBrokerLicenseNumber = broker==null?"":broker.getLicenseNumber();
				designatedBrokerName = broker==null?"":broker.getUser().getFullName();
				brokerFirstName = broker!=null?broker.getUser().getFirstName():"";
				brokerLastName = broker!=null?broker.getUser().getLastName():"";
				internalBrokerId = broker!=null ?String.valueOf(broker.getId()):"0";
				helpingEntityFlag = "BROKER";
			}else{
				Assister assister = getAssignedAssister();
				if(assister!=null && "Active".equalsIgnoreCase(assister.getStatus())){
					designatedAssisterLicenseNumber = assister!=null && assister.getCertificationNumber()!=null?String.valueOf(assister.getCertificationNumber()):"";
					designatedAssisterName = assister!=null?new StringBuilder(assister.getFirstName()).append(" ").append(assister.getLastName()).toString():"";
					assisterFirstName = assister!=null?assister.getFirstName():"";
					assisterLastName = assister!=null?assister.getLastName():"";
					internalAssisterId = assister!=null ?String.valueOf(assister.getId()):"0";
					helpingEntityFlag = "ASSISTER";
				}
			}
		} catch (Exception e) {
			ssapControllerLogger.error("ERROR LOADING BROKER/ASSISTER INFORMATION>>>>>>>>>>>>>>>>>", e);
			throw new GIRuntimeException(e);
		}


		Map<String,String> countriesMap =  new HashMap<String, String>();

		List<Countries> alcountriesList  = iCountriesRepository.findAll();
		//ssapControllerLogger.debug("alcountriesList size ====" +alcountriesList.size());
		for (Countries countriesObj : alcountriesList) {
			if(StringUtils.isNotEmpty(countriesObj.getCountryCode())){
				countriesMap.put(countriesObj.getCountryCode(), countriesObj.getCountryName());
			}
		}

		ssapControllerLogger.debug(" Country of Issuance found");

		model.addAttribute("ssapCurrentPageId",currentPageId);
		model.addAttribute("ssapApplicationStatus",applicationStatus);
		model.addAttribute("applicationMode",applicationMode);
		model.addAttribute("dateOfServer",dateOfServer);
		model.addAttribute("designatedBrokerLicenseNumber",designatedBrokerLicenseNumber);
		model.addAttribute("designatedBrokerName",designatedBrokerName);
		model.addAttribute("brokerFirstName",brokerFirstName);
		model.addAttribute("brokerLastName",brokerLastName);
		model.addAttribute("internalBrokerId",internalBrokerId);
		model.addAttribute("internalAssisterId",internalAssisterId);
		model.addAttribute("designatedAssisterLicenseNumber",designatedAssisterLicenseNumber);
		model.addAttribute("designatedAssisterName",designatedAssisterName);
		model.addAttribute("assisterFirstName",assisterFirstName);
		model.addAttribute("assisterLastName",assisterLastName);
		model.addAttribute("helpingEntity",helpingEntityFlag);
		model.addAttribute("alCountryOfIssuances",platformGson.toJson(SsapUtility.simpleMapValueSort(countriesMap)));
		model.addAttribute("cmrHouseHold",platformGson.toJson(ssapPrimaryApplicantInfo));
		model.addAttribute("applicationSource",applicationSource);
		model.addAttribute("applicantCounter", getMaxpersonId(caseNumberInFlash));
		model.addAttribute("applicationType", applicationType);
		model.addAttribute("coverageYear", coverageYear);
		model.addAttribute("ssapId", ssapId);
		
		String stateExchangeCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		model.addAttribute("stateExchangeCode", stateExchangeCode);

		// TODO: Temp test for React UI, this will be referenced as bean later on.
		final GsonBuilder testBuilder = new GsonBuilder();
		testBuilder.registerTypeAdapterFactory(new IexEnumAdapterFactory());
		testBuilder.serializeNulls();
		testBuilder.serializeSpecialFloatingPointValues();

		if(ssapId == 0 && (ssapApplicationJson == null || ssapApplicationJson.trim().equals(""))) {
			try {
				testBuilder.setDateFormat("yyyy-MM-dd"); // 1980-04-25
				Gson testGson = testBuilder.create();
				ssapControllerLogger.info("ssapId == 0, building new SSAP Json stub");
				SingleStreamlinedApplication newApp = singleStreamlinedApplicationService.createNewSingleStreamlinedApplication(household);
				newApp.setSsapApplicationId("0");
				model.addAttribute("ssapApplicationJson", testGson.toJson(newApp));
			} catch (Exception e) {
				ssapControllerLogger.error("Exception occurred while building new SSAP JSON stub", e);
			}
		} else {
			testBuilder.setDateFormat("MMM dd, yyyy"); // 1980-04-25
			Gson testGson = testBuilder.create();
			LinkedHashMap m = testGson.fromJson(ssapApplicationJson, LinkedHashMap.class);
			model.addAttribute("ssapApplicationJson",testGson.toJson(m.get("singleStreamlinedApplication")));
		}

		//set pre-populattion data
		PreferencesDTO preferencesDTO= preferencesService.getPreferences(household.getId(), false);
		if(preferencesDTO !=null){
			model.addAttribute("writtenLang", StringUtils.isBlank(preferencesDTO.getPrefWrittenLang())?"English":preferencesDTO.getPrefWrittenLang());
			model.addAttribute("spokenLang", StringUtils.isBlank(preferencesDTO.getPrefSpokenLang())?"English":preferencesDTO.getPrefSpokenLang());
			model.addAttribute("prefCommunication", preferencesDTO.getPrefCommunication() ==null ?"Email":preferencesDTO.getPrefCommunication().getMethod());
		}
		model.addAttribute("mailingAddress", household.getPrefContactLocation());

		model.addAttribute("userEmailAddress", household.getEmail());
		// Get the QEP grace period + 60 days
		int enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (StringUtils.isNumeric(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
		model.addAttribute("qepAllowedInterval", enrollmentGracePeriod + 60);

		if(!"INDIVIDUAL".equalsIgnoreCase(userService.getLoggedInUser().getDefRole().getName()) && "N".equalsIgnoreCase(csrOverRide)) {
			model.addAttribute(SsapConstants.TRACK_APPLICATION_SOURCE,"Y");
		}
		else {
			model.addAttribute(SsapConstants.TRACK_APPLICATION_SOURCE,"N");
		}

		if(session.getAttribute(SsapConstants.CSR_OVER_RIDE) != null) {
			model.addAttribute(SsapConstants.CSR_OVER_RIDE,session.getAttribute(SsapConstants.CSR_OVER_RIDE).toString());
		}
		//request.getSession().setAttribute(SSAP_VISITED, "N");

		GiAuditParameterUtil.add(new GiAuditParameter("caseNumber", caseNumberInFlash));
		GiAuditParameterUtil.add(new GiAuditParameter("coverageYear", coverageYear));
		GiAuditParameterUtil.add(new GiAuditParameter("ssapID", ssapId));


		return "ssapmain";
	}

	@RequestMapping(value="/ssap", method = RequestMethod.GET)
	@GiAudit(transactionName = "Read default ssap-application data for creating new application.", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.NON_FINANCIAL_APP )
	@PreAuthorize(SsapConstants.SSAP_VIEW)
	public String startSSAP(@ModelAttribute("caseNumber") String caseNumberInFlash,
													@ModelAttribute("applicationType") String applicationType,
													@ModelAttribute("coverageYear") String coverageYear, Model model,
			HttpServletRequest request,HttpServletResponse response) throws InvalidUserException, GIException
	{
		// For Idaho use old page, because we don't want to give new REACT SSAP to Idaho for now (2019).
		switch (stateConfiguration.getStateCode()) {
			case "ID":
			case "CA":
				return startSSAPOld(caseNumberInFlash, applicationType, coverageYear, model, request, response);
			default:
				return startSSAPReact(caseNumberInFlash, applicationType, coverageYear, model, request, response);
		}
	}

	@RequestMapping(value="/lifeChangeEvent.nmhx", method = RequestMethod.GET)
	public @ResponseBody String lifeChangeEvent(){
		ssapControllerLogger.debug("inside lifeChangeEvent");
		Locale locale=LocaleContextHolder.getLocale();

		//ssapControllerLogger.debug("Locale display name:"+locale.getDisplayName());
		//ssapControllerLogger.debug("Locale display language:"+locale.getDisplayLanguage());
		return  "lifeChangeEvent";
	}
	private void logIndividualAppEvent(String eventName, String eventType,String applicationId){

		try {
			//event type and category
			AccountUser user=userService.getLoggedInUser();
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			EventInfoDto eventInfoDto = new EventInfoDto();

			Map<String, String> mapEventParam = new HashMap<>();
			mapEventParam.put(APPLICATION_ID,applicationId );
			mapEventParam.put(APPLICATION_TYPE,NON_FINANCIAL_APPLICATION );

			eventInfoDto.setModuleId(user.getActiveModuleId());
			eventInfoDto.setModuleName(user.getActiveModuleName().toUpperCase());
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);

		} catch(Exception e){
			ssapControllerLogger.error("Exception occured while log Application event"+e.getMessage(), e);
			throw new GIRuntimeException(e);
		}
	}
	@RequestMapping(value="/saveERT.nmhx", method = RequestMethod.POST)
	@PreAuthorize(SsapConstants.SSAP_EDIT)
	public @ResponseBody String saveERT(HttpServletRequest request, HttpServletResponse response) throws GIException, InvalidUserException{
		ssapControllerLogger.debug("inside saveERT");
		Map<String, Object> memberData = null;
		HttpSession session = request.getSession();
		long ssapID=0;
		String newJson = request.getParameter("webJson");
		String decodedNewwJson = newJson.replaceAll("&quot;","\"");
		String ipAddress = request.getRemoteAddr();
		String currentPage = request.getParameter("currentPage");
		String isMailingAddressUpdated = request.getParameter("isMailingAddressUpdated");
		String ssapApplicationLink = "";
		String applicantGUID = null, externalApplicantID = null;
		SingleStreamlinedApplication singleStreamlinedApplication1 = null;
		Map<String,SsapApplicantResource> applicantMap = null;
		Household household = null;
		household = consumerPortalUtil.getHouseholdRecord(userService.getLoggedInUser().getActiveModuleId());

		String sepEvent = "";
		String sepEventDate = "";
		String applicationType;

		if(request.getParameter("sepEvent") !=null) {
			sepEvent = request.getParameter("sepEvent");
		}
		 applicationType = request.getParameter("applicationType");

		 String coverageYearStr = request.getParameter("coverageYear");
		 Integer coverageYear = null;
		 if(coverageYearStr != null ) {
			 coverageYear = Integer.parseInt(coverageYearStr);
		 }

		if(request.getParameter("sepEventDate") !=null) {
			sepEventDate = request.getParameter("sepEventDate");
		}

		if(session.getAttribute(SSAP_APPLICATION_ID)!=null && StringUtils.isNumeric(session.getAttribute(SSAP_APPLICATION_ID).toString())){
			 ssapID = Integer.parseInt(session.getAttribute(SSAP_APPLICATION_ID).toString());
		}

		String csrOverRide = (session.getAttribute(SsapConstants.CSR_OVER_RIDE)==null)?"N":"Y";
		 try {
			 singleStreamlinedApplication1 = ssapJsonBuilder.transformFromJson(decodedNewwJson);

			 Validate.notNull(singleStreamlinedApplication1, "SingleStreamlinedApplication is missing.");

			 if(applicationType == null) {
				 applicationType = singleStreamlinedApplication1.getApplicationType();
			 }
			 String applicantGuidToBeRemoved = request.getParameter("applicantGuidToBeRemoved")!=null &&
			 				NumberUtils.isNumber(request.getParameter("applicantGuidToBeRemoved"))?request.getParameter("applicantGuidToBeRemoved"):"";

			 Integer personId = Integer.parseInt( request.getParameter("personId")==null?"0": request.getParameter("personId")) ;
			 if(personId > 1) {
				 deleteApplicantFromApplication(ssapID,singleStreamlinedApplication1,applicantGuidToBeRemoved, personId);
			 }
			 String ridpverified =household!=null && "Y".equalsIgnoreCase(household.getRidpVerified()) ? "Y":"N" ; // y or n
			 if(StringUtils.isNotEmpty(ridpverified) &&  ridpverified.equalsIgnoreCase("N") && "N".equalsIgnoreCase(csrOverRide)
					 && SIGN_AND_SAVE_PAGE_ID.equalsIgnoreCase(currentPage)) {
				 return "redirect:ridp/verification?caseNumber=" + singleStreamlinedApplication1.getApplicationGuid();
			 }
			//ssapControllerLogger.debug("SEcond OUTPUT>>>>>>>>>>>>>>>"+ssapJsonBuilder.transformToJson(singleStreamlinedApplication1));

		} catch (Exception e) {
			ssapControllerLogger.error("ERROR PARSING JSON >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage(), e);
			 throw new GIRuntimeException(e);
		}

		List<HouseholdMember> members = singleStreamlinedApplication1.getTaxHousehold().get(0).getHouseholdMember();

		for(int icount=0;icount<members.size();icount++){
			HouseholdMember householdMember = members.get(icount);
			if(householdMember!=null && (householdMember.getApplicantGuid()==null || StringUtils.isEmpty(householdMember.getApplicantGuid()))){
				memberData = new HashMap<String, Object>();

				if(null != householdMember.getName()) {
					memberData.put("firstName", householdMember.getName().getFirstName());
					memberData.put("lastName", householdMember.getName().getLastName());
				}

				memberData.put("birthDate", householdMember.getDateOfBirth());
				memberData.put("gender", householdMember.getGender());

				if(null != householdMember.getSocialSecurityCard()) {
					memberData.put("socialSecurityNumber", householdMember.getSocialSecurityCard().getSocialSecurityNumber());
				}

				Validate.notNull(household.getId());
				memberData.put("consumerId", household.getId());

				Map<String, String> applicantIDs = ssapUtil.retrieveApplicantIDs(memberData);

				if(null == applicantIDs || applicantIDs.isEmpty()) {
					householdMember.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAP_APPLICANT_DISPLAY_ID));
				}
				else {
					applicantGUID = applicantIDs.get("APPLICANT_GUID");

					if(StringUtils.isBlank(applicantGUID)) {
						householdMember.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAP_APPLICANT_DISPLAY_ID));
					}
					else {
						householdMember.setApplicantGuid(applicantGUID);
					}

					externalApplicantID = applicantIDs.get("EXTERNAL_APPLICANT_ID");
					householdMember.setExternalId(externalApplicantID);

				}
				//HIX-114162 Fix to set Applicant person type, which comes null in case of ID Non financial application
				if(householdMember.getApplicantPersonType()==null) {
					if(householdMember.getPersonId() == 1) {
						householdMember.setApplicantPersonType(SsapApplicantPersonType.PC_PTF);
					}else {
						householdMember.setApplicantPersonType(SsapApplicantPersonType.DEP);
					}
				}

			}
		}


    boolean anyoneAlaskanIndianOrNativeAmerican = false;

		if(members != null && !members.isEmpty()) {
		  anyoneAlaskanIndianOrNativeAmerican = members.stream().anyMatch(hhm -> hhm.getAmericanIndianAlaskaNative() != null && hhm.getAmericanIndianAlaskaNative().getMemberOfFederallyRecognizedTribeIndicator() == Boolean.TRUE);
    }

		Date date = new TSDate();

		 SsapApplication objssapApplication = new SsapApplication();
		 boolean isfinanceIndicator=singleStreamlinedApplication1.getApplyingForFinancialAssistanceIndicator()!=null?
				 singleStreamlinedApplication1.getApplyingForFinancialAssistanceIndicator():false ;
		 //objssapApplication.setApplicationType(SsapApplicationEventTypeEnum.QEP.name());
		 objssapApplication.setCurrentPageId(currentPage);
		 objssapApplication.setFinancialAssistanceFlag(isfinanceIndicator?"Y":"N");
		 try {
			if(ssapID==0){
				if(!isNewApplicationCreationApplicable(userService.getLoggedInUser().getActiveModuleId(), new Long(coverageYearStr))){
					 throw new GIRuntimeException("Active Application already available for the Household Id");
				}
				String guid = ssapUtil.getNextSequenceFromDB("SSAP_APPLICATION_DISPLAY_ID");//UUID.randomUUID().toString();
				 objssapApplication.setApplicationStatus(ApplicationStatus.OPEN.getApplicationStatusCode());
				 objssapApplication.setApplicationType(applicationType);
				 objssapApplication.setCaseNumber(guid);
				 objssapApplication.setCreationTimestamp(new Timestamp(date.getTime()));
				 objssapApplication.setLastUpdateTimestamp(new Timestamp(date.getTime()));
				 objssapApplication.setStartDate(new Timestamp(date.getTime()));
				 objssapApplication.setEligibilityStatus(EligibilityStatus.PE.toString());
				 objssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE.toString());
				 objssapApplication.setCreatedBy(new BigDecimal(userService.getLoggedInUser().getId()));
				 objssapApplication.setLastUpdatedBy(new BigDecimal(userService.getLoggedInUser().getId()));
				 objssapApplication.setSource(ApplicationSource.ONLINE_APPLICATION.getApplicationSourceCode());
				 objssapApplication.setCoverageYear(coverageYear);
				try {
					objssapApplication.setCmrHouseoldId(userService.getLoggedInUser().getActiveModuleId()!=0?new BigDecimal(userService.getLoggedInUser().getActiveModuleId()):null);
				} catch (InvalidUserException e1) {
					ssapControllerLogger.error("ERROR SETTING CMR_HOUSEHOLD_ID>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e1.getMessage(), e1);
					throw new GIRuntimeException(e1);
				}
				singleStreamlinedApplication1.setApplicationGuid(guid);
				singleStreamlinedApplication1.setSsapApplicationId(String.valueOf(ssapID));
				singleStreamlinedApplication1.setClientIp(ipAddress);
				decodedNewwJson = ssapJsonBuilder.transformToJson(singleStreamlinedApplication1);
				objssapApplication.setApplicationData(decodedNewwJson);

				objssapApplication.setNativeAmerican(anyoneAlaskanIndianOrNativeAmerican ? "Y" : "N");
				ssapApplicationLink = resourceCreatorImpl.saveSsapApplication(objssapApplication);

				if(!"".equals(ssapApplicationLink)) {
					ssapID = Util.getNumerIDAtEndOfLink(ssapApplicationLink);
				}

				request.getSession().setAttribute(CASE_NUMBER_IN_SESSION, guid); // required for refreshing the page by F5
				session.setAttribute(SSAP_APPLICATION_ID,ssapID);
				ssapControllerLogger.debug("Data Inserted : SSAP_APPLICATION : ");
				logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_CREATED,ssapID+"");
			}else{
				SsapApplicationResource ssapApplicationResource = getApplicationByCaseNumber(singleStreamlinedApplication1.getApplicationGuid());
				if(userService.getLoggedInUser().getActiveModuleId() != ssapApplicationResource.getCmrHouseoldId().intValueExact()) {
						ssapControllerLogger.error("Permission denied to save the application with caseNumber:: ");
						throw new GIRuntimeException("Permission denied to save the application");
				}
				if(csrOverRide != null && "Y".equalsIgnoreCase(csrOverRide)){
					if(SsapApplicationEventTypeEnum.SEP.name().equalsIgnoreCase(ssapApplicationResource.getApplicationType())){
						ssapControllerLogger.error("Permission denied to edit SEP application with the caseNumber:: ");
						throw new GIRuntimeException("SEP application cannot be edited by CSR");
					}

					if(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplicationResource.getApplicationStatus())) {
						ssapControllerLogger.error("Permission denied to edit EN status application with the caseNumber:: ");
						throw new GIRuntimeException("application in EN status cannot be edited by CSR");
					}
				}

				applicantMap = resourceCreatorImpl.getApplicantsWithGuidAsKey(ssapID);
				singleStreamlinedApplication1.setSsapApplicationId(String.valueOf(ssapID));
				decodedNewwJson = ssapJsonBuilder.transformToJson(singleStreamlinedApplication1);
				if(csrOverRide ==null || "N".equalsIgnoreCase(csrOverRide)){
					//objssapApplication.setApplicationStatus(Util.deriveSsapApplicationStatus(currentPage,applicantMap));
					objssapApplication.setApplicationStatus(ssapApplicationResource.getApplicationStatus());
				}
				else { // set the same status while csr overrides
					objssapApplication.setApplicationStatus(ssapApplicationResource.getApplicationStatus());
				}
				 objssapApplication.setCreationTimestamp(ssapApplicationResource.getCreationTimestamp());
				 objssapApplication.setStartDate(ssapApplicationResource.getStartDate());
				 objssapApplication.setEligibilityStatus(ssapApplicationResource.getEligibilityStatus());
				 objssapApplication.setExchangeEligibilityStatus(ssapApplicationResource.getExchangeEligibilityStatus());
				 objssapApplication.setCmrHouseoldId(ssapApplicationResource.getCmrHouseoldId());
				 objssapApplication.setCaseNumber(singleStreamlinedApplication1.getApplicationGuid());
				 objssapApplication.setApplicationData(decodedNewwJson);
				 objssapApplication.setCurrentPageId(currentPage);
				 objssapApplication.setEligibilityReceivedDate(ssapApplicationResource.getEligibilityReceivedDate());
				 objssapApplication.setEhbAmount(ssapApplicationResource.getEhbAmount());
				 objssapApplication.setCsrLevel(ssapApplicationResource.getCsrLevel());
				 objssapApplication.setNativeAmerican(anyoneAlaskanIndianOrNativeAmerican ? "Y" : "N");
				 objssapApplication.setMaximumAPTC(ssapApplicationResource.getMaximumAPTC());
				 objssapApplication.setElectedAPTC(ssapApplicationResource.getElectedAPTC());
				 objssapApplication.setAvailableAPTC(ssapApplicationResource.getAvailableAPTC());
				 objssapApplication.setLastUpdateTimestamp(new Timestamp(date.getTime()));
				 objssapApplication.setCreatedBy(ssapApplicationResource.getCreatedBy());
				 objssapApplication.setLastUpdatedBy(new BigDecimal(userService.getLoggedInUser().getId()));
				 objssapApplication.setAllowEnrollment(ssapApplicationResource.getAllowEnrollment());

				 objssapApplication.setEligibilityResponseType(ssapApplicationResource.getEligibilityResponseType());
				 objssapApplication.setExternalApplicationId(ssapApplicationResource.getExternalApplicationId());
				 objssapApplication.setExemptHouseHold(ssapApplicationResource.getExemptHouseHold());

				 if(objssapApplication.getCoverageYear() == 0) {
					 objssapApplication.setCoverageYear((int)ssapApplicationResource.getCoverageYear());
				 }

				 objssapApplication.setSource(ssapApplicationResource.getSource());
				 objssapApplication.setApplicationType(ssapApplicationResource.getApplicationType());

				if(SIGN_AND_SAVE_PAGE_ID.equalsIgnoreCase(currentPage)){
					Map<String, String>primaryApplicantName = getPrimaryApplicantName(members);
					objssapApplication.setEsignFirstName(primaryApplicantName.get(SsapConstants.PRIMARY_APPLICANT_FIRST_NAME)==null?
							"": primaryApplicantName.get(SsapConstants.PRIMARY_APPLICANT_FIRST_NAME));
					objssapApplication.setEsignLastName(primaryApplicantName.get(SsapConstants.PRIMARY_APPLICANT_LAST_NAME)==null?
							"": primaryApplicantName.get(SsapConstants.PRIMARY_APPLICANT_LAST_NAME));
					objssapApplication.setEsignDate(new Timestamp(date.getTime()));

					 objssapApplication.setSource(getApplicationSource(request, ssapApplicationResource));

					//request.getSession().removeAttribute(SsapConstants.CSR_OVER_RIDE);
				}

				if(csrOverRide != null && "Y".equalsIgnoreCase(csrOverRide)) {
					ssapApplicationLink = resourceCreatorImpl.updateSsapApplication(objssapApplication, ssapID);
				}
				else if(ApplicationStatus.OPEN.getApplicationStatusCode().equalsIgnoreCase(ssapApplicationResource.getApplicationStatus())){
					ssapApplicationLink = resourceCreatorImpl.updateSsapApplication(objssapApplication, ssapID);
				}
				else{

					if(SIGN_AND_SAVE_PAGE_ID.equalsIgnoreCase(currentPage)){
						request.getSession().removeAttribute(SsapConstants.CSR_OVER_RIDE);
					}
					ssapControllerLogger.error("The application is already Esigned and cannot be edited again");
					return "500:ESIGNED_APPLICATION";
				}
				if(SIGN_AND_SAVE_PAGE_ID.equalsIgnoreCase(currentPage)){
					request.getSession().removeAttribute(SsapConstants.CSR_OVER_RIDE);
				}
			}

			//MEC Check for financial application
			if(isfinanceIndicator && FAMILY_HOUSEHOLD_SECTION_LAST_PAGE.equalsIgnoreCase(currentPage)){
				ssapControllerLogger.debug("Triggering the MEC Check Flow ");
				String mecCheckStatus = mecCheckIntegration(singleStreamlinedApplication1.getApplicationGuid());
				ssapControllerLogger.debug("MEC Check Status is "+mecCheckStatus);
				ssapControllerLogger.debug("MEC Check Flow triggered successfully");
				singleStreamlinedApplication1.setMecCheckStatus(mecCheckStatus);
			}

			List<BloodRelationship> bloodRelationship = ssapUtil.getBloodRelationship(members);

			boolean isUpdateApplicant = false;
			Location mailingLocation = saveMailingLocation(members, applicantMap);
			Location homeLocation = savePrimaryLocation(members, applicantMap);
			String responsibleMemberPersonId = ssapUtil.getPrimaryTaxFilerPersonIdFromHouseholdMembers(members);
			
			for(int icount=0;icount<members.size();icount++){
				HouseholdMember householdMember = members.get(icount);
				long newPersonId = householdMember.getPersonId();
				SsapApplicantResource ssapApplicant;
				if(applicantMap !=null && applicantMap.containsKey(householdMember.getApplicantGuid())){
					 ssapApplicant = applicantMap.get(householdMember.getApplicantGuid());
					 isUpdateApplicant = true;
				}else{
					 ssapApplicant = new SsapApplicantResource();
					 ssapApplicant.setPersonId(new Long(newPersonId));
					 ssapApplicant.setIncomeVerificationStatus(null);//SsapEnums.ApplicantVerificationStatusEnum.NOT_VERIFIED.getValue()
					 ssapApplicant.setMecVerificationStatus(null);
					 ssapApplicant.setSsnVerificationStatus(null);
					 ssapApplicant.setVlpVerificationStatus(null);
					 ssapApplicant.setCreationTimestamp(new Timestamp(date.getTime()));
					 //ssapApplicant.setApplicantGuid(ssapUtil.getNextSequenceFromDB("SSAP_APPLICANT_DISPLAY_ID"));
					 isUpdateApplicant = false;
				 }

				if(bloodRelationship!=null && bloodRelationship.size()>0){
					ssapApplicant.setRelationship(ssapUtil.getApplicantRelation(bloodRelationship, ssapApplicant.getPersonId(),responsibleMemberPersonId));
				}

				 ssapApplicant.setSsapApplication(ssapApplicationLink);
				 ssapApplicant.setFirstName(householdMember.getName().getFirstName());
				 ssapApplicant.setMiddleName(householdMember.getName().getMiddleName());
				 ssapApplicant.setLastName(householdMember.getName().getLastName());
				 ssapApplicant.setGender(householdMember.getGender());
				 ssapApplicant.setMarried(householdMember.getMarriedIndicator()!=null && householdMember.getMarriedIndicator()?"true":"false");
				 ssapApplicant.setApplyingForCoverage(householdMember.getApplyingForCoverageIndicator()?"Y":"N");
				 ssapApplicant.setHouseholdContactFlag(householdMember.getHouseholdContactIndicator()?"Y":"N");
				 ssapApplicant.setSsn(householdMember.getSocialSecurityCard().getSocialSecurityNumber());
				 ssapApplicant.setMobilePhoneNumber(members.get(0).getHouseholdContact().getPhone().getPhoneNumber());
				 ssapApplicant.setPhoneNumber(members.get(0).getHouseholdContact().getOtherPhone().getPhoneNumber());
				 ssapApplicant.setEmailAddress(members.get(0).getHouseholdContact().getContactPreferences().getEmailAddress());
				 ssapApplicant.setBirthDate(householdMember.getDateOfBirth());
				 ssapApplicant.setApplicantGuid(householdMember.getApplicantGuid());
				 ssapApplicant.setExternalApplicantId(householdMember.getExternalId());
				 ssapApplicant.setLastUpdateTimestamp(new Timestamp(date.getTime()));
				 ssapApplicant.setMailiingLocationId(new BigDecimal(mailingLocation.getId()));
				 ssapApplicant.setOtherLocationId(new BigDecimal(homeLocation.getId()));
				 ssapApplicant.setNativeAmericanFlag(householdMember.getSpecialCircumstances().getAmericanIndianAlaskaNativeIndicator()!=null && householdMember.getSpecialCircumstances().getAmericanIndianAlaskaNativeIndicator()?"Y":"N");
				 ssapApplicant.setPersonType(householdMember.getApplicantPersonType().getPersonType());
				 if(isUpdateApplicant){
					 resourceCreatorImpl.updateSsapApplicant(ssapApplicant);
				 }else{
					 resourceCreatorImpl.createSsapApplicant(ssapApplicant, ssapApplicationLink);
				 }
			}
			//create application event and applicant events after Esign
			if(SIGN_AND_SAVE_PAGE_ID.equalsIgnoreCase(currentPage)){
				//Added below validation code for HIX-67907
				 Validator validator =  Validation.buildDefaultValidatorFactory().getValidator();
				 Set<ConstraintViolation<SingleStreamlinedApplication>> constraintVoilation =  validator.validate(singleStreamlinedApplication1);
				 if(!constraintVoilation.isEmpty()){
					 /*
					GIRuntimeException giRuntimeException = new GIRuntimeException();
					giRuntimeException.setComponent(Component.LCE.toString());
					giRuntimeException.setNestedStackTrace(constraintVoilation.toString());
					giRuntimeException.setErrorMsg(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					throw giRuntimeException;
					*/
				 }
				//update household
				updatePreferences(isMailingAddressUpdated, household, mailingLocation,members.get(0).getHouseholdContact().getContactPreferences(),ssapID);
			}

		} catch (JsonGenerationException e) {
			ssapControllerLogger.error("Json generation Exception occured while saving", e);
			throw new GIRuntimeException(e.getMessage());
		} catch (JsonMappingException e) {
			ssapControllerLogger.error("JSON Mapping Exception occured while saving", e);
			throw new GIRuntimeException(e.getMessage());
		} catch (IOException e) {
			ssapControllerLogger.error("IO Exception occured while saving", e);
			throw new GIRuntimeException(e.getMessage());
		} catch (Exception e) {
			ssapControllerLogger.error("Exception occured while saving", e);
			throw new GIRuntimeException(e.getMessage());
		}
		try{
			if(SIGN_AND_SAVE_PAGE_ID.equalsIgnoreCase(currentPage)){
					triggerSSAPIntegration(ssapID, sepEvent, sepEventDate);
			}

		}catch(Exception e){
			ssapControllerLogger.error("FAILED TO PUSH IN QUEUE WITH ERROR : "+e.getMessage(),e);
			//throw new GIRuntimeException(e.getMessage());
		}
		ssapControllerLogger.info("application saved successfully with applicationId:");
		return decodedNewwJson;
	}
	private void updatePreferences(String isMailingAddressUpdated, Household household, Location mailingLocation,ContactPreferences contactPreferences,long ssapId) throws InvalidUserException {
		PreferencesDTO preferencesDTO= preferencesService.getPreferences(household.getId(), false);
		SsapPreferencesDTO ssapPreferencesDTO = new SsapPreferencesDTO();
		ssapPreferencesDTO.setHouseholdId(household.getId());
		ssapPreferencesDTO.setUpdatedBy(userService.getLoggedInUser().getId());
		if(preferencesDTO.getPrefCommunication() == null || StringUtils.isBlank(preferencesDTO.getPrefSpokenLang()) || StringUtils.isBlank(preferencesDTO.getPrefWrittenLang())){
			if(contactPreferences.getPreferredContactMethod()!=null && contactPreferences.getPreferredContactMethod().equals("POSTAL MAIL")){
				ssapPreferencesDTO.setPreferedContactMethod("Mail");
			}else if(contactPreferences.getPreferredContactMethod()!=null && contactPreferences.getPreferredContactMethod().equals("EMAIL")){
				ssapPreferencesDTO.setPreferedContactMethod("Email");
			}else{
				ssapPreferencesDTO.setPreferedContactMethod(contactPreferences.getPreferredContactMethod());
			}
			ssapPreferencesDTO.setPreferedWrittenLanguages(contactPreferences.getPreferredWrittenLanguage());
			ssapPreferencesDTO.setPreferedSpokenLangages(contactPreferences.getPreferredSpokenLanguage());
			ssapPreferencesDTO.setUpdatePreferences(true);
		}
		
		if(isMailingAddressUpdated != null && isMailingAddressUpdated.equalsIgnoreCase("yes")){
			ssapPreferencesDTO.setPrefContactLocation(mailingLocation);
			ssapPreferencesDTO.setUpdateMailingAddress(true);
			ssapPreferencesDTO.setTriggerMailingLocationApi(true);
		}
		
		if(ssapPreferencesDTO.isUpdateMailingAddress() || ssapPreferencesDTO.isUpdatePreferences()){
			ssapPreferencesDTO.setUserName(userService.getLoggedInUser().getUsername()); 
			String result = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+UPDATE_PREFERENCES, ssapPreferencesDTO, String.class);
			if(!result.equals(GhixConstants.RESPONSE_SUCCESS)){
				ssapControllerLogger.error("Error occured while updating mailing address for household:"+household.getId());
				throw new GIRuntimeException("Error occured while updating mailing address for household:"+household.getId());
			}
			if(ssapPreferencesDTO.isUpdateMailingAddress()){
				logIndividualAppEvent(APPEVENT_PREFERENCES, CONTACT_INFORMATION_UPDATE, String.valueOf(ssapId));
			}
			
		}
	}



	private void deleteApplicantFromApplication(long ssapID,
			SingleStreamlinedApplication singleStreamlinedApplication1,
			String applicantGuidToBeRemoved, int personId) {

		if(!canProcessDeleteApplicant(singleStreamlinedApplication1)) {
			ssapControllerLogger.error("Unable to process the deletion of the applicant");
			throw new GIRuntimeException("Unable to process the deletion of the applicant");
		}
		List<HouseholdMember> members = singleStreamlinedApplication1.getTaxHousehold().get(0).getHouseholdMember();
		if(!StringUtils.isEmpty(applicantGuidToBeRemoved)){
			SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
		    ssapApplicationRequest.setApplicantGuidToRemove(applicantGuidToBeRemoved);
		    ssapApplicationRequest.setSsapApplicationId(ssapID);
		    boolean result = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+ "application/deleteSsapApplicationApplicant",ssapApplicationRequest, Boolean.class);
		    if(!result){
		          throw new GIRuntimeException("Error Deleting the Applicant");
		    }
		    Iterator<HouseholdMember> it = members.iterator();
		    while(it.hasNext()){
		          HouseholdMember householdMember = it.next();
		          if(householdMember.getApplicantGuid().equalsIgnoreCase(applicantGuidToBeRemoved)){
		        	  if(householdMember.getPersonId() > 1) {
		                it.remove();
		        	  }
		        	  else {
		        		  ssapControllerLogger.error("The primary applicant cannot be deleted");
		      			  throw new GIRuntimeException("The primary applicant cannot be deleted");
		        	  }
		          }
		    }
		}
		else {
			// The applicant is not saved in the database at all. just clean the JSON
			Iterator<HouseholdMember> it = members.iterator();
			while(it.hasNext()){
		          HouseholdMember householdMember = it.next();
		          if(householdMember.getPersonId()== personId){
		        	  if(householdMember.getPersonId() > 1) {
		                it.remove();
		        	  }
		        	  else {
		        		  ssapControllerLogger.error("The primary applicant cannot be deleted");
		      			  throw new GIRuntimeException("The primary applicant cannot be deleted");
		        	  }
		          }
		    }
		}
		//removeRelationship(personId);
		//correctIncarcerationIndicator(personId);
		List<HouseholdMember> list =  singleStreamlinedApplication1.getTaxHousehold().get(0).getHouseholdMember();
		HouseholdMember objHouseholdMember = list.get(0);
		List<BloodRelationship> listBloodRelationship = objHouseholdMember.getBloodRelationship();
		Iterator<BloodRelationship> it = listBloodRelationship.iterator();
		while(it.hasNext()){
			BloodRelationship objBloodRelationship = it.next();
			if(objBloodRelationship.getIndividualPersonId() != null || objBloodRelationship.getRelatedPersonId()!=null){
				if ( Integer.parseInt(objBloodRelationship.getIndividualPersonId()) == personId ||  Integer.parseInt(objBloodRelationship.getRelatedPersonId()) == personId ){
		    		it.remove();
		    	}
			}
		}
		singleStreamlinedApplication1.setIncarcerationAsAttestedIndicator(false);
		for(HouseholdMember objHouseholdMember1 : list){
			if(objHouseholdMember1.getIncarcerationStatus().getIncarcerationStatusIndicator()){
				singleStreamlinedApplication1.setIncarcerationAsAttestedIndicator(true);
				break;
			}
		}
		singleStreamlinedApplication1.getTaxHousehold().get(0).setHouseholdMember(members);
	}


	private void triggerSSAPIntegration(long applicationId, String sepEvent, String sepEventDate) {
		try {
			final SingleStreamlinedApplication application = singleStreamlinedApplicationService.getApplicationByApplicationId(applicationId);
			ssapControllerLogger.info("Invoking new eligibility engine for applicationId: {}, sepEvent: {}, sepEventDate: {}", application.getSsapApplicationId(), sepEvent, sepEventDate);
			EligibilityResponse eligibilityResponse = eligibilityEngineIntegrationService.invokeEligibilityEngine(application, sepEvent, sepEventDate, RunMode.ONLINE);

			if(eligibilityResponse.getStatus() == ResponseStatus.success) {
				if(! DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase("ID")){
			  ssapControllerLogger.info("Eligibility responded with successful status, calling IFSV service");
			  
			  IfsvResponse ifsvResponse = ifsvService.invoke(applicationId);
			  ssapControllerLogger.info("IFSV Response: {}", ifsvResponse);
				}
      }
		} catch (Exception e) {
			ssapControllerLogger.error("Unable to retrieve application by given applicationId: {}", applicationId);
			ssapControllerLogger.error("Exception while retrieving application by applicationId", e);
		}

    try {
      final String integrationResponse = restTemplate.postForObject(GhixEndPoints.SsapIntegrationEndpoints.SSAP_INTEGRATION_URL,
        applicationId, String.class);
      ssapControllerLogger.info("SSAP Integration response: {}", integrationResponse);
    } catch (Exception e) {
      ssapControllerLogger.error("Error while triggering SSAP integration: {}", e.getMessage());
      ssapControllerLogger.debug("Exception details", e);
    }
	}

	private String mecCheckIntegration(String caseNumber) {
		try {


		} catch (Exception ex) {
			ssapControllerLogger.error("Error invoking the SSAP orchestration flow", ex);
		}
		return "N";
	}

	@RequestMapping(value="/saveData", method = RequestMethod.GET)
	@GiAudit(transactionName = "Save modified individuals ssap-application data.", eventType = EventTypeEnum.MODIFY_PII_DATA, eventName = EventNameEnum.NON_FINANCIAL_APP )
	public @ResponseBody JsonResponse saveData(HttpServletRequest request, HttpServletResponse response) throws GIException{
		ssapControllerLogger.debug("inside saveERT");
		JsonResponse jsonResponse = new JsonResponse();
		 SsapApplication objssapApplication = new SsapApplication();

		 try {
			resourceCreatorImpl.updateSsapApplication(objssapApplication, 0);
		} catch (Exception e) {
			ssapControllerLogger.error("Error saving Application in /saveData>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage(), e);
			throw new GIRuntimeException(e);
		}
		return  jsonResponse;
	}

	public static SingleStreamlinedApplication parseJSON(String jsonString){
		Gson gson = new Gson();
		JsonParser jsonParser = new JsonParser();
		SingleStreamlinedApplication singleStreamlinedApplication = null;
		try {
			JsonObject jsonObject = jsonParser.parse(jsonString).getAsJsonObject();
			//ssapControllerLogger.debug("jsonString:::::::::::" + jsonString);
			singleStreamlinedApplication = gson.fromJson(jsonObject.get(SINGLE_STREAMLINED_APPLICATION), SingleStreamlinedApplication.class);
		} catch(Exception e){
			ssapControllerLogger.error("Error in parseJSON ", e);
		}
		return singleStreamlinedApplication;
	}

	@RequestMapping(value="ssap/ssapverification", method = RequestMethod.GET)
	@GiAudit(transactionName = "Access application data for Verification.", eventType = EventTypeEnum.MODIFY_PII_DATA, eventName = EventNameEnum.NON_FINANCIAL_APP )
	public String verification(Model model, HttpServletRequest request,HttpServletResponse response) throws GIException{
		if(isRidpVerified(request)) {
			long ssapID=0;
			if(request.getSession().getAttribute(SSAP_APPLICATION_ID)!=null && StringUtils.isNumeric(request.getSession().getAttribute(SSAP_APPLICATION_ID).toString())){
				 ssapID = Integer.parseInt(request.getSession().getAttribute(SSAP_APPLICATION_ID).toString());
			}
			Date date = new TSDate();
			SsapApplicationResource ssapApplicationResource = null;
			SsapApplication objssapApplication = new SsapApplication();
			try {
				long cmrHouseHoldID = new Long(userService.getLoggedInUser().getActiveModuleId());
				ssapApplicationResource = resourceCreatorImpl.getSsapApplicationByCmrHouseoldId(cmrHouseHoldID);
				ssapApplicationResource.setCurrentPageId("appscr83");
				objssapApplication.setApplicationData(ssapApplicationResource.getApplicationData());
				objssapApplication.setApplicationStatus(ssapApplicationResource.getApplicationStatus());
				objssapApplication.setSource(ApplicationSource.ONLINE_APPLICATION.getApplicationSourceCode());
				objssapApplication.setEligibilityStatus(EligibilityStatus.PE.toString());
				objssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE.toString());
				objssapApplication.setApplicationType(ssapApplicationResource.getApplicationType());
				objssapApplication.setLastUpdateTimestamp(new Timestamp(date.getTime()));
				objssapApplication.setCreationTimestamp(new Timestamp(date.getTime()));
				objssapApplication.setEsignDate(new Timestamp(date.getTime()));
				 //objssapApplication.setEsignFirstName(ssa.getTaxHousehold().get(0).getHouseholdMember().get(0).getName().getFirstName());
				 //objssapApplication.setEsignLastName(ssa.getTaxHousehold().get(0).getHouseholdMember().get(0).getName().getLastName());
				objssapApplication.setEsignFirstName("TEST");
				objssapApplication.setEsignLastName("TEST");
				objssapApplication.setCurrentPageId("appscr83");
				objssapApplication.setCmrHouseoldId(userService.getLoggedInUser().getActiveModuleId()!=0?new BigDecimal(userService.getLoggedInUser().getActiveModuleId()):null);
				resourceCreatorImpl.updateSsapApplication(objssapApplication, ssapID);
			} catch (Exception e) {
				ssapControllerLogger.error("ERROR IN ssap/ssapverification>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage(), e);
				throw new GIRuntimeException(e);
			}
			return "redirect:/ssapverificationstatus";
		}
		else {
			return "verification";
		}
	}

	@RequestMapping(value="/ssapverificationstatus", method = RequestMethod.GET)
	@GiAudit(transactionName = "Get ssap-application data for verification.", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.NON_FINANCIAL_APP )
	public String getVerificationPage(Model model, HttpServletRequest request,HttpServletResponse response) throws GIException {
		try {
			Long ssapApplicationId = Long.parseLong(request.getSession().getAttribute(SSAP_APPLICATION_ID).toString());
			List<SsapApplicantResource> applicants = new ArrayList<SsapApplicantResource>(resourceCreatorImpl.getApplicants(ssapApplicationId).values());
			model.addAttribute(SSAP_APPLICANTS, applicants);
			return "ssapverificationstatus";
		}
		catch (Exception e) {
			ssapControllerLogger.error("Error getting the applciants",e);
			throw new GIException("Error getting the applicants", e);
		}
	}

	@RequestMapping(value="/ssap/countycodedetails", method = RequestMethod.POST)
	@GiAudit(transactionName = "Get county code from sate name and county name.", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.NON_FINANCIAL_APP )
	@PreAuthorize(SsapConstants.SSAP_COUNTY_VIEW)
	@ResponseBody
	public String getCountyCodeFromStateCodeAndCountyName(@RequestParam String state, @RequestParam String county,  HttpServletRequest request, HttpServletResponse response) throws GIException {
		try {
			return zipCodeService.getCountyCodeByStateAndCounty(state, county);
		}
		catch (Exception e) {
			ssapControllerLogger.error("Error getting the countycodedetails",e);
			throw new GIException("Error getting the countycodedetails", e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<SsapApplicantResource> getVerificationResults() throws GIException {
		try {
			long cmrHouseHoldID = new Long(userService.getLoggedInUser().getActiveModuleId());
			SsapApplicationResource ssapApplicationResource = resourceCreatorImpl.getSsapApplicationByCmrHouseoldId(cmrHouseHoldID);
			Object links = ssapApplicationResource.get_links();
			JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
			Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
			String selfHref= (self.get("href")==null)?"": self.get("href");
			URI myUri = null;
			if("".equals(selfHref)) {
				throw new GIRuntimeException("The end point to update the applicant is incorrect or not defined");
			}
			else {
				myUri = URI.create(selfHref);
			}
			long ssapID = Long.parseLong(myUri.getPath().replace(GhixEndPoints.SsapEndPoints.RELATTIVE_SSAP_APPLICATION_DATA_URL, ""));
			return  new ArrayList<SsapApplicantResource>(resourceCreatorImpl.getApplicants(ssapID).values());
		}
		catch (Exception e) {
			ssapControllerLogger.error("Error getting the applciants",e);
			throw new GIRuntimeException("Error getting the applicants", e);
		}
	}

	@RequestMapping(value = "/ssap/addCounties",  method = RequestMethod.POST)
	@GiAudit(transactionName = "Add county.", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.NON_FINANCIAL_APP )
	@PreAuthorize(SsapConstants.SSAP_COUNTY_VIEW)
	@ResponseBody
	public String getCountyList(@RequestParam String zipCode,  HttpServletRequest request, HttpServletResponse response) {
		//ssapControllerLogger.debug("The Zip Code is: " + zipCode);

		String stateExchangeCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		//ssapControllerLogger.debug("stateExchangeCode: " + stateExchangeCode);
		List <ZipCode> zipcodes = new ArrayList<ZipCode>();
		Map<String,String> countyMap =  new HashMap<String, String>();

		zipcodes = zipCodeService.getCountyListForZipAndState(zipCode, stateExchangeCode);
		//ssapControllerLogger.debug("zipcode size ====" +zipcodes.size());
		for (ZipCode zipCodeObj : zipcodes) {
			if(StringUtils.isNotEmpty(zipCodeObj.getCounty())){
				countyMap.put(zipCodeObj.getCounty(), zipCodeObj.getCountyCode());
			}
		}
		ssapControllerLogger.debug(" county found");
		response.setContentType("application/json");
		return platformGson.toJson(countyMap);
	}

	@RequestMapping(value = "/ssap/addCountiesForStates",  method = RequestMethod.POST)
	@GiAudit(transactionName = "Add county for states.", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.NON_FINANCIAL_APP )
    @ResponseBody
	public String getCountyListForStates(@RequestParam String zipCode, @RequestParam String stateCode, HttpServletRequest request, HttpServletResponse response) {
		//ssapControllerLogger.debug("The Zip Code is: " + zipCode);

		String stateExchangeCode = stateCode;
		//ssapControllerLogger.debug("stateExchangeCode: " + stateExchangeCode);
		 
		List <ZipCode> zipcodes = new ArrayList<ZipCode>();
		Map<String,String> countyMap =  new HashMap<String, String>();

		zipcodes = zipCodeService.getCountyListForZipAndState(zipCode, stateExchangeCode);
		//ssapControllerLogger.debug("zipcode size ====" +zipcodes.size());
		for (ZipCode zipCodeObj : zipcodes) {
			if(StringUtils.isNotEmpty(zipCodeObj.getCounty())){
				countyMap.put(zipCodeObj.getCounty(), zipCodeObj.getCountyCode());
			}
		}
		ssapControllerLogger.debug(" county found");
		response.setContentType("application/json");
		return platformGson.toJson(countyMap);
	}


	@RequestMapping(value = "/ssap/getAlaskaAmericanStatesTribe",  method = RequestMethod.POST)
	@PreAuthorize(SsapConstants.SSAP_TRIBE_VIEW)
	@GiAudit(transactionName = "Get tribe status data.", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.NON_FINANCIAL_APP )
	@ResponseBody
	public String getAlaskaAmericanStatesTribe(@RequestParam String stateCode,  HttpServletRequest request, HttpServletResponse response) {
		//ssapControllerLogger.debug("The State Name for AlaskaAmericanTribes is: " + stateCode);

		 
		List <AmericanAlaskaTribe> tribeNames = new ArrayList<AmericanAlaskaTribe>();
		tribeNames = SsapUtility.getAmericanAlaskaStateTribeName(stateCode);
		Map<String,String> tribeMap =  new HashMap<String, String>();
		for (AmericanAlaskaTribe americanAlaskaTribe : tribeNames) {
			if(StringUtils.isNotEmpty(americanAlaskaTribe.getTribeCode())){
				tribeMap.put(americanAlaskaTribe.getTribeCode(), americanAlaskaTribe.getTribeName());
			}
		}
		response.setContentType("application/json");
		return platformGson.toJson(tribeMap);
	}

	@RequestMapping(value = "/ssap/isRidpVerified",  method = RequestMethod.GET)
	@PreAuthorize(SsapConstants.SSAP_RIDP_VIEW)
	@GiAudit(transactionName = "Access application data for Verification.", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.NON_FINANCIAL_APP )
	@ResponseBody
	public String isRidpVerified(HttpServletRequest request, HttpServletResponse response) {
		return String.valueOf(isRidpVerified(request)) ;
	}

	public boolean isRidpVerified(HttpServletRequest request) {
		boolean isRidpVerified = false;
		try {
			if(request.getSession().getAttribute(SsapConstants.CSR_OVER_RIDE) != null && "Y".equals(request.getSession().getAttribute(SsapConstants.CSR_OVER_RIDE).toString())) {
				isRidpVerified = true;
			}
			else {
			    isRidpVerified = !ridpService.isRidpRequired();
			}
		}
		catch (Exception e) {
			ssapControllerLogger.error("RIDP psrolem", e);
		}
		return isRidpVerified;
	}

	/** This is called for CSR overrides
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/editssap",  method = RequestMethod.GET)
	@PreAuthorize(IndividualPortalConstants.PORTAL_EDIT_APP_PERMISSION)
	public String getApplication(@ModelAttribute("caseNumber") String caseNumberInFlash, Model model, HttpServletRequest request) throws GIException {
		//String caseNumber = request.getParameter("caseNumber")==null?"":request.getParameter("caseNumber").toString();
		if("".equals(caseNumberInFlash)) {
			ssapControllerLogger.error("Error in CSR override:caseNumber cannot be empty");
			throw new GIException("Error in CSR override:caseNumber cannot be empty");
		}
		request.getSession().setAttribute(SsapConstants.CSR_OVER_RIDE, "Y");
		//request.getSession().setAttribute("caseNumberInSession", null);
		//request.getSession().setAttribute("caseNumberInSession", caseNumber);
		return "redirect:/ssap";
	}

	/** The session is renewed every 5 minutes
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/ssap/ping",  method = RequestMethod.GET)
	@PreAuthorize(SsapConstants.SSAP_SESSION_EDIT)
	@ResponseBody
	public String sessionPing(HttpServletRequest request, HttpServletResponse response) {
		ssapControllerLogger.debug("Session extended");
		return "" ;
	}

	/**
	 * This method is for previewing the application while working on a ticket related to the
	 * document verification
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws GIException
	 */
	@RequestMapping(value = "/ssaptkmpreview",  method = RequestMethod.GET)
	@GiAudit(transactionName = "Get application data.", eventType = EventTypeEnum.ACCESS_PII_DATA, eventName = EventNameEnum.NON_FINANCIAL_APP )
	@PreAuthorize(SsapConstants.SSAP_TKM_PREVIEW)
	public String previewApplication(Model model, HttpServletRequest request, HttpServletResponse response)  throws GIException {
		String caseNumber = request.getParameter("caseNumber")==null?"": request.getParameter("caseNumber");
		if("".equals(caseNumber)) {
			ssapControllerLogger.error("Error in previewing the application:caseNumber cannot be empty");
			throw new GIException("Error in previewing the application:caseNumber cannot be empty");
		}

		SsapApplicationResource ssapApplicationResource = getApplicationByCaseNumber(caseNumber);
		if(null == ssapApplicationResource) {
			ssapControllerLogger.error("Error in previewing the application:Application is not retrieved");
			throw new GIException("Error in previewing the application:Application is not retrieved");
		}

		String ssapApplicationJson = ssapApplicationResource.getApplicationData();
		String currentPageId = ssapApplicationResource.getCurrentPageId();
		String applicationStatus = ssapApplicationResource.getApplicationStatus();
		String applicationMode = "editmode";
		long ssapId = getApplicationId(ssapApplicationResource);
		HttpSession session = request.getSession();
		session.setAttribute(SSAP_APPLICATION_ID,ssapId);
		String dateOfServer = getServerDate();

		SsapPrimaryApplicantInfo ssapPrimaryApplicantInfo = new SsapPrimaryApplicantInfo();
		int houseHoldId = 0;
		Household household =  null;
		String designatedBrokerLicenseNumber = "";
		String designatedBrokerName = "";
		String brokerFirstName = "";
		String brokerLastName = "";
		String designatedAssisterLicenseNumber = "";
		String designatedAssisterName = "";
		String assisterFirstName = "";
		String assisterLastName = "";
		String helpingEntityFlag = "NONE";

		try {
			houseHoldId = userService.getLoggedInUser().getActiveModuleId();
			household = consumerPortalUtil.getHouseholdRecord(houseHoldId);
			if(household!=null){
				ssapPrimaryApplicantInfo.setFirstName(household.getFirstName());
				ssapPrimaryApplicantInfo.setMiddleName(household.getMiddleName());
				ssapPrimaryApplicantInfo.setLastName(household.getLastName());
				ssapPrimaryApplicantInfo.setPhoneNumber(household.getPhoneNumber());
				ssapPrimaryApplicantInfo.setEmailAddress(household.getEmail());
			}
		}
		catch (InvalidUserException e1) {
			ssapControllerLogger.error("ERROR LOADING HOUSEHOLD INFORMATION>>>>>>>>>>>>>>>>>", e1);
			throw new GIRuntimeException(e1);
		}

		try {
			Broker broker = getAssignedBroker();
			if(broker!=null){
				designatedBrokerLicenseNumber = broker==null?"":broker.getLicenseNumber();
				designatedBrokerName = broker==null?"":broker.getUser().getFullName();
				brokerFirstName = broker!=null?broker.getUser().getFirstName():"";
				brokerLastName = broker!=null?broker.getUser().getLastName():"";
				helpingEntityFlag = "BROKER";
			}else{
				Assister assister = getAssignedAssister();
				if(assister!=null && "Active".equalsIgnoreCase(assister.getStatus())){
					designatedAssisterLicenseNumber = assister!=null && assister.getCertificationNumber()!=null?String.valueOf(assister.getCertificationNumber()):"";
					designatedAssisterName = assister!=null?assister.getUser().getFullName():"";
					assisterFirstName = assister!=null?assister.getUser().getFirstName():"";
					assisterLastName = assister!=null?assister.getUser().getLastName():"";
					helpingEntityFlag = "ASSISTER";
				}
			}
		} catch (Exception e) {
			ssapControllerLogger.error("ERROR LOADING BROKER/ASSISTER INFORMATION>>>>>>>>>>>>>>>>>", e);
			throw new GIRuntimeException(e);
		}


		Map<String,String> countriesMap =  new HashMap<String, String>();

		List<Countries> alcountriesList  = iCountriesRepository.findAll();
		//ssapControllerLogger.debug("alcountriesList size ====" +alcountriesList.size());
		for (Countries countriesObj : alcountriesList) {
			if(StringUtils.isNotEmpty(countriesObj.getCountryCode())){
				countriesMap.put(countriesObj.getCountryCode(), countriesObj.getCountryName());
			}
		}

		model.addAttribute("ssapApplicationJson",ssapApplicationJson);
		model.addAttribute("ssapCurrentPageId",currentPageId);
		model.addAttribute("ssapApplicationStatus",applicationStatus);
		model.addAttribute("applicationMode",applicationMode);
		model.addAttribute("dateOfServer",dateOfServer);
		model.addAttribute("designatedBrokerLicenseNumber",designatedBrokerLicenseNumber);
		model.addAttribute("designatedBrokerName",designatedBrokerName);
		model.addAttribute("brokerFirstName",brokerFirstName);
		model.addAttribute("brokerLastName",brokerLastName);
		model.addAttribute("designatedAssisterLicenseNumber",designatedAssisterLicenseNumber);
		model.addAttribute("designatedAssisterName",designatedAssisterName);
		model.addAttribute("assisterFirstName",assisterFirstName);
		model.addAttribute("assisterLastName",assisterLastName);
		model.addAttribute("helpingEntity",helpingEntityFlag);
		model.addAttribute("alCountryOfIssuances",platformGson.toJson(countriesMap));
		model.addAttribute("cmrHouseHold",platformGson.toJson(ssapPrimaryApplicantInfo));
		request.getSession().setAttribute(SSAP_VISITED, "Y");
		return  "ssapmain";

	}

	@RequestMapping(value="/deleteapplicant", method = RequestMethod.POST)
	@GiAudit(transactionName = "Deleting applicant data.", eventType = EventTypeEnum.DELETE_PII_DATA , eventName = EventNameEnum.NON_FINANCIAL_APP )
	@PreAuthorize(SsapConstants.SSAP_EDIT)
	public @ResponseBody String deleteApplicant(HttpServletRequest request, HttpServletResponse response) throws GIException, InvalidUserException{
		Integer personId = Integer.parseInt(request.getParameter("personId")==null?"0": request.getParameter("personId"));

		if(personId < 2) {
			ssapControllerLogger.error("The primary applicant cannot be deleted");
			throw new GIRuntimeException("The primary applicant cannot be deleted");
		}
		return saveERT( request, response);
	}

	private void startEventCreation(long ssapApplicationId, String qepEvent, String qepEventDate, String caseNumber, String applicationType) {
		

		try{
			 QepEventRequest qepEventRequest = createQepEventRequest(ssapApplicationId, qepEvent, qepEventDate, caseNumber, applicationType);
			 ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(QepEventRequest.class);
			  
			hIXHTTPClient.getPOSTData(GhixEndPoints.ELIGIBILITY_URL + "SsapEventsController/processEvents", writer.writeValueAsString(qepEventRequest), "application/json");
		}
		catch(IOException e) {
			ssapControllerLogger.error("Error in creating QepEvents for the caseNumber:", e);
			throw new GIRuntimeException("Error in creating QepEvents for the caseNumber:", e);
		}
		catch(Exception e) {
			ssapControllerLogger.error("Error in creating QepEvents for the caseNumber:" , e);
			throw new GIRuntimeException("Error in creating QepEvents for the caseNumber:", e);
		}
	}

	/**
	 * Returns current date in MM/dd/yyyy
	 * @return current date in MM/dd/yyyy format.
	 */
	private String getServerDate() {
		Date localDate = new TSDate();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
		return sdf.format(localDate);
	}
}
