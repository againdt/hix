package com.getinsured.ssap.model.eligibility;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.indportal.enums.SsapApplicationTypeEnum;

/**
 * Describes Eligibility Engine request.
 * @author Yevgen Golubenko
 * @since 4/4/19
 */
public class EligibilityRequest
{
  private String requestId;
  private long applicationId;
  private String countyCode;
  private boolean financialAssistance;
  private boolean failureToReconcile;
  private String zipCode;
  private int coverageYear;

  private int householdSize;
  private long householdIncome;

  private RunMode runMode = RunMode.ONLINE;

  private SsapApplicationTypeEnum typeOfApplication;

  private List<HouseholdMemberRequest> members = new ArrayList<>();

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(long applicationId) {
    this.applicationId = applicationId;
  }

  public String getCountyCode() {
    return countyCode;
  }

  public void setCountyCode(String countyCode) {
    this.countyCode = countyCode;
  }

  public boolean isFinancialAssistance() {
    return financialAssistance;
  }

  public void setFinancialAssistance(boolean financialAssistance) {
    this.financialAssistance = financialAssistance;
  }

  public boolean isFailureToReconcile() {
    return failureToReconcile;
  }

  public void setFailureToReconcile(boolean failureToReconcile) {
    this.failureToReconcile = failureToReconcile;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public int getCoverageYear() {
    return coverageYear;
  }

  public void setCoverageYear(int coverageYear) {
    this.coverageYear = coverageYear;
  }

  public int getHouseholdSize() {
    return householdSize;
  }

  public void setHouseholdSize(int householdSize) {
    this.householdSize = householdSize;
  }

  public long getHouseholdIncome() {
    return householdIncome;
  }

  public void setHouseholdIncome(long householdIncome) {
    this.householdIncome = householdIncome;
  }

  public List<HouseholdMemberRequest> getMembers() {
    return members;
  }

  public void setMembers(List<HouseholdMemberRequest> members) {
    this.members = members;
  }

  public RunMode getRunMode() {
    return runMode;
  }

  public void setRunMode(RunMode runMode) {
    this.runMode = runMode;
  }

  public SsapApplicationTypeEnum getTypeOfApplication() {
    return typeOfApplication;
  }

  public void setTypeOfApplication(SsapApplicationTypeEnum typeOfApplication) {
    this.typeOfApplication = typeOfApplication;
  }

  @Override
  public String toString() {
    return "EligibilityRequest{" +
      "requestId='" + requestId + '\'' +
      ", applicationId=" + applicationId +
      ", countyCode='" + countyCode + '\'' +
      ", financialAssistance=" + financialAssistance +
      ", failureToReconcile=" + failureToReconcile +
      ", zipCode='" + zipCode + '\'' +
      ", coverageYear=" + coverageYear +
      ", householdSize=" + householdSize +
      ", householdIncome=" + householdIncome +
      ", runMode=" + runMode +
      ", typeOfApplication=" + typeOfApplication +
      ", members=" + members +
      '}';
  }
}
