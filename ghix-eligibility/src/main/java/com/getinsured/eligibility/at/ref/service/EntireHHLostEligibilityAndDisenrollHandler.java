package com.getinsured.eligibility.at.ref.service;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Component("entireHHLostEligibilityAndDisenroll")
@Scope("singleton")
public class EntireHHLostEligibilityAndDisenrollHandler extends LceProcessHandlerBaseService {
	
	private static final Logger LOGGER = Logger.getLogger(EntireHHLostEligibilityAndDisenrollHandler.class);

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	/**
	 * When all applicant has non Qhp events invoke disenrollment api to disenrol the individual. if success Set ssap to CL. If failed manual process will be invoked. individual needs to disenroll from dashboard
	 */

	public void handleAllApplicantHasNonQhpIndicator(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap) {
		ssapApplicationEventService.createDeniedHouseholdApplicationEvent(currentApplication, applicantEventMap);
		updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		getValidationStatus(currentApplication.getCaseNumber());
		
		//HIX-108047
		if(   currentApplication.getId() != 0  ) {
			currentApplication = loadCurrentApplication(currentApplication.getId());
		}
		
		final String response = executeDisenrollment(enrolledApplication, applicantEventMap);
		handleDisenrollmentResponse(currentApplication, enrolledApplication, response);
	}

	

	/**
	 * To be consumed by batch process to disenroll SEP if no action taken by consumer.
	 * 
	 * @param currentApplication
	 * @param enrolledApplication
	 * @param terminationDate
	 */
	public String batchDisenroll(String[] arguments) {
		String response = GhixConstants.RESPONSE_FAILURE;
		long currentApplicationId = 0;
		long enrolledApplicationId = 0;
		try {
			currentApplicationId = Long.valueOf(arguments[0]);
			enrolledApplicationId = Long.valueOf(arguments[1]);
			String terminationDateStr = arguments[2];

			if (currentApplicationId == 0 || enrolledApplicationId == 0) {
				throw new GIRuntimeException("Curent application id or Enrolled application id is not correct" + currentApplicationId + " " + enrolledApplicationId);
			} else {
				SsapApplication currentApplication = ssapApplicationRepository.findOne(currentApplicationId);
				SsapApplication enrolledApplication = ssapApplicationRepository.findOne(enrolledApplicationId);
				Date terminationDate = ReferralUtil.convertStringToDate(terminationDateStr, ReferralConstants.DB_DATE_FORMAT);
				if (terminationDate == null) {
					throw new GIRuntimeException("Termination date is not valid for " + currentApplicationId);
				}

				if (currentApplication == null || enrolledApplication == null || currentApplication.getCmrHouseoldId() == null || enrolledApplication.getCmrHouseoldId() == null) {
					throw new GIRuntimeException("Curent application or Enrolled application not found for" + currentApplicationId + " " + enrolledApplicationId);
				}
				if (currentApplication.getCmrHouseoldId().compareTo(enrolledApplication.getCmrHouseoldId()) != 0) {
					String errorMsg = "Households are not same for suppiled newSsapApplicationId and enrolledSsapApplicationId ";
					LOGGER.error(errorMsg);
					throw new GIRuntimeException(errorMsg);
				}
				response = executeDisenrollment(enrolledApplication, terminationDate);
				if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(response)) {
					handleDisenrollmentResponse(currentApplication, enrolledApplication, response);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while batch disenrolling for currentApplicationId " + currentApplicationId +" - " + ExceptionUtils.getFullStackTrace(e));
		}
		return response;
	}

}
