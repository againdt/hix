package com.getinsured.ssap.service;

import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.ssap.model.eligibility.EligibilityResponse;
import com.getinsured.ssap.model.eligibility.RunMode;

/**
 * Interface for Eligibility Engine Integration.
 *
 * @author Yevgen Golubenko
 * @since 4/4/19
 */
public interface EligibilityEngineIntegrationService
{
  /**
   * Invokes eligibility engine and returns {@link EligibilityResponse}.
   * It does not do anything with the {@link EligibilityResponse} afterwards,
   * it's up to the caller to do something with it. If you are interested
   * in full eligibility flow, see {@link #invokeEligibilityEngine(SingleStreamlinedApplication, String, String, RunMode)}.
   *
   * @param application {@link SingleStreamlinedApplication} for which to call eligibility engine.
   * @return {@link EligibilityResponse} object returned from eligibility engine.
   * @throws Exception if exception occurred during eligibility engine invocation.
   */
  EligibilityResponse callEligibilityEngine(SingleStreamlinedApplication application, RunMode runMode) throws Exception;

  /**
   * Invokes eligibility engine with given {@link SingleStreamlinedApplication}.
   *
   * @param application {@link SingleStreamlinedApplication} object.
   * @param specialEnrollmentEvent if there is any special enrollment event associated with this application, can be {@code null}
   * @param specialEnrollmentDate if there is date of the special enrollment.
   * @param runMode {@link RunMode} mode in which we are running.
   * @return {@link EligibilityResponse} object.
   */
  EligibilityResponse invokeEligibilityEngine(SingleStreamlinedApplication application, String specialEnrollmentEvent, String specialEnrollmentDate, RunMode runMode);
}
