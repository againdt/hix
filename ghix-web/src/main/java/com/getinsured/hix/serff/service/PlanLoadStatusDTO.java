package com.getinsured.hix.serff.service;

import java.util.Date;

import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.BATCH_STATUS;
import com.getinsured.hix.platform.util.DateUtil;

/**
 * DTO class is used to get data from SerffPlanMgmtBatch & SerffPlanMgmt tables for Plan Load Status page grid.
 * 
 * @author Bhavin Parmar
 * @since 03 March, 2017
 */
public class PlanLoadStatusDTO {

	public enum STATUS {
		IN_PROGRESS, WAITING, IN_QUEUE, SUCCESS, FAILURE;
	}

	public enum TYPE {
		DRUG_LIST("Drug List"), PLANS("Plans");

		private final String name;

		private TYPE(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	private Long serffReqID;			// SerffPlanMgmtBatch.serffReqId
	private String issuerName;			// SerffPlanMgmtBatch.career
	private String hiosIssuerID;		// SerffPlanMgmtBatch.issuerId
	private String uploadedDate;		// SerffPlanMgmtBatch.ftpEndTime
	private String type;				// SerffPlanMgmtBatch.operationType
	private String status;				// SerffPlanMgmtBatch.batchStatus
	private BATCH_STATUS batchStatus;	// SerffPlanMgmtBatch.batchStatus
	private String description;			// SerffPlanMgmt.remarks
	private String logs;				// SerffPlanMgmt.responseXml
	private String differenceReport;	// SerffPlanMgmt.responseXml

	public PlanLoadStatusDTO() {
	}

	public PlanLoadStatusDTO(Long serffReqID, String issuerName,
			String hiosIssuerID, Date uploadedDate, String operationType, BATCH_STATUS batchStatus,
			String description, String logs, String differenceReport) {

		this.serffReqID = serffReqID;
		this.issuerName = issuerName;
		this.hiosIssuerID = hiosIssuerID;

		if (null != uploadedDate) {
			this.uploadedDate = DateUtil.dateToString(uploadedDate, "MMM dd, yyyy");
		}

		if (null != operationType) {

			if (SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_JSON.toString().equals(operationType)) {
				this.type = "Drug List - JSON";
				this.differenceReport = differenceReport;
			}
			else if (SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_XML.toString().equals(operationType)) {
				this.type = "Drug List - XML";
				this.differenceReport = differenceReport;
			}
			else if (SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.toString().equals(operationType)
					|| SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.toString().equals(operationType)) {
				this.type = "Plans";
			}
			else {
				this.type = operationType;
			}
		}

		if (null != batchStatus) {

			switch (batchStatus) {

				case COMPLETED: {
					this.status = STATUS.SUCCESS.toString();
					break;
				}
				case FAILED: {
					this.status = STATUS.FAILURE.toString();
					break;
				}
				case WAITING: {
					this.status = STATUS.WAITING.toString();
					break;
				}
				case IN_QUEUE: {
					this.status = STATUS.IN_QUEUE.toString();
					break;
				}
				default:
					this.status = STATUS.IN_PROGRESS.toString();
					break;
			}
		}
		this.description = description;
		this.logs = logs;
	}

	public Long getSerffReqID() {
		return serffReqID;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public String getHiosIssuerID() {
		return hiosIssuerID;
	}

	public String getUploadedDate() {
		return uploadedDate;
	}

	public String getType() {
		return type;
	}

	public String getStatus() {
		return status;
	}

	public BATCH_STATUS getBatchStatus() {
		return batchStatus;
	}

	public String getDescription() {
		return description;
	}

	public String getLogs() {
		return logs;
	}

	public String getDifferenceReport() {
		return differenceReport;
	}
}
