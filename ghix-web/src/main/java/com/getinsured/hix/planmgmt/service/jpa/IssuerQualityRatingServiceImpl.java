package com.getinsured.hix.planmgmt.service.jpa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AdminDocument;
import com.getinsured.hix.model.AdminDocument.DOCUMENT_TYPE;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerQualityRating;
import com.getinsured.hix.model.IssuerQualityRating.IssuerQualityRatingField;
import com.getinsured.hix.model.IssuerQualityRating.IssuerQualityRatingsXMLTags;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.mapper.IssuerQualityRatingPojo;
import com.getinsured.hix.planmgmt.repository.AdminDocumentRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerQualityRatingRepository;
import com.getinsured.hix.planmgmt.service.IssuerQualityRatingService;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.file.reader.FileReader;
import com.getinsured.hix.platform.file.reader.ReaderFactory;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;

@Service("issuerQualityRatingService")
@Repository
@Transactional
public class IssuerQualityRatingServiceImpl implements IssuerQualityRatingService {
	public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerQualityRatingServiceImpl.class);
	public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";	
	private static final int TOTAL_ROW_COUNT = 24;
	private static final int DECREMENT_SPAN_FOR_END_DATE = -1;
	private static final int END_DATE_SPAN = 12;

	@Autowired private IIssuerQualityRatingRepository qualityRatingRepository;
	@Autowired private AdminDocumentRepository adminDocumentRepository;
	@Autowired private IssuerService issuerService;	
	@Autowired private PlanMgmtService planMgmtService;
	
	@Value(PlanMgmtConstants.DATABASE_TYPE_CONFIG)
	private String DB_TYPE;
	@PersistenceUnit private EntityManagerFactory emf;

	@Override
	public boolean saveQualityRating(IssuerQualityRating issuerRatingObj) {
		qualityRatingRepository.save(issuerRatingObj);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 	This method will return a Map of all the values of the ISSUER_QUALITY_RATINGS table to
	 * the caller
	 * @see com.getinsured.hix.planmgmt.service.IssuerQualityRatingService#getIssuerQualityRatingByIssuerId(int)
	 */
	@Override
	public Map<String, String> getIssuerQualityRatingByIssuerId(int issuerId) {
		LOGGER.info("getIssuerQualityRatingByIssuerId()");
		Map<String, String> toPlanSelectionFieldsMap = new LinkedHashMap<String, String>();
		List<IssuerQualityRating> issuerQualityRating = new ArrayList<IssuerQualityRating>();
		issuerQualityRating = qualityRatingRepository.findByIssuerId(issuerId);

		if (issuerQualityRating.size() > 0) { // pass only one record
			// Retrieve the main fields of the table and put inside the Map
			String toPlanSelectionIssuerId = String.valueOf(issuerQualityRating.get(0).getIssuerId());
			String toPlanSelectionOverallQualityRating = issuerQualityRating.get(0).getQualityRating();
			String toPlanSelectionQualitySource = issuerQualityRating.get(0).getQualitySource();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String toPlanSelectionEffectiveDate = dateFormat.format(issuerQualityRating.get(0).getEffectiveDate());
			IssuerQualityRatingField attrib = IssuerQualityRatingField.IssuerId;
			toPlanSelectionFieldsMap.put(attrib.toString(), toPlanSelectionIssuerId);
			attrib = IssuerQualityRatingField.QualityRating;
			toPlanSelectionFieldsMap.put(attrib.toString(), toPlanSelectionOverallQualityRating);
			attrib = IssuerQualityRatingField.QualitySource;
			toPlanSelectionFieldsMap.put(attrib.toString(), toPlanSelectionQualitySource);
			attrib = IssuerQualityRatingField.EffectiveDate;
			toPlanSelectionFieldsMap.put(attrib.toString(), toPlanSelectionEffectiveDate);

			// Retrieve the CSV values from 'quality_rating_details' field and
			// put inside a Map
			String allRatingsCSVString = issuerQualityRating.get(0).getQualityRatingDetails();
			List<String> ratings = new ArrayList<String>(Arrays.asList(allRatingsCSVString.split(",")));
			int i = 0;
			for (IssuerQualityRatingsXMLTags tag : IssuerQualityRatingsXMLTags.values()) {
				String fieldKey = tag.toString();
				String fieldValue = ratings.get(i);
				i++;
				toPlanSelectionFieldsMap.put(fieldKey, fieldValue);
			}
		}
	
		return toPlanSelectionFieldsMap;
	}

	@Override
	public boolean saveDocument(AdminDocument adminDocument) {
		adminDocumentRepository.save(adminDocument);
		return true;
	}

	@Override
	public List<AdminDocument> getAllAdminDocument(String documentType) {
		List<AdminDocument> documentsList = adminDocumentRepository.findAll();
		List<AdminDocument> returnedList = new ArrayList<AdminDocument>();
		for (AdminDocument adminDocument : documentsList) {
			if (adminDocument.getDocumentType().equalsIgnoreCase(documentType)) {
				returnedList.add(adminDocument);
			}
		}
		//LOGGER.debug("returnedList: " + SecurityUtil.sanitizeForLogging(String.valueOf(returnedList.size())));

		Collections.sort(returnedList, new Comparator<AdminDocument>() {
			public int compare(AdminDocument ad1, AdminDocument ad2) {
				return ad2.getLastUpdateTimestamp().compareTo(ad1.getLastUpdateTimestamp());
			}
		});
		return returnedList;
	}

	/*	Check IssuerQualityRating Exist OR Not*/
	@Override
	public boolean getIssuerQualityRatingWithEffectiveStartandEndDate(IssuerQualityRating qualityRating) {
		String effectiveStartDate = DateUtil.dateToString(qualityRating.getEffectiveDate(), REQUIRED_DATE_FORMAT);
		String effectiveEndDate = DateUtil.dateToString(qualityRating.getEffectiveEndDate(), REQUIRED_DATE_FORMAT);

		IssuerQualityRating issuerQualityRatingObj = qualityRatingRepository.getIssuerQualityRatingByIssuerId(qualityRating.getIssuerId(), qualityRating.getPlanHiosId(), effectiveStartDate, effectiveEndDate);
		if (issuerQualityRatingObj != null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean getIssuersEffectiveEndDates(IssuerQualityRating qualityRating) {
		String effectiveStartDate = DateUtil.dateToString(qualityRating.getEffectiveDate(), REQUIRED_DATE_FORMAT);
		List<IssuerQualityRating> issuerExistWithGreaterEndDate = qualityRatingRepository.getIssuersEffectiveEndDates(qualityRating.getIssuerId(), qualityRating.getPlanHiosId(), effectiveStartDate);
		if(issuerExistWithGreaterEndDate.size() > 0 ){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean getIssuersEffectiveDateRange(IssuerQualityRating qualityRating) {
		String effectiveStartDate = DateUtil.dateToString(qualityRating.getEffectiveDate(), REQUIRED_DATE_FORMAT);
		boolean result = false;
		
		if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
			result = getIssuersEffectiveDateRangeForPostgres(qualityRating.getIssuerId(), qualityRating.getPlanHiosId() ,effectiveStartDate);
		}
		else{
			IssuerQualityRating issuerQualityRating = qualityRatingRepository.checkEffectiveDateBetweenStartAndEndDate(qualityRating.getIssuerId(), qualityRating.getPlanHiosId() ,effectiveStartDate);
			if (issuerQualityRating != null) {
				result = true;
			} else {
				result =  false;
			}
		}
		return result;
	}
	
	public boolean getIssuersEffectiveDateRangeForPostgres(int issuerId, String planHiosId, String effectiveStartDate) {
		EntityManager em = null;
		boolean result = false;
		List<Object[]> qrList = new ArrayList<Object[]>();
		
		if(planHiosId == null || effectiveStartDate == null){
			return false;
		}
	
		String queryString = "SELECT iqr.id from issuer_quality_rating iqr " +
		                     "where iqr.issuer_id=:issuerId and iqr.plan_hios_id=:planNumber " + 
				             "and TO_DATE(:effectiveStartDate,'MM-DD-YYYY') between TO_DATE(to_char(iqr.effective_date,'MM-DD-YYYY'),'MM-DD-YYYY') + interval '1 days'  " +
		                     " AND TO_DATE(to_char(iqr.effective_end_date,'MM-DD-YYYY'),'MM-DD-YYYY') AND is_deleted='N' ";
		try{
			em = emf.createEntityManager();
			Query query = em.createNativeQuery(queryString);
			query.setParameter("issuerId", issuerId);
			query.setParameter("effectiveStartDate", effectiveStartDate);
			query.setParameter("planNumber", planHiosId);
			List<?> qualityRatingAreaList = query.getResultList();
			if (qualityRatingAreaList != null && qualityRatingAreaList.size() > 0) {
				result = true;
			}
		}catch (Exception e) {
			LOGGER.error("ERROR in getIssuersEffectiveDateRangeForPostgres >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in getIssuersEffectiveDateRangeForPostgres >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage(), e);
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		return result;
	}

	@Override
	public IssuerQualityRating getQualitRatingForIssuer(Integer issuerId) {
		IssuerQualityRating issuerQualityRating = qualityRatingRepository.getQualitRatingForIssuer(issuerId);
		if (issuerQualityRating != null) {
			return issuerQualityRating;
		} else {
			return null;
		}
	}
	
	@Override
	public boolean saveDocument(AccountUser user, int commentid, MultipartFile qualityRating, String documentName) {
		//	LOGGER.info("UserInfo: " + user.getId() + "::"+ user.getEmail(),LogLevel.INFO);
		
		AdminDocument document = new AdminDocument();
		document.setDocumentType(DOCUMENT_TYPE.QUALITY_RATING.toString());
		document.setCreatedBy(user.getId());
		document.setLastUpdatedBy(user.getId());
		document.setDocumentName(documentName);
		document.setUserID(user.getId());
		document.setCommentID(commentid);
		document.setCreationTimestamp(DateUtil.StringToDate(getSpecificFormat(), REQUIRED_DATE_FORMAT));
		document.setLastUpdateTimestamp(DateUtil.StringToDate(getSpecificFormat(), REQUIRED_DATE_FORMAT));
		document.setFileName(qualityRating.getOriginalFilename());
		document.setFileType(qualityRating.getContentType());
		document.setFileSize(qualityRating.getSize());
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Save Quality Rating file");
		}
		adminDocumentRepository.save(document);
		return true;
	}
	
	@Override
	public List<List<IssuerQualityRatingPojo>> processQualityRatingData(String filePath, String stateCode) {
		
		List<List<IssuerQualityRatingPojo>> issuerQualityRatingList = new ArrayList<List<IssuerQualityRatingPojo>>();
		List<List<String>> data = null;

		@SuppressWarnings("unchecked")
		FileReader<List<String>> qualityReader = ReaderFactory.<List<String>> getReader(filePath, ',', List.class);
		data = qualityReader.readData();
		
		List<IssuerQualityRatingPojo> issuerQualityRatingValidList = new ArrayList<IssuerQualityRatingPojo>();
		List<IssuerQualityRatingPojo> issuerQualityRatinginvalidList = new ArrayList<IssuerQualityRatingPojo>();
		try{
		
			if(data!=null && data.size()>0){
				Issuer issuerObj = null;
				boolean isValidHIOSPlanId = false; 
				boolean isMatchingIssuerIdAndPLan = false;
				boolean hasValidEffectiveDate = false;
				boolean isFutureDate = false;
				boolean isEffectiveDateLessThenOneYear = false;
				boolean isGlobleRatingValid = false;
				IssuerQualityRatingPojo issuerQualityRatingPojo = null;
				IssuerQualityRatingPojo invalidIssuerQualityRatingPojo =null;
				
				for(int i=0;i<data.size();i++){
					
					if(data.get(i).size()>TOTAL_ROW_COUNT){
							/*if(LOGGER.isDebugEnabled()){
								LOGGER.debug(stateCode + " Company name , HOISIssuerID & HOISPlan Id :" + data.get(i).get(PlanMgmtConstants.ZERO) + "::" + data.get(i).get(PlanMgmtConstants.ONE) + "::" + data.get(i).get(PlanMgmtConstants.TWO)+ " :: " + data.get(i).get(PlanMgmtConstants.THREE).trim() + " :: " + data.get(i).get(PlanMgmtConstants.FOUR).trim()   );
							}*/
							issuerObj = issuerService.getIssuerByHiosID(data.get(i).get(PlanMgmtConstants.ONE).trim());
							
							isValidHIOSPlanId = isHIOSPlanIdValid(data.get(i).get(PlanMgmtConstants.TWO).trim());
							
							if(issuerObj != null && issuerObj.getId() > 0 && StringUtils.isNotBlank(data.get(i).get(PlanMgmtConstants.TWO).trim())){
								isMatchingIssuerIdAndPLan = isMatchingIssuerIdToPLan(issuerObj.getId(), data.get(i).get(PlanMgmtConstants.TWO).trim());
							}

							hasValidEffectiveDate = hasValidEffectiveDate(data.get(i).get(PlanMgmtConstants.THREE).trim());

							if (hasValidEffectiveDate) {
								isFutureDate = isFutureDate(data.get(i).get(PlanMgmtConstants.THREE).trim());
								isEffectiveDateLessThenOneYear = isEffectiveDateLessThenOneYear(data.get(i).get(PlanMgmtConstants.THREE).trim());
							}
							
							/********** FOR CA STATE EXCHANGE, GLOBAL RATING COULD BE NULL IN TEMPLATE. DON'T VALIDATE GLOBAL RATING *************/
							if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode) || "MN".equalsIgnoreCase(stateCode)){
								isGlobleRatingValid = true;
							}else{
								isGlobleRatingValid = isGlobalRatingValid(data.get(i).get(PlanMgmtConstants.FOUR).trim());
							}
							
							/*LOGGER.info("isValidHIOSProductId " + isValidHIOSProductId);
							LOGGER.info("isMatchingIssuerIdAndPLan " + isMatchingIssuerIdAndPLan);
							LOGGER.info("isFutureDate " + isFutureDate);
							LOGGER.info("isEffectiveDateLessThenOneYear " + isEffectiveDateLessThenOneYear);
							LOGGER.info("isGlobleRatingValid " + isGlobleRatingValid);*/
							
							if (issuerObj != null && issuerObj.getId() > 0 && isValidHIOSPlanId && hasValidEffectiveDate
								&& isFutureDate && isEffectiveDateLessThenOneYear && isMatchingIssuerIdAndPLan
								&& isGlobleRatingValid) {
								issuerQualityRatingPojo = new IssuerQualityRatingPojo();
								
								issuerQualityRatingPojo.setCompanyLegal(data.get(i).get(PlanMgmtConstants.ZERO));
								issuerQualityRatingPojo.setIsuerId(issuerObj.getId());
								//LOGGER.info("ISSUER ID " + issuerQualityRatingPojo.getIsuer().getId());
								issuerQualityRatingPojo.setHIOSPlanId(data.get(i).get(PlanMgmtConstants.TWO).trim());
								issuerQualityRatingPojo.setEffectiveDate(data.get(i).get(PlanMgmtConstants.THREE).trim());
								issuerQualityRatingPojo.setGlobalRating(data.get(i).get(PlanMgmtConstants.FOUR));
								
								/********** CA STATE EXCHANGE QUALITY RATING PROPERTIES START HERE ***********/
								if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode) || "MN".equalsIgnoreCase(stateCode)){
									issuerQualityRatingPojo.setSummaryClinicalQualityManagement(data.get(i).get(PlanMgmtConstants.FIVE));
									issuerQualityRatingPojo.setClinicalEffectiveness(data.get(i).get(PlanMgmtConstants.SIX));
									issuerQualityRatingPojo.setAsthmaCare(data.get(i).get(PlanMgmtConstants.SEVEN));
									issuerQualityRatingPojo.setBehavioralHealth(data.get(i).get(PlanMgmtConstants.EIGHT));
									issuerQualityRatingPojo.setCardiovascularCare(data.get(i).get(PlanMgmtConstants.NINE));
									issuerQualityRatingPojo.setDiabetesCare(data.get(i).get(PlanMgmtConstants.TEN));
									issuerQualityRatingPojo.setDomainPatientSafety(data.get(i).get(PlanMgmtConstants.ELEVEN));
									issuerQualityRatingPojo.setPatientSafety(data.get(i).get(PlanMgmtConstants.TWELVE));
									issuerQualityRatingPojo.setPrevention(data.get(i).get(PlanMgmtConstants.THIRTEEN));
									issuerQualityRatingPojo.setCheckingForCancer(data.get(i).get(PlanMgmtConstants.FOURTEEN));
									issuerQualityRatingPojo.setMaternalCare(data.get(i).get(PlanMgmtConstants.FIFTEEN));
									issuerQualityRatingPojo.setStayingHealthyAdult(data.get(i).get(PlanMgmtConstants.SIXTEEN));
									issuerQualityRatingPojo.setStayingHealthyChild(data.get(i).get(PlanMgmtConstants.SEVENTEEN));
									issuerQualityRatingPojo.setSummaryEnrolleeExperience(data.get(i).get(PlanMgmtConstants.EIGHTEEN));
									issuerQualityRatingPojo.setDomainAccessToCare(data.get(i).get(PlanMgmtConstants.NINTEEN));
									issuerQualityRatingPojo.setAccessToCare(data.get(i).get(PlanMgmtConstants.TWENETY));
									issuerQualityRatingPojo.setDomainCareCoordination(data.get(i).get(PlanMgmtConstants.TWENETYONE));
									issuerQualityRatingPojo.setCareCoordination(data.get(i).get(PlanMgmtConstants.TWENETYTWO));
									issuerQualityRatingPojo.setDomainDoctorAndCare(data.get(i).get(PlanMgmtConstants.TWENETYTHREE));
									issuerQualityRatingPojo.setDoctorAndCare(data.get(i).get(PlanMgmtConstants.TWENETYFOUR));
									issuerQualityRatingPojo.setSummaryPlanEfficiencyAffordabilityAndManagement(data.get(i).get(PlanMgmtConstants.TWENETYFIVE));
									//These columns are not received for MN
									if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)){
										issuerQualityRatingPojo.setEfficiencyAndAffordability(data.get(i).get(PlanMgmtConstants.TWENETYSIX));
										issuerQualityRatingPojo.setEfficientCare(data.get(i).get(PlanMgmtConstants.TWENETYSEVEN));
										issuerQualityRatingPojo.setPlanService(data.get(i).get(PlanMgmtConstants.TWENETYEIGHT));
										issuerQualityRatingPojo.setEnrolleeExperienceWithHealthPlan(data.get(i).get(PlanMgmtConstants.TWENETYNINE));
									}
								}
								/********** NON CA STATE EXCHANGE QUALITY RATING PROPERTIES START HERE ***********/
								else{
									issuerQualityRatingPojo.setAccessRating(data.get(i).get(PlanMgmtConstants.FIVE));
									issuerQualityRatingPojo.setGettingTimelyAppointmentscare(data.get(i).get(PlanMgmtConstants.SIX));
									issuerQualityRatingPojo.setGettingCarewithYourPersonalDoctor(data.get(i).get(PlanMgmtConstants.SEVEN));
									issuerQualityRatingPojo.setStayingHealthyRating(data.get(i).get(PlanMgmtConstants.EIGHT));
									issuerQualityRatingPojo.setCheckingForCancer(data.get(i).get(PlanMgmtConstants.NINE));
									issuerQualityRatingPojo.setHelpForHealthyBehaviors(data.get(i).get(PlanMgmtConstants.TEN));
									issuerQualityRatingPojo.setWellnessCareForChildren(data.get(i).get(PlanMgmtConstants.ELEVEN));
									issuerQualityRatingPojo.setPlanServiceRating(data.get(i).get(PlanMgmtConstants.TWELVE));
									issuerQualityRatingPojo.setPlanCustomerService(data.get(i).get(PlanMgmtConstants.THIRTEEN));
									issuerQualityRatingPojo.setGettingClaimsPaid(data.get(i).get(PlanMgmtConstants.FOURTEEN));
									issuerQualityRatingPojo.setShoppingServicestoFindAffordableMedicalCare(data.get(i).get(PlanMgmtConstants.FIFTEEN));
									issuerQualityRatingPojo.setGettinganInterpreterWhenSeeingTheDoctor(data.get(i).get(PlanMgmtConstants.SIXTEEN));
									issuerQualityRatingPojo.setMemberComplaintsAppeals(data.get(i).get(PlanMgmtConstants.SEVENTEEN));
									issuerQualityRatingPojo.setClinicalCareRating(data.get(i).get(PlanMgmtConstants.EIGHTEEN));
									issuerQualityRatingPojo.setGettingtheRighSafeCare(data.get(i).get(PlanMgmtConstants.NINTEEN));
									issuerQualityRatingPojo.setDiabetesCare(data.get(i).get(PlanMgmtConstants.TWENETY));
									issuerQualityRatingPojo.setHeartCare(data.get(i).get(PlanMgmtConstants.TWENETYONE));
									issuerQualityRatingPojo.setMentalHealthCare(data.get(i).get(PlanMgmtConstants.TWENETYTWO));
									issuerQualityRatingPojo.setRespiratoryCare(data.get(i).get(PlanMgmtConstants.TWENETYTHREE));
									issuerQualityRatingPojo.setMaternityCare(data.get(i).get(PlanMgmtConstants.TWENETYFOUR));	
								}
								
								issuerQualityRatingValidList.add(issuerQualityRatingPojo);
							}else{
								invalidIssuerQualityRatingPojo = new IssuerQualityRatingPojo();
																							
								int rowCount = i + 1;
								
								if(null == issuerObj){
									invalidIssuerQualityRatingPojo.setHIOSId(data.get(i).get(PlanMgmtConstants.ONE).trim()+"@"+rowCount);
								}
								if(!isValidHIOSPlanId){
									invalidIssuerQualityRatingPojo.setHIOSPlanId(data.get(i).get(PlanMgmtConstants.TWO).trim()+"@"+rowCount);
								}
								if(!isMatchingIssuerIdAndPLan && issuerObj != null && isValidHIOSPlanId){
									invalidIssuerQualityRatingPojo.setNotExistIssuer(""+"@"+rowCount);
								}

								if (!hasValidEffectiveDate) {
									invalidIssuerQualityRatingPojo.setEffectiveDate(data.get(i).get(PlanMgmtConstants.THREE).trim()+"@"+rowCount);
								}
								else {

									if(!isFutureDate){
										invalidIssuerQualityRatingPojo.setEffectiveDate(data.get(i).get(PlanMgmtConstants.THREE).trim()+"@"+rowCount);
									}

									if(!isEffectiveDateLessThenOneYear){
										invalidIssuerQualityRatingPojo.setEffectiveDate("FailedYearValidation_" + data.get(i).get(PlanMgmtConstants.THREE).trim()+"@"+rowCount);
									}
								}

								if(!isGlobleRatingValid){
									invalidIssuerQualityRatingPojo.setGlobalRatingError(data.get(i).get(PlanMgmtConstants.FOUR).trim()+"@"+rowCount);
								}
								
								issuerQualityRatinginvalidList.add(invalidIssuerQualityRatingPojo);
								/*if(LOGGER.isDebugEnabled()){
									LOGGER.debug("invalidIssuerQualityRatingList :" + issuerQualityRatinginvalidList.size());
								}	*/
							}
						
					}
					else {
						LOGGER.warn("Found record where column count is less than expected count @" + i);
					}
				}
			}
			issuerQualityRatingList.add(issuerQualityRatingValidList);
			issuerQualityRatingList.add(issuerQualityRatinginvalidList);
			return issuerQualityRatingList;
			
		}	catch(Exception ex){
			LOGGER.error("Exception occured in processQualityRatingData " + ex.getMessage());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_QUALITY_RATING_EXCEPTION, null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}	
	}
	
	@Override
	public boolean saveIssuerQualityRating(IssuerQualityRatingPojo issuerQualityRatingPojo, AccountUser user, IssuerQualityRating issuerQualityRatingObj, String stateCode){
			try{	
				// delete Future Effective Date Records
				deleteFutureEffectiveDateRecord(issuerQualityRatingPojo.getIsuerId(), issuerQualityRatingPojo.getHIOSPlanId(), issuerQualityRatingPojo.getEffectiveDate(), user.getId());
				
				issuerQualityRatingObj.setIssuerId(issuerQualityRatingPojo.getIsuerId());
				
				issuerQualityRatingObj.setQualityRating(String.valueOf(issuerQualityRatingPojo.getGlobalRating()));
								
				issuerQualityRatingObj.setQualityRatingDetails(buildQualityRatingStr(issuerQualityRatingPojo, stateCode));
				
				issuerQualityRatingObj.setEffectiveDate(DateUtil.StringToDate(issuerQualityRatingPojo.getEffectiveDate(),REQUIRED_DATE_FORMAT));
				
				Date effectiveEndDateforNewIssuer = getEffectiveEndDate(issuerQualityRatingObj.getEffectiveDate());
				
				issuerQualityRatingObj.setEffectiveEndDate(effectiveEndDateforNewIssuer);
				
				issuerQualityRatingObj.setPlanHiosId(issuerQualityRatingPojo.getHIOSPlanId());
				
				issuerQualityRatingObj.setCreatedBy(user.getId());
				
				issuerQualityRatingObj.setLastUpdatedBy(user.getId());
				
				issuerQualityRatingObj.setIsDeleted(PlanMgmtConstants.N);
				
				boolean  issuerWithSameEffectiveDate = getIssuerQualityRatingWithEffectiveStartandEndDate(issuerQualityRatingObj);
				
				boolean issuerWithGreaterThanEffectiveEndDate = getIssuersEffectiveEndDates(issuerQualityRatingObj);
				
				boolean issuerWithBetweenStartAndEndEffectiveDate = getIssuersEffectiveDateRange(issuerQualityRatingObj);
			
				//LOGGER.info("issuerWithGratterThanEffectiveEndDate: " + issuerWithGratterThanEffectiveEndDate, LogLevel.INFO);
				//LOGGER.info("issuerWithBetweenStartAndEndEffectiveDate: " + issuerWithBetweenStartAndEndEffectiveDate ,LogLevel.INFO);
				
				if(issuerWithGreaterThanEffectiveEndDate && issuerWithBetweenStartAndEndEffectiveDate){
					setDateRangesAndUpdateAndInsert(issuerQualityRatingObj,user,issuerQualityRatingPojo.getHIOSPlanId());
				}else{
				
					if(issuerWithSameEffectiveDate) {
						//LOGGER.info("ExistInQualityRating: " + issuerQualityRatingPojo.getIsuerId());
						issuerQualityRatingObj = getQualitRatingByIssuerIdAndHiosProductId(issuerQualityRatingPojo.getIsuerId(), issuerQualityRatingPojo.getHIOSPlanId());
						
						if(issuerQualityRatingObj != null){
							issuerQualityRatingObj.setPlanHiosId(issuerQualityRatingPojo.getHIOSPlanId());
							issuerQualityRatingObj.setCreatedBy(user.getId());
							issuerQualityRatingObj.setLastUpdatedBy(user.getId());
							saveQualityRating(issuerQualityRatingObj);
						}
					}else if(issuerWithGreaterThanEffectiveEndDate){
						
						saveQualityRating(issuerQualityRatingObj);
						
					}else if(issuerWithBetweenStartAndEndEffectiveDate){
						
						setDateRangesAndUpdateAndInsert(issuerQualityRatingObj,user,issuerQualityRatingPojo.getHIOSPlanId());
						
					}else{
						
						saveQualityRating(issuerQualityRatingObj);
					}
				}	
			
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("IssuerQualityRating Saved for HIOS Plan id : "+ issuerQualityRatingObj.getPlanHiosId());	
				}
				
				return true;

			}	catch(Exception ex){
				LOGGER.error("Exception occured in saveIssuerQualityRating " + ex.getMessage());
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_QUALITY_RATING_EXCEPTION, null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
			}	
	}
	
	private String getSpecificFormat(){
		SimpleDateFormat sdf = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
		Date date = new TSDate();
		return(sdf.format(date));
	}
	
	private boolean isHIOSPlanIdValid(String HIOSPlanId){		
		boolean returnVal = false;
		try{
			List<Plan> planObjList = new ArrayList<Plan>();
			planObjList = planMgmtService.getPlanByHIOSID(HIOSPlanId);
			if(planObjList.size() > 0){
				returnVal = true;
			}
		}catch(Exception ex){
			LOGGER.error("Exception occoured in isHIOSPlanIdValid " + ex.getMessage());
		}
		
		return returnVal;
	}
	
	private boolean isMatchingIssuerIdToPLan(int issuerId, String issuerPlanNumber) {
		boolean returnVal = false;
		try{
			List<Plan> planObjList = new ArrayList<Plan>();
			planObjList = planMgmtService.getPlanByIssuerIdandIssuerPlanNumber(issuerId, issuerPlanNumber);		
			if(planObjList.size() > 0){
				returnVal = true;
			}
		}catch(Exception ex){
			LOGGER.error("Exception occoured in isMatchingIssuerIdToPLan " + ex.getMessage());
		}	
		return returnVal;
	}

	private boolean hasValidEffectiveDate(String effectiveDate) {

		boolean hasFutureDate = false;

		try {
			Date effectiveStartDate = DateUtil.StringToDate(effectiveDate, REQUIRED_DATE_FORMAT);
			hasFutureDate = (null != effectiveStartDate);
		}
		catch (Exception ex) {
			LOGGER.error("Invalid Date format.");
		}
		return hasFutureDate;
	}

	private boolean isFutureDate(String effectiveDate) {

		boolean hasFutureDate = false;

		try {
			Date effectiveStartDate = DateUtil.StringToDate(effectiveDate, REQUIRED_DATE_FORMAT);
			Calendar calendar = TSCalendar.getInstance();
			calendar.setTime(effectiveStartDate);
			Date futureEffectiveStartDate = calendar.getTime();
			// LOGGER.info("Input Date: " + futureEffectiveStartDate);
			Date todayDate = new TSDate();
			// LOGGER.info("Todays Date: " + todayDate);
			hasFutureDate = futureEffectiveStartDate.after(todayDate);
			// LOGGER.debug("Is future date "+b);
		}
		catch (Exception ex) {
			LOGGER.error("Invalid Date format.");
		}
		return hasFutureDate;
	}

	private boolean isEffectiveDateLessThenOneYear(String effectiveDate) {

		boolean hasEffectiveDateLessThenOneYear = false;

		try {
			Date effectiveStartDate = DateUtil.StringToDate(effectiveDate, REQUIRED_DATE_FORMAT);
			Calendar calendar = TSCalendar.getInstance();
			calendar.setTime(effectiveStartDate);
			Date futureEffectiveStartDate = calendar.getTime();

			Calendar calendarCurrent = TSCalendar.getInstance();
			calendarCurrent.add(Calendar.MONTH, 12);
			hasEffectiveDateLessThenOneYear = futureEffectiveStartDate.before(calendarCurrent.getTime());
		}
		catch (Exception ex) {
			LOGGER.error("Invalid Date format.");
		}
		return hasEffectiveDateLessThenOneYear;
	}
	
	private Double convertToDouble(String rating) {
		if(StringUtils.isNotBlank(rating)){
			return Double.parseDouble(rating);
		}else {
			return null;
		}
	
	}
	
	private boolean isGlobalRatingValid(String ratingString) {
		if(StringUtils.isNotBlank(ratingString) &&  Double.parseDouble(ratingString) <= PlanMgmtConstants.FIVE){
			return true;
		}else {
			return false;
		}
	
	}
	
	private boolean setDateRangesAndUpdateAndInsert(IssuerQualityRating qualityRating,AccountUser user,String HIOSProductId){
		String effectiveStartDate = DateUtil.dateToString(qualityRating.getEffectiveDate(), REQUIRED_DATE_FORMAT);
		IssuerQualityRating issuerQualityRating = qualityRatingRepository.checkEffectiveDateBetweenStartAndEndDate(qualityRating.getIssuerId(),qualityRating.getPlanHiosId(),effectiveStartDate);
		/*LOGGER.info("OldEffectiveStartDate: " + issuerQualityRating.getEffectiveDate());
		LOGGER.info("OldEffectiveEndDate: " + issuerQualityRating.getEffectiveEndDate());*/
		
		//Decrement current Effective Date by 1 day and becomes new End Date for Previous Quality Rating
		Calendar calendar = TSCalendar.getInstance();
		calendar.setTime(qualityRating.getEffectiveDate());
		calendar.add(Calendar.DAY_OF_MONTH, DECREMENT_SPAN_FOR_END_DATE);
		SimpleDateFormat dateFormat = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
		String newEffectiveEndDate = dateFormat.format(calendar.getTime());
		issuerQualityRating.setEffectiveEndDate(DateUtil.StringToDate(newEffectiveEndDate, REQUIRED_DATE_FORMAT));
		
		qualityRating.setPlanHiosId(HIOSProductId);
		qualityRating.setCreatedBy(user.getId());
		qualityRating.setLastUpdatedBy(user.getId());
		
		// update existing record's effective end date by decrementing Current Effective Date - 1 day 
		updateNewEndDateInQualityRating(issuerQualityRating,user);
		
		// insert another set of records with Current Effective Date
		insertNewQualityRating(qualityRating,user);
		
		/*LOGGER.info("NewEffectiveEndDateForOldQualityRating: " + newEffectiveEndDate);
		LOGGER.info("NewEffectiveStartDateForNewQualityRating: " + dateFormat.format(qualityRating.getEffectiveDate()));
		LOGGER.info("NewEffectiveEndDateForNewQualityRating: " + dateFormat.format(qualityRating.getEffectiveEndDate()));*/
		return true;
	}
	
	private void updateNewEndDateInQualityRating(IssuerQualityRating qualityRating, AccountUser user){
		qualityRating.setCreatedBy(user.getId());
		qualityRating.setLastUpdateTimestamp(new TSDate());
		saveQualityRating(qualityRating);
		//LOGGER.info("Quality_Rating_Update_Successfully");
	}
	
	private void insertNewQualityRating(IssuerQualityRating qualityRating, AccountUser user){
		qualityRating.setLastUpdatedBy(user.getId());
		qualityRating.setLastUpdateTimestamp(new TSDate());
		saveQualityRating(qualityRating);
		//LOGGER.info("New_Quality_Rating_Inserted_Successfully");
	}
	
	private void deleteFutureEffectiveDateRecord(int issuerId, String hiosPlanNumber, String effectiveStartDate, int userId ) {
		try{
			qualityRatingRepository.deleteFutureEffectiveDateRecord(issuerId, hiosPlanNumber, effectiveStartDate, userId);
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_QUALITY_RATING_EXCEPTION, null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}
	}
	
	private IssuerQualityRating getQualitRatingByIssuerIdAndHiosProductId(int issuerId, String hiosProductId) {
		IssuerQualityRating issuerQualityRating = null; 
		issuerQualityRating = qualityRatingRepository.getQualitRatingByIssuerAndPlanNumber(issuerId, hiosProductId);
		return issuerQualityRating;
	}
	
	private Date getEffectiveEndDate(Date effectiveStartDate){
		Date effectiveEndDate = null;
		Calendar calendar = TSCalendar.getInstance();
		calendar.setTime(effectiveStartDate);
		calendar.set(Calendar.MONTH, (calendar.get(Calendar.MONTH)+END_DATE_SPAN));
		calendar.add(Calendar.DAY_OF_MONTH, DECREMENT_SPAN_FOR_END_DATE);
		effectiveEndDate = calendar.getTime();
		SimpleDateFormat dateFormat = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
		String stringEffectiveEndDate = dateFormat.format(effectiveEndDate);
		Date requiredEffectiveEndDate = DateUtil.StringToDate(stringEffectiveEndDate, REQUIRED_DATE_FORMAT);
		//LOGGER.info("requiredEffectiveEndDate: " + requiredEffectiveEndDate);
		return requiredEffectiveEndDate;
	}
	
	// prepare string builder with comma separate for quality rating details 
	private String buildQualityRatingStr(IssuerQualityRatingPojo issuerQualityRatingPojo, String stateCode){
		StringBuilder concatRating = new StringBuilder(2056);
		
		// FOR CA STATE EXCHANGE BUILD QUALITY RATING DETAILS STRING
		if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode) || "MN".equalsIgnoreCase(stateCode)){
			concatRating.append(issuerQualityRatingPojo.getGlobalRating());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getSummaryClinicalQualityManagement());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getClinicalEffectiveness());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getAsthmaCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getBehavioralHealth());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getCardiovascularCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getDiabetesCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getDomainPatientSafety());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getPatientSafety());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getPrevention());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getCheckingForCancer());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getMaternalCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getStayingHealthyAdult());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getStayingHealthyChild());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getSummaryEnrolleeExperience());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getDomainAccessToCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getAccessToCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getDomainCareCoordination());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getCareCoordination());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getDomainDoctorAndCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getDoctorAndCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getSummaryPlanEfficiencyAffordabilityAndManagement());
			if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)) {
				concatRating.append(PlanMgmtConstants.STR_COMMA);
				concatRating.append(issuerQualityRatingPojo.getEfficiencyAndAffordability());
				concatRating.append(PlanMgmtConstants.STR_COMMA);
				concatRating.append(issuerQualityRatingPojo.getEfficientCare());
				concatRating.append(PlanMgmtConstants.STR_COMMA);
				concatRating.append(issuerQualityRatingPojo.getPlanService());
				concatRating.append(PlanMgmtConstants.STR_COMMA);
				concatRating.append(issuerQualityRatingPojo.getEnrolleeExperienceWithHealthPlan());	
			}
		}else{
			// FOR NON CA STATE EXCHANGE BUILD QUALITY RATING DETAILS STRING
			concatRating.append(issuerQualityRatingPojo.getGlobalRating());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getAccessRating());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getGettingTimelyAppointmentscare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getGettingCarewithYourPersonalDoctor());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getStayingHealthyRating());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getCheckingForCancer());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getHelpForHealthyBehaviors());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getWellnessCareForChildren());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getPlanServiceRating());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getPlanCustomerService());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getGettingClaimsPaid());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getShoppingServicestoFindAffordableMedicalCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getGettinganInterpreterWhenSeeingTheDoctor());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getMemberComplaintsAppeals());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getClinicalCareRating());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getGettingtheRighSafeCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getDiabetesCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getHeartCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getMentalHealthCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getRespiratoryCare());
			concatRating.append(PlanMgmtConstants.STR_COMMA);
			concatRating.append(issuerQualityRatingPojo.getMaternityCare());
		}
		
		return concatRating.toString();
	}

}
