(function() {
	"use strict";
	var enrollmentHistoryApp = angular.module("enrollmentHistoryApp").controller('EnrollmentHistoryCtrl', EnrollmentHistoryCtrl);
	
	EnrollmentHistoryCtrl.$inject = ['$scope', '$http', '$window','$timeout', 'enrollmentHistoryService', '$state', '$filter'];
	
	function EnrollmentHistoryCtrl($scope, $http, $window, $timeout, enrollmentHistoryService, $state, $filter){
		$scope.init =  init;
		$scope.changeYears = changeYears;
		$scope.init();
		$scope.respEnrollments = {};

		function init(){
			$scope.selectedYear = ALL;
			$scope.userPermissionsList = [];
			$scope.stateCode = $("#stateCode").val();
			$scope.localData = {
				rightPanelLoading : true,
				enrollmentHistoryLoading : true,
				enrollmentHistoryIndicator : false //if true, dont make the enrollment history api call
			};

			$scope.isResendAllowed=isAllowed;
			
			$scope.memberEnrlmntPrmHist = $('#memberEnrlmntPrmHist').val() == "true" ? true: false;
			$scope.memberEnrlmnt834hist = $('#memberEnrlmnt834hist').val() == "true" ? true: false;
			$scope.memberEnrlmntLtst834 = $('#memberEnrlmntLtst834').val() == "true" ? true: false;
			$scope.memberEnrlmntAddnlinfo = $('#memberEnrlmntAddnlinfo').val() == "true" ? true: false;

			
			$scope.isEnrollmentReadOnlyAllowed = $('#isEnrollmentReadOnlyAllowed').val();
			$scope.householdExists = $('#householdExists').val();
			
			if($('#enrollmentDetail').val() !== ''){
				
				processResponse($('#enrollmentDetail').val());
				
				$state.go('premiumHistory', { newEnrollment : $scope.enrollment});
			}
			
			
			var encHouseHoldId = $('#encHouseHoldId').val();
			var datToBeSent={
				encHouseholdId:encHouseHoldId
			};

			datToBeSent[csrfToken.paramName]=csrfToken.paramValue;
			
			enrollmentHistoryService.apiPostResponse("/hix/crm/member/enrollmentandenrolleedetails",$.param(datToBeSent),{ 'Content-Type': 'application/x-www-form-urlencoded'},[function(data){return JSON.parse(data);}]).success(function(response){

				if(response === '' || response.enrollmentDataDtoListMap === '' || response === null || response.status.toUpperCase() === "FAILURE"){
					$scope.enrollments = [];
					return;
				}
				$scope.ReinstateenrollmentData = {};
	  			$scope.ReinstateenrollmentData.showReinstateEnrollmentOptions = false;
				$scope.years = ['ALL'];
				$scope.userPermissionsList = response.userPermissionList;
				
				for (var key in response.enrollmentDataDtoListMap) {
					$scope.enrollments = response.enrollmentDataDtoListMap[key];
					
					//calculate enrollee relationship info 
					for(var i = 0; i < $scope.enrollments.length; i++){
						
//						$scope.enrollments[i].enrollmentHistoryList = [];
						
						$scope.enrollments[i].spouseCount = 0;
						$scope.enrollments[i].selfCount = 0;
						$scope.enrollments[i].dependentCount = 0;
						
						//get year and add in year Set
						var fullYear = new Date($scope.enrollments[i].enrollmentBenefitEffectiveStartDate).getFullYear();
						if($scope.years.indexOf(fullYear) === -1) {
							$scope.years.push(fullYear);
						}
						
						//get UI date format
						$scope.enrollments[i].enrollmentBenefitEffectiveEndDateUI = getUIDateFormat($scope.enrollments[i].enrollmentBenefitEffectiveEndDate);
						$scope.enrollments[i].enrollmentBenefitEffectiveStartDateUI = getUIDateFormat($scope.enrollments[i].enrollmentBenefitEffectiveStartDate);
						
						
						for(var j = 0; j < $scope.enrollments[i].enrolleeDataDtoList.length; j++){
							
							//get UI date format
							$scope.enrollments[i].enrolleeDataDtoList[j].enrolleeEffectiveStartDateUI = getUIDateFormat($scope.enrollments[i].enrolleeDataDtoList[j].enrolleeEffectiveStartDate);
							$scope.enrollments[i].enrolleeDataDtoList[j].enrolleeEffectiveEndDateUI = getUIDateFormat($scope.enrollments[i].enrolleeDataDtoList[j].enrolleeEffectiveEndDate);
							
							if($scope.enrollments[i].enrolleeDataDtoList[j].relationshipToSubscriber.toUpperCase() === 'SELF'){
								$scope.enrollments[i].selfCount++;
							}else if($scope.enrollments[i].enrolleeDataDtoList[j].relationshipToSubscriber.toUpperCase() === 'SPOUSE'){
								$scope.enrollments[i].spouseCount++;
							}else{
								$scope.enrollments[i].dependentCount++;
							}
						}
					}
				}
				
				$scope.years.sort(function(a, b){return b-a});
				
				$scope.localData.rightPanelLoading = false;
				$scope.respEnrollments = $scope.enrollments;
			}).error(function(response){
				$scope.enrollments = [];
				$scope.localData.rightPanelLoading = false;
			});
		}
		
		function changeYears(){
			console.log($scope.enrollments);
			console.log($scope.selectedYear);
			if($scope.selectedYear!=ALL){
				var filteredArray = $scope.respEnrollments.filter(function(el){
					return el.enrollmentBenefitEffectiveStartDate.indexOf($scope.selectedYear)>-1;
				});				
				$scope.enrollments = filteredArray;
			}else{
				$scope.enrollments = $scope.respEnrollments;
			}
		}
		
		
		$scope.sortCreationTimestamp = function(enrollment) {
		    var date = new Date(enrollment.creationTimestamp);
		    return -date.getTime();
		};
		//$scope.overrideRecentHistoryModal=false;
		$scope.overrideLatestTransactionModal=false;
		$scope.showRH=false;
		$scope.showRT=false;
		$scope.successRT=false;
		$scope.failureRT=false;
		$scope.modalForm={
			overrideComment:""
		};

		var selectedEntollmentObj=null;


		$scope.overrideRecentHistory =function(enrollment){
			selectedEntollmentObj=enrollment;
			$scope.showRH=true;
			$("#overrideRecentHistoryModal").modal();
			$scope.modalForm.overrideComment="";
			$scope.submitingResendRH=false;
		};

		$scope.overrideLatestTransaction =function(enrollment){
			selectedEntollmentObj=enrollment;
			$scope.showRT=true;
			$("#overrideLatestTransactionModal").modal();
		    $scope.modalForm.overrideComment="";
		    $scope.submitingResendRT=false;
		};
		$scope.cancelAction =function(modalForm){
			modalForm={};
			$("#overrideRecentHistoryModal").modal("hide");
			$("#overrideLatestTransactionModal").modal("hide");
			$scope.showRH=false;
			$scope.showRT=false;
			$scope.successRH=false;
			$scope.failureRH=false;
			$scope.successRT=false;
			$scope.failureRT=false;
		};
		$scope.removeDisabled=function(){
			if($(".overrideText").val().length<1){
				return;
			}
			$(".cntBtn").removeClass("disabled");
		};

		$scope.submitOverrideRHComment =function(comment){
			if(String(comment).length < 1 || comment==undefined)
			{
				return;
			}
			$scope.showRH=false;
			var encEnrollmentId = selectedEntollmentObj.encEnrollmentId;
			var datToBeSent={
			encEnrollmentId:encEnrollmentId,
			eventType:"ALL",
			comment:comment
			}
			var options={
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			};
			options.headers[csrfToken.paramName]=csrfToken.paramValue;
			$scope.submitingResendRH=true;
			enrollmentHistoryService.apiPostResponse("/hix/crm/member/resend834",$.param(datToBeSent),options.headers,[function(data){return JSON.parse(data);}]).success(function(response){
				$scope.submitingResendRH=false;
				if(response.status === true)
					{
						$scope.successRH=true;
					}
				else if(response.status == false || response === null ||response === '' || response === "undefined"){
					$scope.failureRH=true;
				}
			});



		}
		$scope.submitOverrideRTComment =function(comment){
			if(String(comment).length < 1 || comment==undefined)
			{
				return;
			}
			$scope.showRT=false;
			var encEnrollmentId =selectedEntollmentObj.encEnrollmentId;
			var datToBeSent={
			encEnrollmentId:encEnrollmentId,
			eventType:"LAST",
			comment:comment
			}
			var options={
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			};
			options.headers[csrfToken.paramName]=csrfToken.paramValue;
			$scope.submitingResendRT=true;
			enrollmentHistoryService.apiPostResponse("/hix/crm/member/resend834",$.param(datToBeSent),options.headers,[function(data){return JSON.parse(data);}]).success(function(response){
				$scope.submitingResendRT=false;
				if(response.status === true)
					{
						$scope.successRT=true;
					}
				else if(response.status == false || response === null ||response === ''|| response === "undefined"){
					$scope.failureRT=true;
				}
			});
		};
		
		function processResponse(response){
			$scope.enrollment = angular.fromJson(response);
			
			$scope.enrollment.enrollmentBenefitEffectiveEndDateUI = getUIDateFormat($scope.enrollment.enrollmentBenefitEffectiveEndDate);
			$scope.enrollment.enrollmentBenefitEffectiveStartDateUI = getUIDateFormat($scope.enrollment.enrollmentBenefitEffectiveStartDate);
			
			for(var j = 0; j < $scope.enrollment.enrolleeDataDtoList.length; j++){
				$scope.enrollment.enrolleeDataDtoList[j].enrolleeEffectiveStartDateUI = getUIDateFormat($scope.enrollment.enrolleeDataDtoList[j].enrolleeEffectiveStartDate);
				$scope.enrollment.enrolleeDataDtoList[j].enrolleeEffectiveEndDateUI = getUIDateFormat($scope.enrollment.enrolleeDataDtoList[j].enrolleeEffectiveEndDate);
			}
		}
		
		function getUIDateFormat(dbDate){
			var formattedDate = new Date(dbDate);

			var dd = formattedDate.getDate();
			var mm =  formattedDate.getMonth() + 1;
			var yyyy = formattedDate.getFullYear();
			
			
			dd = dd < 10 ? '0'+dd : dd;
			mm = mm < 10 ? '0'+mm : mm;

			return  mm + "/" + dd + "/" + yyyy;
		}
		
		$scope.getAllSubcriberEvents= function(enrollment){
			var enrollmentDataVar = enrollment.enrollmentId;
			var datToBeSent={
				enrollmentId:enrollmentDataVar
			}
			var options={
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			};
			options.headers[csrfToken.paramName]=csrfToken.paramValue;

			enrollmentHistoryService.apiPostResponse("/hix/crm/member/enrollmentdetails",$.param(datToBeSent),options.headers,[function(data){return JSON.parse(data);}]).success(function(response){
				enrollment.enrollmentHistoryList = response.enrollmentEventHistoryDTOList;
			});
		};
		
		$scope.getEnrollmentSnapshotData = function(enrollment, subscriberEventId, creationDate) {
			var enrollmentDataVar = enrollment.enrollmentId;
			var datToBeSent={
				enrollmentId:enrollment.enrollmentId,
				subscriberEventId: subscriberEventId
			}
			var options={
				headers:{
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			};
			options.headers[csrfToken.paramName]=csrfToken.paramValue;

			enrollmentHistoryService.apiPostResponse("/hix/crm/member/enrollmentsnapshot",$.param(datToBeSent),options.headers,[function(data){return JSON.parse(data);}]).success(function(response){
				$scope.enrollmentDataDTOList = response.enrollmentDataDTOList;
				$scope.enrolees = [];
				
				$scope.today = new Date(creationDate);
				
				for(var i = 0; i < $scope.enrollmentDataDTOList.length; i++) {
					var enroleeCount = $scope.enrollmentDataDTOList[i].enrolleeDataDtoList.length;
					$scope.enrollees = $scope.enrollmentDataDTOList[i].enrolleeDataDtoList;
					$scope.eventCreationDate = new Date($scope.enrollmentDataDTOList[i].eventCreationDate);
				}
				$scope.enrollmentSnapshotData = false;
			});
		}
		
		$scope.goToPremiumHistoryPage = function(enrollment){
			$state.go('premiumHistory', { newEnrollment : enrollment });
		};
		
		$scope.relationshipOrderBy = function(enrollee){
			var order = 4;
			switch(enrollee.relationshipToSubscriber) {
				case 'Self':
					order = 1;
					break;
				case 'Spouse':
					order = 2;
					break;
				case 'Child':
					order = 3;
					break;	
				default:
					order = 4;
			}
			
			return order;
		}
		
		$scope.csrContainer = {
				edit:{title: 'Edit Application', route: '', func: $scope.editApplication, header: csrStorage.edit.header, content: csrStorage.edit.content},
				initiate:{title: 'Initiate Verifications', route: 'initVerifications', func: $scope.initVerifications, header: csrStorage.initiate.header, content: csrStorage.initiate.content},
				rerun:{title: 'Re-run Eligibility', route: 'rerunEligibility', func: $scope.rerunEligibility,header: csrStorage.rerun.header, content: csrStorage.rerun.content},
				cancelTerm:{title: 'Cancel or Terminate Plan', route: 'terminatePlan', func: $scope.terminatePlan, header: csrStorage.cancelTerm.header, content: csrStorage.cancelTerm.content},
				update:{title: 'Update Carrier', route: 'updateCarrier', func: $scope.updateCarrier, header: csrStorage.update.header, content: csrStorage.update.content},
				view:{title: 'View Override History', route: 'getOverideHistory', func: $scope.getOverideHistory, header: csrStorage.view.header, content: csrStorage.view.content},
				specialEnroll:{title: 'Special Enrollment', route: 'saveSepDetails', func: $scope.getOverideHistory, header: csrStorage.specialEnroll.header, content: csrStorage.specialEnroll.content},
				coverageDate:{title: 'Change Coverage Start Date', route: 'saveSepDetails', func: $scope.getOverideHistory, header: csrStorage.coverageDate.header, content: csrStorage.coverageDate.content},
				reinstate:{title: 'Reinstate Enrollment', route: 'reinstateenrollment', func: $scope.reinstateEnrollment, header: csrStorage.reinstate.header, content: csrStorage.reinstate.content},
				overrideSEPDenial:{title: 'Override SEP Denial', route: 'seps/reversedenial/', func: $scope.getOverideHistory, header: csrStorage.overrideSEPDenial.header, content: csrStorage.overrideSEPDenial.content},
				overrideEnrollment:{title:'Override Enrollment', route: '', func:'',header:"Override Enrollment Status", content:"Please specify the reason for overriding enrollment status for this customer. This will help keep track of updates to the customer's record."}
			};
		
		$scope.openCSR = function(caseNum, type){
			$scope.modalForm.csr = true;

			$scope.modalForm.overrideComment = "";
			$scope.csrModalCaseNum = caseNum;
			$scope.currentCsr = $scope.csrContainer[type];
			if(type == 'reinstate') {
				$scope.modalForm.overrideEnrollmentDetails = false;
				$scope.ReinstateenrollmentData.showReinstateEnrollmentOptions = true;
				$scope.csrModalCaseNum = caseNum.ssapCaseNumber;
				$scope.reinsEnrollmentId = caseNum.encEnrollmentId;
				$scope.reinsPlanType = caseNum.planType;
			}

		};
		
		$scope.openOverrideEnrollStatus = function(caseNum, type, overrideEnrollStatus){
			$scope.modalForm.csr = true;
			$scope.csrModalCaseNum = caseNum;
			$scope.currentCsr = $scope.csrContainer[type];
			$scope.ReinstateenrollmentData.showReinstateEnrollmentOptions = false;
			if(type === 'overrideEnrollment'){
				$scope.modalForm.overrideEnrollmentDetails = true;
				$scope.overrideEnrollmentData = {
						"enrollmentId":overrideEnrollStatus.encEnrollmentId,
						"caseNumber":caseNum,
						"familyMembers":$scope.convertEnrolleeListToFamilyMemberList(overrideEnrollStatus.enrolleeDataDtoList)
				};
			}
		};
		
		$scope.convertEnrolleeListToFamilyMemberList = function(enrolleeList){
			var fmList = [];
			enrolleeList.forEach(function(e){
				let fm = {};
				fm.exchgIndivIdentifier = e.memberId;
				fm.name = e.firstName+' '+e.lastName;
				fm.issuerAssignedMemberId = e.issuerAssignedMemberId;
				fm.effectiveStartDate = e.enrolleeEffectiveStartDate;
				fm.effectiveEndDate = e.enrolleeEffectiveEndDate;
				fm.relationship = e.relationshipToSubscriber;
				fmList.push(fm);
			});
			console.log(fmList);
			return fmList;
		};
		
		$scope.getOverrideEnrollmentResult = function(overrideCommentText, overrideEnrollmentData){
			$scope.loader = true;
			var postData = overrideEnrollmentData;
			postData['overrideCommentText'] = overrideCommentText;
			postData['houseHoldId'] = $('#encHouseHoldId').val();
			
			var config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'}
			};
			var url = '/hix/crm/updateEnrollmentStatus';

			$http.post(url, postData, config).success(function(resp){
				$scope.loader = false;
				if(typeof(resp)==="string" && (resp !== "success" && resp !== "failure")){
					//location.href='./indportal';
					$scope.refresh();
					return;
	            }
	            //hide member details modal body
	  			$scope.modalForm.overrideEnrollmentDetails = false;

		  		if(resp!=null && resp === "success"){
		  			//show successful modal body
		  			$scope.modalForm.overrideEnrollmentSuccessful = true;
		  		}else{
		  			$scope.modalForm.subResult = 'failure';
		  		}
			}).error(function(){
				  $scope.modalForm.subResult = 'failure';
			});
		};
		
		$scope.validateLastPremiumPaidDate = function($event, condition){//
			var dateStr = $event.target.value;
			if (dateStr.length === 8){
				dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
	        }
	        if (!validateDate.check(dateStr)){
				$scope.LastPremiumPaidDateErrMsg = "Please enter valid date";
				$scope[condition] = true;
			} else {
				delete $scope.LastPremiumPaidDateErrMsg;
				$scope[condition] = false;
			}
		};
		
		//The max # of characters allowed in the override reason textarea
		$scope.maxLength = 4000;

		$scope.cancelCsrAction = function(){
			$scope.SPEopened = false;

			/*if($scope.ReinstateenrollmentData !== undefined){
				$scope.ReinstateenrollmentData.showReinstateEnrollmentOptions = false;
			}*/

			$scope.csrInputTermDate = false;
			$scope.csrInputOverride = false;
			$scope.termDate = null;
			$scope.changeCovStart = false;
			$scope.SEPStartErr = null;
			$scope.SEPEndErr = null;
			$scope.SEPCovErr = null;
			$scope.termDateErr = null;

			$scope.modalForm.overrideComment = "";
			$scope.modalForm.subResult = null;
			$scope.modalForm.csr = false;
			$scope.modalForm.cancelDate = null;

			$scope.modalForm.sepStartDate = null;
			$scope.modalForm.sepEndDate = null;
			$scope.modalForm.coverageStartDate = null;
			$scope.activefailuremsg = false;
			$scope.overrideNewSEPDate = false;
			$scope.newSEPEndDateErr = false;

			$scope.modalForm.overrideEnrollmentDetails = true;
			$scope.modalForm.overrideEnrollmentSuccessful = false;

			if($scope.overrideEnrollmentData){
				angular.forEach($scope.overrideEnrollmentData.familyMembers, function(value, key){
					value.issuerAssignedMemberId = '';
				});
			}

			$scope.newCoverageEndDate = '';

		};
		
		$scope.enrollmentCsrAction = function($event){
			$($event.target).closest('.enrollmentCsrActionDiv').find('.csrButtonDiv').slideToggle();
		};
		
		
		$scope.showBootstrapModal = function(modalId){
			$('#' + modalId).modal({
				backdrop: 'static',
				keyboard: false
			});
		};
		
		$scope.$on('$viewContentLoaded', function(event) {
		    $timeout(function() {
		    	$scope.bindDatepicker();
		    },0);
		});

		$scope.bindDatepicker = function(){

			/*Bind Datepicker to element
			 TODO:  currently we are using jquery for binding, we have to replace it in angular */
			$('.newSEPEndDate').each(function() {
				$(this).datepicker({
					autoclose : true,
					dateFormat : 'MM/DD/YYYY'
				}).on('changeDate', function(){
					$(this).datepicker('hide');
					$('input[name=newSEPEndDate]').trigger('blur');
				});
			}).on('hide', function(event) {
				event.preventDefault();
				event.stopPropagation();
			});

			$(".datePicker").each(function(){
				$(this).datepicker({
					autoclose: true
				});
			}).on('hide', function(event) {
				event.preventDefault();
				event.stopPropagation();
			});

			/*Setting z-index for date-picker when open in modal dialog */
			$('.js-dateTrigger').on('click', function(){
				setTimeout(function(){
					$('.datepicker').css({zIndex: 1070});
				},80);
			});
		};

		$scope.reinstateEnrollment = function(comment, caseNumber,enrollmentId, planType){
			$scope.loader = true;
			var params = {
					"enrollmentId" : $scope.reinsEnrollmentId,
					"caseNumber" : $scope.csrModalCaseNum,
					"planType" : $scope.reinsPlanType,
					"overrideCommentText" : comment,
					"houseHoldId" : $('#encHouseHoldId').val()
				};
			var config = {
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
				params:params,
				transformResponse: function(data) {
					return data;
				}
			};
			$scope.reinstated = "COMPLETE";
			$scope.modalForm.overrideEnrollmentSuccessful = false;
			var response = $http.post('/hix/crm/reinstateenrollment', null, config);
			response.success(function(data, status, headers, config){
				if(data.indexOf("CANNOT_REINSTATED")>-1) {
					$scope.reinstated = "REINSTATED_ERROR";
					$scope.modalForm.subResult = 'failure';
					return;
		  		}else if (response.data!=null && response.data === "SUBSEQUENT_APP_SUBMITTED"){
					$scope.reinstated = "SUBSEQUENT_APP_ERROR";
					$scope.modalForm.subResult = 'failure';
					return;
		  		}
				$scope.loader = false;
				$scope.refresh();
			});

			response.error(function(data, status, headers, config){
				$scope.modalForm.subResult = 'failure';
			});
		};
		
		$scope.refresh = function(){
			$window.location.reload(true);
		};
	
	}
	
	
	enrollmentHistoryApp.directive("modalShow", function ($parse) {
		return {
		    restrict: "A",
		    link: function (scope, element, attrs) {

		        //Hide or show the modal
		        scope.showModal = function (visible, elem) {
		            if (!elem)
		                elem = element;

		            if (visible)
		                $(elem).modal("show");
		            else
		                $(elem).modal("hide");
		        };

		        //Watch for changes to the modal-visible attribute
		        scope.$watch(attrs.modalShow, function (newValue, oldValue) {
		          scope.showModal(newValue, attrs.$$element);
		        }, true);

		        //Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
		        $(element).bind("hide.bs.modal", function () {
		          $parse(attrs.modalShow).assign(scope, false);
		          if (!scope.$$phase && !scope.$root.$$phase)
		              scope.$apply();
		        });
		    }

		};
		});
	
	var validateDate = function () {
		return {
		  check: function(date){
			if (date === ""){
				return false;
			}


			if (/[dmy]/.test(date)){
				return false;
			}
			var dateArr = date.split("/");

			var daysInMo;

    		switch(dateArr[0]){
    		  case "02":
    		    if (dateArr[2] % 4 === 0){
    		      daysInMo = 29;
    		    } else {
    		      daysInMo = 28;
                }
                  break;
    		  case "04" : case "06" : case "09" : case "11":
    		    daysInMo = 30;
    		    break;
    		  default:
    		    daysInMo = 31;
    		}

    		if (dateArr[0] > 12 || dateArr[1] > daysInMo || dateArr[0] < 1 || dateArr[1] < 1){
    			return false;
    		} else {
    			return true;
    		}
		  }
		};
	};

	enrollmentHistoryApp.factory('validateDate', validateDate);
	
	enrollmentHistoryApp.directive("datePicker", datePicker);
	
	enrollmentHistoryApp.filter('reverse', function() {
		  return function(items) {
		    return items && items.length > 0 ? items.slice().reverse() : items;
		  };
		});

	function datePicker(){
		var directive = {
			restrict : 'AE',
			link : function(scope, element, attrs) {
				element.find('.icon-calendar').click(function(){
					element.datepicker({
						autoclose: true,
						orientation: "bottom"
					});
					element.find('input').off('click focus');
				});
			}
		};

		return directive;
	}
})();
