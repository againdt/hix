/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDTO.XMLValidationStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDetails;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDetails.ValidationStatus;
import com.getinsured.hix.enrollment.repository.IEnrollmentOut1095Repository;
import com.getinsured.hix.enrollment.service.EnrollmentServiceImpl;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentIrsSaxErrorHandler;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.NullCharacterEscapeHandler;
import com.getinsured.hix.enrollment.util.ResourceResolver;
import com.getinsured.hix.enrollment.util.ZipHelper;
import com.getinsured.hix.model.enrollment.Attachment;
import com.getinsured.hix.model.enrollment.EnrollmentOut1095;
import com.getinsured.hix.model.enrollment.FilePayloadBatchTransmission;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.JiraUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author negi_s
 *
 */
@Service("enrollmentAnnualIrsBatchService")
public class EnrollmentAnnualIrsBatchServiceImpl implements
EnrollmentAnnualIrsBatchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnualIrsBatchServiceImpl.class);

	@Autowired private IEnrollmentOut1095Repository enrollmentOut1095Repository;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;

	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsBatchService#generateManifestXML(int)
	 */
	@Override
	public void generateManifestXML(int year, String batchCategoryCode, String xmlSourceLocation, Boolean isFreshRun,
			String originalBatchId, String batchId, String wipFolder, String fileCreationTimeStamp,
			Boolean isResubmission, boolean isGeneratedXMLsValid) throws GIException {

		String filePayloadXMLFolderPath = EnrollmentUtils
		.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
				EnrollmentConstants.TRANSFER_DIRECTION_OUT)
		.append(File.separator).append(EnrollmentConstants.WIP_FOLDER_NAME).toString();
		if(null != xmlSourceLocation && !xmlSourceLocation.isEmpty()){
			filePayloadXMLFolderPath = xmlSourceLocation;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		String batchDate = sdf.format(new Date());
		if (null != batchId && !batchId.isEmpty() && !isResubmission) {
			batchDate = batchId;
		}
		String wipPostFix =  batchDate.replace('|', '-').replace(':', '-');
		if(null != wipFolder && !wipFolder.isEmpty()){
			wipPostFix = wipFolder;
		}
		String baseFilePath = EnrollmentUtils
				.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
						EnrollmentConstants.TRANSFER_DIRECTION_OUT).toString();
		String parentManifestFilePath = baseFilePath +  File.separator + EnrollmentConstants.MANIFEST_FOLDER;
		String parentZipFolderPath = baseFilePath +  File.separator + EnrollmentConstants.ZIP_FOLDER;
		String filePackageBatchXMLPath = parentManifestFilePath + File.separator + wipPostFix;
		String directoryTobeZipped = parentZipFolderPath + File.separator + wipPostFix;
		String archiveFilePayloadPath = baseFilePath +  File.separator + EnrollmentConstants.ARCHIVE_FOLDER;
		String hubFTPZipFilePath = baseFilePath +  File.separator + EnrollmentConstants.VALID_FOLDER;
		String failedFileFoldername = baseFilePath +  File.separator + EnrollmentConstants.INVALID_FOLDER;
		String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);

		/* String filePayloadXMLFolderPath = "C:/Data/IRSContentFileXMLFolder";	
		  String FilePackageBatchXMLPath = "C:/Data/ManifestFileFolder";
		  String directoryTobeZipped = "C:/Data/FilePayloadZipFolderLocation";
		  String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder/";
		  String ArchiveFilePayloadPath = "C:/Data/ArchiveFilePayloadXMLFiles";
		  String environment = "T";
		 */ 
		if(filePayloadXMLFolderPath !=null && parentManifestFilePath != null &&  parentZipFolderPath != null && archiveFilePayloadPath != null &&
				hubFTPZipFilePath != null){
			// clean all the directories:
			
			EnrollmentUtils.createDirectory(filePackageBatchXMLPath);
			EnrollmentUtils.createDirectory(directoryTobeZipped);
			EnrollmentUtils.createDirectory(archiveFilePayloadPath);
			EnrollmentUtils.createDirectory(hubFTPZipFilePath);
			EnrollmentUtils.createDirectory(failedFileFoldername);

			FilePayloadBatchTransmission filePayloadBatchTransmission = new FilePayloadBatchTransmission();						 

			filePayloadBatchTransmission.setBatchID(batchDate);
			String partnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
			String cmsPartnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_CMS_PARTNERID);
			filePayloadBatchTransmission.setBatchPartnerID(cmsPartnerID);
			File filePayloadFolder = new File(filePayloadXMLFolderPath);
			int batchAttachmentTotalQuantity = 0;		 
			if(!("".equals(filePayloadXMLFolderPath.trim())) && (new File(filePayloadXMLFolderPath).exists())){
				try{
					batchAttachmentTotalQuantity = countFilesInFilePayloadFolder(filePayloadFolder);

					filePayloadBatchTransmission.setBatchAttachmentTotalQuantity(batchAttachmentTotalQuantity);
					if(null != batchCategoryCode){
						filePayloadBatchTransmission.setBatchCategoryCode(batchCategoryCode);
					}else{
						filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.IRS_ANNUAL_BATCHCATEGORYCODE_INITIAL);					
					}
					if(!EnrollmentConstants.IRS_ANNUAL_BATCHCATEGORYCODE_INITIAL.equalsIgnoreCase(filePayloadBatchTransmission.getBatchCategoryCode())){
						filePayloadBatchTransmission.setOriginalBatchID(originalBatchId);
					}
					//setBatchTransmissionQuantity is set to 1 always since hub will expect 1 zip file at a time
					filePayloadBatchTransmission.setBatchTransmissionQuantity(1);
					filePayloadBatchTransmission.setTransmissionAttachmentQuantity(batchAttachmentTotalQuantity);
					//Transmission Sequence ID equals 1 in all cases. The Hub rejects the batch if Transmission Sequence ID is greater than 1.
					filePayloadBatchTransmission.setTransmissionSequenceID(1);
					filePayloadBatchTransmission.setReportPeriod(String.valueOf(year));

					List<Attachment> listAttachment = getAllAttachments(filePayloadFolder, year);
					if(listAttachment != null && (!listAttachment.isEmpty())){
						filePayloadBatchTransmission.setAttachment(listAttachment);
					}
					String batchTransmissionPath = filePackageBatchXMLPath + File.separator + "manifest" + ".xml";
					generateFilePayloadIRSXMLReport(filePayloadBatchTransmission, batchTransmissionPath,"FilePayloadIRS1095Report.xsl");	
					String zipFileName = "";
					// put in GI_APP_CONFIG		
					SimpleDateFormat dateFormat = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
					String fileCreationDateTime = dateFormat.format(new Date());
					if(null != fileCreationTimeStamp && !fileCreationTimeStamp.isEmpty()){
						fileCreationDateTime = fileCreationTimeStamp;
					}
					zipFileName = partnerID + "."+ EnrollmentConstants.APP + "." + EnrollmentConstants.FUNC_ANNUAL_INBOUND + "."+ fileCreationDateTime + "."+ environment + "."+ EnrollmentConstants.TRANSFER_DIRECTION_IN;

					//Logging details into out table
					insertBatchIDIntoIrsOutboundTrans(filePayloadFolder, batchDate, batchCategoryCode, year, zipFileName, batchId);

					String hubFTPZipFolderPath = hubFTPZipFilePath + File.separator + zipFileName + ".zip";
					//String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder" + zipFileName + ".zip";
					//move File Payload IRS Reporting XML
					EnrollmentUtils.moveFiles(filePayloadXMLFolderPath,directoryTobeZipped,EnrollmentConstants.FILE_TYPE_XML,null);
					//move file package batch XML file
					EnrollmentUtils.moveFiles(filePackageBatchXMLPath,directoryTobeZipped,EnrollmentConstants.FILE_TYPE_XML,null);
					//Zip all the file content and drop into Hub FTP location
					ZipHelper zippy = new ZipHelper();
					zippy.zipDir(directoryTobeZipped,hubFTPZipFolderPath);
					// File (or directory) with old name
					File oldzipFile = new File(hubFTPZipFolderPath);          
					// File (or directory) with new name
					String newZipFileName =  hubFTPZipFilePath + File.separator + zipFileName;			
					File newZipFile = new File(newZipFileName);   // Rename file (or directory)
					batchDate = batchDate.replace('|', '-');
					batchDate = batchDate.replace(':', '-');
					String timeStampArchiveFileFolder = archiveFilePayloadPath + File.separator + batchDate;
					// FileUtils.cleanDirectory(new File(directoryTobeZipped));
					EnrollmentUtils.moveFiles(directoryTobeZipped,timeStampArchiveFileFolder,EnrollmentConstants.FILE_TYPE_XML,null);
					oldzipFile.renameTo(newZipFile);
					//  deleteDir(directoryTobeZipped);
					//zipDirectory(directoryTobeZipped);
					//				clearParentFolders();
					if(!isGeneratedXMLsValid){
						//					String failedFileFoldername = StringUtils.replace(hubFTPZipFilePath, "IRSAnnualReportHubFTPPath", EnrollmentConstants.ANNUAL_IRS_1095_FAILED_FILE);
						List<File> fileList = new ArrayList<File>();
						fileList.add(EnrollmentUtils.searchFile(hubFTPZipFilePath, zipFileName, Boolean.FALSE));
						EnrollmentUtils.moveFiles(hubFTPZipFilePath,failedFileFoldername, null,fileList);
						throw new GIException("Annual IRS 1095 failed validation against XSD : " + zipFileName);
					}
				}
				catch(Exception e){
					LOGGER.error("generateManifestXML()::"+e.getMessage(),e);
					throw new GIException("Error : " + e.getMessage(), e);
				}
			}else {
				LOGGER.error("Folder does not exist : "+ filePayloadXMLFolderPath);
			}
		}else {
			throw new GIException("Error in generateManifestXML() :: No IRS folder structure defined");
		}
	}

	private void generateFilePayloadIRSXMLReport(FilePayloadBatchTransmission filePayloadBatchTransmission,String outputPath, String xsltPath) throws GIException {
		InputStream strXSLPath = null;
		InputStream strEmptyTagXSLPath = null;
		try {
			LOGGER.info("generateEnrollmentsXMLReport outputPath = "+ outputPath);
			if(filePayloadBatchTransmission!=null && filePayloadBatchTransmission.getAttachment() != null && !filePayloadBatchTransmission.getAttachment().isEmpty()){			
				String strXML = null;
				strXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream(xsltPath);
				strEmptyTagXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream("removeEmptyTags.xsl");
				JAXBContext context = JAXBContext.newInstance(com.getinsured.hix.model.enrollment.FilePayloadBatchTransmission.class,com.getinsured.hix.model.enrollment.Attachment.class,ArrayList.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

				StringWriter writerEnroll = new StringWriter();
				StreamResult resultEnroll = new StreamResult(writerEnroll);

				m.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new NullCharacterEscapeHandler());
				m.marshal(filePayloadBatchTransmission, resultEnroll);
				String strEnrollTrans = new String(writerEnroll.toString());
				//TransformerFactory factory = null;
				//factory =  TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",null);
				TransformerFactory factory = new net.sf.saxon.TransformerFactoryImpl();
				Source xslSource = new StreamSource(strXSLPath);
				StreamSource xmlJAXB = new StreamSource(new StringReader(strEnrollTrans));
				Templates template = factory.newTemplates(xslSource);
				Transformer transformer = template.newTransformer();
				StringWriter writerXSLT = new StringWriter();
				StreamResult resultXSLT = new StreamResult(writerXSLT);
				transformer.transform(xmlJAXB, resultXSLT);

				strXML = new String(writerXSLT.toString());
				StreamSource xmlWithEmpty = new StreamSource(new StringReader(strXML));
				Source xslEmptyTagSource = new StreamSource(strEmptyTagXSLPath);
				Templates templateEmpty = factory.newTemplates(xslEmptyTagSource);
				Transformer transformerEmpty = templateEmpty.newTransformer();
				transformerEmpty.transform(xmlWithEmpty, new StreamResult(new File(outputPath)));
			}	

		} catch (Exception exception) {
			LOGGER.error(" EnrollmentAnnualIrsBatchServiceImpl.generateFilePayloadIRSXMLReport Exception "+ exception.getMessage());
			throw new GIException(" EnrollmentAnnualIrsBatchServiceImpl.generateFilePayloadIRSXMLReport Exception ",exception);

		}finally{
			IOUtils.closeQuietly(strXSLPath);
			IOUtils.closeQuietly(strEmptyTagXSLPath);
		}
	}


	private int countFilesInFilePayloadFolder(File directory) {
		int count = 0;
		for(File file : directory.listFiles()) {
			if(!file.isDirectory()) {
				count++;
			}
		}
		return count;
	}

	private List<Attachment> getAllAttachments(File directory, int year) {
		List<Attachment> listAttachment = new ArrayList<Attachment>();
		int count = 0;
		if(directory.isDirectory()){
			File[] files = directory.listFiles();
			Arrays.sort(files, new Comparator<File>(){
				@Override
				public int compare(File f1, File f2)
				{
					return f1.getName().compareToIgnoreCase(f2.getName());
					//					return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
				} });

			for(File file : files) {
				count = count +1;
				if(!file.isDirectory()) {
					Attachment attachment = new Attachment();
					attachment.setBinarySizeValue(file.length());
					attachment.setDocumentFileName(file.getName());
					String fileName = file.getName();
					String[] parts = fileName.split("\\_");
					String seqId = parts[EnrollmentConstants.TWO];
					attachment.setDocumentSequenceID(seqId);
					// get the MD5 checksum number
					try{
						FileInputStream fis = new FileInputStream(file);
						String digest = null;
						digest = org.apache.commons.codec.digest.DigestUtils.sha256Hex(fis);
						attachment.setSha256HashValueText(digest);
						fis.close();
					}catch(Exception ex){
						LOGGER.error(ex.getMessage(),ex);
					}
					listAttachment.add(attachment);
				}
			}
		}
		return listAttachment;
	}

	/**
	 * Log details into the enrollment out 1095 table
	 * @param directory
	 * @param batchID
	 * @param batchCategoryCode
	 * @param year
	 * @param zipFileName
	 * @param resubScenarioBatchId 
	 */
	public void insertBatchIDIntoIrsOutboundTrans(File directory, String batchID, String batchCategoryCode, int year, String zipFileName, String resubScenarioBatchId) {
		for(File file : directory.listFiles()) {
			if(!file.isDirectory()) {
				String fileName = file.getName();
				if (! fileName.equalsIgnoreCase("manifest.xml")){
					try{
						EnrollmentOut1095 enrollmentOut1095 = enrollmentOut1095Repository.findByDocumentFileNameAndBatchId(fileName, batchID);
						if(null != enrollmentOut1095)
						{
							String enrollmentIds = enrollmentOut1095.getEnrollmentIds();
							String parts[] = enrollmentIds.split("\\,");
							Integer numberOfEnrollments = parts.length;
							//						enrollmentOut1095.setBatchId(batchID);
							enrollmentOut1095.setTotalEnrollmentsPerXml(numberOfEnrollments);
						}else{
							enrollmentOut1095 = new EnrollmentOut1095();
							//Re-submission Case :: query out table to get previous out-bound record
							if(EnrollmentUtils.isNotNullAndEmpty(resubScenarioBatchId)){
								EnrollmentOut1095 previousOutRecord = enrollmentOut1095Repository.findByDocumentFileNameAndBatchId(fileName, resubScenarioBatchId);
								if(null != previousOutRecord){
									enrollmentOut1095.setEnrollmentIds(previousOutRecord.getEnrollmentIds());
									enrollmentOut1095.setTotalEnrollmentsPerXml(previousOutRecord.getTotalEnrollmentsPerXml());
								}
							}
							enrollmentOut1095.setDocumentFileName(fileName);
							enrollmentOut1095.setBatchId(batchID);
							enrollmentOut1095.setSubmissionType("R");// Single Character
						}
						enrollmentOut1095.setDocumentFileSize(file.length());
						enrollmentOut1095.setBatchCategoryCode(batchCategoryCode);
						enrollmentOut1095.setYear(year+"");
						enrollmentOut1095.setPackageFileName(zipFileName);
						enrollmentOut1095Repository.save(enrollmentOut1095);
					}catch(Exception e){
						LOGGER.error("Error saving data in IRSOutBoundTransmission table @ logIRSOutBoundTransmissionInformation" , e);
					}
				}
			}
		}
	}	

	@Override
	public boolean validateGeneratedXMLs(String xmlFileLocation, Integer coverageYear){
		boolean isValid = Boolean.FALSE;

		if(StringUtils.isNotEmpty(xmlFileLocation) && new File(xmlFileLocation).exists()){
			File[] listOfGeneratedXMLs = EnrollmentUtils.getFilesInAFolderByName(xmlFileLocation, EnrollmentConstants.FILE_TYPE_XML);
			if(listOfGeneratedXMLs != null && listOfGeneratedXMLs.length > 0){
				String validationLogs = null;
				try{
					EnrollmentXMLValidationDTO enrollmentXMLValidationDTO = new EnrollmentXMLValidationDTO();
					Gson gson = new GsonBuilder().setPrettyPrinting().create();
					int validFileCount = 0, invalidFileCount = 0;

					ClassLoader classLoader = getClass().getClassLoader();
					
					StringBuilder pathBuilder = new StringBuilder();
					pathBuilder.append(classLoader.getResource("/enrollment").getPath() + EnrollmentConstants.ENROLLMENT_1095_XML_SCHEMA_BASE_DIRECTORY);
					if (coverageYear >= 2018) {
						if (coverageYear == 2018) {
							pathBuilder.append("CY_2018/");
						} else if (coverageYear > 2018) {
							pathBuilder.append("CY_2019/");
						}
					} else {
						pathBuilder.append("Legacy/");
					}
					String initialFilePath =  pathBuilder.toString();
					File xsdFile = EnrollmentUtils.searchFile(initialFilePath, 
							EnrollmentConstants.ENROLLMENT_1095_XML_SCHEMA_FILENAME, 
							Boolean.TRUE);
					if(xsdFile != null){
						enrollmentXMLValidationDTO.setServerURL(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
						enrollmentXMLValidationDTO.setXSDFileName(xsdFile.getName());
						enrollmentXMLValidationDTO.setValidationProcessName("ANNUAL_1095_XML");
						String schemaLang = "http://www.w3.org/2001/XMLSchema";
						SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
						//set prefix if your schema is not in the root of classpath
						ResourceResolver resolver = new ResourceResolver(Paths.get(xsdFile.getParent()).getParent().toString() + File.separator);

						factory.setResourceResolver(resolver);
						Schema schema = factory.newSchema(new StreamSource(xsdFile));
						Validator validator = schema.newValidator();
						EnrollmentIrsSaxErrorHandler errorHandler = new EnrollmentIrsSaxErrorHandler();
						List<EnrollmentXMLValidationDetails> enrollmentXMLValidationDetailsList = new ArrayList<EnrollmentXMLValidationDetails>(); 
						for(File xmlFile : listOfGeneratedXMLs){
							EnrollmentXMLValidationDetails enrollmentXMLValidationDetails = new EnrollmentXMLValidationDetails();
							enrollmentXMLValidationDetails.setXMLFileName(xmlFile.getName());
							try{
								errorHandler.resetHandler();
								validator.setErrorHandler(errorHandler);
								validator.validate(new StreamSource(xmlFile));
								/*errorMessageMap.put(xmlfile.getName(), "VALID");*/
								if(errorHandler.isValid()){
									enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.VALID);	
									validFileCount++;
								}else{
									LOGGER.error("XML file invalid");
									enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
									enrollmentXMLValidationDetails.setErrorMessage(errorHandler.getErrorList().toString());
									invalidFileCount++;
								}
							}
							catch(IOException io){
								LOGGER.error("IO Exception occurred in validateIRSAnnualXML ",io);
								enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
								enrollmentXMLValidationDetails.setErrorMessage( io.getMessage() != null ? io.getMessage() : EnrollmentUtils.shortenedStackTrace(io, 3));
								invalidFileCount++;
							}
							catch(SAXException sax){
								LOGGER.error("SAXException occurred in validateIRSAnnualXML ",sax);
								enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
								enrollmentXMLValidationDetails.setErrorMessage(errorHandler.getErrorList().toString());
								invalidFileCount++;
							}
							enrollmentXMLValidationDetailsList.add(enrollmentXMLValidationDetails);
						}

						enrollmentXMLValidationDTO.setEnrollmentXMLValidationDetailsList(enrollmentXMLValidationDetailsList);
						enrollmentXMLValidationDTO.setTotalFileCount(listOfGeneratedXMLs.length);
						enrollmentXMLValidationDTO.setTotalValidFileCount(validFileCount);
						enrollmentXMLValidationDTO.setTotalInValidFileCount(invalidFileCount);

						if(listOfGeneratedXMLs.length == validFileCount){
							isValid = Boolean.TRUE;
							enrollmentXMLValidationDTO.setValidationStatus(XMLValidationStatus.PASS);
						}
						else{
							enrollmentXMLValidationDTO.setValidationStatus(XMLValidationStatus.FAILED);
						}

						validationLogs = gson.toJson(enrollmentXMLValidationDTO);
						if(!isValid){
							logBug("ANNUAL IRS 1095 XML Job:: XSD Validation failed for some of the generated XMLs: ",validationLogs);
						}

						//Save the Generated logs
//						String annualReportFailurePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_ANNUAL_REPORT_FAILURE_FOLDERPATH);
						StringBuilder stringbuilder = EnrollmentUtils
								.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
										EnrollmentConstants.TRANSFER_DIRECTION_OUT);
						stringbuilder.append(File.separator);
						stringbuilder.append(EnrollmentConstants.XML_VALIDATION_LOG_FOLDER);
						stringbuilder.append(File.separator);
						SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.FILENAME_DATE_FORMAT);
						stringbuilder.append(dateFormat.format(new Date()));

						EnrollmentUtils.createDirectory(stringbuilder.toString());
						stringbuilder.append(File.separator);

						//file Name ANNUAL_1095_XML_VALIDATION_RESULTS.txt
						stringbuilder.append("1095_XML_VALIDATION_RESULTS");
						stringbuilder.append(".txt");

						try(Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(stringbuilder.toString()), "utf-8"))){
							writer.write(validationLogs);
						}
						catch(Exception ex){
							throw new GIException(ex);
						}
					}
					else{
						throw new GIException("Annual IRS 1095- XSD is missing at location: "+initialFilePath);
					}
				}catch(Exception ex){
					LOGGER.error("Exception caught while validating Enrollment 1095 XMLs: ",ex);
					//throw new GIException(ex);
					logBug("Exception occurred while validating Annual IRS 1095 generated XML files", 
							EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FOUR));
				}
			}
			else{
				LOGGER.info("No XML file found at required location, Ignoring validation");
				isValid = Boolean.TRUE;
			}
		}else{
			LOGGER.info("No XML file found at required location, Ignoring validation");
			isValid = Boolean.TRUE;
		}
		return isValid;
	}



	/**
	 * Create directory in given location
	 * 
	 * @author negi_s
	 * @param folderName Folder to be created
	 * @return boolean
	 */
	private boolean createDirectory(String folderName) {
		boolean status = false;
		if (folderName!=null && !("".equals(folderName.trim())) &&!(new File(folderName).exists())) {
			new File(folderName).mkdirs();
			status = true;
		}else if(new File(folderName).exists()){
			status = true;
		}
		return status;
	}

	private void logBug(String subject, String bugDescription)
	{
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
		try{
			if(isJiraEnabled){
				JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
						Arrays.asList(fixVersion), bugDescription, 
					subject,
					null);
		}
		
		}catch(Exception exc){
			LOGGER.error("Unable to log jira for xml validation failure: ", exc);
		}
		 enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.ANNUAL,
				 subject,  bugDescription);
	}

}
