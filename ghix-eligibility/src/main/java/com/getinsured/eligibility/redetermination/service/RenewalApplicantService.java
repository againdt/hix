package com.getinsured.eligibility.redetermination.service;

import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.RenewalApplicant;

public interface RenewalApplicantService {
	RenewalApplicant saveRenewalApplicant(RenewalApplicant renewalApplicant);
	
	void updateHealthRenewalStatus(List<String> applicantGUIdList,Long ssapApplicationId,RenewalStatus renewalStatus) throws GIException;
	
	void updateDentalRenewalStatus(Long ssapApplicationId,RenewalStatus renewalStatus) throws GIException;
	
	Integer getToConsiderMemerCount(RenewalStatus renewalStatus,String planType, Long ssapApplicationId);
	
	Map<Long, String> getEnrollmentsWithGroupReasonCodes(Long ssapApplicationId);
	
	Map<Long, List<String>> getEnrollmentsWithMemberAndFalloutReasonCodes(Long ssapApplicationId, String planType);
}