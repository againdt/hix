<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>

<div class="gutter10">
	<div class="row-fluid">
        	<ul class="page-breadcrumb">
                 <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                 <li><a href="#"><spring:message  code="label.plan"/></a></li>
        		<li><spring:message  code="pgheader.staterating"/></li>
            </ul><!--page-breadcrumb ends-->
            <h1><spring:message  code="pgheader.stateratingmapping"/> <small><spring:message  code="pgheader.headingText"/></small></h1>	
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.fileHistory"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li><spring:message  code="label.viewInfo"/></li>
                </ul>
                <!-- end of side bar -->
                <br />
                <a href="<c:url value="/admin/staterating/upload"/>" class="btn btn-primary span11" id="uploadNewFile" name="uploadNewFile"><spring:message  code='pgheader.uploadNewFile'/></a>
			</div>
			<!-- end of span3 -->
			
			<div class="span9" id="rightpanel">
				<form class="form-horizontal" id="frmZipCountyRating" name="frmZipCountyRating" action="#" method="POST">
						<display:table name="documentList" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
							<display:column property="uploadedFileName" title="File Name" sortable="false" headerClass="graydrkbg"/>
				           	<display:column property="uploadedDate" title="Date Updated"  format="{0,date,MMM dd, yyyy}" sortable="false" headerClass="graydrkbg" />
				           	<display:column property="uploadedBy" title="User Name" sortable="false" headerClass="graydrkbg" />
				           	<display:column property="uploadCommentText" title="<a>Comment</a>" sortable="false" headerClass="graydrkbg"/>
				           
				           <display:setProperty name="paging.banner.placement" value="bottom" />
				           <display:setProperty name="paging.banner.some_items_found" value=''/>
				           <display:setProperty name="paging.banner.all_items_found" value=''/>
				           <display:setProperty name="paging.banner.group_size" value='50'/>
				           <display:setProperty name="paging.banner.last" value=''/>
				           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
				           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
	                       <display:setProperty name="paging.banner.onepage" value=''/>
				           <display:setProperty name="paging.banner.one_item_found" value=''/>
				           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
				           <div class="pagination center">
							<ul>
								<li>{0}</li>
								<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
							</ul>
							</div>
							</span>'/>
						<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
							<div class="pagination center">
								<ul>
									<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
									<li>{0}</li>
								</ul>
							</div>
							</span>'/>
						<display:setProperty name="paging.banner.full" value='
							<div class="pagination center">
								<ul>
									<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
									<li>{0}</li>
									<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
								</ul>
							</div>
							'/>
					</display:table>
				</form>
				
            </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
	</div>
</div>
	
<script type="text/javascript">
function showComment(comment){
	var commentStr=comment.replace(/_/g," ");
	$('<div id="alert" class="modalalert"><div class="modal-body"><iframe id="alert" src="#" style="overflow-x:hidden;width:0%;border:0;margin:0;padding:0;height:25px;"></iframe>'+commentStr+'<br/><br/><button class="btn offset2" data-dismiss="modal" aria-hidden="true">OK</button><br/></div></div>').modal({backdrop:false});
}
</script>

