package com.getinsured.hix.batch.ssap.integration.task;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.ssap.service.UpdateEnrlEventForExternalAppIdsService;

public class UpdateEnrlEventForExternalAppIdsTask extends StepExecutionListenerSupport implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateEnrlEventForExternalAppIdsTask.class);
    private static volatile Boolean isBatchRunning = false;

    private String ssapAppIdsFilePath;
    private UpdateEnrlEventForExternalAppIdsService updateEnrlEventForExternalAppIdsService;
    private String issuerIdsCommaSeparated;
    
    public String getSsapAppIdsFilePath() {
        return ssapAppIdsFilePath;
    }

    public void setIssuerIdsCommaSeparated(String issuerIdsCommaSeparated) {
		this.issuerIdsCommaSeparated = issuerIdsCommaSeparated;
	}

	public void setSsapAppIdsFilePath(String ssapAppIdsFilePath) {
        this.ssapAppIdsFilePath = ssapAppIdsFilePath;
    }

    public UpdateEnrlEventForExternalAppIdsService getUpdateEnrlEventForExternalAppIdsService() {
		return updateEnrlEventForExternalAppIdsService;
	}

	public void setUpdateEnrlEventForExternalAppIdsService(
			UpdateEnrlEventForExternalAppIdsService updateEnrlEventForExternalAppIdsService) {
		this.updateEnrlEventForExternalAppIdsService = updateEnrlEventForExternalAppIdsService;
	}

	@Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        synchronized (isBatchRunning) {
            if(isBatchRunning) {
                return RepeatStatus.FINISHED;
            } else {
                isBatchRunning = true;
            }
        }

        try {
            if(LOGGER.isDebugEnabled()) {
                LOGGER.debug(this.getClass().getName() + " started at " + new Timestamp(System.currentTimeMillis()));
            }
            List<String> issuerIds = null;
            if(StringUtils.isNotEmpty(issuerIdsCommaSeparated))
            {
            	issuerIds = Arrays.asList(issuerIdsCommaSeparated.split("\\s*,\\s*"));
            }
            if (!ssapAppIdsFilePath.isEmpty()) {
            	Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
            	updateEnrlEventForExternalAppIdsService.processUpdateEnrlEvent(ssapAppIdsFilePath,jobId,issuerIds);
            } else {
                LOGGER.debug("execute::input file paths not valid");
            }
        } catch(Exception ex) {
            LOGGER.error("execute::error while executing", ex);
        } finally{
            isBatchRunning = false;
        }

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug(this.getClass().getName() + " finishing at " + new Timestamp(System.currentTimeMillis()));
        }

        return RepeatStatus.FINISHED;
    }
}
