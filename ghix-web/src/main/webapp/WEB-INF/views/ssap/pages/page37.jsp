<%@ include file="../include.jsp" %>
<div id="appscr87" style=" display:none;">
	<div class="row-fluid">
		<div id="OEPart" class="alert alert-info" style="display:none;">
			<p class="txt-bold margin10-t"><spring:message code="ssap.page37.applicationComplete" /></p>
			<p class="margin5-t"><spring:message code="ssap.page37.applicationComplete1" /></p>
			<p class="margin5-t"><spring:message code="ssap.page37.applicationComplete2" /></p>
			<p class="margin5-t"><spring:message code="ssap.page37.applicationComplete3" /></p>
			
			<ol class="margin20-b">
				<li><spring:message code="ssap.page37.applicationCompleteStep1"/></li>
				<li><spring:message code="ssap.page37.applicationCompleteStep2"/></li>
				<li><spring:message code="ssap.page37.applicationCompleteStep3"/></li>
				<li><spring:message code="ssap.page37.applicationCompleteStep4"/></li>
				<li><spring:message code="ssap.page37.applicationCompleteStep5"/></li>
			</ol>
			<p ><spring:message code="ssap.page37.verificationAccess1"/></p>
		</div>
		<div id="specialEnrollmentDenied" style="display:none;">
			<h4><spring:message code="ssap.page37.denialMessageTitle"/></h4>
			<div class="alert alert-info">
				<p class="margin5-t"><spring:message code="ssap.page37.denialPara1"/></p>
				<p class="margin5-t"><spring:message code="ssap.page37.denialPara2"/></p>
				<p class="margin5-t"><spring:message code="ssap.page37.denialPara3"/></p>
				<p class="margin5-t"><spring:message code="ssap.page37.denialPara4"/></p>
				<p class="margin5-t"><spring:message code="ssap.page37.denialPara5"/></p>
				<p class="margin5-t"><spring:message code="ssap.page37.denialPara6"/></p>
			</div>
		</div>
		<%-- <div id="specialEnrollmentGranted">
			<h4><spring:message code="ssap.page37.qualifyMessageTitle"/></h4>
			<div class="alert alert-info">
				<p class="margin5-t" id="ssappage37qualifyPara1"><spring:message code="ssap.page37.qualifyPara1"/></p>
				<p class="margin5-t"><strong><spring:message code="ssap.page37.qualifyPara2"/></strong></p>
				<p class="margin5-t">
					<ol class="margin20-b" style="list-style-type: square">
						<li><spring:message code="ssap.page37.qualifyPara3"/></li>
						<li><spring:message code="ssap.page37.qualifyPara4"/></li>
						<li><spring:message code="ssap.page37.qualifyPara5"/></li> 					
					</ol>
				</p>
			</div>
		</div> --%>
	</div>
</div>