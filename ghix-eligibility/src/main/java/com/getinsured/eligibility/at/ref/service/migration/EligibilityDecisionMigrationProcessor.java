package com.getinsured.eligibility.at.ref.service.migration;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.service.ReferralEligibilityDecisionService;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;

@Component("eligibilityDecisionMigrationProcessor")
public class EligibilityDecisionMigrationProcessor {
	private static final Logger LOGGER = Logger.getLogger(EligibilityDecisionMigrationProcessor.class);

	@Autowired
	@Qualifier("referralEligibilityDecisionMigrationService")
	private ReferralEligibilityDecisionService referralEligibilityDecisionService;
	@Autowired
	private MigrationUtil migrationUtil;

	private String OE_DECISION_HEADER = "oeDecisionFlow";

	public Message<String> execute(Message<String> message) throws Exception {
		String headerValue = ReferralEligibilityDecisionService.ERROR_HANDLER;
		ReferralResponse referralResponse = new ReferralResponse();
		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);
		try {
			LOGGER.info("EligibilityDecisionMigrationProcessor starts for " + erpResponse.getSsapApplicationPrimaryKey());
			final boolean blnComplete = referralEligibilityDecisionService.execute(erpResponse.getSsapApplicationPrimaryKey(), erpResponse.isCmrAutoLinking());
			headerValue = blnComplete ? ReferralEligibilityDecisionService.SUCCESS_HANDLER : ReferralEligibilityDecisionService.NF_LINK_HANDLER;
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_ELIGIBILITY_DECISION + erpResponse.getApplicationID());
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_ELIGIBILITY_DECISION).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_ELG_DECISION_PROCESSOR);
			migrationUtil.persistGiMonitorId(erpResponse.getSsapApplicationPrimaryKey(), e);
		} finally {
			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, erpResponse.getSsapApplicationPrimaryKey());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, erpResponse.getGiWsPayloadId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID, erpResponse.getCompareToApplicationId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID, erpResponse.getApplicationsWithSameId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION, erpResponse.getApplicationExtension());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY, erpResponse.getAccountTransferCategory());
			referralResponse.getData().put(ReferralProcessingConstants.REFERRAL_AUTOLINKING, erpResponse.isCmrAutoLinking());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE, erpResponse.isMultipleCmr());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR, erpResponse.isNonFinancialCmr());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID, erpResponse.getNonFinancialCmrId());
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("EligibilityDecisionMigrationProcessor ends for " + erpResponse.getSsapApplicationPrimaryKey() + ", headerValue - " + headerValue);

		return MessageBuilder.withPayload(response).copyHeadersIfAbsent(message.getHeaders()).setHeaderIfAbsent(OE_DECISION_HEADER, headerValue).setHeader("contentType", "application/xml").setHeader("Content-Length", response.length()  ).setHeader("content-length", response.length() ).build();

	}
}
