package com.getinsured.cap.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.getinsured.calldata.dto.CallDataResponseDTO;
import com.getinsured.cap.service.CallDataService;
import com.getinsured.hix.dto.calldata.CallDataIncomingRequestDTO;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.google.gson.Gson;


@Controller
@RequestMapping("/api")
public class AgentGroupController {

    @Autowired
    private ZipCodeService zipCodeService;
    
    @Autowired private Gson platformGson;

    @Autowired private CallDataService callDataService;
    
    @RequestMapping(value = "agentGroup/callParameters", method = RequestMethod.POST , consumes = "application/json" , produces = "application/json")
    public
    @ResponseBody
    ResponseEntity<CallDataResponseDTO> saveCallDataRequest(@RequestBody String payloadJson) {
    	CallDataResponseDTO callDataResponse = new CallDataResponseDTO();
    	
    	
    	if(StringUtils.isEmpty(payloadJson) || "{}".equalsIgnoreCase(payloadJson) || (StringUtils.isEmpty(payloadJson.replaceAll("\\{", "").replaceAll("\\}", "").trim()))){
    		callDataResponse.setResponse("FAIL");
        	callDataResponse.setFailReason("Received Empty Request.");
        	
			return new ResponseEntity<CallDataResponseDTO>(callDataResponse,HttpStatus.PRECONDITION_FAILED);	
    	}
    	else if(!doesRequestContainInteractionId(payloadJson)){
    		callDataResponse.setResponse("FAIL");
        	callDataResponse.setFailReason("Interaction_id is not present in the request.");
        	
			return new ResponseEntity<CallDataResponseDTO>(callDataResponse,HttpStatus.PRECONDITION_FAILED);	
    	}
    	else if(callDataService.doesInteractionIdExistInDb(payloadJson)){
    		callDataResponse.setResponse("FAIL");
        	callDataResponse.setFailReason("Interaction_id already exists.");
        	
			return new ResponseEntity<CallDataResponseDTO>(callDataResponse,HttpStatus.PRECONDITION_FAILED);
    	}
    	else{
    			try{
        			String status = callDataService.modifyAndSaveRequest(payloadJson);
        			callDataResponse.setResponse(status);
                	callDataResponse.setFailReason("");
                	return new ResponseEntity<CallDataResponseDTO>(callDataResponse,HttpStatus.OK);	
        		}
        		catch(Exception e){
        			callDataResponse.setResponse("FAIL");
                	callDataResponse.setFailReason("Request could not be completed. Caught Exception"+e.getMessage());
        			return new ResponseEntity<CallDataResponseDTO>(callDataResponse,HttpStatus.INTERNAL_SERVER_ERROR);
        		}	
    	}
    	
    	
    }
    

	private boolean doesRequestContainInteractionId(String payLoadJson) {
		CallDataIncomingRequestDTO callDataIncomingRequest = platformGson.fromJson(payLoadJson,CallDataIncomingRequestDTO.class);
		if(StringUtils.isEmpty(callDataIncomingRequest.getInteraction_id())){
			return Boolean.FALSE;
		}
		else{
			return Boolean.TRUE;
		}
	}
	
}




