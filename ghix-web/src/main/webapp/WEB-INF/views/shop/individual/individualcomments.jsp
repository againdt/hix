<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ page import="java.util.HashMap" %>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/style.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<%--<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />--%>

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>


<c:set var="individualUserId" ><encryptor:enc value="${individualUser.id}" isurl="true"/> </c:set>
<c:set var="isIndividualUserExists" value="${isIndividualUserExists}" ></c:set>
<c:set var="isConsumerActivated" value="${isConsumerActivated}" ></c:set>
<script type="text/javascript">
	$(function() {
		$(".newcommentiframe").click(function(e){
	        e.preventDefault();
	        var href = $(this).attr('href');
	        if (href.indexOf('#') != 0) {
		        $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:270px;" scrolling="no"></iframe></div></div>').modal({
		        	backdrop: 'static'
		        });
			}
		});
	});
	
	function closeCommentBox() {
		//$("#newcommentiframe").remove(); //not working in FF changed to below line also HIX-57529 fixed
		$("#newcommentiframe").modal('hide');
		window.location.reload(true);
	}
	function closeIFrame() {
		$("#newcommentiframe").modal('hide');
		window.location.reload(true);
	}
</script>


	
<div class="gutter10-lr">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a
					href="<c:url value="/broker/individuals"/>"><spring:message
							code="label.individuals" /></a></li>
				<li>Active</li>
				<li>${individualUser.firstName} ${individualUser.lastName}</li>
				<li>Comments</li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>
	

<!--  Latest UI -->

<c:if test="${message != null}">
<div class="errorblock alert alert-info">
	<p>
		${message}
	</p>
</div>
</c:if>

		<h1>${individualUser.firstName} ${individualUser.lastName}</h1>
	<div class="row-fluid">
		<c:set var="encryptedId" ><encryptor:enc value="${individualUser.id}" isurl="true"/> </c:set>
			<div id="sidebar" class="span3">
				<h4 class="header"></h4>					
				<ul class="nav nav-list">
					<c:choose>
						<c:when test="${userActiveRoleName != 'admin' && userActiveRoleName != 'broker_admin' && userActiveRoleName != 'broker'}">	
							<li class="link"><a href="/hix/entity/individualcase/${individualUserId}"><spring:message code="label.employeeDetails.Summary"/></a></li>													
						</c:when>
						<c:otherwise>												
							<li class="link"><a href="/hix/individual/individualcase/${individualUserId}"><spring:message code="label.employeeDetails.Summary"/></a></li>
						</c:otherwise>
					</c:choose>		                 	
                  	<li class="active"><spring:message code="label.employeeDetails.Comments"/></li>
                </ul>
				<br>
					<h4 class="header"><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
						<ul class="nav nav-list">
						
							<li><a name="addComment" href="<c:url value="/individual/newcomment?target_id=${individualUserId}&target_name=DESIGNATEBROKER&individualName=${individualUser.firstName} ${individualUser.lastName}"/>" id ="addComment" class="newcommentiframe"> <i class="icon-comment"></i><spring:message code="label.employeeDetails.NewComments"/></a></li>
							
							<c:if test="${userActiveRoleName != 'admin' && userActiveRoleName != 'broker_admin' && userActiveRoleName != 'assisterenrollmententity' && !CA_STATE_CODE}">
		                  		<li><a href="#viewindModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.indViewIndividualAccount"/></a></li>
							</c:if>
							
							<c:if test="${userActiveRoleName != 'assisterenrollmententity' && !CA_STATE_CODE}"> 	
								<c:if test="${isConsumerActivated == false}"> 	
									<li><a name="resendActivationLink" href="#" id ="resendActivationLink" class=""><i class="icon-envelope"></i><spring:message code="label.agent.individual.activationLink"/></a></li>
								</c:if>
							</c:if>
		                </ul>
		</div>
		<!-- Modal -->
		
		<div id="rightpanel" class="span9">
		
			<form class="form-vertical" id="commentForm" name="commentForm" action="" method="POST">
				<table class="table">
					<thead>
						<tr class="header">
							<th><h4><spring:message code="label.employeeDetails.Comments"/></h4></th>
							<%-- <th class="txt-right"><a class="btn btn-primary showcomments"><spring:message code="label.brkaddcomments"/></a></th> --%>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2" class="">					
								<comment:view targetId="${individualUser.id}" targetName="DESIGNATEBROKER">
								</comment:view>
								
								<jsp:include page="../../platform/comment/addcomments.jsp">
									<jsp:param value="" name=""/>
								</jsp:include>
								<input type="hidden" value="DESIGNATEBROKER" id="target_name" name="target_name" /> 
								<input type="hidden" value="${individualUser.id}" id="target_id" name="target_id" />
								<input type="hidden" name="userActiveRoleName" id="userActiveRoleName" value="${userActiveRoleName}" />
								<input type="hidden" name="castatecode" id="castatecode" value="${CA_STATE_CODE}" />    
							</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="individualName" id="individualName" value="${individualUser.firstName} ${individualUser.lastName}" />
			</form>
		</div>
	</div>	
		 
		<!-- Modal -->
		
      <!-- Modal -->
      <div id="viewindModal" class="modal hide fade" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">&times;</button>
          <h3 id="myModalLabel"><spring:message code="label.indViewIndividualAccount"/></h3>
        </div>
        <div class="modal-body">
          <p><spring:message code="label.indClickingIndividualView"/> ${individualUser.firstName} ${individualUser.lastName}.<spring:message code="label.IndThroughThisPortal"/>
          </p>
          <p><spring:message code="label.indProceedtoIndividualview"/></p>
        </div>
        
        <c:set var="idAsString">${individualUser.id}</c:set>
        
        <jsp:useBean id="moduleParamsMap" class="java.util.HashMap"/>
		<c:set target="${moduleParamsMap}" property="switchToModuleName" value="individual" />
		<c:set target="${moduleParamsMap}" property="switchToModuleId" value="${idAsString}" />
		<c:set target="${moduleParamsMap}" property="switchToResourceName" value="${individualUser.firstName} ${individualUser.lastName}" />
		<surl:secureUrl paramMap="${moduleParamsMap}" var="ecyptedModuleParamsMap"/>
		
        <form action="<c:url value='/account/user/switchUserRole'/>" id="frmPopup" name="frmPopup">
        	<div class="modal-footer clearfix">
        			<label for="showPopupInFuture" class="aria-hidden">Show pop up in future checkbox</label> <!-- Accessibility Use -->       	
	 				<input class="pull-left"  type="checkbox" id="showPopupInFuture" name="showPopupInFuture"/>
	    			<div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/></div>
				    <button class="btn btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.brkCancel"/></button>
					
					<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="individual"/>" />
					<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${individualUser.id}"/>" />
					<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="<encryptor:enc value="${individualUser.firstName} ${individualUser.lastName}"/>" />
					
					<input type="hidden" name="ref" id="ref" value="${ecyptedModuleParamsMap}" />
					
				   <button class="btn btn-primary" type="submit" ><spring:message code="label.indIndividualView"/></button >
			</div>			
		</form>
      </div>
      <!-- Modal end -->	
	</div>
<!--  Latest UI -->
<script type="text/javascript">
$("document").ready(function (){
	$("#resendActivationLink").click(function () {
		   if(${isConsumerActivated}) {
			   $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p> ${individualUser.firstName}&nbsp;${individualUser.lastName}&nbsp;<spring:message code="label.agent.individual.activationLink.message.denied"/></p>'
				       +'</div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
		   }
		   else  if(${isIndividualUserExists}) {
			   $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p> ${individualUser.firstName}&nbsp;${individualUser.lastName}&nbsp;<spring:message code="label.agent.individual.activationLink.message.denied"/></p>'
				       +'</div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
		   }
		   else {
			   	
			   $.post("<c:url value='/broker/sendconsumeractivationlink/${encryptedId}'><c:param name='${df:csrfTokenParameter()}'> <df:csrfToken plainToken='true'/> </c:param></c:url>", null,
				   function(data,status){
					   if(data.indexOf('Missing CSRF token') != -1){
							$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
							return false;
						}
						if(status === 'success') {
					    	 if(-1 == data.search("SUCCESS")) { 
					    		 $('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px; "><P>   <spring:message code="label.agent.individual.activationLink.message.success.negative.pre"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}<spring:message code="label.agent.individual.activationLink.message.success.negative.post"/>' + data + '.</P></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					    	 }
					    	 else {
					    		 $('<div class="modal popup-address" ><div class="modal-header " style="border-bottom:0; "><h3>Success!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p><spring:message code="label.agent.individual.activationLink.message.success.positive"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					    	 }
					     }
					     else {
					    	 $('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p>   <spring:message code="label.agent.individual.activationLink.message.failure"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					     }
					   });
		   } 
		  
	});
});

</script>
