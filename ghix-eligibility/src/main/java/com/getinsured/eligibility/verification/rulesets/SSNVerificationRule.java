package com.getinsured.eligibility.verification.rulesets;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


/**
 * Encapsulates Death Verification Rule methods
 */
@Component
public class SSNVerificationRule {

	private static final Logger LOGGER = LoggerFactory.getLogger(SSNVerificationRule.class);
	
	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;

	public static void main(String[] asdf) throws Exception {
		SSNVerificationRule ssnVerificationRule = new SSNVerificationRule();
		//ssnVerificationRule.verifySSN(SSAPUtils.loadSSAPJSON(), SSAPUtils.loadHUBJSON());
	}

	/**
	 * This rule will verify Social Security Number Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponseJson
	 * @throws Exception 
	 */
	public boolean verifySSN(String ssapJson, String hubResponseJson, String pPersonId) throws Exception {
		LOGGER.info("Verify SSN Rule : Starts ");

		String status = null;
		boolean verificationStatus = false;
		JsonArray householdMembers;
		JsonObject householdMember;
		JsonObject socialSecurityCard;
		String socialSecurityCardHolderIndicator;
		String reasonableExplanationForNoSSN;
		String personId;
		boolean personChecked = false;
		JsonObject singleStreamlinedApplication;

		try {
			singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(ssapJson);
			
			householdMembers = SSAPUtils.getFilteredHouseholdMembers(singleStreamlinedApplication);
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
				
				try {
					if(personId.equals(pPersonId)){
						socialSecurityCard = householdMember.get(SSAPConstants.SOCIAL_SECURITY_CARD).getAsJsonObject();
						socialSecurityCardHolderIndicator = socialSecurityCard.get(SSAPConstants.SOCIAL_SECURITY_CARD_HOLDER_INDICATOR).getAsString();
						
						if(!SSAPUtils.isEmpty(socialSecurityCardHolderIndicator) && SSAPConstants.TRUE.equals(socialSecurityCardHolderIndicator)){
							if(!SSAPUtils.isEmpty(hubResponseJson)){
								String sSNVerificationIndicator = SSAPUtils.searchHubResponseJson(
										SSAPConstants.SSN_VERIFICATION_INDICATOR, hubResponseJson);

								if (!SSAPUtils.isEmpty(sSNVerificationIndicator) && SSAPConstants.TRUE.equals(sSNVerificationIndicator)) {
									status = SSAPConstants.VERIFIED;
								} else {
									status = SSAPConstants.NOT_VERIFIED;
								}
							} else {
								status = SSAPConstants.PENDING;
							}
						} else {
							reasonableExplanationForNoSSN = socialSecurityCard.get(SSAPConstants.REASONABLE_EXPLANATION_FOR_NO_SSN).isJsonNull()?null:socialSecurityCard.get(SSAPConstants.REASONABLE_EXPLANATION_FOR_NO_SSN).getAsString();
							if(reasonableExplanationForNoSSN != null && checkNoSSNReasons(reasonableExplanationForNoSSN)){
								status = SSAPConstants.VERIFIED;
							} else {
								status = SSAPConstants.NOT_VERIFIED;
							}
						}
						personChecked = true;
					}

					if(personChecked)break;
				} catch(Exception exception){
					LOGGER.error("Exception occurred while Verifying SSN : Person : "+pPersonId, exception);
					String rule = SSAPConstants.RULES.SSN_VERIFICATION_RULE.toString();
					giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
							hubResponseJson, null, null, null, rule), rule));
				}
			}
			
			if(!SSAPUtils.isEmpty(status)){
				verificationRulesService.updateVerificationStatus(Long.valueOf(SSAPUtils.getApplicationIdFromSsapJson(singleStreamlinedApplication)),
						Long.valueOf(pPersonId), status, SSAPConstants.SSN_VERIFICATION_RULE);
				verificationStatus = true;
			} 
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying SSN : ", exception);
			throw exception;
		}

		LOGGER.info("Verify SSN Rule : Ends ");
		
		return verificationStatus;
	}
	
	private boolean checkNoSSNReasons(String reason){
		boolean checkReason = false;
		
		try {
			if(getReasons().contains(reason)){
				checkReason = true;
			}
		} catch(Exception exception){
			throw exception;
		}
		return checkReason;
	}
	
	private static List<String> getReasons(){
		List<String> reasons = new ArrayList<>();
		
		try {
			reasons.add("JUST_APPLIED");
			reasons.add("RELIGIOUS_EXCEPTION");
			reasons.add("ILLNESS_EXCEPTION");
			reasons.add("CITIZEN_EXCEPTION");
		} catch(Exception exception){
			throw exception;
		}
		return reasons;
	}
}
