package com.getinsured.hix.dto.employees;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Transient;

/**
 * Data Transfer Object Class for Employee specific information
 * @author gawade_s
 *
 */
public class EmployeeDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;

	private int brokerId;
	
	private String employeeName;
	
	private String employeeFirstName;
	
	private String employeeLastName;
	
	private String companyName;
	
	private String designationStatus;
	
	private String enrollmentStatus;
	
	private Date effectiveDate;
	
	private Date renewalDate;
	
	private Date inactiveSinceDate;
	
	private String inactiveSinceFromDate;
	
	private String inactiveSinceToDate;
	
	private String employeePhoneNumber;
	
	private String employeeEmail;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the brokerId
	 */
	public int getBrokerId() {
		return brokerId;
	}

	/**
	 * @param brokerId the brokerId to set
	 */
	public void setBrokerId(int brokerId) {
		this.brokerId = brokerId;
	}

	/**
	 * @return the employeeName
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @param employeeName the employeeName to set
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @return the employeeFirstName
	 */
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}

	/**
	 * @param employeeFirstName the employeeFirstName to set
	 */
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}

	/**
	 * @return the employeeLastName
	 */
	public String getEmployeeLastName() {
		return employeeLastName;
	}

	/**
	 * @param employeeLastName the employeeLastName to set
	 */
	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the designationStatus
	 */
	public String getDesignationStatus() {
		return designationStatus;
	}

	/**
	 * @param designationStatus the designationStatus to set
	 */
	public void setDesignationStatus(String designationStatus) {
		this.designationStatus = designationStatus;
	}

	/**
	 * @return the enrollmentStatus
	 */
	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	/**
	 * @return the effectiveDate
	 */
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the renewalDate
	 */
	public Date getRenewalDate() {
		return renewalDate;
	}

	/**
	 * @param renewalDate the renewalDate to set
	 */
	public void setRenewalDate(Date renewalDate) {
		this.renewalDate = renewalDate;
	}

	/**
	 * @return the inactiveSinceDate
	 */
	public Date getInactiveSinceDate() {
		return inactiveSinceDate;
	}

	/**
	 * @param inactiveSinceDate the inactiveSinceDate to set
	 */
	public void setInactiveSinceDate(Date inactiveSinceDate) {
		this.inactiveSinceDate = inactiveSinceDate;
	}

	/**
	 * @return the inactiveSinceFromDate
	 */
	public String getInactiveSinceFromDate() {
		return inactiveSinceFromDate;
	}

	/**
	 * @param inactiveSinceFromDate the inactiveSinceFromDate to set
	 */
	public void setInactiveSinceFromDate(String inactiveSinceFromDate) {
		this.inactiveSinceFromDate = inactiveSinceFromDate;
	}

	/**
	 * @return the inactiveSinceToDate
	 */
	public String getInactiveSinceToDate() {
		return inactiveSinceToDate;
	}

	/**
	 * @param inactiveSinceToDate the inactiveSinceToDate to set
	 */
	public void setInactiveSinceToDate(String inactiveSinceToDate) {
		this.inactiveSinceToDate = inactiveSinceToDate;
	}

	/**
	 * @return the employeePhoneNumber
	 */
	public String getEmployeePhoneNumber() {
		return employeePhoneNumber;
	}

	/**
	 * @param employeePhoneNumber the employeePhoneNumber to set
	 */
	public void setEmployeePhoneNumber(String employeePhoneNumber) {
		this.employeePhoneNumber = employeePhoneNumber;
	}

	/**
	 * @return the employeeEmail
	 */
	public String getEmployeeEmail() {
		return employeeEmail;
	}

	/**
	 * @param employeeEmail the employeeEmail to set
	 */
	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}
}
