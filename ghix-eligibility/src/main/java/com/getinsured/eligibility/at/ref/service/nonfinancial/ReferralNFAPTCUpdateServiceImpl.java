package com.getinsured.eligibility.at.ref.service.nonfinancial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerBaseService;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;

@Component("referralNFAPTCUpdateService")
@Scope("singleton")
public class ReferralNFAPTCUpdateServiceImpl extends LceProcessHandlerBaseService implements ReferralNonFinancialService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralNFAPTCUpdateServiceImpl.class);

	@Override
	@ReferralTransactionAnno
    public boolean execute(NFProcessDTO nfProcessDTO) throws Exception {
		//this flows and class are not used any more
		final SsapApplication currentApplication = loadCurrentApplication(nfProcessDTO.getCurrentApplicationId());

		if (currentApplication == null) {
			LOGGER.error(ReferralConstants.NO_SSAP_FOUND + nfProcessDTO.getCurrentApplicationId());
			throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + nfProcessDTO.getCurrentApplicationId());
		}

		final SsapApplication nonFinancialApplication = loadCurrentApplication(nfProcessDTO.getNfApplicationId());

		if (nonFinancialApplication == null) {
			LOGGER.error(ReferralConstants.NO_SSAP_FOUND + nfProcessDTO.getNfApplicationId());
			throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + nfProcessDTO.getNfApplicationId());
		}
		
		final AptcUpdate aptcUpdate = populateAptcUpdateRequest(currentApplication, nonFinancialApplication.getId(),null);
		if (aptcUpdate == null) {
			LOGGER.info("Health/Dental enrollment was not found - " + currentApplication.getId());
			failedAPTCChanges(currentApplication);
		} else {
			aptcUpdate.setIsFinancialConversion(true);
			final String status = executeAptcChangesApi(currentApplication, nonFinancialApplication.getId(), aptcUpdate);
			if (GhixConstants.RESPONSE_SUCCESS.equals(status)) {
				successAPTCChanges(currentApplication, nonFinancialApplication);
			} else {
				failedAPTCChanges(currentApplication);
			}
		}
	    return true;
    }
	private void failedAPTCChanges(SsapApplication currentApplication) {
		LOGGER.info("Handle Failure APTC changes case for Application id - " + currentApplication.getId());
		updateAllowEnrollment(currentApplication, Y);
		triggerNFtoFinancialConversionManualEmail(currentApplication);
	}
	
	private void successAPTCChanges(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Handle Success APTC changes case for Application id - " + currentApplication.getId());
		final String status = moveEnrollmentToNewApplication(currentApplication, enrolledApplication);
		if (!status.equals(GhixConstants.RESPONSE_SUCCESS)) {
			throw new GIRuntimeException(LceProcessHandlerService.ERROR_MOVE_ENROLLMENT + currentApplication.getId());
		}

		closeSsapApplication(enrolledApplication);
		updateCurrentAppToEN(currentApplication);
		updateAllowEnrollment(currentApplication, Y);
		triggerNFtoFinancialConversionEmail(currentApplication);
	}
}
