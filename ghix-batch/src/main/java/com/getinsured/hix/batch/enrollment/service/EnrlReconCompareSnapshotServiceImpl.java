/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static net.javacrumbs.jsonunit.JsonAssert.when;
import static net.javacrumbs.jsonunit.core.Option.TREATING_NULL_AS_ABSENT;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.ReconEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.ReconMemberDTO;
import com.getinsured.hix.dto.enrollment.ReconMonthlyPremiumDTO;
import com.getinsured.hix.enrollment.repository.IEnrlReconDiscrepancyRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconLkpRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSnapshotRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSummaryRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy.DiscrepancyStatus;
import com.getinsured.hix.model.enrollment.EnrlReconLkp;
import com.getinsured.hix.model.enrollment.EnrlReconSnapshot;
import com.getinsured.hix.model.enrollment.EnrlReconSummary;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.google.gson.Gson;

/**
 * @author panda_p
 *
 */
@Service("enrlReconCompareSnapshotService")
public class EnrlReconCompareSnapshotServiceImpl implements EnrlReconCompareSnapshotService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconCompareSnapshotServiceImpl.class);

	@Autowired private IEnrlReconSnapshotRepository iEnrlReconSnapshotRepository;
	@Autowired private IEnrlReconDiscrepancyRepository iEnrlReconDiscrepancyRepository;
	@Autowired private IEnrlReconLkpRepository iEnrlReconLkpRepository;
	@Autowired private IEnrlReconSummaryRepository iEnrlReconSummaryRepository;
	//@Autowired private IEnrollmentRepository iEnrollmentRepository;

	private static String BAD_MAILING_ADDRESS_CODE = "8100_AA";
	private static String MEMBER_NOT_IN_YHI_SYSTEM_CODE = "8000_AA";
	private static String MEMBER_NOT_IN_CARRIER_SYSTEM_CODE = "8000_AB";
	private static String OVERALLOCATION_OF_APTC_JANUARY_CODE ="9500_AA";
	private static String OVERALLOCATION_OF_APTC_FEBURARY_CODE ="9500_AB";
	private static String OVERALLOCATION_OF_APTC_MARCH_CODE ="9500_AC";
	private static String OVERALLOCATION_OF_APTC_APRIL_CODE ="9500_AD";
	private static String OVERALLOCATION_OF_APTC_MAY_CODE ="9500_AE";
	private static String OVERALLOCATION_OF_APTC_JUNE_CODE ="9500_AF";
	private static String OVERALLOCATION_OF_APTC_JULY_CODE ="9500_AG";
	private static String OVERALLOCATION_OF_APTC_AUGUST_CODE ="9500_AH";
	private static String OVERALLOCATION_OF_APTC_SEPTEMBER_CODE ="9500_AI";
	private static String OVERALLOCATION_OF_APTC_OCTOBER_CODE ="9500_AJ";
	private static String OVERALLOCATION_OF_APTC_NOVEMBER_CODE ="9500_AK";
	private static String OVERALLOCATION_OF_APTC_DECEMBER_CODE ="9500_AL";
	private static String EFFECTUATION_STATUS_CODE ="8200_AA";
	private static String ENROLLMENT_CANCELLED_IN_HIX_CODE="8200_AC";
	private static String ENRL_MISSING_IN_HIX ="8000_AC";
	private static String ENRL_MISSING_IN_FILE="8000_AD";
	private static String SUBSCRIBER_BEGIN_DATE_CODE ="2300_AB";
	private static String SUBSCRIBER_END_DATE_CODE="2300_AC";
	private static String ENROLLMENT_CANCELLED_IN_ISSUER_CODE="8200_AD";

	private static String RECORD_CODE = "recordCode";
	private static String TRADING_PARTNER_ID = "tradingPartnerId";
	private static String SPOE_ID = "spoeId";
	private static String TENANT_ID = "tenantId";
	private static String PARTIAL_QHP_PLAN_ID = "partialQhpPlanId";
	private static String ISSUER_EXTRACT_DATE = "issuerExtractDate";
	private static String ISSUER_EXTRACT_TIME = "issuerExtractTime";
	private static String PAID_THROUGH_DATE = "paidThroughDate";
	private static String EOY_TERMINATION_INDICATOR = "eoyTerminationIndicator";
	private static String INITIAL_PREMIUMPAID_STATUS = "InitialPremiumPaidStatus";
	private static String ISSUER_ASSIGNED_RECORD_TRACE_NUMBER = "issuerAssignedRecordTraceNumber";
	private static String ENROLLEE_ID = "enrolleeId";
	private static String ISSUER_PREMIUM = "issuerpremium";
	private static String RATING_AREA = "ratingArea";
	private static String ACTIVE_DAYS = "activeDays";
	private static String ENROLLEE_STATUS = "enrolleeStatus";
	private static String MEMBER_START_DATE = "memberStartDate";
	private static String ENROLLMENT_STATUS = "enrollmentStatus";
	private static String HIOS_ID = "hiosId";
	private static String QHP_ID = "qhpId";
	private static String ISSUER_ASSIGNED_POLICY_ID = "issuerAssignedPolicyId";
	private static String EXCHANGE_ASSIGNED_MEMBER_ID = "exchangeAssignedMemberId";
	private static String ISSUER_ASSIGNED_SUBSCRIBER_ID = "issuerAssignedSubscriberId";
	private static String ISSUER_ASSIGNED_MEMBER_ID = "issuerAssignedMemberId";
	private static String SUBSCRIBER_INDICATOR = "subscriberIndicator";
	private static String EXCHANGE_ASSIGNED_POLICY_ID = "exchangeAssignedPolicyid";
	private static String MEMBER_COUNT = "memberCount";
	private static String MONTH_START_DATE = "monthStartdate";
	private static String MONTH_END_DATE = "monthEnddate";
	private static String ENROLLMENT_CONFIRMATION_DATE = "enrollmentconfirmationDate";
	private static String INDIVIDUAL_PREMIUM = "individualPremium";
	private static String CSR_AMT = "csrAmt_";
	private static String INSURANCE_TYPE="insuranceTypeCode";
	
	Predicate<ReconMemberDTO> isMemberCancelled = e -> EnrollmentConstants.ENROLLMENT_STATUS_CANCEL.equalsIgnoreCase(e.getEnrolleeStatus())
			|| (null != e.getMemberBeginDate() && null != e.getMemberEndDate()
			&& e.getMemberBeginDate().equalsIgnoreCase(e.getMemberEndDate()));

	/*
	private static String CASE_FIRST_NAME = "firstName";
	private static String CASE_LAST_NAME = "lastName";
	private static String CASE_MIDDLE_NAME = "middleName";
	private static String CASE_GENDER = "gender";
	private static String CASE_SUBSCRIBER_IND = "subscriberIndicator";
	private static String CASE_HOME_ADDR1 = "homeAddress1";
	private static String CASE_HOME_ADDR2 = "homeAddress2";
	private static String CASE_HOME_CITY = "homeCity";
	private static String CASE_HOME_STATE = "homeState";
	private static String CASE_MAIL_ADDR1 = "mailingAddress1";
	private static String CASE_MAIL_ADDR2 = "mailingAddress2";
	private static String CASE_MAIL_CITY = "mailingCity";
	private static String CASE_MAIL_STATE = "mailingState";
	private static String CASE_AGENT_BROKER_NAME = "agentBrokerName";



	private boolean checkCaseSensitiveField(String fdName){
		if(fdName!=null
				||CASE_FIRST_NAME.equalsIgnoreCase(fdName)
				||CASE_LAST_NAME.equalsIgnoreCase(fdName)
				||CASE_MIDDLE_NAME.equalsIgnoreCase(fdName)
				||CASE_GENDER.equalsIgnoreCase(fdName)
				||CASE_SUBSCRIBER_IND.equalsIgnoreCase(fdName)
				||PARTIAL_QHP_PLAN_ID.equalsIgnoreCase(fdName)
				||CASE_HOME_ADDR1.equalsIgnoreCase(fdName)
				||CASE_HOME_ADDR2.equalsIgnoreCase(fdName)
				||CASE_HOME_CITY.equalsIgnoreCase(fdName)
				||CASE_HOME_STATE.equalsIgnoreCase(fdName)
				||CASE_MAIL_ADDR1.equalsIgnoreCase(fdName)
				||CASE_MAIL_ADDR2.equalsIgnoreCase(fdName)
				||CASE_MAIL_CITY.equalsIgnoreCase(fdName)
				||CASE_MAIL_STATE.equalsIgnoreCase(fdName)
				||CASE_AGENT_BROKER_NAME.equalsIgnoreCase(fdName)
				)
		{
			return true;
		}else{
			return false;
		}

	}*/

	/**
	 * @author panda_p
	 * @since 12/Mar/2017
	 * 
	 * 
	 * @param fileId
	 */

	@Override
	public boolean compareFileSnapshot(Long fileId, Job job, JobLauncher jobLauncher) throws Exception{
		if(fileId!=null && fileId>0){

			List<Integer> snapshotIds=iEnrlReconSnapshotRepository.findIdByFileIdAndEmptyStatus(fileId.intValue());

			if(snapshotIds!=null && !snapshotIds.isEmpty()){
				int successCount= 0;
				int discrepancyCount= 0;
				int errorCount= 0;
				int missingInHIx= 0;
				int missingInFile= 0;
				Map<Long, Integer> missingEnrlSnapshotMap = new HashMap<>();
				for(Integer snapshotId:snapshotIds){
					EnrlReconSnapshot objEnrlReconSnapshot = null;
					try{
						objEnrlReconSnapshot = iEnrlReconSnapshotRepository.findById(snapshotId);
						if(objEnrlReconSnapshot!=null){

							if(!EnrollmentUtils.isNotNullAndEmpty(objEnrlReconSnapshot.getHixData())){
								missingInHIx++;
								missingEnrlSnapshotMap.put(objEnrlReconSnapshot.getHixEnrollmentId(), snapshotId);
								objEnrlReconSnapshot.setComparisonStatus("MISSING_IN_HIX");
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug("HIX - DATA is null . This seems to be a case of missing Enrollment in HIX. ");
								}
							}else if(!EnrollmentUtils.isNotNullAndEmpty(objEnrlReconSnapshot.getIssuerData())) {
								missingInFile++;
								missingEnrlSnapshotMap.put(objEnrlReconSnapshot.getHixEnrollmentId(), snapshotId);
								objEnrlReconSnapshot.setComparisonStatus("MISSING_IN_FILE");
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug("HIX - DATA is null . This seems to be a case of missing Enrollment in file. ");
								}
							}else{
								if(compareSnapshot(objEnrlReconSnapshot,fileId)){
									discrepancyCount++;
									objEnrlReconSnapshot.setComparisonStatus("DISCREPANCY");
								}else{
									objEnrlReconSnapshot.setComparisonStatus("SUCCESS");
									successCount++;
								}
							}
						}else{
							if(LOGGER.isDebugEnabled()) {
								LOGGER.debug(" SKIP : enrlReconCompareSnapshotService - Snapshot not found for snapshotId : "+snapshotId);
							}
							continue;
						}
					}catch (Exception e){
						if(objEnrlReconSnapshot!=null){
							errorCount++;
							objEnrlReconSnapshot.setComparisonStatus("ERROR");
							objEnrlReconSnapshot.setErrorMessage(EnrollmentUtils.getExceptionMessage(e).substring(0,290));
						}
					}finally{
						if(objEnrlReconSnapshot!=null){
							try{
								iEnrlReconSnapshotRepository.saveAndFlush(objEnrlReconSnapshot);
							}catch (Exception e){
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug(" enrlReconCompareSnapshotService : "
											+ " Not able to update comparison status to snapshot table for snapshotId : "+snapshotId
											+ " \n Exception Details : ", e);
								}
							}
						}
					}//End of Finally block

				}// End of For-  snapshotIds

				/**
				 * Updating Summary table for file 
				 */
				updateFileSummary(fileId, successCount, discrepancyCount, errorCount,missingInHIx, missingInFile);

				processMissingEnrollmentDiscrepancies(fileId.intValue(), missingEnrlSnapshotMap);

				String jobChaining = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENRL_RECON_BATCH_JOB_CHAINING);

				if(null != jobChaining && "false".equalsIgnoreCase(jobChaining)){

					LOGGER.info(" NOT Triggering SNAPSHOT COMPARATOR JOB  for batch File_ID(S) : " + fileId);
					LOGGER.info(" ENRL_RECON_BATCH_JOB_CHAINING : "+ jobChaining);

				}else{

					triggerBatchJobWithParams( fileId,  job,  jobLauncher);
				}
			}else{
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug(" enrlReconCompareSnapshotService : No valid Snapshot entry found for given fileId");
				}
			}
		}else{
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug(" enrlReconCompareSnapshotService : fileId is null or zero or negative");
			}
		}
		return true;
	}

	private void updateFileSummary(Long fileId, int successCount, int discrepancyCount, int errorCount, int missingInHIx, int missingInFile){

		EnrlReconSummary objEnrlReconSummary = null;
		try{
			if(fileId!=null){
				objEnrlReconSummary = iEnrlReconSummaryRepository.getReconSummaryByFileId(fileId.intValue());
				if(objEnrlReconSummary!=null){
					objEnrlReconSummary.setEnrlCntSuccess(successCount);
					objEnrlReconSummary.setEnrlCntDiscrepancies(discrepancyCount);
					//					objEnrlReconSummary.setStatus(SummaryStatus.COMPARED.toString());
					objEnrlReconSummary.setEnrlCntMissingInHix(missingInHIx);
					objEnrlReconSummary.setEnrlCntMissingInFile(missingInFile);
					if(objEnrlReconSummary.getTotalEnrlCntInFile()!=null){
						objEnrlReconSummary.setRecCntFailed(objEnrlReconSummary.getTotalEnrlCntInFile()-(successCount+discrepancyCount+missingInHIx));
					}
					Long disCount = iEnrlReconDiscrepancyRepository.findDiscrepancyCountByFileId(fileId.intValue());
					if(disCount!=null){
						objEnrlReconSummary.setCntDiscrepancies(disCount.intValue());
					}
					iEnrlReconSummaryRepository.saveAndFlush(objEnrlReconSummary);
				}else{
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug(" no objEnrlReconSummary found for File id:  "+fileId);
					}
				}
			}
		}catch (Exception e){
			LOGGER.error(" unable to save File Summary : \n " +e.getMessage());
		}
	}

	/**
	 * 
	 * @param objEnrlReconSnapshot
	 * @param fileId
	 * @return
	 */
	private boolean compareSnapshot(EnrlReconSnapshot objEnrlReconSnapshot, Long fileId){

		List<EnrlReconDiscrepancy> enrlReconDiscrepancies = new ArrayList<EnrlReconDiscrepancy>();
		String jsonhix=null;
		String jsonissuer=null;
		boolean isDiscrepancyFound = false;
		try {

			if(objEnrlReconSnapshot!=null){

				/*try {
					jsonhix = new Scanner(new File("D:\\jsontest\\hix.json")).useDelimiter("\\Z").next();
					jsonissuer = new Scanner(new File("D:\\jsontest\\issuer.json")).useDelimiter("\\Z").next();
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}*/

				jsonhix = objEnrlReconSnapshot.getHixData();
				jsonissuer = objEnrlReconSnapshot.getIssuerData();

				Gson gson = new Gson();
				ReconEnrollmentDTO  hixDTO = gson.fromJson(jsonhix, ReconEnrollmentDTO.class);
				ReconEnrollmentDTO  issuerDTO = gson.fromJson(jsonissuer, ReconEnrollmentDTO.class);

				if(hixDTO==null && issuerDTO !=null ){
					throw new Exception("HIX - DATA is null . This seems to be a case of missing Enrollment in HIX. Issuer Enrollment id : "+ issuerDTO.getExchangeAssignedPolicyid());
				}
				/**
				 * Removing additional subscriber info from issuerDTO before comparison
				 */
				issuerDTO.setAdditionalSubInfo(null);
				/**
				 * comparing extra or missing members
				 */
				List<EnrlReconDiscrepancy> memCompareDiscps= compareMembers(hixDTO, issuerDTO);
				if(memCompareDiscps!=null && !memCompareDiscps.isEmpty()){
					enrlReconDiscrepancies.addAll(memCompareDiscps);
				}

				/**
				 * comparing  extra or missing premiums
				 */
				List<EnrlReconDiscrepancy> premCompareDiscps= comparePremiums(hixDTO, issuerDTO);
				if(premCompareDiscps!=null && !premCompareDiscps.isEmpty()){
					enrlReconDiscrepancies.addAll(premCompareDiscps);
				}
				/**
				 * Comparing over allocation of APTC in issues data i.e. APTC>issuerGrossPrem 
				 */
				List<EnrlReconDiscrepancy> aptcOverAllocDiscps= checkAptcOverallocation(issuerDTO, hixDTO.getExchangeAssignedPolicyid());
				if(aptcOverAllocDiscps!=null && !aptcOverAllocDiscps.isEmpty()){
					enrlReconDiscrepancies.addAll(aptcOverAllocDiscps);
				}
				/*
				 * Descrepancy added against HIX-103478
				 */
				EnrlReconDiscrepancy enrollmentStatusDiscps= compareEnrollmentStatus(issuerDTO, hixDTO);
				if(enrollmentStatusDiscps!=null){
					enrlReconDiscrepancies.add(enrollmentStatusDiscps);
				}

				if(issuerDTO.getInitialPremiumPaidStatus()!=null && "Y".equalsIgnoreCase(issuerDTO.getInitialPremiumPaidStatus())){
					EnrlReconDiscrepancy effectuationDisp = null;

					if(hixDTO.getEnrollmentconfirmationDate()==null || "".equals(hixDTO.getEnrollmentconfirmationDate())){
						effectuationDisp = new EnrlReconDiscrepancy();

						if(hixDTO.getExchangeAssignedPolicyid() != null && !"".equalsIgnoreCase(hixDTO.getExchangeAssignedPolicyid())){
							effectuationDisp.setHixEnrollmentId(Long.valueOf(hixDTO.getExchangeAssignedPolicyid()));
						}
						if(issuerDTO.getExchangeAssignedPolicyid() != null && !"".equalsIgnoreCase(issuerDTO.getExchangeAssignedPolicyid())){
							effectuationDisp.setIssuerEnrollmentId(Integer.valueOf(issuerDTO.getExchangeAssignedPolicyid()));
						}
						effectuationDisp.setIssuerValue("Y");
						effectuationDisp.setHixValue(hixDTO.getEnrollmentStatus());
						/*
						 commenting following code for HIX-100801 We should not be generating any cancel discrepancy if dates match. 

						 if("CANCEL".equalsIgnoreCase(hixDTO.getEnrollmentStatus())){
							effectuationDisp.setEnrlReconLkpId(findLkpIdByCode(ENROLLMENT_CANCELLED_IN_HIX_CODE));
						}else{
							effectuationDisp.setEnrlReconLkpId(findLkpIdByCode(EFFECTUATION_STATUS_CODE));
						}*/


						effectuationDisp.setEnrlReconLkpId(findLkpIdByCode(EFFECTUATION_STATUS_CODE));
						enrlReconDiscrepancies.add(effectuationDisp);
					}
				}
				String insuranceTypeLkp= hixDTO.getInsuranceTypeCode();
				String csLevel= EnrollmentUtils.isNotNullAndEmpty(issuerDTO.getQhpId())?issuerDTO.getQhpId().substring(Math.max(issuerDTO.getQhpId().length() - 2, 0)):null;
				jsonhix = gson.toJson(hixDTO);
				jsonissuer = gson.toJson(issuerDTO);
				String compareJsonString =compareJsonString( jsonhix,  jsonissuer);
				LOGGER.info("==================================\n"+compareJsonString+"\n ================================");


				List<Integer> newDiscrepLookups = null; 
				Long hixEnrollmentId = objEnrlReconSnapshot.getHixEnrollmentId();

				if(compareJsonString!=null && "SUCCESS"!=compareJsonString){
					newDiscrepLookups = new ArrayList<Integer>();
					List<String> errList = Arrays.asList(compareJsonString.split("\n"));
					int count=0;
					for(String err : errList){
						if(count==0){
							count++;
							continue;
						}
						enrlReconDiscrepancies.addAll( processErrorString(err, hixDTO, issuerDTO));
					}
					for(EnrlReconDiscrepancy objEnrlReconDiscrepancy : enrlReconDiscrepancies){
						try{
							if(objEnrlReconDiscrepancy!=null){
								if(null != objEnrlReconDiscrepancy.getFieldName() && objEnrlReconDiscrepancy.getFieldName().contains(CSR_AMT) && !isCSRDiscrepant(insuranceTypeLkp, csLevel, objEnrlReconDiscrepancy.getHixValue(), objEnrlReconDiscrepancy.getIssuerValue())){
									continue;
								}else if(objEnrlReconDiscrepancy.getHixValue()!=null 
										&& objEnrlReconDiscrepancy.getHixValue().equalsIgnoreCase(objEnrlReconDiscrepancy.getIssuerValue())){
									continue;
								}else{
									if(objEnrlReconDiscrepancy.getEnrlReconLkpId()!=null){
										newDiscrepLookups.add(objEnrlReconDiscrepancy.getEnrlReconLkpId().getId());
									}
									if(fileId!=null){
										objEnrlReconDiscrepancy.setFileId(Integer.valueOf(fileId.toString()));
									}
									objEnrlReconDiscrepancy.setStatus(DiscrepancyStatus.OPEN);
									objEnrlReconDiscrepancy.setSnapshotId(objEnrlReconSnapshot.getId());
									isDiscrepancyFound = true;
									iEnrlReconDiscrepancyRepository.saveAndFlush(objEnrlReconDiscrepancy);
								}
							}
						}catch (Exception e) {
							LOGGER.info("Not able to persist discrepancy : ");	
							if(objEnrlReconDiscrepancy !=null){
								LOGGER.error("Unsaved Discrepancy Details: "+objEnrlReconDiscrepancy.toString());
							}
							LOGGER.info("Not able to persist discrepancy : Failure reason ------"+e.getMessage());	
							//e.printStackTrace();
						}
					}
					/**
					 * clear all other type (not in newDiscrepancyLkpList )  of  discrepancies for this enrollment
					 */
					resolveHistoricalDiscrepancies(hixEnrollmentId, newDiscrepLookups);

				}else{
					/**
					 * clear all the previous discrepancies for this enrollment
					 */
					resolveHistoricalDiscrepancies(hixEnrollmentId, null);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while processing compareSnapshot:    "+e.getMessage());
			e.printStackTrace();
		}

		return isDiscrepancyFound;
	}


	private void resolveHistoricalDiscrepancies(Long hixEnrollmentId, List<Integer> newDiscrepLookups){
		if(hixEnrollmentId != null){
			List<EnrlReconDiscrepancy> oldEnrlReconDiscrepList=null;
			List<DiscrepancyStatus> discrepancyStatusList = Arrays.asList(DiscrepancyStatus.OPEN,DiscrepancyStatus.HOLD);
			if(newDiscrepLookups!=null && !newDiscrepLookups.isEmpty()){
				oldEnrlReconDiscrepList=
						iEnrlReconDiscrepancyRepository.findByDiscrepancyLkpIdsAndHixEnrllmentIdAndStatus(newDiscrepLookups, hixEnrollmentId, discrepancyStatusList);
			}else{
				oldEnrlReconDiscrepList=
						iEnrlReconDiscrepancyRepository.findByHixEnrllmentIdAndStatus(hixEnrollmentId, discrepancyStatusList);
			}
			List<Map<Integer,String>> failedList = resolveDiscrepancies(oldEnrlReconDiscrepList);
			if(failedList!=null && !failedList.isEmpty()){
				LOGGER.info("Successfully resolved all historical Discrepancies");
			}else{
				LOGGER.info("Failed to resolve following historical Discrepancies" + failedList);
			}
		}else{
			LOGGER.info("Not able to resolve historical data as hixEnrollmentId is null");	
		}
	}

	private List<Map<Integer,String>> resolveDiscrepancies(List<EnrlReconDiscrepancy> enrlReconDiscrepList){
		List<Map<Integer,String>> failedIds=new ArrayList<Map<Integer,String>>();
		Map<Integer,String> failMap =null;
		if(enrlReconDiscrepList!=null && !enrlReconDiscrepList.isEmpty()){
			for(EnrlReconDiscrepancy oldDiscrep:enrlReconDiscrepList){
				try{
					oldDiscrep.setStatus(DiscrepancyStatus.RESOLVED);
					oldDiscrep.setDateResolved(new Date());
					iEnrlReconDiscrepancyRepository.saveAndFlush(oldDiscrep);
				}catch (Exception e) {
					if(failMap==null){
						failMap = new HashMap<Integer,String>();
						failMap.put(oldDiscrep.getId(), EnrollmentUtils.getExceptionMessage(e).substring(0, 300));
					}else{
						failMap.put(oldDiscrep.getId(), EnrollmentUtils.getExceptionMessage(e).substring(0, 300));
					}
				}
			}
		}else{
			LOGGER.info("No historical discrepancy data for the given hix enrollment Id.");	
		}
		return failedIds;
	}

	/**
	 * @author panda_p
	 * @since Mar/07/2017
	 * 
	 * 
	 * @param errorString
	 * @param hixDTO
	 * @param issuerDTO
	 * @return
	 */
	private List<EnrlReconDiscrepancy> processErrorString(String errorString, ReconEnrollmentDTO  hixDTO, ReconEnrollmentDTO issuerDTO){
		List<EnrlReconDiscrepancy> enrlReconDiscrepancies = new ArrayList<EnrlReconDiscrepancy>();
		if(EnrollmentUtils.isNotNullAndEmpty(errorString)){
			EnrlReconDiscrepancy objEnrlReconDiscrepancy = null;
			if(errorString.replaceAll("\"", "").contains("Array monthlyPremiums has different length")){
				processDiffPremiumLength(errorString, hixDTO, issuerDTO);
			}else if(errorString.contains("Different keys found in node") && errorString.contains("monthlyPremiums[")){
				LOGGER.info("Premium missing fields :");
				int index=0;
				String[] arrString = errorString.replaceAll("\"", "").split("\\.");
				if(arrString!=null && arrString.length>2){
					String premiumIndex = StringUtils.substringBetween(arrString[0], "[", "]").trim();
					if(EnrollmentUtils.isNotNullAndEmpty(premiumIndex)){
						index = Integer.parseInt(premiumIndex);	
					}
				}
				if(errorString.contains("Missing")){
					String missingSubStr = StringUtils.substringBetween(errorString, "Missing:", "Extra:");
					if(missingSubStr==null){
						missingSubStr = StringUtils.substringAfter(errorString, "Missing:");
					}
					List<String> missingFields = Arrays.asList(missingSubStr.split(",")).stream().map(val->val.replaceAll("\"", "").trim()).collect(Collectors.toList());
					for(String missingFieldName: missingFields){
						if(checkRequiredFieldName(StringUtils.substringAfter(missingFieldName, "."))){
							objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
							setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
							enrlReconDiscrepancies.add(initialiazeDiscrepance(StringUtils.substringAfter(missingFieldName, "."),  hixDTO.getMonthlyPremiums().get(index), issuerDTO.getMonthlyPremiums().get(index),objEnrlReconDiscrepancy));
						}
					}
				}
				if(errorString.contains("Extra")){
					String extraSubStr = StringUtils.substringAfter(errorString, "Extra:");
					LOGGER.info("Extra fields :");
					List<String> extraFields = Arrays.asList(extraSubStr.split(",")).stream().map(val->val.replaceAll("\"", "").trim()).collect(Collectors.toList());
					for(String estraFieldName: extraFields){
						if(checkRequiredFieldName(StringUtils.substringAfter(estraFieldName, "."))){
							objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
							setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
							enrlReconDiscrepancies.add(initialiazeDiscrepance(StringUtils.substringAfter(estraFieldName, "."),  hixDTO.getMonthlyPremiums().get(index), issuerDTO.getMonthlyPremiums().get(index), objEnrlReconDiscrepancy));
						}

					}
				}

			}else if(errorString.contains("Different value found in node") && errorString.contains("monthlyPremiums[")){
				LOGGER.info("Premium extra fields :");
				String[] arrString = errorString.replaceAll("\"", "").split("\\.");
				if(arrString!=null && arrString.length>2){
					int index=0;
					String premiumIndex = StringUtils.substringBetween(arrString[0], "[", "]").trim();
					if(EnrollmentUtils.isNotNullAndEmpty(premiumIndex)){
						index = Integer.parseInt(premiumIndex);	
					}

					String fdName=arrString[1].trim();
					if(checkRequiredFieldName(fdName)){
						objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
						//Different value found in node "monthlyPremiums[0].grossPremium". Expected 530.11, got 4240.88.
						objEnrlReconDiscrepancy.setHixValue(StringUtils.substringBetween(errorString.replaceAll("\"", ""), "Expected",",").trim());
						String issuerVal = StringUtils.substringAfter(errorString.replaceAll("\"", ""), "got").trim();
						if (issuerVal != null && issuerVal.length() > 0 && issuerVal.charAt(issuerVal.length()-1)=='.'){
							objEnrlReconDiscrepancy.setIssuerValue(issuerVal.substring(0,issuerVal.length()-1));
						} else{
							objEnrlReconDiscrepancy.setIssuerValue(issuerVal);
						}
						if(hixDTO != null &&  hixDTO.getMonthlyPremiums() != null &&  hixDTO.getMonthlyPremiums().get(index) != null){
							objEnrlReconDiscrepancy.setFieldName(fdName + "_"+ hixDTO.getMonthlyPremiums().get(index).getCoverageMonth());
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName(objEnrlReconDiscrepancy.getFieldName()));
						}
						else if(issuerDTO != null &&  issuerDTO.getMonthlyPremiums() != null &&  issuerDTO.getMonthlyPremiums().get(index) != null){
							objEnrlReconDiscrepancy.setFieldName(fdName + "_"+ issuerDTO.getMonthlyPremiums().get(index).getCoverageMonth());
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName(objEnrlReconDiscrepancy.getFieldName()));
						}
						setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
						enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
					}
				}

			}else if(errorString.replaceAll("\"", "").contains("Array members has different length")){
				processDiffMemberLength(errorString, hixDTO, issuerDTO);
			}else if(errorString.contains("Different keys found in node") && errorString.contains("members[")){
				LOGGER.info("Member missing fields :");
				/**
				 * Different keys found in node "members[0]". Expected [birthDate, exchangeAssignedMemberId, exchangeAssignedPolicyid, exchangeAssignedSubscriberId, firstName, homeAddress1, homeAddress2, homeCity, homeCountyCode, homeState, homeZip, individualRelationshipCode, issuerAssignedMemberId, issuerAssignedPolicyId, issuerAssignedSubscriberId, lastName, mailingAddress1, mailingAddress2, mailingCity, mailingState, mailingZip, ratingArea, ssn, subscriberIndicator, telephoneNumber, tobacco], got [exchangeAssignedMemberId, exchangeAssignedPolicyid, exchangeAssignedSubscriberId, firstName, gender, homeAddress1, homeAddress2, homeCity, homeCountyCode, homeState, homeZip, individualRelationshipCode, issuerAssignedMemberId, issuerAssignedPolicyId, issuerAssignedSubscriberId, mailingAddress1, mailingAddress2, mailingCity, mailingState, mailingZip, middleName, ratingArea, ssn, subscriberIndicator, telephoneNumber, tobacco]. Missing: "members[0].birthDate","members[0].lastName" Extra: "members[0].gender","members[0].middleName"
				 */
				int index=0;
				String[] arrString = errorString.replaceAll("\"", "").split("\\.");
				if(arrString!=null && arrString.length>2){
					String memberIndex = StringUtils.substringBetween(arrString[0], "[", "]").trim();
					if(EnrollmentUtils.isNotNullAndEmpty(memberIndex)){
						index = Integer.parseInt(memberIndex);	
					}
				}
				if(errorString.contains("Missing")){
					String missingSubStr = StringUtils.substringBetween(errorString, "Missing:", "Extra:");
					if(missingSubStr==null){
						missingSubStr = StringUtils.substringAfter(errorString, "Missing:");
					}
					List<String> missingFields = Arrays.asList(missingSubStr.split(",")).stream().map(val->val.replaceAll("\"", "").trim()).collect(Collectors.toList());
					for(String missingFieldName: missingFields){
						if(checkRequiredFieldName(StringUtils.substringAfter(missingFieldName, "."))){
							objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
							setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
							enrlReconDiscrepancies.add(initialiazeDiscrepance(StringUtils.substringAfter(missingFieldName, "."),  hixDTO.getMembers().get(index), issuerDTO.getMembers().get(index), "MEMBER", objEnrlReconDiscrepancy));	
						}

					}
				}
				if(errorString.contains("Extra")){
					String extraSubStr = StringUtils.substringAfter(errorString, "Extra:");
					LOGGER.info("Member Extra fields :");
					List<String> extraFields = Arrays.asList(extraSubStr.split(",")).stream().map(val->val.replaceAll("\"", "").trim()).collect(Collectors.toList());
					for(String extraFieldName: extraFields){
						if(checkRequiredFieldName(StringUtils.substringAfter(extraFieldName, "."))){
							objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
							setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
							enrlReconDiscrepancies.add(initialiazeDiscrepance(StringUtils.substringAfter(extraFieldName, "."),  hixDTO.getMembers().get(index), issuerDTO.getMembers().get(index), "MEMBER", objEnrlReconDiscrepancy));
						}
					}
				}


			}else if(errorString.contains("Different value found in node") && errorString.contains("members[")){
				//Different value found in node "members[0].firstName". Expected "John1", got "John".
				int index=0;
				String[] arrString = errorString.replaceAll("\"", "").split("\\.");
				if(arrString!=null && arrString.length>2){
					ReconMemberDTO hixMember = null;
					String memberIndex = StringUtils.substringBetween(arrString[0], "[", "]").trim();
					if(EnrollmentUtils.isNotNullAndEmpty(memberIndex)){
						index = Integer.parseInt(memberIndex);	
						hixMember = hixDTO.getMembers().get(index);
					}

					String fdName = arrString[1].trim();
					if(checkRequiredFieldName(fdName)){
						objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
						if(hixMember!=null){
							objEnrlReconDiscrepancy.setEnrolleeId(hixMember.getEnrolleeId());
							if(hixMember.getExchangeAssignedMemberId()!=null){
								objEnrlReconDiscrepancy.setMemberId(Integer.parseInt(hixMember.getExchangeAssignedMemberId()));
							}
						}
						objEnrlReconDiscrepancy.setFieldName(fdName);
						objEnrlReconDiscrepancy.setHixValue(StringUtils.substringBetween(errorString.replaceAll("\"", ""), "Expected ",", got ").trim());
						String issuerVal = StringUtils.substringAfter(errorString.replaceAll("\"", ""), ", got ").trim();
						if (issuerVal != null && issuerVal.length() > 0 && issuerVal.charAt(issuerVal.length()-1)=='.'){
							issuerVal = issuerVal.substring(0,issuerVal.length()-1);
							objEnrlReconDiscrepancy.setIssuerValue(issuerVal);
						} else{
							objEnrlReconDiscrepancy.setIssuerValue(issuerVal);
						}
						if("mailingAddress1".equalsIgnoreCase(fdName) 
								&& "Bad Address".equalsIgnoreCase(issuerVal)){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(BAD_MAILING_ADDRESS_CODE));
						}else if("Y".equalsIgnoreCase(hixMember.getSubscriberIndicator()) && "memberBeginDate".equalsIgnoreCase(fdName)) {
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(SUBSCRIBER_BEGIN_DATE_CODE));
						}else if("Y".equalsIgnoreCase(hixMember.getSubscriberIndicator()) && "memberEndDate".equalsIgnoreCase(fdName)) {
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(SUBSCRIBER_END_DATE_CODE));
						}else{
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName(objEnrlReconDiscrepancy.getFieldName()));
						}
						setHixAndIssuerEnrollmentId(objEnrlReconDiscrepancy, hixDTO, issuerDTO);

						if(("homeZip".equalsIgnoreCase(fdName) || "mailingZip".equalsIgnoreCase(fdName)) && 
								(EnrollmentUtils.isNotNullAndEmpty(objEnrlReconDiscrepancy.getIssuerValue()) 
										&& objEnrlReconDiscrepancy.getIssuerValue().length()>4)
								) {
							if(!objEnrlReconDiscrepancy.getIssuerValue().substring(0,5).equalsIgnoreCase(objEnrlReconDiscrepancy.getHixValue())) {
								enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
							}
						}else if("middleName".equalsIgnoreCase(fdName) ){
							if(!EnrollmentUtils.compareMiddleName(objEnrlReconDiscrepancy.getIssuerValue(), objEnrlReconDiscrepancy.getHixValue())){
								enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
							}
						}else if(fdName!=null && "individualRelationshipCode".equalsIgnoreCase(fdName)){
							if(isChildRelationSame(objEnrlReconDiscrepancy.getHixValue(),objEnrlReconDiscrepancy.getIssuerValue())){
								//do nothing as member RelationshipCode are same
							}else{
								enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);	
							}
						}
						else {
							enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
						}

					}
				}
			}else if(errorString.contains("Different keys found in node")){
				LOGGER.info("Enrollment- ");
				if(errorString.contains("Missing")){
					String missingSubStr = StringUtils.substringBetween(errorString, "Missing:", "Extra:");
					if(missingSubStr==null){
						missingSubStr = StringUtils.substringAfter(errorString, "Missing:");
					}
					LOGGER.info("missing fields :");
					List<String> missingFields = Arrays.asList(missingSubStr.split(",")).stream().map(val->val.replaceAll("\"", "").trim()).collect(Collectors.toList());
					for(String missingFieldName: missingFields){
						if(checkRequiredFieldName(missingFieldName)){
							enrlReconDiscrepancies.add(initialiazeDiscrepance(missingFieldName,  hixDTO, issuerDTO));
						}
					}
				}
				if(errorString.contains("Extra")){
					String extraSubStr = StringUtils.substringAfter(errorString, "Extra:");
					LOGGER.info("Extra fields :");
					List<String> extraFields = Arrays.asList(extraSubStr.split(",")).stream().map(val->val.replaceAll("\"", "").trim()).collect(Collectors.toList());
					for(String extraFieldName: extraFields){
						if(checkRequiredFieldName(extraFieldName)){
							enrlReconDiscrepancies.add(initialiazeDiscrepance(extraFieldName,  hixDTO, issuerDTO));
						}
					}
				}
			}else if(errorString.contains("Different value found in node")){

				String[] arrString = errorString.split("Different value found in node");
				if(arrString!=null && arrString.length>1){
					String enrlDiscrep =arrString[1].trim();
					if(EnrollmentUtils.isNotNullAndEmpty(enrlDiscrep)){
						enrlDiscrep = enrlDiscrep.replaceAll("\"", "");
						String[] fields = enrlDiscrep.split("\\.");
						if(fields!=null && fields.length>1){
							//							String[] values = fields[1].split(",");
							LOGGER.info("Fields Name :"+fields[0] +" hix_val :"+StringUtils.substringBetween(fields[1], "Expected ",", got ") +
									"  issuer_val :"+StringUtils.substringAfter(fields[1], ", got "));
							String fdName= fields[0];
							if(checkRequiredFieldName(fdName)){
								objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
								objEnrlReconDiscrepancy.setFieldName(fdName);
								objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName(objEnrlReconDiscrepancy.getFieldName()));
								objEnrlReconDiscrepancy.setHixValue(StringUtils.substringBetween(fields[1], "Expected ",", got "));
								objEnrlReconDiscrepancy.setIssuerValue(StringUtils.substringAfter(fields[1], ", got "));
								setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
								//enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
								if(fdName!=null && "agentBrokerName".equalsIgnoreCase(fdName)){
									if(!EnrollmentUtils.compareAgentName(objEnrlReconDiscrepancy.getIssuerValue(), objEnrlReconDiscrepancy.getHixValue())){
										enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
									}
								}else{
									enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
								}
							}
						}
					}
				}
			}
		}

		return enrlReconDiscrepancies;
	}

	/**
	 * 
	 * @param hixRelationshipCode
	 * @param isserRelationshipCode
	 * @return
	 */
	private static boolean isChildRelationSame(String hixRelationshipCode, String isserRelationshipCode){
		List<String> relCodeList = Arrays.asList("09", "17","19");
		if(relCodeList.contains(hixRelationshipCode) && relCodeList.contains(isserRelationshipCode) ){
			//System.out.println("true");
			return true;
		}else{
			//System.out.println("false");
			return false;
		}
	}

	/**
	 * 
	 * @param fdName
	 * @return
	 */
	private boolean checkRequiredFieldName(String fdName){
		if(fdName==null
				||"".equalsIgnoreCase(fdName)
				||RECORD_CODE.equalsIgnoreCase(fdName)
				||TRADING_PARTNER_ID.equalsIgnoreCase(fdName)
				||SPOE_ID.equalsIgnoreCase(fdName)
				||TENANT_ID.equalsIgnoreCase(fdName)
				||PARTIAL_QHP_PLAN_ID.equalsIgnoreCase(fdName)
				||ISSUER_EXTRACT_DATE.equalsIgnoreCase(fdName)
				||ISSUER_EXTRACT_TIME.equalsIgnoreCase(fdName)
				||PAID_THROUGH_DATE.equalsIgnoreCase(fdName)
				||EOY_TERMINATION_INDICATOR.equalsIgnoreCase(fdName)
				||INITIAL_PREMIUMPAID_STATUS.equalsIgnoreCase(fdName)
				||ISSUER_ASSIGNED_RECORD_TRACE_NUMBER.equalsIgnoreCase(fdName)
				||ENROLLEE_ID.equalsIgnoreCase(fdName)
				||ISSUER_PREMIUM.equalsIgnoreCase(fdName)
				||ACTIVE_DAYS.equalsIgnoreCase(fdName)
				||ENROLLEE_STATUS.equalsIgnoreCase(fdName)
				||MEMBER_START_DATE.equalsIgnoreCase(fdName)
				||ENROLLMENT_STATUS.equalsIgnoreCase(fdName)
				||HIOS_ID.equalsIgnoreCase(fdName)
				||QHP_ID.equalsIgnoreCase(fdName)
				||ISSUER_ASSIGNED_POLICY_ID.equalsIgnoreCase(fdName)
				||EXCHANGE_ASSIGNED_MEMBER_ID.equalsIgnoreCase(fdName)
				||EXCHANGE_ASSIGNED_POLICY_ID.equalsIgnoreCase(fdName)
				||MEMBER_COUNT.equalsIgnoreCase(fdName)
				||MONTH_START_DATE.equalsIgnoreCase(fdName)
				||MONTH_END_DATE.equalsIgnoreCase(fdName)
				||ENROLLMENT_CONFIRMATION_DATE.equalsIgnoreCase(fdName)
				||INDIVIDUAL_PREMIUM.equalsIgnoreCase(fdName)
				||ISSUER_ASSIGNED_SUBSCRIBER_ID.equalsIgnoreCase(fdName)
				||ISSUER_ASSIGNED_MEMBER_ID.equalsIgnoreCase(fdName)
				||RATING_AREA.equalsIgnoreCase(fdName)
				||INSURANCE_TYPE.equalsIgnoreCase(fdName)
				)
		{
			return false;
		}else{
			return true;
		}

	}

	private EnrlReconDiscrepancy  compareEnrollmentStatus(ReconEnrollmentDTO issuerDTO,ReconEnrollmentDTO  hixDTO){
		EnrlReconDiscrepancy statusDiscp= null;

		if((issuerDTO.getEnrollmentStatus()!=null && "CANCEL".equalsIgnoreCase(issuerDTO.getEnrollmentStatus()) )&& (hixDTO.getEnrollmentStatus()==null || !"CANCEL".equalsIgnoreCase(hixDTO.getEnrollmentStatus()))){
			statusDiscp= new EnrlReconDiscrepancy();

			statusDiscp.setFieldName(ENROLLMENT_STATUS);
			if(issuerDTO.getExchangeAssignedPolicyid()!=null && !"".equals(issuerDTO.getExchangeAssignedPolicyid())){
				statusDiscp.setIssuerEnrollmentId(Integer.valueOf(issuerDTO.getExchangeAssignedPolicyid()));
			}
			if(hixDTO.getExchangeAssignedPolicyid()!=null && !"".equals(hixDTO.getExchangeAssignedPolicyid())){
				statusDiscp.setHixEnrollmentId(Long.valueOf(hixDTO.getExchangeAssignedPolicyid()));
			}
			statusDiscp.setHixValue(hixDTO.getEnrollmentStatus());
			statusDiscp.setIssuerValue(issuerDTO.getEnrollmentStatus());
			statusDiscp.setEnrlReconLkpId(findLkpIdByCode(ENROLLMENT_CANCELLED_IN_ISSUER_CODE));
		}
		return statusDiscp;
	}


	private List<EnrlReconDiscrepancy> checkAptcOverallocation(ReconEnrollmentDTO  issuerDTO, String hixEnrollmentId ){
		List<EnrlReconDiscrepancy> enrlReconDiscrepancies = new ArrayList<EnrlReconDiscrepancy>();
		if(issuerDTO!=null && issuerDTO.getMonthlyPremiums()!=null && !issuerDTO.getMonthlyPremiums().isEmpty()){
			List<ReconMonthlyPremiumDTO> issuerPremiums = issuerDTO.getMonthlyPremiums();
			for(ReconMonthlyPremiumDTO issuerPremium : issuerPremiums){
				EnrlReconDiscrepancy objEnrlReconDiscrepancy =null;
				Float premium = issuerPremium.getIssuerpremium();
				Float aptc = issuerPremium.getAptcAmt();
				boolean genDiscrep=false;
				if(premium!=null && aptc!=null ){
					if(aptc>premium){
						genDiscrep= true;
					}
				}else if(premium == null && aptc!=null ){
					if(aptc>0.0f){
						genDiscrep= true;
					}
				}
				if(genDiscrep){
					objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
					objEnrlReconDiscrepancy.setFieldName("overallocationAptcAmt_"+issuerPremium.getCoverageMonth());
					if(issuerDTO.getExchangeAssignedPolicyid()!=null && !"".equals(issuerDTO.getExchangeAssignedPolicyid())){
						objEnrlReconDiscrepancy.setIssuerEnrollmentId(Integer.valueOf(issuerDTO.getExchangeAssignedPolicyid()));
					}
					if(hixEnrollmentId!=null && !"".equals(hixEnrollmentId)){
						objEnrlReconDiscrepancy.setHixEnrollmentId(Long.valueOf(hixEnrollmentId));
					}
					objEnrlReconDiscrepancy.setHixValue(""+premium);
					objEnrlReconDiscrepancy.setIssuerValue(""+aptc);

					if(issuerPremium.getCoverageMonth()!=null){
						if(issuerPremium.getCoverageMonth()==1){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_JANUARY_CODE));
						}else if(issuerPremium.getCoverageMonth()==2){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_FEBURARY_CODE));
						}else if(issuerPremium.getCoverageMonth()==3){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_MARCH_CODE));
						}else if(issuerPremium.getCoverageMonth()==4){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_APRIL_CODE));
						}else if(issuerPremium.getCoverageMonth()==5){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_MAY_CODE));
						}else if(issuerPremium.getCoverageMonth()==6){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_JUNE_CODE));
						}else if(issuerPremium.getCoverageMonth()==7){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_JULY_CODE));
						}else if(issuerPremium.getCoverageMonth()==8){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_AUGUST_CODE));
						}else if(issuerPremium.getCoverageMonth()==9){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_SEPTEMBER_CODE));
						}else if(issuerPremium.getCoverageMonth()==10){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_OCTOBER_CODE));
						}else if(issuerPremium.getCoverageMonth()==11){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_NOVEMBER_CODE));
						}else if(issuerPremium.getCoverageMonth()==12){
							objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(OVERALLOCATION_OF_APTC_DECEMBER_CODE));
						}
					}
					enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
				}
			}//End of for
		}

		return enrlReconDiscrepancies;
	}

	private List<EnrlReconDiscrepancy> comparePremiums(ReconEnrollmentDTO  hixDTO, ReconEnrollmentDTO  issuerDTO ){
		List<EnrlReconDiscrepancy> enrlReconDiscrepancies = new ArrayList<EnrlReconDiscrepancy>();
		List<ReconMonthlyPremiumDTO> hixPremiums = null;
		List<ReconMonthlyPremiumDTO> issuerPremiums = null;
		ReconMonthlyPremiumDTO defaultPremium = new ReconMonthlyPremiumDTO();
		defaultPremium.setCoverageMonth(-1);
		if(hixDTO!=null){
			hixPremiums =hixDTO.getMonthlyPremiums();
			if(null == hixPremiums) {
				hixPremiums = new ArrayList<>();
				hixPremiums.add(defaultPremium);
				hixDTO.setMonthlyPremiums(hixPremiums);
			}
		}
		if(issuerDTO!=null){
			issuerPremiums = issuerDTO.getMonthlyPremiums();
			if(null == issuerPremiums) {
				issuerPremiums = new ArrayList<>();
				issuerPremiums.add(defaultPremium);
				issuerDTO.setMonthlyPremiums(issuerPremiums);
			}
		}
		if(hixPremiums !=null && !hixPremiums.isEmpty() && issuerPremiums!=null && !issuerPremiums.isEmpty()){
			List<ReconMonthlyPremiumDTO> removeHixPremiums = new ArrayList<ReconMonthlyPremiumDTO>();
			List<ReconMonthlyPremiumDTO> removeIssuerPremiums = new ArrayList<ReconMonthlyPremiumDTO>();

			ReconMemberDTO hixSubscriber = hixDTO.getSubscriber();
			ReconMemberDTO issuerSubscriber = issuerDTO.getSubscriber();

			int hixStartMonth = Integer.parseInt(hixSubscriber.getMemberBeginDate().substring(EnrollmentConstants.FOUR,EnrollmentConstants.SIX));
			int hixEndMonth = Integer.parseInt(hixSubscriber.getMemberEndDate().substring(EnrollmentConstants.FOUR,EnrollmentConstants.SIX));
			int issuerStartMonth = Integer.parseInt(issuerSubscriber.getMemberBeginDate().substring(EnrollmentConstants.FOUR,EnrollmentConstants.SIX));
			int issuerEndMonth = Integer.parseInt(issuerSubscriber.getMemberEndDate().substring(EnrollmentConstants.FOUR,EnrollmentConstants.SIX));

			int rangeStart = hixStartMonth > issuerStartMonth ? hixStartMonth : issuerStartMonth;
			int rangeEnd = hixEndMonth < issuerEndMonth ? hixEndMonth : issuerEndMonth;

			for(ReconMonthlyPremiumDTO tempHixPremium: hixPremiums){
				EnrlReconDiscrepancy objEnrlReconDiscrepancy =null;
				int tempHixCoverageMonth = -1;
				if(tempHixPremium!=null && tempHixPremium.getCoverageMonth()!=null){
					tempHixCoverageMonth = tempHixPremium.getCoverageMonth();
				}
				if(tempHixCoverageMonth < rangeStart ||  tempHixCoverageMonth > rangeEnd) {
					removeHixPremiums.add(tempHixPremium);
					continue;
				}
				boolean matchFound=false;
				for(ReconMonthlyPremiumDTO tempissuerPremium: issuerPremiums){
					int tempIssuerCoverageMonth = -1;
					if(tempissuerPremium!=null && tempissuerPremium.getCoverageMonth()!=null){
						tempIssuerCoverageMonth = tempissuerPremium.getCoverageMonth();
					}
					if(tempHixCoverageMonth==tempIssuerCoverageMonth){
						matchFound=true;
						break;
					}
				}
				if(!matchFound){
					/**
					 * "Premium Not in Carrier System but present in HIX DB
					 */
					if(tempHixPremium.getGrossPremium()!=null){//Set grossPremium Discrepancy
						objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
						setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
						enrlReconDiscrepancies.add(initialiazeDiscrepance("grossPremium",  tempHixPremium, null, objEnrlReconDiscrepancy));
						objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName("grossPremium_"+tempHixPremium.getCoverageMonth()));
					}
					if(tempHixPremium.getAptcAmt()!=null){//Set aptcAmt Discrepancy
						objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
						setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
						enrlReconDiscrepancies.add(initialiazeDiscrepance("aptcAmt",  tempHixPremium, null, objEnrlReconDiscrepancy));
						objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName("aptcAmt_"+tempHixPremium.getCoverageMonth()));
					}
					if(tempHixPremium.getCsrAmt()!=null){//Set csrAmt Discrepancy
						objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
						setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
						enrlReconDiscrepancies.add(initialiazeDiscrepance("csrAmt",  tempHixPremium, null, objEnrlReconDiscrepancy));
						objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName("csrAmt_"+tempHixPremium.getCoverageMonth()));
					}

					removeHixPremiums.add(tempHixPremium);
				}
			}//End of HIX For Loop
			/**
			 * Process list for monthlyPremium Present on Issuer-JSON but not is HIX-JSON
			 */
			for(ReconMonthlyPremiumDTO tempissuerPremium: issuerPremiums){
				EnrlReconDiscrepancy objEnrlReconDiscrepancy =null;
				int tempIssuerCoverageMonth = -1;
				if(tempissuerPremium!=null && tempissuerPremium.getCoverageMonth()!=null){
					tempIssuerCoverageMonth =tempissuerPremium.getCoverageMonth();
				}
				if(tempIssuerCoverageMonth < rangeStart ||  tempIssuerCoverageMonth > rangeEnd) {
					removeHixPremiums.add(tempissuerPremium);
					continue;
				}
				boolean matchFound=false;
				for(ReconMonthlyPremiumDTO tempHixPremium: hixPremiums){
					int tempHixCoverageMonth = -1;
					if(tempHixPremium!=null && tempHixPremium.getCoverageMonth()!=null){
						tempHixCoverageMonth = tempHixPremium.getCoverageMonth();
					}
					if(tempIssuerCoverageMonth==tempHixCoverageMonth){
						matchFound=true;
						break;
					}
				}
				if(!matchFound){
					/**
					 * "Premium Not in HIX DB System· But Present in carrier system 
					 */

					if(tempissuerPremium.getGrossPremium()!=null){//Set grossPremium Discrepancy
						objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
						setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
						enrlReconDiscrepancies.add(initialiazeDiscrepance("grossPremium",  null, tempissuerPremium, objEnrlReconDiscrepancy));	
						objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName("grossPremium_"+tempissuerPremium.getCoverageMonth()));
					}
					if(tempissuerPremium.getAptcAmt()!=null){//Set aptcAmt Discrepancy					
						objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
						setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
						enrlReconDiscrepancies.add(initialiazeDiscrepance("aptcAmt",  null, tempissuerPremium, objEnrlReconDiscrepancy));	
						objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName("aptcAmt_"+tempissuerPremium.getCoverageMonth()));
					}
					if(tempissuerPremium.getCsrAmt()!=null){//Set csrAmt Discrepancy
						objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
						setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
						enrlReconDiscrepancies.add(initialiazeDiscrepance("csrAmt",  null, tempissuerPremium, objEnrlReconDiscrepancy));
						objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName("csrAmt_"+tempissuerPremium.getCoverageMonth()));
					}

					removeIssuerPremiums.add(tempissuerPremium);
				}
			}//End of Issuer For Loop

			if(!removeHixPremiums.isEmpty()){			
				hixDTO.getMonthlyPremiums().removeAll(removeHixPremiums);
			}
			if(!removeIssuerPremiums.isEmpty()){
				issuerDTO.getMonthlyPremiums().removeAll(removeIssuerPremiums);
			}
		}
		return enrlReconDiscrepancies;
	}

	private List<EnrlReconDiscrepancy> compareMembers(ReconEnrollmentDTO  hixDTO, ReconEnrollmentDTO  issuerDTO ){
		List<EnrlReconDiscrepancy> enrlReconDiscrepancies = new ArrayList<EnrlReconDiscrepancy>();
		List<ReconMemberDTO> hixMembers = null;
		List<ReconMemberDTO> issuerMembers = null;
		if(hixDTO!=null){
			hixMembers =hixDTO.getMembers();
		}
		if(issuerDTO!=null){
			issuerMembers = issuerDTO.getMembers();
		}
		if(hixMembers !=null && !hixMembers.isEmpty() && issuerMembers!=null && !issuerMembers.isEmpty()){
			List<ReconMemberDTO> removeHixMembers = new ArrayList<ReconMemberDTO>();
			List<ReconMemberDTO> removeIssuerMembers = new ArrayList<ReconMemberDTO>();

			for(ReconMemberDTO tempHixMember: hixMembers){
				EnrlReconDiscrepancy objEnrlReconDiscrepancy =null;
				int tempHixMemberId = -1;
				if(tempHixMember!=null && tempHixMember.getExchangeAssignedMemberId()!=null){
					tempHixMemberId = Integer.parseInt(tempHixMember.getExchangeAssignedMemberId());
				}
				boolean matchFound=false;
				for(ReconMemberDTO tempissuerMember: issuerMembers){
					int tempIssuerMemberId = -1;
					if(tempissuerMember!=null && tempissuerMember.getExchangeAssignedMemberId()!=null){
						tempIssuerMemberId = Integer.parseInt(tempissuerMember.getExchangeAssignedMemberId());
					}
					if(tempHixMemberId==tempIssuerMemberId){
						matchFound=true;
						break;
					}
				}
				if(!matchFound){
					/**
					 * 8000_AB	Member Not in Carrier System	
					 * "Member Not in Carrier System·  
					 * On Discrepant Field pass [First Name Last Name and SSN] of the missing member"
					 */
					if(tempHixMember.getEnrolleeStatus()!= null &&  !("CANCEL".equalsIgnoreCase(tempHixMember.getEnrolleeStatus())) ){
						objEnrlReconDiscrepancy  = new EnrlReconDiscrepancy();
						setHixAndIssuerEnrollmentId(objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
						objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(MEMBER_NOT_IN_CARRIER_SYSTEM_CODE));
						objEnrlReconDiscrepancy.setEnrolleeId(tempHixMember.getEnrolleeId());
						if(EnrollmentUtils.isNotNullAndEmpty(tempHixMember.getExchangeAssignedMemberId())){
							objEnrlReconDiscrepancy.setMemberId(Integer.valueOf(tempHixMember.getExchangeAssignedMemberId()));
						}

						String fieldName =null;
						String fieldVal =null;
						//Set FirstName Discrepancy
						if(EnrollmentUtils.isNotNullAndEmpty(tempHixMember.getFirstName())){
							if(fieldName==null){
								fieldName="firstName";
								fieldVal=tempHixMember.getFirstName();
							}
						}
						//Set LastName Discrepancy					
						if(EnrollmentUtils.isNotNullAndEmpty(tempHixMember.getLastName()) ){
							if(fieldName==null){
								fieldName="lastName";
								fieldVal=tempHixMember.getLastName();
							}else{
								fieldName=fieldName+" "+"lastName";
								fieldVal=fieldVal+" "+tempHixMember.getLastName();
							}
						}
						//Set SSN Discrepancy
						/* commenting SSN setting as per email from linu on Wed 03/05/2017 11:57 AM - subject - Fwd: Can you review this?
						 * if(EnrollmentUtils.isNotNullAndEmpty(tempHixMember.getSsn())){
							if(fieldName==null){
								fieldName="ssn";
								fieldVal=tempHixMember.getSsn();
							}else{
								fieldName=fieldName+"|"+"ssn";
								fieldVal=fieldVal+"|"+tempHixMember.getSsn();
							}
						}*/
						objEnrlReconDiscrepancy.setFieldName(fieldName);
						objEnrlReconDiscrepancy.setHixValue(fieldVal);

						enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);
					}


					removeHixMembers.add(tempHixMember);
				}
			}//End of HIX For Loop
			for(ReconMemberDTO tempissuerMember: issuerMembers){
				EnrlReconDiscrepancy objEnrlReconDiscrepancy =null;
				int tempIssuerMemberId = -1;
				if(tempissuerMember!=null && tempissuerMember.getExchangeAssignedMemberId()!=null){
					tempIssuerMemberId = Integer.parseInt(tempissuerMember.getExchangeAssignedMemberId());
				}
				boolean matchFound=false;
				for(ReconMemberDTO tempHixMember: hixMembers){
					int tempHixMemberId = -1;
					if(tempHixMember!=null && tempHixMember.getExchangeAssignedMemberId()!=null){
						tempHixMemberId = Integer.parseInt(tempHixMember.getExchangeAssignedMemberId());
					}
					if(tempIssuerMemberId==tempHixMemberId){
						matchFound=true;
						break;
					}
				}
				if(!matchFound){
					/**
					 * 8000_AA 	Member Not in YHI System	
					 * "Member Not in YHI System·  
					 * On Discrepant Field pass [First Name Last Name and SSN] of the missing member  
					 */

					objEnrlReconDiscrepancy  = new EnrlReconDiscrepancy();
					setHixAndIssuerEnrollmentId(objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);
					objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(MEMBER_NOT_IN_YHI_SYSTEM_CODE));
					objEnrlReconDiscrepancy.setEnrolleeId(tempissuerMember.getEnrolleeId());
					if(EnrollmentUtils.isNotNullAndEmpty(tempissuerMember.getExchangeAssignedMemberId())){
						objEnrlReconDiscrepancy.setMemberId(Integer.valueOf(tempissuerMember.getExchangeAssignedMemberId()));
					}
					String fieldName =null;
					String fieldVal =null;
					//Set FirstName Discrepancy
					if(EnrollmentUtils.isNotNullAndEmpty(tempissuerMember.getFirstName())){
						if(fieldName==null){
							fieldName="firstName";
							fieldVal=tempissuerMember.getFirstName();
						}
					}
					//Set LastName Discrepancy					
					if(EnrollmentUtils.isNotNullAndEmpty(tempissuerMember.getLastName())){
						if(fieldName==null){
							fieldName="lastName";
							fieldVal=tempissuerMember.getLastName();
						}else{
							fieldName=fieldName+" "+"lastName";
							fieldVal=fieldVal+" "+tempissuerMember.getLastName();
						}
					}
					//Set SSN Discrepancy
					/*if(EnrollmentUtils.isNotNullAndEmpty(tempissuerMember.getSsn())){
						if(fieldName==null){
							fieldName="ssn";
							fieldVal=tempissuerMember.getSsn();
						}else{
							fieldName=fieldName+"|"+"ssn";
							fieldVal=fieldVal+"|"+tempissuerMember.getSsn();
						}
					}*/

					objEnrlReconDiscrepancy.setFieldName(fieldName);
					//objEnrlReconDiscrepancy.setHixValue(fieldVal);
					objEnrlReconDiscrepancy.setIssuerValue(fieldVal);

					enrlReconDiscrepancies.add(objEnrlReconDiscrepancy);

					removeIssuerMembers.add(tempissuerMember);
				}
			}//End of Issuer For Loop

			if(!removeHixMembers.isEmpty()){			
				hixDTO.getMembers().removeAll(removeHixMembers);
			}
			if(!removeIssuerMembers.isEmpty()){
				issuerDTO.getMembers().removeAll(removeIssuerMembers);
			}
			/**
			 * Removing duplicate non-subscriber members
			 */
			removeDuplicateMembers(issuerDTO);
			removeCancelledMembers(hixDTO);
			balanceRepeatedMembers(hixDTO, issuerDTO);
		}
		return enrlReconDiscrepancies;
	}

	private void balanceRepeatedMembers(ReconEnrollmentDTO  hixDTO, ReconEnrollmentDTO issuerDTO){
		List<ReconMemberDTO> updatedHixMembers = new ArrayList<ReconMemberDTO>();
		List<ReconMemberDTO> updatedIssuerMembers = new ArrayList<ReconMemberDTO>();
		List<ReconMemberDTO> hixMembers = hixDTO.getMembers();//Arrays.asList(1,1,2,3,4,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7);
		List<ReconMemberDTO> issuerMembers = issuerDTO.getMembers();//Arrays.asList(1,2,2,2,2,2,3,4,4,5,6,7,7,7,7,7,7,7);

		Map<String,Integer> hixMap= new HashMap<String,Integer>();//Map<MemberId, Count for that member>
		Map<String,Integer> issuerMap= new HashMap<String,Integer>();

		for(int i=0; i<hixMembers.size(); i++){
			Integer val = hixMap.get(hixMembers.get(i).getExchangeAssignedMemberId());
			if(val==null){
				hixMap.put(hixMembers.get(i).getExchangeAssignedMemberId(), new Integer(1));
			}else{
				hixMap.put(hixMembers.get(i).getExchangeAssignedMemberId(), val+1);
			}

		}
		for(int i=0; i<issuerMembers.size(); i++){
			Integer val = issuerMap.get(issuerMembers.get(i).getExchangeAssignedMemberId());
			if(val==null){
				issuerMap.put(issuerMembers.get(i).getExchangeAssignedMemberId(), new Integer(1));
			}else{
				issuerMap.put(issuerMembers.get(i).getExchangeAssignedMemberId(), val+1);
			}
		}

		for (String memberId : hixMap.keySet()){
			Integer hixCount = hixMap.get(memberId);
			Integer issuerCount =issuerMap.get(memberId);
			int maxRepeatCount = 0;
			if(hixCount>=issuerCount){
				maxRepeatCount = hixCount;
			}else{
				maxRepeatCount= issuerCount;
			}
			int hixCnt =0;
			for(ReconMemberDTO member : hixMembers){
				if(memberId.equalsIgnoreCase(member.getExchangeAssignedMemberId())){
					updatedHixMembers.add(member);
					hixCnt++;
				}
			}
			if(hixCnt<maxRepeatCount){
				for(int i=0; i<maxRepeatCount-hixCnt; i++){
					ReconMemberDTO emptyMember = new ReconMemberDTO();
					emptyMember.setExchangeAssignedMemberId(memberId);
					updatedHixMembers.add(emptyMember);
				}
			}
			int issuerCnt =0;
			for(ReconMemberDTO member : issuerMembers){
				if(memberId.equalsIgnoreCase(member.getExchangeAssignedMemberId())){
					updatedIssuerMembers.add(member);
					issuerCnt++;
				}
			}
			if(issuerCnt<maxRepeatCount){
				for(int i=0; i<maxRepeatCount-issuerCnt; i++){
					ReconMemberDTO emptyMember = new ReconMemberDTO();
					emptyMember.setExchangeAssignedMemberId(memberId);
					updatedIssuerMembers.add(emptyMember);
				}
			}
		}//End of For 

		hixDTO.setMembers(updatedHixMembers);
		issuerDTO.setMembers(updatedIssuerMembers);
	}

	private EnrlReconDiscrepancy  initialiazeDiscrepance(String fieldName, ReconMonthlyPremiumDTO  hixDTO, ReconMonthlyPremiumDTO issuerDTO, EnrlReconDiscrepancy objEnrlReconDiscrepancy){
		if(EnrollmentUtils.isNotNullAndEmpty(fieldName)){
			if(objEnrlReconDiscrepancy==null){
				objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();	
			}

			Method getter = null;
			try {
				if(hixDTO!=null){
					objEnrlReconDiscrepancy.setFieldName(fieldName+"_"+hixDTO.getCoverageMonth());
					objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName(fieldName+"_"+hixDTO.getCoverageMonth()));
				}else{
					objEnrlReconDiscrepancy.setFieldName(fieldName+"_"+issuerDTO.getCoverageMonth());
					objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName(fieldName+"_"+issuerDTO.getCoverageMonth()));
				}

				//String getterMethodName = getGetterMethodName(fieldName);
				getter = ReconMonthlyPremiumDTO.class.getMethod(getGetterMethodName(fieldName));
				if(hixDTO!=null && getter!=null && getter.invoke(hixDTO)!=null){
					String fieldValue = getter.invoke(hixDTO).toString();
					objEnrlReconDiscrepancy.setHixValue(fieldValue);
					LOGGER.info(" HIX Value : "+fieldValue);
				}
				if(issuerDTO!=null && getter!=null && getter.invoke(issuerDTO)!=null){
					String fieldValue = getter.invoke(issuerDTO).toString();
					objEnrlReconDiscrepancy.setIssuerValue(fieldValue);
					LOGGER.info("Issuer Value : "+fieldValue);
				}
				//objEnrlReconDiscrepancy.setFieldName(fieldName);


			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			return objEnrlReconDiscrepancy;
		}
		return objEnrlReconDiscrepancy;
	}

	private EnrlReconDiscrepancy  initialiazeDiscrepance(String fieldName, ReconMemberDTO  hixDTO, ReconMemberDTO issuerDTO, String discrepLevel, EnrlReconDiscrepancy objEnrlReconDiscrepancy){
		if(EnrollmentUtils.isNotNullAndEmpty(fieldName)){
			if(objEnrlReconDiscrepancy==null){
				objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();	
			}

			Method getter = null;
			try {
				objEnrlReconDiscrepancy.setFieldName(fieldName);
				objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName(fieldName));

				//String getterMethodName = getGetterMethodName(fieldName);
				getter = ReconMemberDTO.class.getMethod(getGetterMethodName(fieldName));
				if(hixDTO!=null && getter!=null && getter.invoke(hixDTO)!=null){
					String fieldValue = getter.invoke(hixDTO).toString();
					objEnrlReconDiscrepancy.setHixValue(fieldValue);
					LOGGER.info(" HIX Value : "+fieldValue);
				}
				if(issuerDTO!=null &&  getter!=null && getter.invoke(issuerDTO)!=null){
					String fieldValue = getter.invoke(issuerDTO).toString();
					objEnrlReconDiscrepancy.setIssuerValue(fieldValue);
					if("mailingAddress1".equalsIgnoreCase(fieldName) 
							&& "Bad Address".equalsIgnoreCase(fieldValue)){
						objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByCode(BAD_MAILING_ADDRESS_CODE));
					}

					LOGGER.info("Issuer Value : "+fieldValue);
				}


				getter = ReconMemberDTO.class.getMethod(getGetterMethodName("exchangeAssignedMemberId"));
				if(hixDTO!=null && getter!=null && getter.invoke(hixDTO)!=null){
					String fieldValue= getter.invoke(hixDTO).toString();
					objEnrlReconDiscrepancy.setMemberId(Integer.valueOf(fieldValue));
					LOGGER.info("exchangeAssignedMemberId Value : "+fieldValue);
				}
				//enrolleeId

				getter = ReconMemberDTO.class.getMethod(getGetterMethodName("enrolleeId"));
				if(hixDTO!=null && getter!=null && getter.invoke(hixDTO)!=null){
					String fieldValue= getter.invoke(hixDTO).toString();
					objEnrlReconDiscrepancy.setEnrolleeId(Integer.valueOf(fieldValue));
					LOGGER.info("exchangeAssignedMemberId Value : "+fieldValue);
				}

			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			return objEnrlReconDiscrepancy;
		}
		return objEnrlReconDiscrepancy;
	}

	private EnrlReconDiscrepancy  initialiazeDiscrepance(String fieldName, ReconEnrollmentDTO  hixDTO, ReconEnrollmentDTO issuerDTO){
		EnrlReconDiscrepancy objEnrlReconDiscrepancy = null;
		if(EnrollmentUtils.isNotNullAndEmpty(fieldName) ){
			objEnrlReconDiscrepancy = new EnrlReconDiscrepancy();
			Method getter = null;
			try {
				objEnrlReconDiscrepancy.setFieldName(fieldName);
				objEnrlReconDiscrepancy.setEnrlReconLkpId(findLkpIdByFieldName(fieldName));
				String getterMethodName = getGetterMethodName(fieldName);
				getter = ReconEnrollmentDTO.class.getMethod(getterMethodName);
				if(getter!=null && getter.invoke(hixDTO)!=null){
					String fieldValue = getter.invoke(hixDTO).toString();
					objEnrlReconDiscrepancy.setHixValue(fieldValue);
					LOGGER.info(" HIX Value : "+fieldValue);
				}
				if(getter!=null && getter.invoke(issuerDTO)!=null){
					String fieldValue = getter.invoke(issuerDTO).toString();
					objEnrlReconDiscrepancy.setIssuerValue(fieldValue);
					LOGGER.info("Issuer Value : "+fieldValue);
				}
				objEnrlReconDiscrepancy.setFieldName(fieldName);
				setHixAndIssuerEnrollmentId( objEnrlReconDiscrepancy,   hixDTO,  issuerDTO);

			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			return objEnrlReconDiscrepancy;
		}
		return objEnrlReconDiscrepancy;
	}

	private void setHixAndIssuerEnrollmentId(EnrlReconDiscrepancy objEnrlReconDiscrepancy, ReconEnrollmentDTO  hixDTO, ReconEnrollmentDTO issuerDTO){

		//exchangeAssignedMemberId, 
		Method getter;
		String hixEnlrGetterMethodName = getGetterMethodName("exchangeAssignedPolicyid");
		try {
			getter = ReconEnrollmentDTO.class.getMethod(hixEnlrGetterMethodName);
			if(getter!=null && getter.invoke(hixDTO)!=null){
				String fieldValue= getter.invoke(hixDTO).toString();
				objEnrlReconDiscrepancy.setHixEnrollmentId(Long.valueOf(fieldValue));
				LOGGER.info(" HIX Enrollment Value : "+fieldValue);
			}

			if(getter!=null && getter.invoke(issuerDTO)!=null){
				String fieldValue= getter.invoke(issuerDTO).toString();
				objEnrlReconDiscrepancy.setIssuerEnrollmentId(Integer.valueOf(fieldValue));
				LOGGER.info(" Issuer Enrollment Value : "+fieldValue);
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

	}

	private String getGetterMethodName(String fieldName){
		String str =null;
		if(EnrollmentUtils.isNotNullAndEmpty(fieldName)){
			fieldName = fieldName.trim();
			str = "get"+fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1,fieldName.length()).replaceAll(" ","");
		}
		return str;
	}

	private void processDiffMemberLength(String errorString, ReconEnrollmentDTO  hixDTO, ReconEnrollmentDTO issuerDTO){
		LOGGER.info("Array monthlyPremiums has different length :");
	}
	private void processDiffPremiumLength(String errorString, ReconEnrollmentDTO  hixDTO, ReconEnrollmentDTO issuerDTO){
		LOGGER.info("Array members has different length :");
	}


	/**
	 * @author panda_p
	 * @since Mar/07/2017
	 * 
	 * Compares two input JSON Strings
	 * 
	 * @param json1
	 * @param json2
	 * @return Comparison result String
	 */
	private static String compareJsonString(String json1, String json2){
		String compareResult = "FAILURE";
		try{
			assertJsonEquals(json1, json2, when(TREATING_NULL_AS_ABSENT));
			compareResult = "SUCCESS";
		}catch(AssertionError e ){
			compareResult = e.getMessage();
		}
		return compareResult;
	}


	/**
	 * 
	 * @param fieldName
	 * @return
	 */
	private EnrlReconLkp findLkpIdByFieldName(String fieldName){
		EnrlReconLkp objEnrlReconLkp=null;
		if(fieldName!=null){
			List<Integer> lkpIds = iEnrlReconLkpRepository.findLkpIdByFieldName(fieldName);
			if(lkpIds!=null && !lkpIds.isEmpty()){
				objEnrlReconLkp = new EnrlReconLkp();
				objEnrlReconLkp.setId(lkpIds.get(0));
			}
		}
		return objEnrlReconLkp;
	}
	/**
	 * 
	 * @param code
	 * @return
	 */
	private EnrlReconLkp findLkpIdByCode(String code){
		EnrlReconLkp objEnrlReconLkp=null;
		if(code!=null){
			List<Integer> lkpIds = iEnrlReconLkpRepository.findLkpIdByCode(code);
			if(lkpIds!=null && !lkpIds.isEmpty()){
				objEnrlReconLkp = new EnrlReconLkp();
				objEnrlReconLkp.setId(lkpIds.get(0));
			}
		}
		return objEnrlReconLkp;
	}

	private void triggerBatchJobWithParams(Long fileId,  Job job,JobLauncher jobLauncher) {

		LOGGER.info("Triggering AutoFix Job");
		//Trigger batch Job
		try {
			JobParameters jobParameters = new JobParametersBuilder()
					.addString("fileId", fileId+"")
					.addLong("EXECUTION_DATE", new Date().getTime())
					.toJobParameters();

			JobExecution jobExecution = jobLauncher.run(job,jobParameters);

			LOGGER.info("Wait till job completes");
			/*while(jobExecution.isRunning()){
			}
			LOGGER.info("Job complete with exit status : " + jobExecution.getExitStatus());*/
		} catch (Exception e) {
			LOGGER.error("Error in AutoFix Job batch job from Snapshot Compare Job", e);
		}
	}

	/**
	 * Process missing enrollment discrepancies by file ID
	 * @param fileId
	 * @param missingEnrlSnapshotMap 
	 * @throws Exception
	 */
	private void processMissingEnrollmentDiscrepancies(Integer fileId, Map<Long, Integer> missingEnrlSnapshotMap) throws Exception{
		if(EnrollmentUtils.isNotNullAndEmpty(fileId)){
			EnrlReconSummary summary= iEnrlReconSummaryRepository.getByFileId(fileId);
			if(summary!=null ){
				Date issuerCutoffDate=null;
				Date issuerFileExtractDate=null;
				issuerFileExtractDate=summary.getIssuerExtractDate();
				if(issuerFileExtractDate ==null){
					//throw new Exception("Issuer Extract Date is null for file :: "+summary.getId());
					issuerFileExtractDate=EnrollmentUtils.getMonthStartDate(new Date());
				}
				if(issuerCutoffDate==null || issuerFileExtractDate.after(issuerCutoffDate)){
					issuerCutoffDate=issuerFileExtractDate;
				}
				//Deduct cutoff Date from latest FileDate
				String cutOffDays=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_MISSING_ENROLLMENT_LOOKBACK_DAYS);
				if(EnrollmentUtils.isNotNullAndEmpty(cutOffDays) && StringUtils.isNumeric(cutOffDays.trim())){
					issuerCutoffDate=EnrollmentUtils.removeTimeFromDate(EnrollmentUtils.getBeforeDayDate(issuerCutoffDate, Integer.parseInt(cutOffDays.trim())-1));
				}
				List<Long> enrlMissingInFile= iEnrlReconSnapshotRepository.getEnrollmentsMissingInFile(fileId);
				List<Long> enrlMissingInHIX= iEnrlReconSnapshotRepository.getEnrollmentsMissingInHix(fileId);
				updateFileSummary(summary, issuerCutoffDate, enrlMissingInFile, enrlMissingInHIX);
				setMissingEnrollmentDiscrepancies(fileId, enrlMissingInFile, enrlMissingInHIX, missingEnrlSnapshotMap);
			}
		}
	}

	/**
	 * Create missing enrollment discrepancies
	 * @param fileId
	 * @param enrlMissingInFile
	 * @param enrlMissingInHIX
	 * @param missingEnrlSnapshotMap 
	 */
	private void setMissingEnrollmentDiscrepancies(Integer fileId, List<Long> enrlMissingInFile, List<Long> enrlMissingInHIX, Map<Long, Integer> missingEnrlSnapshotMap) {
		List<EnrlReconDiscrepancy> discrepancyList = new ArrayList<>();
		if(null != enrlMissingInFile && !enrlMissingInFile.isEmpty()) {
			EnrlReconLkp lkp= findLkpIdByCode(ENRL_MISSING_IN_FILE);
			enrlMissingInFile.forEach(missingInfile -> addMissingEnrlDiscrepancy(missingInfile, fileId, lkp, discrepancyList, missingEnrlSnapshotMap.get(missingInfile)));
		}
		if(null != enrlMissingInHIX && !enrlMissingInHIX.isEmpty()) {
			EnrlReconLkp lkp= findLkpIdByCode(ENRL_MISSING_IN_HIX);
			enrlMissingInHIX.forEach(missingInHix -> addMissingEnrlDiscrepancy(missingInHix, fileId, lkp, discrepancyList, missingEnrlSnapshotMap.get(missingInHix)));
		}
		try {
			iEnrlReconDiscrepancyRepository.save(discrepancyList);
		}catch(Exception e) {
			LOGGER.error("Error saving missing enrollment discrepancies");
		}
	}

	/**
	 * Set missing enrollment discrepancy
	 * @param missingEnrollment
	 * @param fileId
	 * @param lkp
	 * @param discrepancyList
	 * @param snapshotId 
	 */
	private void addMissingEnrlDiscrepancy(Long missingEnrollment, Integer fileId, EnrlReconLkp lkp, List<EnrlReconDiscrepancy> discrepancyList, Integer snapshotId) {
		EnrlReconDiscrepancy discrepancy = new EnrlReconDiscrepancy();
		discrepancy.setFileId(fileId);
		discrepancy.setEnrlReconLkpId(lkp);
		discrepancy.setHixEnrollmentId(missingEnrollment);
		discrepancy.setSnapshotId(snapshotId);
		discrepancy.setStatus(DiscrepancyStatus.OPEN);
		discrepancyList.add(discrepancy);
	}

	/**
	 * Update file summary with missing enrollment discrepancy counts
	 * @param summary
	 * @param issuerCutoffDate
	 * @param enrlMissingInFile
	 * @param enrlMissingInHIX
	 */
	private void updateFileSummary(EnrlReconSummary summary, Date issuerCutoffDate, List<Long> enrlMissingInFile, List<Long> enrlMissingInHIX) {
		if(null != summary) {
			summary.setEnrlCntMissingInHix(null != enrlMissingInHIX ? enrlMissingInHIX.size() : 0);
			summary.setEnrlCntMissingInFile(null != enrlMissingInFile ? enrlMissingInFile.size() : 0);
			summary.setIssuerCutoffDate(issuerCutoffDate);
			summary.setStatus(EnrlReconSummary.SummaryStatus.SUMMARIZED.toString());
			iEnrlReconSummaryRepository.saveAndFlush(summary);
		}
	}

	public static boolean isCSRDiscrepant(String insuranceTypeLkp,String csLevel,String hixValue,String issuerValue){
		boolean isDiscrepant=true;
		Double issuerCSRAmount=null;
		Double hixCSRAmount=null;
		if(EnrollmentUtils.isNotNullAndEmpty(issuerValue)){
			issuerCSRAmount= Double.valueOf(issuerValue);
		}
		if(EnrollmentUtils.isNotNullAndEmpty(hixValue)){
			hixCSRAmount= Double.valueOf(hixValue);
		}
		if(EnrollmentUtils.isNotNullAndEmpty(csLevel) && EnrollmentUtils.isNotNullAndEmpty(insuranceTypeLkp)){
			if((csLevel.equalsIgnoreCase("01") || insuranceTypeLkp.equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_DENTAL_CODE) ) ){
				if (issuerCSRAmount==null || issuerCSRAmount==0){
					isDiscrepant=false;
				}

			}else if(insuranceTypeLkp.equalsIgnoreCase(EnrollmentConstants.INSURANCE_TYPE_HEALTH_CODE) && issuerCSRAmount!=null && hixCSRAmount!=null && isDoubleEqual(issuerCSRAmount,hixCSRAmount, 2)){
				isDiscrepant=false;
			}
		}

		return isDiscrepant;
	}


	public static  <T> boolean isDoubleEqual(double e1, double e2, int floatingPoint){
		boolean isEqual=false;
		try{
			if(!EnrollmentUtils.isNotNullAndEmpty(e1) && !EnrollmentUtils.isNotNullAndEmpty(e2)){
				isEqual=true;
			}else if(EnrollmentUtils.isNotNullAndEmpty(e1) && EnrollmentUtils.isNotNullAndEmpty(e2)){
				double num=Math.pow(10, floatingPoint);
				Double d1=Math.floor(e1 * num) / num;
				Double d2=Math.floor(e2 * num) / num;
				if((d1.compareTo(d2)== 0)){
					isEqual=true;
				}
			}
		}catch(Exception e){
			//LOGGER.error("Error in parsing to float.",e);
			isEqual=false;
		}
		return isEqual;
	}
	
	/** 
	 * Remove duplicate members from the enrollment DTO
	 * @param enrollmentDTO
	 */
	private void removeDuplicateMembers(ReconEnrollmentDTO enrollmentDTO) {
		List<ReconMemberDTO> members = enrollmentDTO.getMembers();
		Set<String> comparisonStringSet = new HashSet<>();
		List<ReconMemberDTO> duplicateMembers = new ArrayList<>();
		for(ReconMemberDTO member : members) {
			if(!"Y".equalsIgnoreCase(member.getSubscriberIndicator()) 
					&& null != member.getExchangeAssignedMemberId() 
					&& null != member.getMemberBeginDate()
					&& null != member.getMemberEndDate()) {
				String searchCombinationStr = (member.getExchangeAssignedMemberId() + member.getMemberBeginDate() + member.getMemberEndDate()).trim();
				if(comparisonStringSet.stream().anyMatch(e -> e.equalsIgnoreCase(searchCombinationStr))) {
					duplicateMembers.add(member);
				}else {
					comparisonStringSet.add(searchCombinationStr);
				}
			}
		}
		if(!duplicateMembers.isEmpty()) {
			enrollmentDTO.getMembers().removeAll(duplicateMembers);			
		}
	}
	
	/** 
	 * Remove cancelled members from the enrollment DTO
	 * @param enrollmentDTO
	 */
	private void removeCancelledMembers(ReconEnrollmentDTO enrollmentDTO) {
		List<ReconMemberDTO> members = enrollmentDTO.getMembers();
		List<ReconMemberDTO> cancelledMembers = members.stream().filter(isMemberCancelled).collect(Collectors.toList());

		Set<String> cancelledMemberIds = cancelledMembers.stream().map(e -> e.getExchangeAssignedMemberId())
				.collect(Collectors.toSet());

		Set<String> removeIdList = members.stream().filter(
				e -> cancelledMemberIds.contains(e.getExchangeAssignedMemberId()) && isMemberCancelled.negate().test(e))
				.map(e -> e.getExchangeAssignedMemberId()).collect(Collectors.toSet());
		
		List<ReconMemberDTO> removeList = cancelledMembers.stream()
				.filter(e -> removeIdList.contains(e.getExchangeAssignedMemberId())).collect(Collectors.toList());
		
		if (null != removeList && !removeList.isEmpty()) {
			enrollmentDTO.getMembers().removeAll(removeList);
		}

	}

}
