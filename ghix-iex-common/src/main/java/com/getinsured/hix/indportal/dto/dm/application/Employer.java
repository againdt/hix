package com.getinsured.hix.indportal.dto.dm.application;

public class Employer {
	 private String name;
	 private String employerPhoneNumber;


	 // Getter Methods 

	 public String getName() {
	  return name;
	 }

	 public String getEmployerPhoneNumber() {
	  return employerPhoneNumber;
	 }

	 // Setter Methods 

	 public void setName(String name) {
	  this.name = name;
	 }

	 public void setEmployerPhoneNumber(String employerPhoneNumber) {
	  this.employerPhoneNumber = employerPhoneNumber;
	 }
	}
