/**
 * 
 */
package com.getinsured.hix.batch.enrollment.skip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 * @author negi_s
 *
 */
@Component("enrollment1095StagingParams")
public class Enrollment1095StagingParams {
	List<Integer> enrollmentIdList=null;
	Map<String,String> skippedEnrollmentMap=null;
	private boolean isFreshRun;
	private Integer totalRecordCount;
	private Integer totalUpdateCount;
	private Integer totalFailedCount;
	private Integer totalOverWrittenCount;

	public Enrollment1095StagingParams() {
		this.enrollmentIdList = new ArrayList<Integer>();
		this.skippedEnrollmentMap = new HashMap<String,String>();;
		this.isFreshRun = false;
		this.totalRecordCount = 0;
		this.totalUpdateCount = 0;
		this.totalFailedCount = 0;
		this.totalOverWrittenCount = 0;
	}

	public boolean isFreshRun() {
		return isFreshRun;
	}

	public synchronized void setFreshRun(boolean isFreshRun) {
		this.isFreshRun = isFreshRun;
	}

	public Integer getTotalRecordCount() {
		return totalRecordCount;
	}

	public synchronized void setTotalRecordCount(Integer totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

	public Integer getTotalUpdateCount() {
		return totalUpdateCount;
	}

	public synchronized void incrementTotalUpdateCount(Integer incrementBy) {
		this.totalUpdateCount += incrementBy;
	}

	public Integer getTotalOverWrittenCount() {
		return totalOverWrittenCount;
	}

	public synchronized void incrementTotalOverWrittenCount(Integer incrementBy) {
		this.totalOverWrittenCount += incrementBy;
	}
	
	public synchronized void addAllToEnrollmentIdList(List<Integer> enrollmentIdList){
		this.enrollmentIdList.addAll(enrollmentIdList);
	}
	
	public synchronized void addToEnrollmentIdList(Integer enrollmentId){
		this.enrollmentIdList.add(enrollmentId);
	}
	
	public List<Integer> getEnrollmentIdList(){
		return enrollmentIdList;
	}
	public synchronized void clearEnrollmentIdList(){
		this.enrollmentIdList.clear();
	}

	public synchronized void putToSkippedEnrollmentMap(String key, String value){
		skippedEnrollmentMap.put(key, value);
	}
	public synchronized void putAllToSkippedEnrollmentMap(Map<String,String> failedHouseholdMap){
		this.skippedEnrollmentMap.putAll(failedHouseholdMap);
	}	
	
	public Map<String,String> getSkippedEnrollmentMap(){
		return skippedEnrollmentMap;
	}
	
	public synchronized void clearSkippedEnrollmentMap(){
		this.skippedEnrollmentMap.clear();
	}	
	
	public Integer getTotalFailedCount() {
		return totalFailedCount;
	}
	
	public synchronized void incrementTotalFailedCount(Integer incrementBy) {
		this.totalFailedCount += incrementBy;
	}
	

	public synchronized void resetCounters(){
		this.totalRecordCount = 0;
		this.totalUpdateCount = 0;
		this.totalOverWrittenCount = 0;
		this.totalFailedCount = 0;
		this.skippedEnrollmentMap.clear();
	}
}
