package com.getinsured.hix.batch.enrollment.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.launch.JobLauncher;
import com.getinsured.hix.model.enrollment.ReconDetailCsvDto;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author meher_a
 *
 */
public interface EnrollmentReconciliationService {
	
	String populateReconciliationData(String fileName, Long batchExecutionId, JobService jobService, StepExecution stepExecution, File archivePath, JobLauncher jobLauncher, Job job)  throws Exception;
	
	public ReconDetailCsvDto convertStringToreconDetailCsvDto(String line);
	
	void processReconciliationInFile(JobService jobService, StepExecution stepExecution,  JobLauncher jobLauncher, Job job) throws IOException, GIException, InterruptedException;
	
	/**
	 * @author meher_a
	 * @since 1 April 2017
	 * @param jobId
	 * 
	 * Send the notification about new/updated Reconciliation
	 */
	void sendReconciliationNotification(Long jobId) throws GIException;
	
	/**
	 * @author panda_p
	 * @since 30-MAR-2017
	 * @param cutOffDate
	 * 
	 * Deletes the recornData table records upto given cutOffDate
	 */
	void deleteReconData(String cutOffDate, long jobExecutionId);

}
