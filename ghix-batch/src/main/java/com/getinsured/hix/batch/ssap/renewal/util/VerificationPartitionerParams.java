package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Component;

/**
 * DTO class is used to add Verification Job parameters.
 * 
 * @since July 30, 2019
 */
@Component("verificationPartitionerParams")
public class VerificationPartitionerParams {

	private List<Long> ssapApplicationIdList = null;
	private AtomicLong renewalYear;

	public VerificationPartitionerParams() {
		this.ssapApplicationIdList = new CopyOnWriteArrayList<Long>();
	}

	public List<Long> getSsapApplicationIdList() {
		return ssapApplicationIdList;
	}

	public synchronized void clearSsapApplicationIdList() {
		this.ssapApplicationIdList.clear();
	}

	public synchronized void addAllToSsapApplicationIdList(List<Long> ssapApplicationList) {
		this.ssapApplicationIdList.addAll(ssapApplicationList);
	}

	public synchronized void addToSsapApplicationIdList(Long ssapApplicationId) {
		this.ssapApplicationIdList.add(ssapApplicationId);
	}

	public AtomicLong getRenewalYear() {
		return renewalYear;
	}

	public void setRenewalYear(long renewalYear) {
		this.renewalYear = new AtomicLong(renewalYear);
	}
}
