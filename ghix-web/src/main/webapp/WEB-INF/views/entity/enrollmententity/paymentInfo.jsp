<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<META http-equiv="Cache-Control" content="max-age=0" />
<META http-equiv="Cache-Control" content="no-cache,no-store,must-revalidate" />
<META HTTP-EQUIV="Expires" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
<META http-equiv="Pragma" content="no-cache" />

<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>
<!-- Content -->

<!-- #sidebar -->
<div class="gutter10">

	<%-- <div class="row-fluid">
		<div class="page-header">
			<h1 id="addAssisterStepHeader" class="margin0"><spring:message code="label.entity.step"/> 7: <spring:message code="label.entity.paymentInformation"/></h1>
		</div>
	</div> --%>


	<div class="row-fluid margin20-t">
		<jsp:include page="../leftNavigationMenu.jsp" />

		<div class="span9" id="rightpanel">
			<div class="header">
				<h4><spring:message code="label.entity.step"/> 7: <spring:message code="label.entity.paymentInformation"/></h4>
			</div>
		
		<!-- buttons commented out and added to the bottom of the file due to the HIX-13567 -->
				<%-- <c:choose>
					<c:when test="${isAdmin == 'true'}">
						<h1><spring:message code="label.entity.payment"/></h1>
						<a class="btn" type="button" href="/hix/entity/entityadmin/viewpaymentinfo/${enrollmentEntity.id}"><spring:message code="label.entity.cancel"/></a>
						<a class="btn btn-primary pull-right" id="save" name="save"><spring:message code="label.entity.save"/></a>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${paymentMethodsObj != null}">
								<h1><spring:message code="label.entity.payment"/></h1>
								<a class="btn" type="button" href="/hix/entity/enrollmententity/viewpaymentinfo"><spring:message code="label.entity.cancel"/></a>
								<a class="btn btn-primary pull-right" id="save" name="save"><spring:message code="label.entity.save"/></a>
							</c:when>
							<c:otherwise>
								<h1><spring:message code="label.entity.payment"/></h1>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
				 --%>
			
       		
       		<div class="gutter10">
			<form class="form-horizontal entityAddressValidation" id="frmentityeditpayment" name="frmentityeditpayment" method="POST" autocomplete="off" >
			<df:csrfToken/>
				<input type="hidden" id="paymentId" name="paymentId" value="<encryptor:enc value="${paymentMethodsObj.id}"/>" />
				<input type="hidden" name="loggedUser" id="loggedUser" value="${loggedUser}"/>
				<input type="hidden" name="entityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>"/>  
				<input type="hidden" name="entityStatus" id = "entityStatus" value="${enrollmentEntity.registrationStatus}"/>
				<c:choose>
					<c:when test="${paymentMethodsObj.id == null}">
						<input type="hidden" id="redirectTo" name="redirectTo"
							value="addpaymentinfo" />
					</c:when>
					<c:otherwise>
						<input type="hidden" id="redirectTo" name="redirectTo"
							value="editpaymentinfo" />
					</c:otherwise>
				</c:choose>

				<div class="control-group">
	              <fieldset>
	                <legend class="control-label" for="receivedPayments"><spring:message code="label.entity.receivePayments"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></legend>
	                <div class="controls">
	                   <c:choose>
	                  	<c:when test="${enrollmentEntity.receivedPayments != null}">
	                  		<label class="radio" for="receivedPayments1">
			                  	<c:choose>
									<c:when test="${enrollmentEntity.receivedPayments  == '1'}">
										<input type="radio" name="receivedPayments" id="receivedPayments1" value="1" checked='checked' onclick="$('#paymentinfodivid').show();"/>
									</c:when>
									<c:otherwise>
										  <input type="radio" name="receivedPayments" id="receivedPayments1" value="1" onclick="$('#paymentinfodivid').show();"/>
									</c:otherwise>
								</c:choose>                   
			                    <spring:message code="label.entity.yes"/>
			                  </label>
			                  
			                  <label class="radio" for="receivedPayments2">
			                   	 <c:choose>
									<c:when test="${enrollmentEntity.receivedPayments  == '0'}">
										 <input type="radio" name="receivedPayments" id="receivedPayments2" value="0" checked='checked' onclick="$('#paymentinfodivid').hide()"/>
									</c:when>
									<c:otherwise>
										 <input type="radio" name="receivedPayments" id="receivedPayments2" value="0" onclick="$('#paymentinfodivid').hide()"/>
									</c:otherwise>
								</c:choose>                   
			                    <spring:message code="label.entity.no"/> 
			                  </label>
		                  	</c:when>
		                  	<c:otherwise>
	                  			 <label class="radio" for="receivedPayments1">
				   					<input type="radio" name="receivedPayments" id="receivedPayments1" value="1" onclick="$('#paymentinfodivid').show()"/>
					              	<spring:message code="label.entity.yes"/>
				                  </label>
				                  
				                  <label class="radio" for="receivedPayments2">
			                  		<input type="radio" name="receivedPayments" id="receivedPayments2" value="0" checked="checked" onclick="$('#paymentinfodivid').hide()"/>
						             <spring:message code="label.entity.no"/> 
				                  </label>
		                 	</c:otherwise>
	                  </c:choose>
	                  <div id="receivedPayments_error"></div>
	                </div>
	                </fieldset>
	            </div>

				<div id="paymentinfodivid" class="row-fluid">
				
					<div class="control-group">
						<fieldset>
						<legend class="control-label">
						<spring:message code="label.entity.paymentMethod"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" />
						</legend>
						<div class="controls">
							<div class="controls span3">
								<label class="radio" for="CHECK">
								<input type="radio" ${paymentMethodsObj.paymentType== 'CHECK' ? 'checked="checked"' : ''}
									name="paymentType" id="CHECK" value="CHECK" onclick="$('#paymentinfocheckdivid').show();$('#paymentinfoeftdivid').hide()">
									<spring:message code="label.entity.check"/>
								</label> 
								<label class="radio" for="EFT"><input type="radio" ${paymentMethodsObj.paymentType== 'EFT' ? 'checked="checked"' : ''} 
									name="paymentType" id="EFT" value="EFT" onclick="$('#paymentinfoeftdivid').show();$('#paymentinfocheckdivid').hide()">
									<spring:message code="label.entity.eft"/> </label>
							</div>
							<div id="CHECK_error"></div>
						</div>
						</fieldset>
					</div>
					
					<div id="paymentinfoeftdivid" style="display:none">
						<div class="header">
							<h4 class="pull-left"><spring:message code="label.entity.accountInformation"/></h4>
						</div>
						<div  class="gutter10">
						<div class="control-group">
							<label for="bankAccountName" class="control-label"><spring:message
									code="label.entity.nameofaccount" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="bankAccountName"
									name="financialInfo.bankInfo.nameOnAccount"
									placeholder="<spring:message code="label.firstnamelastnameplaceholder"/>" size="30" maxlength="50"
									value="${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}">
								<div id="bankAccountName_error"></div>
							</div>
						</div>
						
							<div class="control-group">
								<label for="bankName" class="control-label"><spring:message
										code="label.bankName" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="bankName"
										name="financialInfo.bankInfo.bankName" size="30"
										value="${paymentMethodsObj.financialInfo.bankInfo.bankName}">
									<div id="bankName_error"></div>
								</div>
							</div>
							
							<div class="control-group">
								<label for="bankABARoutingNumber" class="control-label"><spring:message
										code="label.routingNumber" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="bankABARoutingNumber"
										name="financialInfo.bankInfo.routingNumber"
										value="${paymentMethodsObj.financialInfo.bankInfo.routingNumber}"
										maxlength="9" size="30">
									<div id="bankABARoutingNumber_error"></div>
								</div>
							</div>
							
							<div class="control-group">
								<label for="accountNumber" class="control-label"><spring:message
										code="label.accountNumber" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="bankAccountNumber"
										name="financialInfo.bankInfo.accountNumber" maxlength="17"
										size="30"
										value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}">
									<div class="" id="bankAccountNumber_error"></div>
								</div>
							</div>
							
							
							<div class="control-group">
							<fieldset>
								<legend for="bankAccountType" class="control-label"><spring:message code="label.entity.accountType"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></legend>
								<div class="controls">
									<div class="controls span3">
										<label class="radio" for="Checking"><input type="radio"
											${paymentMethodsObj.financialInfo.bankInfo.accountType == 'C' ? 'checked="checked"' : ''}
											name="financialInfo.bankInfo.accountType" id="Checking"
											value="C" checked="checked"><spring:message code="label.entity.checking"/></label> <label
											class="radio" for="Savings"><input type="radio"
											${paymentMethodsObj.financialInfo.bankInfo.accountType == 'S' ? 'checked="checked"' : ''}
											name="financialInfo.bankInfo.accountType" id="Savings" value="S"><spring:message code="label.entity.savings"/></label>
										<div id="bankAccountType_error"></div>
									</div>
								</div>
								</fieldset>
							</div>
							
						</div>
					</div>
		
					<div id="paymentinfocheckdivid" style="display:none">
						<div class="header">
							<h4 class="pull-left"><spring:message code="label.entity.paymentAddress"/></h4>
						</div>
							<fieldset class="gutter10 idMisMatch">
							<div class="control-group">
								<label for="address1_mailing" class="control-label required"><spring:message
										code="label.streetaddress" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text"
										placeholder="<spring:message code="label.address1placeholder"/>"
										name="financialInfo.address1" id="address1_mailing"
										value="${paymentMethodsObj.financialInfo.address1}"
										class="input-xlarge" size="30"  autocomplete="off" /> <input type="hidden"
										id="address1_mailing_hidden" name="address1_mailing_hidden"
										value="${paymentMethodsObj.financialInfo.address1}">
									<div id="address1_mailing_error"></div>
								</div>
								<!-- end of controls-->
							</div>
							<!-- end of control-group -->
			
							<div class="control-group">
								<label for="address2_mailing" class="control-label"><spring:message
										code="label.suite" /></label>
								<div class="controls">
									<input type="text"
										placeholder="<spring:message code="label.address2placeholder"/>"
										name="financialInfo.address2" id="address2_mailing"
										value="${paymentMethodsObj.financialInfo.address2}"
										class="input-xlarge" size="30"  autocomplete="off" /> <input type="hidden"
										id="address2_mailing_hidden" name="address2_mailing_hidden"
										value="${paymentMethodsObj.financialInfo.address2}">
								</div>
								<!-- end of controls-->
							</div>
							<!-- end of control-group -->
			
							<div class="control-group">
								<label for="city_mailing" class="control-label"><spring:message
										code="label.entity.city" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text" placeholder="<spring:message code="label.cityplaceholder"/>"
										name="financialInfo.city" id="city_mailing"
										value="${paymentMethodsObj.financialInfo.city}"
										class="input-xlarge" size="30"  autocomplete="off" /> <input type="hidden"
										id="city_mailing_hidden" name="city_mailing_hidden"
										value="${paymentMethodsObj.financialInfo.city}">
									<div id="city_mailing_error"></div>
								</div>
								<!-- end of controls-->
							</div>
							<!-- end of control-group -->
			
							<div class="control-group">
								<label for="state_mailing" class="control-label"><spring:message
										code="label.entity.state" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<select  id="state_mailing" name="financialInfo.state"
										path="statelist" class="input-medium" autocomplete="off" >
										<option value=""><spring:message code="label.entity.select"/></option>
										<c:forEach var="state" items="${statelist}">
											<option id="${state.code}"
												<c:if test="${state.code == paymentMethodsObj.financialInfo.state}"> selected="selected" </c:if>
												value="<c:out value="${state.code}" />">
												<c:out value="${state.name}" />
											</option>
										</c:forEach>
									</select> <input type="hidden" id="state_mailing_hidden"
										name="state_mailing_hidden"
										value="${paymentMethodsObj.financialInfo.state}">
									<div id="state_mailing_error"></div>
								</div>
								<!-- end of controls-->
							</div>
							<!-- end of control-group -->
							
							<div class="control-group">
								<label for="zip_mailing" class="control-label"><spring:message
										code="label.entity.zipCode" /><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text" placeholder="" name="financialInfo.zipcode"
										id="zip_mailing"
										value="${paymentMethodsObj.financialInfo.zipcode}"
										class="input-mini entityZipCode" maxlength="5"  autocomplete="off" /> <input
										type="hidden" id="zip_mailing_hidden" name="zip_mailing_hidden"
										value="${paymentMethodsObj.financialInfo.zipcode}">
									<div id="zip_mailing_error"></div>
								</div>
								<!-- end of controls-->
							</div>
							
					
				<input type="hidden" name="mailingLocation.lat" id="lat_mailing" value="" />
					<input type="hidden" name="mailingLocation.lon" id="lon_mailing" value="" />
					<input type="hidden" name="mailingLocation.rdi" id="rdi_mailing" value="" />
					<input type="hidden" name="mailingLocation.county" id="county_mailing" value="" />	
		
							<!-- end of control-group -->
					</fieldset>
				</div>	
				</div>
				<div id="loading" class="txt-center" style="display:none">
				<span class="green" class="gutter10" style="display:block;">Saving your data.</span>
				<img  src="<%=request.getContextPath() %>/resources/img/loader_greendots.gif" width="15%" alt="green loader dots"/>
				</div>
				<c:if test="${paymentMethodsObj==null && isAdmin != 'true'}">
					<div class="form-actions">  
				         <div class="clear">
				         		<input type="button" name="back" id="back" value="<spring:message code="label.entity.back"/>" class="btn" onClick="window.location.href='/hix/entity/enrollmententity/documententity'" />
				         		<input type="submit" name="finish" id="finish" value="<spring:message code="label.entity.submit"/>" class="btn btn-primary pull-right" />
				         </div>	           
			         </div>	  
		         </c:if>           	
		          <%-- <div class="control-group">  
			         <table class="table table-border-none">
						<c:if test="${paymentMethodsObj==null && isAdmin != 'true'}">
							<tr>				                     
					            <td><input type="button" name="back" id="back" value="<spring:message code="label.entity.back"/>" class="btn" onClick="window.location.href='/hix/entity/enrollmententity/documententity'" /></td>
					            <td width="50"><input type="submit" name="finish" id="finish" value="<spring:message code="label.entity.submit"/>" class="btn btn-primary" /></td>   
						    </tr>
						</c:if>
					</table>	           
		          </div> --%>
		           		
		          	
		          <input type="hidden" name="zipcodeID" id="zipcodeID" value="">		
			</form>
			
			<!-- HIX-13567 Start -->
			<div class="form-actions">
				<c:choose>
					<c:when test="${isAdmin == 'true'}">
						<a class="btn" type="button" href="/hix/entity/entityadmin/viewpaymentinfo/<encryptor:enc value="${enrollmentEntity.id}" isurl="true"/>"><spring:message code="label.entity.cancel"/></a>
						<a class="btn btn-primary pull-right" id="save" name="save"><spring:message code="label.entity.save"/></a>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${paymentMethodsObj != null}">
								<a class="btn cancel-btn" type="button" href="/hix/entity/enrollmententity/viewpaymentinfo"><spring:message code="label.entity.cancel"/></a>
								<a class="btn btn-primary" id="save" name="save"><spring:message code="label.entity.save"/></a>
							</c:when>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</div>
			<!-- HIX-13567 Ends -->
			</div>
		</div>
		<!-- end of span9 -->
	</div>
	<!--  end of row-fluid -->
</div>

<div id="reviewapp" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="congratulations"  aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="congratulations"><spring:message code="label.entity.congratulations"/></h3>
  </div>
  <div class="modal-body">
    <p><spring:message code="label.entity.congratulationsUhaveAppliedAsAnEnrollmentEntity"/>
</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="label.entity.close"/></button>
  </div>
</div>


<div class="notes" style="display: none">
	<div class="row">
		<div class="span">
			<p><spring:message code="label.entity.ThisInformationIsRequiredToDetermine"/></p>
		</div>
	</div>
</div>
<!--  end of .notes -->


<script type="text/javascript">

$(document).ready(function() {
	// disable autocomplete
	if (document.getElementsByTagName) {
		var inputElements = null;
		var elemIds = ["address1_mailing", "address2_mailing", "city_mailing", "state_mailing", "zip_mailing"];
		
		for(i=0; elemIds[i]; i++) {
			inputElements = document.getElementById(elemIds[i]);
			
			for (j=0; inputElements[j]; j++) {							
				if (inputElements[j].className && (inputElements[j].className.indexOf("disableAutoComplete") != -1)) {
					inputElements[j].setAttribute("autocomplete","off");
				}
			}
		}
	}
});
/* 	$('.ententtip').tooltip();
	

	
	$(document).ready(function() {
		
		var entId = ${entity.id};
		document.getElementById("EFT").defaultChecked;

		if(brkId != 0) {
			$('#certificationInfo').attr('class', 'done');
			$('#buildProfile').attr('class', 'done');
		} else {
			$('#certificationInfo').attr('class', 'visited');
			$('#buildProfile').attr('class', 'visited');
		};
		
	}); */
	

	function shiftbox(element,nextelement) {
		maxlength = parseInt(element.getAttribute('maxlength'));
		if(element.value.length == maxlength) {
			nextelement = document.getElementById(nextelement);
			nextelement.focus();
		}
	}
	
	 $(function() {
		$("#save").click(function(e){
			if($("#frmentityeditpayment").validate().form()) {
				$("#frmentityeditpayment").submit();
			}
		});
	}); 
	 
	 jQuery.validator.addMethod("AccountNumberCheck", function(value, element, param) {
		var accountNumber = $("#bankAccountNumber").val();
		var accountNumberPriv = '${paymentMethodsObj.financialInfo.bankInfo.accountNumber}';
		if(accountNumber == accountNumberPriv){
			return true;
		}
		var chechF=(/^[0-9]*$/.test(accountNumber));
		if(!chechF){
			return false;
		}
		if(accountNumber == "" || isNaN(accountNumber) || accountNumber.length < 9){
			return false;
		}
		return true;
	});
	 
	 jQuery.validator.addMethod("BankRoutingNumberCheck", function(value, element, param) {
			var routingNumber = $("#bankABARoutingNumber").val();
			var chechF=(/^[0-9]*$/.test(routingNumber));
			if(!chechF){
				return false;
			}
			if(routingNumber=="" || isNaN(routingNumber) || routingNumber.length < 9){
				return false;
			}
			return true;
		});
	 
	 jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
			zip = $("#zip_mailing").val(); 
			if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || zip == '00000' )){ 
				return false; 
			}
			return true;
		});
	
	   $.validator.addMethod("loginRegex", function(value, element) {
	        return this.optional(element) || /^[a-z0-9\s]+$/i.test(value);
	    });

		jQuery.validator.addMethod("disallowAllZero", function(value, element, param) {
			var bankRoutingNumber = $('#bankABARoutingNumber').val();
			var disallowAllZeroPtr = /^[0]{1,9}$/;
			if(disallowAllZeroPtr.test(bankRoutingNumber)){
				return false;
			}
			return true;
		});
		
		
	var validator = $("#frmentityeditpayment").validate({
		
		onkeyup : false,
		onclick : false,
		rules : {
			receivedPayments : {required : true},
			paymentType : {required : true},
			"financialInfo.bankInfo.nameOnAccount" : {required : true},
			"financialInfo.bankInfo.bankName" : {required : true, loginRegex:true},
			"financialInfo.bankInfo.routingNumber" : {required : true, BankRoutingNumberCheck : true, disallowAllZero : true},
			"financialInfo.bankInfo.accountNumber" : {required : true, AccountNumberCheck : true},
			"financialInfo.address1" : {required : true},
			"financialInfo.city" : {required : true},
			"financialInfo.state" : {required : true},
			"financialInfo.zipcode" : {required : true, MailingZipCodecheck : true}
		},
		messages : {
			receivedPayments :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseReceivedPayment' javaScriptEscape='true'/></span>"},
			paymentType :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseSelectPaymentType' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.nameOnAccount" :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterAccountName' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.bankName" :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterBankName' javaScriptEscape='true'/></span>",
				loginRegex:"<span><em class='excl'>!</em><spring:message code='label.validateEnterValidBankName' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.routingNumber" :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterRoutingNumber' javaScriptEscape='true'/></span>", 
				BankRoutingNumberCheck : "<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidRoutingNumber' javaScriptEscape='true'/></span>",
				disallowAllZero : "<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidRoutingNumber' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.accountNumber" :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterAccountNumber' javaScriptEscape='true'/></span>", 
				AccountNumberCheck : "<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidAccountNumber' javaScriptEscape='true'/></span>"},
			"financialInfo.address1" :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterAddress' javaScriptEscape='true'/></span>"},
			"financialInfo.city" :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterCity' javaScriptEscape='true'/></span>"},
			"financialInfo.state" :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseSelectState' javaScriptEscape='true'/></span>"},
			"financialInfo.zipcode" :{required :"<span><em class='excl'>!</em><spring:message code='label.validatePleaseEnterZipCode' javaScriptEscape='true'/></span>", MailingZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidZipCode' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		}, 
	});	
	


$(document).ready(function() {
	var isShow ='${isShow}';
	var paymenttype ='${paymentType}'; 
	if(isShow=="true") {
		$("#paymentinfodivid").show();
		if(paymenttype=="CHECK") {
			$("#paymentinfocheckdivid").show();
		} else if(paymenttype=="EFT") {
			$("#paymentinfoeftdivid").show();
		} else {
			
		}
	}  else {	
		$("#paymentinfodivid").hide();
	}

	$("#paymentInfo").removeClass("link");
	$("#paymentInfo").addClass("active");
	
     $('form').submit(function() {
     var frm = $('#frmentityeditpayment');
     if(! frm.valid()) return false;
     
     if ($('#loading').is(':hidden')) {
		$('#loading').show('slow', 'linear');
		$("#finish").attr("disabled","disabled");
	 }
        $.ajax({
            type: frm.attr('method'),
            url: "<c:url value='/entity/enrollmententity/paymentinfo'/>",
            data: frm.serialize(),
            success: function(responseText){
            	if(responseText.indexOf('Invalid Bank Routing No:') != -1){
            		$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4>' + responseText + '</h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
            		$('#loading').hide('slow', 'linear');
            		$("#finish").removeAttr("disabled");
            		return false;
            	}
            	if(responseText.indexOf('Invalid Bank Account No:') != -1){
            		$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4>' + responseText + '</h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
            		$('#loading').hide('slow', 'linear');
            		$("#finish").removeAttr("disabled");
            		return false;
            	}
            	if(responseText == 'success'){
            		if($("#loggedUser").val()!="entityAdmin"){
            			if($("#entityStatus").val() == "Incomplete"){
            				$('#reviewapp').modal('show');
            				$('#reviewapp').on('hidden', function(){
            					$('#loading').hide('slow', 'linear');
            						window.location.href="<c:url value='/entity/enrollmententity/registrationstatus'/>";
            				});
            			}
            			else{
            				window.location.href="<c:url value='/entity/enrollmententity/viewpaymentinfo'/>";
            			}
					}
            		if($("#loggedUser").val()=="entityAdmin"){
						<c:set var="enrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/></c:set>
						window.location.href="<c:url value='/entity/entityadmin/viewpaymentinfo/${enrollmentEntityId}'/>";
					}
            	}else{  
            		$('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
            		$('#loading').hide('slow', 'linear');
            		$("#finish").removeAttr("disabled");
            		return false;
            	}
				
            }
        });
 
        return false;
    });
	 
	
});
</script>
