<%@page import="java.util.ArrayList"%>
<%@page import="antlr.collections.List"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="com.getinsured.hix.platform.util.*"   %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/parsley.js" />"></script>

<script type="text/javascript" >
var memberCount=0;
var addMemberHitCount=0;

var individual_member = {
	line_index:    0,
	location_row:'<fieldset id="individual_member_FID">\n'+
	              '<legend class="removemember" id="">Member <span id="deletebutton_FID"><a href="javascript:individual_member.delete_line(\'FID\')" class="btn btn-mini btn-danger pull-right">Remove Member</a></span></legend>\n'+
	              '<div class="profile">\n'+
	              '<div class="control-group">\n'+
				   '<label class="control-label" for="input01">First Name</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="subscriberFirstName_FID" name="aetnaEnrollmentList[FID].subscriberFirstName">\n'+
						'<div id="subscriberFirstName_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			   
			   '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Last Name</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="subscriberLastName_FID" name="aetnaEnrollmentList[FID].subscriberLastName">\n'+
						'<div id="subscriberLastName_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label">DOB</label>\n'+
					'<div class="controls dob">\n'+
						'<label for="dob_mm_FID"><input class="input-mini" id="dob_mm_FID" name="dob_mm_FID" maxlength="2" placeholder="MM" type="text" ></label> \n'+
						'<label for="dob_dd_FID"><input class="input-mini" id="dob_dd_FID" name="dob_dd_FID" maxlength="2" placeholder="DD" type="text" ></label> \n'+
						'<label for="dob_yy_FID"><input class="input-mini" size="4" maxlength="4" id="dob_yy_FID" name="dob_yy_FID" placeholder="YYYY" type="text" ></label> \n'+
						'<div class="" id="dob_mm_FID_error"></div>\n'+
						'<div class="" id="dob_dd_FID_error"></div>\n'+
						'<div class="" id="dob_yy_FID_error"></div>\n'+
						'<div class="" id="subscriberDob_FID_error"></div>\n'+
					'</div>\n'+
					'<input type="hidden" name="aetnaEnrollmentList[FID].subscriberDob" id="subscriberDob_FID"> \n'+
			   '</div>\n'+
			   
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">ZipCode</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="subscriberZip_FID" name="aetnaEnrollmentList[FID].subscriberZip">\n'+
						'<div id="subscriberZip_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Medical PlanId</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="medicalPlanId_FID" name="aetnaEnrollmentList[FID].medicalPlanId">\n'+
						'<div id="medicalPlanId_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Dental Indicator</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="dentalIndicator_FID" name="aetnaEnrollmentList[FID].dentalIndicator">\n'+
						'<div id="dentalIndicator_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			  
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Gender</label>\n'+
					'<div class="controls">\n'+
						'<select id="gender_FID" name="aetnaEnrollmentList[FID].gender">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="1">Male</option>\n'+
							'<option value="2">Female</option>\n'+
						'</select>\n'+
						'<div id="gender_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			     '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Marital Status</label>\n'+
					'<div class="controls">\n'+
						'<select id="maritalStatus_FID" name="aetnaEnrollmentList[FID].maritalStatus">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="1">Married</option>\n'+
							'<option value="2">Single</option>\n'+
							'<option value="2147483647">Domestic Partner</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Family Code</label>\n'+
					'<div class="controls">\n'+
						'<select id="familyCode_FID" name="aetnaEnrollmentList[FID].familyCode" onchange="enableSubscriberUIElements(this.value);">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="APP">Subscriber</option>\n'+
							'<option value="SP">Spouse</option>\n'+
							'<option value="01">Dependant</option>\n'+
						'</select>\n'+
						'<div id="familyCode_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Enrollment Reason</label>\n'+
					'<div class="controls">\n'+
						'<select id="enrollmentReason_FID" name="aetnaEnrollmentList[FID].enrollmentReason">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="0">New Medical Coverage</option>\n'+
							'<option value="7">Add dependent(s) to current coverage</option>\n'+
							'<option value="8">Change current coverage</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Tobacco Use (within last 12 months)</label>\n'+
					'<div class="controls">\n'+
						'<select id="tobaccoUse_FID" name="aetnaEnrollmentList[FID].tobaccoUse">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Are you a Citizen of United States?( A3200)</label>\n'+
					'<div class="controls">\n'+
						'<select id="citizenOfUnitedStates_FID" name="aetnaEnrollmentList[FID].citizenOfUnitedStates">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
						'<div id="citizenOfUnitedStates_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Is this applicant not a citizen of United States? (A2201)</label>\n'+
					'<div class="controls">\n'+
						'<select id="notCitizenOfUnitedStates_FID" name="aetnaEnrollmentList[FID].notCitizenOfUnitedStates">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="true">True</option>\n'+
							'<option value="false">False</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label">Arrival Date</label>\n'+
					'<div class="controls dob">\n'+
						'<label for="arrivalDate_mm_FID"><input class="input-mini" id="arrivalDate_mm_FID" name="arrivalDate_mm_FID" maxlength="2" placeholder="MM" type="text" ></label> \n'+
						'<label for="arrivalDate_dd_FID"><input class="input-mini" id="arrivalDate_dd_FID" name="arrivalDate_dd_FID" maxlength="2" placeholder="DD" type="text" ></label> \n'+
						'<label for="arrivalDate_yy_FID"><input class="input-mini" size="4" maxlength="4" id="arrivalDate_yy_FID" name="arrivalDate_yy_FID" placeholder="YYYY" type="text" ></label> \n'+
						'<div class="" id="arrivalDate_mm_FID_error"></div>\n'+
						'<div class="" id="arrivalDate_dd_FID_error"></div>\n'+
						'<div class="" id="arrivalDate_yy_FID_error"></div>\n'+
						'<div class="" id="arrivalDate_FID_error"></div>\n'+
					'</div>\n'+
					'<input type="hidden" name="aetnaEnrollmentList[FID].arrivalDate" id="arrivalDate_FID"> \n'+
		   		'</div>\n'+
		   
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">INSD No</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="insdNo_FID" name="aetnaEnrollmentList[FID].insdNo">\n'+
						'<div id="insdNo_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Is this application submitted between October 1, 2013 and March 31, 2014(A2603)</label>\n'+
					'<div class="controls">\n'+
						'<select id="isAppSubmitted_FID" name="aetnaEnrollmentList[FID].isAppSubmitted">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			   '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Reason for special annual enrollment(A2604)</label>\n'+
					'<div class="controls">\n'+
						'<select id="reasonForSpecAnnualEnrollment_FID" name="aetnaEnrollmentList[FID].reasonForSpecAnnualEnrollment">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="0">Coverage needed for new dependent through marriage, birth or adoption or placement for adoption</option>\n'+
							'<option value="1">Coverage for applicant who had Medicaid or CHIP terminate due to loss of eligibility</option>\n'+
							'<option value="2">Coverage for applicant following death of policy holder</option>\n'+
							'<option value="3">Coverage for applicant following divorce or legal separation from policy holder</option>\n'+
							'<option value="4">Coverage for applicant because policy holder eligible for Medicare</option>\n'+
							'<option value="5">Coverage for applicant who has ceased to be eligible as dependent</option>\n'+
							'<option value="6">Other</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Other(A2605)</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="other_FID" name="aetnaEnrollmentList[FID].other">\n'+
						'<div id="other_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			     '<div class="control-group">\n'+
					'<label class="control-label" for="input01">What is your primary spoken language?( A2101)</label>\n'+
					'<div class="controls">\n'+
						'<select id="primarySpokenLanguage_FID" name="aetnaEnrollmentList[FID].primarySpokenLanguage">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="1">Arabic</option>\n'+
							'<option value="2">Armenian</option>\n'+
							'<option value="3">Chinese</option>\n'+
							'<option value="4">Hmong</option>\n'+
							'<option value="5">Japanese</option>\n'+
							'<option value="6">Khmer</option>\n'+
							'<option value="7">Korean</option>\n'+
							'<option value="8">Persian</option>\n'+
							'<option value="9">Portuguese</option>\n'+
							'<option value="10">Punjabi</option>\n'+
							'<option value="11">Russian</option>\n'+
							'<option value="12">Spanish</option>\n'+
							'<option value="13">Tagalog</option>\n'+
							'<option value="14">Vietnamese</option>\n'+
							'<option value="15">Other</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Please enter your spoken language(A2102)</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="spokenLanguage_FID" name="aetnaEnrollmentList[FID].spokenLanguage">\n'+
						'<div id="spokenLanguage_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">What is your primary written language?( A2103)</label>\n'+
					'<div class="controls">\n'+
						'<select id="primaryWrittenLanguage_FID" name="aetnaEnrollmentList[FID].primaryWrittenLanguage">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="1">Arabic</option>\n'+
							'<option value="2">Armenian</option>\n'+
							'<option value="3">Chinese</option>\n'+
							'<option value="4">Hmong</option>\n'+
							'<option value="5">Japanese</option>\n'+
							'<option value="6">Khmer</option>\n'+
							'<option value="7">Korean</option>\n'+
							'<option value="8">Persian</option>\n'+
							'<option value="9">Portuguese</option>\n'+
							'<option value="10">Punjabi</option>\n'+
							'<option value="11">Russian</option>\n'+
							'<option value="12">Spanish</option>\n'+
							'<option value="13">Tagalog</option>\n'+
							'<option value="14">Vietnamese</option>\n'+
							'<option value="15">Other</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Please enter your written language(A2104)</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="writtenLanguage_FID" name="aetnaEnrollmentList[FID].writtenLanguage">\n'+
						'<div id="writtenLanguage_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Are you a resident of the state in which you are applying?(A2099)</label>\n'+
					'<div class="controls">\n'+
						'<select id="areYouResidentOfState" name="aetnaEnrollmentList[FID].areYouResidentOfState">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">If we need to call you with any question about your application, when is the best time to reach you: Morning</label>\n'+
					'<div class="controls">\n'+
						'<select id="callMorning_FID" name="aetnaEnrollmentList[FID].callMorning">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="true">True</option>\n'+
							'<option value="false">False</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">If we need to call you with any question about your application, when is the best time to reach you: Afternoon</label>\n'+
					'<div class="controls">\n'+
						'<select id="callAfternoon_FID" name="aetnaEnrollmentList[FID].callAfternoon">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="true">True</option>\n'+
							'<option value="false">False</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">If we need to call you with any question about your application, when is the best time to reach you: Evening</label>\n'+
					'<div class="controls">\n'+
						'<select id="callEvening_FID" name="aetnaEnrollmentList[FID].callEvening">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="true">True</option>\n'+
							'<option value="false">False</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM100</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM100_FID" name="aetnaEnrollmentList[FID].caseManagementCM100">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM101</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM101_FID" name="aetnaEnrollmentList[FID].caseManagementCM101">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM102</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM102_FID" name="aetnaEnrollmentList[FID].caseManagementCM102">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
			   
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM103</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM103_FID" name="aetnaEnrollmentList[FID].caseManagementCM103">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM104</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM104_FID" name="aetnaEnrollmentList[FID].caseManagementCM104">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM105</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM105_FID" name="aetnaEnrollmentList[FID].caseManagementCM105">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM106</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM106_FID" name="aetnaEnrollmentList[FID].caseManagementCM106">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM107</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM107_FID" name="aetnaEnrollmentList[FID].caseManagementCM107">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM108</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM108_FID" name="aetnaEnrollmentList[FID].caseManagementCM108">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM109</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM109_FID" name="aetnaEnrollmentList[FID].caseManagementCM109">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	
		    	'<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM110</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM110_FID" name="aetnaEnrollmentList[FID].caseManagementCM110">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM111</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM111_FID" name="aetnaEnrollmentList[FID].caseManagementCM111">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    			    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM112</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM112_FID" name="aetnaEnrollmentList[FID].caseManagementCM112">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM113</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM113_FID" name="aetnaEnrollmentList[FID].caseManagementCM113">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM114</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM114_FID" name="aetnaEnrollmentList[FID].caseManagementCM114">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM115</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM115_FID" name="aetnaEnrollmentList[FID].caseManagementCM115">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM116</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM116_FID" name="aetnaEnrollmentList[FID].caseManagementCM116">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM117</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM117_FID" name="aetnaEnrollmentList[FID].caseManagementCM117">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM118</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM118_FID" name="aetnaEnrollmentList[FID].caseManagementCM118">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM119</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM119_FID" name="aetnaEnrollmentList[FID].caseManagementCM119">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM120</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM120_FID" name="aetnaEnrollmentList[FID].caseManagementCM120">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM121</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM121_FID" name="aetnaEnrollmentList[FID].caseManagementCM121">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	 
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management CM122</label>\n'+
					'<div class="controls">\n'+
						'<select id="caseManagementCM122_FID" name="aetnaEnrollmentList[FID].caseManagementCM122">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
		    	'</div>\n'+
		    	
			   '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Case Management (CM123) – Other</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="caseManagementOther_FID" name="aetnaEnrollmentList[FID].caseManagementOther">\n'+
						'<div id="caseManagementOther_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Source Type</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="sourceType_FID" name="aetnaEnrollmentList[FID].sourceType">\n'+
						'<div id="sourceType_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Source Reference Id</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="sourceReferenceId_FID" name="aetnaEnrollmentList[FID].sourceReferenceId">\n'+
						'<div id="sourceReferenceId_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			     
			     
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Value of Context Code R401 (I personally read and completed the Individual Application form for the applicant named below, because :)</label>\n'+
					'<div class="controls">\n'+
						'<select id="valueOfContextCode_FID" name="aetnaEnrollmentList[FID].valueOfContextCode">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="5">Applicant does not have sufficient command of the English language to complete this application</option>\n'+
							'<option value="6">Applicant is legally incapacitated and unable to complete this application</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Would you like to receive emails from us regarding your benefits, programs and general health information?</label>\n'+
					'<div class="controls">\n'+
						'<select id="receiveEmailsFromUs_FID" name="aetnaEnrollmentList[FID].receiveEmailsFromUs">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Would you like to turn off paper?</label>\n'+
					'<div class="controls">\n'+
						'<select id="turnOffPaper_FID" name="aetnaEnrollmentList[FID].turnOffPaper">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="Yes">Yes</option>\n'+
							'<option value="No">No</option>\n'+
						'</select>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
					'<label class="control-label" for="input01">State</label>\n'+
					'<div class="controls">\n'+
					   '<select size="1"  id="state_FID" name="aetnaEnrollmentList[FID].state">\n'+
                        '<option value="">Select</option>\n'+
                        '<option value="AL">Alabama</option>\n'+
                        '<option value="AK">Alaska</option>\n'+
                        '<option value="AZ">Arizona</option>\n'+
                        '<option value="AR">Arkansas</option>\n'+
                        '<option value="CA">California</option>\n'+
                        '<option value="CO">Colorado</option>\n'+
                        '<option value="CT">Connecticut</option>\n'+
                        '<option value="DE">Delaware</option>\n'+
                        '<option value="DC">Dist of Columbia</option>\n'+
                        '<option value="FL">Florida</option>\n'+
                        '<option value="GA">Georgia</option>\n'+
                        '<option value="HI">Hawaii</option>\n'+
                        '<option value="ID">Idaho</option>\n'+
                        '<option value="IL">Illinois</option>\n'+
                        '<option value="IN">Indiana</option>\n'+
                        '<option value="IA">Iowa</option>\n'+
                        '<option value="KS">Kansas</option>\n'+
                        '<option value="KY">Kentucky</option>\n'+
                        '<option value="LA">Louisiana</option>\n'+
                        '<option value="ME">Maine</option>\n'+
                        '<option value="MD">Maryland</option>\n'+
                        '<option value="MA">Massachusetts</option>\n'+
                        '<option value="MI">Michigan</option>\n'+
                        '<option value="MN">Minnesota</option>\n'+
                        '<option value="MS">Mississippi</option>\n'+
                        '<option value="MO">Missouri</option>\n'+
                        '<option value="MT">Montana</option>\n'+
                        '<option value="NE">Nebraska</option>\n'+
                        '<option value="NV">Nevada</option>\n'+
                        '<option value="NH">New Hampshire</option>\n'+
                        '<option value="NJ">New Jersey</option>\n'+
                        '<option value="NM">New Mexico</option>\n'+
                        '<option value="NY">New York</option>\n'+
                        '<option value="NC">North Carolina</option>\n'+
                        '<option value="ND">North Dakota</option>\n'+
                        '<option value="OH">Ohio</option>\n'+
                        '<option value="OK">Oklahoma</option>\n'+
                        '<option value="OR">Oregon</option>\n'+
                        '<option value="PA">Pennsylvania</option>\n'+
                        '<option value="RI">Rhode Island</option>\n'+
                        '<option value="SC">South Carolina</option>\n'+
                        '<option value="SD">South Dakota</option>\n'+
                        '<option value="TN">Tennessee</option>\n'+
                        '<option value="TX">Texas</option>\n'+
                        '<option value="UT">Utah</option>\n'+
                        '<option value="VT">Vermont</option>\n'+
                        '<option value="VA">Virginia</option>\n'+
                        '<option value="WA">Washington</option>\n'+
                        '<option value="WV">West Virginia</option>\n'+
                        '<option value="WI">Wisconsin</option>\n'+
                        '<option value="WY">Wyoming</option>\n'+
                        '</select>\n'+
                    '</div>\n'+      
				 '</div>\n'+
			    
			     '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Covered Individual Status</label>\n'+
					'<div class="controls">\n'+
						'<select id="coveredIndividualStatus_FID" name="aetnaEnrollmentList[FID].coveredIndividualStatus">\n'+
							'<option value="">--Select--</option>\n'+
							'<option value="1">Application in Progress</option>\n'+
							'<option value="2">Progress</option>\n'+
							'<option value="3">Approved</option>\n'+
							'<option value="4">Approved with Rate-up - Pending Response</option>\n'+
							'<option value="5">Approved with Rate-up</option>\n'+
							'<option value="6">Auto-Declined</option>\n'+
							'<option value="7">Closed</option>\n'+
							'<option value="8">Declined Ineligible</option>\n'+
							'<option value="9">Pended for Incomplete Information</option>\n'+
							'<option value="10">Pended for Review</option>\n'+
						'</select>\n'+
					'</div>\n'+
			   	  '</div>\n'+
 
			   	 '<div class="control-group">\n'+
					'<label class="control-label" for="input01">Employment Info</label>\n'+
					'<div class="controls">\n'+
						'<input type="text" class="input-xlarge" id="employmentInfo_FID" name="aetnaEnrollmentList[FID].employmentInfo">\n'+
						'<div id="employmentInfo_FID_error" class=""></div>\n'+
					'</div>\n'+
			    '</div>\n'+
			    
			    '<div class="control-group">\n'+
				'<label class="control-label" for="input01">State Of Birth</label>\n'+
				'<div class="controls">\n'+
				   '<select size="1"  id="stateOfBirth_FID" name="aetnaEnrollmentList[FID].stateOfBirth">\n'+
                    '<option value="">Select</option>\n'+
                    '<option value="AL">Alabama</option>\n'+
                    '<option value="AK">Alaska</option>\n'+
                    '<option value="AZ">Arizona</option>\n'+
                    '<option value="AR">Arkansas</option>\n'+
                    '<option value="CA">California</option>\n'+
                    '<option value="CO">Colorado</option>\n'+
                    '<option value="CT">Connecticut</option>\n'+
                    '<option value="DE">Delaware</option>\n'+
                    '<option value="DC">Dist of Columbia</option>\n'+
                    '<option value="FL">Florida</option>\n'+
                    '<option value="GA">Georgia</option>\n'+
                    '<option value="HI">Hawaii</option>\n'+
                    '<option value="ID">Idaho</option>\n'+
                    '<option value="IL">Illinois</option>\n'+
                    '<option value="IN">Indiana</option>\n'+
                    '<option value="IA">Iowa</option>\n'+
                    '<option value="KS">Kansas</option>\n'+
                    '<option value="KY">Kentucky</option>\n'+
                    '<option value="LA">Louisiana</option>\n'+
                    '<option value="ME">Maine</option>\n'+
                    '<option value="MD">Maryland</option>\n'+
                    '<option value="MA">Massachusetts</option>\n'+
                    '<option value="MI">Michigan</option>\n'+
                    '<option value="MN">Minnesota</option>\n'+
                    '<option value="MS">Mississippi</option>\n'+
                    '<option value="MO">Missouri</option>\n'+
                    '<option value="MT">Montana</option>\n'+
                    '<option value="NE">Nebraska</option>\n'+
                    '<option value="NV">Nevada</option>\n'+
                    '<option value="NH">New Hampshire</option>\n'+
                    '<option value="NJ">New Jersey</option>\n'+
                    '<option value="NM">New Mexico</option>\n'+
                    '<option value="NY">New York</option>\n'+
                    '<option value="NC">North Carolina</option>\n'+
                    '<option value="ND">North Dakota</option>\n'+
                    '<option value="OH">Ohio</option>\n'+
                    '<option value="OK">Oklahoma</option>\n'+
                    '<option value="OR">Oregon</option>\n'+
                    '<option value="PA">Pennsylvania</option>\n'+
                    '<option value="RI">Rhode Island</option>\n'+
                    '<option value="SC">South Carolina</option>\n'+
                    '<option value="SD">South Dakota</option>\n'+
                    '<option value="TN">Tennessee</option>\n'+
                    '<option value="TX">Texas</option>\n'+
                    '<option value="UT">Utah</option>\n'+
                    '<option value="VT">Vermont</option>\n'+
                    '<option value="VA">Virginia</option>\n'+
                    '<option value="WA">Washington</option>\n'+
                    '<option value="WV">West Virginia</option>\n'+
                    '<option value="WI">Wisconsin</option>\n'+
                    '<option value="WY">Wyoming</option>\n'+
                    '</select>\n'+
                '</div>\n'+      
			 '</div>\n'+
			 
			 '<div class="control-group">\n'+
				'<label class="control-label" for="input01">Time Spent Out Of State</label>\n'+
				'<div class="controls">\n'+
					'<input type="text" class="input-xlarge" id="timeSpentOutOfState_FID" name="aetnaEnrollmentList[FID].timeSpentOutOfState">\n'+
					'<div id="timeSpentOutOfState_FID_error" class=""></div>\n'+
				'</div>\n'+
		    '</div>\n'+
		
		    '<div class="control-group">\n'+
				'<label class="control-label" for="input01">Other Insurance For HCR</label>\n'+
				'<div class="controls">\n'+
					'<input type="text" class="input-xlarge" id="otherInsuranceForHCR_FID" name="aetnaEnrollmentList[FID].otherInsuranceForHCR">\n'+
					'<div id="otherInsuranceForHCR_FID_error" class=""></div>\n'+
				'</div>\n'+
	    	'</div>\n'+
	    
	    	'<div id="div_enrollmentPeriodConfirm_FID" class="control-group" style="display:none">\n'+
				'<label class="control-label" for="input01">Enrollment Period Confirm</label>\n'+
				'<div class="controls">\n'+
					'<input type="text" class="input-xlarge" id="enrollmentPeriodConfirm_FID" name="aetnaEnrollmentList[FID].enrollmentPeriodConfirm">\n'+
					'<div id="enrollmentPeriodConfirm_FID_error" class=""></div>\n'+
				'</div>\n'+
	    	'</div>\n'+
    	
	    	'<div id="div_enrollmentPeriodQns_FID" class="control-group" style="display:none">\n'+
				'<label class="control-label" for="input01">Enrollment Period QNS</label>\n'+
				'<div class="controls">\n'+
					'<input type="text" class="input-xlarge" id="enrollmentPeriodQns_FID" name="aetnaEnrollmentList[FID].enrollmentPeriodQns">\n'+
					'<div id="enrollmentPeriodQns_FID_error" class=""></div>\n'+
				'</div>\n'+
    		'</div>\n'+
		    
    		'<div id="div_enrollmentPeriodOther_FID" class="control-group" style="display:none">\n'+
				'<label class="control-label" for="input01">Enrollment Period Other</label>\n'+
				'<div class="controls">\n'+
					'<input type="text" class="input-xlarge" id="enrollmentPeriodOther_FID" name="aetnaEnrollmentList[FID].enrollmentPeriodOther">\n'+
					'<div id="enrollmentPeriodOther_FID_error" class=""></div>\n'+
				'</div>\n'+
			'</div>\n'+
		
			 '<div id="div_dateOfEvent_FID" class="control-group" style="display:none">\n'+
				'<label class="control-label">Date Of Event</label>\n'+
				'<div class="controls dob">\n'+
					'<label for="dateOfEvent_mm_FID"><input class="input-mini" id="dateOfEvent_mm_FID" name="dateOfEvent_mm_FID" maxlength="2" placeholder="MM" type="text" ></label> \n'+
					'<label for="dateOfEvent_dd_FID"><input class="input-mini" id="dateOfEvent_dd_FID" name="dateOfEvent_dd_FID" maxlength="2" placeholder="DD" type="text" ></label> \n'+
					'<label for="dateOfEvent_yy_FID"><input class="input-mini" size="4" maxlength="4" id="dateOfEvent_yy_FID" name="dateOfEvent_yy_FID" placeholder="YYYY" type="text" ></label> \n'+
					'<div class="" id="dateOfEvent_mm_FID_error"></div>\n'+
					'<div class="" id="dateOfEvent_dd_FID_error"></div>\n'+
					'<div class="" id="dateOfEvent_yy_FID_error"></div>\n'+
					'<div class="" id="dateOfEvent_FID_error"></div>\n'+
				'</div>\n'+
				'<input type="hidden" name="aetnaEnrollmentList[FID].dateOfEvent" id="dateOfEvent_FID"> \n'+
	   		'</div>\n'+
	   		
	   		'<div id="div_renewalDate_FID" class="control-group" style="display:none">\n'+
			'<label class="control-label">Renewal Date</label>\n'+
			'<div class="controls dob">\n'+
				'<label for="renewalDate_mm_FID"><input class="input-mini" id="renewalDate_mm_FID" name="renewalDate_mm_FID" maxlength="2" placeholder="MM" type="text" ></label> \n'+
				'<label for="renewalDate_dd_FID"><input class="input-mini" id="renewalDate_dd_FID" name="renewalDate_dd_FID" maxlength="2" placeholder="DD" type="text" ></label> \n'+
				'<label for="renewalDate_yy_FID"><input class="input-mini" size="4" maxlength="4" id="renewalDate_yy_FID" name="renewalDate_yy_FID" placeholder="YYYY" type="text" ></label> \n'+
				'<div class="" id="renewalDate_mm_FID_error"></div>\n'+
				'<div class="" id="renewalDate_dd_FID_error"></div>\n'+
				'<div class="" id="renewalDate_yy_FID_error"></div>\n'+
				'<div class="" id="renewalDate_FID_error"></div>\n'+
			'</div>\n'+
			'<input type="hidden" name="aetnaEnrollmentList[FID].renewalDate" id="renewalDate_FID"> \n'+
   		'</div>\n'+
   		
   		'<div id="div_eligibleMilitaryVeteran_FID" class="control-group" style="display:none">\n'+
			'<label class="control-label" for="input01">Eligible Military Veteran</label>\n'+
			'<div class="controls">\n'+
				'<select id="eligibleMilitaryVeteran_FID" name="aetnaEnrollmentList[FID].eligibleMilitaryVeteran">\n'+
					'<option value="">--Select--</option>\n'+
					'<option value="Yes">Yes</option>\n'+
					'<option value="No">No</option>\n'+
				'</select>\n'+
			'</div>\n'+
   		'</div>\n'+
    
   		'<div id="div_relationshipToApplicant_FID" class="control-group" style="display:none">\n'+
			'<label class="control-label" for="input01">Relationship To Applicant</label>\n'+
			'<div class="controls">\n'+
				'<input type="text" class="input-xlarge" id="relationshipToApplicant_FID" name="aetnaEnrollmentList[FID].relationshipToApplicant">\n'+
				'<div id="relationshipToApplicant_FID_error" class=""></div>\n'+
			'</div>\n'+
		'</div>\n'+
   		
			  '</div>\n'+ // end of the profile div class
		 '</fieldset>',
				add_member: function(){
					
				 	if(memberCount>10){
						alert("You can insured upto ten members");
						return false;
					} 

					var row = this.location_row; 
					
					row = row.replace(/FID/g, this.line_index);
					
					$('#memberinfo').append(row);
					this.line_index++;
					memberCount++;
					addMemberHitCount++;
				},
												
				delete_line: function(index){
					if(confirm('Are you sure you want to delete this record ?')){
						memberCount--;
						$('#individual_member_'+index).remove();
					}								
				}	 
			};
  </script>	              
              
              <div class="span9 columns pull-right" id="rightpanel">
	              <div class="gutter10">
                        <form class="form-horizontal" id="frmAetnaEnrollmentWebServiceMock" name="frmAetnaEnrollmentWebServiceMock" action='<c:url value="/aetna/enrollment/webservice/mock/submit"/>' method="post">
                                  
				        <div class="control-group">
							<label class="control-label" for="input01">Enrollment FormId</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="enrollmentFormId" name="enrollmentFormId">
								<div id="enrollmentFormId_error" class=""></div>
							</div>
					    </div>
					     <div class="control-group">
							<label class="control-label">Requested Effective Date</label>
							<div class="controls dob">
								<label for="mm_req_eff_date"><input class="input-mini" id="mm_req_eff_date" name="mm_req_eff_date" maxlength="2" placeholder="MM" type="text" ></label>
								<label for="dd_req_eff_date"><input class="input-mini" id="dd_req_eff_date" name="dd_req_eff_date" maxlength="2" placeholder="DD" type="text" ></label>
								<label for="yy_req_eff_date"><input class="input-mini" size="4" maxlength="4" id="yy_req_eff_date" name="yy_req_eff_date" placeholder="YYYY" type="text" ></label>
								<div class="" id="mm_req_eff_date_error"></div>
								<div class="" id="dd_req_eff_date_error"></div>
								<div class="" id="yy_req_eff_date_error"></div>
								<div class="" id="requestedEffectiveDate_error"></div>
							</div>
							<input type="hidden" name="requestedEffectiveDate" id="requestedEffectiveDate">
					   </div>
			   		   			   
		                <div class="control-group">
							<label class="control-label" for="input01">ApplicationId</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="applicationId" name="applicationId">
								<div id="applicationId_error" class=""></div>
							</div>
					    </div>
					    
					    <div class="control-group">
							<label class="control-label" for="input01">Application Status</label>
							<div class="controls">
								<select id="applicationStatus" name="applicationStatus">
									<option value="">--Select--</option>
									<option value="1">Received by Aetna</option>
									<option value="2">Duplicate Application</option>
									<option value="3">Application in Progress</option>
									<option value="4">Pending</option>
									<option value="5">Pending Rate Acceptance</option>
									<option value="6">Application Processing Completed </option>
								</select>
							</div>
					   </div>
					
					<div class="control-group">
					<label class="control-label" for="input01">CreditCard AccountNumber</label>
					<div class="controls">
						<input type="text" class="input-xlarge" id="creditCardAccountNumber" name="creditCardAccountNumber">
						<div id="creditCardAccountNumber_error" class=""></div>
					</div>
			    </div>
			    
			     <div class="control-group">
					<label class="control-label" for="input01">Card Type</label>
					<div class="controls">
						<select id="cardType" name="cardType">
							<option value="">--Select--</option>
							<option value="2">Visa</option>
							<option value="3">Master</option>
						</select>
					</div>
			    </div>
			    
			    <div class="control-group">
					<label class="control-label" for="input01">ElectronicFund AccountNumber</label>
					<div class="controls">
						<input type="text" class="input-xlarge" id="electronicFundAccountNumber" name="electronicFundAccountNumber">
						<div id="electronicFundAccountNumber_error" class=""></div>
					</div>
			    </div>
			    
			    <div class="control-group">
					<label class="control-label" for="input01">Routing Number</label>
					<div class="controls">
						<input type="text" class="input-xlarge" id="routingNumber" name="routingNumber">
						<div id="routingNumber_error" class=""></div>
					</div>
			    </div>
			   
                        <div id="memberinfo">
							<div class="box-loose">
								<a class="btn btn-primary small primary addmember offset9"  onclick="individual_member.add_member();"  id="" href="#">Add Member</a>
							</div>
					     </div>
					
                		  <div class="form-actions">
								<a href="#" onclick="formValidateAndSubmit();" class="btn btn-primary pull-right">
								Submit
							</a>
							</div>                    
                         
                      </form>
                      
           		</div>
     	</div>
     	
     	
<script type="text/javascript">
individual_member.add_member();
enableSubscriberUIElements("");
$('.ttclass').tooltip();
function enableSubscriberUIElements(value){
	for(var i = 0; i <=addMemberHitCount; i++) {
		
		$("#"+"div_renewalDate_" + i).css("display","none");
		$("#"+"div_dateOfEvent_" + i).css("display","none");
		$("#"+"div_enrollmentPeriodOther_" + i).css("display","none");
		$("#"+"div_enrollmentPeriodConfirm_" + i).css("display","none");
		$("#"+"div_enrollmentPeriodQns_" + i).css("display","none");
		$("#"+"div_eligibleMilitaryVeteran_" + i).css("display","none");
		$("#"+"div_relationshipToApplicant_" + i).css("display","none");
		
		if(value=="APP"){	
			$("#"+"div_renewalDate_" + i).css("display","block");
			$("#"+"div_dateOfEvent_" + i).css("display","block");
			$("#"+"div_enrollmentPeriodOther_" + i).css("display","block");
			$("#"+"div_enrollmentPeriodConfirm_" + i).css("display","block");
			$("#"+"div_enrollmentPeriodQns_" + i).css("display","block");
		}
		else if(value=="01"){
			$("#"+"div_eligibleMilitaryVeteran_" + i).css("display","block");
			$("#"+"div_relationshipToApplicant_" + i).css("display","block");
		}
			
		
		
	}
}
function formValidateAndSubmit(){
	 var aetnaEnrollmentListCount = addMemberHitCount;
	var return_errors = false;
	 for (var i = 0; i < aetnaEnrollmentListCount; i++) {
	 
	    $("#"+"subscriberFirstName_" + i + "_error").html("");
	    $("#"+"subscriberLastName_" + i + "_error").html("");
	    $("#"+"subscriberDob_" + i + "_error").html("");
	    $("#"+"dob_mm_" + i + "_error").html("");
	    $("#"+"dob_dd_" + i + "_error").html("");
	    $("#"+"dob_yy_" + i + "_error").html("");
	    $("#"+"arrivalDate_mm_" + i + "_error").html("");
	    $("#"+"arrivalDate_dd_" + i + "_error").html("");
	    $("#"+"arrivalDate_yy_" + i + "_error").html("");
	    $("#"+"subscriberZip_" + i + "_error").html("");
	    $("#"+"enrollmentFormId_error").html("");
	    $("#"+"medicalPlanId_" + i + "_error").html("");
	    $("#"+"dentalIndicator_" + i + "_error").html("");
	    $("#"+"gender_" + i + "_error").html("");
	    $("#"+"familyCode_" + i + "_error").html("");
	    $("#"+"citizenOfUnitedStates_" + i + "_error").html("");
	    $("#"+"insdNo_" + i + "_error").html("");
	    $("#"+"sourceType_" + i + "_error").html("");
	    $("#"+"sourceReferenceId_" + i + "_error").html("");
	    $("#"+"electronicFundAccountNumber_" + i + "_error").html("");
	    $("#"+"routingNumber_" + i + "_error").html("");
	    
	    $("#"+"applicationId_error").html("");
	    $("#"+"requestedEffectiveDate_error").html("");
	    
	    
	    if($("#"+"subscriberFirstName_"+i).val().length<=0){
	    	  $("#"+"subscriberFirstName_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"subscriberFirstName_" + i + "_error").html("<span><em class='excl'>!</em>FirstName is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"subscriberLastName_"+i).val().length<=0){
	    	  $("#"+"subscriberLastName_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"subscriberLastName_" + i + "_error").html("<span><em class='excl'>!</em>LastName is required.</span>");
	    	  return_errors = true;
	     }
	    if(document.getElementById("dob_mm_" + i)) {
			var dob1 = $("#dob_mm_"+i).val(); 
			var dob2 = $("#dob_dd_"+i).val(); 
			var dob3 = $("#dob_yy_"+i).val(); 

			if(dob1.length < 2 || dob2.length < 2 || dob3.length < 4 || isNaN(dob1+dob2+dob3)){
				$("#"+"dob_mm_" + i + "_error").attr("class","requiredError help-block");
		    	$("#"+"dob_mm_" + i + "_error").html("<span><em class='excl'>!</em>Please enter valid DOB.</span>");
				return_errors = true;
			}
			if(dob3.length < 4 && dob3.length>0){
				$("#"+"dob_yy_" + i + "_error").attr("class","requiredError help-block");
		    	$("#"+"dob_yy_" + i + "_error").html("<span><em class='excl'>!</em>Please enter year in YYYY format</span>");
				document.getElementById("dob_yy_"+i).focus();
				return_errors = true;
			}
			if( (dob1 == "" || dob2 == "" || dob3 == "")  || (isNaN(dob1)) || (isNaN(dob2)) || (isNaN(dob3))  ){ 
				$("#subscriberDob_"+i).val('');
				$("#dob_yy_"+i).val('');
				$("#"+"subscriberDob_" + i + "_error").attr("class","requiredError help-block");
		    	$("#"+"subscriberDob_" + i + "_error").html("<span><em class='excl'>!</em>Date of birth is required.</span>");
		    	return_errors = true;
			}
			else{
				var dobVal = $("#dob_mm_"+i).val()+ "/" +$("#dob_dd_"+i).val()+ "/" +$("#dob_yy_"+i).val();
				$("#subscriberDob_"+i).val(dobVal);
			}
		}
	    
	    if(document.getElementById("arrivalDate_mm_" + i)){
			var month = $("#arrivalDate_mm_"+i).val(); 
			var day = $("#arrivalDate_dd_"+i).val(); 
			var year = $("#arrivalDate_yy_"+i).val(); 
           if(month.length>0 || day.length>0 || year.length>0){    
				if(month.length < 2 || day.length < 2 || year.length < 4 || isNaN(month+day+year)){
					$("#"+"arrivalDate_mm_" + i + "_error").attr("class","requiredError help-block");
			    	$("#"+"arrivalDate_mm_" + i + "_error").html("<span><em class='excl'>!</em>Please enter valid Arrival Date.</span>");
					return_errors = true;
				}
				else if(year.length < 4 && year.length>0){
					$("#"+"arrivalDate_yy_" + i + "_error").attr("class","requiredError help-block");
			    	$("#"+"arrivalDate_yy_" + i + "_error").html("<span><em class='excl'>!</em>Please enter year in YYYY format</span>");
					document.getElementById("arrivalDate_yy_"+i).focus();
					return_errors = true;
				} else{
					var arrivalDateVal = $("#arrivalDate_mm_"+i).val()+ "/" +$("#arrivalDate_dd_"+i).val()+ "/" +$("#arrivalDate_yy_"+i).val();
					$("#arrivalDate_"+i).val(arrivalDateVal);
				}
           }
		}
	    if(document.getElementById("mm_req_eff_date")) {
			var month = $("#mm_req_eff_date").val(); 
			var day = $("#dd_req_eff_date").val(); 
			var year = $("#yy_req_eff_date").val(); 

			if(month.length < 2 || day.length < 2 || year.length < 4 || isNaN(month+day+year)){
				$("#"+"mm_req_eff_date_error").attr("class","requiredError help-block");
		    	$("#"+"mm_req_eff_date_error").html("<span><em class='excl'>!</em>Please enter valid Date.</span>");
				return_errors = true;
			}
			if(year.length < 4 && year.length>0){
				$("#"+"yy_req_eff_date_error").attr("class","requiredError help-block");
		    	$("#"+"yy_req_eff_date_error").html("<span><em class='excl'>!</em>Please enter year in YYYY format</span>");
				document.getElementById("dob_yy_req_eff_date").focus();
				return_errors = true;
			}
			if( (month == "" || day == "" || year == "")  || (isNaN(month)) || (isNaN(day)) || (isNaN(year))  ){ 
				$("#requestedEffectiveDate").val('');
				$("#yy_req_eff_date").val('');
				$("#"+"requestedEffectiveDate_error").attr("class","requiredError help-block");
		    	$("#"+"requestedEffectiveDate_error").html("<span><em class='excl'>!</em>Requested Effective Date is required.</span>");
			}
			else{
				var dateVal = $("#mm_req_eff_date").val()+ "/" +$("#dd_req_eff_date").val()+ "/" +$("#yy_req_eff_date").val();
				$("#requestedEffectiveDate").val(dateVal);
			}
		}
	   
	    if($("#"+"subscriberZip_"+i).val().length<=0){
	    	  $("#"+"subscriberZip_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"subscriberZip_" + i + "_error").html("<span><em class='excl'>!</em>ZipCode is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"enrollmentFormId").val().length<=0){
	    	  $("#"+"enrollmentFormId_error").attr("class","requiredError help-block");
	    	  $("#"+"enrollmentFormId_error").html("<span><em class='excl'>!</em>Enrollment formId is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"medicalPlanId_"+i).val().length<=0){
	    	  $("#"+"medicalPlanId_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"medicalPlanId_" + i + "_error").html("<span><em class='excl'>!</em>Medical planId is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"dentalIndicator_"+i).val().length<=0){
	    	  $("#"+"dentalIndicator_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"dentalIndicator_" + i + "_error").html("<span><em class='excl'>!</em>Dental indicator is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"gender_"+i).val().length<=0){
	    	  $("#"+"gender_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"gender_" + i + "_error").html("<span><em class='excl'>!</em>Gender is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"familyCode_"+i).val().length<=0){
	    	  $("#"+"familyCode_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"familyCode_" + i + "_error").html("<span><em class='excl'>!</em>Family code is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"citizenOfUnitedStates_"+i).val().length<=0){
	    	  $("#"+"citizenOfUnitedStates_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"citizenOfUnitedStates_" + i + "_error").html("<span><em class='excl'>!</em>Citizen of United States is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"insdNo_"+i).val().length>13){
	    	  $("#"+"insdNo_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"insdNo_" + i + "_error").html("<span><em class='excl'>!</em>INSD No. should be maximum of 13 characters.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"sourceType_"+i).val().length<=0){
	    	  $("#"+"sourceType_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"sourceType_" + i + "_error").html("<span><em class='excl'>!</em>Source type is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"sourceReferenceId_"+i).val().length<=0){
	    	  $("#"+"sourceReferenceId_" + i + "_error").attr("class","requiredError help-block");
	    	  $("#"+"sourceReferenceId_" + i + "_error").html("<span><em class='excl'>!</em>Source reference id is required.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"electronicFundAccountNumber").val().length>20){
	    	  $("#"+"electronicFundAccountNumber_error").attr("class","requiredError help-block");
	    	  $("#"+"electronicFundAccountNumber_error").html("<span><em class='excl'>!</em>Electronic fund account number should not exceed 20 characters in length.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"routingNumber").val().length>9){
	    	  $("#"+"routingNumber_error").attr("class","requiredError help-block");
	    	  $("#"+"routingNumber_error").html("<span><em class='excl'>!</em>Routing number should not exceed 9 characters in length.</span>");
	    	  return_errors = true;
	     }
	    if($("#"+"applicationId").val().length>11){
	    	  $("#"+"applicationId_error").attr("class","requiredError help-block");
	    	  $("#"+"applicationId_error").html("<span><em class='excl'>!</em> Application Id should not exceed 11 characters in length.</span>");
	    	  return_errors = true;
	     }
	}
	 if(return_errors){
		 return false;
	 }
	 else{
	 	$("#frmAetnaEnrollmentWebServiceMock").submit();
	 }
}
</script>
