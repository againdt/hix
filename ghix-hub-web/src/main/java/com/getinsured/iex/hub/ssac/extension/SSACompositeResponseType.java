
package com.getinsured.iex.hub.ssac.extension;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * Response from SSA to HUB for the SSA composite service. 
 *             
 * 
 * <p>Java class for SSACompositeResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSACompositeResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;choice>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSACompositeIndividualResponse" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}ResponseMetadata"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSACompositeResponseType", propOrder = {
    "ssaCompositeIndividualResponse",
    "responseMetadata"
})
public class SSACompositeResponseType
    extends ComplexObjectType
{

    @XmlElement(name = "SSACompositeIndividualResponse")
    protected List<SSACompositeIndividualResponseType> ssaCompositeIndividualResponse;
    @XmlElement(name = "ResponseMetadata")
    protected ResponseMetadataType responseMetadata;

    /**
     * Gets the value of the ssaCompositeIndividualResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ssaCompositeIndividualResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSSACompositeIndividualResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SSACompositeIndividualResponseType }
     * 
     * 
     */
    public List<SSACompositeIndividualResponseType> getSSACompositeIndividualResponse() {
        if (ssaCompositeIndividualResponse == null) {
            ssaCompositeIndividualResponse = new ArrayList<SSACompositeIndividualResponseType>();
        }
        return this.ssaCompositeIndividualResponse;
    }

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

}
