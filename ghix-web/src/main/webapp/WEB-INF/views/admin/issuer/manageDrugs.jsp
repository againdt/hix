<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<%@ page import="java.util.Calendar"%>
<%@ page isELIgnored="false"%>

<link rel="stylesheet" href="//cdn.rawgit.com/angular-ui/bower-ui-grid/master/ui-grid.min.css">

<style>
	.ui-grid-pager-control input {
	    height: 14px;
	}

	.ui-grid-pager-control .ui-grid-pager-max-pages-number {
	    vertical-align: middle;
	}

	.grid-left-align {
	    text-align: left;
	}

	h1 img {
		height: auto;
		width: auto;
	}
</style>

<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/angular-animate.1.5.6.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/angular-touch.1.5.6.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/ui-grid.min.4.0.2.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/lce/ui-bootstrap-tpls-0.11.0.js"/>'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js"/>'></script>

<script type="text/javascript" src='<gi:cdnurl value="/resources/js/planMgmt/manageDrugs.js" />'></script>

<script type="text/javascript">
	$(document).ready(function() {
		var scope = angular.element(document.getElementById("manageDrugListDiv")).scope();

		if (scope) {
			scope.$apply(function () {

				scope.invalidRxcuiDataMsg = '<spring:message code="label.drugs.error.invalid.rxcui" />';
				scope.exceedsRxcuiDataMsg = '<spring:message code="label.drugs.error.exceeds.rxcui" />';

				scope.displayName = {
					rxcui : '<spring:message code="label.drugs.grid.col.rxcui" />',
					name : '<spring:message code="label.drugs.grid.col.name" />',
					route : '<spring:message code="label.drugs.grid.col.route" />',
					fullName : '<spring:message code="label.drugs.grid.col.fullName" />',
					genericRxcui : '<spring:message code="label.drugs.grid.col.genericRxcui" />',
					status : '<spring:message code="label.drugs.grid.col.status" />'
				};
			});
		}
	});
</script>

<div class="gutter10" id="manageDrugListDiv" ng-app="manageDrugListApp" ng-controller="ManageDrugListCtrl">

	<div class="row-fluid">
		<div class="span3">
			<h1 class="pull-left">
				<spring:message code="pgheader.manageDrugs" />
				<small>{{gridOptions.data.length}}&nbsp;<spring:message code="label.drugs.total.rxcui" /></small>
			</h1>
		</div> <!-- end of span3 -->
	</div><!-- end of row-fluid -->

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
			</div> <!-- end of header -->

			<ul class="nav nav-list graybg">
				<li class="active"><a href="#"><spring:message code='label.drugs.search.rxcui'/></a></li>
			</ul>
		</div> <!-- end of span3 -->

		<div class="span9" id="rightpanel">
			<div class="row-fluid">
				<div class="header">
					<h4 class="margin0">
						<spring:message code='label.drugs.header.rxcui' />
					</h4>
				</div> <!-- end of header -->

				<form class="form-vertical gutter10 lightgray" id="frmManageDrugs" name="frmManageDrugs" action="${pageContext.request.contextPath}/admin/getDrugDataList" method="POST">
					<df:csrfToken/>

					<div class="control-group">
						<label for="rxcuiFilter" class="control-label"><spring:message code='label.drugs.enter.rxcui' />&nbsp;<span style="color:red">*</span></label>
						<div class="controls">
							<textarea name="rxcuiFilter" id="rxcuiFilter" rows="3" ng-model="rxcuiFilter" class="span7" required rxcui-validation></textarea>
							<br />
							<span style="color:red" ng-show="frmManageDrugs.rxcuiFilter.$dirty && frmManageDrugs.rxcuiFilter.$invalid">
								<span ng-show="frmManageDrugs.rxcuiFilter.$error.required"><spring:message code='label.drugs.error.required.rxcui' /></span>
								<span ng-show="frmManageDrugs.rxcuiFilter.$error.rxcuiValidation">{{invalidRxcuiData}}</span>
							</span>
						</div> <!-- end of controls-->
					</div> <!-- end of control-group -->

					<div class="txt-left">
						<input type="button" class="btn btn-primary" ng-disabled="!rxcuiFilter || invalidRxcui" ng-click="getDrugDataByRxcuiList()" value="<spring:message code='label.search' />" title="<spring:message  code='label.search' />">&nbsp;&nbsp;
						<input type="button" class="btn" ng-disabled="!rxcuiFilter || invalidRxcui" ng-click="exportData()" id="exportData" name="exportData" value="<spring:message code='label.export' />" title="<spring:message code='label.export' />" />&nbsp;&nbsp;
						<input type="reset" class="btn" ng-click="resetFilter()" id="resetFilter" name="resetFilter" value="<spring:message code='label.reset' />" title="<spring:message code='label.reset' />" />
					</div> <!-- end of txt-left -->
				</form> <!-- end of frmManageDrugs -->

				<div class="watermark" ng-show="!gridOptions.data.length"><spring:message code='label.drugs.error.nodata' /></div>
				<div id="drugGridData" ui-grid="gridOptions" style="height: 100%" class="grid" ui-grid-resize-columns ui-grid-selection ui-grid-exporter></div>
			</div><!-- end of .row-fluid -->
		</div> <!-- end of span9 -->
	</div> <!-- end of row-fluid -->
</div><!-- end .gutter10 -->
