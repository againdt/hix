<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
	<div class="row-fluid">
        	<ul class="page-breadcrumb">
        		<li><a href="<c:url value="/planmgmt/issuer/account/profile/company/info"/>">&lt; <spring:message  code="label.back"/></a></li>
            	<li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            	<li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            	<li><spring:message  code="pgheader.accreditationDocuments"/></li>
            </ul><!--page-breadcrumb ends-->
			<%-- <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
			<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
		</div>
	
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 		<jsp:param name="pageName" value="accreditation"/>
 	 	 	 	</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			
			<div class="span9" id="rightpanel">
                    <div class="header">
                        <h4 class="pull-left"><spring:message  code="pgheader.accreditationDocuments"/></h4>
                        <a class="btn btn-small pull-right" href="<c:url value="/planmgmt/issuer/account/profile/accreditationdocument/edit"/>"><spring:message  code="label.edit"/></a>
                    </div>
                    <div class="gutter10">
						<p><strong><spring:message  code="pgheader.uploadDocumentsInfoview"/></strong></p>
                    <form class="form-horizontal" id="frmIssuerApp" name="frmIssuerApp" action="" method="GET">
						<div class="header">
							<h4><spring:message  code="label.issuerUploadAuthority"/></h4>
						</div>
						<div class="gutter10-t">
						 <c:choose>
							<c:when test="${fn:length(authFilesList) == 0}">
								<div class="control-group clearfix">
									<div class="controls paddingT5 txt-bold"></div>
								</div>
							</c:when>
						   <c:otherwise>
								<c:forEach var="authFilesListObj" items="${authFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required">File<c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${authFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</div>
						<div class="header">
							<h4><spring:message  code="label.issuerUploadAccreditation"/></h4>
						</div>
						<div class="gutter10-t">
						<div class="control-group clearfix">
							<label class="control-label required"><spring:message  code="label.issuerUploadAccreditation"/></label>
							<div class="controls paddingT5 txt-bold"><c:out value="${issuerAccreditation}"/></div>
						</div><!-- end of control-group -->
						
						<div class="control-group clearfix">
							<label class="control-label required"><spring:message  code="label.accreditingEntity"/></label>
							<div class="controls paddingT5 txt-bold"><c:out value="${accreditionEntity}"/></div>
						</div><!-- end of control-group -->
						
						<fmt:formatDate pattern="MM/dd/yyyy" value="${issuerObj.accreditationExpDate}" var="expDate" timeZone="${timezone}"/>
						<div class="control-group">
							<label class="control-label required"><spring:message  code="label.expirationDate"/></label>
							<div class="controls gutter5-t txt-bold">${accExpDate}</div>
						</div><!-- end of control-group-->
						
						<c:forEach var="accreditationFilesListObj" items="${accreditationFilesList}" varStatus="indexvar">
							<div class="control-group clearfix">
								<label for="issuerName" class="control-label required"> <spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
									<div class="controls paddingT5 txt-bold">${accreditationFilesListObj.uploadedFileName}</div>	
							</div><!-- end of control-group -->						
						</c:forEach>
					</div>
						<div class="header">
							<h4><spring:message  code="label.organizationData"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(organizationFilesList) == 0}">
								<div class="control-group clearfix">
									<div class="controls paddingT5 txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="organizationFilesListObj" items="${organizationFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
											<div class="controls paddingT5 txt-bold">${organizationFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</div>
						<div class="header">
							<h4><spring:message  code="label.issuerUploadMarketingReq"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(marketingFilesList) == 0}">
								<div class="control-group clearfix">
									<div class="controls paddingT5 txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="marketingFilesListObj" items="${marketingFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${marketingFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</div>
						<div class="header">
							<h4><spring:message  code="label.financialDisclosures"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(disclosureFilesList) == 0}">
								<div class="control-group clearfix">
									<div class="controls paddingT5 txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="disclosureFilesListObj" items="${disclosureFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${disclosureFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</div>
						
						<div class="header">
							 <h4><spring:message  code="label.claimsPaymentPolPract"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(payPolPractFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="payPolPractFilesListObj" items="${payPolPractFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${payPolPractFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="header">
							<h4><spring:message  code="label.enrollmentAndDisenroll"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(enrollDisDataFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="enrollDisDataFilesListObj" items="${enrollDisDataFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${enrollDisDataFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						
						<div class="header">
							<h4><spring:message  code="label.ratingPractices"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(ratingPractFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="ratingPractFilesListObj" items="${ratingPractFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${ratingPractFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="header">
							 <h4><spring:message  code="label.costSharAndPayments"/></h4>
						</div>
						<div class="gutter10-t">
						 <c:choose>
							<c:when test="${fn:length(costSharPaymentFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="costSharPaymentFilesListObj" items="${costSharPaymentFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${costSharPaymentFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						
						<div class="header">
							<h4><spring:message  code="label.issuerUploadAdditionalInfo"/></h4>
						</div>
						<div class="gutter10-t">
						 <c:choose>
							<c:when test="${fn:length(addsuppFilesList) == 0}">
								<div class="control-group clearfix">
									<div class="controls paddingT5 txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="addsuppFilesListObj" items="${addsuppFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${addsuppFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
					</form>
                </div>
            </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
	</div>
<df:csrfToken/>
<script type="text/javascript">
var csrValue = $("#csrftoken").val();
$(document).ready(function() {
	var button_id_names='#btn_upload_authority,'+
						'#btn_upload_accrediting,'+
						'#btn_upload_orgData,'+
						'#btn_upload_marketing,'+
						'#btn_upload_finDisclosures,'+
						'#btn_upload_addSupportDoc';
						
		$(button_id_names).click(function(){
		var idName = this.id;
		var upload = idName.split("_");
		$('#fileToUpload').val(upload[2]);
		$('#frmIssuerApp').ajaxSubmit({
			url: "<c:url value='/planmgmt/issuer/application/documents/upload'/>",
			data: {"csrftoken":csrValue},
			success: function(responseText){
	        	 
				var val = responseText.split("|");    	 
	        	 if(val[0]=='authority'){
		        	 $("#authority_display").text(val[1]);
					 $("#hdnAuthority").val(val[2]);
	        	 }
	        	 else if(val[0]=='accrediting'){
		        	 $("#accrediting_display").text(val[1]);
					 $("#hdnAccrediting").val(val[2]);
	        	 }
	        	 else if(val[0]=='orgData'){
		        	 $("#orgData_display").text(val[1]);
					 $("#hdnOrgData").val(val[2]);
	        	 }
	        	 if(val[0]=='marketing'){
		        	 $("#marketing_display").text(val[1]);
					 $("#hdnMarketing").val(val[2]);
	        	 }
	        	 else if(val[0]=='finDisclosures'){
		        	 $("#finDisclosures_display").text(val[1]);
					 $("#hdnFinDisclosures").val(val[2]);
	        	 }
	        	 else if(val[0]=='addSupportDoc'){
		        	 $("#addSupportDoc_display").text(val[1]);
					 $("#hdnAddSupportDoc").val(val[2]);
	        	 }
	        	 
	   		 }
		});
	  return false; 
	});	// button click close
	}); // ready method close
	
	var validator = $("#frmIssuerApp").validate({ 
		rules : {
			authority : {required : true},
			accreditingEntity : {required : true},
			accrediting : { required : true},
			orgData : { required : true},
			marketing : { required : true},
			finDisclosures : { required : true},		
			addSupportDoc : {required : true}
		},
		messages : {
			authority : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadAuthority' javaScriptEscape='true'/></span>"},
			accreditingEntity: { required : "<span><em class='excl'>!</em><spring:message  code='err.validateAccreditingEntity' javaScriptEscape='true'/></span>"},
			accrediting : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadAccreditation' javaScriptEscape='true'/></span>"},
			orgData : { required : "<span><em class='excl'>!</em><spring:message  code='err.validateUploadOrgData' javaScriptEscape='true'/></span>"},
			marketing: { required : "<span><em class='excl'>!</em><spring:message  code='label.validateUploadMarketingReq' javaScriptEscape='true'/></span>"},
			finDisclosures : { required : "<span><em class='excl'>!</em><spring:message  code='err.validateUploadFinDisclosures' javaScriptEscape='true'/></span>"},		
			addSupportDoc : {required : "<span><em class='excl'>!</em><spring:message  code='err.validateAddSupportDoc' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});

</script>
