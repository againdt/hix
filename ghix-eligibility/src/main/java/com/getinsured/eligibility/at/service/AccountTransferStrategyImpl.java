package com.getinsured.eligibility.at.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.eligibility.at.ref.service.ReferralCancelService;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.resp.service.SsapApplicationService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.timeshift.TimeshiftContext;

public abstract class AccountTransferStrategyImpl implements AccountTransferStrategy {

	private static final String ERRORACTION = "/atresponse/erroractions";

	private static final String PROCESS_STATE_REFERRAL = "/processStateReferral";
	private static final String PROCESS_LCE_REFERRAL = "/processLceReferral";
	private static final String PROCESS_QE_REFERRAL = "/processQEReferral";
	private static final String INITIATE_AT_FULL_RESPONSE = "/initiateATFullResponse";
	private static final String PROCESS_LCE_APP = "/processLceAppReferral";

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountTransferStrategyImpl.class);

	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private ReferralCancelService referralCancelService;
	@Autowired
	private SsapApplicationService ssapApplicationService;

	@Value("#{configProp['ghixEligibilityServiceURL']}")
	private String erpSvcUrl;

	@Autowired
	private GIWSPayloadRepository giwsPayloadRepository;

	@Autowired
	@Qualifier("referralOEService")
	private ReferralOEService referralOEService;

	protected void callFullResponseProcess(AccountTransferRequestPayloadType request, Integer giwsPayloadId, boolean isFullDetermination, String caseNumber, String atResponseType) {
		AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
		setTimeshiftContext(accountTransferRequestDTO);
		accountTransferRequestDTO.setAccountTransferRequestPayloadType(request);
		accountTransferRequestDTO.setGiwsPayloadId(giwsPayloadId);
		accountTransferRequestDTO.setFullDetermination(isFullDetermination);
		accountTransferRequestDTO.setCaseNumber(caseNumber);
		accountTransferRequestDTO.setAtResponseType(atResponseType);
		ghixRestTemplate.exchange(erpSvcUrl + INITIATE_AT_FULL_RESPONSE, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
	}

	protected void reportError(String ssapApplicationId, String reason) {
		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, ssapApplicationId);
		resultMap.put(EligibilityConstants.ERROR_REASON, reason);
		ghixRestTemplate.exchange(erpSvcUrl + ERRORACTION, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, resultMap);
	}

	protected void callReferralProcess(AccountTransferRequestPayloadType request, Integer giwsPayloadId, boolean isFullDetermination, boolean isLCE, String atResponseType, boolean isQEDetermined) {
		final boolean isQE = referralOEService.isQE();
		AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
		setTimeshiftContext(accountTransferRequestDTO);
		accountTransferRequestDTO.setAccountTransferRequestPayloadType(request);
		accountTransferRequestDTO.setGiwsPayloadId(giwsPayloadId);
		accountTransferRequestDTO.setFullDetermination(isFullDetermination);
		accountTransferRequestDTO.setLCE(isLCE);
		accountTransferRequestDTO.setAtResponseType(atResponseType);
		if (isQE || isQEDetermined) {
			accountTransferRequestDTO.setQE(true);
			ghixRestTemplate.exchange(erpSvcUrl + PROCESS_QE_REFERRAL, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
		} else {
			ghixRestTemplate.exchange(erpSvcUrl + PROCESS_STATE_REFERRAL, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
		}
	}

	protected void callReferralProcess(AccountTransferRequestPayloadType request, Integer giwsPayloadId, boolean isFullDetermination, boolean isLCE, String atResponseType, String notificationType, long enrolledApplicationId, boolean isQEDetermined,
	        List<ApplicationsWithSameIdDTO> applicationsWithSameId) {
		AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
		setTimeshiftContext(accountTransferRequestDTO);
		accountTransferRequestDTO.setAccountTransferRequestPayloadType(request);
		accountTransferRequestDTO.setGiwsPayloadId(giwsPayloadId);
		accountTransferRequestDTO.setFullDetermination(isFullDetermination);
		accountTransferRequestDTO.setLCE(isLCE);
		accountTransferRequestDTO.setAtResponseType(atResponseType);
		accountTransferRequestDTO.setNotificationType(notificationType);
		accountTransferRequestDTO.setEnrolledApplicationId(enrolledApplicationId);
		accountTransferRequestDTO.setApplicationsWithSameId(applicationsWithSameId);
		if (isLCE) {
			LOGGER.info("ID - PROCESS LCE REFERRAL");
			ghixRestTemplate.exchange(erpSvcUrl + PROCESS_LCE_REFERRAL, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
		} else {
			final boolean isQE = referralOEService.isQE();
			if (isQE || isQEDetermined) {
				accountTransferRequestDTO.setQE(true);
				LOGGER.info("ID - PROCESS QE REFERRAL");
				ghixRestTemplate.exchange(erpSvcUrl + PROCESS_QE_REFERRAL, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
			} else {
				LOGGER.info("ID - PROCESS Initial REFERRAL");
				ghixRestTemplate.exchange(erpSvcUrl + PROCESS_STATE_REFERRAL, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
			}
		}
	}

	protected void callReferralProcess(AccountTransferRequestDTO accountTransferRequestDTO) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("ID - PROCESS To be Executed " + accountTransferRequestDTO.getProcessToBeExecuted()+" "+accountTransferRequestDTO.getGiwsPayloadId());
		}
		setTimeshiftContext(accountTransferRequestDTO);
		try {
			ghixRestTemplate.exchange(erpSvcUrl + accountTransferRequestDTO.getProcessToBeExecuted(), getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
		}catch(Exception e) {
			LOGGER.error("Exception in AT process "+e);
		}
	}
	
	protected void callApplicationProcess(AccountTransferRequestDTO accountTransferRequestDTO) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("ID - PROCESS To be Executed " + PROCESS_LCE_APP+" "+accountTransferRequestDTO.getGiwsPayloadId());
		}
		setTimeshiftContext(accountTransferRequestDTO);
		try {
			ghixRestTemplate.exchange(erpSvcUrl + PROCESS_LCE_APP, getUserName(), HttpMethod.POST, MediaType.APPLICATION_XML, String.class, accountTransferRequestDTO);
		}catch(Exception e) {
			LOGGER.error("Exception in AT process "+e);
		}
	}
	
	private String getUserName() {
		String userName = TimeshiftContext.getCurrent().getUserName();
		if(userName == null) {
			userName = ReferralConstants.EXADMIN_USERNAME;
		}
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Username in AT context:{}",userName);
		}
		return userName;
	}

	protected void updateSSAPApplicationGiWsPayload(long ssapApplicationId, int giwsPayloadId) {
		giwsPayloadRepository.updateSsapApplicationId(ssapApplicationId, giwsPayloadId);
	}

	protected void updateSSAPApplicantGiWsPayload(long ssapApplicantId, int giwsPayloadId) {
		GIWSPayload giwsPayload = giwsPayloadRepository.findOne(giwsPayloadId);
		giwsPayload.setSsapApplicantId(ssapApplicantId);
		giwsPayloadRepository.save(giwsPayload);
	}

	protected boolean closeApp(SsapApplication erApp) {
		/* close app */
		ssapApplicationService.closeApplication(erApp.getId());
		/* If application status is other than UC then dont ignore reseting activation object (as there is none) */
		if (!ApplicationStatus.UNCLAIMED.equals(erApp.getApplicationStatus())){
			return true;
		}
		int id = (int) erApp.getId();
		/* call Referral API to Cancel Activation and from result determine whether UC was LCE */
		return referralCancelService.cancelActivation(id);
	}
	
	private void setTimeshiftContext(AccountTransferRequestDTO requestDTO) {
		if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
			requestDTO.setUser(TimeshiftContext.getCurrent().getUserName());
			requestDTO.setTsOffset(TimeshiftContext.getCurrent().getTimeOffset());
		}
		
	}

}
