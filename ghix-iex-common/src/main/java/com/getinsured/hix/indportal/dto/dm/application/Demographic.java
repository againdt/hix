package com.getinsured.hix.indportal.dto.dm.application;

import java.util.List;

public class Demographic {
	 private String birthDate;
	 private Name name;
	 private String maritalStatus;
	 private Boolean noHomeAddressIndicator;
	 private Boolean hispanicOriginIndicator;
	 private Boolean liveOutsideStateTemporarilyIndicator;
	 private Boolean americanIndianAlaskanNativeIndicator;
	 private List<String> ethnicity;
	 private String sex;
	 private List<String> race;
	 private String ssn;
	private HomeAddress homeAddress;
	 private MailingAddress mailingAddress;


	 // Getter Methods 

	 public String getBirthDate() {
	  return birthDate;
	 }

	 public Name getName() {
	  return name;
	 }

	 public String getMaritalStatus() {
	  return maritalStatus;
	 }

	 public Boolean getNoHomeAddressIndicator() {
	  return noHomeAddressIndicator;
	 }

	 public Boolean getLiveOutsideStateTemporarilyIndicator() {
	  return liveOutsideStateTemporarilyIndicator;
	 }

	 public Boolean getAmericanIndianAlaskanNativeIndicator() {
	  return americanIndianAlaskanNativeIndicator;
	 }

	 public String getSex() {
	  return sex;
	 }

	 public HomeAddress getHomeAddress() {
	  return homeAddress;
	 }

	 public MailingAddress getMailingAddress() {
	  return mailingAddress;
	 }

	 // Setter Methods 

	 public void setBirthDate(String birthDate) {
	  this.birthDate = birthDate;
	 }

	 public void setName(Name name) {
	  this.name = name;
	 }

	 public void setMaritalStatus(String maritalStatus) {
	  this.maritalStatus = maritalStatus;
	 }

	 public void setNoHomeAddressIndicator(Boolean noHomeAddressIndicator) {
	  this.noHomeAddressIndicator = noHomeAddressIndicator;
	 }

	 public void setLiveOutsideStateTemporarilyIndicator(Boolean liveOutsideStateTemporarilyIndicator) {
	  this.liveOutsideStateTemporarilyIndicator = liveOutsideStateTemporarilyIndicator;
	 }

	 public void setAmericanIndianAlaskanNativeIndicator(Boolean americanIndianAlaskanNativeIndicator) {
	  this.americanIndianAlaskanNativeIndicator = americanIndianAlaskanNativeIndicator;
	 }

	 public void setSex(String sex) {
	  this.sex = sex;
	 }

	 public void setHomeAddress(HomeAddress homeAddress) {
	  this.homeAddress = homeAddress;
	 }

	 public void setMailingAddress(MailingAddress mailingAddress) {
	  this.mailingAddress = mailingAddress;
	 }

	 public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public List<String> getRace() {
		return race;
	}

	public void setRace(List<String> race) {
		this.race = race;
	}

	public List<String> getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(List<String> ethnicity) {
		this.ethnicity = ethnicity;
	}

	public Boolean getHispanicOriginIndicator() {
		return hispanicOriginIndicator;
	}

	public void setHispanicOriginIndicator(Boolean hispanicOriginIndicator) {
		this.hispanicOriginIndicator = hispanicOriginIndicator;
	}

	}
