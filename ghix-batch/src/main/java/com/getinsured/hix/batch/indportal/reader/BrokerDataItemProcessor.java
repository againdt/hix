package com.getinsured.hix.batch.indportal.reader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import com.getinsured.hix.batch.indportal.utils.BrokerService;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Location;

public class BrokerDataItemProcessor implements ItemProcessor<BrokerData, Broker>{
	private static final Logger LOGGER = Logger.getLogger(BrokerDataItemProcessor.class);
	private static final String DATE_FORMAT_FROM_JSON = "MM/dd/yyyy";

	private String skipPattern;
	private BrokerService brokerService;

	public void setSkipPattern(String skipPattern) {
		this.skipPattern = skipPattern;
	}

	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}
	@Override
    public Broker process(BrokerData result) throws Exception {
		LOGGER.info(new StringBuffer().append("Processing result :").append(result));
        
        Broker brokerObj = brokerService.findBrokerByNpn(result.getNpn());
        if("skipExisting".equalsIgnoreCase(skipPattern) && brokerObj != null)
        {
        	//--- skip existing record from re-inserting ---
    		return null;
        }
        if(brokerObj == null)
        {
        	brokerObj = new Broker();
        	brokerObj.setApplicationDate(new Date());
        	brokerObj.setCertificationStatus("Incomplete");
            brokerObj.setClientsServed("Individuals / Families");
        }
        String s1[]=result.getName().split("[ ]+");
        if(s1.length > 0)
        {
        	brokerObj.setFirstName(s1[0]);
        }
        if(s1.length>1)
        {
        	brokerObj.setLastName(s1[s1.length-1]);
        }
        brokerObj.setLicenseNumber(result.getLicense());
        brokerObj.setProductExpertise(result.getQualificationType());
        brokerObj.setYourPublicEmail(result.getEmail());
        brokerObj.setContactNumber(result.getPhone());
        brokerObj.setNpn(result.getNpn());
        populateLicenseRenewalDate(brokerObj,result);
        
        Location location = new  Location();
		location.setAddress1(result.getAddress());
		location.setCity(result.getCityName());
		location.setState(result.getState());
		location.setZip(result.getZip());
		brokerObj.setLocation(location);
		return brokerObj;
    }
	
	private void populateLicenseRenewalDate(Broker brokerObj,BrokerData result) {
		
		if (!StringUtils.isEmpty(result.getLicenseExpiryDate())) {

			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_FROM_JSON);
				Date date = simpleDateFormat.parse(result.getLicenseExpiryDate());
				brokerObj.setLicenseRenewalDate(date);
			} catch (ParseException e) {
				LOGGER.error("Error while converting License expiry date",e);
			}
		}
		
	}
 
}
