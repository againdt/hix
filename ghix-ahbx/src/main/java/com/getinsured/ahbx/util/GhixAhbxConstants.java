/**
 * 
 */
package com.getinsured.ahbx.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author panda_p
 *
 */
@Component
public final class GhixAhbxConstants {

	/**
	 * 
	 */
	public GhixAhbxConstants() {
		// TODO Auto-generated constructor stub
	}
	
	public static String DATE_FORMAT = "MM/dd/yyyy";
	public static String PLAIN_DATE_FORMAT = "mmddyyyy";
	
	
	public static  String TARGET_SERVER_URL;
	@Value("#{configProp.targetServerUrl}")
	public void setTARGET_SERVER_URL(String target_server_url) {
		TARGET_SERVER_URL = target_server_url;
	}
	
	public static  String WSDL_URL_IND20;
	@Value("#{configProp['enrollment.wsdlAcnSendIndvPSData']}")
	public void setWSDL_URL_IND20(String wSDL_URL_IND20) {
		WSDL_URL_IND20 = wSDL_URL_IND20;
	}
	
	public static  String WSDL_URL_IND21;
	@Value("#{configProp['enrollment.wsdAcnIndvConfirmation']}")
	public void setWSDL_URL_IND21(String wSDL_URL_IND21) {
		WSDL_URL_IND21 = wSDL_URL_IND21;
	}
	
	public static  String WSDL_URL_IND70;
	@Value("#{configProp['enrollment.adminUpdateConfirmation']}")
	public void setWSDL_URL_IND70(String wSDL_URL_IND70) {
		WSDL_URL_IND70 = wSDL_URL_IND70;
	}

	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final String RESPONSE_FAILURE = "FAILURE";
	public static final String RESPONSE_HANDLING_FAILURE = "RESPONSE_HANDLING_FAILURE";
	
	public static final String MARKET_TYPE_SHOP = "24";
	public static final String MARKET_TYPE_INDIVIDUAL = "FI";
	public static final String MARKET_TYPE_MEDICAL="MEDICAL";
	
	public static final String INVALID_INPUT_MSG = "Invalid input: input data is null or empty";
	public static final int INVALID_INPUT_CODE = 4001;
		
	public static String WSDL_URL_IND35;
	public static String sendBrokerDetailsEndPointImplPort;	
	public static String WSDL_URL_IND47;
	//For IND49-50
	public static String retrieveEmployerBOBEndPointImplPort;
	//For IND51-52
	public static String retrieveIndividualBOBEndPointImplPort;
	//For IND72
	public static String WSDL_URL_IND72;
	
	public static String WSDL_URL_IND73;
	
	@Value("#{configProp['sendEntityDetailsEndPointImplPort']}")
	public void setSendEntityDetailsEndPointImplPort(String wSDL_URL_IND35)
	{
		WSDL_URL_IND35 = wSDL_URL_IND35;
	}
	
	@Value("#{configProp['sendEntityDesignationEndPointImplPort']}")
	public void setSendBrokerDesignationEndPointImplPort(
			String wSDL_URL_IND47)
	{
		WSDL_URL_IND47 = wSDL_URL_IND47;
	}
	
	@Value("#{configProp['retrieveEmployerBOBEndPointImplPort']}")
	public void setRetrieveEmployerBOBEndPointImplPort(
			String pRetrieveEmployerBOBEndPointImplPort) {
		retrieveEmployerBOBEndPointImplPort = pRetrieveEmployerBOBEndPointImplPort;
	}
	@Value("#{configProp['retrieveIndividualBOBEndPointImplPort']}")
	public void setRetrieveIndividualBOBEndPointImplPort(
			String pRetrieveIndividualBOBEndPointImplPort) {
			retrieveIndividualBOBEndPointImplPort = pRetrieveIndividualBOBEndPointImplPort;
	}
	public static  boolean ENRL_AHBX_WSDL_CALL_ENABLE;
	@Value("#{configProp['enrollment.ahbxWsdlCallEnable']}")
	public void setENRL_AHBX_WSDL_CALL_ENABLE(
			boolean eNRL_AHBX_WSDL_CALL_ENABLE) {
		ENRL_AHBX_WSDL_CALL_ENABLE = eNRL_AHBX_WSDL_CALL_ENABLE;
	}
	public static final String EMPLOYER_ROLE = "EMPLOYER";
	public static final String INDIVIDUAL_ROLE = "INDIVIDUAL";
	
	public static String WSDL_URL_IND54;
	@Value("#{configProp.delegateAccessCodeEndPointImplPort}")
	public void setSEND_ASSISTER_DETAILS(
			String wSDL_URL_IND54) {
		WSDL_URL_IND54 = wSDL_URL_IND54;
	}
	
	@Value("#{configProp['agentAssisterBookOfBusinessEndPointImplPort']}")
	public void setAgentAssisterBookOfBusinessEndPointImplPort(String wSDL_URL_IND72)
	{
		WSDL_URL_IND72 = wSDL_URL_IND72;
	}
		
	@Value("#{configProp['individualBobDetailsEndPointImplPort']}")
	public void setIndividualBobDetailsEndPointImplPort(String wSDL_URL_IND73)
	{
		WSDL_URL_IND73 = wSDL_URL_IND73;
	}
		
	public static final String AGENT = "AGENT";
	public static final String ASSISTER = "ASSISTER";
	public static final String STATUS_ACTIVE = "Active";
	public static final String STATUS_INACTIVE = "InActive";
	public static final String STATUS_PENDING = "Pending";
	public static final String ISSUER = "ISSUER";
	public static final String ISSUER_ENROLL_REP = "ISSUER_ENROLL_REP";
	public static final String APPROVEDADMINSTAFFL1 = "APPROVEDADMINSTAFFL1"; 
	public static final String APPROVEDADMINSTAFFL2 = "APPROVEDADMINSTAFFL2";
	
	public static final String AHBXWSDLCALLENABLE ="enrollment.ahbxWsdlCallEnable";
}
