<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<style>
#rightpanel a.btn {
    margin: 9px 7px;
}
</style>
<div class="gutter10">
  <div class="row-fluid">
  		 <ul class="page-breadcrumb">
            <li><a href="#">&lt; Back</a></li>
            <li><a href="#">Account</a></li>
            <li>Manage Account Information</li>
        	
        </ul><!--page-breadcrumb ends-->
    <h1 id="skip">${name}</h1>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <h4 class="graydrkbg"><spring:message  code="label.emprattesttxt1"/></h4>
        <ul class="nav nav-list">
          <li class="active"><a href="#"><spring:message  code="label.emprattesttxt2"/></a></li>
          <c:if test="${eStatus != 'NEW'}">
          <li><a href="<c:url value="/shop/employer/application/attestationresult?prePage=/shop/employer/accounts/acccompanyinfo" />"><spring:message  code="label.emprattesttxt3"/></a></li>
          </c:if>
        </ul>
      </div>
      <!-- /.sidebar-->
      <div class="span9" id="rightpanel">
        <div class="row-fluid">
        	<div class="graydrkaction margin0">
                <h4 class="pull-left"><spring:message  code="label.emprattesttxt4"/></h4>
                <a href="<c:url value="../application/editcompanyinfo?prePage=/shop/employer/accounts/acccompanyinfo"/>" class="btn btn-small pull-right"><spring:message  code="label.empredit"/></a>
            </div>
          <div class="gutter10">
           	  			<table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.businessname"/></td>
                                <td><strong>${employer.name}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right span4">EIN</td>
                                <td><strong>${employer.federalEIN}</strong></td>
                            </tr>
                             <tr>
                                <td class="txt-right span4"><spring:message  code="label.emprattestcorptype"/></td>
                                <td><strong>${orgType}</strong></td>
                            </tr>
                             <tr>
                                <td class="txt-right span4"><spring:message  code="label.emprfultimeemp"/></td>
                                <td><strong>${employer.fullTimeEmp}</strong></td>
                            </tr> 
                             <tr>
                                <td class="txt-right span4"><spring:message  code="label.emprpriworksiteaddr"/></td>
                                <td><strong>${primaryWkSite}</strong></td>
                            </tr>
                            
                            <c:forEach items="${additionalWkSites}" var="additionalWkSite" varStatus="theCount">
								<tr>
									<td class="txt-right span4"><spring:message  code="label.employeradditionalworksite"/> ${theCount.count}</td>
									<td><strong>${additionalWkSite}</strong></td>
								</tr>
							</c:forEach>
						</table>
          </div>
          <!--gutter10--> 
        </div>
        <!--row-fluid--> 
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<!--gutter10-->