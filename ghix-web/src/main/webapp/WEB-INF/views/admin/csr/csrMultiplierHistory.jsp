<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<style type="text/css">
	#sidebar .csrm-sidebar  li{border: 1px solid #E9E9E9;}
</style>
<div class="gutter10">
	<div class="row-fluid">
        <h1 class="span10 margin0 gutter20-b"><spring:message code="label.multiplier"/></h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.userfulInfo"/></h4>
       		</div>
       		<ul class="nav nav-list graybg csrm-sidebar">
	       		<li> 
		       		<spring:message code="label.csrInfo"/>
				</li>
			</ul>
        </div>
		<div class="span9" id="rightpanel">
			<display:table pagesize="${pageSize}" name="csrMultipliers" list="csrMultipliers" sort="list" class="table table-condensed table-border-none table-striped" requestURI="/admin/planmgmt/csrmultiplier/history">
			   <display:column titleKey="label.date" property="creation_timestamp" sortable="false"/>
	           <display:column titleKey="label.csrVariation" property="csrVariation" sortable="false"/>
	           <display:column titleKey="label.metalTier" property="planLevel" sortable="false" />
	           <display:column titleKey="label.previousVal" property="prvMultiplierVal" sortable="false" />
	           <display:column titleKey="label.newVal" property="newMultiplierVal" sortable="false" />
			   <display:column titleKey="label.updatedBy" property="lastUpdatedBy" sortable="false" />
			   <display:setProperty name="paging.banner.placement" value="bottom" />
	           <display:setProperty name="paging.banner.some_items_found" value=''/>
	           <display:setProperty name="paging.banner.all_items_found" value=''/>
	           <display:setProperty name="paging.banner.group_size" value='50'/>
	           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
	           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
               <display:setProperty name="paging.banner.onepage" value=''/>
	           <display:setProperty name="paging.banner.one_item_found" value=''/>
	           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
			           <div class="pagination center">
						<ul>
							<li></li>
							<li>{0}</li>
							<li><a href="{3}">Next &gt;</a></li>
						</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
						<div class="pagination center">
							<ul>
								<li><a href="{2}">&lt; Prev</a></li>
								<li>{0}</li>
								<li></li>
							</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.full" value='
						<div class="pagination center">
							<ul>
								<li><a href="{2}">&lt; Prev</a></li>
								<li>{0}</li>
								<li><a href="{3}">Next &gt;</a></li>
							</ul>
						</div>
						'/>
			</display:table>
			<div class="control-group center">
				<div class="span6 txt-center"><a class="btn btn-primary" role="button" href="<c:url value="/admin/planmgmt/csrmultiplier/manage"/>"><spring:message code="label.back"/></a></div>
			</div> 
		</div>
	</div>
</div>
