package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlprda.AdditionalFieldsType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlprda.ObjectFactory;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;


public class AdditionalFieldsTypeWrapper implements JSONAware{
	private static final Logger LOGGER = LoggerFactory.getLogger(AdditionalFieldsTypeWrapper.class);
	private ObjectFactory factory;
	private AdditionalFieldsType additionalFieldsType;
	
	private String commentsToAgencyText;
    private String uscisBenefitsText;
    private String expirationDate;
    private String coaCode;
    private String deferredActionStatusExpirationDate;
    private boolean deferredActionExpirationNAIndicator;
    private String familyUnityExpirationDate;
    private String paroleeExpirationDate;
    private String admittedToDate;
    private boolean durationOfStatusIndicator;
    private boolean approvedSelfPetitionIndicator;
    private boolean i360ApplicationFiledIndicator;    
    private boolean pendingPrimaFacieSelfPetitionIndicator;

    private static final String COMMENTSTOAGENCYTEXT = "CommentsToAgencyText";
    private static final String USCISBENEFITSTEXT = "UscisBenefitsText";
    private static final String EXPIRATIONDATE = "ExpirationDate";
    private static final String COACODE = "CoaCode";
    private static final String DEFERREDACTIONSTATUSEXPIRATIONDATE = "DeferredActionStatusExpirationDate";
    private static final String DEFERREDACTIONEXPIRATIONNAINDICATOR = "DeferredActionExpirationNAIndicator";
    private static final String FAMILYUNITYEXPIRATIONDATE = "FamilyUnityExpirationDate";
    private static final String PAROLEEEXPIRATIONDATE = "ParoleeExpirationDate";
    private static final String ADMITTEDTODATE = "AdmittedToDate";
    private static final String DURATIONOFSTATUSINDICATOR = "DurationOfStatusIndicator";
    private static final String APPROVEDSELFPETITIONINDICATOR = "ApprovedSelfPetitionIndicator";
    private static final String I360APPLICATIONFILEDINDICATOR = "I360ApplicationFiledIndicator";
    private static final String PENDINGPRIMAFACIESELFPETITIONINDICATOR = "PendingPrimaFacieSelfPetitionIndicator";
	

	public AdditionalFieldsTypeWrapper(){
		this.factory = new ObjectFactory();
		this.additionalFieldsType = factory.createAdditionalFieldsType();
	}
	
	public AdditionalFieldsTypeWrapper(AdditionalFieldsType additionalFieldsType){
		
		if(additionalFieldsType == null){
			return;
		}
		
		this.commentsToAgencyText = additionalFieldsType.getCommentsToAgencyText();                      
		this.uscisBenefitsText= additionalFieldsType.getUscisBenefitsText();                         
		this.coaCode= additionalFieldsType.getCoaCode();                                   
		this.deferredActionExpirationNAIndicator= additionalFieldsType.isDeferredActionExpirationNAIndicator();
		this.durationOfStatusIndicator= additionalFieldsType.isDurationOfStatusIndicator();                
		this.approvedSelfPetitionIndicator= additionalFieldsType.isApprovedSelfPetitionIndicator();
		this.i360ApplicationFiledIndicator= additionalFieldsType.isI360ApplicationFiledIndicator();
		this.pendingPrimaFacieSelfPetitionIndicator= additionalFieldsType.isPendingPrimaFacieSelfPetitionIndicator();
		
		try {
			this.deferredActionStatusExpirationDate= VLPServiceUtil.xmlDateToString(additionalFieldsType.getDeferredActionStatusExpirationDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT); 
			this.expirationDate= VLPServiceUtil.xmlDateToString(additionalFieldsType.getExpirationDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);                            
			this.familyUnityExpirationDate= VLPServiceUtil.xmlDateToString(additionalFieldsType.getFamilyUnityExpirationDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);             
			this.paroleeExpirationDate= VLPServiceUtil.xmlDateToString(additionalFieldsType.getParoleeExpirationDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);                    
			this.admittedToDate= VLPServiceUtil.xmlDateToString(additionalFieldsType.getAdmittedToDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);                         
		}catch(Exception e){
    		LOGGER.error("Exception occured while setting date",e);
    	}
	}

	public String getCommentsToAgencyText() {
		return commentsToAgencyText;
	}

	public void setCommentsToAgencyText(String commentsToAgencyText) {
		this.commentsToAgencyText = commentsToAgencyText;
		this.additionalFieldsType.setCommentsToAgencyText(commentsToAgencyText);
	}

	public String getUscisBenefitsText() {
		return uscisBenefitsText;
	}

	public void setUscisBenefitsText(String uscisBenefitsText) {
		this.uscisBenefitsText = uscisBenefitsText;
		this.additionalFieldsType.setUscisBenefitsText(uscisBenefitsText);
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		try{
			this.expirationDate = expirationDate;
			this.additionalFieldsType.setExpirationDate(VLPServiceUtil.stringToXMLDate(expirationDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}catch(Exception e){
			LOGGER.error("Exception occured while setting DS 2019 Document Expiration date",e);
		}
	}

	public String getCoaCode() {
		return coaCode;
	}

	public void setCoaCode(String coaCode) {
		this.coaCode = coaCode;
		this.additionalFieldsType.setCoaCode(coaCode);
	}

	public String getDeferredActionStatusExpirationDate() {
		return deferredActionStatusExpirationDate;
	}

	public void setDeferredActionStatusExpirationDate(String deferredActionStatusExpirationDate) {
		
		try{
			this.deferredActionStatusExpirationDate = deferredActionStatusExpirationDate;
			this.additionalFieldsType.setDeferredActionStatusExpirationDate(VLPServiceUtil.stringToXMLDate(deferredActionStatusExpirationDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}catch(Exception e){
			LOGGER.error("Exception occured while setting DS 2019 Document Expiration date",e);
		}
	}

	public boolean isDeferredActionExpirationNAIndicator() {
		return deferredActionExpirationNAIndicator;
	}

	public void setDeferredActionExpirationNAIndicator(boolean deferredActionExpirationNAIndicator) {
		this.deferredActionExpirationNAIndicator = deferredActionExpirationNAIndicator;
		this.additionalFieldsType.setDeferredActionExpirationNAIndicator(deferredActionExpirationNAIndicator);
	}

	public String getFamilyUnityExpirationDate() {
		return familyUnityExpirationDate;
	}

	public void setFamilyUnityExpirationDate(String familyUnityExpirationDate) {
		try{
			this.familyUnityExpirationDate = familyUnityExpirationDate;
			this.additionalFieldsType.setFamilyUnityExpirationDate(VLPServiceUtil.stringToXMLDate(familyUnityExpirationDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}catch(Exception e){
			LOGGER.error("Exception occured while setting DS 2019 Document Expiration date",e);
		}
	}

	public String getParoleeExpirationDate() {
		return paroleeExpirationDate;
	}

	public void setParoleeExpirationDate(String paroleeExpirationDate) {
		try{
			this.paroleeExpirationDate = paroleeExpirationDate;
			this.additionalFieldsType.setParoleeExpirationDate(VLPServiceUtil.stringToXMLDate(paroleeExpirationDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}catch(Exception e){
			LOGGER.error("Exception occured while setting DS 2019 Document Expiration date",e);
		}
	}

	public String getAdmittedToDate() {
		return admittedToDate;
	}

	public void setAdmittedToDate(String admittedToDate) {
		this.admittedToDate = admittedToDate;
		try{
			this.additionalFieldsType.setAdmittedToDate(VLPServiceUtil.stringToXMLDate(admittedToDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}catch(Exception e){
			LOGGER.error("Exception occured while setting DS 2019 Document Expiration date",e);
		}
	}

	public boolean isDurationOfStatusIndicator() {
		return durationOfStatusIndicator;
	}

	public void setDurationOfStatusIndicator(boolean durationOfStatusIndicator) {
		this.durationOfStatusIndicator = durationOfStatusIndicator;
		this.additionalFieldsType.setDurationOfStatusIndicator(durationOfStatusIndicator);
	}

	public boolean isApprovedSelfPetitionIndicator() {
		return approvedSelfPetitionIndicator;
	}

	public void setApprovedSelfPetitionIndicator(boolean approvedSelfPetitionIndicator) {
		this.approvedSelfPetitionIndicator = approvedSelfPetitionIndicator;
		this.additionalFieldsType.setApprovedSelfPetitionIndicator(approvedSelfPetitionIndicator);
	}

	public boolean isI360ApplicationFiledIndicator() {
		return i360ApplicationFiledIndicator;
	}

	public void setI360ApplicationFiledIndicator(boolean i360ApplicationFiledIndicator) {
		this.i360ApplicationFiledIndicator = i360ApplicationFiledIndicator;
		this.additionalFieldsType.setI360ApplicationFiledIndicator(i360ApplicationFiledIndicator);
	}

	public boolean isPendingPrimaFacieSelfPetitionIndicator() {
		return pendingPrimaFacieSelfPetitionIndicator;
	}

	public void setPendingPrimaFacieSelfPetitionIndicator(boolean pendingPrimaFacieSelfPetitionIndicator) {
		this.pendingPrimaFacieSelfPetitionIndicator = pendingPrimaFacieSelfPetitionIndicator;
		this.additionalFieldsType.setPendingPrimaFacieSelfPetitionIndicator(pendingPrimaFacieSelfPetitionIndicator);
	}

	public AdditionalFieldsType getAdditionalFieldsType(){
		return this.additionalFieldsType;
	}
	

	public static AdditionalFieldsTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create AdditionalFieldsTypeWrapper from null or empty input");
		}

		Object tmpObj = null;
		
		AdditionalFieldsTypeWrapper wrapper = new AdditionalFieldsTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
	    tmpObj = obj.get(COMMENTSTOAGENCYTEXT);
		if(tmpObj != null){
			wrapper.commentsToAgencyText = (String)tmpObj;
			wrapper.additionalFieldsType.setCommentsToAgencyText(wrapper.commentsToAgencyText);
		}
		
		tmpObj = obj.get(USCISBENEFITSTEXT);
		if(tmpObj != null){
			wrapper.uscisBenefitsText = (String)tmpObj;
			wrapper.additionalFieldsType.setUscisBenefitsText(wrapper.uscisBenefitsText);
		}
		
		tmpObj = obj.get(DEFERREDACTIONSTATUSEXPIRATIONDATE);
		if(tmpObj != null){
			wrapper.deferredActionStatusExpirationDate = (String)tmpObj;
			try {
				wrapper.additionalFieldsType.setDeferredActionStatusExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.deferredActionStatusExpirationDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating DS2019DocumentID27Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		tmpObj = obj.get(FAMILYUNITYEXPIRATIONDATE);
		if(tmpObj != null){
			wrapper.familyUnityExpirationDate = (String)tmpObj;
			try {
				wrapper.additionalFieldsType.setFamilyUnityExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.familyUnityExpirationDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating DS2019DocumentID27Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		tmpObj = obj.get(PAROLEEEXPIRATIONDATE);
		if(tmpObj != null){
			wrapper.paroleeExpirationDate = (String)tmpObj;
			try {
				wrapper.additionalFieldsType.setParoleeExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.paroleeExpirationDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating DS2019DocumentID27Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		tmpObj = obj.get(ADMITTEDTODATE);
		if(tmpObj != null){
			wrapper.admittedToDate = (String)tmpObj;
			try {
				wrapper.additionalFieldsType.setAdmittedToDate(VLPServiceUtil.stringToXMLDate(wrapper.admittedToDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating DS2019DocumentID27Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		tmpObj = obj.get(DEFERREDACTIONEXPIRATIONNAINDICATOR);
		if(tmpObj != null){
			wrapper.deferredActionExpirationNAIndicator = (Boolean)tmpObj;
			wrapper.additionalFieldsType.setDeferredActionExpirationNAIndicator(wrapper.deferredActionExpirationNAIndicator);
		}
		
		tmpObj = obj.get(DURATIONOFSTATUSINDICATOR);
		if(tmpObj != null){
			wrapper.durationOfStatusIndicator = (Boolean)tmpObj;
			wrapper.additionalFieldsType.setDurationOfStatusIndicator(wrapper.durationOfStatusIndicator);
		}
		
		tmpObj = obj.get(APPROVEDSELFPETITIONINDICATOR);
		if(tmpObj != null){
			wrapper.approvedSelfPetitionIndicator = (Boolean)tmpObj;
			wrapper.additionalFieldsType.setApprovedSelfPetitionIndicator(wrapper.approvedSelfPetitionIndicator);
		}
		
		tmpObj = obj.get(I360APPLICATIONFILEDINDICATOR);
		if(tmpObj != null){
			wrapper.i360ApplicationFiledIndicator = (Boolean)tmpObj;
			wrapper.additionalFieldsType.setI360ApplicationFiledIndicator(wrapper.i360ApplicationFiledIndicator);
		}
		
		tmpObj = obj.get(PENDINGPRIMAFACIESELFPETITIONINDICATOR);
		if(tmpObj != null){
			wrapper.pendingPrimaFacieSelfPetitionIndicator = (Boolean)tmpObj;
			wrapper.additionalFieldsType.setPendingPrimaFacieSelfPetitionIndicator(wrapper.pendingPrimaFacieSelfPetitionIndicator);
		}
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(COMMENTSTOAGENCYTEXT, this.commentsToAgencyText);
		obj.put(USCISBENEFITSTEXT, this.uscisBenefitsText);
		obj.put(EXPIRATIONDATE, this.expirationDate);
		obj.put(COACODE, this.coaCode);
		obj.put(DEFERREDACTIONSTATUSEXPIRATIONDATE, this.deferredActionStatusExpirationDate);
		obj.put(DEFERREDACTIONEXPIRATIONNAINDICATOR, this.deferredActionExpirationNAIndicator);
		obj.put(FAMILYUNITYEXPIRATIONDATE, this.familyUnityExpirationDate);
		obj.put(PAROLEEEXPIRATIONDATE, this.paroleeExpirationDate);
		obj.put(ADMITTEDTODATE, this.admittedToDate);
		obj.put(DURATIONOFSTATUSINDICATOR, this.durationOfStatusIndicator);
		obj.put(APPROVEDSELFPETITIONINDICATOR, this.approvedSelfPetitionIndicator);
		obj.put(I360APPLICATIONFILEDINDICATOR, this.i360ApplicationFiledIndicator);
		obj.put(PENDINGPRIMAFACIESELFPETITIONINDICATOR, this.pendingPrimaFacieSelfPetitionIndicator);


		return obj.toJSONString();
	}
	
	public static AdditionalFieldsTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		AdditionalFieldsTypeWrapper wrapper = new AdditionalFieldsTypeWrapper();
		wrapper.setCommentsToAgencyText((String) jsonObj.get(COMMENTSTOAGENCYTEXT));
		wrapper.setUscisBenefitsText((String) jsonObj.get(USCISBENEFITSTEXT));
		//TODO: add all fields
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(COMMENTSTOAGENCYTEXT, this.commentsToAgencyText);
		obj.put(USCISBENEFITSTEXT, this.uscisBenefitsText);
		obj.put(EXPIRATIONDATE, this.expirationDate);
		obj.put(COACODE, this.coaCode);
		obj.put(DEFERREDACTIONSTATUSEXPIRATIONDATE, this.deferredActionStatusExpirationDate);
		obj.put(DEFERREDACTIONEXPIRATIONNAINDICATOR, this.deferredActionExpirationNAIndicator);
		obj.put(FAMILYUNITYEXPIRATIONDATE, this.familyUnityExpirationDate);
		obj.put(PAROLEEEXPIRATIONDATE, this.paroleeExpirationDate);
		obj.put(ADMITTEDTODATE, this.admittedToDate);
		obj.put(DURATIONOFSTATUSINDICATOR, this.durationOfStatusIndicator);
		obj.put(APPROVEDSELFPETITIONINDICATOR, this.approvedSelfPetitionIndicator);
		obj.put(I360APPLICATIONFILEDINDICATOR, this.i360ApplicationFiledIndicator);
		obj.put(PENDINGPRIMAFACIESELFPETITIONINDICATOR, this.pendingPrimaFacieSelfPetitionIndicator);

		return obj;
	}
}
