import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from '@cmsgov/design-system-core';
import { Field, reduxForm, SubmissionError, change } from 'redux-form';
import { FormattedMessage } from 'react-intl';
import { withRouter } from 'react-router';

import { Checkbox } from '../components/form';
import PhoneNumber from '../components/PhoneNumber';
import phoneNumberUtils from '../utils/phoneNumberUtils';
import ENDPOINTS from '../constants/endPointsList';
import { TERM_AND_CONDITIONS_LINK } from '../constants/DefaultData';
import { validatePhoneNumber } from '../utils/validators';

import {
  fetchParticipationDetails,
  submitParticipationDetails,
  submitAvailability
} from '../actions/actionCreators';
import {
  BROKER_ACTIVE,
  BROKER_INACTIVE
} from '../constants/AvailabilityConstants';

const BrokerParticipation = props => {
  const { actions, data, available } = props;

  useEffect(() => {
    let brokerParticipation = document.getElementById('brkHelpOnDemand');
    let brokerAvailability = document.getElementById('brkConnect');
    brokerParticipation.classList.add('active');
    brokerAvailability.classList.remove('active');
    actions.fetchParticipationDetails(ENDPOINTS.PARTICIPATION_DETAILS_ENDPOINT);
  }, []);

  const enrollUnenrollCheckbox =
    data.status === BROKER_ACTIVE ? (
      <FormattedMessage
        id="broker.participation.unenrollText"
        defaultMessage="Remove participation from broker connect program"
      />
    ) : (
      <div>
        <FormattedMessage
          id="broker.ondemand.agreeToTermsText"
          defaultMessage="I agree to "
        />
        <a
          id="HOD_terms_and_conditions_link"
          className="ds-u-text-decoration--underline"
          href={TERM_AND_CONDITIONS_LINK}
          rel="noopener noreferrer"
          target="_blank"
        >
          <FormattedMessage
            id="broker.ondemand.agreeToTermsLink"
            defaultMessage="Broker Connect Terms and Conditions"
          />
        </a>
      </div>
    );

  const enrollUnenrollButtonText =
    data.status === BROKER_ACTIVE ? (
      <FormattedMessage
        id="broker.participation.button.submit.unenroll"
        defaultMessage="Remove Participation"
      />
    ) : (
      <FormattedMessage
        id="broker.participation.button.submit.enroll"
        defaultMessage="Confirm Participation"
      />
    );

  const onSubmit = values => {
    console.log('Submit info');
    console.log(JSON.stringify(values, null, 2));

    let status = data.status;
    let errors = {};
    let history;
    if (values.enrollmentChoiceCheckbox === true) {
      if (status === BROKER_ACTIVE) {
        status = BROKER_INACTIVE;
      } else if (status === BROKER_INACTIVE) {
        status = BROKER_ACTIVE;
        history = props.history; // If becoming active then pass history to allow redirect to Connect Hours
      }
    } else if (!values.enrollmentChoiceCheckbox && status === BROKER_INACTIVE) {
      errors.enrollmentChoiceCheckbox = (
        <FormattedMessage
          id="broker.participation.submitError.terms"
          defaultMessage="You must agree to the terms"
        />
      );
      errors.brokerConnectPhone = validatePhoneNumber(
        values.brokerConnectPhone
      );
      throw new SubmissionError(errors);
    } else if (!values.enrollmentChoiceCheckbox && status === BROKER_ACTIVE) {
      errors.enrollmentChoiceCheckbox = (
        <FormattedMessage
          id="broker.participation.submitError.unenroll"
          defaultMessage="You must accept removal"
        />
      );
      throw new SubmissionError(errors);
    }

    let body = {
      available,
      phoneNumber: phoneNumberUtils.cleanedUpPhone(values.brokerConnectPhone),
      status
    };
    props.change('enrollmentChoiceCheckbox', false);
    actions.submitParticipationDetails(
      ENDPOINTS.PARTICIPATION_DETAILS_ENDPOINT,
      body,
      history
    );
  };

  return (
    <Fragment>
      <div className="ds-l-row header">
        <div className="ds-l-xl-col--10 ds-l-lg-col--10 ds-l-md-col--7 ds-l-sm-col--9 ds-l-xs-col">
          <h4>
            <FormattedMessage
              id="broker.participation.header"
              defaultMessage="Participation Information"
            />
          </h4>
        </div>
      </div>
      <div className="ds-l-row">
        <div className="ds-l-col ds-u-text-align--left">
          <p className="gutter10">
            {data.status === BROKER_INACTIVE ? (
              <FormattedMessage
                id="broker.participation.enroll.info"
                defaultMessage="Broker Connect is a program where brokers can join to receive leads for consumers that need help with completing their health insurance application or shop for a plan."
              />
            ) : (
              <FormattedMessage
                id="broker.participation.unenroll.info"
                defaultMessage="You are currently in the broker connect program. By clicking the button below your participation will be removed from this program with immediate effect."
              />
            )}
          </p>
        </div>
      </div>
      <form onSubmit={props.handleSubmit(onSubmit)}>
        {data.status === BROKER_INACTIVE ? <PhoneNumber /> : ''}
        <Field
          className="checkbox-1"
          component={Checkbox}
          name="enrollmentChoiceCheckbox"
          id="enrollmentChoiceCheckbox"
          label={enrollUnenrollCheckbox}
        />
        <div className="ds-l-row">
          <div className="ds-l-col">
            <div className="ds-l-row ds-u-justify-content--end">
              <Button
                className="nv-button-primary ds-u-margin--3"
                variation="primary"
                id="confirm_participate"
                type="submit"
              >
                {enrollUnenrollButtonText}
              </Button>
            </div>
          </div>
        </div>
      </form>
    </Fragment>
  );
};

const validate = values => {
  let errors = {};

  errors.brokerConnectPhone = validatePhoneNumber(values.brokerConnectPhone);
  //console.log('Validate errors: ' + JSON.stringify(errors));
  return errors;
};

const mapStateToProps = state => {
  //console.log('Map State: ' + JSON.stringify(state));

  return {
    data: state.onDemandReducer,
    available:
      state.brokerAvailabilityReducer.available ||
      state.onDemandReducer.isAvailable,
    initialValues: {
      brokerConnectPhone: phoneNumberUtils.normalizePhone(
        state.onDemandReducer.phoneNumber,
        ''
      ),
      enrollmentChoiceCheckbox: false
    }
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      fetchParticipationDetails,
      submitParticipationDetails,
      submitAvailability,
      change
    },
    dispatch
  )
});

const enableReinitialize = true;

const formWrapped = reduxForm({
  form: 'ParticipationDetails',
  validate,
  enableReinitialize
})(BrokerParticipation);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(formWrapped)
);
