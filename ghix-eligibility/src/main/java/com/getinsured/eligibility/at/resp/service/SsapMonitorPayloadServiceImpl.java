package com.getinsured.eligibility.at.resp.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.iex.ssap.model.SsapMonitorPayload;
import com.getinsured.iex.ssap.repository.SsapMonitorPayloadRepository;

@Service
@DependsOn("dynamicPropertiesUtil")
@Transactional
public class SsapMonitorPayloadServiceImpl implements SsapMonitorPayloadService {


	private static final Logger LOGGER = Logger.getLogger(SsapMonitorPayloadServiceImpl.class);
	
	@Autowired
	private SsapMonitorPayloadRepository ssapMonitorPayloadRepository;

	
	@Override
	@Transactional
	public SsapMonitorPayload save(SsapMonitorPayload ssapMonitorPayload) {
		if (LOGGER.isDebugEnabled()){
			LOGGER.debug("Saving/Updating SsapMonitorPayload object - "+ ssapMonitorPayload);
		}
		return ssapMonitorPayloadRepository.save(ssapMonitorPayload);
	}
	
	@Override
	public SsapMonitorPayload findById(Integer id){
		return ssapMonitorPayloadRepository.findById(id);
	}

	@Override
	public List<SsapMonitorPayload> findBySSAPID(Long ssapId) {
		return ssapMonitorPayloadRepository.findBySsapApplicationId(ssapId);
	}

}
