package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanDentalBenefit;

public interface IPlanDentalBenefitRepository extends JpaRepository<PlanDentalBenefit, Integer> {
	
	@Query("SELECT phb from PlanDentalBenefit phb WHERE phb.plan.id = :planId")
	List<PlanDentalBenefit> getPlanDentalBenefit(@Param("planId") Integer planId);
	
	@Query("SELECT pb.networkLimitation, pb.networkLimitationAttribute, pb.networkExceptions, " +
			"pb.networkT1CopayVal, pb.networkT1CopayAttr, pb.networkT1CoinsurVal, pb.networkT1CoinsurAttr, " +
			"pb.networkT2CopayVal, pb.networkT2CopayAttr, pb.networkT2CoinsurVal, pb.networkT2CoinsurAttr, " +
			"pb.outOfNetworkCopayVal, pb.outOfNetworkCopayAttr, pb.outOfNetworkCoinsurVal, pb.outOfNetworkCoinsurAttr, pb.name " +
			"from PlanDentalBenefit pb WHERE (pb.plan.id = :planId) and (TO_DATE (:effectiveDate, 'YYYY-MM-DD') BETWEEN pb.effStartDate AND pb.effEndDate)")
	List<Object[]> getPlanDentalBenefitTest(@Param("planId") Integer planId, @Param("effectiveDate") String effectiveDate);

}