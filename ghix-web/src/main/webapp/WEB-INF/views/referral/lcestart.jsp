<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript"
	src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>


	<style type="text/css">
.square,.well-step {
	border-radius: 0px !important;
}

.checkboxlce {
	margin: 4px !important;
}

.nomargindate {
	border: 1px solid #ddd;
	margin: -21px 0px 0px 0px;
}

.eligibility-wrapper #sidebar li:before {
	content: none;
}

ul.nav.nav-list>li {
	margin: 0px !important;
}

.square,.well-step {
	border-radius: 0px !important;
}

.checkboxlce,.checkboxlcenone {
	margin: 4px !important;
}

.nomargindate {
	border: 1px solid #ddd;
	margin: -21px 0px 0px 0px;
}

h4.square {
	border: 1px solid #ddd !important;
}

.lceeventdate0 {
	border-top: 0px;
	/*position: relative;
  	top: 10px;*/
}
h4.square {
	margin-top: 0px;
}
</style>
<div class="gutter10">
	<div class="gutter10">
		<div id="titlebar">
				<h1>Life Change Events</h1>
			</div>
		<div class="row-fluid margin30-t eligibility-wrapper"
			data-original-title="">
			<div id="sidebar" class="span3">
				<div class="header">
					<h4>
						<spring:message code="label.referral.sideheader" />
					</h4>
				</div>
				<ul class="nav nav-list">
					<li><spring:message code="label.referral.securityquestions" />
					</li>
					<li class="active"><spring:message code="label.referral.lce" />
					</li>
					<li><spring:message code="label.referral.linktoapplication" />
					</li>
				</ul>
			</div>
			<!-- ngView:  -->
			<div class="span9" id="rightpanel">
				<div class="gutter10 center-text">
				<p><spring:message code="label.referral.lce.message" /></p>
			</div>
			</div>
		</div>
	</div>

</div>