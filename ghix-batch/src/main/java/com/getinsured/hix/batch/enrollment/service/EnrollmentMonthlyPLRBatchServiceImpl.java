package com.getinsured.hix.batch.enrollment.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import com.getinsured.hix.batch.enrollment.skip.EnrollmentPLROut;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDTO.XMLValidationStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDetails;
import com.getinsured.hix.dto.enrollment.EnrollmentXMLValidationDetails.ValidationStatus;
import com.getinsured.hix.enrollment.repository.IEnrollment1095Repository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPLRExecutionRepository;
import com.getinsured.hix.enrollment.repository.IPLROutboundRepository;
import com.getinsured.hix.enrollment.service.EnrollmentServiceImpl;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentIrsEscapeHandler;
import com.getinsured.hix.enrollment.util.EnrollmentIrsSaxErrorHandler;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.NullCharacterEscapeHandler;
import com.getinsured.hix.enrollment.util.ResourceResolver;
import com.getinsured.hix.enrollment.util.ZipHelper;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.Attachment;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095;
import com.getinsured.hix.model.enrollment.EnrollmentOutPlr;
import com.getinsured.hix.model.enrollment.EnrollmentPLRExecution;
import com.getinsured.hix.model.enrollment.EnrollmentPremium1095;
import com.getinsured.hix.model.enrollment.FilePayloadBatchTransmission;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.JiraUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



import us.gov.treasury.irs.plr.common.AssociatedPolicyType;
import us.gov.treasury.irs.plr.common.BooleanStringType;
import us.gov.treasury.irs.plr.common.CompletePersonNameType;
import us.gov.treasury.irs.plr.common.CoveredIndividualType;
import us.gov.treasury.irs.plr.common.DependentGrpType;
import us.gov.treasury.irs.plr.common.DependentPersonType;
import us.gov.treasury.irs.plr.common.EPDPersonType;
import us.gov.treasury.irs.plr.common.HouseholdType;
import us.gov.treasury.irs.plr.common.IRSHouseholdGrpType;
import us.gov.treasury.irs.plr.common.IndividualExchangeType;
import us.gov.treasury.irs.plr.common.InsuranceCoverageType;
import us.gov.treasury.irs.plr.common.InsurancePolicyType;
import us.gov.treasury.irs.plr.common.PersonAddressGrpType;
import us.gov.treasury.irs.plr.common.PersonInformationType;
import us.gov.treasury.irs.plr.common.PrimaryGrpType;
import us.gov.treasury.irs.plr.common.SpouseGrpType;
import us.gov.treasury.irs.plr.common.StateType;
import us.gov.treasury.irs.plr.common.TaxHouseholdCoverageType;
import us.gov.treasury.irs.plr.common.TaxHouseholdType;
import us.gov.treasury.irs.plr.common.USAddressGrpType;
import us.gov.treasury.irs.plr.msg.sbmpolicylevelenrollment.HealthExchangeType;
import us.gov.treasury.irs.plr.msg.sbmpolicylevelenrollment.ObjectFactory;

@Service("enrollmentMonthlyPLRBatchService")
@Transactional
public class EnrollmentMonthlyPLRBatchServiceImpl implements EnrollmentMonthlyPLRBatchService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPLRBatchServiceImpl.class);

	@Autowired private IEnrollment1095Repository enrollment1095Repository;
	@Autowired private IEnrollmentPLRExecutionRepository enrollmentPLRExecutionRepository;
	@Autowired private IPLROutboundRepository plrOutboundRepo;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private EnrollmentPLROut enrollmentPLROut;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	
	@Override
	public List<String> getUniqueHouseHolds(int month, int year) throws GIException{
		Date currentMonthStartDate = null;
		List<String> houseHoldIdList = null; 
		Date yearStartDate = null;
		int intMonth = month;
		int intYear = year;
		try {
			currentMonthStartDate = EnrollmentUtils.getMonthStartDateTime(intMonth,intYear);
		} catch (GIException e) {
			throw new GIException("Error getting currentMonthStartDate", e);
		}
		//To handle boundary condition when month passed is January
		int applicableYear = intYear;
		if(intMonth == 0){
			applicableYear--;
		}
		yearStartDate = EnrollmentUtils.getCurrentYearStartDate(applicableYear);


		if(EnrollmentUtils.isNotNullAndEmpty(currentMonthStartDate) && EnrollmentUtils.isNotNullAndEmpty(yearStartDate)){
			houseHoldIdList = enrollment1095Repository.getHouseholdIdsFromStaging(
					 DateUtil.dateToString(yearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(currentMonthStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		}	
		return houseHoldIdList;
	}

	@Override
	public void processPLRHousehold(List<String> householdCaseIdList, int applicableMonth, int applicableYear, int partition) throws GIException{
		if(householdCaseIdList!=null && !householdCaseIdList.isEmpty()){
			String fileName="";
			Map<String, List<Integer>> household1095IdsMap= new HashMap<>();
			try{

				HealthExchangeType healthExchangeTypeObj = new HealthExchangeType();
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				healthExchangeTypeObj.setSubmissionMonthNum(cal.get(Calendar.MONTH)+1);
				healthExchangeTypeObj.setSubmissionYr(getSubmissionYear(cal.get(Calendar.YEAR)));
				int intSubmissionMonth = applicableMonth + 1;

				if(intSubmissionMonth == 1){					
					healthExchangeTypeObj.setApplicableCoverageYr(getSubmissionYear(applicableYear-1));
				}
				else{

					healthExchangeTypeObj.setApplicableCoverageYr(getSubmissionYear(applicableYear));

				}
				healthExchangeTypeObj.setIndividualExchange(getIndividualExchange(householdCaseIdList, applicableMonth, applicableYear,  household1095IdsMap));
				fileName=generatePLRXml(healthExchangeTypeObj, partition);
			}catch(Exception e){
				String msg=EnrollmentUtils.shortenedStackTrace(e, 3);
				for (String householdId: householdCaseIdList){
					enrollmentPLROut.putToSkippedHouseholdMap(householdId,msg );
				}

			}finally{
				try{
					logPLRSkipSuccessDetails(householdCaseIdList,  applicableMonth, applicableYear, fileName, household1095IdsMap);
				}catch(Exception e){
					//Log msg :TODO
				}
			}

		}

	}

	List<TaxHouseholdCoverageType> getMonthWiseTaxHouseholdAndInsurance(String householdCaseId,int applicableMonth, int applicableYear, Date yearStartDate, Date currentMonthStartDate, TaxHouseholdType taxHousehold, List<InsurancePolicyType> houseHoldInsurancePolicyList, Map<String, List<Integer>> household1095IdsMap) throws GIException{

		List<TaxHouseholdCoverageType> taxCovList=taxHousehold.getTaxHouseholdCoverage();
		//List<InsuranceCoverageType> insuranceCoverageList =houseHoldInsurancePolicy.getInsuranceCoverage();

		List<Enrollment1095> enrollmentList=null;
		enrollmentList= (ArrayList<Enrollment1095>) enrollment1095Repository.getByHouseholdCaseId(householdCaseId,  
				DateUtil.dateToString(yearStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY), DateUtil.dateToString(currentMonthStartDate, EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY));
		if(enrollmentList!=null && enrollmentList.size()>0){

			getInsuranceCoverageList(enrollmentList, houseHoldInsurancePolicyList, applicableMonth, applicableYear, household1095IdsMap);

			for(int month = 0; month < applicableMonth; month++){
				Date endDate = EnrollmentUtils.getStartDateForNextMonthIndividualReport(month,applicableYear);
				Date startDate = new Date();
				try {
					startDate = EnrollmentUtils.getMonthStartDateTime(month, applicableYear);
				} catch (GIException e) {
					LOGGER.debug("Could not get start date", e);
				}
				List<Enrollment1095> activeEnrollments=getActiveEnrollments(enrollmentList, startDate, endDate);
				if(activeEnrollments!=null && activeEnrollments.size()>0){
					TaxHouseholdCoverageType taxHouseholdCoverage= new TaxHouseholdCoverageType();
					getHouseholdOrRelAdult(taxHouseholdCoverage, activeEnrollments, startDate, endDate, month);

					taxCovList.add(taxHouseholdCoverage);

				}
			}
		}


		return taxCovList;
	}

	private void getInsuranceCoverageList(List<Enrollment1095> enrollmentList, List<InsurancePolicyType> insurancePolicyList, int applicableMonth,int applicableYear, Map<String, List<Integer>> household1095IdsMap){
		//sort enrollments in ascending
		Collections.sort(enrollmentList, new Comparator<Enrollment1095>() {
			@Override
			public int compare(Enrollment1095 o1, Enrollment1095 o2) {
				Date v1 = (o1.getPolicyStartDate());
				Date v2 = (o2.getPolicyStartDate());
				if(v1.equals(v2) ) {
					return 0;
				}
				if(v1.before(v2)) {
					return -1;  
				}
				return 1;
			}
		});
		for(Enrollment1095 enr: enrollmentList){

			List<InsuranceCoverageType> coverageList= new ArrayList<>();

			List<Integer> enrollment1095Ids=household1095IdsMap.get(enr.getHouseHoldCaseId());
			if(enrollment1095Ids==null ){
				enrollment1095Ids= new ArrayList<>();
				enrollment1095Ids.add(enr.getId());

			}else if(!enrollment1095Ids.contains(enr.getId())){
				enrollment1095Ids.add(enr.getId());
			}
			household1095IdsMap.put(enr.getHouseHoldCaseId(), enrollment1095Ids);

			for(int month = 0; month <applicableMonth; month++){

				Date endDate = EnrollmentUtils.getStartDateForNextMonthIndividualReport(month,applicableYear);
				Date startDate = new Date();
				try {
					startDate = EnrollmentUtils.getMonthStartDateTime(month, applicableYear);
				} catch (GIException e) {
					LOGGER.debug("Could not get start date", e);
				}
				List<CoveredIndividualType> coveredIndividualType= getCoveredIndividual( getTypedMemberFromEnrollment(enr, EnrollmentMember1095.MemberType.MEMBER, startDate, endDate), startDate, endDate);
				if(enr.getPolicyStartDate().before(endDate) && !DateUtils.isSameDay(enr.getPolicyStartDate(), endDate) && (enr.getPolicyEndDate().compareTo(startDate)>=0 || DateUtils.isSameDay(enr.getPolicyEndDate(), startDate))){
					EnrollmentPremium1095 pre = enr.getEnrollmentPremium1095ForMonth(month+1);
					if(null != pre && pre.getMonthNumber().equals(month+1)){
							InsuranceCoverageType insCoverage= new InsuranceCoverageType();
						boolean isFinancial = false;
						boolean showInsuranceCoverage = true;
							insCoverage.getCoveredIndividual().addAll(coveredIndividualType);
							insCoverage.setApplicableCoverageMonthNum(month+1);
							insCoverage.setAPTCPaymentAmt(getBigDecimalFromFloat(pre.getAptcAmount()!=null ?pre.getAptcAmount(): 0.0f));
							insCoverage.setIssuerNm(enr.getPolicyIssuerName());
						//							insCoverage.setPediatricDentalPlanPremiumInd(getBooleanStringType(Boolean.FALSE));
							insCoverage.setPolicyCoverageEndDt(getXmlGregorianCalendarDate(enr.getPolicyEndDate()));
							insCoverage.setPolicyCoverageStartDt(getXmlGregorianCalendarDate(enr.getPolicyStartDate()));
						insCoverage.setQHPId(enr.getCmsPlanId());
							insCoverage.setQHPIssuerEIN(EnrollmentUtils.removeSpecialCharacters(enr.getIssuerEin()));
							insCoverage.setQHPPolicyNum(String.format("%06d", enr.getExchgAsignedPolicyId()));
						if(null == pre.getAptcAmount()){
							insCoverage.setSLCSPMonthlyPremiumAmt(getBigDecimalFromFloat(pre.getSlcspAmount()!=null ? pre.getSlcspAmount(): 1.0f));
						}else{
							isFinancial = true;
						}
							Float monthlyPremiumAmount = null;
							if(pre.getGrossPremium()!=null && pre.getEhbPercent()!=null){
								monthlyPremiumAmount=0.0f;
								monthlyPremiumAmount=pre.getGrossPremium()*pre.getEhbPercent();
							}
							if(pre.getPediatricEhbAmt()!=null && pre.getAccountableMemberDental()!=null){
								if(monthlyPremiumAmount==null){
									monthlyPremiumAmount=0.0f;
								}
								if(enr.getCoverageYear() >= 2017){
									monthlyPremiumAmount+=pre.getPediatricEhbAmt();
								}else{
								monthlyPremiumAmount+=(pre.getPediatricEhbAmt()*pre.getAccountableMemberDental());
							}
							}
							if(monthlyPremiumAmount!=null){
								insCoverage.setTotalQHPMonthlyPremiumAmt(getBigDecimalFromFloat(monthlyPremiumAmount));
							}
						//Check if overlap exists and remove NF application if overlap with F application
						for(Enrollment1095 enrollment1095 : enrollmentList){
							if(enrollment1095.getId() != enr.getId() && null != enrollment1095.getEnrollmentPremium1095ForMonth(month+1) && null != enrollment1095.getEnrollmentPremium1095ForMonth(month + 1).getGrossPremium()){
								showInsuranceCoverage = isFinancial || (null == enrollment1095.getEnrollmentPremium1095ForMonth(month+1).getAptcAmount());
							}
						}
						int occurence = enrollmentList.indexOf(enr);
						for(int i = 0; i<occurence; i++){
							Enrollment1095 enrollment1095 = enrollmentList.get(i);
							if (null != enrollment1095.getEnrollmentPremium1095ForMonth(month + 1)
									&& null != enrollment1095.getEnrollmentPremium1095ForMonth(month + 1).getGrossPremium()
									&& ((isFinancial && null != enrollment1095.getEnrollmentPremium1095ForMonth(month + 1).getAptcAmount())
											|| (!isFinancial && null == enrollment1095.getEnrollmentPremium1095ForMonth(month + 1).getAptcAmount()))) {
								showInsuranceCoverage = false;
							break;
						}
					}
						if(showInsuranceCoverage){
							coverageList.add(insCoverage);
						}
					}
				}

			}
			if(coverageList!=null && coverageList.size()>0){
				InsurancePolicyType insurancePolicy= new InsurancePolicyType();
				insurancePolicy.getInsuranceCoverage().addAll(coverageList);
				insurancePolicyList.add(insurancePolicy);
			}
		}
	}

	private List<CoveredIndividualType> getCoveredIndividual( List<EnrollmentMember1095> members, Date startDate, Date endDate){
		List<CoveredIndividualType> coveredIndividualList= new ArrayList<>();
		if(members!=null && members.size()>0){
			for(EnrollmentMember1095 mem: members){
				if( 
						mem.getCoverageStartDate().before(endDate) && !DateUtils.isSameDay(mem.getCoverageStartDate(), endDate) && (mem.getCoverageEndDate().compareTo(startDate)>=0 || DateUtils.isSameDay(mem.getCoverageEndDate(), startDate))
						){
					CoveredIndividualType coveredIndividual= new CoveredIndividualType();
					if(mem.getCoverageEndDate()!=null){
						coveredIndividual.setCoverageEndDt(getXmlGregorianCalendarDate(mem.getCoverageEndDate()));
					}
					if(mem.getCoverageStartDate()!=null){
						coveredIndividual.setCoverageStartDt(getXmlGregorianCalendarDate(mem.getCoverageStartDate()));
					}
					EPDPersonType person= new EPDPersonType();
					if(mem.getBirthDate()!=null){
						person.setBirthDt(getXmlGregorianCalendarDate(mem.getBirthDate()));
					}

					CompletePersonNameType personName= new CompletePersonNameType();
					personName.setPersonFirstName(mem.getFirstName());
					personName.setPersonLastName(mem.getLastName());
					personName.setPersonMiddleName(mem.getMiddleName());
					personName.setSuffixName(mem.getNameSuffix());
					person.setCompletePersonName(personName);
					person.setSSN(mem.getSsn());
					coveredIndividual.setInsuredPerson(person);
					coveredIndividualList.add(coveredIndividual);
				}

			}
		}
		return coveredIndividualList;
	}

	private void getHouseholdOrRelAdult(TaxHouseholdCoverageType houseHoldCov, List<Enrollment1095> activeEnrollments,Date startDate,Date endDate, int month){
		if(activeEnrollments!=null && activeEnrollments.size()>0){
			HouseholdType household= new HouseholdType();
			//sort enrollments in ascending order
			Collections.sort(activeEnrollments, new Comparator<Enrollment1095>() {
				@Override
				public int compare(Enrollment1095 o1, Enrollment1095 o2) {
					Date v1 = (o1.getPolicyStartDate());
					Date v2 = (o2.getPolicyStartDate());
					if(v1.equals(v2) ) {
						return 0;
					}
					if(v1.before(v2)) {
					return -1;
				}
					return 1;
				}
			});
//			EnrollmentMember1095 responsible= getTypedMemberFromEnrollment(activeEnrollments.get(0), EnrollmentMember1095.MemberType.RECEPIENT, startDate, endDate).get(0);
			Enrollment1095 enrollment1095 = getAssociatedPolicyType(activeEnrollments, household, month);
			EnrollmentMember1095 responsible= getTypedMemberFromEnrollment(enrollment1095, EnrollmentMember1095.MemberType.RECEPIENT, startDate, endDate).get(0);
			EnrollmentPremium1095 pre = enrollment1095.getEnrollmentPremium1095ForMonth(month+1);
			boolean hasFinancial=null != pre && pre.getMonthNumber().equals(month+1) && pre.getAptcAmount()!=null;
			houseHoldCov.setApplicableCoverageMonthNum(month+1);
			if(hasFinancial){
				EnrollmentMember1095 spouse=findSpouse(activeEnrollments, startDate, endDate);
				List<String> spouseRespnsibleList=new ArrayList<String>();
				if(responsible!=null){
					spouseRespnsibleList.add(responsible.getMemberId()+"");

				}
				if(spouse!=null){
					spouseRespnsibleList.add(spouse.getMemberId()+"");
				}
				getPrimaryGroup(responsible, household);
				getSpouseGroup(spouse, household);
				List<EnrollmentMember1095> dependents= findDependents(activeEnrollments, spouseRespnsibleList, startDate, endDate);
				getDependentGroup(dependents, household);
				houseHoldCov.setHousehold(household);
			}else{
				//set Other 
				houseHoldCov.setOtherRelevantAdult(getPersonInfo(responsible));
			}

		}
	}

	private Enrollment1095 getAssociatedPolicyType(List<Enrollment1095> activeEnrollments,HouseholdType household, int month ){ 
		boolean hasFinancial=false;
		Enrollment1095 applicableEnrollment = null;
		//is Financial Flow
		if(activeEnrollments!=null && activeEnrollments.size()>0 && household!=null){
			//			List<AssociatedPolicyType> associatedHouseholdPolicies= new ArrayList<>();
			applicableEnrollment = activeEnrollments.get(0);
			AssociatedPolicyType associatedPolicyType= null;
			for(Enrollment1095 enrollment: activeEnrollments){
				associatedPolicyType= new AssociatedPolicyType();
				//				associatedPolicyType.setPediatricDentalPlanPremiumInd(getBooleanStringType(Boolean.FALSE));
				associatedPolicyType.setQHPIssuerEIN(EnrollmentUtils.removeSpecialCharacters(enrollment.getIssuerEin()));
				associatedPolicyType.setQHPPolicyNum(String.format("%06d", enrollment.getExchgAsignedPolicyId()));
				EnrollmentPremium1095 pre = enrollment.getEnrollmentPremium1095ForMonth(month+1);
				if(null != pre && pre.getMonthNumber().equals(month+1) && pre.getAptcAmount()!=null){
							hasFinancial=true;
						associatedPolicyType.setSLCSPAdjMonthlyPremiumAmt(getBigDecimalFromFloat(pre.getSlcspAmount()!=null ? pre.getSlcspAmount(): 1.0f));
						associatedPolicyType.setHouseholdAPTCAmt(getBigDecimalFromFloat(pre.getAptcAmount()));
						Float monthlyPremiumAmount = null;
						if(pre.getGrossPremium()!=null && pre.getEhbPercent()!=null){
							monthlyPremiumAmount=0.0f;
							monthlyPremiumAmount=pre.getGrossPremium()*pre.getEhbPercent();
						}
						if(pre.getPediatricEhbAmt()!=null && pre.getAccountableMemberDental()!=null){
							if(monthlyPremiumAmount==null){
								monthlyPremiumAmount=0.0f;
							}
							if(enrollment.getCoverageYear() >= 2017){
								monthlyPremiumAmount+=pre.getPediatricEhbAmt();
							}else{
								monthlyPremiumAmount+=(pre.getPediatricEhbAmt()*pre.getAccountableMemberDental());
							}
						}
						if(monthlyPremiumAmount!=null){
							associatedPolicyType.setTotalHsldMonthlyPremiumAmt(getBigDecimalFromFloat(monthlyPremiumAmount));
						}
					/*	if(null == mainPolicy){
						mainPolicy = associatedPolicyType;
					}*/
					/*if(pre.getAptcAmount() != null){
						associatedHouseholdPolicies.add(associatedPolicyType);
						}*/
					applicableEnrollment = enrollment;
						break;
					}
			}
			if(hasFinancial && null != associatedPolicyType){
				household.getAssociatedPolicy().add(associatedPolicyType);
			}
		}
		return applicableEnrollment;
	}

	private void getSpouseGroup(EnrollmentMember1095 spouse, HouseholdType household){
		if(spouse!=null && household!=null){
			SpouseGrpType spouseGrp= new SpouseGrpType();
			spouseGrp.setSpouse(getEPDPersonType(spouse));
			household.setSpouseGrp(spouseGrp);	
		}



	}

	private void getDependentGroup(List<EnrollmentMember1095> dependents, HouseholdType household){
		if(dependents!=null && dependents.size()>0 && household!=null ){
			List<DependentGrpType> personGrpList= new ArrayList<>();
			for(EnrollmentMember1095 member: dependents){
				DependentPersonType person= getDependentPersonType(member);
				if(person!=null){
					DependentGrpType dependentGroup= new DependentGrpType();
					dependentGroup.setDependentPerson(person);
					personGrpList.add(dependentGroup);
				}

			}
			if(personGrpList.size()>0){
				household.getDependentGrp().addAll(personGrpList);
			}
		}



	}

	private void getPrimaryGroup(EnrollmentMember1095 recepient, HouseholdType household){
		if(recepient!=null && household!=null){
			PrimaryGrpType primaryGrp= new PrimaryGrpType();
			primaryGrp.setPrimary(getPersonInfo(recepient));
			household.setPrimaryGrp(primaryGrp);
		}
	}

	private DependentPersonType getDependentPersonType(EnrollmentMember1095 member){
		DependentPersonType personInfo=null;
		if(member!=null){
			personInfo= new DependentPersonType();
			CompletePersonNameType personName= new CompletePersonNameType();

			personName.setPersonFirstName(member.getFirstName());
			personName.setPersonLastName(member.getLastName());
			personName.setPersonMiddleName(member.getMiddleName());
			personName.setSuffixName(member.getNameSuffix());

			personInfo.setCompletePersonName(personName);
			if(member.getBirthDate()!=null){
				personInfo.setBirthDt(getXmlGregorianCalendarDate(member.getBirthDate()));
			}
			personInfo.setSSN(member.getSsn());

		}
		return personInfo;
	}


	private EPDPersonType getEPDPersonType(EnrollmentMember1095 member){
		EPDPersonType personInfo=null;
		if(member!=null){
			personInfo= new EPDPersonType();
			CompletePersonNameType personName= new CompletePersonNameType();

			personName.setPersonFirstName(member.getFirstName());
			personName.setPersonLastName(member.getLastName());
			personName.setPersonMiddleName(member.getMiddleName());
			personName.setSuffixName(member.getNameSuffix());

			personInfo.setCompletePersonName(personName);
			if(member.getBirthDate()!=null){
				personInfo.setBirthDt(getXmlGregorianCalendarDate(member.getBirthDate()));
			}
			personInfo.setSSN(member.getSsn());

		}
		return personInfo;
	}

	private PersonInformationType getPersonInfo(EnrollmentMember1095 member){
		PersonInformationType personInfo=null;
		if(member!=null){
			personInfo= new PersonInformationType();
			CompletePersonNameType personName= new CompletePersonNameType();
			PersonAddressGrpType personAddressGrpType = new PersonAddressGrpType();

			personName.setPersonFirstName(member.getFirstName());
			personName.setPersonLastName(member.getLastName());
			personName.setPersonMiddleName(member.getMiddleName());
			personName.setSuffixName(member.getNameSuffix());

			personInfo.setCompletePersonName(personName);
			if(member.getBirthDate()!=null){
				personInfo.setBirthDt(getXmlGregorianCalendarDate(member.getBirthDate()));
			}
			personAddressGrpType.setUSAddressGrp(getUSAddressGrp(member));
			personInfo.setPersonAddressGrp(personAddressGrpType);
			
			personInfo.setSSN(member.getSsn());

		}
		return personInfo;
	}

	private USAddressGrpType getUSAddressGrp(EnrollmentMember1095 member) {

		USAddressGrpType updatedAddress=null;
		if(member!=null && member.getAddress1() !=null ){
			updatedAddress=  new USAddressGrpType();

			updatedAddress.setAddressLine1Txt(removePeriodAndExtraSpaces(member.getAddress1() ));
			updatedAddress.setAddressLine2Txt(removePeriodAndExtraSpaces(member.getAddress2()));
			updatedAddress.setCityNm(removePeriodAndExtraSpaces(member.getCity()));
			updatedAddress.setUSZIPCd(member.getZip());

			StateType usStateCd = null;
			if(null != member.getState()){
				usStateCd = StateType.fromValue(member.getState());
			}
			updatedAddress.setUSStateCd(usStateCd);
		}
		return updatedAddress;
	}
	private List<EnrollmentMember1095> findDependents(List<Enrollment1095> activeEnrollments, List<String> memberIds,Date startDate,Date endDate){
		List<EnrollmentMember1095> dependents=new ArrayList<>();
		for(Enrollment1095  enr: activeEnrollments){
			List<EnrollmentMember1095> members=getTypedMemberFromEnrollment(enr, EnrollmentMember1095.MemberType.MEMBER, startDate, endDate);
			if(members!=null && members.size()>0){
				for(EnrollmentMember1095 member: members){
					if(member.getMemberId()==null || memberIds==null ||( memberIds.size()>0 && !memberIds.contains(member.getMemberId()+""))){
						dependents.add(member);
						memberIds.add(member.getMemberId()+"");
					}
				}
			}
		}
		return dependents;
	}

	private EnrollmentMember1095 findSpouse(List<Enrollment1095> activeEnrollments,Date startDate,Date endDate){
		EnrollmentMember1095 spouse=null;
		for(Enrollment1095  enr: activeEnrollments){
			List<EnrollmentMember1095> spouses=getTypedMemberFromEnrollment(enr, EnrollmentMember1095.MemberType.SPOUSE, startDate, endDate);
			if(spouses!=null && spouses.size()>0){
				spouses=getTypedMemberFromEnrollment(enr, EnrollmentMember1095.MemberType.MEMBER, startDate, endDate, spouses.get(0).getMemberId());
				if(spouses!=null && spouses.size()>0){
					spouse=spouses.get(0);
					break;
				}
			}
		}
		return spouse;
	}


	List<EnrollmentMember1095> getTypedMemberFromEnrollment(Enrollment1095 enrollment, EnrollmentMember1095.MemberType memberType, Date startDate, Date endDate){
		List<EnrollmentMember1095> memberTypedList=new ArrayList<>();
		if(enrollment!=null && enrollment.getEnrollmentMembers()!=null && enrollment.getEnrollmentMembers().size()>0 && memberType!=null){
			for(EnrollmentMember1095 member: enrollment.getEnrollmentMembers()){
				if(member.getMemberType()!=null && member.getMemberType().equalsIgnoreCase(memberType.toString()) && (member.getIsActive()==null || member.getIsActive().equalsIgnoreCase(Enrollment1095.YorNFlagIndicator.Y.toString())) 
						){

					if(!memberType.toString().equalsIgnoreCase(EnrollmentMember1095.MemberType.MEMBER.toString()) || (member.getCoverageStartDate().before(endDate) && !DateUtils.isSameDay(member.getCoverageStartDate(), endDate) && (member.getCoverageEndDate().compareTo(startDate)>=0 || DateUtils.isSameDay(member.getCoverageEndDate(), startDate)))){
						memberTypedList.add(member);
					}
				}
			}
		}
		return memberTypedList;
	}


	List<EnrollmentMember1095> getTypedMemberFromEnrollment(Enrollment1095 enrollment, EnrollmentMember1095.MemberType memberType, Date startDate, Date endDate,Integer memberId){
		List<EnrollmentMember1095> memberTypedList=new ArrayList<>();
		if(enrollment!=null && enrollment.getEnrollmentMembers()!=null && enrollment.getEnrollmentMembers().size()>0 && memberType!=null){
			for(EnrollmentMember1095 member: enrollment.getEnrollmentMembers()){
				if(member.getMemberType()!=null && member.getMemberType().equalsIgnoreCase(memberType.toString()) && (member.getIsActive()==null || member.getIsActive().equalsIgnoreCase(Enrollment1095.YorNFlagIndicator.Y.toString())) 
						&& member.getMemberId().equals(memberId)){

					if(!memberType.toString().equalsIgnoreCase(EnrollmentMember1095.MemberType.MEMBER.toString()) || (member.getCoverageStartDate().before(endDate) && !DateUtils.isSameDay(member.getCoverageStartDate(), endDate) && (member.getCoverageEndDate().compareTo(startDate)>=0 || DateUtils.isSameDay(member.getCoverageEndDate(), startDate)))){
						memberTypedList.add(member);
					}
				}
			}
		}
		return memberTypedList;
	}

	private List<Enrollment1095> getActiveEnrollments(List<Enrollment1095> enrollmentList,Date startDate,Date endDate){
		List<Enrollment1095> activeEnrollmentList= null;
		if(startDate!=null && endDate!=null){
			activeEnrollmentList= new ArrayList<>();
			for(Enrollment1095 enrollment: enrollmentList){

				if(enrollment.getPolicyStartDate().before(endDate) && !DateUtils.isSameDay(enrollment.getPolicyStartDate(), endDate) && (enrollment.getPolicyEndDate().compareTo(startDate)>=0 || DateUtils.isSameDay(enrollment.getPolicyEndDate(), startDate))){
					activeEnrollmentList.add(enrollment);
				}
			}
		}
		return activeEnrollmentList;
	}

	private IndividualExchangeType getIndividualExchange(List<String> householdCaseIdList,int  applicableMonth,int applicableYear, Map<String, List<Integer>> household1095IdsMap)throws GIException {
		IndividualExchangeType individualExchangeTypeObj = new IndividualExchangeType();

		// Confirm HealthExchangeId 
		individualExchangeTypeObj.setHealthExchangeId(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_HEALTH_EXCHANGE_ID));

		getIRSHouseholdGrpType(householdCaseIdList, individualExchangeTypeObj.getIRSHouseholdGrp(), applicableMonth, applicableYear,  household1095IdsMap);

		return individualExchangeTypeObj;
	}

	private void getIRSHouseholdGrpType(List<String> householdCaseIdList, List<IRSHouseholdGrpType> irsHouseholdGrp, int month,int  year, Map<String, List<Integer>> household1095IdsMap) throws GIException {
		Date currentMonthStartDate = null;
		Date yearStartDate = null;
		//int intMonth = month;
		//int intYear = year;
		try {
			currentMonthStartDate = EnrollmentUtils.getMonthStartDateTime(month,year);
		} catch (GIException e) {
			throw new GIException("Error getting currentMonthStartDate", e);
		}
		//To handle boundary condition when month passed is January
		//int applicableYear = intYear;
		if(month == 0){
			year--;
			month=12;
		}
		yearStartDate = EnrollmentUtils.getCurrentYearStartDate(year);
		for (String householdCaseId : householdCaseIdList) {

			try{

				String irsGroupIdNum = StringUtils.leftPad(householdCaseId, 16, "0");
				IRSHouseholdGrpType irsHouseholdGrpType = new IRSHouseholdGrpType();
				irsHouseholdGrpType.setIRSGroupIdentificationNum(irsGroupIdNum);
				TaxHouseholdType taxHousehold= new TaxHouseholdType();
				List<InsurancePolicyType> houseHoldInsurancePolicy= new ArrayList<InsurancePolicyType>();
				getMonthWiseTaxHouseholdAndInsurance(householdCaseId, month, year, yearStartDate, currentMonthStartDate, taxHousehold,  houseHoldInsurancePolicy, household1095IdsMap);

				if(taxHousehold.getTaxHouseholdCoverage()!=null && taxHousehold.getTaxHouseholdCoverage().size()>0){
					irsHouseholdGrpType.getTaxHousehold().add(taxHousehold);
				}
				if(houseHoldInsurancePolicy.size()>0)
				{
					irsHouseholdGrpType.getInsurancePolicy().addAll(houseHoldInsurancePolicy);
				}
				irsHouseholdGrp.add(irsHouseholdGrpType);
			}catch(Exception ex){
				enrollmentPLROut.getSkippedHouseholdMap();
				if(enrollmentPLROut.getSkippedHouseholdMap() != null){
					enrollmentPLROut.getSkippedHouseholdMap().put(householdCaseId, EnrollmentUtils.isNotNullAndEmpty(ex.getMessage()) ? ex.getMessage() : shortenedStackTrace(ex, 3));
				}
				LOGGER.error("Exception Occurred in getIRSHouseholdGrpType method for HouseHold: "+householdCaseId +" Exception: ", ex);
			}
		}
	}

	/**
	 * 
	 * @param e
	 * @param maxLines
	 * @return
	 */
	public static String shortenedStackTrace(Exception e, int maxLines) {
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		String[] lines = writer.toString().split("\n");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Math.min(lines.length, maxLines); i++) {
			sb.append(lines[i]).append("\n");
		}
		return sb.toString();
	}

	/**
	 * 
	 * Returns Current Year
	 * 
	 * @author Aditya-S
	 * @since 30-07-2014
	 * 
	 * @return
	 */
	private XMLGregorianCalendar getSubmissionYear(int year){

		//Calendar calender = Calendar.getInstance();
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(GregorianCalendar.YEAR, year);
		XMLGregorianCalendar xmlGregorianCalendar = null;
		try {
			xmlGregorianCalendar = DatatypeFactory.newInstance()
					.newXMLGregorianCalendarDate(year,
							DatatypeConstants.FIELD_UNDEFINED,
							DatatypeConstants.FIELD_UNDEFINED,
							DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException dex) {
			LOGGER.error("DatatypeConfigurationException @ getSubmissionYear", dex);
		}
		return xmlGregorianCalendar;
	}


	/**
	 * Convert normal date to XMLGregorianCalendar date
	 * @param date
	 * @return XMLGregorianCalendar 
	 */
	private XMLGregorianCalendar getXmlGregorianCalendarDate(
			Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		XMLGregorianCalendar xmlGregorianDate = null;
		cal.setTime(date);
		try {
			xmlGregorianDate = DatatypeFactory.newInstance()
					.newXMLGregorianCalendarDate(cal.get(Calendar.YEAR),
							cal.get(Calendar.MONTH) + 1,
							cal.get(Calendar.DAY_OF_MONTH),
							DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			LOGGER.error("Error converting to XMLGregorianCalendar Date", e);
		}
		return xmlGregorianDate;
	}



	/**
	 * Converts float to BigDecimal
	 * @param amount
	 * @return
	 */
	private BigDecimal getBigDecimalFromFloat(Float amount) {

		BigDecimal bigDecimalAmount = null;
		if(amount !=null){
			try{
				bigDecimalAmount = new BigDecimal(Float.toString(amount)).setScale(2, BigDecimal.ROUND_HALF_UP);
			}catch(Exception e){
				LOGGER.error("Error parsing float amount" , e);
			}
		}
		return bigDecimalAmount;
	}


	/**
	 * Converts Boolean to BooleanStringType
	 * @param parameter
	 * @return
	 */
	private BooleanStringType getBooleanStringType(
			Boolean parameter) {
		return parameter ? BooleanStringType.Y : BooleanStringType.N;
	}

	/**
	 * Remove occurrences of period and extra spaces
	 * @param str
	 * @return String null if input string is null
	 */
	private String removePeriodAndExtraSpaces(String str) {
		String result = null;
		if(null != str){
			result = EnrollmentUtils.removeExtraSpaces(str).replaceAll("\\.", "");
		}
		return result;
	}

	private String generatePLRXml(HealthExchangeType healthExchangeType, int fileNumber) throws GIException{
		ObjectFactory objectFactory = new ObjectFactory();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmssSSS'Z'");
		String fileName = null;
		try {
			fileName = "EOM_Request_"+String.format("%05d", fileNumber+1)+"_"+dateFormat.format(new Date())+".xml";
			StringBuilder irsFileNameBuilder = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(
					EnrollmentConstants.ReportType.PLR.toString(), EnrollmentConstants.TRANSFER_DIRECTION_OUT);
			irsFileNameBuilder.append(File.separatorChar).append(EnrollmentConstants.WIP_FOLDER_NAME);
			EnrollmentUtils.createDirectory(irsFileNameBuilder.toString());
			irsFileNameBuilder.append("/");
			irsFileNameBuilder.append(fileName);


			JAXBContext jaxbContext = JAXBContext.newInstance(HealthExchangeType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
			jaxbMarshaller.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new EnrollmentIrsEscapeHandler());

			JAXBElement<HealthExchangeType> je =  objectFactory.createHealthExchange(healthExchangeType);
			jaxbMarshaller.marshal(je, new File(irsFileNameBuilder.toString()));

		} catch (JAXBException e) {
			LOGGER.error("EnrollmentIrsReportServiceImpl @ generateIrsXml  ", e);
			throw new GIException("Error generating XML @ generateIrsXml", e);
		}
		return fileName;
	}


	@Override
	public void generateFilePayloadBatchTransmissionXML(int month, int year, boolean isGeneratedXMLValid) throws GIException {

		String plrFolderPath= EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(
				EnrollmentConstants.ReportType.PLR.toString(), EnrollmentConstants.TRANSFER_DIRECTION_OUT).toString();
		//String plrFolderPath= "D:/plr";
		String filePayloadXMLFolderPath = plrFolderPath + File.separatorChar + EnrollmentConstants.WIP_FOLDER_NAME;
		String FilePackageBatchXMLPath = plrFolderPath + File.separatorChar + EnrollmentConstants.MANIFEST_FOLDER;
		String directoryTobeZipped =  plrFolderPath + File.separatorChar + EnrollmentConstants.ZIP_FOLDER;
		String ArchiveFilePayloadPath =  plrFolderPath + File.separatorChar + EnrollmentConstants.ARCHIVE_FOLDER;
		String hubFTPZipFilePath =plrFolderPath + File.separatorChar + EnrollmentConstants.VALID_FOLDER; 
		String plrValidationFailedFile = plrFolderPath + File.separatorChar + EnrollmentConstants.INVALID_FOLDER;
		String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_PLR_REPORT_ENVIRONMENT);


		if(filePayloadXMLFolderPath !=null && FilePackageBatchXMLPath != null &&  directoryTobeZipped != null && ArchiveFilePayloadPath != null &&
				hubFTPZipFilePath != null){

			//Verify Directory Exists or Not, If Not exists then create directory
			EnrollmentUtils.createDirectory(FilePackageBatchXMLPath);
			EnrollmentUtils.createDirectory(directoryTobeZipped);
			EnrollmentUtils.createDirectory(ArchiveFilePayloadPath);
			EnrollmentUtils.createDirectory(hubFTPZipFilePath);
			EnrollmentUtils.createDirectory(plrValidationFailedFile);

			// clean all the directories:
			File zipDiectory = new File(directoryTobeZipped);
			File manifestDirectory = new File(FilePackageBatchXMLPath);
			EnrollmentUtils.deleteDirectory(zipDiectory);
			// delete the sub dir
			EnrollmentUtils.deleteDir(directoryTobeZipped + File.separator +"ResentFile_batchID");
			//delete the manifest dir
			EnrollmentUtils.deleteDirectory(manifestDirectory);

			FilePayloadBatchTransmission filePayloadBatchTransmission = new FilePayloadBatchTransmission();						 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			String batchDate = sdf.format(new Date()); 
			filePayloadBatchTransmission.setBatchID(batchDate);
			String partnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.TradingPartnerID);
			String cmsPartnerID = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_CMS_PARTNERID);
			filePayloadBatchTransmission.setBatchPartnerID(cmsPartnerID);
			File filePayloadFolder = new File(filePayloadXMLFolderPath);
			int batchAttachmentTotalQuantity = 0;		 
			if(filePayloadXMLFolderPath!=null && !("".equals(filePayloadXMLFolderPath.trim()))){
				if (!(new File(filePayloadXMLFolderPath).exists())) {
					throw new GIException("File Payload XML folder doesn't exists");
				}
			}	
			try{

				batchAttachmentTotalQuantity = EnrollmentUtils.countFilesInFilePayloadFolder(filePayloadFolder);

				filePayloadBatchTransmission.setBatchAttachmentTotalQuantity(batchAttachmentTotalQuantity);
				filePayloadBatchTransmission.setBatchCategoryCode(EnrollmentConstants.BATCHCATEGORYCODE_INDIVIDUAL);
				//setBatchTransmissionQuantity is set to 1 always since hub will expect 1 zip file at a time
				filePayloadBatchTransmission.setBatchTransmissionQuantity(1);
				filePayloadBatchTransmission.setTransmissionAttachmentQuantity(batchAttachmentTotalQuantity);
				//Transmission Sequence ID equals 1 in all cases. The Hub rejects the batch if Transmission Sequence ID is greater than 1.
				filePayloadBatchTransmission.setTransmissionSequenceID(1);
				if(month == 0)
				{
					year = year-1;
					month = 12;
					filePayloadBatchTransmission.setReportPeriod(year + "-" + month);
				}
				else{
					int length = String.valueOf(month).length();
					if(length >= 2){
						filePayloadBatchTransmission.setReportPeriod(year + "-" + month);
					}
					else{
						filePayloadBatchTransmission.setReportPeriod(year + "-0" + month);

					}
				}
				List<Attachment> listAttachment = getAllAttachments(filePayloadFolder);
				if(listAttachment != null && (!listAttachment.isEmpty())){
					filePayloadBatchTransmission.setAttachment(listAttachment);
				}
				String batchTransmissionPath = FilePackageBatchXMLPath + File.separator + "manifest" + ".xml";
				generateFilePayloadIRSXMLReport(filePayloadBatchTransmission, batchTransmissionPath,"FilePayloadIRSReport.xsl");	
				String zipFileName = "";
				// put in GI_APP_CONFIG		
				SimpleDateFormat dateFormat = new SimpleDateFormat("'D'yyMMdd'.T'HHmmssSSS");
				String fileCreationDateTime = dateFormat.format(new Date());
				String funcInbound="";
				if(year<2015){
					funcInbound=EnrollmentConstants.PLR_FUNC_INBOUND;
				}else{
					funcInbound=EnrollmentConstants.PLR_FUNC_INBOUND+StringUtils.substring(year+"", 2);
				}

				zipFileName = partnerID + "."+ EnrollmentConstants.PLR_APP + "." + funcInbound + "."+ fileCreationDateTime + "."+ environment + "."+ EnrollmentConstants.TRANSFER_DIRECTION_IN;
				String hubFTPZipFolderPath = hubFTPZipFilePath + File.separator + zipFileName + ".zip";
				//String hubFTPZipFilePath = "C:/Data/HubFtpLocationFolder" + zipFileName + ".zip";
				insertBatchIDIntoPLROutboundTrans(filePayloadFolder,batchDate, zipFileName); 
				//move File Payload IRS Reporting XML
				EnrollmentUtils.moveFiles(filePayloadXMLFolderPath,directoryTobeZipped,EnrollmentConstants.FILE_TYPE_XML,null);
				//move file package batch XML file
				EnrollmentUtils.moveFiles(FilePackageBatchXMLPath,directoryTobeZipped,EnrollmentConstants.FILE_TYPE_XML,null);
				//Zip all the file content and drop into Hub FTP location
				ZipHelper zippy = new ZipHelper();
				zippy.zipDir(directoryTobeZipped,hubFTPZipFolderPath);
				// File (or directory) with old name
				File oldzipFile = new File(hubFTPZipFolderPath);          
				// File (or directory) with new name
				String newZipFileName =  hubFTPZipFilePath + File.separator + zipFileName;			
				File newZipFile = new File(newZipFileName);   // Rename file (or directory)
				batchDate = batchDate.replace('|', '-');
				batchDate = batchDate.replace(':', '-');
				String timeStampArchiveFileFolder = ArchiveFilePayloadPath + File.separator + batchDate;
				// FileUtils.cleanDirectory(new File(directoryTobeZipped));
				EnrollmentUtils.moveFiles(directoryTobeZipped,timeStampArchiveFileFolder,EnrollmentConstants.FILE_TYPE_XML,null);
				oldzipFile.renameTo(newZipFile);
				//  deleteDir(directoryTobeZipped);
				//zipDirectory(directoryTobeZipped);

				if(!isGeneratedXMLValid){
					//Move the Package file to Bad Folder
					List<File> fileToMove = new ArrayList<File>();
					fileToMove.add(EnrollmentUtils.searchFile(hubFTPZipFilePath, zipFileName, Boolean.FALSE));
					EnrollmentUtils.moveFiles(hubFTPZipFilePath, plrValidationFailedFile, null, fileToMove);
					throw new Exception("Monthly PLR validation failed against XSD, Refer validation logs for more details");
				}

			}
			catch(Exception e){
				LOGGER.error("generateFilePayloadBatchTransmissionXML()::"+e.getMessage(),e);
				throw new GIException("Error in generateFilePayloadBatchTransmissionXML() :: No IRS folder structure defined: "+e);
			}
		}else {
			throw new GIException("Error in generateFilePayloadBatchTransmissionXML() :: No IRS folder structure defined");
		}


	}

	public List<Attachment> getAllAttachments(File directory) {
		List<Attachment> listAttachment = new ArrayList<Attachment>();
		int count = 0;
		if(directory.isDirectory()){
			File[] files = directory.listFiles();
			Arrays.sort(files, new Comparator<File>(){
				@Override
				public int compare(File f1, File f2)
				{
					return f1.getName().compareToIgnoreCase(f2.getName());
					//					return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
				} });

			for(File file : files) {
				count = count +1;
				if(!file.isDirectory()) {
					Attachment attachment = new Attachment();
					attachment.setBinarySizeValue(file.length());
					attachment.setDocumentFileName(file.getName());
					String fileName = file.getName();
					String parts[] = fileName.split("\\_");
					String seqId = parts[2];
					attachment.setDocumentSequenceID(seqId);
					// get the MD5 checksum number
					try{
						FileInputStream fis = new FileInputStream(file);
						String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
						//						String md5 = org.apache.commons.codec.digest.DigestUtils.sha384Hex(fis);
						attachment.setMD5ChecksumText(md5);
						fis.close();
					}catch(Exception ex){
						LOGGER.error(ex.getMessage(),ex);
					}
					listAttachment.add(attachment);
				}
			}
		}
		return listAttachment;
	}

	private void generateFilePayloadIRSXMLReport(FilePayloadBatchTransmission filePayloadBatchTransmission,String outputPath, String xsltPath) throws GIException {	
		try {
			LOGGER.info("generateEnrollmentsXMLReport outputPath = "+ outputPath);
			if(filePayloadBatchTransmission!=null && filePayloadBatchTransmission.getAttachment() != null && !filePayloadBatchTransmission.getAttachment().isEmpty()){			
				String strXML = null;
				InputStream strXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream(xsltPath);
				InputStream strEmptyTagXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream("removeEmptyTags.xsl");
				JAXBContext context = JAXBContext.newInstance(com.getinsured.hix.model.enrollment.FilePayloadBatchTransmission.class,com.getinsured.hix.model.enrollment.Attachment.class,ArrayList.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

				StringWriter writerEnroll = new StringWriter();
				StreamResult resultEnroll = new StreamResult(writerEnroll);

				m.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new NullCharacterEscapeHandler());
				m.marshal(filePayloadBatchTransmission, resultEnroll);
				String strEnrollTrans = new String(writerEnroll.toString());
				//TransformerFactory factory = null;
				//factory =  TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",null);
				TransformerFactory factory = new net.sf.saxon.TransformerFactoryImpl();
				Source xslSource = new StreamSource(strXSLPath);
				StreamSource xmlJAXB = new StreamSource(new StringReader(strEnrollTrans));
				Templates template = factory.newTemplates(xslSource);
				Transformer transformer = template.newTransformer();
				StringWriter writerXSLT = new StringWriter();
				StreamResult resultXSLT = new StreamResult(writerXSLT);
				transformer.transform(xmlJAXB, resultXSLT);

				strXML = new String(writerXSLT.toString());
				StreamSource xmlWithEmpty = new StreamSource(new StringReader(strXML));
				Source xslEmptyTagSource = new StreamSource(strEmptyTagXSLPath);
				Templates templateEmpty = factory.newTemplates(xslEmptyTagSource);
				Transformer transformerEmpty = templateEmpty.newTransformer();
				transformerEmpty.transform(xmlWithEmpty, new StreamResult(new File(outputPath)));
			}	

		} catch (Exception exception) {

			LOGGER.error(" EnrollmentServiceImpl.generateFilePayloadIRSXMLReport Exception "+ exception.getMessage());
			throw new GIException(" EnrollmentServiceImpl.generateFilePayloadIRSXMLReport Exception ",exception);

		}
	}

	public void insertBatchIDIntoPLROutboundTrans(File directory, String batchID, String zipFileName) {
		for(File file : directory.listFiles()) {
			if(!file.isDirectory()) {
				String fileName = file.getName();
				if (! fileName.equalsIgnoreCase("manifest.xml")){
					EnrollmentOutPlr plrOutBound = plrOutboundRepo.findplrOutboundTransmissionByFileName(fileName);
					if(plrOutBound != null)
					{
						plrOutBound.setBatchId(batchID);
						plrOutBound.setDocumentFileSize(file.length());
						plrOutBound.setPackageFileName(zipFileName);
						try{
							plrOutboundRepo.save(plrOutBound);
						}catch(Exception e){
							LOGGER.error("Error saving data in IRSOutBoundTransmission table @ logIRSOutBoundTransmissionInformation" , e);
						}
					}

				}
			}
		}

	}	 

	@Transactional
	private void logPLRSkipSuccessDetails(List<String> householdCaseIds, int month, int year, String filename, Map<String, List<Integer>> household1095IdsMap) throws GIException{
		if(householdCaseIds!=null && householdCaseIds.size()>0){
			List<String> successHouseholdList= new ArrayList<>();
			for (String householdCaseId : householdCaseIds) {
				List<Integer> enrollment1095Ids= household1095IdsMap.get(householdCaseId);
				//List<Enrollment1095> enrollmentList=(ArrayList<Enrollment1095>) enrollment1095Repository.getByHouseholdCaseId(householdCaseId, yearStartDate, currentMonthStartDate);
				//List<EnrollmentPLRExecution> enrollmentList=(ArrayList<EnrollmentPLRExecution>) enrollmentPLRExecutionRepository.getByHouseholdCaseId(householdCaseId, yearStartDate, currentMonthStartDate);
				List<EnrollmentPLRExecution> enrollmentPLRExecutionList=null;
				if(enrollment1095Ids!=null && enrollment1095Ids.size()>0){
					enrollmentPLRExecutionList= (ArrayList<EnrollmentPLRExecution>) enrollmentPLRExecutionRepository.getByEnrollment1095Id(enrollment1095Ids);
				}
				List<Integer> existing1095Ids= new ArrayList<>();
				if(enrollmentPLRExecutionList!=null && enrollmentPLRExecutionList.size()>0){
					if(!enrollmentPLROut.getSkippedHouseholdMap().containsKey(householdCaseId)){
						//success
						for(EnrollmentPLRExecution enr: enrollmentPLRExecutionList){
							if(!existing1095Ids.contains(enr.getEnrollment1095())){
								existing1095Ids.add(enr.getEnrollment1095());
							}
							enr.setPlrXmlFileName(filename);
							enr.setPlrXmlGeneratedOn(new Date());
							enr.setPlrSkippedFlag(null);
							enr.setPlrSkippedMsg(null);
						}
						if(!successHouseholdList.contains(householdCaseId)){
							successHouseholdList.add(householdCaseId);
						}

					}else{
						//failure
						for(EnrollmentPLRExecution enr: enrollmentPLRExecutionList){
							if(!existing1095Ids.contains(enr.getEnrollment1095())){
								existing1095Ids.add(enr.getEnrollment1095());
							}
							enr.setPlrSkippedFlag(Enrollment1095.YorNFlagIndicator.Y.toString());
							enr.setPlrSkippedMsg(enrollmentPLROut.getSkippedHouseholdMap().get(householdCaseId));
							enr.setPlrXmlFileName(null);
							enr.setPlrXmlGeneratedOn(null);
						}
					}


				}
				for(Integer enrollment1095Id: enrollment1095Ids){
					if(!existing1095Ids.contains(enrollment1095Id)){
						EnrollmentPLRExecution enr= new EnrollmentPLRExecution();
						if(!enrollmentPLROut.getSkippedHouseholdMap().containsKey(householdCaseId)){
							enr.setPlrXmlFileName(filename);
							enr.setPlrXmlGeneratedOn(new Date());
							enr.setPlrSkippedFlag(null);
							enr.setPlrSkippedMsg(null);
							enr.setEnrollment1095(enrollment1095Id);
							if(!successHouseholdList.contains(householdCaseId)){
								successHouseholdList.add(householdCaseId);
							}
						}else{

							enr.setPlrSkippedFlag(Enrollment1095.YorNFlagIndicator.Y.toString());
							enr.setPlrSkippedMsg(enrollmentPLROut.getSkippedHouseholdMap().get(householdCaseId));
							enr.setPlrXmlFileName(null);
							enr.setPlrXmlGeneratedOn(null);
							enr.setEnrollment1095(enrollment1095Id);
						}
						if(enrollmentPLRExecutionList==null){
							enrollmentPLRExecutionList= new ArrayList<>();
						}
						enrollmentPLRExecutionList.add(enr);
					}

				}

				enrollmentPLRExecutionRepository.save(enrollmentPLRExecutionList);

			}

			EnrollmentOutPlr plrOutBound= new EnrollmentOutPlr();
			if(successHouseholdList!=null && successHouseholdList.size()>0){
				plrOutBound.setHouseholdCaseIds(successHouseholdList.toString().replace("{", ""));
				plrOutBound.setHouseholdsPerXml(successHouseholdList!=null ?successHouseholdList.size(): 0);
			}

			plrOutBound.setDocumentFileName(filename);
			plrOutBound.setMonth((month+1) + "");
			plrOutBound.setYear(""+year);
			plrOutBound.setCreatedOn(new Date());
			plrOutBound.setSubmissionType("I");
			plrOutBound.setReportType("plr");
			plrOutBound.setBatchCategoryCode(EnrollmentConstants.BATCHCATEGORYCODE_INDIVIDUAL);

			plrOutboundRepo.save(plrOutBound);

		}
	}

	@Override
	public List<BatchJobExecution> getRunningBatchList(String jobName) {
		return batchJobExecutionService.findRunningJob(jobName);
	}

	@Override
	public boolean validateGeneratedMonthlyPLRXML(Long jobId) {
		boolean isGeneratedFilesValid = Boolean.FALSE;
		String plrFolderPath = EnrollmentUtils
				.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.PLR.toString(),
						EnrollmentConstants.TRANSFER_DIRECTION_OUT).toString();
		String filePayloadXMLFolderPath = plrFolderPath + File.separator + EnrollmentConstants.WIP_FOLDER_NAME;
		if(StringUtils.isNotEmpty(filePayloadXMLFolderPath) && (new File(filePayloadXMLFolderPath).exists())){
			String validationLogs = null;
			File[] listOfXmlFiles = EnrollmentUtils.getFilesInAFolderByName(filePayloadXMLFolderPath, EnrollmentConstants.FILE_TYPE_XML);
			if(listOfXmlFiles != null && listOfXmlFiles.length >0){
				//Create Directory if not present
				String plrValidationLogDirectory = plrFolderPath + File.separator + EnrollmentConstants.XML_VALIDATION_LOG_FOLDER;
				EnrollmentUtils.createDirectory(plrValidationLogDirectory);

				/*Map <String, String> errorMessageMap = new HashMap<String, String>();*/
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				EnrollmentXMLValidationDTO enrollmentXMLValidationDTO = new EnrollmentXMLValidationDTO();
				try
				{
					int validFileCount = 0, invalidFileCount = 0;
					ClassLoader classLoader = getClass().getClassLoader();

					String initialFilePath =  classLoader.getResource("/enrollment").getPath() + EnrollmentConstants.MONTHLY_PLR_SCHEMA_BASE_DIRECORY; 
					File xsdFile = EnrollmentUtils.searchFile(initialFilePath, 
							EnrollmentConstants.MONTHLY_PLR_SCHEMA_FILENAME, 
							Boolean.TRUE);

					if(xsdFile != null){
						enrollmentXMLValidationDTO.setServerURL(GhixPlatformEndPoints.GHIXWEB_SERVICE_URL);
						enrollmentXMLValidationDTO.setXSDFileName(xsdFile.getName());
						enrollmentXMLValidationDTO.setValidationProcessName("MONTHLY_PLR");
						String schemaLang = "http://www.w3.org/2001/XMLSchema";
						SchemaFactory factory = SchemaFactory.newInstance(schemaLang);
						//set prefix if your schema is not in the root of classpath
						ResourceResolver resolver = new ResourceResolver(Paths.get(xsdFile.getParent()).getParent().toString() + File.separator);

						factory.setResourceResolver(resolver);
						Schema schema = factory.newSchema(new StreamSource(xsdFile));
						Validator validator = schema.newValidator();
						EnrollmentIrsSaxErrorHandler errorHandler = new EnrollmentIrsSaxErrorHandler();
						List<EnrollmentXMLValidationDetails> enrollmentXMLValidationDetailsList = new ArrayList<EnrollmentXMLValidationDetails>();
						for(File xmlfile : listOfXmlFiles)
						{
							EnrollmentXMLValidationDetails enrollmentXMLValidationDetails = new EnrollmentXMLValidationDetails();
							enrollmentXMLValidationDetails.setXMLFileName(xmlfile.getName());
							try{
								errorHandler.resetHandler();
								validator.setErrorHandler(errorHandler);
								validator.validate(new StreamSource(xmlfile));
								/*errorMessageMap.put(xmlfile.getName(), "VALID");*/
								if(errorHandler.isValid()){
									enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.VALID);	
									validFileCount++;
								}else{
									LOGGER.error("XML file invalid");
									enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
									enrollmentXMLValidationDetails.setErrorMessage(errorHandler.getErrorList().toString());
									invalidFileCount++;
								}
							}
							catch(IOException io){
								LOGGER.error("IO Exception occurred in validateIRSAnnualXML ",io);
								enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
								enrollmentXMLValidationDetails.setErrorMessage( io.getMessage() != null ? io.getMessage() : EnrollmentUtils.shortenedStackTrace(io, 3));
								invalidFileCount++;
							}
							catch(SAXException sax){
								LOGGER.error("SAXException occurred in validateIRSAnnualXML ",sax);
								enrollmentXMLValidationDetails.setValidationStatus(ValidationStatus.INVALID);
								enrollmentXMLValidationDetails.setErrorMessage(errorHandler.getErrorList().toString());
								invalidFileCount++;
							}
							enrollmentXMLValidationDetailsList.add(enrollmentXMLValidationDetails);
						}
						enrollmentXMLValidationDTO.setEnrollmentXMLValidationDetailsList(enrollmentXMLValidationDetailsList);
						enrollmentXMLValidationDTO.setTotalFileCount(listOfXmlFiles.length);
						enrollmentXMLValidationDTO.setTotalValidFileCount(validFileCount);
						enrollmentXMLValidationDTO.setTotalInValidFileCount(invalidFileCount);

						if(listOfXmlFiles.length == validFileCount){
							isGeneratedFilesValid = Boolean.TRUE;
							enrollmentXMLValidationDTO.setValidationStatus(XMLValidationStatus.PASS);
						}
						else{
							enrollmentXMLValidationDTO.setValidationStatus(XMLValidationStatus.FAILED);
						}

						validationLogs = gson.toJson(enrollmentXMLValidationDTO);
						if(!isGeneratedFilesValid){
							logBug("MONTHLY PLR XML Job:: XSD Validation failed for some of the generated XMLs: ",validationLogs);
						}

						//Save the Result to a file
						StringBuilder stringbuilder = new StringBuilder(64);
						stringbuilder.append(plrValidationLogDirectory);
						stringbuilder.append(File.separator);
						SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.FILENAME_DATE_FORMAT);
						stringbuilder.append(dateFormat.format(new Date()));

						EnrollmentUtils.createDirectory(stringbuilder.toString());
						stringbuilder.append(File.separator);

						//file Name PLR_XML_VALIDATION_RESULTS_{JobID}.txt
						stringbuilder.append("PLR_XML_VALIDATION_RESULTS");
						stringbuilder.append("_");
						stringbuilder.append(jobId);
						stringbuilder.append(".txt");

						try(Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(stringbuilder.toString()), "utf-8"))){
							writer.write(validationLogs);
						}
						catch(Exception ex){
							throw new GIException(ex);
						}
					}
					else{
						throw new GIException("Monthly PLR XSD is missing at location: "+initialFilePath);
					}
				}
				catch(Exception ex){
					LOGGER.error("Exception occurred while validating Monthly PLR XML files: ", ex);
					//throw new GIException(ex);
					logBug("Exception occurred while validating Monthly PLR generated XML files", 
							EnrollmentUtils.shortenedStackTrace(ex, EnrollmentConstants.FOUR));
				}
			}
			else{
				//Since no XML present then mark the Boolean flag as TRUE
				//For Runs when no xml is generated
				isGeneratedFilesValid = Boolean.TRUE;
			}
		}
		return isGeneratedFilesValid;
	}

	private void logBug(String subject, String bugDescription){
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
		try{
			if(isJiraEnabled){
				JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
						Arrays.asList(fixVersion), bugDescription, 
					subject,
					null);
		}
		}
		catch(Exception exc){
			LOGGER.error("Unable to log jira for xml validation failure: ", exc);
		}
		 enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.IRS,
				 subject, bugDescription);
	} 
}
