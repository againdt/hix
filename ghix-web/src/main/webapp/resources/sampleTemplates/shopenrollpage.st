<div class="row-fluid header" style='height:auto;'>
    <div class="">
      	<h4 class='span8'>$enrolleeNames$
			
		</h4>	
		$if(showTermOption)$
		<div style="margin:10px 8px" class="span4">
			$if(isHealthPlan)$
		  		<a class="btn btn-primary btn-small pull-right" id="terminateHealthLink" href='#' onclick="terminateCoverage('HEALTH');" style="margin: 0 0 0 10px;">Terminate Health Plan</a>
		  		<input class="pull-right" type='checkbox' id='terminateHealth' name='terminatePlan' value='health' onclick="selectPlan('HEALTH');"/>
		  		$if(havingActiveDental)$
		  			<span class="terminationPageTooltip">Terminating a health plan will also terminate your dental plan</span>
		  		$endif$
			$endif$
			$if(isDentalPlan)$
		  		<a class="btn btn-primary btn-small pull-right" id="terminateDentalLink" href="#" onclick="terminateCoverage('DENTAL');" style="margin: 0 0 0 10px;">Terminate Dental Plan</a>
		  		<input class="pull-right" type='checkbox' id='terminateDental' name='terminatePlan' value='dental' onclick="selectPlan('DENTAL');"/>
			$endif$
		</div>
		$endif$
     </div>
</div>

<div class="row-fluid">
	<div class="span6 info-block">                    
	  <div class="gutter10">
	    <div class="plan-selected alert alert-success">
	        <div class="info-header txt-center">
	            <img src="$logoStr$" alt="$issuerName$" class="resize-img" style="margin:0 auto;">
	            <p>$planName$</p>
	            <h5 class="payment">&#36;$monthlyPremium$</h5>
	        </div>
	        $if(showTerminated)$
	        <div class="mask" id="plan-mask-sample"><img alt="This is a sample plan summary" src="/hix/resources/img/terminate_stamp.png"></div>
	        
	        $endif$
	        <div class="data-display well">
	         <dl class="dl-horizontal">
	            <dt>Plan Type:</dt>
	            <dd id="plan-type_$planId$">$planType$</dd>
	            <dt>Office Visit Copay:</dt>
	            <dd id="off-visit_$planId$">$officeVisit$</dd>
	            <dt>Generic Drugs Copay:</dt>
	            <dd id="generic-medic_$planId$">$genericMedications$</dd>
	            <dt>Deductible Ind:</dt>
	            <dd id="deductible_$planId$">$deductible$</dd>
	            <dt>Max Out-of-Pocket Ind:</dt>
	            <dd id="m-o-p_$planId$">$oopMax$</dd>
	          </dl>
	        </div>
	        $if(!showTerminated)$
	        <div class="center">
	        	<a href="$PlanUrl$" data-toggle="modal" id="viewBenifitDetails_$planId$" class="btn viewBenifitDetails" alt="View Benifit Details">
	        		View Benefit Details
	        	</a>
	      </div>
	      $endif$
	      </div>
	       
	       $if(showTerminated)$
		<div class="gutter10 alert alert-info margin10-t">             
			<p><span> This $insuranceType$ plan was terminated on $terminatedOn$. The qualifying event associated with the termination was <strong>$terminationReason$</strong>.</span></p>            
		</div>
		 $endif$
		 
		 <div class="gutter10-tb"></div>  
	  </div>
	</div> <!-- .span6 -->
	
	
	 
	
    <div class="span6 info-block">
      <div class="gutter10">
       	  <div class="header"><h4>Policy Information</h4></div>              
          <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Subscriber Policy Number:</dt>
                <dd id="policy-number_$planId$">$policyNumber$</dd>
                <dt>Coverage Period:</dt>
                <dd id="enrollment-date_$planId$">$coverageStartDate$ - $coverageEndDate$</dd>
              </dl>                   
          </div>
          <div class="header"><h4>Payment Information</h4></div>
		  <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Monthly Premium:</dt>
                <dd id="monthly-premium_$planId$">&#36; $monthlyPremium$</dd>
                <dt>Employer Contribution:</dt>
                <dd id="emp-contribution_$planId$"><span>-</span>&#36; $employerContribution$</dd>
                <dt class="noWhiteSpace"><strong>Monthly Payroll Deduction:</strong></dt>
                <dd id="monthly-payroll-deduct_$planId$"><strong>&#36; $monthlyPayrollDeduction$</strong></dd>
              </dl>                   
          </div>
          <div class="header"><h4>Plan Contact</h4></div>              
          <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Customer Service:</dt>
                <dd id="cust-service_$planId$">$issuerPhone$</dd>
                <dt>Web:</dt>
                <dd id="web-address_$planId$" class="breakword">$issuerSite$</dd>
              </dl>                   
          </div>
        </div><!-- .gutter10-->
    </div> <!-- .span6 -->
   
    
    
 </div><!-- .row-fluid -->
