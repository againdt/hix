package com.getinsured.hix.batch.enrollment.xmlEnrollments.processor;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.enrollment.Enrollment;


@Component("EnrollmentRecordProcessor")
public class EnrollmentRecordProcessor implements ItemProcessor<Enrollment, Enrollment> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentRecordProcessor.class);
	private ExecutionContext executionContext;
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
	 
		if(stepExecution!=null){
			executionContext = stepExecution.getJobExecution().getExecutionContext();
		}
	}
	
	@Override
	public Enrollment process(Enrollment enrollment) throws Exception {
		try{
		if(enrollment != null && enrollment.getIssuerId() != null){
			LOGGER.debug(Thread.currentThread().getName() + " Processing enrollment : " + enrollment.getId() +" of issuer " + enrollment.getIssuerId());
		}
		
		return enrollment;
	}catch(Exception e){
		if(executionContext != null){
			executionContext.put("jobExecutionStatus", "failed");
			executionContext.put("errorMessage", e.getMessage());
		}
		LOGGER.error("Exception in Writer :"+ e.getMessage());
		throw e;
	}
	}
}