package com.getinsured.hix.admin.web;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.PaymentMethods.PaymentType;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.thoughtworks.xstream.XStream;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class AdminIssuerFinancialController {
	@Value("#{configProp['appUrl']}") 
	private static String appUrl;

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminIssuerFinancialController.class);
	private static final String ROLE_NAME ="activeRoleName";
	
	@Autowired private UserService userService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IssuerService issuerService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private GIMonitorService giMonitorService;
	
		// issuer financial information dashboard
		/*
		 * @Suneel: Secured the method with permission
		 */
		@RequestMapping(value = "/admin/issuer/financial/info/list/{encIssuerId}", method = RequestMethod.GET)
		@PreAuthorize(PlanMgmtConstants.MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING)
		public String issuerFinancialListInfo(Model model, @PathVariable("encIssuerId") String encIssuerId, HttpServletRequest prequest) throws InvalidUserException {
			try{
				Integer issuerId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId));			
				AccountUser accountUser = userService.getLoggedInUser();
				String activeRoleName = accountUser.getActiveModuleName().toUpperCase();
				Issuer issuerObj = issuerService.getIssuerById(issuerId);
				prequest.setAttribute(PlanMgmtConstants.DEFAULT_SORT, "financialInfo.bankInfo.updated");
				Pageable pageable = issuerService.getPagingAndSorting(model, prequest, null);
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setModuleID(issuerObj.getId());
				paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
				
				if(pageable.getPageNumber() == 0){
					paymentMethodRequestDTO.setPageNumber(pageable.getPageNumber()+1);
				}else{
					paymentMethodRequestDTO.setPageNumber(pageable.getPageNumber());
				}
				paymentMethodRequestDTO.setPageSize(pageable.getPageSize());
				paymentMethodRequestDTO.setSortOrder(pageable.getSort().toString());
				
				// call finance module API to get list of payment methods
				String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_MODULEID_MODULENAME_PAGE, paymentMethodRequestDTO, String.class);
				XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
				
			
				PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
				if(paymentMethodResponse.getPagePaymentMethods() != null){
					Page<PaymentMethods> paymentMethods = paymentMethodResponse.getPagePaymentMethods();
					model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethods.getContent());
					model.addAttribute(PlanMgmtConstants.RESULT_SIZE, paymentMethods.getTotalElements());
				}
				
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(ROLE_NAME, activeRoleName);
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				
			}catch(Exception ex){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_FINANCIAL_LIST_INFORMATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
			}
			return "admin/issuer/financial/info/list";
		}

		// issuer account financial information
		@RequestMapping(value = "/admin/issuer/financial/info/{encIssuerId}", method = RequestMethod.GET)
		@PreAuthorize(PlanMgmtConstants.MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING)
		public String issuerFinancialInfo(Model model, @PathVariable("encIssuerId") String encIssuerId, @RequestParam("paymentId") String encPaymentId) throws GIException {
			try{
				AccountUser accountUser = userService.getLoggedInUser();
				String activeRoleName = accountUser.getActiveModuleName().toUpperCase();
				PaymentMethods paymentMethodsObj = new PaymentMethods();
				Integer issuerId  = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId));	
				Integer paymentId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encPaymentId));
				Issuer issuerObj = issuerService.getIssuerById(issuerId);
				boolean isPaymentGateway = Boolean.valueOf(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY));
				String paymentMethodsString = PlanMgmtConstants.EMPTY_STRING;
				
				// call finance API to get payment methods information
				if (!isPaymentGateway) {
					paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID, paymentId, String.class);
					paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				} else {
					paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI + paymentId, String.class);
					paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				}
				
				model.addAttribute(ROLE_NAME, activeRoleName);
				model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, isPaymentGateway);
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethodsObj);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				
			}catch(Exception ex){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_FINANCIAL_INFORMATION_EXCEPTION.getCode(), ex, null, Severity.LOW);
			}
			return "admin/issuer/financial/info";
		}

		@RequestMapping(value = "/admin/issuer/financial/info/changepaymentptatus", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING)
		@ResponseBody
		public String changePaymentStatus(Model model ,@RequestParam Integer id, @RequestParam String status) {
			PaymentMethods paymentMethodsObj = new PaymentMethods();
			try{
				AccountUser accountUser = userService.getLoggedInUser();
				String activeRoleName = accountUser.getActiveModuleName().toUpperCase();
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setId(id);
				
				if (status.equalsIgnoreCase(PaymentStatus.InActive.toString())) {
					paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.InActive);		
				}else if (status.equalsIgnoreCase(PaymentStatus.Active.toString())) {
					paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.Active);	
				}
				// call finance API to update payment status
				String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
				paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				model.addAttribute(ROLE_NAME, activeRoleName);
		
			}catch(Exception ex){
//				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.CHANGE_PAYMENT_STATUS_EXCEPTION.getCode(), ex, null, Severity.HIGH);
				giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.CHANGE_PAYMENT_STATUS_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),ex.getStackTrace().toString(), null);
				return PlanMgmtConstants.EXCEPTION +":"+ ex.getMessage();
			}
			return paymentMethodsObj.getStatus().toString();
		}

		@RequestMapping(value = "/admin/issuer/financial/info/makepaymentdefault", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING)
		@ResponseBody
		public String makePaymentDefault(Model model ,@RequestParam Integer id, @RequestParam Integer existingDefaultId) {
			PaymentMethods paymentMethodsObj2 = new PaymentMethods(); 
			try{
				AccountUser accountUser=userService.getLoggedInUser();
				String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
				model.addAttribute(ROLE_NAME, activeRoleName);
				
				// call finance API to get payment obj by payment id
				String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID , existingDefaultId, String.class);
				PaymentMethods paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				
				// set default payment status to N for existing payment method 
				if (paymentMethodsObj != null) {
					PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
					paymentMethodRequestDTO.setId(existingDefaultId);
					paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.N);
					paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
					paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				}
				
				// set default payment status to Y for new payment method
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setId(id);
				paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);
				paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
				paymentMethodsObj2 = getPaymentMethods(paymentMethodsString);
				
			}catch(Exception ex){
//				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.MAKE_PAYMENT_DEFAULT_EXCEPTION.getCode(), ex, null, Severity.HIGH);
				giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.MAKE_PAYMENT_DEFAULT_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),ex.getStackTrace().toString(), null);
				return PlanMgmtConstants.EXCEPTION +":"+ ex.getMessage();
			}
			return  paymentMethodsObj2.getIsDefault().toString();
		}

		// validate routing number
		@RequestMapping(value = "/admin/issuer/financial/info/validateroutingnumber", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING)
		@ResponseBody
		public String validatebankinfo(@RequestParam String routingNumber) {
			String response = "INVALID";
			AccountUser accountUser = null;
			try{
				accountUser = userService.getLoggedInUser();
//				BankAccInfo bankaccinfo = bankAccInfoService.getBankAccInfoDetails(routingNumber);		
//				if (bankaccinfo != null) {
//					response = "VALID" ;
//				}
			}catch(Exception ex){
		       giMonitorService.saveOrUpdateErrorLog(String.valueOf(PlanMgmtErrorCodes.ErrorCode.VALIDATE_ROUTING_NUMBER_EXCEPTION), new TSDate(), Exception.class.getName(),ExceptionUtils.getFullStackTrace(ex), accountUser);
		       response = "INVALID";
			}
			return response;
		}

		// issuer add financial information
		/*
		 * @Suneel: Secured the method with permission
		 */
		@RequestMapping(value = "/admin/issuer/financial/info/add/{encIssuerId}", method = {RequestMethod.GET, RequestMethod.POST })
		@PreAuthorize(PlanMgmtConstants.MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING)
		public String issuerAddFinancialInfo(Model model, @PathVariable("encIssuerId") String encIssuerId, HttpServletRequest request) {
			try{
				AccountUser accountUser=userService.getLoggedInUser();
				String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
				boolean isPaymentGateway = true;
				Integer issuerId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId));
				Issuer issuerObj = issuerService.getIssuerById(issuerId);
				
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setModuleID(issuerObj.getId());
				paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
				paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);

				String paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
				Map<String, Object> paymentMethods = getPaymentMethodsMap(paymentMethodsString);
				List<PaymentMethods> paymentMethodList = (List<PaymentMethods>)paymentMethods.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
				PaymentMethods paymentMethod = null;
				if(paymentMethodList != null && !paymentMethodList.isEmpty()){
					paymentMethod = paymentMethodList.get(0);
				}
				
				if(null == paymentMethod){
					model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, 0);
				}else{
					model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, paymentMethod.getId());				
				}
				//model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, (paymentMethod == null) ? 0 : paymentMethod.getId());
				
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, isPaymentGateway);
				if (isPaymentGateway) {
					Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
					if (map != null) {
						PaymentMethods paymentMethodsInfo = (PaymentMethods) map.get(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ);
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ + "::" + paymentMethodsInfo.getFinancialInfo().getBankInfo().getRoutingNumber());
						}	
						model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethodsInfo);
					}
				}
				model.addAttribute(PlanMgmtConstants.REDIRECT_URL, "redirect:/admin/issuer/financial/info/list/"+ encIssuerId + "");
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				model.addAttribute(ROLE_NAME, activeRoleName);
				
			}catch(Exception ex){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_ADD_FINANCIAL_INFORMATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
			}
			return "admin/issuer/financial/info/edit";
		}

		// issuer edit financial information
		/*
		 * @Suneel: Secured the method with permission
		 */
		@RequestMapping(value = "/admin/issuer/financial/info/edit/{encIssuerId}", method = {RequestMethod.GET, RequestMethod.POST })
		@PreAuthorize(PlanMgmtConstants.MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING)
		public String editFinancialInfo(Model model, @PathVariable("encIssuerId") String encIssuerId, @RequestParam("paymentId") String encPaymentId, HttpServletRequest request) throws GIException {
			PaymentMethods paymentMethodsObj = null;
			try{
				AccountUser accountUser=userService.getLoggedInUser();
				String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
				model.addAttribute(ROLE_NAME, activeRoleName);
				Integer issuerId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId));
				Integer paymentId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encPaymentId));
				boolean isPaymentGateway = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY));
				Issuer issuerObj = issuerService.getIssuerById(issuerId);
				model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, isPaymentGateway);
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				
				if(request.getParameter(PlanMgmtConstants.PAYMENT_ID)!=null){
					model.addAttribute(PlanMgmtConstants.FLOW, PlanMgmtConstants.EDIT);
				}
				// call financial API to pull payment methods by id
				String paymentMethodsString = null;
				if (!isPaymentGateway) {
					paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID , paymentId, String.class);
					paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				} else {
					paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI + paymentId, String.class);
					paymentMethodsObj = getPaymentMethods(paymentMethodsString);
				}
		
				PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
				paymentMethodRequestDTO.setModuleID(issuerObj.getId());
				paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
				paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);
				paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
				//PaymentMethods paymentMethods = getPaymentMethods(paymentMethodsString);
				Map<String, Object> paymentMethods = getPaymentMethodsMap(paymentMethodsString);
				
				model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, isPaymentGateway);
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				
				if(request.getParameter(PlanMgmtConstants.PAYMENT_ID)!=null){
					model.addAttribute(PlanMgmtConstants.FLOW, PlanMgmtConstants.EDIT);
				}
				
				// Adding condition for for fixing HIX-21190
				if(request.getParameter(PlanMgmtConstants.UPDATE)!=null && request.getParameter(PlanMgmtConstants.UPDATE).toString().trim().equalsIgnoreCase(PlanMgmtConstants.YES)){
					Map<String, ?> map = RequestContextUtils.getInputFlashMap(request);
					if (map != null) {
						PaymentMethods paymentMethodsInfo = (PaymentMethods) map.get(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ);
						paymentMethodsInfo.setId(paymentMethodsObj.getId());
						paymentMethodsInfo.setFinancialInfo(paymentMethodsObj.getFinancialInfo());
						FinancialInfo financialInfo=paymentMethodsObj.getFinancialInfo();
						paymentMethodsInfo.setFinancialInfo(financialInfo);
						paymentMethodsInfo.setStatus(paymentMethodsObj.getStatus());
						paymentMethodsInfo.setIsDefault(paymentMethodsObj.getIsDefault());				
						model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethodsInfo);
					}
				}else{
					model.addAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ, paymentMethodsObj);
				}
				List<PaymentMethods> paymentMethodList = (List<PaymentMethods>)paymentMethods.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
				PaymentMethods paymentMethod = null;
				if(paymentMethodList != null && !paymentMethodList.isEmpty()){
					paymentMethod = paymentMethodList.get(0);
				}
				if(null == paymentMethod){
					model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, 0);
				}else{
					model.addAttribute(PlanMgmtConstants.CURRENT_PAYMENT_ID, paymentMethod.getId());
				}
				model.addAttribute(PlanMgmtConstants.EXCHANGE_STATE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			}catch(Exception ex){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.ISSUER_EDIT_FINANCIAL_INFORMATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
			}
			
			return "admin/issuer/financial/info/edit";

		}

		@RequestMapping(value = "/admin/issuer/financial/info/save", method = RequestMethod.POST)
		@PreAuthorize(PlanMgmtConstants.MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING)
		public String saveFinancialInfo(Model model, @RequestParam(value = PlanMgmtConstants.ISSUER_ID, required = false) String encryptedIssuerId, 
				@ModelAttribute(PlanMgmtConstants.PAYMENT_METHODS_OBJ) PaymentMethods paymentMethod, @RequestParam(value = "paymentId", required = false) Integer paymentId, 
				@RequestParam(value = "chkboxAutoPay", required = false) String chkboxAutoPay, @RequestParam(value = "currentPaymentId", required = false) int currentPaymentId,
				@RequestParam(value = "currentBankAccountNumber", required = false) String currentBankAccountNumber, @RequestParam(value = "accountNumChanged", required = false) String accountNumChanged,
				RedirectAttributes redirectAttributes) throws GIException {
			Integer issuerId = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedIssuerId));
			String redirectUrl = "redirect:/admin/issuer/financial/info/list/" + encryptedIssuerId;
			try{
				AccountUser accountUser=userService.getLoggedInUser();
				String activeRoleName=accountUser.getActiveModuleName().toUpperCase();
				model.addAttribute(ROLE_NAME, activeRoleName);
				Issuer issuerObj = issuerService.getIssuerById(issuerId);
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				boolean isFinancialInfoAdded = false;
				BankInfo bankInfo = null;
				FinancialInfo financialInfo = null;
				PaymentMethods paymentMethods = null;
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				boolean isPaymentGateway = true;
				model.addAttribute(PlanMgmtConstants.IS_PAYMENT_GATEWAY_FLAG, isPaymentGateway);
				
				String paymentMethodsString = null;
				PaymentMethods paymentMethodsObj = null;
				try {
					// get the PaymentMethods, FinancialInfo, BankInfo which is already exists, if not exists, create one.
					
					if(!isPaymentGateway){
						if(null != paymentId && paymentId > 0){
							paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID , paymentId, String.class);
							paymentMethods = getPaymentMethods(paymentMethodsString);
							
						}
					}else{
						if(null != paymentId && paymentId > 0){
							paymentMethodsString = ghixRestTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+paymentId, String.class);
							paymentMethods = getPaymentMethods(paymentMethodsString);
							
						}
					}
					if(paymentMethods == null){
						paymentMethods = new PaymentMethods();
					}
					
					if(null != paymentMethods && null != paymentMethods.getFinancialInfo()){
						financialInfo =  paymentMethods.getFinancialInfo();
					}else{
						financialInfo = new FinancialInfo();
					}
					//financialInfo = (paymentMethods.getFinancialInfo() != null) ? paymentMethods.getFinancialInfo() : new FinancialInfo();
					
					if(null != financialInfo && null != financialInfo.getBankInfo()){
						bankInfo = financialInfo.getBankInfo();
					}else{
						bankInfo = new BankInfo();
					}
					//bankInfo = (financialInfo.getBankInfo() != null) ? financialInfo.getBankInfo() : new BankInfo();
		
					// adding the bank related information to the BankInfo object
					if(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber() != null){
					     bankInfo.setAccountNumber(paymentMethod.getFinancialInfo().getBankInfo().getAccountNumber());
					}else{
					     bankInfo.setAccountNumber(currentBankAccountNumber); 
					}

					bankInfo.setBankName(paymentMethod.getFinancialInfo().getBankInfo().getBankName());
					bankInfo.setRoutingNumber(paymentMethod.getFinancialInfo().getBankInfo().getRoutingNumber());
					bankInfo.setAccountType(paymentMethod.getFinancialInfo().getBankInfo().getAccountType());
					bankInfo.setNameOnAccount(paymentMethod.getFinancialInfo().getBankInfo().getNameOnAccount());
		
					 //server side validation
					 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
					 Set<ConstraintViolation<BankInfo>> violations = null;
				     if(StringUtils.isNotBlank(accountNumChanged) && PlanMgmtConstants.TRUE_STRING.equalsIgnoreCase(accountNumChanged)){
				    	 violations = validator.validate(bankInfo, BankInfo.EditPaymentMethodBankInfo.class);
				     }else{
				    	 violations = validator.validate(bankInfo, BankInfo.EditPaymentMethod.class); 
				     }
					 
					 if(violations != null && !violations.isEmpty()){
						//throw exception in case client side validation breached
						 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					 }
					 
					// adding the financial information to the FinancialInfo object
					if (financialInfo.getPaymentType() == null) {
						financialInfo.setPaymentType(com.getinsured.hix.model.FinancialInfo.PaymentType.BANK);
					}
		
					// setting the BankInfo object to the FinancialInfo object
					financialInfo.setBankInfo(bankInfo);
		
					/*	Information of issuer for Financial Information start here */
					financialInfo.setAddress1(issuerObj.getAddressLine1());
					financialInfo.setAddress2(issuerObj.getAddressLine2());
					financialInfo.setCity(issuerObj.getCity());
					financialInfo.setState(issuerObj.getState());
					financialInfo.setZipcode(issuerObj.getZip());
					financialInfo.setEmail(issuerObj.getEmailAddress());
		
					Location location=new Location();
					location.setAddress1(issuerObj.getAddressLine1());
					location.setAddress2(issuerObj.getAddressLine2());
					location.setCity(issuerObj.getCity());
					location.setState(issuerObj.getState());
					location.setZip(issuerObj.getZip());
					financialInfo.setLocation(location);
					
					/*IssuerRepresentative issuerRepresentative = issuerService.getIssuerRepresentativeForIssuer(issuerObj);
		
					if (issuerRepresentative != null) {
						LOGGER.info("issuerRepresentative: " + issuerRepresentative.getId());
						financialInfo.setFirstName(issuerRepresentative.getFirstName());
						financialInfo.setLastName(issuerRepresentative.getLastName());
						financialInfo.setContactNumber(issuerRepresentative.getPhone());
					}*/
		
					/*	Information of issuer for Financial Information end here */
		
					// setting the FinancialInfo object to the PaymentMethods object
					paymentMethods.setFinancialInfo(financialInfo);
					paymentMethods.setPaymentMethodName(paymentMethod.getPaymentMethodName());
		
					//server side validation
					 Validator validatorPaymentMethod = Validation.buildDefaultValidatorFactory().getValidator();
					 Set<ConstraintViolation<PaymentMethods>> violationsPaymentMethod = validatorPaymentMethod.validate(paymentMethods, PaymentMethods.EditPaymentMethod.class);
					 if(violationsPaymentMethod != null && !violationsPaymentMethod.isEmpty()){
						 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
						 }
					 
					// check whether the paymentType, Status already exists, if not,
					// assign the defaults
					if (paymentMethods.getPaymentType() == null) {
						paymentMethods.setPaymentType(PaymentType.BANK);
					}
		
					// when issuer add payment method for first time, make that payment
					// method as default one
					if (currentPaymentId == 0) {
						paymentMethods.setIsDefault(PaymentMethods.PaymentIsDefault.Y);
						paymentMethods.setStatus(PaymentStatus.Active);
					} else {
						// when issuer have more than one payment method, if check box checked
						if (chkboxAutoPay != null) {
							PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
							paymentMethodRequestDTO.setModuleID(issuerObj.getId());
							paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ISSUER);
							paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);
							
							String defaultPaymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, paymentMethodRequestDTO, String.class);
							Map<String, Object> defaultPaymentMethods = getPaymentMethodsMap(defaultPaymentMethodsString);
							List<PaymentMethods> defaultPaymentMethodList = (List<PaymentMethods>)defaultPaymentMethods.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
//							PaymentMethods defaultPaymentMethod = null;
							if(defaultPaymentMethodList != null && !defaultPaymentMethodList.isEmpty()){
								for (PaymentMethods defaultPaymentMethod: defaultPaymentMethodList){
									if(LOGGER.isDebugEnabled()){
										LOGGER.debug( "defaultPaymentMethod.getId() = " + defaultPaymentMethod.getId());
									}	
									paymentMethodRequestDTO = new PaymentMethodRequestDTO();
									paymentMethodRequestDTO.setId(defaultPaymentMethod.getId());
									paymentMethodRequestDTO.setModuleName(defaultPaymentMethod.getModuleName());
									paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.N);
									String resultString  = ghixRestTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
									if(LOGGER.isDebugEnabled()){
										LOGGER.debug("Modified Financial Information "+resultString);
									}	
								}
								
							}
							
							paymentMethods.setIsDefault(PaymentMethods.PaymentIsDefault.Y);
						} else if (paymentMethods.getIsDefault() == null) {
							paymentMethods.setIsDefault(PaymentMethods.PaymentIsDefault.N);
						}
		
						if (paymentMethods.getStatus() == null) {
							paymentMethods.setStatus(PaymentStatus.Active);
						}
					}
		
					paymentMethods.setModuleId(issuerObj.getId());
					// Module name set to Issuer
					paymentMethods.setModuleName(PaymentMethods.ModuleName.ISSUER);
					// saving the PaymentMethods info into the payment_methods table
					
					paymentMethodsString = ghixRestTemplate.postForObject(FinanceServiceEndPoints.SAVE_PAYMENT_METHOD, paymentMethods, String.class);
					
					XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
				
					PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
					paymentMethodsObj = paymentMethodResponse.getPaymentMethods();
					if(null != paymentMethodsObj){
						isFinancialInfoAdded = true;
					}
					encryptedIssuerId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(issuerObj.getId()));
					if(isFinancialInfoAdded){
						// send email notification to Issuer rep only for ID state exchange
						if(stateCode.equalsIgnoreCase("ID")){
							issuerService.sendFinancialNotificationToIssuerRep(issuerObj);
						}	
					}else{
						if (paymentId != null && !(paymentId.equals(PlanMgmtConstants.ZERO))) {
							String encryptedPaymentId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(paymentId));
							redirectUrl = "redirect:/admin/issuer/financial/info/edit/" + encryptedIssuerId + "?paymentId=" + encryptedPaymentId +"&update=YES";
							redirectAttributes.addFlashAttribute("payment_error", paymentMethodResponse.getErrMsg());
							redirectAttributes.addFlashAttribute(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ, paymentMethod);
						} else {
							redirectUrl = "redirect:/admin/issuer/financial/info/add/" + encryptedIssuerId;
							redirectAttributes.addFlashAttribute(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ, paymentMethod);
							redirectAttributes.addFlashAttribute("payment_error", paymentMethodResponse.getErrMsg());
						}
					}
		
				} catch (Exception ex) {
					LOGGER.error("Error in financial Info: ", ex);
					if (paymentId != null && !(paymentId.equals(PlanMgmtConstants.ZERO))) {
						String encryptedPaymentId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(paymentId));
						redirectUrl = "redirect:/admin/issuer/financial/info/edit/" + encryptedIssuerId + "?paymentId=" + encryptedPaymentId +"&update=YES";
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.ROUTING_NUMBER, paymentMethods.getFinancialInfo().getBankInfo().getRoutingNumber());
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ, paymentMethod);
					} else {
						redirectUrl = "redirect:/admin/issuer/financial/info/add/" + encryptedIssuerId;
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.PAYMENT_METHOD_INFO_OBJ, paymentMethod);
					}
					redirectAttributes.addFlashAttribute(PlanMgmtConstants.INVALID_ROUTING_NUMBER, ex.getLocalizedMessage());
				}
			}catch(Exception ex){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SAVE_FINANCIAL_INFORMATION_EXCEPTION.getCode(), ex, null, Severity.HIGH);
			}
			return redirectUrl;
		}
		
		
		
		private  PaymentMethods getPaymentMethods(String paymentMethodsString){
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			
			PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
			return paymentMethodResponse.getPaymentMethods();
		}
		
		private  Map<String, Object> getPaymentMethodsMap(String paymentMethodsString){
			XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			
			PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
			return paymentMethodResponse.getPaymentMethodMap();
		}

}
