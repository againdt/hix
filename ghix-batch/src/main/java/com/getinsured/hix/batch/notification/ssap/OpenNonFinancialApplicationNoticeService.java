package com.getinsured.hix.batch.notification.ssap;

import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface OpenNonFinancialApplicationNoticeService {

	String generateNoticeDocument(
			SsapApplication ssapApplication) throws NoticeServiceException;

}