
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>


<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	

<%@ page isELIgnored="false"%>


<div class="gutter10">
<div class="row-fluid">
	<ul class="breadcrumb txt-left">
	<li><a href="<c:url value="/shop/employer/" />">Employer Portal </a> <span class="divider">/</span></li>
	<li><a href="<c:url value="/shop/employer/viewpayment" />">Payments </a> <span class="divider">/</span></li>


	<li class="active"><spring:message code="label.viewEmployerInvoicePayments.title"/></li>
	</ul>
			<h1><spring:message code="label.viewEmployerInvoicePayments.title"/> <a href="#" rel="tooltip" data-placement="top"
			data-original-title="You can view all your invoices here. 
			Click on link or the gear button to view invoice details, showing your aggregated 
			premiums, payment dues and payment due date." 
			class="info"><i class="icon-question-sign customMargin"></i></a></h1>


</div>
</div>

<div class="row-fluid">

	<div class="span3" id="sidebar">
      	<div class="header">
			<h4 class="margin0"><spring:message code="label.finance.refineResults"/></h4>
		</div>
		<div class="gray graybg">
					<form class="form-vertical gutter10" id="frminvoiceinfolist"
			name="frminvoiceinfolist" action="viewinvoicepayments" method="POST">
			<df:csrfToken/>
            <input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="id" id="id" />
						<div class="control-group">
							<label class="control-label"><spring:message code="label.financeFilter.startDate"/></label>
							<div class="input-append date date-picker" id="date" data-date="">
								<input class="span10" type="text" name="startdate" id="mystartdate" value="${searchCriteria.startdate}" pattern="MM/DD/YYYY" onChange="setdate();"/> 
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<div id="mystartdate_error" class="help-inline"></div>
						</div>
						
						<div class="control-group">
							<label class="control-label"><spring:message code="label.financeFilter.endDate"/></label>

							<div class="input-append date date-picker" id="date" data-date="">
								<input class="span10" type="text" name="enddate" id="myenddate" value="${searchCriteria.enddate}" pattern="MM/DD/YYYY" onChange="setdate();"/> 
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<div id="myenddate_error" class="help-inline"></div>
						</div>
                       
                        <div class="control-group">
                            <label class="control-label"><spring:message  code='label.financeFilter.status'/></label>
                            <div class="controls">
                                <select  name="paidstatus" id="paidstatus" class="input-medium">
                                    <option value="">All</option>
                                 <c:forEach var="varPaidStatus" items="${paidStatusList}">
									<option <c:if test="${varPaidStatus == searchCriteria.paidstatus}"> SELECTED </c:if> value="${varPaidStatus}">${varPaidStatus}</option>
								</c:forEach>
                                    
                                </select>
                            </div> 
                        </div>
                        
                       <div class="txt-center">
							<input type="submit" name="submitBtn" id="submitBtn" Value="Go" title="Go" class="btn input-medium">
						</div>
				
					</form>

				</div>




	</div>


	<div class="span9" id="rightpanel">
    <!-- <h4 class="graydrkbg gutter10"></h4> -->

		<form id="frminvoicelist"
			name="frminvoicelist" action="employerinvoice" method="POST">
				<df:csrfToken/>
			<input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="id" id="id" />
			<input type="hidden" value="" name="startdate" id="startdate" />
		    <input type="hidden" value="" name="enddate" id="enddate" />
		    
			<c:choose>
				<c:when test="${resultSize >= 1}">
					<c:choose>
						<c:when test="${fn:length(invoicelist) > 0}">
								<table class="table table-striped">
									<thead>
										<tr class="header">
											<!--  span9 header -->
											<!-- <th><input type="checkbox"> </th> -->
											<th class="sortable" scope="col"><dl:sort
													title="Invoice" sortBy="invoiceNumber"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Premium Amount" sortBy="premiumThisPeriod"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Due on"
													sortBy="paymentDueDate"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Status" sortBy="paidStatus"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Amount Paid/Due" sortBy="totalAmountDue"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th></th>
										</tr>
									</thead>
									<jsp:useBean id="currDate" class="java.util.Date"/>
									<fmt:formatDate var="currentDate" value="${currDate}" pattern="yyyyMMdd"/>	
									<c:forEach items="${invoicelist}" var="invoice" varStatus="vs">
									<fmt:formatDate var = "payDueDate" value="${invoice.paymentDueDate}" pattern="yyyyMMdd" />
									<tr>
											<input type="hidden" value="<encryptor:enc value = "${invoice.id}"/>" name="invoiceid" id="invoiceid_${vs.index}" />
											<input type="hidden" value="" name="existinginvoiceid" id="existinginvoiceid" />
											<input type="hidden" value="<encryptor:enc value = "${invoice.invoiceNumber}"/>" name="invoiceNumber" id="invoiceNumber_${vs.index}" />
											<!-- <td><input type="checkbox"> </td> -->
											 <c:set var="ecmServerDocId" >
											 <c:if test="${invoice.ecmDocId != null && invoice.ecmDocId !=' '}">
											 <encryptor:enc value="${invoice.ecmDocId}" isurl="true"/>
											 </c:if>
											  </c:set>
											<td style ="white-space:nowrap"><a href="#" onClick="showdetail('${ecmServerDocId}');" id="edit_${vs.index}">${invoice.invoiceNumber}</a></td>

											<td><fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${invoice.premiumThisPeriod}" type="currency"/></td>
											
											<td><fmt:formatDate
													value="${invoice.paymentDueDate}"
													pattern="MM-dd-yyyy" /></td>
											
											<td> 						
											<c:choose>
											<c:when test="${invoice.paidStatus =='PARTIALLY_PAID'}">PARTIALLY PAID
											</c:when>
											<c:otherwise>${invoice.paidStatus}</c:otherwise></c:choose></td>
											
											<td>
											<fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${invoice.totalAmountDue - invoice.totalPaymentReceived}" type="currency"/></td>
											<td><div class="controls">
													<div class="dropdown">
														<a href="/page.html" data-target="#"
															data-toggle="dropdown" role="button" id="dLabel"
															class="dropdown-toggle"> <i
															class="icon-cog"></i> <b class="caret"></b>
														</a>
														<c:choose>
															<c:when test="${'DUE' == invoice.paidStatus}">
																<c:choose>
																	<c:when test="${(invoice.invoiceType == 'BINDER_INVOICE') && (currentDate gt payDueDate)}">
																		<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
																		class="dropdown-menu pull-right">																		
																		<li><a href="#" onClick="showdetail('${ecmServerDocId}');" id="edit_${vs.index}">View Detail</a></li>
																		</ul>
																		<c:set var="showMsg" value="true"/>
																	</c:when> 
																	 <c:otherwise>
																		<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
																		class="dropdown-menu pull-right">
																		<li><a href="#" onClick="makePayment(this);" id="edit_${vs.index}">Make Payment</a></li>
																		<li><a href="#" onClick="showdetail('${ecmServerDocId}');" id="edit_${vs.index}">View Detail</a></li>			
																		</ul>
																	 </c:otherwise>
																</c:choose>	
															</c:when> 
															 <c:otherwise>														
																<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
																class="dropdown-menu pull-right">
																<li><a href="#" onClick="showdetail('${ecmServerDocId}');" id="edit_${vs.index}">View Detail</a></li>				
																</ul>
															 </c:otherwise>
														</c:choose>	
													</div>
												</div></td>
										</tr>				
									</c:forEach>
								</table>
                                <div class="pagination">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
								</div>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
			</c:choose>
			<c:if test="${showMsg == 'true'}">
			<p class="alert txt-center"><strong>Payments cannot be made past the due date for a binder invoice.</strong></p>
			</c:if>
		</form>
	</div>
</div>



<div class="row-fluid">
	
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C)
					dependant on a particular Employer's eligibility to use the SHOP
					Exchange.</p>
			</div>
		</div>
	</div>

</div>
<!-- row-fluid -->


<script type="text/javascript">
$(document).ready(function(){
	$('.date-picker').datepicker({
	    autoclose: true,
	    format: 'mm/dd/yyyy'
	});
});

$.validator.addMethod("endDate", function(value, element) {
	var startdatevalue = $('#mystartdate').val();
	if (value == null || value.length <1)
		return true;
	return Date.parse(startdatevalue) < Date.parse(value);
});
	
function setdate(){
	$("#startdate").val($("#mystartdate").val());
	$("#enddate").val($("#myenddate").val());
}

function showdetail(docid) {
	if($.trim(docid).length==0 ||docid==""||docid==" "||docid==null){
		alert("Invoice is not yet generated, so please contact administrator");
	}
	else{
		var  contextPath =  "<%=request.getContextPath()%>";
	    var documentUrl = contextPath + "/shop/employer/viewinvoicedtl?documentId="+docid;
	    window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=600, height=280, top=200, left=200");
	}
 }
	
function makePayment(e) {	
	var startindex = (e.id).indexOf("_");
	var index = (e.id).substring(startindex, (e.id).length);
	//var empId = ${employerid};
	var invoiceNum = $("#invoiceNumber"+index ).val();	
	
	$.ajax({
		url: '<c:url value="/shop/employer/findPreviousActiveDueInProcessInvoice"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" /> 	</c:param> </c:url>',
		type: "POST",
		data: {employerId: $("#id").val(), invoiceNumber: invoiceNum},
		success: function(response, xhr){
			if(isInvalidCSRFToken(xhr))	    				
				return;
			if(response==""){
				$("#existinginvoiceid").val($("#invoiceid" + index).val());
				$("#frminvoicelist").attr("action", "reviewpayment");
				$("#frminvoicelist").submit();
			}
			else if(response.indexOf("Error") >=0){
				alert(response);
			}
			else{
				//alert("Please first make a payment for your previous due invoice "+response);
				alert(response);
			}
		}
	});
}

function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		alert($('Session is invalid').text());
		rv = true;
	}
	return rv;
}
	
var validator = $("#frminvoiceinfolist").validate({ 
		ignore: "",
		rules : {
			enddate : { endDate : true}
		}, 
		messages : {
			enddate : { endDate : "<span> <em class='excl'>!</em><spring:message  code='label.validateEndDate'/></span>"}
		},
		highlight: function (element, errorClass) {
			$('#myenddate').removeAttr('error');
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class', 'error');
		} ,
		
});

$('.info').tooltip()
</script>