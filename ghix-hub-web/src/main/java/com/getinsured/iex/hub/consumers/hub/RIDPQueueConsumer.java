package com.getinsured.iex.hub.consumers.hub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.rabbitmq.client.Channel;

public class RIDPQueueConsumer implements ChannelAwareMessageListener {

	@Autowired
	private WebServiceTemplate ridpServiceTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RIDPQueueConsumer.class);

	private void invokePrimaryRidp() throws HubMappingException,
			BridgeException, HubServiceException, InputDataValidationException {
		HubServiceBridge ridpPrimary = HubServiceBridge
				.getHubServiceBridge("RIDPPrimaryRequest");
		ridpPrimary.processInputParameter("personBirthDate", "08/10/1963");
		ridpPrimary.processInputParameter("personGivenName", "Erin");
		ridpPrimary.processInputParameter("personMiddleName", "J");
		ridpPrimary.processInputParameter("personSurName", "Rashid");
		ridpPrimary.processInputParameter("personSSNIdentification",
				"666-02-4882");
		ridpPrimary.processInputParameter("personNameSuffixText", "Jr.");
		ridpPrimary.processInputParameter("fullTelephoneNumber", "15555555555");
		ridpPrimary.processInputParameter("streetName", "302 E ADAMS ST");
		ridpPrimary.processInputParameter("locationCityName", "PITTSBURG");
		ridpPrimary.processInputParameter("locationStateUSPostalServiceCode",
				"KS");
		ridpPrimary.processInputParameter("locationPostalCode", "66762");
		ridpPrimary
				.processInputParameter("locationPostalExtensionCode", "5406");
		
		Object payLoad = ridpPrimary.getServiceHandler().getRequestpayLoad();
		Object x = ridpServiceTemplate.marshalSendAndReceive(payLoad);
		LOGGER.info("RIDP RESPONSE RECEIVED:" + ridpPrimary.handleResponse(x));

	}

	private void invokeSecondaryRidp() throws HubMappingException,
			BridgeException, HubServiceException, InputDataValidationException {
		HubServiceBridge ridpSecondary = HubServiceBridge
				.getHubServiceBridge("RIDPSecondaryRequest");
		ridpSecondary.processInputParameter("sessionIdentification",
				"121321233");
		Object payLoad = ridpSecondary.getServiceHandler().getRequestpayLoad();
		Object x = ridpServiceTemplate.marshalSendAndReceive(payLoad);
		LOGGER.info("RIDP RESPONSE RECEIVED:" + ridpSecondary.handleResponse(x));

	}

	@Override
	public void onMessage(Message arg0, Channel arg1) throws BridgeException {
		String threadId = Thread.currentThread().getName();
		String routingKey = arg0.getMessageProperties().getReceivedRoutingKey();
		if(routingKey == null){
			throw new IllegalArgumentException("No routing key available for incoming RIDP message, don't know how to process it");
		}
		LOGGER.info("Thread[" + threadId + "]Routing Key:"
				+ arg0.getMessageProperties().getReceivedRoutingKey()
				+ " Received Message:" + new String(arg0.getBody())
				+ " from channel:" + arg1.getChannelNumber());

		try {
			if(routingKey.equalsIgnoreCase("primary")){
				invokePrimaryRidp();
			}else if(routingKey.equalsIgnoreCase("secondary")){
				invokeSecondaryRidp();
			}else{
				throw new IllegalArgumentException("Routing Key:"+routingKey+" not supported");
			}
			
		} catch (SoapFaultClientException e) {
			LOGGER.error("Request failed with Reason:"
					+ e.getFaultStringOrReason() + " message:"
					+e.getMessage()
					+ "And fault code:"+e.getFaultCode(),e);
		} catch (HubServiceException e) {
			LOGGER.info("Exception", e);
		} catch (HubMappingException e) {
			LOGGER.info("Exception", e);
		} catch (InputDataValidationException ie) {
			LOGGER.info("Exception", ie);
		}
	}
}
