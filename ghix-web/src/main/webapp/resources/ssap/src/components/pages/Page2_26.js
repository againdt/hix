import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import {changeData} from '../../actions/actionCreators';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../constants/validationTypes';
import {VALIDATION_ERROR_MESSAGES} from '../../constants/validationErrorMessages';
import moment from 'moment';

// Components

// ActionCreators

// Utils

// Constants


/**
 *
 * @param {Object} props
 * @returns {React.Component} - Page 'Parent and Care Taker'
 *
 */
const Page2_26 = props => {

    const {
        data,
        form: {
            Form: {
                isFormSubmitted,
                isFormOnTopLevelValid
            }
        },
        householdMembers,
        familyStatus,
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        name,
        parentCaretaker,
        addressId,
    } = currentHhm;


    const onFieldChange = (newData) => {
        const newState = { ...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
        props.actions.changeData(newState);
    };
    const getChildren = () => {
        const dependantOfTheHousehold = [];
        if(familyStatus.relatives.length <= 0) return false;
        if(familyStatus.relatives.length > 0){
            familyStatus.relatives.forEach(function (dependant) {
                householdMembers.filter(member => {
                    if(member.personId === dependant.personId && moment().diff(member.dateOfBirth, 'years') < 19 && addressId === member.addressId) {
                        dependantOfTheHousehold.push(normalizeNameOfPerson(member.name));
                        return dependantOfTheHousehold;
                    }
                });
            });
        }
        return (
            dependantOfTheHousehold.map((child,i) => {
                return(
                    <Fragment key={i}>
                        <li>{child}</li>
                    </Fragment>
                );
            })
        );
    };

    return (
        <div id="fhParentCareTaker" className="fade-in">
            <Form>
                <Fragment>
                    <div className="subsection">
                        <ValidationErrorMessage
                            isVisible={isFormSubmitted && !isFormOnTopLevelValid}
                            messageText={VALIDATION_ERROR_MESSAGES.ANY_VALUE}
                            errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                        />
                        <UniversalInput
                            legend={{
                                legendText: <div>
                                    <p className="required-true">Is&nbsp;
                                        <strong>
                                            {normalizeNameOfPerson(name)}&nbsp;
                                        </strong>
                                    the main person taking care of any of these children?
                                    </p>
                                    <ul>
                                        { getChildren()}
                                    </ul>
                                </div>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02', 'required-false'],
                                ulClass: ['usa-width-one-whole'],
                                showRequiredIcon: false
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'parentCaretaker',
                                value: parentCaretaker,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: householdMembers[member],
                                fields: []

                            }}
                            inputActions={{
                                onFieldChange
                            }}
                            errorClasses={['margin-l-0 margin-l-20-ve']}
                        />

                    </div>
                </Fragment>
            </Form>
        </div>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,
    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_26.defaultProps = {
    name: 'Page2_26'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_26);
