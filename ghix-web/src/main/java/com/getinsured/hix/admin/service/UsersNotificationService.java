package com.getinsured.hix.admin.service;

import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;


/**
 * 
 * @author Satyakam_k
 *
 */
public interface UsersNotificationService 
{
	void sendEmailNotification(String notificationType,String recipient,String mainContent,String subject,String userName) throws NotificationTypeNotFound;
}
