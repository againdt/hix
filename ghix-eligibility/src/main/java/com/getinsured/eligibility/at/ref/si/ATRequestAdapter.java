package com.getinsured.eligibility.at.ref.si;

import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.at.server.endpoint.ATContextHolder;
import com.getinsured.eligibility.enums.AtResponseTypeEnum;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 *
 */
public abstract class ATRequestAdapter {
	private static final Logger LOGGER = Logger.getLogger(ATRequestAdapter.class);

	@Autowired
	private GIWSPayloadRepository giwsPayloadRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private ExceptionUtil exceptionUtil;

	@SuppressWarnings("unchecked")
	public Object transformToATRequest(Message<String> message) throws Exception {
		ReferralResponse referralResponse = new ReferralResponse();
		long startTime = System.nanoTime();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("ATRequestAdapter transformToATRequest starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
			int giwsPayloadId = 0;
			AccountTransferRequestPayloadType payload = ATContextHolder.getAccountTransferRequestPayloadType();
			if(input.getData().get(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID) != null) 
			{
				giwsPayloadId = (int) input.getData().get(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID);
				LOGGER.info(" GIWSPayload -  " + giwsPayloadId);
			}
			if (payload == null && giwsPayloadId>0) {
				final GIWSPayload giwsPayload = giwsPayloadRepository.findOne(giwsPayloadId);
				if(!"NV".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)))
				{
					String requestBody = ReferralUtil.extractSoapBoady(giwsPayload.getRequestPayload());
					payload = ReferralUtil.unmarshal(requestBody);
				}
				else
				{
					payload = ReferralUtil.getAccountTransferRequestObject(giwsPayload.getRequestPayload());
				}
				
			}

			/*if ("NM".equals(getStateCode())) {
				isFullDetermination = ReferralUtil.isFullDetermination(payload, getStateCode());
				atResponseType = isFullDetermination ? AtResponseTypeEnum.FULL_DETERMINATION.toString() : AtResponseTypeEnum.ASSESSMENT.toString();
			} else {
				boolean isUpdated = ReferralUtil.isFullDetermination(payload, getStateCode());
				atResponseType = isUpdated ? AtResponseTypeEnum.UPDATED.toString() : AtResponseTypeEnum.FULL_DETERMINATION.toString();
				isFullDetermination = true;
			}*/

			final String caseNumber = fetchCaseNumber(ssapApplicationId);
			accountTransferRequestDTO.setAccountTransferRequestPayloadType(payload);
			accountTransferRequestDTO.setGiwsPayloadId(giwsPayloadId);
			accountTransferRequestDTO.setCaseNumber(caseNumber);
			accountTransferRequestDTO.setFullDetermination(true);
			accountTransferRequestDTO.setAtResponseType(AtResponseTypeEnum.UPDATED.toString());
			accountTransferRequestDTO.setCompareToApplicationId((long) input.getData().get(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID));
			boolean cmrAutoLinking = false;
			if (input.getData().get(ReferralProcessingConstants.REFERRAL_AUTOLINKING) != null)
			{
				cmrAutoLinking = ReferralUtil.convertToBoolean(input.getData().get(ReferralProcessingConstants.REFERRAL_AUTOLINKING));
			}
			accountTransferRequestDTO.setCmrAutoLinking(cmrAutoLinking);
			accountTransferRequestDTO.setApplicationsWithSameId(input.getData().get(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID) == null ? null : (List<ApplicationsWithSameIdDTO>) input.getData().get(
			        ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID));
			accountTransferRequestDTO.setRequester(input.getData().get(ReferralProcessingConstants.PROCESSOR_REQUESTER) != null ? (String)input.getData().get(ReferralProcessingConstants.PROCESSOR_REQUESTER) : "");
			populateAccountTransferProperties(accountTransferRequestDTO, input);

			LOGGER.info("ATRequestAdapter transformToATRequest ends ");

			if (LOGGER.isInfoEnabled()){
				long executionTime = System.nanoTime() - startTime;
				LOGGER.info("Time spent in AT Request Adapter -- "+ executionTime + " ns");
			}

			return accountTransferRequestDTO;
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_AT_ADAPTER).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_AT_ADAPTER);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
			return EligibilityUtils.marshal(referralResponse);
		}
		finally
		{
			try {
				ATContextHolder.removeAccountTransferRequestPayloadType();
            } catch (Exception e2) {
            	/**Eat the Exception*/
            }
		}

	}

	abstract void populateAccountTransferProperties(AccountTransferRequestDTO accountTransferRequestDTO, ReferralResponse input);

	abstract String getStateCode();

	private String fetchCaseNumber(long ssapApplicationId) {
		return ssapApplicationRepository.fetchCaseNumberById(ssapApplicationId);
	}

}
