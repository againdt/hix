package com.getinsured.ssap.model.hub.vlp.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.ssap.model.hub.vlp.VlpActionResult;

/**
 * Describes response for retrieve step 2 case resolution request.
 *
 * @author Yevgen Golubenko
 * @since 2019-07-26
 */
public class VlpStepTwoResolutionResponse {
  /*
   * {
   *  "RetriveStep2ResponseTypeResponseSet":{},
   *  "ResponseMetadata":{
   *    "ResponseCode": "HE010043",
   *     "ResponseDescriptionText":"Additional (Step 2) or Third (Step 3) verification results for the requested case are not available at this time."
   *   }
   * }
   */
  @JsonProperty("RetriveStep2ResponseTypeResponseSet")
  private Object responseType;

  @JsonProperty("ResponseMetadata")
  private ResponseMetadata responseMetadata;

  private VlpActionResult actionResult;

  private ResponseCode responseCode;

  private String remoteUrl;

  public Object getResponseType() {
    return responseType;
  }

  public void setResponseType(Object responseType) {
    this.responseType = responseType;
  }

  public ResponseMetadata getResponseMetadata() {
    return responseMetadata;
  }

  public void setResponseMetadata(ResponseMetadata responseMetadata) {
    this.responseMetadata = responseMetadata;
  }

  public VlpActionResult getActionResult() {
    return actionResult;
  }

  public void setActionResult(VlpActionResult actionResult) {
    this.actionResult = actionResult;
  }

  public ResponseCode getResponseCode() {
    return responseCode;
  }

  public void setResponseCode(ResponseCode responseCode) {
    this.responseCode = responseCode;
  }

  public String getRemoteUrl() {
    return remoteUrl;
  }

  public void setRemoteUrl(String remoteUrl) {
    this.remoteUrl = remoteUrl;
  }
}
