<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	
<div class="gutter10">
	<div class="page-header">
		<h1>Employer Application: <small>Getting Started</small></h1>
	</div>

	
	<div class="row-fluid">		
		<div class="span3">
		    <ul class="nav nav-list">
			    <li class="nav-header">Learn More </li>
			    <li><a href="http://www.healthcare.gov/news/factsheets/2011/08/small-business.html">Small Businesses and the Affordable Care Act</a></li>
			    <li><a href="http://www.irs.gov/uac/Small-Business-Health-Care-Tax-Credit-for-Small-Employers">Tax Credit for Small Employers</a></li>
		    </ul>	
		</div>

		<div class="span9">
			<h3>Who is Eligible</h3>
					<ul>
						<li>Employer with fewer than 50 full-time employees (30+ hours/week)</li>
						<li>Employers that will offer coverage to all full-time employees</li>
						<li>Employers with worksites that are located within California</li>
					</ul>	
				<div>
					<h3>How to Apply</h3>
					<p>If you are authorized to enroll your company in benefits program, you can begin the application process now. Start by creating an account for your small business.</p>
				</div>
				<form class="form-horizontal" id="frmstart" name="frmstart" action="register" method="POST">
					<df:csrfToken/>
					<input type="hidden" name="role.id" id="role.id" value="${roleid}">
					<div class="profile">
						<div class="control-group">
							<label for="email" class="control-label required">Email Address <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="user.email" id="email" class="input-xlarge" size="30">
								<div id="email_error"></div>			
							</div>
						</div>
						<div class="control-group">
							<label for="password" class="control-label required">Password <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="password" name="user.password" id="password" class="input-xlarge" maxlength="20" size="30" autocomplete="off">	
								<p><small>8-20 characters, include at least one uppercase and one lowercase letter and a number.</small></p>		
								<div id="password_error"></div>
							</div>
						</div>
						<div class="control-group">
							<label for="retypePassword" class="control-label required">Confirm Password <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="password" name="retypePassword" id="retypePassword" maxlength="20" class="input-xlarge" size="30" autocomplete="off">	
								<div id="retypePassword_error"></div>									
							</div>
						</div>
					</div><!-- profile -->
					<div class="form-actions">
						<input type="submit" name="submitBtn" id="submitBtn" Value="Create Account" class="btn btn-primary  pull-right"  title="Create Account"><br /><br />
						<p class="pull-right"><a href="<c:url value="/account/user/login" />" >Already have an account? Log in</a></p>			
					</div>
				</form>
			</div>
	</div>
</div>
<script type="text/javascript">

jQuery.validator.addMethod("complexity", function(value, element, param) {
	return /(?=^.{8,20}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=^.*[^\s].*$).*$/.test(value); 
});

var validator = $("#frmstart").validate({ 
	onkeyup: false,
	rules : {
			'user.email': { required : true, email: true,  remote:{ url:"<c:url value='/account/user/isUserNameUnique' />", 
																	data: {  userName: function() { return $("#email").val(); }  }
			                                                       }
			},
			'user.password' : { required: true, complexity :true},
			'retypePassword' : { required: true,  equalTo: "#password" }
	},
	messages : {
		'user.email': { required : "<span> <em class='excl'>!</em>Email is required.</span>",
	     				email : "<span> <em class='excl'>!</em>Please enter valid email.</span>",
	     				remote : "<span> <em class='excl'>!</em>Email is already in use.</span>"
	    			  },
	    'user.password': { required : "<span> <em class='excl'>!</em>Password is required.</span>",
     		              complexity : "<span> <em class='excl'>!</em>Password must be 8-20 characters include at least one upper case and one lower case letter and a number.</span>"
                         },
	    'retypePassword': { required : "<span> <em class='excl'>!</em>Confirm Password is required.</span>",
	    					equalTo : "<span> <em class='excl'>!</em>Password is not same as Confirm password.</span>"
                        }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	}
});
	
</script>	