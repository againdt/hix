package com.getinsured.hix.entity.assister.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.assister.repository.IAssisterLanguagesRepository;
import com.getinsured.hix.entity.assister.service.AssisterLanguagesService;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;

/**
 * Implements {@link AssisterLanguagesService} to perform Assister Languages
 * related operations like find,save,update etc.
 * 
 */
@Service("assisterLanguagesService")
@Transactional
public class AssisterLanguagesServiceImpl implements AssisterLanguagesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterLanguagesServiceImpl.class);
	
	@Autowired
	private IAssisterLanguagesRepository assisterLanguagesRepository;

	/**
	 * @see AssisterLanguagesService#findByAssister(Assister)
	 * 
	 * @param assister
	 *            {@link Assister}
	 * 
	 * @return {@link AssisterLanguages}
	 */
	@Override
	@Transactional(readOnly = true)
	public AssisterLanguages findByAssister(Assister assister) {
		
		LOGGER.info("findByAssister: START");
		LOGGER.info("findByAssister: END");
		
		return assisterLanguagesRepository.findByAssisterId(assister.getId());
	}

	/**
	 * @see AssisterLanguagesService#update(AssisterLanguages)
	 * 
	 * @param assisterLanguages
	 *            {@link AssisterLanguages}
	 * 
	 * @return {@link AssisterLanguages}
	 */
	@Override
	@Transactional
	public AssisterLanguages update(AssisterLanguages assisterLanguages) {
		
		LOGGER.info("update: START");
		
		AssisterLanguages existingAssisterLanguages = assisterLanguagesRepository.findOne(assisterLanguages.getId());
		if (existingAssisterLanguages != null) {
			assisterLanguagesRepository.save(assisterLanguages);
		}
		
		LOGGER.info("update: END");
		
		return existingAssisterLanguages;
	}

	/**
	 * @see AssisterLanguagesService#saveAssisterLanguages(AssisterLanguages)
	 * 
	 * @param assisterLanguages
	 *            {@link AssisterLanguages}
	 * 
	 * @return {@link AssisterLanguages}
	 */
	@Override
	@Transactional
	public AssisterLanguages saveAssisterLanguages(AssisterLanguages assisterLanguages) {
		
		LOGGER.info("saveAssisterLanguages: START");
		LOGGER.info("saveAssisterLanguages: END");
		
		return assisterLanguagesRepository.save(assisterLanguages);
	}

}
