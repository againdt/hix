package com.getinsured.eligibility.ssap.integration.household.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.timeshift.TSCalendar;

@Component(value="nmecRequestServiceActivator")
public class NMECRequestServiceActivator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NMECRequestServiceActivator.class);
	
	@SuppressWarnings("unchecked")
	@ServiceActivator
	public String receive(Message<String> message) throws IOException, ParseException {
		String filteredSsapJSON = message.getHeaders().get(SSAPIntegrationConstants.FILTERED_SSAP_JSON_HEADER).toString();
		List<SsapApplicantEvent> ssapApplicantEvents = (List<SsapApplicantEvent>) message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICANT_DATA_HEADER); 
		SsapApplicantEvent ssapApplicantEvent = ssapApplicantEvents.get(0); 
		JSONParser parser = new JSONParser();
		String applicationId = message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER).toString();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("applicationId", Long.parseLong(applicationId));
		JSONObject ssapJSONObject = (JSONObject)parser.parse(filteredSsapJSON);
		ssapJSONObject = (JSONObject) ssapJSONObject.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION);
		JSONArray taxHouseholds = (JSONArray)ssapJSONObject.get(SSAPIntegrationConstants.TAX_HOUSEHOLD);
		JSONArray updatedTaxHouseholds = new JSONArray();
		
		for(Object taxHousehold : taxHouseholds) {
			JSONObject household = (JSONObject) taxHousehold;
			JSONArray householdMembers = (JSONArray) household.get(SSAPIntegrationConstants.HOUSEHOLD_MEMBER);
			JSONArray updatedHouseholdMembers = new JSONArray();
			for(Object householdMember : householdMembers) {
				JSONObject member = (JSONObject) householdMember;
				// Remove the applicants who did not provide SSN
				JSONObject socialSecurityCard = (JSONObject)member.get(SSAPIntegrationConstants.SOCIAL_SECURITY_CARD);
                if (null != socialSecurityCard
                        && null != socialSecurityCard.get(SSAPIntegrationConstants.SOCIAL_SECURITY_NUMBER)
                        && StringUtils.isNotBlank(socialSecurityCard.get(SSAPIntegrationConstants.SOCIAL_SECURITY_NUMBER).toString())) {
    				JSONObject insuranceSection = new JSONObject();
    				// As the coverage start date and end date is set at the application event level 
    				// and all the applicant events belong the same application event, 
    				// we take one applicant event, extract application event from it and then use the coverage start and end date.
    				Calendar coverageStartDate = TSCalendar.getInstance();
    				coverageStartDate.setTimeInMillis(ssapApplicantEvent.getSsapAplicationEvent().getEnrollmentStartDate().getTime());
    				Calendar coverageEndDate = TSCalendar.getInstance();
    				coverageEndDate.setTimeInMillis(ssapApplicantEvent.getSsapAplicationEvent().getEnrollmentEndDate().getTime());
    				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    				insuranceSection.put("insurancePolicyEffectiveDate", df.format(coverageStartDate.getTime()));
    				insuranceSection.put("insurancePolicyExpirationDate", df.format(coverageEndDate.getTime()));
    				member.put("insurance", insuranceSection);
    				if(null != member && null != member.get(SSAPIntegrationConstants.GENDER)) {
    					String gender =  member.get(SSAPIntegrationConstants.GENDER).toString();
    					if(gender.equalsIgnoreCase("MALE")) {
    						member.put(SSAPIntegrationConstants.GENDER, "M");
    					} 
    					if(gender.equalsIgnoreCase("FEMALE")) {
    						member.put(SSAPIntegrationConstants.GENDER, "F");
    					}
    				}
   				
                    JSONObject nameObject = (JSONObject)member.get("name");
                    if(null != nameObject && nameObject.get("middleName") != null &&
												StringUtils.isBlank(nameObject.get("middleName").toString()))
                    {
                        nameObject.remove("middleName");
                    }
    				updatedHouseholdMembers.add(member);
                }
			}
			
			if(updatedHouseholdMembers.size() > 0) {
				household.put(SSAPIntegrationConstants.HOUSEHOLD_MEMBER, updatedHouseholdMembers);
			} else {
				LOGGER.error("No household members to verify NMEC");
				throw new IllegalStateException("No household members to verify NMEC");
			}
			updatedTaxHouseholds.add(taxHouseholds);
		}
		ssapJSONObject.put(SSAPIntegrationConstants.TAX_HOUSEHOLD, taxHouseholds);
		jsonObj.put("payload", ssapJSONObject);
		LOGGER.debug(">>>>>>>>>>>>>>>>>> NMEC Request: " + jsonObj.toJSONString());
		return jsonObj.toJSONString();
	}
}
