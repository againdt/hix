

// For running on 8888 only

export const BLOOD_RELATIONSHIP_CODES = {
    SPOUSE: '01',
    PARENT_OF_CHILD: '03',
    GRANDPARENT: '04',
    GRANDCHILD: '05',
    UNCLE_OR_AUNT: '07',
    PARENT_OF_ADOPTED_CHILD: 'G8',
    ADOPTED_CHILD: '09',
    WARD_OF_COURT_APPOINTED_GUARDIAN: '15',
    STEPPARENT: '16',
    STEPCHILD: '17',
    CHILD: '19',
    COURT_APPOINTED_GUARDIAN: '31',
    SIBLING: '14',
    SELF: '18',
    FORMER_SPOUSE: '25'
};

// For running on 8888 only

export const BLOOD_RELATIONSHIP_TEXTS = {
    '01': 'Spouse',
    '03': 'Parent of Child',
    '04': 'Grandparent',
    '05': 'Grandchild',
    '07': 'Uncle or Aunt',
    'G8': 'Parent of adopted child',
    '09': 'Adopted child',
    '15': 'Ward of court appointed guardian',
    '16': 'Stepparent',
    '17': 'Stepchild',
    '19': 'Child',
    '31': 'Court Appointed or Live-In Guardian',
    '14': 'Sibling',
    '18': 'Self',
    '25': 'Former Spouse'
};

// For running on 8888 only

export const BLOOD_RELATIONSHIP_MIRROR_CODES = {
    '01': '01',
    '03': '19',
    '04':'05',
    '05': '04',
    '07': '07',
    'G8': '09',
    '09': 'G8',
    '15': '31',
    '16': '17',
    '17': '16',
    '19': '03',
    '31': '15',
    '14': '14',
    '18': '18',
    '25': '25'
};




const SPOUSE = 'SPOUSE';
const PARENT_OF_CHILD = 'PARENT';
const PARENT_OF_ADOPTED_CHILD = 'PARENT_OF_ADOPTED_CHILD';
const ADOPTED_CHILD = 'ADOPTED_CHILD';
const WARD_OF_COURT_APPOINTED_GUARDIAN = 'WARD_OF_COURT_APPOINTED_GUARDIAN';
const STEPPARENT = 'STEPPARENT';
const STEPCHILD = 'STEPCHILD';
const CHILD = 'CHILD';
const COURT_APPOINTED_GUARDIAN = 'COURT_APPOINTED_GUARDIAN';
const SIBLING = 'SIBLING';
const SELF = 'SELF';
const GRANDPARENT = 'GRANDPARENT';
const GRANDCHILD = 'GRANDCHILD';
const FORMER_SPOUSE = 'FORMER_SPOUSE';

export default {
    SPOUSE,
    PARENT_OF_CHILD,
    PARENT_OF_ADOPTED_CHILD,
    ADOPTED_CHILD,
    WARD_OF_COURT_APPOINTED_GUARDIAN,
    STEPPARENT,
    STEPCHILD,
    GRANDPARENT,
    GRANDCHILD,
    FORMER_SPOUSE,
    CHILD,
    COURT_APPOINTED_GUARDIAN,
    SIBLING,
    SELF
};
