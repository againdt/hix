package com.getinsured.hix.entity.assister.controller;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.entity.assister.service.AssisterDesignateService;
import com.getinsured.hix.entity.assister.service.AssisterLanguagesService;
import com.getinsured.hix.entity.assister.service.AssisterService;
import com.getinsured.hix.entity.enrollmententity.service.SiteService;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.AssisterNumber;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.DesignateAssister.Status;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.thoughtworks.xstream.XStream;

/**
 * The controller class that receives Rest requests from ghix-web. It then
 * performs the requested operation and returns response
 */
@Controller
@RequestMapping(value = "/entity/assister")
public class AssisterController {

	private static final String HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER = "hasPermission(#model, 'ASSISTER_EDIT_ASSISTER')";
	private static final String HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL = "hasPermission(#model, 'ASSISTER_VIEW_INDIVIDUAL')";
	private static final String HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER = "hasPermission(#model, 'ASSISTER_VIEW_ASSISTER')";
	private static final String RETURNING_EE_LIST_TO_GHIX_WEB = "Returning EE list to ghix-web";
	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterController.class);
	private static final String INACTIVE = "InActive";
	private static final String PENDING = "Pending";
	

	@Autowired
	private AssisterService assisterService;

	@Autowired
	private AssisterDesignateService assisterDesignateService;

	@Autowired
	private AssisterLanguagesService assisterLanguagesService;
	
	@Autowired
	private SiteService subSiteService;
	
	@Autowired
	private ZipCodeService zipCodeService;
	
	@Autowired
	private EntityUtils entityUtils;
	
	/**
	 * To test {@link AssisterController}
	 * 
	 * @return welcome message
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		
		LOGGER.info("welcome: START");
		
		LOGGER.info("Welcome to Enrollment Entity module");
		
		LOGGER.info("welcome: END");
		
		return "Welcome to Enrollment Entity module";
	}

	/**
	 * Invokes Assister service and Assister Languages service to save assister
	 * registration information
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating assister details
	 * @return marshaled assister response DTO
	 * @throws JsonProcessingException 
	 */

	@RequestMapping(value = "/saveassisterdetails", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String saveAssisterDetails(@RequestBody String requestXML) throws JsonProcessingException {
		
		LOGGER.debug("saveAssisterDetails: START");
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		String response = null;
		LOGGER.info("Save Assister Detail Starts : Received Rest call from ghix-web.");

		try {
			
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
			AssisterRequestDTO assisterRequestDTO = reader.readValue(requestXML);
	
			Assister assister = assisterRequestDTO.getAssister();
			AssisterLanguages assisterLanguages = assisterRequestDTO.getAssisterLanguages();
			if(null != assister && assister.getMailingLocation()!=null && (assister.getMailingLocation().getLat()==0.0) && (assister.getMailingLocation().getLon()==0.0)) {
				ZipCode zipCode=zipCodeService.findByZipCode(assister.getMailingLocation().getZip().toString());
				
				if(zipCode!=null){
					assister.getMailingLocation().setLat(zipCode.getLat());
					assister.getMailingLocation().setLon(zipCode.getLon());
				}
			}
			if (assister != null && assisterLanguages != null) {
				if (assister.getId() != 0) {
					assister = assisterService.updateAssisterInformation(assister, assisterRequestDTO.isCertificationNumberUpdate());
					// assister =
					// assisterService.saveAssister(assisterRequestDTO.getAssister(),
					// true);
				} else {
					Assister newassister = assisterRequestDTO.getAssister();
					newassister.setStatus(INACTIVE);
					newassister.setCertificationStatus(PENDING);
					newassister.setStatusChangeDate(new TSDate());
					assister = assisterService.saveAssister(newassister);
				}
				assisterLanguages.setAssister(assister);
	
				assisterLanguages = assisterLanguagesService.saveAssisterLanguages(assisterLanguages);
	
				assisterResponseDTO.setAssister(assister);
				assisterResponseDTO.setAssisterLanguages(assisterLanguages);
	
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} else {
				LOGGER.error("Save Assister Detail : Invalid input. Unable to save Assister Detail");
				assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
		} catch (Exception exception) {
			LOGGER.error("Save Assister Detail : Exception occurred while saving Assister Detail : "
					, exception);
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
		}

		LOGGER.info("Save Assister Detail Ends.");

		
		
		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for save assister." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.debug("saveAssisterDetails: END");
		return response;
	}

	/**
	 * Invokes Assister service update assister information
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating assister details
	 * @return marshaled assister response DTO
	 */

	@RequestMapping(value = "/updateassisterinformation", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String updateAssisterInformation(@RequestBody String request) {
		
		LOGGER.info("updateAssisterInformation: START");
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();

		LOGGER.debug("Update Assister Detail Starts : Received Rest call from ghix-web.");
		String response = null;
		
		try {
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
			AssisterRequestDTO assisterRequestDTO = reader.readValue(request);
			Assister assister = assisterRequestDTO.getAssister();

			if (assister != null) {
					assister = assisterService.updateAssisterInformation(assisterRequestDTO.getAssister(), true);
					assisterResponseDTO.setAssister(assister);

					assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
					assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} else {
				LOGGER.error("Update Assister Detail : Invalid input. Unable to update Assister Detail");

				assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
		} catch (Exception exception) {
			LOGGER.error("Save Assister Detail : Exception occurred while saving Assister Detail : "
					, exception);
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
		}

		LOGGER.debug("Update Assister Detail Ends.");
		
		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for update assister." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("updateAssisterInformation: END");
		
		return response;
	}

	/**
	 * Invokes Assister service and Assister Languages service to get Assister
	 * details
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating assister id
	 * @return marshaled assister response DTO
	 */
	//@GiAudit(transactionName = "Get Assister Deatil By Identifier", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/geteassisterdetailbyid", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String getAssisterDetailsById(@RequestBody AssisterRequestDTO assisterRequestDTO) {
		
		LOGGER.debug("getAssisterDetailsById: START");
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		Assister assister = null;
		AssisterLanguages assisterLanguages = null;
		String response = null;
		//LOGGER.info("Get Assister Detail Starts : Received Rest call from ghix-web. AssisterRequestDTO received in Request: " + assisterRequestDTO);

		Integer assisterId = assisterRequestDTO.getId();
		Integer siteId = assisterRequestDTO.getSiteId();
		//LOGGER.debug("Assister Id : {}", assisterId);

		if (assisterId != null && assisterId.intValue() != 0) {
			try {
				assister = assisterService.findById(assisterId);
				//LOGGER.debug("Assiter record based on assister id : {}", assister);
				
				if (assister != null) {
					assisterLanguages = assisterLanguagesService.findByAssister(assister);

					if(siteId != null && siteId != 0){
						Site site = subSiteService.findByEESiteId(siteId);
						assister.setPrimaryAssisterSite(site);
					}
				}

				//LOGGER.debug("Setting assister and assister languages in response object");
				assisterResponseDTO.setAssister(assister);
				assisterResponseDTO.setAssisterLanguages(assisterLanguages);

				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);

			} catch (Exception exception) {
				LOGGER.error("Get Assister Detail : Exception occurred while getting Assister Detail : "
						, exception);

				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				assisterResponseDTO.setResponseDescription(exception.getMessage());
			}
		} else {
			LOGGER.error("Get Assister Detail : Invalid input. Unable to Get Assister Detail");

			assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
			assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
		}

		//LOGGER.info("Get Assister Detail Ends : Get Assister Detail Response : " + assisterResponseDTO);

		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for get assister details." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.debug("getAssisterDetailsById: END");
		
		return response;
	}

	/**
	 * Invokes Assister service to get list of Assisters against given
	 * enrollment entity
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating enrollment entity details
	 * @return marshaled assister response DTO
	 */

	@RequestMapping(value = "/getlistofassistersbyenrollmententity", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String getListOfAssistersByEnrollmentEntity(@RequestBody String request) {

		LOGGER.debug("getListOfAssistersByEnrollmentEntity: START");
		
		LOGGER.info("Get List of Assisters By Enrollment Entity Starts : Received Rest call from ghix-web.");
		
		Map<Integer, Integer> assisterClientCountMap = new HashMap<Integer, Integer>();
		List<Assister> listOfAssisters = null;
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		int totalClientsOfAsssister = -1;
		int totalAssistersCount = 0;
		List<DesignateAssister> designateAssistersList =  null;
		String response = null;
		try {
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
			AssisterRequestDTO assisterRequestDTO = reader.readValue(request);

			EnrollmentEntity enrollmentEntity = assisterRequestDTO.getEnrollmentEntity();
			
			if (enrollmentEntity != null) {
				if (!assisterRequestDTO.isAdmin()) {
					totalAssistersCount = assisterService
							.findByAssisterEnrollmentEntity(assisterRequestDTO,
									enrollmentEntity.getId());
				} else {
					listOfAssisters = assisterService
							.findByEnrollmentEntity(enrollmentEntity);
				}
				if ( listOfAssisters != null && !listOfAssisters.isEmpty() ) {
					
					for (Assister assister : listOfAssisters ) {
						
						totalClientsOfAsssister = 0;
						designateAssistersList =  null;
						
						if ( assister != null && assister.getId() > 0 ) {
						
							designateAssistersList = assisterService.findDesignateAssisterByAssisterId(assister.getId());
							
							if (designateAssistersList != null && !designateAssistersList.isEmpty() ) {
								
								totalClientsOfAsssister = designateAssistersList.size();
							}

							assisterClientCountMap.put(assister.getId(),totalClientsOfAsssister);
						}
					}
				}

				assisterResponseDTO.setAssisterClientCountMap(assisterClientCountMap);

				assisterResponseDTO.setListOfAssisters(listOfAssisters);
				assisterResponseDTO.setTotalAssisters(totalAssistersCount);
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} else {
				LOGGER.error("Get List of Assisters By Enrollment Entity : Invalid input. Unable to Get List of Assisters for Enrollment Entity");

				assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
		} catch (Exception exception) {
			LOGGER.error("Get List of Assisters By Enrollment Entity : Exception occurred while getting List of Assisters : "
					, exception);

			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
		}
		

		LOGGER.info("Get List of Assisters By Enrollment Entity Ends");

		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for list of assister by enrollment enttiy." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.debug("getListOfAssistersByEnrollmentEntity: END");
		
		return response;
	}

	/**
	 * Invokes Assister service and Assister Languages service to get Assister
	 * details By User
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating user id
	 * @return marshaled assister response DTO
	 */

	@RequestMapping(value = "/getassisterdetailbyuserid", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String getAssisterDetailByUserId(@RequestBody AssisterRequestDTO assisterRequestDTO) {
		
		LOGGER.info("getAssisterDetailByUserId: START");
		
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		Assister assister = null;
		AssisterLanguages assisterLanguages = null;

		LOGGER.debug("Get Assister Detail By User Id Starts : Received Rest call from ghix-web.");

		Integer userId = assisterRequestDTO.getUserId();

		if (userId != null && userId.intValue() != 0) {
			try {
				
				if(EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
					LOGGER.info("getAssisterDetailByUserId: StateCode: "+EntityConstants.CA_STATE_CODE);
					assister = assisterService.getAssisterRecordByUserId(userId);
					LOGGER.info("getAssisterDetailByUserId: assisterId: "+assister.getId());
				}else{
					assister = assisterService.findByUserId(userId);
				}

				LOGGER.debug("Assister record based on user id.");
				if (assister == null) {
					LOGGER.debug("Assister is null hence retrieving based on record id");
					// In case assister is not yet associated with user then we
					// have to find assister based on record id i.e.
					// asisterId i.e. set as userId in this flow
					assister = assisterService.findById(userId);
					LOGGER.debug("Assister record based on record id.");
				}

				if (assister != null) {
					assisterLanguages = assisterLanguagesService.findByAssister(assister);
				}

				LOGGER.debug("Setting assister and assister languages in response object");
				assisterResponseDTO.setAssister(assister);
				assisterResponseDTO.setAssisterLanguages(assisterLanguages);

				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);

			} catch (Exception exception) {
				LOGGER.error("Get Assister Detail By User Id : Exception occurred while getting Assister Detail : "
						, exception);

				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				assisterResponseDTO.setResponseDescription(exception.getMessage());
			}
		} else {
			LOGGER.error("Get Assister Detail By User Id : Invalid input. Unable to Get Assister Detail");

			assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
			assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
		}

		LOGGER.debug("Get Assister Detail By User Id Ends.");
		

		LOGGER.info("getAssisterDetailByUserId: END");
		
		return xstream.toXML(assisterResponseDTO);
	}

	@RequestMapping(value = "/testStub", method = RequestMethod.GET)
	public void testDelegationFlow() {
		
		LOGGER.info("testDelegationFlow: START");
		
		LOGGER.info("Invoked REST url for IND54");

		Assister assister = assisterService.findById(1);
		LOGGER.info("Found Assister in DB");

		assisterService.update(assister, true);
		LOGGER.info("Assister has been successfully updated");
		
		LOGGER.info("testDelegationFlow: END");
	}

	@RequestMapping(value = "/populatenavigationmenumap", method = RequestMethod.POST)
	@ResponseBody
	public String populateNavigationMenuMap(@RequestBody String requestXML) {
		
		LOGGER.info("populateNavigationMenuMap: START");
		
		LOGGER.debug("Populate Left Navigation Map Starts : Received Rest call from ghix-web.");
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		Map<String, Boolean> navigationMenuMap = new HashMap<String, Boolean>();
		String response = null;
		try {
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
			AssisterRequestDTO assisterRequestDTO = reader.readValue(requestXML);
			Assister assister = assisterRequestDTO.getAssister();

			if (assister != null) {
				navigationMenuMap.put("ASSISTER_INFO", true);
				navigationMenuMap.put("ASSISTER_PROFILE", true);
				navigationMenuMap.put("ASSISTER_CERTIFICATION", true);
				navigationMenuMap.put("ASSISTER_STATUS", true);
				assisterResponseDTO.setNavigationMenuMap(navigationMenuMap);
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} else {
				LOGGER.error("Populate Left Navigation Map : Invalid input. Unable to Populate Left Navigation Map");

				assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
			LOGGER.debug("Populate Left Navigation Map Ends :");
		} catch (Exception exception) {
			LOGGER.error("Populate Left Navigation Map : Exception occurred while populating Left Navigation Map : "
					, exception);

			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
		}
		
		
		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for Populate Left Navigation Map." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		
		LOGGER.info("populateNavigationMenuMap: END");
		
		return response;
	}

	/**
	 * Invokes Assister service to get List of Assisters for given Enrollment
	 * Entity
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getassistersbyenrollmententityid", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String getAssistersByEnrollmentEntityId(@RequestBody String request) {
		
		LOGGER.info("getAssistersByEnrollmentEntityId: START");
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		List<Assister> listOfAssisters = null;
		Map<Assister, AssisterLanguages> map = new HashMap<Assister, AssisterLanguages>();
		AssisterLanguages assisterLanguages = null;
		String response = null;
		try {
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
			AssisterRequestDTO assisterRequestDTO = reader.readValue(request);
			LOGGER.debug("Get List of Assisters Starts : Received Rest call from ghix-web. AssisterRequestDTO received in Request: "
					+ assisterRequestDTO);

			Integer enrollmentEntityId = assisterRequestDTO.getId();

			if (enrollmentEntityId != null && enrollmentEntityId.intValue() != 0) {
				listOfAssisters = assisterService.findAssistersByEEId(enrollmentEntityId);

				for (Assister assister : listOfAssisters) {
					assisterLanguages = assisterLanguagesService.findByAssister(assister);
					map.put(assister, assisterLanguages);
				}

				assisterResponseDTO.setListOfAssisters(listOfAssisters);
				assisterResponseDTO.setAssisterWithLanguages(map);
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);

			} else {
				LOGGER.error("Get Assister Detail : Invalid input. Unable to Get Assister Detail");

				assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
		} catch (Exception exception) {
			LOGGER.error("Get Assister Detail : Exception occurred while getting Assister Detail : "
					, exception);

			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
		}

		LOGGER.debug("Get Assister Detail Ends : Get Assister Detail Response : " + assisterResponseDTO);

		
		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for get Assister Details." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getAssistersByEnrollmentEntityId: END");
		
		return response;
	}

	/**
	 * Invokes Assister service to get List of Assisters for given Enrollment
	 * Entity
	 * 
	 * @param requestXML
	 * @return
	 */
	@RequestMapping(value = "/getexternalindividualbyid", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String getIndividualInfo(@RequestBody String requestXML) {
		
		LOGGER.info("getIndividualInfo: START");
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		String response = null;
		
		try {
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
			AssisterRequestDTO assisterRequestDTO = reader.readValue(requestXML);
			LOGGER.debug("Get List of Assisters Starts : Received Rest call from ghix-web. AssisterRequestDTO received in Request: "
					+ assisterRequestDTO);

			Integer individualId = assisterRequestDTO.getIndividualId();
			Assister assisterObj = assisterRequestDTO.getAssister();

			if (individualId != null && assisterObj != null && individualId.intValue() != 0) {
				ExternalIndividual externalIndividual = assisterService.findIndividualInfo(individualId);
				LOGGER.info("findByEnrollmentEntityAndSite: START");
				//abAhbxRestCallInvoker.retrieveAndSaveExternalIndividualDetail(individualId);
				LOGGER.info("findByEnrollmentEntityAndSite: END");
				
				DesignateAssister designateAssister = assisterDesignateService.findAssisterByIndividualId(individualId);
				
				if(designateAssister != null && assisterObj.getId() == designateAssister.getAssisterId()){
					
					assisterResponseDTO.setExternalIndividual(externalIndividual);
					assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
					assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
				}
				else{
					assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
					assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);
				}
			} else {
				LOGGER.error("Get Assister Detail : Invalid input. Unable to Get Assister Detail");

				assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
		} catch (Exception exception) {
			LOGGER.error("Get Assister Detail : Exception occurred while getting Assister Detail : "
					, exception);
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
		}

		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for get assister details by individual id." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		
		LOGGER.debug("Get Assister Detail Ends : Get Assister Detail Response : " + assisterResponseDTO);

		LOGGER.info("getIndividualInfo: END");
		
		return response;
	}

	/**
	 * Retrieves Individual List
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/getindividuallistforassister", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String getIndividualListForEnrollmentEntity(@RequestBody AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("getIndividualListForEnrollmentEntity: START");
		String response = null;
		try{
			Map<String, Object> searchCriteria = assisterRequestDTO.getSearchCriteria();
			Status desigStatus = assisterRequestDTO.getDesigStatus();
			Integer startRecord = assisterRequestDTO.getStartRecord();
			Integer pageSize = assisterRequestDTO.getPageSize();
			AssisterResponseDTO assisterResponseDTO = assisterService.searchIndividualForAssister(searchCriteria,desigStatus.toString(), startRecord, pageSize);
			// Marshaling the response before it is sent out to ghix-web
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for get assister details by individual id." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("getIndividualListForEnrollmentEntity: END");
		return response;
	}

	/**
	 * Retrieves Individual List
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	//@GiAudit(transactionName = "Get Designate Assister", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/designateassisterforrequest", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String getDesignateAssister(@RequestBody AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("getDesignateAssister: START");
		
		LOGGER.info("Inside getDesignateAssister method");
		String response = null;
		try{
			Integer individualId = assisterRequestDTO.getId();
			String strId = Integer.toString(individualId);
			DesignateAssister designateAssister = assisterService.getDesignateAssister(Integer.valueOf(strId));
			AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
			assisterResponseDTO.setDesignateAssister(designateAssister);
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing designate assister request." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info(RETURNING_EE_LIST_TO_GHIX_WEB);
		
		LOGGER.info("getDesignateAssister: END");
		
		// Marshaling the response before it is sent out to ghix-web
		return response;
	}

	/**
	 * Retrieves Individual List
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	//@GiAudit(transactionName = "Save Designate Assister", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/savedesignateassister", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String saveDesignateAssister(@RequestBody AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("saveDesignateAssister: START");	
		
		LOGGER.info("Inside getDesignateAssister method");
		DesignateAssister designateAssister = assisterRequestDTO.getDesignateAssister();
		String response = null;
		try {	
			if(null != designateAssister) {
				int loggedInUserId=EntityUtils.getLoggedInUser();
				if(loggedInUserId != -1)
				{
				//	designateAssister.setCreatedBy(String.valueOf(loggedInUserId));
					designateAssister.setLastUpdatedBy(Long.valueOf(loggedInUserId));
				}
				
				
					if(designateAssister.getStatus().equals(Status.InActive)) {
						// Update the enrollment table if CEC dedesignates the individual.
						 entityUtils.updateEnrollmentDetails(EntityConstants.ENROLLMENT_TYPE_INDIVIDUAL,
								new Long(designateAssister.getIndividualId()), designateAssister.getAssisterId(),
								EntityConstants.ASSISTER_ROLE, EntityConstants.REMOVEACTIONFLAG);
						 
					} else if(designateAssister.getStatus().equals(Status.Active)) {
						
						// Update the enrollment table if CEC approves the individual.
						 entityUtils.updateEnrollmentDetails(EntityConstants.ENROLLMENT_TYPE_INDIVIDUAL,
								new Long(designateAssister.getIndividualId()), designateAssister.getAssisterId(),
								EntityConstants.ASSISTER_ROLE, EntityConstants.ADDACTIONFLAG);
					}
				
			}
			LOGGER.debug("Persisting the DesignateAssister Record");
			assisterDesignateService.saveDesignateAssister(designateAssister, 0);
			AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
			assisterResponseDTO.setDesignateAssister(designateAssister);
			LOGGER.info(RETURNING_EE_LIST_TO_GHIX_WEB);
			// Marshaling the response before it is sent out to ghix-web
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while updating an enrollment details : ", e);					
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("saveDesignateAssister: END");
		
		return response;
	}

	/**
	 * Retrieves Individual List
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	//@GiAudit(transactionName = "Get Assister By User Identifier", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/getassisterbyuserid", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String getAssisterbyUserId(@RequestBody AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("getAssisterbyUserId: START");
		
		LOGGER.info("Inside getDesignateAssister method");
		Integer userId = assisterRequestDTO.getModuleId();
		AssisterLanguages assisterLanguages = null;
		AssisterNumber assisterNumber = null;
		String response = null;
		Assister assisterNew = assisterService.getAssisterByUserId(userId);
		if (assisterNew != null) {
			assisterLanguages = assisterLanguagesService.findByAssister(assisterNew);
			assisterNumber = assisterService.findAssisterNumberByAssisterId(assisterNew.getId());
		}
		
		if(null != assisterNumber) {
			assisterNew.setAssisterNumber(assisterNumber.getId());
		}
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		assisterResponseDTO.setAssister(assisterNew);
		assisterResponseDTO.setAssisterLanguages(assisterLanguages);
		
		try{
			LOGGER.info(RETURNING_EE_LIST_TO_GHIX_WEB);
			// Marshaling the response before it is sent out to ghix-web
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for get assister by user id." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getAssisterbyUserId: END");
		
		return response;
	}
	
	//@GiAudit(transactionName = "Get Assister By User Identifier", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/getassisterRecordbyuserid", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String getassisterRecordbyuserid(@RequestBody AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("getAssisterbyUserId: START");
		
		LOGGER.info("Inside getDesignateAssister method");
		Integer userId = assisterRequestDTO.getModuleId();
		AssisterLanguages assisterLanguages = null;
		AssisterNumber assisterNumber = null;
		String response = null;
		Assister assisterNew = assisterService.getAssisterRecordByUserId(userId);
		if (assisterNew != null) {
			assisterLanguages = assisterLanguagesService.findByAssister(assisterNew);
			assisterNumber = assisterService.findAssisterNumberByAssisterId(assisterNew.getId());
		}
		
		if(null != assisterNumber) {
			assisterNew.setAssisterNumber(assisterNumber.getId());
		}
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		assisterResponseDTO.setAssister(assisterNew);
		assisterResponseDTO.setAssisterLanguages(assisterLanguages);
		
		try{
			LOGGER.info(RETURNING_EE_LIST_TO_GHIX_WEB);
			// Marshaling the response before it is sent out to ghix-web
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for get assister record by user id." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getAssisterbyUserId: END");
		
		return response;
	}

	@RequestMapping(value = "/checkAssisterForEmail", method = RequestMethod.POST)
	@ResponseBody
	public String checkAssisterForEmail(@RequestBody String email) {
		List<Assister> listOfAssister = assisterService
				.findByEmailAddress(email);

		if (listOfAssister != null && !listOfAssister.isEmpty()) {
			return "true";
		}
		return "false";
	}
	
}
