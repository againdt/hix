package com.getinsured.ssap.model.financial;

import java.util.List;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.ssap.model.financial.ifsv.exception.IfsvException;

/**
 * Describes IFSV response object.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IfsvResponse {
  @JsonProperty("requestID")
  private String requestId;

  @JsonProperty("ifsvVerification")
  private IfsvVerification ifsvVerification;

  @JsonProperty("ErrorMessageDetail")
  private List<IfsvErrorMessageDetail> errorMessageDetail;

  @JsonProperty("Exception")
  private IfsvException exception;

  /**
   * Returns {@code boolean} flag that indicates if we have any errors
   * in the response stored in {@link #errorMessageDetail}.
   * @return {@code true} if any errors occurred, {@code false} otherwise.
   */
  public boolean hasErrors() {
    return (this.errorMessageDetail != null && !this.errorMessageDetail.isEmpty()) ||
        (this.exception != null && this.exception.getExceptionMessage() != null);
  }

  /**
   * Returns error details. You can invoke {@link #hasErrors()} before
   * accessing this property in order to find out if there was any errors
   * to begin with.
   * @return list of {@link IfsvErrorMessageDetail} objects.
   */
  public List<IfsvErrorMessageDetail> getErrorMessageDetail() {
    return errorMessageDetail;
  }

  public void setErrorMessageDetail(List<IfsvErrorMessageDetail> errorMessageDetail) {
    this.errorMessageDetail = errorMessageDetail;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public IfsvVerification getIfsvVerification() {
    return ifsvVerification;
  }

  public void setIfsvVerification(IfsvVerification ifsvVerification) {
    this.ifsvVerification = ifsvVerification;
  }

  public IfsvException getException() {
    return exception;
  }

  public void setException(IfsvException exception) {
    this.exception = exception;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", IfsvResponse.class.getSimpleName() + "[", "]")
      .add("requestId='" + requestId + "'")
      .add("ifsvVerification=" + ifsvVerification)
      .add("errorMessageDetail=" + errorMessageDetail)
      .add("exception=" + exception)
      .toString();
  }
}
