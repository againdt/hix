export const OTHER_ETHNICITY_CODE = '0000-0';

export default [
    {
        'label': 'Cuban',
        'code': '2182-4',
        'otherLabel': null
    },
    {
        'label': 'Mexican, Mexican American, or Chicano/a',
        'code': '2148-5',
        'otherLabel': null
    },
    {
        'label': 'Puerto Rican',
        'code': '2180-8',
        'otherLabel': null
    },
    {
        'label': '',
        'code': OTHER_ETHNICITY_CODE,
        'otherLabel': null
    }
];