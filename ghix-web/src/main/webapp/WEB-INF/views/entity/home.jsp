<div id="wrap">

<!-- Masthead -->
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a href="index.html">
          <img class="brand" src="images/logo_ca.png" alt="LOGO">
        </a>
      </div><!-- .container -->
    </div><!-- .navbar-inner -->
  </div><!-- .navbar navbar-fixed-top -->
<!-- Masthead ENDS -->

<!-- Content -->
  <div class="container">
    <div class="row-fluid">
      
      <div id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          
          <div class="item active">
            <img src="images/family.png" alt="">
            <div class="carousel-caption">
              <h2>Enrollment Entities</h2>
              <p>Helping Californians enroll in the highest quality health plans.</p>
              <p><a class="btn btn-primary large" href="entitieInfo.html">First time here? Register your entitie</a></p>
              <p class="small">Already a member? <a href="#">Sign In</a></p>
            </div><!-- .carousel-caption -->
          </div><!-- .item -->
          
          <div class="item">
            <img src="images/employer.png" alt="">
            <div class="carousel-caption">
              <h2>Enrollment Entities</h2>
              <p>Helping Californians enroll in the highest quality health plans.</p>
              <p>First time here? <a class="btn btn-primary large" href="#">Register your entitie</a></p>
              <p class="small">Already a member? <a href="#">Sign In</a></p>
            </div><!-- .carousel-caption -->
          </div><!-- .item -->
          
          <div class="item">
            <img src="images/brokers.png" alt="">
            <div class="carousel-caption">
              <h2>Enrollment Entities</h2>
              <p>Helping Californians enroll in the highest quality health plans.</p>
              <p>First time here? <a class="btn btn-primary large" href="#">Register your entitie</a></p>
              <p class="small">Already a member? <a href="#">Sign In</a></p>
            </div><!-- .carousel-caption -->
          </div><!-- .item -->
        </div><!-- .carousel-inner -->
        
        <ul>
          <li class="menuItem inact act"><a href="">Enrollment Entities</a></li>
          <li class="menuItem inact"><a href="">Assister</a></li>
          <li class="menuItem inact"><a href="">Agents</a></li>
        </ul>
        
      </div><!-- #myCarousel .carousel slide -->
    </div><!-- .row-fluid -->
  </div><!-- .container -->
<!-- Content ENDS -->

  <div id="push"></div><!-- #"push" used to push footer to the bottom of the page --> 
</div><!-- #wrap -->
