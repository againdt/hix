package com.getinsured.hix.batch.referral;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.DependsOn;

import com.getinsured.hix.batch.referral.service.PassiveEnrollmentNotice;
import com.getinsured.hix.batch.referral.service.PassiveEnrollmentNoticeDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@DependsOn("dynamicPropertiesUtil")
public class PassiveEnrollmentNotificationTask extends
		StepExecutionListenerSupport implements Tasklet {

	private Date startDate;
	private Date endDate;
	private List<String> roleNames;
	private Long ssapId;
	

	private PassiveEnrollmentNotice passiveEnrollmentNotice;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PassiveEnrollmentNotificationTask.class);

	@Override
	public void beforeStep(StepExecution stepExecution) {
		roleNames = new ArrayList<String>();
		DateTime datetime = new DateTime();
		DateMidnight today = datetime.toDateMidnight();
		Date systemStartDate = today.toDate();
		Date systemEndDate = new DateMidnight(systemStartDate.getTime())
				.plusDays(1).toDate();

		List<String> systemRoleNames = convertStringTokenToList(DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PASSIVE_ENROLLMENT_NOTIFICATION_ROLES),",");

		
		JobParameters jobParameters = stepExecution.getJobParameters();
		String startDateStr = jobParameters.getString("start_date");
		startDate = startDateStr != null ? getDate(startDateStr)
				: systemStartDate;

		if (startDate == null) {
			throw new GIRuntimeException("Start Date is not valid");
		}
		String endDateStr = jobParameters.getString("end_date");
		endDate = endDateStr != null ? getDate(endDateStr) : systemEndDate;

		if (endDate == null) {
			throw new GIRuntimeException("End Date is not valid");
		}

		if (endDate.before(startDate)) {
			throw new GIRuntimeException(
					"End Date should be higher than Start Date");
		}
		
		String ssapIdstr=jobParameters.getString("ssapapplicationid");
		
		if(ssapIdstr!=null){	
		if (! StringUtils.isNumeric(ssapIdstr)) {
			throw new GIRuntimeException("Ssap application id is not valid");
		}
		ssapId = Long.parseLong(ssapIdstr);
		}

		List<String> roles = convertStringTokenToList(
				jobParameters.getString("roles"), "|");

		if (roles.size() == 0) {
			roleNames = systemRoleNames;
		} else {
			roleNames = roles;
		}

	}

	private Date getDate(String startDatestr) {
		if (StringUtils.isNotBlank(startDatestr)) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				return formatter.parse(startDatestr);
			} catch (Exception e) {
				return null;
			}
		}
		return null;

	}

	private List<String> convertStringTokenToList(String strParam1,
			String strParam2) {
		List<String> objData = new ArrayList<String>();
		if (strParam1 != null) {
			objData = new ArrayList<String>();
			StringTokenizer st = new StringTokenizer(strParam1, strParam2);
			while (st.hasMoreTokens()) {
				objData.add(st.nextToken().toUpperCase());
			}
			st = null;
		}
		return objData;
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1)
			throws Exception {

		List<PassiveEnrollmentNoticeDTO> finalData;
		try {
			finalData = passiveEnrollmentNotice.getEnrollmentData(startDate,
					endDate, roleNames,ssapId);
		} catch (Exception e) {
			throw e;
		}
		if (ssapId != null){
			if (finalData == null || finalData.size() == 0) {
				throw new GIRuntimeException("No Passive Enrollment notice generated. No Enrollment records found for supplied Ssap application id - " + ssapId);
			}
		}
		for (PassiveEnrollmentNoticeDTO passiveEnrollmentNoticeDTO : finalData) {
			try {
				passiveEnrollmentNotice
						.generateNoticeInInbox(passiveEnrollmentNoticeDTO);
			} catch (Exception e) {
				LOGGER.error("Notification not generated for "
						+ passiveEnrollmentNoticeDTO.getCaseNumber() +" "+e);
			}

		}

		return RepeatStatus.FINISHED;
	}

	public PassiveEnrollmentNotice getPassiveEnrollmentNotice() {
		return passiveEnrollmentNotice;
	}

	public void setPassiveEnrollmentNotice(
			PassiveEnrollmentNotice passiveEnrollmentNotice) {
		this.passiveEnrollmentNotice = passiveEnrollmentNotice;
	}

}
