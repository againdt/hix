import React, { Fragment } from 'react';
import { connect } from 'react-redux';

// Components

import IncomeInformationSummary from '../summary/IncomeInformationSummary';
import PageHeader from '../headers/PageHeader';



const Page3_37_E1 = props => {
    const {
        data: {
            singleStreamlinedApplication: {
                coverageYear
            }
        },
        householdMembers
    } = props;
    const {
        buttonContainer,
        PAGES
    } = props;

    return (
        <Fragment>
            <div id="incomeSummary" className="fade-in">
                <div className="subsection">
                    <PageHeader
                        text="Review and Confirm"
                        headerClassName={'usa-heading-alt'}
                    />
                    <div className="usa-width-one-whole">
                        <div className="usa-alert usa-alert-info alert--info alert__review">
                            <div className="usa-alert-body" >
                                <p className="usa-alert-text">
                                    {
                                        `Here is the information you provided about everyone who is part of your household.
                                            Please take a moment to review and double-check the information.
                                            If you see any mistakes, please edit them now.`
                                    }
                                </p>
                            </div>
                        </div>
                    </div>
                    <div  className="usa-width-one-whole">
                        <IncomeInformationSummary
                            {...{buttonContainer, householdMembers, coverageYear, PAGES}}
                        />
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,
});

Page3_37_E1.defaultProps = {
    name: 'Page3_37_E1'
};

export default connect(mapStateToProps)(Page3_37_E1);
