import React, { Fragment } from 'react';
import _ from 'lodash';

import UniversalInput from '../../../../../lib/UniversalInput';

import INPUT_TYPES from '../../../../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../../../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../../../../constants/validationTypes';

const TemporaryAddressInputs = props => {

    const {
        _postalCode,
        _state,
        _city,
        currentData,
        indexInArray,
        ...restProps
    } = props;

    const cleanedRestProps = _.omit(restProps, 'children');

    return (
        <Fragment>
            <UniversalInput
                label={{
                    text: 'City',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: '_city',
                    helperClasses: [],
                    value: _city,
                    placeholder: 'City',
                    validationType: VALIDATION_TYPES.CITY,
                    currentData: currentData,
                    fields: ['otherAddress', 'livingOutsideofStateTemporarilyAddress']
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Zip',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: '_postalCode',
                    helperClasses: [],
                    value: _postalCode,
                    placeholder: 'Zip',
                    validationType: VALIDATION_TYPES.ZIP_CODE,
                    currentData: currentData,
                    fields: ['otherAddress', 'livingOutsideofStateTemporarilyAddress']
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'State',
                    isLabelRequired: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.DROPDOWN,
                    name: '_state',
                    value: _state,
                    options: DROPDOWN_OPTIONS.STATE,
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: currentData,
                    fields: ['otherAddress', 'livingOutsideofStateTemporarilyAddress']
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
        </Fragment>
    );
};

export default TemporaryAddressInputs;