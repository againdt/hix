package com.getinsured.iex.hub.vlp.servicehandler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifRequestSetType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifRequestType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifResponseType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants.VLP_DHS_ID_TYPES;
import com.getinsured.iex.hub.vlp.wrapper.Agency3InitVerifIndividualResponseTypeWrapper;
import com.getinsured.iex.hub.vlp.wrapper.Agency3InitVerifRequestTypeWrapper;
import com.getinsured.iex.hub.vlp.wrapper.Agency3InitVerifResponseTypeWrapper;
import com.getinsured.iex.hub.vlp.wrapper.CertOfCitDocumentID23Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.DHSIDTypeWrapper;
import com.getinsured.iex.hub.vlp.wrapper.DS2019DocumentID27Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.I20DocumentID26Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.I327DocumentID3Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.I551DocumentID4Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.I571DocumentID5Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.I766DocumentID9Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.I94DocumentID2Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.I94UnexpForeignPassportDocumentID10Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.MacReadI551DocumentID22Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.NatrOfCertDocumentID20Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.OtherCase1DocumentID1Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.OtherCase2DocumentID1Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.TempI551DocumentID21Wrapper;
import com.getinsured.iex.hub.vlp.wrapper.UnexpForeignPassportDocumentID30Wrapper;

/**
 * Service handler for VLP Service
 * 
 * author - Nikhil Talreja
 * since - 14-Jan-2014
 */

@SuppressWarnings("rawtypes")
public class VLPServiceHandler extends AbstractServiceHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(VLPServiceHandler.class);
	
	private static ObjectFactory factory;
	
	private static Map<Object,Class> dhsIdMappings;
	
	private static Map<String,String> dhsIdKeys;
	
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
		
		/*
		 * Mappings for DHS ID types to corresponding wrapper classes
		 * 
		 * author - Nikhil Talreja
		 * since - 12-Feb-2014
		 */
		dhsIdMappings = new HashMap<Object, Class>();
		
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I327.toString(),I327DocumentID3Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I551.toString(),I551DocumentID4Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I571.toString(),I571DocumentID5Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I766.toString(),I766DocumentID9Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.CertOfCit.toString(),CertOfCitDocumentID23Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.NatrOfCert.toString(),NatrOfCertDocumentID20Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.MacReadI551.toString(),MacReadI551DocumentID22Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.TempI551.toString(),TempI551DocumentID21Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I94Document.toString(),I94DocumentID2Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I94UnexpForeignPassport.toString(),I94UnexpForeignPassportDocumentID10Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.UnexpForeignPassport.toString(),UnexpForeignPassportDocumentID30Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.I20.toString(),I20DocumentID26Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.DS2019.toString(),DS2019DocumentID27Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.OtherCase1.toString(),OtherCase1DocumentID1Wrapper.class);
		dhsIdMappings.put(VLP_DHS_ID_TYPES.OtherCase2.toString(),OtherCase2DocumentID1Wrapper.class);
		
		dhsIdKeys = new HashMap<String, String>();
		
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I327.toString(),"I327DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I551.toString(),"I551DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I571.toString(),"I571DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I766.toString(),"I766DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.CertOfCit.toString(),"CertOfCitDocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.NatrOfCert.toString(),"NatrOfCertDocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.MacReadI551.toString(),"MacReadI551");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.TempI551.toString(),"TempI551DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I94Document.toString(),"I94DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I94UnexpForeignPassport.toString(),"I94UnexpForeignPassportDocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.UnexpForeignPassport.toString(),"UnexpForeignPassportDocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.I20.toString(),"I20DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.DS2019.toString(),"DS2019DocumentID27");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.OtherCase1.toString(),"OtherCase1DocumentID");
		dhsIdKeys.put(VLP_DHS_ID_TYPES.OtherCase2.toString(),"OtherCase2DocumentID");
	}
	
	/**
	 * Method to generate the payload for request
	 * 
	 * author - Nikhil Talreja
	 * since - 14-Jan-2014
	 * @throws HubServiceException 
	 * @throws  
	 * @throws HubMappingException 
	 */
	
	private DHSIDTypeWrapper setDocumentType(String dhsIdType,String clsName) throws HubServiceException{
		
		Object obj = getNamespaceObjects().get(clsName);
		if(obj == null){
			throw new HubServiceException("Invalid DHSID type ["+dhsIdType+"] provided, no data found for this document");
		}
		
		return new DHSIDTypeWrapper(obj);
	}
	
	private String validateDHSID(JSONObject payloadObj, String dhsIdType) throws HubServiceException{
		
		/*
		 * Checking if provided DHS ID key in JSON input is same as 
		 * object Identifier
		 */
		Object dhsId = payloadObj.get("DHSID");
		if(dhsId == null){
			throw new HubServiceException("DHS Id is missing in request");
		}
		JSONObject dhsIdObject = (JSONObject)dhsId;
		String dhsIdValue = (String)dhsIdObject.keySet().toArray()[0];
		
		LOGGER.info("Received DHS ID type : " + dhsIdType);
		Class cls = dhsIdMappings.get(dhsIdType);
		
		if(dhsIdType == null || cls == null){
			throw new HubServiceException("The DHS Id Type " + dhsIdType + " is not valid for this request, one of following DHSID (\"documentType\") types is expected:["+VLPServiceConstants.getAvailableDHSIdTypes()+"]");
		}
		
		
		if(!dhsIdKeys.get(dhsIdType).equalsIgnoreCase(dhsIdValue)){
			throw new HubServiceException("Mismatch in DHS ID information provided. documentType is " + dhsIdType + " but payload contains " + dhsIdValue + " information.");
		}
		
		return cls.getName();
		
	}
	
	public Object getRequestpayLoad() throws HubServiceException{
		
			String dhsIdType = (String) getContext().get("objectIdentifier");
			try{
				JSONParser parser = new JSONParser();
				JSONObject payloadObj = (JSONObject) parser.parse(getJsonInput());
				
				String clsName = validateDHSID(payloadObj, dhsIdType);
				
				processJSONObject(payloadObj);
				
				Agency3InitVerifRequestTypeWrapper obj = (Agency3InitVerifRequestTypeWrapper)getNamespaceObjects().get(Agency3InitVerifRequestTypeWrapper.class.getName());
				if(obj == null){
					throw new HubServiceException("required VLP Request parameters not available");
				}
				
				
				obj.setDhsid(this.setDocumentType(dhsIdType,clsName));
				Agency3InitVerifRequestSetType request = obj.getRequest();
				Agency3InitVerifRequestType reqObj = factory.createAgency3InitVerifRequestType();
				reqObj.getAgency3InitVerifRequestSet().add(request);
				setJsonInput(obj.toJSONString());
				return reqObj;
			}catch(Exception pe){
				throw new HubServiceException("Failed to generate the payload:",pe);
			}
			
	}
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	this.handleInputParameter(key, value);
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
			// should we expect anything other than JSONObject in this array?
		}
	}

	/**
	 * Method to handle the response
	 * 
	 * author - Nikhil Talreja
	 * since - 14-Jan-2014
	 */
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		String message = "";
		try{
			
			JAXBElement<Agency3InitVerifResponseType> resp= (JAXBElement<Agency3InitVerifResponseType>)responseObject;
			Agency3InitVerifResponseType payload = resp.getValue();
			Agency3InitVerifResponseTypeWrapper response = new Agency3InitVerifResponseTypeWrapper(payload);
			Agency3InitVerifIndividualResponseTypeWrapper individualResponse = null; 
			
			if(response != null && response.getAgency3InitVerifResponseSet() != null){	
				message = response.toJSONString();
				LOGGER.debug("Response Payload " + message);
				/*
				 * To set Response code for VLP, we will use the first response in the set of individual responses
				 * Since VLP service will always be called separately for each applicant, we will receive at most
				 * once response 
				 */
				individualResponse = response.getAgency3InitVerifResponseSet().getAgency3InitVerifIndividualResponse().get(0);
				if(individualResponse != null && individualResponse.getResponseMetadata() != null){
					this.setResponseCode(individualResponse.getResponseMetadata().getResponseCode());
				}
				/*
				 * HIX-34256 - Case needs to be closed as soon as we get step 1 response
				 */
				this.requestStatus = RequestStatus.PENDING_CLOSE;
			}
			
			getCaseNumber(individualResponse);
			
			return message;
		}catch(ClassCastException ce){
			message = "Help! I do not any thing about "+responseObject.getClass().getName()+" Expecting:"+Agency3InitVerifResponseTypeWrapper.class.getName();
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			throw new HubServiceException(message, ce);
		}
	}
	
	/**
	 * Method to get Case Number from HUB response
	 * 
	 * author - Nikhil Talreja
	 * since - 26-May-2014
	 */
	private void getCaseNumber(Agency3InitVerifIndividualResponseTypeWrapper individualResponse){
		
		//Case number
		if(individualResponse != null && individualResponse.getAgency3InitVerifIndividualResponseSet() != null){
			this.getResponseContext().put("HUB_CASE_NUMBER", individualResponse.getAgency3InitVerifIndividualResponseSet().getCaseNumber());
		}
		
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
