import React, {Fragment} from 'react';
import PropTypes from 'prop-types';


const Input  = props => {

    const {
        inputData: {
            id,
            isInputRequired,
            helperClasses,
            name,
            placeholder,
            type,
            value
        },
        inputActions: {
            handleFieldChange,
            handleFieldBlur
        },
        label: {
            text,
            labelClasses
        },
    } = props;

    const handleValueChange = e => {
        handleFieldChange(e.target.value);
    };

    const handleValueBlur = e => {
        handleFieldBlur(e.target.value);
    };

    return (
        <Fragment>
            <div className={`usa-grid gi-component gi-${type} ${helperClasses && helperClasses.join(' ')}`}>
                <label
                    htmlFor={id}
                    className={`usa-width-one-third gi-${type}-label ${labelClasses && labelClasses.join(' ')} required-${isInputRequired}`}
                    aria-label={isInputRequired}
                >
                    { text }
                </label>
                <input
                    id={id}
                    className={`usa-width-two-thirds gi-${type}-input`}
                    name={name}
                    type="text"
                    value={value || ''}
                    aria-required={isInputRequired}
                    placeholder={placeholder}
                    onChange={handleValueChange}
                    onBlur={handleValueBlur}
                    onFocus={props.inputActions.onFieldFocus}
                />
            </div>
        </Fragment>
    );
};

Input.propTypes = {
    inputData: PropTypes.shape({
        name: PropTypes.string.isRequired,
        classes: PropTypes.arrayOf([PropTypes.string]),
        value: PropTypes.any,
        placeholder: PropTypes.string,
    })
};

export default Input;
