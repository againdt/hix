
/**
 * 
 */
package com.getinsured.hix.batch.enrollment.task;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author parhi_s
 *
 */
public class ShopEnrollmentReconciliationTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(ShopEnrollmentReconciliationTask.class);
	private EnrollmentBatchService enrollmentBatchService;
	private EnrollmentEmailUtils enrollmentEmailUtils;
	private String extractMonth;
	private String hiosIssuerIdStr;
	private String hiosIssuerId;
	
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws GIException {
		
		LOGGER.info("ShopEnrollmentReconciliationTask.execute : START");
		EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
		enrollmentBatchEmailDTO.setFileLocation(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.XMLSHOPRECONEXTRACTPATH));
		enrollmentBatchEmailDTO.setErrorCode(201);
		enrollmentBatchEmailDTO.setAppServer(GhixBatchConstants.appserver);
		try{		
			
			if(extractMonth!=null &&!(extractMonth.equalsIgnoreCase("null"))  &&!(extractMonth.trim().equals("")) && !(extractMonth.equalsIgnoreCase(EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR)) && !(extractMonth.equalsIgnoreCase(EnrollmentConstants.IND_RECON_EXTRACT_MONTH_CURRENT))){
				LOGGER.error("Error in ShopEnrollmentReconciliationTask Job =="+"Reconciliation_Month = "+extractMonth + " is not a valid value."+ "You can either pass as "+EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR+" or " + EnrollmentConstants.IND_RECON_EXTRACT_MONTH_CURRENT+ " or null "+ " else you can leave it blank \n");
				enrollmentBatchEmailDTO.setErrorMessage("Reconciliation_Month = "+extractMonth + " is not a valid value."+ "You can either pass as "+EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR+" or " + EnrollmentConstants.IND_RECON_EXTRACT_MONTH_CURRENT+ " or null "+" else you can leave it blank");
				throw new GIException("Reconciliation_Month = "+extractMonth + " is not a valid value."+ "You can either pass as "+EnrollmentConstants.IND_RECON_EXTRACT_MONTH_PRIOR+" or " + EnrollmentConstants.IND_RECON_EXTRACT_MONTH_CURRENT+ " or null "+" else you can leave it blank \n");
			}
			
			// Apply Validation on HIOSIssuer Id 
			if(isNotNullAndEmpty(hiosIssuerIdStr)){
			 hiosIssuerId = hiosIssuerIdStr;
			 
			}
			
			enrollmentBatchService.serveEnrollmentReconciliationJob(extractMonth,EnrollmentConstants.ENROLLMENT_TYPE_SHOP,hiosIssuerId);
			LOGGER.info("ShopEnrollmentReconciliationTask.execute : END");
		}catch(Exception e){
			LOGGER.error("ShopEnrollmentReconciliationTask.execute : FAIL "+e.getMessage(),e);
			enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
			throw new GIException("Error in ShopEnrollmentReconciliationTask.execute() ::"+e.getMessage(),e);
		}
		return RepeatStatus.FINISHED;
	}
	
	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}
	public void setEnrollmentBatchService(
			EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}
	public String getExtractMonth() {
		return extractMonth;
	}
	public void setExtractMonth(String extractMonth) {
		this.extractMonth = extractMonth;
	}
	public EnrollmentEmailUtils getEnrollmentEmailUtils() {
		return enrollmentEmailUtils;
	}

	public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
		this.enrollmentEmailUtils = enrollmentEmailUtils;
	}

	public String getHiosIssuerIdStr() {
		return hiosIssuerIdStr;
	}

	public void setHiosIssuerIdStr(String hiosIssuerIdStr) {
		this.hiosIssuerIdStr = hiosIssuerIdStr;
	}


}
