package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.SSATitleIIMonthlyIncomeType;

public class SSATitleIIMonthlyIncomeWrapper implements JSONAware{
	
	 private boolean personDisabledIndicator;
	 private String ongoingMonthlyBenefitCreditedAmount;
	 private String ongoingMonthlyOverpaymentDeductionAmount;
	 private boolean ongoingPaymentInSuspenseIndicator;
	 private SSATitleIIMonthlyInformationTypeWrapper requestedMonthInformation;
	 private SSATitleIIMonthlyInformationTypeWrapper requestedMonthMinusOneInformation;
	 private SSATitleIIMonthlyInformationTypeWrapper requestedMonthMinusTwoInformation;
	 private SSATitleIIMonthlyInformationTypeWrapper requestedMonthMinusThreeInformation;
	
	public SSATitleIIMonthlyIncomeWrapper (SSATitleIIMonthlyIncomeType titleIIMonthlyIncomeType) {

		if(titleIIMonthlyIncomeType == null){
			return;
		}
		
		if(titleIIMonthlyIncomeType.getPersonDisabledIndicator() != null){
			this.personDisabledIndicator = titleIIMonthlyIncomeType.getPersonDisabledIndicator().isValue();
		}
		
		if(titleIIMonthlyIncomeType.getOngoingMonthlyBenefitCreditedAmount() != null){
			this.ongoingMonthlyBenefitCreditedAmount = titleIIMonthlyIncomeType.getOngoingMonthlyBenefitCreditedAmount().toString();
		}
		
		if(titleIIMonthlyIncomeType.getOngoingMonthlyOverpaymentDeductionAmount() != null){
			this.ongoingMonthlyOverpaymentDeductionAmount = titleIIMonthlyIncomeType.getOngoingMonthlyOverpaymentDeductionAmount().toString();
		}
		
		if(titleIIMonthlyIncomeType.getOngoingPaymentInSuspenseIndicator() != null){
			this.ongoingPaymentInSuspenseIndicator = titleIIMonthlyIncomeType.getOngoingPaymentInSuspenseIndicator().isValue();
		}
		
		this.requestedMonthInformation = new SSATitleIIMonthlyInformationTypeWrapper(titleIIMonthlyIncomeType.getRequestedMonthInformation());
		this.requestedMonthMinusOneInformation = new SSATitleIIMonthlyInformationTypeWrapper(titleIIMonthlyIncomeType.getRequestedMonthMinusOneInformation());
		this.requestedMonthMinusTwoInformation = new SSATitleIIMonthlyInformationTypeWrapper(titleIIMonthlyIncomeType.getRequestedMonthMinusTwoInformation());
		this.requestedMonthMinusThreeInformation = new SSATitleIIMonthlyInformationTypeWrapper(titleIIMonthlyIncomeType.getRequestedMonthMinusThreeInformation());
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("PersonDisabledIndicator", this.personDisabledIndicator);
		obj.put("OngoingMonthlyBenefitCreditedAmount", this.ongoingMonthlyBenefitCreditedAmount);
		obj.put("OngoingMonthlyOverpaymentDeductionAmount", this.ongoingMonthlyOverpaymentDeductionAmount);
		obj.put("OngoingPaymentInSuspenseIndicator", this.ongoingPaymentInSuspenseIndicator);
		
		if(this.requestedMonthInformation != null){
			obj.put("RequestedMonthInformation", this.requestedMonthInformation);
		}
		if(this.requestedMonthMinusOneInformation != null){
			obj.put("RequestedMonthMinusOneInformation", this.requestedMonthMinusOneInformation);
		}
		if(this.requestedMonthMinusTwoInformation != null){
			obj.put("RequestedMonthMinusTwoInformation", this.requestedMonthMinusTwoInformation);
		}
		if(this.requestedMonthMinusThreeInformation != null){
			obj.put("RequestedMonthMinusThreeInformation", this.requestedMonthMinusThreeInformation);
		}
		return obj.toJSONString();
	}

	public boolean getPersonDisabledIndicator() {
		return personDisabledIndicator;
	}

	public String getOngoingMonthlyBenefitCreditedAmount() {
		return ongoingMonthlyBenefitCreditedAmount;
	}

	public String getOngoingMonthlyOverpaymentDeductionAmount() {
		return ongoingMonthlyOverpaymentDeductionAmount;
	}

	public boolean getOngoingPaymentInSuspenseIndicator() {
		return ongoingPaymentInSuspenseIndicator;
	}

	public SSATitleIIMonthlyInformationTypeWrapper getRequestedMonthInformation() {
		return requestedMonthInformation;
	}

	public SSATitleIIMonthlyInformationTypeWrapper getRequestedMonthMinusOneInformation() {
		return requestedMonthMinusOneInformation;
	}

	public SSATitleIIMonthlyInformationTypeWrapper getRequestedMonthMinusTwoInformation() {
		return requestedMonthMinusTwoInformation;
	}

	public SSATitleIIMonthlyInformationTypeWrapper getRequestedMonthMinusThreeInformation() {
		return requestedMonthMinusThreeInformation;
	}

}
