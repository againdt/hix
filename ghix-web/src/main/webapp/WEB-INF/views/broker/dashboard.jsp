<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%> 
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/highcharts.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/underscore-min.js" />"></script>


<div class="gutter10">
	<div class="row-fluid">
		<h1><a name="skip"></a><spring:message  code="label.brkdashboard"/></h1>
	</div>
	<div class="row-fluid">
	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3" id="sidebar">
            	<div class="header"><h4><spring:message  code="label.brkquicklinks"/></h4></div>
                <ul class="nav nav-list">
                 	<c:if test="${isEmployerFlowAllowed}">
                  		<li><a href="<c:url value='/broker/employers?desigStatus=Pending'/>"><i class=" icon-share-alt"></i> <spring:message  code="label.brkpendingreq"/></a></li>
					</c:if>
                  <c:if test="${isIndividualFlowAllowed}">
                 	 <c:choose>
		  				<c:when test="${broker.status=='InActive'}">
		  					<li><a href="javascript:void();" style="cursor : not-allowed;"><i class="icon-time"></i> <spring:message  code="label.brkpendingind"/></a></li>
		  				</c:when>
		  				<c:otherwise>

		  					<li><a href="<c:url value='/broker/individuals?desigStatus=Pending'/>" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Left Nav Clicks', 'Click', 'Pending Delegation Requests', 1]);"><i class="icon-time"></i> <spring:message  code="label.brkpendingind"/></a></li>
		  				</c:otherwise>
		  			</c:choose>
                  	
                  </c:if>
                  <li><a href="<c:url value='/broker/viewprofile'/>" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Left Nav Clicks', 'Click', 'My Profile', 1]);"><i class="icon-user"></i> <spring:message  code="label.brkmyprofile"/></a></li>
				  <c:if test="${isEmployerFlowAllowed}">
				  	 <li><a href="<c:url value='/broker/employer/dashboard?switchToModuleName=Employer&switchToModuleId=-1&switchToResourceName=SettingUpNewEmployer'/>"><i class=" icon-share-alt"></i> <spring:message  code="label.brkaddemployer"/></a></li>
				  </c:if>
			  		<c:if test="${CA_STATE_CODE && !isAdminUser}"> 
			  			<c:choose>
			  				<c:when test="${broker.status=='InActive'}">
			  					<li><a name="IND66_URL_CA" href="javascript:void();" style="cursor : not-allowed;"><i class="icon-time"></i> <spring:message  code="label.brkaddindividual"/></a></li>
			  				</c:when>
			  				<c:otherwise>
			  					<li><a name="IND66_URL_CA" href="<c:url value='${switchAccUrl}&_pageLabel=individualHomePage&ahbxId=&ahbxUserType=1&recordId=${encryptedBrokerId}&recordType=2&newAppInd=Y&lang='/><spring:message  code="label.language.${lang}"/>" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent Portal Left Nav Clicks', 'Click', 'Start New Application', 1]);"><i class="icon-time"></i> <spring:message  code="label.brkaddindividual"/></a></li>
			  				</c:otherwise>
			  			</c:choose>
				  	</c:if>
				  	<c:if test="${!CA_STATE_CODE}">
                        <li><a href="<c:url value='/cap/consumer/addconsumer'/>"><i class="icon-user"></i> <spring:message  code="label.brkaddindividual"/> </a></li>
				  </c:if>
				  <c:if test="${CA_STATE_CODE && (userActiveRoleName == 'agency_manager' || userActiveRoleName == 'broker' || userActiveRoleName == 'broker_admin')}"> 
					  	<li>
					  		<a name="enrollerToolkits" id="enrollerToolkits" href="https://hbex.coveredca.com/toolkit/" target="_blank" >
					  		<i class="icon-wrench"></i> <spring:message  code="label.enrollerToolkits"/></a>
					  	</li>
				  	</c:if>
				</ul>
				<br>

             <!-- </div> -->
             
             <c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
	          	<!-- ACCESS CODE STARTS--->
	          	<div class="margin20-t" ng-app="accessCodeApp" ng-controller="accessCodeController" ng-cloak>
					<form name="accessCodeForm" action="<c:url value="/referral/verifyaccesscode"/>" method="post" class="noBorder" novalidate>
					<df:csrfToken/>
						<div class="header">
							<h4 for="access-code" id="access-code-dashboard">
			 	 				<spring:message code="label.homePage.accessCode"/>
			 	 			</h4>
		 	 			</div>	 	 			
		 	 			<div class="margin10-t">
			 	 			<input type="text" class="input-small margin10-l" id="access-code" name="accessCode" ng-focus length="10" required ng-model="accessCode" autocomplete="off">
			 	 			<input type="submit" class="btn margin10-l margin10-b" value="<spring:message code="label.homePage.submit"/>" ng-disabled="accessCodeForm.$invalid">
			 	 			<div class="help-inline" id="accessCode_error">
								<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.required && !accessCodeForm.accessCode.$focused">
									<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeRequired"/></span>
								</label>
								<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.pattern && !accessCodeForm.accessCode.$focused">
									<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeValidation"/></span>
								</label>
							</div>
		 	 			</div>
					</form>
				</div>
				<!-- ACCESS CODE ENDS--->
           	</c:if>
			
	
	</div>
	
 <c:if test="${csrView == true}"> 
 <c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>	           
                <h3>
<strong >Viewing ${broker.user.fullName}</strong> <a href="/hix/admin/broker/viewprofile/${encBrokerId}"class="btn btn-primary showcomments"><spring:message code="label.account"/></a>
	</h3>
	</c:if>
		
	<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9" id="rightpanel">
			<div class="header">
                  <h4 class="margin0"><spring:message  code="label.brkenrollmenthighlight"/></h4>
            </div>
            <br>
                 
		<script type="text/javascript">
           
		    $(document).ready(function() {
		    	
		    	var chart;
			    var jsonObj = ${json};
			    var text = '';
			    var subtitle = '';
			    
			    if(jsonObj.length < 4){
			    	
			    	//console.log('NO plan data receive setting to default :-( ');
			    	jsonObj = [{"planLevel":"<spring:message  code="label.brkPlatinum"/>","enrollmentCount":0},
			    	           {"planLevel":"<spring:message  code="label.brkGold"/>","enrollmentCount":0},
			    	           {"planLevel":"<spring:message  code="label.brkSilver"/>","enrollmentCount":0},
			    	           {"planLevel":"<spring:message  code="label.brkBronze"/>","enrollmentCount":0}];
			    	
			    	text = removeSpanishSpecialCharacter('<spring:message  code="label.berNoEnrollmentDataPast30Days"/>');
					
			    	
			    }else{
			    	text = removeSpanishSpecialCharacter('<spring:message  code="label.brkYourEmployerEnrollmentsPast30Days"/>');
			    	subtitle = '<spring:message  code="label.brkSourceNMHIX"/>';
			    }
			    
			    var colors = Highcharts.getOptions().colors;
			    
			    //console.log('JSON :'+jsonObj);
			    
	            var categories = _.pluck(jsonObj, 'planLevel'); 
	    	    
	            //console.log(' categories : '+categories);
	    	    
	            var data = [];
	            
	            for(var i = 0, jsonLength = jsonObj.length ; i < jsonLength ; i++ ){
	                var enrollmentCount = jsonObj[i];
	                // alternate colors, then repeat it if exceed the highchart options
	                data.push({y: parseInt(enrollmentCount.enrollmentCount) , color: colors[ i % colors.length] });
	            }
	            
	            //console.log(' data : '+data);
		    	//subtitle: { text: '<spring:message  code="label.brkSourceGetinsured"/>' },
		        chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container_dashboard_hc',
		                type: 'column'
		            },
		            title: {
						useHTML: true,
		                text: '<spring:message  code="label.brkYourEnrollmentsPast30Days"/>'
		            },
		            colors: [
			        	     	'#cf6f1e', 
			        	     	'#b1c2c5',
			        	     	'#d3a900',
			        	     	'#89b6cd'
			        	     	],
		            xAxis: {
		                categories: categories
		            },
		            yAxis: {
		                min: 0,
		                title: {
							useHTML: true,
		                    text: '<spring:message  code="label.brkEnrollments"/>'
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                backgroundColor: '#FFFFFF',
		                align: 'left',
		                verticalAlign: 'top',
		                x: 100,
		                y: 70,
		                floating: true,
		                shadow: true,
						useHTML: true,
						labelFormatter: function () {
							return '<spring:message  code="label.brkEnrollmentType"/>';
						}
		            },
		            tooltip: {
		                formatter: function() {
		                    return ''+
		                        this.x +': '+ this.y +' ';
		                }
		            },
		            credits: {
		                enabled: false,
		            },
		            plotOptions: {
		                column: {
		                    pointPadding: .02,
		                    borderWidth: 0
		                }
		            },
		            series: [{
		                data: data , dataLabels: { enabled: true}      
		                  }]
		        });
		    });
			    	
			function removeSpanishSpecialCharacter(text) {
				
				return text.replace('&Aacute;', 'Á')
					.replace('&aacute;', 'á')
					.replace('&Eacute;', 'É')
					.replace('&eacute;', 'é')
					.replace('&Iacute;', 'Í')
					.replace('&iacute;', 'í')
					.replace('&Ntilde;', 'Ñ')
					.replace('&ntilde;', 'ñ')
					.replace('&Oacute;', 'Ó')
					.replace('&oacute;', 'ó')
					.replace('&Uacute;', 'Ú')
					.replace('&uacute;', 'ú')
					.replace('&Uuml;', 'Ü')
					.replace('&uuml;', 'ü')
			}
	
		</script>
	
	
			
			
			<div id="container_dashboard_hc" style="min-width: 750px; height: 400px; margin: 0 auto"></div>
    </div> <!-- End of span6 -->
          
 </div> <!-- End of Row Fluid -->
	
</div>

<script>

var accessCodeApp = angular.module('accessCodeApp',[]);

accessCodeApp.controller('accessCodeController', function($scope) {	
	
	
});

accessCodeApp.directive('ngFocus', [function() {
	var FOCUS_CLASS = "ng-focused";
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$focused = false;
			element.bind('focus', function(evt) {
				element.addClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = true;
				});
			}).bind('blur', function(evt) {
				element.removeClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = false;
				});
			});
		}
	};
}]);
</script>