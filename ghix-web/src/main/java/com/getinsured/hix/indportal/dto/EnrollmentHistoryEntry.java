package com.getinsured.hix.indportal.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.getinsured.hix.dto.enrollment.EnrolleeDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.indportal.IndividualPortalConstants;

/**
 * HIX-59164
 * Display Enrollment History for the Household
 * 
 * @author sahoo_s
 */
public class EnrollmentHistoryEntry {

	//Enrollment Level Details
	private String enrollmentId;
	private String policyId;
	private String coverageStartDate;
	private String coverageEndDate;
	private String enrollmentStatus;
	private String monthlypremium;
	private String netPremium;
	private String electedAptc;
	private String electedStateSubsidy;
	private String productType;
	private String csrLevel;
	private String premiumEffDate;
	
	//Plan Level Details 
	private String planLogoUrl;
	private String planName;
	private String planId;
	private String officeVisit;
	private String genericMedications;
	private String deductible;
	private String oopMax;
	private String paymentUrl;
	private String issuerPhone;
	private String issuerSite;
	
	//Enrollee level Information
	private List<EnrolleeHistoryEntry> enrolleeHistoryEntries = new ArrayList<EnrolleeHistoryEntry>();

	//Common Information
	private boolean allowDisenroll;
	private boolean allowPaymentRedirect;
	private String benefitDetailsUrl;
	private Date creationTimestamp;
	private String caseNumber;
	
	private String isPlanEffectuated = "N" ;
	private String endOfCurrentMonth;
	private String endOfNextMonth;
	private String endOfMonthAfterNext;
	private String planType;
	private boolean terminationDateChangeAllowed;
	
	private static final String BENEFIT_URL = "/private/planinfo/";
	private SimpleDateFormat formator = new SimpleDateFormat("MM/dd/yyyy");
	private SimpleDateFormat dateformator = new SimpleDateFormat("MMddyyyy");
	
	//HIX-79572 : CSR Override Option for Overriding the Enrollment Status
	private boolean allowEnrollStatusUpdate;
	private boolean insideOEEnrollmentWindow;
	
	/**
	 * Constructor to create Enrollment history entries
	 * 
	 * @param enrollmentDataDTO
	 * @param enrolleeDataDTOList
	 * @param planResponse
	 * @param issuerLogoURL
	 * @param encryptedEnrollmentId
	 */
	public EnrollmentHistoryEntry(EnrollmentDataDTO enrollmentDataDTO, List<EnrolleeDataDTO> 
		enrolleeDataDTOList, PlanResponse planResponse, String issuerLogoURL, String encryptedEnrollmentId) {

		this.enrollmentId = encryptedEnrollmentId;
		if(enrollmentDataDTO.getEnrolleeDataDtoList() != null &&
		        !enrollmentDataDTO.getEnrolleeDataDtoList().isEmpty()){
			this.policyId = (enrollmentDataDTO.getEnrolleeDataDtoList().get(0).getCoveragePolicyNumber());
		}
		this.coverageStartDate = (formator.format(enrollmentDataDTO.getEnrollmentBenefitEffectiveStartDate()));
		this.coverageEndDate = (formator.format(enrollmentDataDTO.getEnrollmentBenefitEffectiveEndDate()));
		this.enrollmentStatus = (IndividualPortalConstants.ENROLLMENT_STATUS.get(enrollmentDataDTO.getEnrollmentStatus()));
		this.monthlypremium = (String.valueOf(enrollmentDataDTO.getGrossPremiumAmt()));
		this.netPremium = (String.valueOf(enrollmentDataDTO.getNetPremiumAmt()));
		this.electedAptc = (String.valueOf(enrollmentDataDTO.getAptcAmount()));
		this.electedStateSubsidy = (String.valueOf(enrollmentDataDTO.getStateSubsidyAmount()));
		this.productType = (planResponse.getNetworkType());
		this.planType = (enrollmentDataDTO.getPlanType());
		this.csrLevel = (planResponse.getCostSharing());
		this.premiumEffDate = enrollmentDataDTO.getPremiumEffDate();
		
		
		this.planLogoUrl = (issuerLogoURL);
		this.planName = (enrollmentDataDTO.getPlanName());
		this.planId = (String.valueOf(enrollmentDataDTO.getPlanId()));
		this.officeVisit = (planResponse.getOfficeVisit());
		this.genericMedications = (planResponse.getGenericMedications());
		this.paymentUrl = (planResponse.getPaymentUrl());
		this.issuerPhone = (planResponse.getIssuerPhone());
		this.issuerSite = (planResponse.getIssuerSite());
		
		for (EnrolleeDataDTO enrolleeDataDTO : enrolleeDataDTOList) {
			EnrolleeHistoryEntry enrolleeHistoryEntry = new EnrolleeHistoryEntry(enrolleeDataDTO);
			enrolleeHistoryEntries.add(enrolleeHistoryEntry);
		}
		
		if (enrolleeHistoryEntries.size() == 1) {
			if (StringUtils.isNotBlank(planResponse.getDeductible())) {
				this.deductible = (IndividualPortalConstants.DOLLAR_SYMBOL + 
						String.format(IndividualPortalConstants.NUMBER_FORMAT, Float.valueOf(planResponse.getDeductible())));
			}
			if (StringUtils.isNotBlank(planResponse.getOopMax())) {
				this.oopMax = (IndividualPortalConstants.DOLLAR_SYMBOL + 
						String.format(IndividualPortalConstants.NUMBER_FORMAT, Float.valueOf(planResponse.getOopMax())));
			}
		} else {
			if (StringUtils.isNotBlank(planResponse.getDeductibleFamily())) {
				this.deductible = (IndividualPortalConstants.DOLLAR_SYMBOL + 
						String.format(IndividualPortalConstants.NUMBER_FORMAT,	Float.valueOf(planResponse.getDeductibleFamily())));
			}
			if (StringUtils.isNotBlank(planResponse.getOopMaxFamily())
					&& NumberUtils.isNumber(planResponse.getOopMaxFamily())) {
				this.oopMax = (IndividualPortalConstants.DOLLAR_SYMBOL + 
						String.format(IndividualPortalConstants.NUMBER_FORMAT,	Float.valueOf(planResponse.getOopMaxFamily())));
			}
		}
		
		this.benefitDetailsUrl = (BENEFIT_URL+dateformator.format(enrollmentDataDTO.getEnrollmentBenefitEffectiveStartDate())+"/"+enrollmentDataDTO.getPlanId());
		if (StringUtils.isNotEmpty(planResponse.getPaymentUrl())) {
			this.allowPaymentRedirect = (true);
		}
		this.creationTimestamp = (enrollmentDataDTO.getCreationTimestamp());
		this.caseNumber = (enrollmentDataDTO.getSsapCaseNumber());
		
		if(new TSDate().getTime() >= enrollmentDataDTO.getEnrollmentBenefitEffectiveStartDate().getTime()){
		    this.isPlanEffectuated = "Y";
		}
		else{
		    this.isPlanEffectuated = "N";
		}
		
		//HIX-79572 : CSR Override Option for Overriding the Enrollment Status
		if (enrollmentDataDTO.getEnrollmentConfirmationDate() == null
				&& IndividualPortalConstants.ALLOWABLE_ENROLLMENT_STATUS_FOR_1095_OVERRIDE.contains(enrollmentDataDTO.getEnrollmentStatus()) ) {
			this.allowEnrollStatusUpdate = (true);
		}
	}

	public String getEnrollmentId() {
		return enrollmentId;
	}

	public void setEnrollmentId(String enrollmentId) {
		this.enrollmentId = enrollmentId;
	}

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public String getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public String getMonthlypremium() {
		return monthlypremium;
	}

	public void setMonthlypremium(String monthlypremium) {
		this.monthlypremium = monthlypremium;
	}

	public String getNetPremium() {
		return netPremium;
	}

	public void setNetPremium(String netPremium) {
		this.netPremium = netPremium;
	}

	public String getElectedAptc() {
		return electedAptc;
	}

	public void setElectedAptc(String electedAptc) {
		this.electedAptc = electedAptc;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getPlanLogoUrl() {
		return planLogoUrl;
	}

	public void setPlanLogoUrl(String planLogoUrl) {
		this.planLogoUrl = planLogoUrl;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getOfficeVisit() {
		return officeVisit;
	}

	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}

	public String getGenericMedications() {
		return genericMedications;
	}

	public void setGenericMedications(String genericMedications) {
		this.genericMedications = genericMedications;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getOopMax() {
		return oopMax;
	}

	public void setOopMax(String oopMax) {
		this.oopMax = oopMax;
	}

	public String getPaymentUrl() {
		return paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public String getIssuerPhone() {
		return issuerPhone;
	}

	public void setIssuerPhone(String issuerPhone) {
		this.issuerPhone = issuerPhone;
	}

	public String getIssuerSite() {
		return issuerSite;
	}

	public void setIssuerSite(String issuerSite) {
		this.issuerSite = issuerSite;
	}

	public List<EnrolleeHistoryEntry> getEnrolleeHistoryEntries() {
		return enrolleeHistoryEntries;
	}

	public void setEnrolleeHistoryEntries(
			List<EnrolleeHistoryEntry> enrolleeHistoryEntries) {
		this.enrolleeHistoryEntries = enrolleeHistoryEntries;
	}

	public boolean getAllowDisenroll() {
		return allowDisenroll;
	}

	public void setAllowDisenroll(boolean allowDisenroll) {
		this.allowDisenroll = allowDisenroll;
	}

	public boolean getAllowPaymentRedirect() {
		return allowPaymentRedirect;
	}

	public void setAllowPaymentRedirect(boolean allowPaymentRedirect) {
		this.allowPaymentRedirect = allowPaymentRedirect;
	}

	public String getBenefitDetailsUrl() {
		return benefitDetailsUrl;
	}

	public void setBenefitDetailsUrl(String benefitDetailsUrl) {
		this.benefitDetailsUrl = benefitDetailsUrl;
	}

	public Date getCreationTimestamp() {
		return new Date(creationTimestamp.getTime());
	}

	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = new Date(creationTimestamp.getTime());
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getIsPlanEffectuated() {
		return isPlanEffectuated;
	}

	public void setIsPlanEffectuated(String isPlanEffectuated) {
		this.isPlanEffectuated = isPlanEffectuated;
	}

	public String getEndOfCurrentMonth() {
		return endOfCurrentMonth;
	}

	public void setEndOfCurrentMonth(String endOfCurrentMonth) {
		this.endOfCurrentMonth = endOfCurrentMonth;
	}

	public String getEndOfNextMonth() {
		return endOfNextMonth;
	}

	public void setEndOfNextMonth(String endOfNextMonth) {
		this.endOfNextMonth = endOfNextMonth;
	}

	public String getEndOfMonthAfterNext() {
		return endOfMonthAfterNext;
	}

	public void setEndOfMonthAfterNext(String endOfMonthAfterNext) {
		this.endOfMonthAfterNext = endOfMonthAfterNext;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}
	
	public boolean getAllowEnrollStatusUpdate() {
		return allowEnrollStatusUpdate;
	}

	public void setAllowEnrollStatusUpdate(boolean allowEnrollStatusUpdate) {
		this.allowEnrollStatusUpdate = allowEnrollStatusUpdate;
	}

	/**
	 * @return the terminationDateChangeAllowed
	 */
	public boolean isTerminationDateChangeAllowed() {
		return terminationDateChangeAllowed;
	}

	/**
	 * @param terminationDateChangeAllowed the terminationDateChangeAllowed to set
	 */
	public void setTerminationDateChangeAllowed(boolean terminationDateChangeAllowed) {
		this.terminationDateChangeAllowed = terminationDateChangeAllowed;
	}

	/**
	 * @return the premiumEffDate
	 */
	public String getPremiumEffDate() {
		return premiumEffDate;
	}

	/**
	 * @param premiumEffDate the premiumEffDate to set
	 */
	public void setPremiumEffDate(String premiumEffDate) {
		this.premiumEffDate = premiumEffDate;
	}

	public String getElectedStateSubsidy() {
		return electedStateSubsidy;
	}

	public void setElectedStateSubsidy(String electedStateSubsidy) {
		this.electedStateSubsidy = electedStateSubsidy;
	}

	public boolean isInsideOEEnrollmentWindow() {
		return insideOEEnrollmentWindow;
	}

	public void setInsideOEEnrollmentWindow(boolean insideOEEnrollmentWindow) {
		this.insideOEEnrollmentWindow = insideOEEnrollmentWindow;
	}
	
}
