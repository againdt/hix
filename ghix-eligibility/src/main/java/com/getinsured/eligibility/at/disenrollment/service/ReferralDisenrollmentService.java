package com.getinsured.eligibility.at.disenrollment.service;

import java.util.Date;
import java.util.Map;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface ReferralDisenrollmentService {
	
	public String invokeDisenrollmentAPI(
			Map<String, ApplicantEvent> applicantEventMap, SsapApplication enrolledApplication);
	
	String invokeDisenrollmentAPI(SsapApplication enrolledApplication,
			Date terminationDate);

	public String disenrollHealthOrDentalOnly(String[] arguments);

}
