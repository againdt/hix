 <div class="span6 info-block">                    
	<div class="gutter10">
	      <div class="header"><h4>Employer Summary</h4></div>
	           <div class="widget">
				<h5 class="header-secondary">$status$</h5>
				<div class="gutter10 tile">
				<p>You must offer coverage to all employees that work 30 or more hours per week.</p>
				<div class="data-display well-noborder">
				<dl class="dl-horizontal">
				   <dt>Employees</dt>
				    <dd id="your-coverage">$empcount$</dd>
				    <dt>Dependents</dt>
				    <dd id="dependent-coverage">$dependantcount$</dd>
				    </dl>
			    </div>
				</div> <!-- End /.gutter10 tile -->
			</div> <!-- End /.widget -->
	  </div> <!-- End /.gutter10 -->
 </div> <!-- .span6 -->