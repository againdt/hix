package com.getinsured.iex.hub.vlp37.wrapper.util;

import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.wrapper.ResubmitRequestTypeWrapper;



public final class ResubmitRequestTypeWrapperUtil {
	
	private ResubmitRequestTypeWrapperUtil(){
		
	}
	
	private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
	private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	
	
	public static void addIndicatorsToWrapper(JSONObject obj, ResubmitRequestTypeWrapper wrapper){
		
		Object param = null;
		param = obj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY);
		if(param != null){
			wrapper.setFiveYearBarApplicabilityIndicator((Boolean) param);
		}
		
		param = obj.get(REQUESTERCOMMENTSFORHUB_KEY);
		if(param != null){
			wrapper.setRequesterCommentsForHub((String)param);
		}
	}
	
}
