//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.02 at 02:36:50 PM PDT 
//


package com.getinsured.ahbx.enrollment.sendupdatedenrollment;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for saveEnrollmentConfirmation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="saveEnrollmentConfirmation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enrollmentConfirmationRequest" type="{http://impl.webservice.ahbx.accenture.com/}indvEnrollmentConfirmationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "saveEnrollmentConfirmation", propOrder = {
    "enrollmentConfirmationRequest"
})
public class SaveEnrollmentConfirmation {

    protected IndvEnrollmentConfirmationRequest enrollmentConfirmationRequest;

    /**
     * Gets the value of the enrollmentConfirmationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link IndvEnrollmentConfirmationRequest }
     *     
     */
    public IndvEnrollmentConfirmationRequest getEnrollmentConfirmationRequest() {
        return enrollmentConfirmationRequest;
    }

    /**
     * Sets the value of the enrollmentConfirmationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndvEnrollmentConfirmationRequest }
     *     
     */
    public void setEnrollmentConfirmationRequest(IndvEnrollmentConfirmationRequest value) {
        this.enrollmentConfirmationRequest = value;
    }

}
