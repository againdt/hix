package com.getinsured.eligibility.at.mes.common;

import org.joda.time.DateTime;

/**
 * This class is to have all the utilities related to multiple eligibility span support in AT
 * @author sahasrabuddhe_n
 *
 */
public class MESUtils {
	
	public static DateTime getDate(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date source) {
		return new DateTime(source.getValue().toGregorianCalendar().getTime());
	}

}
