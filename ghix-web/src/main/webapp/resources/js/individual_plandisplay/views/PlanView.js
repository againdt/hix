(function (views) {
	views.Plans = Backbone.View.extend({
		events : {
			'click a.addToCart': 'addToCart',
			'click input.addToCart': 'addToCart',
			'click a.addToCompare' : 'addToCompare',
			'click a.removeCompare' : 'removeCompare',
			'click a.showCart' : 'showCart',
			'click a.goToCart' : 'goToCart',
			'click a.checkOut' : 'checkOut',
			'change input.filter_checkbox' : 'filter'
		},
		el : '#mainSummary',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'loadAll', 'addOne','addToCart','addToCompare','removeCompare','showCart','goToCart','checkOut', 'filter');
			var self = this;


			self.collection.fetch({
				success : function () {

					if(self.collection.length==0){
						$('#mainSummary').empty();
						$('#mainSummary').hide();
						$('#mainSummary_err').show();
						$('#filter').show();
					}
						$('.loading-complete').show();
					
						
					
					self.collection.setSort('planScore', 'desc');
					self.collection.pager();
					self.loadAll();
					/*show/hide cart buttons depending on individual type*/
					if($('#indType').val() == 'ANONYMOUS'){
						$(".addToCart").hide();
						$('#cartBtn').hide();
					}

				},
				silent:true
			});

			self.collection.cartSummary.fetched = false;
			self.cartCollection = options.cartCollection;
			var currentGroupId = $('#currentGroupId').val();

			self.cartCollection.fetch({
				success : function () {
					self.cartCollection.setCartSummary(false,currentGroupId);
					self.collection.cartSummary.fetched = true;
				}
			});

			self.cartModel = options.cartModel;

			$('.showCart').bind('click', this.showCart);
			$('.goToCart').bind('click', this.goToCart);
			$('.filter_checkbox').bind('change', this.filter);


			self.collection.bind('reset', self.addAll);
		},

		addAll : function () {
			var self = this;
			$('#mainSummary').empty();
			
			$('#estimatedCost').find('.plan-details').remove();
			$('#qualityRatingGlobal').find('.plan-details').remove();
			$('#doctorSearch').find('.plan-details').remove();
			$('#facilitySearch').find('.plan-details').remove();
			$('#dentistSearch').find('.plan-details').remove();
			$('#providerSearch').find('.plan-details').remove();
			$('#productType').find('.plan-details').remove();
			$('#discounts').find('.plan-details').remove();
			
			$('#qualityRating1').find('.plan-details').remove();
			$('#qualityRating2').find('.plan-details').remove();
			$('#qualityRating3').find('.plan-details').remove();
			$('#qualityRating4').find('.plan-details').remove();
			$('#qualityRating5').find('.plan-details').remove();

			$('#planDeductible1').find('.plan-details').remove();
			$('#planDeductible2').find('.plan-details').remove();
			$('#planDeductible3').find('.plan-details').remove();
			$('#planDeductible4').find('.plan-details').remove();
			$('#planDeductible5').find('.plan-details').remove();
			$('#planDeductible6').find('.plan-details').remove();
			$('#planDeductible7').find('.plan-details').remove();
			$('#planDeductible8').find('.plan-details').remove();
			$('#planDeductible9').find('.plan-details').remove();
			$('#planDeductible10').find('.plan-details').remove();
			$('#planDeductible11').find('.plan-details').remove();
			$('#planDeductible12').find('.plan-details').remove();
			
			$('#doctorVisit1').find('.plan-details').remove();
			$('#doctorVisit2').find('.plan-details').remove();
			$('#doctorVisit3').find('.plan-details').remove();
			$('#doctorVisit4').find('.plan-details').remove();
			
			$('#test1').find('.plan-details').remove();
			$('#test2').find('.plan-details').remove();
			$('#test3').find('.plan-details').remove();
			
			$('#drug1').find('.plan-details').remove();
			$('#drug2').find('.plan-details').remove();
			$('#drug3').find('.plan-details').remove();
			$('#drug4').find('.plan-details').remove();
			
			$('#outpatient1').find('.plan-details').remove();
			$('#outpatient2').find('.plan-details').remove();
			
			$('#urgent1').find('.plan-details').remove();
			$('#urgent2').find('.plan-details').remove();
			$('#urgent3').find('.plan-details').remove();
			
			$('#hospital1').find('.plan-details').remove();
			$('#hospital2').find('.plan-details').remove();
			
			$('#mentalHealth1').find('.plan-details').remove();
			$('#mentalHealth2').find('.plan-details').remove();
			$('#mentalHealth3').find('.plan-details').remove();
			$('#mentalHealth4').find('.plan-details').remove();
			
			$('#pregnancy1').find('.plan-details').remove();
			$('#pregnancy2').find('.plan-details').remove();
			$('#pregnancy3').find('.plan-details').remove();
			
			$('#specialNeed1').find('.plan-details').remove();
			$('#specialNeed2').find('.plan-details').remove();
			$('#specialNeed3').find('.plan-details').remove();
			$('#specialNeed4').find('.plan-details').remove();
			$('#specialNeed5').find('.plan-details').remove();
			$('#specialNeed6').find('.plan-details').remove();
			
			$('#childrensVision1').find('.plan-details').remove();
			$('#childrensVision2').find('.plan-details').remove();
			
			$('#childrensDental1').find('.plan-details').remove();
			$('#childrensDental2').find('.plan-details').remove();
			$('#childrensDental3').find('.plan-details').remove();
			$('#childrensDental4').find('.plan-details').remove();
			$('#childrensDental5').find('.plan-details').remove();
			$('#childrensDental6').find('.plan-details').remove();
			$('#childrensDental7').find('.plan-details').remove();
			$('#childrensDental8').find('.plan-details').remove();
			$('#childrensDental9').find('.plan-details').remove();

			self.collection.each (self.addOne);
		},

		loadAll : function () {
			var self = this;
			self.collection.loadIssuers();
			self.loadCartSummary();
			self.collection.updateCompareSummary();
		},

		addOne : function (model) {
			var self = this;

			var PERSON_COUNT = $('#PERSON_COUNT').val();
			model.set({"PERSON_COUNT": PERSON_COUNT});

			var PLAN_TYPE = $('#PLAN_TYPE').val();
			model.set({"PLAN_TYPE": PLAN_TYPE});
			
			var ISSUBSIDIZED = $('#ISSUBSIDIZED').val();
			model.set({"ISSUBSIDIZED":ISSUBSIDIZED});
			
			var pgrmType = $('#PGRM_TYPE').val();
			model.set({"PGRM_TYPE":pgrmType});
			
			var elegibilityComplete = $('#elegibilityComplete').val();
			model.set({"elegibilityComplete":elegibilityComplete});
			
			var providerLinkOnOff = $('#providerLinkOnOff').val();
			model.set({"providerLinkOnOff":providerLinkOnOff});
			
			var viewSummary = new Summary({model:model});

			$('#mainSummary').append(viewSummary.render());
			
			$('#estimatedCost').append(viewSummary.getEstimatedCost());
			$('#qualityRatingGlobal').append(viewSummary.getQualityRatingGlobal());
			$('#doctorSearch').append(viewSummary.getDoctor());
			$('#facilitySearch').append(viewSummary.getFacility());
			$('#dentistSearch').append(viewSummary.getDentist());
			$('#providerSearch').append(viewSummary.getProvider());
			$('#productType').append(viewSummary.getProductType());
			$('#discounts').append(viewSummary.getDiscount());
			
			$('#qualityRating1').append(viewSummary.getQualityRating1());
			$('#qualityRating2').append(viewSummary.getQualityRating2());
			$('#qualityRating3').append(viewSummary.getQualityRating3());
			$('#qualityRating4').append(viewSummary.getQualityRating4());
			$('#qualityRating5').append(viewSummary.getQualityRating5());
			
			$('#planDeductible1').append(viewSummary.getDeductible1());
			$('#planDeductible2').append(viewSummary.getDeductible2());
			$('#planDeductible3').append(viewSummary.getDeductible3());
			$('#planDeductible4').append(viewSummary.getDeductible4());
			$('#planDeductible5').append(viewSummary.getDeductible5());
			$('#planDeductible6').append(viewSummary.getDeductible6());
			$('#planDeductible7').append(viewSummary.getDeductible7());
			$('#planDeductible8').append(viewSummary.getDeductible8());
			$('#planDeductible9').append(viewSummary.getDeductible9());
			$('#planDeductible10').append(viewSummary.getDeductible10());
			$('#planDeductible11').append(viewSummary.getDeductible11());
			$('#planDeductible12').append(viewSummary.getDeductible12());
			
			$('#doctorVisit1').append(viewSummary.getDoctorVisit1());
			$('#doctorVisit2').append(viewSummary.getDoctorVisit2());
			$('#doctorVisit3').append(viewSummary.getDoctorVisit3());
			$('#doctorVisit4').append(viewSummary.getDoctorVisit4());
			
			$('#test1').append(viewSummary.getTest1());
			$('#test2').append(viewSummary.getTest2());
			$('#test3').append(viewSummary.getTest3());
			
			$('#drug1').append(viewSummary.getDrug1());
			$('#drug2').append(viewSummary.getDrug2());
			$('#drug3').append(viewSummary.getDrug3());
			$('#drug4').append(viewSummary.getDrug4());
			
			$('#outpatient1').append(viewSummary.getOutpatient1());
			$('#outpatient2').append(viewSummary.getOutpatient2());
			
			$('#urgent1').append(viewSummary.getUrgent1());
			$('#urgent2').append(viewSummary.getUrgent2());
			$('#urgent3').append(viewSummary.getUrgent3());
			
			$('#hospital1').append(viewSummary.getHospital1());
			$('#hospital2').append(viewSummary.getHospital2());
			
			$('#mentalHealth1').append(viewSummary.getMentalhealth1());
			$('#mentalHealth2').append(viewSummary.getMentalhealth2());
			$('#mentalHealth3').append(viewSummary.getMentalhealth3());
			$('#mentalHealth4').append(viewSummary.getMentalhealth4());
			
			$('#pregnancy1').append(viewSummary.getPregnancy1());
			$('#pregnancy2').append(viewSummary.getPregnancy2());
			$('#pregnancy3').append(viewSummary.getPregnancy3());
			
			$('#specialNeed1').append(viewSummary.getSpecialNeed1());
			$('#specialNeed2').append(viewSummary.getSpecialNeed2());
			$('#specialNeed3').append(viewSummary.getSpecialNeed3());
			$('#specialNeed4').append(viewSummary.getSpecialNeed4());
			$('#specialNeed5').append(viewSummary.getSpecialNeed5());
			$('#specialNeed6').append(viewSummary.getSpecialNeed6());
			
			$('#childrensVision1').append(viewSummary.getChildrensVision1());
			$('#childrensVision2').append(viewSummary.getChildrensVision2());
			
			$('#childrensDental1').append(viewSummary.getChildrensDental1());
			$('#childrensDental2').append(viewSummary.getChildrensDental2());
			$('#childrensDental3').append(viewSummary.getChildrensDental3());
			$('#childrensDental4').append(viewSummary.getChildrensDental4());
			$('#childrensDental5').append(viewSummary.getChildrensDental5());
			$('#childrensDental6').append(viewSummary.getChildrensDental6());
			$('#childrensDental7').append(viewSummary.getChildrensDental7());
			$('#childrensDental8').append(viewSummary.getChildrensDental8());
			$('#childrensDental9').append(viewSummary.getChildrensDental9());

		},


		loadCartSummary : function () {
			var self = this;
			var fetchedVal = self.collection.cartSummary.fetched;
			if(fetchedVal == false){
				$('#planCount').html("(Loading...)");
				setTimeout(function(){self.loadCartSummary();}, 1000);
				return;
			}else{
				self.collection.loadCartSummary(self.cartCollection.cartSummary,self.cartCollection.cartItems,self.cartCollection.models);
			}
		},


		addToCart: function (e) {
			var self = this;
			var planModel = "";

			if(e.target.id.indexOf('cart_') >= 0){
				itemId = e.target.id.replace("cart_","");
			}else{
				itemId = e.target.id.replace("plan_","");
			}
			var ISSUBSIDIZED = $('#ISSUBSIDIZED').val();
			var exchangeType = $('#exchangeType').val();
			var stateCode = $('#stateCode').val();
			var multipleItemsEntryOnOff = $('#multipleItemsEntryOnOff').val();
			var Child_Flag = $('#Child_Flag').val();
			
			var fav = "cart";
			if (ISSUBSIDIZED == 'YES')
				{
					fav = "favourites";
				}
			if($("#"+e.target.id).attr("class").indexOf('addedItem') >= 0){
				
				if($("#cart_"+itemId).length){
					
					if (ISSUBSIDIZED == 'YES')
						{
						if(exchangeType != 'STATE')
							{
								$("#cart_"+itemId).attr("class","addToCart addedToFav btn btn-small");
								$("#cart_"+itemId).html('Select');
							}
						else
							{
								$("#cart_"+itemId).attr("class","addToCart addedToFav btn btn-small icon icon-heart");
							}
						
							//$("#cart_"+itemId).attr("class","addToCart addedToFav btn btn-small icon icon-heart");
//						$("#cart_"+itemId).attr("class","addToCart addedToFav btn btn-small");
//						$("#cart_"+itemId).html('Selected');
						
						}
					else
						{
							$("#cart_"+itemId).attr("class","addToCart btn btn-primary btn-small icon-shopping-cart");
							$("#cart_"+itemId).html('&nbsp;'+' Add');
						}
					
					if (ISSUBSIDIZED == 'YES' && exchangeType != 'STATE'){
						$("#cart_"+itemId).attr("title","Add this plan");
					}else{
						$("#cart_"+itemId).attr("title","Add this plan to your "+ fav);
					}
					
				}

				self.collection.emptyCart(self.cartCollection.models);
				if (ISSUBSIDIZED != 'YES' && !(exchangeType == 'STATE'))
					{
						$('#cart-box').show();
					}
				

				planModel = self.collection.searchPlan(this.cartCollection.models, itemId);
				orgId = planModel.id;

				planModel.set({id: planModel.get("orderItemId")});
				planModel.destroy({
					success : function () {
						planModel.set({id: orgId});
						self.cartCollection.remove(planModel);
						self.collection.updateCart(self.cartCollection.models);
					}
				});


				self.collection.removeFromCart(itemId, planModel.get("level").toLowerCase());

				$('#cartmessageplanremove').fadeIn(250,function() {
					setTimeout(function(){
						$('#cartmessageplanremove').hide();
					},1500);
				});
				
				this.collection.updateCartSummary();
				if(exchangeType == 'STATE'){
					 $('#holdcartNM a').addClass('disabled').unbind('click');
		 	 	 	 	if(Child_Flag == 'TRUE'){
		 	 	 	 		$('#holdcartNM a').text("Next to Dental");
		 	 	 	 		}else{
		 	 	 	 		  $('#holdcartNM a').text("Your Cart");
		 	 	 	 	}
		 	 	 }
				
			}
			else{
				
				//Condition NM.
				if(exchangeType == 'STATE'){
					$('#holdcartNM a').removeClass('disabled').bind('click', this.goToCart);
					if(Child_Flag == 'TRUE'){
	 	 	 	 		$('#holdcartNM a').text("Next to Dental");
	 	 	 	 		}else{
	 	 	 	 		$('#holdcartNM a').text("Go to Cart");
	 	 	 	 	}
				}
				
				
				
				//var planAddedForGroup = self.collection.searchCart(self.cartCollection.models,this.collection.cartItems,itemId);
				/*var multipleItemsEntryOnOff = $('#multipleItemsEntryOnOff').val();
				var stateCode = $('#stateCode').val();
				var exchangeType = $('#exchangeType').val();*/
				var planAddedForGroup ;//= false ;
				
				planAddedForGroup = self.collection.searchCart(self.cartCollection.models,this.collection.cartItems,itemId);
				
				/*if(exchangeType == 'STATE'){
					
					 planAddedForGroup = self.collection.searchCart(self.cartCollection.models,this.collection.cartItems,itemId);
					
				}
				else{
					 planAddedForGroup = self.collection.searchCart(this.collection.cartItems,itemId);
					}*/
				
				
				if(planAddedForGroup == false){
					
//					if(exchangeType == 'STATE'){
//						
//						planModel = self.collection.searchPlan(this.collection.models, itemId);
//						var groupsEhb = self.collection.getEhb(self.cartCollection.models,planModel);
//						var ehb = groupsEhb["groupEhb_"+planModel.get("groupId")];
//						if(ehb > 10){
//							alert("Health Plan is already added. If you wish to add this health plan kindly remove the previously added one.");
//							return false;
//						}		
//					}
					
					var healthPlanAdded = self.collection.healthPlanAdded(self.cartCollection.models);
					if(healthPlanAdded == true){
						alert("Health Plan is already added. If you wish to add this Health plan kindly remove the previously added one.");
						return false;
					}
					
					planModel = self.collection.searchPlan(this.collection.models, itemId);
					var groupsEhb = 10;
					
					var  tempPlanType;
					tempPlanType =planModel.get("PLAN_TYPE");
					
					var EXSISTING_SADP_ID = $('#existingSADPEnrollmentID').val();
					

					if( tempPlanType == "Combined"   &&  EXSISTING_SADP_ID > 0 )
					{ //Separate Combined //tempPlanType == "HEALTH" ||

						var disenrollMsg=confirm("This plan includes children's dental coverage. If you choose to enroll in this plan we'll disenroll you automatically from the existing children's dental plan. Is this okay?"); 

						if(disenrollMsg==true) // press Ok button
						{
							
							// disenroll dental and  health

							if( jQuery.inArray(itemId, this.collection.cartItems) === -1 ) {
								$("#"+itemId).attr("class","addToCart addedItem btn btn-primary");
								$("#"+itemId).attr("title","Remove this plan from your "+ fav);//added to fav
								
								if (ISSUBSIDIZED != 'YES' && !(exchangeType == 'STATE'))
								{
									$("#cart").show();
									$('#cart-box').show();
								}

								this.cartModel.save({id: planModel.get("planId"), premium: planModel.get("premiumAfterCredit"), premiumBeforeCredit: planModel.get("premiumBeforeCredit"), planDetailsByMember: planModel.get("planDetailsByMember")},{
									success : function (model,resp) {
										var newModel = new Backbone.Model({id: planModel.id, name: planModel.get("name"), planType: planModel.get("planType"),
											level: planModel.get("level"), premium: planModel.get("premium"), issuer: planModel.get("issuer"), 
											issuerText: planModel.get("issuerText"), networkType: planModel.get("networkType"), deductible: planModel.get("deductible"),
											premiumBeforeCredit: planModel.get("premiumBeforeCredit"), aptc: planModel.get("aptc"), premiumAfterCredit: planModel.get("premiumAfterCredit"),
											groupId: planModel.get("groupId"), groupName: planModel.get("groupName"), groupMembers: planModel.get("groupMembers"), 
											planId: planModel.get("planId"), ehb: planModel.get("ehb"), orderItemId:model.id, totalContribution: planModel.get("totalContribution"), issuerLogo: planModel.get("issuerLogo")});

										self.cartCollection.add(newModel);

										self.collection.emptyCart(self.cartCollection.models);
										self.collection.updateCart(self.cartCollection.models);
										$('.removeFromCart').bind('click', self.addToCart);
										$('.checkOut').bind('click', self.checkOut);
									}
								});

								this.collection.setCartSummary(itemId, planModel.get("level").toLowerCase());

								$('#cartmessageplan').fadeIn(250,function() {
									setTimeout(function(){
										$('#cartmessageplan').hide();
									},1500);
								});
							}else{
								alert("Selected plan is already present in the cart");
							}

						}
						
					}
					else{
					/////end

						/*
					var groupsEhb = self.collection.getEhb(self.cartCollection.models,planModel);
					var ehb = groupsEhb["groupEhb_"+planModel.get("groupId")];
					if(ehb < 10){
						alert("Low Ehb msg. selected health plan does not have all ehb's");
					}else if(ehb > 10.5){
						alert("high Ehb msg. cart already have one health plan");
						return false;
					}else if(ehb > 10){
						alert("high Ehb msg. already have one dental plan in cart so can not add health plan with all ehb's");
						return false;
					}*/
					
					
					
						if( jQuery.inArray(itemId, this.collection.cartItems) === -1 ) {
							
							if(ISSUBSIDIZED != "YES" )
							{
								$("#"+itemId).attr("class","addToCart addedItem btn btn-primary");
							}
							else
							{
								if(exchangeType != 'STATE')
								{
									//$("#"+itemId).attr("class","addToCart addedItem btn btn-primary");
									$("#"+itemId).attr("class","addToCart addedItem btn btn-primary");
									$("#cart_"+itemId).html('Selected');
								}
								else
								{
									$("#"+itemId).attr("class","removeCompare addToCart addedToFav addedItem btn btn-primary");
								}
							}
							$("#"+itemId).attr("title","Remove this plan from your "+ fav);
							
							
							if (ISSUBSIDIZED != 'YES' && !(exchangeType == 'STATE'))
							{
								$("#cart").show();
								$('#cart-box').show();
							}
							this.cartModel.save({id: planModel.get("planId"), premium: planModel.get("premiumAfterCredit"), premiumBeforeCredit: planModel.get("premiumBeforeCredit"), planDetailsByMember: planModel.get("planDetailsByMember"), aptc: planModel.get("aptc")},{
								success : function (model,resp) {
									var newModel = new Backbone.Model({id: planModel.id, name: planModel.get("name"), planType: planModel.get("planType"),
										level: planModel.get("level"), premium: planModel.get("premium"), issuer: planModel.get("issuer"), 
										issuerText: planModel.get("issuerText"), networkType: planModel.get("networkType"), deductible: planModel.get("deductible"),
										premiumBeforeCredit: planModel.get("premiumBeforeCredit"), aptc: planModel.get("aptc"), premiumAfterCredit: planModel.get("premiumAfterCredit"),
										groupId: planModel.get("groupId"), groupName: planModel.get("groupName"), groupMembers: planModel.get("groupMembers"), 
										planId: planModel.get("planId"), ehb: planModel.get("ehb"), orderItemId:model.id, totalContribution: planModel.get("totalContribution"), issuerLogo: planModel.get("issuerLogo")});

									self.cartCollection.add(newModel);

									self.collection.emptyCart(self.cartCollection.models);
									self.collection.updateCart(self.cartCollection.models);
									$('.removeFromCart').bind('click', self.addToCart);
									$('.checkOut').bind('click', self.checkOut);
								}
							});

							this.collection.setCartSummary(itemId, planModel.get("level").toLowerCase());

							$('#cartmessageplan').fadeIn(250,function() {
								setTimeout(function(){
									$('#cartmessageplan').hide();
								},1500);
							});
						}else{
							alert("Selected plan is already present in the cart");
						}
						
					}//disenrollment else 
				


				}
				else{
					alert("Plan for current group is already present in the cart");
				}
				this.collection.updateCartSummary();
			}

			
		},



		showCart: function () {
			var self = this;
			self.collection.emptyCart(self.cartCollection.models);
			$('#cart-box').show();
			self.collection.updateCart(self.cartCollection.models);
			$('.removeFromCart').bind('click', self.addToCart);
			$('.checkOut').bind('click', this.checkOut);
		},
		

		goToCart: function(){
					//If childFlag is true 
					var Child_Flag = $('#Child_Flag').val();
					var nMExistingSADPID = $('#nMExistingSADPID').val();
					
					if(Child_Flag == 'TRUE'){
						if(nMExistingSADPID != null && nMExistingSADPID != ''){ // Case of NM special Enrollment,redirect to renewal page for Dental Plan if existing SADP ID exist.
							window.location = "dentalRenewal";
						}else{ //Open the dental light box.
							
							$('<div id="dentalBox" class="modal bigModal"><div class="dentalModal-header gutter10-lr"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></div><div class=""><iframe height="400" width="700" id="dental" src="dentalTemplate" class="dentalModal-body"></iframe></div></div>')
							.modal();
						}
												
					}else{//Else take to showcart
							var self = this;
							var fullEhb = true;
							var groupsEhb = self.collection.getEhb(self.cartCollection.models);
						if(self.cartCollection.models.length <= 0){
							alert("Cart is empty");
						}
						else{
							window.location = "showcart";
						}
					}
					
				},


		checkOut: function () {
			var self = this;
			var fullEhb = true;
			var groupsEhb = self.collection.getEhb(self.cartCollection.models);
			if(self.cartCollection.models.length <= 0){
				alert("Cart is empty");
			}
			else{
				window.location = "showcart";
			}
			/*}else{
				_.each(groupsEhb, function(groupEhb){
					if(groupEhb < 10){
						fullEhb = false;
					}	
				});
			}
			if(fullEhb){
				window.location = "showcart";
			}else{
				alert("Partial EHB message for some group");
			}*/
		},

		addToCompare: function (e) {
			if(e.target.id.indexOf('ompare_')){
				$('#favmessage').fadeIn(250,function() {
					setTimeout(function(){
						$('#favmessage').hide();
					},1500);
				});
			}
			this.collection.addToCompare(e.target.id.replace("compare_",""));
			$('#compareCount').html(this.collection.compareModels.length);

			var selid = e.target.id.replace("compare_","");

			$('#compare_'+selid + '.addToCompare').hide();
			$('#remove_'+selid + '.removeCompare').show();
		},

		removeCompare: function (e) {
			var self=this;
			/* if not compare view*/
			if(this.collection.showCompare == false){
				this.collection.removeCompare(e.target.id.replace("remove_",""),"pview");
				var selid = e.target.id.replace("remove_","");
				$('#compare_'+selid + '.addToCompare').show();
				$('#remove_'+selid + '.removeCompare').hide();

			}
			else{ /*if from compare view*/

				this.collection.removeCompare(e.target.id.replace("remove_",""),"cview");
				/*if no items in compare array*/
				if(this.collection.compareModels.length < 1)
					this.collection.showCompare == false;

			}
			this.collection.updateCompareSummary();


		},

		getFilterRules : function() {

			var matches = [];
			$(".filter_checkbox").each(function() {
				if (this.checked) {
					matches.push($(this).val());
				}
			});

			var rules = new Object();
			var nameObj = new Object();
			nameObj['type'] = 'pattern';
			nameObj['field'] = 'level';
			nameObj['value'] = matches;
			rules['planName'] = nameObj;

			return rules;
		},

		filter : function(e) {
			//e.preventDefault();
			var rules = this.getFilterRules();
			this.collection.setFieldFilter(rules);
			this.collection.preserveOtherOptions();			
		}

	});

	var Summary = Backbone.View.extend({
		template: _.template($('#resultItemTemplateMainSummary').html()),
		estimatedCostTpl: _.template($('#estimatedCostTpl').html()),
		providerSearchTpl: _.template($('#providerSearchTpl').html()),
		providerSearchLinkTpl: _.template($('#providerSearchLinkTpl').html()),
		productTypeTpl: _.template($('#productTypeTpl').html()),
		discountTpl: _.template($('#discountTpl').html()),
		quolityRatingTpl: _.template($('#quolityRatingTpl').html()),
		deductibleIndTpl: _.template($('#deductibleIndTpl').html()),
		deductibleFlyTpl: _.template($('#deductibleFlyTpl').html()),
		benefitTpl: _.template($('#benefitTpl').html()),
		
		
		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},

		render : function () {
			return this.template(this.model.toJSON());
		},

		getEstimatedCost : function () {return this.estimatedCostTpl({ANNUAL_PREMIUM_AFTER_CREDIT :  this.model.toJSON().annualPremiumAfterCredit, OOP_MAX : this.model.toJSON().oopMax, ESTIMATED_TOTAL : this.model.toJSON().estimatedTotalHealthCareCost});},
		getQualityRatingGlobal : function () {return this.quolityRatingTpl({QUALITY :  this.model.toJSON().issuerQualityRating.GlobalRating});},
		getDoctor : function () {return this.providerSearchTpl({PROVIDERS :  this.model.toJSON().doctors, PROVIDERS_SUPPORTED : this.model.toJSON().doctorsSupported});},
		getFacility : function () {return this.providerSearchTpl({PROVIDERS :  this.model.toJSON().facilities, PROVIDERS_SUPPORTED : this.model.toJSON().facilitiesSupported});},
		getDentist : function () {return this.providerSearchTpl({PROVIDERS :  this.model.toJSON().dentists, PROVIDERS_SUPPORTED : this.model.toJSON().dentistsSupported});},
		getProvider : function () {return this.providerSearchLinkTpl({PROVIDER_LINK :  this.model.toJSON().providerLink});},
		getProductType : function () {return this.productTypeTpl({NETWORK_TYPE :  this.model.toJSON().networkType});},
		getDiscount: function () {return this.discountTpl({LEVEL :  this.model.toJSON().level});},
		
		getQualityRating1 : function () { var issuerQualityRating = this.model.toJSON().issuerQualityRating != null ? this.model.toJSON().issuerQualityRating.GlobalRating : null; return this.quolityRatingTpl({QUALITY : issuerQualityRating});},
		getQualityRating2 : function () { var issuerQualityRating = this.model.toJSON().issuerQualityRating != null ? this.model.toJSON().issuerQualityRating.AccessRating : null; return this.quolityRatingTpl({QUALITY : issuerQualityRating});},
		getQualityRating3 : function () { var issuerQualityRating = this.model.toJSON().issuerQualityRating != null ? this.model.toJSON().issuerQualityRating.StayingHealthyRating : null; return this.quolityRatingTpl({QUALITY : issuerQualityRating});},
		getQualityRating4 : function () { var issuerQualityRating = this.model.toJSON().issuerQualityRating != null ? this.model.toJSON().issuerQualityRating.PlanServiceRating : null; return this.quolityRatingTpl({QUALITY : issuerQualityRating});},
		getQualityRating5 : function () { var issuerQualityRating = this.model.toJSON().issuerQualityRating != null ? this.model.toJSON().issuerQualityRating.ClinicalCareRating : null; return this.quolityRatingTpl({QUALITY : issuerQualityRating});},
		
		getDeductible1 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_INTG_MED_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
		getDeductible2 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_INTG_MED_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
		getDeductible3 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_INTG_MED_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
		getDeductible4 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_INTG_MED_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
		getDeductible5 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
		getDeductible6 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
		getDeductible7 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
		getDeductible8 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
		getDeductible9 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
		getDeductible10 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
		getDeductible11 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
		getDeductible12 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },

		getDoctorVisit1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.PRIMARY_VISIT}); },
		getDoctorVisit2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SPECIAL_VISIT}); },
		getDoctorVisit3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OTHER_PRACTITIONER_VISIT}); },
		getDoctorVisit4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.PREVENT_SCREEN_IMMU}); },
		
		getTest1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.LABORATORY_SERVICES}); },
		getTest2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.IMAGING_XRAY}); },
		getTest3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.IMAGING_SCAN}); },
		
		getDrug1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.GENERIC}); },
		getDrug2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.PREFERRED_BRAND}); },
		getDrug3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.NON_PREFERRED_BRAND}); },
		getDrug4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SPECIALTY_DRUGS}); },
		
		getOutpatient1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_FACILITY_FEE}); },
		getOutpatient2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_SURGERY_SERVICES}); },
		
		getUrgent1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.EMERGENCY_SERVICES}); },
		getUrgent2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.EMERGENCY_TRANSPORTATION}); },
		getUrgent3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.URGENT_CARE}); },
		
		/*getHospital1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_FACILITY_FEE}); },
		getHospital2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_SURGERY_SERVICES}); },*/
		
	    getHospital1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.INPATIENT_HOSPITAL_SERVICE}); },
	    getHospital2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.INPATIENT_PHY_SURGICAL_SERVICE}); },
		
		getMentalhealth1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MENTAL_HEALTH_OUT}); },
		getMentalhealth2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MENTAL_HEALTH_IN}); },
		getMentalhealth3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SUBSTANCE_OUTPATIENT_USE}); },
		getMentalhealth4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SUBSTANCE_INPATIENT_USE}); },
		
		getPregnancy1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.PRENATAL_POSTNATAL}); },
		getPregnancy2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.INPATIENT_HOSPITAL_SERVICE}); },
		getPregnancy3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.INPATIENT_PHY_SURGICAL_SERVICE}); },
		
		getSpecialNeed1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.HOME_HEALTH_SERVICES}); },
		getSpecialNeed2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.OUTPATIENT_REHAB_SERVICES}); },
		getSpecialNeed3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.HABILITATION}); },
		getSpecialNeed4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.SKILLED_NURSING_FACILITY}); },
		getSpecialNeed5 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DURABLE_MEDICAL_EQUIP}); },
		getSpecialNeed6 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.HOSPICE_SERVICE}); },
		
		getChildrensVision1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.RTN_EYE_EXAM_CHILDREN}); },
		getChildrensVision2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.GLASSES_CHILDREN}); },
		
		getChildrensDental1 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ACCIDENTAL_DENTAL}); },
		getChildrensDental2 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_ADULT}); },
		getChildrensDental3 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_CHILD}); },
		getChildrensDental4 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.DENTAL_CHECKUP_CHILDREN}); },
		getChildrensDental5 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_ADULT}); },
		getChildrensDental6 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_CHILD}); },
		getChildrensDental7 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ORTHODONTIA_ADULT}); },
		getChildrensDental8 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.ORTHODONTIA_CHILD}); },
		getChildrensDental9 : function () { return this.benefitTpl({BENEFIT : this.model.toJSON().planTier1.RTN_DENTAL_ADULT}); },
				
	});
})(App.views);
