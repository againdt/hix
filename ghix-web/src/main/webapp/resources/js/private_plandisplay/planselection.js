(function(){
	_.templateSettings = {
		    interpolate: /\<\@\=(.+?)\@\>/gim,
		    evaluate: /\<\@(.+?)\@\>/gim
		};
	window.App = {};
	App.collections = {};
	App.models = {};
	App.views = {};
	App.backbone = {};
	
	// Defer initialization until doc ready.
	$(function(){
		var collection = new App.collections.Plans();
		var cartCollection = new App.collections.Carts();
		var cartModel = new App.models.Cart();
		App.views.compareView = new App.views.CompareView({collection:collection});
		App.views.detailView = new App.views.DetailView({collection:collection});
		App.views.plans = new App.views.Plans({collection:collection,cartModel:cartModel,cartCollection:cartCollection});
		new App.views.Pagination({collection:collection});
		new App.views.SortView({collection:collection});
	});

})();