////store dateOfChange
//var eventInfo = { "eventCategory": "",
//          "eventSubCategory":"",
//          "changedApplicants":[]
//        };
//var eventInfoArray=[];
////variable to tracking new household member added in Marital Status
//var maritalHouseholdAdded=false;
////global variable to hold dependent Added
//var dependentForHouseholdAdded =  false;
//var dependentHousholdStarted = 0;
//
//
//var eventInfoArrayTemp= [];
//
////=========================================================================
////// Including ngTranslate: START //
////=========================================================================
//angular.module("ngTranslate", ["ng"]).config(["$provide", function (t) {
//    $TranslateProvider = function () {
//        var t, n = {};
//        this.translations = function (t, r) {
//            if (!t && !r) return n;
//            if (t && !r) {
//                if (angular.isString(t)) return n[t];
//                n = t
//            } else n[t] = r
//        }, this.uses = function (r) {
//            if (!r) return t;
//            if (!n[r]) throw Error("$translateProvider couldn't find translationTable for langKey: '" + r + "'");
//            t = r
//        }, this.$get = ["$interpolate", "$log", function (r, a) {
//            return $translate = function (e, i) {
//                var l = t ? n[t][e] : n[e];
//                return l ? r(l)(i) : (a.warn("Translation for " + e + " doesn't exist"), e)
//            }, $translate.uses = function (n) {
//                return n ? (t = n, void 0) : t
//            }, $translate
//        }]
//    }, t.provider("$translate", $TranslateProvider)
//}]), angular.module("ngTranslate").directive("translate", ["$filter", "$interpolate", function (t, n) {
//    var r = t("translate");
//    return {
//        restrict: "A",
//        scope: !0,
//        link: function (t, a, e) {
//            e.$observe("translate", function (r) {
//                t.translationId = angular.equals(r, "") ? n(a.text())(t.$parent) : r
//            }), e.$observe("values", function (n) {
//                t.interpolateParams = n
//            }), t.$watch("translationId + interpolateParams", function () {
//                a.html(r(t.translationId, t.interpolateParams))
//            })
//        }
//    }
//}]), angular.module("ngTranslate").filter("translate", ["$parse", "$translate", function (t, n) {
//    return function (r, a) {
//        return angular.isObject(a) || (a = t(a)()), n(r, a)
//    }
//}]);
////=========================================================================
////// Including ngTranslate: END //
////=========================================================================
//
//// 1. Module named 'newMexico'
// var newMexico = angular.module('newMexico', ['ngRoute', 'ui.bootstrap', 'ngTranslate']);
//
////=========================================================================
////// RootScope: START //
////=========================================================================
// //add wrappers for common methods in root scope
// newMexico.run(function($rootScope) {
//  //add saveSSAPJSON to root scope as this will be common for all controllers
//  //$rootScope.saveSSAPJSON = saveSSAPJSON;
//  $rootScope.counter = 0;
//  $rootScope.lceCode = "N/A";
//  $rootScope.lceEvent = "N/A"
//  $rootScope.lceCounter = 0;
// });
////=========================================================================
//////RootScope: END //
////=========================================================================
//
// 
////2. Configure our routes
// newMexico.config(function($routeProvider, $locationProvider) {
// $routeProvider
//   .when('/', { templateUrl : '../../resources/js/templates/lce/home.html', controller  : 'lifeChangeCtrl' })
//   .when('/next', { templateUrl : '../../resources/js/templates/lce/next.html', controller  : 'nextCtrl' })
//   .when('/incacerationComplete', { templateUrl : '../../resources/js/templates/lce/nextIncarceration.html', controller  : 'nextCtrl' });
// });
//
//
//// 3. Factory
//newMexico.factory('myService', function() {
//	//variable to track parialCount
//	var partialCount=0;
//	
//  // Declare variable in the entire scope
//  var lifeEventSelection=[];
//  // Declare list of incomes for income selection
//  var incomeTypes = [
//   {type: "Job", url: "../../resources/js/templates/lce/income-job.html", value: false},
//   {type: "Self-Employed", url: "../../resources/js/templates/lce/income-self.html", value: false},
//   {type: "Social Security Benefits", url: "../../resources/js/templates/lce/income-ssb.html", value: false},
//   {type: "Unemployement", url: "../../resources/js/templates/lce/income-unemployment.html", value: false},
//   {type: "Retirement/Pension", url: "../../resources/js/templates/lce/income-retire.html", value: false},
//   {type: "Capital Gains", url: "../../resources/js/templates/lce/income-capitalgains.html", value: false},
//   {type: "Investment Income", url: "../../resources/js/templates/lce/income-invest.html", value: false},
//   {type: "Rental or Royal Income", url: "../../resources/js/templates/lce/income-rental.html", value: false},
//   {type: "Farming/Fishing Income", url: "../../resources/js/templates/lce/income-farming.html", value: false},
//   {type: "Alimony Money", url: "../../resources/js/templates/lce/income-alimony.html", value: false},
//   {type: "Other", url: "../../resources/js/templates/lce/income-other.html", value: false},
//   ];
//  incomeTypes.selected = 0;
//
//  // Array of partials for adding dependents
//  var dependentForms = ['../../resources/js/templates/lce/maritalstatus-details.html',
//    '../../resources/js/templates/lce/maritalstatus-address.html',
//    '../../resources/js/templates/lce/maritalstatus-ssn.html',
//    '../../resources/js/templates/lce/maritalstatus-citizenship.html',
//    '../../resources/js/templates/lce/maritalstatus-ethnicityrace.html',
//    '../../resources/js/templates/lce/maritalstatus-special.html',
//    '../../resources/js/templates/lce/maritalstatus-summary.html',
//    '../../resources/js/templates/lce/maritalstatus-income.html',
//    '../../resources/js/templates/lce/maritalstatus-incomededuction.html',
//    '../../resources/js/templates/lce/maritalstatus-incomesummary.html',
//    '../../resources/js/templates/lce/maritalstatus-other.html'
//    ];
//  var addDependents = [{ partials: dependentForms, value: false}];
//
//  var currentEventService;
//  // Factory returns the 2 functions:
//  // 1. getFiltered..
//  // 2. setLifeEvent...
//  // 3.set and get currentEvent
//  return {
//    getFilteredEvents: function() {
//      // Declare empty array 'filtered'
//      var filtered = [];
//      // Iterate through 'lifeEventSelection', which consists of
//      for (var i = 0; i < lifeEventSelection.length; i++) {
//        if (lifeEventSelection[i].value) {
//          filtered.push(lifeEventSelection[i]);
//        }
//      }
//      return filtered;
//    },
//    // Set function takes in the $scope.lifeEvents as argument and makes 'lifeEventSelection' equal to that
//    // We filter this above in the for loop and push to empty array 'filterd'
//    setLifeEventSelection: function (cat) {
//      lifeEventSelection = cat;
//    },
//    // This is for the income selection process
//    getIncomeEvents: function() {
//        return incomeTypes;
//    },
//    editIncomeEvents: function(event) {
//        for (var i = 0; i < incomeTypes.length; i++) {
//            if (incomeTypes[i].type === event.type) {
//              incomeTypes[i].value = !incomeTypes[i].value;
//              return incomeTypes;
//            }
//        }
//    },
//    getDepForms: function() {
//        return addDependents;
//    },
//    setDepForms: function(event) {
//        for (var i = 0; i < addDependents.length; i++) {
//          addDependents[i].value = !addDependents[i].value;
//        }
//        return addDependents;
//    },
//    setCurrentEvent:function(event) {
//      currentEventService=event;
//      },
//    getCurrentEvent:function(){
//      return currentEventService;
//    },
//    setMember: function(event) {
//    	currentMember = event;
//    },
//    getMember: function() {
//    	return currentMember;
//    },
//    getPartialCount:function(){
//    	return partialCount;
//    },
//    setPartialCount:function(patialCnt){
//    	partialCount=patialCnt;
//    }
//
//  };
//}); // End of Factory
//
//
//  //change it global as this will be used in Number of dependents controller
//  //Array of number of dependents partials that user has to traverse through
//  var depedentsArray = ['../../resources/js/templates/lce/numofdep-add.html',
//    '../../resources/js/templates/lce/numofdep-remove.html',
//    '../../resources/js/templates/lce/numofdep-complete.html',
//  ];
//
////4. Controller 1 'lifeChangeCtrl':
// newMexico.controller('lifeChangeCtrl', ['$scope', '$http','$location', 'myService', '$translate', '$rootScope', '$timeout', function($scope, $http, $location, myService, $translate, $rootScope, $timeout) {
//	 function reLoadHomeView() {
// 		 if( window.localStorage ) {
//		    if( !localStorage.getItem('ignoreReload') ) {
//		      localStorage['ignoreReload'] = true;
//		      window.location.reload();
//		    }  
//		    else {
//		      localStorage.removeItem('ignoreReload');
//		    }
// 		 }
//	 }
//	 
//	$timeout(reLoadHomeView, 0);
//	 
//    $scope.toggleLang = function () {
//        $translate.uses() === 'english' ? $translate.uses('spanish') : $translate.uses('english');
//    };	 
//   // Array of marital status partials that user has to traverse
//   var maritalArray = ['../../resources/js/templates/lce/maritalstatus-details.html',
//     '../../resources/js/templates/lce/maritalstatus-address.html',
//     '../../resources/js/templates/lce/maritalstatus-ssn.html',
//     '../../resources/js/templates/lce/maritalstatus-citizenship.html',
//     '../../resources/js/templates/lce/maritalstatus-ethnicityrace.html',
//     '../../resources/js/templates/lce/maritalstatus-special.html',
//     '../../resources/js/templates/lce/maritalstatus-summary.html',
//     '../../resources/js/templates/lce/maritalstatus-income.html',
//     '../../resources/js/templates/lce/maritalstatus-incomededuction.html',
//     '../../resources/js/templates/lce/maritalstatus-incomesummary.html',
//     '../../resources/js/templates/lce/maritalstatus-other.html',
//     '../../resources/js/templates/lce/maritalstatus-complete.html'
//   ];
//   // Array of incarceration partials that user has to traverse through
//   var incarcerationArray = ['../../resources/js/templates/lce/incarceration-next.html'];
//   var lossArray = ['../../resources/js/templates/lce/lossofmin-summary.html', '../../resources/js/templates/lce/lossofmin-complete.html'];
//   var gainArray = ['../../resources/js/templates/lce/gainofmin-summary.html', '../../resources/js/templates/lce/gainofmin-complete.html'];
//   var addressArray = ['../../resources/js/templates/lce/changeinaddress-complete.html'];
//   var lawArray = ['../../resources/js/templates/lce/lawfulpresence-complete.html'];
//   var additionalDependents = [];
//   var incomeArray = ['../../resources/js/templates/lce/income-summary.html','../../resources/js/templates/lce/income-complete.html'];
//   var nameArray = ['../../resources/js/templates/lce/namechange-summary.html','../../resources/js/templates/lce/namechange-complete.html'];
//   var tribeArray = ['../../resources/js/templates/lce/tribechange-summary.html','../../resources/js/templates/lce/tribechange-complete.html'];
//
//   // Array of sub tasks to go to the sidebar
//   var eventdate = ['Event Date'];
//   var eventArray = ['Table of Dependents'];
//   var subMarital = ['Personal Information', 'Address', 'Social Security Number', 'Citizenship', 'Ethnicity & Race', 'Special Circumstances', 'Individual Summary', 'Income', 'Income Deductions', 'Income Summary', 'Additional Questions', 'Complete!'];
//   var subDependents = ['Additonal Dependents', 'Dependent Information'];
//   var subarray = ['Edit Income', 'Income Summary'];
//   var otherarray = ['Complete!'];
//   var summaries = ['Summary', 'Complete!'];
//
//
//   // Reasons for each life event
//   var maritalStatusReason = "A person's state of being single, married, separated, divorced, or widowed.";
//   var dependentReason = "Changes in dependents status include a birth, adoption, or death, marriage or divorce, a child no longer being eligible due to age restrictions, marriage, or other restrictions, adding a parent to your claim, etc.";
//   var dummyReason = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
//
//   // Scope variable that contains the life event types and the initial page that the user sees upon selection
//   $scope.lifeEvents = [{eventCode:"LCE_MARITAL_STATUS",eventName:"Marital Status ", value: false, eventdate: eventdate, subcategories: subMarital, url: '../../resources/js/templates/lce/maritalstatus.html', partials: maritalArray, reason: maritalStatusReason},
//                        {eventCode:"LCE_NUMBER_OF_DEPENDENTS",eventName:"Add or Remove a Dependent", value: false, eventdate: eventdate, subcategories: subDependents, url: '../../resources/js/templates/lce/numberofdependents.html', partials: depedentsArray, reason: dependentReason},
//                        {eventCode:"LCE_INCOME",eventName:"Income Changes", value: false, eventdate: eventArray, subcategories: subarray, url: '../../resources/js/templates/lce/income.html', partials: incomeArray, reason: dummyReason},
//                        {eventCode:"LCE_CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS",eventName:"Address Change", value: false, eventdate: eventArray, subcategories: otherarray, url: '../../resources/js/templates/lce/changeinaddress.html', partials: addressArray, reason: dummyReason},
//                        {eventCode:"LCE_LAWFUL_PRESENCE",eventName:"Citizenship (Lawful Presence)", value: false, eventdate: eventArray, subcategories: otherarray, url: '../../resources/js/templates/lce/lawfulpresence.html', partials: lawArray, reason: dummyReason},
//                        {eventCode:"LCE_CHANGE_IN_NAME",eventName:"Name Change", value: false, eventdate: eventArray, subcategories: summaries, url: '../../resources/js/templates/lce/namechange.html', partials: nameArray, reason: dummyReason},
//                        {eventCode:"LCE_INCARCERATION",eventName:"Incarceration (Jail) Status", value: false, eventdate: eventArray, subcategories: otherarray, url: '../../resources/js/templates/lce/incarceration.html', partials: incarcerationArray, reason: dummyReason},
//                        {eventCode:"LCE_CHANGE_IN_TRIBE_STATUS",eventName:"Tribe Status", value: false, eventdate: eventArray, subcategories: summaries, url: '../../resources/js/templates/lce/tribechange.html', partials: tribeArray, reason: dummyReason},
//                        {eventCode:"LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE",eventName:"Loss in Minimum Essential Coverage", value: false, eventdate: eventArray, subcategories: summaries, url: '../../resources/js/templates/lce/lossofmin.html', partials: lossArray, reason: dummyReason},
//                        {eventCode:"LCE_GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE",eventName:"Gain Other Minimum Essential Coverage", value: false, eventdate: eventArray, subcategories: summaries, url: '../../resources/js/templates/lce/gainothermin.html', partials: gainArray, reason: dummyReason},
//                        {eventCode:"LCE_OTHER",eventName:"Change Due to Other Reasons", value: false, eventdate: eventArray, subcategories: otherarray, url: '../../resources/js/templates/lce/other.html', reason: dummyReason}
//                        ];
//
//  //code to set previously selected values true.
//  //This code get selected events from service and set  $scope.lifeEvents[].value to true.
//   try{
//     $scope.checkedElements= myService.getFilteredEvents();
//     for(var loopVar=0;loopVar< $scope.lifeEvents.length;loopVar++){
//       for(var j=0;j< $scope.checkedElements.length;j++){
//        if( $scope.lifeEvents[loopVar].eventCode==$scope.checkedElements[j].eventCode){
//          $scope.lifeEvents[loopVar].value=true;
//          break;
//        }
//       }
//     }
//
//   }catch(err){
//     //very first time getFilteredEvents throws exception because var lifeEventSelection in service is null
//   }
//
//   // Scope function for the next button on the page with the life event changes
//   // This goes through the route because we give it a path in the html
//  var lifeEventCounter = 0;
//
//  $scope.currentLifeEventSeelcted = 'Nothing';
//
//  var counter = 0;
//  $scope.next = function(path) {
//	counter++;
//	$scope.currentLifeEventSeelcted = $scope.lifeEvents[lifeEventCounter].eventName;
//
//    myService.setLifeEventSelection($scope.lifeEvents);
//    $location.path(path);
//    $scope.selectedEvents = myService.getFilteredEvents();
//
//    //Code to check if marital status selected or not
//    var maritalStatusAdded=false;
//    for (var i = 0; i < $scope.selectedEvents.length; i++) {
//      if($scope.selectedEvents[i].eventName=="Marital Status"){
//        maritalStatusAdded=true;
//      }
//    }
//    if(!maritalStatusAdded){
//      removeHousehold();//If marital status is not selected then remove household member (If added)
//    }
//
//    $scope.lifeEventCounter = lifeEventCounter;
//
//
//    //>> Hide all subcategories
//    hideAllSubcategories();
//
//    //>> Set the rootScope variables to update the header and the omnious sidebar
//    $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter].eventCode;
//	$rootScope.lceEvent = $scope.selectedEvents[lifeEventCounter].eventName;
//	
//    }; // End of Next function
//}]); // End of Controller 1
//
////Set lifeEventCounter to 0 - later to increment
//var lifeEventCounter = 0;
//
////5. Controller 2 'nextCtrl':
// newMexico.controller('nextCtrl', ['$scope', '$http','$location', 'myService', '$translate', '$rootScope', function($scope, $http, $location, myService, $translate, $rootScope) {
//     
//	 $scope.toggleLang = function () {
//	        $translate.uses() === 'english' ? $translate.uses('spanish') : $translate.uses('english');
//	    };	 
//
//   // Relationships and codes
//   $scope.relationships = [
//                           {code: '01', type:'Spouse' },
//                           {code: '03', type:'Parent (father or mother) ' },
//                           {code: '04', type:'Grandparent (grandfather or grandmother)' },
//                           {code: '05', type:'Grandchild (grandson or granddaughter)' },
//                           {code: '06', type:'Uncle or Aunt' },
//                           {code: '07', type:'Nephew or Niece' },
//                           {code: '08', type:'First Cousin' },
//                           {code: '09', type:'Adopted Child (son or daughter)' },
//                           {code: '10', type:'Foster Child (foster son or foster daughter)' },
//                           {code: '11', type:'Son-in-law or Daughter-in-law' },
//                           {code: '12', type:'Brother-in-law or Sister-in-law' },
//                           {code: '13', type:'Mother-in-law or Father-in-law' },
//                           {code: '14', type:'Sibling (brother or sister)' },
//                           {code: '15', type:'Ward' },
//                           {code: '16', type:'Stepparent (stepfather or stepmother)' },
//                           {code: '17', type:'Stepchild (stepson or stepdaughter)' },
//                           {code: '18', type:'Self' },
//                           {code: '19', type:'Child (son or daughter)' },
//                           {code: '23', type:'Sponsored dependent' },
//                           {code: '24', type:'Dependent of a minor dependent' },
//                           {code: '25', type:'Former spouse' },
//                           {code: '26', type:'Guardian' },
//                           {code: '31', type:'Court-appointed guardian' },
//                           {code: '38', type:'Collateral dependent' },
//                           {code: '53', type:'Domestic partner' },
//                           {code: '60', type:'Annuitant' },
//                           {code: 'D2', type:'Trustee' },
//                           {code: 'G8', type:'Unspecified relationship' },
//                           {code: 'G9', type:'Unspecified relative' },
//                           {code: '03-53', type: "Parent's domestic partner"},
//                           {code: '53-19', type:'Child of domestic partner'}
//                           ];
//
//   $scope.suffix = ['Jr.', 'Sr.', 'I', 'II', 'III', 'IV', 'V'];
//  // Set counter to 0 - later to increment...
//  // Set lifeEventCounter to 0 - later to increment...
//  var counter = 0;
//  myService.setPartialCount(counter);
//  var summaryPageAdded=false;
//  var nextButtonLifeEventCounter = 0;
//  
//  //When next controller loads... Populate global array of all households.
//  //While adding new dependents...While Marriage event add new entry to this array
//  //Add required variables to object
//  eventInfoArrayTemp = [];
//  //add temp array to controller scope
//  $scope.eventInfoArrayTemp=eventInfoArrayTemp;
//  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//
//  //push the elements in the temp event array
//  for(var i=0;i<householdArray.length;i++){
//    
//      var tempEventInfo = {
//    		  changeInAddressDate:'',
//    		  citizenShipChangeDate:'',
//    		  tribeChangeDate:'',
//    		  changeInLossDate:'',
//    		  nameChangeDate:'',
//    		  incomeChangeDate:'',
//    		  incomeChangeIndicator:'',
//    		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//    		  hasOtherIncomeTypes:'No',
//    		  lossEventSubCategory:'',
//    		  changeInGainDate:'',
//    		  gainEventSubCategory:'',
//    		  dateOfChange:''
//        };
//      eventInfoArrayTemp.push(tempEventInfo);
//   
//  }
//  
//  //$scope.incomeTypes = myService.getIncomeEvents();
//
//  // Now that the scope was filtered in the above function in the factory, we create a scope variable
//  $scope.selectedEvents = myService.getFilteredEvents();
//  // Create a scope variable 'currentPage' to equal the $scope.selectedEvents and by index: 'lifeCounter'
//  $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
//  //this variable is used to store event level temporary information(such as eventDate, which will be passed to applicantDate)
//  $scope.currentEvent=$scope.selectedEvents[lifeEventCounter];
//  myService.setCurrentEvent($scope.currentEvent);
//  // SSAP JSON object that we need to save to
//   $scope.jsonObject = ssapJson;
//   $scope.eventInfo= eventInfo;
//   //wrapper for save method
//   $scope.saveSSAPJSON = function(eventSubCategory) {
//     saveSSAPJSON($scope,lifeEventCounter);
//   };
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// currentPageSidebar
//   $scope.currentPageSidebar = "Blah";
//   $scope.test = $scope.lifeEventCounter;
//
//   // Scope function 'next' for all partials after the initial partial that the user sees after they select life events
//   $scope.nextButtonLifeEvent;
//   var sideBarCounter = 0;
//   $scope.next = function() {
//
//   //>> Updating the rootScope variables
//   if (counter == $scope.selectedEvents[lifeEventCounter].partials.length) {
//	   if ($scope.selectedEvents[lifeEventCounter+1] == undefined) {
//		   $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter].eventCode;
//		   $rootScope.lceEvent = $scope.selectedEvents[lifeEventCounter].eventName;
//	   } else {
//		   $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter+1].eventCode;
//		   $rootScope.lceEvent = $scope.selectedEvents[lifeEventCounter+1].eventName;
//		   $rootScope.lceCounter++;
//	   }
//   } else {
//	   $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter].eventCode;
//	   $rootScope.lceEvent = $scope.selectedEvents[lifeEventCounter].eventName;
//   }
//    	
//	// SIDEBAR FUNCTION
//    sideBarCounter++;
//    var subcat = $scope.selectedEvents[lifeEventCounter].subcategories.length + 2;
//    if ( $rootScope.lceCounter === 0) {
//	    for (var i = 0; i < subcat; i++) {
//	        if (sideBarCounter == i) {
//	          var subId = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
//	          var subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i-1);
//	          $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].subcategories[(i-2)] + ' &nbsp; <i class="icon-ok"></i>');
//	          if ( $scope.selectedEvents[lifeEventCounter].subcategories[(i-2)] == undefined ) {
//	        	$(subIdRPrevious).html("Table of Dependents" + ' &nbsp; <i class="icon-ok"></i>');
//	          }
//	          $(subId).html($scope.selectedEvents[lifeEventCounter].subcategories[(i-1)] + ' &nbsp; <i class="icon-chevron-right"></i>');
//	          $('.subcategories2').addClass('show');
//	        } 
//	    }
//		//>> The first subcategory that displays will have a cheveron
//	   	$('#stuff1').find('.subcategories').html('Table of Dependents '+ ' &nbsp; <i class="icon-chevron-right"></i>');
//	   	console.log("=======");
//	   	console.log(i);
// 	} else if ( $rootScope.lceCounter === 1) {
// 		$('#stuff0').find('.subcategories').hide();
// 		$('#stuff0').find('.subcategories2').hide();
// 		$('#stuff1').find('.subcategories').show();
//		if (counter < $scope.selectedEvents[lifeEventCounter].subcategories.length) {
//		  $('#stuff1').find('.subcategories2').show();
//		  //>>>
//		  for (var i = 0; i < 10; i++) {
//			  if (counter == i) {
//			    subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
//			    if ( $scope.selectedEvents[lifeEventCounter].eventdate[i] == undefined ) {
//			    	  $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].subcategories[i-1] + ' &nbsp; <i class="icon-ok"></i>');
//			    	} else {
//			    	  $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].eventdate[i] + ' &nbsp; <i class="icon-ok"></i>');
//			    	}
//			    $('#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i+1)).html($scope.selectedEvents[lifeEventCounter].subcategories[i] + ' &nbsp; <i class="icon-chevron-right"></i>');
//			  }
//			}
//		} else {
//		  $('#stuff1').find('.subcategories2').hide();
//		}
//		//>> The first subcategory that displays will have a cheveron
//	   	$('#stuff2').find('.subcategories').html('Table of Dependents '+ ' &nbsp; <i class="icon-chevron-right"></i>');
//	   	console.log("=======");
//	   	console.log(i);
//	} else if ( $rootScope.lceCounter  === 2) {
//		$('#stuff1').find('.subcategories').hide();
//		$('#stuff1').find('.subcategories2').hide();
//		$('#stuff2').find('.subcategories').show();
//		if (counter < $scope.selectedEvents[lifeEventCounter].subcategories.length) {
//		  $('#stuff2').find('.subcategories2').show();
//		  //>>>
//		  for (var i = 0; i < 10; i++) {
//			  if (counter == i) {
//			    subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
//			    if ( $scope.selectedEvents[lifeEventCounter].eventdate[i] == undefined ) {
//			    	  $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].subcategories[i-1] + ' &nbsp; <i class="icon-ok"></i>')
//			    	} else {
//			    	  $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].eventdate[i] + ' &nbsp; <i class="icon-ok"></i>');
//			    	}
//			    $('#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i+1)).html($scope.selectedEvents[lifeEventCounter].subcategories[i] + ' &nbsp; <i class="icon-chevron-right"></i>')
//			  }
//			}
//		} else {
//		  $('#stuff2').find('.subcategories2').hide()
//		}
//	} else if ( $rootScope.lceCounter  === 3) {
//		$('#stuff2').find('.subcategories').hide();
//		$('#stuff2').find('.subcategories2').hide();
//		$('#stuff3').find('.subcategories').show();
//		if (counter < $scope.selectedEvents[lifeEventCounter].subcategories.length) {
//		  $('#stuff3').find('.subcategories2').show();
//		  //>>>
//		  for (var i = 0; i < 10; i++) {
//			  if (counter == i) {
//			    subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
//			    if ( $scope.selectedEvents[lifeEventCounter].eventdate[i] == undefined ) {
//			    	  $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].subcategories[i-1] + ' &nbsp; <i class="icon-ok"></i>')
//			    	} else {
//			    	  $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].eventdate[i] + ' &nbsp; <i class="icon-ok"></i>');
//			    	}
//			    $('#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i+1)).html($scope.selectedEvents[lifeEventCounter].subcategories[i] + ' &nbsp; <i class="icon-chevron-right"></i>')
//			  }
//			}
//		} else {
//		  $('#stuff3').find('.subcategories2').hide()
//		}
//	}
//   // SIDEBAR FUNCTION
//
//
//     //if counter is lenght-1 then set the next event in the scope as success page will be partials.length-1
//     if (counter === $scope.selectedEvents[lifeEventCounter].partials.length-1) {
//       nextButtonLifeEventCounter++;
//       if(nextButtonLifeEventCounter < $scope.selectedEvents.length){
//         $scope.nextButtonLifeEvent = ": "+$scope.selectedEvents[nextButtonLifeEventCounter].eventName;
//       }
//       else{
//         $scope.nextButtonLifeEvent="";
//       }
//       
//       if(!summaryPageAdded){
//    	   eventInfo.eventCategory = $scope.selectedEvents[lifeEventCounter].eventCode;
//    	  
//    	   if((eventInfo.eventCategory === 'LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE' || eventInfo.eventCategory === 'LCE_GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE' )&& eventInfo.changedApplicants.length>0){
//    		   for(var i=0;i<eventInfo.changedApplicants.length;i++){
//    			   var tmpEventInfo= {changedApplicants:[],eventCategory: '',eventSubCategory:''};
//                   tmpEventInfo.changedApplicants=[];
//                   tmpEventInfo.changedApplicants.push({"personId":eventInfo.changedApplicants[i].personId,"eventDate":eventInfo.changedApplicants[i].eventDate});
//                   tmpEventInfo.eventCategory=eventInfo.eventCategory;
//                   
//                   var taxHousehold=$scope.jsonObject.singleStreamlinedApplication.taxHousehold;
//                   for(var j=0;j<taxHousehold.length;j++ ){
//                	   for(var k=0;k < taxHousehold[j].householdMember.length ;k++){
//                		 if(   taxHousehold[j].householdMember[k].personId == eventInfo.changedApplicants[i].personId){
//                			 
//                			 if((eventInfo.eventCategory === 'LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE')){
//                				 tmpEventInfo.eventSubCategory=eventInfoArrayTemp[k].lossEventSubCategory;
//                			 }else{
//                				 tmpEventInfo.eventSubCategory=eventInfoArrayTemp[k].gainEventSubCategory;
//                			 }
//                			 break;
//                		 }
//                	   }
//                   }
//                   eventInfoArray.push(tmpEventInfo);
//    		   }
//    	   }else{
//    		   var tmpEventInfo= {changedApplicants:[],eventCategory: '',eventSubCategory:''};
//               tmpEventInfo.changedApplicants=eventInfo.changedApplicants;
//               tmpEventInfo.eventCategory=eventInfo.eventCategory;
//               tmpEventInfo.eventSubCategory=eventInfo.eventSubCategory;
//
//               eventInfoArray.push(tmpEventInfo);
//    	   }
//    	   
//           initEventInfo();
//       }
//      
//       
//     }
//
//     // Loop through 'lifeEventCounter' (earlier set to 0) to increment if counter equals the number of selected life event partials
//     // This condition is to check if we reach the end of the current partial list and need to go to the next life event
//     if (counter === $scope.selectedEvents[lifeEventCounter].partials.length) {
//        lifeEventCounter++;
//        counter = 0;
//        myService.setPartialCount(counter);
//        if(lifeEventCounter==$scope.selectedEvents.length){
//        	//console.log(lifeEventCounter==$scope.selectedEvents.length);
//        	if(!summaryPageAdded){
//        		summaryPageAdded=true;
//        		
//        		lifeEventCounter--;
//            	$scope.currentEvent.partials.push(['../../resources/js/templates/lce/summary.html', '../../resources/js/templates/lce/review-dummy.html']);
//            	flatten($scope.currentEvent.partials);
//            	$scope.currentEvent.partials = flatten($scope.currentEvent.partials);
//            	counter=$scope.currentEvent.partials.length- 2;
//             $scope.currentPage =$scope.currentEvent.partials[counter];// '../../resources/js/templates/lce/review-dummy.html';
//             counter++;
//             myService.setPartialCount(counter);
////             debugger
//        	}
//        	
//        }else{
//        	//debugger
//        	$scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
//        	//debugger
//        }
//        //this variable is used to store event level temporary information(such as eventDate, which will be passed to applicantDate)
//        $scope.currentEvent=$scope.selectedEvents[lifeEventCounter];
//        myService.setCurrentEvent($scope.currentEvent);
//        return;
//     }
//     $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter];
//     $scope.currentPageSidebar = $scope.currentPage;
//     ///////////////////////////////////////////////////////////////////////////////
//   //  console.log("currentPage >>>> "+ $scope.currentPage);
//
//
//     counter++;
//     myService.setPartialCount(counter);
//     //debugger;
//     var runSum = 0;
//     for (var i = 0; i < $scope.depCounter + 1; i++) {
//      $scope['person'+i+'array'] = $scope['person'+i+'array'] || myService.getIncomeEvents();
//      runSum += $scope['person'+i+'array'].selected;
//      console.log('selected income events: ',$scope['person'+i+'array'].selected);
//     }
//
//     $scope.dependentForms = $scope.dependentForms || [];
//     $scope.depCounter += Math.floor( counter / ($scope.dependentForms.length * ($scope.depCounter + 1) + runSum + 1));
//     //console.log($scope.depCounter);
//     //alert($scope.currentPage);
//     //alert(counter)
//     //console.log("Partial #: "+counter)
//     //var subcatLength = $('.sidebar').find('.subcategories').length
//     //console.log("There are " + subcatLength + " subcategories")
//     $scope.changeMe = $scope.selectedEvents[lifeEventCounter].eventCode;
//     $scope.counter = counter;
//     $scope.lifeEventCounter = lifeEventCounter;
//     
//// 	$rootScope.counter++;
//// 	console.log("rootscope counter controller 1: " + $rootScope.counter);
//
////  	debugger
//// 	if ( counter ==  $scope.selectedEvents[lifeEventCounter].partials.length ) {
//// 	  $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter+1].eventCode;
//// //	  console.log("rootscope lceCode controller 2: " + $rootScope.lceCode)
//// 	} else {
//// 	  $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter].eventCode;
//// //	  console.log("rootscope lceCode controller 2: " + $rootScope.lceCode)
//// 	};
//   }; // End of Next Function
//
//   // This function will send the user last partial of the current life event
//   $scope.lastSuccess = function ()  {;
//     var arrayofpartials = $scope.selectedEvents[lifeEventCounter].partials;
//     var poppedlastpartial = arrayofpartials.pop();
//     $scope.currentPage = poppedlastpartial;
//   };
//
//   // This function will send the user first partial of the current life event
//   $scope.firstPartial = function () {
//   var currentPartials =  $scope.selectedEvents[lifeEventCounter].partials;
//     //find the index marital status in cuurent arrray
//   var maritalStatusUrl = '../../resources/js/templates/lce/maritalstatus-details.html';
//   counter =$.inArray(maritalStatusUrl,currentPartials,dependentHousholdStarted);
//   myService.setPartialCount(counter);
//   if(counter != -1){
//     $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter++];
//   }
//   };
//
//   // This function is JUST for the income summary table so user can go back to income partial
//   $scope.editIncome = function () {
//     //get the current parital with lifeEventCounter
//     var currentPartials = $scope.selectedEvents[lifeEventCounter].partials.slice(0);
//     //get the number of income templates in current parital
//     var noOfIncomeTemplatesInPartials = getNoOfIncomeTemplatesInPartials($scope.incomeTypes,currentPartials);
//     //change the counter to index of income page
//     counter = counter - (noOfIncomeTemplatesInPartials+3);
//     myService.setPartialCount(counter);
//     $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter++];
//  };
//
//   // This function will send the user back by index-1
//   $scope.back = function() {
//	    // SIDEBAR FUNCTION
//	     sideBarCounter--;
//	     var subcat = $scope.selectedEvents[lifeEventCounter].subcategories.length;
//
//	    for (var i = 0; i < subcat; i++) {
////	        console.log($scope.selectedEvents[lifeEventCounter].eventCode+i);
////	        console.log('sideBarCounter: ' + sideBarCounter);
//	        if (sideBarCounter == i) {
//	         // console.log("sidebar ===equals=== i")
//	          var subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i-1);
//	          var subId = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
//	          if ( $scope.selectedEvents[lifeEventCounter].subcategories[(i-2)] == undefined ) {
//  	        	$(subIdRPrevious).html("Event Date" + ' &nbsp; <i class="icon-chevron-left"></i>');
//  	          }
//	          $(subId).html($scope.selectedEvents[lifeEventCounter].subcategories[(i-1)] + ' &nbsp; <i class="icon-chevron-left"></i>');
//	        }
//	    }
//	    // SIDEBAR FUNCTION
//     if (counter === 1) {
//       $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
//     } else if (counter > 1) {
//       $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter-2];
//     } else {
//       lifeEventCounter--;
//       if (lifeEventCounter < 0) {
//         $scope.currentPage = $location.path('/');
//    // lifeEventCounter = 0;
//    // counter = 0;
//       } else {
//         counter = $scope.selectedEvents[lifeEventCounter].partials.length-1;
//         myService.setPartialCount(counter);
//         $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter];
//       }
//     }
//     counter--;
//     myService.setPartialCount(counter);
//   };
//
//	var flatten = function(array) {
//	      var result = [], self = arguments.callee;
//	      array.forEach(function(item) {
//	        Array.prototype.push.apply(
//	          result,
//	          Array.isArray(item) ? self(item) : [item]
//	        );
//	      });
//	      return result;
//	    };
//	    
//    
//    // Adding a dependent
//    $scope.truIds = [];
//    $scope.addDep = function(numParam) {
//	    var currentPartials = $scope.selectedEvents[lifeEventCounter].partials.slice(0);
//	    $scope.depCounter=0;
//	    $scope.numDependents = numParam;
//	    // Get number of dependents and save into this variable: Integer
//	    var num = parseInt(numParam);
//	    // Forms that are reused from Marital Status section (details-other html pages/partials): Arraylist
//	    $scope.dependentForms = myService.setDepForms(this)[0].partials;
//	    // Empty array that will contain ($scope.dependentForms) x num and be a multidimensional array
//	    // If user enters 2 dependents, the array will have 2 arrays: [Array[11], Array[11]]
//	    var newPartials = [];
//
//    	var trueIds = [];
//	    if ( $scope.currentEvent.eventCode === 'LCE_INCOME') {
//	    	//Delete all partials except last two. Because here we are goinf to add new partials as per selection.
//	    	currentPartials.splice(0,currentPartials.length-2);
//	    	var copies = 0;
//	    	$scope.truIds = trueIds;
//	    	for (var i = 0; i < eventInfoArrayTemp.length; i++) {
//    		  if (eventInfoArrayTemp[i].incomeChangeIndicator === true) {
//    			  copies++;
//    			  trueIds.push(eventInfoArrayTemp[i].index);
//    		  }
//    		}
//	    	$scope.numCopies = copies;
//	    	console.log($scope.numCopies);
//	    	console.log(trueIds);
//
//	    	console.log($scope.householdMember);
//	        // For loop that will push the forms that are reused from Marital Status section according to the number of dependents entered
//		    for (var i = 0; i < $scope.numCopies; i++) {
//		      newPartials.push("../../resources/js/templates/lce/maritalstatus-income.html");
//		    }
//			currentPartials.splice(0,0,newPartials);
//			currentPartials = flatten(currentPartials);
////			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[trueIds[0]].householdMember[trueIds[0]];
////			console.log(newPartials);
//    	} else {
//			  // For loop that will push the forms that are reused from Marital Status section according to the number of dependents entered
//			  for (var i = 0; i < num; i++) {
//			    newPartials.push($scope.dependentForms.slice(0));
//			  }
//			  // Splice in the multidimensional array into the current partial
//			  currentPartials.splice(1,0,newPartials);
//			  // After the marital status arrays have been spliced into the original array we splice the 'partials/numofdep-remove.html' partial
//			  //==============================| currentPartials is a MULTIDIMENSIONAL ARRAY | ===================================
//			  currentPartials.splice(-2,1);
//    	}
//
//		  // Now we flatten the multidimensional array
//		  var temp3 = flatten(currentPartials);
//		  // Reassign the flattened multidimensional array to be the '$scope.selectedEvents[0].partials' so that the user can traverse back and fourth thru the new array
//		  $scope.selectedEvents[lifeEventCounter].partials = temp3;
//		  selectPartialsCopy = $scope.selectedEvents[lifeEventCounter].partials.slice(0);
//
//    }; // End of Adding a dependent
//
//    var whichOne = 0;
//    $scope.truIds;
//    $scope.whichDep = function () {
//		console.log("====="+whichOne);
////    	$scope.householdMember = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember[$scope.truIds[whichOne]];
//
//    	$scope.householdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[$scope.truIds[whichOne]];
//		myService.setMember($scope.householdMember);
//		
//    	whichOne++;
//		//debugger;
//    };
//    
//    // Remove a Depedent
//    $scope.removeDependents = function() {
//        $scope.selectedEvents[lifeEventCounter].partials.splice(0,1);
//        if (counter === $scope.selectedEvents[lifeEventCounter].partials.length) {
//          lifeEventCounter++;
//          counter = 0;
//          myService.setPartialCount(counter);
//          $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
//          return;
//          // alert(lifeEventCounter);
//        }
//        $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter];
//        counter++;
//        myService.setPartialCount(counter);
//      };
//
//   //add income array into scope
//   $scope.incomeTypes = myService.getIncomeEvents();
//
//   //to init income select on other income No Selection
//   $scope.initIncomes = function(){
//    // initIncomeTypes($scope);
//	   var houseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//		var individualIncomeTypes=[];
//		var currentHshld= myService.getMember();
//	   for(var i=0;i<houseHoldMemberArray.length;i++){
//			if(houseHoldMemberArray[i].personId    === currentHshld.personId){
//				individualIncomeTypes= eventInfoArrayTemp[i].incomeTypes;
//				break;
//			}
//		}
//	   
//	   for (var i = individualIncomeTypes.length -1; i >= 0; i--) {
//		   individualIncomeTypes[i].value = false;
//		  }
//   };
//
//   //find the selected income count
//   $scope.selectedIncomeCount = function(){
//	   var houseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//		var individualIncomeTypes=[];
//		var currentHshld= myService.getMember();
//	   for(var i=0;i<houseHoldMemberArray.length;i++){
//			if(houseHoldMemberArray[i].personId    === currentHshld.personId){
//				individualIncomeTypes= eventInfoArrayTemp[i].incomeTypes;
//				break;
//			}
//		}
//     totalSelectedCount = 0;
//     for (var i = individualIncomeTypes.length -1; i >= 0; i--) {
//         if (individualIncomeTypes[i].value) {
//           totalSelectedCount++;
//         }
//       }
//     return totalSelectedCount;
//   };
//
//   //TODO - Select Income
//   $scope.selectIncome = function() {
//   var currentPartials = $scope.selectedEvents[lifeEventCounter].partials.slice(0);
//   //if user came from income summary then of income page will be already in partial
//   //need to remove them from partial and add again to avoid duplicate income pages
//   var houseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//   var individualIncomeTypes=[];
//   var currentHshld= myService.getMember();
//   for(var i=0;i<houseHoldMemberArray.length;i++){
//		if(houseHoldMemberArray[i].personId    === currentHshld.personId){
//			individualIncomeTypes= eventInfoArrayTemp[i].incomeTypes;
//			break;
//		}
//	}
//   
//   
//   var noOfIncomePagesInTemplates = getNoOfIncomeTemplatesInPartials(individualIncomeTypes,currentPartials,counter);
//   //remove the income page from the current partials
//   if(noOfIncomePagesInTemplates>0){
//     currentPartials.splice(counter, noOfIncomePagesInTemplates);
//   }
//   var newPartials = [];
//   console.log("newPartials: "+ newPartials);
//     var copies =angular.copy( currentPartials);// currentPartials.slice(0);
//     //$scope.incomeTypes = myService.editIncomeEvents(this.income);
//     for (var i = individualIncomeTypes.length -1; i >= 0; i--) {
//       if (individualIncomeTypes[i].value) {
//         // currentPartials.splice(counter,0, $scope.incomeTypes[i].url);
//         // newPartials.push($scope.incomeTypes[i].url);
//         copies.splice(counter, 0, individualIncomeTypes[i].url);
//       }
//     }
//     // $scope.selectedEvents[0].partials = currentPartials.slice(0);
//     // copies.splice(counter, 0 , newPartials);
//     $scope.selectedEvents[lifeEventCounter].partials = copies;
//   }; // Select your income
//   
//
//    // This function redirects user to the second  partial of the above array for Number of Depedents, which is the remove page
//    $scope.removeDependents = function() {
//    $scope.selectedEvents[lifeEventCounter].partials=   ['../../resources/js/templates/lce/numofdep-remove.html',
//                                                            '../../resources/js/templates/lce/numofdep-complete.html',
//                                                          ];
//      if (counter === $scope.selectedEvents[lifeEventCounter].partials.length) {
//        lifeEventCounter++;
//        counter = 0;
//        myService.setPartialCount(counter);
//        $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
//        return;
//        // alert(lifeEventCounter);
//      }
//      $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter];
//      counter++;
//      myService.setPartialCount(counter);
//    };
//
// //add addChangedApplicants to scope methods
// $scope.addChangedApplicants = addChangedApplicants;
//
// //next button function for death/divorse and legal seperation
// $scope.deathDivorseLegalNext= function(changeDate){
//   //TODO: When Multiple Spous..then which spous will be specified as dead?
//   if(eventInfo.eventSubCategory==='DEATH_OF_SPOUSE' || eventInfo.eventSubCategory=== 'DIVORCE_OR_ANULLMENT' ){
//     $scope.localHouseholdMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//     for(var i=0;i < $scope.localHouseholdMemberArray.length ; i++){
//       $scope.localHouseholdMember= $scope.localHouseholdMemberArray[i];
//       $scope.relationShipArray=  $scope.localHouseholdMember.bloodRelationship;
//
//       for (var j=0;j<$scope.relationShipArray.length;j++){
//         if($scope.relationShipArray[j].relationshipCode=='01'){//01 is Spous relationship code.
//           $scope.localHouseholdMember.socialSecurityCard.deathIndicator=true;
//           addChangedApplicants( $scope.localHouseholdMember.personId,changeDate);
//           break;
//         }
//       }
//
//     }
//  }
//  //update partial counter
//   counter =$scope.selectedEvents[lifeEventCounter].partials.length-1;
//   myService.setPartialCount(counter);
//   $scope.next();
//   //calling save function.This will save data and load next page
//  // saveSSAPJSON($scope,lifeEventCounter);
// };
//
//  //Added to false the
//  $scope.nextHouseholdDependent = function(){
//    //if event it Number of Dependent then add a Dependent household
//    if( $scope.selectedEvents[lifeEventCounter].eventCode=='LCE_NUMBER_OF_DEPENDENTS'){
//      if(dependentForHouseholdAdded){
//        dependentForHouseholdAdded = false;
//      }
//      //assign counter to dependent start index
//      dependentHousholdStarted = counter;
//    }else if($scope.selectedEvents[lifeEventCounter].eventCode=='LCE_MARITAL_STATUS'){
//      var householdMemberArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//      householdMemberArray[0].marriedIndicator=true;
//    }
//    //initialize the income array to default values
//   // initIncomeTypes($scope);
//    //go to next page
//    $scope.next();
//
//  };
//
//
//  $scope.activeDeathFilter = function (householdMember) {
//    var active= householdMember.active;
//    var deathIndicator=householdMember.socialSecurityCard.deathIndicator;//if active is not present OR active is true && death false  true ELSE false
//
//    if((active == undefined || active == null || active == true) && deathIndicator==false ){
//      return true;
//    }else{
//      return false;
//    }
//
//  };
//  
////Filter for income summary screens [MARRIAGE/CHANGE IN INCOME/ADD DEPENDENT]
//  $scope.incomeSummaryFilter = function (householdMember) {
//	    var active= householdMember.active;
//	    var deathIndicator=householdMember.socialSecurityCard.deathIndicator;//if active is not present OR active is true && death false  true ELSE false
//	    var activeDeathIndicator=false;
//	    var incomeChangeSelectedIndicator=false;
//	    if((active == undefined || active == null || active == true) && deathIndicator==false ){
//	    	activeDeathIndicator=true;
//	    }else{
//	    	activeDeathIndicator=false;
//	    }
//	    if(eventInfo.eventSubCategory==='INCOME'){
//	    	//check if change in income has been changed for this house hold or not? 
//	    	
//	    	  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//	    	  for(var i=0;i<householdArray.length;i++){
//	    		  if(householdArray[i].personId === householdMember.personId){
//	    			  if(eventInfoArrayTemp[i].incomeChangeIndicator){
//	    				  incomeChangeSelectedIndicator=true;
//	    			  } 
//	    			  break;
//	    		  }
//	    	  }
//	    	 return (activeDeathIndicator && incomeChangeSelectedIndicator); 
//	    }
//	    return activeDeathIndicator;
//	  };
//
//  //numofdep-remove.html..code for death OR active indicator
//  $scope.activeDeathStatusChange = function (householdMember) {
//     if(eventInfo.eventSubCategory=='death'){
//       householdMember.socialSecurityCard.deathIndicator=true;
//    }else if(eventInfo.eventSubCategory=='remove'){
//      householdMember.active=false;
//    }
//  };
//
//  $scope.showAdditionalJobs=function(household){
//	  var showAddtionalJobs = false;
//	  for(var i=1;i< household.detailedIncome.jobIncome.length;i++){
//		  if(household.detailedIncome.jobIncome[i].incomeAmountBeforeTaxes > 0){
//			  showAddtionalJobs= true;
//			  break;
//		  }
//	  }
//	  return showAddtionalJobs;
//  };
//	  
//  
//  
//  $scope.getRelation=function(relationShipCode){
//    if (relationShipCode=="01"){
//      return "Spouse";
//    } else if(relationShipCode=="03"){
//      return "Parent (father or mother)";
//    }else if(relationShipCode=="04"){
//      return "Grandparent (grandfather or grandmother)";
//    }else if(relationShipCode=="05"){
//      return "Grandchild (grandson or granddaughter)";
//    }else if(relationShipCode=="06"){
//      return "Uncle or aunt";
//    }else if(relationShipCode=="07"){
//      return "Nephew or niece";
//    }else if(relationShipCode=="08"){
//      return "First cousin";
//    }else if(relationShipCode=="09"){
//      return "Adopted child (son or daughter)";
//    }else if(relationShipCode=="10"){
//      return "Foster child (foster son or foster daughter)";
//    }else if(relationShipCode=="11"){
//      return "Son-in-law or daughter-in-law";
//    }else if(relationShipCode=="12"){
//      return "Brother-in-law or sister-in-law";
//    }else if(relationShipCode=="13"){
//      return "Mother-in-law or father-in law";
//    }else if(relationShipCode=="14"){
//      return "Sibling (brother or sister)";
//    }else if(relationShipCode=="15"){
//      return "Ward";
//    }else if(relationShipCode=="16"){
//      return "Stepparent (stepfather or stepmother)";
//    }else if(relationShipCode=="17"){
//      return "Stepchild (stepson or stepdaughter)";
//    }else if(relationShipCode=="18"){
//      return "Self";
//    }else if(relationShipCode=="19"){
//      return "Child (son or daughter)";
//    }else if(relationShipCode=="23"){
//      return "Sponsored dependent";
//    }else if(relationShipCode=="24"){
//      return "Dependent of a minor dependent";
//    }else if(relationShipCode=="25"){
//      return "Former spouse";
//    }else if(relationShipCode=="26"){
//      return "Guardian";
//    }else if(relationShipCode=="31"){
//      return "Court-appointed guardian";
//    }else if(relationShipCode=="38"){
//      return "Collateral dependent";
//    }else if(relationShipCode=="53"){
//      return "Domestic partner";
//    }else if(relationShipCode=="60"){
//      return "Annuitant";
//    }else if(relationShipCode=="D2"){
//      return "Trustee";
//    }else if(relationShipCode=="G8"){
//      return "Unspecified relationship";
//    }else if(relationShipCode=="G9"){
//      return "Unspecified relative";
//    }else if(relationShipCode=="03-53"){
//      return "Parent's domestic partner";
//    }else if(relationShipCode=="53-19"){
//        return "Child of domestic partner";
//    }
//  };
//
// }]); // End of Controller 2
//
//
////for padding of number with leading zeros
//Number.prototype.pad = function(size) {
//   var s = String(this);
//   if(typeof(size) !== "number"){size = 2;}
//
//   while (s.length < size) {s = "0" + s;}
//       return s;
//};
//
//
//function getDateInMMDDYYYY(val){
//	if(val !=undefined && val != "" && val.length>10 ){
//		var year = val.substr(8,4);
//	  	var day = val.substr(4,2);
//	  	var month = ($.inArray(val.substr(0,3),monthNames)+1).pad(2);
//	  	return month+"/"+day+"/"+year;
//	}
//	return val;
//   	
//}
//
//
////date formatter for non-input elements
//newMexico.directive('dateFormatterNonInput', function() {
//   return {
//       restrict: 'A',
//       link: function(scope, element, attrs, ngModelCtrl) {
//	    	var dateInMMDDYYYY =  getDateInMMDDYYYY(scope.$eval(attrs.date));
//	    	$(element).html(dateInMMDDYYYY);
//		}
//}});
//
////Directive for Datepicker
//newMexico.directive('dateformatter', function() {
//   return {
//       restrict: 'A',
//       require : 'ngModel',
//       link: function(scope, element, attrs, ngModelCtrl) {
//       	ngModelCtrl.$formatters.push(function(val){
//       		 var ngModel = $(element).attr('ng-model');
//               if(val !=undefined && val != "" && val.length>=10 && (ngModel.indexOf('eventInfo') == -1 && ngModel.indexOf('currentEvent') == -1)){
//                  return getDateInMMDDYYYY(val);
//               }
//               else
//                 return val;
//              });
//       	}
//   };
//}); // End of Datepicker directive
//
//
//var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
//                   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
//
// // Directive for Datepicker
// newMexico.directive('datetimez', function() {
//    return {
//        restrict: 'A',
//        require : 'ngModel',
//        link: function(scope, element, attrs, ngModelCtrl) {
//         element.datetimepicker({
//           dateFormat:'dd-MM-yyyy',
//           language: 'en',
//           pickTime: true
//           //commented date range as Date of birth can be less than 2013
//           //startDate: '01-11-2013',      // set a minimum date
//           //endDate: '01-11-2030'          // set a maximum date
//          }).on('changeDate', function(e) {
//          //concat prpoerties of date object as per required format Mar 13, 2014 00:00:00 AM
//          if(e.date == null || e.date == undefined){
//            ngModelCtrl.$setViewValue('');
//            scope.$apply();
//            return;
//          }
//          var ngModel = $(this).attr('ng-model');
//
//          if(ngModel.indexOf('eventInfo') == -1 && ngModel.indexOf('currentEvent') == -1){
//            ngModelCtrl.$setViewValue(monthNames[e.date.getMonth()]+" "+e.date.getUTCDate().pad(2)+", "+e.date.getFullYear()+" 00:00:00 AM");
//          }else{
//            ngModelCtrl.$setViewValue((e.date.getMonth()+1).pad(2)+"/"+e.date.getUTCDate().pad(2)+"/"+e.date.getFullYear());
//          }
//            scope.$apply();
//          });
//        }
//    };
//}); // End of Datepicker directive
//
// newMexico.controller('settings', ['$scope', function($scope) {
//   $scope.contacts = [
//     {type: 'job', value: ''},
//   ];
//    $scope.addContact = function() {
//      this.contacts.push({type: 'job'});
//    };
//    $scope.removeContact = function(contactToRemove) {
//     var index = this.contacts.indexOf(contactToRemove);
//      this.contacts.splice(index, 1);
//    };
// }]); // End of special controller to add addition jobs in Job Income partial
//
// newMexico.controller('maritalStatusDetail', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//  //if event it Number of Dependent then add a Dependent household
//  if( $scope.selectedEvents[lifeEventCounter].eventCode=='LCE_NUMBER_OF_DEPENDENTS'){
//    //add a household member
//    if(!dependentForHouseholdAdded){
//      var newHouseHold=getDefaultHousehold();
//
//      var curEvent=myService.getCurrentEvent();
//      addChangedApplicants(newHouseHold.personId,curEvent.dateOfChange);
//      //TODO:make this dependent as child till further information. 19 is relationship code for child
//      newHouseHold.bloodRelationship[0].relationshipCode="19";
//      ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length]=newHouseHold;
//      dependentForHouseholdAdded = true;
//
//    }
//  }
// }]);
//
// //New controller to get Marital Status subtypes
// newMexico.controller('maritalStatus', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//   $scope.maritaStatusSubCategory= [ {label:"Marriage or Civil Union",value:"MARRIAGE"},
//                                     {label:"Death of Spouse",value:"DEATH_OF_SPOUSE"},
//                                     {label:"Divorce or Legal Separation",value:"DIVORCE_OR_ANULLMENT"},
////                                     {label:"Legal Separation",value:"LEGAL_SEPERATION"}
//                                     ];
//
//   $scope.$watch("eventInfo.eventSubCategory",function( newCategory ) {
//
//       if(newCategory=='MARRIAGE' || newCategory=='CIVIL_UNION'){//if selected marriage then add new household member.
//         if(maritalHouseholdAdded==false){
//           var defaultHousehold=getDefaultHousehold();
//           ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length]=defaultHousehold;
//           maritalHouseholdAdded=true;
//
//           var curEvent=myService.getCurrentEvent();
//
//          addChangedApplicants(defaultHousehold.personId,curEvent.dateOfChange);
//
//         }
//       }else{//In other cases remove household member if newly added
//         removeHousehold();
//       }
//       //TODO:set other values to default.
//    }
//   );//watch ended
//  //This function is being called from marital status next button
//   $scope.maritalStatusNext=function( newDate ) {
//     //get all added applicants and set there date to newDate
//     for(var i=0;i<eventInfo.changedApplicants.length;i++){
//       eventInfo.changedApplicants[i].eventDate=newDate;
//     }
//   };
//
// }]);
// //Added method to get default household
// function getDefaultHousehold(){
//    var defaultHousehold={"name": {"firstName": "","middleName": "","lastName": ""},
//                "dateOfBirth": "","householdContactIndicator": false,"applyingForCoverageIndicator": false,
//                "householdContact": {"homeAddressIndicator": false,"homeAddress": { "streetAddress1": "", "streetAddress2": "","city": "","state": "","postalCode":0 ,"county": "" }, "mailingAddressSameAsHomeAddressIndicator": false, "mailingAddress": { "streetAddress1": "", "streetAddress2": "", "city": "","state": "", "postalCode": 0, "county": ""  }, "phone": {}, "otherPhone": {}, "contactPreferences": { "preferredSpokenLanguage": "", "preferredWrittenLanguage": "", "emailAddress": "" },"stateResidencyProof": "" },
//                "livesWithHouseholdContactIndicator": false,"marriedIndicator": true,"gender":"","tobaccoUserIndicator": false,
//                "livesAtOtherAddressIndicator": true, "otherAddress": {"address": {"streetAddress1": "", "streetAddress2": "", "city": "","state": "","postalCode": 0, "county": "" }, "livingOutsideofStateTemporarilyIndicator": false },
//                "specialEnrollmentPeriod": {"birthOrAdoptionEventIndicator": false},
//                "socialSecurityCard": { "socialSecurityCardHolderIndicator":null,"socialSecurityNumber": "", "ssnManualVerificationIndicator": false,"nameSameOnSSNCardIndicator": null, "firstNameOnSSNCard": "", "middleNameOnSSNCard": "", "lastNameOnSSNCard": "", "deathIndicatorAsAttested": false, "deathIndicator": false, "deathManualVerificationIndicator": false, "useSelfAttestedDeathIndicator": false},
//                "specialCircumstances": { "americanIndianAlaskaNativeIndicator": false, "tribalManualVerificationIndicator": false, "pregnantIndicator": null, "numberBabiesExpectedInPregnancy": 0, "fosterChild": false, "everInFosterCareIndicator": false },
//                "americanIndianAlaskaNative": { "memberOfFederallyRecognizedTribeIndicator": false, "tribeName": ""},
//                "citizenshipImmigrationStatus": { "citizenshipAsAttestedIndicator": false,"citizenshipStatusIndicator": false,"citizenshipManualVerificationStatus": false, "naturalizedCitizenshipIndicator": false,"eligibleImmigrationStatusIndicator": false, "livedIntheUSSince1996Indicator": false, "lawfulPresenceIndicator": false,
//                  "citizenshipDocument": { "SEVISId" : "","alienNumber" : "","cardNumber" : "","documentDescription" : "","documentExpirationDate" : "","foreignPassportCountryOfIssuance" : "","foreignPassportOrDocumentNumber" : "",
//                                            "i94Number" : "","nameOnDocument" : { "firstName" : "","lastName" : "","middleName" : "","suffix" : ""},"nameSameOnDocumentIndicator" : false,"sevisId" : " ","visaNumber" : ""}},
//                "ethnicityAndRace": { "hispanicLatinoSpanishOriginIndicator": false,  "ethnicities": {  "ethnicity": [] }, "races": { "race": [] }  },
//                "expeditedIncome": { "irsReportedAnnualIncome": null, "irsIncomeManualVerificationIndicator": false, "expectedAnnualIncomeUnknownIndicator": false,"irsReportedAnnualIncome" : ""},
//                "healthCoverage": { "haveBeenUninsuredInLast6MonthsIndicator": false,"isOfferedHealthCoverageThroughJobIndicator": false, "employerWillOfferInsuranceIndicator": false,
//                      "currentEmployer": [  { "employer": { "phone": {  "phoneNumber": "", "phoneExtension": " ",  "phoneType": ""  }, "employerContactPersonName": " ", "employerContactEmailAddress": " "  }, "currentEmployerInsurance": {  "isCurrentlyEnrolledInEmployerPlanIndicator": false, "willBeEnrolledInEmployerPlanIndicator": false, "expectedChangesToEmployerCoverageIndicator": false, "employerWillNotOfferCoverageIndicator": false, "memberPlansToDropEmployerPlanIndicator": false, "employerWillOfferPlanIndicator": false, "memberPlansToEnrollInEmployerPlanIndicator": false, "currentLowestCostSelfOnlyPlanName": " ",  "currentPlanMeetsMinimumStandardIndicator": false,  "comingLowestCostSelfOnlyPlanName": " ",  "comingPlanMeetsMinimumStandardIndicator": false  }  }],
//                      "currentlyEnrolledInCobraIndicator": false, "currentlyEnrolledInRetireePlanIndicator": false,"currentlyEnrolledInVeteransProgramIndicator": false, "willBeEnrolledInCobraIndicator": false,"willBeEnrolledInRetireePlanIndicator": false, "willBeEnrolledInVeteransProgramIndicator": false, "hasPrimaryCarePhysicianIndicator": false,  "primaryCarePhysicianName": " ",
//                      "formerEmployer": [ { "employerName": " ",   "address": {  "streetAddress1": " ", "streetAddress2": " ", "city": " ",  "state": ""   },  "phone": { "phoneNumber": "", "phoneExtension": " ",  "phoneType": ""   }, "employerIdentificationNumber": " ",  "employerContactPersonName": " ",  "employerContactPhone": {  "phoneNumber": "",   "phoneExtension": " ", "phoneType": ""  },   "employerContactEmailAddress": " "    }  ],
//                      "currentOtherInsurance": {   "medicareEligibleIndicator": false,  "tricareEligibleIndicator": false, "peaceCorpsIndicator": false, "otherStateOrFederalProgramIndicator": false, "otherStateOrFederalProgramType": " ", "otherStateOrFederalProgramName": " "  },
//                      "currentlyHasHealthInsuranceIndicator": false,  "otherInsuranceIndicator": false,
//                      "medicaidInsurance": {  "requestHelpPayingMedicalBillsLast3MonthsIndicator": false  },
//                      "chpInsurance": {"insuranceEndedDuringWaitingPeriodIndicator": false,"reasonInsuranceEndedDuringWaitingPeriod": "OTHER",  "reasonInsuranceEndedOther": " "  } },
//                "incarcerationStatus": { "incarcerationAsAttestedIndicator": false,  "incarcerationStatusIndicator": false,  "incarcerationPendingDispositionIndicator": false,  "incarcerationManualVerificationIndicator": false,   "useSelfAttestedIncarceration": false  },
//                "detailedIncome": { "jobIncome": [    {   "employerName": "", "incomeAmountBeforeTaxes": 0  }   ],
//                            "otherIncome": {   "otherIncomeTypeDescription": "",  "incomeAmount": 0   },
//                            "capitalGains": {  "annualNetCapitalGains": 0   },
//                            "rentalRoyaltyIncome": {  "netIncomeAmount": 0  },
//                            "farmFishingIncome": {  "netIncomeAmount": 0  },
//                            "alimonyReceived": { "amountReceived": 0,"incomeFrequency" : ""  },
//                            "selfEmploymentIncome": { "typeOfWork": "",   "monthlyNetIncome": 0   },
//                            "deductions": {   "alimonyDeductionAmount":0,"alimonyDeductionFrequency":"",  "studentLoanDeductionAmount":0,"studentLoanDeductionFrequency":"", "otherDeductionAmount":0,   "otherDeductionFrequency":"",  "deductionAmount":0 , "deductionFrequency":"MONTHLY",
//                                                     "deductionType":[ "ALIMONY", "STUDENT_LOAN_INTEREST", "OTHER" ]
//                                           },
//                            "unemploymentBenefit": {  "stateGovernmentName": "",  "benefitAmount": 0 , "stateGovernmentName" : "", "unemploymentDate" : "" },
//                            "retirementOrPension": {  "taxableAmount": 0 },
//                            "socialSecurityBenefit": {  "benefitAmount": 0,"incomeFrequency" : ""  },
//                            "investmentIncome": { "incomeAmount": 0  },
//                            "discrepancies": {"hasChangedJobsIndicator": false, "stopWorkingAtEmployerIndicator": false, "didPersonEverWorkForEmployerIndicator": false,  "hasHoursDecreasedWithEmployerIndicator": false,  "hasWageOrSalaryBeenCutIndicator": false,  "explanationForJobIncomeDiscrepancy": "none", "seasonalWorkerIndicator": false,  "explanationForDependantDiscrepancy": "none"  },
//                            "wageIncomeProof": "", "scholarshipIncomeProof": "", "dividendsIncomeProof": "", "taxableInterestIncomeProof": "",  "annuityIncomeProof": "", "pensionIncomeProof": "", "royaltiesIncomeProof": "", "unemploymentCompensationIncomeProof": "","foreignEarnedIncomeProof": "", "rentalRealEstateIncomeProof": "",  "sellingBusinessPropertyIncomeProof": "",  "farmIncomeProof": "",  "partnershipAndSwarpIncomeIncomeProof": "",  "businessIncomeProof": "", "childNaTribe": "",  "taxExemptedIncomeProof": "",  "socialSecurityBenefitProof": "",  "selfEmploymentTaxProof": "", "studentLoanInterestProof": "", "receiveMAthrough1619": false,   "receiveMAthroughSSI": false,
//                            "wageIncome": {  "incomeAmount": 0   },
//                            "foreignEarnedIncome": { "incomeAmount": 0  },
//                            "rentalRealEstateIncome": {  "incomeAmount": 0  },
//                            "sellingBusinessProperty": {   "incomeAmount": 0 },
//                            "farmIncom": {  "incomeAmount": 0 },
//                            "partnershipsCorporationsIncome": { "incomeAmount": 0 },
//                            "businessIncome": {  "incomeAmount": 0 },
//                            "selfEmploymentTax": {  "incomeAmount": 0 },
//                            "studentLoanInterest": {  "incomeAmount": 0  }  },
//                "bloodRelationship": [  {   "taxDependantOf": false, "relatedTo": 1,   "relationshipCode": "01"  } ],
//                "migrantFarmWorker": false,  "birthDateVerificationStatus": 1, "birthCertificateType": "NO_CRETIFICATE", "infantIndicator": false, "PIDIndicator": false, "PIDVerificationIndicator": false,  "livingArrangementIndicator": false, "IPVIndicator": false, "strikerIndicator": false, "drugFellowIndicator": false,  "SSIIndicator": false,  "residencyVerificationIndicator": false,  "disabilityIndicator": false,  "shelterAndUtilityExpense": 0, "dependentCareExpense": 0, "childSupportExpense": 0,  "medicalExpense": 0, "heatingCoolingindicator": false   };
//    var lastHouseholdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//    var calculatedPersonId=lastHouseholdMember.personId+1;
//    defaultHousehold.personId=calculatedPersonId;
//    return defaultHousehold;
//  }
//
// newMexico.controller('maritalStatusSSN', ['$scope', '$http','$location', '$timeout', 'myService', function($scope, $http, $location, $timeout, myService) {
//
//   function trim(rawStr) {
//          return rawStr.replace(/^\s+|\s+$/g,"");
//    }
//
//    function populateSSNFieldsFromJSON() {
//      var socialSecurityNumber = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard.socialSecurityNumber;
//
//      if(null != socialSecurityNumber && socialSecurityNumber != undefined  ) {
//        var matches = socialSecurityNumber.match(/(\d{3})*-(\d{2})*-(\d{4})*/);
//
//        if(null != matches && matches != undefined && matches.length > 0) {
//          if(null != matches[1] && matches[1] != undefined) {
//            $("#ssn1").val(trim(matches[1]));
//          }
//
//          if(null != matches[2] && matches[2] != undefined) {
//            $("#ssn2").val(trim(matches[2]));
//          }
//
//          if(null != matches[3] && matches[3] != undefined) {
//            $("#ssn3").val(trim(matches[3]));
//          }
//        }
//      }
//      else {
//        $("#ssn1").val("");
//        $("#ssn2").val("");
//        $("#ssn3").val("");
//      }
//    }
//
//    /** Populate SSN fields from JSON on load.*/
//    $timeout(populateSSNFieldsFromJSON, 0);
//
//    /** Truncate the JSON Marital Status SSN indicator toggling for conditional fields*/
//    $scope.processSSNNumberIndicatorToggle = function(currentStatus) {
//         if(currentStatus == 'true') {
//           ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard=truncateMaritalStatusJsonForSSNIndicatorYes(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard);
//         }
//         else if(currentStatus == 'false') {
//           ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard=truncateMaritalStatusJsonForSSNIndicatorNo(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard);
//         }
//    };
//
//    /** Truncate the JSON Marital Status SSN Yes indicator's same name indicator toggling for conditional fields*/
//    $scope.processSSNNumberIndicatorSameNameIndicatorToggle = function(currentStatus) {
//       if(currentStatus == 'true') {
//         ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard=truncateMaritalStatusJsonForSSNIndicatorYesForSameNameYes(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard);
//       }
//    };
//
//    $scope.processIndividualMandateExceptionIndicatorToggle = function(currentStatus) {
//       if(currentStatus == 'false') {
//         ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard.individualManadateExceptionID = "";
//
//         $("#individualManadateExceptionID").val("");
//       }
//    };
//   }]);
//
//   /** Function to truncate MaritalStatus's SSN's JSON for 'Yes'0 SSN, if JSON for 'No' flow already added earlier and final option is 'Yes'.*/
//   function truncateMaritalStatusJsonForSSNIndicatorYes(currentSocialSecurityCard) {
////     expnantion field
//     currentSocialSecurityCard.individualMandateExceptionIndicator = "";
//     currentSocialSecurityCard.individualManadateExceptionID = "";
//
//     $("#exemptionT").val("");
//     $("#exemptionF").val("");
//
//     $("#individualManadateExceptionID").val("");
//
//     return currentSocialSecurityCard;
//   }
//
//   /** Function to truncate MaritalStatus's SSN's JSON for 'No' SSN, if JSON for 'Yes' flow already added earlier and final option is 'No'.*/
//   function truncateMaritalStatusJsonForSSNIndicatorNo(currentSocialSecurityCard) {
//     currentSocialSecurityCard.socialSecurityNumber = "";
//     currentSocialSecurityCard.nameSameOnSSNCardIndicator = null;
//     currentSocialSecurityCard.firstNameOnSSNCard = "";
//     currentSocialSecurityCard.middleNameOnSSNCard = "";
//     currentSocialSecurityCard.lastNameOnSSNCard = "";
//     currentSocialSecurityCard.suffixOnSSNCard = "";
//
//     $("#socialSecurityNumber").val("");
//     $("#ssn1").val("");
//     $("#ssn2").val("");
//     $("#ssn3").val("");
//
//     $("#samenameT").val("");
//     $("#samenameF").val("");
//
//     $("#firstNameOnSSNCard").val("");
//     $("#middleNameOnSSNCard").val("");
//     $("#lastNameOnSSNCard").val("");
//     $("#suffixOnSSNCard").val("");
//
//     return currentSocialSecurityCard;
//   }
//
//   /** Function to truncate MaritalStatus's SSN's JSON for same name on SSN card, if JSON for 'No' flow already added earlier and final option is 'Yes'.*/
//   function truncateMaritalStatusJsonForSSNIndicatorYesForSameNameYes(currentSocialSecurityCard) {
//     currentSocialSecurityCard.firstNameOnSSNCard = "";
//     currentSocialSecurityCard.middleNameOnSSNCard = "";
//     currentSocialSecurityCard.lastNameOnSSNCard = "";
//     currentSocialSecurityCard.suffixOnSSNCard = "";
//
//     $("#firstNameOnSSNCard").val("");
//     $("#middleNameOnSSNCard").val("");
//     $("#lastNameOnSSNCard").val("");
//     $("#suffixOnSSNCard").val("");
//
//     return currentSocialSecurityCard;
//   }
//
//  newMexico.controller('maritalStatusSC', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//    $scope.otherStateOrFederalProgramIndicator= function(){
//      var checked = $("#otherStateOrFederalProgramIndicator").is(":checked");
//      var lastHouseholdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//      if(!checked){
//        lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramType='';
//        lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramName='';
//      }
//    };
//
//    $scope.noneOfTheAbove = function(){
//      var checked = $("#noneOfTheAbove").is(":checked");
//      var lastHouseholdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//      if(checked){
//        lastHouseholdMember.currentOtherInsurance.medicareEligibleIndicator=false;
//        lastHouseholdMember.currentOtherInsurance.tricareEligibleIndicator=false;
//        lastHouseholdMember.currentOtherInsurance.peaceCorpsIndicator=false;
//        lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramIndicator=false;
//        lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramType='';
//        lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramName='';
//      }
//
//    };
//   }]);
//
//
//
//
//  newMexico.controller('incomeJob', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//    
//	  if(eventInfo.eventSubCategory=='INCOME'){
//			
//		  $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//	  
//	  /** Function to add Job JSON in income job array */
//	    $scope.addIncomeJob = function () {
//	      //ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].detailedIncome.jobIncome[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].detailedIncome.jobIncome.length] = addDefaultMaritalStatusIncomeJob();
//	    	 $scope.householdMember.detailedIncome.jobIncome.push(addDefaultMaritalStatusIncomeJob());
//	      
//	    };
//
//	    /** Function to remove the current Job JSON from income job array */
//	    $scope.removeIncomeJob = function (currentJob) {
//	     
//	        var incomeJobs = $scope.householdMember.detailedIncome.jobIncome;
//
//	        if(1 < incomeJobs.length) {
//	          var index = incomeJobs.indexOf(currentJob);
//	          incomeJobs.splice(index, 1);
//
//	          $scope.householdMember.detailedIncome.jobIncome = incomeJobs;
//	          incomeJobs = $scope.householdMember.detailedIncome.jobIncome;
//	        }
//
//	    };
//
//	    //to check all child forms in the screen are valid
//	    $scope.isAllFormValid=function(){
//	      var isValid = true;
//	      var listOfFormsResult =  $('.jobIncomeformValidation');
//	      for(var i=0;i<listOfFormsResult.length;i++){
//	        if($(listOfFormsResult[i]).html() == 'false'){
//	          isValid = false;
//	          break;
//	        }
//	      }
//	      return isValid;
//	    };
//
//  }]);
//
//
//   /** Function to add empty Marital Status's Income's Job. */
//   function addDefaultMaritalStatusIncomeJob() {
//     var defaultIncomeJobJSON = {"employerName": "", "incomeAmountBeforeTaxes": "", "incomeFrequency": ""};//job hours
//     return defaultIncomeJobJSON;
//   }
//
//
//   newMexico.controller('incomeSelf', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//
//   newMexico.controller('incomeSSB', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//
//   newMexico.controller('incomeUnemployment', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//   
//   
//   newMexico.controller('incomeRetire', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//   
//   
//   newMexico.controller('incomeCapGains', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//   
//   newMexico.controller('incomeInvest', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//   
//   newMexico.controller('incomeRental', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//   
//   newMexico.controller('incomeFarming', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//   
//   newMexico.controller('incomeAlimony', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//   
//   newMexico.controller('incomeOther', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	   if(eventInfo.eventSubCategory=='INCOME'){
//			
//		   $scope.householdMember = myService.getMember();
//			
//		}else{
//			$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		}
//   }]);
//   
//   
// //New controller added for custom logic of ethnicityRace page and having states races tribes values from look up tables.
//newMexico.controller('ethnicityRace', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//   $scope.states= ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District Of Columbia', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
//
//   $scope.tribes= ['Lakota','Navaho','Omaha'];
//
//   $scope.races= ['American Indian or Alaskan Native', 'Asian Indian', 'Black or African American', 'Chinese','Filipino','Guamaniam or Chamorro','Japanese','Korean','Native Hawaiian','Other Asian','Other Pacific Islander','Samoan','Vietnamese','White or Caucasian'];
//
//   $scope.selectedraces=[];
//   try{
//     $scope.selectedraces=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].ethnicityAndRace.races.race;
//   }catch(err){
//     $scope.selectedraces=[];
//     ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].ethnicityAndRace =   { "hispanicLatinoSpanishOriginIndicator": true,"ethnicities": { "ethnicity": [] }, "races": { "race": [] } };
//   }
//
//
//   $scope.selectedEthnicities =  ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].ethnicityAndRace.ethnicities.ethnicity;
//   $scope.uncheckAllEthnicities = function() {
//     $scope.selectedEthnicities = [];
//	   
//   };
//   $scope.addRemoveElement= function(event) {
//    var elementString= $(event.currentTarget).attr('array-content') ;
//     var elementIndex=$scope.selectedEthnicities.indexOf(elementString);
//
//     if(event.currentTarget.checked){
//       if (elementIndex === -1) $scope.selectedEthnicities.push(elementString);
//     }else{
//       if (elementIndex !== -1) $scope.selectedEthnicities.splice(elementIndex, 1);
//     }
//
//   };
//
//   $scope.toggleRaceSelection = function(raceSelected) {
//     var val=raceSelected.race;
//     var idx = $scope.selectedraces.indexOf(val);
//
//     if (idx > -1) {// Currently selected
//       $scope.selectedraces.splice(idx, 1);
//       if($scope.races.indexOf(val)==0){
//         ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].specialCircumstances.americanIndianAlaskaNativeIndicator=false;
//         //SET DEFAULT VALUES
//         ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator=false;
//         ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].americanIndianAlaskaNative.state="";
//         ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].americanIndianAlaskaNative.tribeName="";
//       }
//     }else{
//        $scope.selectedraces.push(val);
//        if($scope.races.indexOf(val)==0){
//          ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].specialCircumstances.americanIndianAlaskaNativeIndicator=true;
//      }
//     }
//
//     ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].ethnicityAndRace.races.race=$scope.selectedraces;
//   };
//
//
//
// }]);
////New controller added for address to have states and counties values from look-up table
//newMexico.controller('addressCtrl', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//	$scope.states=[{"code":"" ,"name":"Select"},{"code":"AL" ,"name":"Alabama"},{"code":"AK" ,"name":"Alaska"},{"code":"AZ" ,"name":"Arizona"},{"code":"AR" ,"name":"Arkansas"},
//                   {"code":"CA" ,"name":"California"},{"code":"CO" ,"name":"Colorado"},{"code":"CT" ,"name":"Connecticut"},{"code":"DE" ,"name":"Delaware"},{"code":"DC" ,"name":"District Of Columbia"},
//                   {"code":"FL" ,"name":"Florida"},{"code":"GA" ,"name":"Georgia"},{"code":"HI" ,"name":"Hawaii"},{"code":"ID" ,"name":"Idaho"},{"code":"IL" ,"name":"Illinois"},
//                   {"code":"IN" ,"name":"Indiana"},{"code":"IA" ,"name":"Iowa"},{"code":"KS" ,"name":"Kansas"},{"code":"KY" ,"name":"Kentucky"},{"code":"LA" ,"name":"Louisiana"},
//                   {"code":"ME" ,"name":"Maine"},{"code":"MD" ,"name":"Maryland"},{"code":"MA" ,"name":"Massachusetts"},{"code":"MI" ,"name":"Michigan"},{"code":"MN" ,"name":"Minnesota"},
//                   {"code":"MS" ,"name":"Mississippi"},{"code":"MO" ,"name":"Missouri"},{"code":"MT" ,"name":"Montana"},{"code":"NE" ,"name":"Nebraska"},{"code":"NV" ,"name":"Nevada"},
//                   {"code":"NH" ,"name":"New Hampshire"},{"code":"NJ" ,"name":"New Jersey"},{"code":"NM" ,"name":"New Mexico"},{"code":"NY" ,"name":"New York"},{"code":"NC" ,"name":"North Carolina"},
//                   {"code":"ND" ,"name":"North Dakota"},{"code":"OH" ,"name":"Ohio"},{"code":"OK" ,"name":"Oklahoma"},{"code":"OR" ,"name":"Oregon"},{"code":"PA" ,"name":"Pennsylvania"},
//                   {"code":"RI" ,"name":"Rhode Island"},{"code":"SC" ,"name":"South Carolina"},{"code":"SD" ,"name":"South Dakota"},{"code":"TN" ,"name":"Tennessee"},{"code":"TX" ,"name":"Texas"},
//                   {"code":"UT" ,"name":"Utah"},{"code":"VT" ,"name":"Vermont"},{"code":"VA" ,"name":"Virginia"},{"code":"WA" ,"name":"Washington"},{"code":"WV" ,"name":"West Virginia"},
//                   {"code":"WI" ,"name":"Wisconsin"},{"code":"WY" ,"name":"Wyoming"}];
//	
//
//	$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//	$scope.selectedState=$scope.states[0];
//	//Code to load selected state.
//	for(var i=0;i<$scope.states.length;i++){
//		if($scope.householdMember.otherAddress.address.state ===   $scope.states[i].code){
//			$scope.selectedState=$scope.states[i];
//			break;
//		}
//	}
//	
//	
//	
//	
//	
//    //function to copy the home address
//	$scope.changeAddress=function(){
//
//		var lastHousehold = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//		changeAddressOfHouseHold(lastHousehold);
//
//	};
//
//	  /** Method to asynchronous populate counties for selected state. */
//	  $scope.loadCounties = function() {
//		  var currState =$scope.selectedState.code;
//		 
//		  $scope.householdMember.otherAddress.address.state=currState;
//		
//		  
//		  if(null != currState && currState !== "") {
//			  var URL = 'getCounties?state=' + currState;
//			  
//			  $http({method: 'GET', url: URL}).
//			    success(function(data, status, headers, config) {
//			        $scope.status = status;
//			        $scope.counties = data;
//			        $scope.selectedCounty=$scope.counties[0];
//			        for(var i=0;i<$scope.counties.length;i++){
//			        	if($scope.householdMember.otherAddress.address.county === $scope.counties[i].code){
//			        		  $scope.selectedCounty=$scope.counties[i];
//			        		  break;
//			        	}
//			        }
//			    }).
//			    error(function(data, status, headers, config) {
//			        $scope.counties = null;
//			        
//			        console.log(data || "Request failed");
//			        $scope.status = status;
//			    });
//		  }
//	  };
//	  
//	  	$scope.selectedCounty=null;
//		$scope.counties=null;
//		
//		if($scope.selectedState.code != ""){
//			$scope.loadCounties();
//		}
//	
//	  $scope.assignCounty= function() {
//		  var currCounty = $scope.selectedCounty.code;
//		  $scope.householdMember.otherAddress.address.county=currCounty;
//	  };
//	  
//}]);
//
////controller for incarceration
//newMexico.controller('incarcerationController', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//  //sets default value for event sub category as incarceration has only one sub category
//  eventInfo.eventSubCategory = 'INCARCERATION';
//  eventInfoArrayTemp = [];
//  //add temp array to controller scope
//  $scope.eventInfoArrayTemp=eventInfoArrayTemp;
//  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//
//  //push the elements in the temp event array
//  for(var i=0;i<householdArray.length;i++){
//    if($scope.activeDeathFilter(householdArray[i])){
//    	var tempEventInfo = {
//      		  changeInAddressDate:'',
//      		  citizenShipChangeDate:'',
//      		  tribeChangeDate:'',
//      		  changeInLossDate:'',
//      		  nameChangeDate:'',
//      		  incomeChangeDate:'',
//      		  incomeChangeIndicator:'',
//      		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//      		  hasOtherIncomeTypes:'No',
//      		  lossEventSubCategory:'',
//      		  changeInGainDate:'',
//      		  gainEventSubCategory:'',
//      		  dateOfChange:''
//          };
//      eventInfoArrayTemp.push(tempEventInfo);
//    }
//  }
//
//  $scope.updateDates= function(){
//
//    for(var i=0;i<householdArray.length;i++){
//
//      for(var j=0;j<eventInfo.changedApplicants.length;j++){
//        if(eventInfo.changedApplicants[j].personId == householdArray[i].personId){
//          eventInfo.changedApplicants[j].eventDate=eventInfoArrayTemp[i].dateOfChange;
//          break;
//  }
//    }
//  }
//  };
//
//}]);
//
///*                        *
// *  Controller for Number of depedents starts *
// *                        *
// */
//
////Controller for number of dependent primary page.
//newMexico.controller('numOfDepController', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//  //to reinitialize the partials array
//  $scope.selectedEvents[lifeEventCounter].partials = depedentsArray;
//}]);
//
////Controller for number of dependent add page.
//newMexico.controller('noOfDependentsAdd', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
//  if(dependentForHouseholdAdded){
//    ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.splice(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1, 1);
//    dependentForHouseholdAdded=false;
//    eventInfo.changedApplicants =[];
//  }
//}]);
//
//
//
//
//
//
//
///*                        *
// *  Controller for Number of depedents ends   *
// *                        *
// */
//
////global function to initialize income array when event is changed
//
//function initIncomeTypes($scope){
//  for (var i = $scope.incomeTypes.length -1; i >= 0; i--) {
//      $scope.incomeTypes[i].value = false;
//  }
//}
//
////global function to find number of income templates in partial
//function getNoOfIncomeTemplatesInPartials(incomeTypes,currentPartials,afterIndex){
//  var noOfIncomePagesInTemplates = 0;
//  for (var i = incomeTypes.length -1; i >= 0; i--) {
//       //used $.inArray jquery method to find element in array, as array.indexOf didn't work below IE 9
//	  
//	  if(eventInfo.eventSubCategory === 'INCOME'){
//		  if (($.inArray(incomeTypes[i].url,currentPartials,afterIndex) !=-1)){
//		         noOfIncomePagesInTemplates++;
//		  }
//	  }else{
//		  if (($.inArray(incomeTypes[i].url,currentPartials,dependentHousholdStarted) !=-1)){
//		         noOfIncomePagesInTemplates++;
//		  }  
//	  }
//        
//  }
//  return noOfIncomePagesInTemplates;
//}
//
//
//// Ajax Request to save after second-to-last page in life event
//function saveSSAPJSON($scope,lifeEventCounter){
//  //eventInfo.eventCategory = $scope.selectedEvents[lifeEventCounter].eventCode;
//  //set application type to LCE
//  ssapJson.singleStreamlinedApplication.ApplicationType = "LCE";
//  $.ajax({
//          url:"/hix/iex/lce/savessapapplication",
//          type:"POST",
//          dataType:"json",
//          data:{ssapJson :JSON.stringify(ssapJson),eventInfo:JSON.stringify(eventInfoArray),csrftoken:reportYourChangeForm.csrftoken.value}
//       }).done(function(data) {
//          if(data.status != undefined){
//            if(data.status =="success"){
//                //update the existing global json with new one
//                //ssapJson = data.ssapJson;
//                //update the scope json with new one
//                //$scope.jsonObject = ssapJson;
//                //initialize eventInfo to default value.
//                //initEventInfo();
//              alert(data.message);
//                //call the next to load next template
//               // $scope.next();
//                //call apply to refresh the scope changes on ui.
//              //  $scope.$apply();
//            }else{
//              alert(data.message);
//            }
//          }else{
//            alert("error occured while saving.");
//          }
//       }).fail(function() {
//         alert( "error occured while saving." );
//    });
//}
//
// function addChangedApplicants(householdPersonId,changeDate){
//  var alreadyInChangedApplicants =  false;
//  //if changedApplicants is empty then add to array and enable the next button
//  if(eventInfo.changedApplicants.length == 0){
//    if(changeDate != undefined && changeDate != null){
//      eventInfo.changedApplicants.push({"personId":householdPersonId,"eventDate":changeDate});
//    }else{
//      eventInfo.changedApplicants.push({"personId":householdPersonId});
//    }
//  }else{
//    //iterated the changedApplicants array and find if household already added
//    for(var i=0;i<eventInfo.changedApplicants.length;i++){
//        //if household is already in changedApplicatnts
//        if(householdPersonId == eventInfo.changedApplicants[i].personId){
//          if(changeDate != undefined && changeDate != null){
//            eventInfo.changedApplicants[i].eventDate=changeDate;
//          }
//          alreadyInChangedApplicants = true;
//          break;
//        }
//    }
//    //if household is not in changedApplicatnts then add it
//    if(!alreadyInChangedApplicants){
//      if(changeDate != undefined && changeDate != null){
//        eventInfo.changedApplicants.push({"personId":householdPersonId,"eventDate":changeDate});
//      }else{
//        eventInfo.changedApplicants.push({"personId":householdPersonId});
//      }
//
//    }
//  }
//}
//
//function initEventInfo(){
//  eventInfo.eventCategory='';
//  eventInfo.eventSubCategory = '';
//  eventInfo.changedApplicants =[];
//}
//
//function removeHousehold(){
//   if(maritalHouseholdAdded==true){
//     ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.splice(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1, 1);
//     maritalHouseholdAdded=false;
//     eventInfo.changedApplicants =[];
//   }
//}
////This function takes household and updates its address same as first member of household array
//function changeAddressOfHouseHold( lastHousehold){
//  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//
//  if(!lastHousehold.livesAtOtherAddressIndicator){
//    lastHousehold.householdContact.homeAddress.streetAddress1 = householdArray[0].householdContact.homeAddress.streetAddress1;
//    lastHousehold.householdContact.homeAddress.streetAddress2 = householdArray[0].householdContact.homeAddress.streetAddress2;
//    lastHousehold.householdContact.homeAddress.city = householdArray[0].householdContact.homeAddress.city;
//    lastHousehold.householdContact.homeAddress.state = householdArray[0].householdContact.homeAddress.state;
//    lastHousehold.householdContact.homeAddress.postalCode = householdArray[0].householdContact.homeAddress.postalCode;
//    lastHousehold.householdContact.homeAddress.county = householdArray[0].householdContact.homeAddress.county;
//    //change otherAddress to default
//    lastHousehold.otherAddress.address.streetAddress1 = '';
//    lastHousehold.otherAddress.address.streetAddress2 = '';
//    lastHousehold.otherAddress.address.city = '';
//    lastHousehold.otherAddress.address.state = '';
//    lastHousehold.otherAddress.address.postalCode = 0;
//    lastHousehold.otherAddress.address.county = '';
//  }
//  else{
//    lastHousehold.householdContact.homeAddress.streetAddress1 = '';
//    lastHousehold.householdContact.homeAddress.streetAddress2 = '';
//    lastHousehold.householdContact.homeAddress.city = '';
//    lastHousehold.householdContact.homeAddress.state = '';
//    lastHousehold.householdContact.homeAddress.postalCode = 0;
//    lastHousehold.householdContact.homeAddress.county = '';
//  }
//
//}
//
//
//
//
/////////////////////////MODAL FOR LAWFUL PRESENCE/////////////////////////////
//
//newMexico.directive('modalDialog', function() {
//  return {
//    restrict: 'E',
//    scope: {
//      show: '=',
//      modalHouseholdMember:'='
//    },
//    replace: true, // Replace with the template below
//    transclude: true, // we want to insert custom content inside the directive
//    link: function(scope, element, attrs) {
//      scope.dialogStyle = {};
//      if (attrs.width)
//        scope.dialogStyle.width = attrs.width;
//      if (attrs.height)
//        scope.dialogStyle.height = attrs.height;
//      scope.hideModal = function() {
//        scope.show = false;
//      };
//    },
//    template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-dialog-content' ng-transclude></div><a class='btn btn-primary ng-modal-close' ng-click='hideModal()'>Cancel</a><a class='btn btn-primary ng-modal-close' ng-click='hideModal()'>Save</a></div></div>"
//  };
//});
//
////////////////////////////Controller for change in address
//newMexico.controller('changeInAddress', ['$scope', '$http', function($scope, $http) {
//  eventInfo.eventSubCategory = 'CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS';
//  $scope.stateArray=[{"code":"" ,"name":"Select"},{"code":"AL" ,"name":"Alabama"},{"code":"AK" ,"name":"Alaska"},{"code":"AZ" ,"name":"Arizona"},{"code":"AR" ,"name":"Arkansas"},
//                     {"code":"CA" ,"name":"California"},{"code":"CO" ,"name":"Colorado"},{"code":"CT" ,"name":"Connecticut"},{"code":"DE" ,"name":"Delaware"},{"code":"DC" ,"name":"District Of Columbia"},
//                     {"code":"FL" ,"name":"Florida"},{"code":"GA" ,"name":"Georgia"},{"code":"HI" ,"name":"Hawaii"},{"code":"ID" ,"name":"Idaho"},{"code":"IL" ,"name":"Illinois"},
//                     {"code":"IN" ,"name":"Indiana"},{"code":"IA" ,"name":"Iowa"},{"code":"KS" ,"name":"Kansas"},{"code":"KY" ,"name":"Kentucky"},{"code":"LA" ,"name":"Louisiana"},
//                     {"code":"ME" ,"name":"Maine"},{"code":"MD" ,"name":"Maryland"},{"code":"MA" ,"name":"Massachusetts"},{"code":"MI" ,"name":"Michigan"},{"code":"MN" ,"name":"Minnesota"},
//                     {"code":"MS" ,"name":"Mississippi"},{"code":"MO" ,"name":"Missouri"},{"code":"MT" ,"name":"Montana"},{"code":"NE" ,"name":"Nebraska"},{"code":"NV" ,"name":"Nevada"},
//                     {"code":"NH" ,"name":"New Hampshire"},{"code":"NJ" ,"name":"New Jersey"},{"code":"NM" ,"name":"New Mexico"},{"code":"NY" ,"name":"New York"},{"code":"NC" ,"name":"North Carolina"},
//                     {"code":"ND" ,"name":"North Dakota"},{"code":"OH" ,"name":"Ohio"},{"code":"OK" ,"name":"Oklahoma"},{"code":"OR" ,"name":"Oregon"},{"code":"PA" ,"name":"Pennsylvania"},
//                     {"code":"RI" ,"name":"Rhode Island"},{"code":"SC" ,"name":"South Carolina"},{"code":"SD" ,"name":"South Dakota"},{"code":"TN" ,"name":"Tennessee"},{"code":"TX" ,"name":"Texas"},
//                     {"code":"UT" ,"name":"Utah"},{"code":"VT" ,"name":"Vermont"},{"code":"VA" ,"name":"Virginia"},{"code":"WA" ,"name":"Washington"},{"code":"WV" ,"name":"West Virginia"},
//                     {"code":"WI" ,"name":"Wisconsin"},{"code":"WY" ,"name":"Wyoming"}];
//  $scope.countyArray=null;
//
//  $scope.eventInfoArrayTemp=eventInfoArrayTemp;
//  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//  if(eventInfoArrayTemp.length<householdArray.length){
//    for(var i=(eventInfoArrayTemp.length);i<householdArray.length;i++){
//    	var tempEventInfo = {
//      		  changeInAddressDate:'',
//      		  citizenShipChangeDate:'',
//      		  tribeChangeDate:'',
//      		  changeInLossDate:'',
//      		  nameChangeDate:'',
//      		  incomeChangeDate:'',
//      		  incomeChangeIndicator:'',
//      		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//      		  hasOtherIncomeTypes:'No',
//      		  lossEventSubCategory:'',
//      		  changeInGainDate:'',
//      		  gainEventSubCategory:'',
//      		  dateOfChange:''
//          };
//      eventInfoArrayTemp.push(tempEventInfo);
//    }
//  }
//
//  $scope.changeAddress=changeAddressOfHouseHold;
//  $scope.modalShown = false;
//  $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
//    $scope.modalHouseholdMember=houseHoldFromParent;
//    $scope.modalShown = !$scope.modalShown;
//    $scope.originalIndex=originalIndex;
//  };
//
//  $scope.getOriginalIndex=function(content,array){
//    return  $.inArray(content,array);
//  };
//
//
//  $scope.updateDates=function(){
//
//    for(var i=0;i<householdArray.length;i++){
//
//      for(var j=0;j<eventInfo.changedApplicants.length;j++){
//        if(eventInfo.changedApplicants[j].personId == householdArray[i].personId){
//          eventInfo.changedApplicants[j].eventDate=eventInfoArrayTemp[i].changeInAddressDate;
//          break;
//             }
//       }
//      }
//    return;
//  };
//
//  /** Method to asynchronous populate counties for selected state. */
//  $scope.loadCounties = function() {
//	  var currState = $("#state").val();
//	  
//	  if(null != currState && currState !== "") {
//		  var URL = 'getCounties?state=' + currState;
//		  
//		  $http({method: 'GET', url: URL}).
//		    success(function(data, status, headers, config) {
//		        $scope.status = status;
//		        $scope.countyArray = data;
//		    }).
//		    error(function(data, status, headers, config) {
//		        $scope.countyArray = null;
//		        
//		        console.log(data || "Request failed");
//		        $scope.status = status;
//		    });
//	  }
//  };
//
//}]);
//
////////////////////////////Controller for lawful presence
//newMexico.controller('lawfulModal', ['$scope', 'myService', function($scope, myService) {
//  eventInfo.eventSubCategory = 'LAWFUL_PRESENCE';
//  $scope.modalShown = false;
//  $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
//    $scope.modalHouseholdMember=houseHoldFromParent;
//    $scope.modalShown = !$scope.modalShown;
//    $scope.originalIndex=originalIndex;
//  };
//
//  $scope.getOriginalIndex=function(content,array){
//      return  $.inArray(content,array);
//  };
//
//  //add temp array to controller scope
//    $scope.eventInfoArrayTemp=eventInfoArrayTemp;
//    var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//	  
//    //push the elements in the temp event array
//    for(var i=0;i<householdArray.length;i++){
//      if($scope.activeDeathFilter(householdArray[i])){
//    	  var tempEventInfo = {
//        		  changeInAddressDate:'',
//        		  citizenShipChangeDate:'',
//        		  tribeChangeDate:'',
//        		  changeInLossDate:'',
//        		  nameChangeDate:'',
//        		  incomeChangeDate:'',
//        		  incomeChangeIndicator:'',
//        		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//        		  hasOtherIncomeTypes:'No',
//        		  lossEventSubCategory:'',
//        		  changeInGainDate:'',
//        		  gainEventSubCategory:'',
//        		  dateOfChange:''
//            };
//        eventInfoArrayTemp.push(tempEventInfo);
//      }
//    }
//
//      $scope.updateDates= function(){
//
//          for(var i=0;i<householdArray.length;i++){
//
//              for(var j=0;j<eventInfo.changedApplicants.length;j++){
//                  if(eventInfo.changedApplicants[j].personId == householdArray[i].personId){
//                    eventInfo.changedApplicants[j].eventDate=eventInfoArrayTemp[i].citizenShipChangeDate;
//                    break;
//                  }
//              }
//          }
//      };
//
//}]);
//
////////////////////////////Controller for tribe change
//newMexico.controller('tribeChangeController', ['$scope', 'myService', function($scope, myService) {
//  eventInfo.eventSubCategory = 'CHANGE_IN_TRIBE_STATUS';
//   $scope.stateArray=[{"code":"" ,"name":"Select"},{"code":"AL" ,"name":"Alabama"},{"code":"AK" ,"name":"Alaska"},{"code":"AZ" ,"name":"Arizona"},{"code":"AR" ,"name":"Arkansas"},
//                       {"code":"CA" ,"name":"California"},{"code":"CO" ,"name":"Colorado"},{"code":"CT" ,"name":"Connecticut"},{"code":"DE" ,"name":"Delaware"},{"code":"DC" ,"name":"District Of Columbia"},
//                       {"code":"FL" ,"name":"Florida"},{"code":"GA" ,"name":"Georgia"},{"code":"HI" ,"name":"Hawaii"},{"code":"ID" ,"name":"Idaho"},{"code":"IL" ,"name":"Illinois"},
//                       {"code":"IN" ,"name":"Indiana"},{"code":"IA" ,"name":"Iowa"},{"code":"KS" ,"name":"Kansas"},{"code":"KY" ,"name":"Kentucky"},{"code":"LA" ,"name":"Louisiana"},
//                       {"code":"ME" ,"name":"Maine"},{"code":"MD" ,"name":"Maryland"},{"code":"MA" ,"name":"Massachusetts"},{"code":"MI" ,"name":"Michigan"},{"code":"MN" ,"name":"Minnesota"},
//                       {"code":"MS" ,"name":"Mississippi"},{"code":"MO" ,"name":"Missouri"},{"code":"MT" ,"name":"Montana"},{"code":"NE" ,"name":"Nebraska"},{"code":"NV" ,"name":"Nevada"},
//                       {"code":"NH" ,"name":"New Hampshire"},{"code":"NJ" ,"name":"New Jersey"},{"code":"NM" ,"name":"New Mexico"},{"code":"NY" ,"name":"New York"},{"code":"NC" ,"name":"North Carolina"},
//                       {"code":"ND" ,"name":"North Dakota"},{"code":"OH" ,"name":"Ohio"},{"code":"OK" ,"name":"Oklahoma"},{"code":"OR" ,"name":"Oregon"},{"code":"PA" ,"name":"Pennsylvania"},
//                       {"code":"RI" ,"name":"Rhode Island"},{"code":"SC" ,"name":"South Carolina"},{"code":"SD" ,"name":"South Dakota"},{"code":"TN" ,"name":"Tennessee"},{"code":"TX" ,"name":"Texas"},
//                       {"code":"UT" ,"name":"Utah"},{"code":"VT" ,"name":"Vermont"},{"code":"VA" ,"name":"Virginia"},{"code":"WA" ,"name":"Washington"},{"code":"WV" ,"name":"West Virginia"},
//                       {"code":"WI" ,"name":"Wisconsin"},{"code":"WY" ,"name":"Wyoming"}];
//
//   $scope.tribes= ['Lakota','Navaho','Omaha'];
//
//  $scope.modalShown = false;
//  $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
//      $scope.modalHouseholdMember=houseHoldFromParent;
//      $scope.modalShown = !$scope.modalShown;
//      $scope.originalIndex=originalIndex;
//  };
//
//  $scope.getOriginalIndex=function(content,array){
//    return  $.inArray(content,array);
//  };
//
//
////add temp array to controller scope
//  $scope.eventInfoArrayTemp=eventInfoArrayTemp;
//  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//
//  //push the elements in the temp event array
//  for(var i=0;i<householdArray.length;i++){
//    if($scope.activeDeathFilter(householdArray[i])){
//    	var tempEventInfo = {
//      		  changeInAddressDate:'',
//      		  citizenShipChangeDate:'',
//      		  tribeChangeDate:'',
//      		  changeInLossDate:'',
//      		  nameChangeDate:'',
//      		  incomeChangeDate:'',
//      		  incomeChangeIndicator:'',
//      		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//      		  hasOtherIncomeTypes:'No',
//      		  lossEventSubCategory:'',
//      		  changeInGainDate:'',
//      		  gainEventSubCategory:'',
//      		  dateOfChange:''
//          };
//      eventInfoArrayTemp.push(tempEventInfo);
//    }
//  }
//
//    $scope.updateDates= function(){
//
//        for(var i=0;i<householdArray.length;i++){
//
//            for(var j=0;j<eventInfo.changedApplicants.length;j++){
//                if(eventInfo.changedApplicants[j].personId == householdArray[i].personId){
//                  eventInfo.changedApplicants[j].eventDate=eventInfoArrayTemp[i].tribeChangeDate;
//                  break;
//                }
//            }
//        }
//    };
//
//
//}]);
////////////////////////////Controller for name change
//newMexico.controller('incomeChangeController', ['$scope', function($scope) {
//		eventInfo.eventSubCategory='INCOME';
//
//		$scope.eventInfoArrayTemp=eventInfoArrayTemp;
//		var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//		if(eventInfoArrayTemp.length<householdArray.length){
//		  for(var i=(eventInfoArrayTemp.length);i<householdArray.length;i++){
//			  var tempEventInfo = {
//		    		  changeInAddressDate:'',
//		    		  citizenShipChangeDate:'',
//		    		  tribeChangeDate:'',
//		    		  changeInLossDate:'',
//		    		  nameChangeDate:'',
//		    		  incomeChangeDate:'',
//		    		  incomeChangeIndicator:'',
//		    		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//		    		  hasOtherIncomeTypes:'No',
//		    		  lossEventSubCategory:'',
//		    		  changeInGainDate:'',
//		    		  gainEventSubCategory:'',
//		    		  dateOfChange:''
//		        };
//		    eventInfoArrayTemp.push(tempEventInfo);
//		  }
//		}
//
//		$scope.getOriginalIndex=function(content,array){
//		  return  $.inArray(content,array);
//		};
//
//		$scope.updateEventInfo=function(){
//					
//			for (var i = 0; i < eventInfoArrayTemp.length; i++) {
//				if (eventInfoArrayTemp[i].incomeChangeIndicator === true) {
//					
//					addChangedApplicants(householdArray[i].personId,eventInfoArrayTemp[i].incomeChangeDate);
//				 }
//			}
//			 
//		};
//	
//}]);
//newMexico.controller('mainIncomeController', ['$scope', 'myService', function($scope, myService) { 
//	
//	//Populate global array
//	$scope.eventInfoArrayTemp=eventInfoArrayTemp;
//	  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//	  if(eventInfoArrayTemp.length<householdArray.length){
//	    for(var i=(eventInfoArrayTemp.length);i<householdArray.length;i++){
//	    	var tempEventInfo = {
//	      		  changeInAddressDate:'',
//	      		  citizenShipChangeDate:'',
//	      		  tribeChangeDate:'',
//	      		  changeInLossDate:'',
//	      		  nameChangeDate:'',
//	      		  incomeChangeDate:'',
//	      		  incomeChangeIndicator:'',
//	      		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//	      		  hasOtherIncomeTypes:'No',
//	      		  lossEventSubCategory:'',
//	      		  changeInGainDate:'',
//	      		  gainEventSubCategory:'',
//	      		  dateOfChange:''
//	          };
//	      eventInfoArrayTemp.push(tempEventInfo);
//	    }
//	  }
//	
//	
//	if(eventInfo.eventSubCategory=='INCOME'){
//		var curEvent=myService.getCurrentEvent();
//		var partialCount=myService.getPartialCount();
//		var mainIncomePageCount=0;
//		for(var i=0;i< partialCount;i++){
//			if(curEvent.partials[i]=== '../../resources/js/templates/lce/maritalstatus-income.html'){
//				mainIncomePageCount++;
//			}
//		}
//		
//		$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[0];
//		
//		var individualSelectionRank=0;
//		for(var i=0;i<eventInfoArrayTemp.length;i++){
//			if(eventInfoArrayTemp[i].incomeChangeIndicator === true){
//				
//				individualSelectionRank++;
//				if(mainIncomePageCount === individualSelectionRank){
//					$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[i];
//					
//					break;
//				}
//				
//			}
//		}
//		
//	}else{
//		$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
//	}
//	myService.setMember($scope.householdMember);
//	var houseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//	for(var i=0;i<houseHoldMemberArray.length;i++){
//		if(houseHoldMemberArray[i].personId    === $scope.householdMember.personId){
//			$scope.individualIncomeTypes= eventInfoArrayTemp[i].incomeTypes;
//			$scope.hasOtherIncomeTypes=   eventInfoArrayTemp[i].hasOtherIncomeTypes;
//			$scope.originalIndexHsHldIndex=i;
//			break;
//		}
//	}
//	
//	
//	$scope.loadPreviousHouseHold=function(){
//		if(eventInfo.eventSubCategory=='INCOME'){
//			//////////////////////
//			var curEvent=myService.getCurrentEvent();
//			var partialCount=myService.getPartialCount();
//			partialCount--;
//			//delete sub-income pages after current partial
//			//Count pages
//			 var noOfIncomePagesInTemplates = getNoOfIncomeTemplatesInPartials(myService.getIncomeEvents(),curEvent.partials,partialCount);
//			//remove the income page from the current partials
//			if(noOfIncomePagesInTemplates>0){
//				curEvent.partials.splice((partialCount+1), noOfIncomePagesInTemplates);
//			} 
//			
//			
//			if(partialCount>0){
//						var mainIncomePageCount=0;
//						for(var i=0;i< partialCount;i++){
//							if(curEvent.partials[i]=== '../../resources/js/templates/lce/maritalstatus-income.html'){
//								mainIncomePageCount++;
//							}
//						}
//						
//						$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[0];
//						
//						var individualSelectionRank=0;
//						for(var i=0;i<eventInfoArrayTemp.length;i++){
//							if(eventInfoArrayTemp[i].incomeChangeIndicator === true){
//								
//								individualSelectionRank++;
//								if(mainIncomePageCount === individualSelectionRank){
//									$scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[i];
//									
//									break;
//								}
//								
//							}
//						}
//						
//						
//						myService.setMember($scope.householdMember);
//			}
//			
//		}
//	};
//	
//}]);
//
//
////////////////////////////Controller for name change
//newMexico.controller('nameChangeController', ['$scope', function($scope) {
//  eventInfo.eventSubCategory='CHANGE_IN_NAME';
//
//  $scope.modalShown = false;
//    $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
//      $scope.modalHouseholdMember=houseHoldFromParent;
//      $scope.modalShown = !$scope.modalShown;
//      $scope.originalIndex=originalIndex;
//
//    };
//
//    $scope.getOriginalIndex=function(content,array){
//        return  $.inArray(content,array);
//   };
//
//  $scope.eventInfoArrayTemp=eventInfoArrayTemp;
//    var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//    if(eventInfoArrayTemp.length<householdArray.length){
//      for(var i=(eventInfoArrayTemp.length);i<householdArray.length;i++){
//    	  var tempEventInfo = {
//        		  changeInAddressDate:'',
//        		  citizenShipChangeDate:'',
//        		  tribeChangeDate:'',
//        		  changeInLossDate:'',
//        		  nameChangeDate:'',
//        		  incomeChangeDate:'',
//        		  incomeChangeIndicator:'',
//        		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//        		  hasOtherIncomeTypes:'No',
//        		  lossEventSubCategory:'',
//        		  changeInGainDate:'',
//        		  gainEventSubCategory:'',
//        		  dateOfChange:''
//            };
//        eventInfoArrayTemp.push(tempEventInfo);
//      }
//    }
//
//    $scope.getOriginalIndex=function(content,array){
//      return  $.inArray(content,array);
//    };
//
//    $scope.updateDates=function(){
//
//      for(var i=0;i<householdArray.length;i++){
//
//        for(var j=0;j<eventInfo.changedApplicants.length;j++){
//          if(eventInfo.changedApplicants[j].personId == householdArray[i].personId){
//            eventInfo.changedApplicants[j].eventDate=eventInfoArrayTemp[i].nameChangeDate;
//            break;
//               }
//         }
//        }
//      return;
//    };
//
//}]);
////////////////////////////Controller for loss of minimum coverage
//newMexico.controller('lossOfMinimumCoverageController', ['$scope', function($scope) {
//  //eventInfo.eventSubCategory = 'LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE';
//  //$scope.desinationArray=[];
//  $scope.modalShown = false;
//  $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
//    $scope.modalHouseholdMember=houseHoldFromParent;
//    $scope.modalShown = !$scope.modalShown;
//    $scope.originalIndex=originalIndex;
//   // $scope.desinationArray= eventInfoArrayTemp[originalIndex].lossReasons;
//  };
//
//
// /* $scope.addRemoveElement= function(event) {
//    var elementString= $(event.currentTarget).attr('array-content') ;
//     var elementIndex=$scope.desinationArray.indexOf(elementString);
//
//     if(event.currentTarget.checked){
//       if (elementIndex === -1) $scope.desinationArray.push(elementString);
//     }else{
//       if (elementIndex !== -1) $scope.desinationArray.splice(elementIndex, 1);
//     }
//
//   };*/
//
//  $scope.eventInfoArrayTemp=eventInfoArrayTemp;
//  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//  if(eventInfoArrayTemp.length<householdArray.length){
//    for(var i=(eventInfoArrayTemp.length);i<householdArray.length;i++){
//    	var tempEventInfo = {
//      		  changeInAddressDate:'',
//      		  citizenShipChangeDate:'',
//      		  tribeChangeDate:'',
//      		  changeInLossDate:'',
//      		  nameChangeDate:'',
//      		  incomeChangeDate:'',
//      		  incomeChangeIndicator:'',
//      		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//      		  hasOtherIncomeTypes:'No',
//      		  lossEventSubCategory:'',
//      		  changeInGainDate:'',
//      		  gainEventSubCategory:'',
//      		  dateOfChange:''
//          };
//      eventInfoArrayTemp.push(tempEventInfo);
//    }
//  }
//
//  $scope.getOriginalIndex=function(content,array){
//    return  $.inArray(content,array);
//  };
//
//  $scope.updateDates=function(){
//
//    for(var i=0;i<householdArray.length;i++){
//
//      for(var j=0;j<eventInfo.changedApplicants.length;j++){
//        if(eventInfo.changedApplicants[j].personId == householdArray[i].personId){
//          eventInfo.changedApplicants[j].eventDate=eventInfoArrayTemp[i].changeInLossDate;
//          eventInfo.eventSubCategory =eventInfoArrayTemp[i].lossEventSubCategory;
//          break;
//             }
//       }
//      }
//    return;
//  };
//
//  $scope.updateEventSubCategory=function(subCategory){
//	  eventInfo.eventSubCategory = subCategory;
//  };
//  
//}]);
//
////////////////////////////Controller for gain  Other Minimum Essential Coverage
//newMexico.controller('gainMinimumCoverageController', ['$scope', function($scope) {
//  //eventInfo.eventSubCategory = 'GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE';
//  $scope.modalShown = false;
//  $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
//    $scope.modalHouseholdMember=houseHoldFromParent;
//    $scope.modalShown = !$scope.modalShown;
//    $scope.originalIndex=originalIndex;
//  };
//
//  $scope.eventInfoArrayTemp=eventInfoArrayTemp;
//  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//  if(eventInfoArrayTemp.length<householdArray.length){
//    for(var i=(eventInfoArrayTemp.length);i<householdArray.length;i++){
//    	var tempEventInfo = {
//      		  changeInAddressDate:'',
//      		  citizenShipChangeDate:'',
//      		  tribeChangeDate:'',
//      		  changeInLossDate:'',
//      		  nameChangeDate:'',
//      		  incomeChangeDate:'',
//      		  incomeChangeIndicator:'',
//      		  incomeTypes:angular.copy(myService.getIncomeEvents()),
//      		  hasOtherIncomeTypes:'No',
//      		  lossEventSubCategory:'',
//      		  changeInGainDate:'',
//      		  gainEventSubCategory:'',
//      		  dateOfChange:''
//          };
//      eventInfoArrayTemp.push(tempEventInfo);
//    }
//  }
//
//  $scope.getOriginalIndex=function(content,array){
//    return  $.inArray(content,array);
//  };
//
//  $scope.updateDates=function(){
//
//    for(var i=0;i<householdArray.length;i++){
//
//      for(var j=0;j<eventInfo.changedApplicants.length;j++){
//        if(eventInfo.changedApplicants[j].personId == householdArray[i].personId){
//          eventInfo.changedApplicants[j].eventDate=eventInfoArrayTemp[i].changeInGainDate;
//          eventInfo.eventSubCategory =eventInfoArrayTemp[i].gainEventSubCategory;
//          break;
//             }
//       }
//      }
//    return;
//  };
//  
//  
//}]);
//
////////////////////////////Controller for income-deductions
//newMexico.controller('incomeDeductions', ['$scope', function($scope) {
//
//	
//	 var localHouseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
//	 $scope.deductionArray=localHouseHoldMemberArray[localHouseHoldMemberArray.length-1].detailedIncome.deductions.deductionType;
//	 
//	 //Load other deduction value
//	 $scope.otherDeductionType='';
//	 for (var i=0;i<$scope.deductionArray.length;i++){
//		 if($scope.deductionArray[i] != 'ALIMONY' &&  $scope.deductionArray[i] != 'STUDENT_LOAN_INTEREST'){$scope.otherDeductionType=$scope.deductionArray[i];break;}
//    }
//
//
//	 $scope.addRemoveElement= function(event) {
//		  var elementString= $(event.currentTarget).attr('array-content') ;
//		  var elementIndex=$scope.deductionArray.indexOf(elementString);
//
//		   if(event.currentTarget.checked){
//			   if (elementIndex === -1) $scope.deductionArray.push(elementString);
//            }else{
//			   if (elementIndex !== -1) $scope.deductionArray.splice(elementIndex, 1);
//            }
//
//	};
//
//	$scope.displayDeduction= function(deduction) {
//			  var index=$scope.deductionArray.indexOf(deduction);
//			  if(index==-1){
//				  return false;
//            }else{
//				  return true;
//            }
//	};	
//
//	$scope.displayOtherDeduction= function() {
//		for (var i=0;i<$scope.deductionArray.length;i++){
//			 if($scope.deductionArray[i] != 'ALIMONY' &&  $scope.deductionArray[i] != 'STUDENT_LOAN_INTEREST'){return true;break;}
//                      }
//		return false;
//	};	
//
//	$scope.addOtherTextToArray=function(){
//    //This function is on blur of other text box
//    //First remove any "Other" value from array and then add text box value to array
//    var indexFound=-1;
//    for(var i=0;i<$scope.deductionArray.length;i++){
//      if($scope.deductionArray[i]!='ALIMONY' && $scope.deductionArray[i]!='STUDENT_LOAN_INTEREST'){
//        indexFound=i;
//                        break;
//                      }
//                    }
//    if(indexFound!=-1){
//    	$scope.deductionArray.splice(indexFound, 1);
//                  }
//    $scope.deductionArray.push( $scope.otherDeductionType);
//  };
//
//  $scope.areOtherEmpty=function(){
//	  if($scope.deductionArray.length==0){return true;}else{return false;}
//  };
//  $scope.clearOthers=function(){
//	  $scope.deductionArray=[];  
//	  localHouseHoldMemberArray[localHouseHoldMemberArray.length-1].detailedIncome.deductions.deductionType=[];
//  };
//
//
//}]);
//
///*
//***********************************
//* Directive for validations starts
//***********************************
//*/
//
//newMexico.directive('numbersOnly', function(){
//     return {
//       require: 'ngModel',
//       link: function(scope, element, attrs, modelCtrl) {
//       //formatter to remove zero and show it as blank
//       var formatZeroAsBlank = $(element).attr('format-zero-as-blank');
//       if(formatZeroAsBlank == undefined || (formatZeroAsBlank != undefined && formatZeroAsBlank == 'true')){
//         modelCtrl.$formatters.push(function(val){
//            if(val=='0')
//              return '';
//            else
//              return val;
//           });
//       }
//         modelCtrl.$parsers.push(function (inputValue) {
//             // this next if is necessary for when using ng-required on your input.
//             // In such cases, when a letter is typed first, this parser will be called
//             // again, and the 2nd time, the value will be undefined
//             if (inputValue == undefined)
//               return undefined ;
//
//
//             //if starts with 0
//             //check 0 replace attr
//             var replaceZero = $(element).attr('numbers-only-replace-zero');
//             if(replaceZero == undefined || (replaceZero != undefined && replaceZero == 'true') ){
//               if(inputValue.indexOf('0')==0){
//                 //if length is 1
//                 if(inputValue.length == 1){
//                   modelCtrl.$setViewValue('');
//                   modelCtrl.$render();
//                   return undefined;
//                 }else if(inputValue.length > 1){//if length greater than 1
//                   var numericValue = Number(inputValue);//parse to number
//                   if(numericValue != 'NaN' && numericValue> 0 ) //if number is greater than 0 or a valid number
//                   {
//                     modelCtrl.$setViewValue(numericValue+''); //replace with numeric value
//                     modelCtrl.$render();
//                     return parseInt(inputValue);
//                   }else{
//                   modelCtrl.$setViewValue('');//replce with blank if value is zero
//                   modelCtrl.$render();
//                   return undefined;
//                   }
//                 }
//
//               }
//             }
//
//             //max
//             var max = $(element).attr('numbers-only-max');
//             if(max != undefined){
//               var numericValue = Number(inputValue);//parse to number
//               var maxNumeric =  Number(max);
//               if(numericValue != 'NaN' && maxNumeric != 'NaN'){
//                 if(numericValue>maxNumeric){
//                   modelCtrl.$setValidity('maxNumber',false);
//                 }else{
//                   modelCtrl.$setValidity('maxNumber',true);
//                }
//
//               }else{
//                 modelCtrl.$setValidity('maxNumber',true);
//               }
//             }
//             //min
//             var min = $(element).attr('numbers-only-min');
//             if(min != undefined){
//               var numericValue = Number(inputValue);//parse to number
//               var minNumeric =  Number(min);
//               if(numericValue != 'NaN' && minNumeric != 'NaN'){
//                 if(numericValue<minNumeric){
//                   modelCtrl.$setValidity('minNumber',false);
//                 }else{
//                   modelCtrl.$setValidity('minNumber',true);
//                 }
//
//               }else{
//                 modelCtrl.$setValidity('minNumber',true);
//               }
//             }
//
//             var transformedInput = inputValue.replace(/[^0-9]/g, '');
//             if (transformedInput!=inputValue) {
//                modelCtrl.$setViewValue(transformedInput);
//                modelCtrl.$render();
//             }
//
//             if(transformedInput!="" && !isNaN(transformedInput)){
//               return parseInt(transformedInput);;
//             }else{
//               return undefined;
//             }
//
//       });
//     }
//   };
//});
//
//
//
//
//newMexico.directive('validSsn', function(){
//    return {
//      require: 'ngModel',
//      link: function(scope, element, attrs, modelCtrl) {
//     //formatter to remove zero and show it as blank
//        modelCtrl.$formatters.push(function(val){
//          if(val=='0')
//            return '';
//          else
//            return val;
//        });
//        modelCtrl.$parsers.unshift(function (inputValue) {
//            // this next if is necessary for when using ng-required on your input.
//            // In such cases, when a letter is typed first, this parser will be called
//            // again, and the 2nd time, the value will be undefined
//             if (inputValue == undefined)
//               return '' ;
//             //if starts with 0
//             //check 0 replace attr
//
//            var arr  = inputValue.split('-');
//            if(arr.length == 3){
//              var ssn1 = Number(arr[0]);
//              var ssn2 = Number(arr[1]);
//              var ssn3 = Number(arr[2]);
//
//
//              if(ssn1 != 'NaN' && ssn2 != 'NaN' && ssn3 != 'NaN'){
//                // should not be 666 or 900-999 in the first digit group
//                if(!(ssn1 == 666 || (ssn1 >=900 && ssn1 <= 999)) ){
//                  modelCtrl.$setValidity('ssn',true);
//                  return inputValue;
//                }
//            }
//
//            }
//            modelCtrl.$setValidity('ssn',false);
//            return inputValue;
//
//
//      });
//    }
//  };
//});
//
//
//
///*
// ***********************************
// * Directive for validations ends
// ***********************************
// */
///*
// ***********************************
// * Directive for checkbox
// * (Where checking/unchecking will insert/remove @string data into @array)
// * CheckBox should have @array in which @string will be inserted
// * For adding element to destinationArray function should be present in controller.
// * This function shoud be called on click of checkbox by providing event as parameter.
// * If want to clear all checkboxes then empty array in controller.
// * @array=destinationArray
// * @string=arrayContent
// ***********************************
// */
//newMexico.directive("customCheckbox", function () {
//    return {
//        restrict: "A",
//        scope: {childCheckBoxArray: "=destinationArray",
//              arrayContent:"@arrayContent"},
//
//
//        link: function (scope, elem, attrs) {
//            if (scope.childCheckBoxArray.indexOf(scope.arrayContent) !== -1) {
//                elem[0].checked = true;
//            }else{
//              elem[0].checked = false;
//            }
//
//            scope.$parent.$watchCollection(attrs.destinationArray, function() {
//              if( scope.$parent[attrs.destinationArray].indexOf(scope.arrayContent) !== -1){
//               elem[0].checked = true;
//             }else{
//               elem[0].checked = false;
//             }
//
//            }, true);
//
//        }
//    };
//});
//
//function postPopulateIndex(zipCodeValue, zipId){
//    /* console.log(zipCodeValue,'---page---',zipId); */
//    jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
//    getCountyList(zipCodeValue, '', zipId);  
//}
//
//function getCountyList(zip, county, zipId) {
//    /* console.log(zip, county, 'get county list', zipId); */
//    var subUrl =  '/hix/shop/employee/addCounties';
//    $.ajax({
//           type : "POST",
//           url : subUrl,
//           data : {zipCode : zip}, 
//           dataType:'json',
//           success : function(response) {
//                  populateCounties(response, county, zipId);
//           },error : function(e) {
//                  alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
//           }
//    });
//}
//
//function populateCounties(response, county, zipIdFocused) {
//    /* console.log(zipIdFocused,'populate counties'); */
// 
//	var optionsstring = '<option value="">Select County</option>';
//           var i =0;
//           $.each(response, function(key, value) {
//                  var selected = (county == key) ? 'selected' : '';
//                  var optionVal = key;/*+'#'+value;*/
//                  var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
//                  optionsstring = optionsstring + options;
//                  i++;
//           });
//           //$('#'+zipIdFocused).parents('.addressBlock').find('select[data-rel="countyPopulate"]').html(optionsstring);
//}