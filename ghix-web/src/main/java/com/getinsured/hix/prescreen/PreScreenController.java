package com.getinsured.hix.prescreen;

import java.text.NumberFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.HIXHTTPClient;

/**
 * PreScreen jsp uses this controller to calculate income for Anonymous
 * pre-screen for given family size.
 * 
 * @author desu_s
 * @since 17-Dec-2012
 */
@Controller
public class PreScreenController {

	private static final Logger LOGGER = Logger
			.getLogger(PreScreenController.class);

	@Autowired
	private HIXHTTPClient hIXHTTPClient;

	private static NumberFormat NUM_FORMAT = NumberFormat.getInstance();

	/**
	 * In AJAX Call we will handle Exception and will display respective
	 * message. Using HIXHTTP client, a POST request, with json payload, is made
	 * to the eligibility module to retrieve the income anonymously for a given
	 * family size. Family income value is extracted from the returned json
	 * response and returned.
	 * 
	 * @param model
	 * @param request
	 * @return familyIncome
	 */
	@RequestMapping(value = "/prescreening/getIncomeForAnonymousPrescreen", method = RequestMethod.GET)
	@ResponseBody
	public String getIncomeForAnonymousPrescreen(Model model,
			HttpServletRequest request) {
		LOGGER.info("Calculating the income based on familySize for anonymous prescreen"); 
		//for a family of size "
				//+ request.getParameter("familySize"));
		String familyIncome = null;

		try {
			String postRequestContentType = "application/json";
			LOGGER.info("Url for anyonymous prescreen from ghixConstants is "
					+ GhixConstants.GET_INCOME_FOR_ANONYMOUS_PRESCREEN_URL);
			String postResponse = hIXHTTPClient.getPOSTData(
					GhixConstants.GET_INCOME_FOR_ANONYMOUS_PRESCREEN_URL,
					"{\"familySize\":" + request.getParameter("familySize")
							+ "}", postRequestContentType);
			familyIncome = postResponse.split("\"familyIncome\"")[1]
					.subSequence(
							1,
							postResponse.split("\"familyIncome\"")[1]
									.indexOf(',')).toString();
		} catch (Exception e) {
			LOGGER.error("Exception occurred while posting to eligibility module "
					+ e.getMessage());
		}
		return NUM_FORMAT.format(Double.parseDouble(familyIncome));
	}

	public HIXHTTPClient gethIXHTTPClient() {
		return hIXHTTPClient;
	}

	public void sethIXHTTPClient(HIXHTTPClient hIXHTTPClient) {
		this.hIXHTTPClient = hIXHTTPClient;
	}

}
