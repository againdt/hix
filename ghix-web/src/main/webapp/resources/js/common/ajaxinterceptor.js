(function(open) {

	XMLHttpRequest.prototype.open = function(method, url, async, user, pass) {

		this.addEventListener("readystatechange", function() {
			if(this.readyState === 1){
				this.setRequestHeader('X-Requested-With','XMLHttpRequest');
			}
			if (this.readyState === 4 && this.status === 401) {
				window.location.href = '/hix/error/pageExpired';
			}
		}, false);

		open.apply(this, arguments);
	};

})(XMLHttpRequest.prototype.open);
