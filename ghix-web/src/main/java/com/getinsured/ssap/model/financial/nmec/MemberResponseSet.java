
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mecResults",
    "ssn"
})
public class MemberResponseSet implements Serializable
{

    @JsonProperty("mecResults")
    private MecResults mecResults;
    @JsonProperty("ssn")
    private String ssn;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -5843639311053019728L;

    @JsonProperty("mecResults")
    public MecResults getMecResults() {
        return mecResults;
    }

    @JsonProperty("mecResults")
    public void setMecResults(MecResults mecResults) {
        this.mecResults = mecResults;
    }

    @JsonProperty("ssn")
    public String getSsn() {
        return ssn;
    }

    @JsonProperty("ssn")
    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mecResults", mecResults).append("ssn", ssn).append("additionalProperties", additionalProperties).toString();
    }

}
