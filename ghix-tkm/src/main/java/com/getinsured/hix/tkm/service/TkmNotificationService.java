package com.getinsured.hix.tkm.service;

import java.util.Map;

import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;


/**
 * 
 * @author Satyakam_k
 *
 */
public interface TkmNotificationService 
{
	void sendEmailNotification(Map<String, String> emailTokens,TkmTickets tkmTickets) throws NotificationTypeNotFound;

	void sendSecureEmail(Map<String, Object> emailTokens, Map<String, String> externalTokens) throws NotificationTypeNotFound;

	void sendSecureEmail(Map<String, String> emailTokens) throws NotificationTypeNotFound;
}
