/**
 * 
 */
package com.getinsured.eligibility.at.ref.dozzer.converter;

import org.dozer.DozerConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("dozzerDateConverter")
@Scope("singleton")
public class GlobalDateConverter
		extends
		DozerConverter<com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date, java.util.Date> {

	public GlobalDateConverter() {
		super(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date.class,
				java.util.Date.class);
	}

	@Override
	public com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date convertFrom(
			java.util.Date arg0,
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date arg1) {
		return null;
	}

	@Override
	public java.util.Date convertTo(
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date source,
			java.util.Date dest) {
		return ReferralUtil.extractDate(source);
	}

}
