(function() {
'use strict';
    angular.module('brokerAdminApp')
    .controller('ManageAgencyController', ManageAgencyController);

    ManageAgencyController.$inject = ['$scope', '$http', '$window', 'dataStorageService', 'StatusService'];

    function ManageAgencyController($scope, $http, $window, dataStorageService, StatusService) {
        var vm = this;
        vm.isAdminRole = StatusService.IsAdminRole;
    }
})();
