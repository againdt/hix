package com.getinsured.hix.entity.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Query;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * Contains Utility methods used in Entity module.
 */
@Component
public final class EntityUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(EntityUtils.class);
	private EntityUtils(){
		
	}
	@Autowired private Gson platformGson;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	
	/**
	 * @see EntityUtils#convertUtilToSQLDate(Date) Converts java util date to java sql date
	 * @param date
	 *            the util date to be converted into sql date
	 * @return java.sql.Date
	 */
	public static java.sql.Date convertUtilToSQLDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}

	/**
	 * @see EntityUtils#formatDate(String) Formats the given date into format defined in
	 *      EntityConstants.DATE_FORMAT
	 * @param date
	 *            the date to be formatted
	 * @return java.util.Date formatted date
	 * @throws ParseException 
	 */
	public static Date formatDate(String date) throws ParseException {
		Date formattedDate;
		formattedDate = new SimpleDateFormat(EntityConstants.DATE_FORMAT).parse(date);
		return formattedDate;
	}

	/**
	 * Changes date format as per argument
	 * 
	 * @param dateFormat
	 *            String
	 * @param dateInput
	 *            {@link Date}
	 * @return String formatted date
	 */
	public static String formatDate(String dateFormat, Date dateInput) {

		return dateInput != null ? new SimpleDateFormat(dateFormat).format(dateInput) : null;
	}

	/**
	 * @see EntityUtils#isEmpty(String) Checks for null or blank String objects
	 * @param value
	 *            the string to check
	 * @return true or false depending upon the condition
	 */
	public static boolean isEmpty(String value) {
		return (value != null && !"".equals(value)) ? false : true;
	}
	/**
	 * @see EntityUtils#splitStringValues(String,String) 
	 * @param value
	 *            the string to check
	 * @return true or false depending upon the condition
	 */
	public static List<String> splitStringValues(String values, String splitBy) {
		List<String> items = null;
		items = Arrays.asList(values.split(splitBy));
		items.removeAll(Collections.singletonList(null));

		List<String> stringList = new ArrayList<String>();
		for (Iterator<String> iter = items.iterator(); iter.hasNext();) {
			String element = iter.next();
			if (!stringList.contains(element.trim())) {
				stringList.add(element.trim());
			}
		}
		return stringList;
	}
	
	/**
	 * @see EntityUtils#appendZeros(String, int)
	 * 
	 *      Checks the length of a given string with given length and append
	 *      leading zeros
	 * 
	 * @param str
	 *            the string to check
	 * @param length
	 *            the length of the number to check
	 * @return original string with leading zeros
	 */
	public static String appendZeros(String str, int length) {
		int numberLength;
		StringBuilder numberWithZeros = new StringBuilder();
		if (str.length() != length) {
			numberLength = length - str.length();
			for (int i = 0; i < numberLength; i++) {
				numberWithZeros.append(0);
			}
		}
		return numberWithZeros.append(str).toString();
	}
	
	/**
	 * @param query
	 * @param columnValue
	 * @param isDateQuery 
	 */
	public static void appendToSearchAssisterQuery(StringBuilder query, String columnName, String columnValue, String columnReferenceKey, String operator, boolean isLikeQuery, boolean isDateQuery) {
		if (columnValue != null && !"".equals(columnValue)) {
			if(isLikeQuery) {
				query.append(" AND UPPER(").append(columnName).append(") ").append(operator).append(" UPPER(:").append(columnReferenceKey).append(")");
			}
			else {
				if(isDateQuery) {
					query.append(" AND ").append(columnName).append(" ").append(operator).append(" to_date(:").append(columnReferenceKey).append(",'mm-dd-yyyy')");
				
				}
				else {
					query.append(" AND ").append(columnName).append(" ").append(operator).append(" :").append(columnReferenceKey);
				}
				
			}
		}
	}
	
	public static void appendToSearchAssisterQuery(StringBuilder query, String columnName, String columnValue, String columnReferenceKey, String operator, boolean isLikeQuery, boolean isDateQuery,boolean incrementDate) {
		if (columnValue != null && !"".equals(columnValue)) {
			if(isLikeQuery) {
				query.append(" AND UPPER(").append(columnName).append(") ").append(operator).append(" UPPER(:").append(columnReferenceKey).append(")");
			}
			else {
				if(isDateQuery) {
					query.append(" AND ").append(columnName).append(" ").append(operator).append(" to_date(:").append(columnReferenceKey).append(",'mm-dd-yyyy')+1");
				
				}
				else {
					query.append(" AND ").append(columnName).append(" ").append(operator).append(" :").append(columnReferenceKey);
				}
				
			}
		}
	}
	
	
	
	/**
	 * @param searchQuery
	 * @param assisterFirstName
	 */
	public static void setQueryParameter(Query searchQuery, String parameter, String value, boolean isWildSearch) {
		if (value != null && !"".equals(value)) {
			if(isWildSearch) {
				searchQuery.setParameter(parameter, "%"+value.trim()+"%");
			}
			else {
				searchQuery.setParameter(parameter, value.trim());
			}
		}
	}
	
	public static int getLoggedInUser()
	{
		final SecurityContext ctx = SecurityContextHolder.getContext();

		if(ctx != null)
		{
			final Authentication auth = ctx.getAuthentication();

			if(auth != null)
			{
				final Object principal = auth.getPrincipal();
				//log.debug("GHIXJpaDialect::getLoggedInUser() - UserAccount: " + principal);

				if(principal instanceof AccountUser)
				{
					final AccountUser accountUser = (AccountUser)principal;
					return accountUser.getId();
				}
			}
		}

		return -1;
	}
	
	public static AccountUser getLoggedInUserObj()
	{
		final SecurityContext ctx = SecurityContextHolder.getContext();

		if(ctx != null)
		{
			final Authentication auth = ctx.getAuthentication();

			if(auth != null)
			{
				final Object principal = auth.getPrincipal();				

				if(principal instanceof AccountUser)
				{
					final AccountUser accountUser = (AccountUser)principal;
					return accountUser;
				}
			}
		}

		return null;
	}
	
public EnrollmentResponse updateEnrollmentDetails(String marketType,Long householdId,Integer assisterID,String roleType,String actionFlag) {
		
		String userName = getLoggedInUserObj().getUsername();
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO =  new EnrollmentBrokerUpdateDTO();

		if(checkStateCode(EntityConstants.CA_STATE_CODE)){
			enrollmentBrokerUpdateDTO.setHouseHoldCaseId(null);
			enrollmentBrokerUpdateDTO.setExchgIndividualIdentifier(String.valueOf(householdId));
		}else{
			enrollmentBrokerUpdateDTO.setHouseHoldCaseId(String.valueOf(householdId));
			enrollmentBrokerUpdateDTO.setExchgIndividualIdentifier(null);
		}
		enrollmentBrokerUpdateDTO.setRoleType(roleType);
		enrollmentBrokerUpdateDTO.setAssisterBrokerId(assisterID);
		enrollmentBrokerUpdateDTO.setAddremoveAction(actionFlag);
		enrollmentBrokerUpdateDTO.setMarketType(marketType);			
		enrollmentRequest.setEnrollmentBrokerUpdateData(enrollmentBrokerUpdateDTO);
		
		String response = null;
		EnrollmentResponse enrollmentResponse = null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentRequest.class);
		try {
			response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS, 
					userName, HttpMethod.POST, MediaType.APPLICATION_JSON, 
					String.class, writer.writeValueAsString(enrollmentRequest)).getBody();
			
//			LOGGER.debug("Response : " + response);
			
			if (null != response) { 
				
				 enrollmentResponse = platformGson.fromJson(response, EnrollmentResponse.class);
				 if(enrollmentResponse != null	&& enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
						LOGGER.info("Failed to update enrollment: "+ "Error Code : " + enrollmentResponse.getErrCode() +
								" Error Message : " + enrollmentResponse.getErrMsg());
						
						//throw new GIRuntimeException(null, null, Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
					}
			}
			
		} catch (Exception e) {
			LOGGER.error("Exception occured while updating enrollment details : ", e);					
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}		
		return enrollmentResponse;		
	}

	public static boolean checkStateCode(String code){
	  String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	  return stateCode.equalsIgnoreCase(code);
	}

	
	
}
