package com.getinsured.eligibility.renewal.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.eligibility.at.resp.service.SsapApplicationService;
import com.getinsured.eligibility.redetermination.service.RenewalApplicationService;
import com.getinsured.hix.dto.eligibility.RenewalRequest;
import com.getinsured.hix.dto.eligibility.RenewalResponse;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderRequestDTO;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderResponseDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * 
 * @author Dhananjay
 *
 */
@RestController
@RequestMapping("/renewal")
public class RenewalController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RenewalController.class);

	@Autowired
	private RenewalApplicationService renewalApplicationService;
	
	@Autowired 
	private SsapApplicationService ssapApplicationService;
	
	
	@RequestMapping(value = "/getRenewalStatus", method = RequestMethod.POST)
	@ResponseBody
	public RenewalResponse getRenewalStatus(@RequestBody RenewalRequest request) {
		LOGGER.info("getRenewalStatus for applicationId = " + request.getApplicationId());
		RenewalResponse response = new RenewalResponse();
		try 
		{
			Long ssapApplicationId = NumberUtils.toLong((request.getApplicationId()));
			if (ssapApplicationId == null || ssapApplicationId <= 0) {
				response.setStatusCode(HttpStatus.BAD_REQUEST.value());
				response.setMessage("Invalid SSAP Application ID: " +  request.getApplicationId());
				return response;
			}
			else
			{
				Map<String,Object> renewalApplicationStatus =  renewalApplicationService.getRenewalApplicationStatus(ssapApplicationId);
				response.setData(renewalApplicationStatus);
				response.setMessage("SUCCESS");
				response.setStatusCode(HttpStatus.OK.value());
			}
		}
		catch(GIException e) {		
			response.setMessage(e.getMessage());
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		catch(Exception e) {		
			response.setMessage("FAILURE");
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return response;
	}

	@ApiOperation(value = "Set RenewalStatus to NOT_TO_CONSIDER", notes = "API is used to set RenewalStatus.NOT_TO_CONSIDER by SSAP Application ID and Plan Type in Renewal Application and Renewal Applicant tables")
	@RequestMapping(value = "/setNotToConsiderRenewalStatus", method = RequestMethod.PUT)
	@ResponseBody
	public RenewalStatusNotToConsiderResponseDTO setNotToConsiderRenewalStatus(@RequestBody RenewalStatusNotToConsiderRequestDTO requestDTO) {

		LOGGER.info("Inside API /setNotToConsiderRenewalStatus");
		return renewalApplicationService.setNotToConsiderRenewalStatusByApplIdAndPlanType(requestDTO);
	}
	
	@RequestMapping(value = "/fetchHouseholdRenewalStatus/{houseHoldId}/{year}", method = RequestMethod.GET, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public HttpEntity<String> fetchHouseholdRenewalStatus(@PathVariable("houseHoldId") String houseHoldId, @PathVariable("year") String enrollmentYear) {
		LOGGER.info("fetchHouseholdRenewalStatus for cmrHouseholdId ");
		String currentOEStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
		String currentOEEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
		String householdRenewalStatus = "CLOSED";
		String currentDate = ReferralUtil.formatDate(ReferralUtil.currentDate(), ReferralConstants.DEFDATEPATTERN);
		LOGGER.warn("FetchHouseholdRenewalStatus - System time: "+ currentDate);
		LOGGER.warn("currentOEStartDate - "+ currentOEStartDate);
		LOGGER.warn("currentOEEndDate - "+ currentOEEndDate);
		Long coverageYear = null;
		try{
			if(houseHoldId!=null && !houseHoldId.isEmpty()){
				BigDecimal householdId = new BigDecimal((houseHoldId));
				if(enrollmentYear != null && !enrollmentYear.isEmpty() && StringUtils.isNumeric(enrollmentYear)){
					coverageYear = Long.valueOf(enrollmentYear);
				}else{
					coverageYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
				}
				List<SsapApplication> ssapApplications = ssapApplicationService.findByHHIdAndCoverageYear(householdId,coverageYear);
				if(ssapApplications != null && ssapApplications.size()>0){
					SsapApplication ssapApplication  = ssapApplications.get(0);
					//Enrollment Period
					if(currentOEStartDate!=null && !currentOEStartDate.isEmpty() && currentOEEndDate!=null && !currentOEEndDate.isEmpty() && currentDate!=null && !currentDate.isEmpty() 
							&& ReferralUtil.compareDate(ReferralUtil.convertStringToDate(currentOEStartDate), ReferralUtil.convertStringToDate(currentDate)) >= 0 
							&& ReferralUtil.compareDate(ReferralUtil.convertStringToDate(currentOEEndDate), ReferralUtil.convertStringToDate(currentDate)) <= 0){
						//application_type should be OE
						LOGGER.warn("FetchHouseholdRenewalStatus - In OE duration and applicationType: "+ ssapApplication.getApplicationType());
						if(ssapApplication.getApplicationType().equalsIgnoreCase("OE")){
							//check application_Status and application_dental_status
							if((("CL").equalsIgnoreCase(ssapApplication.getApplicationStatus()) || ("EN").equalsIgnoreCase(ssapApplication.getApplicationStatus())
									&& ("EN").equalsIgnoreCase(ssapApplication.getApplicationDentalStatus()))){
								householdRenewalStatus = "CLOSED";
							}else {
								householdRenewalStatus = "OPEN";
							}
						}else{
							householdRenewalStatus = "CLOSED";
						}
					}
				}else{
					LOGGER.warn("FetchHouseholdRenewalStatus - Application is not yet created ");
					//if there is no application and if it is in OE duration
					if(currentOEStartDate!=null && !currentOEStartDate.isEmpty() && currentOEEndDate!=null && !currentOEEndDate.isEmpty() && currentDate!=null && !currentDate.isEmpty()
							&& ReferralUtil.compareDate(ReferralUtil.convertStringToDate(currentOEStartDate), ReferralUtil.convertStringToDate(currentDate)) >= 0 
							&& ReferralUtil.compareDate(ReferralUtil.convertStringToDate(currentOEEndDate), ReferralUtil.convertStringToDate(currentDate)) <= 0){
						LOGGER.warn("FetchHouseholdRenewalStatus - In OE duration but application is not yet created ");
						householdRenewalStatus = "OPEN";
					}
				}
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occurred while retrieving renewal status.",ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(householdRenewalStatus, HttpStatus.OK);
	}

}
