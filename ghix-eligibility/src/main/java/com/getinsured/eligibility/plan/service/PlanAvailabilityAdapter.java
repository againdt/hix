package com.getinsured.eligibility.plan.service;

import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS2;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS3;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS4;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS5;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS6;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.ind19.util.service.EnrollmentsExtractionService;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemPersonResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdPersonRequest;
import com.getinsured.hix.dto.plandisplay.planavailability.PdRequestPlanAvailability;
import com.getinsured.hix.dto.plandisplay.planavailability.PdResponsePlanAvailability;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Component("planAvailabilityAdapter")
public class PlanAvailabilityAdapter {

	private static final String DLT_NOT_RESPONSE_CODE = "104";
	private static final String HLT_NOT_RESPONSE_CODE = "103";
	private static final String HLT_DLT_NOT_RESPONSE_CODE = "102";
	private static final String PLAN_AVAILABILITY = "PLAN_AVAILABILITY";
	private static final String EXCEPTION = "Exception - ";
	private static final String SEPARATOR = " - ";
	private static final String PLAN_AVAILABILITY_RETURNED_ERROR_CODE = "planAvailability returned error code - ";
	private static final String SUCCESS_RESPONSE_CODE = "200";
	private static final String RESPONSE = "Response - ";
	private static final String REQUEST = "Request - ";
	private static final String NEW_LINE = "\n";
	private static final String SUCCESS = "SUCCESS";
	private static final String FAILURE = "FAILURE";

	private static final String PRIMARY_PERSON_ID = "1";
	private static final String NO_ELIGIBLE_MEMBERS_FOUND = "No Eligible members found!";
	private static final String PRIMARY_INFORMATION_NOT_FOUND = "Primary information not found!";
	private static final String NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION = "No Application Events found for application!";
	private static final String EMPTY_RESPONSE_FROM_PLAN_AVAILABILITY_API = "Empty response from planAvailability API";
	private static final String THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR = "There are no enrolled SSAPs for this household for coverage year - ";
	private static final String EXISTING_MEDICAL_ENROLLMENT_COULD_NOT_BE_RETRIEVED_FOR_THE_APPLICATION_WITH_CASENUMBER = "Existing medical enrollment could not be retrieved for the application with casenumber ";
	private static final String GETTING_ENROLLMENT_DETAILS_FOR_LINKED_ENROLLED_APPLICATION = "Getting enrollment details for linked enrolled application - ";
	private static final String ERROR_COMPUTING_COVERAGE_START_DATE_CURRENT_APPLICATION = "Error computing coverage start date current application - ";
	private static final String HEALTH = "health";
	private static final String DENTAL = "dental";
	private static final String UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID = "Unable to find application for ssapApplicationId - ";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	
	@Autowired
	private CoverageStartDateService coverageStartDateService;
	
	@Autowired
	private EnrollmentsExtractionService enrollmentsExtractionService;
	
	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired 
	private GhixRestTemplate ghixRestTemplate;

	@Autowired private IntegrationLogService integrationLogService;
	
	private static List<String> CHILD_RELATIONSHIP_CODES = Arrays.asList("05", "07", "09", "10", "17", "19");

	private static List<String> SPOUSE_RELATIONSHIP_CODES = Arrays.asList("01");
	private static List<String> SELF_RELATIONSHIP_CODES = Arrays.asList("18");

	public PlanAvailabilityAdapterResponse execute(PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {

		PdRequestPlanAvailability request;
		try {
			validatePlanAvailabilityAdapterRequest(planAvailabilityAdapterRequest);
			request = formRequest(planAvailabilityAdapterRequest.getSsapApplicationId(), 
					planAvailabilityAdapterRequest.getHealthEnrollees(), planAvailabilityAdapterRequest.getDentalEnrollees());
		} catch (Exception e) {
			logData(planAvailabilityAdapterRequest.getSsapApplicationId(), e.getMessage(), FAILURE);
			throw new GIRuntimeException(e);
		}
		
		PdResponsePlanAvailability pdResponse = invokePlanAvailabilityAPI(request, planAvailabilityAdapterRequest.getSsapApplicationId());
		
		Long healthEnrollmentId = null;
		Long dentalEnrollmentId = null;
		for (PdPersonRequest person : request.getPdPersonCRDTOList()) {
			if (person.getHealthEnrollmentId() != null) {
				healthEnrollmentId = person.getHealthEnrollmentId();
			}
			if (person.getDentalEnrollmentId() != null) {
				dentalEnrollmentId = person.getDentalEnrollmentId();
			}
			if (healthEnrollmentId != null && dentalEnrollmentId != null) {
				break;
			}

		}
		return formResponse(pdResponse, request.getCoverageDate(),dentalEnrollmentId,healthEnrollmentId);
	}

	private PlanAvailabilityAdapterResponse formResponse(PdResponsePlanAvailability pdResponse, String coverageDate, Long dentalEnrollmentId, Long healthEnrollmentId) {
		
		Map<String, ApplicantPlanDto> healthPlanDisplayMap = map(pdResponse, PlanDisplayEnum.InsuranceType.HEALTH);
		Map<String, ApplicantPlanDto> dentalPlanDisplayMap = map(pdResponse, PlanDisplayEnum.InsuranceType.DENTAL);
		
				
		PlanAvailabilityAdapterResponse response = new PlanAvailabilityAdapterResponse();
		response.setHealthPlanDisplayMap(healthPlanDisplayMap);
		response.setDentalPlanDisplayMap(dentalPlanDisplayMap);
		response.setCoverageStartDate(ReferralUtil.convertStringToDate(coverageDate));
		if(dentalEnrollmentId != null)
		{
			response.setDentalEnrollmentId(dentalEnrollmentId);
		}
		response.setHealthEnrollmentId(healthEnrollmentId);
		
		return response;
	}
	
	private Map<String, ApplicantPlanDto> map(PdResponsePlanAvailability pdResponse, InsuranceType type) {

		Map<String, ApplicantPlanDto> planDisplayMap = new HashMap<String, ApplicantPlanDto>();
		if (pdResponse != null && pdResponse.getPdOrderItemResponse() != null) {
			for (PdOrderItemResponse plandisplayresponse : pdResponse.getPdOrderItemResponse()) {
				if (type == plandisplayresponse.getInsuranceType()) {
					if (ReferralUtil.listSize(plandisplayresponse.getPdOrderItemPersonResponseList()) != 0) {
						for (PdOrderItemPersonResponse pdApplicantDetails : plandisplayresponse.getPdOrderItemPersonResponseList()){
							ApplicantPlanDto applicantPlanDto = populateApplicantPlanDto(pdApplicantDetails, plandisplayresponse);
							planDisplayMap.put(String.valueOf(pdApplicantDetails.getMemberGUID()), applicantPlanDto);
						}
					}
				}
			}
		}
		return planDisplayMap;
	}

	private ApplicantPlanDto populateApplicantPlanDto(PdOrderItemPersonResponse pdApplicantDetails, PdOrderItemResponse plandisplayresponse) {
			
		ApplicantPlanDto applicantPlanDto = new ApplicantPlanDto();
		applicantPlanDto.setMemberGUID(pdApplicantDetails.getMemberGUID());
		applicantPlanDto.setSubscriberFlag(pdApplicantDetails.getSubscriberFlag());
		applicantPlanDto.setPremium(pdApplicantDetails.getPremium());
		applicantPlanDto.setRegion(pdApplicantDetails.getRegion());
		applicantPlanDto.setInsuranceType(plandisplayresponse.getInsuranceType());
		applicantPlanDto.setPlanId(plandisplayresponse.getPlanId());
		applicantPlanDto.setPlanLevelMonthlySubsidy(plandisplayresponse.getMonthlySubsidy());
		applicantPlanDto.setPlanLevelPremiumAfterCredit(plandisplayresponse.getPremiumAfterCredit());
		applicantPlanDto.setPlanLevelPremiumBeforeCredit(plandisplayresponse.getPremiumBeforeCredit());
		
		return applicantPlanDto;
	}

	private PdRequestPlanAvailability formRequest(Long ssapApplicationId, List<String> healthEnrollees, List<String> dentalEnrollees) {
		SsapApplication currentApplication = validate(ssapApplicationId);

		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
		
		SsapApplicant primaryApplicantDB = getPrimaryApplicantDB(currentApplication);
		HouseholdMember primaryApplicantJson = getPrimaryApplicantJson(members, primaryApplicantDB);
		
		if (primaryApplicantDB == null || primaryApplicantJson == null){
			throw new GIRuntimeException(PRIMARY_INFORMATION_NOT_FOUND);
		}
		
		Map<Long, SsapApplicant> eligibleApplicantDBRowMap = new HashMap<Long, SsapApplicant>();
		Map<Long, HouseholdMember> eligibleApplicantJsonRowMap = new HashMap<Long, HouseholdMember>();		
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus())){
				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0){
						eligibleApplicantJsonRowMap.put(applicant.getPersonId(), householdMember);
						break;
					}
				}
				eligibleApplicantDBRowMap.put(applicant.getPersonId(), applicant);
			}
		}
		
		if (eligibleApplicantJsonRowMap.isEmpty() || eligibleApplicantDBRowMap.isEmpty()){
			throw new GIRuntimeException(NO_ELIGIBLE_MEMBERS_FOUND);
		}

		List<BloodRelationship> relationshipList = getRelationships(primaryApplicantJson);
		List<String> enrollees = new ArrayList<String>();
		if (CollectionUtils.isNotEmpty(healthEnrollees)) {
			enrollees.addAll(healthEnrollees);
		}
		if(CollectionUtils.isNotEmpty(dentalEnrollees)) {
			enrollees.addAll(dentalEnrollees);
		}
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = getEnrollmentDetails(currentApplication, enrollees);
		Date coverageDate = computeCoverageStartDate(currentApplication.getId(), enrollmentsByTypeMap);
		
		EnrollmentShopDTO healthEnrollmentShopDTO = enrollmentsByTypeMap.get("health");
		
		if(healthEnrollmentShopDTO != null && ReferralUtil.isCoverageDatePastEnrollmentDate(coverageDate, healthEnrollmentShopDTO.getCoverageEndDate())) {
			throw new GIRuntimeException(ReferralConstants.COVERAGE_DATE_IS_PAST_ENROLLMENT_DATE + ReferralConstants.COVERAGE_DATE_STR_VAL + coverageDate + ReferralConstants.CURRENT_ENROLLMENT_END_DATE_STR_VAL + healthEnrollmentShopDTO.getCoverageEndDate());        
		}
	
		PdRequestPlanAvailability request = populateRequest(currentApplication, primaryApplicantDB, primaryApplicantJson,
				eligibleApplicantDBRowMap, eligibleApplicantJsonRowMap,
				relationshipList, enrollmentsByTypeMap, coverageDate, healthEnrollees, dentalEnrollees);
		
		return request;
	}

	private PdRequestPlanAvailability populateRequest(
			SsapApplication currentApplication,
			SsapApplicant primaryApplicantDB,
			HouseholdMember primaryApplicantJson,
			Map<Long, SsapApplicant> eligibleApplicantDBRowMap,
			Map<Long, HouseholdMember> eligibleApplicantJsonRowMap,
			List<BloodRelationship> relationshipList,
			Map<String, EnrollmentShopDTO> enrollmentsByTypeMap,
			Date coverageDate,
			List<String> healthEnrollees, List<String> dentalEnrollees
			) {
		
		PdRequestPlanAvailability request = new PdRequestPlanAvailability();
		request.setHouseholdGUID(currentApplication.getCmrHouseoldId().longValue());
		request.setCoverageDate(ReferralUtil.formatDate(coverageDate, ReferralConstants.DEFDATEPATTERN));
		request.setShoppingType(ShoppingType.INDIVIDUAL);
		request.setEnrollmentType(EnrollmentType.S);
		request.setCostSharing(currentApplication.getCsrLevel() != null ? mapCostSharing(currentApplication.getCsrLevel()) : null);		
		request.setMaxSubsidy(currentApplication.getMaximumAPTC() != null ? currentApplication.getMaximumAPTC().floatValue() : null);

		List<PdPersonRequest> pdPersonCRDTOList = new ArrayList<PdPersonRequest>();
		Set<Long> persons = new TreeSet<Long>(eligibleApplicantDBRowMap.keySet());

		for (Long personId : persons) {
			PdPersonRequest person = new PdPersonRequest();
			SsapApplicant applicantDBRow = eligibleApplicantDBRowMap.get(personId);
			HouseholdMember applicantJsonRow = eligibleApplicantJsonRowMap.get(personId);
			
			person.setCountyCode(getCounty(applicantDBRow, applicantJsonRow, primaryApplicantDB, primaryApplicantJson));
			person.setDob(ReferralUtil.formatDate(applicantJsonRow.getDateOfBirth(), ReferralConstants.DEFDATEPATTERN));
			person.setDentalEnrollmentId(getEnrollmentId(enrollmentsByTypeMap, applicantDBRow.getApplicantGuid(), dentalEnrollees, DENTAL));
			person.setHealthEnrollmentId(getEnrollmentId(enrollmentsByTypeMap, applicantDBRow.getApplicantGuid(), healthEnrollees, HEALTH));
			person.setMemberGUID(applicantDBRow.getApplicantGuid());
			person.setRelationship(getRelationShip(relationshipList, String.valueOf(applicantJsonRow.getPersonId())));
			person.setTobacco(StringUtils.equalsIgnoreCase(ReferralConstants.Y, applicantDBRow.getTobaccouser()) ? YorN.Y : YorN.N);
			person.setZipCode(getZipCode(applicantDBRow, applicantJsonRow, primaryApplicantDB, primaryApplicantJson));
			
			pdPersonCRDTOList.add(person);

		}
		request.setPdPersonCRDTOList(pdPersonCRDTOList);
		return request;
	}

	private List<BloodRelationship> getRelationships(HouseholdMember primaryApplicantJson) {
		List<BloodRelationship> relationshipList = new ArrayList<BloodRelationship>();
		for(BloodRelationship relationship : primaryApplicantJson.getBloodRelationship()){
			if (PRIMARY_PERSON_ID.equals(relationship.getRelatedPersonId())){
				relationshipList.add(relationship);
			}
		}
		
		return relationshipList;
	}

	private HouseholdMember getPrimaryApplicantJson(List<HouseholdMember> members, SsapApplicant primaryApplicantDB) {
		HouseholdMember primaryApplicantJson = null;
		for (HouseholdMember householdMember : members) {
			if (householdMember.getPersonId().compareTo((int) primaryApplicantDB.getPersonId()) == 0){
				primaryApplicantJson = householdMember;
				break;
			}
		}
		return primaryApplicantJson;
	}

	private SsapApplicant getPrimaryApplicantDB(SsapApplication currentApplication) {
		SsapApplicant primaryApplicantDB = null;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if (applicant.isPrimaryTaxFiler()){
				primaryApplicantDB = applicant;
				break;
			}
		}
		if(primaryApplicantDB == null){
			for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
				if (applicant.getPersonId() == 1){
					primaryApplicantDB = applicant;
					break;
				}
			}
		}
		return primaryApplicantDB;
	}

	private SsapApplication validate(Long ssapApplicationId) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException(UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID + ssapApplicationId);
		}

		SsapApplication currentApplication = ssapApplications.get(0);
		
		SsapApplicationEvent currentApplicationEvent = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(ssapApplicationId);
		
		if (currentApplicationEvent == null){
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}
		return currentApplication;
	}
	
	private void validatePlanAvailabilityAdapterRequest(PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {
		if (planAvailabilityAdapterRequest.getSsapApplicationId() == 0){
			throw new GIRuntimeException("Invalid planAvailabilityAdapterRequest ssap application id" + planAvailabilityAdapterRequest.getSsapApplicationId());
		}
		
		if (planAvailabilityAdapterRequest.getHealthEnrollmentId() == 0){
			throw new GIRuntimeException("Invalid planAvailabilityAdapterRequest health enrollment id" + planAvailabilityAdapterRequest.getHealthEnrollmentId());
		}
		
		if (planAvailabilityAdapterRequest.getHealthEnrollmentId() != 0 
				&& (planAvailabilityAdapterRequest.getHealthEnrollees() == null || planAvailabilityAdapterRequest.getHealthEnrollees().size() == 0)){
			throw new GIRuntimeException("Missing planAvailabilityAdapterRequest health enrollee list for " + planAvailabilityAdapterRequest.getHealthEnrollmentId());
		}
		
		if (planAvailabilityAdapterRequest.getDentalEnrollmentId() != 0 
				&& (planAvailabilityAdapterRequest.getDentalEnrollees() == null || planAvailabilityAdapterRequest.getDentalEnrollees().size() == 0)){
			throw new GIRuntimeException("Missing planAvailabilityAdapterRequest dental enrollee list for " + planAvailabilityAdapterRequest.getDentalEnrollmentId());
		}
		
	}

	private PdResponsePlanAvailability invokePlanAvailabilityAPI(PdRequestPlanAvailability request, Long ssapApplicationId) {
		PdResponsePlanAvailability pdResponsePlanAvailability = null;
		StringBuilder finalPayload = new StringBuilder();
		String status = FAILURE;
		try{
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			finalPayload.append(REQUEST).append(requestJson);
			ResponseEntity<PdResponsePlanAvailability> response = ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.FIND_PLAN_AVAILABILITY, ReferralConstants.EXADMIN_USERNAME, 
					HttpMethod.POST, MediaType.APPLICATION_JSON, PdResponsePlanAvailability.class, request);
			
			if (response == null || response.getBody() == null){
				throw new GIRuntimeException(EMPTY_RESPONSE_FROM_PLAN_AVAILABILITY_API);
			}
			
			pdResponsePlanAvailability = response.getBody();
			
			String responseJson = mapper.writeValueAsString(pdResponsePlanAvailability);
			
			finalPayload.append(NEW_LINE).append(RESPONSE).append(responseJson);
			
			if (!StringUtils.equals(pdResponsePlanAvailability.getErrCode(), SUCCESS_RESPONSE_CODE)
					&& !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), HLT_DLT_NOT_RESPONSE_CODE)
					&& !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), HLT_NOT_RESPONSE_CODE)
					&& !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), DLT_NOT_RESPONSE_CODE)){
				throw new GIRuntimeException(PLAN_AVAILABILITY_RETURNED_ERROR_CODE + pdResponsePlanAvailability.getErrCode() + SEPARATOR  + pdResponsePlanAvailability.getResponse());
			} else {
				status = SUCCESS;
			}
			
		} catch(Exception e){
			finalPayload.append(NEW_LINE).append(EXCEPTION).append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		} finally {
			logData(ssapApplicationId, finalPayload.toString(), status);
		}
		return pdResponsePlanAvailability;
	}

	private Long getEnrollmentId(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String applicantGuid, List<String> enrollees, String enrollmentType) {
		Long enrollmentId = null;
		if (enrollees != null && enrollees.size() > 0 && enrollees.contains(applicantGuid)){
			EnrollmentShopDTO enrollment = enrollmentsByTypeMap.get(enrollmentType);
			enrollmentId =  enrollment != null ? enrollment.getEnrollmentId().longValue() : null;
		}
		return enrollmentId;
		
	}

	private Timestamp computeCoverageStartDate(Long currentApplicationId, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap) {
		try {
			return coverageStartDateService.computeCoverageStartDate(BigDecimal.valueOf(currentApplicationId), 
					enrollmentsByTypeMap, 
					false,false);
		} catch (GIException e) {
			throw new GIRuntimeException(ERROR_COMPUTING_COVERAGE_START_DATE_CURRENT_APPLICATION + currentApplicationId);
		}
	}

	private Map<String, EnrollmentShopDTO> getEnrollmentDetails(SsapApplication currentApplication, List<String> enrollees) {
		
		SsapApplication enrolledApplication = getEnrolledSsapForCoverageYearAndHousehold(currentApplication);
		
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>(0);
		try {
			enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveEnrollmentsForApplicants(enrolledApplication.getId(), enrollees);
		} catch (GIException e) {
			throw new GIRuntimeException(GETTING_ENROLLMENT_DETAILS_FOR_LINKED_ENROLLED_APPLICATION + enrolledApplication.getCaseNumber());
		}
		if(enrollmentsByTypeMap == null || enrollmentsByTypeMap.isEmpty() || null == enrollmentsByTypeMap.get(HEALTH)){
			throw new GIRuntimeException(EXISTING_MEDICAL_ENROLLMENT_COULD_NOT_BE_RETRIEVED_FOR_THE_APPLICATION_WITH_CASENUMBER+ enrolledApplication.getCaseNumber());
		}
		return enrollmentsByTypeMap;
	}
	
	public SsapApplication getEnrolledSsapForCoverageYearAndHousehold(SsapApplication currentApplication) {
		SsapApplication enrolledApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
		if(enrolledApplication == null){
			throw new GIRuntimeException(THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR + currentApplication.getCoverageYear());
		}
		return enrolledApplication;
	}

	private Relationship getRelationShip(List<BloodRelationship> relationshipList, String personId ) {
		Relationship relationShipCode = null;
		for (BloodRelationship bloodRelationship : relationshipList) {
			if (personId.equals(bloodRelationship.getIndividualPersonId())){
				if (SELF_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())){
					relationShipCode = Relationship.SELF;
				} else if (SPOUSE_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())){
					relationShipCode = Relationship.SPOUSE;
				} else if (CHILD_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())){
					relationShipCode = Relationship.CHILD;
				} else {
					relationShipCode = Relationship.DEPENDENT;
				}
			}
		}
		return relationShipCode;
	}

	private String getZipCode(SsapApplicant applicantDBRow,	HouseholdMember applicantJsonRow, SsapApplicant primaryApplicantDB, HouseholdMember primaryApplicantJson) {
		String zipCode = primaryApplicantJson.getHouseholdContact().getHomeAddress().getPostalCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null){ 
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()){
				zipCode = applicantJsonRow.getOtherAddress().getAddress().getPostalCode(); 
			}
		}
		return zipCode;
	}

	private String getCounty(SsapApplicant applicantDBRow, HouseholdMember applicantJsonRow, SsapApplicant primaryApplicantDB, HouseholdMember primaryApplicantJson) {
		String countyCode = primaryApplicantJson.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null){ 
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()){
				countyCode = applicantJsonRow.getOtherAddress().getAddress().getCounty(); 
			}
		}
		return countyCode;
	}

	private CostSharing mapCostSharing(String csrLevel) {
		CostSharing csLevel = null;
		switch (csrLevel) {
			case ReferralConstants.CS2:
				csLevel = CS2;
				break;
			case ReferralConstants.CS3:
				csLevel = CS3;
				break;
			case ReferralConstants.CS4:
				csLevel = CS4;
				break;
			case ReferralConstants.CS5:
				csLevel = CS5;
				break;
			case ReferralConstants.CS6:
				csLevel = CS6;
				break;
			default:
				break;
		}

		return csLevel;
	}

	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, PLAN_AVAILABILITY, status, ssapApplicationId, null);
	}

}
