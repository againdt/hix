
package com.getinsured.iex.ssap;

import javax.validation.constraints.Pattern;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class SocialSecurityCard {

    @Expose
    private String middleNameOnSSNCard;
    @Expose
    private Boolean deathManualVerificationIndicator=false;
   
    @Pattern(regexp = "|^[0-9]{9}$")
    @Expose
    private String socialSecurityNumber;
    @Expose
    private String suffixOnSSNCard;
    @Expose
    private Boolean individualMandateExceptionIndicator=false;
    @Expose
    private Boolean ssnManualVerificationIndicator=false;
    @Expose
    private Boolean deathIndicatorAsAttested=false;
    @Expose
    private Boolean nameSameOnSSNCardIndicator = null; // UI doesn't want anything preselected until user picks.
    @Expose
    private Boolean deathIndicator=false;
    @Expose
    private String firstNameOnSSNCard;

    @Expose
    private Boolean socialSecurityCardHolderIndicator= null; // UI doesn't want anything preselected until user picks.
    @Expose
    private Boolean useSelfAttestedDeathIndicator=false;
    @Expose
    private String lastNameOnSSNCard;
    @Expose
    private String reasonableExplanationForNoSSN;

    public String getMiddleNameOnSSNCard() {
        return middleNameOnSSNCard;
    }

    public void setMiddleNameOnSSNCard(String middleNameOnSSNCard) {
        this.middleNameOnSSNCard = middleNameOnSSNCard;
    }

    public Boolean getDeathManualVerificationIndicator() {
        return deathManualVerificationIndicator;
    }

    public void setDeathManualVerificationIndicator(Boolean deathManualVerificationIndicator) {
        this.deathManualVerificationIndicator = deathManualVerificationIndicator;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getSuffixOnSSNCard() {
        return suffixOnSSNCard;
    }

    public void setSuffixOnSSNCard(String suffixOnSSNCard) {
        this.suffixOnSSNCard = suffixOnSSNCard;
    }

    public Boolean getIndividualMandateExceptionIndicator() {
        return individualMandateExceptionIndicator;
    }

    public void setIndividualMandateExceptionIndicator(Boolean individualMandateExceptionIndicator) {
        this.individualMandateExceptionIndicator = individualMandateExceptionIndicator;
    }

    public Boolean getSsnManualVerificationIndicator() {
        return ssnManualVerificationIndicator;
    }

    public void setSsnManualVerificationIndicator(Boolean ssnManualVerificationIndicator) {
        this.ssnManualVerificationIndicator = ssnManualVerificationIndicator;
    }

    public Boolean getDeathIndicatorAsAttested() {
        return deathIndicatorAsAttested;
    }

    public void setDeathIndicatorAsAttested(Boolean deathIndicatorAsAttested) {
        this.deathIndicatorAsAttested = deathIndicatorAsAttested;
    }

    public Boolean getNameSameOnSSNCardIndicator() {
        return nameSameOnSSNCardIndicator;
    }

    public void setNameSameOnSSNCardIndicator(Boolean nameSameOnSSNCardIndicator) {
        this.nameSameOnSSNCardIndicator = nameSameOnSSNCardIndicator;
    }

    public Boolean getDeathIndicator() {
        return deathIndicator;
    }

    public void setDeathIndicator(Boolean deathIndicator) {
        this.deathIndicator = deathIndicator;
    }

    public String getFirstNameOnSSNCard() {
        return firstNameOnSSNCard;
    }

    public void setFirstNameOnSSNCard(String firstNameOnSSNCard) {
        this.firstNameOnSSNCard = firstNameOnSSNCard;
    }

    public Boolean getSocialSecurityCardHolderIndicator() {
        return socialSecurityCardHolderIndicator;
    }

    public void setSocialSecurityCardHolderIndicator(Boolean socialSecurityCardHolderIndicator) {
        this.socialSecurityCardHolderIndicator = socialSecurityCardHolderIndicator;
    }

    public Boolean getUseSelfAttestedDeathIndicator() {
        return useSelfAttestedDeathIndicator;
    }

    public void setUseSelfAttestedDeathIndicator(Boolean useSelfAttestedDeathIndicator) {
        this.useSelfAttestedDeathIndicator = useSelfAttestedDeathIndicator;
    }

    public String getLastNameOnSSNCard() {
        return lastNameOnSSNCard;
    }

    public void setLastNameOnSSNCard(String lastNameOnSSNCard) {
        this.lastNameOnSSNCard = lastNameOnSSNCard;
    }

    public String getReasonableExplanationForNoSSN() {
        return reasonableExplanationForNoSSN;
    }

    public void setReasonableExplanationForNoSSN(String reasonableExplanationForNoSSN) {
        this.reasonableExplanationForNoSSN = reasonableExplanationForNoSSN;
    }

}
