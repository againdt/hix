package com.getinsured.hix.batch.enrollment.util;

import java.util.List;
import java.util.concurrent.Callable;

import com.getinsured.hix.batch.enrollment.service.EnrlReconSnapshotGenerationService;
import com.getinsured.hix.model.enrollment.EnrlReconSummary;

public class EnrlReconSnapshotGenJobThread implements Callable<Boolean> {

	private final EnrlReconSnapshotGenerationService enrlReconSnapshotGenerationService;
	private final List<Long> policyIds;
	private final Integer fileId;
	private final List<Long> failedPolicyIds;
	private final EnrlReconSummary summary;
	private final long jobExecutionId;
	public EnrlReconSnapshotGenJobThread(List<Long> policyIds, EnrlReconSnapshotGenerationService enrlReconSnapshotGenerationService, Integer fileId, List<Long> failedPolicyIds, EnrlReconSummary summary, long jobExecutionId){
		this.policyIds=policyIds;
		this.enrlReconSnapshotGenerationService=enrlReconSnapshotGenerationService;
		this.fileId=fileId;
		this.failedPolicyIds=failedPolicyIds;
		this.summary=summary;
		this.jobExecutionId=jobExecutionId;
	}
	
	@Override
	public Boolean call() throws Exception {
		enrlReconSnapshotGenerationService.populateEnrlReconSnapshot(policyIds, fileId, failedPolicyIds, summary, jobExecutionId);
		return true;
	}

}
