package com.getinsured.hix.indportal.dto;

import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;

public class MemberEligibilityStatusComparator implements Comparator<MemberEligibilityStatus>{

	@Override
	public int compare(MemberEligibilityStatus MemberEligibilityStatus1, MemberEligibilityStatus MemberEligibilityStatus2) {
		if(StringUtils.isNotBlank(MemberEligibilityStatus1.getEligibilityStatus()) && 
				StringUtils.isNotBlank(MemberEligibilityStatus2.getEligibilityStatus())){
			return MemberEligibilityStatus1.getEligibilityStatus().compareTo(MemberEligibilityStatus2.getEligibilityStatus());
		}
		
		return 0;
	}

}
