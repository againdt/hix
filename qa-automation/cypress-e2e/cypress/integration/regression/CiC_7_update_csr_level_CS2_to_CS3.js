
const inputHouseholds = ["regression/APTC_CSR2_2HH"]
before(function () {
    cy.runBeforeBlock()
})

inputHouseholds.forEach( file => {
    describe(`CIC7: AI/AN change (CS2 to CS3) for ${file} enrolled into Health/Dental`, function() {

        it (`Init household info with SSN, DOB with checks for ${file}`, function() {
            cy.ServerInfo = this.serverInfo
            cy.ServerConfig = this.serverConfig.body
            cy.ServerTime = this.serverTime.body
            cy.initHousehold(file)
        })
        it(`Creates Account Transfer for ${file}`, function () {
            cy.createAccountTransfer(cy.household)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Enrolls household into Medical and Dental - ${file}`, function () {
            cy.enrollIntoMedicalAndDental();
        })
        it(`Logout`, function () {
            cy.logout()
        })
        it(`Update csrLevel from CS2 to CS3 for ${file}`, function () {
            cy.csrChange("CS3")
          //  cy.updateAccountTransfer(cy.h)
            //cy.h.appStatus = 'enroll_med_dent_demo_update'
        })
        it(`AT update for ${file}`, function () {
            cy.updateAccountTransfer(cy.h)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
    })
})









