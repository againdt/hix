package com.getinsured.consumer.service;

import com.getinsured.consumer.exceptions.ConsumerServiceException;

public interface ConsumerDocumentService {
	String updateDocument(String documentDataJson) throws ConsumerServiceException;
	String handleException(Throwable exception);
}
