package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.ResponseMetadataType;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualResponseType;
import com.getinsured.iex.hub.ssac.extension.SSAResponseType;

public class SSACompositeIndividualResponseWrapper implements JSONAware{
	private ResponseMetadataWrapper responseMetadataWrapper;
	private SSAResponseWrapper ssaResponseWrapper;
	private String personSSN;

	public SSACompositeIndividualResponseWrapper(SSACompositeIndividualResponseType individualResponse){
		ResponseMetadataType metadata = individualResponse.getResponseMetadata();
		this.responseMetadataWrapper = new ResponseMetadataWrapper(metadata);
		this.personSSN = individualResponse.getPersonSSNIdentification().getValue();
		SSAResponseType v = individualResponse.getSSAResponse();
		this.ssaResponseWrapper = new SSAResponseWrapper(v);
	}
	
	public String getPersonSSN() {
		return personSSN;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject keyObj = new JSONObject();
		JSONObject obj = new JSONObject();
		//Metadata
		if(this.responseMetadataWrapper != null){
			obj.put("ResponseMetadata", responseMetadataWrapper);
		}
		obj.put("PersonSSNIdentification", this.personSSN);
		//Response from SSA
		obj.put("SSAResponse", this.ssaResponseWrapper);
		keyObj.put("SSACompositeIndividualResponse", obj);
		return keyObj.toJSONString();
	}
	

}
