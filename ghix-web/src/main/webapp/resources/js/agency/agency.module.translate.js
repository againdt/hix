(function() {
'use strict'; 

	angular.module('translateApp', ['pascalprecht.translate'])
	.config(['$translateProvider', function ($translateProvider) {

          var json = null;

          $translateProvider.preferredLanguage('en');
          $translateProvider.useSanitizeValueStrategy('escapeParameters');
          if($("#lang").val() === 'en'){
            $.ajax({
                'async': false,
                'url': "/hix/resources/i18n/json/agencyportal_en.json",
                complete: function (data) {
                    json = JSON.parse(data.responseText);
                    $translateProvider.translations('en',json);
                    $translateProvider.preferredLanguage('en');
                }
            });
          }
          if($("#lang").val() === 'es'){
            $.ajax({
                'async': false,
                'url': "/hix/resources/i18n/json/agencyportal_es.json",
                complete: function (data) {
                    json = JSON.parse(data.responseText);
                    $translateProvider.translations('es',json);
                    $translateProvider.preferredLanguage('es');
                }
            });
          }

      }]);
})();