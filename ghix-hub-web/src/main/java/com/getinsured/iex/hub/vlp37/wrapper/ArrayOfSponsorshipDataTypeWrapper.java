package com.getinsured.iex.hub.vlp37.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpsda.ArrayOfSponsorshipDataType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpsda.SponsorshipDataType;

/**
 * Wrapper Class for ArrayOfSponsorshipDataType
 * 
 * @author Nikhil Talreja
 * @since  14-Feb-2014 
 * 
 */
public class ArrayOfSponsorshipDataTypeWrapper implements JSONAware{
	
	private List<SponsorshipDataTypeWrapper> sponsorshipData;
	
	private static final String SPONSORSHIP_DATE_KEY = "SponsorshipData";
	
	public ArrayOfSponsorshipDataTypeWrapper(ArrayOfSponsorshipDataType arrayOfSponsorshipDataType){
		
		if(arrayOfSponsorshipDataType == null){
			return;
		}
		
		for(SponsorshipDataType sponsorshipDataType : arrayOfSponsorshipDataType.getSponsorshipData()){
			this.getSponsorshipData().add(new SponsorshipDataTypeWrapper(sponsorshipDataType));
		}
	}

	public List<SponsorshipDataTypeWrapper> getSponsorshipData() {
		
		if(this.sponsorshipData == null){
			this.sponsorshipData = new ArrayList<SponsorshipDataTypeWrapper>();
		}
		
		return sponsorshipData;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONArray sponsorshipDataArray = new JSONArray();
		if(this.sponsorshipData != null){
			for(SponsorshipDataTypeWrapper data : this.sponsorshipData){
				if(data != null){
					sponsorshipDataArray.add(data);
				}
			}
		}
		obj.put(SPONSORSHIP_DATE_KEY,sponsorshipDataArray);
		return obj.toJSONString();
	}
}
