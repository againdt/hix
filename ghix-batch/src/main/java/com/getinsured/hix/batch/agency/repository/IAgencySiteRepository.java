package com.getinsured.hix.batch.agency.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.agency.AgencySite;

public interface IAgencySiteRepository extends JpaRepository<AgencySite, Integer> {

	AgencySite findById(Long id);
	
	@Query("select agencySite from AgencySite agencySite where agencySite.agency.id = :agencyId")
	List<AgencySite> findByAgencyId(@Param("agencyId") Long agencyId);

}
