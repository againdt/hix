package com.getinsured.eligibility.at.ref.common;

import java.util.Date;
import java.util.List;

import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * @author chopra_s
 * 
 */
public class ReferralBatchDisenrollDTO {
	private long currentApplicationId;
	private long enrolledApplicationId;
	private Date terminationDate;
	private SsapApplication currentApplication;
	private SsapApplication enrolledApplication;
	private List<String> healthEnrollees;
	private long healthEnrollmentId;
	private boolean healthEnrolled;
	private boolean healthTerminationFuture;
	private List<String> dentalEnrollees;
	private long dentalEnrollmentId;
	private boolean dentalEnrolled;
	private boolean dentalTerminationFuture;
	private String terminationDateStr;
	
	private boolean isHealthDisEnrolled;
	private boolean isDentalDisEnrolled;
	private boolean triggerNotification;

	public String getTerminationDateStr() {
		return terminationDateStr;
	}

	public void setTerminationDateStr(String terminationDateStr) {
		this.terminationDateStr = terminationDateStr;
	}

	public List<String> getHealthEnrollees() {
		return healthEnrollees;
	}

	public void setHealthEnrollees(List<String> healthEnrollees) {
		this.healthEnrollees = healthEnrollees;
	}

	public long getHealthEnrollmentId() {
		return healthEnrollmentId;
	}

	public void setHealthEnrollmentId(long healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}

	public List<String> getDentalEnrollees() {
		return dentalEnrollees;
	}

	public void setDentalEnrollees(List<String> dentalEnrollees) {
		this.dentalEnrollees = dentalEnrollees;
	}

	public long getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}

	public void setDentalEnrollmentId(long dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}

	public long getCurrentApplicationId() {
		return currentApplicationId;
	}

	public void setCurrentApplicationId(long currentApplicationId) {
		this.currentApplicationId = currentApplicationId;
	}

	public long getEnrolledApplicationId() {
		return enrolledApplicationId;
	}

	public void setEnrolledApplicationId(long enrolledApplicationId) {
		this.enrolledApplicationId = enrolledApplicationId;
	}

	public SsapApplication getCurrentApplication() {
		return currentApplication;
	}

	public void setCurrentApplication(SsapApplication currentApplication) {
		this.currentApplication = currentApplication;
	}

	public SsapApplication getEnrolledApplication() {
		return enrolledApplication;
	}

	public void setEnrolledApplication(SsapApplication enrolledApplication) {
		this.enrolledApplication = enrolledApplication;
	}

	public boolean isHealthEnrolled() {
		return healthEnrolled;
	}

	public void setHealthEnrolled(boolean healthEnrolled) {
		this.healthEnrolled = healthEnrolled;
	}

	public boolean isHealthTerminationFuture() {
		return healthTerminationFuture;
	}

	public void setHealthTerminationFuture(boolean healthTerminationFuture) {
		this.healthTerminationFuture = healthTerminationFuture;
	}

	public boolean isDentalEnrolled() {
		return dentalEnrolled;
	}

	public void setDentalEnrolled(boolean dentalEnrolled) {
		this.dentalEnrolled = dentalEnrolled;
	}

	public boolean isDentalTerminationFuture() {
		return dentalTerminationFuture;
	}

	public void setDentalTerminationFuture(boolean dentalTerminationFuture) {
		this.dentalTerminationFuture = dentalTerminationFuture;
	}

	public Date getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}

	public boolean isHealthDisEnrolled() {
		return isHealthDisEnrolled;
	}

	public void setHealthDisEnrolled(boolean isHealthDisEnrolled) {
		this.isHealthDisEnrolled = isHealthDisEnrolled;
	}

	public boolean isDentalDisEnrolled() {
		return isDentalDisEnrolled;
	}

	public void setDentalDisEnrolled(boolean isDentalDisEnrolled) {
		this.isDentalDisEnrolled = isDentalDisEnrolled;
	}

	public boolean isTriggerNotification() {
		return triggerNotification;
	}

	public void setTriggerNotification(boolean triggerNotification) {
		this.triggerNotification = triggerNotification;
	}

	@Override
	public String toString() {
		return "ReferralBatchDisenrollDTO [currentApplicationId=" + currentApplicationId + ", enrolledApplicationId=" + enrolledApplicationId + ", healthEnrollmentId=" + healthEnrollmentId + ", dentalEnrollmentId=" + dentalEnrollmentId
		        + ", terminationDateStr=" + terminationDateStr + ", isHealthDisEnrolled=" + isHealthDisEnrolled + ", isDentalDisEnrolled=" + isDentalDisEnrolled + ", triggerNotification=" + triggerNotification + "]";
	}

}
