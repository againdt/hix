package com.getinsured.hix.ridp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ConsumerDocument;

public interface ConsumerDocumentRepository extends JpaRepository<ConsumerDocument, Long> {
	
	@Query("select doc from ConsumerDocument doc where targetId = :householdId and targetName = 'HOUSEHOLD'")
	List<ConsumerDocument> getDocumentsForHouseholdId(@Param("householdId") Long householdId);
	
}
