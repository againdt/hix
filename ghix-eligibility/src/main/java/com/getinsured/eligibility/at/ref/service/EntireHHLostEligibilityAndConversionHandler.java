package com.getinsured.eligibility.at.ref.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

@Component("entireHHLostEligibilityAndConversion")
@Scope("singleton")
public class EntireHHLostEligibilityAndConversionHandler extends LceProcessHandlerBaseService {

	String ERROR_MOVE_ENROLLMENT = "An error occurred while invoking Move Enrollment API for Application Id - ";
	String ERROR_UPDATE_APTC_AMOUNT = "An error occurred while updating APTC amount api for Application Id -";

	private static final Logger LOGGER = Logger.getLogger(EntireHHLostEligibilityAndConversionHandler.class);
	
	private static String LOST_ELIGIBILITY = "LOST_ELIGIBILITY";
	private static final String APP_EVENT_TYPE = "APPEVENT_APPLICATION";
	

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	private FinancialApplicationConversionService financialApplicationConversionService;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;


	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private AppEventService appEventService;
	
	/**
	 * When some applicant has non Qhp indicator create application and applicant events by taking least event date as enrollment start date +70 days as end date modify json and other data for financail to non finacial conv. update current application to SEP-ER.
	 * 
	 * @param enrolledApplicantsGuid
	 * @param lceProcessRequestDTO 
	 */

	public void executeSomeApplicantHasNonQhpIndicator(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap, List<String> enrolledApplicantsGuid, LCEProcessRequestDTO lceProcessRequestDTO) {
		ssapApplicationEventService.createDeniedHouseholdApplicationEvent(currentApplication, applicantEventMap);
		Date coverageStartDate = updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		getValidationStatus(currentApplication.getCaseNumber());
		
		//HIX-108047
		if(   currentApplication.getId() != 0  ) {
			currentApplication = loadCurrentApplication(currentApplication.getId());
		}
		
		
		financialApplicationConversionService.convertFinancialAppToNonFinancial(currentApplication, enrolledApplication, applicantEventMap);
		if (!checkAnyEnrolledMemberIsRemoved(currentApplication, enrolledApplicantsGuid)) {
			if (checkIfEntireHouseholdElgLost(currentApplication)) {
				final String response = executeDisenrollment(enrolledApplication, applicantEventMap);
				handleDisenrollmentResponse(currentApplication, enrolledApplication, response);
			} else {
				updateCurrentApplication(currentApplication, enrolledApplication, applicantEventMap, lceProcessRequestDTO, coverageStartDate);
			}
		} else {

			updateCurrentApplication(currentApplication, enrolledApplication, applicantEventMap, lceProcessRequestDTO, coverageStartDate);
		}

	}

	/**
	 * When no applicant has non Qhp indicator create application and applicant events by taking today's date as enrollment start date +70 days as end date modify json and other data for financail to non finacial conv. Check enrolled plan has cs plan benefits and handle.
	 * 
	 * @param enrolledApplicantsGuid
	 * @param lceProcessRequestDTO 
	 * 
	 */

	public void executeNoApplicantHasNonQhpIndicator(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap, List<String> enrolledApplicantsGuid, String enrollmentCostSharing, LCEProcessRequestDTO lceProcessRequestDTO) {
		ssapApplicationEventService.createDeniedHouseholdApplicationEvent(currentApplication, applicantEventMap);
		Date coverageStartDate = updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		getValidationStatus(currentApplication.getCaseNumber());
		
		//HIX-108047
		if(   currentApplication.getId() != 0  ) {
			currentApplication = loadCurrentApplication(currentApplication.getId());
		}
		
		financialApplicationConversionService.convertFinancialAppToNonFinancial(currentApplication, enrolledApplication, applicantEventMap);

		if (!checkAnyEnrolledMemberIsRemoved(currentApplication, enrolledApplicantsGuid)) {
			if (checkIfEntireHouseholdElgLost(currentApplication)) {
				final String response = executeDisenrollment(enrolledApplication, applicantEventMap);
				handleDisenrollmentResponse(currentApplication, enrolledApplication, response);
			} else {
				if (checkEnrollmentHasNoCSPlanBenefits(enrollmentCostSharing, currentApplication)) {
					handleEnrolledAppIsNotCSEligible(currentApplication, enrolledApplication, applicantEventMap,coverageStartDate); /* automate */
				} else {
					updateCurrentApplication(currentApplication, enrolledApplication, applicantEventMap, lceProcessRequestDTO,coverageStartDate); /* manual */
				}
			}
		} else {
			updateCurrentApplication(currentApplication, enrolledApplication, applicantEventMap, lceProcessRequestDTO, coverageStartDate);
		}
    }
	
	public SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}

	private boolean checkAnyEnrolledMemberIsRemoved(SsapApplication currentApplication, List<String> enrolledApplicantsGuid) {

		final List<String> applicantGuidsOnCurrentApplication = fetchApplicantGuidsForApplicantsOnCurrentApplication(currentApplication.getSsapApplicants());

		// check each enrolled member is present in applicantGuidsOnCurrentApplication, if not then false
		boolean enrolledMemberRemoved = false;
		for (String applicantGuid : enrolledApplicantsGuid) {
			if (!applicantGuidsOnCurrentApplication.contains(applicantGuid)) {
				return true;
			}
		}

		return enrolledMemberRemoved;
	}

	private void updateCurrentApplication(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap, LCEProcessRequestDTO lceProcessRequestDTO, Date coverageStartDate) {

		String status = StringUtils.EMPTY;
		status = invokeApitoUpdateAPTCAmount(currentApplication, enrolledApplication, applicantEventMap, coverageStartDate);
		if (GhixConstants.RESPONSE_SUCCESS.equals(status)) {
			currentApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
			updateAllowEnrollment(currentApplication, Y);
			//triggerNonFinConversionNoticeWithAptcCsrIneligibile(currentApplication.getCaseNumber()); /* handle in post process */
		}
	}

	private void handleErrorScenario(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap) {
		LOGGER.info("Error while processing entire household lost elgibility for " +currentApplication.getId());
		currentApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
		updateAllowEnrollment(currentApplication, Y);
		triggerNonFinConversionNoticeWithAptcCsrIneligibile(currentApplication.getCaseNumber());
	}

	private void handleEnrolledAppIsNotCSEligible(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap,Date coverageStartDate) {

		LOGGER.info("Enrolled App not having CS benefits " + currentApplication.getId());
		String status = StringUtils.EMPTY;
		status = invokeApitoUpdateAPTCAmount(currentApplication, enrolledApplication, applicantEventMap,coverageStartDate);
		if (GhixConstants.RESPONSE_SUCCESS.equals(status)) {

			status = moveEnrollmentToNewApplication(currentApplication, enrolledApplication);
			if (GhixConstants.RESPONSE_SUCCESS.equals(status)) {
				closeSsapApplication(enrolledApplication);
				currentApplication.setAllowEnrollment(Y);
				updateCurrentAppToEN(currentApplication);
				logEligibilityLostEvent(currentApplication,enrolledApplication);
				ssapApplicationEventService.setChangePlan(currentApplication.getId());
				triggerNonFinConversionNoticeWithAptcIneligibile(currentApplication.getCaseNumber());
			} else {
				handleErrorScenario(currentApplication, enrolledApplication, applicantEventMap);
			}

		}

	}

	private String invokeApitoUpdateAPTCAmount(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap, Date coverageStartDate) {
		String status = GhixConstants.RESPONSE_FAILURE;
		long enrolledApplicationId = enrolledApplication.getId();
		AptcUpdate aptcUpdate = populateAptcUpdateRequest(currentApplication, enrolledApplicationId, coverageStartDate);
		if (aptcUpdate == null) {
			handleErrorScenario(currentApplication, enrolledApplication, applicantEventMap);
			return status;
		}
		aptcUpdate.setIsNonFinancialConversion(true);
		status = executeAptcChangesApi(currentApplication, enrolledApplicationId, aptcUpdate);
		if (GhixConstants.RESPONSE_SUCCESS.equals(status)) {
			status = GhixConstants.RESPONSE_SUCCESS;
		} else {
			handleErrorScenario(currentApplication, enrolledApplication, applicantEventMap);
		}
		return status;
	}

	/**
	 * Have to check the cost sharing info from plan table.
	 * 
	 * @param currentApplication
	 * 
	 *            Cost sharing should be equal to CS1 and CS3(for native american)
	 * 
	 */
	private boolean checkEnrollmentHasNoCSPlanBenefits(String enrollmentCostSharing, SsapApplication currentApplication) {

		boolean noCsPlanBenefits = false;
		if (StringUtils.equalsIgnoreCase(ReferralConstants.CS1, enrollmentCostSharing)) {
			noCsPlanBenefits = true;
		} else if (StringUtils.equalsIgnoreCase(ReferralConstants.CS3, enrollmentCostSharing) && StringUtils.equalsIgnoreCase(ReferralConstants.CS3, currentApplication.getCsrLevel())) { // for native
			// american
			noCsPlanBenefits = true;
		}
		return noCsPlanBenefits;
	}

	private boolean checkIfEntireHouseholdElgLost(SsapApplication currentApplication) {
		boolean noneIsEligible = true;

		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (StringUtils.equalsIgnoreCase(ReferralConstants.QHP, ssapApplicant.getEligibilityStatus())) {
				noneIsEligible = false;
				break;
			}
		}

		return noneIsEligible;
	}

	private void logEligibilityLostEvent(SsapApplication currentApplication, SsapApplication enrolledApplication){
		EventInfoDto eventInfoDto = new EventInfoDto();
		Map<String, String> mapEventParam = new HashMap<String, String>();
		
		//event type and category
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APP_EVENT_TYPE,LOST_ELIGIBILITY);
		eventInfoDto.setEventLookupValue(lookupValue);
		mapEventParam.put("Current APTC", "NA");
		mapEventParam.put("Previous APTC", String.valueOf(enrolledApplication.getMaximumAPTC()));
		eventInfoDto.setModuleId(currentApplication.getCmrHouseoldId().intValue());
		eventInfoDto.setModuleName(GhixRole.INDIVIDUAL.toString());
		appEventService.record(eventInfoDto, mapEventParam);
	}
}
