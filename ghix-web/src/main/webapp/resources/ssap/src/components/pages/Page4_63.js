import React, { Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

// Components

import InstructionalText from '../notifications/InstructionalText';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData, clearMethods, fetchMemberAffordability, setMethods, showModal } from '../../actions/actionCreators';

// Utils

import cloneObj from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import CONFIG from '../../.env';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import EMPLOYER_DETAILS from '../../constants/employerSponsoredCoverage';
import ENDPOINTS from '../../constants/endPointsList';
import INPUT_TYPES from '../../constants/inputTypes';
import MODAL_TYPES from '../../constants/modalTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';




/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Enrolled In Other Health Insurance
 *
 */
const Page4_63 = props => {

    const {
        affordability: {
            percent
        },
        data,
        data: {
            singleStreamlinedApplication: {
                coverageYear
            }
        },
        form: {
            Form: {
                isFormSubmitted
            }
        },
        oepDates,
        householdMembers,
        buttonContainer: ButtonContainer,
        hhmIndex : member,
        familyStatus : {
            primaryTaxFilerHouseholdMembers
        },
        currentHhm,
        page
    } = props;

    const {
        name,
        healthCoverage : {
            employerWillOfferInsuranceIndicator
        }
    } = currentHhm;

    const currentDate = moment().format('YYYY-MM-DD');
    const oepEndDate = moment(oepDates.oepEndDate).format('YYYY-MM-DD');
    const oepStartDate = moment(oepDates.oepStartDate).format('YYYY-MM-DD');

    const displayPeriod = currentDate >= oepStartDate && currentDate <= oepEndDate ? `January 1st, ${coverageYear}` : moment().format('YYYY-MM-01');

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            props.actions.fetchMemberAffordability(`${window.location.origin}${ENDPOINTS.MEMBER_AFFORDABILITY}${coverageYear}` );
        }
    }, []);

    const [localState, setLocalState] = useState({employerWillOfferInsuranceIndicator: false});

    const {localEmployerWillOfferInsuranceIndicator} = localState;

    useEffect(() => {
        employerWillOfferInsuranceIndicator && checkboxState.length === 0 &&
        props.actions.showModal(
            MODAL_TYPES.INFORM_TO_ADD_INCOME_SOURCE_MODAL,
            MODAL_TYPES.INFORM_TO_ADD_INCOME_SOURCE_MODAL,
            [
                householdMembers,
                member,
                () => props.actions.changeData
            ]
        );
    }, [employerWillOfferInsuranceIndicator]);

    const onFieldChange = newData => {
        setLocalState(state => {
            return {
                ...state,
                employerWillOfferInsuranceIndicator: localEmployerWillOfferInsuranceIndicator
            };
        });
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = {...newData};
        props.actions.changeData(newState);
    };

    const populateMemberHealthCoverageDetails = () => {
        let collectUniqueMemberEmployer = [];
        let prepareMemberEmployerPair = [];
        let employerCoverageDetail = [];
        const uniqueEmployer = new Set();
        householdMembers.map(member => {
            const { incomes, name } = member;
            employerCoverageDetail = incomes.filter(income => income.type === 'JOB' || income.type === 'UNEMPLOYMENT');
            employerCoverageDetail.map(sn => uniqueEmployer.add(sn.sourceName));
            collectUniqueMemberEmployer = [
                ...collectUniqueMemberEmployer,
                {
                    name: normalizeNameOfPerson(name),
                    personId: member.personId,
                    sourceName: [...uniqueEmployer]
                }
            ];
            uniqueEmployer.clear();
        });

        const getButtonState = (emp, personId) => {
            return (result => result ? result : false)(householdMembers[member].employerSponsoredCoverage
                .find(e => e.companyName === emp && e.personId === personId));
        };

        collectUniqueMemberEmployer.map(usn => {
            const { sourceName, personId } = usn;
            sourceName.map(emp => {
                prepareMemberEmployerPair = [
                    ...prepareMemberEmployerPair,
                    {
                        memberName: usn.name,
                        personId: personId,
                        companyName: emp,
                        value: false,
                        btnState: getButtonState(emp, personId)
                    }
                ];
            });

        });
        return prepareMemberEmployerPair;
    };
    const [checkboxState, setCheckboxState] = useState(populateMemberHealthCoverageDetails());
    const onFieldChange_Checkbox = (...args) => {
        const [newData, index, , [, value]] = args;

        setCheckboxState(state => state.map((cb, idx) => idx === index ? { ...cb, btnState: value } : cb));

        const newReduxState = {...data};
        const entryPoint = newReduxState.singleStreamlinedApplication.taxHousehold[0].householdMember[member].employerSponsoredCoverage;

        value ? entryPoint.push({
            ...cloneObj(EMPLOYER_DETAILS),
            companyName: newData.companyName,
            memberName: newData.memberName,
            personId: newData.personId,
        }) : entryPoint.splice(entryPoint.findIndex(emp => emp.companyName === newData.companyName && emp.memberName === newData.memberName),1);
        props.actions.changeData(newReduxState);

    };
    const checkAffordability = (income, premiumAmt, frequency) => {
        // premium amount per month
        switch (frequency) {
        case 'YEARLY' :
            premiumAmt = premiumAmt / 12;
            break;
        case 'QUARTERLY':
            premiumAmt = premiumAmt * 3 / 12;
            break;
        case 'WEEKLY':
            premiumAmt = premiumAmt * 52 / 12;
            break;
        case 'BIWEEKLY':
            premiumAmt = premiumAmt * 26 / 12;
            break;
        case 'BIMONTHLY':
            premiumAmt = premiumAmt * 24 / 12;
            break;
        default:
            premiumAmt;
            break;
        }
        return (income * percent / 100) > premiumAmt;
    };
    const calculateMonthlyIncome = (members) => {

        return members
            .map(member => member.annualTaxIncome.projectedIncome > 0 ? member.annualTaxIncome.projectedIncome : member.annualTaxIncome.reportedIncome)
            .reduce((annualHouseholdIncome, annualIncome) => annualHouseholdIncome + annualIncome, 0);
    };

    const onFieldChange_EmployerCoverageInfo = (newData, index) => {
        const newState = {...data };

        const {
            healthCoverage: {
                employerWillOfferInsuranceIndicator
            }
        } = householdMembers[member];
        const entryPoint = newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member].employerSponsoredCoverage;
        const annualHouseholdIncome = calculateMonthlyIncome(primaryTaxFilerHouseholdMembers);
        const monthlyIncome = annualHouseholdIncome/12;
        const isEmployerOfferAffordable = newData.employerPremium > 0 ? checkAffordability (monthlyIncome, newData.employerPremium, newData.employerPremiumFrequency) : false;
        entryPoint[index] = {...newData};
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member].employerSponsoredCoverage[index].employerOfferAffordable = isEmployerOfferAffordable;
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member].hasESI = entryPoint.length > 0 ?
            entryPoint.some(emp => emp.minimumValuePlan && emp.employerOfferAffordable && employerWillOfferInsuranceIndicator) : false;
        props.actions.changeData(newState);
    };

    useEffect(() => {
    }, [checkboxState.map(cb => cb.value)]);

    const handleAddEmployerDetailsClick = (...args) => {
        const [ householdMembers, member, chbox, index ] = args;
        const employerEmployeeCoverageDetails = householdMembers[member].employerSponsoredCoverage.find(emp => emp.companyName === chbox.companyName && emp.memberName === chbox.memberName);
        props.actions.showModal(
            MODAL_TYPES.ADD_EMPLOYER_COVERAGE_DETAIL_MODAL,
            undefined,
            [
                employerEmployeeCoverageDetails,
                index,
                'employerSponsoredCoverage',
                page
            ]
        );
    };

    useEffect(() => {
        props.actions.setMethods('Page4_63',
            {
                handleAddEmployerDetailsClick
            });

        return () => {
            props.actions.clearMethods('Page4_63');
        };
    }, []);

    const getContent = (memberInfo, member) => {

        return (
            checkboxState.map((chbox, index) => {
                const {
                    memberName,
                    companyName,
                    value,
                    btnState
                } = chbox;
                const findIndex = memberInfo.employerSponsoredCoverage.findIndex(emp => emp.companyName === chbox.companyName && emp.memberName === chbox.memberName);
                const {
                    phone,
                    minimumValuePlan,
                    employerPremium,
                    employerPremiumFrequency
                } = ~findIndex  ?
                    memberInfo.employerSponsoredCoverage[findIndex] : {};
                return (
                    <li className='list__with__btn' key={`${memberName}${companyName}`}>
                        <UniversalInput
                            label={{
                                text: <span><span className='li__box'>{companyName}</span><span className='li__box'>{memberName}</span></span>

                            }}
                            inputData={{
                                isInputRequired: false,
                                classes: ['layout--inline margin-t-20'],
                                type: INPUT_TYPES.CHECKBOX,
                                name: 'value',
                                checked: btnState,
                                value: value,
                                currentData: chbox,
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange_Checkbox
                            }}
                            indexInArray = {index}
                        />
                        { btnState && !!~findIndex &&
                        <Fragment>
                            <Fragment>
                                <ButtonContainer
                                    type="addEmployerCoverageDetailButton"
                                    args={[householdMembers, member, chbox, findIndex]}
                                />
                                <InstructionalText
                                    Component={
                                        <span data-instructions="coverage_add_employer"/>
                                    }
                                />
                            </Fragment>

                            <div className='usa-width-one-whole bordered-b-box'>
                                <UniversalInput
                                    label={{
                                        text: 'Phone',
                                        labelClasses: []
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.PHONE_NUMBER_INPUT,
                                        name: 'phone',
                                        helperClasses: [],
                                        value: phone,
                                        placeholder: '(xxx) xxx-xxxx',
                                        validationType: VALIDATION_TYPES.PHONE_NUMBER,
                                        currentData: memberInfo.employerSponsoredCoverage[findIndex],
                                        fields: []
                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange_EmployerCoverageInfo
                                    }}
                                    indexInArray = {findIndex}
                                />

                                <UniversalInput
                                    legend={{
                                        legendText: `Does ${companyName} offer a health plan that meets the minimum value standard?`,
                                        dataInstructions: 'coverage_minimum_standard'
                                    }}
                                    label={{
                                        text: '',
                                        labelClass: ['usa-width-one-whole', 'layout--02'],
                                        ulClass: ['usa-width-one-whole', 'margin-b-10'],
                                        showRequiredIcon: true
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.RADIO_BUTTONS,
                                        name: 'minimumValuePlan',
                                        value: minimumValuePlan,
                                        options: DROPDOWN_OPTIONS.YES_NO,
                                        validationType: VALIDATION_TYPES.CHOSEN,
                                        currentData: memberInfo.employerSponsoredCoverage[findIndex],
                                        fields: []
                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange_EmployerCoverageInfo
                                    }}
                                    indexInArray = {findIndex}
                                    errorClasses={['margin-l-20-ve']}
                                />

                                { minimumValuePlan &&
                                <Fragment>
                                    <div>What is the premium amount for the lowest cost plan available to <strong>{memberName}</strong> that meets the minimum value standard?</div>
                                    <UniversalInput
                                        label={{
                                            text: 'Total amount',
                                            labelClasses: ['usa-width-one-third']
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.MONEY_INPUT,
                                            name: 'employerPremium',
                                            value: employerPremium,
                                            isOnlyPositiveAvailable: true,
                                            inputClasses: ['usa-width-two-thirds'],
                                            validationType: VALIDATION_TYPES.MONEY,
                                            placeholder: 'Amount',
                                            validationErrorMessage: ['Please enter a dollar value'],
                                            currentData: memberInfo.employerSponsoredCoverage[findIndex],
                                            fields: []
                                        }}
                                        inputActions={{
                                            onFieldChange: onFieldChange_EmployerCoverageInfo
                                        }}
                                        indexInArray = {findIndex}
                                    />
                                    <UniversalInput
                                        label={{
                                            text: 'How often?',
                                            isLabelRequired: false
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.DROPDOWN,
                                            name: 'employerPremiumFrequency',
                                            value: employerPremiumFrequency,
                                            options: DROPDOWN_OPTIONS.FREQUENCY,
                                            validationType: VALIDATION_TYPES.SPECIFIED,
                                            currentData: memberInfo.employerSponsoredCoverage[findIndex],
                                            fields: []

                                        }}
                                        indexInArray = {findIndex}
                                        inputActions={{
                                            onFieldChange: onFieldChange_EmployerCoverageInfo
                                        }}
                                    />
                                </Fragment>
                                }

                            </div>
                        </Fragment>
                        }
                    </li>
                );
            })
        );
    };
    const checkTopLevelValidation = () => {
        const {
            employerWillOfferInsuranceIndicator
        } = householdMembers[member].healthCoverage;
        return !employerWillOfferInsuranceIndicator ? true  : checkboxState.some(emp => emp.btnState);
    };
    return (
        <Fragment>
            <Form topLevelValidity={checkTopLevelValidation()}>

                <div id="aiEmployerCoverageDetail" className="fade-in">
                    <div className="subsection">
                        <ValidationErrorMessage
                            isVisible={isFormSubmitted && !checkTopLevelValidation()}
                            messageText='Please select the employer that will offer health coverage'
                            errorClasses={['margin-l-20-ve']}
                        />
                        <UniversalInput
                            legend={{
                                legendText:
                                    <span>
                                        Will&nbsp;
                                        <strong>{normalizeNameOfPerson(name)}</strong>&nbsp;
                                        be offered health coverage through a job (including another person&apos;s job,
                                        like a spouse or parent)? Tell us about coverage offers that apply to&nbsp;
                                        <strong>{normalizeNameOfPerson(name)}</strong> starting {displayPeriod}.
                                    </span>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'employerWillOfferInsuranceIndicator',
                                value: employerWillOfferInsuranceIndicator,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: householdMembers[member],
                                fields: ['healthCoverage']

                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                            errorClasses={['margin-l-20-ve']}
                        />
                        {
                            employerWillOfferInsuranceIndicator && checkboxState.length > 0 &&
                            <Fragment>
                                <div className="usa-width-one-whole">
                                    <fieldset className="usa-fieldset-inputs usa-sans">
                                        <legend className="gi-checkbox  usa-width-one-whole margin-tb-10">
                                            <strong>
                                                <span className="li__box">Employer Name</span>
                                                <span className="li__box margin-l-20">Employee Name</span>
                                            </strong>
                                        </legend>
                                        <ul className="usa-unstyled-list">
                                            {
                                                getContent(householdMembers[member], member)
                                            }
                                        </ul>
                                    </fieldset>
                                </div>
                            </Fragment>
                        }
                    </div>
                </div>
            </Form>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    affordability: state.affordability,
    data: state.data,
    form: state.form,
    oepDates: state.headerLinks,
    household: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0],

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        clearMethods,
        fetchMemberAffordability,
        setMethods,
        showModal
    }, dispatch)
});

Page4_63.defaultProps = {
    name: 'Page4_63'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page4_63);
