package com.getinsured.hix.admin.service;


import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;


/**
 * {@link AccountLockNotificationEmail} extends {@link NotificationAgent} 
 * overrides the getSingleData() method of NotificationAgent
 * with customized implementation for sending account lock notification and is
 * used to set Email details such as sender, receiver, unlock URL . 
 * 
 * @author Kenil Shah
 *
 */

@Component
public class AccountLockNotificationEmail extends NotificationAgent{

	@Value("#{configProp.exchangename}")
	private String exchangeName;
	
	public static final String UNLOCK_CONTROLLER_URL = "account/unlock/email";

	
	
	/**
	 * overrides the implementation of {@link NotificationAgent} agent 
	 * with customized implementation as required for Account Unlock
	 * @return {@link Map}
	 */
	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
		AccountUser userObj = (AccountUser) notificationContext.get("USER_OBJ");
		
		Map<String,String> bean = new HashMap<String, String>();
		
		String unlockUrl = new StringBuffer(GhixEndPoints.GHIXWEB_SERVICE_URL).append(UNLOCK_CONTROLLER_URL).toString();
		bean.put("name",userObj.getFirstName()+" "+userObj.getLastName() );
		bean.put("exchangeName", exchangeName);
		bean.put("id",Integer.toString(userObj.getId()));
		bean.put("unlockUrl", unlockUrl );
		bean.put("emailAddress", userObj.getEmail());
		return bean;
	}
	
	@Override
	public Map<String, String> getEmailData(
			Map<String, Object> notificationContext) {
		AccountUser userObj = (AccountUser) notificationContext.get("USER_OBJ");
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", userObj.getEmail());
		data.put("UserId", Integer.toString(userObj.getId()));
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if(!"NV".equalsIgnoreCase(stateCode)) {
			data.put("Subject", "Account Locked");
		}
		data.put("From", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		return data;
	}
	
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}
