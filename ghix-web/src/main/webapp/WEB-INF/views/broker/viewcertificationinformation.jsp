<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>
<%@ page import="java.util.HashMap" %>
<title>Getinsured Health Exchange: Agent Certification
	Application</title>
<script src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"
	type="text/javascript"></script>
<script src="<gi:cdnurl value="/resources/js/vimo.assister.js" />" type="text/javascript"></script>
<script src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"
	type="text/javascript"></script>
<script src="<gi:cdnurl value="/resources/js/bootstrap-typeahead.js" />"
	type="text/javascript"></script>
<link href="<gi:cdnurl value="/resources/css/assister.css" />" media="screen"
	rel="stylesheet" type="text/css" />
<link href="<gi:cdnurl value="/resources/css/accessibility.css" />" media="screen"
	rel="stylesheet" type="text/css" />
<%-- <link href="<gi:cdnurl value="/resources/css/autoSuggest.css" />" media="screen"
	rel="stylesheet" type="text/css" /> --%>
	<link href="<gi:cdnurl value="/resources/css/broker.css" />" media="screen" rel="stylesheet" type="text/css" />

<%-- <script src="<gi:cdnurl value="/resources/js/jquery.autoSuggest.js" />"
	type="text/javascript"></script> --%>
<script src="<gi:cdnurl value="/resources/js/jquery.autoSuggest.minified.js" />"
	type="text/javascript"></script>
<!--src="<c:url value="/resources/js/employer_location.js" />"  -->
<meta name="description" content="">
<meta name="author" content="">

<link href="<gi:cdnurl value="/resources/css/assister.css" />" media="screen"
	rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value="/resources/js/phoneUtils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/utility/dateUtils.js" />"></script>

<script type="text/javascript">
	function highlightThis(val) {

		var currUrl = window.location.href;
		var newUrl = "";
		/* Check if Query String parameter is set or not */
		if (currUrl.indexOf("?", 0) > 0) { /* If Yes */
			if (currUrl.indexOf("?lang=", 0) > 0) {/* Check if locale is already set without querystring param  */
				newUrl = "?lang=" + val;
			} else if (currUrl.indexOf("&lang=", 0) > 0) { /*  Check if locale is already set with querystring param  */
				newUrl = currUrl.substring(0, currUrl.length - 2) + val;
			} else {
				newUrl = currUrl + "&lang=" + val;
			}
		} else { /* If No  */
			newUrl = currUrl + "?lang=" + val;
		}
		window.location = newUrl;
	}


	$(document).ready(function() {
		$("#brkCertInfo").removeClass("link");
		$("#brkCertInfo").addClass("active");

		var brkId = ${broker.id};
		if (brkId != 0) {
			$('#certificationInfo').attr('class', 'active');
			$('#buildProfile').attr('class', 'done');
		} else {
			$('#certificationInfo').attr('class', 'visited');
			$('#buildProfile').attr('class', 'visited');
		};

		var paymentid = ${paymentId};
		if (paymentid != 0) {
			$('#paymentinfo').attr('class', 'done');
		} else {
			$('#paymentinfo').attr('class', 'visited');
		};
	});

$(document).ready(function(){
		formatAllPhoneNumbers(["sPrimaryPhoneNumber", "sBusinessContactPhoneNumber", "sAlternatePhoneNumber", "sFaxNumber"]);
		formatDates(["sLicenseRenewalDate"]);
	});
</script>

<div class="gutter10">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a id="aid_agentInfoNavBack" href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a id="aid_agentInfoNavUserName"
					href="<c:url value="/broker/viewcertificationinformation"/>"><spring:message code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
			</ul>
		</div>
	</div>
	<!--page-breadcrumb ends-->

	<div class="row-fluid">
		<c:choose>
			<c:when
				test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == 'Incomplete' || broker.certificationStatus == 'Pending' || broker.certificationStatus == null || broker.certificationStatus ==''}">
				<h1 class="aid_agentInfoHeaderUserName">${broker.user.firstName}&nbsp;${broker.user.lastName}</h1><!-- <spring:message  code="label.newAgentRegistration"/>  -->
			</c:when>
			<c:otherwise>
				<h1 class="aid_agentInfoHeaderUserName">${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
			</c:otherwise>
		</c:choose>

		<c:if test="${broker.certificationStatus == 'Pending'}">
		<!--	<font size="2%"><p class="alert alert-success offset3 span9"><spring:message  code="label.brkapplicationreceivedmessage"/></p></font> -->
		</c:if>
	</div>
	<div class="row-fluid broker-panel">
		<!-- #sidebar -->
	       <c:choose>
	       		<c:when test="${ (agencyCertificationStatus == 'PENDING' or agencyCertificationStatus == 'CERTIFIED' )  and isAgencyManager}">
	       			<jsp:include page="brokerLeftNavigationMenu.jsp" />
	       		</c:when>
				<c:when test="${isAgencyManager  && requestScope.newBrokerRegistration == null && requestScope.brokerId == null }">
					<jsp:include page="agencyLeftNavigationMenu.jsp" />
				</c:when>
				<c:otherwise>
	       <jsp:include page="brokerLeftNavigationMenu.jsp" />
				</c:otherwise>
			</c:choose>

	    <!-- #sidebar ENDS -->
		<!-- end of span3 -->

		<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9" id="rightpanel">
				<div class="header">
					<h4 id="aid_agentInfoHeader" class="pull-left"><spring:message  code="label.brkBrokerInformation"/></h4>
					<c:if test="${(broker.certificationStatus == 'Certified' || broker.certificationStatus == 'Incomplete') and (empty broker.agencyId)}">
						<%-- <a title="<spring:message code='label.brkEdit'/>" class="btn btn-small pull-right"  type="button"  href="<c:url value="/broker/certificationapplication?isCertified=true"/>"><spring:message code="label.brkEdit"/></a> --%>
						<button id="aid-editPage" title="<spring:message code='label.brkEdit'/>" class="btn btn-small pull-right" type="button"><spring:message code="label.brkEdit"/></button>
					</c:if>
					<c:if test="${ (isAgencyManager) and (broker.certificationStatus == 'Certified' or broker.certificationStatus == 'Incomplete' )    }">
						<jsp:useBean id="moduleParamsMap" class="java.util.HashMap"/>
						<c:set target="${moduleParamsMap}" property="brokerId" value="${broker.id}" />
						<surl:secureUrl paramMap="${moduleParamsMap}" var="ecyptedModuleParamsMap"/>
						<%-- <a title="<spring:message code='label.brkEdit'/>" class="btn btn-small pull-right"  type="button"  href="<c:url value="/broker/certificationapplication?isCertified=true&ref=${ecyptedModuleParamsMap}"/>"><spring:message code="label.brkEdit"/></a> --%>
						<button id="aid-editMap" title="<spring:message code='label.brkEdit'/>" class="btn btn-small pull-right" type="button"><spring:message code="label.brkEdit"/></button>
					</c:if>
				</div>

			<form class="form-horizontal" id="frmviewCertification" name="frmviewCertification" action="viewcertificationinformation" method="POST">
				<!-- <p class="gutter10"><spring:message  code="label.brkstaticnotification"/></p> -->
			<div class="row-wrapper">
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
					<table class="table table-border-none table-condensed table-res" role="presentation">
						<tbody>
							<c:if test="${broker.user.firstName != '' && broker.user.firstName != null}">
							<tr>
								<td id="aid_agentFirstNameLbl" class="txt-right"><spring:message  code="label.brkFirstName"/></td>
								<td id="aid_agentFirstName"><strong>${broker.user.firstName}</strong></td>
							</tr>
							</c:if>
							<c:if test="${broker.user.lastName != '' && broker.user.lastName != null}">
							<tr>
								<td id="aid_agentLastNameLbl" class="txt-right"><spring:message  code="label.brkLastName"/></td>
								<td id="aid_agentLastName"><strong>${broker.user.lastName}</strong></td>
							</tr>
							</c:if>

							<c:if test="${broker.licenseNumber != '' && broker.licenseNumber != null}">
							<tr>
								<td id="aid_agentLicenseNumberLbl" class="txt-right"><spring:message code="label.brkLicenseNumber"/></td>
								<td id="aid_agentLicenseNumber"><strong>${broker.licenseNumber}</strong></td>
							</tr>
							</c:if>
							
							<c:if test="${stateCode=='NV' &&  broker.npn != '' && broker.npn != null}">
							<tr>
								<td id="aid_agentNpnLbl" class="txt-right"><spring:message code="label.brkNpn"/></td>
								<td id="aid_agentNpn"><strong>${broker.npn}</strong></td>
							</tr>
							</c:if>

							<c:if test="${broker.licenseRenewalDate != '' && broker.licenseRenewalDate != null}">
							<tr>
								<td id="aid_agentRenewalDateLbl" class="txt-right"><spring:message code="label.brkLicenseRenewalDate"/></td>
								<td id="aid_agentRenewalDate"><strong id="sLicenseRenewalDate"><fmt:formatDate value="${broker.licenseRenewalDate}" pattern="MM/dd/yyyy"/> </strong></td>
							</tr>
							</c:if>

							<c:if test="${broker.personalEmailAddress != '' && broker.personalEmailAddress != null}">
								<tr>
									<td id="aid_agentPersonalEmailLbl" class="txt-right"><spring:message code="label.brkPersonalEmail"/></td>
									<td id="aid_agentPersonalEmail"><strong>${broker.personalEmailAddress}</strong></td>
								</tr>
							</c:if>

							<tr>
								<td id="aid_agentPrimaryPhoneNumberLbl" class="txt-right"><spring:message code="label.brkPrimaryPhoneNumber"/></td>
								<td><strong id="sPrimaryPhoneNumber">(${phone1}) ${phone2}-${phone3}</strong></td>
							</tr>

							<c:if test="${broker.businessContactPhoneNumber != '' && broker.businessContactPhoneNumber != null && fn:length(broker.businessContactPhoneNumber)==12}">
							<tr>
								<td id="aid_agentBusinessPhoneNumberLbl" class="txt-right"><spring:message code="label.brkBusinessContactPhoneNumber"/></td>
								<td id="aid_agentBusinessPhoneNumber"><strong id="sBusinessContactPhoneNumber">${broker.businessContactPhoneNumber}</strong></td>
							</tr>
							</c:if>

							<c:if test="${broker.alternatePhoneNumber != '' && broker.alternatePhoneNumber != null && fn:length(broker.alternatePhoneNumber)==12 }">
							<tr>
								<td id="aid_agentAlternatePhoneNumberLbl" class="txt-right"><spring:message code="label.brkAlternatePhoneNumber"/></td>
								<td id="aid_agentAlternatePhoneNumber"><strong id="sAlternatePhoneNumber">${broker.alternatePhoneNumber}</strong></td>
							</tr>
							</c:if>

							<c:if test="${broker.faxNumber != '' && broker.faxNumber != null  && fn:length(broker.faxNumber)==12 }">
							<tr>
								<td id="aid_agentFaxNumberLbl" class="txt-right"><spring:message code="label.brkFaxNumber"/></td>
								<td id="aid_agentFaxNumber"><strong id="sFaxNumber">${broker.faxNumber}</strong></td>
							</tr>
							</c:if>

							<c:if test="${broker.communicationPreference != '' && broker.communicationPreference != null}">
                            <tr>
								<td id="aid_agentPrefCommLbl" class="txt-right"><spring:message code="label.brkPreferredMethodofCommunication"/></td>
								<td id="aid_agentPrefComm"><strong>${broker.communicationPreference}</strong></td>
							</tr>
							</c:if>

							<c:if test="${allowMailNotice=='Y'}">
							<tr>
								<td class="txt-right vertical-align-top"><spring:message code="label.brk.postalMail"/></td>
									<c:choose>
										<c:when test="${broker.postalMailEnabled == 'Y'}">
											<td id="aid_agentpostalMailYes"><strong> <spring:message code="label.brk.yes"/> </strong> </td>

										</c:when>
										<c:otherwise>
											<td id="aid_agentpostalMailNo"><strong> <spring:message code="label.brk.no"/> </strong></td>
										</c:otherwise>
									</c:choose>

								</tr>

								</c:if>

							<c:if test="${broker.companyName != '' && broker.companyName != null}">
							<tr>
								<td id="aid_agentCompanyNameLbl" class="txt-right"><spring:message code="label.brkCompanyName"/></td>
								<td id="aid_agentCompanyName"><strong>${broker.companyName}</strong></td>
							</tr>
							</c:if>

							<c:if test="${broker.federalEIN != '' && broker.federalEIN != null}">
							<tr>
								<td id="aid_agentfederalEINLbl" class="txt-right"><spring:message code="label.brkfederalEIN"/></td>
								<td id="aid_agentfederalEIN"><strong>${broker.federalEIN}</strong></td>
							</tr>
							</c:if>

							<c:if test="${ ROLECHANGEALLOWED == 'TRUE' }">
								<tr>
									<td class="txt-right"><spring:message code="label.brkRole"/></td>
									<td>
										<c:choose>
											<c:when test="${CURRENTROLE == 'AGENCYMANAGER'}">
												<strong><spring:message code="label.brkAgencyManager"/></strong>
											</c:when>
											<c:otherwise>
												<strong><spring:message code="label.brkAgent"/></strong>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							 </c:if >


							<!-- 							<tr> -->
							<!-- 								<td class="txt-right">Doing Business As</td> -->
							<%-- 								<td><strong>${broker.doingBusinessAs}</strong></td> --%>
							<!-- 							</tr> -->


						</tbody>
					</table>
				</div>
			</div>
			<!-- end of .row-wrapper -->

			<div class="row-wrapper">
				<div class="">
					<h4 class="heading_border_b"><spring:message code="label.brkBusinessAddress"/></h4>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
					<table class="table table-border-none table-condensed table-res" role="presentation">
						<tbody>
							<tr>
								<td id="aid_agentBusinessAddress_1" class="txt-right vertical-align-top"><spring:message code="label.brkBusinessAddress"/></td>
								<td id="aid_agentBusinessAddress_2">
								<strong>${locationObj.address1}
									<c:if test="${!empty fn:trim(locationObj.address2) }">
									<br>
										${locationObj.address2}
									</c:if>
								</strong>
								</td>
							</tr>
							<tr>
								<td class="txt-right">&nbsp;</td>
								<td id="aid_agentBusinessAddress_3"><strong> <c:if test= "${!empty fn:trim(locationObj.city)}" >${locationObj.city},</c:if>
								${locationObj.state} ${locationObj.zip}
								</strong></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- end of .row-wrapper -->

			<div class="row-wrapper">
				<div class="">
					<h4 class="heading_border_b"><spring:message code="label.brkMailingAddress"/></h4>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
				<table class="table table-border-none table-condensed table-res" role="presentation">
						<tbody>
							<c:if  test="${ (not isAgencyManager) && (not isAdminStaffL2)}">
							<tr>
								<td class="txt-right col-xs-6"><spring:message code="label.brkSameAsBusinessAddress"/></td>
								<c:choose>
									<c:when test="${locationMatching == 'Y'}">
										<td id="aid_agentIcon_ok"><i class="icon-ok"></i></td>
									</c:when>
									<c:otherwise>
										<td id="aid_agentIcon_minus"><i class="icon-minus-sign"></i></td>
									</c:otherwise>
								</c:choose>
							</tr>
							</c:if>
							<tr>
								<td id="aid_agentMailingAddressCheck" class="txt-right vertical-align-top"><spring:message code="label.brkMailingAddressCheck"/></td>
								<td id="aid_agentMailingAddress">
								<strong>${mailingLocationObj.address1}
									<c:if test="${!empty fn:trim(mailingLocationObj.address2) }">
									<br>
										${mailingLocationObj.address2}
									</c:if>
								</strong>
								</td>
							</tr>


							<tr>
								<td class="txt-right">&nbsp;</td>
								<td id="aid_agentMailingLocation"><strong>
								${mailingLocationObj.city},
								${mailingLocationObj.state} ${mailingLocationObj.zip}</strong></td>
							</tr>
						</tbody>
					</table>
				</div>


			<!-- end of .row-wrapper -->
			</form>
		</div>


		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p><spring:message code="label.brkstaticnotes"/></p>
				</div>
			</div>
		</div>

	</div>
</div> <!-- end of .gutter10 -->

<script>
	$(document).ready(function(){
		$('td.txt-right').each(function(){
		if($(this).is(':empty')){

			$(this).parents('tr').remove();
		}
		});
		$('#aid-editPage').click(function(){
			window.location = "/hix/broker/certificationapplication?isCertified=true";
		});
		$('#aid-editMap').click(function(){
			window.location = "/hix/broker/certificationapplication?isCertified=true&ref=${ecyptedModuleParamsMap}";
		});
	});
</script>
