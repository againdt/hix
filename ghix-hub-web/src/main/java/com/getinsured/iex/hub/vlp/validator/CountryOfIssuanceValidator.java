package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for country of issuance
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class CountryOfIssuanceValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CountryOfIssuanceValidator.class);
	
	/** 
	 * Validations for country of issuance
	 * String having country code between 1-5 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String countryOfIssuance = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			countryOfIssuance = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if (!countryOfIssuance.matches(VLPServiceConstants.COUNTRY_CODE_REGEX)){
			throw new InputDataValidationException(VLPServiceConstants.COUNTRY_CODE_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
