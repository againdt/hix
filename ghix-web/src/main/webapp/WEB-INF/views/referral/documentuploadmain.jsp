<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript"
	src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>

<!-- NOTE: Test page as a placeholder to test document upload. To-be-deleted as soon as UI is ready to test in an integrated way -->
<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1><spring:message code="label.referral.pageheader" /></h1>
	</div>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4><spring:message code="label.referral.sideheader" /></h4>
			</div>
			<ul class="nav nav-list">
				<li><spring:message code="label.referral.securityquestions" />
				</li>
				<li class="active"><spring:message code="label.referral.documentupload" />
				</li>
				<li><spring:message code="label.referral.linktoapplication" />
				</li>
			</ul>
		</div>
		<div id="rightpanel" class="span9 dashboard-rightpanel">

			<div class="header">
				<h4><spring:message code="label.referral.documentupload.txt" /></h4>
			</div>
			<c:if test="${requestScope.failure == '1'}">
				<div id='msgContinue'>
					<div class="gutter10">
						<p>
							<spring:message code="msg.referral.invalid.multiple" />
						</p>
						<div class="span11 offset1" style="padding-bottom: 10px;">
							<ul class="span6">
								<li><spring:message code="label.driving.license" /></li>
								<li><spring:message code="label.hospital.certificate" /></li>
								<li><spring:message code="label.birth.certificate" /></li>
								<li><spring:message code="label.school.record" /></li>
								<li><spring:message code="label.school.id" /></li>
								<li><spring:message code="label.state.id" /></li>
							</ul>
							<ul class="span6">	
								<li><spring:message code="label.passport" /></li>
								<li><spring:message code="label.voter.reg.card" /></li>
								<li><spring:message code="label.us.military.id.card" /></li>
								<li><spring:message code="label.military.dep.id.card" /></li>
								<li><spring:message code="label.us.coast.guard" /></li>
								<li><spring:message code="label.tribal.doc" /></li>

							</ul>
						</div>
					<p><spring:message code="msg.referral.document.content1" /></p>		 			
		 			<p><spring:message code="msg.referral.document.content2" /></p>
					</div>
					<div class="form-actions">
						<button class="btn btn-primary pull-right" id="btnContinue">Continue</button>
					</div>
				</div>
			</c:if>

			<c:if test="${requestScope.failure == 'Y'}">
				<div class="gutter10 center-text">
					<p>
						<spring:message code="msg.referral.document.error" /> 
					</p>
				</div>
			</c:if>

			<c:if test="${requestScope.documentUploaded}">
				<div class="gutter10 center-text">
					<p>
						<spring:message code="msg.referral.pending.csrverify" />
					</p>
				</div>						
			</c:if>
			<c:choose>
				<c:when test="${requestScope.failure == '1'}">
					<div id="mainForm" class="hide">
				</c:when>
				<c:otherwise>
					<div>
				</c:otherwise>
			</c:choose>
			<% 
			 Object fieldErrorObj = session.getAttribute("fieldErrors");
			 if(fieldErrorObj != null)
	         {
	        	 %>
						<font color="red"><spring:message code="${fieldErrors}" /></font><br/>
				<%         	 
	         	session.removeAttribute("fieldErrors");
	         }
	         %>	
			
			<div class="margin10-b gutter10" id="uploadeddocumentlist11">
				<div class="txt-center hide" id="loading11">
					<label class="green">Uploading Document ....</label> <img
						width="15%" alt="loader dots"
						src="/hix/resources/img/loader_greendots.gif">
				</div>
				<c:if test="${documents != null && documents.size() > 0}">
					<table class="table table-condensed margin20-t">
						<thead>
							<tr>
								<th width="50%">Document Name</th>
								<th width="25%">Document Type</th>
								<th width="25%">Accepted</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${documents}" var="document">

								<tr>
									<td><c:out value="${document.documentName}" /></td>
									<td><c:out value="${document.documentType}" /></td>
									<td><c:out value="${document.accepted}" /></td>
								</tr>
							</c:forEach>
						</tbody>

					</table>
				</c:if>
			</div>

			<form:form method="POST" enctype="multipart/form-data"
				onsubmit="return startUpload()" class="form-horizontal">
				 <df:csrfToken/>
				<div class="accordion-inner">
					<div class="cloneMe border-bottom">
						<c:if test="${!requestScope.documentUploaded}">
							<div class="control-group margin10-t">
								<label class="control-label" for="documentTye">Select
									Document Type</label>
								<div class="controls">
									<select name="documentType" class="input-xlarge" >
										<c:forEach items="${documentTypes}" var="documentType">
											<option value='${documentType}'>${documentType}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						
							<div class="control-group">
								<label class="required control-label" for="">Upload
									Selected Document</label>
								<div class="controls">
									<input type="file" name="documentFile" id="documentFile"
										multiple="" class="custom-file-input"  required> <a
										class="pull-right"><input type="submit" value="upload"
										class="btn btn-small btn-primary"> </a>
								</div>
							</div>
						</c:if>
						
						<div>
						<p> <spring:message code="msg.referral.document.type" /></p>
					</div>
					</div>
				</div>
					
			</form:form>
			<div id="confirm" class="modal hide fade modelcntr">
			  <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <h3 id="myModalLabel"><spring:message code="msg.header.document.filetype" javaScriptEscape="true"/></h3>
		          </div>
  <div class="modal-body">
   <p><spring:message code="msg.referral.document.type"/></p>
  </div>
 <div id="submitButton1" class="modal-footer">
		            <button class="btn pull-left" data-dismiss="modal""><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
		          </div>
</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	var startUpload = function() {
		var file=Checkfiles();
		if(true===file){
	    $(":submit").attr("disabled", true);
		$("#loading11").show();
	    }
		return file;
	};
	<c:if test="${requestScope.failure == '1'}">
	var onContinue = function() {
		$("#mainForm").show();
		$("#msgContinue").hide();
	};
	$(function() {
		$("#btnContinue").bind("click", onContinue);
	});
	</c:if>
	 function Checkfiles()
	    {
	        var fup = document.getElementById('documentFile');
	        var fileName = fup.value;
	        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

	    if("txt"==ext.toLowerCase() ||"pdf"==ext.toLowerCase() || "jpeg"==ext.toLowerCase()  ||"png"==ext.toLowerCase() 
	    		|| "jpg"==ext.toLowerCase() || "doc"==ext.toLowerCase() || "docx"==ext.toLowerCase())
	    {
	        return true;
	    }
	    else
	    {
	    	 $('#confirm')
		        .modal({ backdrop: 'static', keyboard: false })
		        .one('click', '[data-value]', function (e) {
		            if($(this).data('value')) {
		                alert('confirmed');
		            } else {
		                alert('canceled');
		            }
		        });
	        return false;
	    }
	    }
</script>
