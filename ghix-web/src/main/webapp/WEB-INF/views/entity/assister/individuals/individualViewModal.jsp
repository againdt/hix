<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<div id="viewindModal" class="modal hide fade" tabindex="-1">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h3 id="myModalLabel"><spring:message code="label.assister.viewIndividualAccount"/></h3>
	</div>
	<div class="modal-body">
		<p><spring:message code="label.assister.commentPart1"/>${externalIndividual.firstName}
			${externalIndividual.lastName}.<spring:message code="label.assister.commentPart2"/> </p>
		<p><spring:message code="label.assister.proceedToIndividualView"/></p>
	</div>
	<form action="<c:url value='/switchToIndividualView/dashboard'/>" id="frmPopup" name="frmPopup">
	<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="individual"/>" />
 	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${externalIndividual.id}"/>" />
 	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${externalIndividual.firstName} ${externalIndividual.lastName}" />
	<div class="modal-footer">
	<input class="pull-left" type="checkbox" id="checkAgentView" name="checkAgentView">
	<div class="pull-left"><spring:message code="label.assister.donshowThisMessageAgain"/></div>
		<button class="btn" data-dismiss="modal"><spring:message code="label.assister.cancel"/></button>
		<button class="btn btn-primary" type="submit"><spring:message code="label.assister.IndividualView"/></button>
			
	</div>
	</form>
</div>