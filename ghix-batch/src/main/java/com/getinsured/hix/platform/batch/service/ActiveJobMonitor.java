package com.getinsured.hix.platform.batch.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.admin.service.NoSuchStepExecutionException;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Component
public class ActiveJobMonitor {
    private static final Logger logger = LoggerFactory.getLogger(ActiveJobMonitor.class);

    private static ActiveJobMonitor monitor;

    // More than one ActiveJobMonitor instances are created and assigned to more than one Spring
    // bean factory. Use static data structure until this issue is resolved.
    private static final Map<Long,GhixStoppableTasklet> runningSteps = new ConcurrentHashMap<>();
    private final JobService jobService;

    @Autowired
    public ActiveJobMonitor(JobService jobService) {
        this.jobService = jobService;
    }

    @PostConstruct
    public void setup() {
        monitor = this;
    }

    public static void started(GhixStoppableTasklet stepExecution) {
        monitor.onStart(stepExecution);
    }

    public static void finished(long stepId) {
        monitor.onFinish(stepId);
    }

    public GhixStoppableTasklet getStoppableTasklet(StepExecution stepExecution) {
        return runningSteps.get(stepExecution.getId());
    }

//    @Scheduled(fixedDelay = 5*1000)
//    public void logJobProgress() {
//        logger.info("**** Job Progress ****");
//        for (JobProgress jobProgress : getJobProgress()) {
//            logger.info(jobProgress.toString());
//        }
//    }

    public List<JobProgress> getJobProgress() {
        List<JobProgress> jobProgressList = new ArrayList<>(runningSteps.size());
        for (GhixStoppableTasklet tasklet : runningSteps.values()) {
            StepExecution stepExecution = tasklet.getStepExecution();
            if (tasklet.hasSetProgress()) {
                jobProgressList.add(getProgressFromTasklet(tasklet, stepExecution));
                continue;
            }
            else {
                jobProgressList.add(calculateProgress(stepExecution));
            }
        }
        return jobProgressList;
    }

    private JobProgress calculateProgress(StepExecution stepExecution) {
        JobExecution jobExecution = stepExecution.getJobExecution();
        String jobName = jobExecution.getJobInstance().getJobName();
        JobProgress jobProgress = new JobProgress(stepExecution);
        StepExecution se = findFirstCompletedStepExecution(jobName, stepExecution.getStepName());
        if (se != null) {
            jobProgress.setPreviousStepExecutionTime(se.getEndTime().getTime() - se.getStartTime().getTime());
            jobProgress.calculate();
        }
        else {
            jobProgress.cannotGuess();
        }
        return jobProgress;
    }

    private JobProgress getProgressFromTasklet(GhixStoppableTasklet tasklet, StepExecution stepExecution) {
        return new JobProgress(stepExecution, tasklet.getProgressPercentage().longValue());
    }


    private static class StepKey {
        private final String jobName;
        private final String stepName;

        public StepKey(String jobName, String stepName) {
            this.jobName = jobName;
            this.stepName = stepName;
        }

        public String getJobName() {
            return jobName;
        }

        public String getStepName() {
            return stepName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            StepKey stepKey = (StepKey) o;

            if (!jobName.equals(stepKey.jobName)) return false;
            return stepName.equals(stepKey.stepName);
        }

        @Override
        public int hashCode() {
            int result = jobName.hashCode();
            result = 31 * result + stepName.hashCode();
            return result;
        }
    }

    LoadingCache<StepKey,StepExecution> cache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build(new CacheLoader<StepKey, StepExecution>() {
                @Override
                public StepExecution load(StepKey stepKey) throws Exception {
                    try {
                        logger.info("Loading " + stepKey.getJobName() + "/" + stepKey.getStepName());
                        int numSteps = jobService.countStepExecutionsForStep(stepKey.getJobName(), stepKey.getStepName());
                        if (numSteps == 0) {
                            throw new NoSuchStepExecutionException("job:" + stepKey.getJobName() + ", step:" + stepKey.getStepName());
                        }

                        int start = 0;
                        int count = 100;
                        do {
                            Collection<StepExecution> stepExecutions = jobService.listStepExecutionsForStep(
                                    stepKey.getJobName(), stepKey.getStepName(), start, count);
                            for (StepExecution stepExecution : stepExecutions) {
                                if (stepExecution.getStatus().equals(BatchStatus.COMPLETED)) {
                                    return stepExecution;
                                }
                                start = count + 1;
                            }
                        } while (start < numSteps);
                        throw new NoSuchStepExecutionException("job:" + stepKey.getJobName() + ", step:" + stepKey.getStepName());
                    }
                    catch (Exception e) {
                        logger.error(e.getMessage(), e);
                        throw e;
                    }
                }
            });

    private StepExecution findFirstCompletedStepExecution(String jobName, String stepName) {
        try {
            return cache.get(new StepKey(jobName, stepName));
        } catch (Exception e) {
            return null;
        }
    }

    private void onStart(GhixStoppableTasklet ghixStoppableTasklet) {
        runningSteps.put(ghixStoppableTasklet.getStepExecution().getId(), ghixStoppableTasklet);
    }

    private void onFinish(long stepId) {
        runningSteps.remove(stepId);
    }

}
