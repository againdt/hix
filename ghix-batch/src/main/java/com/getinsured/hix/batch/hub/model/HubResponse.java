package com.getinsured.hix.batch.hub.model;

import java.io.Serializable;

public class HubResponse implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	
	private String caseNumber;
	
	private String status;
	
	private int retryCount;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the retryCount
	 */
	public int getRetryCount() {
		return retryCount;
	}

	/**
	 * @param retryCount the retryCount to set
	 */
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	@Override
	public String toString() {
		return "HubResponse [id=" + id + ", caseNumber=" + caseNumber
				+ ", status=" + status + ", retryCount=" + retryCount +"]";
	}
}
