<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	
<div class="gutter10">
	<h1>Reset your password</h1>
	<script>
		document.title = 'Email Verification / ' + $('#exchangeName').val();
	</script>
		
	<%-- <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	</div> --%>

	<div class="row-fluid">		
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>You should know</h4>
			</div>
		    <ul class="gutter10 unstyled">
			    <li>In order to securely verify your identity and prevent identity theft, we use both security questions, and your email to verify your identity.</li>
		    </ul>	
		</div>

		<div class="span9" id="rightpanel">
			<div class="header">
				<h4>Your information</h4>
			</div>
			
			<c:if test="${!empty errorMsg}">
				<div class="control-group">
					<div class="controls">	
						<div class="alert alert-error alert-error-custom">
							<c:out value="${errorMsg}"></c:out>
						</div>
					</div>
				</div>	
			</c:if>
			
				<c:if test="${errorMsg == ''}">
					<form class="form-horizontal" id="frmstart" name="frmstart" action="questions" method="POST">
						<df:csrfToken/>
						<div class="question_answer">
							<div class="control-group">
								<p>If an account exists, you'll receive an email from us.</p>
								<p>Please check your email and follow the instructions.</p>							
							</div>										
						</div><!-- question_answer -->
						<!--<div class="control-group nmhide">
							<div class="controls margin-left0">							
								<a href="/hix/account/user/questions" class="btn pull-right">Back</a>
							</div>
						</div>	-->
					</form>
				</c:if>
			</div>
	</div>
</div>	