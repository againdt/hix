import React, { useEffect, useState, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';

import Verification from '../components/Verification';
import { VERIFICATION_DATA, PAGE_CONTAINERS } from '../constants/constants';
import {
    switchPageContainer,
    fetchVerificationData,
    fetchIssuers,
    updateVerificationData,
    selectApplicantIndex,
    submitVerificationData
} from '../actions/actionCreators';
import Spinner from '../components/Spinner';

const AdditionInfoContainer = props => {
    const {
        actions,
        verificationData,
        eventVerificationDetailsDTO,
        selectedApplicantIndex,
        issuersList,
        spinner
    } = props;

    const { ISSUER_CODE, GROUP_NUMBER, POLICY_NUMBER, ACTION_SUBMIT, ACTION_SKIP, ACTION_OVERRIDE } = VERIFICATION_DATA;
    const CASE_NUMBER = localStorage.getItem('enrollCaseNumber');
    // For these statuses, the successful theme will be applied (green colors and icons);
    const validationStatusOk = ['SUBMITTED', 'VERIFIED', 'OVERRIDDEN'];

    const selectedApplicantData = verificationData &&
        verificationData.eventVerificationDetailsDTO &&
        verificationData.eventVerificationDetailsDTO[selectedApplicantIndex];

    const VERIFICATION_DETAILS_API = 'realtime/event/verification/details';

    // Submit action: SUBMIT/OVERRIDE/SKIP.
    const [action, setAction] = useState(null);

    // Form input errors.
    const [errors, setErrors] = useState({});

    /** TODO: Once we move react on top level, move this out.
     * Hides left nav jsp for Verification page (as per design requirements).
     * Shows left nav back after component is unmounted.
     */
    useEffect(() => {
        $('#sidebar').hide();
        $('.indDashboard-welcome').hide();
        $('.dashboard-rightpanel').removeClass('span9');

        return () => {
            $('#sidebar').show();
            $('.indDashboard-welcome').show();
            $('.dashboard-rightpanel').addClass('span9');
        };
    });

    /**
     * Once the new applicant has been selected, reset action and errors from a previous applicant.
     */
    useEffect(() => {
        setAction(null);
        setErrors({});
    }, [selectedApplicantIndex]);

    /**
     * Once some action has been changed call a specific action handler.
     */
    useEffect(() => {
        if (action) {
            switch (action) {
                case ACTION_SKIP:
                    onSkipHandler();
                    break;
                case ACTION_SUBMIT:
                case ACTION_OVERRIDE:
                    onSubmitHandler();
                    break;
            }

        }
    }, [action]);

    /**
     * If there are no registered errors submit data.
     * Resets action after, so it can be called again and again.
     */
    useEffect(() => {
        if (Object.keys(errors).length === 0 && action) {
            const payload = formatData();
            actions.submitVerificationData(VERIFICATION_DETAILS_API, payload);
        }

        if (action) {
            setAction(null);
        }

    }, [errors]);

    /**
     * Watches for input changes to validate values.
     */
    useEffect(() => {
        Object.keys(errors).length !== 0 && validateForm();
    }, [
        selectedApplicantData && selectedApplicantData[ISSUER_CODE],
        selectedApplicantData && selectedApplicantData[GROUP_NUMBER],
        selectedApplicantData && selectedApplicantData[POLICY_NUMBER]
    ]);

    /**
     * Fetches only once verificationData and issuersList
     */
    useEffect(() => {
        actions.fetchVerificationData(`realtime/event/verification/details/${CASE_NUMBER}`);
        actions.fetchIssuers('realtime/event/verification/loadIssuers');
    }, []);

    /**
     * Updates the value of a passed verification input type.
     * @param e
     */
    const onChangeHandler = (e) => {
        const { name, value } = e.target;
        actions.updateVerificationData(name, value);
    };

    /**
     * Validates form inputs. No empty values allowed.
     */
    const validateForm = () => {
        const alphanumeric = /^[0-9a-zA-Z]+$/;
        const { issuerCode, policyNumber, groupNumber } = eventVerificationDetailsDTO[selectedApplicantIndex];
        const errors = {};
        if (!groupNumber) {
            errors.groupNumber = 'Please enter group number'
        }
        if (!policyNumber) {
            errors.policyNumber = 'Please enter policy number'
        }
        if (!issuerCode) {
            errors.issuerCode = 'Please enter issuer code'
        }

        if (groupNumber && !groupNumber.match(alphanumeric)) {
            errors.groupNumber = 'Group number may only contain alphanumeric characters';
        }

        if (policyNumber && !policyNumber.match(alphanumeric)) {
            errors.policyNumber = 'Policy number may only contain alphanumeric characters';
        }
        setErrors(errors);
    };

    /**
     * General action handler for CTA which updates action property of a selected applicant.
     * @param e
     * @param actionType
     */
    const onActionHandler = (e, actionType) => {
        e.preventDefault();

        // OVERRIDE button is located inside Accordion's header, which can be collapsed/expanded.
        if (actionType === ACTION_OVERRIDE) {
            e.stopPropagation();
        }
        setAction(actionType);
    };

    /**
     * On SKIP there is no need to validate inputs. We are submitting only "id" and "action" as a payload request.
     */
    const onSkipHandler = () => {
        const payload = formatData();
        actions.submitVerificationData(VERIFICATION_DETAILS_API, payload);
        setAction(null);
    };

    /**
     * On SUBMIT/OVERRIDE inputs should be validated.
     */
    const onSubmitHandler = () => {
        validateForm();
    };

    /**
     * Changing pageContainer name (Navigation back to the Dashboard).
     * @param e
     */
    const onClickHandlerBtnBack = (e) => {
        e.preventDefault();
        actions.switchPageContainer(PAGE_CONTAINERS.DASHBOARD);
    };

    /**
     * Checks if the validationStatus of a selected applicant is verified.
     * @returns {boolean}
     */
    const isValidationStatusVerified = () => validationStatusOk.includes(eventVerificationDetailsDTO[selectedApplicantIndex].validationStatus);

    /**
     * Gets validation status of an active selected applicant.
     * @returns {string}
     */
    const getValidationStatus = () => eventVerificationDetailsDTO[selectedApplicantIndex].validationStatus;

    /**
     * Gets the icon class depending on a validation status.
     * @param applicantIndex
     * @returns {string}
     */
    const getIcon = (applicantIndex) => {
        return validationStatusOk.includes(eventVerificationDetailsDTO[applicantIndex].validationStatus) ?
            'icon--ml icon-ok-sign' :
            'icon--ml icon-exclamation-sign';
    };

    /**
     * Returns household members, who need verification.
     * @returns {*}
     */
    const getApplicants = () => {
        return eventVerificationDetailsDTO.map((applicant, index) => {
            return (
                <li key={applicant.id}
                    className={index === selectedApplicantIndex ? 'addition-info__applicant addition-info__applicant--active' : 'addition-info__applicant'}
                    onClick={() => {
                        selectApplicant(index)
                    }}
                >
                    {`${applicant.firstName} ${applicant.lastName}`}
                    <i className={getIcon(index)}/>
                </li>
            )
        });
    };

    /**
     * Selects active applicant. Sets selectedApplicantIndex property in the redux store.
     * @param index
     */
    const selectApplicant = (index) => {
        actions.selectApplicantIndex(index);
    };

    /**
     * Gets the list of all issuers in ascending order.
     * @returns {*[]}
     */
    const getIssuers = () => {
        return _.orderBy(issuersList, ['issuerName'], ['asc']).map((issuer) => {
            return (
                <option key={issuer.issuerCode}
                        value={issuer.issuerCode}
                >
                    {`${issuer.issuerName}`}
                </option>
            )
        });
    };

    /**
     * Formats data according to expected payload depending on local state - "action".
     * For SKIP action only applicant's id and action are required in the payload request.
     * @returns {*}
     */
    const formatData = () => {
        const { id, issuerCode, policyNumber, groupNumber } = eventVerificationDetailsDTO[selectedApplicantIndex];

        switch (action) {
            case ACTION_SKIP: {
                return {
                    id,
                    action: ACTION_SKIP
                }
            }
            case ACTION_OVERRIDE:
            case ACTION_SUBMIT:
            default: {
                return {
                    id,
                    issuerCode,
                    policyNumber,
                    groupNumber,
                    action: action
                }
            }
        }
    };

    return (
        <Fragment>
            {spinner && spinner.activeSpinnersQty >  0 &&
            (<Spinner role="progressbar" size="big" ariaText="Loading" customStyle='spinner-custom-large'/>)}
            {
                verificationData && verificationData.eventVerificationDetailsDTO &&
                verificationData.eventVerificationDetailsDTO.length > 0 &&
                (<Verification
                    applicants={getApplicants()}
                    issuers={getIssuers()}
                    validationStatus={getValidationStatus()}
                    isValidationStatusVerified={isValidationStatusVerified()}
                    selectedApplicantData={selectedApplicantData}
                    errors={errors}
                    showOverride={verificationData.showOverride}
                    onClickHandlerBtnBack={onClickHandlerBtnBack}
                    onActionHandler={onActionHandler}
                    onChangeHandler={onChangeHandler}
                />)
            }
        </Fragment>
    );
};

const mapStateToProps = ({ pageContainer, verificationData, issuersList, spinner }) => {
    const { eventVerificationDetailsDTO, selectedApplicantIndex } = verificationData;
    return {
        pageContainer,
        verificationData,
        eventVerificationDetailsDTO,
        selectedApplicantIndex,
        issuersList,
        spinner
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        switchPageContainer,
        fetchVerificationData,
        fetchIssuers,
        updateVerificationData,
        selectApplicantIndex,
        submitVerificationData,
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AdditionInfoContainer);