//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.erp.org.nmhix.ssa.person;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CitizenshipDocument complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CitizenshipDocument">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="documentType" type="{http://www.cohbe.org/SSA/Person}ImmigrationDocumentType-Enum"/>
 *         &lt;element name="alienNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="I94Number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="foreignPassportOrDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="foreignPassportCountryOfIssuance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="foreignPassportExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="sevisId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentOtherType" type="{http://www.cohbe.org/SSA/Person}ImmigrationDocumentTypeOther-Enum" minOccurs="0"/>
 *         &lt;element name="otherFreeText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nameSameOnDocumentIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="nameOnDocument" type="{http://www.cohbe.org/SSA/Person}Name" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CitizenshipDocument", propOrder = {
    "documentType",
    "alienNumber",
    "i94Number",
    "foreignPassportOrDocumentNumber",
    "foreignPassportCountryOfIssuance",
    "foreignPassportExpirationDate",
    "sevisId",
    "documentDescription",
    "documentOtherType",
    "otherFreeText",
    "nameSameOnDocumentIndicator",
    "nameOnDocument"
})

@Entity
@Table(name="CDocument")
public class CitizenshipDocument {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
    @XmlElement(required = true)
    protected ImmigrationDocumentTypeEnum documentType;
    protected String alienNumber;
    @XmlElement(name = "I94Number")
    protected String i94Number;
    
    @Column(name="FPODNumber")
    protected String foreignPassportOrDocumentNumber;
    
    @Column(name="FPCOIssuance")
    protected String foreignPassportCountryOfIssuance;
    
    @Column(name="FPEDate")
    @XmlSchemaType(name = "date")
    protected Date foreignPassportExpirationDate;
    
    protected String sevisId;
    protected String documentDescription;
    protected ImmigrationDocumentTypeOtherEnum documentOtherType;
    protected String otherFreeText;
    
    @Column(name="NSODIndicator")
    protected Boolean nameSameOnDocumentIndicator;
    
    @OneToOne(cascade = {CascadeType.ALL})
    protected Name nameOnDocument;

    /**
     * Gets the value of the documentType property.
     * 
     * @return
     *     possible object is
     *     {@link ImmigrationDocumentTypeEnum }
     *     
     */
    public ImmigrationDocumentTypeEnum getDocumentType() {
        return documentType;
    }

    /**
     * Sets the value of the documentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImmigrationDocumentTypeEnum }
     *     
     */
    public void setDocumentType(ImmigrationDocumentTypeEnum value) {
        this.documentType = value;
    }

    /**
     * Gets the value of the alienNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlienNumber() {
        return alienNumber;
    }

    /**
     * Sets the value of the alienNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlienNumber(String value) {
        this.alienNumber = value;
    }

    /**
     * Gets the value of the i94Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getI94Number() {
        return i94Number;
    }

    /**
     * Sets the value of the i94Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setI94Number(String value) {
        this.i94Number = value;
    }

    /**
     * Gets the value of the foreignPassportOrDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignPassportOrDocumentNumber() {
        return foreignPassportOrDocumentNumber;
    }

    /**
     * Sets the value of the foreignPassportOrDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignPassportOrDocumentNumber(String value) {
        this.foreignPassportOrDocumentNumber = value;
    }

    /**
     * Gets the value of the foreignPassportCountryOfIssuance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignPassportCountryOfIssuance() {
        return foreignPassportCountryOfIssuance;
    }

    /**
     * Sets the value of the foreignPassportCountryOfIssuance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignPassportCountryOfIssuance(String value) {
        this.foreignPassportCountryOfIssuance = value;
    }

    /**
     * Gets the value of the foreignPassportExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public Date getForeignPassportExpirationDate() {
        return foreignPassportExpirationDate;
    }

    /**
     * Sets the value of the foreignPassportExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setForeignPassportExpirationDate(Date value) {
        this.foreignPassportExpirationDate = value;
    }

    /**
     * Gets the value of the sevisId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSevisId() {
        return sevisId;
    }

    /**
     * Sets the value of the sevisId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSevisId(String value) {
        this.sevisId = value;
    }

    /**
     * Gets the value of the documentDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentDescription() {
        return documentDescription;
    }

    /**
     * Sets the value of the documentDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentDescription(String value) {
        this.documentDescription = value;
    }

    /**
     * Gets the value of the documentOtherType property.
     * 
     * @return
     *     possible object is
     *     {@link ImmigrationDocumentTypeOtherEnum }
     *     
     */
    public ImmigrationDocumentTypeOtherEnum getDocumentOtherType() {
        return documentOtherType;
    }

    /**
     * Sets the value of the documentOtherType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImmigrationDocumentTypeOtherEnum }
     *     
     */
    public void setDocumentOtherType(ImmigrationDocumentTypeOtherEnum value) {
        this.documentOtherType = value;
    }

    /**
     * Gets the value of the otherFreeText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherFreeText() {
        return otherFreeText;
    }

    /**
     * Sets the value of the otherFreeText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherFreeText(String value) {
        this.otherFreeText = value;
    }

    /**
     * Gets the value of the nameSameOnDocumentIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNameSameOnDocumentIndicator() {
        return nameSameOnDocumentIndicator;
    }

    /**
     * Sets the value of the nameSameOnDocumentIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNameSameOnDocumentIndicator(Boolean value) {
        this.nameSameOnDocumentIndicator = value;
    }

    /**
     * Gets the value of the nameOnDocument property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getNameOnDocument() {
        return nameOnDocument;
    }

    /**
     * Sets the value of the nameOnDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setNameOnDocument(Name value) {
        this.nameOnDocument = value;
    }

}
