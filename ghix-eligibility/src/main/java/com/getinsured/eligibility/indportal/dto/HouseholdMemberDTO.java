package com.getinsured.eligibility.indportal.dto;

public class HouseholdMemberDTO {
	 private String id;
     private String firstName;
     private String middleName;
     private String lastName;
     private String suffix;
     private String ssn;
     private String primaryPhone;
     private String secondaryPhone;
     private String preferredPhone;
     private String preferredEmail;
     private String homeAddress1;
     private String homeAddress2;
     private String homeCity;
     private String homeState;
     private String homeZip;
     private String type;
     
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getPrimaryPhone() {
		return primaryPhone;
	}
	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}
	public String getSecondaryPhone() {
		return secondaryPhone;
	}
	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}
	public String getPreferredPhone() {
		return preferredPhone;
	}
	public void setPreferredPhone(String preferredPhone) {
		this.preferredPhone = preferredPhone;
	}
	public String getPreferredEmail() {
		return preferredEmail;
	}
	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}
	public String getHomeAddress1() {
		return homeAddress1;
	}
	public void setHomeAddress1(String homeAddress1) {
		this.homeAddress1 = homeAddress1;
	}
	public String getHomeAddress2() {
		return homeAddress2;
	}
	public void setHomeAddress2(String homeAddress2) {
		this.homeAddress2 = homeAddress2;
	}
	public String getHomeCity() {
		return homeCity;
	}
	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}
	public String getHomeState() {
		return homeState;
	}
	public void setHomeState(String homeState) {
		this.homeState = homeState;
	}
	public String getHomeZip() {
		return homeZip;
	}
	public void setHomeZip(String homeZip) {
		this.homeZip = homeZip;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	     
}