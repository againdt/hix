package com.getinsured.hix.batch.enrollment.partitioner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnual1095BatchService;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095Out;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;

@Component("enrollmentAnnual1095OutPartitioner")
@Scope("step")
public class EnrollmentAnnual1095OutPartitioner implements Partitioner {
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	private  String irs1095OutboundCommitInterval;
	private EnrollmentAnnual1095BatchService enrollmentAnnual1095BatchService;
	private Enrollment1095Out enrollment1095Out;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnual1095OutPartitioner.class);
	private String householdCount;
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		
		List<BatchJobExecution> batchExecutionList = enrollmentAnnual1095BatchService.getRunningBatchList("annualIRS1095IndivPartitionedJob");
		
		if(batchExecutionList != null && batchExecutionList.size() == 1){
			Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
			/*ExecutionContext executionContext = null;
			
			if(stepExecution!=null){
				executionContext=stepExecution.getJobExecution().getExecutionContext();
			}*/
			Integer householdIdCount=null;
			if(EnrollmentUtils.isNotNullAndEmpty(householdCount))
			{
				householdIdCount= Integer.valueOf(householdCount.trim());
			}
			
			
			List<Integer> enrollment1095IdList =null;
			if(householdIdCount!=null && householdIdCount>0){
				List<String> householdIds= enrollmentAnnual1095BatchService.getUniqueHouseholdIdsFor1095Pdf();
				if(householdIds!=null && householdIds.size()>0){
					enrollment1095IdList= enrollmentAnnual1095BatchService.getUniqueIdsFor1095Pdf(householdIds.subList(0, Math.min(householdIds.size(), householdIdCount)));
				}
			}else{
				enrollment1095IdList=enrollmentAnnual1095BatchService.getUniqueIdsFor1095Pdf(); //TODO Get effectuated enrollments for the year
			}
			
			
			if(enrollment1095IdList!=null && !enrollment1095IdList.isEmpty()){
				
				LOGGER.info("======= Enrollment_1095_PDF :: Total Count: "+enrollment1095IdList.size() +" List: "+enrollment1095IdList);
				enrollment1095Out.clearEnrollmentIdList();
				enrollment1095Out.addAllToEnrollmentIdList(enrollment1095IdList);
				int maxEnrollmentsPerPartitions =10000;
				int size = enrollment1095IdList.size();
				if(this.irs1095OutboundCommitInterval !=null && !this.irs1095OutboundCommitInterval.isEmpty()){
					maxEnrollmentsPerPartitions=Integer.valueOf(this.irs1095OutboundCommitInterval.trim());
				}
				int numberOfPartitions = size / maxEnrollmentsPerPartitions;
				if (size % maxEnrollmentsPerPartitions != 0) {
					numberOfPartitions++;
				}
				int firstIndex = 0;
				int lastIndex = 0;
				for (int i = 0; i < numberOfPartitions; i++) {
					firstIndex = i * maxEnrollmentsPerPartitions;
					lastIndex = (i + 1) * maxEnrollmentsPerPartitions;
					if (lastIndex > size) {
						lastIndex = size;
					}

					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition",  i);
					partitionMap.put("partition - " + i, value);
					LOGGER.debug("EnrollmentAnnualIRSOutPartitioner : partition():: partition map entry end for partition :: "+i);

				}
			}
			return partitionMap;
		}
		
		return null;
		
	}
	public EnrollmentAnnual1095BatchService getEnrollmentAnnual1095BatchService() {
		return enrollmentAnnual1095BatchService;
	}
	public void setEnrollmentAnnual1095BatchService(
			EnrollmentAnnual1095BatchService enrollmentAnnual1095BatchService) {
		this.enrollmentAnnual1095BatchService = enrollmentAnnual1095BatchService;
	}
	public Enrollment1095Out getEnrollment1095Out() {
		return enrollment1095Out;
	}
	public void setEnrollment1095Out(Enrollment1095Out enrollment1095Out) {
		this.enrollment1095Out = enrollment1095Out;
	}
	public String getIrs1095OutboundCommitInterval() {
		return irs1095OutboundCommitInterval;
	}
	public void setIrs1095OutboundCommitInterval(
			String irs1095OutboundCommitInterval) {
		this.irs1095OutboundCommitInterval = irs1095OutboundCommitInterval;
	}
	public String getHouseholdCount() {
		return householdCount;
	}
	public void setHouseholdCount(String householdCount) {
		this.householdCount = householdCount;
	}

}
