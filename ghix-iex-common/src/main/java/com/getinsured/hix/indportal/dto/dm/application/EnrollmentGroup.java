package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;
import java.util.Map;


public class EnrollmentGroup {
	 private String enrollmentGroupIdentifier;
	 private String enrollmentGroupName;
	 private String defaultEnrollmentGroupReasonType;
	 private String insuranceProductDivisionType;
	 private Map<String,EnrollmentGroupMember> enrollees;
	 private ArrayList < EnrollmentGroupPlan > enrollmentGroupPlans;


	 // Getter Methods 

	 public String getEnrollmentGroupIdentifier() {
	  return enrollmentGroupIdentifier;
	 }

	public Map<String, EnrollmentGroupMember> getEnrollees() {
		return enrollees;
	}

	public void setEnrollees(Map<String, EnrollmentGroupMember> enrollees) {
		this.enrollees = enrollees;
	}

	public ArrayList<EnrollmentGroupPlan> getEnrollmentGroupPlans() {
		return enrollmentGroupPlans;
	}

	public void setEnrollmentGroupPlans(ArrayList<EnrollmentGroupPlan> enrollmentGroupPlans) {
		this.enrollmentGroupPlans = enrollmentGroupPlans;
	}

	public String getEnrollmentGroupName() {
	  return enrollmentGroupName;
	 }

	 public String getDefaultEnrollmentGroupReasonType() {
	  return defaultEnrollmentGroupReasonType;
	 }

	 public String getInsuranceProductDivisionType() {
	  return insuranceProductDivisionType;
	 }


	 // Setter Methods 

	 public void setEnrollmentGroupIdentifier(String enrollmentGroupIdentifier) {
	  this.enrollmentGroupIdentifier = enrollmentGroupIdentifier;
	 }

	 public void setEnrollmentGroupName(String enrollmentGroupName) {
	  this.enrollmentGroupName = enrollmentGroupName;
	 }

	 public void setDefaultEnrollmentGroupReasonType(String defaultEnrollmentGroupReasonType) {
	  this.defaultEnrollmentGroupReasonType = defaultEnrollmentGroupReasonType;
	 }

	 public void setInsuranceProductDivisionType(String insuranceProductDivisionType) {
	  this.insuranceProductDivisionType = insuranceProductDivisionType;
	 }
	}
