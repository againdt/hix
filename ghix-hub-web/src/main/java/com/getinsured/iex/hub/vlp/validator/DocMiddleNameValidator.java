package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for doc middle Name
 * 
 * @author Nikhil Talreja
 * @since 06-Feb-2014
 *
 */
public class DocMiddleNameValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocMiddleNameValidator.class);
	
	/** 
	 * Validations for doc middle name
	 * String between 1-25 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String docMiddleName = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			docMiddleName = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(docMiddleName.length() < VLPServiceConstants.DOC_MIDDLE_NAME_MIN_LEN
				|| docMiddleName.length() > VLPServiceConstants.DOC_MIDDLE_NAME_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.DOC_MIDDLE_NAME_LEN_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
