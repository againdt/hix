const inputHouseholds = ["regression/UQHP_2HH"]
before(function () {
    cy.runBeforeBlock()
})

inputHouseholds.forEach( file => {
    describe(`CIC9: Change uQHP to false for ${file} enrolled into silver medical/dental`, function() {

        it (`Init household info with SSN, DOB with checks for ${file}`, function() {
            cy.ServerInfo = this.serverInfo
            cy.ServerConfig = this.serverConfig.body
            cy.ServerTime = this.serverTime.body
            cy.initHousehold(file)
        })
        it(`Creates Account Transfer for ${file}`, function () {
            cy.createAccountTransfer(cy.household)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Enrolls household into Medical and Dental - ${file}`, function () {
            cy.enrollIntoMedicalAndDental();
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
        it(`Change uQHP to false for ${file}`, function () {
            for (let i = 0; i < cy.h.people.length; i++){
                cy.h.people[i].aptcEligibilityIndicator = true
            }
        })
        it(`Increase APTC max amount for ${file}`, function () {
            cy.aptcMaxAmountChange(125) // increase by 25
        })
        it(`AT update for ${file}`, function () {
            cy.updateAccountTransfer(cy.h)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
    })
})