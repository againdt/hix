<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="gutter10">
	<h1>Unlock Your Account</h1>
	<%-- <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
		<c:if test="${errorMsg != ''}">
			<p><c:out value="${errorMsg}"></c:out><p/>
		</c:if>
			<br>
		</div>
	</div> --%>
	<div class="row-fluid">
	
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>You should know</h4>
			</div>
			<ul class="nav nav-list">
			    <li>To unlock your account you will need your email, answers to security questions and certain miscellaneous personal information.</li>
		    </ul>
		</div>
		
		<div class="span9" id="rightpanel">
		
				
			<div class="header">
				<h4>Step 3: Answer security questions</h4>
			</div>
			
	
							
			<form class="form-horizontal gutter10" id="frmUserUnlockSecurityQues" name="frmUserUnlockSecurityQues" action="<c:url value="/account/unlock/questions"/>" method="POST">
		
				<df:csrfToken/>
	<%-- 			<div class="control-group">
					<label for="securityQuestion1"><c:out value="${securityQuestion1}"></c:out></label>
					<label for="" class="control-label">Answer</label>
						<div class="controls">
							<input type="text"  id="securityAnswer1" name="securityAnswer1" class="input-xlarge" size="30" value="${securityAnswer1}">	
							<div id="securityAnswer1_error"></div>						
						</div>
				</div>	 --%>
				
						
				<div class="control-group">
					<c:forEach items="${accountUserDto.userSecQstnAnsList}" var="currSecQstnAns" varStatus="status">
						<c:set var="qstnAnsIdx" value="${status.index+1}" />
						<form:input type="hidden"  path="accountUserDto.userSecQstnAnsList[${status.index}].idx" value="${qstnAnsIdx}" class="input-xlarge" size="30"  />	
       					<label for="securityQuestion${qstnAnsIdx}"><c:out value="${currSecQstnAns.question}"></c:out></label>
       					<form:input type="hidden"  path="accountUserDto.userSecQstnAnsList[${status.index}].question" value="${currSecQstnAns.question}" class="input-xlarge"  />		
					 	<label for="" class="control-label">Answer</label>
						<div class="controls">
							<form:input type="text"  path="accountUserDto.userSecQstnAnsList[${status.index}].answer" class="input-xlarge" size="30"  />	
							<div id="securityAnswer${qstnAnsIdx}_error"></div>						
						</div> 
    				</c:forEach>
				</div>	
							
				
				<div class="control-group">
					<div class="controls">							
						<input type="submit" value="<spring:message  code='label.brkContinue'/>" class="btn btn-primary btn-large" title="<spring:message  code='label.brkContinue'/>">
					
						<c:if test="${!empty errorMsg}">
							<div class="alert alert-error alert-error-custom">
								<c:out value="${errorMsg}"></c:out>
							</div>
						</c:if>	
					</div>
				</div>		
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
var validator = $("#frmUserUnlockSecurityQues").validate({ 
	rules : { securityAnswer1 : { required : true,maxlength : 4000},
		securityAnswer2 : { required : true,maxlength : 4000},
		securityAnswer3 : { required : true,maxlength : 4000}
	},
	messages : {
		securityAnswer1: { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
			   			   maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"},
		securityAnswer2 : { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
			   				maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"},
		securityAnswer3 : { required : "<span> <em class='excl'>!</em>Please provide your security answer.</span>",
				   			maxlength : "<span> <em class='excl'>!</em>Entered details should not be more than 4000 characters.</span>"}		
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo($("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class',
				'error help-inline');
	} 
	
});
</script>