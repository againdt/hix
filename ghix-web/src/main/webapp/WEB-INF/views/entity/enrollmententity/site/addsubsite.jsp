<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<META http-equiv="Cache-Control" content="max-age=0" />
<META http-equiv="Cache-Control" content="no-cache,no-store,must-revalidate" />
<META HTTP-EQUIV="Expires" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
<META http-equiv="Pragma" content="no-cache" />

<link href="<gi:cdnurl value="/resources/css/chosen.css" />" rel="stylesheet" type="text/css" media="screen,print">

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/chosen.jquery.js" />"></script>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/modal-zipcode-utils.js" />"></script>
 <script type="text/javascript">
  $(document).ready(function(){
	  var config = {
		      '.chosen-select'           : {},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
  });
  
 $(document).ready(function() {
		// disable autocomplete
		if (document.getElementsByTagName) {
			var inputElements = null;
			var elemIds = ["physicalLocation_address1", "physicalLocation_address2", "physicalLocation_city", "physicalLocation_state", "physicalLocation_zip", "mailingLocation_address1", "mailingLocation_address2", "mailingLocation_city", "mailingLocation_state", "mailingLocation_zip"];
			
			for(i=0; elemIds[i]; i++) {
				inputElements = document.getElementById(elemIds[i]);
				
				for (j=0; inputElements[j]; j++) {							
					if (inputElements[j].className && (inputElements[j].className.indexOf("disableAutoComplete") != -1)) {
						inputElements[j].setAttribute("autocomplete","off");
					}
				}
			}
		}
	});
	
</script>

           		<div class="control-group">
	                <label class="control-label" id="siteLocationName"  for="siteLocationName"><spring:message code="label.entity.siteName"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
	                <div class="controls">
	                  <input type="text" id="siteLocationName" name ="siteLocationName" placeholder="<spring:message code="label.entity.siteName"/>" value="${site.siteLocationName}" aria-required="true">
	               	   <div id="siteLocationName_error"></div>
	                </div><!-- .controls -->
	            </div><!-- .control-group -->
          
              <div class="control-group">
                <label class="control-label" for="primaryEmailAddress"><spring:message code="label.entity.primaryEmailAddress"/> </label>
                <div class="controls">
                  <input type="text" id="primaryEmailAddress" name="primaryEmailAddress" placeholder="<spring:message code="label.entity.primaryEmailAddress"/>" value="${site.primaryEmailAddress}">
                  <div id="primaryEmailAddress_error"></div> 
                </div><!-- .controls -->
              </div><!-- .control-group -->	
              
              <div class="control-group">
                <label class="control-label" for="primaryPhoneNumber"><spring:message code="label.entity.primaryPhoneNumber"/></label>
                <label class="hide" for="primaryPhone1">Primary Number 1</label>
                 <label class="hide" for="primaryPhone2">Primary Number 2</label>
                  <label class="hide" for="primaryPhone3">Primary Number 3</label>
                <div class="controls">
                  <input type="text" name="primaryPhone1" id="primaryPhone1" value="${primaryPhone1}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
                  <input type="text" name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
                  <input type="text" name="primaryPhone3" id="primaryPhone3" value="${primaryPhone3}" size="4" maxlength="4" class="input-mini" placeholder="xxxx"/>  
                  <input type="hidden" name="primaryPhoneNumber" id="primaryPhoneNumber" value="${site.primaryPhoneNumber}" /> 
                  <div id="primaryPhone1_error"></div>
                  <div id="primaryPhone3_error"></div>   
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">
                <label class="control-label" for="secondaryPhoneNumber"><spring:message code="label.entity.secondaryPhoneNumber"/></label>
                <label class="hide" for="secondaryPhone1">Secondary Number 1</label>
                 <label class="hide" for="secondaryPhone2">Secondary Number 2</label>
                  <label class="hide" for="secondaryPhone3">Secondary Number 3</label>
                <div class="controls">
                   <input type="text" name="secondaryPhone1" id="secondaryPhone1" value="${secondaryPhone1}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
                  <input type="text" name="secondaryPhone2" id="secondaryPhone2" value="${secondaryPhone2}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
                  <input type="text" name="secondaryPhone3" id="secondaryPhone3" value="${secondaryPhone3}" size="4" maxlength="4" class="input-mini" placeholder="xxxx"/>
                  <input type="hidden" name="secondaryPhoneNumber" id="secondaryPhoneNumber" value="${site.primaryPhoneNumber}" /> 
				   <div id="secondaryPhone1_error"></div>    
				   <div id="secondaryPhone3_error"></div>               
                </div><!-- .controls -->
              </div><!-- .control-group -->
               <div class="control-group">
          		  <div id="from-to-day">
            		  <h5><spring:message code="label.entity.hoursOfOperation"/> <img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></h5>
            				<c:forEach var="day" items="${daysList}">
					                <div class="control-group" id="from-to">
					                		
							                <label id="dayId" class="control-label" for="${day.lookupValueLabel}"> ${day.lookupValueLabel}</label>
							                 <input id="${day.lookupValueLabel}" class="hide">
							                 
							                 <label for="${day.lookupValueLabel}From" class="hide">${day.lookupValueLabel}<spring:message code="label.entity.From"/></label>
							                 <label for="${day.lookupValueLabel}To" class="hide">${day.lookupValueLabel}<spring:message code="label.entity.to"/></label>
							                <div class="controls">				          
												<select size="1" id="${day.lookupValueLabel}From" name="${day.lookupValueLabel}From" class="input-medium">
													<option value=""><spring:message code="label.entity.select"/></option>
													<c:forEach var="fromhour" items="${fromTimelist}">
														<option id="DayStart:${day.lookupValueLabel}" class="hourclass"value="<c:out value="${fromhour.lookupValueCode}" />"<c:if test="${fromhour.lookupValueCode == siteLocationHoursMap[day.lookupValueLabel].fromTime}"> SELECTED </c:if>>
														${fromhour.lookupValueLabel}
														</option>
															
													</c:forEach>
												</select>		
												<span>&nbsp;<spring:message code="label.entity.to"/> &nbsp;</span>
											<select size="1" id="${day.lookupValueLabel}To" name="${day.lookupValueLabel}To" class="input-medium">
											<option value=""><spring:message code="label.entity.select"/></option>
											<c:forEach var="tohour" items="${toTimelist}">
												<option id="DayEnd:${day.lookupValueLabel}" class="hourclass" value="<c:out value="${tohour.lookupValueCode}" />"<c:if test="${tohour.lookupValueCode == siteLocationHoursMap[day.lookupValueLabel].toTime}"> SELECTED </c:if>>
														${tohour.lookupValueLabel}
												</option>
											</c:forEach>
										</select>	
												
									</div>										
								 </div>
							</c:forEach>
							<input type="hidden" id="siteLocationHoursHidden" name="siteLocationHoursHidden" value=""/>
							<div id="siteLocationHoursHidden_error"></div>
							</div>
							</div>
     						
     			
            	 <div class="control-group">
          		  <div id="section" class="entityAddressValidation">
            		  <h5><spring:message code="label.entity.mailingAddres"/></h5>
            		  	<div class="control-group">
		            		  <label class="control-label" for="mailingLocation_address1"><spring:message code="label.entity.streetAddress"/> <img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
		                		<div class="controls">
		                  			<c:choose>
										<c:when test="${CA_STATE_CODE}">
											<input type="text" id="mailingLocation_address1" name="mailingLocation.address1" maxlength="25" placeholder="<spring:message code="label.entity.streetAddress"/>" value="${site.mailingLocation.address1}" />
										</c:when>
										<c:otherwise>
											<input type="text" id="mailingLocation_address1" name="mailingLocation.address1" placeholder="<spring:message code="label.entity.streetAddress"/>" value="${site.mailingLocation.address1}" autocomplete="off" />
										</c:otherwise>
									</c:choose>
		                  			<input type="hidden" id="address1_mailing_hidden" name="address1_mailing_hidden" value="${site.mailingLocation.address1}">
		            	   			<div id="mailingLocation_address1_error"></div> 	
		            	   		</div>
		            	   			
	            	   		</div>
	            	   		<div class="control-group">
		            	   		<label class="control-label" for="mailingLocation_address2"><spring:message code="label.entity.suite"/></label>
			                		<div class="controls">
				                		<c:choose>
											<c:when test="${CA_STATE_CODE}">
												<input type="text" id="mailingLocation_address2" name="mailingLocation.address2" placeholder="<spring:message code="label.entity.suite"/>" value="${site.mailingLocation.address2}" maxlength="25" />
											</c:when>
											<c:otherwise>
												<input type="text" id="mailingLocation_address2" name="mailingLocation.address2" placeholder="<spring:message code="label.entity.suite"/>" value="${site.mailingLocation.address2}" autocomplete="off" />
											</c:otherwise>
										</c:choose>
			                  			<input type="hidden" id="address2_mailing_hidden" name="address2_mailing_hidden" value="${site.mailingLocation.address2}">
			            	   		</div>
			            	 </div> 
			            	   <div class="control-group">
		            	   		<label class="control-label" for="mailingLocation_city"><spring:message code="label.entity.city"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
			                		<div class="controls">
			                  			<c:choose>
											<c:when test="${CA_STATE_CODE}">
												<input type="text" id="mailingLocation_city" name="mailingLocation.city" placeholder="<spring:message code="label.entity.city"/>" value="${site.mailingLocation.city}" maxlength="30" />
											</c:when>
											<c:otherwise>
												<input type="text" id="mailingLocation_city" name="mailingLocation.city" placeholder="<spring:message code="label.entity.city"/>" value="${site.mailingLocation.city}" autocomplete="off" />
											</c:otherwise>
										</c:choose>
			                  			<input type="hidden" id="city_mailing_hidden" name="city_mailing_hidden" value="${site.mailingLocation.city}">
			            	   			<div id="mailingLocation_city_error"></div>
			            	   		</div>
			            	   		
			            	 </div> 
			            	 <div class="control-group">
								<label for="mailingLocation_state" class="control-label"><spring:message code="label.entity.state"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<select size="1" id="mailingLocation_state" name="mailingLocation.state"  class="input-medium" autocomplete="off" >
									<option value=""><spring:message code="label.entity.select"/></option>
									<c:forEach var="state" items="${statelist}">
										<option id="${state.code}" <c:if test="${state.code == site.mailingLocation.state}"> selected="selected" </c:if> value="<c:out value="${state.code}" />">
											<c:out value="${state.name}" />
										</option>
									</c:forEach>
									</select>
								</div>
							<div id="mailingLocation_state_error"></div> 
							<input type="hidden" id="state_mailing_hidden" name="state_mailing_hidden" value="${site.mailingLocation.state}">								
						</div><!-- end of control-group -->    
						
						
			            	 <div class="control-group"> 		
		            	   			<label class="control-label" for="mailingLocation_zip"><spring:message code="label.entity.zipCode"/> <img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
			                		<div class="controls">
			                  			<input type="text" id="mailingLocation_zip" class="input-mini entityZipCode" name="mailingLocation.zip" placeholder="<spring:message code="label.entity.zipCode"/>" value ="${site.mailingLocation.zip}" maxlength="5" autocomplete="off" >
			          					<input type="hidden" id="zip_mailing_hidden" name="zip_mailing_hidden" value="${site.mailingLocation.zip}">
			            	   			<div id="mailingLocation_zip_error"></div> 
			            	   		</div>
			            	   		
			            	  </div>
			            	  <input type="hidden" id="county_physical" name="location.county" value="${site.physicalLocation.county}" />
					<!-- <input type="hidden" name="location.lat" id="lat_physical" value="${site.physicalLocation.lat != null ? site.physicalLocation.lat : 0.0}" />
					<input type="hidden" name="location.lon" id="lon_physical" value="${site.physicalLocation.lon != null ? site.physicalLocation.lon : 0.0}" />	 -->
					<input type="hidden" name="location.rdi" id="rdi_physical" value="${site.physicalLocation.rdi != null ? site.physicalLocation.rdi : ''}" />
					
				<!-- <input type="hidden" name="mailingLocation.lat" id="lat_mailing" value="${site.mailingLocation.lat != null ? site.mailingLocation.lat : 0.0}" />
					<input type="hidden" name="mailingLocation.lon" id="lon_mailing" value="${site.mailingLocation.lon != null ? site.mailingLocation.lon : 0.0}" /> -->
					<input type="hidden" name="mailingLocation.rdi" id="rdi_mailing" value="${site.mailingLocation.rdi != null ? site.mailingLocation.rdi : ''}" />
					<input type="hidden" name="mailingLocation.county" id="county_mailing" value="${site.mailingLocation.county != null ? site.mailingLocation.county : ''}" />
			          </div>	            	   		    	   		
            	 	</div>
            	<div>																								          
                <div class="control-group">
          		  <div id="section">
            		  <h5><spring:message code="label.entity.physicalAddress"/></h5>
            		    <div class="control-group">
						<label class="control-label" for="physicalAddressCheck"> <spring:message code="label.entity.sameAsMailingAddress"/></label>
						<div class="controls">
								<input type="checkbox" name="physicalAddressCheck" id="physicalAddressCheck" onclick="updatedSubmit()" ${locationMatching=='Y' ? 'checked' : 'unchecked'}>
						 </div>
						</div>
					      <div class="control-group">
							  <label class="control-label" for="physicalLocation_address1"><spring:message code="label.entity.streetAddress"/></label>
								<div class="controls">
									<c:choose>
										<c:when test="${CA_STATE_CODE}">
											<input type="text" id="physicalLocation_address1" name="physicalLocation.address1" placeholder="<spring:message code="label.entity.streetAddress"/>" value="${site.physicalLocation.address1}" maxlength="25" />
										</c:when>
										<c:otherwise>
											<input type="text" id="physicalLocation_address1" name="physicalLocation.address1" placeholder="<spring:message code="label.entity.streetAddress"/>" value="${site.physicalLocation.address1}" autocomplete="off" />
										</c:otherwise>
									</c:choose>
									<input type="hidden" id="address1_physical_hidden" name="address1_physical_hidden" value="${site.physicalLocation.address1}">
								</div>
							</div>
							<div class="control-group">
	            	   		<label class="control-label" for="physicalLocation_address2"><spring:message code="label.entity.suite"/></label>
		                		<div class="controls">
		                  			<c:choose>
										<c:when test="${CA_STATE_CODE}">
											<input type="text" id="physicalLocation_address2" name ="physicalLocation.address2" placeholder="<spring:message code="label.entity.suite"/>" value="${site.physicalLocation.address2}" maxlength="25" />
										</c:when>
										<c:otherwise>
											<input type="text" id="physicalLocation_address2" name ="physicalLocation.address2" placeholder="<spring:message code="label.entity.suite"/>" value="${site.physicalLocation.address2}" autocomplete="off" />
										</c:otherwise>
									</c:choose>
		                  			<input type="hidden" id="address2_physical_hidden" name="address2_physical_hidden" value="${site.physicalLocation.address2}">
		            	   		</div>
		            	   	</div>
		            	   	<div class="control-group">
		            	   		<label class="control-label" for="physicalLocation_city"><spring:message code="label.entity.city"/></label>
			                		<div class="controls">
			                  			<c:choose>
											<c:when test="${CA_STATE_CODE}">
												<input type="text" id="physicalLocation_city" name="physicalLocation.city" placeholder="<spring:message code="label.entity.city"/>" value="${site.physicalLocation.city}" maxlength="30" />
											</c:when>
											<c:otherwise>
												<input type="text" id="physicalLocation_city" name="physicalLocation.city" placeholder="<spring:message code="label.entity.city"/>" value="${site.physicalLocation.city}" autocomplete="off" />
											</c:otherwise>
										</c:choose>
			                  			<input type="hidden" id="city_physical_hidden" name="city_physical_hidden" value="${site.physicalLocation.city}">
			            	   		</div>
			            	   	
			            	 </div> 
		            	   	
		            	 <div class="control-group">
							<label class="control-label" for="physicalLocation.state" class="physicalLocation_state"><spring:message code="label.entity.state"/> </label>
							<div class="controls">
								<select size="1" id="physicalLocation_state" name="physicalLocation.state" class="input-medium" autocomplete="off" >
								<option value=""><spring:message code="label.entity.select"/></option>
								<c:forEach var="state" items="${statelist}">
									<option id="${state.code}" <c:if test="${state.code == site.physicalLocation.state}"> selected="selected" </c:if> value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<input type="hidden" id="state_physical_hidden" name="state_physical_hidden" value="${site.physicalLocation.state}">
						</div><!-- end of control-group -->        	   		
            	 	</div>
					
			            	  
		            	   	<div class="control-group">	
	            	   		<label class="control-label" for="physicalLocation_zip"><spring:message code="label.entity.zipCode"/> </label>
		                		<div class="controls">
		                  			<input type="text" id="physicalLocation_zip" class="input-mini entityZipCode" name="physicalLocation.zip" placeholder="<spring:message code="label.entity.zipCode"/>" value="${site.physicalLocation.zip}" maxlength="5" autocomplete="off" >
		                  			<input type="hidden" id="zip_physical_hidden" name="zip_physical_hidden" value="${site.physicalLocation.zip}">
		            	   		</div>
		            	   	</div>
		            	   	<div id="physicalLocation_zip_error"></div> 
            	</div> 	                	

            	<h5 data-toggle="collapse" data-target="#languagesSupported"><spring:message code="label.entity.languagesSupported"/></h5>
            	<div class="control-group">
          			<div id="section">
		                <p><spring:message code="label.entity.selectAllThatApply"/></p>
				             <table  id="languageTable" class="offset1">
									 <tr>				                     
				                      <th><spring:message code="label.entity.spokenLangSelectAllThatApply"/> <img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></th>	
				                     </tr>				                  
				                  <tbody>
				                
				                  <c:forEach var="language" items="${listOflanguages}">
				                    	<tr>				                      
					                      <td>
					                      <label class="hide" for="spokenLanguagesId">${language.lookupValueLabel}</label>
					                      <input type="checkbox" name='spoken' id="spokenLanguagesId"  value="${language.lookupValueLabel}" ${fn:contains(siteLanguages.spokenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
					                      <c:out value="${language.lookupValueLabel}" /></td>
				                      </tr>
				                      </c:forEach>
				                     
				                     <tr>				                      
				                     <td style="text-shadow: none;">
				                     <input type="checkbox" name="otherSpokenLanguageCheckBox" id="otherSpokenLanguageCheckBox" value="otherSpokenLanguage2" style="margin-top:0;" ${not empty langList[1].language ? 'checked="checked"' : ''}/>
				                      <spring:message code="label.entity.other"/>
				                     <label class="hide" for="otherSpokenLanguage">${otherSpokenLanguage}</label>
		                      		 <select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="otherSpokenLanguage" name="otherSpokenLanguage"  class="chosen-select" multiple style="width:350px;" tabindex="4" ></select>
				                     <input type="hidden" id="spokenLanguages" name="spokenLanguages" value=""/>
				                     </td></tr>
				                      <tr>
				                     	<td id="spokenLanguagesId_error"></td>
		                      			<td id="otherSpokenLanguage_error"></td>
		                      		 </tr>
		                      		 </table>
		                      		 <div id="otherSpokenLanguageCheckBox_error"></div>
		                      		  <table  id="languageTable" class="offset1">
				               		  <tr>				                     
					                      <th><spring:message code="label.entity.wriLanSelectAllThatApply"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></th>	
					                   </tr>	
					                     <c:forEach var="language" items="${listOflanguages}"> 
					                     	<tr>
						                      <td>
						                      <label class="hide" for="writtenLanguagesId">${language.lookupValueLabel}</label>
						                      <input type="checkbox" id="writtenLanguagesId" name='written' value="${language.lookupValueLabel}" ${fn:contains(siteLanguages.writtenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
						                      <c:out value="${language.lookupValueLabel}" /></td>
					                    </tr>
					                  </c:forEach>
									   <tr>
					                   <td style="text-shadow: none;">
					                   	<input type="checkbox" name="otherWrittenLanguageCheckBox" id="otherWrittenLanguageCheckBox" value="otherWrittenLanguage2" style="margin-top:0;" ${not empty langList[1].language ? 'checked="checked"' : ''}/>
					                    <spring:message code="label.entity.other"/>
			                      		<select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="otherWrittenLanguage" name="otherWrittenLanguage"  class="chosen-select" multiple style="width:350px;" tabindex="4" ></select>   
					                 	 <input type="hidden" id="writtenLanguages" name="writtenLanguages" value=""/>
					                  </td>
					                 </tr> 
					                  <tr>
					                  	<td id="writtenLanguagesId_error"></td>	                  		
										<td id="otherWrittenLanguage_error"></td>				                  	
					                  </tr>
					              </tbody>
				                </table>
				            <div id="otherWrittenLanguageCheckBox_error"></div>	
				       </div>
				 </div>    
            </div> 	         				
		      </div>   
		      	<input type="hidden" id="siteType" name="siteType" value="${site.siteType}"/>
		      	
		      	<input type="hidden" name="siteLanguageId" value="${siteLanguages.id}"/>   
		      	<div class="clearfix">                                
					<input type="submit" name="save" id="save" class="btn btn-primary pull-right margin10-b" value="<spring:message code="label.entity.saveSite"/>" onclick="setActionAndMethodType('SUBSITE','add',0)" />
				</div> 
	         
	        
<script type="text/javascript">

$(document).ready(function() {
	$("#locationAndHours").removeClass("link");
	$("#locationAndHours").addClass("active");
	var isEdit='${isEdit}';
	if(isEdit!="true")
	{	
		$("#primaryPhone1").val("");
		$("#primaryPhone2").val(""); 
		$("#primaryPhone3").val(""); 
		$("#secondaryPhone1").val("");
		$("#secondaryPhone2").val(""); 
		$("#secondaryPhone3").val("");
	}
	
	if($("#physicalAddressCheck").is(":checked")) {
		$("#physicalLocation_address1").attr('readonly', true);
		$("#physicalLocation_address2").attr('readonly', true);
		$("#physicalLocation_zip").attr('readonly', true);
		$("#physicalLocation_city").attr('readonly', true);
		$("#physicalLocation_state").attr('readonly', true);
	} else {
		$("#physicalLocation_address1").removeAttr("readonly"); 
		$("#physicalLocation_address2").removeAttr("readonly"); 		
		$("#physicalLocation_zip").removeAttr("readonly"); 
		$("#physicalLocation_city").removeAttr("readonly"); 
		$("#physicalLocation_state").removeAttr("readonly");
	}
	
	$(".input-medium").change(function(){
		var currentId = $(this).attr('id');
		currentElement = $(this).attr('id');
		currentId = currentId.replace(/From|To/g, "");
		if(currentElement.indexOf("From") > 0){
			if($("#"+currentId+"From").val() == "Closed"){
				$("#" + currentId + "To").val("Closed");
				$("#" + currentId + "To").attr("disabled","disabled");
				return;
			}
			if($("#"+currentId+"From").val() != "Select" && $("#"+currentId+"From").val() != "Closed"){
				if($("#" + currentId + "To").val() == "Closed"){
					$("#" + currentId + "To").val("Select");
				}
				$("#" + currentId + "To").attr("disabled", false);
				return;
			}
		}
		else{
			if($("#" + currentId + "To").val() == "Closed"){
				$("#" + currentId + "From").val("Closed");
				$("#" + currentId + "To").attr("disabled","disabled");
				return;
			}
			if($("#" + currentId + "To").val() != "Select" && $("#" + currentId + "To").val() != "Closed"){
				if($("#" + currentId + "From").val() == "Closed"){
					$("#" + currentId + "From").val("Select");
				}
				return;
			}
		}
		
	});
	
});

function  updatedSubmit() {
	ie8Trim();
	if($("#physicalAddressCheck").is(":checked")) {
		var strMailingAddress1 = $("#mailingLocation_address1").val();
		$("#physicalLocation_address1").val(strMailingAddress1);
		
		var strMailingSuite = $("#mailingLocation_address2").val();
		$("#physicalLocation_address2").val(strMailingSuite);
	
		var strMailingState = $("#mailingLocation_state").val();
		$("#physicalLocation_state").val(strMailingState);
	
		var strMailingZip = $("#mailingLocation_zip").val();
		$("#physicalLocation_zip").val(strMailingZip);
		
		var strcity = $("#mailingLocation_city").val();
		$("#physicalLocation_city").val(strcity);
		
		$("#physicalLocation_address1").attr('readonly', true);
		$("#physicalLocation_address2").attr('readonly', true);
		$("#physicalLocation_zip").attr('readonly', true);
		$("#physicalLocation_city").attr('readonly', true);
		$("#physicalLocation_state").attr('readonly', true);
		
	}else{
		$("#physicalLocation_address1").val("");
		$("#physicalLocation_address2").val("");
		$("#physicalLocation_state").val("");	
		$("#physicalLocation_zip").val("");	
		$("#physicalLocation_city").val("");
		
		$("#physicalLocation_address1").removeAttr("readonly"); 
		$("#physicalLocation_address2").removeAttr("readonly"); 		
		$("#physicalLocation_zip").removeAttr("readonly"); 
		$("#physicalLocation_city").removeAttr("readonly"); 
		$("#physicalLocation_state").removeAttr("readonly");
	}; 
};

</script>	
<script>
	$('#addSubSite').hide();
	
	
	$('#physicalLocation_address1').focusin(function() {
		
		if(($('#physicalLocation_address2').val())==="Address Line 2"){
			$('#physicalLocation_address2').val('');
		}
	
	

});

	$('#mailingLocation_address1').focusin(function() {
	
	if(($('#mailingLocation_address2').val())==="Address Line 2"){
		$('#mailingLocation_address2').val('');
	}



});
</script>
<script type="text/javascript">
 //load the jquery chosen plugin 
	function loadLanguages() {
		$("#otherSpokenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherSpokenLanguage}';
		for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkCounties(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	     
	     $('#otherSpokenLanguage').trigger("liszt:updated");
	   }

	function checkCounties(county){
	     var counties='${otherSpokenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguages();
	});
	

</script>
<script type="text/javascript">
 //load the jquery chosen plugin 
	function loadLanguagesForWritten() {
		$("#otherWrittenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherWrittenLanguage}';
	    for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkCounties1(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	    $('#otherWrittenLanguage').trigger("liszt:updated");
	   
	   }

	function checkCounties1(county){
	     var counties='${otherWrittenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguagesForWritten();
	});
	
</script>