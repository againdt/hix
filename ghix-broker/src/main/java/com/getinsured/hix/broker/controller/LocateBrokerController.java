package com.getinsured.hix.broker.controller;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.dto.broker.BrokerAvailabilityDTO;
import com.getinsured.hix.dto.broker.BrokerConnectHoursDTO;
import com.getinsured.hix.dto.broker.BrokerParticipationDTO;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.model.BrokerCallPrioritization;
import com.getinsured.hix.model.BrokerConnect;
import com.getinsured.hix.model.BrokerConnectHours;
import com.getinsured.hix.model.BrokerConnectResponse;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.util.TSDate;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.dto.broker.BrokerContactDTO;
import com.getinsured.hix.dto.broker.BrokerRequestDTO;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.BrokerResponse;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.platform.util.GhixUtils;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

@Controller
@RequestMapping(value = "/broker")
public class LocateBrokerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocateBrokerController.class);
    @Autowired
    private Gson platformGson;

    @Autowired
    private DesignateService designateService;

    @Autowired
    private BrokerService brokerService;

    @Autowired
    private IBrokerRepository brokerRepository;

    @Autowired
    private GhixRestServiceInvoker ghixRestServiceInvoker;

    private static final String CONNECT_HOURS_CLOSED_VALUE = "closed";

    @RequestMapping(value = "/findIndividualDesignation", method = RequestMethod.POST)
    @ResponseBody
    public DesignateBroker getActiveDesignation(@RequestBody Integer householdId) {
        DesignateBroker designateBroker = null;
        designateBroker = designateService.findBrokerByIndividualId(householdId);
        return designateBroker;
    }

    @RequestMapping(value = "/designateIndividual", method = RequestMethod.POST)
    @ResponseBody
    public String designateAssister(@RequestBody String requestXML) {

        LOGGER.info("designateAssister: START");

        BrokerResponse brokerResponse = new BrokerResponse();
        DesignateBroker designateBroker = null;
        Broker broker = null;

        LOGGER.debug("Locate Assister : Save Assister Designation Detail Starts");

        XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
        BrokerRequestDTO brokerRequestDTO = (BrokerRequestDTO) xstream.fromXML(requestXML);

        try {
            designateBroker = designateService.findBrokerByIndividualId(brokerRequestDTO.getIndividualId());

            if (designateBroker == null) {
                designateBroker = new DesignateBroker();

                designateBroker.setCreated(new TSDate());
                designateBroker.setCreatedBy(brokerRequestDTO.getUserId());
            } else {
                designateBroker.setUpdated(new TSDate());
                designateBroker.setUpdatedBy(brokerRequestDTO.getUserId());
            }

            if (brokerRequestDTO.getId() != null) {
                broker = brokerService.findById(brokerRequestDTO.getId());
            } else {
                broker = brokerService.getBrokerDetailByUserId(brokerRequestDTO.getUserId());
            }

            designateBroker.setIndividualId(brokerRequestDTO.getIndividualId());
            designateBroker.setBrokerId(broker.getId());
            designateBroker.setStatus(brokerRequestDTO.getDesigStatus());
            designateBroker.setEsignBy(brokerRequestDTO.getEsignName());
            designateBroker.setEsignDate(new TSDate());
            designateBroker.setShow_switch_role_popup('Y');
            designateBroker.setExternalIndividualId(brokerRequestDTO.getIndividualId());


            designateBroker = designateService.saveDesignateBroker(designateBroker);

            if (designateBroker != null) {
                brokerResponse.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                brokerResponse.setResponseDescription(BrokerConstants.RESPONSE_SUCCESS);
                brokerResponse.setStatus(BrokerConstants.RESPONSE_SUCCESS);
            } else {
                brokerResponse.setStatus(BrokerConstants.RESPONSE_FAILURE);
            }
        } catch (Exception exception) {
            LOGGER.error("Locate Assister : Save Assister Designation Detail : Exception occurred while saving Assister Detail : "
                    , exception);

            brokerResponse.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
            brokerResponse.setResponseDescription(exception.getMessage());

            brokerResponse.setStatus(BrokerConstants.RESPONSE_FAILURE);
        }

        LOGGER.debug("Locate Assister : Save Assister Designation Detail Ends");

        LOGGER.info("designateAssister: END");

        return GhixUtils.xStreamHibernateXmlMashaling().toXML(brokerResponse);
    }


    @RequestMapping(value = "/getListOfCertifiedBroker", method = RequestMethod.POST)
    @ResponseBody
    public String getListOfCertifiedBroker() {

        LOGGER.info("designateAssister: START");
        List<BrokerContactDTO> brokers = null;
        BrokerResponse brokerResponse = new BrokerResponse();
        //DesignateBroker designateBroker = null;
        //Broker broker = null;

        //LOGGER.info("Locate Assister : Save Assister Designation Detail Starts : Request : " + requestXML);
        //XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
        //BrokerRequestDTO brokerRequestDTO = (BrokerRequestDTO) xstream.fromXML(requestXML);

        try {
            brokers = brokerService.findBrokerListByStatus();
        } catch (Exception exception) {
            LOGGER.error("Locate Assister : Save Assister Designation Detail : Exception occurred while saving Assister Detail : "
                    , exception);
        }

        LOGGER.debug("Locate Assister : Save Assister Designation Detail Ends : Response : {}", brokerResponse);

        LOGGER.info("designateAssister: END");

        return platformGson.toJson(brokers);
    }

    @RequestMapping(value = "/participationDetails/{brokerId}", method = RequestMethod.GET)
    @ResponseBody
    public String getBrokerParticipationDetails(@PathVariable("brokerId") Integer brokerId) {
        LOGGER.debug("getBrokerOnDemandData::brokerId = {}", brokerId);
        BrokerParticipationDTO response = new BrokerParticipationDTO();
        response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);

        try {
            if (brokerId != null) {
                Broker broker = getBrokerForBrokerId(brokerId);

                if(broker != null) {
                    LOGGER.debug("getBrokerOnDemandData::brokerId = {}", broker.getId());
                    BrokerConnect brokerConnect = brokerService.getBrokerConnectDetails(broker);
                    if (brokerConnect != null) {
                        response.setAvailable(brokerConnect.isAvailable());
                        response.setStatus(brokerConnect.getStatus().toString());
                        response.setPhoneNumber(brokerConnect.getPhoneNumber());
                        response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                    } else {
                        LOGGER.debug("getBrokerOnDemandData::broker connect query object null");
                        // Pre-fill broker's contact number
                        response.setPhoneNumber(broker.getContactNumber());
                        response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error("getBrokerOnDemandData::error getting broker connect details", ex);
        }

        return platformGson.toJson(response);
    }

    @RequestMapping(value = "/participationDetails/{brokerId}", method = RequestMethod.POST)
    @ResponseBody
    public String setBrokerParticipationDetails(@RequestBody BrokerParticipationDTO brokerParticipation, @PathVariable("brokerId") Integer brokerId) {
        BrokerConnectResponse response = new BrokerConnectResponse();
        LOGGER.debug("setBrokerParticipationDetails::brokerId = {}", brokerId);
        try {
            if (brokerParticipation != null && StringUtils.isNotBlank(brokerParticipation.getPhoneNumber()) && brokerParticipation.getPhoneNumber().length() == 10
                    && brokerParticipation.getStatus() != null && brokerId != null) {
                LOGGER.debug("setBrokerParticipationDetails::valid request");
                Broker broker = getBrokerForBrokerId(brokerId);
                if (broker != null) {
                    LOGGER.debug("getBrokerOnDemandData::brokerId = {}", broker.getId());
                    BrokerConnect brokerConnect = brokerService.getBrokerConnectDetails(broker);
                    if (brokerConnect == null) {
                        brokerConnect = new BrokerConnect();
                    }
                    boolean isAvailable = brokerParticipation.getAvailable();
                    if (brokerParticipation.getStatus().equals(BrokerConnect.Status.INACTIVE.toString())) {
                        isAvailable = false;
                    }

                    brokerConnect.setBrokerId(broker);
                    brokerConnect.setPhoneNumber(brokerParticipation.getPhoneNumber().replaceAll("[^\\d.]", ""));
                    brokerConnect.setAvailable(isAvailable);
                    brokerConnect.setStatus(brokerParticipation.getStatus().equals(BrokerConnect.Status.ACTIVE.toString()) ?
                            BrokerConnect.Status.ACTIVE : BrokerConnect.Status.INACTIVE);

                    LOGGER.debug("setBrokerParticipationDetails::saving details");
                    BrokerConnect updateResponse = brokerService.addBrokerConnectDetails(brokerConnect);
                    if (updateResponse != null) {
                        LOGGER.debug("setBrokerParticipationDetails::saving details");
                        response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                    } else {
                        response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
                    }
                }
            } else {
                LOGGER.debug("setBrokerParticipationDetails::request was invalid");
                response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
            }
        } catch (Exception ex) {
            LOGGER.error("setBrokerParticipationDetails::failed to set the participation details", ex);
            response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
        }

        return platformGson.toJson(response);
    }

    @RequestMapping(value = "/connectHours/{brokerId}", method = RequestMethod.GET)
    @ResponseBody
    public String getBrokerConnectHours(@PathVariable("brokerId") Integer brokerId) {
        LOGGER.debug("getBrokerConnectHours::brokerId = {}", brokerId);
        BrokerConnectHoursDTO response = new BrokerConnectHoursDTO();
        response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);

        try {
            if (brokerId != null) {
                Broker broker = getBrokerForBrokerId(brokerId);
                LOGGER.debug("getBrokerConnectHours::getting broker connect object");
                BrokerConnect brokerConnect = brokerService.getBrokerConnectDetails(broker);
                if (brokerConnect != null) {
                    LOGGER.debug("getBrokerConnectHours::getting broker connect hours");
                    List<BrokerConnectHours> brokerConnectHoursList = brokerService.getBrokerConnectHours(brokerConnect);
                    if (brokerConnectHoursList != null) {
                        response.setIsAvailable(brokerConnect.isAvailable());
                        response.setStatus(brokerConnect.getStatus().toString());
                        response.setPhoneNumber(brokerConnect.getPhoneNumber());
                        List<BrokerAvailabilityDTO> brokerAvailabilities = new ArrayList<>();
                        for (BrokerConnectHours brokerConnectHours : brokerConnectHoursList) {
                            BrokerAvailabilityDTO brokerAvailability = new BrokerAvailabilityDTO();
                            brokerAvailability.setDay(brokerConnectHours.getDay().toString());
                            brokerAvailability.setFromTime(brokerConnectHours.getFromTime());
                            brokerAvailability.setToTime(brokerConnectHours.getToTime());
                            brokerAvailabilities.add(brokerAvailability);
                        }
                        response.setBrokerAvailabilities(brokerAvailabilities);
                        response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error("getBrokerConnectHours::error getting broker connect hours", ex);
            response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
        }

        return platformGson.toJson(response);
    }

    @RequestMapping(value = "/connectHours/{brokerId}", method = RequestMethod.POST)
    @ResponseBody
    public String setBrokerConnectHours(@RequestBody BrokerConnectHoursDTO brokerConnectHoursDTO, @PathVariable("brokerId") Integer brokerId) {
        BrokerConnectResponse response = new BrokerConnectResponse();
        response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
        LOGGER.debug("setBrokerConnectHours::brokerId = {}", brokerId);
        LOGGER.debug("setBrokerConnectHours::brokerConnectHoursDTO = {}", brokerConnectHoursDTO);

        try {
            if (brokerConnectHoursDTO != null && brokerConnectHoursDTO.getBrokerAvailabilities() != null &&
                    !brokerConnectHoursDTO.getBrokerAvailabilities().isEmpty() && brokerId != null && validateBrokerConnectHours(brokerConnectHoursDTO.getBrokerAvailabilities())) {
                LOGGER.debug("setBrokerConnectHours::hours and id not null");
                Broker broker = getBrokerForBrokerId(brokerId);
                BrokerConnect brokerConnect = brokerService.getBrokerConnectDetails(broker);
                if (brokerConnect != null) {
                    List<BrokerConnectHours> request = new ArrayList<>();
                    List<BrokerConnectHours> brokerConnectHoursList = brokerService.getBrokerConnectHours(brokerConnect);

                    if (brokerConnectHoursList != null && !brokerConnectHoursList.isEmpty()) {
                        LOGGER.debug("setBrokerConnectHours::DB hours list not null or empty");
                        Map<String, BrokerAvailabilityDTO> availabilityMap = brokerConnectHoursDTO.getBrokerAvailabilities().stream().collect(Collectors.toMap(BrokerAvailabilityDTO::getDay, Function.identity()));
                        for (BrokerConnectHours brokerConnectHour : brokerConnectHoursList) {
                            BrokerAvailabilityDTO availability = availabilityMap.get(brokerConnectHour.getDay().name());
                            brokerConnectHour.setDay(BrokerConnectHours.Days.valueOf(availability.getDay()));
                            if (StringUtils.isNotBlank(availability.getFromTime()) && StringUtils.isNotBlank(availability.getToTime())) {
                                brokerConnectHour.setFromTime(availability.getFromTime());
                                brokerConnectHour.setToTime(availability.getToTime());
                            } else {
                                throw new IllegalArgumentException("Times are not valid");
                            }
                            request.add(brokerConnectHour);
                        }
                    } else {
                        LOGGER.debug("setBrokerConnectHours::DB hours list null or empty");
                        for (BrokerAvailabilityDTO brokerAvailability : brokerConnectHoursDTO.getBrokerAvailabilities()) {
                            BrokerConnectHours brokerConnectHour = new BrokerConnectHours();
                            brokerConnectHour.setBrokerConnectId(brokerConnect);
                            brokerConnectHour.setDay(BrokerConnectHours.Days.valueOf(brokerAvailability.getDay()));
                            brokerConnectHour.setFromTime(brokerAvailability.getFromTime());
                            brokerConnectHour.setToTime(brokerAvailability.getToTime());
                            request.add(brokerConnectHour);
                        }
                    }
                    LOGGER.debug("setBrokerConnectHours::created request");
                    List<BrokerConnectHours> result = brokerService.addBrokerConnectHours(request);
                    if (result != null) {
                        response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                        LOGGER.debug("setBrokerConnectHours::was success updating broker availability based on request");
                        setBrokerAvailability(brokerConnectHoursDTO.getIsAvailable(), brokerId);
                    } else {
                        response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
                    }
                } else {
                    LOGGER.debug("setBrokerConnectHours::userid or brokerConnectHoursDTO were null");
                }
            }
        } catch (Exception ex) {
            LOGGER.error("setBrokerConnectHours::error setting broker hours", ex);
        }

        return platformGson.toJson(response);
    }

    private boolean validateBrokerConnectHours(List<BrokerAvailabilityDTO> brokerAvailabilities) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");

            for (BrokerAvailabilityDTO availability : brokerAvailabilities) {
                if (!availability.getFromTime().equals(CONNECT_HOURS_CLOSED_VALUE) && !availability.getToTime().equals(CONNECT_HOURS_CLOSED_VALUE)) {
                    LOGGER.debug("validateBrokerConnectHours::from and to times are both not closed");
                    LocalTime fromTime = LocalTime.parse(availability.getFromTime(), dtf);
                    LocalTime toTime = LocalTime.parse(availability.getToTime(), dtf);

                    if (fromTime.isAfter(toTime) || fromTime.equals(toTime) || toTime.isBefore(fromTime)) {
                        LOGGER.debug("validateBrokerConnectHours::from ({}) and to ({}) times not valid", fromTime, toTime);
                        return false;
                    }
                } else if (availability.getFromTime().equals(CONNECT_HOURS_CLOSED_VALUE) && !availability.getToTime().equals(CONNECT_HOURS_CLOSED_VALUE) ||
                        !availability.getFromTime().equals(CONNECT_HOURS_CLOSED_VALUE) && availability.getToTime().equals(CONNECT_HOURS_CLOSED_VALUE)) {
                    LOGGER.debug("validateBrokerConnectHours::one value is closed and one is not: fromTime {}, toTime {}", availability.getFromTime(), availability.getToTime());
                    return false;
                }
            }
        } catch (Exception ex) {
            LOGGER.error("validateBrokerConnectHours::error validating broker hours, return false", ex);
            return false;
        }

        return true;
    }

    @RequestMapping(value = "/availability/{brokerId}", method = RequestMethod.POST)
    @ResponseBody
    public String setBrokerAvailability(@RequestBody Boolean availability, @PathVariable("brokerId") Integer brokerId) {
        LOGGER.debug("setBrokerAvailability::availability = {}", availability);
        LOGGER.debug("setBrokerAvailability::brokerId = {}", brokerId);
        BrokerConnectResponse response = new BrokerConnectResponse();
        response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);

        try {
            if (availability != null && brokerId != null) {
                Broker broker = getBrokerForBrokerId(brokerId);
                BrokerConnect brokerConnectUpdate = brokerService.getBrokerConnectDetails(broker);
                if (brokerConnectUpdate != null) {
                    Integer brokerConnectId = brokerConnectUpdate.getId();
                    BrokerConnect brokerConnect = brokerService.updateBrokerAvailability(brokerConnectId, availability);
                    if (brokerConnect != null) {
                        response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error("setBrokerAvailability::failure to update broker availability", ex);
            response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
        }

        return platformGson.toJson(response);
    }

    private Broker getBrokerForBrokerId(Integer brokerId) {
        Broker broker = null;

        if (brokerId != null) {
            broker = brokerService.getBrokerDetail(brokerId);
            if (broker != null) {
                LOGGER.debug("getBrokerOnDemandData::success getting broker");
            }
        }
        return broker;
    }

    @RequestMapping(value = "/search/", method = RequestMethod.POST)
    @ResponseBody
    public String getBrokerListCallPrioritization(@RequestBody BrokerCallPrioritization brokerCallPrioritization) {
        List<String> brokerPhoneList = new ArrayList<>();

        if (brokerCallPrioritization != null && StringUtils.isNotEmpty(brokerCallPrioritization.getZipCode())) {
            if (StringUtils.isBlank(brokerCallPrioritization.getLanguage())) {
                brokerCallPrioritization.setLanguage("English");
            }

            brokerPhoneList = brokerService.searchAvailableBrokers(brokerCallPrioritization);
        }

        return String.join(",", brokerPhoneList);
    }

    @RequestMapping(value = "/designated/name/{individualId}", method = RequestMethod.GET)
    @ResponseBody
    public String getDesignatedBrokerAssistorName(@PathVariable("individualId") Integer individualId) {
        String name = "";

        try {
            DesignateBroker designateBroker = designateService.findBrokerByIndividualId(individualId);
            if (designateBroker != null) {
                Broker broker = brokerRepository.findById(designateBroker.getBrokerId());
                name = broker.getUser().getFirstName() + " " + broker.getUser().getLastName();
            }

            AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
            assisterRequestDTO.setIndividualId(individualId);

            AssisterDesignateResponseDTO assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
            if (assisterDesignateResponseDTO != null) {
                DesignateAssister designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
                Assister assister = assisterDesignateResponseDTO.getAssister();
                if (designateAssister != null && assister != null && !designateAssister.getStatus().toString().equals(DesignateAssister.Status.InActive.toString())) {
                    name = assister.getFirstName() + " " + assister.getLastName();
                }
            }
        } catch (Exception ex) {
            LOGGER.error("getDesignatedBrokerAssistorName::error getting broker/assistor name", ex);
        }

        return name;
    }


    /**
     * Method to retrieve Assister designation detail by individual identifier.
     *
     * @param assisterRequestDTO
     *            The AssisterRequestDTO instance.
     * @return assisterResponseDTO The AssisterResponseDTO instance.
     * @throws GIException
     * @throws Exception
     */
    private AssisterDesignateResponseDTO retrieveAssisterDesignationDetailByIndividualId(
            AssisterRequestDTO assisterRequestDTO) throws GIException {
        AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
        LOGGER.debug("REST call start to the GHIX-ENITY module to retreive Assister Designation details.");
        try {
            assisterDesignateResponseDTO = ghixRestServiceInvoker.invokeRestService(assisterRequestDTO,
                    GhixEndPoints.EnrollmentEntityEndPoints.GET_ASSISTER_DESIGNATION_DETAIL_BY_INDIVIDUAL_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString(), AssisterDesignateResponseDTO.class);

            if (assisterDesignateResponseDTO != null
                    && assisterDesignateResponseDTO.getResponseCode() == 404) {
                throw new GIException();
            }

            LOGGER.debug("REST call Ends to the GHIX-ENITY module."
                    + assisterDesignateResponseDTO);
        } catch (Exception ex) {
            LOGGER.error("Exception in retrieving Designated Assister details, REST response code is: "
                    + ((assisterDesignateResponseDTO != null) ? assisterDesignateResponseDTO
                    .getResponseCode()
                    : "assisterDesignateResponseDTO is null"));
            //persistToGiMonitor(ex);
            throw new GIException(
                    "Exception in retrieving designatedAssister details."
                            + ex.getMessage());
        }

        return assisterDesignateResponseDTO;
    }
}
