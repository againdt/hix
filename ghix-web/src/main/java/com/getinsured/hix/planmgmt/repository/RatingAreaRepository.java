package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.RatingArea;

public interface RatingAreaRepository extends JpaRepository<RatingArea, Integer> {
	
	@Query("SELECT ra FROM RatingArea ra WHERE ra.ratingArea=:strRatingArea")
	RatingArea getRatingAreaByName(@Param("strRatingArea") String strRatingArea);
	
	@Query("SELECT DISTINCT ra.ratingArea FROM RatingArea ra WHERE ra.state=:stateCode order by ra.ratingArea")
	List<String> getAllRatingAreaName(@Param("stateCode") String stateCode);
	
	@Query("SELECT DISTINCT ra FROM RatingArea ra WHERE LOWER(ra.ratingArea) = LOWER(:strRatingArea) AND LOWER(ra.state) = LOWER(:strState)")
	RatingArea getRatingAreaByNameAndState(@Param("strRatingArea") String strRatingArea, @Param("strState") String strState);
	
	RatingArea findByRatingAreaAndState(String RatingArea, String State);
		
}
