const MergeRecursively = (obj1, obj2, currentFields, fields, value, name) => {
    if (fields && fields.length === 0) {
        obj1[name] = value;
        return obj1;
    }
    else {
        for(var p in obj1) {
            if(obj1.hasOwnProperty(p)) {
                if ( obj1[p] instanceof Object ) {
                    obj1[p] = MergeRecursively(obj1[p], obj2, [...currentFields, p], fields, value, name);
                } else {

                    if(obj2[p] !== undefined && obj1[p] !== obj2[p]) {
                        const isFieldsTheSame = fields.length === currentFields.length &&
                            fields.every((value, index) => value === currentFields[index]);
                        if (isFieldsTheSame) {
                            obj1[p] = obj2[p];
                        }
                    }
                }
            }
        }
        return obj1;
    }
};

export default MergeRecursively;