package com.getinsured.ssap.service

import com.getinsured.iex.ssap.*
import com.getinsured.iex.ssap.financial.Expense
import com.getinsured.iex.ssap.financial.Income
import com.getinsured.iex.ssap.financial.type.ExpenseType
import com.getinsured.iex.ssap.financial.type.Frequency
import com.getinsured.iex.ssap.financial.type.IncomeType
import com.getinsured.ssap.model.ComparisonResult
import com.getinsured.ssap.util.FamilyBuilder
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.slf4j.LoggerFactory
import java.util.*

/**
 * Unit tests to [SsapComparisonService].
 *
 * @author Yevgen Golubenko
 * @since 8/10/19
 */
class SsapComparisonServiceImplTest {
  companion object {
    private val log = LoggerFactory.getLogger(SsapComparisonServiceImplTest::class.java)
  }

  private val comparisonService = SsapComparisonServiceImpl()

  private var current: SingleStreamlinedApplication? = null
  private var old: SingleStreamlinedApplication? = null

  @Before
  fun setUp() {
    log.info("Setting up Ssap Comparison Api Test")
    current = SingleStreamlinedApplication()
    old = SingleStreamlinedApplication()
  }

  @Test
  fun compareDifferentNumberOfMembers() {
    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 2

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm1, hhm2)

    old?.taxHousehold = listOf(taxHousehold1)
    current?.taxHousehold = listOf(taxHousehold2)

    val comparisonResult = compare(current, old)

    Assert.assertNotNull("comparisonResult is $comparisonResult", comparisonResult)

    Assert.assertTrue("Comparison result should contain SSAC", comparisonResult.contains(ComparisonResult.SSAC))
    Assert.assertTrue("Comparison result should contain VLP", comparisonResult.contains(ComparisonResult.VLP))
    Assert.assertTrue("Comparison result should contain IFSV", comparisonResult.contains(ComparisonResult.IFSV))
    Assert.assertTrue("Comparison result should contain NON_ESI_MEC", comparisonResult.contains(ComparisonResult.NON_ESI_MEC))
    Assert.assertFalse("Comparison result should not contain NONE", comparisonResult.contains(ComparisonResult.NONE))
  }

  @Test
  fun compareSameFinancialFlag() {
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    hhm1.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm1)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should have 1 result", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])
  }

  @Test
  fun compareDifferentFinancialFlagNewIsTrue() {
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = false
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    hhm1.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm1)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val comparisonResult = compare(current, old)
    comparisonResult.forEach { cr ->
      Assert.assertNotEquals("comparisonResult should not contain: $cr", ComparisonResult.IFSV, cr)
      Assert.assertNotEquals("comparisonResult should not contain: $cr", ComparisonResult.NON_ESI_MEC, cr)
    }
  }

  @Test
  fun compareDifferentFinancialFlagNewIsFalse() {
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = false
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val comparisonResult = compare(current, old)
    comparisonResult.forEach { cr ->
      Assert.assertNotEquals("comparisonResult should not contain: $cr", ComparisonResult.IFSV, cr)
      Assert.assertNotEquals("comparisonResult should not contain: $cr", ComparisonResult.NON_ESI_MEC, cr)
    }
  }

  @Test
  fun compareForIfsv() {
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    val income = Income()
    income.amount = 35_000_00L
    income.type = IncomeType.JOB
    income.frequency = Frequency.YEARLY
    hhm1.incomes = listOf(income)

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    var comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.IFSV}", ComparisonResult.IFSV, comparisonResult[0])

    hhm2.incomes = listOf(income)
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    val expense = Expense()
    expense.amount = 1_500_00L
    expense.type = ExpenseType.ALIMONY
    expense.frequency = Frequency.MONTHLY
    hhm1.expenses = listOf(expense)

    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.IFSV}", ComparisonResult.IFSV, comparisonResult[0])

    hhm2.expenses = hhm1.expenses
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])
  }

  @Test
  fun compareNameSame() {
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val n1 = Name()
    n1.firstName = "Test"
    n1.lastName = "User"
    n1.middleName = "Middle"
    hhm1.name = n1

    val n2 = Name()
    n2.firstName = "Test"
    n2.lastName = "User"
    n2.middleName = "Middle"
    hhm2.name = n2

    var comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])
  }

  @Test
  fun compareNameDifferent() {
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val n1 = Name()
    n1.firstName = "Test"
    n1.lastName = "User"
    n1.middleName = "Middle"
    hhm1.name = n1

    val n2 = Name()
    n2.firstName = "New"
    n2.lastName = "User"
    n2.middleName = "Name"
    hhm2.name = n2

    var comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 4 items", 4, comparisonResult.size)
    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr != ComparisonResult.NONE
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }

    current?.applyingForFinancialAssistanceIndicator = false
    old?.applyingForFinancialAssistanceIndicator = false
    comparisonResult = compare(current, old)
    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr == ComparisonResult.SSAC ||
      cr == ComparisonResult.VLP
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }
  }


  @Test
  fun compareSsnDifferent() {
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val socialSecurityCard = SocialSecurityCard()
    socialSecurityCard.socialSecurityNumber = "555334411"
    hhm1.socialSecurityCard = socialSecurityCard

    val socialSecurityCard2 = SocialSecurityCard()
    socialSecurityCard2.socialSecurityNumber = "555334422"
    hhm2.socialSecurityCard = socialSecurityCard2

    var comparisonResult = compare(current, old)

    // For non-financial, we only should do SSAC
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.SSAC}", ComparisonResult.SSAC, comparisonResult[0])

    // For financial: SSAC, IFSV, Non-ESI MEC
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 3 items", 3, comparisonResult.size)

    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr == ComparisonResult.SSAC ||
      cr == ComparisonResult.IFSV ||
      cr == ComparisonResult.NON_ESI_MEC
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }
  }

  @Test
  fun compareSsnSame() {
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val socialSecurityCard = SocialSecurityCard()
    socialSecurityCard.socialSecurityNumber = "555334411"
    hhm1.socialSecurityCard = socialSecurityCard

    val socialSecurityCard2 = SocialSecurityCard()
    socialSecurityCard2.socialSecurityNumber = "555334411"
    hhm2.socialSecurityCard = socialSecurityCard2

    var comparisonResult = compare(current, old)

    // For non-financial:
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    // For financial:
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])
  }

  @Test
  fun compareDateOfBirthSame() {
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val dob = Date(2010, 2, 23)
    val dob2 = Date(2010, 2, 23)
    hhm1.dateOfBirth = dob
    hhm2.dateOfBirth = dob2

    var comparisonResult = compare(current, old)

    // For non-financial:
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    // For financial:
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])
  }

  @Test
  fun compareDateOfBirthDifferent() {
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val dob = Date(2010, 2, 23)
    val dob2 = Date(2011, 2, 23)
    hhm1.dateOfBirth = dob
    hhm2.dateOfBirth = dob2

    var comparisonResult = compare(current, old)

    // For non-financial:
    Assert.assertEquals("comparisonResult should contain 3 item", 2, comparisonResult.size)

    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr == ComparisonResult.SSAC ||
      cr == ComparisonResult.VLP
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }

    // For financial:
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 3 item", 3, comparisonResult.size)

    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr == ComparisonResult.SSAC ||
      cr == ComparisonResult.VLP ||
      cr == ComparisonResult.NON_ESI_MEC
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }
  }

  @Test
  fun compareSexSame() {
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    hhm1.gender = "Female"
    hhm2.gender = "fEmalE"
    var comparisonResult = compare(current, old)

    // For non-financial:
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    hhm1.gender = "M"
    hhm2.gender = "male"
    comparisonResult = compare(current, old)

    // For non-financial:
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    // For financial:
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])
  }


  @Test
  fun compareSexDifferent() {
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    hhm1.gender = "F"
    hhm2.gender = "M"
    var comparisonResult = compare(current, old)

    // For non-financial:
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    hhm1.gender = "M"
    hhm2.gender = "Female"
    comparisonResult = compare(current, old)

    // For non-financial we dont care about sex as per JIRA HIX-114995
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    // For financial only non-ESI MEC check lists sex for comparison. So if it's different result should have NON_ESI_MEC check,
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NON_ESI_MEC}", ComparisonResult.NON_ESI_MEC, comparisonResult[0])
  }

  @Test
  fun compareAddressDifferent() {
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val address = HomeAddress()
    address.addressId = 1
    address.streetAddress1 = "7291 S Las Vegas Blvd"
    address.streetAddress2 = null
    address.city = "Las Vegas"
    address.state = "NV"
    address.postalCode = "89119"
    address.county = "Ada"
    address.countyCode = "32003"
    address.primaryAddressCountyFipsCode = "16001"

    val address2 = HomeAddress()
    address2.addressId = 1
    address2.streetAddress1 = "7291 S Las Vegas Blvd"
    address2.streetAddress2 = "Apartment 102"
    address2.city = "Las Vegas"
    address2.state = "NV"
    address2.postalCode = "89119"
    address2.county = "Ada"
    address2.countyCode = "32003"
    address2.primaryAddressCountyFipsCode = "16001"

    val hhc1 = HouseholdContact()
    hhc1.homeAddress = address
    hhm1.householdContact = hhc1

    val hhc2 = HouseholdContact()
    hhc2.homeAddress = address2
    hhm2.householdContact = hhc2

    val comparisonResult = compare(current, old)
    Assert.assertNotNull("comparisonResult should not be null", comparisonResult)
    Assert.assertEquals("comparisonResult should have 1 value", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should have ${ComparisonResult.NON_ESI_MEC}", ComparisonResult.NON_ESI_MEC, comparisonResult[0])
  }

  @Test
  fun compareVlpRelatedInfoDifferent() {
    current?.applyingForFinancialAssistanceIndicator = true
    old?.applyingForFinancialAssistanceIndicator = true
    current?.primaryTaxFilerPersonId = 1
    old?.primaryTaxFilerPersonId = 1

    val taxHousehold1 = TaxHousehold()
    val taxHousehold2 = TaxHousehold()

    val hhm1 = HouseholdMember()
    val hhm2 = HouseholdMember()
    hhm1.personId = 1
    hhm2.personId = 1

    taxHousehold1.householdMember = listOf(hhm1)
    taxHousehold2.householdMember = listOf(hhm2)

    current?.taxHousehold = listOf(taxHousehold1)
    old?.taxHousehold = listOf(taxHousehold2)

    val cis = CitizenshipImmigrationStatus()
    val cis2 = CitizenshipImmigrationStatus()
    cis.citizenshipAsAttestedIndicator = true
    cis2.citizenshipAsAttestedIndicator = false

    hhm1.citizenshipImmigrationStatus = cis
    hhm2.citizenshipImmigrationStatus = cis2

    var comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should have 2 items", 2, comparisonResult.size)

    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr == ComparisonResult.SSAC ||
      cr == ComparisonResult.VLP
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }

    cis.citizenshipAsAttestedIndicator = false
    cis2.citizenshipAsAttestedIndicator = false
    comparisonResult = compare(current, old)
    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    val doc = CitizenshipDocument()
    val doc2 = CitizenshipDocument()

    doc.alienNumber = "670011845"
    doc.cardNumber = "OCM6060011846"
    doc.documentExpirationDate = Date(120, 3, 3)

    doc2.alienNumber = "670011845"
    doc2.cardNumber  = "OCM6060011846"
    doc2.documentExpirationDate = Date(122, 3,5)
    cis.citizenshipDocument = doc
    cis2.citizenshipDocument = doc2

    comparisonResult = compare(current, old)

    Assert.assertEquals("comparisonResult should contain 1 item", 2, comparisonResult.size)

    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr == ComparisonResult.SSAC ||
      cr == ComparisonResult.VLP
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }

    doc2.documentExpirationDate = Date(120, 3,3)
    comparisonResult = compare(current, old)

    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    // Add I-94 Number
    doc.i94Number = "1021293"
    comparisonResult = compare(current, old)

    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr == ComparisonResult.SSAC ||
      cr == ComparisonResult.VLP
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }
    // make i-94 the same
    doc2.i94Number = doc.i94Number
    comparisonResult = compare(current, old)

    Assert.assertEquals("comparisonResult should contain 1 item", 1, comparisonResult.size)
    Assert.assertEquals("comparisonResult should be only ${ComparisonResult.NONE}", ComparisonResult.NONE, comparisonResult[0])

    // Change alienNumber
    doc.alienNumber = "670011843"
    comparisonResult = compare(current, old)

    comparisonResult.containsAll<ComparisonResult>(ComparisonResult.values().filter {
      cr -> cr == ComparisonResult.SSAC ||
      cr == ComparisonResult.VLP
    }).let {
      Assert.assertTrue("comparisonResult does not contain expected result", it)
    }
  }

  @Test
  fun compareDifferentNumberOfMembers_fromJson() {
    val oneMemberSsap = FamilyBuilder.sampleApplicationFromJson("comparison/one-member.json")
    val twoMemberSsap = FamilyBuilder.sampleApplicationFromJson("comparison/two-members.json")


    Assert.assertNotNull("SSAP with 1 member should not be null", oneMemberSsap)
    Assert.assertNotNull("SSAP with 2 members should not be null", twoMemberSsap)
    Assert.assertEquals("SSAP with 1 member should have 1 member", 1, oneMemberSsap.taxHousehold[0].householdMember.size)
    Assert.assertEquals("SSAP with 2 members should have 2 member", 2, twoMemberSsap.taxHousehold[0].householdMember.size)

    val comparisonResult = compare(twoMemberSsap, oneMemberSsap)

    log.info("Comparison results between {} and {} => {}", oneMemberSsap.ssapApplicationId, twoMemberSsap.ssapApplicationId, comparisonResult)
    Assert.assertNotNull("comparisonResult is $comparisonResult", comparisonResult)

    Assert.assertTrue("Comparison result should contain SSAC", comparisonResult.contains(ComparisonResult.SSAC))
    Assert.assertTrue("Comparison result should contain VLP", comparisonResult.contains(ComparisonResult.VLP))
    Assert.assertTrue("Comparison result should contain IFSV", comparisonResult.contains(ComparisonResult.IFSV))
    Assert.assertTrue("Comparison result should contain NON_ESI_MEC", comparisonResult.contains(ComparisonResult.NON_ESI_MEC))
    Assert.assertFalse("Comparison result should not contain NONE", comparisonResult.contains(ComparisonResult.NONE))
  }

  private fun compare(one: SingleStreamlinedApplication?, two: SingleStreamlinedApplication?): List<ComparisonResult> {
    return one?.let {
      two?.let {
        it1 -> comparisonService.compare(it, it1) }
    }!!
  }
}
