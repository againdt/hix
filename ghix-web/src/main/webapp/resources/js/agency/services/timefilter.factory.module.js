(function(){
    'use strict';

    angular.module('timefilterFactoryModule', [])
    .filter('timeValidate', timeValidateFilter);

	function timeValidateFilter(){
        return function (input, filterCriteria) {

            if(filterCriteria && filterCriteria!== "select"){
                input = input || [];
                if (filterCriteria === 'closed') {
                  return ['closed'];
                }
                var out = [];
                input.forEach(function(time){
                    if(time !== 'select' && time !== 'closed') {
                        if(moment(time, 'h:mm a').isAfter(moment(filterCriteria, 'h:mm a'))){
                            if(time !== 'select' || time !== 'closed') {
                                out.push(time);
                            }
                        } 
                    } else {
                        out.push(time);
                    }
                });
                return out;
            }else {
                return input;
            }
		};
	}

})();
