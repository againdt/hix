<script type="text/ng-template" id="accordionTemplate.html">
<div class='accordion-group' id="myAppAccordion">
  <div class='accordion-heading'>
    <div class='accordion-toggle' data-toggle='collapse' style='background: #ededed; height:20px'><p>blah <em>blah</em> blah</p></div>
  </div>
  <div class='accordion-body collapse'>
    <div class='gutter10' ng-transclude><p>Medical <em>or</em> Dental</p></div>
  </div>
</div>
</script>

<script type="text/ng-template" id="accordionTemplate2.html">
<div class='accordion' ng-transclude><p>Medical <em>or</em> Dental</p></div>
</script>