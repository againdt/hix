package com.getinsured.consumer.service;

import java.util.Comparator;

import com.getinsured.hix.consumer.history.ConsumerHistoryBase;

class HistoryListComparator implements Comparator<ConsumerHistoryBase>{

	@Override
	public int compare(ConsumerHistoryBase o1, ConsumerHistoryBase o2) {
	
		
		if(o1 == null || o2 == null)
		{
			throw new NullPointerException("Could not compare o1 and o2");
		}
		return o2.getDate().compareTo(o1.getDate());
			
		
	}

}