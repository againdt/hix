/**
 * 
 */
package com.getinsured.eligibility.prescreen.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.estimator.mini.EligLead;

/**
 * Repository class for Elig Lead table
 * @author Nikhil Talreja
 * @since 14 August, 2013
 *
 */

@Repository
public interface IEligLeadRepository extends JpaRepository<EligLead, Long> {

	@Query("FROM EligLead as elig where elig.batchProcessed=:batchStage AND elig.createdByAPI=:api")
	List<EligLead> getAllOutboundCallLeads(@Param("batchStage") Long batchStage, @Param("api") String api);
}
