var incomesecion = {'Financial_Job':false, 'Financial_Self_Employment':false, 'Financial_SocialSecuritybenefits':false, 
			'Financial_Unemployment':false, 'Financial_Retirement_pension':false, 'Financial_Capitalgains':false,
			'Financial_InvestmentIncome':false, 'Financial_RentalOrRoyaltyIncome':false, 
			'Financial_FarmingOrFishingIncome':false, 'Financial_AlimonyReceived':false, 
			'Financial_OtherIncome':false};

/*
 * Save Data To JSON from Primary Contact Information Page
 */
function saveData53Json(){		
	if(useJSON == true){
	resetData53Json();
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	householdMemberObj.name.firstName = trimSpaces($('#firstName').val());
	householdMemberObj.name.middleName = trimSpaces($('#middleName').val());
	householdMemberObj.name.lastName = trimSpaces($('#lastName').val());
	householdMemberObj.name.suffix = $('#appscr53Suffix').val();
	householdMemberObj.dateOfBirth = DBDateformat($('#dateOfBirth').val());
	
	householdMemberObj.householdContact.contactPreferences.emailAddress = $('#emailAddress').val();
	householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage = $('#preffegred_spoken_language').val();
	householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage = $('#preffered_written_language').val();
	
	var preferredContactBy = "";
	
	if($("#email").is(":checked")) {
		preferredContactBy = "EMAIL";
		householdMemberObj.householdContact.contactPreferences.preferredContactMethod = "EMAIL";
	}
	
	if($("#inTheEmail").is(":checked")) {
		
		if(preferredContactBy != ""){
			preferredContactBy = preferredContactBy + " | POSTAL MAIL";
		}else {
			preferredContactBy = "POSTAL MAIL";			
		}
		
	}
	
	householdMemberObj.householdContact.contactPreferences.preferredContactMethod = preferredContactBy;
	
	if((householdMemberObj.householdContact.contactPreferences.preferredContactMethod == null) || (householdMemberObj.householdContact.contactPreferences.preferredContactMethod=="")) {
		householdMemberObj.householdContact.contactPreferences.preferredContactMethod = "EMAIL";
	}
	
	householdMemberObj.householdContact.homeAddressIndicator = true;
	householdMemberObj.householdContact.homeAddress.city = trimSpaces($('#home_primary_city').val());
	householdMemberObj.householdContact.homeAddress.county = $("#home_primary_county option:selected").text();
	householdMemberObj.householdContact.homeAddress.primaryAddressCountyFipsCode = $('#home_primary_county').val();
	householdMemberObj.householdContact.homeAddress.postalCode =  trimSpaces($('#home_primary_zip').val());
	householdMemberObj.householdContact.homeAddress.state = $('#home_primary_state').val();
	householdMemberObj.householdContact.homeAddress.streetAddress1 = trimSpaces($('#home_addressLine1').val());
	householdMemberObj.householdContact.homeAddress.streetAddress2 = trimSpaces($('#home_addressLine2').val());
	
	householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator = $('#mailingAddressIndicator').val();
	
	if($('#mailingAddressIndicator').is(':checked')){
		householdMemberObj.householdContact.mailingAddress.city = householdMemberObj.householdContact.homeAddress.city;
		householdMemberObj.householdContact.mailingAddress.county = householdMemberObj.householdContact.homeAddress.county;
		householdMemberObj.householdContact.mailingAddress.postalCode = householdMemberObj.householdContact.homeAddress.postalCode;
		householdMemberObj.householdContact.mailingAddress.state = householdMemberObj.householdContact.homeAddress.state;
		householdMemberObj.householdContact.mailingAddress.streetAddress1 = householdMemberObj.householdContact.homeAddress.streetAddress1;
		householdMemberObj.householdContact.mailingAddress.streetAddress2 = householdMemberObj.householdContact.homeAddress.streetAddress2;
	}else{
		householdMemberObj.householdContact.mailingAddress.city = trimSpaces($('#mailing_primary_city').val());
		householdMemberObj.householdContact.mailingAddress.county = $('#mailing_primary_county').val();
		householdMemberObj.householdContact.mailingAddress.postalCode = trimSpaces($('#mailing_primary_zip').val());
		householdMemberObj.householdContact.mailingAddress.state = $('#mailing_primary_state').val();
		householdMemberObj.householdContact.mailingAddress.streetAddress1 = trimSpaces($('#mailing_addressLine1').val());
		householdMemberObj.householdContact.mailingAddress.streetAddress2 = trimSpaces($('#mailing_addressLine2').val());
	}
	
	
	householdMemberObj.householdContact.phone.phoneNumber = $('#first_phoneNo').val().replace(/\D+/g, "");
	householdMemberObj.householdContact.phone.phoneExtension = $('#first_ext').val();
	//householdMemberObj.householdContact.phone.phoneType = $('#first_phoneType').val();
	householdMemberObj.householdContact.phone.phoneType = "CELL";
	
	householdMemberObj.householdContact.otherPhone.phoneNumber = $('#first_homePhoneNo').val().replace(/\D+/g, "");
	householdMemberObj.householdContact.otherPhone.phoneExtension = $('#first_homeExt').val();
	//householdMemberObj.householdContact.otherPhone.phoneType = $('#second_phoneType').val();
	householdMemberObj.householdContact.otherPhone.phoneType = "HOME";	
	
	
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = householdMemberObj;	
	}
	
	
}

function saveData54Json(){
	if(useJSON == true){
	resetData54Json();

	var helpType = webJson.singleStreamlinedApplication.helpType;
	
	
	//get help from broker
	if(helpType == "BROKER"){
		BrokerObj=webJson.singleStreamlinedApplication.broker;
		
		BrokerObj.brokerFederalTaxIdNumber = $('#brokerID54').val();
		BrokerObj.brokerName = $('#brokerName54').val();
		BrokerObj.internalBrokerId = parseInt($('#internalBrokerId').val());
		webJson.singleStreamlinedApplication.broker = BrokerObj;
		
		webJson.singleStreamlinedApplication.getHelpIndicator = true;
	//get help from assister	
	}else if(helpType == "ASSISTER"){
		assisterObj=webJson.singleStreamlinedApplication.assister;
		
		assisterObj.assisterID = $('#guideID54').val();
		assisterObj.assisterName = $('#guideName54').val();
		webJson.singleStreamlinedApplication.broker = assisterObj;
		
		webJson.singleStreamlinedApplication.getHelpIndicator = true;
	}else{
		if($('input[name=authorizedRepresentativeHelp]:radio:checked').val() == 'yes'){
			webJson.singleStreamlinedApplication.getHelpIndicator = true;
		}else if($('input[name=authorizedRepresentativeHelp]:radio:checked').val() == 'no'){
			webJson.singleStreamlinedApplication.getHelpIndicator = false;	
			webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator = false;
		}
	}
	
	if(webJson.singleStreamlinedApplication.getHelpIndicator === true){
		if($('input[name=authorizedRepresentativeIndicator]:radio:checked').val() == 'yes'){
			webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator = true;
		}else if($('input[name=authorizedRepresentativeIndicator]:radio:checked').val() == 'no'){
			webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator = false;		
		}
	}
	
	if(webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator === true){
		AuthorizedRepresentativeObj = webJson.singleStreamlinedApplication.authorizedRepresentative;
		
		AuthorizedRepresentativeObj.name.firstName = $('#authorizedFirstName').val();
		AuthorizedRepresentativeObj.name.middleName = $('#authorizedMiddleName').val();
		AuthorizedRepresentativeObj.name.lastName = $('#authorizedLastName').val();
		AuthorizedRepresentativeObj.name.suffix = $('#authorizedSuffix').val();
		
		AuthorizedRepresentativeObj.emailAddress = $('#authorizedEmailAddress').val();
		
		AuthorizedRepresentativeObj.address.streetAddress1 = $('#authorizedAddress1').val();
		AuthorizedRepresentativeObj.address.streetAddress2 = $('#authorizedAddress2').val();
		AuthorizedRepresentativeObj.address.city = $('#authorizedCity').val();
		AuthorizedRepresentativeObj.address.state = $('#authorizedState').val();
		AuthorizedRepresentativeObj.address.postalCode = $('#authorizedZip').val();
		
		//cell
		AuthorizedRepresentativeObj.phone[0].phoneNumber = $('#appscr54_phoneNumber').val().replace(/\D+/g, "");
		AuthorizedRepresentativeObj.phone[0].phoneExtension = $('#appscr54_ext').val();
		
		//home
		AuthorizedRepresentativeObj.phone[1].phoneNumber = $('#appscr54_homePhoneNumber').val().replace(/\D+/g, "");
		AuthorizedRepresentativeObj.phone[1].phoneExtension = $('#appscr54_homeExt').val();

		
		//work
		AuthorizedRepresentativeObj.phone[2].phoneNumber = $('#appscr54_workPhoneNumber').val().replace(/\D+/g, "");
		AuthorizedRepresentativeObj.phone[2].phoneExtension = $('#appscr54_workExt').val();
		
		
		if($('input[name=isPersonHelpingyou4ApplingHealthInsurance]:radio:checked').val() == 'yes'){
			AuthorizedRepresentativeObj.partOfOrganizationIndicator = true;
			
			AuthorizedRepresentativeObj.companyName = $('#authorizeCompanyName').val();
			AuthorizedRepresentativeObj.organizationId = $('#authorizeOrganizationId').val();
			
		}else if($('input[name=isPersonHelpingyou4ApplingHealthInsurance]:radio:checked').val() == 'no'){
			AuthorizedRepresentativeObj.partOfOrganizationIndicator = false;			
		}
		
		
		
		if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'signature'){
			AuthorizedRepresentativeObj.signatureisProofIndicator = true;
			AuthorizedRepresentativeObj.signature = $("#authorizeSignature").val();
		}else if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'later'){
			AuthorizedRepresentativeObj.signatureisProofIndicator = false;			
		}
		
		webJson.singleStreamlinedApplication.authorizedRepresentative=AuthorizedRepresentativeObj;
	}	
	
	}
	
}

function saveData55Json(){
	if(useJSON == true){
		resetData55Json();
		webJson.singleStreamlinedApplication.applyingForhouseHold = $('input[name=ApplyingForhouseHold]:radio:checked').val();	
		if($('input[name=wantToGetHelpPayingHealthInsurance]:radio:checked').val() == 'Yes')
			householdMemberObj.applyingForFinancialAssistanceIndicator = true;
		else if($('input[name=wantToGetHelpPayingHealthInsurance]:radio:checked').val() == 'No')
			householdMemberObj.applyingForFinancialAssistanceIndicator = false;
		
		checkFinacialAssistance();
		
		//webJson.singleStreamlinedApplication = householdMemberObj;
	}
	
}

function continueData60Json(i){
	if(useJSON == true){
	resetData60Json(i);
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	
	if($('input[name=planToFileFTRIndicator]:radio:checked').val() == 'yes'){
		householdMemberObj.planToFileFTRIndicator = true;
	}else if($('input[name=planToFileFTRIndicator]:radio:checked').val() == 'no'){
		householdMemberObj.planToFileFTRIndicator = false;
	}else{
		householdMemberObj.planToFileFTRIndicator = null;
	}
	
	
	if($('input[name=marriedIndicator]:radio:checked').val() == 'yes'){
		householdMemberObj.marriedIndicator =  true;
	}else if($('input[name=marriedIndicator]:radio:checked').val() == 'no'){
		householdMemberObj.marriedIndicator = false;
	}else{
		householdMemberObj.marriedIndicator = null;
	}	
	
	

	if(householdMemberObj.planToFileFTRIndicator === true && householdMemberObj.marriedIndicator ===  true && $('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'yes' ){
		householdMemberObj.planToFileFTRJontlyIndicator = true;
	}else if(householdMemberObj.planToFileFTRIndicator === true && householdMemberObj.marriedIndicator ===  true && $('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'no' ){
		householdMemberObj.planToFileFTRJontlyIndicator = false;
	}else{
		householdMemberObj.planToFileFTRJontlyIndicator = null;		
	}
	
	
	
	if(householdMemberObj.marriedIndicator ===  true && $('input[name=liveWithSpouseIndicator]:radio:checked').val() == 'yes'){
		householdMemberObj.taxFiler.liveWithSpouseIndicator = true;
	}else if(householdMemberObj.marriedIndicator ===  true && $('input[name=liveWithSpouseIndicator]:radio:checked').val() == 'no'){
		householdMemberObj.taxFiler.liveWithSpouseIndicator = false;
	}else{
		householdMemberObj.taxFiler.liveWithSpouseIndicator = null;
	}
	
	//householdMemberObj.taxFiler.spouseHouseholdMemberId = $('input[name=householdContactSpouse]:radio:checked').val();
		
	if(householdMemberObj.planToFileFTRIndicator === true && $('input[name=householdHasDependant]:radio:checked').val() == 'yes'){
		householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator = true;
	}else if(householdMemberObj.planToFileFTRIndicator === true && $('input[name=householdHasDependant]:radio:checked').val() == 'no'){
		householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator = false;
	}else{
		householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator = null;
	}
	
	
	if(!householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator && $('input[name=doesApplicantHasTaxFiller]:radio:checked').val() == 'yes'){
		householdMemberObj.taxFilerDependant.taxFilerDependantIndicator = true;
	}else if(!householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator && $('input[name=doesApplicantHasTaxFiller]:radio:checked').val() == 'no'){
		householdMemberObj.taxFilerDependant.taxFilerDependantIndicator = false;
	}else{
		householdMemberObj.taxFilerDependant.taxFilerDependantIndicator = null;
	}
	
	
	var count = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	
	
	/*****************************************************************************************************************************************/
	/********************************************************Update primary tax filer Id*****************************************************/
	/***************************************************************************************************************************************/
	//TF is decided in PC page
	if(HouseHoldRepeat == 1){
		//primary contact is tax filer
		if(householdMemberObj.planToFileFTRIndicator === true && !householdMemberObj.taxFilerDependant.taxFilerDependantIndicator){
			webJson.singleStreamlinedApplication.primaryTaxFilerPersonId = parseInt(i);
		}else if(householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === true){
			//Primary Contact is not a tax filer and being claimed as a dependent of someone who is part of the household
			//add one more condition is because "Will [] be claimed" will be asked even PC is TF
			var taxFilerDependantId = parseInt($('input[name=householdContactFiler]:radio:checked').attr('id').substring(21));
			//if NaN, that means someone else not in family household, we set it to -1
			if(!isNaN(taxFilerDependantId)){
				webJson.singleStreamlinedApplication.primaryTaxFilerPersonId =  taxFilerDependantId;
			}else{
				webJson.singleStreamlinedApplication.primaryTaxFilerPersonId = -1;
			}
		}else{
			webJson.singleStreamlinedApplication.primaryTaxFilerPersonId = 0;
		}
			
	}
	
	
	
	/*****************************************************************************************************************************************/
	/********************************************************Update primary tax filer Id End*************************************************/
	/***************************************************************************************************************************************/
	
	
	/*****************************************************************************************************************************************/
	/***********************************************************Update spouse Id*************************************************************/
	/***************************************************************************************************************************************/
	//get spouse
	if(householdMemberObj.marriedIndicator ===  true){
		//get spouse ID
		var spouseId = parseInt($('input[name=householdContactSpouse]:radio:checked').attr('id').substring(22));
		
		//if spouse is changed on PC page, we need to reset the spouse for all applicants because if PC is not TF then TF's spouse will be changed base on PC's spouse
		if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.spouseHouseholdMemberId != spouseId){
			for(var n=1; n<=count ; n++){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[n-1].taxFiler.spouseHouseholdMemberId = 0;
			}
		}
		
		//if spouse is part of family
		if(!isNaN(spouseId)){
			householdMemberObj.taxFiler.spouseHouseholdMemberId = spouseId;
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[spouseId-1].taxFiler.spouseHouseholdMemberId = householdMemberObj.personId;	
		}else{
			//if spouse is NOT part of family
			//set PC's spouse Id to -1 
			householdMemberObj.taxFiler.spouseHouseholdMemberId = -1;
			
			//if PC already has a spouse who is part of family before, set spouse IDs to 0 for that spouse
			if(householdMemberObj.taxFiler.spouseHouseholdMemberId != 0 && householdMemberObj.taxFiler.spouseHouseholdMemberId != -1){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemberObj.taxFiler.spouseHouseholdMemberId-1].taxFiler.spouseHouseholdMemberId = 0;	
			}
		}
	}else{
		
		//if spouse is changed on PC page, we need to reset the spouse for all applicants because if PC is not TF then TF's spouse will be changed base on PC's spouse
		if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.spouseHouseholdMemberId != 0){
			for(var n=1; n<=count ; n++){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[n-1].taxFiler.spouseHouseholdMemberId = 0;
			}
		}else if(householdMemberObj.taxFiler.spouseHouseholdMemberId !=0){
			//if PC is not TF, only remove TF's spouse id and keep PC's spouse
			var temp = householdMemberObj.taxFiler.spouseHouseholdMemberId;
			householdMemberObj.taxFiler.spouseHouseholdMemberId = 0;
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[temp-1].taxFiler.spouseHouseholdMemberId = 0;	
		};
		
		//if PC is TF(for back situation), remove all spouse id
		/*if(HouseHoldRepeat == 1 && householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === false){
			for(var j=1; j<=count; j++){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[j-1].taxFiler.spouseHouseholdMemberId = 0;
			}
		}else if(householdMemberObj.taxFiler.spouseHouseholdMemberId !=0){
			//if PC is not TF, only remove TF's spouse id
			var temp = householdMemberObj.taxFiler.spouseHouseholdMemberId;
			householdMemberObj.taxFiler.spouseHouseholdMemberId = 0;
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[temp-1].taxFiler.spouseHouseholdMemberId = 0;	
		};
*/
	}
	
	
	/*****************************************************************************************************************************************/
	/***********************************************************Update spouse Id End*********************************************************/
	/***************************************************************************************************************************************/
	
	
	/*****************************************************************************************************************************************/
	/********************************************Update dependents Id and seeking coverage indicator*****************************************/
	/***************************************************************************************************************************************/
	
	//Update array when TF and claim dependents 
	if(householdMemberObj.planToFileFTRIndicator === true && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == true){
		//remove previous dependents
		webJson.singleStreamlinedApplication.taxFilerDependants = [];
		
		//update dependents and their coverage indicator
		$("#householdDependantDiv").find("input").each(function(){
			var dependantId = parseInt($(this).attr("id").substring(25));
			if($(this).is(":checked")){				
				if(!isNaN(dependantId) && webJson.singleStreamlinedApplication.taxFilerDependants.indexOf(dependantId) == -1){
					webJson.singleStreamlinedApplication.taxFilerDependants.push(dependantId);			
					
					//check applyingForCoverageIndicatorArrCopy
					if(applyingForCoverageIndicatorArrCopy[i-1] === true){
						webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependantId-1].applyingForCoverageIndicator = true;
					}
				}
			}else if(!isNaN(dependantId)){
				//remove unchecked dependent from array
				var dependantIndex = webJson.singleStreamlinedApplication.taxFilerDependants.indexOf(dependantId);
				if(dependantIndex != -1) {
					webJson.singleStreamlinedApplication.taxFilerDependants.splice(dependantIndex, 1);
				}
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependantId-1].applyingForCoverageIndicator = false;
			}
		});
		
		// if TF is not PC, that means he/she is not in dependent list, so we need to update its applyingForCoverageIndicator manually 
		if(HouseHoldRepeat != 1){
			if(applyingForCoverageIndicatorArrCopy[HouseHoldRepeat-1] === true){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[HouseHoldRepeat-1].applyingForCoverageIndicator = true;
			}
		}
		
	//empty array when not TF and not be claimed
	//empty array when TF and not to claim other people		
	}else if((householdMemberObj.planToFileFTRIndicator === true && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator === false && householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === false) || (householdMemberObj.planToFileFTRIndicator === false && householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === false)){
		//empty dependents
		webJson.singleStreamlinedApplication.taxFilerDependants = [];
		
		//NEVER CHANGE PC'S SEEKING COVERAGE INDICATOR
		
		//update seeking coverage indicator to false for all people except PC
		//this situation only happen on HouseHoldRepeat = 1
		/*if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == false){*/
			for(var k=2;k<=count;k++){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[k-1].applyingForCoverageIndicator = false;					
				
			}
		/*}*/
		
	//otherwise don't change array	
	}else{
		
	}
	
	
	
	/*
	//collect dependents ID only on TF tax page, and update seeking coverage indicator
	if(householdMemberObj.planToFileFTRIndicator == true && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == true){
		//update dependents and their coverage indicator
		if(householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == true){
			$("#householdDependantDiv").find("input").each(function(){
				var dependantId = parseInt($(this).attr("id").substring(25));
				if($(this).is(":checked")){				
					if(!isNaN(dependantId) && webJson.singleStreamlinedApplication.taxFilerDependants.indexOf(dependantId) == -1){
						webJson.singleStreamlinedApplication.taxFilerDependants.push(dependantId);			
						
						//check applyingForCoverageIndicatorArrCopy
						if(applyingForCoverageIndicatorArrCopy[i-1] == true){
							webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependantId-1].applyingForCoverageIndicator = true;
						}
					}
				}else if(!isNaN(dependantId)){
					//remove unchecked dependent from array
					var dependantIndex = webJson.singleStreamlinedApplication.taxFilerDependants.indexOf(dependantId);
					if(dependantIndex != -1) {
						webJson.singleStreamlinedApplication.taxFilerDependants.splice(dependantIndex, 1);
					}
					webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependantId-1].applyingForCoverageIndicator = false;
				}
			});
			
			//need to update applyingForCoverageIndicator flag if TF is not PC
			if(HouseHoldRepeat != 1){
				if(applyingForCoverageIndicatorArrCopy[HouseHoldRepeat-1] == true){
					webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[HouseHoldRepeat-1].applyingForCoverageIndicator = true;
				}
			}
			
			
		}else{
			//dependent indicator is false, that means no dependent
			webJson.singleStreamlinedApplication.taxFilerDependants = [];
			
			//if this is PC, planToFileFTRIndicator = false, and will not be claimed
			//update seeking coverage indicator to false for all people except PC
			if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == false){
				for(var k=2;k<=count;k++){
					webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[k-1].applyingForCoverageIndicator = false;					
					
				}
			}
		}
		
		//update spouses and their coverage indicator
		if(householdMemberObj.planToFileFTRIndicator != true && householdMemberObj.taxFiler.spouseHouseholdMemberId > 0){
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemberObj.taxFiler.spouseHouseholdMemberId-1].applyingForCoverageIndicator = false;
		}
		
	}else{
		//remove dependents
		webJson.singleStreamlinedApplication.taxFilerDependants = [];	
		
		//if this is PC, planToFileFTRIndicator = false, and will not be claimed
		//update seeking coverage indicator to false for all people except PC
		if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == false){
			for(var k=2;k<=count;k++){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[k-1].applyingForCoverageIndicator = false;					
				
			}
		}
	}*/
	
	//if TF file a joint tax, and spouse is part of household, and spouse will seeking coverage, then update spouse's indicator to true
	if(householdMemberObj.planToFileFTRJontlyIndicator === true && householdMemberObj.taxFiler.spouseHouseholdMemberId > 0 && applyingForCoverageIndicatorArrCopy[householdMemberObj.taxFiler.spouseHouseholdMemberId-1] === true){
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemberObj.taxFiler.spouseHouseholdMemberId - 1].applyingForCoverageIndicator = true;	
	}else if(householdMemberObj.planToFileFTRJontlyIndicator === false && householdMemberObj.taxFiler.spouseHouseholdMemberId > 0){
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemberObj.taxFiler.spouseHouseholdMemberId - 1].applyingForCoverageIndicator = false;
	}
	
	/*****************************************************************************************************************************************/
	/********************************************Update dependents Id and seeking coverage indicator End*************************************/
	/***************************************************************************************************************************************/
	
	/*	
	householdMemberObj.livesWithHouseholdContactIndicator = false;
	if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'yes'){
		householdMemberObj.livesWithHouseholdContactIndicator = true;
	}
	*/
	
	//householdMemberObj.taxFiler.spouseHouseholdMemberId = false;
	
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1] = householdMemberObj;
	
	//var spouseid = householdMemberObj.taxFiler.spouseHouseholdMemberId.charAt(9);
	spouseObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	spousename = spouseObj.name.firstName + " " +  spouseObj.name.middleName + " " + spouseObj.name.lastName;
	if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'yes' && $('input[name=marriedIndicator]:radio:checked').val() == 'yes'){
		$(".householdContactSpouseName").text("and "+ spousename);
	}
	
	
	//var planOnFilingJointFTRIndicator = $('input[name=planOnFilingJointFTRIndicator]:radio:checked').val();
	//var householdContactSpouse = $('input[name=householdContactSpouse]:radio:checked').val();
		
	//var householdHasDependant = $('input[name=householdHasDependant]:radio:checked').val();
	//var doesApplicantHasTaxFiller = $('input[name=doesApplicantHasTaxFiller]:radio:checked').val();
	
	}
	
}

function continueData61Json(currentID){	
	if(useJSON == true){
	resetData61Json(currentID);
	
	householdMemberObj.gender = $('input[name=appscr61_gender]:radio:checked').val();	
	
	if($('input[name=socialSecurityCardHolderIndicator]:radio:checked').val() == 'yes'){
		householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator = true;
		householdMemberObj.socialSecurityCard.socialSecurityNumber = $('#ssn').attr('data-ssn');
		if($('input[name=fnlnsSame]:radio:checked').val() == 'yes'){
			householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator = true;
		}else if($('input[name=fnlnsSame]:radio:checked').val() == 'no'){
			householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator = false;
			
			householdMemberObj.socialSecurityCard.firstNameOnSSNCard = $('#firstNameOnSSNCard').val();	
			householdMemberObj.socialSecurityCard.middleNameOnSSNCard = $('#middleNameOnSSNCard').val();	
			householdMemberObj.socialSecurityCard.lastNameOnSSNCard = $('#lastNameOnSSNCard').val();
			householdMemberObj.socialSecurityCard.suffixOnSSNCard = $('#suffixOnSSNCard').val();
		}	
	}else if($('input[name=socialSecurityCardHolderIndicator]:radio:checked').val() == 'no'){
		householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator = false;
		householdMemberObj.socialSecurityCard.socialSecurityNumber = '';
		householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN = $('#reasonableExplanationForNoSSN').val();
	}	
	if($('input[name=fnlnsExemption]:radio:checked').val() == 'yes'){
		householdMemberObj.socialSecurityCard.individualMandateExceptionIndicator = true;
	} else if($('input[name=fnlnsExemption]:radio:checked').val() == 'no'){
		householdMemberObj.socialSecurityCard.individualMandateExceptionIndicator = false;
	}
	householdMemberObj.socialSecurityCard.individualManadateExceptionID = $('#exemptionId').val();
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;	
	
	//var socialSecurityNameSuffix = $('#suffixOnSSNCard').val();	
	//var fnlnsExemption = $('input[name=fnlnsExemption]:radio:checked').val();
	//var exemptionId = $('#exemptionId').val();
	}
	
}

function continueData62Part1Json(currentID){
	if(useJSON == true){
	
		//before set new value and saving, assigan default value		
		var defaultJson = getDefatulJson(); // get default json		
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]; 
		//Assign default value
		householdMemberObj.citizenshipImmigrationStatus = defaultJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].citizenshipImmigrationStatus;
		
/*	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];	
	householdMemberObj.citizenshipImmigrationStatus.citizenshipManualVerificationStatus = $('input[name=UScitizenManualVerification]:radio:checked').val();	
	householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val();	
	householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val();	*/
	
	
	/*25 April*/
	//householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]; 
	 
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator = $('input[name=UScitizen]:radio:checked').val() == "true" ? true : false ;
	 
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipAsAttestedIndicator = $('input[name=UScitizen]:radio:checked').val() == "true" ? true : false ;
	 
	 if( householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator == true) return;	 
	// if(householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator)
	 
	 //suneel 05/09/2014
	 //householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]').val()== "true" ? true : false ;	 
	 householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val() == "true" ? true : false ;
	 
	 householdMemberObj.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator = $('input[name=livesInUS]:radio:checked').val() == "true" ? true : false ;
	 
	 //suneel:05/09/2014
	 //householdMemberObj.citizenshipImmigrationStatus.honorablyDischargedOrActiveDutyMilitaryMemberIndicator = $('input[name=honourably-veteran]').val()== "true" ? true : false ;
	 householdMemberObj.citizenshipImmigrationStatus.honorablyDischargedOrActiveDutyMilitaryMemberIndicator = $('input[name=honourably-veteran]:radio:checked').val()== "true" ? true : false ;
	 
	 //suneel:05/09/2014
	 //householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator = $('input[name=EligibleImmigrationStatuscheckbox]').is(':checked') == true  ? true : false;
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator = $('input[name=EligibleImmigrationStatuscheckbox]').is(':checked') == true  ? true : false;
	 
	 //householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I551Indicator = $('input[name=EligibleImmigrationStatus]:radio:checked').val() == true  ? true : false;
	 /*	input-permcarddetails
		arrivaldeprecordForeignPassportexpdate
		arrivaldeprecordForeignppcardno
		arrivaldeprecordForeignSevisID
		arrivaldeprecordForeignCounty
		arrivaldeprecordForeignI94no
		arrivaldeprecordForeignppvisano*/
	 
	 var docType =  $('input[name=docType]:radio:checked').attr('id');

	 if(docType == undefined) docType =  $('input[name=documentType]:radio:checked').attr('id');
	 
	 if(docType == "permcarddetails"){
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I551";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I551Indicator = true;// remove  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());   
	  
	 }		 
	 else if(docType == "tempcarddetails"){
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "TempI551";
		 
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator = true;// remove  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();		  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"Passportexpdate").val());
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();
	 }	 
	 else if(docType == "machinecarddetails"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "MacReadI551";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].MachineReadableVisaIndicator = true;// remove  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber = $("#"+docType+"ppvisano").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val(); 		  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat( $("#"+docType+"dateOfExp").val());		  
			
		  // add passport expiration date later
		 }
	 
	 else if(docType == "empauthcarddetails"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I766";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I766Indicator = true;// remove  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());  				  
		 }	 
	 else if(docType == "arrivaldeparturerecord"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I94";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94Indicator = true;  // remove
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94no").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"I94no").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());  		  
		 }	 
	 else if(docType == "arrivaldeprecordForeign"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I94UnexpForeignPassport";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94InPassportIndicator = true;  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94no").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();    	
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber = $("#"+docType+"ppvisano").val();  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val(); 
		 }	 
	 else if(docType == "NoticeOfAction"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I797";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I797Indicator = true; // remove
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94").val();
		 } 
	 else if(docType == "ForeignppI94"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "UnexpForeignPassport";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].UnexpiredForeignPassportIndicator = true;    // remove
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"AlignNumber").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();    	
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val()); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val(); 
		 } 
	 else if(docType == "rentrypermit"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I327";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I327Indicator = true;  // remove 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val()); 
		 } 
	 else if(docType == "refugeetraveldocI-571"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I571";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I571Indicator = true;   // remove
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val()); 
		 } 
	 else if(docType == "certificatenonimmigrantF1-Student-I20"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I20";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I20Indicator = true;   // remove
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val();   
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94no").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();		  
		 }  
	 else if(docType == "certificatenonimmigrantJ1-Student-DS2019"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "DS2019";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].DS2019Indicator = true;  // remove 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val();   
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94no").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
	} 	  
	 else if(docType == "docType_other"){
		 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "OtherCase1";
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].OtherDocumentTypeIndicator = true;  // remove
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#DocAlignNumber").val();  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#DocI-94Number").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#passportDocNumber").val();
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#passportExpDate").val());  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#sevisIDNumber").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentDescription = $("#documentDescription").val();
	}
	 else if(docType == "naturalizedCitizenNaturalizedIndicator"){
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "NaturalizationCertificate";
		  
		  //householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator = true;// remove  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#naturalizationAlignNumber").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId  = $("#naturalizationCertificateNumber").val();    
	  
	 }
	 else if(docType == "naturalizedCitizenNaturalizedIndicator2"){
		  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "CitizenshipCertificate";
		  
		  //householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator = true;// remove  
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#citizenshipAlignNumber").val(); 
		  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId  = $("#appscr62p1citizenshipCertificateNumber").val();    
	  
	 }
	 
	// other documents
	 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORRCertificationIndicator =  $('#otherDocORR').is(':checked');
	 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORREligibilityLetterIndicator =  $('#otherDocORR-under18').is(':checked');
	 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.CubanHaitianEntrantIndicator =  $('#Cuban-or-HaitianEntrant').is(':checked');
	 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.WithholdingOfRemovalIndicator =  $('#docRemoval').is(':checked');
	 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.AmericanSamoanIndicator =  $('#americanSamaoresident').is(':checked');
	 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.StayOfRemovalIndicator =  $('#adminOrderbyDeptofHomelanSecurity').is(':checked');
	
	// name same on the documents
	 var isNameSameOnDocument;
	 if($('#62Part2_UScitizenYes').is(':checked')){
		 isNameSameOnDocument = true;
	 }else if($('#62Part2_UScitizen').is(':checked')){
		 isNameSameOnDocument = false;
	 }else{
		 isNameSameOnDocument = null;
	 }
	 
	 
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator = isNameSameOnDocument;
	 if(isNameSameOnDocument == false) {
		 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName = $("#documentFirstName").val();
		 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName = $("#documentMiddleName").val();
		 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName = $("#documentLastName").val();
		 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix = $("#documentSuffix").val()=="0"?"":$("#documentSuffix").val();		 
		 
	 }
	 else {
		 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName = "";
		 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName = "";
		 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName = "";
		 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix = "";
	 }
	
	
	
	
/*	input-permcarddetails
	arrivaldeprecordForeignPassportexpdate
	arrivaldeprecordForeignppcardno
	arrivaldeprecordForeignSevisID
	arrivaldeprecordForeignCounty
	arrivaldeprecordForeignI94no
	arrivaldeprecordForeignppvisano*/
	
	//var checkboxid = $('input[name=UScitizenManualVerification]:radio:checked').attr('id');
	var checkboxid = $('input[name=docType]:radio:checked').attr('id');
	
	
	
	
	
/*		
	var AlignNumber = $('#'+checkboxid+"AlignNumber").val();
	var I94no = $('#'+checkboxid+"I94no").val();
	var ppcardno = $('#'+checkboxid+"ppcardno").val();
	var County = $('#'+checkboxid+"County").val();
	var SevisID = $('#'+checkboxid+"SevisID").val();
	var ppvisano = $('#'+checkboxid+"ppvisano").val();
	var Passportexpdate = $('#'+checkboxid+"Passportexpdate").val();	
	
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = AlignNumber;
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = I94no;
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = ppcardno;
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = County;	
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.sevisId = SevisID;
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber = ppvisano;
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = Passportexpdate;*/
	
	
	
	
	
	
	
	
	//webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = householdMemberObj;	 //suneel 05/09/2014
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;	
	
/*	var inputvalues=[];
	$('input[name=docType]:radio:checked').each(function(){
		  var type = $(this).attr("type");
	        if ((type == "checkbox" || type == "radio") && $(this).is(":checked")) {
	            inputValues.push($(this).val());
	        }		
	});
	
	
	$('input[name=docType]:radio:checked').parent().next('div').find('.controls').each(function(){
		$(this).child('input[type=text]').val();
	});*/
	
	
	//FOLLOWING DATA PROPERTIES NOT FOUND IN JSON
	
	//var UScitizen = $('input[name=UScitizen]:radio:checked').val();
	//var naturalizedCitizen = $('input[name=naturalizedCitizen]:radio:checked').val();
	//var naturalizationAlignNumber = $('#naturalizationAlignNumber').val();
	//var naturalizationCertificateNumber = $('#naturalizationCertificateNumber').val();
	//var naturalizedDonotHave = $('#naturalizedDonotHave').is(':checked');
	//var citizenshipAlignNumber = $('#citizenshipAlignNumber').val();
	//var appscr62p1citizenshipCertificateNumber = $('#appscr62p1citizenshipCertificateNumber').val();
	//var citizenDonotHave = $('#citizenDonotHave').is(':checked');
	//var naturalCitizen_eligibleImmigrationStatus = $('#naturalCitizen_eligibleImmigrationStatus').is(':checked');
	//var docType = $('input[name=docType]:radio:checked').val();
	//var passportExpDate = $('#passportExpDate').val();
	//var documentDescription = $('#documentDescription').text();
	}
	
} 

function continueData62Part2Json(){	
	if(useJSON == true){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber =$('#appscr62OtherDocAlignNumber').val(); 
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number =$('#appscr62P2I-94Number').val();	
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName = $('#documentFirstName').val();
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName = $('#documentMiddleName').val();
	householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName = $('#documentLastName').val();	
	householdMemberObj.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator = $('input[name=livedIntheUSSince1996Indicator]:radio:checked').val();	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0]=householdMemberObj;	
	
	//FOLLOWING DATA PROPERTIES NOT FOUND IN JSON
	
	//var docType1 = $('input[name=docType1]:radio:checked').val();
	
	//var appscr62OtherComment = $('#appscr62OtherComment').text();
	//var UScitizen62 = $('input[name=UScitizen62]:radio:checked').val();
	
	//var documentSuffix = $('#documentSuffix').val();
	//var livesInUS = $('input[name=livesInUS]:radio:checked').val();
	//var livedIntheUSSince1996Indicator = $('input[name=livedIntheUSSince1996Indicator]:radio:checked').val();
	}	
	
}

function continueData63Json(currentMemember){
	
if(useJSON == true){
		
		var cnt = currentMemember;
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		
		if($('input[name=appscr63AtLeastOneChildUnder19Indicator]:radio:checked').val() == 'yes'){
			householdMemberObj.parentCaretakerRelatives.mainCaretakerOfChildIndicator = true;
		} else if($('input[name=appscr63AtLeastOneChildUnder19Indicator]:radio:checked').val() == 'no'){
			householdMemberObj.parentCaretakerRelatives.mainCaretakerOfChildIndicator = false;
		}
		
		//householdMemberObj.parentCaretakerRelatives.personId[0].personid =1 ;
		
		householdMemberObj.parentCaretakerRelatives.anotherChildIndicator = false;		
		if($('#takeCareOf_anotherChild').is(':checked')){
			householdMemberObj.parentCaretakerRelatives.anotherChildIndicator = true;
		}
		
		householdMemberObj.parentCaretakerRelatives.name.firstName = $('#parent-or-caretaker-firstName').val();
		householdMemberObj.parentCaretakerRelatives.name.middleName = $('#parent-or-caretaker-middleName').val();
		householdMemberObj.parentCaretakerRelatives.name.lastName = $('#parent-or-caretaker-lastName').val();
		householdMemberObj.parentCaretakerRelatives.name.suffix = $('#parent-or-caretaker-suffix').val();
		householdMemberObj.parentCaretakerRelatives.dateOfBirth = DBDateformat($('#parent-or-caretaker-dob').val());
		householdMemberObj.parentCaretakerRelatives.relationship = $('#applicant-relationship').val();
		
		householdMemberObj.parentCaretakerRelatives.liveWithAdoptiveParentsIndicator = false;		
		if($('input[name=birthOrAdoptiveParents]:radio:checked').val() == 'yes'){
			householdMemberObj.parentCaretakerRelatives.liveWithAdoptiveParentsIndicator = true;
		}		
		
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;			
	}
	
}

function continueData64Json(currentID) {
	
	if(useJSON == true){
		
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
		
		if($('input[name=checkPersonNameLanguage]:radio:checked').val() == 'yes'){
			householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator = true;
			
			householdMemberObj.ethnicityAndRace.ethnicity = [];
			
			$('input:checkbox[name="ethnicity"]').each(function(){
				if($(this).is(':checked') && $(this).attr('id') != 'ethnicity_other'){
					
					householdMemberObj.ethnicityAndRace.ethnicity.push({
						'label' : $(this).closest('label').text().trim(),
						'code' : $(this).val()
					});
				}
			});
			
			//for other ethnicity
			if($('#ethnicity_other').is(':checked')){
				householdMemberObj.ethnicityAndRace.ethnicity.push({
					'label' : $('#otherEthnicity').val(),
					'code' : $('#ethnicity_other').val()
				});
			}
			
			
			
		}else if($('input[name=checkPersonNameLanguage]:radio:checked').val() == 'no'){
			householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator = false;
			householdMemberObj.ethnicityAndRace.ethnicity = [];
		}
		
		
		householdMemberObj.ethnicityAndRace.race = [];
		$('input:checkbox[name="race"]').each(function(){
			if($(this).is(':checked') && $(this).attr('id') != 'race_other'){
				
				householdMemberObj.ethnicityAndRace.race.push({
					'label' : $(this).closest('label').text().trim(),
					'code' : $(this).val()
				});
			}
		});
		
		//for other ethnicity
		if($('#race_other').is(':checked')){
			householdMemberObj.ethnicityAndRace.race.push({
				'label' : $('#otherRace').val(),
				'code' : $('#race_other').val()
			});
		}
		
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;	
		
		
		}
	
	
	/*if(useJSON == true){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
	
	var defaultJson = getDefatulJson(); // get default json		
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]; 
	//Assign default value
	householdMemberObj.ethnicityAndRace = defaultJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].ethnicityAndRace;
	
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
	if($('input[name=checkPersonNameLanguage]:radio:checked').val() == 'yes')
		householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator = true;
	else if($('input[name=checkPersonNameLanguage]:radio:checked').val() == 'no')
		householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator = false;
	
	ethnicityCount=0;
	
	for(var icount=0;icount<householdMemberObj.ethnicityAndRace.ethnicity.length;icount++){
		householdMemberObj.ethnicityAndRace.ethnicity.splice(icount);
	}
	
	if($('#ethnicity_cuban').is(':checked')){
		var ethinicity =  {"label" : "","code" : "", "otherLabel": ""};
		ethinicity.code=$('#ethnicity_cuban').val();
		ethinicity.label="cuban";
		householdMemberObj.ethnicityAndRace.ethnicity.push(ethinicity);
		ethnicityCount++;
	}
	
	if($('#ethnicity_maxican').is(':checked')){
		var ethinicity =  {"label" : "","code" : "", "otherLabel": ""};
		ethinicity.code=$('#ethnicity_maxican').val();
		ethinicity.label="mexican";
		householdMemberObj.ethnicityAndRace.ethnicity.push(ethinicity);
		ethnicityCount++;
	}
	if($('#ethnicity_puertoRican').is(':checked')){
		var ethinicity =  {"label" : "","code" : "", "otherLabel": ""};
		ethinicity.code=$('#ethnicity_puertoRican').val();
		ethinicity.label="puerto rican";
		householdMemberObj.ethnicityAndRace.ethnicity.push(ethinicity);
		ethnicityCount++;
	}
	if($('#ethnicity_other').is(':checked')){
		ethinicity.code=$('#other_ethnicity').val();
		ethinicity.label="other";
		householdMemberObj.ethnicityAndRace.ethnicity.push(ethinicity);
		ethnicityCount++;
	}
	
	
	raceCount=0;
	
	for(var icount=0;icount<householdMemberObj.ethnicityAndRace.race.length;icount++){
		householdMemberObj.ethnicityAndRace.race.splice(icount);
	}
	
	if($('#americanIndianOrAlaskaNative').is(':checked')){
		
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#americanIndianOrAlaskaNative').val();
		race.label="american indian or alaska native"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	
	if($('#asianIndian').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#asianIndian').val();
		race.label="asian indian"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	
	if($('#BlackOrAfricanAmerican').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#BlackOrAfricanAmerican').val();
		race.label="black or african american"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#chinese').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#chinese').val();
		race.label="chinese"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#filipino').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#filipino').val();
		race.label="filipino"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#guamanianOrChamorro').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#guamanianOrChamorro').val();
		race.label="guamanian or chamorro"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#japanese').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#japanese').val();
		race.label="japanese"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#korean').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#korean').val();
		race.label="korean"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	
	if($('#nativeHawaiian').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#nativeHawaiian').val();
		race.label="native hawaiian"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#otherAsian').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#otherAsian').val();
		race.label="other asian"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#otherPacificIslander').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#otherPacificIslander').val();
		race.label="other pacific islander"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	
	if($('#samoan').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#samoan').val();
		race.label="samoan"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#vietnamese').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#vietnamese').val();
		race.label="vietnamese"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	if($('#whiteOrCaucasian').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#whiteOrCaucasian').val();
		race.label="white or caucasian"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	
	if($('#raceOther').is(':checked')){
		var race =  {"label" : "","code" : "", "otherLabel": ""};
		race.code=$('#other_race').val();
		race.label="other_race"; 
		householdMemberObj.ethnicityAndRace.race.push(race);
		raceCount++;
	}
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;	
	}*/
	
}

function continueData65Json() {
	if(useJSON == true){
		
		var householdCount = getNoOfHouseHolds();
		var primaryContactObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];	
		
		for(var i=2;i<=householdCount;i++){
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			
			//this applicant is living at other address
			if($('#householdContactAddress'+i).is(":checked")){	
				
				householdMemberObj.livesAtOtherAddressIndicator	= true;
				householdMemberObj.livesWithHouseholdContactIndicator = false; 
				
				householdMemberObj.otherAddress.address.streetAddress1 = $('#applicant_or_non-applican_address_1'+i).val();
				householdMemberObj.otherAddress.address.streetAddress2 = $('#applicant_or_non-applican_address_2'+i).val();
				householdMemberObj.otherAddress.address.city = $('#city'+i).val();
				householdMemberObj.otherAddress.address.state = $('#applicant_or_non-applican_state'+i +" option:selected").val();
				householdMemberObj.otherAddress.address.postalCode = $('#zip'+i).val();
				householdMemberObj.otherAddress.address.county=$('#applicant_or_non-applican_county'+i +" option:selected").val();
							
				householdMemberObj.householdContact.homeAddressIndicator = false;
				
				householdMemberObj.householdContact.homeAddress.streetAddress1 = "";
				householdMemberObj.householdContact.homeAddress.streetAddress2 = "";
				householdMemberObj.householdContact.homeAddress.city = "";
				householdMemberObj.householdContact.homeAddress.state = "";
				householdMemberObj.householdContact.homeAddress.postalCode = "";
				householdMemberObj.householdContact.homeAddress.county="";
				
						
				temporaryLiving = $('input[name=isTemporarilyLivingOutsideIndicator'+i+']:radio:checked').val();
				if(temporaryLiving == 'yes'){				
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = true;					
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = $('#applicant2city'+i).val();
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = $('#applicant_or_non-applican_stateTemp'+i + " option:selected").val();
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = $('#applicant-2-zip'+i).val();
					
				} else if(temporaryLiving == 'no')  {
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = false;							
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = "";
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = "";
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = "";
				}
				
			}else if(!$('#householdContactAddress'+i).is(":checked")){
				
				householdMemberObj.livesAtOtherAddressIndicator	= false;
				householdMemberObj.livesWithHouseholdContactIndicator = true;
						
				householdMemberObj.householdContact.homeAddress.streetAddress1 = primaryContactObj.householdContact.homeAddress.streetAddress1;
				householdMemberObj.householdContact.homeAddress.streetAddress2 = primaryContactObj.householdContact.homeAddress.streetAddress2;
				householdMemberObj.householdContact.homeAddress.city = primaryContactObj.householdContact.homeAddress.city;
				householdMemberObj.householdContact.homeAddress.state = primaryContactObj.householdContact.homeAddress.state;
				householdMemberObj.householdContact.homeAddress.postalCode = primaryContactObj.householdContact.homeAddress.postalCode;
				householdMemberObj.householdContact.homeAddress.county=primaryContactObj.householdContact.homeAddress.county;
				
				householdMemberObj.householdContact.homeAddressIndicator = true;
				
				householdMemberObj.otherAddress.address.streetAddress1 = "";
				householdMemberObj.otherAddress.address.streetAddress2 = "";
				householdMemberObj.otherAddress.address.city = "";
				householdMemberObj.otherAddress.address.state = "";
				householdMemberObj.otherAddress.address.postalCode = "";
				householdMemberObj.otherAddress.address.county="";
				
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = false;							
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = "";
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = "";
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = "";
			}
			
			//mailling address will be always the same with PC's
			householdMemberObj.householdContact.mailingAddress.streetAddress1 = primaryContactObj.householdContact.mailingAddress.streetAddress1;
			householdMemberObj.householdContact.mailingAddress.streetAddress2 = primaryContactObj.householdContact.mailingAddress.streetAddress2;
			householdMemberObj.householdContact.mailingAddress.city = primaryContactObj.householdContact.mailingAddress.city;
			householdMemberObj.householdContact.mailingAddress.state = primaryContactObj.householdContact.mailingAddress.state;
			householdMemberObj.householdContact.mailingAddress.postalCode = primaryContactObj.householdContact.mailingAddress.postalCode;
			householdMemberObj.householdContact.mailingAddress.county=primaryContactObj.householdContact.mailingAddress.county;
			
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1]=householdMemberObj;
		}
		
		
		
	/*var x=$('#noOfPeople53').val();

	for(var y=1;y<=x;y++){
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1];
		var fname = householdMemberObj.name.firstName;
		var mname = householdMemberObj.name.middleName;
		var lname = householdMemberObj.name.lastName;
		
		var name = fname + "_" + mname + "_" + lname;
	
		//placeholders for address information
		var addressLine1=$('#home_addressLine1').val(); 
		var addressLine2=$('#home_addressLine2').val();
		var city = $('#home_primary_city').val();
		var zip=$('#home_primary_zip').val();
		var state=$('#home_primary_state').val();
		var secondCity="";
		var secondZip="";
		var secondState="";
		var livesAtOtherAddressIndicator=false;
		var temporaryLiving="no";
		
		householdMemberObj.householdContact.homeAddressIndicator = true;
		if(y!=1){
		if(!$('#householdContactAddress'+y).is(":checked")){			
			applicantObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];			
			householdMemberObj.householdContact.homeAddress.streetAddress1 = applicantObj.householdContact.homeAddress.streetAddress1;
			householdMemberObj.householdContact.homeAddress.streetAddress2 = applicantObj.householdContact.homeAddress.streetAddress2;
			householdMemberObj.householdContact.homeAddress.city = applicantObj.householdContact.homeAddress.city;
			householdMemberObj.householdContact.homeAddress.state = applicantObj.householdContact.homeAddress.state;
			householdMemberObj.householdContact.homeAddress.postalCode = applicantObj.householdContact.homeAddress.postalCode;
			householdMemberObj.householdContact.homeAddress.county=applicantObj.householdContact.homeAddress.county;
			householdMemberObj.householdContact.homeAddressIndicator = true;
			
			householdMemberObj.otherAddress.address.streetAddress1 = "";
			householdMemberObj.otherAddress.address.streetAddress2 = "";
			householdMemberObj.otherAddress.address.city = "";
			householdMemberObj.otherAddress.address.state = "";
			householdMemberObj.otherAddress.address.postalCode = "";
			householdMemberObj.otherAddress.address.county="";
			householdMemberObj.livesWithHouseholdContactIndicator = true;
			
		}
		
		if($('#householdContactAddress'+y).is(":checked")) //this applicant is living at other address
			{	
				householdMemberObj.otherAddress.address.streetAddress1 = $('#applicant_or_non-applican_address_1'+y).val();
				householdMemberObj.otherAddress.address.streetAddress2 = $('#applicant_or_non-applican_address_2'+y).val();
				householdMemberObj.otherAddress.address.city = $('#city'+y).val();
				householdMemberObj.otherAddress.address.state = $('#applicant_or_non-applican_state'+y +" option:selected").val();
				householdMemberObj.otherAddress.address.postalCode = $('#zip'+y).val();
				householdMemberObj.otherAddress.address.county="";
				householdMemberObj.householdContact.homeAddressIndicator = false;				
				
				householdMemberObj.householdContact.homeAddress.streetAddress1 = "";
				householdMemberObj.householdContact.homeAddress.streetAddress2 = "";
				householdMemberObj.householdContact.homeAddress.city = "";
				householdMemberObj.householdContact.homeAddress.state = "";
				householdMemberObj.householdContact.homeAddress.postalCode = "";
				householdMemberObj.householdContact.homeAddress.county="";
				
				livesAtOtherAddressIndicator= true;				
				temporaryLiving = $('input[name=isTemporarilyLivingOutsideIndicator'+y+']:radio:checked').val();
				
				householdMemberObj.livesWithHouseholdContactIndicator = false; 
				
				if(temporaryLiving == 'yes'){				
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = true;					
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = $('#applicant2city'+y).val();
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = $('#applicant_2_state'+y + " option:selected").text();
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = $('#applicant-2-zip'+y).val();
					
				} else if(temporaryLiving == 'no')  {
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = false;							
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = "";
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = "";
					householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = "";
				}			
			}
		}
		householdMemberObj.livesAtOtherAddressIndicator = livesAtOtherAddressIndicator;		
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1]=householdMemberObj;
		
	}*/	
	}
}
function continueData66Json () {
	
	resetData66Json();
	if(useJSON == true){
	
	//var x=$('#noOfPeople53').val();		
	var x =	getNoOfHouseHolds();
	
	if($("#disabilityNoneOfThese").is(":checked")){
		noneOftheseDisabilityIndicator = true;
	}else{
		noneOftheseDisabilityIndicator = false;
	}
	
	if($("#assistanceServicesNoneOfThese").is(":checked")){
		noneOftheseAssistanceServices = true;
	}else{
		noneOftheseAssistanceServices = false;
	}
	
	if($("#nativeNoneOfThese").is(":checked")){
		noneOftheseAmericanIndianAlaskaNativeIndicator = true;
	}else{
		noneOftheseAmericanIndianAlaskaNativeIndicator = false;
	}
	
	if($("#personfosterCareNoneOfThese").is(":checked")){
		noneOftheseEverInFosterCareIndicator = true;
	}else{
		noneOftheseEverInFosterCareIndicator = false;
	}
	
	if($("#deceasedPersonNoneOfThese").is(":checked")){
		noneoftheseDeceased = true;
	}else{
		noneoftheseDeceased = false;
	}	
	
	if(document.getElementById('pregnancyNoneOfThese')!=null && document.getElementById('pregnancyNoneOfThese').checked){
		noneofthesePregnancy = true;
	}else{
		noneofthesePregnancy = false;
	}
	
	for(var y=1;y<=x;y++){
		
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1];
		 
		  var disabled=false;
		  if($('#disability'+y).is(":checked")){			 
			  disabled=true;			 
		  }
		  /*
		  var assistanceServcices="no";
		  if($('#assistanceServices'+y).is(":checked")){
			  assistanceServices="yes";
		  }*/
		  var native=false;		  
		  var tribeName="";
		  var tribeState="";
		  var tribeFullName = "";
		  var stateFullName = "";
		  if($('#native'+y).is(":checked")){
			  native=true;			  
		  }
		  var fosterCare=false;
		  if($('#personfosterCare'+y).is(":checked")){
			  fosterCare=true;
		  }
		  /*var deceased="no";
		  if($('#deceased'+y).is(":checked")){
			  
			  deceased="yes";
			  
		  }
		  var medicaid="no";
		  if($('input[name=isPersonGettingHealthCareYes'+i+']:radio:checked').val()=="yes"){
			  medicaid="yes";
		  }*/
		  
		  var pregnancy=false;
		  var numberBabiesExpectedInPregnancy = 0;
		  if($('#pregnancy'+y).is(":checked")){
			  pregnancy=true;
			  numberBabiesExpectedInPregnancy = $('#number-of-babies'+y).val();
		  }
		  var americanIndianAlaskaNative = false;
		  if(native==true){
			  if($('input[name=AmericonIndianQuestionRadio'+y+']:radio:checked').val()=="yes"){
				  americanIndianAlaskaNative  = true;
				  tribeName = $('#tribeName'+y).val();
				  tribeState = $('#americonIndianQuestionSelect'+y).val();
				  tribeFullName = $('#tribeName'+y+' option:selected').text();
				  stateFullName = $('#americonIndianQuestionSelect'+y+' option:selected').text();
			  }
		  }
		/*  if(document.getElementById('appscr78Medicare'+y).checked){
			  householdMemberObj.healthCoverage.currentOtherInsurance.medicareEligibleIndicator = true;
		  }
		  
		  if(document.getElementById('appscr78Tricare'+y).checked){
			  householdMemberObj.healthCoverage.currentOtherInsurance.tricareEligibleIndicator = true;
		  }
		  
		  if(document.getElementById('appscr78PeaceCorps'+y).checked){
			  householdMemberObj.healthCoverage.currentOtherInsurance.peaceCorpsIndicator = true;
		  }
		  
		  if(document.getElementById('appscr78OtherStateFHBP'+y).checked){
			  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramIndicator = true;
		  }
		  
		  if(document.getElementById('appscr78None'+y).checked){
		  	householdMemberObj.healthCoverage.currentOtherInsurance.nonofTheAbove = true;
		  }*/
		  
		  var fedHealthVal = $("input:radio[name=fedHealth"+y+"]:checked").val();
		  if(fedHealthVal == "None"){
			  householdMemberObj.healthCoverage.otherInsuranceIndicator = false;
			  householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected = "";
			  
			  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType = "";
			  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName = "";
		  }else{
			  householdMemberObj.healthCoverage.otherInsuranceIndicator = true;
			  householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected = fedHealthVal;
			  
			  if(fedHealthVal == "Other"){
				  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType = $("#appscr78TypeOfProgram" + y).val();
				  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName = $("#appscr78NameOfProgram" + y).val();
			  }else{
				  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType = "";
				  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName = "";
			  }
			  
		  }		  
		  
		  householdMemberObj.specialCircumstances.americanIndianAlaskaNativeIndicator = native;
		  householdMemberObj.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator = americanIndianAlaskaNative;
		  if(americanIndianAlaskaNative == true){		 	  
			  householdMemberObj.americanIndianAlaskaNative.state = tribeState;
			  householdMemberObj.americanIndianAlaskaNative.tribeName = tribeName;
			  householdMemberObj.americanIndianAlaskaNative.tribeFullName = tribeFullName;
			  householdMemberObj.americanIndianAlaskaNative.stateFullName = stateFullName;
		  } 
		  householdMemberObj.specialCircumstances.fosterChild = fosterCare;		  
		  householdMemberObj.disabilityIndicator = disabled;		  		  
		  householdMemberObj.specialCircumstances.pregnantIndicator = pregnancy;
		  householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy = numberBabiesExpectedInPregnancy;		  
		  webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1]=householdMemberObj;		  
	}
	}

}

function continueData69Json(currentID){	
	if(useJSON == true){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];	
	householdMemberObj.expeditedIncome.irsReportedAnnualIncome=null;
	householdMemberObj.expeditedIncome.irsReportedAnnualIncome=getDecimalNumber(cleanMoneyFormat($('#expeditedIncome69').val()));
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;	  
	  $("#Financial_None").prop('checked', false);
	  $("#appscr70radios4IncomeYes").prop('checked', false);
	  $("#appscr70radios4IncomeNo").prop('checked', false);
	  $("#incomeSourceDiv").hide();
	  $.each(incomesecion, function(key, value) {
		    $("#"+key).prop('checked', false);			
		});
	}
}

function continueData70Json(currentID){
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
	householdMemberObj.detailedIncome.jobIncomeIndicator = true;	
	if( $('#Financial_Job').is(':checked') == false){  
		householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes = null;
		householdMemberObj.detailedIncome.jobIncomeIndicator = null;
		householdMemberObj.detailedIncome.jobIncome[0].employerName = "";
		householdMemberObj.detailedIncome.jobIncome[0].incomeFrequency = "";
		householdMemberObj.detailedIncome.jobIncome[0].workHours = null;
	}
	householdMemberObj.detailedIncome.selfEmploymentIncomeIndicator = true;
	if( $('#Financial_Self_Employment').is(':checked') == false){  
		householdMemberObj.detailedIncome.selfEmploymentIncome.monthlyNetIncome = null;
		householdMemberObj.detailedIncome.selfEmploymentIncomeIndicator = null;
		householdMemberObj.detailedIncome.selfEmploymentIncome.typeOfWork = "";
		//householdMemberObj.detailedIncome.selfEmploymentIncome.incomeFrequency="";
	}
	householdMemberObj.detailedIncome.socialSecurityBenefitIndicator = true;
	if( $('#Financial_SocialSecuritybenefits').is(':checked') == false){
		householdMemberObj.detailedIncome.socialSecurityBenefit.benefitAmount = null;
		householdMemberObj.detailedIncome.socialSecurityBenefitIndicator = null;
		householdMemberObj.detailedIncome.socialSecurityBenefit.incomeFrequency = "";
	}
	householdMemberObj.detailedIncome.unemploymentBenefitIndicator = true;
	if( $('#Financial_Unemployment').is(':checked') == false)  {
		householdMemberObj.detailedIncome.unemploymentBenefit.benefitAmount = null;
		householdMemberObj.detailedIncome.unemploymentBenefitIndicator = null;
		householdMemberObj.detailedIncome.unemploymentBenefit.stateGovernmentName = "";
		householdMemberObj.detailedIncome.unemploymentBenefit.incomeFrequency = "";
		householdMemberObj.detailedIncome.unemploymentBenefit.unemploymentDate = "";
	}
	householdMemberObj.detailedIncome.retirementOrPensionIndicator = true;
	if( $('#Financial_Retirement_pension').is(':checked') == false) {
		householdMemberObj.detailedIncome.retirementOrPension.taxableAmount = null;
		householdMemberObj.detailedIncome.retirementOrPensionIndicator = null;
		householdMemberObj.detailedIncome.retirementOrPension.incomeFrequency = "";
	}
	householdMemberObj.detailedIncome.capitalGainsIndicator = true;
	if( $('#Financial_Capitalgains').is(':checked') == false) {
		householdMemberObj.detailedIncome.capitalGains.annualNetCapitalGains = null;
		householdMemberObj.detailedIncome.capitalGainsIndicator = null;
		//householdMemberObj.detailedIncome.capitalGains.incomeFrequency = "";
		
	}
	householdMemberObj.detailedIncome.investmentIncomeIndicator = true;
	if( $('#Financial_InvestmentIncome').is(':checked') == false) {
		householdMemberObj.detailedIncome.investmentIncome.incomeAmount = null;
		householdMemberObj.detailedIncome.investmentIncomeIndicator = null;
		householdMemberObj.detailedIncome.investmentIncome.incomeFrequency = "";
	}
	householdMemberObj.detailedIncome.rentalRoyaltyIncomeIndicator = true;
	if( $('#Financial_RentalOrRoyaltyIncome').is(':checked') == false){  
		householdMemberObj.detailedIncome.rentalRoyaltyIncome.netIncomeAmount = null;
		householdMemberObj.detailedIncome.rentalRoyaltyIncomeIndicator = null;
		householdMemberObj.detailedIncome.rentalRoyaltyIncome.incomeFrequency = ""
	}
	householdMemberObj.detailedIncome.farmFishingIncomeIndictor = true;
	if( $('#Financial_FarmingOrFishingIncome').is(':checked') == false) {
		householdMemberObj.detailedIncome.farmFishingIncome.netIncomeAmount = null;
		householdMemberObj.detailedIncome.farmFishingIncomeIndictor = null;
		householdMemberObj.detailedIncome.farmFishingIncome.incomeFrequency = "";
	}
	householdMemberObj.detailedIncome.alimonyReceivedIndicator = true;
	if( $('#Financial_AlimonyReceived').is(':checked') == false) {
		householdMemberObj.detailedIncome.alimonyReceived.amountReceived = null;
		householdMemberObj.detailedIncome.alimonyReceivedIndicator = null;
		householdMemberObj.detailedIncome.alimonyReceived.incomeFrequency = "";
	}
	householdMemberObj.detailedIncome.otherIncomeIndicator = true;
	if( $('#Financial_OtherIncome').is(':checked') == false)  {
		householdMemberObj.detailedIncome.otherIncomeIndicator = null;
		householdMemberObj.detailedIncome.otherIncome[0].incomeAmount = null;			
		householdMemberObj.detailedIncome.otherIncome[0].incomeFrequency = "";
		householdMemberObj.detailedIncome.otherIncome[1].incomeAmount = null;			
		householdMemberObj.detailedIncome.otherIncome[1].incomeFrequency = "";
		householdMemberObj.detailedIncome.otherIncome[2].incomeAmount = null;			
		householdMemberObj.detailedIncome.otherIncome[2].incomeFrequency = "";
	}
	
	if( $('#Financial_None').is(':checked') == true)  {
		
		householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount = null;
		householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency = "";
		householdMemberObj.detailedIncome.deductions.studentLoanDeductionAmount = null;
		householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency = "";
		householdMemberObj.detailedIncome.deductions.otherDeductionAmount = null;
		householdMemberObj.detailedIncome.deductions.otherDeductionFrequency = "";		
	}
	
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;
	
	$('#incomesStatus'+currentID).text("No");
	
	$.each(incomesecion, function(key, value) {
	    incomesecion[key] = false;
	    if($('#'+key).is(":checked") == true)
		{
	    	incomesecion[key] = true;
	    	$('#incomesStatus'+currentID).text("Yes");
		}
	});
	
	
	
	
	
	//Nothing to do here....this is just for knowing how many incomes there. 
	
	//householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];	
	
}
function continueData71Json(currentID){
	
	if(useJSON == true){
	
	//if($('#Financial_Job_select_id').val() != ""){	
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];	
		householdMemberObj.detailedIncome.jobIncome[0].employerName=$('#Financial_Job_EmployerName_id').val();
		householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes=getDecimalNumber(cleanMoneyFormat($('#Financial_Job_id').val()));
		householdMemberObj.detailedIncome.jobIncome[0].incomeFrequency=$('#Financial_Job_select_id').val();
		householdMemberObj.detailedIncome.jobIncome[0].workHours=getIntegerNumber($('#Financial_Job_HoursOrDays_id').val());
	//}
	
	//if($('#Financial_Self_Employment_id').val() != ""){
		householdMemberObj.detailedIncome.selfEmploymentIncome.typeOfWork=$('#Financial_Self_Employment_TypeOfWork').val();
		householdMemberObj.detailedIncome.selfEmploymentIncome.monthlyNetIncome=cleanMoneyFormat($('#Financial_Self_Employment_id').val());
		//householdMemberObj.detailedIncome.selfEmploymentIncome.incomeFrequency="Monthly";
	//}
	//if($('#Financial_SocialSecuritybenefits_id').val() != ""){
		householdMemberObj.detailedIncome.socialSecurityBenefit.benefitAmount=cleanMoneyFormat($('#Financial_SocialSecuritybenefits_id').val());
		householdMemberObj.detailedIncome.socialSecurityBenefit.incomeFrequency=$('#Financial_SocialSecuritybenefits_select_id').val();
	//}
	//if($('#Financial_Unemployment_select_id').val() != ""){
		householdMemberObj.detailedIncome.unemploymentBenefit.stateGovernmentName=$('#Financial_Unemployment_StateGovernment').val();
		householdMemberObj.detailedIncome.unemploymentBenefit.benefitAmount=cleanMoneyFormat($('#Financial_Unemployment_id').val());
		householdMemberObj.detailedIncome.unemploymentBenefit.incomeFrequency=$('#Financial_Unemployment_select_id').val();
		householdMemberObj.detailedIncome.unemploymentBenefit.unemploymentDate=DBDateformat($('#unemploymentIncomeDate').val());
	//}
	//if($('#Financial_Retirement_pension_id').val() != ""){
		householdMemberObj.detailedIncome.retirementOrPension.taxableAmount=cleanMoneyFormat($('#Financial_Retirement_pension_id').val());	
		householdMemberObj.detailedIncome.retirementOrPension.incomeFrequency=$('#Financial_Retirement_pension_select_id').val();
	//}
	
	householdMemberObj.detailedIncome.capitalGains.annualNetCapitalGains=cleanMoneyFormat($('#Financial_Capitalgains_id').val());	
	//householdMemberObj.detailedIncome.capitalGains.incomeFrequency="Yearly";
	//if($('#Financial_InvestmentIncome_id').val() != ""){
		householdMemberObj.detailedIncome.investmentIncome.incomeAmount=cleanMoneyFormat($('#Financial_InvestmentIncome_id').val());	
		householdMemberObj.detailedIncome.investmentIncome.incomeFrequency=($('#Financial_InvestmentIncome_select_id').val());
	//}
	
	//if($('#Financial_RentalOrRoyaltyIncome_id').val() != ""){
		householdMemberObj.detailedIncome.rentalRoyaltyIncome.netIncomeAmount=cleanMoneyFormat($('#Financial_RentalOrRoyaltyIncome_id').val());
		householdMemberObj.detailedIncome.rentalRoyaltyIncome.incomeFrequency=$('#Financial_RentalOrRoyaltyIncome_select_id').val();
	//}
	
	//if($('#Financial_FarmingOrFishingIncome_id').val() != ""){
		householdMemberObj.detailedIncome.farmFishingIncome.netIncomeAmount=cleanMoneyFormat($('#Financial_FarmingOrFishingIncome_id').val());
		householdMemberObj.detailedIncome.farmFishingIncome.incomeFrequency=$('#Financial_FarmingOrFishingIncome_select_id').val();
	//}
	//if($('#Financial_AlimonyReceived_id').val() != ""){
		householdMemberObj.detailedIncome.alimonyReceived.amountReceived=cleanMoneyFormat($('#Financial_AlimonyReceived_id').val());
		householdMemberObj.detailedIncome.alimonyReceived.incomeFrequency=$('#Financial_AlimonyReceived_select_id').val();
	//}
	
		if( $('#appscr71CanceleddebtsCB').is(':checked') == true){
			householdMemberObj.detailedIncome.otherIncome[0].incomeAmount=cleanMoneyFormat($('#cancelled_Debts_amount').val());
			householdMemberObj.detailedIncome.otherIncome[0].incomeFrequency=$('#cancelledDebtsFrequency').val();
			householdMemberObj.detailedIncome.otherIncome[0].otherIncomeTypeDescription='Canceled debts';
		}
		
		if( $('#appscr71CourtAwardsCB').is(':checked') == true){			
			householdMemberObj.detailedIncome.otherIncome[1].incomeAmount=cleanMoneyFormat($('#court_awards_amount').val());
			householdMemberObj.detailedIncome.otherIncome[1].incomeFrequency=$('#courtAwrdsFrequency').val();
			householdMemberObj.detailedIncome.otherIncome[1].otherIncomeTypeDescription='Court Awards';
		}
		
		if( $('#appscr71JuryDutyPayCB').is(':checked') == true){			
			householdMemberObj.detailedIncome.otherIncome[2].incomeAmount=cleanMoneyFormat($('#jury_duty_pay_amount').val());
			householdMemberObj.detailedIncome.otherIncome[2].incomeFrequency=$('#juryDutyPayFrequency').val();
			householdMemberObj.detailedIncome.otherIncome[2].otherIncomeTypeDescription='Jury duty pay';
		}
	
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;	

		/*var Financial_OtherTypeIncome = $('#Financial_OtherTypeIncome').val();
		var Financial_OtherIncome_id = $('#Financial_OtherIncome_id').val();
		var Financial_OtherIncome_select_id = $('#Financial_OtherIncome_select_id').val();*/
		//var Financial_OtherTypeIncome = $('#cancelled_Debts_amount').val();
		//var Financial_OtherIncome_id = $('#court_awards_amount').val();
		//var Financial_OtherIncome_select_id = $('#jury_duty_pay_amount').val();
	
		setTotalFamilyIncomeJSON(); // Count and set total family annual income
	}
	
}

function continueData72Json(currentID){
	
	if(useJSON == true){
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
	
	householdMemberObj.detailedIncome.deductions.deductionType[0] = "ALIMONY";
	householdMemberObj.detailedIncome.deductions.deductionType[1] = "STUDENT_LOAN_INTEREST";
	householdMemberObj.detailedIncome.deductions.deductionType[2] = $('#deductionTypeInput').val();
	
	householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount = null;
	householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency = "";
	
	if($('#appscr72alimonyPaid').is(':checked')){
		householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount=getDecimalNumber(cleanMoneyFormat($('#alimonyAmountInput').val()));
		householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency=$('#alimonyFrequencySelect').val();
	}
	householdMemberObj.detailedIncome.deductions.studentLoanDeductionAmount = null;
	householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency = "";
	if($('#appscr72studentLoanInterest').is(':checked')){	
		householdMemberObj.detailedIncome.deductions.studentLoanDeductionAmount=getDecimalNumber(cleanMoneyFormat($('#studentLoanInterestAmountInput').val()));
		householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency=$('#studentLoanInterestFrequencySelect').val();
	}
	householdMemberObj.detailedIncome.deductions.otherDeductionAmount = null;
	householdMemberObj.detailedIncome.deductions.otherDeductionFrequency = "";
	if($('#appscr72otherDeduction').is(':checked')){
		householdMemberObj.detailedIncome.deductions.otherDeductionAmount=getDecimalNumber(cleanMoneyFormat($('#deductionAmountInput').val()));
		householdMemberObj.detailedIncome.deductions.otherDeductionFrequency=$('#deductionFrequencySelect').val();
	}
	
	householdMemberObj.expeditedIncome.expectedIncomeVaration = null;
	householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator = null;
	if($('input[name=aboutIncomeSpped]:radio:checked').val() == 'yes')
		householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator = true;
	else if($('input[name=aboutIncomeSpped]:radio:checked').val() == 'no')
		householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator = false;
	
	if($('input[name=incomeHighOrLow]:radio:checked').val() == 'higher')
		householdMemberObj.expeditedIncome.expectedIncomeVaration =  'higher';
	else if($('input[name=incomeHighOrLow]:radio:checked').val() == 'lower')
		householdMemberObj.expeditedIncome.expectedIncomeVaration = 'lower';
	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;
	
	//var appscr72incomeNone = $('#appscr72incomeNone').is(':checked');
	//var aboutIncomeSpped = $('input[name=aboutIncomeSpped]:radio:checked').val();
	
	//var incomeHighOrLow = $('input[name=incomeHighOrLow]:radio:checked').val();
	}
}

function continueData73Json(currentID){
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
	
	if($('input[name=stopWorking]:radio:checked').val() == 'yes'){
		householdMemberObj.detailedIncome.discrepancies.stopWorkingAtEmployerIndicator = true;
	} else if ($('input[name=stopWorking]:radio:checked').val() == 'no'){
		householdMemberObj.detailedIncome.discrepancies.stopWorkingAtEmployerIndicator = false;
	}
	
	if($('input[name=workAt]:radio:checked').val() == 'yes'){
		householdMemberObj.detailedIncome.discrepancies.didPersonEverWorkForEmployerIndicator = true;
	} else if ($('input[name=workAt]:radio:checked').val() == 'no'){
		householdMemberObj.detailedIncome.discrepancies.didPersonEverWorkForEmployerIndicator = false;
	}
	
	if($('input[name=decresedAt]:radio:checked').val() == 'yes'){
		householdMemberObj.detailedIncome.discrepancies.hasHoursDecreasedWithEmployerIndicator = true;
	} else if ($('input[name=decresedAt]:radio:checked').val() == 'no'){
		householdMemberObj.detailedIncome.discrepancies.hasHoursDecreasedWithEmployerIndicator = false;
	}
	
	if($('input[name=salaryAt]:radio:checked').val() == 'yes'){
		householdMemberObj.detailedIncome.discrepancies.hasWageOrSalaryBeenCutIndicator = true;
	} else if ($('input[name=salaryAt]:radio:checked').val() == 'no'){
		householdMemberObj.detailedIncome.discrepancies.hasWageOrSalaryBeenCutIndicator = false;
	}	
	householdMemberObj.detailedIncome.discrepancies.explanationForJobIncomeDiscrepancy = $('#explanationTextArea').val();	
	if($('input[name=sesWorker]:radio:checked').val() == 'yes'){
		householdMemberObj.detailedIncome.discrepancies.seasonalWorkerIndicator = true;
	} else if ($('input[name=sesWorker]:radio:checked').val() == 'no'){
		householdMemberObj.detailedIncome.discrepancies.seasonalWorkerIndicator = false;
	}
	householdMemberObj.detailedIncome.discrepancies.explanationForDependantDiscrepancy = $('#appscr73ExplanationTaxtArea').val();
	if($('input[name=OtherJointIncome]:radio:checked').val() == 'yes'){
		householdMemberObj.detailedIncome.discrepancies.otherAboveIncomeIncludingJointIncomeIndicator = true;
	} else if ($('input[name=OtherJointIncome]:radio:checked').val() == 'no'){
		householdMemberObj.detailedIncome.discrepancies.otherAboveIncomeIncludingJointIncomeIndicator = false;
	}
}

function appscr58EditUdateJson(currentMemember){
	if(useJSON == true){
	var cnt = currentMemember;
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
	householdMemberObj.name.firstName = trimSpaces($('#appscr57FirstName'+cnt).val());
	householdMemberObj.name.middleName = trimSpaces($('#appscr57MiddleName'+cnt).val());
	householdMemberObj.name.lastName = trimSpaces($('#appscr57LastName'+cnt).val());
	householdMemberObj.name.suffix = $('#appscr57Suffix'+cnt).val();
	householdMemberObj.dateOfBirth = DBDateformat($('#appscr57DOB'+cnt).val());
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;	
	}
	
}

function continueData76P1Json(currentMemember){
	if(useJSON == true){
		
		var cnt = currentMemember;
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		
		householdMemberObj.healthCoverage.isOfferedHealthCoverageThroughJobIndicator =  $('input[name=isHealthCoverageThroughJob]:radio:checked').val() == "yes" ? true:false;
		householdMemberObj.healthCoverage.employerWillOfferInsuranceIndicator =  $('input[name=willHealthCoverageThroughJob]:radio:checked').val() == "yes" ? true:false;
		householdMemberObj.healthCoverage.employerWillOfferInsuranceStartDate = DBDateformat($('#appscr76P1HealthCoverageDate').val());
		householdMemberObj.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator =  $('input[name=wasUninsuredFromLast6Month]:radio:checked').val() == "yes" ? true:false;
		
		
		if($('#appscr76P1CheckBoxForEmployeeName').is(':checked')){
			
			householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactPersonName = $('#appscr76P1EmployerContactName').val();			
			householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneNumber = $('#appscr76P1_firstPhoneNumber').val().replace(/\D+/g, "");
			householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneExtension = $('#appscr76P1_firstExt').val();
			householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneType = $('#appscr76P1_firstPhoneType').val();
			householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactEmailAddress = $('#appscr76P1_emailAddress').val();
		}
		
		if($('#appscr76P1CheckBoxForOtherEmployee').is(':checked')){
			
			householdMemberObj.healthCoverage.formerEmployer[0].employerName = $('#appscr76P1_otherEmployerName').val();
			householdMemberObj.healthCoverage.formerEmployer[0].employerIdentificationNumber = $('#appscr76P1_employerEIN').val();
			householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress1 = $('#appscr76P1_otherEmployerAddress1').val();
			householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress2 = $('#appscr76P1_otherEmployerAddress2').val();
			householdMemberObj.healthCoverage.formerEmployer[0].address.city = $('#appscr76P1_otherEmployerCity').val();
			householdMemberObj.healthCoverage.formerEmployer[0].address.state = $('#appscr76P1_otherEmployerState').val();
			householdMemberObj.healthCoverage.formerEmployer[0].address.zipCode = $('#appscr76P1_otherEmployerZip').val();
			
			householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneNumber = $('#appscr76P1_secondPhoneNumber').val().replace(/\D+/g, "");
			householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneExtension = $('#appscr76P1_secondExt').val();
			
			householdMemberObj.healthCoverage.formerEmployer[0].employerContactPersonName = $('#appscr76P1_otherEmployerContactName').val();			
			householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneNumber = $('#appscr76P1_thirdPhoneNumber').val().replace(/\D+/g, "");
			householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneExtension = $('#appscr76P1_thirdExt').val();
			householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneType = $('#appscr76P1_thirdPhoneType').val();
			householdMemberObj.healthCoverage.formerEmployer[0].employerContactEmailAddress = $('#appscr76P1_otherEmployer_emailAddress').val();
		}
		
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;		
	}
	
}

function continueData76P2Json(currentMemember){
	if(useJSON == true){
		
		var cnt = currentMemember;
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		
		householdMemberObj.healthCoverage.currentlyEnrolledInCobraIndicator =  $('input[name=appscr76p2radiosgroup]:radio:checked').val() == "corba" ? true:false;
		householdMemberObj.healthCoverage.currentlyEnrolledInRetireePlanIndicator =  $('input[name=appscr76p2radiosgroup]:radio:checked').val() == "retire health plan" ? true:false;
		householdMemberObj.healthCoverage.currentlyEnrolledInVeteransProgramIndicator =  $('input[name=appscr76p2radiosgroup]:radio:checked').val() == "veterans health program" ? true:false;
		householdMemberObj.healthCoverage.currentlyEnrolledInNoneIndicator =  $('input[name=appscr76p2radiosgroup]:radio:checked').val() == "none" ? true:false;
		
		householdMemberObj.healthCoverage.willBeEnrolledInCobraIndicator =  $('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val() == "corba" ? true:false;
		householdMemberObj.healthCoverage.willBeEnrolledInRetireePlanIndicator =  $('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val() == "retiree health plan" ? true:false;
		householdMemberObj.healthCoverage.willBeEnrolledInVeteransProgramIndicator =  $('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val() == "veterans health plans" ? true:false;
		householdMemberObj.healthCoverage.willBeEnrolledInNoneIndicator =  $('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val() == "none" ? true:false;
		
		householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.isCurrentlyEnrolledInEmployerPlanIndicator = $('input[name=77P1CurrentlyEnrolledInHealthPlan]:radio:checked').val() == "yes" ? true:false;
		householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.willBeEnrolledInEmployerPlanIndicator = $('input[name=77P1WillBeEnrolledInHealthPlan]:radio:checked').val() == "yes" ? true:false;
				
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;
	}
	
}

function continueData77Part1Json(currentMemember){
	if(useJSON == true){
		
		var cnt = currentMemember;
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		
		householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.isCurrentlyEnrolledInEmployerPlanIndicator = $('input[name=77P1CurrentlyEnrolledInHealthPlan]:radio:checked').val() == "yes" ? true:false;
		householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.willBeEnrolledInEmployerPlanIndicator = $('input[name=77P1WillBeEnrolledInHealthPlan]:radio:checked').val() == "yes" ? true:false;
				
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;		
		/*
		 * Following data not available in json
		 * 
		var appscr77P1WillBeEnrolledInHealthPlanDate = $('#77P1WillBeEnrolledInHealthPlanDate').val();
		var appscr77P1ExpectChangesToHealthCoverage = $('input[name=77P1ExpectChangesToHealthCoverage]:radio:checked').val();
		var appscr77P1WillNoLongerHealthCoverage = $('#77P1WillNoLongerHealthCoverage').is(':checked');
		var appscr77P1NoLongerHealthCoverageDate = $('#77P1NoLongerHealthCoverageDate').val();
		var appscr77P1PlanToDropHealthCoverage = $('#77P1PlanToDropHealthCoverage').is(':checked');
		var appscr77P1PlanToDropHealthCoverageDate = $('#77P1PlanToDropHealthCoverageDate').val();
		var appscr77P1WillOfferCoverage = $('#77P1WillOfferCoverage').is(':checked');
		var appscr77P1WillOfferCoverageDate = $('#77P1WillOfferCoverageDate').val();
		var appscr77P1PlaningToEnrollInHC = $('#77P1PlaningToEnrollInHC').is(':checked');
		var appscr77P1PlaningToEnrollInHCDate = $('#77P1PlaningToEnrollInHCDate').val();
		var appscr77P1HealthPlanOptionsGoingToChange = $('#appscr77P1HealthPlanOptionsGoingToChange').is(':checked');
		var appscr77P1HealthPlanOptionsGoingToChangeDate = $('#77P1HealthPlanOptionsGoingToChangeDate').val();*/
		
	}
	
}

function continueData77Part2Json(currentMemember){
	if(useJSON == true){
		
		var cnt = currentMemember;
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		
				
		householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentLowestCostSelfOnlyPlanName = $('#currentLowestCostSelfOnlyPlanName').val();
		householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentPlanMeetsMinimumStandardIndicator = $('#currentPlanMeetsMinimumStandard').is(':checked') == true  ? false : true;
		
		householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingLowestCostSelfOnlyPlanName = $('#comingLowestCostSelfOnlyPlanName').val();
		householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingPlanMeetsMinimumStandardIndicator = $('#comingPlanMeetsMinimumStandard').is(':checked') == true ? false : true;
		
				
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;		
		
		/*
		 * Following data not found in json
		 * var lowestCostSelfOnlyPremiumAmount = $('#lowestCostSelfOnlyPremiumAmount').val();
		var lowestCostSelfOnlyPremiumFrequency = $('#lowestCostSelfOnlyPremiumFrequency').val();
		var appscr77p2IsAffordable = $('input[name=appscr77p2IsAffordable]:radio:checked').val();
		*/
		
	}
	
}


function openJsonTab(){
	something = window.open("data:text/json," + encodeURIComponent(JSON.stringify(webJson)), "_blank");
	something.focus();
	return false;	
}

function cleanMoneyFormat(strMoney){
	if(strMoney == null || strMoney == "") return null;
	strMoney= strMoney.replace(/[&\/\\#,+()$~%'":*?<>{}]/g,'');
	return getDecimalNumber(strMoney);
	//return strMoney.replace(/[^0-9]/g,'');
}
/**********************************************************************************************************************************************/

	/**************************************From Here start DATA Reset functions*********************************************/ 

/**********************************************************************************************************************************************/

function resetData53Json(){	
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	householdMemberObj.name.firstName = "";
	householdMemberObj.name.middleName = "";
	householdMemberObj.name.lastName = "";
	householdMemberObj.name.suffix = "";
	householdMemberObj.dateOfBirth = "";
	
	householdMemberObj.householdContact.contactPreferences.emailAddress = "";
	householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage = "";
	householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage = "";	
	
	householdMemberObj.householdContact.contactPreferences.preferredContactMethod = "";
	
	householdMemberObj.householdContact.homeAddressIndicator = false;
	householdMemberObj.householdContact.homeAddress.city = "";
	householdMemberObj.householdContact.homeAddress.county = "";
	householdMemberObj.householdContact.homeAddress.postalCode = "";
	householdMemberObj.householdContact.homeAddress.state = "";
	householdMemberObj.householdContact.homeAddress.streetAddress1 = "";
	householdMemberObj.householdContact.homeAddress.streetAddress2 = "";
	
	householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator = true;
	householdMemberObj.householdContact.mailingAddress.city = "";
	householdMemberObj.householdContact.mailingAddress.county = "";
	householdMemberObj.householdContact.mailingAddress.postalCode = "";
	householdMemberObj.householdContact.mailingAddress.state = "";
	householdMemberObj.householdContact.mailingAddress.streetAddress1 = "";
	householdMemberObj.householdContact.mailingAddress.streetAddress2 = "";
	
	householdMemberObj.householdContact.phone.phoneNumber = "";
	householdMemberObj.householdContact.phone.phoneExtension = "";	
	householdMemberObj.householdContact.phone.phoneType = "CELL";
	
	householdMemberObj.householdContact.otherPhone.phoneNumber = "";
	householdMemberObj.householdContact.otherPhone.phoneExtension = "";
	householdMemberObj.householdContact.otherPhone.phoneType = "HOME";	
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = householdMemberObj;
	
}

function resetData54Json(){	
	var helpType = webJson.singleStreamlinedApplication.helpType;	
	BrokerObj=webJson.singleStreamlinedApplication.broker;	
	BrokerObj.brokerFederalTaxIdNumber = "";
	BrokerObj.brokerName = "";
	webJson.singleStreamlinedApplication.broker = BrokerObj;
	assisterObj=webJson.singleStreamlinedApplication.assister;	
	assisterObj.assisterID = "";
	assisterObj.assisterName = "";
	webJson.singleStreamlinedApplication.broker = assisterObj;	
	webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator = null;
	webJson.singleStreamlinedApplication.getHelpIndicator = null;	
	
	AuthorizedRepresentativeObj = webJson.singleStreamlinedApplication.authorizedRepresentative;
	AuthorizedRepresentativeObj.name.firstName = "";
	AuthorizedRepresentativeObj.name.middleName = "";
	AuthorizedRepresentativeObj.name.lastName = "";
	AuthorizedRepresentativeObj.name.suffix = "";		
	AuthorizedRepresentativeObj.emailAddress ="";		
	AuthorizedRepresentativeObj.address.streetAddress1 = "";
	AuthorizedRepresentativeObj.address.streetAddress2 = "";
	AuthorizedRepresentativeObj.address.city = "";
	AuthorizedRepresentativeObj.address.state = "";
	AuthorizedRepresentativeObj.address.postalCode = "";
		
	AuthorizedRepresentativeObj.phone[0].phoneNumber = "";
	AuthorizedRepresentativeObj.phone[0].phoneExtension = "";
	AuthorizedRepresentativeObj.phone[1].phoneNumber = "";
	AuthorizedRepresentativeObj.phone[1].phoneExtension = "";
	AuthorizedRepresentativeObj.phone[2].phoneNumber = "";
	AuthorizedRepresentativeObj.phone[2].phoneExtension = "";
		
	AuthorizedRepresentativeObj.partOfOrganizationIndicator = null;			
	AuthorizedRepresentativeObj.companyName = "";
	AuthorizedRepresentativeObj.organizationId = "";
	AuthorizedRepresentativeObj.partOfOrganizationIndicator = null;
	AuthorizedRepresentativeObj.signatureisProofIndicator = null;	
	AuthorizedRepresentativeObj.signature = "";
		
	webJson.singleStreamlinedApplication.authorizedRepresentative=AuthorizedRepresentativeObj;	
}
function resetData55Json(){	
	webJson.singleStreamlinedApplication.applyingForhouseHold = null;
	webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator = null;
}

function resetData60Json(i){	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];	
	householdMemberObj.planToFileFTRIndicator = null;	
	householdMemberObj.marriedIndicator = null;
	householdMemberObj.planToFileFTRJontlyIndicator = null;
	householdMemberObj.taxFiler.liveWithSpouseIndicator = null;
	householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator = null;
	householdMemberObj.taxFilerDependant.taxFilerDependantIndicator = null;	
	//householdMemberObj.applyingForCoverageIndicator = false; // HIX-50485 - According to Anmol applyingForCoverageIndicator should not be reset.
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1] = householdMemberObj;
}
function resetData61Json(currentID){	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
	householdMemberObj.gender = "";
	householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator = null;
	householdMemberObj.socialSecurityCard.socialSecurityNumber = "";
	householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator = null;
	householdMemberObj.socialSecurityCard.firstNameOnSSNCard = "";
	householdMemberObj.socialSecurityCard.middleNameOnSSNCard = "";	
	householdMemberObj.socialSecurityCard.lastNameOnSSNCard = "";
	householdMemberObj.socialSecurityCard.suffixOnSSNCard = "";
	householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN = "";
	householdMemberObj.socialSecurityCard.individualMandateExceptionIndicator = null;
	householdMemberObj.socialSecurityCard.individualManadateExceptionID = null;
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1] = householdMemberObj;	
}
function resetData66Json () {
	var x =	getNoOfHouseHolds();
	for(var y=1;y<=x;y++){
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1]; 
		householdMemberObj.healthCoverage.otherInsuranceIndicator = null;
		householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected = "";		  
		householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType = "";
		householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName = "";
		householdMemberObj.specialCircumstances.americanIndianAlaskaNativeIndicator = null;
		householdMemberObj.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator = null;
		householdMemberObj.americanIndianAlaskaNative.state = "";
		householdMemberObj.americanIndianAlaskaNative.tribeName = "";		  
		householdMemberObj.specialCircumstances.fosterChild = false;		  
		householdMemberObj.disabilityIndicator = false;		  		  
		householdMemberObj.specialCircumstances.pregnantIndicator = false;
		householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy = 0;		  
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1]=householdMemberObj;		  
	}
}


