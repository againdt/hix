package com.getinsured.hix.batch.agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.agency.Agency;

@Repository
public interface IAgencyRepository extends  JpaRepository<Agency, Integer> {

	Agency findById(Long agencyId);

	@Query("from Agency a where a.federalTaxId = :federalTaxId")
	Agency findAgencyByFederalTaxId(@Param("federalTaxId")String federalTaxId);

}