package com.getinsured.hix.batch.enrollment.partitioner;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.CmsSftpService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.EnrollmentCmsFileTransferLog;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;

@Component("cmsSftpInboundPartitioner")
@Scope("step")
public class CmsSftpInboundPartitioner implements Partitioner {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CmsSftpInboundPartitioner.class);

	private CmsSftpService cmsSftpService;
	
	@Override
	public Map<String, ExecutionContext> partition(int gridZise) {
		LOGGER.info("CmsSftpOutboundPartitioner" + new Date());
		List<BatchJobExecution> batchExecutionList = cmsSftpService.getRunningBatchList("cmsSftpInboundJob");
		Boolean enableJob =  Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.CMS_SFTP_SWITCH));
		if(batchExecutionList != null && batchExecutionList.size() == EnrollmentConstants.ONE){
			Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
			if(enableJob){
				cmsSftpService.checkConnection();
				int partition = 1;
				for(EnrollmentCmsFileTransferLog.ReportType reportType : EnrollmentCmsFileTransferLog.ReportType.values()){
					if(EnrollmentCmsFileTransferLog.ReportType.CARRIERRECON != reportType) {
					partitionMap.put("partition-"+"1-" + reportType.toString(),
							getExecutionContext(reportType.toString().toString(), partition++));
				}
				}	
			}
			else if(LOGGER.isWarnEnabled()){
				LOGGER.warn("CmsSftpInboundPartitioner :: Exiting the batch since either of one proerty is failed: enableJob: "+enableJob);
			}
			return partitionMap;
		}
		return null;
	}

	private ExecutionContext getExecutionContext(String reportType, int partitionNumber) {
		ExecutionContext ec = new ExecutionContext();
		ec.putInt("partition", partitionNumber);
		ec.putString("reportType", reportType);
		return ec;
	}

	/**
	 * @return the cmsSftpService
	 */
	public CmsSftpService getCmsSftpService() {
		return cmsSftpService;
	}

	/**
	 * @param cmsSftpService the cmsSftpService to set
	 */
	public void setCmsSftpService(CmsSftpService cmsSftpService) {
		this.cmsSftpService = cmsSftpService;
	}
	
}
