<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="row">
	<div class="span4">
		<h2>Create Role</h2>
	</div>
	<div class="span12">
	<form class="form-stacked" id= "frmcreate" name="frmcreate" action="create" method="post">
		<df:csrfToken/>
		<fieldset>
			<legend></legend>
				<p></p><br />
			<div class="profile">
				<div class="clearfix">
					<label for="role_name" class="required">Role Name</label>
					<div class="input">
						<input type="text" name="name" id="name" value="" class="xlarge" size="30">
					</div>				          	
				</div>
				<div class="clearfix">
					<label for="description" class="required">Provide Role Description</label>
					<div class="input">
						<textarea name="description" id="description" class="xlarge" rows="2" cols="20" size="5"></textarea>
					</div>				           
				</div>
				<div class="clearfix">
					<label for="landing_page" class="required">Landing Page</label>
					<span style="float:left;width:160px;">
						<br><%= request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() %>
					</span>
						<div class="input">
							<input type="text" name="landingPage" id="landingPage" value="" class="xlarge" size="30">
						</div>				            
				</div>
			</div>
		</fieldset>
		<div class="actions">
			<input type="submit" name="submit" id="submit" value="Create Role" class="btn primary" title="Create Role">    		    
		</div>
	</form>
	</div>
	<div class="notes" style="display:none">
		<div class="row">
			<div class="span">
		      <p>This information is required to determine initial eligibility to use the SHOP Exchange.  
		      All fields are subjected to a basic format validation of the input (e.g. a zip code must be a 
		      5 digit number). The question mark icon which opens a light-box provides helpful information. 
		      The EIN number can be validated with an external data source if an adequate web API is available.
		      </p>
		    </div>
	    </div>
	</div>			
</div>