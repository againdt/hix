<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style>
#tile.span4 {margin-top: 13px !important;}
.tooltip-inner {
	max-width:24em;
  overflow: hidden;
}
#providerSearchHeader .map__button {margin-left: 0;  }
.benefits .table .details {padding-left: 10px;}
.w100 { width: 100%; }
.w50 { width: 50%; }
.w25 { width: 25%; }
.detailPlanTile #offv th{padding-top: 8px;}
.detailPlanTile #gd th{padding-top: 8px;}
.detailPlanTile #ee td{padding-top: 4px; vertical-align: top;}
#estimatedCostDetail .cell { width: 50%; }
#havingBaby .cell { width: 50%; }
.providerRows .cell { width: 50%; }
.providerHeatMapDetail .cell { width: 50%; }
.row-detail.providerRows .e-cell {width: 100%;}
.row-detail.providerRows .e-cell a{margin-left: 0;}
[id^=doctorVisit] .cell,
[id^=test] .cell,
[id^=drug] .cell,
[id^=outpatient] .cell,
[id^=urgent] .cell,
[id^=hospital] .cell,
[id^=mentalHealth] .cell,
[id^=pregnancy] .cell,
[id^=specialNeed] .cell,
[id^=childrensVision] .cell,
[id^=childrensDental] .cell { width: 25%; }
#providerHeatMapDetail .map__button { padding: 7px 0; }
</style>



<div class="ps-detail__highlights-container">

</div>


<c:if test="${providerSearchConfig == 'ON'}">
	<div class="ps-detail__group-container">
		<div class="ps-detail__group">
			<div class="ps-detail__header ps-detail__header--group">
				<spring:message code="pd.label.title.doctorsandfacilities"/>
			</div>

			<div class="ps-detail__group-body ">
				<div class="ps-detail__providers-list">

				</div>
				<c:if test="${providerMapConfig == 'ON'}">
					<div class="ps-detail__provider-map" id="providerHeatMapDetail">
						<label for="providerDetailDistance" class="pull-left margin5-t"><span id="providerHeatMapDetailCount" class="details"></span>
						<spring:message code="pd.label.title.doctorsAvailable"/></label>
						<select class="input-medium u-margin-0" id="providerDetailDistance" name="providerDetailDistance" onchange="App.views.detailView.detailPlan();">
							<option value="1"><spring:message code="pd.label.preferences.radius1"/></option>
							<option value="2"><spring:message code="pd.label.preferences.radius2"/></option>
							<option value="5"><spring:message code="pd.label.preferences.radius5"/></option>
							<option selected value="10"><spring:message code="pd.label.preferences.radius10"/></option>
							<option value="20"><spring:message code="pd.label.preferences.radius20"/></option>
							<option value="30"><spring:message code="pd.label.preferences.radius30"/></option>
							<option value="50"><spring:message code="pd.label.preferences.radius50"/></option>
							<option value="100"><spring:message code="pd.label.preferences.radius100"/></option>
						</select>
						<spring:message code="pd.label.title.radiusOf"/> ${zipcode}.
						<span id="providerHeatMapDetailButton" class="details"></span>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</c:if>


<div class="ps-detail__group-container">

	<c:choose>
		<c:when test="${simplifiedDeductible == 'ON' }">
			<div class="ps-detail__group">
				<div class="ps-detail__header ps-detail__header--group">
					<spring:message code="pd.label.planDetail.deductible"/> & <spring:message code="pd.label.icon.outOfPocket"/>
					<i class="icon-chevron-up toggle"></i>
				</div>

				<div class="toggle-div">
					<div class="ps-detail__group-body u-table">
						<div class="ps-detail__sub-header">
							<div class="u-table-cell">

							</div>
							<div class="u-table-cell">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
							</div>
							<div class="u-table-cell">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
							</div>
							<div class="u-table-cell u-table-cell--10">
							</div>
						</div>

						<div class="ps-detail__service" id="simplifiedDeductibleDetail">
							<div class="u-table-cell ps-detail__service-label">
								<c:choose>
									<c:when test="${stateCode == 'CT' || stateCode == 'MN'}">
										<a
											href="#"
											rel="tooltip"
											role="tooltip" data-placement="right" data-html="true"
											data-original-title="<spring:message code="pd.label.tooltip.deductible"/>"
											aria-label="<spring:message code="pd.label.planDetail.deductible"/>  help text: <spring:message code="pd.label.tooltip.deductible"/> help text finished">
											<spring:message code="pd.label.planDetail.deductible"/>
										</a>
									</c:when>
									<c:otherwise>
										<spring:message code="pd.label.planDetail.deductible"/>
									</c:otherwise>
								</c:choose>
							</div>
						</div>

						<div class="ps-detail__service" id="simplifiedSeparateDrugDeductibleDetail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.separate.drugdeductible"/>
							</div>
						</div>

						<div class="ps-detail__service" id="simplifiedOOPMaxDetail">
							<div class="u-table-cell ps-detail__service-label">
								<c:if test="${stateCode != 'CA'}">
									<a
										href="#"
										rel="tooltip"
										role="tooltip"
										data-placement="right"
										data-html="true"
										data-original-title="<spring:message code="pd.tooltip.oopmax"/>" aria-label="<spring:message code="pd.label.deductible.oopmax"/> help text:<spring:message code="pd.tooltip.oopmax"/> help text finished">
								</c:if>
								<spring:message code="pd.label.deductible.oopmax"/>
								<c:if test="${stateCode != 'CA'}">
									</a>
								</c:if>
							</div>
						</div>

						<c:if test="${stateCode == 'CA'}">
							<div class="ps-detail__service" id="simplifiedMaxCostPerPrescriptionDetail">
								<div class="u-table-cell ps-detail__service-label">
									<spring:message code="pd.label.maxCostPerPrescription"/>
								</div>
							</div>
						</c:if>

						<div class="ps-detail__service" id="simplifiedOtherDeductibleDetail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.deductible.otherDeductibles"/>
							</div>
						</div>
					</div>
				</div>
			</div>

		</c:when>
		<c:otherwise>
			<div class="ps-detail__group">
				<div class="ps-detail__header ps-detail__header--group">
					<spring:message code="pd.label.planDetail.deductible"/> & <spring:message code="pd.label.icon.outOfPocket"/>
					<i class="icon-chevron-up toggle"></i>
				</div>

				<div class="toggle-div">
					<div class="ps-detail__group-body u-table">
						<div class="ps-detail__service" id="planDeductible1Detail">
							<div class="u-table-cell ps-detail__service-label">
								<a data-original-title="<spring:message code="pd.label.deductible.link1indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link1"/></a>
								(<spring:message code="pd.label.commontext.individual"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible2Detail">
							<div class="u-table-cell ps-detail__service-label">
								<a data-original-title="<spring:message code="pd.label.deductible.link2indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link2"/></a>
								(<spring:message code="pd.label.commontext.individual"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible3Detail">
							<div class="u-table-cell ps-detail__service-label">
								<a data-original-title="<spring:message code="pd.label.deductible.link1famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link1"/></a>
								(<spring:message code="pd.label.commontext.family"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible4Detail">
							<div class="u-table-cell ps-detail__service-label">
								<a data-original-title="<spring:message code="pd.label.deductible.link2famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link2"/></a>
								(<spring:message code="pd.label.commontext.family"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible5Detail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.deductible.link3"/> (<spring:message code="pd.label.commontext.individual"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible6Detail">
							<div class="u-table-cell ps-detail__service-label">
								<a data-original-title="<spring:message code="pd.label.deductible.link4indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link4"/></a>
								(<spring:message code="pd.label.commontext.individual"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible7Detail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.deductible.link5"/> (<spring:message code="pd.label.commontext.individual"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible8Detail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.deductible.link6"/> (<spring:message code="pd.label.commontext.individual"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible9Detail">
							<div class="u-table-cell ps-detail__service-label">
								<a data-original-title="<spring:message code="pd.label.deductible.link3famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link3"/></a>
								(<spring:message code="pd.label.commontext.family"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible10Detail">
							<div class="u-table-cell ps-detail__service-label">
								<a data-original-title="<spring:message code="pd.label.deductible.link4famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link4"/></a>
								(<spring:message code="pd.label.commontext.family"/>)
							</div>
						</div>


						<div class="ps-detail__service" id="planDeductible11Detail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.deductible.link5"/> (<spring:message code="pd.label.commontext.family"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductible12Detail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.deductible.link6"/> (<spring:message code="pd.label.commontext.family"/>)
							</div>
						</div>

						<div class="ps-detail__service" id="planDeductibleOtherDetail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.deductible.linkOther"/>
							</div>
						</div>

					</div>
				</div>
			</div>


		</c:otherwise>
	</c:choose>

	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<spring:message code="pd.label.icon.doctorVisit"/>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>
					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>

					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>

					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>

				</div>

				<div class="ps-detail__service" id="doctorVisit1Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode != 'ID'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link1tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.doctorVisit.link1"/>

						<c:if test="${stateCode != 'ID'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="doctorVisit2Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode != 'ID'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link2tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.doctorVisit.link2"/>

						<c:if test="${stateCode != 'ID'}">
							</a>
						</c:if>
					</div>
				</div>

				<c:if test="${stateCode != 'CT'}">
					<div class="ps-detail__service" id="doctorVisit3Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode != 'ID'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link3tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.doctorVisit.link3"/>

							<c:if test="${stateCode != 'ID'}">
								</a>
							</c:if>
						</div>
					</div>
				</c:if>

				<div class="ps-detail__service" id="doctorVisit4Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode != 'ID'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link4tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.doctorVisit.link4"/>

						<c:if test="${stateCode != 'ID'}">
							</a>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<c:if test="${stateCode == 'CT'}">
				<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.icon.tests.tooltip" htmlEscape="false"/>">
			</c:if>
			<spring:message code="pd.label.icon.tests"/>
			<c:if test="${stateCode == 'CT'}"></a></c:if>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>
					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>


					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>

					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>
				</div>

				<c:if test="${stateCode != 'MN'}">
					<div class="ps-detail__service" id="test1Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tests.link1tooltip" htmlEscape="false"/>">
							</c:if>
							<spring:message code="pd.label.tests.link1"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
				</c:if>

				<div class="ps-detail__service" id="test2Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA' || stateCode == 'CT'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tests.link2tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.tests.link2"/>

						<c:if test="${stateCode == 'CA' || stateCode == 'CT'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="test3Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA' || stateCode == 'CT'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tests.link3tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.tests.link3"/>

						<c:if test="${stateCode == 'CA' || stateCode == 'CT'}">
							</a>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>




	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<c:if test="${stateCode == 'CT'}">
				<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.icon.drugs.tooltip" htmlEscape="false"/>">
			</c:if>
			<spring:message code="pd.label.icon.drugs"/>
			<c:if test="${stateCode == 'CT'}">
				</a>
			</c:if>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>
					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>



					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>
					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>
				</div>

				<div class="ps-detail__service" id="drug1Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode != 'MN'}">
							<a
								href="#"
								rel="tooltip"
								data-html="true"
								role="tooltip"
								data-placement="right"
								data-original-title="<spring:message code="pd.label.title.details.genericDrugs.tooltip" htmlEscape="false"/>"
								aria-label="<spring:message code="pd.label.drugs.link1"/> help text: <spring:message code="pd.label.title.details.genericDrugs.tooltip" htmlEscape="false"/> help text finished">
						</c:if>

						<spring:message code="pd.label.drugs.link1"/>


						<c:if test="${stateCode != 'MN'}">
							</a>
						</c:if>

					</div>
				</div>

				<div class="ps-detail__service" id="drug2Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link2tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.drugs.link2"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="drug3Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link3tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.drugs.link3"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="drug4Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link4tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.drugs.link4"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<c:if test="${stateCode == 'CA'}">
					<div class="ps-detail__service" id="drug5Detail">
						<div class="u-table-cell ps-detail__service-label">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link5tooltip" htmlEscape="false"/>">
								<spring:message code="pd.label.drugs.link5"/>
							</a>
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>



	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<c:if test="${stateCode == 'CT'}">
				<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.icon.outpatient.tooltip" htmlEscape="false"/>">
			</c:if>
			<spring:message code="pd.label.icon.outpatient"/>
			<c:if test="${stateCode == 'CT'}"> </a> </c:if>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>
					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>


					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>

					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>
				</div>

				<c:if test="${stateCode != 'MN'}">
					<div class="ps-detail__service" id="outpatient1Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.outpatient.link1tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.outpatient.link1"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
				</c:if>

				<c:if test="${stateCode != 'CT'}">
					<div class="ps-detail__service" id="outpatient2Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.outpatient.link2tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.outpatient.link2"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>

					<c:if test="${stateCode != 'MN' && stateCode != 'NV'}">
						<div class="ps-detail__service" id="outpatientServicesOfficeVisitsDetail">
							<div class="u-table-cell ps-detail__service-label">
								<c:if test="${stateCode == 'CA'}">
									<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.outpatient.officeVisitstooltip" htmlEscape="false"/>">
								</c:if>

								<spring:message code="pd.label.outpatient.officeVisits"/>

								<c:if test="${stateCode == 'CA'}">
									</a>
								</c:if>
							</div>
						</div>
					</c:if>
				</c:if>


			</div>
		</div>
	</div>



	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<spring:message code="pd.label.icon.er"/> & <spring:message code="pd.label.icon.urgentCare"/>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>

					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>



					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>
					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>
				</div>


				<div class="ps-detail__service" id="urgent1Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.urgentCare.link1tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.urgentCare.link1"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>

					</div>
				</div>

				<div class="ps-detail__service" id="urgent2Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.urgentCare.link2tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.urgentCare.link2"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<div class="ps-detail__service" id="urgent3Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.urgentCare.link3tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.urgentCare.link3"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>

				<c:if test="${stateCode != 'CT' && stateCode != 'MN' && stateCode != 'NV'}">
					<div class="ps-detail__service" id="urgentProfessionalFeeDetail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.urgentCare.professionalFeetooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.urgentCare.professionalFee"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>



	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<spring:message code="pd.label.icon.hospital"/>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>
					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>



					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>

					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>
				</div>

				<div class="ps-detail__service" id="hospital1Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode != 'ID'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.hospital.link1tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.hospital.link1"/>

						<c:if test="${stateCode != 'ID'}">
							</a>
						</c:if>
					</div>
				</div>

				<c:if test="${stateCode != 'CT'}">
					<div class="ps-detail__service" id="hospital2Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.hospital.link2tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.hospital.link2"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>





	<c:if test="${stateCode != 'CT'}">
		<div class="ps-detail__group">
			<div class="ps-detail__header ps-detail__header--group">
				<spring:message code="pd.label.icon.health"/>
				<i class="icon-chevron-up toggle"></i>
			</div>

			<div class="toggle-div">
				<div class="ps-detail__group-body u-table">
					<div class="ps-detail__sub-header">
						<div class="u-table-cell">

						</div>
						<div class="u-table-cell">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
						</div>


						<div class="u-table-cell">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
						</div>

						<div class="u-table-cell u-table-cell--10">
							<spring:message code="pd.label.title.additionalInfo"/>
						</div>
					</div>

					<div class="ps-detail__service" id="mentalHealth1Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.link1tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.mentalHealth.link1"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>

					<div class="ps-detail__service" id="mentalHealth2Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.link2tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.mentalHealth.link2"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>

					<c:if test="${stateCode != 'MN' && stateCode != 'NV'}">
						<div class="ps-detail__service" id="mentalHealthInpatientProfFeeDetail">
							<div class="u-table-cell ps-detail__service-label">
								<c:if test="${stateCode == 'CA'}">
									<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.inpatientProfFeetooltip" htmlEscape="false"/>">
								</c:if>

								<spring:message code="pd.label.mentalHealth.inpatientProfFee"/>

								<c:if test="${stateCode == 'CA'}">
									</a>
								</c:if>
							</div>
						</div>
					</c:if>

					<div class="ps-detail__service" id="mentalHealth3Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.link3tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.mentalHealth.link3"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>

					<div class="ps-detail__service" id="mentalHealth4Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.link4tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.mentalHealth.link4"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>

					<c:if test="${stateCode != 'MN' && stateCode != 'NV'}">
						<div class="ps-detail__service" id="mentalHealthSubDisorderInpProfFeeDetail">
							<div class="u-table-cell ps-detail__service-label">
								<c:if test="${stateCode == 'CA'}">
									<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.subsDisorderInpProfFeetooltip" htmlEscape="false"/>">
								</c:if>

								<spring:message code="pd.label.mentalHealth.subsDisorderInpProfFee"/>

								<c:if test="${stateCode == 'CA'}">
									</a>
								</c:if>
							</div>
						</div>
					</c:if>

				</div>
			</div>
		</div>



		<div class="ps-detail__group">
			<div class="ps-detail__header ps-detail__header--group">
				<spring:message code="pd.label.icon.pregnancy"/>
				<i class="icon-chevron-up toggle"></i>
			</div>

			<div class="toggle-div">
				<div class="ps-detail__group-body u-table">
					<div class="ps-detail__sub-header">
						<div class="u-table-cell">

						</div>
						<div class="u-table-cell">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
						</div>

						<div class="u-table-cell">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
						</div>

						<div class="u-table-cell u-table-cell--10">
							<spring:message code="pd.label.title.additionalInfo"/>
						</div>
					</div>

					<c:if test="${stateCode != 'MN'}">
						<div class="ps-detail__service" id="pregnancy1Detail">
							<div class="u-table-cell ps-detail__service-label">
								<c:if test="${stateCode == 'CA'}">
									<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.pregnancy.link1tooltip" htmlEscape="false"/>">
								</c:if>

								<spring:message code="pd.label.pregnancy.link1"/>

								<c:if test="${stateCode == 'CA'}">
									</a>
								</c:if>
							</div>
						</div>
					</c:if>

					<div class="ps-detail__service" id="pregnancy2Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.pregnancy.link2tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.pregnancy.link2"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>

					<c:if test="${stateCode != 'MN' && stateCode != 'NV'}">
						<div class="ps-detail__service" id="pregnancyInpatientProfFeeDetail">
							<div class="u-table-cell ps-detail__service-label">
								<c:if test="${stateCode == 'CA'}">
									<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.pregnancy.inpatientProfFeetooltip" htmlEscape="false"/>">
								</c:if>

								<spring:message code="pd.label.pregnancy.inpatientProfFee"/>

								<c:if test="${stateCode == 'CA'}">
									</a>
								</c:if>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</div>



		<div class="ps-detail__group">
			<div class="ps-detail__header ps-detail__header--group">
				<spring:message code="pd.label.icon.otherSpecialNeeds"/>
				<i class="icon-chevron-up toggle"></i>
			</div>

			<div class="toggle-div">
				<div class="ps-detail__group-body u-table">
					<div class="ps-detail__sub-header">
						<div class="u-table-cell">

						</div>
						<div class="u-table-cell">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
						</div>


						<div class="u-table-cell">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
						</div>

						<div class="u-table-cell u-table-cell--10">
							<spring:message code="pd.label.title.additionalInfo"/>
						</div>
					</div>

					<div class="ps-detail__service" id="specialNeed1Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link1tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link1"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed2Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link2tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link2"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed3Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link3tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link3"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed4Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link4tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link4"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed5Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link5tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link5"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed6Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link6tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link6"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed7Detail">
						<div class="u-table-cell ps-detail__service-label">
							<spring:message code="pd.label.otherSpecialNeeds.link7"/>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed8Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link8tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link8"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>

					<c:if test="${stateCode != 'MN'}">
						<div class="ps-detail__service" id="specialNeed9Detail">
							<div class="u-table-cell ps-detail__service-label">
								<c:if test="${stateCode == 'CA'}">
									<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link9tooltip" htmlEscape="false"/>">
								</c:if>

								<spring:message code="pd.label.otherSpecialNeeds.link9"/>

								<c:if test="${stateCode == 'CA'}">
									</a>
								</c:if>
							</div>
						</div>
					</c:if>


					<div class="ps-detail__service" id="specialNeed10Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link10tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link10"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed11Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link11tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link11"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed12Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link12tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link12"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed13Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link13tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link13"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
					<div class="ps-detail__service" id="specialNeed14Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link14tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.otherSpecialNeeds.link14"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="ps-detail__group">
			<div class="ps-detail__header ps-detail__header--group">
				<spring:message code="pd.label.icon.children.vision"/>
				<i class="icon-chevron-up toggle"></i>
			</div>

			<div class="toggle-div">
				<div class="ps-detail__group-body u-table">
					<div class="ps-detail__sub-header">
						<div class="u-table-cell">

						</div>
						<div class="u-table-cell">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
						</div>


						<div class="u-table-cell">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
						</div>

						<div class="u-table-cell u-table-cell--10">
							<spring:message code="pd.label.title.additionalInfo"/>
						</div>
					</div>

					<div class="ps-detail__service" id="childrensVision1Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode != 'MN' && stateCode != 'ID'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.vision.link1tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.children.vision.link1"/>

							<c:if test="${stateCode != 'MN' && stateCode != 'ID'}">
								</a>
							</c:if>
						</div>
					</div>

					<div class="ps-detail__service" id="childrensVision2Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode != 'MN' && stateCode != 'ID'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.vision.link2tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.children.vision.link2"/>

							<c:if test="${stateCode != 'MN' && stateCode != 'ID'}">
								</a>
							</c:if>
						</div>
					</div>

					<c:if test="${stateCode == 'MN'}">
						<div class="ps-detail__service" id="hearingAidsDetail">
							<div class="u-table-cell ps-detail__service-label">
								<spring:message code="pd.label.children.hearing.link1"/>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</c:if>



	<div class="ps-detail__group">
		<div class="ps-detail__header ps-detail__header--group">
			<spring:message code="pd.label.icon.children.dental"/>
			<i class="icon-chevron-up toggle"></i>
		</div>

		<div class="toggle-div">
			<div class="ps-detail__group-body u-table">
				<div class="ps-detail__sub-header">
					<div class="u-table-cell">

					</div>
					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
					</div>



					<div class="u-table-cell">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
					</div>

					<div class="u-table-cell u-table-cell--10">
						<spring:message code="pd.label.title.additionalInfo"/>
					</div>
				</div>

				<div class="ps-detail__service" id="childrensDental1Detail">
					<div class="u-table-cell ps-detail__service-label">
						<spring:message code="pd.label.children.dental.link1"/>
					</div>
				</div>
				<div class="ps-detail__service" id="childrensDental2Detail">
					<div class="u-table-cell ps-detail__service-label">
						<spring:message code="pd.label.children.dental.link2"/>
					</div>
				</div>
				<div class="ps-detail__service" id="childrensDental3Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link3tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.link3"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>
				<div class="ps-detail__service" id="childrensDental4Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link4tooltip" htmlEscape="false"/>">
						</c:if>
						<spring:message code="pd.label.children.dental.link4"/>
						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>
				<div class="ps-detail__service" id="childrensDental5Detail">
					<div class="u-table-cell ps-detail__service-label">
						<spring:message code="pd.label.children.dental.link5"/>
					</div>
				</div>
				<div class="ps-detail__service" id="childrensDental6Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link6tooltip" htmlEscape="false"/>">
						</c:if>
						<spring:message code="pd.label.children.dental.link6"/>
						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>
				<div class="ps-detail__service" id="childrensDental7Detail">
					<div class="u-table-cell ps-detail__service-label">
						<spring:message code="pd.label.children.dental.link7"/>
					</div>
				</div>

				<c:if test="${stateCode != 'MN'}">
					<div class="ps-detail__service" id="childrensDental8Detail">
						<div class="u-table-cell ps-detail__service-label">
							<c:if test="${stateCode == 'CA'}">
								<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link8tooltip" htmlEscape="false"/>">
							</c:if>

							<spring:message code="pd.label.children.dental.link8"/>

							<c:if test="${stateCode == 'CA'}">
								</a>
							</c:if>
						</div>
					</div>
				</c:if>



				<div class="ps-detail__service" id="childrensDental9Detail">
					<div class="u-table-cell ps-detail__service-label">
						<c:if test="${stateCode == 'CA'}">
							<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link9tooltip" htmlEscape="false"/>">
						</c:if>

						<spring:message code="pd.label.children.dental.link9"/>

						<c:if test="${stateCode == 'CA'}">
							</a>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>



</div>

<script type="text/html" id="tileTpl">
	<div class="ps-detail__panel">
		<@ if($("#gpsVersion").val() == 'V1') { @>
			<@ if(smartScore >=80){ @>
				<div class="ps-detail__tile detailPlanTile best details">
					<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.green"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
			<@ } else if(smartScore >=60){ @>
				<div class="ps-detail__tile detailPlanTile ok details">
					<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.yellow"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
			<@ } else { @>
				<div class="ps-detail__tile detailPlanTile poor details">
					<a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.red"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
			<@ } @>

				<div class="score">
					<div class="circle-wrapper">
						<div class="center-circle"></div>
						<div class="slice">
							<div class="pie score_<@=smartScore@> pie-color"></div>
						</div>
						<div class="circle-core center-circle">
							<div class="circle-num"><@=smartScore@></div>
						</div>
					</div>
				</div>
			</a>
		<@ } else { @>
			<div class="ps-detail__tile" tabindex="0" aria-label="You are on plan <@=name@>">
		<@ } @>
			<!-- tile header -->
			<@ if($('#stateCode').val() != "CA") { @>
				<div class="cp-tile__header">
					<a
						href="#"
						rel="tooltip"
						data-html="true"
						data-placement="bottom"
						data-original-title="<spring:message code="pd.tooltip.summary.estimatedCostDetail"/>">
	    	        	<spring:message code="pd.label.title.expenseEstimate"/>
	        	   	</a>

					<@ if(expenseEstimate == "Lower Cost"){ @>
						<span class="ps-detail__tile-estimate ps-detail__tile-estimate--lower"><span><spring:message code="pd.label.low"/></span></span>
					<@ } else if(expenseEstimate == "Average"){ @>
						<span class="ps-detail__tile-estimate ps-detail__tile-estimate--average"><span><spring:message code="pd.label.medium"/></span></span>
					<@ } else if(expenseEstimate == "Pricey"){ @>
						<span class="ps-detail__tile-estimate ps-detail__tile-estimate--higher"><span><spring:message code="pd.label.high"/></span></span>
					<@ } @>
				</div>
			<@ } @>
			<!-- tile header end-->


			<!-- tile body -->
	        <div class="cp-tile__body">
	            <!-- logo img -->
	            <div id="detail_<@=id@>" class="cp-tile__img-link cp-tile__img-link-white detail">
	                <@ if ($('#stateCode').val() == 'CT' && issuer.length > 75) {@>
	                    <img src="<@=issuerLogo@>" alt="<@=issuer.substr(0,75)@>..." class="cp-tile__img">
	                    <!-- <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer.substr(0,75)@>..." onload="resizeimage(this)" /></div> -->
	                <@ } else if($('#stateCode').val() != 'CT' && issuer.length > 24) { @>
	                    <img src="<@=issuerLogo@>" alt="<@=issuer.substr(0,24)@>..." class="cp-tile__img">
	                    <!-- <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer.substr(0,24)@>..." onload="resizeimage(this)" /></div> -->
	                <@ } else { @>
	                    <img src="<@=issuerLogo@>" alt="<@=issuer@>" class="cp-tile__img">
	                    <!-- <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="planimg carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer@>" onload="resizeimage(this)" /></div> -->
	                <@ } @>
	            </div>
	            <!-- logo img end-->

	            <!-- plan name -->
	            <div class="cp-tile__plan-name">
	                <a class="detail" href="#detail" id="detail_<@=id@>" data-placement="bottom" rel="tooltip" role="tooltip" data-original-title="<@=name@>" aria-label="<@=name@>">
	                    <@ if ($('#stateCode').val() == 'CT' && name.length > 75) {@>
	                        <@=name.substr(0,75)@>...
	                    <@ } else if($('#stateCode').val() != 'CT' && name.length > 24) { @>
	                        <@=name.substr(0,24)@>...
	                    <@ } else {@>
	                        <@=name@>
	                    <@ } @>
	                </a>
	            </div>
	            <!-- plan name end-->

				<!-- metal tier -->
	            <@ if(level === 'PLATINUM') {@>
	                <div class="cp-tile__metal-tier cp-tile__metal-tier--platinum"><spring:message code="pd.label.filterby.platinum"/> &nbsp;<@=networkType@></div>
	            <@ } else if(level === 'GOLD') { @>
	                <div class="cp-tile__metal-tier cp-tile__metal-tier--gold"><spring:message code="pd.label.filterby.gold"/> &nbsp;<@=networkType@></div>
	            <@ } else if(level === 'SILVER') { @>
					<div class="cp-tile__metal-tier cp-tile__metal-tier--silver"><spring:message code="pd.label.filterby.silver"/> &nbsp;<@=networkType@></div>
				<@ } else if(level === 'BRONZE') {@>
                    <div class="cp-tile__metal-tier cp-tile__metal-tier--bronze"><spring:message code="pd.label.filterby.bronze"/> &nbsp;<@=networkType@></div>
                <@ } else if(level === 'CATASTROPHIC') {@>
                    <div class="cp-tile__metal-tier cp-tile__metal-tier--catastrophic"><spring:message code="pd.label.filterby.catastropic"/> &nbsp;<@=networkType@></div>
	            <@ } else {@>
	                <div class="cp-tile__metal-tier cp-tile__metal-tier--bronze"><@=level@> &nbsp;<@=networkType@></div>
	            <@ } @>

				<@ var planNum = issuerPlanNumber.substr(issuerPlanNumber.length - 2, issuerPlanNumber.length) @>
				<@ if(planNum !== '00' && planNum !== '01') {@>
					<div class="cp-tile__metal-tier cp-tile__metal-tier--csr cp-tile__metal-tier--csr-right ps-csr-show hide">
						<spring:message code="pd.label.title.csr"/>
						<a
							class="cp-tooltip__icon-link"
							href="#"
							rel="tooltip"
							data-html="true"
							data-placement="top"
							data-original-title="<spring:message code="pd.label.title.CSREligibletooltip"/>">
		    	       		<i class="icon-question-sign cp-tile__icon"></i>
		        	 	</a>
					</div>
				<@ } @>
	            <!-- metal tier end-->

				<!-- current plan -->
				<@ if ($("#enrollmentType").val() == 'A' && $("#initialHealthPlanId").val() == planId){ @>
					<div>
						<span class="cp-tile__current-plan">
							<spring:message code="pd.label.title.yourCurrentPlan"/>
						</span>
					</div>
				<@ } @>
				<!-- current plan end -->

	            <!-- premium -->
	            <div class="cp-tile__premium">
					<span class="cp-tile__premium-amount">$<@=premiumAfterCredit.toFixed(2) @></span>
					<span class="cp-tile__premium-month"><spring:message code="pd.label.title.premiumPerMonth"/></span>

					<@ if($('#stateCode').val() === "MN") { @>
						<a
							class="u-margin-left-10"
							href="#"
							rel="tooltip"
							data-html="true"
							data-placement="bottom"
							data-original-title="<@=getMemberLevelPremiumHtml(planDetailsByMember, premium.toFixed(2), aptc.toFixed(2), premiumAfterCredit.toFixed(2))@>">
							<spring:message code="pd.label.title.details"/>
						</a>
					<@ } @>
				</div>
	            <!-- premium end-->

	            <!-- tax credit -->
				<c:if test="${exchangeType != 'OFF' && (maxAptc > 0 || maxStateSubsidy > 0)}">
					<@ var totalCredit = 0; @>
					<@ if (aptc > 0 || $('#stateCode').val() === 'CA') {@>
					<@ totalCredit = totalCredit + (+aptc.toFixed(2)); @>
					<@ } @>
					<@ if(stateSubsidy){ @>
					<@    totalCredit = totalCredit + (+stateSubsidy.toFixed(2)); @>
					<@ } @>
					<div class="cp-tile__tax-credit">
					<@ if (totalCredit !== 0 && stateSubsidy !== null) { @>
						<a
						href="#"
						rel="tooltip"
						role="tooltip"
						data-html="true"
						data-placement="bottom"
						data-original-title="<table>
                                                <tr>
                                                    <td><spring:message code="pd.label.commontext.federal" htmlEscape="false"/>
                                                    <td>=</td>
                                                    <td>$<@=aptc && aptc.toFixed(2)@><td/>
                                                </tr>
                                                <tr>
                                                    <td><spring:message code="pd.label.commontext.state" htmlEscape="false"/>
                                                    <td>=</td>
                                                    <td>$<@=stateSubsidy && stateSubsidy.toFixed(2)@><td/>
                                                <tr>
                                                    <td><spring:message code="pd.label.commontext.total" htmlEscape="false"/>
                                                    <td>=</td>
                                                    <td>$<@=totalCredit && totalCredit.toFixed(2)@><td/>
                                                </tr>
                                              </table>">
							<spring:message arguments="<@=totalCredit.toFixed(2)@>" code="pd.label.commontext.afterTaxCredit" htmlEscape="false"/>
						</a>

					<@ } else { @>
						<spring:message arguments="<@=totalCredit.toFixed(2)@>" code="pd.label.commontext.afterTaxCredit" htmlEscape="false"/>
					<@ } @>
					</div>
				</c:if>
	            <!-- tax credit end -->
	        </div>
	        <!-- tile body ends -->

			<!-- tile footer -->
	        <div class="ps-detail__tile-footer <@=issuerPlanNumber@>">
	            <@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
					<a href="#" id="cart_<@=planId@>" class="btn btn-primary ps-detail__tile-add addToCart" title="<spring:message code="pd.label.title.add"/>">
	                    <spring:message code="pd.label.title.add"/>
	                    <i class="icon-shopping-cart"></i>
	                </a>
	            <@ } @>
			</div>
	        <!-- tile footer ends -->
		</div>
		<!-- tile end -->

		<div class="ps-detail__highlights">
			<p class="ps-detail__header"><spring:message code="pd.label.commontext.planHighLights"/></p>

			<table class="ps-detail__highlights-table">

				<!-- total expense estimate -->
				<@ if($('#stateCode').val() == "CA") { @>
					<tr>
						<th scope="row">
							<a
								href="#"
								class="gtm_tile_total_expense"
								onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover', 'eventAction': '<spring:message code="pd.label.title.expenseEstimate" /> - Hover', 'eventLabel': '<@=name@>'});"
								rel="tooltip"
								role="tooltip"
								data-placement="right"
								data-html="true"
								data-original-title="<spring:message code="pd.tooltip.summary.estimatedCostDetail" htmlEscape="true"/>"
								aria-label="<spring:message code="pd.label.title.expenseEstimate"/>help text:<spring:message code="pd.tooltip.summary.estimatedCostDetail" htmlEscape="true"/> help text finished">

								<spring:message code="pd.label.title.expenseEstimate" />

							</a>
						</th>
						<td class="cp-tile__value">
							<a
								href="#"
								rel="tooltip"
								role="tooltip"
								data-placement="bottom"
								data-html="true"
								data-original-title="<spring:message arguments="<@=parseFloat(annualPremiumAfterCredit).toFixed(2)@>,<@=parseFloat(estimatedTotalHealthCareCost - annualPremiumAfterCredit).toFixed(2)@>, <@=parseFloat(estimatedTotalHealthCareCost).toFixed(2)@>" code="pd.tooltip.summary.dynamicCostDetail"/>"
								aria-label="<spring:message code="pd.label.title.expenseEstimate"/>help text:<spring:message code="pd.tooltip.summary.dynamicCostDetail" htmlEscape="true"/> help text finished">

								$<@=parseFloat(estimatedTotalHealthCareCost).toFixed(2)@>
							</a>
						</td>
					</tr>
				<@ } @>
				<!-- total expense estimate end -->

				<!-- Plan name (Adult) -->
				<tr>
					<th scope="row">
						<spring:message code="pd.label.planName"/>
					</th>
					<td class="cp-tile__value">
						<@=name@>
					</td>
				</tr>
				<!-- Plan name end -->

				<!-- primary care visits -->
				<tr>
					<th scope="row">
						<c:choose>
							<c:when test="${stateCode == 'CT' || stateCode == 'MN'}">
								<a
									href="#"
									rel="tooltip"
									role="tooltip"
									data-html="true"
									data-placement="right"
									data-original-title="<spring:message code="pd.label.title.details.officeVisits.tooltip" htmlEscape="false"/>"
									aria-label="<spring:message code="pd.label.title.officeVisits"/> help text: <spring:message code="pd.label.title.details.officeVisits.tooltip" htmlEscape="false"/> help text finished">
									<spring:message code="pd.label.title.officeVisits"/>
								</a>
							</c:when>
							<c:otherwise>
								<spring:message code="pd.label.title.officeVisits"/>
							</c:otherwise>
						</c:choose>
					</th>
					<td class="cp-tile__value">
						<@ if($('#stateCode').val() == "CA") { @>
							<spring:message code="pd.label.title.youpay"/>&nbsp;<@=planTier1.PRIMARY_VISIT.tileDisplayVal@>
						<@ } else if($('#stateCode').val() == "MN") { @>

							<a
								href="#"
								rel="tooltip"
								role="tooltip"
								data-html="true"
								data-placement="top"
								data-original-title="<@=planTier1.PRIMARY_VISIT.displayVal@>"
								aria-label="<@=planTier1.PRIMARY_VISIT.displayVal@>">
								<@=planTier1.PRIMARY_VISIT.tileDisplayVal@>
								<@ if(planTier1.PRIMARY_VISIT.tileDisplayVal.indexOf('/') === -1) { @>
									<@ if( planTier1.PRIMARY_VISIT.tileDisplayVal.indexOf('%') !== -1 ) { @>
										<spring:message code="pd.label.coinsurance"/>
									<@ } else { @>
										<spring:message code="pd.label.copay"/>
									<@ } @>
								<@ } @>
							</a>
						<@ } else { @>
							<@=planTier1.PRIMARY_VISIT.tileDisplayVal@>
						<@ } @>
					</td>
				</tr>
				<!-- primary care visits end-->

				<!-- generic drugs -->
				<tr>
					<th scope="row">
						<c:choose>
							<c:when test="${stateCode == 'CT' || stateCode == 'MN'}">
								<a
									href="#"
									rel="tooltip"
									data-html="true"
									role="tooltip"
									data-placement="right"
									data-original-title="<spring:message code="pd.label.title.details.genericDrugs.tooltip" htmlEscape="false"/>"
									aria-label="<spring:message code="pd.label.title.genericDrugs"/> help text: <spring:message code="pd.label.title.details.genericDrugs.tooltip" htmlEscape="false"/> help text finished">
									<spring:message code="pd.label.title.genericDrugs"/>
								</a>
							</c:when>
							<c:otherwise>
								<spring:message code="pd.label.title.genericDrugs"/>
							</c:otherwise>
						</c:choose>

					</th>
					<td class="cp-tile__value">
						<@ if($('#stateCode').val() == "CA") { @>
                            <spring:message code="pd.label.title.youpay"/>&nbsp;<@=planTier1.GENERIC.tileDisplayVal @>
                        <@ } else if($('#stateCode').val() == "MN"){ @>
							<a
                                href="#"
                                rel="tooltip"
                                data-html="true"
                                role="tooltip"
                                data-placement="top"
                                data-original-title="<@=planTier1.GENERIC.displayVal @>"
                                aria-label="<@=planTier1.GENERIC.displayVal @>">
                                <@=planTier1.GENERIC.tileDisplayVal @>
                            </a>
						<@ } else { @>
                            <@=planTier1.GENERIC.tileDisplayVal @>
                        <@ } @>
					</td>
				</tr>
				<!-- generic drugs end-->

				<!-- deductible -->
				<tr>
					<th scope="row">
						<c:choose>
							<c:when test="${stateCode == 'CT' || stateCode == 'MN'}">
								<a
									href="#"
									class="gtm_tile_yearly_deductible"
		                            onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover', 'eventAction': '<spring:message code="pd.label.Deductible"/> - Hover', 'eventLabel': '<@=name@>'});"
									rel="tooltip"
									role="tooltip"
									data-placement="right"
									data-html="true"
									data-original-title="<spring:message code="pd.label.tooltip.deductible"/>"
									aria-label="<spring:message code="pd.label.Deductible"/>  help text: <spring:message code="pd.label.tooltip.deductible"/> help text finished">
									<spring:message code="pd.label.Deductible"/>
								</a>
							</c:when>
							<c:otherwise>
								<spring:message code="pd.label.Deductible"/>
							</c:otherwise>
						</c:choose>

					</th>
					<td class="cp-tile__value">
						<@ if(intgMediDrugDeductible !=null ){@>
							<a
								href="#"
								rel="tooltip"
								role="tooltip"
								data-placement="top"
								data-html="true"
								data-original-title="<spring:message arguments="<@=intgMediDrugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>"
								aria-label="$<@=intgMediDrugDeductible.toFixed()@> help text:<spring:message arguments="<@=intgMediDrugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/> help text finished">
								$<@=intgMediDrugDeductible.toFixed()@>
							</a>
						<@ } else if((medicalDeductible != null) || (drugDeductible!= null)){ @>
							<@  if(medicalDeductible !=null && drugDeductible != null){ @>
								<a
									href="#"
									rel="tooltip"
									role="tooltip"
									data-placement="top"
									data-html="true"
									data-original-title="<spring:message arguments="<@=medicalDeductible.toFixed()@>,<@=drugDeductible.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/>"
									aria-label="$<@=medicalDeductible.toFixed()@> / $<@=drugDeductible.toFixed()@> help text <spring:message arguments="<@=medicalDeductible.toFixed()@>,<@=drugDeductible.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/> help text finished">
									$<@=medicalDeductible.toFixed()@> / $<@=drugDeductible.toFixed() @>
								</a>
							<@ }else if(medicalDeductible !=null  ){ @>
								<a
									href="#"
									rel="tooltip"
									role="tooltip"
									data-placement="top"
									data-html="true"
									data-original-title="<spring:message arguments="<@=medicalDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>"
									aria-label="$<@=medicalDeductible.toFixed()@> help text <spring:message arguments="<@=medicalDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>help text finished">
									$<@=medicalDeductible.toFixed() @>
								</a>
							<@ }else if(drugDeductible != null ){ @>
								<a
									href="#"
									rel="tooltip"
									role="tooltip"
									data-placement="top"
									data-html="true"
									data-original-title="<spring:message arguments="<@=drugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>"
									aria-label="$<@=drugDeductible.toFixed()@>help text<spring:message arguments="<@=drugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>help text finished">
									$<@=drugDeductible.toFixed() @>
								</a>
							<@ } @>
						<@ } @>

						<@ if($('#stateCode').val() == "CA") { @>
							<a
								href="#"
								rel="tooltip"
								role="tooltip"
								data-placement="top"
								data-html="true"
								data-original-title="<spring:message code="pd.label.title.deductibledesc.tooltip"/>"
								aria-label="<spring:message code="pd.label.title.deductibledesc.tooltip"/> help text finished">
								<spring:message code="pd.label.title.deductibledesc"/>
							</a>
						<@ } @>
					</td>
				</tr>
				<!-- deductible end -->


				<!-- out-of-pocket maximum -->
				<@ if($("#showOopOnOff").val() == 'ON') { @>
					<tr>
						<th scope="row">

							<a
								href="#"
								rel="tooltip"
								role="tooltip"
								data-placement="right"
								data-html="true"
								data-original-title="<spring:message code="pd.tooltip.oopmax"/>" aria-label="<spring:message code="pd.label.oopmax"/> help text:<spring:message code="pd.tooltip.oopmax"/> help text finished">
								<spring:message code="pd.label.oopmax"/>
							</a>
						</th>

						<td class="cp-tile__value">
							<@ if(intgMediDrugOopMax != null){@>
								<a
									href="#"
									rel="tooltip"
									role="tooltip"
									data-placement="top"
									data-html="true"
									data-original-title="<spring:message arguments="<@=intgMediDrugOopMax.toFixed()@>" code="pd.tooltip.intgMediDrugOopMax" htmlEscape="false"/>"
									aria-label="$<@=intgMediDrugOopMax.toFixed()@>help text<spring:message arguments="<@=intgMediDrugOopMax.toFixed()@>" code="pd.tooltip.intgMediDrugOopMax" htmlEscape="false"/>help text finished">
									$<@=intgMediDrugOopMax.toFixed()@>
								</a>
							<@ } else if((medicalOopMax != null ) || (drugOopMax != null)){ @>
								<@ if(medicalOopMax !=null && drugOopMax != null){ @>
									<a
										href="#"
										rel="tooltip"
										role="tooltip"
										data-placement="top"
										data-html="true"
										data-original-title="<spring:message arguments="<@=medicalOopMax.toFixed()@>,<@=drugOopMax.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/>"
										aria-label="$<@=medicalOopMax.toFixed()@> / $<@=drugOopMax.toFixed()@>help text<spring:message arguments="<@=medicalOopMax.toFixed()@>,<@=drugOopMax.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/>help text finished">
										$<@=medicalOopMax.toFixed()@> / $<@=drugOopMax.toFixed()@>
									</a>
								<@ } else if(medicalOopMax !=null){@>
									<a
										href="#"
										rel="tooltip"
										role="tooltip"
										data-placement="top"
										data-html="true"
										data-original-title="<spring:message arguments="<@=medicalOopMax.toFixed()@>" code="pd.tooltip.medicalOopMax" htmlEscape="false"/>"
										aria-label="$<@=medicalOopMax.toFixed()@>help text<spring:message arguments="<@=medicalOopMax.toFixed()@>" code="pd.tooltip.medicalOopMax" htmlEscape="false"/>help text finished">
										$<@=medicalOopMax.toFixed()@>
									</a>
								<@ } else if(drugOopMax != null){ @>
									<a
										href="#"
										rel="tooltip"
										role="tooltip"
										data-placement="top"
										data-html="true"
										data-original-title="<spring:message arguments="<@=drugOopMax.toFixed()@>" code="pd.tooltip.drugOOPMax" htmlEscape="false"/>"
										aria-label="$<@=drugOopMax.toFixed()@>help text<spring:message arguments="<@=drugOopMax.toFixed()@>" code="pd.tooltip.drugOOPMax" htmlEscape="false"/>help text finished">
										$<@=drugOopMax.toFixed()@>
									</a>
								<@ } @>
							<@ } else if($('#stateCode').val() == "CA"){ @>
								<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
							<@ } @>
						</td>
					</tr>
				<@ } @>
				<!-- out-of-pocket maximum end-->

				<!-- network -->
				<@ if($("#showNetworkTransparencyRating").val() == 'ON') { @>
					<tr>
						<th scope="row">
							<a
								href="#"
								rel="tooltip"
								role="tooltip"
								data-placement="right"
								data-html="true"
								data-original-title="<spring:message code="pd.tooltip.network" htmlEscape="true"/>"
								aria-label="<spring:message code="pd.label.network" htmlEscape="true"/>help text:<spring:message code="pd.tooltip.network" htmlEscape="true"/>help text finished">
								<spring:message code="pd.label.network" htmlEscape="true"/>
							</a>
						</th>

						<td>
							<@ if(networkTransparencyRating === null){@>
								<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
							<@ } else {@>
								<@ if(networkTransparencyRating.toUpperCase() === 'BASIC'){@>
									<a
										href="#"
										rel="tooltip"
										role="tooltip"
										data-placement="top"
										data-html="true"
										data-original-title="<spring:message code="pd.tooltip.network.basic" htmlEscape="true"/>"
										aria-label="<spring:message code="pd.network.basic" htmlEscape="true"/>help text<spring:message code="pd.tooltip.network.basic" htmlEscape="true"/>help text finished">
										<spring:message code="pd.network.basic" htmlEscape="true"/>
									</a>
								<@ } else if(networkTransparencyRating.toUpperCase() === 'STANDARD') { @>
									<a
										href="#"
										rel="tooltip"
										role="tooltip"
										data-placement="top"
										data-html="true"
										data-original-title="<spring:message code="pd.tooltip.network.standard" htmlEscape="true"/>"
										aria-label="<spring:message code="pd.network.standard" htmlEscape="true"/>help text<spring:message code="pd.tooltip.network.standard" htmlEscape="true"/>help text finished">
										<spring:message code="pd.network.standard" htmlEscape="true"/>
									</a>
								<@ } else if(networkTransparencyRating.toUpperCase() === 'BROAD') { @>
									<a
										href="#"
										rel="tooltip"
										role="tooltip"
										data-placement="top"
										data-html="true"
										data-original-title="<spring:message code="pd.tooltip.network.broad" htmlEscape="true"/>"
										aria-label="<spring:message code="pd.network.broad" htmlEscape="true"/>help text<spring:message code="pd.tooltip.network.broad" htmlEscape="true"/>help text finished">
										<spring:message code="pd.network.broad" htmlEscape="true"/>
									</a>
								<@ } @>
							<@ } @>
						</td>
					</tr>
				<@ } @>
				<!-- network end -->

				<!-- HSA compatible -->
				<tr id="hsaTypeDetail">
					<th scope="row">
						<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.commontext.hsatypetooltip"/>">
							<spring:message code="pd.label.commontext.hsatype" />
						</a>
					</th>

				</tr>
				<!-- HSA compatible end -->

				<!-- quality rating -->
				<@ if($("#showQualityRating").val() == 'YES') { @>
					<tr>
						<th scope="row">
							<a
								href="#"
								class="gtm_tile_quality_rating"
								onmouseover="window.dataLayer.push({'event': 'hoverEvent', 'eventCategory': 'Plan Selection - Hover', 'eventAction': '<spring:message code="pd.label.tooltip.qualityRatings.1"/> - Hover', 'eventLabel': '<@=name@>'});"
								rel="tooltip"
								role="tooltip"
								data-html="true"
								data-placement="right"
								data-original-title="<spring:message code="pd.label.qualityRating.tooltip" htmlEscape="false"/>" aria-label="<spring:message code="pd.label.tooltip.qualityRatings.1"/> help text:<spring:message code="pd.label.qualityRating.tooltip" htmlEscape="false"/> help text finished">
								<spring:message code="pd.label.tooltip.qualityRatings.1"/>
							</a>
						</th>
						<td class="cp-tile__value">
							<!-- qualityRating values -->
							<@ if (issuerQualityRating != null) { @>
								<@= populateQualityRatingsHtml(issuerQualityRating)@>
								<c:if test="${stateCode == 'MN'}">
									&nbsp;&nbsp;<spring:message code="pd.label.tooltip.qualityRatings.10"/>
								</c:if>
							<@ } else { @>
								<c:if test="${stateCode == 'CA'}">
									<a
										data-placement="top"
										rel="tooltip"
										role="tooltip"
										href="#"
										data-original-title="<spring:message code="pd.label.tooltip.qualityRatings.9"/>"
										aria-label="<spring:message code="pd.label.tooltip.qualityRatings.9"/>">
										<spring:message code="pd.label.tooltip.qualityRatings.7"/>
									</a>
								</c:if>

								<c:if test="${stateCode != 'CA'}">
									<spring:message code="pd.label.tooltip.qualityRatings.7"/>
								</c:if>

								<@ } @>
							<!-- qualityRating values ends -->
						</td>
					</tr>
				<@ } @>
				<!-- quality rating end -->


				<!-- benefits -->
				<c:forEach var="benefit" items="${benefitsArray}">
					<tr>
						<th scope="row">
							<@ if(['${benefit}'] == "Pediatric dental"){ @>
                                <@ if($('#stateCode').val() == "MN"){ @>
                                <a
                                    href="#"
								   data-placement="right"
                                    data-original-title="<spring:message code="pd.label.commontext.benefits.pediatricDental.description" htmlEscape="true"/>"
                                    rel="tooltip"
                                    role="tooltip"
                                    aria-label="<spring:message code="pd.label.commontext.benefits.pediatricDental.description" htmlEscape="true"/>">
                                    <spring:message code="pd.label.title.childrensDental" htmlEscape="true"/>
                                </a>
                                <@ } else {@>
                                    <spring:message code="pd.label.title.childrensDental" htmlEscape="true"/>
                                <@ } @>
                            <@ } else if(['${benefit}'] == "HSA Eligible"){ @>
						   		 <a
                                    href="#"
                                    data-placement="right"
								   	data-original-title="<spring:message code="pd.label.title.hsaQualifiedtooltip" htmlEscape="true"/>"
                                    rel="tooltip"
                                    role="tooltip"
                                    aria-label="<spring:message code="pd.label.title.hsaQualifiedtooltip" htmlEscape="true"/>">
									<spring:message code="pd.label.title.hsaEligible" htmlEscape="true"/>
                                </a>
						   <@ } else { @>
                                ${benefit}
                            <@ } @>
						</th>
						<td class="cp-tile__value">
							<@ if(benefitsCoverage['${benefit}'] == "GOOD"){ @>
								<a
									href="#"
									data-placement="top"
									data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.good" htmlEscape="true"/>"
									rel="tooltip"
									role="tooltip"
									aria-label="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.good" htmlEscape="true"/>">
									<i class="icon-circle icon-circle--good"></i>
								</a>
							<@ } else if(benefitsCoverage['${benefit}'] == "OK"){ @>
								<a
									href="#"
									data-placement="top"
									data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.ok" htmlEscape="true"/>"
									rel="tooltip"
									role="tooltip"
									aria-label="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.ok" htmlEscape="true"/>">
									<i class="icon-circle icon-circle--ok"></i>
								</a>
							<@ } else if(benefitsCoverage['${benefit}'] == "POOR"){ @>
								<a
									href="#"
									data-placement="top"
									data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.poor" htmlEscape="true"/>"
									rel="tooltip"
									role="tooltip"
									aria-label="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.poor" htmlEscape="true"/>">
									<i class="icon-circle icon-circle--poor"></i>
								</a>
							<@ } else { @>
								<a
									href="#"
									data-placement="top"
									data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.na" htmlEscape="true"/>"
									rel="tooltip"
									role="tooltip"
									aria-label="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.na" htmlEscape="true"/>">
									<i class="icon-ban-circle"></i>
								</a>
							<@ } @>
						</td>
					</tr>
				</c:forEach>
				<!-- benefits end -->

				<!-- prescriptions -->
				<c:if test="${prescriptionSearchConfig=='ON'}">
					<@ _.each (prescriptionResponseList, function (prescription) { @>
						<tr>
							<th scope="row">
								<@ let drugName = prescription.drugName; @>
								<@ let drugInfo = prescription.drugDosage; @>
								<@ if(prescription.isGenericCovered === "Y" && (prescription.isDrugCovered === "N" || prescription.isDrugCovered === "U")){@>
									<@ drugInfo = prescription.genericDosage; @>
								<@ } @>

								<a
									href="#"
									data-placement="right"
									rel="tooltip"
									data-html="true"
									data-original-title="<@=drugInfo@>">
									<@ if (drugName.length > 19) {@>
										<@=drugName.substr(0, 19)@>...
									<@ } else { @>
										<@=drugName@>
									<@ } @>
								</a>
							</th>

							<td class="cp-tile__value">
								<@ if(prescription.isDrugCovered === "Y" || prescription.isGenericCovered === "Y"){ @>
								  	<@ let authRequired = prescription.authRequired; @>
								  	<@ let stepTherapyRequired = prescription.stepTherapyRequired; @>
								  	<@ if(prescription.isGenericCovered === "Y" && (prescription.isDrugCovered === "N" || prescription.isDrugCovered === "U")){ @>
									 	<@ authRequired = prescription.genericAuthRequired; @>
										<@ stepTherapyRequired = prescription.genericStepTherapyRequired; @>
								    <@ } @>
									<a
										href="#"
										data-placement="top"
										data-original-title="<spring:message arguments="<@=authRequired@>,<@=stepTherapyRequired@>" code="pd.label.drugCovered"/>"
										rel="tooltip"
										role="tooltip"
										aria-label="<spring:message arguments="<@=authRequired@>,<@=stepTherapyRequired@>" code="pd.label.drugCovered"/>">
										<i class="icon-ok-sign"></i>
									</a>

									<@ if(prescription.isGenericCovered === "Y" && (prescription.isDrugCovered === "N" || prescription.isDrugCovered === "U")){ @>
										<a
											href="#"
											data-placement="top"
											data-original-title="<spring:message code="pd.label.generic.tooltip"/>"
											rel="tooltip"
											role="tooltip"
											aria-label="<spring:message code="pd.label.generic.tooltip"/>">
											<spring:message code="pd.label.generic"/>
										</a>
									<@ } @>
								<@ } else { @>
									<@if(prescription.isDrugCovered === "U"){ @>
										<a
											href="#"
											data-placement="top"
											data-original-title="<spring:message code="pd.label.unknown"/>"
											rel="tooltip"
											role="tooltip"
											aria-label="<spring:message code="pd.label.unknown"/>">
											<i class='UNKNOWN'></i>
										</a>
									<@} else if(prescription.isDrugCovered === "N"){ @>
										<a
											href="#"
											data-placement="top"
											data-original-title="<spring:message code="pd.label.notCovered"/>"
											rel="tooltip"
											role="tooltip"
											aria-label="<spring:message code="pd.label.notCovered"/>">
											<i class='icon-ban-circle'></i>
										</a>
									<@}@>
								<@}@>
							</td>
						</tr>
					<@ }); @>
				</c:if>
				<!-- prescriptions end -->

				<!-- providers -->
				<c:if test="${providerSearchConfig=='ON'}">
					<@ _.each (doctors, function (doctor) { @>
						<tr>
							<th scope="row">
								<@ var providerType = doctor.providerType @>
								<@ var docName = doctor.providerName; @>
								<@ if (providerType == "doctor") { @>
									<@ if (docName.indexOf('Dr.') != 0 && $('#stateCode').val() == 'CA') { @>
										<@ docName = "<spring:message code="pd.label.doctor.title"/>" + " "+ docName @>
									<@ } @>


									<a
										href="#"
										data-placement="right"
										rel="tooltip"
										role="tooltip"
										data-original-title="<@=docName@>"
										aria-label="<@=docName@>">
										<@ if (docName.length > 15) { @>
											<@=docName.substr(0, 15)@>...
										<@ } else { @>
											<@=docName@>
										<@ } @>
									</a>
								<@ } else if (providerType == "dentist") { @>
									<@ docName =  " " + docName + " "@>

									<a
										href="#"
										data-placement="right"
										rel="tooltip"
										role="tooltip"
										data-original-title="<@=docName@>"
										aria-label="<@=docName@>">
										<@ if (docName.length > 15) {@>
											<@=docName.substr(0, 15)@>...
										<@ } else { @>
											<@=docName@>
										<@ } @>
									</a>

								<@ } else { @>
									<a
										href="#"
										data-placement="right"
										rel="tooltip" role="tooltip"
										data-original-title="<@=docName@>">
										<@ if (docName.length > 15) { @>

												<@=docName.substr(0, 15)@>...
										<@ } else { @>
											<@=docName@>
										<@ } @>
									</a>
								<@ } @>

							</th>

							<td class="cp-tile__value">
								<@ if(doctor.networkStatus== "outNetWork"){ @>
									<a
										href="#"
										data-delay='{"hide": "2000"}'
										data-placement="top"
										data-html="true"
										data-original-title="<spring:message code="pd.label.outNetwork"/>"
										rel="tooltip"
										role="tooltip"
										aria-label="<spring:message code="pd.label.outNetwork"/>">
										<i class="icon-ban-circle"></i>
									</a>
								<@ } else if(doctor.networkStatus == "inNetWork"){ @>
									<a
										href="#"
										data-placement="top"
										data-original-title="<spring:message code="pd.label.availableInNetwork"/>"
										rel="tooltip"
										role="tooltip"
										aria-label="<spring:message code="pd.label.availableInNetwork"/>">
										<i class="icon-ok-sign"></i>
									</a>
								<@ } else { @>
									<a
										href="#"
										data-placement="top"
										data-original-title="<spring:message code="pd.label.availablilityUnknown"/>"
										rel="tooltip"
										role="tooltip"
										aria-label="<spring:message code="pd.label.availablilityUnknown"/>">
										<i class="icon-question-sign"></i>
									</a>
								<@ } @>
							</td>
						</tr>
					<@ }); @>
				</c:if>
				<!-- providers end -->

			</table>
		</div>
	</div>

	<div class="ps-detail__benefits">
		<p class="ps-detail__header"><spring:message code="pd.label.commontext.benefitsResournces"/></p>
		<ul class="ps-detail__benefits-list">
			<li class="ps-detail__benefit-list">
				<@ if(sbcUrl != null && sbcUrl != '') { @>
					<a
					class="ps-detail__benefit-link ps-detail__benefit-link--active border-b gtm_summary_benefits_external"
					target="_blank" href="<@=sbcUrl@>"
					id="externalWarnBox"
					onclick="window.dataLayer.push({'event': 'contentdownloadEvent', 'eventCategory': 'Plan Selection - Content Download', 'eventAction': 'Document Download', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.sbcUrl"/>'});
					window.dataLayer.push({'event': 'outboundlinkEvent', 'eventCategory': 'Plan Selection - Outbound Links', 'eventAction': 'Outbound Link Click', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.sbcUrl"/>'});">
						<i class="icon-list-ul"></i>
						<spring:message code="pd.label.link.sbcUrl" htmlEscape="true"/>
					</a>
				<@ } else { @>
					<span class="ps-detail__benefit-link ps-detail__benefit-link--inactive">
						<i class="icon-list-ul"></i>
						<spring:message code="pd.label.link.sbcUrlNotAvailable" htmlEscape="true"/>
					</span>
				<@ } @>
			</li>

			<li class="ps-detail__benefit-list">
				<@ if(planBrochureUrl != null && planBrochureUrl != '') { @>
					<a
					class="ps-detail__benefit-link ps-detail__benefit-link--active border-b gtm_plan_brochure"
					target="_blank"
					href="<@=planBrochureUrl@>"
					id="externalWarnBox"
					onclick="window.dataLayer.push({'event': 'contentdownloadEvent', 'eventCategory': 'Plan Selection - Content Download', 'eventAction': 'Document Download', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.downloadBrochure"/>'});
					window.dataLayer.push({'event': 'outboundlinkEvent', 'eventCategory': 'Plan Selection - Outbound Links', 'eventAction': 'Outbound Link Click', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.downloadBrochure"/>'});">
						<i class="icon-file-text-alt"></i>
						<spring:message code="pd.label.link.downloadBrochure"/>
					</a>
				<@ } else { @>
					<span class="ps-detail__benefit-link ps-detail__benefit-link--inactive">
						<i class="icon-file-text-alt"></i>
						<spring:message code="pd.label.link.planBrochureNotAvailable" htmlEscape="true"/>
					</span>
				<@ } @>
			</li>

			<li class="ps-detail__benefit-list">
				<@ if(providerLink != null && providerLink != '') { @>
					<a class="ps-detail__benefit-link ps-detail__benefit-link--active border-b gtm_provider_directory" target="_blank" href="<@=providerLink@>" id="externalWarnBox"
						onclick="window.dataLayer.push({'event': 'outboundlinkEvent', 'eventCategory': 'Plan Selection - Outbound Links', 'eventAction': 'Outbound Link Click', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.providerDirectoryUrl"/>'});"
						<i class="icon-stethoscope"></i>
						<spring:message code="pd.label.link.providerDirectoryUrl" htmlEscape="true"/>
					</a>
				<@ } else { @>
					<span class="ps-detail__benefit-link ps-detail__benefit-link--inactive">
						<i class="icon-stethoscope"></i>
						<spring:message code="pd.label.link.providerDirectoryNotAvailable" htmlEscape="true"/>
					</span>
				<@ } @>
			</li>

			<li class="ps-detail__benefit-list">
				<@ if(formularyUrl != null && formularyUrl != '') { @>
					<a class="ps-detail__benefit-link ps-detail__benefit-link--active border-b gtm_drug_list"
					target="_blank"
					href="<@=formularyUrl@>"
					id="externalWarnBox"
					onclick="window.dataLayer.push({'event': 'outboundlinkEvent', 'eventCategory': 'Plan Selection - Outbound Links', 'eventAction': 'Outbound Link Click', 'eventLabel': '<@=issuer@> | <spring:message code="pd.label.link.formularyUrl"/>'});">
						<i class="icon-medkit"></i>
						<spring:message code="pd.label.link.formularyUrl" htmlEscape="true"/>
					</a>
				<@ } else { @>
					<span class="ps-detail__benefit-link ps-detail__benefit-link--inactive">
						<i class="icon-medkit"></i>
						<spring:message code="pd.label.link.formularyUrlNotAvailable" htmlEscape="true"/>
					</span>
				<@ } @>
			</li>

		</ul>
	</div>
</script>


<script type="text/html" id="providerSearchInfoTplDetail">
	<@ if(doctors && doctors != ""){ @>
		<@ _.each (doctors, function (doctor) { @>
			<div class="ps-detail__provider-container">
				<div class="ps-detail__provider-details">
					<div class="u-bold">
						<@ if (doctor.providerType == "doctor") { @>
							<@ if (doctor.providerName.indexOf('Dr.') !== 0) { @>
								<spring:message code="pd.label.doctor.title"/>
							<@ } @>
							<@=doctor.providerName@>
						<@ } else if (doctor.providerType == "dentist") {@>
							<@=doctor.providerName@>
						<@ } else { @>
							<@=doctor.providerName@>
						<@ } @>
					</div>
					<div class="ps-detail__provider-specialty">
						<@=doctor.providerSpecialty@>
					</div>
					<@if(doctor.providerPhone !== ""){@>
						<div>
							<@=doctor.providerPhone@>
						</div>
					<@}@>
					<div><@=doctor.providerAddress@></div>
					<div><@=doctor.providerCity@>, <@=doctor.providerState@> <@=doctor.providerZip@></div>
					<a href="preferences" class="txt-capitalize">
						<spring:message code="pd.label.title.edit" htmlEscape="true"/>
					</a>
				</div>

				<div class="ps-detail__provider-coverage">
					<@ if(doctor.networkStatus == "outNetWork"){ @>
                        <@ if($('#stateCode').val() == "CA"){ @>
                            <i class="icon-ban-circle"></i>
                            <spring:message code="pd.label.outNetwork"/>
                        <@ } else {@>
                            <a
							href="#"
							data-delay='{"hide": "2000"}'
							data-placement="top"
							data-html="true"
							data-original-title="<spring:message code="pd.label.outNetwork"/>"
							rel="tooltip"
							role="tooltip"
							aria-label="<spring:message code="pd.label.outNetwork"/>">
							<i class="icon-ban-circle"></i>
						</a>
                        <@ } @>
					<@ }else if(doctor.networkStatus == "inNetWork"){ @>
						<i class="GOOD"></i>
						<spring:message code="pd.label.inNetwork" htmlEscape="true"/>
					<@ }else{ @>
						<i class="UNKNOWN"></i>
						<spring:message code="pd.label.unknownAffiliation" htmlEscape="true"/>
					<@ } @>
				</div>
			</div>

		<@ }); @>

	<@ } else { @>
		<div>
			<a href="preferences" class="margin10-l">
				<spring:message code="pd.label.title.checkDoctor" />
			</a>
		</div>
	<@ } @>
</script>

<div class="row-fluid" id="detailHead" style="display:none">

  <div class="table-row span4 margin0" id="tile"></div>


  <div class="span8 detail-wrapper">
    <div class="benefits dental-benefits clearfix">
      <%-- <div class="table">
		    <div class="row header">
			    <div class="cell header-name">
			    	<c:if test="${stateCode == 'CT'}">
			    		<a href="#" rel="tooltip" data-html="true" data-placement="right" data-original-title="<spring:message code="pd.label.title.summary.tooltip" htmlEscape="false"/>">
			    	</c:if>
				    <spring:message code="pd.label.title.summary"/>
				    <c:if test="${stateCode == 'CT'}">
				    	</a>
				    </c:if>
			    </div>
		    </div>
        <div id="estimatedCostDetail" class="row-detail clearfix">
          <div class="cell">
          	<a href="#" rel="tooltip" data-html="true" data-placement="right" data-original-title="<spring:message code="pd.tooltip.summary.estimatedCostDetail"/>">
            	<spring:message code="pd.label.planDetail.expenseEstimate"/>
           	</a>
          </div>
        </div>
        <div id="productNameDetail" class="row-detail clearfix">
        	<div class="cell">
	          	<spring:message code="pd.label.planName"/>
       	  	</div>
        </div>
        <div id="productTypeDetail" class="row-detail clearfix">
        	<div class="cell">
	          	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.producttype" htmlEscape="true"/>">
	          		<spring:message code="pd.label.commontext.productType"/>
	        	</a>
       	  	</div>
        </div>
        <div id="csrDetail" class="row-detail clearfix">
        	<div class="cell">
	        	<a data-placement="right"  rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.csr"/>">
	        		<spring:message code="pd.label.summary.csr"/>
	        	</a>
        	</div>
        </div>

         <c:if test="${exchangeType != 'OFF' && PGRM_TYPE == 'Individual'}">
       	<div id="discountsDetail" class="row-detail clearfix">
        	<div class="cell">
           		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.discount"/>">
           			<spring:message code="pd.label.summary.link6"/>
         		</a>
        	</div>
        </div>
         </c:if>

        <div id="hsaTypeDetail" class="row-detail clearfix">
    		 <div class="cell">
    		 	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.commontext.hsatypetooltip"/>">
    		 		<spring:message code="pd.label.commontext.hsatype"/>
    		 	</a>
   		 	</div>
   	  	</div>
        <c:if test="${showNetworkTransparencyRating == 'ON'}">
        <div id="networkTransparencyRatingDetail" class="row-detail clearfix">
      	  <div class="cell">
      		  <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.tooltip.network" htmlEscape="true"/>">
      			<spring:message code="pd.label.network"/>
     		    </a>
     	    </div>
        </div>
        </c:if>
      <c:if test="${showQualityRating == 'YES'}">
        <div id="qualityRatingDetail" class="row-detail clearfix">
     		 <div class="cell">
     		 	  <c:if test="${stateCode == 'CT'}">
					<a href="#" rel="tooltip" data-html="true" data-placement="right" data-original-title="<spring:message code="pd.label.qualityRating.tooltip" htmlEscape="false"/>">
				  </c:if>
     			  <spring:message code="pd.label.tooltip.qualityRatings.1"/>
     			  <c:if test="${stateCode == 'CT'}">
					</a>
				  </c:if>
     		</div>
     	  </div>
      </c:if>

      	<c:if test="${prescriptionSearchConfig == 'ON' && prescriptionList.size() > 0}">
      	  <div id="drugDetail" class="row-detail clearfix">
     		 <div class="cell">
     			  <spring:message code="pd.label.title.yourDrugs"/>
     		</div>
     	  </div>
      	</c:if>
      </div> --%>






  <%-- <c:if test="${showSBCScenarios == 'ON'}">
    <div class="table">
		    <div class="row header">
			    <div class="cell header-name">
				    <spring:message code="pd.label.title.SampleCareCosts"/>
			    </div>
		    </div>
        <div id="havingBaby" class="row-detail clearfix">
          <div class="cell">
          	<a href="#" rel="tooltip" data-placement="right" data-original-title="">
            	<spring:message code="pd.health.label.havingABaby"/>
           	</a>
          </div>
        </div>
        <div id="havingDiabetes" class="row-detail clearfix">
        	<div class="cell">
	          	<a data-placement="right" rel="tooltip" href="#" data-original-title="">
	          		<spring:message code="pd.health.label.havingDiabetes"/>
	        	</a>
       	  	</div>
        </div>
        <div id="treatmentSimpleFracture" class="row-detail clearfix">
        	<div class="cell">
	        	<a data-placement="right"  rel="tooltip" href="#" data-original-title="">
	        		<spring:message code="pd.health.label.treatmentOfASimpleFracture"/>
	        	</a>
        	</div>
        </div>

      </div>
    </c:if> --%>





    <%-- <c:if test="${providerSearchConfig == 'ON'}">
	  <div class="table">
      <div class="row header providerHeader" id="providerSearchHeader">
        <div class="cell header-name">
          <spring:message code="pd.label.title.doctorsandfacilities"/>
        </div>
      </div>
      <c:if test="${providerMapConfig == 'ON'}">
  	  <div class="row-detail clearfix" id="providerHeatMapDetail">
  	    <div class="cell">
  	      <spring:message code="pd.provider.label.physiciansIn"/> <br/>
  	      <select class="input-medium" id="providerDetailDistance" name="providerDetailDistance" onchange="App.views.detailView.detailPlan();">
	          <option value="1"><spring:message code="pd.label.preferences.radius1"/></option>
    			  <option value="2"><spring:message code="pd.label.preferences.radius2"/></option>
    			  <option value="5"><spring:message code="pd.label.preferences.radius5"/></option>
    			  <option selected value="10"><spring:message code="pd.label.preferences.radius10"/></option>
    			  <option value="20"><spring:message code="pd.label.preferences.radius20"/></option>
    			  <option value="30"><spring:message code="pd.label.preferences.radius30"/></option>
    			  <option value="50"><spring:message code="pd.label.preferences.radius50"/></option>
    			  <option value="100"><spring:message code="pd.label.preferences.radius100"/></option>
		      </select>
		      <spring:message code="pd.provider.label.of"/> ${zipcode}
  		  </div>
  	  </div>
  	  </c:if>
    </div>
    </c:if> --%>

    <c:choose>
    <c:when test="${simplifiedDeductible == 'ON' }">
      <%-- <div class="table">
        <div class="row header">
          <div class="cell header-name">
          	<spring:message code="pd.label.planDetail.deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/>
		  </div>
          <div class="cell header-name">
          	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
       	  </div>
          <div class="cell header-name">
           	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
          </div>

        </div>

          <div id="simplifiedDeductibleDetail" class="row-detail clearfix">
            <div class="cell"><spring:message code="pd.label.planDetail.deductible"/></div>
          </div>
          <div class="row-detail clearfix" id="simplifiedSeparateDrugDeductibleDetail">
            <div class="cell">
            	<spring:message code="pd.label.separate.drugdeductible"/>
            </div>
          </div>
          <div class="row-detail clearfix" id="simplifiedOOPMaxDetail">
            <div class="cell"><spring:message code="pd.label.deductible.oopmax"/></div>
          </div>
          <c:if test="${stateCode != 'CT'}">
	          <div class="row-detail clearfix" id="simplifiedMaxCostPerPrescriptionDetail">
	            <div class="cell"><spring:message code="pd.label.maxCostPerPrescription"/></div>
	          </div>
          </c:if>
          <div class="row-detail clearfix" id="simplifiedOtherDeductibleDetail">
            <div class="cell"><spring:message code="pd.label.deductible.otherDeductibles"/></div>
          </div>

        </div> --%>

    </c:when>
    <c:otherwise>
      <%-- <div class="table">
        <div class="row header">
            <div class="cell header-name"><spring:message code="pd.label.planDetail.deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/></div>
        </div>

          <div class="row-detail clearfix" id="planDeductible1Detail">
            <div class="cell">
              <a data-original-title="<spring:message code="pd.label.deductible.link1indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link1"/></a>
               (<spring:message code="pd.label.commontext.individual"/>)
            </div>
          </div>
          <div class="row-detail clearfix" id="planDeductible2Detail">
           	<div class="cell">
              <a data-original-title="<spring:message code="pd.label.deductible.link2indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link2"/></a>
              (<spring:message code="pd.label.commontext.individual"/>)
           	</div>
          </div>
          <div class="row-detail clearfix" id="planDeductible3Detail">
            <div class="cell">
              <a data-original-title="<spring:message code="pd.label.deductible.link1famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link1"/></a>
               (<spring:message code="pd.label.commontext.family"/>)
            </div>
          </div>
          <div class="row-detail clearfix" id="planDeductible4Detail">
             <div class="cell">
              	<a data-original-title="<spring:message code="pd.label.deductible.link2famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link2"/></a>
               	(<spring:message code="pd.label.commontext.family"/>)
             </div>
          </div>
          <div class="row-detail clearfix" id="planDeductible5Detail">
            <div class="cell"><spring:message code="pd.label.deductible.link3"/> (<spring:message code="pd.label.commontext.individual"/>)</div>
          </div>
          <div class="row-detail clearfix" id="planDeductible6Detail">
          	<div class="cell">
              <a data-original-title="<spring:message code="pd.label.deductible.link4indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link4"/></a>
               (<spring:message code="pd.label.commontext.individual"/>)
          	</div>
          </div>
          <div class="row-detail clearfix" id="planDeductible7Detail">
            <div class="cell"><spring:message code="pd.label.deductible.link5"/> (<spring:message code="pd.label.commontext.individual"/>)
            </div>
          </div>
          <div class="row-detail clearfix" id="planDeductible8Detail">
            <div class="cell"><spring:message code="pd.label.deductible.link6"/> (<spring:message code="pd.label.commontext.individual"/>)</div>
          </div>
          <div class="row-detail clearfix" id="planDeductible9Detail">
             <div class="cell"><a data-original-title="<spring:message code="pd.label.deductible.link3famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link3"/></a>
             (<spring:message code="pd.label.commontext.family"/>)</div>
          </div>
          <div class="row-detail clearfix" id="planDeductible10Detail">
            <div class="cell">
            	<a data-original-title="<spring:message code="pd.label.deductible.link4famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link4"/></a>
             	(<spring:message code="pd.label.commontext.family"/>)
            </div>
          </div>
          <div class="row-detail clearfix" id="planDeductible11Detail">
            <div class="cell"><spring:message code="pd.label.deductible.link5"/> (<spring:message code="pd.label.commontext.family"/>)
            </div>
          </div>
          <div class="row-detail clearfix" id="planDeductible12Detail">
            <div class="cell"><spring:message code="pd.label.deductible.link6"/> (<spring:message code="pd.label.commontext.family"/>)</div>
          </div>
          <div class="row-detail clearfix" id="planDeductibleOtherDetail">
            <div class="cell"><spring:message code="pd.label.deductible.linkOther"/></div>
          </div>

      </div> --%>
    </c:otherwise>
    </c:choose>
  <!--  <script>
    $(document).ready(function(){
      var column =  '<div class="columns left-column">'+
        '<div class="header-name xs-show"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>'+
    '<div class="header-name xs-show"><spring:message code="pd.label.title.appliesToDeductible"/></div>'+
    '<div class="header-name xs-show"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>'+
    '<div class="header-name xs-show"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>'+
    '<div class="header-name xs-show ml-hide"><spring:message code="pd.label.title.additionalInfo"/></div>'+
  '</div>';
      $(window).resize(function(){
        var windowWidth = $(window).width();
        console.log(windowWidth);
        if(windowWidth < 987)
      {
          $('.row-detail').each(function(){

            $(this).not(':has(.left-column)').find(".columns").before(column);
              console.log('found');
          });
      }
      });
    });
  </script> -->


	 <%-- <div class="table mobile-table">
        <div class=" row header">
            <div class="cell header-name"><spring:message code="pd.label.icon.doctorVisit"/></div >
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
        </div>

        <div id="doctorVisit1Detail" class="row-detail clearfix">
           <div class="cell column-name">
            <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.doctorVisit.link1"/></a>
          </div>
        </div>

        <div id="doctorVisit2Detail" class="row-detail clearfix">
          <div class="cell column-name">
            <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.doctorVisit.link2"/></a>
          </div>
        </div>

        <c:if test="${stateCode != 'CT'}">
	        <div id="doctorVisit3Detail" class="row-detail clearfix">
	          <div class="cell column-name">
	            <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link3tooltip" htmlEscape="false"/>"><spring:message code="pd.label.doctorVisit.link3"/></a>
	          </div>
	        </div>
        </c:if>

        <div id="doctorVisit4Detail" class="row-detail clearfix">
 		    <div class="cell column-name">
        		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.doctorVisit.link4tooltip" htmlEscape="false"/>"><spring:message code="pd.label.doctorVisit.link4"/></a>
        	</div>
        </div>

      </div> --%>

    <%-- <div class="table mobile-table">
        <div class="row header">
            <div class="cell header-name">
           		<c:if test="${stateCode == 'CT'}">
           			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.icon.tests.tooltip" htmlEscape="false"/>">
           		</c:if>
           		<spring:message code="pd.label.icon.tests"/>
           		<c:if test="${stateCode == 'CT'}"></a></c:if>
            </div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
        </div>
        <div id="test1Detail" class="row-detail clearfix">
            <div class="cell column-name">
              	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tests.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.tests.link1"/></a>
             </div>
      	</div>
    	<div id="test2Detail" class="row-detail clearfix">
    	    <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tests.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.tests.link2"/></a>
          	</div>
    	</div>
    	<div id="test3Detail" class="row-detail clearfix">
     	    <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tests.link3tooltip" htmlEscape="false"/>"><spring:message code="pd.label.tests.link3"/></a>
          	</div>
    	</div>
    </div> --%>


        <%-- <div class="table mobile-table">

	        <div class="row header">
	            <div class="cell header-name">
            		<c:if test="${stateCode == 'CT'}">
            			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.icon.drugs.tooltip" htmlEscape="false"/>">
            		</c:if>
            		<spring:message code="pd.label.icon.drugs"/>
            		<c:if test="${stateCode == 'CT'}">
            			</a>
            		</c:if>
	            </div >
	           <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
	        </div>

          <div id="drug1Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.drugs.link1"/></a>
           </div>
          </div>
          <div id="drug2Detail" class="row-detail clearfix">
             <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.drugs.link2"/></a></div>
          </div>
          <div id="drug3Detail" class="row-detail clearfix">
             <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link3tooltip" htmlEscape="false"/>"><spring:message code="pd.label.drugs.link3"/></a></div>
          </div>
          <div id="drug4Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link4tooltip" htmlEscape="false"/>"><spring:message code="pd.label.drugs.link4"/></a></div>
          </div>
          <c:if test="${stateCode != 'CT'}">
	          <div id="drug5Detail" class="row-detail clearfix">
	            <div class="cell column-name">
	              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.drugs.link5tooltip" htmlEscape="false"/>"><spring:message code="pd.label.drugs.link5"/></a></div>
	          </div>
          </c:if>
        </div> --%>

         <%-- <div class="table mobile-table">
	        <div class="row header">
	            <div class="cell header-name">

	            		<c:if test="${stateCode == 'CT'}">
	            			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.icon.outpatient.tooltip" htmlEscape="false"/>">
	            		</c:if>
	            		<spring:message code="pd.label.icon.outpatient"/>
	            		<c:if test="${stateCode == 'CT'}"> </a> </c:if>

	            </div >
	             <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
	        </div>

          <div id="outpatient1Detail" class="row-detail clearfix">
	           <div class="cell column-name">
	              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.outpatient.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.outpatient.link1"/></a>
	            </div>
          </div>

          <c:if test="${stateCode != 'CT'}">
	          <div id="outpatient2Detail" class="row-detail clearfix">
	            <div class="cell column-name">
	              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.outpatient.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.outpatient.link2"/></a>
	              </div>
	          </div>

	          <div id="outpatientServicesOfficeVisitsDetail" class="row-detail clearfix">
	             <div class="cell column-name">
	              	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.outpatient.officeVisitstooltip" htmlEscape="false"/>"><spring:message code="pd.label.outpatient.officeVisits"/></a>
	             </div>
	          </div>
          </c:if>

      </div> --%>


      <%-- <div class="table mobile-table">
	        <div class="row header">
	            <div class="cell header-name"><spring:message code="pd.label.icon.er"/> & <spring:message code="pd.label.icon.urgentCare"/></div >
	             <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
	        </div>

          <div id="urgent1Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.urgentCare.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.urgentCare.link1"/></a>
            </div>
          </div>
          <div id="urgent2Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.urgentCare.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.urgentCare.link2"/></a>
            </div>
          </div>
          <div id="urgent3Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.urgentCare.link3tooltip" htmlEscape="false"/>"><spring:message code="pd.label.urgentCare.link3"/></a>
            </div>
          </div>
          <c:if test="${stateCode != 'CT'}">
	          <div id="urgentProfessionalFeeDetail" class="row-detail clearfix">
	            <div class="cell column-name">
	              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.urgentCare.professionalFeetooltip" htmlEscape="false"/>"><spring:message code="pd.label.urgentCare.professionalFee"/></a>
	            </div>
	          </div>
          </c:if>
     </div> --%>

     <%-- <div class="table mobile-table">
          <div class="row header">
              <div class="cell header-name"><spring:message code="pd.label.icon.hospital"/></div >
                <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
          </div>

          <div id="hospital1Detail" class="row-detail clearfix">
            <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.hospital.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.hospital.link1"/></a>
            </div>
          </div>
		  <c:if test="${stateCode != 'CT'}">
		      <div id="hospital2Detail" class="row-detail clearfix">
	            <div class="cell column-name">
	              	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.hospital.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.hospital.link2"/></a>
	            </div>
	          </div>
	      </c:if>
     </div> --%>

    <%-- <c:if test="${stateCode != 'CT'}">
      <div class="table mobile-table">
          <div class="row header">
              <div class="cell header-name"><spring:message code="pd.label.icon.health"/></div >
               <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
          </div>

          <div id="mentalHealth1Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.mentalHealth.link1"/></a>
            </div>
          </div>
          <div id="mentalHealth2Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.mentalHealth.link2"/></a>
            </div>
          </div>

          <div id="mentalHealthInpatientProfFeeDetail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.inpatientProfFeetooltip" htmlEscape="false"/>"><spring:message code="pd.label.mentalHealth.inpatientProfFee"/></a>
            </div>
          </div>

          <div id="mentalHealth3Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.link3tooltip" htmlEscape="false"/>"><spring:message code="pd.label.mentalHealth.link3"/></a>
            </div>
          </div>
          <div id="mentalHealth4Detail" class="row-detail clearfix">
            <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.link4tooltip" htmlEscape="false"/>"><spring:message code="pd.label.mentalHealth.link4"/></a>
           	</div>
          </div>

          <div id="mentalHealthSubDisorderInpProfFeeDetail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.mentalHealth.subsDisorderInpProfFeetooltip" htmlEscape="false"/>"><spring:message code="pd.label.mentalHealth.subsDisorderInpProfFee"/></a>
            </div>
          </div>

      </div>

          <div class="table mobile-table">
          <div class="row header">
              <div class="cell header-name"><spring:message code="pd.label.icon.pregnancy"/></div >
              <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
          </div>

          <div id="pregnancy1Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.pregnancy.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.pregnancy.link1"/></a>
            </div>
          </div>
          <div id="pregnancy2Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.pregnancy.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.pregnancy.link2"/></a>
            </div>
          </div>

          <div id="pregnancyInpatientProfFeeDetail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.pregnancy.inpatientProfFeetooltip" htmlEscape="false"/>"><spring:message code="pd.label.pregnancy.inpatientProfFee"/></a>
            </div>
          </div>

        </div>


   <div class="table mobile-table">
        <div class="row header">
            <div class="cell header-name"><spring:message code="pd.label.icon.otherSpecialNeeds"/></div >
             <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
        </div>

          <div id="specialNeed1Detail" class="row-detail clearfix">
            <div class="cell column-name">
               <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link1"/></a>
            </div>
          </div>
          <div id="specialNeed2Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link2"/></a>
            </div>
          </div>
          <div id="specialNeed3Detail" class="row-detail clearfix">
            <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link3tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link3"/></a>
            </div>
          </div>
          <div id="specialNeed4Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link4tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link4"/></a>
            </div>
          </div>
          <div id="specialNeed5Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link5tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link5"/></a>
             </div>
          </div>
          <div id="specialNeed6Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link6tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link6"/></a>
             </div>
          </div>
          <div id="specialNeed7Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link7tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link7"/></a>
             </div>
          </div>
          <div id="specialNeed8Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link8tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link8"/></a>
             </div>
          </div>
          <div id="specialNeed9Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link9tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link9"/></a>
             </div>
          </div>
          <div id="specialNeed10Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link10tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link10"/></a>
             </div>
          </div>
          <div id="specialNeed11Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link11tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link11"/></a>
             </div>
          </div>
          <div id="specialNeed12Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link12tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link12"/></a>
             </div>
          </div>
          <div id="specialNeed13Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link13tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link13"/></a>
             </div>
          </div>
          <div id="specialNeed14Detail" class="row-detail clearfix">
            <div class="cell column-name">
             <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.otherSpecialNeeds.link14tooltip" htmlEscape="false"/>"><spring:message code="pd.label.otherSpecialNeeds.link14"/></a>
             </div>
          </div>
       </div>


        <div class="table mobile-table">
          <div class="row header">
              <div class="cell header-name"><spring:message code="pd.label.icon.children.vision"/></div >
               <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
          </div>

          <div id="childrensVision1Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.vision.link1tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.vision.link1"/></a>
            </div>
          </div>
          <div id="childrensVision2Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.vision.link2tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.vision.link2"/></a>
            </div>
          </div>
          <c:if test="${stateCode == 'MN'}">
          <div id="hearingAidsDetail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title=""><spring:message code="pd.label.children.hearing.link1"/></a>
            </div>
          </div>
          </c:if>
       </div>
       </c:if> --%>

    <%-- <div class="table mobile-table">
        <div class="row header">
            <div class="cell header-name"><spring:message code="pd.label.icon.children.dental"/></div >
             <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></div>
            <c:if test="${stateCode == 'ID'}">
            <div  class="cell txt-center header-name xs-hide"><spring:message code="pd.label.title.appliesToDeductible"/></div>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.tiertwo" htmlEscape="false"/>"><spring:message code="pd.label.title.tier2Network"/></a></div>
            </c:if>
            <div  class="cell header-name xs-hide"><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></div>
            <div  class="cell header-name xs-hide"><spring:message code="pd.label.title.additionalInfo"/></div>
        </div>

          <div id="childrensDental1Detail" class="row-detail clearfix">
            <div class="cell column-name"><spring:message code="pd.label.children.dental.link1"/></div>
          </div>
          <div id="childrensDental2Detail" class="row-detail clearfix">
            <div class="cell column-name"><spring:message code="pd.label.children.dental.link2"/></div>
          </div>
          <div id="childrensDental3Detail" class="row-detail clearfix">
            <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link3tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.link3"/></a>
           	</div>
          </div>
          <div id="childrensDental4Detail" class="row-detail clearfix">
            <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link4tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.link4"/></a>
            </div>
          </div>
          <div id="childrensDental5Detail" class="row-detail clearfix">
            <div class="cell column-name"><spring:message code="pd.label.children.dental.link5"/>
            </div>
          </div>
          <div id="childrensDental6Detail" class="row-detail clearfix">
            <div class="cell column-name">
              <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link6tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.link6"/></a>
            </div>
          </div>
          <div id="childrensDental7Detail" class="row-detail clearfix">
            <div class="cell column-name"><spring:message code="pd.label.children.dental.link7"/>
            </div>
          </div>
          <div id="childrensDental8Detail" class="row-detail clearfix">
            <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link8tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.link8"/></a>
           	</div>
          </div>
          <div id="childrensDental9Detail" class="row-detail clearfix">
            <div class="cell column-name">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.children.dental.link9tooltip" htmlEscape="false"/>"><spring:message code="pd.label.children.dental.link9"/></a>
           	</div>
          </div>
    </div> --%>
  </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		setPageTitle('<spring:message code="pd.label.title.viewPlanDetails"/>');
	});
  window.scrollTo(0,0);
  function getSubToDeduct(APPLIES){
  var str = "";
  if(APPLIES != null && APPLIES.AppliesNetwk && APPLIES.AppliesNetwk != "" && APPLIES.AppliesNetwk != "Not Applicable"){
    str += "<i class='has-deductible-"+APPLIES.AppliesNetwk+"'></i>";

  } else {
    str += "<p><small><spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/></small></p>";
  }
  return str;

  }

</script>

<script type="text/javascript">
var exchName =  '${exchangeName}';
$("#externalWarnBox").live('click',function(e){
	var that = $(this),
	getURL = $(this).attr('href'),
    urlhostname = $('<a>').prop('href', getURL).prop('hostname'),
    getExchangeName = exchName,
    htmlContent = '<h4><spring:message arguments="${exchangeName}" code="pd.label.commontext.externalWarnBox.leavingAlert"/></h4> <p><spring:message code="pd.label.commontext.externalWarnBox.access"/> </p>',
    htmlURL = '<a href="'+getURL+'" target="_blank">'+getURL+'</a>',
    htmlContent1 = '<p>'+getExchangeName+' <spring:message code="pd.label.commontext.externalWarnBox1"/></p>'+
    	'<div><a href="#" class="btn externalModalClose"><spring:message code="pd.label.commontext.externalWarnBox.no"/><span class="aria-hidden"><spring:message code="pd.label.commontext.externalWarnBox.closeModal"/></span></a> <a href="'+getURL+'" class="btn btn-primary" target="_blank" id="yesBtn" aria-hidden="false"><spring:message code="pd.label.commontext.externalWarnBox.yes"/></a></div>'+
    	'<p><spring:message code="pd.label.commontext.externalWarnBox.thankYou"/></p>';

    if(urlhostname !='' && window.location.hostname != urlhostname){
    	$('#warningBox').remove();
        e.preventDefault();
    	var warningHTML = $('<div class="modal" id="warningBox"><div class="modal-header" style="border-bottom:0; "><button type="button" class="close externalModalClose" aria-hidden="false"><span aria-hidden="true">&times;</span><span class="aria-hidden">close modal</span></button></div><div class="modal-body gutter10" style="max-height:470px;"><div class="uploadText"></div></div><div class="modal-footer"></div></div>').modal({backdrop:"static",keyboard:"false"});
    	$(warningHTML).find('div.uploadText').html(htmlContent + htmlURL + htmlContent1);
    }

    $('.externalModalClose').live('click',function(){
		that.attr('href',getURL);
		$('#warningBox').modal("hide");
    });

    $('#yesBtn').live('click',function(){
		that.attr('href',getURL);
		$('#warningBox').modal("hide");
    });
 });
</script>

<script type="text/html" id="planNameTpl">
	<span class="details"><@=name@></span>
</script>

<%-- <script type="text/html" id="tileTpl">
<@ if($("#gpsVersion").val() == 'V1') { @>
  <@ if(smartScore >=80){ @>
    <div class="tile detailPlanTile best details">
  <a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.green"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
  <@ } else if(smartScore >=60){ @>
    <div class="tile detailPlanTile ok details">
    <a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.yellow"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
  <@ } else { @>
    <div class="tile detailPlanTile poor details">
    <a data-original-title="<spring:message code="pd.label.tooltip.summary.planDetail.red"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#">
  <@ } @>
  <div class="score">
    <div class="circle-wrapper">
      <div class="center-circle"></div>
      <div class="slice">
        <div class="pie score_<@=smartScore@> pie-color"></div>
      </div>
      <div class="circle-core center-circle">
      <div class="circle-num"><@=smartScore@></div>
      </div>
    </div>
  </div>
    </a>
<@ } else { @>
  <div class="tile detailPlanTile details">
  <@ } @>
  <div class="tile-header">
    <@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
        <a class="addToCart btn btn-small ie10" rel="tooltip" title="<spring:message code="pd.label.title.addToCart"/>" href="#" id="cart_<@=planId@>">
            <span><spring:message code="pd.label.title.addToCart" htmlEscape="true"/></span> <i class="icon-shopping-cart"></i>
        </a>
    <@ } @>
    <a href="#" id="detail_<@=id@>" class="btn-block detail">
      <@ if (issuer.length > 24) {@>
        <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer.substr(0,24)@>..." onload="resizeimage(this)" /></div>
      <@ } else { @>
        <div class="planimg" id="detail_<@=id@>"><img id="detail_<@=id@>" class="planimg carrierlogo hide" src='<@=issuerLogo@>' alt="<@=issuer@>" onload="resizeimage(this)" /></div>
      <@ } @>
    </a>
    <p><a class="detail" href="#" id="detail_<@=id@>" data-placement="bottom" rel="tooltip" data-original-title="<@=name@>">
      <@ if (name.length > 24) {@>
        <@=name.substr(0,24)@>...
      <@ } else { @>
        <@=name@>
      <@ } @>
    </a></p>
   <small><@=level@> &nbsp; <@=networkType@></small>
  </div>

  <!-- .tile-header-->

      <div class="payment">
      <@ if($('#stateCode').val() == "ID") { @>
      <h3>$<@=premiumAfterCredit.toFixed(2) @></h3>
      <span>/month</span>
    <@ if($('#exchangeType').val() != 'OFF') {@>
        <@ if (aptc > 0) {@>
        <p class="was"><spring:message code="pd.label.title.was" htmlEscape="true"/> <del>$<@=premiumBeforeCredit.toFixed(2) @></del> <spring:message code="pd.label.beforeCredit" htmlEscape="true"/></p>
        <@ } @>
    <@ } @>
    <@ } @>
      </div>
      <div class="plan-options">
      <table class="table">
          <@ if($('#stateCode').val() != "ID") { @>
        <tr id="mp">
        <th><spring:message code="pd.label.title.premium"/></th>
        <td>
          <p> $<@=premiumAfterCredit.toFixed(2) @></p>
          <p class="plan-options__tile__notes"><spring:message code="pd.label.title.aftertax1"/> $<@=aptc.toFixed(2)@> <spring:message code="pd.label.title.aftertax2"/></p>
        </td>
        </tr>

    <@ }  @>
          <tr id="offv">
          <th class="span1"><spring:message code="pd.label.title.officeVisits"/></th>
      <td><p class="pcv">
      <@ if($('#stateCode').val() == "CA") { @>
      <small><spring:message code="pd.label.title.youpay"/>&nbsp;<@=planTier1.PRIMARY_VISIT.tileDisplayVal@> </small>
      <@ } else { @>
          <small><@=planTier1.PRIMARY_VISIT.tileDisplayVal@> </small>
      <@ } @>
      </p></td>
          </tr>
          <tr id="gd">
          <th><spring:message code="pd.label.title.genericDrugs"/></th>
      <td><p class="drug">
      <@ if($('#stateCode').val() == "CA") { @>
    <small><spring:message code="pd.label.title.youpay"/>&nbsp;<@=planTier1.GENERIC.tileDisplayVal @> </small>
        <@ } else { @>
        <small><@=planTier1.GENERIC.tileDisplayVal @> </small>
        <@ } @>
      </p></td>
          </tr>
          <tr id="yd">
          <th><a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message code="pd.label.tooltip.deductible"/>"><spring:message code="pd.label.Deductible"/></a></th>
          <td>
            <@ if(intgMediDrugDeductible !=null ){@>
              <a href="#" rel="tooltip" data-placement="bottom" data-html="true" data-original-title="<spring:message arguments="<@=intgMediDrugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>">$<@=intgMediDrugDeductible.toFixed()@></a>
                <@ if($('#stateCode').val() == "CA") { @>
			          <a herf="#" rel="tooltip" role="tooltip" data-placement="bottom" data-html="true" data-original-title="<spring:message code="pd.label.title.deductibledesc.tooltip"/>" aria-label="<spring:message code="pd.label.title.deductibledesc.tooltip"/> help text finished"><spring:message code="pd.label.title.deductibledesc"/></a>
                <@ } @>
            <@ } else if((medicalDeductible != null) || (drugDeductible!= null)){ @>
              <@  if(medicalDeductible !=null && drugDeductible != null){ @>
              	<a href="#" rel="tooltip" data-placement="bottom" data-html="true" data-original-title="<spring:message arguments="<@=medicalDeductible.toFixed()@>,<@=drugDeductible.toFixed()@>" code="pd.tooltip.separate.ductible" htmlEscape="false"/>">
          			$<@=medicalDeductible.toFixed()@> / $<@=drugDeductible.toFixed()@></a>
        	  <@ }else if(medicalDeductible !=null  ){@>
                 	<a href="#" rel="tooltip" data-placement="bottom" data-html="true" data-original-title="<spring:message arguments="<@=medicalDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>">$<@=medicalDeductible.toFixed()@></a>
          	  <@ }else if(drugDeductible != null ){ @>
                 	<a href="#" rel="tooltip" data-placement="bottom" data-html="true" data-original-title="<spring:message arguments="<@=drugDeductible.toFixed()@>" code="pd.tooltip.combined.deductible" htmlEscape="false"/>">$<@=drugDeductible.toFixed()@></a>
              <@ } @>
                <@ if($('#stateCode').val() == "CA") { @>
			          <a herf="#" rel="tooltip" role="tooltip" data-placement="bottom" data-html="true" data-original-title="<spring:message code="pd.label.title.deductibledesc.tooltip"/>" aria-label="<spring:message code="pd.label.title.deductibledesc.tooltip"/> help text finished"><spring:message code="pd.label.title.deductibledesc"/></a>
                <@ } @>
            <@ } else {  @>
              <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
      	 	<@ } @>
          </td>
          </tr>
          <tr id="ee">
          <th class="plan-options__tile__expenseEstimate"><a href="#" rel="tooltip" data-html="true" data-placement="top" data-original-title="<spring:message code="pd.tooltip.summary.estimatedCostDetail"/>"><spring:message code="pd.label.title.expenseEstimate"/></a></th>
      <td>
          <@ if(expenseEstimate == "Lower Cost"){ @>
        <span class="best"><spring:message code="pd.label.low"/> <i class="icon-flag"></i>
          <@ } else if(expenseEstimate == "Average"){ @>
        <span class="ok"><spring:message code="pd.label.average"/> <i class="icon-flag"></i>
          <@ } else if(expenseEstimate == "Pricey"){ @>
        <span class="poor"><spring:message code="pd.label.high"/> <i class="icon-flag"></i>
          <@ } else { @>
            <@=expenseEstimate@>
              <@ } @>
          </td>
          </tr>

        <c:forEach var="benefit" items="${benefitsArray}">
      <tr>
              <th>
        <@ if(['${benefit}'] == "Pediatric dental"){ @>
            <spring:message code="pd.label.title.childrensDental" htmlEscape="true"/>
        <@ } else { @>
                ${benefit}
            <@ } @>
          </th>
              <td class="gutter20-l">
        <@ if(benefitsCoverage['${benefit}'] == "GOOD"){ @>
          <a href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.good"/>" rel="tooltip"><i class="GOOD"></i></a>
          <@ } else if(benefitsCoverage['${benefit}'] == "OK"){ @>
          <a href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.ok"/>" rel="tooltip"><i class="OK"></i></a>
          <@ } else if(benefitsCoverage['${benefit}'] == "POOR"){ @>
          <a href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.poor"/>" rel="tooltip"><i class="POOR"></i></a>
          <@ } else { @>
            <a href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.benefitsCoverage.na"/>" rel="tooltip"><i class="NA"></i></a>
        <@ } @>
          </td>
          </tr>
      </c:forEach>
      </table>
      </div>
      </div>
     </div>
   <div class="clearfix details">
		<@ if($('#stateCode').val() == "CT") { @>
			 <spring:message code="pd.label.moreInfomation" htmlEscape="false"/>
		<@ } @>

      <ul class="unstyled quick-list">
    <@ if(sbcUrl != null && sbcUrl != '') { @>
      <li><a href="<@=sbcUrl@>" id="externalWarnBox"><spring:message code="pd.label.link.sbcUrl" htmlEscape="true"/></a></li>
    <@ } else { @>
      <li><span><spring:message code="pd.label.link.sbcUrlNotAvailable" htmlEscape="true"/></span></li>
    <@ } @>
    <@ if(planBrochureUrl != null && planBrochureUrl != '') { @>
      <li><a href="<@=planBrochureUrl@>" id="externalWarnBox"><spring:message code="pd.label.link.downloadBrochure"/></a></li>
    <@ } else { @>
      <li><span><spring:message code="pd.label.link.planBrochureNotAvailable" htmlEscape="true"/></span></li>
    <@ } @>
    <@ if(providerLink != null && providerLink != '') { @>
      <li><a href="<@=providerLink@>" id="externalWarnBox" onclick="triggerGoogleTrackingEvent(['_trackEvent', 'View Directory Click', 'click', '<@=issuer@>', 1]);"><spring:message code="pd.label.link.providerDirectoryUrl" htmlEscape="true"/></a></li>
    <@ } else { @>
      <li><span><spring:message code="pd.label.link.providerDirectoryNotAvailable" htmlEscape="true"/></span></li>
    <@ } @>
    <@ if(formularyUrl != null && formularyUrl != '') { @>
      <li><a href="<@=formularyUrl@>" id="externalWarnBox"><spring:message code="pd.label.link.formularyUrl" htmlEscape="true"/></a></li>
    <@ } else { @>
      <li></i><span><spring:message code="pd.label.link.formularyUrlNotAvailable" htmlEscape="true"/></span></li>
    <@ } @>
      </ul>
  </div>
</script> --%>

<script type="text/html" id="estimatedCostTplDetail">
  <div class="details">
    <@ if($('#stateCode').val() != "ID") { @>
	  <@ if(EXPENSE_ESTIMATE != null && EXPENSE_ESTIMATE != ""){@>
		<a href="#" class="expense" rel="tooltip" data-html="true" data-placement="right" data-original-title="<spring:message arguments="<@=parseFloat(ANNUAL_PREMIUM).toFixed(2)@>,<@=parseFloat(EXPENSE_ESTIMATE-ANNUAL_PREMIUM).toFixed(2)@>, <@=parseFloat(EXPENSE_ESTIMATE).toFixed(2)@>" code="pd.tooltip.summary.dynamicCostDetail"/>">
            	$<@=parseFloat(EXPENSE_ESTIMATE).toFixed(2)@>
        </a>

	  <@ } else { @>
	    <p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
	  <@ } @>
	<@ } else { @>
	  <@ if(EXPENSE_ESTIMATE == "Lower Cost"){ @>
      	<span class="best"><spring:message code="pd.label.low"/> <i class="icon-flag"></i>
  	  <@ } else if(EXPENSE_ESTIMATE == "Average"){ @>
    	<span class="ok"><spring:message code="pd.label.average"/> <i class="icon-flag"></i>
  	  <@ } else if(EXPENSE_ESTIMATE == "Pricey"){ @>
    	<span class="poor"><spring:message code="pd.label.high"/> <i class="icon-flag"></i>
  	  <@ } else { @>
      	<@=EXPENSE_ESTIMATE@>
      <@ } @>
	<@ } @>
  </div>
</script>

<script type="text/html" id="hsaTypeTpl">
	<td class="cp-tile__value">
		<@=HSA@>
	</td>
</script>
<script type="text/html" id="networkTransparencyTpl">
	<c:if test="${showNetworkTransparencyRating == 'ON'}">
		<div class="details">
			<@ if(networkTransparencyRating === null){ @>
				<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
			<@ } else {@>
            	<@ if(networkTransparencyRating.toUpperCase() === 'BASIC'){@>
              		<a href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message code="pd.tooltip.network.basic" htmlEscape="true"/>"><spring:message code="pd.network.basic" htmlEscape="true"/></a>
            	<@ } else if(networkTransparencyRating.toUpperCase() === 'STANDARD') { @>
              		<a href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message code="pd.tooltip.network.standard" htmlEscape="true"/>"><spring:message code="pd.network.standard" htmlEscape="true"/></a>
            	<@ } else if(networkTransparencyRating.toUpperCase() === 'BROAD') { @>
              		<a href="#" rel="tooltip" data-placement="right" data-html="true" data-original-title="<spring:message code="pd.tooltip.network.broad" htmlEscape="true"/>"><spring:message code="pd.network.broad" htmlEscape="true"/></a>
            	<@ } @>
            <@ } @>
		</td>
	</c:if>
</script>
<script type="text/html" id="qualityRatingTplDetail">
	<c:if test="${showQualityRating == 'YES'}">
   		<div class="details">
  			<@ if (issuerQualityRating != null) { @>
				<@= populateQualityRatingsHtml(issuerQualityRating)@>
			<@ } else { @>
				<p class="noStarsMessage">
					<c:if test="${stateCode == 'CA'}">
						<a data-placement="top" rel="tooltip" role="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.qualityRatings.9"/>" aria-label="<spring:message code="pd.label.tooltip.qualityRatings.9"/>">
					</c:if>
					<spring:message code="pd.label.tooltip.qualityRatings.7"/>
					<c:if test="${stateCode == 'CA'}">
						</a>
					</c:if>
				</p>
			<@ } @>

  		 </div>
	</c:if>
</script>
<script type="text/html" id="drugTplDetail">
<c:if test="${prescriptionSearchConfig == 'ON'}">
   <div class="details">
    <@ if (prescriptionResponseList != null) { @>
      <@ _.each (prescriptionResponseList, function (prescription) { @>
	    <p>
		  <a href="#" class="txt-capitalize" data-placement="bottom" rel="tooltip" data-original-title="<@=prescription.drugDosage@>">
			<@=prescription.drugName@> <@if(prescription.drugType!=null){@>(<@=prescription.drugType@>)<@}@>:
		  </a>
		  <@if(prescription.isDrugCovered=="Y"){@>
			<i class='GOOD'></i> <@=prescription.drugCost@>
		  <@ } else {@>
			<a href="#" data-placement="bottom" rel="tooltip" data-original-title="<spring:message code="pd.label.notCovered" htmlEscape="true"/>">
			  <i class="icon-ban-circle"></i>
			</a>
		  <@ } @>
		  <@if(prescription.genericID != null && prescription.genericID != "" && prescription.genericDosage != null && prescription.genericDosage != ''){@>
		  	<@ var genericDrugName = prescription.genericDosage.split(' ')[0]; @>
		  	<@ genericDrugName = genericDrugName.charAt(0).toUpperCase() + genericDrugName.slice(1); @>
			<br/>
			<a href="#" class="txt-capitalize" data-placement="bottom" rel="tooltip" data-original-title="<@=prescription.genericDosage@>">
			  <@=prescription.drugName@> (Generic: <@=genericDrugName@>):
			</a>
 			<@if(prescription.isGenericCovered=="Y"){@>
			  <i class='GOOD'></i> <@=prescription.genericCost@>
		  	<@ } else {@>
			  <a href="#" data-placement="bottom" rel="tooltip" data-original-title="<spring:message code="pd.label.notCovered" htmlEscape="true"/>">
			    <i class="icon-ban-circle"></i>
			  </a>
		    <@ } @>
		  <@ } @>
	    </p>
	  <@});@>
	<@}@>
   </div>
</c:if>
</script>
<script type="text/html" id="productTypeTplDetail">
  <div class="details">
  <@ if(NETWORK_TYPE == 'PPO') { @> <a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.ppo" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'HMO') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hmo" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'POS') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.pos" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'DHMO') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dhmo" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'DPPO') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dppo" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else if(NETWORK_TYPE == 'HSA') { @><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hsa" htmlEscape="true"/>"><@=NETWORK_TYPE@></a>
    <@ } else { @> <@=NETWORK_TYPE@> <@ } @>
  </div>
</script>

<script type="text/html" id="csrTplDetail">
  <div class="details">
  <@ if(CSR != null && CSR != "" && CSR != "CS1") { @>
    <@ if(CSR == "CS2" || CSR == "CS3" || CSR == "CS4" || CSR == "CS5" || CSR == "CS6"  ) { @>
      <p><spring:message code="pd.label.summary.csr.message" htmlEscape="true"/></p>
    <@ } else { @>
          <p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
        <@ } @>
      <@ } else { @>
        <p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
      <@ } @>
  </div>
</script>

<script type="text/html" id="discountTplDetail">
  <div class="details">
  <@ if($('#exchangeType').val() != 'OFF') {@>
      <@ if(COSTSHARING != null && COSTSHARING != "" && COSTSHARING != "CS1") { @>
    <@ if(COSTSHARING == "CS2" || COSTSHARING == "CS3" || COSTSHARING == "CS4" || COSTSHARING == "CS5" || COSTSHARING == "CS6"  ) { @>
      <p><spring:message code="pd.label.script.specialSavings" htmlEscape="true"/></p>
    <@ } else { @>
          <p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
        <@ } @>
      <@ } else { @>
        <p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
      <@ } @>
    <@ } @>
  </div>
</script>

<script type="text/html" id="deductibleIndTplDetail">
  <div class="details u-table-cell">
	<@ if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1") {@>
  <@ if( DEDUCTIBLE != null && ((DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != "") || (DEDUCTIBLE.inNetworkTier2Ind != null && DEDUCTIBLE.inNetworkTier2Ind != "") || (DEDUCTIBLE.outNetworkInd != null && DEDUCTIBLE.outNetworkInd != "") || (DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != ""))) {@>
  <@ if( DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != "" ) {@>
      $<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@> (<spring:message code="pd.label.title.inNetwork" htmlEscape="true"/>) <@} @>
  <@ if( DEDUCTIBLE.inNetworkTier2Ind != null && DEDUCTIBLE.inNetworkTier2Ind != "" ) {@>
      $<@=parseFloat(DEDUCTIBLE.inNetworkTier2Ind).toFixed()@> (<spring:message code="pd.label.title.tier2Network" htmlEscape="true"/>) <@} @>
  <@ if( DEDUCTIBLE.outNetworkInd != null && DEDUCTIBLE.outNetworkInd != "" ) {@>
      $<@=parseFloat(DEDUCTIBLE.outNetworkInd).toFixed()@> (<spring:message code="pd.label.title.outOfNetwork" htmlEscape="true"/>) <@} @>
  <@ if( DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != "" ) {@>
      $<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@> (<spring:message code="pd.label.title.combinedAndOutOfNetwork"/>) <@} @>
  <@} else {@>
        <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
    <@} @>
	<@ } else if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1") { @>

		<@ if (DEDUCTIBLE != null && ((DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != "") || (DEDUCTIBLE.inNetworkTier2FlyPerPerson != null && DEDUCTIBLE.inNetworkTier2FlyPerPerson != "") || (DEDUCTIBLE.outNetworkFlyPerPerson != null && DEDUCTIBLE.outNetworkFlyPerPerson != "") || (DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""))) {@>

			<@ if( DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != "" ) {@>
				$<@=parseFloat(DEDUCTIBLE.inNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.title.inNetwork" htmlEscape="true"/>)
			<@} @>

			<@ if( DEDUCTIBLE.inNetworkTier2FlyPerPerson != null && DEDUCTIBLE.inNetworkTier2FlyPerPerson != "" ) {@>
				$<@=parseFloat(DEDUCTIBLE.inNetworkTier2FlyPerPerson).toFixed()@> (<spring:message code="pd.label.title.tier2Network" htmlEscape="true"/>)
			<@} @>

			<@ if( DEDUCTIBLE.outNetworkFlyPerPerson != null && DEDUCTIBLE.outNetworkFlyPerPerson != "" ) {@>
				$<@=parseFloat(DEDUCTIBLE.outNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.title.outOfNetwork" htmlEscape="true"/>)
			<@} @>

			<@ if( DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != "" ) {@>
				$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.title.combinedAndOutOfNetwork"/>)
			<@} @>

		<@} else {@>
			<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
		<@} @>
	<@} @>
  </div>
</script>

<script type="text/html" id="deductibleFlyTplDetail">
  <div class="details u-table-cell">
    <@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1" ) {@>
    <@ if( DEDUCTIBLE != null && ((DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != "") || (DEDUCTIBLE.inNetworkTier2Fly != null && DEDUCTIBLE.inNetworkTier2Fly != "") || (DEDUCTIBLE.outNetworkFly != null && DEDUCTIBLE.outNetworkFly != "") || (DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""))) {@>
  <@ if( DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != "" ) {@>
      $<@=parseFloat(DEDUCTIBLE.inNetworkFly).toFixed()@> (<spring:message code="pd.label.title.inNetwork" htmlEscape="true"/>) <@} @>
  <@ if( DEDUCTIBLE.inNetworkTier2Fly != null && DEDUCTIBLE.inNetworkTier2Fly != "" ) {@>
      $<@=parseFloat(DEDUCTIBLE.inNetworkTier2Fly).toFixed()@> (<spring:message code="pd.label.title.tier2Network" htmlEscape="true"/>) <@} @>
  <@ if( DEDUCTIBLE.outNetworkFly != null && DEDUCTIBLE.outNetworkFly != "" ) {@>
      $<@=parseFloat(DEDUCTIBLE.outNetworkFly).toFixed()@> (<spring:message code="pd.label.title.outOfNetwork" htmlEscape="true"/>) <@} @>
  <@ if( DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != "" ) {@>
      $<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@> (<spring:message code="pd.label.title.combinedAndOutOfNetwork"/>) <@} @>
    <@} else {@>
        <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
      <@} @>
  <@} else {@>
      <spring:message code="pd.label.commontext.notApplicable" htmlEscape="true"/>
    <@} @>
  </div>
</script>

<script type="text/html" id="benefitTplDetail">
<div class="details u-table-cell">
	<div class="u-show-block-xs u-margin-top-10">
		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
			<spring:message code="pd.label.title.inNetwork"/>
		</a>
	</div>

	<@ if(BENEFIT1 != null && BENEFIT1.displayVal !=null && BENEFIT1.displayVal != "") { @>
		<@=BENEFIT1.displayVal@>
	<@ }else{ @>
		<spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/>
	<@ } @>
</div>

<div class="details u-table-cell">
	<div class="u-show-block-xs u-margin-top-10">
		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
			<spring:message code="pd.label.title.outOfNetwork"/>
		</a>
	</div>

	<@ if(BENEFITOUT != null && BENEFITOUT.displayVal !=null && BENEFITOUT.displayVal != "") { @>
		<@=BENEFITOUT.displayVal@>
	<@}else{@>
		<spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/>
	<@}@>
</div>

<div class="details u-table-cell">
	<div class="u-show-block-xs u-margin-top-10">
		<spring:message code="pd.label.title.additionalInfo"/>
	</div>

	<@ if(( EXPLANATION!=null && (EXPLANATION.displayVal != null && EXPLANATION.displayVal != "" )) && ( EXCLUSION!=null && (EXCLUSION.displayVal != null && EXCLUSION.displayVal != "" ))) {@>
		<a
			class="u-hide-xs"
			href="#"
			rel="tooltip"
			data-placement="left"
			data-html="true"
			data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>,<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.explanationAndExclusions" htmlEscape="false"/>">
			<spring:message code="pd.label.title.view" htmlEscape="true"/>
		</a>

		<a
			class="u-show-inline-block-xs"
			href="#"
			rel="tooltip"
			data-placement="right"
			data-html="true"
			data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>,<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.explanationAndExclusions" htmlEscape="false"/>">
			<spring:message code="pd.label.title.view" htmlEscape="true"/>
		</a>
	<@} else if(( EXPLANATION!=null && (EXPLANATION.displayVal == null || EXPLANATION.displayVal == "") ) && ( EXCLUSION!=null && (EXCLUSION.displayVal != null && EXCLUSION.displayVal != "" ))) {@>
		<a
			class="u-hide-xs"
			href="#"
			rel="tooltip"
			data-placement="left"
			data-html="true"
			data-original-title="<spring:message arguments="<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.limitsAndExclusions" htmlEscape="false"/>">
			<spring:message code="pd.label.title.view" htmlEscape="true"/>
		</a>

		<a
			class="u-show-inline-block-xs"
			href="#"
			rel="tooltip"
			data-placement="right"
			data-html="true"
			data-original-title="<spring:message arguments="<@=EXCLUSION.displayVal@>" code="pd.tooltip.title.limitsAndExclusions" htmlEscape="false"/>">
			<spring:message code="pd.label.title.view" htmlEscape="true"/>
		</a>
	<@} else if(( EXPLANATION!=null && (EXPLANATION.displayVal != null && EXPLANATION.displayVal != "" )) && ( EXCLUSION!=null && (EXCLUSION.displayVal == null || EXCLUSION.displayVal == "" ))) {@>
		<a
			class="u-hide-xs"
			href="#"
			rel="tooltip"
			data-placement="left"
			data-html="true"
			data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>" code="pd.tooltip.title.benefitExplanation" htmlEscape="false"/>">
			<spring:message code="pd.label.title.view" htmlEscape="true"/>
		</a>

		<a
			class="u-show-inline-block-xs"
			href="#"
			rel="tooltip"
			data-placement="right"
			data-html="true"
			data-original-title="<spring:message arguments="<@=EXPLANATION.displayVal@>" code="pd.tooltip.title.benefitExplanation" htmlEscape="false"/>">
			<spring:message code="pd.label.title.view" htmlEscape="true"/>
		</a>
	<@} @>
</div>

</script>

<script type="text/html" id="providerSearchLinkTplDetail">
  <div class="details"><p>
    <@ if( PROVIDER_LINK && PROVIDER_LINK != "") { @>
      <a href="<@=PROVIDER_LINK@>" target="_blank"> <spring:message code="pd.label.info.yesDir"/></a>
    <@ } else { @>
    <spring:message code="pd.label.info.noDir"/>
    <@ } @>
  </p></div>
</script>

<script type="text/html" id="deductibleTplDetail">
  <div class="plan-details">
  <p>
	<@ if(Object.keys(DEDUCTIBLES).length > 0) { @>
      <@ for(DEDUCTIBLE_NAME in DEDUCTIBLES) { @>
      <@ if(DEDUCTIBLES[DEDUCTIBLE_NAME] != null) { @>
      	<@ var DEDUCTIBLE = DEDUCTIBLES[DEDUCTIBLE_NAME]; @>
	  	<span><@=DEDUCTIBLE_NAME@> : </span>
		<@ if((DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != "") ||(DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != "") ||(DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != "") ||(DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != "") || (DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != "") ||(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != "")){ @>

			<@ if($('#PERSON_COUNT').val() == 1) { @>

				<@ if(DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != ""){ @>
					<span>$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@> <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>
          		<@ } else if(DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != ""){ @>
            		<span>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@> <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>
          		<@ } @>

          	<@} else if($('#PERSON_COUNT').val() > 1){ @>

				<@ if(DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != ""){ @>
					<span>$<@=parseFloat(DEDUCTIBLE.inNetworkFlyPerPerson).toFixed()@> <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>
          		<@ } else if(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){ @>
            		<span>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@> <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>
          		<@ } @>

				<@ if(DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != "" && DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != "" && DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != ""){@>
              		<span>,&nbsp;</span>
          		<@ } else if(DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != "" && DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != "" && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){@>
          	  		<span>,&nbsp;</span>
          		<@ } @>

          		<@ if(DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != ""){@>
              		<span>$<@=parseFloat(DEDUCTIBLE.inNetworkFly).toFixed()@> <spring:message code='pd.label.commontext.familySmall' htmlEscape='true'/></span>
            	<@ } else if(DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""){ @>
              		<span>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@> <spring:message code='pd.label.commontext.familySmall' htmlEscape='true'/></span>
            	<@ } @>

			<@ } @>

		<@ } else { @>
    	  <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
    	<@ } @>
      	<br>
      <@ } @>
    <@ } @>
    <@ } else { @>
      <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
    <@ } @>
  </p>
  </div>
</script>

<script type="text/html" id="simplifiedDeductibleTplDetail">
<div class="details u-table-cell">
	<div class="u-show-block-xs u-margin-top-10">
		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
			<spring:message code="pd.label.title.inNetwork"/>
		</a>
	</div>

	<@ var inNetworkDeductible = false; @>

	<@ if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1") { @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != ""){ @>
			<@ inNetworkDeductible = true; @>
			<p>$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
		<@ } @>

	<@ } else if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1") { @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.inNetworkFlyPerPerson != null && DEDUCTIBLE.inNetworkFlyPerPerson != ""){ @>
			<@ inNetworkDeductible = true; @>
			<p>$<@=parseFloat(DEDUCTIBLE.inNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
		<@ } @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.inNetworkFly != null && DEDUCTIBLE.inNetworkFly != ""){ @>
			<@ inNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.inNetworkFly).toFixed()@> (<spring:message code="pd.label.commontext.family" />)</p>
		<@ } @>
	<@ } @>

	<@ if(inNetworkDeductible == false) { @>

		<@ if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1") { @>

			<@ if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != ""){ @>
				<@ inNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
			<@ } @>

		<@ } else if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1") { @>

			<@ if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){ @>
				<@ inNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
			<@ } @>

			<@ if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""){ @>
				<@ inNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@> (<spring:message code="pd.label.commontext.family" />)</p>
			<@ } @>

		<@ } @>
	<@ } @>

	<@ if(inNetworkDeductible == false && $('#stateCode').val() != "CT") { @>
		<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
	<@ } @>

	<@ if(inNetworkDeductible == false && $('#stateCode').val() == "CT") { @>
		<spring:message code="pd.label.commontext.includedInDeductible" htmlEscape="true"/>
	<@ } @>
</div>

<div class="details u-table-cell">
	<div class="u-show-block-xs u-margin-top-10">
		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
			<spring:message code="pd.label.title.outOfNetwork"/>
		</a>
	</div>

	<@ var outNetworkDeductible = false; @>

	<@ if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1") { @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.outNetworkInd != null && DEDUCTIBLE.outNetworkInd != ""){ @>
			<@ outNetworkDeductible = true; @>
			$<@=parseFloat(DEDUCTIBLE.outNetworkInd).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)
		<@ } @>

	<@ } else if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1") { @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.outNetworkFlyPerPerson != null && DEDUCTIBLE.outNetworkFlyPerPerson != ""){ @>
			<@ outNetworkDeductible = true; @>
			$<@=parseFloat(DEDUCTIBLE.outNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)
		<@ } @>

		<@ if(DEDUCTIBLE != null && DEDUCTIBLE.outNetworkFly != null && DEDUCTIBLE.outNetworkFly != ""){@>
			<@ outNetworkDeductible = true; @>
			$<@=parseFloat(DEDUCTIBLE.outNetworkFly).toFixed()@> (<spring:message code="pd.label.commontext.family" />)
		<@ } @>
	<@ } @>

	<@ if(outNetworkDeductible == false) { @>

		<@ if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() == "1") { @>

			<@ if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkInd != null && DEDUCTIBLE.combinedInOutNetworkInd != ""){ @>
				<@ outNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkInd).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
			<@ } @>

		<@ } else if ($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != "0" && $("#PERSON_COUNT").val() != "1") { @>

			<@ if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != null && DEDUCTIBLE.combinedInOutNetworkFlyPerPerson != ""){ @>
				<@ outNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFlyPerPerson).toFixed()@> (<spring:message code="pd.label.commontext.individual" />)</p>
			<@ } @>

			<@ if(DEDUCTIBLE != null && DEDUCTIBLE.combinedInOutNetworkFly != null && DEDUCTIBLE.combinedInOutNetworkFly != ""){ @>
				<@ outNetworkDeductible = true; @>
				<p>$<@=parseFloat(DEDUCTIBLE.combinedInOutNetworkFly).toFixed()@> (<spring:message code="pd.label.commontext.family" />)</p>
			<@ } @>
		<@ } @> 
	<@ } @>

	<@ if(outNetworkDeductible == false) { @>
		<@ if($('#stateCode').val() != "CT" || IS_SEPARATE_DEDUCTIBLE == false || $('#simplifiedDeductibleDetail .details:last-child').text().trim() === 'Not Available') { @>
			<@ if($('#stateCode').val() != "MN" && $('#stateCode').val() != "CA") {@>
				<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
			<@ } else { @>
				<spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
			<@ } @>
		<@ } else { @>
			<spring:message code="pd.label.commontext.includedInDeductible" htmlEscape="true"/>
		<@ } @>
	<@ } @>

</div>

<div class="details u-table-cell"></div>
</script>


<script type="text/html" id="simplifiedOtherDeductibleTplDetail">
<div class="details">
  <p>
    <@ if(Object.keys(DEDUCTIBLES).length > 0) { @>
      <@ for(DEDUCTIBLE_NAME in DEDUCTIBLES) { @>
      <@ if(DEDUCTIBLES[DEDUCTIBLE_NAME] != null) { @>
      <@ var DEDUCTIBLE = DEDUCTIBLES[DEDUCTIBLE_NAME]; @>
      <@ if(DEDUCTIBLE.inNetworkInd != null && DEDUCTIBLE.inNetworkInd != ""){@>
          <span><@=DEDUCTIBLE_NAME@> : </span>
            <span>$<@=parseFloat(DEDUCTIBLE.inNetworkInd).toFixed()@></span>
          <@ } @>
          <br>
      <@ } @>
    <@ } @>
    <@ } @>
  </p>
  </div>
<div class="details"><p> </p></div>
</script>

<script type="text/html" id="maxCoinseForSpecialtyDrugsTplDetail">
<div class="details u-table-cell">
	<p>
		<@ if(BENEFIT1 != null) { @>
     		$<@=BENEFIT1@>
    	<@ } else { @>
      		<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
		<@ } @>
	</p>
</div>
<div class="details u-table-cell">

</div>
</script>

<script type="text/html" id="providerHeatMapCountTpl">
	<span id="providerHeatDetail_<@=PLAN_ID@>_<@=PROVIDER_NETWORK_KEY@>"></span>
</script>
<script type="text/html" id="providerHeatMapButtonTpl">
	<a href="#" id="viewMapDetail_<@=PLAN_ID@>_<@=PROVIDER_NETWORK_KEY@>" data-toggle="modal" data-target="#map-modal" class="map__button">
		<spring:message code="pd.provider.buttons.viewMap" htmlEscape="true"/>
	</a>
</script>
<%-- <script type="text/html" id="providerSearchInfoTplDetail">
  <@if(doctors && doctors!=""){@>
    <@ _.each (doctors, function (doctor) { @>
      <div class="row-detail clearfix providerRows">
        <div class="cell">
          <@ if (doctor.providerType == "doctor") {@>
			<i class='user-md'></i>&nbsp;
			<@ if (doctor.providerName.indexOf('Dr.') != 0 && $('#stateCode').val() === 'CA') {@>
				<spring:message code="pd.label.doctor.title"/>&nbsp;
			<@ } @>
			<@=doctor.providerName@>
          <@ } else if (doctor.providerType == "dentist") {@>
			<i class='user-md'></i>&nbsp;<@=doctor.providerName@>
          <@ } else { @>
			<i class='hospital'></i>&nbsp;<@=doctor.providerName@>
          <@ } @>
          &nbsp; <a href="preferences?slide=2" class="margin10-l"><spring:message code="pd.label.title.edit" htmlEscape="true"/></a></br>
          <span><@=doctor.providerSpecialty@> </br>
		  <@if(doctor.providerPhone != ""){@>
      	  	<@=doctor.providerPhone@><br>
		  <@}@>
          <@=doctor.providerAddress@><br>
          <@=doctor.providerCity@>, <@=doctor.providerState@> <@=doctor.providerZip@><br>
        </div>
        <div class="details">
          <i class='<@if(doctor.networkStatus=="outNetWork"){@>NA<@}else if(doctor.networkStatus=="inNetWork"){@>GOOD<@}else{@><spring:message code="pd.label.unknown" htmlEscape="true"/><@}@>'></i>
          <@if(doctor.networkStatus=="outNetWork"){@><spring:message code="pd.label.outNetwork" htmlEscape="true"/><@}else if(doctor.networkStatus=="inNetWork"){@><spring:message code="pd.label.inNetwork" htmlEscape="true"/><@}else{@><spring:message code="pd.label.unknownAffiliation" htmlEscape="true"/><@}@>
        </div>
      </div>
    <@ }); @>
    <@ } else { @>
    <div class="row-detail clearfix providerRows">
      <div class="cell">
          <a href="preferences?slide=2" class="margin10-l"><spring:message code="pd.label.title.checkDoctor" /></a>
      </div>
	  <div class="details"></div>
    </div>
  <@ } @>
</script> --%>

<script type="text/html" id="havingBabySBC">
  <div class="details col-xs-6">
<@ if(TOTAL_COST !== null && TOTAL_COST !== "") { @>
     $<@=TOTAL_COST@>
<@ } else { @>
	  <small><spring:message code="pd.label.commontext.notAvailable" /></small>
    <@ } @>

  </div>
</script>
<script type="text/html" id="havingDiabetesSBC">
  <div class="details col-xs-6">
<@ if(TOTAL_COST !== null &&  TOTAL_COST !== "") { @>
    $<@=TOTAL_COST@>
<@ } else { @>
	  <small><spring:message code="pd.label.commontext.notAvailable" /></small>
    <@ } @>
</script>
<script type="text/html" id="havingFractureSBC">
  <div class="details col-xs-6">
<@ if(TOTAL_COST !== null && TOTAL_COST !== "") { @>
    $<@=TOTAL_COST@>
<@ } else { @>
	  <small><spring:message code="pd.label.commontext.notAvailable" /></small>
    <@ } @>
</script>
