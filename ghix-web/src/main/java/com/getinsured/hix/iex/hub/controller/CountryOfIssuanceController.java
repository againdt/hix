package com.getinsured.hix.iex.hub.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.iex.hub.Countries;
import com.getinsured.hix.iex.hub.repository.ICountriesRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;


/**
 * Countries class that will invoke the HUB service to get
 * list of countries that are valid for the country of Issuance input field
 * of VLP services
 * 
 * HIX-34223
 * 
 * @author Nikhil Talreja
 * @since 11-Apr-2014
 *
 */

@Controller
public class CountryOfIssuanceController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CountryOfIssuanceController.class);
	
	@Autowired private GhixRestTemplate ghixRestTemplate; 
	
	@Autowired private ICountriesRepository iCountriesRepository;
	
	/**
	 * This method will call the HUB service to get list of countries
	 * and persist them into COUNTRIES table
	 * 
	 * HIX-34223
	 * 
	 * @author Nikhil Talreja
	 * @since 11-Apr-2014
	 *
	 */
	@RequestMapping(value="/hub/getCountryOfIssuanceListFromHub")
	@PreAuthorize("hasPermission(#model, 'GET_COI_LIST')")
	@ResponseBody
	public void getCountryOfIssuanceListFromHub(HttpServletRequest request){
		
		try{
			String countryList = ghixRestTemplate.postForObject(GhixEndPoints.HubIntegrationEndpoints.COI_URL, null, String.class);
		
			if(countryList != null){
				JSONParser parser = new JSONParser();
				JSONObject countries = (JSONObject) parser.parse(countryList);
				JSONObject countrySet = (JSONObject)countries.get("GetCountryOfIssuanceListResponseSet");
				if(countrySet != null){
					LOGGER.debug("Countries received : " + countrySet.get("NumberOfCountryCodes"));
					JSONObject countryCodeArray = (JSONObject)countrySet.get("CountryCodeArray");
					JSONArray countryCodeList = (JSONArray)countryCodeArray.get("CountryCodeList");
					if(countryCodeList != null){
						LOGGER.debug("Country names : " + countryCodeList);
						List<Countries> countryRecords = new ArrayList<Countries>();
						for(Object rec : countryCodeList){
							JSONObject country = (JSONObject)rec;
							Countries record = new Countries();
							record.setCountryCode(country.get("CountryCode").toString());
							record.setCountryName(country.get("CountryDescription").toString());
							countryRecords.add(record);
						}
						iCountriesRepository.deleteAll();
						iCountriesRepository.save(countryRecords);
						LOGGER.debug("Country records saved successfully");
					}
				}
				else{
					LOGGER.debug("No countries in list");
				}
				
			}
			
			
		}
		catch (Exception e) {
			LOGGER.error("Unable to get country of issuance list ",e);
		}
		
	}
	
	/**
	 * This method will get list of countries
	 * from COUNTRIES table
	 * 
	 * HIX-34223
	 * 
	 * @author Nikhil Talreja
	 * @return 
	 * @since 11-Apr-2014
	 *
	 */
	@RequestMapping(value="/hub/getCountryOfIssuanceList")
	@ResponseBody
	public List<Countries> getCountryOfIssuanceList(HttpServletRequest request){
		LOGGER.debug("Fetching list of countries from COUNTRIES table");
		return iCountriesRepository.findAll();
	}
	
}
