package com.getinsured.hix.iex.lce;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LifeChangeEventControllerMock {
	public static void main(String asdf[]) throws IOException {
		LifeChangeEventControllerMock mock = new LifeChangeEventControllerMock();
		System.out.println(mock.loadMaritalStatusJson());
	}

	public static String loadMaritalStatusJson() throws IOException {
		return loadJson("ssap_marital_status_json.json");
	}
	
	public static String loadIncarcerationJson() throws IOException {
		return loadJson("ssap_incarceration_json.json");
	}

	public static String loadNoOfDependentsJson() throws IOException {
		return loadJson("ssap_no_of_dependents_json.json");
	}

	public static String loadChangeInAddressJson() throws IOException {
		return loadJson("ssap_change_in_address_json.json");
	}
	
	public static String loadNameChangeJson() throws IOException {
		return loadJson("ssap_name_change_json.json");
	}
	
	public static String loadChangeInIncomeJson() throws IOException {
		return loadJson("ssap_change_in_income_json.json");
	}
	
	public static String loadLawfulPresenceJson() throws IOException {
		return loadJson("ssap_lawful_presence_json.json");
	}
	
	public static String loadLossOfMinimumEssentialCoverageJson() throws IOException {
		return loadJson("ssap_loss_of_minimum_essential_coverage_json.json");
	}
	
	public static String loadGainInMinimumEssentialCoverageJson() throws IOException {
		return loadJson("ssap_gain_in_minimum_essential_coverage_json.json");
	}
	
	public static String loadChangeInTribeStatusJson() throws IOException {
		return loadJson("ssap_tribe_status_json.json");
	}
	
	public static String loadJson(String fileName) throws IOException {
		String line = null;
		StringBuilder builder = new StringBuilder();
		try {
			InputStream stream = LifeChangeEventControllerMock.class.getResourceAsStream(fileName);
			InputStreamReader streamReader = new InputStreamReader(stream);
			BufferedReader br = new BufferedReader(streamReader);

			while ((line = br.readLine()) != null) {
				builder.append(line).append("\n");
			}
			br.close();
		} catch (IOException exception) {
			throw exception;
		}
		return builder.toString();
	}
}
