package com.getinsured.hix.batch.provider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer.RemoteSolrException;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.batch.provider.service.ProviderService;
import com.getinsured.hix.model.ProviderUpload;

public class ProviderRecordClusterWriter implements ItemStreamWriter<SolrInputDocument> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProviderRecordClusterWriter.class);
	private static final String ERROR_MSG_SOLR = "Failed to write Provider data file at SOLR server. ";
	private static final String ERROR_MSG_NO_SOLR = "No SOLR Server available to write data.";
	private static final String YES_VALUE = "yes";
	private static final String FACILITY_JOB_NAME = "FacilitiesDirectoryJob";
	private static final String PROVIDER_JOB_NAME = "PractitionerDirectoryJob";

	private int port = 8080;
	private String solrServerList;
	private String providerCore;
	private String deleteExistingData;
	private List<HttpSolrServer> solrServersToUse;
	private boolean failedToConnectSolr = false;
	private String jobName;
	private ProviderService batchProviderService;

	public ProviderRecordClusterWriter() {
	}

	@Override
	public void write(List<? extends SolrInputDocument> recordList) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Record List is empty: " + CollectionUtils.isEmpty(recordList));
			LOGGER.debug("Parameter Solr Server URL: " + solrServerList);
			LOGGER.debug("Provider Core: " + providerCore);
		}

		if (StringUtils.isNotBlank(solrServerList) && StringUtils.isNotBlank(providerCore)
				&& !CollectionUtils.isEmpty(recordList)) {

			List<Thread> serverHandles = new ArrayList<>();

			if (!CollectionUtils.isEmpty(solrServersToUse)) {

				for (HttpSolrServer solrServerToUse : solrServersToUse) {
					serverHandles.add(processSolrSend(solrServerToUse, recordList));
				}

				for (Thread threadToJoin : serverHandles) {
					threadToJoin.join();
				}
			}
			else {
				LOGGER.error(ERROR_MSG_NO_SOLR);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Thread processSolrSend(HttpSolrServer solrServerToUse, List<? extends SolrInputDocument> recordList) throws Exception {

		Thread serverClientThread = new Thread(new Runnable() {

			@Override
			public void run() {

				try {

					UpdateResponse response = null;
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("SOLR Server URL To Use " + solrServerToUse.getBaseURL());
					}

					solrServerToUse.add((Collection<SolrInputDocument>) recordList);
					response = solrServerToUse.commit();

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Pushed " + recordList.size() + " documents in " + response.getElapsedTime() + " ms");
					}
				}
				catch (RemoteSolrException | SolrServerException | IOException ex) {
					failedToConnectSolr = true;
					LOGGER.error(ERROR_MSG_SOLR + ex.getMessage(), ex);
				}
			}
		}, "SOLR_CLIENT:[" + solrServerToUse.getBaseURL() + "]");

		serverClientThread.start();
		return serverClientThread;
	}

	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {

		if (this.solrServerList == null) {
			throw new ItemStreamException("Mandatory hob parameter \"solrServerList\" is not available");
		}

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Registering SOLR server list : " + this.solrServerList);
			LOGGER.debug("Delete Existing Data : " + deleteExistingData);
		}

		solrServersToUse = new ArrayList<HttpSolrServer>();
		List<String> solrURLs = Arrays.asList(solrServerList.split(";"));
		UpdateResponse response = null;

		for (String paramSolrURL : solrURLs) {

			try {

				HttpSolrServer solrServerToUse = new HttpSolrServer(paramSolrURL + providerCore);

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("SOLR Server URL: " + solrServerToUse.getBaseURL());
				}

				if (StringUtils.isNotBlank(deleteExistingData) && YES_VALUE.equalsIgnoreCase(deleteExistingData)) {
					response = solrServerToUse.deleteByQuery("*:*");
	
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("Delete data set returned in " + response.getElapsedTime() + " with status " + response.getStatus());
					}
					response = solrServerToUse.commit();
				}
				// In case it fails for any of above, exception will be throw.
				solrServersToUse.add(solrServerToUse);
			}
			catch (RemoteSolrException | SolrServerException | IOException e) {
				failedToConnectSolr = true;
				LOGGER.error("Failed to clean up the server at: " + paramSolrURL + ", Ignoring", e);
			}
		}

		if (CollectionUtils.isEmpty(solrServersToUse)) {
			throw new ItemStreamException (ERROR_MSG_NO_SOLR);
		}
	}

	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
	}

	@BeforeStep
	public void getInterstepData(StepExecution stepExecution) {
	    this.jobName = stepExecution.getJobExecution().getJobInstance().getJobName();

	    if (LOGGER.isInfoEnabled()) {
	    	LOGGER.info("Current Job Name: " + jobName);
	    }
	}

	@Override
	public void close() throws ItemStreamException {

		if (!CollectionUtils.isEmpty(solrServersToUse)) {
			this.solrServersToUse.clear();

			if (null != batchProviderService) {

				String providerTypeIndicator = null;

				if (FACILITY_JOB_NAME.equals(jobName)) {
					providerTypeIndicator = ProviderUpload.PROVIDER_TYPE_INDICATOR.F.name();
				}
				else if (PROVIDER_JOB_NAME.equals(jobName)) {
					providerTypeIndicator = ProviderUpload.PROVIDER_TYPE_INDICATOR.P.name();
				}

				if (StringUtils.isNotBlank(providerTypeIndicator)) {
					// Update status with COMPLETED to IN-PROGRESS Tracking Record.
					batchProviderService.updateTrackingStatusFromInProgressToCompleteOrFail(!failedToConnectSolr, providerTypeIndicator);
				}
			}
			LOGGER.info("------------------------------------ Done SOLR commit ---------------------------------------");
		}
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getSolrServerList() {
		return solrServerList;
	}

	public void setSolrServerList(String solrServerList) throws SolrServerException, IOException {
		this.solrServerList = solrServerList;
	}

	public String getProviderCore() {
		return providerCore;
	}

	public void setProviderCore(String providerCore) {
		this.providerCore = providerCore;
	}

	public String getDeleteExistingData() {
		return deleteExistingData;
	}

	public void setDeleteExistingData(String deleteExistingData) {
		this.deleteExistingData = deleteExistingData;
	}

	public ProviderService getBatchProviderService() {
		return batchProviderService;
	}

	public void setBatchProviderService(ProviderService batchProviderService) {
		this.batchProviderService = batchProviderService;
	}
}
