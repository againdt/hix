<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<style>
.accordion-border-none .accordion-group {
	border: 0 none;
}
</style>
<!-- start of secondary navbar -->

		
	<div class="gutter10">
    	<ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code='label.manageIssuerAcctlabel'/></a></li>
            <li><spring:message  code="pgheader.updateCertiStatus"/></li>
        </ul><!--page-breadcrumb ends-->
        
		<ul class="nav nav-pills">
		    <li class="active"><a href="<c:url value="/admin/manageissuer" />"><spring:message  code='label.manageIssuerAcct'/></a></li>
		    <li><a href="<c:url value="/admin/addNewIssuer"/>" ><spring:message  code='label.createIssuerAccount'/></a></li>
		</ul>
      	<!-- end of secondary navbar -->
    </div>
    
	<div class="row-fluid">
	    <div class="span12">
	    	<div class="gutter10">
				<div class="page-header">
					<h3><a name="skip" class="skip">${issuer.name}: <small><spring:message  code="pgheader.updateCertiStatus"/></small></a></h3>
				</div><!-- end of page-header -->	    
	    	</div>
	    	    	
	             <form class="form-horizontal gutter10" id="frmIssuerStatus" name="frmIssuerStatus" action="<c:url value="/admin/changeissuerstatus" />" method="POST">
	                <df:csrfToken/>
	              				   
				    <input type="hidden" id="issuerpid" name="issuerid" value="<encryptor:enc value="${issuer.id}"></encryptor:enc>"/>
				    <input type="hidden" id="target_name" name="target_name" value="<encryptor:enc value="ISSUER"></encryptor:enc>"/>
				    <input type="hidden" id="target_id" name="target_id" value="<encryptor:enc value="${issuer.id}"></encryptor:enc>"/>
				    
	               <div class="profile">
		               <div class="row-fluid">
	                    <table class="table table-border-none table-condensed">
	                        <tbody>
	                            <tr>
	                                <td class="span4 txt-right"><spring:message  code="label.licenseNumber"/></td>
	                                <td><strong>${issuer.licenseNumber}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.licenseStatus"/></td>
	                                <td><strong>${issuer.licenseStatus}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.NAICCompanyNumber"/></td>
	                                <td><strong>${issuer.naicCompanyCode}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.NAICGroupCode"/></td>
	                                <td><strong>${issuer.naicGroupCode}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.HIOSIssuerId"/></td>
	                                <td><strong>${issuer.hiosIssuerId}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.busiAddress"/></td>
	                                <td><strong>${issuer.addressLine1} ${issuer.addressLine2} <br/> ${issuer.city},  ${issuer.state}  ${issuer.zip}</strong></td>
	                            </tr>
	                            
	                        </tbody>
	                    </table>
	                    <hr />
		              	
	                    <div class="accordion accordion-border-none" id="accordion">
							<div class="accordion-group">
								<div id="issuerDocuments" class="accordion-body collapse">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#noIssuerDocuments">
										<i class="icon-minus"></i> <spring:message  code="pgheader.hideAppDoc" />
									</a>

								  										
											<table class="table table-border-none table-condensed">
												<tbody>
													<tr>
														<td class="span4 txt-right"><spring:message  code="label.issuerUploadAuthority"/></td>
														<td>
															<c:forEach var="authorityFileObj" items="${authorityFile}">
																<div><a href="<c:url value="/ecm/filedownloadbyid?uploadedFileId=${authorityFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
															</c:forEach>
														</td>
													</tr>
													<tr>
														<td class="txt-right"><spring:message  code="label.issuerUploadMarketingReq"/></td>
														<td>
															<c:forEach items="${marketingFile}" var="marketingFileObj">
																<div><a href="<c:url value="/ecm/filedownloadbyid?uploadedFileId=${marketingFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
															</c:forEach>
														</td>
													</tr>
													<tr>
														<td class="txt-right"><spring:message  code="label.financialDisclosures"/></td>
														<td>
															<c:forEach items="${disclosuresFile}" var="disclosuresFileObj">
																<div><a href="<c:url value="/ecm/filedownloadbyid?uploadedFileId=${disclosuresFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
															</c:forEach>
														</td>
													</tr>
													<tr>
														<td class="txt-right"><spring:message  code="label.issuerUploadAccreditation" /></td>
														<td>
															<c:forEach items="${accreditationFile}" var="accreditationFileObj">
																<div><a href="<c:url value="/ecm/filedownloadbyid?uploadedFileId=${accreditationFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
															</c:forEach>
														</td>
													</tr>
													<tr>
														<td class="txt-right"><spring:message  code="label.organizationData"/></td>
														<td>
															<c:forEach items="${addInfoFile}" var="addInfoFileObj">
																<div><a href="<c:url value="/ecm/filedownloadbyid?uploadedFileId=${addInfoFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
															</c:forEach>
														</td>
													</tr>
													<tr>
														<td class="txt-right"><spring:message  code="label.certificationDoc"/> </td>
														<td>
														<c:if test="${not empty certificationDoc}" >														
															<div><a href="<c:url value="/ecm/filedownloadbyid?uploadedFileId=${certificationDoc}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
														</c:if>
															
														</td>
													</tr>
												</tbody>
											</table>										
									
								</div>
							</div>
							<div class="accordion-group">
								<div id="noIssuerDocuments" class="accordion-body collapse in">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#issuerDocuments">
										<i class="icon-plus"></i> <spring:message  code="pgheader.showAppDoc" />
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="box-gutter10">
							<h4 class="alert alert-info"><spring:message  code="label.currentStatus" />: ${issuer.certificationStatus}</h4>
						
		                    <div class="control-group">
		                        <label for="select01" class="control-label"><spring:message  code="label.certificationStatus"/></label>
		                        <div class="controls">
		                            <select id="issuerCertifyNewStatus" name="issuerCertifyNewStatus">
		                                <option value=""><spring:message  code="label.selectUpperCase" /></option>
		                                <c:forEach var="issuerCertStatus" items="${issuerCertStatusList}">
								    		<option value="${issuerCertStatus.key}">${issuerCertStatus.value}</option>
										</c:forEach>		                                
		                            </select>
		                            <div id="issuerCertifyNewStatus_error" class="help-inline"></div>
		                        </div> <!-- end of controls-->
		                    </div>
		                    <!-- end of control-group --> 
		
			            <div class="control-group">
				         <label for="textarea" class="control-label"><spring:message  code="label.comment"/></label>
				            <div class="controls">
				              <textarea class="input-xlarge" name="comment_text" id="comment_text" rows="4" cols="40" style="resize: none;" 
							maxlength="500" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();"></textarea>
							<div id="comment_text_error"  class="help-inline"></div>
							<div id="chars_left"><spring:message  code="label.charLeft"/> <b><spring:message  code="label.charLeftValue"/></b></div>
				            </div>
			         	</div>
			         	
						<!-- end of control-group --> 
						<div class="control-group">
				            <label for="fileInput" class="control-label"><spring:message  code="label.uploadFile"/></label>
				            
				            <div class="controls">				            
				              <input type="file" class="input-file" id="certSuppDoc" name="certSuppDoc">&nbsp;
				              <input type="submit" id="btnCertSuppDoc" value="<spring:message  code="label.upload"/>" class="btn" title="<spring:message  code="label.upload"/>">
				              <div id="certSuppDoc_error" class="help-inline"></div>
				              <div id="certSuppDoc_display">${certiSuppDoc}</div>
		                   	   <input type="hidden" id="hdnCertSuppDoc" name="hdnCertSuppDoc" value=""/>
		                   	   		                   	   
				            </div>
			          	</div>
						<!-- end of control-group --> 
					<div class="form-actions">
	                    <div class="control-group span5">                            
	                    	<input type="button" name="button" id="btnUpdate" value="<spring:message  code='label.update'/>" title="<spring:message  code='label.update'/>" class="btn btn-primary"  onclick="formSubmit();"/>
	                    </div>  
	                </div> 
	                 <!-- end of form actions -->
						
						</div> <!-- box-loose --> 
					</div> <!-- row fluid forms --> 
					</div> <!-- end of profile -->
					
	                </form>                        
	                      
	    </div><!-- end of span12 -->

</div><!--  end of row-fluid -->
	
<script type="text/javascript">
var commentMaxLen = 500;
var csrValue = $("#csrftoken").val();
$(document).ready(function(){	
	$("#btnCertSuppDoc").click(function(){
		if($("#certSuppDoc").val() != ""){  // if file browsed then do upload 
			$('#btnUpdate').attr("disabled","disabled"); // disable update button during file uploadig
			$('#frmIssuerStatus').ajaxSubmit({
				url: "<c:url value='/admin/uploadissuercertsuppdoc'/>",
				data: {"csrftoken":csrValue},
				success: function(responseText){
						if(responseText != ""){
							var responseArray = responseText.split('|') // split the response
							$("#certSuppDoc_display").text(responseArray[0]); // show original file name
							$("#hdnCertSuppDoc").val(responseArray[1]); // store DMS id
							$('#btnUpdate').removeAttr("disabled"); // enable update button after file upload
						}else{
							$("#certSuppDoc_error").html("Fail to upload file");							
							$('#btnUpdate').removeAttr("disabled"); // enable update button
						}	
		       	}
			});
		}	
	  return false; 
	});	// button click close
})


function  formSubmit(){	
	$("#frmIssuerStatus").submit();
}

// form validation
var validator = $("#frmIssuerStatus").validate({ 
	rules : {
		issuerCertifyNewStatus : {required : true}
	},
	messages : {
		issuerCertifyNewStatus : { required : "<span><em class='excl'>!</em><spring:message  code='err.selectCertStatus' javaScriptEscape='true'/></span>"}
		},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');		
	}
	
});

// Update remaining characters for comments
function updateCharCount(){	
	var currentLen = $.trim(document.getElementById("comment_text").value).length;
	var charLeft = commentMaxLen - currentLen;
	
	$('#chars_left').html("<spring:message  code='label.charactersLeft'/> <b>" + charLeft + "</b>" );
}
</script>