
function UpdateAddressDetailsNonfinancial(i)
{
	if(validation(currentPage)==false)  
		return false;
	var home_addressLine1=$('#home_addressLine1').val();
	var home_addressLine2=$('#home_addressLine2').val();
	var home_primary_city=$('#home_primary_city').val();
	var home_primary_zip=$('#home_primary_zip').val();
	var home_primary_state=$('#home_primary_state').val();
	var liveAtOtherAddressIndicator=false;
	
	var mailing_addressLine1=$('#mailing_addressLine1').val();
	var mailing_addressLine2=$('#mailing_addressLine2').val();
	var mailing_primary_city=$('#mailing_primary_city').val();
	var mailing_primary_zip=$('#mailing_primary_zip').val();
	var mailing_primary_state=$('#mailing_primary_state').val();
	var mailing_primary_county=$('#mailing_primary_county').val();
	
	var houseHoldPhone1=$('#first_phoneNo').val();
	var houseHoldPhoneExt1=$('#first_ext').val();
	var houseHoldPhoneType1=$('#first_phoneType').val();
	var houseHoldPhone2=$('#second_phoneNo').val();
	var houseHoldPhoneExt2=$('#second_ext').val();
	var houseHoldPhoneType2=$('#second_phoneType').val();
	var houseHoldSpokenLanguag=$('#preffegred_spoken_language').val();
	var houseHoldWrittenLanguag=$('#preffered_written_language').val();
	var isTemporarilyLivingOutsideIndicator=true;
	 var otherAddressOfHouseHold="";
	
	 
	
		
			if(i==0)
			{
			
				$("#appscr57HouseHoldhome_addressLine1TD"+(i+1)).html(home_addressLine1);
				$("#appscr57HouseHoldhome_addressLine2TD"+(i+1)).html(home_addressLine2);
				$("#appscr57HouseHoldhome_primary_cityTD"+(i+1)).html(home_primary_city);
				$("#appscr57HouseHoldhome_primary_zipTD"+(i+1)).html(home_primary_zip);
				$("#appscr57HouseHoldhome_primary_stateTD"+(i+1)).html(home_primary_state);
				
				$("#appscr57HouseHoldmailing_addressLine1TD"+(i+1)).html(mailing_addressLine1);
				$("#appscr57HouseHoldmailing_addressLine2TD"+(i+1)).html(mailing_addressLine2);
				$("#appscr57HouseHoldmailing_primary_cityTD"+(i+1)).html(mailing_primary_city);
				$("#appscr57HouseHoldmailing_primary_zipTD"+(i+1)).html(mailing_primary_zip);
				$("#appscr57mailing_primary_stateTD"+(i+1)).html(mailing_primary_state);
				$("#appscr57HouseHoldmailing_primary_countyTD"+(i+1)).html(mailing_primary_county);			
						
				$("#appscr57houseHoldPhoneFirstTD"+(i+1)).html(houseHoldPhone1);
				$("#appscr57houseHoldPhoneExtFirstTD"+(i+1)).html(houseHoldPhoneExt1);
				$("#appscr57houseHoldPhoneTypeFirstTD"+(i+1)).html(houseHoldPhoneType1);
				$("#appscr57houseHoldPhoneSecondTD"+(i+1)).html(houseHoldPhone2);
				$("#appscr57houseHoldPhoneExtSecondTD"+(i+1)).html(houseHoldPhoneExt2);
				$("#appscr57houseHoldPhoneTypeSecondTD"+(i+1)).html(houseHoldPhoneType2);
				$("#appscr57houseHoldSpokenLanguageTD"+(i+1)).html(houseHoldSpokenLanguag);
				$("#appscr57houseHoldWrittenLanguageTD"+(i+1)).html(houseHoldWrittenLanguag);
				
				
				$("#appscr57liveAtOtherAddressIndicatorTD"+(i+1)).html(liveAtOtherAddressIndicator);
				$("#appscr57isTemporarilyLivingOutsideIndicatorTD"+(i+1)).html(isTemporarilyLivingOutsideIndicator);
				$("#appscr57otherAddressState"+(i+1)).html(home_primary_state);
			}
		else
			{
		
			 var actualName=getNameByTRIndex(i);
	 
			  home_addressLine1=$('#applicant_or_non-applican_address_1'+actualName).val();
			  home_addressLine2=$('#applicant_or_non-applican_address_2'+actualName).val();
			  home_primary_city=$('#city'+actualName).val();
			  home_primary_zip=$('#zip'+actualName).val();
			  otherAddressOfHouseHold=$('#applicant_or_non-applican_state'+actualName).val();
			  
			  if ($('#householdContactAddress'+actualName).is(':checked')) {
				  liveAtOtherAddressIndicator=true;
				  home_primary_state="MI";
			  }
			  else
				  {
				  liveAtOtherAddressIndicator=false;
				  
				  home_addressLine1=$('#home_addressLine1').val();
				  home_addressLine2=$('#home_addressLine2').val();
				  home_primary_city=$('#home_primary_city').val();
				  home_primary_zip=$('#home_primary_zip').val();
				  home_primary_state=$('#home_primary_state').val();
				  otherAddressOfHouseHold=home_primary_state;
				  }
			
			  
			  mailing_primary_state=home_primary_state;
			  
			  isTemporarilyLivingOutsideIndicator = $('input[name=isTemporarilyLivingOutsideIndicator'+actualName+']:radio:checked').val();
			  	$("#appscr57HouseHoldhome_addressLine1TD"+(i+1)).html(home_addressLine1);
				$("#appscr57HouseHoldhome_addressLine2TD"+(i+1)).html(home_addressLine2);
				$("#appscr57HouseHoldhome_primary_cityTD"+(i+1)).html(home_primary_city);
				$("#appscr57HouseHoldhome_primary_zipTD"+(i+1)).html(home_primary_zip);
				$("#appscr57HouseHoldhome_primary_stateTD"+(i+1)).html(home_primary_state);

				$("#appscr57HouseHoldmailing_addressLine1TD"+(i+1)).html(mailing_addressLine1);
				$("#appscr57HouseHoldmailing_addressLine2TD"+(i+1)).html(mailing_addressLine2);
				$("#appscr57HouseHoldmailing_primary_cityTD"+(i+1)).html(mailing_primary_city);
				$("#appscr57HouseHoldmailing_primary_zipTD"+(i+1)).html(mailing_primary_zip);
				$("#appscr57mailing_primary_stateTD"+(i+1)).html(mailing_primary_state);
				$("#appscr57HouseHoldmailing_primary_countyTD"+(i+1)).html(mailing_primary_county);			
						
				$("#appscr57houseHoldPhoneFirstTD"+(i+1)).html(houseHoldPhone1);
				$("#appscr57houseHoldPhoneExtFirstTD"+(i+1)).html(houseHoldPhoneExt1);
				$("#appscr57houseHoldPhoneTypeFirstTD"+(i+1)).html(houseHoldPhoneType1);
				$("#appscr57houseHoldPhoneSecondTD"+(i+1)).html(houseHoldPhone2);
				$("#appscr57houseHoldPhoneExtSecondTD"+(i+1)).html(houseHoldPhoneExt2);
				$("#appscr57houseHoldPhoneTypeSecondTD"+(i+1)).html(houseHoldPhoneType2);
				$("#appscr57houseHoldSpokenLanguageTD"+(i+1)).html(houseHoldSpokenLanguag);
				$("#appscr57houseHoldWrittenLanguageTD"+(i+1)).html(houseHoldWrittenLanguag);
				
				$("#appscr57liveAtOtherAddressIndicatorTD"+(i+1)).html(liveAtOtherAddressIndicator);
				$("#appscr57isTemporarilyLivingOutsideIndicatorTD"+(i+1)).html(isTemporarilyLivingOutsideIndicator);
				$("#appscr57otherAddressState"+(i+1)).html(otherAddressOfHouseHold+'');
			
			
			}
			


}






function updateSpecialCircumstances(i) 
{
	
	if(validation(currentPage)==false)
		return false;
	
	
	
	noOfHouseHold = $('#numberOfHouseHold').text();


	
		if(document.getElementById("disability"+i).checked == true)
			$("#appscr57HouseHoldDisability"+(i)).html("yes");
		
		else
			
		$("#appscr57HouseHoldDisability"+i).html("no");
		if(document.getElementById("assistanceServices"+i).checked == true)
			
		$("#appscr57HouseHoldAssistanceServices"+i).html("yes");
			else
		
		$("#appscr57HouseHoldAssistanceServices"+i).html("no");
		
		if(document.getElementById("native"+i).checked == true)
		
			$("#appscr57HouseHoldNative"+i).html("yes");
			else
		
		$("#appscr57HouseHoldNative"+i).html("no");
		
		if(document.getElementById("personfosterCare"+i).checked == true)
		
		$("#appscr57HouseHoldPersonfosterCare"+i).html("yes");
			else
		
				$("#appscr57HouseHoldPersonfosterCare"+i).html("no");
		
		
		$("#appscr57HouseHoldIsPersonGettingHealthCare"+i).html($('input[name=isPersonGettingHealthCare'+i+']:radio:checked').val());
		 
		 
		
		if(document.getElementById("deceased"+i).checked == true)
			$("#appscr57HouseHoldDeceased"+i).html("yes");
		else
			$("#appscr57HouseHoldDeceased"+i).html("no");
				
		
		
		
		
		
		
		 if($('#appscrHouseHoldgender57TD'+i).text() == 'female')
			 {
			 
			 if(	document.getElementById("pregnancy"+i)!=null && document.getElementById("pregnancy"+i).checked == true)
				 {
				 $(".i_pregnant").html("yes");
				 $("#appscr57HouseHoldFetusCount"+i).text($('#number-of-babies option:selected').val());
				 }
			 else
				 {
		
				 	 $(".i_pregnant").html("no");
					 $("#appscr57HouseHoldFetusCount"+i).text("0");
				 }
			 }
		 else
			 {
			 
			 $(".i_pregnant").html("no");
			 $("#appscr57HouseHoldFetusCount"+i).html("0");
			 }
				  


}

function updateIncomeDiscrepancies() 
{
	//var i =($('#houseHoldTRNo').val()).substring(9);
	var i = getCurrentApplicantID();
	$("#appappscr57HouseHoldExplanationTextAreaTD"+i).html($('#explanationTextArea').val());
	
	$("#appscrStopWorking57TD"+i).html($('input[name=stopWorking]:radio:checked').val());
	$("#appscrWorkAt57TD"+i).html($('input[name=workAt]:radio:checked').val());
	$("#appscrDecresedAt57TD"+i).html($('input[name=wecresedAt]:radio:checked').val());
	$("#appscrSalaryAt57TD"+i).html($('input[name=salaryAt]:radio:checked').val());
	$("#appscrSesWorker57TD"+i).html($('input[name=sesWorker]:radio:checked').val());
	$("#appscrExplainText57TD"+i).html($('#appscr73ExplanationTaxtArea').val());
	$("#appscrOtherJointIncome57TD"+i).html($('input[name=OtherJointIncome]:radio:checked').val());

}



function updateIncarceratedDetail(i) 
{
		if(document.getElementById("incarcerationCB"+i).checked == true)
			$("#appscr85incarcerationTD"+i).html("yes");
		else
			$("#appscr85incarcerationTD"+i).html("no");
		
		$("#appscr85disposition"+i).html($('input[name=disposition'+i+']:radio:checked').val());
	
}







function updateFinancialIncomeDetails() 
{
	
		var dataIndex=0;
		$("#incomesStatus"+financialHouseHoldDone).html($('input[name=appscr70radios4Income]:radio:checked').val());
		for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {
		
	
		if(dataIndex==0)
		{
			var irsIncome= $('#irsIncome').val();
				irsIncome = (irsIncome == '')?0:irsIncome;
				$("#appscrirsIncome57TD"+financialHouseHoldDone).html(irsIncome);	
				$('#irsIncome').val('');
		
		continue;
		}
		var data = financial_DetailsArray[dataIndex];
		var fieldData = $('#'+data+'_id').val();
		if(fieldData == '' || fieldData == undefined)
			{
				fieldData = 0;
			}
			
		incomeAmount[data]=fieldData;
		
		
		var fieldFrequencyData= $('#'+data+'_select_id').val();

		incomeFrequency[data]=fieldFrequencyData;
		
			
			$("#appscr"+data+"57TD"+financialHouseHoldDone).html(fieldData);	
			if(dataIndex!=2 && dataIndex!=6)
			$("#appscr"+data+"Frequency57TD"+financialHouseHoldDone).html(fieldFrequencyData);
		}
		/*var expectedIncomeByHouseHoldMember= $('#expectedIncomeByHouseHoldMember').val();
		if(expectedIncomeByHouseHoldMember=='')
			expectedIncomeByHouseHoldMember="N/A";
		
		$("#appscrexpectedIncome57TD"+financialHouseHoldDone).html(expectedIncomeByHouseHoldMember);*/
		
		
		$('#appscrFinancial_JobEmployerName57TD'+financialHouseHoldDone).text($('#Financial_Job_EmployerName_id').val());
		$('#appscrFinancial_JobHoursOrDays57TD'+financialHouseHoldDone).text($('#Financial_Job_HoursOrDays_id').val());
		$('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+financialHouseHoldDone).text($('#Financial_Self_Employment_TypeOfWork').val());
		$('#appscrFinancial_UnemploymentStateGovernment57TD'+financialHouseHoldDone).text($('#Financial_Unemployment_StateGovernment').val());
		$('#appscrFinancial_UnemploymentDate57TD'+financialHouseHoldDone).text($('#unemploymentIncomeDate').val());
		$('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+financialHouseHoldDone).text($('#Financial_OtherTypeIncome').val());
		
		
		$('#appscrStopWorking57TD'+financialHouseHoldDone).text($('input[name=stopWorking]:radio:checked').val());
		$('#appscrWorkAt57TD1'+financialHouseHoldDone).text($('input[name=workAt]:radio:checked').val());
		$('#appscrDecresedAt57TD'+financialHouseHoldDone).text($('input[name=decresedAt]:radio:checked').val());
		$('#appscrSalaryAt57TD'+financialHouseHoldDone).text($('input[name=salaryAt]:radio:checked').val());
		$('#appscr57HouseHoldExplanationTextAreaTD'+financialHouseHoldDone).text($('#explanationTextArea').val());
		$('#appscrSesWorker57TD'+financialHouseHoldDone).text($('input[name=sesWorker]:radio:checked').val());
		$('#appscrExplainText57TD'+financialHouseHoldDone).text($('#appscr73ExplanationTaxtArea').val());
		$('#appscrOtherJointIncome57TD'+financialHouseHoldDone).text($('input[name=OtherJointIncome]:radio:checked').val());
}		











function updateNonFinancialDetailsToTable() 
{
	
	 
	 HouseHoldgender = $('input[name=appscr61_gender]:radio:checked').val();
	 HouseHoldHavingSSN =$('input[name=socialSecurityCardHolderIndicator]:radio:checked').val();
	 /*HouseHoldssn1=  $('#ssn1').val();
	 HouseHoldssn2=  $('#ssn2').val();
	 HouseHoldssn3=  $('#ssn3').val();
	 HouseHoldSSN = HouseHoldssn1 +'-'+HouseHoldssn2+'-'+HouseHoldssn3;*/
	 HouseHoldSSN = $('#ssn').val();
	 notAvailableSSNReason = $('#reasonableExplanationForNoSSN').val();
	 UScitizen= $('input[name=UScitizen]:radio:checked').val();
	 var UScitizenManualVerification= $('input[name=UScitizenManualVerification]:radio:checked').val();
	 naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val();
	 livedIntheUSSince1996Indicator= $('input[name=livedIntheUSSince1996Indicator]:radio:checked').val();
	
	 
	 certificateType = $('input[name=naturalizedCitizen]:radio:checked').val();	
	 certificateAlignNumber = "";
	 certificateNumber = "";
	 
	 documentSameNameIndicator = $('input[name=UScitizen62]:radio:checked').val();
	 documentFirstName = $('#documentFirstName').val();
	 documentLastName = $('#documentLastName').val();
	 documentMiddleName = $('#documentMiddleName').val();
	 documentSuffix = $('#documentSuffix').val();		
	 
	 
	 var naturalCitizen_eligibleImmigrationStatus = $('#naturalCitizen_eligibleImmigrationStatus').prop('checked');
	 if(naturalCitizen_eligibleImmigrationStatus=='')
		 {
		 naturalCitizen_eligibleImmigrationStatus='false';
		 }
 
	 if(getInfantData(nonFinancialHouseHoldDone)=='yes')
	 {
	 
			 $('#appscrHouseHoldgender57TD'+nonFinancialHouseHoldDone).html("male");
			 $('#appscr57HouseHoldHavingSSNTD'+nonFinancialHouseHoldDone).html("no");
			 $('#appscr57HouseHoldSSNTD'+nonFinancialHouseHoldDone).html("--");
			 $('#appscr57notAvailableSSNReasonTD'+nonFinancialHouseHoldDone).html("no");
			 $('#appscr57UScitizenTD'+nonFinancialHouseHoldDone).html(UScitizen);
			 $('#appscr57UScitizenManualVerificationTD'+nonFinancialHouseHoldDone).html(UScitizenManualVerification);
			 $('#appscr57livedIntheUSSince1996IndicatorTD'+nonFinancialHouseHoldDone).html("no");
			 $('#appscr57naturalizedCitizenshipIndicatorTD'+nonFinancialHouseHoldDone).html("no");
			 $('#appscr57ImmigrationStatusTD'+nonFinancialHouseHoldDone).html(naturalCitizen_eligibleImmigrationStatus);
			 
			 $('#appscr57certificateTypeIndicatorTD'+nonFinancialHouseHoldDone).html('None');
			 $('#appscr57naturalizedCitizenshipAlignNumberTD'+nonFinancialHouseHoldDone).html('');
			 $('#appscr57certificateNumberTD'+nonFinancialHouseHoldDone).html('');
			 $('#appscr57documentSameNameIndicatorTD'+nonFinancialHouseHoldDone).html('yes');
			 $('#appscr57documentFirstNameTD'+nonFinancialHouseHoldDone).html('');
			 $('#appscr57documentMiddleNameTD'+nonFinancialHouseHoldDone).html('');
			 $('#appscr57documentLastNameTD'+nonFinancialHouseHoldDone).html('');
			 $('appscr57documentSuffixTD'+nonFinancialHouseHoldDone).html('Suffix');
	 }
 else{
	 	
	 $('#appscrHouseHoldgender57TD'+nonFinancialHouseHoldDone).html(HouseHoldgender);
	 $('#appscr57HouseHoldHavingSSNTD'+nonFinancialHouseHoldDone).html(HouseHoldHavingSSN);
	 $('#appscr57HouseHoldSSNTD'+nonFinancialHouseHoldDone).html(HouseHoldSSN);
	 $('#appscr57notAvailableSSNReasonTD'+nonFinancialHouseHoldDone).html(notAvailableSSNReason);
	 $('#appscr57UScitizenTD'+nonFinancialHouseHoldDone).html(UScitizen);
	 $('#appscr57UScitizenManualVerificationTD'+nonFinancialHouseHoldDone).html(UScitizenManualVerification);
	 $('#appscr57livedIntheUSSince1996IndicatorTD'+nonFinancialHouseHoldDone).html(livedIntheUSSince1996Indicator);
	 $('#appscr57naturalizedCitizenshipIndicatorTD'+nonFinancialHouseHoldDone).html(naturalizedCitizenshipIndicator);
	 $('#appscr57ImmigrationStatusTD'+nonFinancialHouseHoldDone).html(naturalCitizen_eligibleImmigrationStatus);
	 
	 if(certificateType == "CitizenshipCertificate")
	 {
		 certificateAlignNumber = $('#citizenshipAlignNumber').val();
		 certificateNumber = $('#citizenshipCertificateNumber').val();
			
		 $('#appscr57certificateTypeIndicatorTD'+nonFinancialHouseHoldDone).html(certificateType);
		 $('#appscr57naturalizedCitizenshipAlignNumberTD'+nonFinancialHouseHoldDone).html(certificateAlignNumber);
		 $('#appscr57certificateNumberTD'+nonFinancialHouseHoldDone).html(certificateNumber);
	 }
	 else
	 {
		 certificateAlignNumber = $('#naturalizationAlignNumber').val();
		 certificateNumber = $('#naturalizationCertificateNumber').val();
		 
		 $('#appscr57certificateTypeIndicatorTD'+nonFinancialHouseHoldDone).html(documentSameNameIndicator);
		 $('#appscr57naturalizedCitizenshipAlignNumberTD'+nonFinancialHouseHoldDone).html(certificateAlignNumber);
		 $('#appscr57certificateNumberTD'+nonFinancialHouseHoldDone).html(certificateNumber);
	 }
	 $('#appscr57documentSameNameIndicatorTD'+nonFinancialHouseHoldDone).html(documentSameNameIndicator);
	 if(documentSameNameIndicator == "no")
	 {
		 $('#appscr57documentFirstNameTD'+nonFinancialHouseHoldDone).html(documentFirstName);
		 $('#appscr57documentMiddleNameTD'+nonFinancialHouseHoldDone).html(documentMiddleName);
		 $('#appscr57documentLastNameTD'+nonFinancialHouseHoldDone).html(documentLastName);
		 $('appscr57documentSuffixTD'+nonFinancialHouseHoldDone).html(documentSuffix);
	 }
	 else
	 {
		 $('#appscr57documentFirstNameTD'+nonFinancialHouseHoldDone).html('');
		 $('#appscr57documentMiddleNameTD'+nonFinancialHouseHoldDone).html('');
		 $('#appscr57documentLastNameTD'+nonFinancialHouseHoldDone).html('');
		 $('#appscr57documentSuffixTD'+nonFinancialHouseHoldDone).html('Suffix');
	 }
	 
	 	 		
	var ethnicityIndicator = $('input[name=checkPersonNameLanguage]:radio:checked').val();
	$('#appscr57EthnicityIndicatorTD'+nonFinancialHouseHoldDone).text(ethnicityIndicator);
	if(ethnicityIndicator == "yes" || ethnicityIndicator == "Yes")
	{
		 $('#appscr57EthnicityCubanTD'+nonFinancialHouseHoldDone).text($('#ethnicity_cuban').prop("checked"));
		 $('#appscr57EthnicityMexicanMexicanTD'+nonFinancialHouseHoldDone).text($('#ethnicity_maxican').prop("checked"));
		 $('#appscr57EthnicityPuertoRicanTD'+nonFinancialHouseHoldDone).text($('#ethnicity_puertoRican').prop("checked"));
		 /*$('#appscr57EthnicityOtherTD'+nonFinancialHouseHoldDone).text($('#ethnicity_other').prop("checked"));
		 $('#appscr57EthnicityTextTD'+nonFinancialHouseHoldDone).text($('#other_ethnicity ').val());*/
	}
	else
	{
		$('#appscr57EthnicityCubanTD'+nonFinancialHouseHoldDone).text("false");
		 $('#appscr57EthnicityMexicanMexicanTD'+nonFinancialHouseHoldDone).text("false");
		 $('#appscr57EthnicityPuertoRicanTD'+nonFinancialHouseHoldDone).text("false");
		 $('#appscr57EthnicityOtherTD'+nonFinancialHouseHoldDone).text("false");
		 $('#appscr57EthnicityTextTD'+nonFinancialHouseHoldDone).text("");
	}
	 			
	 $('#appscr57RaceAmericanIndianTD'+nonFinancialHouseHoldDone).text($('#americanIndianOrAlaskaNative').prop("checked"));
	 $('#appscr57RaceAsianIndianTD'+nonFinancialHouseHoldDone).text($('#asianIndian').prop("checked"));
	 $('#appscr57RaceBlackOrAfricanAmericanTD'+nonFinancialHouseHoldDone).text($('#BlackOrAfricanAmerican').prop("checked"));
	 $('#appscr57RaceChineseTD'+nonFinancialHouseHoldDone).text($('#chinese').prop("checked"));
	 $('#appscr57RaceFilipinoTD'+nonFinancialHouseHoldDone).text($('#filipino').prop("checked"));
	 $('#appscr57RaceGuamanianOrChamorroTD'+nonFinancialHouseHoldDone).text($('#guamanianOrChamorro').prop("checked"));
	 $('#appscr57RaceJapaneseTD'+nonFinancialHouseHoldDone).text($('#japanese').prop("checked"));
	 $('#appscr57RaceKoreanTD'+nonFinancialHouseHoldDone).text($('#korean').prop("checked"));
	 $('#appscr57RaceNativeHawaiianTD'+nonFinancialHouseHoldDone).text($('#nativeHawaiian').prop("checked"));
	 $('#appscr57RaceOtherAsianTD'+nonFinancialHouseHoldDone).text($('#otherAsian').prop("checked"));
	 $('#appscr57RaceOtherPacificIslanderTD'+nonFinancialHouseHoldDone).text($('#otherPacificIslander').prop("checked"));
	 $('#appscr57RaceSamoanTD'+nonFinancialHouseHoldDone).text($('#samoan').prop("checked"));
	 $('#appscr57RaceVietnameseTD'+nonFinancialHouseHoldDone).text($('#vietnamese').prop("checked"));
	 $('#appscr57RaceWhiteOrCaucasianTD'+nonFinancialHouseHoldDone).text($('#whiteOrCaucasian').prop("checked"));
	 /*$('#appscr57RaceOtherTD'+nonFinancialHouseHoldDone).text($('#raceOther').prop("checked"));
	 $('#appscr57RaceOtherTextTD'+nonFinancialHouseHoldDone).text($('#other_race').val());*/
	 
	 
 	}
}




function updateTaxRelatedData() 
{

	var isMarried=false;
	var spouseId=false;
	var payJointly = false;
	var isTaxFiler = false;
	var isInfant = $('#appscr57IsInfantTD'+(HouseHoldRepeat)).text(); 
	if(isInfant =='no')
	{	
	if($('input[name=planToFileFTRIndicator]:radio:checked').val()== 'yes'){
		isTaxFiler = true;
	}
	
	if($('input[name=marriedIndicator]:radio:checked').val()== 'yes'){
		isMarried = true;
		spouseId=$('input[name=householdContactSpouse]:radio:checked').val();
		if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val()== 'yes'){
			payJointly = true;
	
		}
		else
		{
			payJointly = false;
		
			
		}
		
	}
	}
	
	
		
	$("#appscr57isMarriedTD"+HouseHoldRepeat).html(""+isMarried);
		$("#appscr57spouseIdTD"+HouseHoldRepeat).html(""+spouseId);
		$("#appscr57payJointlyTD"+HouseHoldRepeat).html(""+payJointly);
		$("#appscr57isTaxFilerTD"+HouseHoldRepeat).html(""+isTaxFiler);
		
	}



function updateCurrentIncomeDetail() 
{
	//var i =($('#houseHoldTRNo').val()).substring(9);
	var i = getCurrentApplicantID();


	if( $('#alimonyAmountInput').val() == '')
		
		$("#appscr57HouseHoldalimonyAmountTD"+i).html('0');
	else
	
	$("#appscr57HouseHoldalimonyAmountTD"+i).html($('#alimonyAmountInput').val());
	
	
		$("#appscr57HouseHoldalimonyFrequencyTD"+i).html($("#alimonyFrequencySelect option:selected").val());
	
	
	if($('#studentLoanInterestAmountInput').val() == '')
	
	$("#appscr57HouseHoldstudentLoanAmountTD"+i).html('0');
	else
	
	$("#appscr57HouseHoldstudentLoanAmountTD"+i).html($('#studentLoanInterestAmountInput').val());
		
	
		$("#appscr57HouseHoldstudentLoanFrequencyTD"+i).html($("#studentLoanInterestFrequencySelect option:selected").val());
	
		$("#appscr57HouseHolddeductionsTypeTD"+i).html($('#deductionTypeInput').val());
		
	if($('#deductionAmountInput').val() == '')	
	
	$("#appscr57HouseHolddeductionsAmountTD"+i).html('0');
	else
	
	$("#appscr57HouseHolddeductionsAmountTD"+i).html($('#deductionAmountInput').val());
	
		$("#appscr57HouseHolddeductionsFrequencyTD"+i).html($("#deductionFrequencySelect option:selected").val());
	
		$("#appscr57HouseHoldbasedOnInputTD"+i).html($('#basedOnAmountInput').val());

}

