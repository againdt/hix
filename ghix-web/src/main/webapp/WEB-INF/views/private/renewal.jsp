<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script type="text/javascript" src="<c:url value="/resources/js/preserveImgAspectRatio.js" />"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/tiles.css" />" type="text/css">

<script type="text/javascript">
  var plan_display = new Array();
  
  plan_display['pd.label.title.removeDentalPlan'] = "<spring:message code='pd.label.title.removeDentalPlan' javaScriptEscape='true'/>";
  plan_display['pd.label.title.showCompare'] = "<spring:message code='pd.label.title.showCompare' javaScriptEscape='true'/>";
  plan_display['pd.label.title.hideCompare'] = "<spring:message code='pd.label.title.hideCompare' javaScriptEscape='true'/>";
  plan_display['pd.label.title.removeDental'] = "<spring:message code='pd.label.title.removeDental' javaScriptEscape='true'/>";  
  plan_display['pd.label.selectOneMember'] = "<spring:message code='pd.label.selectOneMember' javaScriptEscape='true'/>";
  plan_display['pd.label.adultDentalSelection'] = "<spring:message code='pd.label.adultDentalSelection' javaScriptEscape='true'/>";
  plan_display['pd.label.selectOnePlan'] = "<spring:message code='pd.label.selectOnePlan' javaScriptEscape='true'/>";
  plan_display['pd.label.dentalPlanAlreadyAdded'] = "<spring:message code='pd.label.dentalPlanAlreadyAdded' javaScriptEscape='true'/>";
  plan_display['pd.label.removeThisPlan'] = "<spring:message code='pd.label.removeThisPlan' javaScriptEscape='true'/>";
  
  plan_display['pd.label.selectPlanAlreadyInCart'] = "<spring:message code='pd.label.selectPlanAlreadyInCart' javaScriptEscape='true'/>";
  plan_display['pd.label.goToCart'] = "<spring:message code='pd.label.goToCart' javaScriptEscape='true'/>";
  plan_display['pd.label.title.addPlan'] = "<spring:message code='pd.label.title.addPlan' javaScriptEscape='true'/>";
</script>
<div class="preferecesPage">
<!-- <div class="topnav"> -->
<!-- 	<ul class="inline"> -->
<%-- 		<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Answer a few questions and we'll help you find a plan that fits."><spring:message code="pd.label.all.personalize"/></a></li> --%>
<%-- 		<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Shop and compare health plans."><spring:message code="pd.label.all.shop"/></a></li> --%>
<%-- 		<c:if test="${exchangeType == 'ON'}"> --%>
<%-- 			<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Finalize your eligibility for tax credit by submitting a Single Streamlined Application."><spring:message code="pd.label.all.apply"/></a></li> --%>
<%-- 		</c:if> --%>
<%-- 		<c:if test="${dentalPlansOnOff == 'ON'}"> --%>
<%-- 			<li><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Get dental coverage, if your health plan is missing it."><spring:message code="pd.label.all.enhance"/></a></li> --%>
<%-- 		</c:if> --%>
<%-- 		<c:if test="${exchangeType == 'ON'}"> --%>
<%-- 			<li class="active"><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Review your selections and make it official." class="last"><spring:message code="pd.label.all.checkout"/></a></li> --%>
<%-- 		</c:if> --%>
<%-- 		<c:if test="${exchangeType == 'OFF'}"> --%>
<%-- 			<li class="active"><a href="#" rel="tooltip" data-placement="bottom" data-original-title="Review your selections and start the application." class="last"><spring:message code="pd.label.all.checkout"/></a></li> --%>
<%-- 		</c:if> --%>
<!-- 	</ul> -->
<!-- </div>  -->

<div class="row-fluid" id="titlebar">
 	<h1 class="choose-plan">
 		<c:choose>
 			<c:when test="${isHealthPlanAvailable == 'Y'}">
 				<spring:message code="pd.label.renewal.title.1"/>
 			</c:when>
 			<c:otherwise>
 				<spring:message code="pd.label.renewal.title.2"/>
 			</c:otherwise>
 		</c:choose>
 	</h1>
</div>
  
<div class="row-fluid margin20-t">
	<div class="span10 offset1" id="rightpanel">
    	<input type="hidden" name="isZipCodeChanged" id="isZipCodeChanged" value="${isZipCodeChanged}"/>
    	
     	<div class="alert alert-info">
<%--         	<h4><spring:message code="pd.label.renewal.explanation"/></h4> --%>
<!--         	<ul> -->
<!--         		<li class="household"> -->
<%--         			<img src="<c:url value="/resources/img/household.png"/>"> --%>
<%--         			<c:choose> --%>
<%--           				<c:when test="${isHealthPlanAvailable == 'Y' }"> --%>
<%--           					<spring:message code="pd.label.renewal.explanationIcon.1"/> --%>
<%--           				</c:when> --%>
<%--           				<c:otherwise> --%>
<%--           					<spring:message code="pd.label.renewal.explanationIcon.2"/> --%>
<%--           				</c:otherwise> --%>
<%--           			</c:choose> --%>
<!--         		</li> -->
<!--         		<li class="zipcode"> -->
<%--         			<img src="<c:url value="/resources/img/zip.png"/>"> --%>
<%--         			<spring:message code="pd.label.renewal.explanationIcon.3"/> --%>
<!--         		</li> -->
<!--         	</ul> -->
<%--         	<c:choose> --%>
<%--         		<c:when test="${isHealthPlanAvailable == 'Y'}"> --%>
<%-- 					<h4><spring:message arguments="${explanationText1}, ${explanationText3}" code="pd.label.renewal.explanationText.1" htmlEscape="false"/></h4> --%>
<%-- 				</c:when> --%>
<%-- 				<c:otherwise> --%>
<%-- 					<h4><spring:message arguments="${explanationText1}, ${explanationText2}" code="pd.label.renewal.explanationText.2" htmlEscape="false"/></h4> --%>
<%-- 				</c:otherwise> --%>
<%-- 			</c:choose> --%>
			<c:choose>
        		<c:when test="${isHealthPlanAvailable == 'Y'}">
					<spring:message code="pd.label.title.message25"/>
				</c:when>
				<c:otherwise>
					<spring:message code="pd.label.title.message26"/> 
				</c:otherwise>
			</c:choose>
		</div>
		
		<div class="form-actions margin-top50 form-horizontal">
      	 	<div class="controls">
      	 		<c:choose><c:when test="${isHealthPlanAvailable == 'Y'}">
      	 			<a href="/hix/private/showcart?isPriceChanged=Y&postSSAPFlag=Y" class="btn btn-primary pull-right" title="<spring:message code='pd.label.renewal.button.1'/>" href="#"><spring:message code="pd.label.renewal.button.1"/><i class="icon-caret-right"></i></a>
      	 		</c:when>
				<c:otherwise>
                	<a href="/hix/private/preferences" class="btn btn-primary pull-right" title="<spring:message code='pd.label.renewal.button.1'/>" href="#"><spring:message code="pd.label.renewal.button.2"/><i class="icon-caret-right"></i></a>
                </c:otherwise></c:choose>
            </div>
            
        </div>
	</div>
</div>
</div>    
    
<script type="text/javascript">
 $(".comma").text(function(i, val) {
     return val.replace(/,/g, ", ");
 });
 
 $('body').tooltip({
     selector: '[rel=tooltip]'
 });
 
 
 $(document).ready(function(){
	 var isZipCodeChanged = $("#isZipCodeChanged").val();
	 if (isZipCodeChanged != 'Y'){
		 $('.household').removeClass('span6');
	 }
	 });
</script>

