package com.getinsured.eligibility.at.resp.si.dto;

import java.util.Date;
import java.util.Map;

public class PersonInfo{

	private String applicantIdentificationId;

	private String ssn;

	private String firstName;

	private String middleName;

	private String lastName;

	private boolean isIncarcerated;

	private boolean isLawfulPresenceAnswered;

	private Date dob;

	private Long primaryKey;

	private String roleOfPersonReference;

	private Map<String, Boolean> personVerificationsMap;	// to store SSAP_APPLICANT level verification indicators...

	private boolean matched;

	public String getApplicantIdentificationId() {
		return applicantIdentificationId;
	}

	public void setApplicantIdentificationId(String applicantIdentificationId) {
		this.applicantIdentificationId = applicantIdentificationId;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getRoleOfPersonReference() {
		return roleOfPersonReference;
	}

	public void setRoleOfPersonReference(String roleOfPersonReference) {
		this.roleOfPersonReference = roleOfPersonReference;
	}

	public Long getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(Long primaryKey) {
		this.primaryKey = primaryKey;
	}


	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

	public Map<String, Boolean> getPersonVerificationsMap() {
		return personVerificationsMap;
	}

	public void setPersonVerificationsMap(Map<String, Boolean> personVerificationsMap) {
		this.personVerificationsMap = personVerificationsMap;
	}

	public boolean isMatched() {
		return matched;
	}

	public void setMatched(boolean matched) {
		this.matched = matched;
	}

	public boolean isIncarcerated() {
		return isIncarcerated;
	}

	public void setIncarcerated(boolean isIncarcerated) {
		this.isIncarcerated = isIncarcerated;
	}

	public boolean isLawfulPresenceAnswered() {
		return isLawfulPresenceAnswered;
	}

	public void setLawfulPresenceAnswered(boolean isLawfulPresenceAnswered) {
		this.isLawfulPresenceAnswered = isLawfulPresenceAnswered;
	}

}

