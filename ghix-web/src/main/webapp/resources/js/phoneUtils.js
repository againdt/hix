/**
 * Phone Utility script.
 * @version 1.0
 * @since Aug 28,2013
 */

/**
 * Function to trim beginning and trailing spaces
 * 
 * @param rawStr The raw String
 * @returns {String} The processes String. 
 */
function trim(rawStr) {
        return rawStr.replace(/^\s+|\s+$/g,"");
}

/**
 * Function to format given phone number into '(XXX) XXX-XXXX' format.
 * 
 * @param rawNumber The raw phone number.
 * @returns The formatted phone number.
 */
function formatPhoneNumber(rawNumber) {
		var patt = /\d{1,3}-\d{1,3}-\d{1,4}/mg;
		
		if(patt.test(rawNumber)) {
			var arr = rawNumber.split("-");
		
			if(null != arr && "undefined" != arr && arr.length > 0) {
				rawNumber = "(" + arr[0] + ") " + arr[1] + "-" + arr[2];
			}
		}
		
		return rawNumber;
	}
	
/**
 * Function to format phone numbers into standard format.
 * 
 * @param phoneNumberIds The array of phone number identifiers to format.
 * @returns {Boolean} The status of operation.
 */
function formatAllPhoneNumbers(phoneNumberIds) {
	var vNumber, eleNumber;
	
	if(null != phoneNumberIds && "undefined" != phoneNumberIds) {
		if(typeof(phoneNumberIds) === "object" && null != phoneNumberIds.length && "undefined" != phoneNumberIds.length && phoneNumberIds.length > 0) {
			for(var i = 0; i < phoneNumberIds.length; i++) {	
				eleNumber = document.getElementById(phoneNumberIds[i]);
				
				if(null != eleNumber && "undefined" != eleNumber) {
					vNumber = eleNumber.innerHTML;
					
					if(null != vNumber && "undefined" != vNumber && "" != vNumber) {
						vNumber = formatPhoneNumber(trim(vNumber));
						
						eleNumber.innerHTML = vNumber;
					}
				}
			}
		}
	}
	
	return true;
}