package com.getinsured.hix.batch.externalnotices.reader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.ExternalNotice;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import  com.getinsured.hix.platform.repository.IExternalNoticeRepository;
import com.getinsured.hix.platform.util.DateUtil;

@Component("externalNoticesFailureReportReader")
@Scope("step")
public class ExternalNoticesFailureReportReader implements ItemReader<ExternalNotice> , ItemStream{
	
	private static final String BATCH_DATE_FORMAT = "yyyyMMddHHmmss";
	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalNoticesFailureReportReader.class);
	private String startDateStr;
	private String endDateStr;
	private BatchJobExecutionService batchJobExecutionService;
	private JdbcTemplate jdbcTemplate;
	private ResultSet resultSet;
	private Connection connection;
	private PreparedStatement statement;
	private Date startDate;
	private Date endDate;
	private SimpleJobOperator jobOperator;
	
	private static final String EXTERNAL_NOTICE_FAILURE_NOTICES_QUERY = "SELECT ID,SSAP_ID,NOTICE_ID,EXTERNAL_HOUSEHOLD_CASE_ID,STATUS,REQUEST_PAYLOAD,RESPONSE_PAYLOAD,"
			+ " CREATION_TIMESTAMP,LAST_UPDATE_TIMESTAMP, RETRY_COUNT"
			+ " FROM NOTICES_EXTERNAL WHERE STATUS ='FAILED' AND RETRY_COUNT >=4 AND LAST_UPDATE_TIMESTAMP BETWEEN ? AND ?";
	
	private IExternalNoticeRepository iExternalNoticesRepository;
	
	/**
	 * 
	 * @param stepExecution
	 * @throws Exception 
	 */
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) throws Exception{
		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob("externalNoticesFailureReportJob");
		if (batchExecutionList != null && batchExecutionList.size() > 1) {
			for (BatchJobExecution batchJobExecution : batchExecutionList) {
				jobOperator.stop(batchJobExecution.getId());
			}
		} else {
			LOGGER.info("Started Execution for externalNoticesFailureReportJob......");
			Date lastRunDate = getJOBLastRunDate("externalNoticesFailureReportJob");
			Calendar currentDate = Calendar.getInstance();
			this.startDate = DateUtil.removeTime(currentDate.getTime());
			this.endDate = new Date();
			startDate = (getStartDateStr() != null && DateUtil.isValidDate(getStartDateStr(), BATCH_DATE_FORMAT)) ? DateUtil.StringToDate(getStartDateStr(), BATCH_DATE_FORMAT)	: lastRunDate;
			endDate = (getEndDateStr() != null && DateUtil.isValidDate(getEndDateStr(), BATCH_DATE_FORMAT)) ? DateUtil.StringToDate(getEndDateStr(), BATCH_DATE_FORMAT) : getLastPosibleTimeOfTheDay(startDate);
		}
	}

	@Override
	public ExternalNotice read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		ExternalNoticeMapper externalNoticeMapper = new ExternalNoticeMapper();
		ExternalNotice externalNotice = null;
		if(this.resultSet.next()){
			externalNotice = externalNoticeMapper.mapRow(this.resultSet,0);
		}
		return externalNotice;
	}
	
	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		try {
			this.connection  =  jdbcTemplate.getDataSource().getConnection();
			this.connection.setAutoCommit(Boolean.FALSE);
			this.statement = connection.prepareStatement(EXTERNAL_NOTICE_FAILURE_NOTICES_QUERY);
			this.statement.setFetchSize(Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXTERNAL_NOTICE_COMMIT_INTERVAL)));
			this.statement.setTimestamp(1, new java.sql.Timestamp(this.startDate.getTime()));
			this.statement.setTimestamp(2, new java.sql.Timestamp(this.endDate.getTime()));
			this.resultSet = this.statement.executeQuery();	
		} catch (SQLException e) {
			throw new ItemStreamException(e.getMessage(),e);
		}
	}
	
	@Override
	public void update(ExecutionContext executionContext) throws ItemStreamException {
		// TODO Auto-generated method stub	
	}

	@Override
	public void close() throws ItemStreamException {
		if(this.statement != null){
			try{
				this.statement.close();
			}
			catch (SQLException e) {
				LOGGER.warn("externalNoticesFailureReportReader :: Exception "+e.getMessage()+" encountered while closing the statement, Ignoring");
			}
		}
		if(this.connection != null){
			try 
			{
				this.connection.close();
				LOGGER.info("externalNoticesFailureReportReader:: closing connection-------------->");
			} catch (SQLException e) {
				LOGGER.warn("externalNoticesFailureReportReader :: Exception "+e.getMessage()+" encountered while closing the connection, Ignoring");
			}
		}
		LOGGER.info("Closing provider read stream");		
	}

	public IExternalNoticeRepository getiExternalNoticesRepository() {
		return iExternalNoticesRepository;
	}

	public void setiExternalNoticesRepository(IExternalNoticeRepository iExternalNoticesRepository) {
		this.iExternalNoticesRepository = iExternalNoticesRepository;
	}

	public SimpleJobOperator getJobOperator() {
		return jobOperator;
	}

	public void setJobOperator(SimpleJobOperator jobOperator) {
		this.jobOperator = jobOperator;
	}
	
	public String getStartDateStr() {
		return startDateStr;
	}
	
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	
	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	/**
	 * @param jdbcTemplate the jdbcTemplate to set
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	private Date getLastPosibleTimeOfTheDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE,      calendar.getMaximum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND,      calendar.getMaximum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
		return calendar.getTime();
	}
	
	public Date getJOBLastRunDate(String jobName){
		Date lastRunDate=null;
		try{
			if(StringUtils.isNotEmpty(jobName)){
				lastRunDate= batchJobExecutionService.getLastJobRunDate(jobName, EnrollmentConstants.BATCH_JOB_STATUS);
			}

		}catch(Exception e){
			LOGGER.debug("Error in getJOBLastRunDate() for External Notices failure report Job ::"+e.getMessage(),e);
		}
		return lastRunDate;
	}
	
}
