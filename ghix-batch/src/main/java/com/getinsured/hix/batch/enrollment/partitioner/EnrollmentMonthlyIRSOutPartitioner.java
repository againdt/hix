package com.getinsured.hix.batch.enrollment.partitioner;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyIRSBatchService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentIRSOut;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;

@Component("enrollmentMonthlyIRSOutPartitioner")
@Scope("step")
public class EnrollmentMonthlyIRSOutPartitioner implements Partitioner {
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	private  String monthlyIRSOutboundCommitInterval;
	private EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService;
	private EnrollmentIRSOut enrollmentIRSOut;
	private String strMonth;
	private String strYear;
	private String regenBatchId;
	private String giAction;
	int applicableMonth;
	int applicableYear;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyIRSOutPartitioner.class);
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		List<BatchJobExecution> batchExecutionList = enrollmentMonthlyIRSBatchService.getRunningBatchList("monthlyIRSOutJob");
		if(batchExecutionList != null && batchExecutionList.size() == 1){
			Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
			ExecutionContext executionContext = null;

			try{
				boolean status = EnrollmentUtils.cleanDirectory(EnrollmentUtils
						.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.IRS.toString(),
								EnrollmentConstants.TRANSFER_DIRECTION_OUT)
						.append(File.separatorChar).append(EnrollmentConstants.WIP_FOLDER_NAME).toString());
				LOGGER.info("Folder cleaning status in IRS Individual partitioner is :: "+status);
				setApplicableMonthYear();
				if(stepExecution!=null){
					executionContext=stepExecution.getJobExecution().getExecutionContext();
					if(executionContext!=null){
						executionContext.put("month", applicableMonth);
						executionContext.put("year", applicableYear);
					}
				}
				enrollmentIRSOut.resetAllFields();
				List<String> houseHoldIdList = null;
				if(null != regenBatchId && !regenBatchId.isEmpty() && null != giAction && !giAction.isEmpty()){
					enrollmentIRSOut.setAction(giAction);
					enrollmentIRSOut.setOldBatchId(regenBatchId);
					houseHoldIdList = enrollmentMonthlyIRSBatchService.getUniqueHouseholdsForRegen(regenBatchId, giAction);
				}else{
					houseHoldIdList =enrollmentMonthlyIRSBatchService.getUniqueHouseHolds(applicableMonth,applicableYear);
				}
				if(houseHoldIdList!=null && !houseHoldIdList.isEmpty()&& houseHoldIdList.size()>0){
					enrollmentIRSOut.addAllToHouseholdCaseIds(houseHoldIdList);
					enrollmentIRSOut.setBatchId();
					int maxHouseholdsPerFile = 1;
					int size = houseHoldIdList.size();
					if(this.monthlyIRSOutboundCommitInterval !=null && !this.monthlyIRSOutboundCommitInterval.isEmpty()){
						maxHouseholdsPerFile=Integer.valueOf(this.monthlyIRSOutboundCommitInterval.trim());
					}
					int numberOfFiles = size / maxHouseholdsPerFile;
					if (size % maxHouseholdsPerFile != 0) {
						numberOfFiles++;
					}
					int firstIndex = 0;
					int lastIndex = 0;
					for (int i = 0; i < numberOfFiles; i++) {
						firstIndex = i * maxHouseholdsPerFile;
						lastIndex = (i + 1) * maxHouseholdsPerFile;
						if (lastIndex > size) {
							lastIndex = size;
						}

						ExecutionContext value = new ExecutionContext();
						value.putInt("startIndex", firstIndex);
						value.putInt("endIndex", lastIndex);
						value.putInt("partition",  i);
						value.putInt("month", applicableMonth);
						value.putInt("year", applicableYear);
						partitionMap.put("partition - " + i, value);
						LOGGER.debug("EnrollmentMonthlyIRSOutPartitioner : partition():: partition map entry end for partition :: "+i);

					}
				}

			}catch(Exception e){
				LOGGER.error("EnrollmentMonthlyIRSOutPartitioner failed to execute : "+ e.getMessage(), e);
				throw new RuntimeException("EnrollmentMonthlyIRSOutPartitioner failed to execute : " + e.getMessage()) ;
			}
			return partitionMap;
		}
		return null;
	}
	public String getMonthlyIRSOutboundCommitInterval() {
		return monthlyIRSOutboundCommitInterval;
	}
	public void setMonthlyIRSOutboundCommitInterval(
			String monthlyIRSOutboundCommitInterval) {
		this.monthlyIRSOutboundCommitInterval = monthlyIRSOutboundCommitInterval;
	}
	private void setApplicableMonthYear() throws GIException{
		int intMonth = 0;
		int intYear = 0;

		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		int currentMonth = calObj.get(Calendar.MONTH);
		if(NumberUtils.isNumber(strMonth) && NumberUtils.isNumber(strYear)){
			intMonth = Integer.parseInt(strMonth);
			intYear = Integer.parseInt(strYear);
			if(((intMonth>0 && intMonth<=12) || intMonth==-1) && intYear <= currentYear){
				applicableMonth = intMonth;
				applicableYear = intYear;
				if(intYear < currentYear && intMonth == -1){
					applicableMonth = 0;
					applicableYear = intYear+1;
				}else if(intMonth == -1){
					applicableMonth = currentMonth;
					applicableYear = intYear;
				}else{
					applicableMonth--;
				}


			}else{
				throw new GIException("Please provide valid month(mm) in the range [1,12] (Jan - Dec) and year <= current year");
			}
		}else{
			throw new GIException("Please provide valid month(mm) and year(yyyy)");
		}

	}



	public String getStrYear() {
		return strYear;
	}

	public void setStrYear(String strYear) {
		if ((null == strYear) || (strYear.equalsIgnoreCase("null")) || strYear.isEmpty()) {
			this.strYear = ""+Calendar.getInstance().get(Calendar.YEAR);
		} else {
			this.strYear = strYear;
		}
	}
	public String getStrMonth() {
		return strMonth;
	}

	public void setStrMonth(String strMonth) {
		if ((null == strMonth) || (strMonth.equalsIgnoreCase("null")) || strMonth.isEmpty()) {
			this.strMonth = "-1";
		} else {
			this.strMonth = strMonth;
		}
	}
	
	/**
	 * @return the regenBatchId
	 */
	public String getRegenBatchId() {
		return regenBatchId;
	}
	/**
	 * @param regenBatchId the regenBatchId to set
	 */
	public void setRegenBatchId(String regenBatchId) {
		this.regenBatchId = regenBatchId;
	}
	/**
	 * @return the giAction
	 */
	public String getGiAction() {
		return giAction;
	}
	/**
	 * @param giAction the giAction to set
	 */
	public void setGiAction(String giAction) {
		this.giAction = giAction;
	}
	public EnrollmentIRSOut getEnrollmentIRSOut() {
		return enrollmentIRSOut;
	}
	public void setEnrollmentIRSOut(EnrollmentIRSOut enrollmentIRSOut) {
		this.enrollmentIRSOut = enrollmentIRSOut;
	}
	public EnrollmentMonthlyIRSBatchService getEnrollmentMonthlyIRSBatchService() {
		return enrollmentMonthlyIRSBatchService;
	}
	public void setEnrollmentMonthlyIRSBatchService(EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService) {
		this.enrollmentMonthlyIRSBatchService = enrollmentMonthlyIRSBatchService;
	}

}
