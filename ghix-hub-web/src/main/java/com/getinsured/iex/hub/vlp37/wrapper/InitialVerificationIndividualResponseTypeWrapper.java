package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationIndividualResponseType;

/**
 * Wrapper Class for InitialVerificationIndividualResponseType
 * 
 * 
 */
public class InitialVerificationIndividualResponseTypeWrapper implements JSONAware{

	private InitialVerificationIndividualResponseType initVerifIndividualResponseType;
	
	private ResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;
	private InitialVerificationIndividualResponseSetTypeWrapper initVerifIndividualResponseSet;
	
	public InitialVerificationIndividualResponseTypeWrapper(InitialVerificationIndividualResponseType initVerifIndividualResponseType){
		
		if(initVerifIndividualResponseType == null){
			return;
		}
		
		ResponseMetadataWrapper responseMetadataWrapper = new ResponseMetadataWrapper(initVerifIndividualResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
		this.lawfulPresenceVerifiedCode = initVerifIndividualResponseType.getLawfulPresenceVerifiedCode();
		
		InitialVerificationIndividualResponseSetTypeWrapper initVerifIndividualResponseSetTypeWrapper = 
				new InitialVerificationIndividualResponseSetTypeWrapper(initVerifIndividualResponseType.getInitialVerificationIndividualResponseSet());
		
		this.initVerifIndividualResponseSet = initVerifIndividualResponseSetTypeWrapper;
	}

	public InitialVerificationIndividualResponseType getInitVerifIndividualResponseType() {
		return initVerifIndividualResponseType;
	}

	public void setInitVerifIndividualResponseType(
			InitialVerificationIndividualResponseType initVerifIndividualResponseType) {
		this.initVerifIndividualResponseType = initVerifIndividualResponseType;
	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	public InitialVerificationIndividualResponseSetTypeWrapper getInitVerifIndividualResponseSet() {
		return initVerifIndividualResponseSet;
	}

	public void setInitVerifIndividualResponseSet(
			InitialVerificationIndividualResponseSetTypeWrapper initVerifIndividualResponseSet) {
		this.initVerifIndividualResponseSet = initVerifIndividualResponseSet;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		if(responseMetadata != null)
			obj.put("ResponseMetadata",this.responseMetadata);
		
		if(lawfulPresenceVerifiedCode != null)
			obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		
		obj.put("InitialVerificationIndividualResponseSet",this.initVerifIndividualResponseSet);
		return obj.toJSONString();
	}

}
