(function() {
'use strict'; 
    angular.module('agencyApp')
    .controller('DocumentController', DocumentController);

    DocumentController.$inject = ['$http', '$scope', 'httpService', '$stateParams', '$state', '$window', 'fileUpload'];

    function DocumentController($http, $scope, httpService, $stateParams, $state, $window, fileUpload) {
        var vm = this;
        vm.nextPage = nextPage;
        vm.backPage = backPage;
        vm.agencyId = $("#agencyId").val();
        vm.certificationStatus = $scope.$parent.vm.agencyCertificationStatus;

        var data = {};
        $scope.$parent.vm.sideNavListOne[0].disableLink = false;
        $scope.$parent.vm.sideNavListOne[1].disableLink = false;
        $scope.$parent.vm.sideNavListOne[2].disableLink = false;
        $scope.$parent.vm.sideNavListOne[3].disableLink = false;
        $scope.$parent.vm.sideNavListOne[0].showOKicon = true;
        $scope.$parent.vm.sideNavListOne[1].showOKicon = true;
        $scope.$parent.vm.sideNavListOne[2].showOKicon = true;
        $scope.$parent.vm.sideNavListOne[3].showOKicon = true;
        $scope.$parent.vm.sideNavListOne[4].showOKicon = true;



        /* GET data for Agency Uploadrmation Page */
        var promise = httpService.apiGetRequest('/hix/agency/documents');
        promise.then(function(response){
            vm.data = response.data || [];
            $scope.$parent.vm.sideNavListOne[4].disableLink = false;
        })  
        .catch(function(error){
            console.log (error);
        });

        
        vm.uploadFile = function() {
            var file = vm.uploadedFile;
            var fields = [
                {'name': 'agencyId', 'data': vm.agencyId},
                {'name': 'documentType', 'data': vm.uploadedFile.type}
            ];
            var url = "/hix/agency/document/upload";           
            fileUpload.uploadFileToUrl('document', file, fields, url)
            .then(function(response){
                vm.data.push(response.data);
                 $('input.image-preview-filename').val('');
            })
            .catch(function(error){
                console.log (error);
            });
        };

        function backPage(){
            $window.location.href = "/hix/broker/buildprofile";
        };
       
        function nextPage(){

            var promise = httpService.apiGetRequest('/hix/agency/certification/view/'+ vm.agencyId);
            promise.then(function(response){
                if(response.status == '200'){
                    $scope.$parent.vm.sideNavListOne[5].disableLink = false;                    
                    $state.go("accsetup.certificationstatus", {agencyId: vm.agencyId, refreshFlag: true }); 
                }
            })  
            .catch(function(error){
                console.log (error);
            }); 
        }
        
    }

})();
 