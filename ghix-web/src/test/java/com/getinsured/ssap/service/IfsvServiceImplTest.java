package com.getinsured.ssap.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.codec.digest.HmacUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.enums.SsapApplicationTypeEnum;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Name;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.SocialSecurityCard;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.financial.Expense;
import com.getinsured.iex.ssap.financial.Income;
import com.getinsured.iex.ssap.financial.TaxHouseholdComposition;
import com.getinsured.iex.ssap.financial.type.ExpenseType;
import com.getinsured.iex.ssap.financial.type.Frequency;
import com.getinsured.iex.ssap.financial.type.IncomeSubType;
import com.getinsured.iex.ssap.financial.type.IncomeType;
import com.getinsured.iex.ssap.financial.type.TaxFilingStatus;
import com.getinsured.iex.ssap.financial.type.TaxRelationship;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.financial.IfsvPayload;
import com.getinsured.ssap.model.financial.IfsvRequest;
import com.getinsured.ssap.model.financial.IfsvResponse;
import com.getinsured.ssap.model.financial.IfsvTaxHousehold;
import com.getinsured.ssap.model.financial.IfsvVerification;
import com.getinsured.ssap.model.financial.TaxFilerCategory;
import com.getinsured.ssap.util.BloodRelationshipCode;
import com.getinsured.ssap.util.FamilyBuilder;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link IfsvServiceImpl} service.
 *
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
@RunWith(MockitoJUnitRunner.class)
public class IfsvServiceImplTest {
  private static final Logger log = LoggerFactory.getLogger(IfsvServiceImplTest.class);
  private static final String SUCCESS_RESPONSE_SAMPLE_JSON_FILE = "ifsv/success-response-sample.json";
  private static final String ERROR_RESPONSE_SAMPLE_JSON_FILE = "ifsv/error-response-sample.json";
  private static final String ERROR_RESPONSE_SAMPLE_JSON_FILE_EXTENDED = "ifsv/error-response-sample-2.json";
  private static final String TEST_SERVER_URL = "https://7ntk685owc.execute-api.us-gov-west-1.amazonaws.com/nv/ghix/";
  private static final String TEST_SERVER_API_KEY = "4c8g907556ce4f609fff13f57a7fdee8";
  private static final String TEST_SERVER_API_SECRET = "f1f0524f-15c1-4094-b76d-6b1fd1t7g39d";
  private static final String TEST_SERVER_CMS_PARTNER_ID = "SSHIX_NV2DEV";

  @Mock
  private IfsvServiceImpl ifsvService;

  @Mock
  private SsapApplicationRepository ssapApplicationRepository;

  @Mock
  private SsapApplicantRepository ssapApplicantRepository;

  @Mock
  private GhixRestTemplate ghixRestTemplate;

  @Mock
  private Properties configProp;

  private ObjectMapper objectMapper = new ObjectMapper();

  @Before
  public void setUp() throws Exception {

    objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

    final ClassPathResource sampleSuccessResponseResource = new ClassPathResource(SUCCESS_RESPONSE_SAMPLE_JSON_FILE);
    final ClassPathResource sampleErrorResponseResource = new ClassPathResource(ERROR_RESPONSE_SAMPLE_JSON_FILE);
    final ClassPathResource sampleErrorResponseResourceExtended = new ClassPathResource(ERROR_RESPONSE_SAMPLE_JSON_FILE_EXTENDED);

    when(ssapApplicationRepository.getOne(anyLong())).thenAnswer(invocation -> {
      return buildSampleSsapApplication(invocation.getArgument(0, Long.class));
    });

    when(configProp.getProperty("ghixHubIntegrationURL")).thenReturn(TEST_SERVER_URL);

    RestTemplate restTemplate = new RestTemplate();

    when(ghixRestTemplate.postForObject(anyString(), any(Class.class), any(Class.class))).thenAnswer(invocation -> {
      final String url = invocation.getArgument(0, String.class);

      Object requestObject = invocation.getArgument(1, Object.class);

      if(requestObject instanceof IfsvRequest) {
        IfsvRequest ifsvRequest = (IfsvRequest)requestObject;
        IfsvPayload ifsvPayload = ifsvRequest.getPayload();

        final String requestId  = getRequestId(ifsvPayload);
        ifsvPayload.setRequestId(requestId);

        if(ifsvRequest.getApplicationId() == 1) {
          final String sampleSuccessJson = JsonUtil.readFile(sampleSuccessResponseResource);
          log.info("ssap id is {}, returning sample success response from JSON: {}", ifsvRequest.getApplicationId(), sampleSuccessJson);
          IfsvResponse ifsvResponse = objectMapper.readValue(sampleSuccessJson, IfsvResponse.class);
          return ifsvResponse;
        } else if(ifsvRequest.getApplicationId() == 2) {
          final String sampleErrorJson = JsonUtil.readFile(sampleErrorResponseResource);
          log.info("ssap id is {}, returning sample error response from JSON: {}", ifsvRequest.getApplicationId(), sampleErrorJson);
          IfsvResponse ifsvResponse = objectMapper.readValue(sampleErrorJson, IfsvResponse.class);
          return ifsvResponse;
        } else if(ifsvRequest.getApplicationId() == 3) {
          final String sampleErrorExtendedJson = JsonUtil.readFile(sampleErrorResponseResourceExtended);
          log.info("ssap id is {}, returning sample extended error response from JSON: {}", ifsvRequest.getApplicationId(), sampleErrorExtendedJson);
          IfsvResponse ifsvResponse = objectMapper.readValue(sampleErrorExtendedJson, IfsvResponse.class);
          return ifsvResponse;
        }

        System.out.format("IfsvRequest: %s%n", objectMapper.writeValueAsString(ifsvRequest));
        HttpEntity<?> requestHttpEntity = new HttpEntity<>(ifsvRequest, getAuthHeaders());
        requestObject = requestHttpEntity;
      }

      /*
      try {
        String responseTest = restTemplate.postForObject(url, requestObject, String.class);
        System.out.format("Raw response: %s%n", responseTest);
      }
      catch(Exception e) {
        log.error("Exception occurred: {}", e.getMessage());
        log.error("Exception details", e);
      }*/
      final Class response = invocation.getArgument(2, Class.class);
      IfsvResponse ifsvResponse = null;
      try {
        restTemplate.postForObject(url, requestObject, response);
      }
      catch (Exception e) {
        ifsvResponse = new IfsvResponse();
        ifsvResponse.setErrorMessageDetail(IfsvServiceImpl.getIfsvErrorDetailsFromException(e));
        ifsvResponse.setIfsvVerification(IfsvVerification.ERROR);
      }

      return ifsvResponse;
    });

    ifsvService = new IfsvServiceImpl(ssapApplicationRepository, ssapApplicantRepository, ghixRestTemplate, configProp);
    ifsvService.postConstruct();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void invokeSuccess() throws Exception {
    IfsvResponse ifsvResponse = ifsvService.invoke(1L);

    System.out.format("Response %s%n", objectMapper.writeValueAsString(ifsvResponse));
    Assert.assertNotNull("IfsvResponse should not be null", ifsvResponse);
    Assert.assertFalse("IfsvResponse should not contain any errors", ifsvResponse.hasErrors());
  }

  @Test
  public void invokeError() throws Exception {
    IfsvResponse ifsvResponse = ifsvService.invoke(2L);

    Assert.assertNotNull("IfsvResponse should not be null", ifsvResponse);
    printIfsvResponseErrors(ifsvResponse);
    Assert.assertTrue("IfsvResponse should contain errors", ifsvResponse.hasErrors());
  }

  @Test
  public void invokeErrorWithExtendedDetails() throws Exception {
    IfsvResponse ifsvResponse = ifsvService.invoke(3L);

    Assert.assertNotNull("IfsvResponse should not be null", ifsvResponse);
    printIfsvResponseErrors(ifsvResponse);
    Assert.assertTrue("IfsvResponse should contain errors", ifsvResponse.hasErrors());
  }

  @Test
  public void invoke() throws Exception {
    IfsvResponse ifsvResponse = ifsvService.invoke(getSsapApplicationId());

    Assert.assertNotNull("IfsvResponse should not be null", ifsvResponse);
    printIfsvResponseErrors(ifsvResponse);
    Assert.assertFalse("IfsvResponse should not contain any errors", ifsvResponse.hasErrors());
    Assert.assertTrue("IFSV should be verified", ifsvResponse.getIfsvVerification() == IfsvVerification.VERIFIED);
  }

  private void printIfsvResponseErrors(IfsvResponse ifsvResponse) {
    if(ifsvResponse.hasErrors()) {
      if(ifsvResponse.getErrorMessageDetail() != null) {
        System.err.println("=== IFSV SERVICE ERROR DETAILS ===");
        ifsvResponse.getErrorMessageDetail().forEach(messageDetails -> {
          System.err.println("XPathContent: " + messageDetails.getxPathContent());
          System.err.println("Response Code: " + messageDetails.getResponseMetadata().getResponseCode());
          System.err.println("Description: " + messageDetails.getResponseMetadata().getResponseDescriptionText());
          System.err.println("TDS Description: " + messageDetails.getResponseMetadata().getTdsResponseDescriptionText());
        });
      } else if(ifsvResponse.getException() != null) {
        System.err.println("Exception Message: " + ifsvResponse.getException().getExceptionMessage());
        System.err.println("Exception Type: " + ifsvResponse.getException().getExceptionType());
        if(ifsvResponse.getException().getExceptionCause() != null) {
          System.err.println("Exception Cause Message: " + ifsvResponse.getException().getExceptionCause().getExceptionMessage());
          System.err.println("Exception Cause Type: " + ifsvResponse.getException().getExceptionCause().getExceptionType());
          if(ifsvResponse.getException().getExceptionCause().getExceptionCauseDetails() != null) {
            System.err.println("Exception Cause Details Type: " + ifsvResponse.getException().getExceptionCause().getExceptionCauseDetails().getExceptionType());
            System.err.println("Exception Cause Details Message: " + ifsvResponse.getException().getExceptionCause().getExceptionCauseDetails().getExceptionMessage());
            System.err.println("Exception Cause Details Cause: " + ifsvResponse.getException().getExceptionCause().getExceptionCauseDetails().getExceptionCause());
          }
        }
      }
    }
  }

  @Test
  public void getRequestForApplication() {
    SsapApplication ssapApplication = buildSampleSsapApplication(getSsapApplicationId());
    IfsvRequest ifsvRequest = ifsvService.getRequestForApplication(ssapApplication);

    Assert.assertNotNull("IFSV request was null", ifsvRequest);
    Assert.assertNotNull("IFSV Payload was null", ifsvRequest.getPayload());
  }

  private SsapApplication buildSampleSsapApplication(final long ssapApplicationId) {
    SsapApplication ssapApplication = new SsapApplication();

    ssapApplication.setId(ssapApplicationId);
    SingleStreamlinedApplication application = getSampleSingleStreamlinedApplication();
    JsonUtil.setApplicationData(ssapApplication, application);

    return ssapApplication;
  }

  private SingleStreamlinedApplication getSampleSingleStreamlinedApplication() {
    SingleStreamlinedApplication application = new SingleStreamlinedApplication();
    application.setCoverageYear(2019);
    application.setSsapApplicationId(String.valueOf(getSsapApplicationId()));
    application.setTaxHousehold(getSampleMomPopChildTaxHouseholds());
    application.setClientIp(getClientIp());
    application.setApplyingForFinancialAssistanceIndicator(true);
    application.setApplicationStatus(ApplicationStatus.SIGNED);
    application.setApplicationType(SsapApplicationTypeEnum.OE.name());
    return application;
  }

  private List<TaxHousehold> getSampleMomPopChildTaxHouseholds() {
    final List<TaxHousehold> taxHouseholds = new ArrayList<>();
    final TaxHousehold taxHousehold = new TaxHousehold();
    final List<HouseholdMember> householdMembers = new ArrayList<>();

    // PRIMARY
    HouseholdMember primary = new HouseholdMember();
    primary.setPersonId(1);
    primary.setAddressId(1);
    primary.setName(getName("Helen", "DeCosta"));
    primary.setSocialSecurityCard(getSocialSecurityCard("041906904"));
    primary.setIncomes(getIncomes(BloodRelationshipCode.SELF));
    primary.setExpenses(getExpenses(BloodRelationshipCode.SELF));
    primary.setDateOfBirth(new Date(80, 3, 25));

    List<BloodRelationship> primaryBloodRelationships = new ArrayList<>();
    BloodRelationship primarySelfBloodRelationship = new BloodRelationship();
    primarySelfBloodRelationship.setIndividualPersonId("1");
    primarySelfBloodRelationship.setRelatedPersonId("1");
    primarySelfBloodRelationship.setRelation(BloodRelationshipCode.SELF.getCode());

    BloodRelationship primaryToSpouseBloodRelationship = new BloodRelationship();
    primaryToSpouseBloodRelationship.setIndividualPersonId("1");
    primaryToSpouseBloodRelationship.setRelatedPersonId("2");
    primaryToSpouseBloodRelationship.setRelation(BloodRelationshipCode.SPOUSE.getCode());

    BloodRelationship primaryToDependentBloodRelationship = new BloodRelationship();
    primaryToDependentBloodRelationship.setIndividualPersonId("1");
    primaryToDependentBloodRelationship.setRelatedPersonId("3");
    primaryToDependentBloodRelationship.setRelation(BloodRelationshipCode.PARENT_OF_CHILD.getCode());

    primaryBloodRelationships.add(primarySelfBloodRelationship);
    primaryBloodRelationships.add(primaryToSpouseBloodRelationship);
    primaryBloodRelationships.add(primaryToDependentBloodRelationship);
    primary.setBloodRelationship(primaryBloodRelationships);

    TaxHouseholdComposition primaryTaxComposition = new TaxHouseholdComposition();
    primaryTaxComposition.setMarried(true);
    primaryTaxComposition.setTaxDependent(false);
    primaryTaxComposition.setLivesWithSpouse(true);
    primaryTaxComposition.setTaxFiler(true);
    primaryTaxComposition.setTaxFilingStatus(TaxFilingStatus.FILING_JOINTLY);
    primaryTaxComposition.setTaxRelationship(TaxRelationship.FILER);

    primary.setTaxHouseholdComposition(primaryTaxComposition);

    householdMembers.add(primary);

    // SPOUSE
    HouseholdMember spouse = new HouseholdMember();
    spouse.setPersonId(2);
    spouse.setAddressId(1);
    spouse.setName(getName("Wonderful", "Human","Being"));
    spouse.setSocialSecurityCard(getSocialSecurityCard("085522104"));
    spouse.setIncomes(getIncomes(BloodRelationshipCode.SPOUSE));
    spouse.setExpenses(getExpenses(BloodRelationshipCode.SPOUSE));
    spouse.setDateOfBirth(new Date(84, 1, 3));

    List<BloodRelationship> spouseBloodRelationships = new ArrayList<>();
    BloodRelationship spouseBloodRelationship = new BloodRelationship();
    spouseBloodRelationship.setIndividualPersonId("2");
    spouseBloodRelationship.setRelatedPersonId("2");
    spouseBloodRelationship.setRelation(BloodRelationshipCode.SELF.getCode());

    BloodRelationship spouseToPrimaryBloodRelationship = new BloodRelationship();
    spouseToPrimaryBloodRelationship.setIndividualPersonId("2");
    spouseToPrimaryBloodRelationship.setRelatedPersonId("1");
    spouseToPrimaryBloodRelationship.setRelation(BloodRelationshipCode.SPOUSE.getCode());

    BloodRelationship spouseToDependentBloodRelationship = new BloodRelationship();
    spouseToDependentBloodRelationship.setIndividualPersonId("1");
    spouseToDependentBloodRelationship.setRelatedPersonId("3");
    spouseToDependentBloodRelationship.setRelation(BloodRelationshipCode.PARENT_OF_CHILD.getCode());

    spouseBloodRelationships.add(spouseToPrimaryBloodRelationship);
    spouseBloodRelationships.add(spouseBloodRelationship);

    spouse.setBloodRelationship(spouseBloodRelationships);

    TaxHouseholdComposition spouseTaxComposition = new TaxHouseholdComposition();
    spouseTaxComposition.setMarried(true);
    spouseTaxComposition.setTaxDependent(false);
    spouseTaxComposition.setLivesWithSpouse(true);
    spouseTaxComposition.setTaxFiler(false);
    spouseTaxComposition.setTaxFilingStatus(TaxFilingStatus.FILING_JOINTLY);
    spouseTaxComposition.setTaxRelationship(TaxRelationship.DEPENDENT);
    Set<Integer> spouseClaimerIds = new HashSet<>();
    spouseClaimerIds.add(1);
    spouseTaxComposition.setClaimerIds(spouseClaimerIds);
    spouse.setTaxHouseholdComposition(spouseTaxComposition);

    householdMembers.add(spouse);

    // DEPENDENT
    HouseholdMember dependent = new HouseholdMember();
    dependent.setPersonId(3);
    dependent.setAddressId(1);
    dependent.setName(getName("John", "Jacob Cool", "Hiltop"));
    dependent.setSocialSecurityCard(getSocialSecurityCard("827777701"));
    dependent.setIncomes(getIncomes(BloodRelationshipCode.CHILD));
    dependent.setExpenses(getExpenses(BloodRelationshipCode.CHILD));
    dependent.setDateOfBirth(new Date(109, 9, 11));

    List<BloodRelationship> dependentBloodRelationships = new ArrayList<>();
    BloodRelationship dependentBloodRelationship = new BloodRelationship();
    dependentBloodRelationship.setIndividualPersonId("3");
    dependentBloodRelationship.setRelatedPersonId("3");
    dependentBloodRelationship.setRelation(BloodRelationshipCode.SELF.getCode());

    BloodRelationship dependentToPrimaryBloodRelationship = new BloodRelationship();
    dependentToPrimaryBloodRelationship.setIndividualPersonId("3");
    dependentToPrimaryBloodRelationship.setRelatedPersonId("1");
    dependentToPrimaryBloodRelationship.setRelation(BloodRelationshipCode.CHILD.getCode());

    BloodRelationship dependentToSpouseBloodRelationship = new BloodRelationship();
    dependentToSpouseBloodRelationship.setIndividualPersonId("3");
    dependentToSpouseBloodRelationship.setRelatedPersonId("2");
    dependentToSpouseBloodRelationship.setRelation(BloodRelationshipCode.CHILD.getCode());

    dependentBloodRelationships.add(dependentToPrimaryBloodRelationship);
    dependentBloodRelationships.add(dependentToSpouseBloodRelationship);
    dependentBloodRelationships.add(dependentBloodRelationship);

    dependent.setBloodRelationship(dependentBloodRelationships);

    TaxHouseholdComposition dependentTaxComposition = new TaxHouseholdComposition();
    dependentTaxComposition.setMarried(false);
    dependentTaxComposition.setTaxDependent(true);
    dependentTaxComposition.setLivesWithSpouse(false);
    dependentTaxComposition.setTaxFiler(false);
    dependentTaxComposition.setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);
    dependentTaxComposition.setTaxRelationship(TaxRelationship.DEPENDENT);
    dependent.setTaxHouseholdComposition(dependentTaxComposition);
    Set<Integer> dependentClaimerIds = new HashSet<>();
    dependentClaimerIds.add(1);
    dependentClaimerIds.add(2);
    dependentTaxComposition.setClaimerIds(dependentClaimerIds);

    householdMembers.add(dependent);

    // Finalize
    taxHousehold.setHouseholdMember(householdMembers);
    taxHouseholds.add(taxHousehold);

    // Income - Expenses: $160,915.21
    // https://docs.google.com/spreadsheets/d/1HEfNU0ziQe7PrshE1nK3BOx8diFexSYp/edit#gid=1799857799
    // Row: 65
    // Request ID: 827777701
    return taxHouseholds;
  }

  private List<Income> getIncomes(final BloodRelationshipCode bloodRelationshipCode) {
    List<Income> incomes = new ArrayList<>();

    switch(bloodRelationshipCode) {
      case SELF:
        // primary
        final Income jobIncome = new Income();
        jobIncome.setAmount(50_000_00L);
        jobIncome.setType(IncomeType.JOB);
        jobIncome.setFrequency(Frequency.YEARLY);
        jobIncome.setSourceName("GetInsured, Inc.");

        final Income gamblingIncome = new Income();
        gamblingIncome.setAmount(120_00L);
        gamblingIncome.setType(IncomeType.OTHER);
        gamblingIncome.setFrequency(Frequency.MONTHLY);
        gamblingIncome.setSubType(IncomeSubType.GAMBLING_PRIZE_AWARD);

        final Income investmentIncome = new Income();
        investmentIncome.setAmount(33_00L);
        investmentIncome.setType(IncomeType.INVESTMENT);
        investmentIncome.setFrequency(Frequency.DAILY);

        incomes.add(jobIncome);
        incomes.add(gamblingIncome);
        incomes.add(investmentIncome);
        // $60,020
        break;
      case SPOUSE:
        final Income spouseJobIncome = new Income();
        spouseJobIncome.setAmount(38_000_00L);
        spouseJobIncome.setType(IncomeType.JOB);
        spouseJobIncome.setFrequency(Frequency.YEARLY);
        spouseJobIncome.setSourceName("GetInsured, Inc.");

        final Income spouseAiAnIncome = new Income();
        spouseAiAnIncome.setAmount(1500_00L);
        spouseAiAnIncome.setFrequency(Frequency.BIWEEKLY);
        spouseAiAnIncome.setType(IncomeType.RENTAL_ROYALTY);
        spouseAiAnIncome.setSourceName("American Native Indian Tribe Member Distribution");

        final Income spouseSelfEmploymentIncome = new Income();
        spouseSelfEmploymentIncome.setAmount(18_00L);
        spouseSelfEmploymentIncome.setType(IncomeType.SELF_EMPLOYMENT);
        spouseSelfEmploymentIncome.setFrequency(Frequency.HOURLY);

        incomes.add(spouseJobIncome);
        incomes.add(spouseAiAnIncome);
        incomes.add(spouseSelfEmploymentIncome);
        // $114,440
        break;
      case CHILD:
        final Income childGameStreamingIncome = new Income();
        childGameStreamingIncome.setAmount(182_55L);
        childGameStreamingIncome.setFrequency(Frequency.BIMONTHLY);
        // $4,381.20 income
        childGameStreamingIncome.setSourceName("Twitch.tv");
        // Fast Internet connection required for streaming $120/month / 2
        childGameStreamingIncome.setRelatedExpense(60_00L);
        // $1,440.00 expense

        incomes.add(childGameStreamingIncome);
        // inc - exp: $2,941.20
        break;
    }

    // $177,401.20 total for members (inc - associated expenses)
    return incomes;
  }

  private List<Expense> getExpenses(final BloodRelationshipCode bloodRelationshipCode) {
    List<Expense> expenses = new ArrayList<>();

    switch(bloodRelationshipCode) {
      case SELF:
        // primary
        final Expense selfAlimonyExpense = new Expense();
        selfAlimonyExpense.setAmount(600_00L);
        selfAlimonyExpense.setFrequency(Frequency.MONTHLY);
        selfAlimonyExpense.setType(ExpenseType.ALIMONY);
        // $7,200

        final Expense selfStudentLoanInterestExpense = new Expense();
        selfStudentLoanInterestExpense.setAmount(120_00L);
        selfStudentLoanInterestExpense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
        selfStudentLoanInterestExpense.setFrequency(Frequency.MONTHLY);
        // $1,440

        expenses.add(selfAlimonyExpense);
        expenses.add(selfStudentLoanInterestExpense);
        // $8,640
        break;
      case SPOUSE:
        final Expense spouseOtherExpense = new Expense();
        spouseOtherExpense.setAmount(345_99L); // For out of state car transfer fee
        spouseOtherExpense.setType(ExpenseType.OTHER);
        spouseOtherExpense.setFrequency(Frequency.ONCE);

        final Expense spouseOtherExpense2 = new Expense();
        spouseOtherExpense2.setAmount(7500_00L); // For example one time donation to some charity
        spouseOtherExpense2.setFrequency(Frequency.ONCE);
        spouseOtherExpense2.setType(ExpenseType.OTHER);

        expenses.add(spouseOtherExpense);
        expenses.add(spouseOtherExpense2);
        // $7,845.99
        break;
      case CHILD:
        // Child has no expenses declared.
        break;
    }

    // $16,485.99
    return expenses;
  }

  private long getSsapApplicationId() {
    return 9999999L;
  }

  private Name getName(String firstName, String lastName) {
    Name n = new Name();
    n.setFirstName(firstName);
    n.setLastName(lastName);

    return n;
  }

  private Name getName(String firstName, String middleName, String lastName) {
    Name n = new Name();
    n.setFirstName(firstName);
    n.setMiddleName(middleName);
    n.setLastName(lastName);
    return n;
  }

  private SocialSecurityCard getSocialSecurityCard(String number) {
    SocialSecurityCard ssnCard = new SocialSecurityCard();
    ssnCard.setSocialSecurityNumber(number);

    return ssnCard;
  }

  private String getClientIp() {
    return "10.10.8.9";
  }

  /**
   * Returns Authorization headers that we need to order to talk to AWS.
   * @return {@link HttpHeaders} with Authorization.
   */
  private HttpHeaders getAuthHeaders() {
    final HttpHeaders headers = new HttpHeaders();
    final String authKey = TEST_SERVER_CMS_PARTNER_ID + ":" + HmacUtils.hmacSha256Hex(TEST_SERVER_API_SECRET.getBytes(), TEST_SERVER_API_KEY.getBytes());
    final String secretValue = Base64.getEncoder().encodeToString(authKey.getBytes(StandardCharsets.UTF_8));
    headers.set("Authorization", "Basic " + secretValue);
    return headers;
  }

  private String getRequestId(final IfsvPayload ifsvPayload) {

    final List<IfsvTaxHousehold> taxHousehold = ifsvPayload.getTaxHousehold();
    final IfsvTaxHousehold primaryHhm = taxHousehold.get(0);

    return requestIdForSsn(primaryHhm.getHouseholdMember().get(0).getSocialSecurityCard().getSocialSecurityNumber());
  }

  private String requestIdForSsn(final String ssn) {
    switch(ssn) {
      case "041906904":
        return "600004488";
      default:
        return "requestIdForSsn() Invalid SSN: " + ssn;
    }
  }

  @Test
  public void testTaxCategoryCode() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("3primary2nonprimary-ifsv-test.json");
    Assert.assertNotNull("Application cannot be null", application);

    final List<HouseholdMember> allMembers = application.getTaxHousehold().get(0).getHouseholdMember();

    // Primary tax filer is the first person, as old IFSV logic assumed:
    HouseholdMember primaryHouseholdMember = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold());

    // old should work when primary is the first person.
    for (HouseholdMember allMember : allMembers) {
      TaxFilerCategory oldTaxFilerCategory = getTaxFilerCategory(primaryHouseholdMember, allMember);
      Assert.assertNotNull("Tax Filer category should not be null", oldTaxFilerCategory);
      Assert.assertNotEquals("Tax Filer category should not be UNKNOWN", TaxFilerCategory.UNKNOWN, oldTaxFilerCategory);
    }

    // Change primary tax filer to be second person:
    application.getTaxHousehold().get(0).setPrimaryTaxFilerPersonId(2);
    primaryHouseholdMember = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold());

    for (HouseholdMember member : allMembers) {
      TaxFilerCategory oldTaxFilerCategory = getTaxFilerCategory(primaryHouseholdMember, member);
      Assert.assertNotNull("Tax Filer category should not be null", oldTaxFilerCategory);
      Assert.assertEquals("Tax Filer category should be UNKNOWN because it's using old logic", TaxFilerCategory.UNKNOWN, oldTaxFilerCategory);
    }

    // Verify that new logic doesn't give us UNKNOWN, even if primary is not the first person
    List<BloodRelationship> bloodRelationshipList = HouseholdUtil.getAllBloodRelationships(allMembers);

    for(HouseholdMember member: allMembers) {
      TaxFilerCategory taxFilerCategory = ifsvService.getTaxFilerCategory(bloodRelationshipList, primaryHouseholdMember, member);
      Assert.assertNotNull("Tax Filer category should not be null", taxFilerCategory);
      Assert.assertNotEquals("Tax Filer category should not be UNKNOWN", TaxFilerCategory.UNKNOWN, taxFilerCategory);
    }

    // Reset primary tax filer to be first person, and run new logic again
    application.getTaxHousehold().get(0).setPrimaryTaxFilerPersonId(1);
    primaryHouseholdMember = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold());
    bloodRelationshipList = HouseholdUtil.getAllBloodRelationships(allMembers);

    for(HouseholdMember member: allMembers) {
      TaxFilerCategory taxFilerCategory = ifsvService.getTaxFilerCategory(bloodRelationshipList, primaryHouseholdMember, member);
      Assert.assertNotNull("Tax Filer category should not be null", taxFilerCategory);
      Assert.assertNotEquals("Tax Filer category should not be UNKNOWN", TaxFilerCategory.UNKNOWN, taxFilerCategory);
    }

    // Mess-up blood relationships, add them to the third member instead of the correct first member
    application.getTaxHousehold().get(0).getHouseholdMember().get(2).setBloodRelationship(application.getTaxHousehold().get(0).getHouseholdMember().get(0).getBloodRelationship());
    application.getTaxHousehold().get(0).getHouseholdMember().get(0).setBloodRelationship(new ArrayList<>());

    bloodRelationshipList = HouseholdUtil.getAllBloodRelationships(allMembers);
    Assert.assertNotNull("Blood relationships moved to the incorrect member, we still should be able to fetch them", bloodRelationshipList);
    Assert.assertEquals("Blood relationships moved to the incorrect member, we still should get all of them", 25, bloodRelationshipList.size());
    primaryHouseholdMember = HouseholdUtil.getPrimaryTaxHouseholdMember(application.getTaxHousehold());

    for(HouseholdMember member: allMembers) {
      TaxFilerCategory taxFilerCategory = ifsvService.getTaxFilerCategory(bloodRelationshipList, primaryHouseholdMember, member);
      Assert.assertNotNull("Tax Filer category should not be null", taxFilerCategory);
      Assert.assertNotEquals("Tax Filer category should not be UNKNOWN", TaxFilerCategory.UNKNOWN, taxFilerCategory);
    }
  }

  /**
   * Old code from IfsvServiceImpl, storing here for negative testing. This will NOT give correct results
   */
  private TaxFilerCategory getTaxFilerCategory(final HouseholdMember member, HouseholdMember hhm) {
    List<BloodRelationship> bloodRelationshipList = member.getBloodRelationship();

    Optional<BloodRelationship> bloodRelationshipOptional =
      bloodRelationshipList.stream()
        .filter(br -> br != null &&
          br.getIndividualPersonId().equals(String.valueOf(hhm.getPersonId())) &&
          br.getRelatedPersonId().equals("1"))
        .findFirst();

    if (bloodRelationshipOptional.isPresent()) {
      BloodRelationshipCode code = BloodRelationshipCode.getBloodRelationForCode(bloodRelationshipOptional.get().getRelation());
      if (code == BloodRelationshipCode.SELF) {
        return TaxFilerCategory.PRIMARY;
      } else if (code == BloodRelationshipCode.SPOUSE) {
        return TaxFilerCategory.SPOUSE;
      } else {
        return TaxFilerCategory.DEPENDENT;
      }
    }
    log.error("Unable to determinate blood relationship for given household member, returning TaxFilerCategory.UNKNOWN");
    return TaxFilerCategory.UNKNOWN;
  }
}
