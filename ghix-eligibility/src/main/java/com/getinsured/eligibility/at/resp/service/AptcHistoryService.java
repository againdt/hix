package com.getinsured.eligibility.at.resp.service;

import java.math.BigDecimal;
import java.util.Date;

import com.getinsured.iex.ssap.model.AptcHistory;

public interface AptcHistoryService {

	boolean updateEffectiveDate(Date effectiveDate, Long ssapApplicationId);

	boolean updateCMRId(BigDecimal cmrHouseholdId, Integer ssapApplicationId);
	
	AptcHistory getLatestByApplicationId(Long ssapApplicationId);

}