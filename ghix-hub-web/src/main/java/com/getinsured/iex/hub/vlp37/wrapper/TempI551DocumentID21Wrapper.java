package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.TempI551DocumentID21Type;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;

/**
 * Wrapper Class for TempI551DocumentID21Type
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class TempI551DocumentID21Wrapper implements JSONAware {
	
private static final Logger LOGGER = LoggerFactory.getLogger(TempI551DocumentID21Wrapper.class);
	
	private ObjectFactory factory;
	private TempI551DocumentID21Type tempI551DocumentID21Type;
	
	private String alienNumber;
	private PassportCountryWrapper passportCountry;
	private String passportNumber;
	private String countryOfIssuance;
	private String expiryDate;

	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String PASSPORT_COUNTRY_KEY = "PassportCountry";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	
	public TempI551DocumentID21Wrapper(){
		this.factory = new ObjectFactory();
		this.tempI551DocumentID21Type = factory.createTempI551DocumentID21Type();
		this.passportCountry = new PassportCountryWrapper();
	}
	
	public void setAlienNumber(String alienNumber) throws HubServiceException{
		
		if(alienNumber == null){
			throw new HubServiceException("No Alien number provided");
		}
		
		this.alienNumber = alienNumber;
		this.tempI551DocumentID21Type.setAlienNumber(this.alienNumber);
	}
	
	public String getAlienNumber() {
		return alienNumber;
	}
	
	public void setPassportCountry(String passportCountry) throws HubServiceException{
		
		if(passportCountry == null){
			throw new HubServiceException("No Passport Country provided");
		}
		
		this.passportCountry = PassportCountryWrapper.fromJsonString(passportCountry);
		this.tempI551DocumentID21Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public void setPassportCountryFromWrapper(PassportCountryWrapper passportCountry) throws HubServiceException{
		
		if(passportCountry == null){
			throw new HubServiceException("No Passport Country provided");
		}
		
		this.passportCountry = passportCountry;
		this.tempI551DocumentID21Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public void setPassportNumber(String passportNumber) throws HubServiceException{
		
		if(passportNumber == null){
			throw new HubServiceException("No Passport number provided");
		}
		
		this.passportNumber = passportNumber;
		this.passportCountry.setPassportNumber(passportNumber);
		this.tempI551DocumentID21Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	
	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException{
		
		if(countryOfIssuance == null){
			throw new HubServiceException("No Country of Issuance provided");
		}
		
		this.countryOfIssuance = countryOfIssuance;
		this.passportCountry.setCountryOfIssuance(countryOfIssuance);
		this.tempI551DocumentID21Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}
	
	public PassportCountryWrapper getPassportCountry() {
		return passportCountry;
	}
	
	public void setExpiryDate(String expiryDate) {
		
		try{
			this.tempI551DocumentID21Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I551 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}
	
	public TempI551DocumentID21Type getTempI551DocumentID21Type(){
		return this.tempI551DocumentID21Type;
	}
	
	
	/**
	 * Converts the JSON String to TempI551DocumentID21Wrapper object
	 * 
	 * @param jsonString - String representation for TempI551DocumentID21Wrapper
	 * @return TempI551DocumentID21Wrapper Object
	 */
	public static TempI551DocumentID21Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create TempI551DocumentID21Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		TempI551DocumentID21Wrapper wrapper = new TempI551DocumentID21Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(ALIEN_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.alienNumber = (String)tmpObj;
			wrapper.tempI551DocumentID21Type.setAlienNumber(wrapper.alienNumber);
		}
		
		tmpObj = obj.get(PASSPORT_COUNTRY_KEY);
		if(tmpObj != null){
			JSONObject passportCountryObj = (JSONObject)tmpObj;
			wrapper.passportCountry = PassportCountryWrapper.fromJSONObject(passportCountryObj);
			wrapper.tempI551DocumentID21Type.setPassportCountry(wrapper.passportCountry.getPassportCountryType());
		}
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.tempI551DocumentID21Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating TempI551DocumentID21Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static TempI551DocumentID21Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		TempI551DocumentID21Wrapper wrapper = new TempI551DocumentID21Wrapper();
		wrapper.setAlienNumber((String) jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		if(jsonObj.get(PASSPORT_COUNTRY_KEY) != null){
			wrapper.setPassportCountryFromWrapper(PassportCountryWrapper.fromJSONObject((JSONObject)jsonObj.get(PASSPORT_COUNTRY_KEY)));
		}
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
}
