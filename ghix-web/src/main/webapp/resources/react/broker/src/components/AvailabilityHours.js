import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from '@cmsgov/design-system-core';
import { reduxForm, change, SubmissionError } from 'redux-form';
import { FormattedMessage } from 'react-intl';
import ReactTooltip from 'react-tooltip';
import _ from 'lodash';

import {
  fetchParticipationDetails,
  fetchConnectDetails,
  submitConnectDetails,
  submitAvailability,
  setAlert,
  removeAlert
} from '../actions/actionCreators';
import { defaultValuesObject, weekdays, days } from '../constants/DefaultData';
import {
  SELECT_CHOICE,
  BROKER_INACTIVE,
  CLOSED_NAME,
  CLOSED_VALUE,
  START,
  END
} from '../constants/AvailabilityConstants';
import Slider from './slider/Slider';
import TimeEntry from '../components/TimeEntry';
import ENDPOINTS from '../constants/endPointsList';
import { validateAvailabilitySubmit } from '../utils/validators';
import { buildAvailabilityHoursRequest } from '../utils/buildRequests';

const AvailabilityHours = props => {
  const { actions, active, available, availabilityError, data } = props;

  useEffect(() => {
    let brokerParticipation = document.getElementById('brkHelpOnDemand');
    let brokerAvailability = document.getElementById('brkConnect');
    brokerParticipation.classList.remove('active');
    brokerAvailability.classList.add('active');
    actions.fetchConnectDetails(ENDPOINTS.CONNECT_HOURS_ENDPOINT);
  }, []);

  const applyToWeekdays = event => {
    if (props.connectForm && props.connectForm.values) {
      _.forEach(weekdays, dayOfWeek => {
        if (props.connectForm.values.closed_Monday === true) {
          props.change(CLOSED_NAME + dayOfWeek, true);
          props.change(dayOfWeek + START, SELECT_CHOICE);
          props.change(dayOfWeek + END, SELECT_CHOICE);
        } else {
          props.change(CLOSED_NAME + dayOfWeek, false);
          props.change(dayOfWeek + START, props.connectForm.values.MondayStart);
          props.change(dayOfWeek + END, props.connectForm.values.MondayEnd);
        }
      });
    }
  };

  const getTimeEntries = disabled => {
    let timeEntryContent = [];
    timeEntryContent = _.map(days, (day, index) => {
      return (
        <TimeEntry
          key={index}
          dayOfWeek={day}
          change={props.change}
          closedText={
            <FormattedMessage
              id="broker.availability.closed.text"
              defaultMessage="Closed"
            />
          }
          disabled={disabled}
        />
      );
    });

    return timeEntryContent;
  };

  const validateSubmit = values => {
    //console.log(values);
    return validateAvailabilitySubmit(values);
  };

  const submitAvailability = available => {
    // Call action to submit availability
    let body = {
      available
    };

    actions.submitAvailability(ENDPOINTS.AVAILABILITY_ENDPOINT, body);
  };

  const onSubmit = values => {
    let errors = validateSubmit(values);
    if (errors && !_.isEmpty(errors)) {
      throw new SubmissionError(errors);
    }

    let body = buildAvailabilityHoursRequest(data.isAvailable, values);
    actions.submitConnectDetails(ENDPOINTS.CONNECT_HOURS_ENDPOINT, body);
  };

  return (
    <Fragment>
      <div className="ds-l-row header">
        <div className="ds-l-xl-col--10 ds-l-lg-col--10 ds-l-md-col--7 ds-l-sm-col--9 ds-l-xs-col ds-l-col--9">
          <h4>
            <FormattedMessage
              id="broker.availability.header"
              defaultMessage="Broker Connect Availability"
            />
          </h4>
        </div>
        <div className="ds-l-xl-col--2 ds-l-lg-col--2 ds-l-md-col--3 ds-l-sm-col--3 ds-l-col--3 ds-u-margin-left--auto">
          <div className="ds-l-row ds-u-padding-top--1">
            <div className="ds-l-col-9">
              <Slider
                id="brokerConnectAvailabilitySwitch"
                name="brokerConnectAvailabilitySwitch"
                onChange={submitAvailability}
                defaultSwitchValue={available}
                error={availabilityError}
                disabled={active === BROKER_INACTIVE}
              />
            </div>
            <div className="ds-l-col-3 ds-u-padding-x--1">
              <a data-tip data-for="broker_availability_tooltip">
                <i className="far fa-question-circle" />
              </a>
              <ReactTooltip
                place="bottom"
                type="dark"
                effect="solid"
                id="broker_availability_tooltip"
              >
                <span>
                  <FormattedMessage
                    id="broker.availability.slider.tooltip"
                    defaultMessage="Set your current availability"
                  />
                </span>
              </ReactTooltip>
            </div>
          </div>
        </div>
      </div>
      <div className="ds-l-row">
        <div className="ds-l-col">
          <p className="gutter10">
            <FormattedMessage
              id="broker.availability.info.heading1"
              defaultMessage="Please, select hours during which you are available to take the calls each day. For days when you don't plan to take any consumer calls, please select 'Closed'."
            />
          </p>
          <p className="gutter10">
            <FormattedMessage
              id="broker.availability.info.heading2"
              defaultMessage="On certain days when you are not available to take calls during your working hours, please use the button above to turn your availability OFF. Also if you would like to take calls beyond your working hours on certain days, you can turn the availability button ON to make yourself available for the calls."
            />
          </p>
          <p className="gutter10">
            <strong>
              <FormattedMessage
                id="broker.availability.info.timezoneNote"
                defaultMessage="Note: Please enter Pacific Time"
              />
            </strong>
          </p>
        </div>
      </div>
      <form onSubmit={props.handleSubmit(onSubmit)}>
        <div className="ds-l-xs-col--12 ds-l-sm-col--12 ds-l-md-col--12 ds-l-lg-col--2 ds-l-xl-col--2 ds-u-xs-float--none ds-u-sm-float--none ds-u-md-float--none ds-u-lg-float--right ds-u-xl-float--right">
          <div className="ds-l-row">
            <div className="ds-l-col gutter10">
              <Button
                className="nv-button-primary"
                variation="primary"
                size="small"
                id="applyToWeekdays"
                name="applyToWeekdays"
                onClick={applyToWeekdays}
                disabled={active === BROKER_INACTIVE}
              >
                <FormattedMessage
                  id="broker.availability.button.apply"
                  defaultMessage="Weekday Hours"
                />
              </Button>
            </div>
          </div>
          <div className="ds-l-row">
            <div className="ds-l-col">
              <p className="ds-u-font-size--small gutter10">
                <FormattedMessage
                  id="broker.availability.button.apply.label"
                  defaultMessage="Click the button above to apply Monday hours to all weekdays"
                />
              </p>
            </div>
          </div>
        </div>
        <div className="ds-l-row">
          <div className="ds-l-col--12 ds-l-sm-col--12 ds-l-md-col--12 ds-l-lg-col--12 ds-l-xl-col--12">
            {getTimeEntries(active === BROKER_INACTIVE)}
          </div>
          {/*<div className="ds-l-col--12 ds-l-sm-col--12 ds-l-md-col--12 ds-l-lg-col--2 ds-l-xl-col--2">*/}
          {/*  <div className="ds-l-row">*/}
          {/*    <div className="ds-l-col">*/}
          {/*      <Button*/}
          {/*        className="nv-button-primary"*/}
          {/*        variation="primary"*/}
          {/*        size="small"*/}
          {/*        id="applyToWeekdays"*/}
          {/*        name="applyToWeekdays"*/}
          {/*        onClick={applyToWeekdays}*/}
          {/*        disabled={active === BROKER_INACTIVE}*/}
          {/*      >*/}
          {/*        <FormattedMessage*/}
          {/*          id="broker.availability.button.apply"*/}
          {/*          defaultMessage="Weekday Hours"*/}
          {/*        />*/}
          {/*      </Button>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*  <div className="ds-l-row">*/}
          {/*    <div className="ds-l-col">*/}
          {/*      <p className="ds-u-font-size--small gutter10">*/}
          {/*        <FormattedMessage*/}
          {/*          id="broker.availability.button.apply.label"*/}
          {/*          defaultMessage="Click the button above to apply Monday hours to all weekdays"*/}
          {/*        />*/}
          {/*      </p>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</div>*/}
        </div>
        <div className="ds-l-row">
          <div className="ds-l-col">
            <div className="ds-l-row ds-u-justify-content--end">
              <Button
                className="ds-u-margin--1 nv-button-primary"
                variation="primary"
                id="save_availability"
                type="submit"
                disabled={active === BROKER_INACTIVE}
              >
                <FormattedMessage
                  id="broker.availability.button.save"
                  defaultMessage="Save Availability"
                />
              </Button>
            </div>
          </div>
        </div>
      </form>
    </Fragment>
  );
};

const validate = values => {
  console.log(values);
  return validateAvailabilitySubmit(values);
};

const mapStateToProps = state => {
  let availabilityData = state.brokerConnectReducer.brokerAvailabilities;
  let initialFormValues = defaultValuesObject;
  if (availabilityData && availabilityData.length > 0) {
    _.map(availabilityData, (availability, index) => {
      if (
        availability.fromTime !== CLOSED_VALUE &&
        availability.toTime !== CLOSED_VALUE
      ) {
        initialFormValues[availability.day + START] = availability.fromTime;
        initialFormValues[availability.day + END] = availability.toTime;
        initialFormValues[CLOSED_NAME + availability.day] = false;
      } else {
        initialFormValues[CLOSED_NAME + availability.day] = true;
      }
    });
  }

  let error = state.alert.errorType === 'error';
  return {
    data: state.brokerConnectReducer,
    available:
      state.brokerAvailabilityReducer.available ||
      state.brokerConnectReducer.isAvailable,
    initialValues: initialFormValues,
    active: state.brokerConnectReducer.status,
    connectForm: state.form.BrokerConnectHours,
    availabilityError: error
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      fetchParticipationDetails,
      fetchConnectDetails,
      submitConnectDetails,
      submitAvailability,
      setAlert,
      removeAlert,
      change
    },
    dispatch
  )
});

const enableReinitialize = true;

const formWrapped = reduxForm({
  form: 'BrokerConnectHours',
  validate,
  enableReinitialize
})(AvailabilityHours);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(formWrapped);
