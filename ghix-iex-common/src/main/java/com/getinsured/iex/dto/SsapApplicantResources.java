package com.getinsured.iex.dto;


import org.springframework.hateoas.Resources;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SsapApplicantResources extends Resources<SsapApplicantResource> { 
	
}


