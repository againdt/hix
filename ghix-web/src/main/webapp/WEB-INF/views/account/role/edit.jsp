<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<div class="row">
	<div class="span4">
		<h2>Edit Role</h2>
	</div>
	<div class="span12">
	<form class="form-stacked" id= "frmcreate" name="frmcreate" action="<c:url value="/account/role/edit" />" method="post">
	    <input type="hidden" name="id" id="id" value="${role.id}">
		<fieldset>
			<legend></legend>
				<p></p><br />
			<div class="profile">
				<div class="clearfix">
					<label for="role_name" class="required">Role Name</label>
					<div class="input">
						<input type="text" name="name" readonly="readonly" id="name" value="${role.name}" class="xlarge" size="30">
					</div>				          	
				</div>
				<div class="clearfix">
					<label for="description" class="required">Provide Role Description</label>
					<div class="input">
						<textarea name="description" id="description" class="xlarge" rows="2" cols="20" size="5">${role.description}</textarea>
					</div>				           
				</div>
				<div class="clearfix">
					<label for="landing_page" class="required">Landing Page</label>
					<span style="float:left;width:160px;">
						<br><%= request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()  %>
					</span>
						<div class="input">
							<input type="text" name="landingPage" id="landingPage" value="${role.landingPage}" class="xlarge" size="30">
						</div>				            
				</div>
			</div>
		</fieldset>
		<div class="actions">
			<input type="submit" name="submit" id="submit" value="Update Role" class="btn primary" title="Update Role">    		    
		</div>
	</form>
	
		<div class="clearfix">
		   <display:table pagesize="5" export="false" name="roleauditlist" sort="list" id="data" requestURI="${role.id}" defaultsort="5" defaultorder="descending">
		      <display:setProperty name="paging.banner.placement">bottom</display:setProperty>
	
			  <display:column property="RoleName" title="Role Name" sortable="true"/>
			  <display:column property="Desc" title="Role Description" sortable="true"/>
			  <display:column property="LandingPage" title="Landing Page" sortable="true"/>
			  <display:column property="Uname" title="User Name" sortable="true"/>
			  <display:column property="Updated" title="Updated On" sortable="true"/>
		  </display:table>
		</div>
	</div>
	<div class="notes" style="display:none">
		<div class="row">
			<div class="span">
		      <p>This information is required to determine initial eligibility to use the SHOP Exchange.  
		      All fields are subjected to a basic format validation of the input (e.g. a zip code must be a 
		      5 digit number). The question mark icon which opens a light-box provides helpful information. 
		      The EIN number can be validated with an external data source if an adequate web API is available.
		      </p>
		    </div>
	    </div>
	</div>			
</div>