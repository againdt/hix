package com.getinsured.hix.webservice.entity.service;

import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdRequest;
import com.getinsured.hix.webservice.entity.generateentityrecordid.GenerateEntityRecordIdResponse;

public interface GenerateEntityRecordIdService {

	GenerateEntityRecordIdResponse createAgencyAndAgentRecords(GenerateEntityRecordIdRequest request,
			GenerateEntityRecordIdResponse response) throws Exception;

	int generateBrokerRecordId(String licenseNo, String federalEIN, String businessLegalName, String email,
			String stateEIN, Long agencyId);

	boolean checkAgencyFederalTaxId(String federalTaxId);

}
