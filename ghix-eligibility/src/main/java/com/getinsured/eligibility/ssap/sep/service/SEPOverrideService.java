package com.getinsured.eligibility.ssap.sep.service;

import com.getinsured.hix.indportal.dto.SEPOverrideDTO;

/**
 * Service class for exposing SSAP related functionality for Individual portal
 * @author vishwanath_s
 *
 */
public interface SEPOverrideService { 

	public String saveOverrideSEPDetails(SEPOverrideDTO sepOverrideDTO) throws Exception;
}
