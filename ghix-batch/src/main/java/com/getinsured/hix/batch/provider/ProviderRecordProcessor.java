package com.getinsured.hix.batch.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.ehcache.EhCacheCacheManager;

public class ProviderRecordProcessor implements
		ItemProcessor<ProviderRecord, SolrInputDocument> {
	private static Logger logger = LoggerFactory
			.getLogger(ProviderRecordProcessor.class);
	@Autowired
	private EhCacheCacheManager cacheManager;
	private Cache solrCache = null;
	private static int providerIndex = 1;

	@Override
	public SolrInputDocument process(ProviderRecord fromReader)
			throws Exception {
		SolrInputDocument inputDoc;
		if (fromReader.getErrorCount() > 0) {
			throw new ProviderValidationException(fromReader.toString());
		}
		// validateForDuplicateAddress(fromReader);
		inputDoc = new SolrInputDocument();
		ArrayList<ProviderDataField> validFields = fromReader.getValidFields();
		Collection<String> multiValue = null;
		String name = null;
		boolean modified = false;
		for (ProviderDataField providerField : validFields) {
			if (providerField.isMultiValue()) {
				name = providerField.getMultiValueFieldName();
			} else {
				name = providerField.getName();
			}
			if (!providerField.getFieldMetaData().isSolrAware()) {
				logger.info("Skipping field:"+name+" SOLR is not aware of this field");
				continue;
			}
			if (providerField.isMultiValue()) {
				logger.info("Name "+name+" is a multi value field");
				multiValue = providerField.getProcessedValue();
				inputDoc.addField(name, multiValue);
			}else{
				if (providerField.isAnIdentityField()) {
					inputDoc.addField("id", providerField.getValue());
				} else {
					inputDoc.addField(name, providerField.getValue());
				}
			}
			if (providerField.getFieldMetaData().isGi_modified()) {
				inputDoc.addField("modified_fields", providerField.getName());
				modified = true;
			}
		}
		inputDoc.addField("gi_modified", modified);
		inputDoc.addField("column_definition",
				fromReader.getColumnDefinitionLine());
		inputDoc.addField("provider_record_data", fromReader.getRecordData());
		inputDoc.addField("provider_index", ProviderRecordProcessor.providerIndex);
		ProviderRecordProcessor.providerIndex++;
		logger.debug(inputDoc.toString());
		return inputDoc;
	}

	@SuppressWarnings("unchecked")
	public void validateForDuplicateAddress(ProviderRecord pr)
			throws ProviderValidationException, InvalidOperationException {
		if (this.solrCache == null) {
			solrCache = this.cacheManager.getCache("solrCache");
		}
		if (solrCache == null) {
			throw new InvalidOperationException(
					"failed to create cache for solr documents, please make sure you have cache named \"solrDocuments\" defined in you \"ehcache.xml\" file");
		}
		ValueWrapper cachedAddressWrapper = null;
		Collection<Object> previousAddresses = null;
		String currentAddress = null;
		String key = pr.getId();
		cachedAddressWrapper = (ValueWrapper) solrCache.get(key);
		if (cachedAddressWrapper != null) {
			previousAddresses = (Collection<Object>) cachedAddressWrapper.get();
		} else {
			previousAddresses = new ArrayList<Object>();
		}
		currentAddress = (String) pr.getField("address");
		logger.debug("ID:"
				+ key
				+ "Current Addresses:"
				+ currentAddress
				+ " Addresses from cache:"
				+ ((previousAddresses != null) ? previousAddresses.size() : "0"));
		if (previousAddresses != null && previousAddresses.size() > 0) {
			// Check duplicate addresses now
			Object tmpAddressObj = null;
			tmpAddressObj = this.checkForDuplicate(currentAddress,
					previousAddresses);
			if (tmpAddressObj == null) {
				throw new ProviderValidationException("[Duplicate Address]"
						+ pr.toString());
			}
			previousAddresses.add(currentAddress);
		}
		logger.debug("ID:" + key + " Populating cache with:" + currentAddress
				+ " Address");
		solrCache.put(key, previousAddresses);
		// Reset All
		cachedAddressWrapper = null;
		previousAddresses = null;
		currentAddress = null;
		key = null;
	}

	private Object checkForDuplicate(Object obj,
			Collection<Object> previousAddresses) {
		if (obj != null) {
			logger.debug("Comparison object class:" + obj.getClass().getName());
			String addressToCheck = (String) obj;
			addressToCheck = addressToCheck.replaceAll(" ", "").toLowerCase();
			String comparisonAddress = null;
			for (Object tmpObj : previousAddresses) {
				comparisonAddress = ((String) tmpObj).replaceAll(" ", "")
						.toLowerCase();
				if (addressToCheck.equals(comparisonAddress)) {
					logger.debug("Duplicate Address found:" + addressToCheck);
					return null;
				}
			}
			return obj;
		}
		return null;
	}

	public Cache getSolrCache() {
		return solrCache;
	}

	public void setSolrCache(Cache solrCache) {
		this.solrCache = solrCache;
	}

	public EhCacheCacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(EhCacheCacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

}
