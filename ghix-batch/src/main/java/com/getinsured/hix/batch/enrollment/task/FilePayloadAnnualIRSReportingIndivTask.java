
package com.getinsured.hix.batch.enrollment.task;

import java.util.Calendar;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsBatchService;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 * 
 */
public class FilePayloadAnnualIRSReportingIndivTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(FilePayloadAnnualIRSReportingIndivTask.class);
	private EnrollmentAnnualIrsBatchService enrollmentAnnualIrsBatchService;
	private String strYear;
	int applicableYear = 0;
	
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws GIException {
		try{
			LOGGER.info("FilePayloadAnnualIRSReportingIndivTask Packaging");
			setApplicableMonthYear();
			enrollmentAnnualIrsBatchService.generateManifestXML(applicableYear,"","",false,"","","","", false, true);
		}
		catch (Exception e) {
			LOGGER.info("FilePayloadAnnualIRSReportingIndivTask.execute : FAIL "+e.getMessage());
			throw new GIException("FilePayloadAnnualIRSReportingIndivTask.execute : FAIL "+ e.getMessage(),e);
		}
		return RepeatStatus.FINISHED;
	}

	public String getStrYear() {
		return strYear;
	}

	public void setStrYear(String strYear) {
		if ((null == strYear) || (strYear.equalsIgnoreCase("null")) || strYear.isEmpty()) {
			this.strYear = ""+Calendar.getInstance().get(Calendar.YEAR);
		} else {
			this.strYear = strYear;
		}
	}
	private void setApplicableMonthYear() throws GIException {
		int intYear = 0;
		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		if (NumberUtils.isNumber(strYear)) {
			intYear = Integer.parseInt(strYear);
			if (intYear > 2014 && intYear >= currentYear) {
				applicableYear = intYear;
			} else {
				throw new GIException("Please provide year (yyyy) <= current year");
			}
		} else {
			throw new GIException("Please provide valid year(yyyy)");
		}

	}

	public EnrollmentAnnualIrsBatchService getEnrollmentAnnualIrsBatchService() {
		return enrollmentAnnualIrsBatchService;
	}

	public void setEnrollmentAnnualIrsBatchService(
			EnrollmentAnnualIrsBatchService enrollmentAnnualIrsBatchService) {
		this.enrollmentAnnualIrsBatchService = enrollmentAnnualIrsBatchService;
	}
}
