import CONFIG from '../.env';
import { dataActionTypes } from '../constants/actionTypes';

export const mockData = {
    'singleStreamlinedApplication': {
        'coverageYear': 2019,
        'applicationType': 'QEP',
        // 'applicationType': 'OP',
        'acceptPrivacyIndicator': true,
        'ssapApplicationId': '4366059',
        'IP': null,
        'applicationGuid': '102353183',
        'applicationSignatureDate': null,
        'getHelpIndicator': true,
        'maxAchievedPage': 7,
        'helpType': 'NONE',
        'authorizedRepresentative': {
            'signatureisProofIndicator': true,
            'phone': [{'phoneExtension': '', 'phoneNumber': '4085208888', 'phoneType': 'CELL'}, {
                'phoneExtension': '1212',
                'phoneNumber': '4085208889',
                'phoneType': 'HOME'
            }, {'phoneExtension': '3232', 'phoneNumber': '4085208890', 'phoneType': 'WORK'}],
            'partOfOrganizationIndicator': true,
            'address': {
                'postalCode': '95136',
                'county': null,
                'primaryAddressCountyFipsCode': null,
                'streetAddress1': '4300 The Woods Drive',
                'streetAddress2': 'apt. 1808',
                'state': 'CA',
                'city': 'San Jose'
            },
            'name': {'middleName': 'Nickolas', 'lastName': 'Woodman', 'firstName': 'Jack', 'suffix': 'Sr.'},
            'emailAddress': 'jack.woodman@yopmail.com',
            'organizationId': '123456789',
            'companyName': 'FreeHelp'
        },
        'applyingForhouseHold': null,
        'applyingForFinancialAssistanceIndicator': true,
        'consentAgreement': null,
        'numberOfYearsAgreed': '',
        'authorizedRepresentativeIndicator': true,
        'applicationStartDate': '2018-11-12',
        'applicationDate': '2014-03-13',
        'taxFilerDependants': [],
        'isRidpVerified': true,
        'clientIp': '127.0.0.1',
        'primaryTaxFilerPersonId': 0,
        'broker': {
            'internalBrokerId': null,
            'brokerFederalTaxIdNumber': null,
            'brokerName': null,
            'brokerFirstName': null,
            'brokerLastName': null
        },
        'assister': {
            'assisterName': '',
            'assisterFirstName': '',
            'assisterLastName': '',
            'assisterID': '',
            'internalAssisterId': 0
        },
        'taxHousehold': [
            {
                'householdMember': [
                    {
                        'seeksQhp': true,
                        'applicantPersonType': 'PTF',
                        'flagBucket': {
                            'taxHouseholdCompositionFlags': 604587156,
                            // 'taxHouseholdCompositionFlags': 142918921, // PrimaryTaxFiler
                            'taxHouseholdCompositionProgressFlags': 21650,
                            // 'taxHouseholdCompositionProgressFlags': 19090, // PrimaryTaxFiler
                            'annualTaxIncomeFlags': 18
                        },
                        'taxHouseholdComposition': {
                            'married': null,
                            'taxFiler': null,
                            'claimingDependents': null,
                            'taxDependent': null,
                            'claimedOutsideHousehold': false,
                            'spouseMatch': null, // Only for UI
                            'livesWithSpouse': null,
                            'taxFilingStatus': 'UNSPECIFIED',
                            'taxRelationship': 'UNSPECIFIED',
                            'claimerIds': [ ]
                        },

                        // 'employerSponsoredCoverage': [],
                        parentCaretaker: null,
                        fullTimeStudent: null,
                        employerSponsoredCoverage: [
                            {
                                companyName: 'A',
                                personId: 1,
                                memberName: 'David John Blacksmith Jr.',
                                name: {
                                    firstName: '',
                                    middleName: '',
                                    lastName: '',
                                    suffix: ''
                                },
                                phone: '1111111111',
                                email: null,
                                address: {
                                    postalCode: '',
                                    county: null,
                                    streetAddress1: '',
                                    streetAddress2: '',
                                    state: '',
                                    city: ''
                                },
                                ein: '',
                                minimumValuePlan: false,
                                employerPremium: 0,
                                employerPremiumFrequency: 'UNSPECIFIED',
                                employerOfferAffordable: false
                            },
                            {
                                companyName: 'B',
                                personId: 2,
                                memberName: 'Sarah Jessica Blacksmith III',
                                name: {
                                    firstName: '',
                                    middleName: '',
                                    lastName: '',
                                    suffix: ''
                                },
                                phone: '2222222222',
                                email: null,
                                address: {
                                    postalCode: '',
                                    county: null,
                                    streetAddress1: '',
                                    streetAddress2: '',
                                    state: '',
                                    city: ''
                                },
                                ein: '',
                                minimumValuePlan: false,
                                employerPremium: 0,
                                employerPremiumFrequency: 'UNSPECIFIED',
                                employerOfferAffordable: false
                            }
                        ],
                        'healthCoverage': {
                            'chpInsurance': {'reasonInsuranceEndedOther': '', 'insuranceEndedDuringWaitingPeriodIndicator': false},
                            'haveBeenUninsuredInLast6MonthsIndicator': null,
                            'currentEmployer': [{
                                'employer': {
                                    'phone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'HOME'},
                                    'employerContactPersonName': '',
                                    'employerContactEmailAddress': ''
                                },
                                'currentEmployerInsurance': {
                                    'currentLowestCostSelfOnlyPlanName': '',
                                    'willBeEnrolledInEmployerPlanIndicator': false,
                                    'memberEmployerPlanStartDate': null,
                                    'willBeEnrolledInEmployerPlanDate': null,
                                    'comingPlanMeetsMinimumStandardIndicator': false,
                                    'memberPlansToDropEmployerPlanIndicator': true,
                                    'employerWillOfferPlanIndicator': true,
                                    'expectedChangesToEmployerCoverageIndicator': true,
                                    'employerCoverageEndingDate': null,
                                    'memberPlansToEnrollInEmployerPlanIndicator': true,
                                    'employerPlanStartDate': null,
                                    'memberEmployerPlanEndingDate': null,
                                    'comingLowestCostSelfOnlyPlanName': '',
                                    'currentPlanMeetsMinimumStandardIndicator': false,
                                    'employerWillNotOfferCoverageIndicator': false,
                                    'isCurrentlyEnrolledInEmployerPlanIndicator': false,
                                    'insuranceName': null,
                                    'policyNumber': null
                                }
                            }],
                            'currentlyHasHealthInsuranceIndicator': true,
                            'currentOtherInsurance': {
                                'otherStateOrFederalMedicaidProgramName': 'Medicaid',
                                'otherStateOrFederalCHIPProgramName':'Nevada Check Up',
                                'otherStateOrFederalPrograms': [
                                    {
                                        'name': 'CHIP',
                                        'type': 'CHIP',
                                        'eligible': true
                                    },
                                    {
                                        'name': 'COBRA Coverage',
                                        'type': 'COBRA_COVERAGE',
                                        'eligible': false
                                    },
                                    {
                                        'name': 'Medicaid',
                                        'type': 'MEDICAID',
                                        'eligible': false
                                    },
                                    {
                                        'name': 'Medicare',
                                        'type':'MEDICARE',
                                        'eligible': false,
                                    },
                                    {
                                        'name': 'Peace Corps',
                                        'type': 'PEACE_CORPS',
                                        'eligible': false
                                    },
                                    {
                                        'name': 'Retire Health Benefits',
                                        'type': 'RETIRE_HEALTH_BENEFITS',
                                        'eligible': false
                                    },
                                    {
                                        'name': 'TRICARE',
                                        'type': 'TRICARE',
                                        'eligible': false
                                    },
                                    {
                                        'name': 'Veterans Affairs (VA) Health Care Program',
                                        'type': 'VETERANS_AFFAIRS_HCP',
                                        'eligible': false
                                    },
                                    {
                                        'name': 'Other Coverage',
                                        'type': 'OTHER_COVERAGE',
                                        'eligible': true
                                    },
                                    {
                                        'name': 'None Of the Above',
                                        'type': 'NONE',
                                        'eligible': false
                                    }
                                ],
                                'currentlyEnrolledInVeteransProgram': false,
                                'hasNonESI': false
                            },
                            'otherInsurance': {
                                'insuranceName': 'kaiser',
                                'policyNumber': 'A12343545',
                                'limitedBenefit': true
                            },
                            'formerEmployer': [{
                                'phone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'CELL'},
                                'employerName': '',
                                'employerIdentificationNumber': '',
                                'employerContactPersonName': '',
                                'address': {'postalCode': '', 'county': null, 'primaryAddressCountyFipsCode': null, 'streetAddress1': '', 'streetAddress2': '', 'state': '', 'city': ''},
                                'employerContactPhone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'HOME'},
                                'employerContactEmailAddress': ''
                            }],
                            'primaryCarePhysicianName': '',
                            'otherInsuranceIndicator': true,
                            'currentlyEnrolledInCobraIndicator': null,
                            'employerWillOfferInsuranceIndicator': true,
                            'currentlyEnrolledInRetireePlanIndicator': null,
                            'currentlyEnrolledInVeteransProgramIndicator': null,
                            'hasPrimaryCarePhysicianIndicator': false,
                            'employerWillOfferInsuranceCoverageStartDate': null,
                            'willBeEnrolledInCobraIndicator': null,
                            'willBeEnrolledInVeteransProgramIndicator': null,
                            'medicaidInsurance': {'requestHelpPayingMedicalBillsLast3MonthsIndicator': false},
                            'isOfferedHealthCoverageThroughJobIndicator': null,
                            'willBeEnrolledInRetireePlanIndicator': null,
                            'employerWillOfferInsuranceStartDate': null,
                            'stateHealthBenefit': null,
                            'eligibleITU': null,
                            'receivedITU': null,
                            'unpaidBill': null,
                            'coveredDependentChild': true,
                            'absentParent': null
                        },
                        'annualTaxIncome': {
                            'reportedIncome': 0,
                            'projectedIncome': 0,
                            // 'variableIncome': null,
                            // 'unknownIncome': false,
                            // 'flags': 18
                        },
                        // Mock properties end here
                        'medicaidChipDenial': null,
                        'medicaidDeniedDate': null,
                        'medicaidDeniedDueToImmigration': null,
                        'changeInImmigrationSince5Year': null,
                        'fiveYearBar': false,
                        'changeInImmigrationSinceMedicaidDenial': null,
                        'blindOrDisabled': false,
                        'longTermCare': false,
                        'disabilityIndicator': false,
                        'hasESI': false,
                        'incomes': [
                            // {
                            //     type: 'RENTAL_ROYALTY',
                            //     amount: -10000,
                            //     frequency: 'MONTHLY',
                            //     sourceName: '',
                            //     relatedExpense: 0,
                            //     subType: 'UNSPECIFIED',
                            //     cyclesPerFrequency: 0,
                            //     tribalAmount: 0
                            // },
                            // {
                            //     type: 'OTHER',
                            //     amount: 1000,
                            //     frequency: 'WEEKLY',
                            //     sourceName: '',
                            //     relatedExpense: 0,
                            //     subType: 'OTHER_INCOME',
                            //     cyclesPerFrequency: 0,
                            //     tribalAmount: 100
                            // },
                            // {
                            //     type: 'JOB',
                            //     amount: 1200000,
                            //     frequency: 'MONTHLY',
                            //     sourceName: 'A',
                            //     relatedExpense: 0,
                            //     subType: 'UNSPECIFIED',
                            //     cyclesPerFrequency: 0,
                            //     tribalAmount: 6000
                            // },
                            // {
                            //     type: 'JOB',
                            //     amount: 560000,
                            //     frequency: 'ONCE',
                            //     sourceName: 'Amazon',
                            //     relatedExpense: 0,
                            //     subType: 'UNSPECIFIED',
                            //     cyclesPerFrequency: 0,
                            //     tribalAmount: 500000
                            // },
                            // {
                            //     type: 'JOB',
                            //     amount: 100000,
                            //     frequency: 'WEEKLY',
                            //     sourceName: 'Intel',
                            //     relatedExpense: 0,
                            //     subType: 'UNSPECIFIED',
                            //     cyclesPerFrequency: 0,
                            //     tribalAmount: 0
                            // },
                            // {
                            //     type: 'JOB',
                            //     amount: 15000,
                            //     frequency: 'DAILY',
                            //     sourceName: 'US Government',
                            //     relatedExpense: 0,
                            //     subType: 'UNSPECIFIED',
                            //     cyclesPerFrequency: 3,
                            //     tribalAmount: 50
                            // }
                        ],
                        'expenses': [
                            {
                                type: 'STUDENT_LOAN_INTEREST',
                                amount: 340000,
                                frequency: 'BIWEEKLY',
                                sourceName: '',
                                relatedExpense: 0,
                                subType: 'UNSPECIFIED'
                            },
                            {
                                type: 'ALIMONY',
                                amount: 110000,
                                frequency: 'MONTHLY',
                                sourceName: '',
                                relatedExpense: 0,
                                subType: 'UNSPECIFIED'
                            }
                        ],
                        'detailedIncome': {
                            'partnershipAndSwarpIncomeIncomeProof': '',
                            'capitalGains': {'annualNetCapitalGains': null},
                            'unemploymentBenefit': {'incomeFrequency': '', 'stateGovernmentName': '', 'benefitAmount': null, 'unemploymentDate': null},
                            'annuityIncomeProof': '',
                            'scholarshipIncomeProof': '',
                            'otherIncome': [
                                {'incomeFrequency': '', 'incomeAmount': null, 'otherIncomeTypeDescription': 'Canceled debts'},
                                {'incomeFrequency': '', 'incomeAmount': null, 'otherIncomeTypeDescription': 'Court Awards'},
                                {'incomeFrequency': '', 'incomeAmount': null, 'otherIncomeTypeDescription': 'Jury duty pay'}],
                            'receiveMAthrough1619': false,
                            'taxExemptedIncomeProof': '',
                            'wageIncomeProof': '',
                            'dividendsIncomeProof': '',
                            'farmFishingIncome': {
                                'incomeFrequency': '',
                                'netIncomeAmount': null
                            },
                            'rentalRoyaltyIncomeIndicator': null,
                            'unemploymentBenefitIndicator': null,
                            'capitalGainsIndicator': null,
                            'jobIncome': [
                                {
                                    'incomeFrequency': '',
                                    'employerName': '',
                                    'incomeAmountBeforeTaxes': null,
                                    'workHours': null
                                }
                            ],
                            'alimonyReceivedIndicator': null,
                            'unemploymentCompensationIncomeProof': '',
                            'selfEmploymentTax': {'incomeAmount': null},
                            'studentLoanInterest': {'incomeAmount': null},
                            'selfEmploymentIncome': {'monthlyNetIncome': null, 'typeOfWork': ''},
                            'businessIncomeProof': '',
                            'sellingBusinessProperty': {'incomeAmount': null},
                            'retirementOrPension': {'taxableAmount': null, 'incomeFrequency': ''},
                            'royaltiesIncomeProof': '',
                            'farmFishingIncomeIndictor': null,
                            'investmentIncome': {'incomeFrequency': '', 'incomeAmount': null},
                            'farmIncom': {'incomeAmount': null},
                            'deductions': {
                                'alimonyDeductionAmount': null,
                                'otherDeductionAmount': null,
                                'alimonyDeductionFrequency': '',
                                'studentLoanDeductionFrequency': '',
                                'deductionFrequency': 'MONTHLY',
                                'studentLoanDeductionAmount': null,
                                'deductionType': ['ALIMONY', 'STUDENT_LOAN_INTEREST', 'OTHER'],
                                'otherDeductionFrequency': '',
                                'deductionAmount': null
                            },
                            'rentalRoyaltyIncome': {'incomeFrequency': '', 'netIncomeAmount': null},
                            'receiveMAthroughSSI': false,
                            'foreignEarnedIncomeProof': '',
                            'socialSecurityBenefit': {'incomeFrequency': '', 'benefitAmount': null},
                            'foreignEarnedIncome': {'incomeAmount': null},
                            'childNaTribe': '',
                            'sellingBusinessPropertyIncomeProof': '',
                            'alimonyReceived': {'amountReceived': null, 'incomeFrequency': ''},
                            'selfEmploymentTaxProof': '',
                            'partnershipsCorporationsIncome': {'incomeAmount': null},
                            'studentLoanInterestProof': '',
                            'farmIncomeProof': '',
                            'retirementOrPensionIndicator': null,
                            'investmentIncomeIndicator': null,
                            'socialSecurityBenefitIndicator': null,
                            'rentalRealEstateIncome': {'incomeAmount': null},
                            'otherIncomeIndicator': null,
                            'pensionIncomeProof': '',
                            'socialSecurityBenefitProof': '',
                            'jobIncomeIndicator': null,
                            'businessIncome': {'incomeAmount': null},
                            'taxableInterestIncomeProof': '',
                            'selfEmploymentIncomeIndicator': null,
                            'rentalRealEstateIncomeProof': '',
                            'discrepancies': {
                                'hasWageOrSalaryBeenCutIndicator': null,
                                'explanationForJobIncomeDiscrepancy': '',
                                'didPersonEverWorkForEmployerIndicator': null,
                                'stopWorkingAtEmployerIndicator': null,
                                'hasHoursDecreasedWithEmployerIndicator': null,
                                'explanationForDependantDiscrepancy': '',
                                'seasonalWorkerIndicator': null,
                                'hasChangedJobsIndicator': false
                            }
                        },
                        'dateOfBirth': '1950-01-25',
                        //'dateOfBirth' : -300560400000,
                        // 'dateOfBirth': null,
                        // 'dateOfBirth': 'Mar 23, 2015 00:00:00 AM',
                        // 'dateOfBirth': '01/25/1999',
                        'parentCaretakerRelatives': {
                            'relationship': '',
                            'dateOfBirth': null,
                            'mainCaretakerOfChildIndicator': null,
                            'name': {'middleName': '', 'lastName': '', 'firstName': '', 'suffix': ''},
                            'liveWithAdoptiveParentsIndicator': true,
                            'personId': [],
                            'anotherChildIndicator': false
                        },
                        'specialCircumstances': {
                            'ageWhenLeftFosterCare': 19,
                            'everInFosterCareIndicator': false,
                            'metFosterCareConditions': false,
                            'fosterChild': false,
                            'numberBabiesExpectedInPregnancy': 0,
                            'pregnantIndicator': false,
                            'gettingHealthCareThroughStateMedicaidIndicator': true,
                            'fosterCareState': 'CA',
                            'tribalManualVerificationIndicator': true,
                            'americanIndianAlaskaNativeIndicator': true
                        },
                        'livesWithHouseholdContactIndicator': true,
                        'socialSecurityCard': {
                            'middleNameOnSSNCard': 'Johny',
                            'deathManualVerificationIndicator': false,
                            'socialSecurityNumber': '123456789',
                            'suffixOnSSNCard': 'IV',
                            'individualMandateExceptionIndicator': null,
                            'ssnManualVerificationIndicator': true,
                            'deathIndicatorAsAttested': false,
                            'nameSameOnSSNCardIndicator': true,
                            'deathIndicator': false,
                            'firstNameOnSSNCard': 'Davido',
                            'socialSecurityCardHolderIndicator': true,
                            'useSelfAttestedDeathIndicator': true,
                            'lastNameOnSSNCard': 'Blucksmith',
                            'reasonableExplanationForNoSSN': 'JUST_APPLIED'
                        },
                        'strikerIndicator': false,
                        'birthCertificateType': 'NO_CRETIFICATE',
                        'citizenshipImmigrationStatus': {
                            'honorablyDischargedOrActiveDutyMilitaryMemberIndicator': null,
                            'citizenshipStatusIndicator': false,
                            'eligibleImmigrationDocumentType': [
                                {
                                    'DS2019Indicator': false,
                                    'UnexpiredForeignPassportIndicator': false,
                                    'TemporaryI551StampIndicator': false,
                                    'I551Indicator': false,
                                    'MachineReadableVisaIndicator': false,
                                    'OtherDocumentTypeIndicator': false,
                                    'I94InPassportIndicator': false,
                                    'I571Indicator': false,
                                    'I766Indicator': false,
                                    'I20Indicator': false,
                                    'I94Indicator': false,
                                    'I797Indicator': false,
                                    'I327Indicator': false,
                                    'noneOfThese': false
                                }
                            ],
                            'naturalizationCertificateIndicator': null,
                            'citizenshipManualVerificationStatus': null,
                            'citizenshipAsAttestedIndicator': null,
                            'eligibleImmigrationStatusIndicator': null,
                            'citizenshipDocument': {
                                // 'SEVISId': '',
                                'foreignPassportOrDocumentNumber': null,
                                'sevisId': null,
                                'alienNumber': null,
                                'nameOnDocument': {
                                    'middleName': null,
                                    'lastName': null,
                                    'firstName': null,
                                    'suffix': null
                                },
                                'documentDescription': null,
                                'nameSameOnDocumentIndicator': null,
                                'visaNumber': null,
                                'documentExpirationDate': null,
                                'i94Number': null,
                                'foreignPassportCountryOfIssuance': null,
                                'cardNumber': null
                            },
                            'lawfulPresenceIndicator': true,
                            'naturalizationCertificateAlienNumber': '',
                            'eligibleImmigrationDocumentSelected': 'NaturalizationCertificate',
                            'otherImmigrationDocumentType': {
                                'cubanHaitianEntrantIndicator': false,
                                'orreligibilityLetterIndicator': false,
                                'stayOfRemovalIndicator': false,
                                'orrcertificationIndicator': false,
                                'americanSamoanIndicator': false,
                                'withholdingOfRemovalIndicator': false,
                                'domesticViolence': false,
                                'indianOfUsOrCanada': false,
                                'noneOfThese': false
                            },
                            'naturalizedCitizenshipIndicator': true,
                            'livedIntheUSSince1996Indicator': false,
                            'naturalizationCertificateNaturalizationNumber': ''
                        },
                        'medicalExpense': null,
                        'specialEnrollmentPeriod': {'birthOrAdoptionEventIndicator': false},
                        'marriedIndicator': null,
                        'bloodRelationship': [
                            {
                                individualPersonId: '1',
                                relatedPersonId: '1',
                                relation: '18',
                                textDependency: null
                            },
                            {
                                individualPersonId: '1',
                                relatedPersonId: '2',
                                relation: '01',
                                textDependency: null
                            },
                            {
                                individualPersonId: '2',
                                relatedPersonId: '1',
                                relation: '01',
                                textDependency: null
                            },
                            {
                                individualPersonId: '2',
                                relatedPersonId: '2',
                                relation: '18',
                                textDependency: null
                            },
                            {
                                individualPersonId: '1',
                                relatedPersonId: '3',
                                relation: '03',
                                textDependency: null
                            },
                            {
                                individualPersonId: '3',
                                relatedPersonId: '1',
                                relation: '19',
                                textDependency: null
                            },
                            {
                                individualPersonId: '2',
                                relatedPersonId: '3',
                                relation: '03',
                                textDependency: null
                            },
                            {
                                individualPersonId: '3',
                                relatedPersonId: '2',
                                relation: '19',
                                textDependency: null
                            },
                            {
                                individualPersonId: '3',
                                relatedPersonId: '3',
                                relation: '18',
                                textDependency: null
                            },
                            {
                                individualPersonId: '3',
                                relatedPersonId: '4',
                                relation: '14',
                                textDependency: null
                            },
                            {
                                individualPersonId: '1',
                                relatedPersonId: '4',
                                relation: '03',
                                textDependency: null
                            },
                            {
                                individualPersonId: '2',
                                relatedPersonId: '4',
                                relation: '03',
                                textDependency: null
                            },
                            {
                                individualPersonId: '4',
                                relatedPersonId: '1',
                                relation: '19',
                                textDependency: null
                            },
                            {
                                individualPersonId: '4',
                                relatedPersonId: '2',
                                relation: '19',
                                textDependency: null
                            },
                            {
                                individualPersonId: '4',
                                relatedPersonId: '3',
                                relation: '14',
                                textDependency: null
                            },
                            {
                                individualPersonId: '4',
                                relatedPersonId: '4',
                                relation: '18',
                                textDependency: null
                            }
                        ],
                        'drugFellowIndicator': false,
                        'livingArrangementIndicator': false,
                        'taxFiler': {
                            'spouseHouseholdMemberId': 0,
                            'claimingDependantsOnFTRIndicator': null,
                            'taxFilerDependants': [{'dependantHouseholdMemeberId': 0, 'taxFilerDependantIndicator': false}],
                            'liveWithSpouseIndicator': null
                        },
                        'shelterAndUtilityExpense': null,
                        'tobaccoUserIndicator': false,
                        'dependentCareExpense': null,
                        'residencyVerificationIndicator': false,
                        'taxFilerDependant': {'dependantHouseholdMemeberId': null, 'taxFilerDependantIndicator': null},
                        'name': {'middleName': 'John', 'lastName': 'Blacksmith', 'firstName': 'David', 'suffix': 'Jr.'},
                        'ethnicityAndRace': {
                            'ethnicity': [
                                {
                                    'label': 'Cuban',
                                    'code': '2182-4',
                                    'otherLabel': null
                                },
                                {
                                    'label': 'Mexican, Mexican American, or Chicano/a',
                                    'code': '2148-5',
                                    'otherLabel': null
                                },
                                {
                                    'label': 'Puerto Rican',
                                    'code': '2180-8',
                                    'otherLabel': null
                                },
                                {
                                    'label': 'Brasilian',
                                    'code': '0000-0',
                                    'otherLabel': null
                                }
                            ],
                            'race': [
                                {
                                    'label': 'American Indian or Alaska Native',
                                    'code': '1002-5',
                                    'otherLabel': null
                                },
                                {
                                    'label': 'Samoan',
                                    'code': '2080-0',
                                    'otherLabel': null
                                },
                                {
                                    'label': 'Vietnamese',
                                    'code': '2047-9',
                                    'otherLabel': null
                                },
                                {
                                    'label': 'Georgian',
                                    'code': '2131-1',
                                    'otherLabel': null
                                }
                            ],
                            'hispanicLatinoSpanishOriginIndicator': true
                        },
                        'personId': 1,
                        'addressId': 1,
                        'householdId' : 0,
                        'gender': 'male',
                        'SSIIndicator': false,
                        'IPVIndicator': false,
                        'householdContactIndicator': true,
                        'americanIndianAlaskaNative': {
                            'tribeName': 'Mohegan Tribe of Indians of Connecticut',
                            'tribeFullName': 'Village of Dot Lake',
                            'state': 'AK',
                            'stateFullName': 'Alaska',
                            'memberOfFederallyRecognizedTribeIndicator': true
                        },
                        'householdContact': {
                            'homeAddressIndicator': true,
                            'phone': {
                                'phoneExtension': '',
                                'phoneNumber': null,
                                'phoneType': 'CELL'
                            },
                            'homeAddress': {
                                'addressId': 1,
                                'postalCode': '83709',
                                'countyCode': 'Santa Clara',
                                'county': 'Ada',
                                'primaryAddressCountyFipsCode': '16001',
                                'streetAddress1': '5799 S Acheron Ave Apt 12',
                                'streetAddress2': 'apt. 12',
                                'state': 'CA',
                                'city': 'Boise'
                            },
                            'stateResidencyProof': '',
                            'otherPhone': {
                                'phoneExtension': '1223',
                                'phoneNumber': '4087995678',
                                'phoneType': 'HOME'
                            },
                            'contactPreferences': {
                                'emailAddress': null,
                                'preferredSpokenLanguage': 'English',
                                'preferredContactMethod': 'Email', // 'Email by default'
                                'preferredWrittenLanguage': null
                            },
                            'mailingAddress': {
                                'postalCode': '83709',
                                'countyCode': 'Santa Clara',
                                'county': 'Ada',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '5799 S Acheron Ave Apt 127777',
                                'streetAddress2': 'apt. 12',
                                'state': 'ID',
                                'city': 'Boise'
                            },
                            'mailingAddressSameAsHomeAddressIndicator': null
                        },
                        'PIDVerificationIndicator': false,
                        'applicantGuid': '1001002145',
                        'PIDIndicator': false,
                        'applyingForCoverageIndicator': true,
                        'planToFileFTRJointlyIndicator': null,
                        'livesAtOtherAddressIndicator': null,
                        'planToFileFTRIndicator': null,
                        'otherAddress': {
                            'address': {
                                'postalCode': '75123',
                                'county': '',
                                'countyCode': 'Santa Clara',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '',
                                'streetAddress2': '',
                                'state': 'CA',
                                'city': ''
                            },
                            'livingOutsideofStateTemporarilyAddress': {
                                'postalCode': '',
                                'county': '',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '',
                                'streetAddress2': '',
                                'state': '',
                                'city': ''
                            },
                            'livingOutsideofStateTemporarilyIndicator': null
                        },
                        'heatingCoolingindicator': false,
                        'incarcerationStatus': {
                            'incarcerationManualVerificationIndicator': false,
                            'incarcerationStatusIndicator': false,
                            'useSelfAttestedIncarceration': false,
                            'incarcerationPendingDispositionIndicator': false,
                            'incarcerationAsAttestedIndicator': false
                        },
                        'infantIndicator'   : false,
                        'childSupportExpense': null,
                        'expeditedIncome': {
                            'irsReportedAnnualIncome': null,
                            'expectedAnnualIncomeUnknownIndicator': null,
                            'irsIncomeManualVerificationIndicator': false,
                            'expectedAnnualIncome': null
                        },
                        'migrantFarmWorker': true,
                        'status': 'ADD_NEW',
                        'mediciadChipDenial': true,
                        'under26Indicator': true,
                        'externalId': null
                    },
                    {
                        'seeksQhp': true,
                        'applicantPersonType': 'PC_PTF',
                        'flagBucket': {
                            'taxHouseholdCompositionFlags': 604587154,
                            // 'taxHouseholdCompositionFlags': 142918921, // PrimaryTaxFiler
                            'taxHouseholdCompositionProgressFlags': 21650,
                            // 'taxHouseholdCompositionProgressFlags': 19602, // PrimaryTaxFiler
                            'annualTaxIncomeFlags': 18
                        },
                        'taxHouseholdComposition': {
                            'married': null,
                            'taxFiler': null,
                            'claimingDependents': null,
                            'taxDependent': null,
                            'claimedOutsideHousehold': false,
                            'spouseMatch': null,
                            'livesWithSpouse': null,
                            'taxFilingStatus': 'UNSPECIFIED',
                            'taxRelationship': 'UNSPECIFIED',
                            'claimerIds': [ ]
                        },
                        // Mock properties start here
                        parentCaretaker: null,
                        fullTimeStudent: null,
                        'employerSponsoredCoverage': [],
                        'agreeToUseIncomeDetails': false,
                        'annualTaxIncome': {
                            'reportedIncome': 0,
                            'projectedIncome': 0,
                            // 'flags': 18
                        },
                        // Mock properties end here
                        'medicaidChipDenial': null,
                        'medicaidDeniedDate': null,
                        'medicaidDeniedDueToImmigration': null,
                        'changeInImmigrationSince5Year': null,
                        'fiveYearBar': null,
                        'changeInImmigrationSinceMedicaidDenial': null,
                        'blindOrDisabled': false,
                        'longTermCare': false,
                        'disabilityIndicator': false,
                        'hasESI': false,
                        'incomes': [
                            // {
                            //     type: 'RENTAL_ROYALTY',
                            //     amount: 12045,
                            //     frequency: 'HOURLY',
                            //     sourceName: '',
                            //     profit: true
                            // }
                            // {
                            //     type: 'JOB',
                            //     amount: 9909,
                            //     frequency: 'WEEKLY',
                            //     sourceName: 'B',
                            //     relatedExpense: 0,
                            //     subType: 'OTHER_INCOME',
                            //     tribalAmount: 0
                            // },
                            // {
                            //     type: 'UNEMPLOYMENT',
                            //     amount: 9909,
                            //     frequency: 'WEEKLY',
                            //     sourceName: 'California',
                            //     relatedExpense: 0,
                            //     subType: 'OTHER_INCOME',
                            //     tribalAmount: 0
                            // },


                        ],
                        'expenses': [],
                        'detailedIncome': {
                            'partnershipAndSwarpIncomeIncomeProof': '',
                            'capitalGains': {'annualNetCapitalGains': null},
                            'unemploymentBenefit': {'incomeFrequency': '', 'stateGovernmentName': '', 'benefitAmount': null, 'unemploymentDate': null},
                            'annuityIncomeProof': '',
                            'scholarshipIncomeProof': '',
                            'otherIncome': [{'incomeFrequency': '', 'incomeAmount': null, 'otherIncomeTypeDescription': 'Canceled debts'}, {
                                'incomeFrequency': '',
                                'incomeAmount': null,
                                'otherIncomeTypeDescription': 'Court Awards'
                            }, {'incomeFrequency': '', 'incomeAmount': null, 'otherIncomeTypeDescription': 'Jury duty pay'}],
                            'receiveMAthrough1619': false,
                            'taxExemptedIncomeProof': '',
                            'wageIncomeProof': '',
                            'dividendsIncomeProof': '',
                            'farmFishingIncome': {'incomeFrequency': '', 'netIncomeAmount': null},
                            'rentalRoyaltyIncomeIndicator': null,
                            'unemploymentBenefitIndicator': null,
                            'capitalGainsIndicator': null,
                            'jobIncome': [{'incomeFrequency': '', 'employerName': '', 'incomeAmountBeforeTaxes': null, 'workHours': null}],
                            'alimonyReceivedIndicator': null,
                            'unemploymentCompensationIncomeProof': '',
                            'selfEmploymentTax': {'incomeAmount': null},
                            'studentLoanInterest': {'incomeAmount': null},
                            'selfEmploymentIncome': {'monthlyNetIncome': null, 'typeOfWork': ''},
                            'businessIncomeProof': '',
                            'sellingBusinessProperty': {'incomeAmount': null},
                            'retirementOrPension': {'taxableAmount': null, 'incomeFrequency': ''},
                            'royaltiesIncomeProof': '',
                            'farmFishingIncomeIndictor': null,
                            'investmentIncome': {'incomeFrequency': '', 'incomeAmount': null},
                            'farmIncom': {'incomeAmount': null},
                            'deductions': {
                                'alimonyDeductionAmount': null,
                                'otherDeductionAmount': null,
                                'alimonyDeductionFrequency': '',
                                'studentLoanDeductionFrequency': '',
                                'deductionFrequency': 'MONTHLY',
                                'studentLoanDeductionAmount': null,
                                'deductionType': ['ALIMONY', 'STUDENT_LOAN_INTEREST', 'OTHER'],
                                'otherDeductionFrequency': '',
                                'deductionAmount': null
                            },
                            'rentalRoyaltyIncome': {'incomeFrequency': '', 'netIncomeAmount': null},
                            'receiveMAthroughSSI': false,
                            'foreignEarnedIncomeProof': '',
                            'socialSecurityBenefit': {'incomeFrequency': '', 'benefitAmount': null},
                            'foreignEarnedIncome': {'incomeAmount': null},
                            'childNaTribe': '',
                            'sellingBusinessPropertyIncomeProof': '',
                            'alimonyReceived': {'amountReceived': null, 'incomeFrequency': ''},
                            'selfEmploymentTaxProof': '',
                            'partnershipsCorporationsIncome': {'incomeAmount': null},
                            'studentLoanInterestProof': '',
                            'farmIncomeProof': '',
                            'retirementOrPensionIndicator': null,
                            'investmentIncomeIndicator': null,
                            'socialSecurityBenefitIndicator': null,
                            'rentalRealEstateIncome': {'incomeAmount': null},
                            'otherIncomeIndicator': null,
                            'pensionIncomeProof': '',
                            'socialSecurityBenefitProof': '',
                            'jobIncomeIndicator': null,
                            'businessIncome': {'incomeAmount': null},
                            'taxableInterestIncomeProof': '',
                            'selfEmploymentIncomeIndicator': null,
                            'rentalRealEstateIncomeProof': '',
                            'discrepancies': {
                                'hasWageOrSalaryBeenCutIndicator': null,
                                'explanationForJobIncomeDiscrepancy': '',
                                'didPersonEverWorkForEmployerIndicator': null,
                                'stopWorkingAtEmployerIndicator': null,
                                'hasHoursDecreasedWithEmployerIndicator': null,
                                'explanationForDependantDiscrepancy': '',
                                'seasonalWorkerIndicator': null,
                                'hasChangedJobsIndicator': false
                            }
                        },
                        //'dateOfBirth': 275382000000,
                        // 'dateOfBirth': 1285225200000, // 09/23/2010
                        'dateOfBirth': '2000-09-23',
                        'parentCaretakerRelatives': {
                            'relationship': '',
                            'dateOfBirth': null,
                            'mainCaretakerOfChildIndicator': null,
                            'name': {'middleName': '', 'lastName': '', 'firstName': '', 'suffix': ''},
                            'liveWithAdoptiveParentsIndicator': true,
                            'personId': [],
                            'anotherChildIndicator': false
                        },
                        'specialCircumstances': {
                            'ageWhenLeftFosterCare': 4,
                            'everInFosterCareIndicator': false,
                            'metFosterCareConditions': false,
                            'fosterChild': false,
                            'numberBabiesExpectedInPregnancy': 7,
                            'pregnantIndicator': false,
                            'gettingHealthCareThroughStateMedicaidIndicator': true,
                            'fosterCareState': '',
                            'tribalManualVerificationIndicator': true,
                            'americanIndianAlaskaNativeIndicator': true
                        },
                        'livesWithHouseholdContactIndicator': true,
                        'socialSecurityCard': {
                            'middleNameOnSSNCard': '',
                            'deathManualVerificationIndicator': false,
                            'socialSecurityNumber': null,
                            'suffixOnSSNCard': '',
                            'individualMandateExceptionIndicator': null,
                            'ssnManualVerificationIndicator': true,
                            'deathIndicatorAsAttested': false,
                            'nameSameOnSSNCardIndicator': null,
                            'deathIndicator': false,
                            'firstNameOnSSNCard': '',
                            'socialSecurityCardHolderIndicator': false,
                            'useSelfAttestedDeathIndicator': true,
                            'lastNameOnSSNCard': '',
                            'reasonableExplanationForNoSSN': 'RELIGIOUS_EXCEPTION'
                        },
                        'strikerIndicator': false,
                        'birthCertificateType': 'NO_CRETIFICATE',
                        'citizenshipImmigrationStatus': {
                            'honorablyDischargedOrActiveDutyMilitaryMemberIndicator': null,
                            'citizenshipStatusIndicator': false,
                            'eligibleImmigrationDocumentType': [{
                                'DS2019Indicator': false,
                                'UnexpiredForeignPassportIndicator': false,
                                'TemporaryI551StampIndicator': false,
                                'I551Indicator': false,
                                'MachineReadableVisaIndicator': false,
                                'OtherDocumentTypeIndicator': false,
                                'I94InPassportIndicator': false,
                                'I571Indicator': false,
                                'I766Indicator': false,
                                'I20Indicator': false,
                                'I94Indicator': false,
                                'I797Indicator': false,
                                'I327Indicator': false,
                                'noneOfThese': false
                            }],
                            'naturalizationCertificateIndicator': null,
                            'citizenshipManualVerificationStatus': null,
                            'citizenshipAsAttestedIndicator': false,
                            'eligibleImmigrationStatusIndicator': false,
                            'citizenshipDocument': {
                                // 'SEVISId': '00463733773',
                                'foreignPassportOrDocumentNumber': '',
                                'sevisId': '',
                                'alienNumber': '3673736',
                                'nameOnDocument': {'middleName': '', 'lastName': '', 'firstName': '', 'suffix': ''},
                                'documentDescription': '',
                                'nameSameOnDocumentIndicator': null,
                                'visaNumber': '',
                                'documentExpirationDate': null,
                                'i94Number': '',
                                'foreignPassportCountryOfIssuance': '0',
                                'cardNumber': ''
                            },
                            'lawfulPresenceIndicator': true,
                            'naturalizationCertificateAlienNumber': '',
                            'eligibleImmigrationDocumentSelected': 'NaturalizationCertificate',
                            'otherImmigrationDocumentType': {
                                'cubanHaitianEntrantIndicator': false,
                                'orreligibilityLetterIndicator': false,
                                'stayOfRemovalIndicator': false,
                                'orrcertificationIndicator': false,
                                'americanSamoanIndicator': false,
                                'withholdingOfRemovalIndicator': false,
                                'domesticViolence': false,
                                'indianOfUsOrCanada': false,
                                'noneOfThese': false
                            },
                            'naturalizedCitizenshipIndicator': true,
                            'livedIntheUSSince1996Indicator': false,
                            'naturalizationCertificateNaturalizationNumber': ''
                        },
                        'medicalExpense': null,
                        'specialEnrollmentPeriod': {'birthOrAdoptionEventIndicator': false},
                        'marriedIndicator': null,
                        'bloodRelationship': [],
                        'drugFellowIndicator': false,
                        'livingArrangementIndicator': false,
                        'taxFiler': {
                            'spouseHouseholdMemberId': 0,
                            'claimingDependantsOnFTRIndicator': null,
                            'taxFilerDependants': [{'dependantHouseholdMemeberId': 0, 'taxFilerDependantIndicator': false}],
                            'liveWithSpouseIndicator': null
                        },
                        'shelterAndUtilityExpense': null,
                        'tobaccoUserIndicator': false,
                        'dependentCareExpense': null,
                        'residencyVerificationIndicator': false,
                        'taxFilerDependant': {'dependantHouseholdMemeberId': null, 'taxFilerDependantIndicator': null},
                        'name': {'middleName': 'Jessica', 'lastName': 'Blacksmith', 'firstName': 'Sarah', 'suffix': 'III'},
                        'ethnicityAndRace': {
                            'ethnicity': [],
                            'race': [{'label': 'American Indian or Alaska Native', 'code': '1002-5', 'otherLabel': null}, {
                                'label': 'Asian Indian',
                                'code': '2029-7',
                                'otherLabel': null
                            }, {'label': 'Black or African American', 'code': '2054-5', 'otherLabel': null}, {'label': 'Other Pacific Islander', 'code': '2500-7', 'otherLabel': null}],
                            'hispanicLatinoSpanishOriginIndicator': false
                        },
                        'personId': 2,
                        'addressId': 1,
                        'householdId' : 0,
                        'gender': 'female',
                        'SSIIndicator': false,
                        'IPVIndicator': false,
                        'householdContactIndicator': true,
                        'americanIndianAlaskaNative': {
                            tribeName: 'AL010001',
                            tribeFullName: '',
                            state: 'AL',
                            stateFullName: '',
                            memberOfFederallyRecognizedTribeIndicator: true
                            // 'memberOfFederallyRecognizedTribeIndicator': false // Temporarily for Tribal income testing
                        },
                        'householdContact': {
                            'homeAddressIndicator': false,
                            'phone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'CELL'},
                            'homeAddress': {
                                'postalCode': '',
                                'countyCode': null,
                                'county': '',
                                'primaryAddressCountyFipsCode': '',
                                'streetAddress1': '',
                                'streetAddress2': '',
                                'state': '',
                                'city': ''
                            },
                            'stateResidencyProof': '',
                            'otherPhone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'HOME'},
                            'contactPreferences': {'emailAddress': '', 'preferredSpokenLanguage': '', 'preferredContactMethod': '', 'preferredWrittenLanguage': ''},
                            'mailingAddress': {
                                'postalCode': '83709',
                                'countyCode': null,
                                'county': 'Ada',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '5799 S Acheron Ave Apt 12',
                                'streetAddress2': 'apt. 12',
                                'state': 'ID',
                                'city': 'Boise'
                            },
                            'mailingAddressSameAsHomeAddressIndicator': true
                        },
                        'PIDVerificationIndicator': false,
                        'applicantGuid': '1001016686',
                        'PIDIndicator': false,
                        'applyingForCoverageIndicator': true,
                        'planToFileFTRJontlyIndicator': null,
                        'livesAtOtherAddressIndicator': null,
                        'healthCoverage': {
                            'chpInsurance': {'reasonInsuranceEndedOther': '', 'insuranceEndedDuringWaitingPeriodIndicator': false},
                            'haveBeenUninsuredInLast6MonthsIndicator': null,
                            'currentEmployer': [{
                                'employer': {
                                    'phone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'HOME'},
                                    'employerContactPersonName': '',
                                    'employerContactEmailAddress': ''
                                },
                                'currentEmployerInsurance': {
                                    'currentLowestCostSelfOnlyPlanName': '',
                                    'willBeEnrolledInEmployerPlanIndicator': false,
                                    'memberEmployerPlanStartDate': null,
                                    'willBeEnrolledInEmployerPlanDate': null,
                                    'comingPlanMeetsMinimumStandardIndicator': false,
                                    'memberPlansToDropEmployerPlanIndicator': true,
                                    'employerWillOfferPlanIndicator': true,
                                    'expectedChangesToEmployerCoverageIndicator': true,
                                    'employerCoverageEndingDate': null,
                                    'memberPlansToEnrollInEmployerPlanIndicator': true,
                                    'employerPlanStartDate': null,
                                    'memberEmployerPlanEndingDate': null,
                                    'comingLowestCostSelfOnlyPlanName': '',
                                    'currentPlanMeetsMinimumStandardIndicator': false,
                                    'employerWillNotOfferCoverageIndicator': false,
                                    'isCurrentlyEnrolledInEmployerPlanIndicator': false,
                                    'insuranceName': null,
                                    'policyNumber': null
                                }
                            }],
                            'currentlyHasHealthInsuranceIndicator': false,
                            'currentOtherInsurance': {
                                'otherStateOrFederalMedicaidProgramName': 'Medicaid',
                                'otherStateOrFederalCHIPProgramName':'Nevada Check Up',
                                'otherStateOrFederalPrograms': [],
                                'currentlyEnrolledInVeteransProgram': false,
                                'hasNonESI': false
                            },
                            'otherInsurance': {
                                'insuranceName': '',
                                'policyNumber': '',
                                'limitedBenefit': false
                            },
                            'formerEmployer': [{
                                'phone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'CELL'},
                                'employerName': '',
                                'employerIdentificationNumber': '',
                                'employerContactPersonName': '',
                                'address': {'postalCode': '', 'county': null, 'primaryAddressCountyFipsCode': null, 'streetAddress1': '', 'streetAddress2': '', 'state': '', 'city': ''},
                                'employerContactPhone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'HOME'},
                                'employerContactEmailAddress': ''
                            }],
                            'primaryCarePhysicianName': '',
                            'otherInsuranceIndicator': true,
                            'currentlyEnrolledInCobraIndicator': null,
                            'employerWillOfferInsuranceIndicator': null,
                            'currentlyEnrolledInRetireePlanIndicator': null,
                            'currentlyEnrolledInVeteransProgramIndicator': null,
                            'hasPrimaryCarePhysicianIndicator': false,
                            'employerWillOfferInsuranceCoverageStartDate': null,
                            'willBeEnrolledInCobraIndicator': null,
                            'willBeEnrolledInVeteransProgramIndicator': null,
                            'medicaidInsurance': {'requestHelpPayingMedicalBillsLast3MonthsIndicator': false},
                            'isOfferedHealthCoverageThroughJobIndicator': null,
                            'willBeEnrolledInRetireePlanIndicator': null,
                            'employerWillOfferInsuranceStartDate': null,
                            'stateHealthBenefit': null,
                            'eligibleITU': null,
                            'receivedITU': null,
                            'unpaidBill': null,
                            'coveredDependentChild': null,
                            'absentParent': null
                        },
                        'planToFileFTRIndicator': null,
                        'otherAddress': {
                            'address': {
                                'postalCode': '95136',
                                'county': '06085',
                                'countyCode': 'Santa Clara',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '4300 The Woods Drive',
                                'streetAddress2': 'Apt. 1808',
                                'state': 'NV',
                                'city': 'San Jose'
                            },
                            'livingOutsideofStateTemporarilyAddress': {
                                'postalCode': '94188',
                                'county': '',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '',
                                'streetAddress2': '',
                                'state': 'CA',
                                'city': 'Sunnyvale'
                            },
                            'livingOutsideofStateTemporarilyIndicator': true
                        },
                        'heatingCoolingindicator': false,
                        'incarcerationStatus': {
                            'incarcerationManualVerificationIndicator': false,
                            'incarcerationStatusIndicator': false,
                            'useSelfAttestedIncarceration': false,
                            'incarcerationPendingDispositionIndicator': false,
                            'incarcerationAsAttestedIndicator': false
                        },
                        'infantIndicator': false,
                        'childSupportExpense': null,
                        'expeditedIncome': {
                            'irsReportedAnnualIncome': null,
                            'expectedAnnualIncomeUnknownIndicator': null,
                            'irsIncomeManualVerificationIndicator': false,
                            'expectedAnnualIncome': null
                        },
                        'migrantFarmWorker': true,
                        'status': 'ADD_NEW',
                        'mediciadChipDenial': false,
                        'under26Indicator': true,
                        'externalId': null
                    },
                    {
                        'seeksQhp': true,
                        'applicantPersonType': 'PTF',
                        'flagBucket': {
                            'taxHouseholdCompositionFlags': 604587154,
                            // 'taxHouseholdCompositionFlags': 168379660, // PrimaryTaxFiler
                            'taxHouseholdCompositionProgressFlags': 21650,
                            // 'taxHouseholdCompositionProgressFlags': 19090, // PrimaryTaxFiler
                            'annualTaxIncomeFlags': 18
                        },
                        'employerSponsoredCoverage': [],
                        'taxHouseholdComposition': {
                            'married': null,
                            'taxFiler': null,
                            'claimingDependents': null,
                            'taxDependent': null,
                            'claimedOutsideHousehold': false,
                            'spouseMatch': null,
                            'livesWithSpouse': null,
                            'taxFilingStatus': 'UNSPECIFIED',
                            'taxRelationship': 'UNSPECIFIED',
                            'claimerIds': [ ]
                        },
                        // Mock properties start here
                        parentCaretaker: null,
                        fullTimeStudent: null,
                        'annualTaxIncome': {
                            'reportedIncome': 0,
                            'projectedIncome': 0,
                            // 'flags': 18
                        },
                        // Mock properties end here
                        'medicaidChipDenial': null,
                        'medicaidDeniedDate': null,
                        'medicaidDeniedDueToImmigration': null,
                        'changeInImmigrationSince5Year': null,
                        'fiveYearBar': null,
                        'changeInImmigrationSinceMedicaidDenial': null,
                        'blindOrDisabled': false,
                        'longTermCare': false,
                        'disabilityIndicator': false,
                        'hasESI': false,
                        'incomes': [],
                        'expenses': [],
                        'detailedIncome': {
                            'partnershipAndSwarpIncomeIncomeProof': '',
                            'capitalGains': {'annualNetCapitalGains': null},
                            'unemploymentBenefit': {'incomeFrequency': '', 'stateGovernmentName': '', 'benefitAmount': null, 'unemploymentDate': null},
                            'annuityIncomeProof': '',
                            'scholarshipIncomeProof': '',
                            'otherIncome': [{'incomeFrequency': '', 'incomeAmount': null, 'otherIncomeTypeDescription': 'Canceled debts'}, {
                                'incomeFrequency': '',
                                'incomeAmount': null,
                                'otherIncomeTypeDescription': 'Court Awards'
                            }, {'incomeFrequency': '', 'incomeAmount': null, 'otherIncomeTypeDescription': 'Jury duty pay'}],
                            'receiveMAthrough1619': false,
                            'taxExemptedIncomeProof': '',
                            'wageIncomeProof': '',
                            'dividendsIncomeProof': '',
                            'farmFishingIncome': {'incomeFrequency': '', 'netIncomeAmount': null},
                            'rentalRoyaltyIncomeIndicator': null,
                            'unemploymentBenefitIndicator': null,
                            'capitalGainsIndicator': null,
                            'jobIncome': [{'incomeFrequency': '', 'employerName': '', 'incomeAmountBeforeTaxes': null, 'workHours': null}],
                            'alimonyReceivedIndicator': null,
                            'unemploymentCompensationIncomeProof': '',
                            'selfEmploymentTax': {'incomeAmount': null},
                            'studentLoanInterest': {'incomeAmount': null},
                            'selfEmploymentIncome': {'monthlyNetIncome': null, 'typeOfWork': ''},
                            'businessIncomeProof': '',
                            'sellingBusinessProperty': {'incomeAmount': null},
                            'retirementOrPension': {'taxableAmount': null, 'incomeFrequency': ''},
                            'royaltiesIncomeProof': '',
                            'farmFishingIncomeIndictor': null,
                            'investmentIncome': {'incomeFrequency': '', 'incomeAmount': null},
                            'farmIncom': {'incomeAmount': null},
                            'deductions': {
                                'alimonyDeductionAmount': null,
                                'otherDeductionAmount': null,
                                'alimonyDeductionFrequency': '',
                                'studentLoanDeductionFrequency': '',
                                'deductionFrequency': 'MONTHLY',
                                'studentLoanDeductionAmount': null,
                                'deductionType': ['ALIMONY', 'STUDENT_LOAN_INTEREST', 'OTHER'],
                                'otherDeductionFrequency': '',
                                'deductionAmount': null
                            },
                            'rentalRoyaltyIncome': {'incomeFrequency': '', 'netIncomeAmount': null},
                            'receiveMAthroughSSI': false,
                            'foreignEarnedIncomeProof': '',
                            'socialSecurityBenefit': {'incomeFrequency': '', 'benefitAmount': null},
                            'foreignEarnedIncome': {'incomeAmount': null},
                            'childNaTribe': '',
                            'sellingBusinessPropertyIncomeProof': '',
                            'alimonyReceived': {'amountReceived': null, 'incomeFrequency': ''},
                            'selfEmploymentTaxProof': '',
                            'partnershipsCorporationsIncome': {'incomeAmount': null},
                            'studentLoanInterestProof': '',
                            'farmIncomeProof': '',
                            'retirementOrPensionIndicator': null,
                            'investmentIncomeIndicator': null,
                            'socialSecurityBenefitIndicator': null,
                            'rentalRealEstateIncome': {'incomeAmount': null},
                            'otherIncomeIndicator': null,
                            'pensionIncomeProof': '',
                            'socialSecurityBenefitProof': '',
                            'jobIncomeIndicator': null,
                            'businessIncome': {'incomeAmount': null},
                            'taxableInterestIncomeProof': '',
                            'selfEmploymentIncomeIndicator': null,
                            'rentalRealEstateIncomeProof': '',
                            'discrepancies': {
                                'hasWageOrSalaryBeenCutIndicator': null,
                                'explanationForJobIncomeDiscrepancy': '',
                                'didPersonEverWorkForEmployerIndicator': null,
                                'stopWorkingAtEmployerIndicator': null,
                                'hasHoursDecreasedWithEmployerIndicator': null,
                                'explanationForDependantDiscrepancy': '',
                                'seasonalWorkerIndicator': null,
                                'hasChangedJobsIndicator': false
                            }
                        },
                        'dateOfBirth': 1285225200000,
                        //'dateOfBirth': 333442800000,
                        'parentCaretakerRelatives': {
                            'relationship': '',
                            'dateOfBirth': null,
                            'mainCaretakerOfChildIndicator': null,
                            'name': {'middleName': '', 'lastName': '', 'firstName': '', 'suffix': ''},
                            'liveWithAdoptiveParentsIndicator': true,
                            'personId': [],
                            'anotherChildIndicator': false
                        },
                        'specialCircumstances': {
                            'ageWhenLeftFosterCare': 0,
                            'everInFosterCareIndicator': false,
                            'metFosterCareConditions': false,
                            'fosterChild': false,
                            'numberBabiesExpectedInPregnancy': 0,
                            'pregnantIndicator': false,
                            'gettingHealthCareThroughStateMedicaidIndicator': true,
                            'tribalManualVerificationIndicator': true,
                            'americanIndianAlaskaNativeIndicator': true
                        },
                        'livesWithHouseholdContactIndicator': true,
                        'socialSecurityCard': {
                            'middleNameOnSSNCard': '',
                            'deathManualVerificationIndicator': false,
                            'socialSecurityNumber': null,
                            'suffixOnSSNCard': '',
                            'individualMandateExceptionIndicator': null,
                            'ssnManualVerificationIndicator': true,
                            'deathIndicatorAsAttested': false,
                            'nameSameOnSSNCardIndicator': null,
                            'deathIndicator': false,
                            'firstNameOnSSNCard': '',
                            'socialSecurityCardHolderIndicator': false,
                            'useSelfAttestedDeathIndicator': true,
                            'lastNameOnSSNCard': '',
                            'reasonableExplanationForNoSSN': 'JUST_APPLIED'
                        },
                        'strikerIndicator': false,
                        'birthCertificateType': 'NO_CRETIFICATE',
                        'citizenshipImmigrationStatus': {
                            'honorablyDischargedOrActiveDutyMilitaryMemberIndicator': null,
                            'citizenshipStatusIndicator': null,
                            'eligibleImmigrationDocumentType': [{
                                'DS2019Indicator': false,
                                'UnexpiredForeignPassportIndicator': false,
                                'TemporaryI551StampIndicator': false,
                                'I551Indicator': false,
                                'MachineReadableVisaIndicator': false,
                                'OtherDocumentTypeIndicator': false,
                                'I94InPassportIndicator': false,
                                'I571Indicator': false,
                                'I766Indicator': false,
                                'I20Indicator': false,
                                'I94Indicator': false,
                                'I797Indicator': false,
                                'I327Indicator': false,
                                'noneOfThese': false
                            }],
                            'naturalizationCertificateIndicator': null,
                            'citizenshipManualVerificationStatus': null,
                            'citizenshipAsAttestedIndicator': false,
                            'eligibleImmigrationStatusIndicator': false,
                            'citizenshipDocument': {
                                // 'SEVISId': '738583838585',
                                'foreignPassportOrDocumentNumber': '',
                                'sevisId': '',
                                'alienNumber': '43737778',
                                'nameOnDocument': {'middleName': '', 'lastName': '', 'firstName': '', 'suffix': ''},
                                'documentDescription': '',
                                'nameSameOnDocumentIndicator': null,
                                'visaNumber': '',
                                'documentExpirationDate': null,
                                'i94Number': '',
                                'foreignPassportCountryOfIssuance': '0',
                                'cardNumber': ''
                            },
                            'lawfulPresenceIndicator': true,
                            'naturalizationCertificateAlienNumber': '',
                            'eligibleImmigrationDocumentSelected': 'CitizenshipCertificate',
                            'otherImmigrationDocumentType': {
                                'cubanHaitianEntrantIndicator': false,
                                'orreligibilityLetterIndicator': false,
                                'stayOfRemovalIndicator': false,
                                'orrcertificationIndicator': false,
                                'americanSamoanIndicator': false,
                                'withholdingOfRemovalIndicator': false,
                                'domesticViolence': false,
                                'indianOfUsOrCanada': false,
                                'noneOfThese': false
                            },
                            'naturalizedCitizenshipIndicator': true,
                            'livedIntheUSSince1996Indicator': false,
                            'naturalizationCertificateNaturalizationNumber': ''
                        },
                        'medicalExpense': null,
                        'specialEnrollmentPeriod': {'birthOrAdoptionEventIndicator': false},
                        'marriedIndicator': null,
                        'bloodRelationship': [],
                        'drugFellowIndicator': false,
                        'livingArrangementIndicator': false,
                        'taxFiler': {
                            'spouseHouseholdMemberId': 0,
                            'claimingDependantsOnFTRIndicator': null,
                            'taxFilerDependants': [{'dependantHouseholdMemeberId': 0, 'taxFilerDependantIndicator': false}],
                            'liveWithSpouseIndicator': null
                        },
                        'shelterAndUtilityExpense': null,
                        'tobaccoUserIndicator': false,
                        'dependentCareExpense': null,
                        'residencyVerificationIndicator': false,
                        'taxFilerDependant': {'dependantHouseholdMemeberId': null, 'taxFilerDependantIndicator': null},
                        'name': {'middleName': 'George', 'lastName': 'Blacksmith', 'firstName': 'Nataniel', 'suffix': 'Jr.'},
                        'ethnicityAndRace': {'ethnicity': [], 'race': [{'label': 'Argentinian', 'code': '2131-1', 'otherLabel': null}], 'hispanicLatinoSpanishOriginIndicator': false},
                        'personId': 3,
                        'addressId': 1,
                        'householdId' : 0,
                        'gender': 'male',
                        'SSIIndicator': false,
                        'IPVIndicator': false,
                        'householdContactIndicator': true,
                        'americanIndianAlaskaNative': {
                            tribeName: 'AL010001',
                            tribeFullName: '',
                            state: 'AL',
                            stateFullName: '',
                            memberOfFederallyRecognizedTribeIndicator: true
                        },
                        'householdContact': {
                            'homeAddressIndicator': true,
                            'phone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'CELL'},
                            'homeAddress': {
                                'postalCode': '',
                                'countyCode': null,
                                'county': '',
                                'primaryAddressCountyFipsCode': '',
                                'streetAddress1': '',
                                'streetAddress2': '',
                                'state': '',
                                'city': ''
                            },
                            'stateResidencyProof': '',
                            'otherPhone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'HOME'},
                            'contactPreferences': {'emailAddress': '', 'preferredSpokenLanguage': '', 'preferredContactMethod': '', 'preferredWrittenLanguage': ''},
                            'mailingAddress': {
                                'postalCode': '83709',
                                'countyCode': null,
                                'county': 'Ada',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '5799 S Acheron Ave Apt 12',
                                'streetAddress2': 'apt. 12',
                                'state': 'ID',
                                'city': 'Boise'
                            },
                            'mailingAddressSameAsHomeAddressIndicator': true
                        },
                        'PIDVerificationIndicator': false,
                        'applicantGuid': '1001016687',
                        'PIDIndicator': false,
                        'applyingForCoverageIndicator': true,
                        'planToFileFTRJontlyIndicator': null,
                        'livesAtOtherAddressIndicator': false,
                        'healthCoverage': {
                            'chpInsurance': {'reasonInsuranceEndedOther': '', 'insuranceEndedDuringWaitingPeriodIndicator': false},
                            'haveBeenUninsuredInLast6MonthsIndicator': null,
                            'currentEmployer': [{
                                'employer': {
                                    'phone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'HOME'},
                                    'employerContactPersonName': '',
                                    'employerContactEmailAddress': ''
                                },
                                'currentEmployerInsurance': {
                                    'currentLowestCostSelfOnlyPlanName': '',
                                    'willBeEnrolledInEmployerPlanIndicator': false,
                                    'memberEmployerPlanStartDate': null,
                                    'willBeEnrolledInEmployerPlanDate': null,
                                    'comingPlanMeetsMinimumStandardIndicator': false,
                                    'memberPlansToDropEmployerPlanIndicator': true,
                                    'employerWillOfferPlanIndicator': true,
                                    'expectedChangesToEmployerCoverageIndicator': true,
                                    'employerCoverageEndingDate': null,
                                    'memberPlansToEnrollInEmployerPlanIndicator': true,
                                    'employerPlanStartDate': null,
                                    'memberEmployerPlanEndingDate': null,
                                    'comingLowestCostSelfOnlyPlanName': '',
                                    'currentPlanMeetsMinimumStandardIndicator': false,
                                    'employerWillNotOfferCoverageIndicator': false,
                                    'isCurrentlyEnrolledInEmployerPlanIndicator': false,
                                    'insuranceName': null,
                                    'policyNumber': null
                                }
                            }],
                            'currentlyHasHealthInsuranceIndicator': false,
                            'currentOtherInsurance': {
                                'otherStateOrFederalMedicaidProgramName': 'Medicaid',
                                'otherStateOrFederalCHIPProgramName':'Nevada Check Up',
                                'otherStateOrFederalPrograms': [],
                                'currentlyEnrolledInVeteransProgram': false,
                                'hasNonESI': false
                            },
                            'otherInsurance': {
                                'insuranceName': '',
                                'policyNumber': '',
                                'limitedBenefit': false
                            },
                            'formerEmployer': [{
                                'phone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'CELL'},
                                'employerName': '',
                                'employerIdentificationNumber': '',
                                'employerContactPersonName': '',
                                'address': {'postalCode': '', 'county': null, 'primaryAddressCountyFipsCode': null, 'streetAddress1': '', 'streetAddress2': '', 'state': '', 'city': ''},
                                'employerContactPhone': {'phoneExtension': '', 'phoneNumber': '', 'phoneType': 'HOME'},
                                'employerContactEmailAddress': ''
                            }],
                            'primaryCarePhysicianName': '',
                            'otherInsuranceIndicator': true,
                            'currentlyEnrolledInCobraIndicator': null,
                            'employerWillOfferInsuranceIndicator': null,
                            'currentlyEnrolledInRetireePlanIndicator': null,
                            'currentlyEnrolledInVeteransProgramIndicator': null,
                            'hasPrimaryCarePhysicianIndicator': false,
                            'employerWillOfferInsuranceCoverageStartDate': null,
                            'willBeEnrolledInCobraIndicator': null,
                            'willBeEnrolledInVeteransProgramIndicator': null,
                            'medicaidInsurance': {'requestHelpPayingMedicalBillsLast3MonthsIndicator': false},
                            'isOfferedHealthCoverageThroughJobIndicator': null,
                            'willBeEnrolledInRetireePlanIndicator': null,
                            'employerWillOfferInsuranceStartDate': null,
                            'stateHealthBenefit': null,
                            'eligibleITU': null,
                            'receivedITU': null,
                            'unpaidBill': null,
                            'coveredDependentChild': null,
                            'absentParent': null
                        },
                        'planToFileFTRIndicator': null,
                        'otherAddress': {
                            'address': {
                                'postalCode': '90055',
                                'county': '',
                                'countyCode': 'Santa Clara',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '',
                                'streetAddress2': '',
                                'state': 'MO',
                                'city': ''
                            },
                            'livingOutsideofStateTemporarilyAddress': {
                                'postalCode': '',
                                'county': '',
                                'primaryAddressCountyFipsCode': null,
                                'streetAddress1': '',
                                'streetAddress2': '',
                                'state': '',
                                'city': ''
                            },
                            'livingOutsideofStateTemporarilyIndicator': null
                        },
                        'heatingCoolingindicator': false,
                        'incarcerationStatus': {
                            'incarcerationManualVerificationIndicator': false,
                            'incarcerationStatusIndicator': false,
                            'useSelfAttestedIncarceration': false,
                            'incarcerationPendingDispositionIndicator': false,
                            'incarcerationAsAttestedIndicator': false
                        },
                        'infantIndicator': false,
                        'childSupportExpense': null,
                        'expeditedIncome': {
                            'irsReportedAnnualIncome': null,
                            'expectedAnnualIncomeUnknownIndicator': null,
                            'irsIncomeManualVerificationIndicator': false,
                            'expectedAnnualIncome': null
                        },
                        'migrantFarmWorker': true,
                        'status': 'ADD_NEW',
                        'mediciadChipDenial': false,
                        'under26Indicator': true,
                        'externalId': null
                    },
                    {
                        seeksQhp: true,
                        applicantPersonType: 'PTF',
                        flagBucket: {
                            taxHouseholdCompositionFlags: 604587154,
                            taxHouseholdCompositionProgressFlags: 21650,
                            annualTaxIncomeFlags: 18
                        },
                        employerSponsoredCoverage: [],
                        taxHouseholdComposition: {
                            claimerIds: [],
                            taxFilingStatus: 'UNSPECIFIED',
                            taxRelationship: 'UNSPECIFIED'
                        },
                        parentCaretaker: null,
                        fullTimeStudent: null,
                        annualTaxIncome: {
                            reportedIncome: 0,
                            projectedIncome: 0
                        },
                        medicaidChipDenial: null,
                        medicaidDeniedDate: null,
                        medicaidDeniedDueToImmigration: null,
                        changeInImmigrationSince5Year: null,
                        fiveYearBar: null,
                        changeInImmigrationSinceMedicaidDenial: null,
                        blindOrDisabled: false,
                        longTermCare: false,
                        disabilityIndicator: false,
                        hasESI: false,
                        incomes: [],
                        expenses: [],
                        detailedIncome: {
                            partnershipAndSwarpIncomeIncomeProof: '',
                            capitalGains: {
                                annualNetCapitalGains: null
                            },
                            unemploymentBenefit: {
                                incomeFrequency: '',
                                stateGovernmentName: '',
                                benefitAmount: null,
                                unemploymentDate: null
                            },
                            annuityIncomeProof: '',
                            scholarshipIncomeProof: '',
                            otherIncome: [
                                {
                                    incomeFrequency: '',
                                    incomeAmount: null,
                                    otherIncomeTypeDescription: 'Canceled debts'
                                },
                                {
                                    incomeFrequency: '',
                                    incomeAmount: null,
                                    otherIncomeTypeDescription: 'Court Awards'
                                },
                                {
                                    incomeFrequency: '',
                                    incomeAmount: null,
                                    otherIncomeTypeDescription: 'Jury duty pay'
                                }
                            ],
                            receiveMAthrough1619: false,
                            taxExemptedIncomeProof: '',
                            wageIncomeProof: '',
                            dividendsIncomeProof: '',
                            farmFishingIncome: {
                                incomeFrequency: '',
                                netIncomeAmount: null
                            },
                            rentalRoyaltyIncomeIndicator: null,
                            unemploymentBenefitIndicator: null,
                            capitalGainsIndicator: null,
                            jobIncome: [
                                {
                                    incomeFrequency: '',
                                    employerName: '',
                                    incomeAmountBeforeTaxes: null,
                                    workHours: null
                                }
                            ],
                            alimonyReceivedIndicator: null,
                            unemploymentCompensationIncomeProof: '',
                            selfEmploymentTax: {
                                incomeAmount: null
                            },
                            studentLoanInterest: {
                                incomeAmount: null
                            },
                            selfEmploymentIncome: {
                                monthlyNetIncome: null,
                                typeOfWork: ''
                            },
                            businessIncomeProof: '',
                            sellingBusinessProperty: {
                                incomeAmount: null
                            },
                            retirementOrPension: {
                                taxableAmount: null,
                                incomeFrequency: ''
                            },
                            royaltiesIncomeProof: '',
                            farmFishingIncomeIndictor: null,
                            investmentIncome: {
                                incomeFrequency: '',
                                incomeAmount: null
                            },
                            farmIncom: {
                                incomeAmount: null
                            },
                            deductions: {
                                alimonyDeductionAmount: null,
                                otherDeductionAmount: null,
                                alimonyDeductionFrequency: '',
                                studentLoanDeductionFrequency: '',
                                deductionFrequency: 'MONTHLY',
                                studentLoanDeductionAmount: null,
                                deductionType: [
                                    'ALIMONY',
                                    'STUDENT_LOAN_INTEREST',
                                    'OTHER'
                                ],
                                otherDeductionFrequency: '',
                                deductionAmount: null
                            },
                            rentalRoyaltyIncome: {
                                incomeFrequency: '',
                                netIncomeAmount: null
                            },
                            receiveMAthroughSSI: false,
                            foreignEarnedIncomeProof: '',
                            socialSecurityBenefit: {
                                incomeFrequency: '',
                                benefitAmount: null
                            },
                            foreignEarnedIncome: {
                                incomeAmount: null
                            },
                            childNaTribe: '',
                            sellingBusinessPropertyIncomeProof: '',
                            alimonyReceived: {
                                amountReceived: null,
                                incomeFrequency: ''
                            },
                            selfEmploymentTaxProof: '',
                            partnershipsCorporationsIncome: {
                                incomeAmount: null
                            },
                            studentLoanInterestProof: '',
                            farmIncomeProof: '',
                            retirementOrPensionIndicator: null,
                            investmentIncomeIndicator: null,
                            socialSecurityBenefitIndicator: null,
                            rentalRealEstateIncome: {
                                incomeAmount: null
                            },
                            otherIncomeIndicator: null,
                            pensionIncomeProof: '',
                            socialSecurityBenefitProof: '',
                            jobIncomeIndicator: null,
                            businessIncome: {
                                incomeAmount: null
                            },
                            taxableInterestIncomeProof: '',
                            selfEmploymentIncomeIndicator: null,
                            rentalRealEstateIncomeProof: '',
                            discrepancies: {
                                hasWageOrSalaryBeenCutIndicator: null,
                                explanationForJobIncomeDiscrepancy: '',
                                didPersonEverWorkForEmployerIndicator: null,
                                stopWorkingAtEmployerIndicator: null,
                                hasHoursDecreasedWithEmployerIndicator: null,
                                explanationForDependantDiscrepancy: '',
                                seasonalWorkerIndicator: null,
                                hasChangedJobsIndicator: false
                            }
                        },
                        dateOfBirth: 1285225200000,
                        parentCaretakerRelatives: {
                            relationship: '',
                            dateOfBirth: null,
                            mainCaretakerOfChildIndicator: null,
                            name: {
                                middleName: '',
                                lastName: '',
                                firstName: '',
                                suffix: ''
                            },
                            liveWithAdoptiveParentsIndicator: true,
                            personId: [],
                            anotherChildIndicator: false
                        },
                        specialCircumstances: {
                            ageWhenLeftFosterCare: 0,
                            everInFosterCareIndicator: false,
                            metFosterCareConditions: false,
                            fosterChild: false,
                            numberBabiesExpectedInPregnancy: 0,
                            pregnantIndicator: false,
                            gettingHealthCareThroughStateMedicaidIndicator: true,
                            tribalManualVerificationIndicator: true,
                            americanIndianAlaskaNativeIndicator: true
                        },
                        livesWithHouseholdContactIndicator: true,
                        socialSecurityCard: {
                            middleNameOnSSNCard: '',
                            deathManualVerificationIndicator: false,
                            socialSecurityNumber: null,
                            suffixOnSSNCard: '',
                            individualMandateExceptionIndicator: null,
                            ssnManualVerificationIndicator: true,
                            deathIndicatorAsAttested: false,
                            nameSameOnSSNCardIndicator: null,
                            deathIndicator: false,
                            firstNameOnSSNCard: '',
                            socialSecurityCardHolderIndicator: false,
                            useSelfAttestedDeathIndicator: true,
                            lastNameOnSSNCard: '',
                            reasonableExplanationForNoSSN: 'JUST_APPLIED'
                        },
                        strikerIndicator: false,
                        birthCertificateType: 'NO_CRETIFICATE',
                        citizenshipImmigrationStatus: {
                            honorablyDischargedOrActiveDutyMilitaryMemberIndicator: null,
                            citizenshipStatusIndicator: null,
                            eligibleImmigrationDocumentType: [
                                {
                                    DS2019Indicator: false,
                                    UnexpiredForeignPassportIndicator: false,
                                    TemporaryI551StampIndicator: false,
                                    I551Indicator: false,
                                    MachineReadableVisaIndicator: false,
                                    OtherDocumentTypeIndicator: false,
                                    I94InPassportIndicator: false,
                                    I571Indicator: false,
                                    I766Indicator: false,
                                    I20Indicator: false,
                                    I94Indicator: false,
                                    I797Indicator: false,
                                    I327Indicator: false,
                                    noneOfThese: false
                                }
                            ],
                            naturalizationCertificateIndicator: null,
                            citizenshipManualVerificationStatus: null,
                            citizenshipAsAttestedIndicator: false,
                            eligibleImmigrationStatusIndicator: false,
                            citizenshipDocument: {
                                foreignPassportOrDocumentNumber: '',
                                sevisId: '',
                                alienNumber: '43737778',
                                nameOnDocument: {
                                    middleName: '',
                                    lastName: '',
                                    firstName: '',
                                    suffix: ''
                                },
                                documentDescription: '',
                                nameSameOnDocumentIndicator: null,
                                visaNumber: '',
                                documentExpirationDate: null,
                                i94Number: '',
                                foreignPassportCountryOfIssuance: '0',
                                cardNumber: ''
                            },
                            lawfulPresenceIndicator: true,
                            naturalizationCertificateAlienNumber: '',
                            eligibleImmigrationDocumentSelected: 'CitizenshipCertificate',
                            otherImmigrationDocumentType: {
                                cubanHaitianEntrantIndicator: false,
                                orreligibilityLetterIndicator: false,
                                stayOfRemovalIndicator: false,
                                orrcertificationIndicator: false,
                                americanSamoanIndicator: false,
                                withholdingOfRemovalIndicator: false,
                                domesticViolence: false,
                                indianOfUsOrCanada: false,
                                noneOfThese: false
                            },
                            naturalizedCitizenshipIndicator: true,
                            livedIntheUSSince1996Indicator: false,
                            naturalizationCertificateNaturalizationNumber: ''
                        },
                        medicalExpense: null,
                        specialEnrollmentPeriod: {
                            birthOrAdoptionEventIndicator: false
                        },
                        marriedIndicator: null,
                        bloodRelationship: [],
                        drugFellowIndicator: false,
                        livingArrangementIndicator: false,
                        taxFiler: {
                            spouseHouseholdMemberId: 0,
                            claimingDependantsOnFTRIndicator: null,
                            taxFilerDependants: [
                                {
                                    dependantHouseholdMemeberId: 0,
                                    taxFilerDependantIndicator: false
                                }
                            ],
                            liveWithSpouseIndicator: null
                        },
                        shelterAndUtilityExpense: null,
                        tobaccoUserIndicator: false,
                        dependentCareExpense: null,
                        residencyVerificationIndicator: false,
                        taxFilerDependant: {
                            dependantHouseholdMemeberId: null,
                            taxFilerDependantIndicator: null
                        },
                        name: {
                            middleName: 'Mary',
                            lastName: 'Blacksmith',
                            firstName: 'Rose',
                            suffix: 'Jr.'
                        },
                        ethnicityAndRace: {
                            ethnicity: [],
                            race: [
                                {
                                    label: 'Argentinian',
                                    code: '2131-1',
                                    otherLabel: null
                                }
                            ],
                            hispanicLatinoSpanishOriginIndicator: false
                        },
                        personId: 4,
                        addressId: 1,
                        householdId: 0,
                        gender: 'male',
                        SSIIndicator: false,
                        IPVIndicator: false,
                        householdContactIndicator: true,
                        americanIndianAlaskaNative: {
                            tribeName: 'AL010001',
                            tribeFullName: '',
                            state: 'AL',
                            stateFullName: '',
                            memberOfFederallyRecognizedTribeIndicator: true
                        },
                        householdContact: {
                            homeAddressIndicator: true,
                            phone: {
                                phoneExtension: '',
                                phoneNumber: '',
                                phoneType: 'CELL'
                            },
                            homeAddress: {
                                postalCode: '',
                                countyCode: null,
                                county: '',
                                primaryAddressCountyFipsCode: '',
                                streetAddress1: '',
                                streetAddress2: '',
                                state: '',
                                city: ''
                            },
                            stateResidencyProof: '',
                            otherPhone: {
                                phoneExtension: '',
                                phoneNumber: '',
                                phoneType: 'HOME'
                            },
                            contactPreferences: {
                                emailAddress: '',
                                preferredSpokenLanguage: '',
                                preferredContactMethod: '',
                                preferredWrittenLanguage: ''
                            },
                            mailingAddress: {
                                postalCode: '83709',
                                countyCode: null,
                                county: 'Ada',
                                primaryAddressCountyFipsCode: null,
                                streetAddress1: '5799 S Acheron Ave Apt 12',
                                streetAddress2: 'apt. 12',
                                state: 'ID',
                                city: 'Boise'
                            },
                            mailingAddressSameAsHomeAddressIndicator: true
                        },
                        PIDVerificationIndicator: false,
                        applicantGuid: '1001016687',
                        PIDIndicator: false,
                        applyingForCoverageIndicator: true,
                        planToFileFTRJontlyIndicator: null,
                        livesAtOtherAddressIndicator: false,
                        healthCoverage: {
                            chpInsurance: {
                                reasonInsuranceEndedOther: '',
                                insuranceEndedDuringWaitingPeriodIndicator: false
                            },
                            haveBeenUninsuredInLast6MonthsIndicator: null,
                            currentEmployer: [
                                {
                                    employer: {
                                        phone: {
                                            phoneExtension: '',
                                            phoneNumber: '',
                                            phoneType: 'HOME'
                                        },
                                        employerContactPersonName: '',
                                        employerContactEmailAddress: ''
                                    },
                                    currentEmployerInsurance: {
                                        currentLowestCostSelfOnlyPlanName: '',
                                        willBeEnrolledInEmployerPlanIndicator: false,
                                        memberEmployerPlanStartDate: null,
                                        willBeEnrolledInEmployerPlanDate: null,
                                        comingPlanMeetsMinimumStandardIndicator: false,
                                        memberPlansToDropEmployerPlanIndicator: true,
                                        employerWillOfferPlanIndicator: true,
                                        expectedChangesToEmployerCoverageIndicator: true,
                                        employerCoverageEndingDate: null,
                                        memberPlansToEnrollInEmployerPlanIndicator: true,
                                        employerPlanStartDate: null,
                                        memberEmployerPlanEndingDate: null,
                                        comingLowestCostSelfOnlyPlanName: '',
                                        currentPlanMeetsMinimumStandardIndicator: false,
                                        employerWillNotOfferCoverageIndicator: false,
                                        isCurrentlyEnrolledInEmployerPlanIndicator: false,
                                        insuranceName: null,
                                        policyNumber: null
                                    }
                                }
                            ],
                            currentlyHasHealthInsuranceIndicator: false,
                            currentOtherInsurance: {
                                otherStateOrFederalMedicaidProgramName: 'Medicaid',
                                otherStateOrFederalCHIPProgramName: 'Nevada Check Up',
                                otherStateOrFederalPrograms: [],
                                currentlyEnrolledInVeteransProgram: false,
                                hasNonESI: false
                            },
                            otherInsurance: {
                                insuranceName: '',
                                policyNumber: '',
                                limitedBenefit: false
                            },
                            formerEmployer: [
                                {
                                    phone: {
                                        phoneExtension: '',
                                        phoneNumber: '',
                                        phoneType: 'CELL'
                                    },
                                    employerName: '',
                                    employerIdentificationNumber: '',
                                    employerContactPersonName: '',
                                    address: {
                                        postalCode: '',
                                        county: null,
                                        primaryAddressCountyFipsCode: null,
                                        streetAddress1: '',
                                        streetAddress2: '',
                                        state: '',
                                        city: ''
                                    },
                                    employerContactPhone: {
                                        phoneExtension: '',
                                        phoneNumber: '',
                                        phoneType: 'HOME'
                                    },
                                    employerContactEmailAddress: ''
                                }
                            ],
                            primaryCarePhysicianName: '',
                            otherInsuranceIndicator: true,
                            currentlyEnrolledInCobraIndicator: null,
                            employerWillOfferInsuranceIndicator: null,
                            currentlyEnrolledInRetireePlanIndicator: null,
                            currentlyEnrolledInVeteransProgramIndicator: null,
                            hasPrimaryCarePhysicianIndicator: false,
                            employerWillOfferInsuranceCoverageStartDate: null,
                            willBeEnrolledInCobraIndicator: null,
                            willBeEnrolledInVeteransProgramIndicator: null,
                            medicaidInsurance: {
                                requestHelpPayingMedicalBillsLast3MonthsIndicator: false
                            },
                            isOfferedHealthCoverageThroughJobIndicator: null,
                            willBeEnrolledInRetireePlanIndicator: null,
                            employerWillOfferInsuranceStartDate: null,
                            stateHealthBenefit: null,
                            eligibleITU: null,
                            receivedITU: null,
                            unpaidBill: null,
                            coveredDependentChild: null,
                            absentParent: null
                        },
                        planToFileFTRIndicator: null,
                        otherAddress: {
                            address: {
                                postalCode: '90055',
                                county: '',
                                countyCode: 'Santa Clara',
                                primaryAddressCountyFipsCode: null,
                                streetAddress1: '',
                                streetAddress2: '',
                                state: 'MO',
                                city: ''
                            },
                            livingOutsideofStateTemporarilyAddress: {
                                postalCode: '',
                                county: '',
                                primaryAddressCountyFipsCode: null,
                                streetAddress1: '',
                                streetAddress2: '',
                                state: '',
                                city: ''
                            },
                            livingOutsideofStateTemporarilyIndicator: null
                        },
                        heatingCoolingindicator: false,
                        incarcerationStatus: {
                            incarcerationManualVerificationIndicator: false,
                            incarcerationStatusIndicator: false,
                            useSelfAttestedIncarceration: false,
                            incarcerationPendingDispositionIndicator: false,
                            incarcerationAsAttestedIndicator: false
                        },
                        infantIndicator: false,
                        childSupportExpense: null,
                        expeditedIncome: {
                            irsReportedAnnualIncome: null,
                            expectedAnnualIncomeUnknownIndicator: null,
                            irsIncomeManualVerificationIndicator: false,
                            expectedAnnualIncome: null
                        },
                        migrantFarmWorker: true,
                        status: 'ADD_NEW',
                        mediciadChipDenial: false,
                        under26Indicator: true,
                        externalId: null
                    }
                ],
                // 'usersForQhp': [],
                'houseHoldIncome': 0.0,
                'applyingForCashBenefitsIndicator': false,
                'reconciledAptc': null,
                'failureToReconcile': false,
                'primaryTaxFilerPersonId': 0,
                'otherAddresses': [
                    {
                        'postalCode': '83709',
                        'countyCode': null,
                        'county': 'Ada',
                        'primaryAddressCountyFipsCode': '16001',
                        'streetAddress1': '5799 S Acheron Ave Apt 12',
                        'streetAddress2': 'apt. 12',
                        'state': 'CA',
                        'city': 'Boise',
                        'addressId': 1
                    },
                    {
                        'postalCode': '95136',
                        'countyCode': null,
                        'county': 'Santa Clara',
                        'primaryAddressCountyFipsCode': '16001',
                        'streetAddress1': '4300 The Woods Drive',
                        'streetAddress2': 'apt. 1207',
                        'state': 'CA',
                        'city': 'San Jose',
                        'addressId': 2
                    },
                    // {
                    //     'postalCode': '77777',
                    //     'countyCode': null,
                    //     'county': 'Santa Clara',
                    //     'primaryAddressCountyFipsCode': '16001',
                    //     'streetAddress1': '4300 The Woods Drive',
                    //     'streetAddress2': 'apt. 1207',
                    //     'state': 'CA',
                    //     'city': 'Mountain View',
                    //     'addressId': 3
                    // },
                ]
            }
        ],
        'medicaidConsent': null,
        'agreeToUseIncomeDetails': true,
        'agreeToEndCoverage': true,
        'incarcerationAsAttestedIndicator': true,
        'mecCheckStatus': 'N',
        'jointTaxFilerSpousePersonId': null,
        'currentApplicantId': 3,
        'ssapVersion': 1.0
    }

};

export const productionInitialState = {
    singleStreamlinedApplication: {
        mockData: true,
        taxHousehold: [
            {
                householdMember: [
                    {
                        bloodRelationship: []
                    }
                ]
            }
        ]
    }
};

export const initialState = CONFIG.ENV.PRODUCTION ? productionInitialState : mockData;

export default (state = initialState, action) => {
    switch (action.type) {
    case dataActionTypes.CHANGE_DATA:
        return action.newState;
    case dataActionTypes.SET_DATA:
        return {
            'singleStreamlinedApplication': action.data
        };
    case dataActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};
