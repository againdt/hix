package com.getinsured.eligibility.rrv.model;

import java.time.LocalDate;

public class RrvMember {

    private String applicantId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String taxIdentifier;
    private String taxFilerCode;
    private LocalDate dateOfBirth;

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTaxIdentifier() {
        return taxIdentifier;
    }

    public void setTaxIdentifier(String taxIdentifier) {
        this.taxIdentifier = taxIdentifier;
    }

    public String getTaxFilerCode() {
        return taxFilerCode;
    }

    public void setTaxFilerCode(String taxFilerCode) {
        this.taxFilerCode = taxFilerCode;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "RrvMember{" +
                "applicantId='" + applicantId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", taxIdentifier='" + taxIdentifier + '\'' +
                ", taxFilerCode='" + taxFilerCode + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}
