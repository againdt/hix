package com.getinsured.iex.shop.mec.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.NonESIMECIndividualResponseType;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.OrganizationCodeSimpleType;
import com.getinsured.iex.shop.mec.enrollment.verifyshopmec.SourceInformationType;

public class NonESIMECIndividualResponseTypeWrapper implements JSONAware{
	
    private String personSSN;
    private SourceInformationTypeWrapper sourceInformationWrapper;
    private String organizationCode;
	    
	public NonESIMECIndividualResponseTypeWrapper(NonESIMECIndividualResponseType nonESIMECIndividualResponse) throws HubServiceException {
		if(nonESIMECIndividualResponse != null){
			personSSN = nonESIMECIndividualResponse.getPersonSSN();
			
			SourceInformationType sourceInformationType = nonESIMECIndividualResponse.getSourceInformation();
			
			if(sourceInformationType != null){
				sourceInformationWrapper = new SourceInformationTypeWrapper(sourceInformationType);
			}
			
			OrganizationCodeSimpleType organizationCodeSimpleType = nonESIMECIndividualResponse.getOrganizationCode();
			if(organizationCodeSimpleType != null){
				organizationCode = organizationCodeSimpleType.value();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("PersonSSN", this.personSSN);
		obj.put("OrganizationCode", this.organizationCode);
		obj.put("SourceInformation", this.sourceInformationWrapper);
		return obj.toJSONString();
	}
}
