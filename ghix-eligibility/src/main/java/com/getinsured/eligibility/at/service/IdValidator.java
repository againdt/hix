package com.getinsured.eligibility.at.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.enums.NativeAmericanCSREnum;
import com.getinsured.eligibility.at.resp.si.enums.NonNativeAmericanCSREnum;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.APTCEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ApplicationExtensionType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.CSREligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantEventType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantNonQHPType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExtendedApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantEventCodeType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 *
 */
@Component("idahoValidator")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class IdValidator extends BaseCustomValidator {

	private static final List<String> CSR_LIST = new ArrayList<String>();
	private static final String IDAHO = "ID";

	static {
		CSR_LIST.add("OpenToIndiansBelow300PercentFPL");
		CSR_LIST.add("OpenToIndiansAbove300PercentFPL");
		CSR_LIST.add("73PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_LIST.add("87PercentActuarialVarianceLevelSilverPlanCSR");
		CSR_LIST.add("94PercentActuarialVarianceLevelSilverPlanCSR");
	}

	private void validateMedicaidCHIPId(AccountTransferRequestPayloadType request, List<String> errorList) {
		String identificationId = null;
		List<PersonType> personList = request.getPerson();
		for (PersonType personType : personList) {
			identificationId = null;
			if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID() != null) {
				identificationId = personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID().getValue();
			}

			if (identificationId == null) {
				if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonCHIPIdentification() != null && personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID() != null) {
					identificationId = personType.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID().getValue();
				}
			}
			if (identificationId == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_IDENTIFICATION_ID, new Object[] { personType.getId() }));
			}
		}

	}

	@Override
	public List<String> validate(AccountTransferRequestPayloadType request) {
		final List<String> errorList = super.validate(request);

		validateMedicaidCHIPId(request, errorList);
		//validateFinancialApplication(request, errorList);
		validateTaxHouseHold(request, errorList);
		validateEligibilities(request, errorList);
		validateBloodRelationshipAvailableForAll(request, errorList);
		validateApplicationExtension(request, errorList);
		validateHousholdCaseId(request, errorList);
		return errorList;
	}

	private void validateHousholdCaseId(AccountTransferRequestPayloadType request, List<String> errorList) {
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		
		if(null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId)) {
			
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes =request.getInsuranceApplication().getApplicationIdentification();
			boolean hhCaseIdPresent = false;
			for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType :iTypes) {
				if( null != tIdentificationType.getIdentificationCategoryText( ) && null != tIdentificationType.getIdentificationCategoryText().getValue() && 
						null !=tIdentificationType.getIdentificationID()  && null != tIdentificationType.getIdentificationID().getValue() && 
						!tIdentificationType.getIdentificationID().getValue().isEmpty() && 
						ReferralConstants.HOUSE_HOLD_CASE_ID.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue()  )   ) {
					hhCaseIdPresent = true;
					break;
				}
			}
			
			if(!hhCaseIdPresent) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "ApplicationIdentification:IdentificationCategoryText" }));
			}
			
		}
		
	}

	private void validateApplicationExtension(AccountTransferRequestPayloadType request, List<String> errorList) {
		final Set<Integer> coverageYears = ReferralUtil.fetchCoverageYears(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.ACCOUNT_TRANSFER_COVERAGE_YEARS));

		ApplicationExtensionType applicationExtension = request.getInsuranceApplication().getApplicationExtension();
		if (applicationExtension == null) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "ApplicationExtension" }));
		} else {
			if (applicationExtension.getCoverageYear() == null || applicationExtension.getCoverageYear().getValue() == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "CoverageYear" }));
			} else if (!coverageYears.contains(applicationExtension.getCoverageYear().getValue().getYear())) {
				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATE, new Object[] { "CoverageYear" }));
			}
			if (ReferralUtil.listSize(applicationExtension.getExtendedApplicant()) != ReferralConstants.NONE) {
				validateApplicationExtensionApplicant(applicationExtension.getExtendedApplicant(), errorList);
			}
		}
	}

	private void validateApplicationExtensionApplicant(List<ExtendedApplicantType> extendedApplicantList, List<String> errorList) {
		String applicantID;
		for (ExtendedApplicantType applicant : extendedApplicantList) {
			if (applicant.getExtendedApplicantMemberIdentification() == null || applicant.getExtendedApplicantMemberIdentification().getIdentificationID() == null
			        || !ReferralUtil.isNotNullAndEmpty(applicant.getExtendedApplicantMemberIdentification().getIdentificationID().getValue())) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "ExtendedApplicantMemberIdentification" }));
			} else {
				applicantID = applicant.getExtendedApplicantMemberIdentification().getIdentificationID().getValue();
				final List<ExtendedApplicantEventType> extendedApplicantEventList = applicant.getExtendedApplicantEvent();
				final List<ExtendedApplicantNonQHPType> extendedApplicantNonQHPList = applicant.getExtendedApplicantNonQHP();

				if ((ReferralUtil.listSize(extendedApplicantEventList) == ReferralConstants.NONE) && (ReferralUtil.listSize(extendedApplicantNonQHPList) == ReferralConstants.NONE)) {
					errorList.add(ReferralUtil.formatMessage(MSG_EVENT_QHP_MANDATORY, new Object[] { applicant.getExtendedApplicantMemberIdentification().getIdentificationID().getValue() }));
				}
				if (ReferralUtil.listSize(extendedApplicantEventList) != ReferralConstants.NONE) {
					validateExtendedApplicantEvent(extendedApplicantEventList, errorList, applicantID);
				}
				if (ReferralUtil.listSize(extendedApplicantNonQHPList) != ReferralConstants.NONE) {
					validateExtendedApplicantNonQHP(extendedApplicantNonQHPList, errorList, applicantID);
				}
			}
		}
	}

	private void validateExtendedApplicantEvent(List<ExtendedApplicantEventType> extendedApplicantEventList, List<String> errorList, String applicantId) {
		// Set<String> eventCodes = new HashSet<String>();
		for (ExtendedApplicantEventType extendedApplicantEventType : extendedApplicantEventList) {

			if (extendedApplicantEventType.getExtendedApplicantEventCode() == null || extendedApplicantEventType.getExtendedApplicantEventCode().getValue() == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantEventCode", applicantId }));
			} /*
			 * else if (!eventCodes.add(extendedApplicantEventType.getExtendedApplicantEventCode().getValue().value())) { errorList.add(ReferralUtil.formatMessage(MSG_CODE_DUPLICATE, new Object[] { "ExtendedApplicantEventCode", applicantId })); }
			 */
			if (extendedApplicantEventType.getExtendedApplicantEventDate() == null || extendedApplicantEventType.getExtendedApplicantEventDate().getDate() == null) {

				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantEventDate", applicantId }));
			} else if (!isValidDate(extendedApplicantEventType.getExtendedApplicantEventDate().getDate())) {

				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATE_APPLICANT, new Object[] { "ExtendedApplicant EventDate", applicantId }));
			} else if (!validationForFutureDate(extendedApplicantEventType.getExtendedApplicantEventDate().getDate(), extendedApplicantEventType.getExtendedApplicantEventCode())) {
				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA, new Object[] { "ExtendedApplicant EventDate", applicantId }));
			}

			if (extendedApplicantEventType.getExtendedApplicantReportDate() == null || extendedApplicantEventType.getExtendedApplicantReportDate().getDate() == null) {

				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantReportDate", applicantId }));
			} else if (!isValidDate(extendedApplicantEventType.getExtendedApplicantReportDate().getDate()) || isFutureDate(extendedApplicantEventType.getExtendedApplicantReportDate().getDate())) {

				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATE_APPLICANT, new Object[] { "ExtendedApplicant ReportDate", applicantId }));
			}

		}
	}

	private boolean validationForFutureDate(Date date, ExtendedApplicantEventCodeType extendedApplicantEventCodeType) {
		if (!ApplicationExtensionEventUtil.FUTURE_DATED_LCE_EVENTS.contains(extendedApplicantEventCodeType.getValue().value()) && isFutureDate(date)) {
			return false;
		}
		return true;

	}

	private void validateExtendedApplicantNonQHP(List<ExtendedApplicantNonQHPType> extendedApplicantNonQHPList, List<String> errorList, String applicantId) {
		// Set<String> eventCodes = new HashSet<String>();

		for (ExtendedApplicantNonQHPType extendedApplicantNonQHPType : extendedApplicantNonQHPList) {
			if (extendedApplicantNonQHPType.getExtendedApplicantNonQHPCode() == null || extendedApplicantNonQHPType.getExtendedApplicantNonQHPCode().getValue() == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantNonQHPCode", applicantId }));
			} /*
			 * else if (!eventCodes.add(extendedApplicantNonQHPType.getExtendedApplicantNonQHPCode().getValue().value())) { errorList.add(ReferralUtil.formatMessage(MSG_CODE_DUPLICATE, new Object[] { "ExtendedApplicantNonQHPCode", applicantId })); }
			 */
			if (extendedApplicantNonQHPType.getExtendedApplicantNonQHPDate() == null || extendedApplicantNonQHPType.getExtendedApplicantNonQHPDate().getDate() == null) {
				errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "ExtendedApplicantNonQHPDate", applicantId }));
			} else if (!isValidDate(extendedApplicantNonQHPType.getExtendedApplicantNonQHPDate().getDate())) {
				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATE_APPLICANT, new Object[] { "ExtendedApplicant NonQHPDate", applicantId }));
			}
		}
	}

	/**
	 * Added by pravin to check blood relationship available for all members (1-to-1) except self.
	 *
	 * @param request
	 * @param errorList
	 */
	private void validateBloodRelationshipAvailableForAll(AccountTransferRequestPayloadType request, List<String> errorList) {

		List<PersonType> personList = request.getPerson();
		boolean relationshiperr = false;
		int familySize = ReferralUtil.listSize(personList);
		for (PersonType personType : personList) {
			List<PersonAssociationType> personAssociationType = personType.getPersonAugmentation() != null ? personType.getPersonAugmentation().getPersonAssociation() : null;

			int assocMemsize = ReferralUtil.listSize(personAssociationType);
			String indPersonId = personType != null ? personType.getId() != null ? personType.getId() : null : null;

			if (relationshiperr || assocMemsize != familySize - 1 || StringUtils.isEmpty(indPersonId)) {
				relationshiperr = true;
				break;
			}

			/* Now check 1-to-1 mapping */
			List<String> associatedPersonId = new ArrayList<String>();
			for (int i = 0; i < assocMemsize; i++) {
				boolean personRefIdPresent = personAssociationType.get(i) != null && personAssociationType.get(i).getPersonReference() != null && personAssociationType.get(i).getPersonReference().size() > 0
				        && personAssociationType.get(i).getPersonReference().get(0) != null && personAssociationType.get(i).getPersonReference().get(0).getRef() != null;

				String personRefId = personRefIdPresent ? ((PersonType) personAssociationType.get(i).getPersonReference().get(0).getRef()).getId() : null;

				String relationshipCode = personRefIdPresent ? (personAssociationType.get(i).getFamilyRelationshipCode() != null ? personAssociationType.get(i).getFamilyRelationshipCode().getValue() : null) : null;

				if (!StringUtils.isEmpty(personRefId) && !associatedPersonId.contains(personRefId) && !StringUtils.isEmpty(relationshipCode) && !indPersonId.equalsIgnoreCase(personRefId)) {
					associatedPersonId.add(personRefId);
				} else {
					relationshiperr = true;
					break;
				}
			}

		}

		if (relationshiperr) {
			errorList.add(MSG_MISSING_RELATIONSHIP);
		}
	}

	private void validateFinancialApplication(AccountTransferRequestPayloadType request, List<String> errorList) {
		boolean blnFlag = false;
		if (request.getInsuranceApplication().getInsuranceApplicationRequestingFinancialAssistanceIndicator() != null && request.getInsuranceApplication().getInsuranceApplicationRequestingFinancialAssistanceIndicator().isValue()) {
			blnFlag = true;
		}
		if (!blnFlag) {
			errorList.add(MSG_FINANCIAL_APP);
		}
	}

	private void validateTaxHouseHold(AccountTransferRequestPayloadType request, List<String> errorList) {

		if (request.getTaxReturn() != null && ReferralUtil.listSize(request.getTaxReturn()) > 1) {
			errorList.add(MSG_TAX_RETURN);
		}
	}

	private void validateEligibilities(AccountTransferRequestPayloadType request, List<String> errorList) {
		List<InsuranceApplicantType> InsuranceApplicantTypeList = request.getInsuranceApplication().getInsuranceApplicant();
		boolean isEligibilityMissing = false;
		for (InsuranceApplicantType insuranceApplicantType : InsuranceApplicantTypeList) {
			List<EligibilityType> eligibilityTypeList = insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();
			boolean isAPTC = false, isCSR = false;
			for (EligibilityType eligibilityType : eligibilityTypeList) {
				String eligibilityIndicatorType = StringUtils.substringAfter(eligibilityType.getClass().toString(), EE_PACKAGE_NAME);

				if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.APTC_ELIGIBILITY_TYPE)) {
					isAPTC = true;
				}

				if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.CSR_ELIGIBILITY_TYPE)) {
					isCSR = true;
				}
			}

			if (!isAPTC || !isCSR) {
				isEligibilityMissing = true;
				break;
			}
		}

		if (isEligibilityMissing) {
			errorList.add(MSG_APTC_CSR_MISSING);
		}
	}

	@Override
	void validateEligibilityDetails(AccountTransferRequestPayloadType request, List<String> errorList) {
		final PersonType signerApplicant = extractSignerApplicantId(request);
		final List<InsuranceApplicantType> insuranceApplicantTypeList = request.getInsuranceApplication().getInsuranceApplicant();
		List<EligibilityType> eligibilityTypeList = null;
		String eligibilityIndicatorType = null;
		String applicantId = null;
		Set<String> nativeAmericancsrAplhaCategoryCode = new HashSet<String>();
		Set<String> nonNativeAmericancsrAplhaCategoryCode = new HashSet<String>();
		List<String> invalidNACsrCategoryCodes = new ArrayList<String>();
		List<String> invalidNonNACsrCategoryCodes = new ArrayList<String>();
		boolean aptcCheck[] = new boolean[] { false, false };
		boolean csrCheck[] = new boolean[] { false, false };
		boolean isSigner = false;
		Boolean aptcEligible = null;
		Boolean csrEligible = null;
		Set<String> reasonCode = new HashSet<String>();
		Set<DateTime> aptcEligibilityStartDates = new HashSet<DateTime>();
		Set<DateTime> csrEligibilityStartDates = new HashSet<DateTime>();
		Set<DateTime> eligibilityStartDates = new HashSet<DateTime>();
		Set<DateTime> eligibilityEndDates = new HashSet<DateTime>();
		for (InsuranceApplicantType insuranceApplicantType : insuranceApplicantTypeList) {
			applicantId = ((PersonType) insuranceApplicantType.getRoleOfPersonReference().getRef()).getId();
			eligibilityTypeList = insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();
			isSigner = StringUtils.equalsIgnoreCase(signerApplicant.getId(), applicantId);
			reasonCode.clear();
			if (eligibilityTypeList != null) {
				aptcEligible = null;
				csrEligible = null;
				for (EligibilityType eligibilityType : eligibilityTypeList) {
					eligibilityIndicatorType = StringUtils.substringAfter(eligibilityType.getClass().toString(), EE_PACKAGE_NAME);
					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.APTC_ELIGIBILITY_TYPE)) {
						aptcEligible = validateAptcDetails(eligibilityType, applicantId, isSigner, errorList, aptcCheck, reasonCode, aptcEligibilityStartDates, eligibilityEndDates);
					}

					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.CSR_ELIGIBILITY_TYPE)) {
						csrEligible = validateCsrDetails(eligibilityType, applicantId, errorList, csrCheck, csrEligibilityStartDates, eligibilityEndDates);
						validateCSRAlphaCategoryCodes(eligibilityType, errorList, nativeAmericancsrAplhaCategoryCode, nonNativeAmericancsrAplhaCategoryCode, invalidNACsrCategoryCodes, invalidNonNACsrCategoryCodes);
					}
				}
				if (csrEligible != null && csrEligible && aptcEligible != null && !aptcEligible) {
					errorList.add(ReferralUtil.formatMessage(MSG_CSR_APTC_INVALID, new Object[] { applicantId }));
				}
				/*
				 * if (reasonCode.contains(ReferralConstants.NOTAPPLYING_CODE) && reasonCode.size() > ReferralConstants.ONE) { errorList.add(ReferralUtil.formatMessage(MSG_REASON_TEXT_INVALID, new Object[] { applicantId })); }
				 */
			}
		}

		if (aptcEligibilityStartDates.size() > 1){
			errorList.add(MSG_MULTIPLE_APTC_ELIGIBILITY_START_DATES_FOUND);
		}
		if (csrEligibilityStartDates.size() > 1){
			errorList.add(MSG_MULTIPLE_CSR_ELIGIBILITY_START_DATES_FOUND);
		}

		if (eligibilityEndDates.size() > 1){
			errorList.add(MSG_MULTIPLE_APTC_AND_CSR_ELIGIBILITY_END_DATES_FOUND);
		}

		for (DateTime dateTime : eligibilityStartDates) {
			if (dateTime.getDayOfMonth() != 1){
				errorList.add(MSG_APTC_AND_CSR_ELIGIBILITY_START_DATE_IS_NOT_FIRST_OF_A_MONTH);
				break;
			}
		}
		eligibilityStartDates.addAll(aptcEligibilityStartDates);
		eligibilityStartDates.addAll(csrEligibilityStartDates);
		ApplicationExtensionType applicationExtension = request.getInsuranceApplication().getApplicationExtension();
		if (applicationExtension != null
				&& applicationExtension.getCoverageYear() != null
					&& applicationExtension.getCoverageYear().getValue() != null) {
			int coverageYear = applicationExtension.getCoverageYear().getValue().getYear();

			for (DateTime dateTime : eligibilityStartDates) {
				if (dateTime.getYear() != coverageYear){
					errorList.add(ReferralUtil.formatMessage(MSG_APTC_AND_CSR_ELIGIBILITY_START_DATE_IS_OUTSIDE_COVERAGE_YEAR, new Object[] { String.valueOf(coverageYear) }));
					break;
				}
			}

			for (DateTime dateTime : eligibilityEndDates) {
				if (dateTime.getYear() != coverageYear){
					errorList.add(ReferralUtil.formatMessage(MSG_APTC_AND_CSR_ELIGIBILITY_END_DATE_IS_OUTSIDE_COVERAGE_YEAR, new Object[] { String.valueOf(coverageYear) }));
					break;
				}
			}

		}

		if (aptcCheck[0] && !aptcCheck[1]) {
			errorList.add(MSG_APTC_FINAL_AMOUNT);
		}

		if (csrCheck[0] && !csrCheck[1]) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "CSR Level" }));
		}

		if (invalidNACsrCategoryCodes != null && invalidNACsrCategoryCodes.size() > 0) {
			errorList.add(MSG_NATIVE_AMR_CSR + nativeAmericancsrAplhaCategoryCode.toString() + " " + invalidNACsrCategoryCodes.toString());
		}

		if (invalidNonNACsrCategoryCodes != null && invalidNonNACsrCategoryCodes.size() > 0) {
			errorList.add(MSG_NON_NATIVE_AMR_CSR + nonNativeAmericancsrAplhaCategoryCode.toString() + " " + invalidNonNACsrCategoryCodes.toString());
		}
	}

	private PersonType extractSignerApplicantId(AccountTransferRequestPayloadType request) {
		return (PersonType) request.getInsuranceApplication().getSSFSigner().getRoleOfPersonReference().getRef();
	}

	private boolean isEligible(EligibilityType eligibilityType) {
		return (eligibilityType.getEligibilityIndicator() != null && eligibilityType.getEligibilityIndicator().isValue());
	}

	private boolean eligibilityDateRange(EligibilityType eligibilityType,
			Set<DateTime> eligibilityStartDates, Set<DateTime> eligibilityEndDates) {
		boolean blnDateRange = false;
		if (eligibilityType.getEligibilityDateRange() != null) {
			blnDateRange = eligibilityType.getEligibilityDateRange().getStartDate() != null
					&& isValidDate(eligibilityType.getEligibilityDateRange().getStartDate().getDate());

			if (blnDateRange){
				eligibilityStartDates.add(getDate(eligibilityType.getEligibilityDateRange().getStartDate().getDate()));
			}
			blnDateRange = blnDateRange && eligibilityType.getEligibilityDateRange().getEndDate() != null && isValidDate(eligibilityType.getEligibilityDateRange().getEndDate().getDate());
			if (blnDateRange){
				eligibilityEndDates.add(getDate(eligibilityType.getEligibilityDateRange().getEndDate().getDate()));
			}
			blnDateRange = blnDateRange && (ReferralUtil.compareDate(ReferralUtil.extractDate(eligibilityType.getEligibilityDateRange().getStartDate()), ReferralUtil.extractDate(eligibilityType.getEligibilityDateRange().getEndDate())) >= 0);
		}

		return blnDateRange;
	}

	private boolean validateAptcDetails(EligibilityType eligibilityType, String applicantId, boolean isSigner, List<String> errorList, boolean aptcCheck[], Set<String> reasonCode,
			Set<DateTime> eligibilityStartDates, Set<DateTime> eligibilityEndDates) {
		final boolean blnEligible = isEligible(eligibilityType);

		if (blnEligible) {
			aptcCheck[0] = true;
		}
		/* For APTC, Date check for all members irrespective of eligibility */
		final boolean blnDateRange = eligibilityDateRange(eligibilityType, eligibilityStartDates, eligibilityEndDates);

		if (!blnDateRange) {
			errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA, new Object[] { "APTC Eligibility Date Range", applicantId }));
		}

		if (eligibilityType.getEligibilityReasonText() != null && eligibilityType.getEligibilityReasonText().getValue() != null) {
			reasonCode.add(eligibilityType.getEligibilityReasonText().getValue());
		} else {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT, new Object[] { "APTC Eligibility Reason Text", applicantId }));
		}

		if (isSigner) {
			final APTCEligibilityType aptc = (APTCEligibilityType) eligibilityType;

			final BigDecimal maxAPTCAmount = aptc != null ? aptc.getAPTC() != null ? aptc.getAPTC().getAPTCMaximumAmount() != null ? aptc.getAPTC().getAPTCMaximumAmount().getValue() : null : null : null;
			if (maxAPTCAmount != null && maxAPTCAmount.doubleValue() >= 0) {
				aptcCheck[1] = true;
			}
		}

		return blnEligible;
	}

	private void validateCSRAlphaCategoryCodes(EligibilityType eligibilityType, List<String> errorList, Set<String> nativeAmericancsrAplhaCategoryCode, Set<String> nonNativeAmericancsrAplhaCategoryCode, List<String> invalidCsrCategoryCodes,
	        List<String> invalidNonNACsrCategoryCodes) {

		CSREligibilityType csr = (CSREligibilityType) eligibilityType;
		final String csrLevel = csr != null ? csr.getCSRAdvancePayment() != null ? csr.getCSRAdvancePayment().getCSRCategoryAlphaCode() != null ? csr.getCSRAdvancePayment().getCSRCategoryAlphaCode().getValue() : null : null : null;
		if (csrLevel != null) {
			if (NativeAmericanCSREnum.value(csrLevel) != null) {
				if (nativeAmericancsrAplhaCategoryCode.size() == 0) {
					nativeAmericancsrAplhaCategoryCode.add(csrLevel);
				} else if (!(nativeAmericancsrAplhaCategoryCode.contains(csrLevel))) {

					invalidCsrCategoryCodes.add(csrLevel);
				}
			}

			if (NonNativeAmericanCSREnum.value(csrLevel) != null) {
				if (nonNativeAmericancsrAplhaCategoryCode.size() == 0) {
					nonNativeAmericancsrAplhaCategoryCode.add(csrLevel);
				} else if (!(nonNativeAmericancsrAplhaCategoryCode.contains(csrLevel))) {
					invalidNonNACsrCategoryCodes.add(csrLevel);
				}
			}

		}
	}

	private boolean validateCsrDetails(EligibilityType eligibilityType, String applicantId, List<String> errorList, boolean csrCheck[],
			Set<DateTime> eligibilityStartDates, Set<DateTime> eligibilityEndDates) {
		final boolean blnEligible = isEligible(eligibilityType);
		final CSREligibilityType csr = (CSREligibilityType) eligibilityType;
		if (blnEligible) {
			final String csrLevel = csr != null ? csr.getCSRAdvancePayment() != null ? csr.getCSRAdvancePayment().getCSRCategoryAlphaCode() != null ? csr.getCSRAdvancePayment().getCSRCategoryAlphaCode().getValue() : null : null : null;
			csrCheck[0] = true;
			if (csrLevel != null) {
				csrCheck[1] = true;
				if (CSR_LIST.indexOf(csrLevel) == -1) {
					errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA, new Object[] { "CSR Level", applicantId }));
				}
			}
			/* For CSR, Date check for all members only if their eligibility is true, as DHW is not sending this info for in-eligible members */
			final boolean blnDateRange = eligibilityDateRange(eligibilityType, eligibilityStartDates, eligibilityEndDates);

			if (!blnDateRange) {
				errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA, new Object[] { "CSR Eligibility Date Range", applicantId }));
			}
		}

		return blnEligible;
	}

	@Override
	boolean isZipValidationApplicable(String state) {
		return StringUtils.equalsIgnoreCase(state, IDAHO);
	}

}
