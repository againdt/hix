package com.getinsured.hix.platform.location;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.location.service.AddressValidatorService;

/**
 * @author polimetla_b
 * @since Oct-31-2012
 */
public class AddressValidatorOEDQTest extends BaseTest {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AddressValidatorOEDQTest.class);

	//ApplicationContext context = null;
	AddressValidatorService av = null;

	public AddressValidatorOEDQTest() {
		//		System.setProperty("GHIX_HOME", "C:/workspace_ghix/");
		//		context = new ClassPathXmlApplicationContext("/applicationContext.xml");
		av = (AddressValidatorService) context.getBean("addressValidator");
	}

	@Test
	public void testValidateSingleAddress() {

		LOGGER.info("============ testValidateSingleAddress Begin =============");

		Location address = new Location();
		address.setAddress1("3200 E camelback rd");
		address.setState("CA");
		address.setZip("12345");
		av.validateAddress(address);
	}
}
