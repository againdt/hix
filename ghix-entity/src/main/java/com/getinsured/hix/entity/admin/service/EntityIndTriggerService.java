package com.getinsured.hix.entity.admin.service;

import com.getinsured.hix.model.entity.EnrollmentEntity;

public interface EntityIndTriggerService {
	public String triggerInd35(EnrollmentEntity entity);
}
