package com.getinsured.eligibility.at.resp.si.dto;

import java.util.Date;

public class Verification {

	public enum VERIFICATION_TYPE {SSN, CITIZENSHIP, ELIGIBLE_IMMIGRATION_STATUS, ANNUAL_INCOME, CURRENT_INCOME, INCARCERATION_STATUS, ESI_MEC, NON_ESI_MEC}

	private Long id;

	private String type;

	private String status;

	private Date startDate;

	private Date endDate;

	private String source;

	private String resultText;

	private Long ssapApplicantId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getResultText() {
		return resultText;
	}

	public void setResultText(String resultText) {
		this.resultText = resultText;
	}

	public Long getSsapApplicantId() {
		return ssapApplicantId;
	}

	public void setSsapApplicantId(Long ssapApplicantId) {
		this.ssapApplicantId = ssapApplicantId;
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

}
