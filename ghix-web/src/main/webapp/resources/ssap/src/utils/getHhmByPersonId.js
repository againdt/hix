export const getHhmByPersonId = (personId, hhms) => {
    return hhms.filter(hhm => hhm.personId === personId)[0];
};