
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for a name by which a person is known, referred, or addressed.
 * 
 * <p>Java class for PersonNameTextType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonNameTextType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://niem.gov/niem/niem-core/2.0>ProperNameTextType">
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonNameTextType")
public class PersonNameTextType
    extends ProperNameTextType
{


}
