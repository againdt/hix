<%@page import="com.getinsured.hix.platform.startup.*"%>
<%@include file="datasource.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>"/>

<%! private SystemInfoDAO systemInfo = null; %>
<%
	if(request.getSession().getAttribute("records") == null){
		systemInfo = (SystemInfoDAO) ctx.getBean("appStartupBeanDao");
		List<SystemInfoRec> records = systemInfo.loadSystemInfo();
		request.getSession().setAttribute("records", records );
	}
%>
<h3>System Information</h3>
<display:table name="${sessionScope.records}"  requestURI="sysinfo" id="table" style="white-space: pre-wrap;" export="true" pagesize="1000" defaultsort="1"> 
	
	<display:column property="group" title="Group" sortable="true" />
	<display:column property="attribute" title="Attribute" sortable="true" />
	<display:column property="value" title="Value" sortable="true" />
	<display:column property="errorMessage" title="Error Message" sortable="true" />
	<display:column property="threshold" title="Threshold" sortable="true" />
	<display:setProperty name="export.xml.filename" value="systemInfo.xml"/>
	<display:setProperty name="export.csv.filename" value="systemInfo.csv"/>
	<display:setProperty name="export.excel.filename" value="systemInfo.xls"/>
	
</display:table>

