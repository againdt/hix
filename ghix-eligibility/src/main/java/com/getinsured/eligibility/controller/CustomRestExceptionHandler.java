package com.getinsured.eligibility.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.hateoas.VndErrors;
import org.springframework.hateoas.VndErrors.VndError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.getinsured.eligibility.exception.DataNotFoundException;
import com.getinsured.eligibility.exception.InvalidIncomingValueException;
import com.getinsured.eligibility.exception.InvalidStateException;

@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
@ResponseBody
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, 
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
        Throwable mostSpecificCause = ex.getMostSpecificCause();
        VndError e;
        if (mostSpecificCause != null) {
            String exceptionName = mostSpecificCause.getClass().getName();
            String message = mostSpecificCause.getMessage();
            e = new VndError(exceptionName, message);
        } else {
        	 e = new VndError(ex.getMessage(), ex.getMessage());
        }
        return new ResponseEntity<Object>(new VndErrors(e), status);
    }

	@Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, 
    		HttpHeaders headers, HttpStatus status, WebRequest request) {
		
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();
        
        return new ResponseEntity<>(processFieldErrors(fieldErrors, globalErrors), HttpStatus.BAD_REQUEST);
    }


	private VndErrors processFieldErrors(List<FieldError> fieldErrors, List<ObjectError> globalErrors) {
		
		List<VndError> errorList = new ArrayList<VndErrors.VndError>(fieldErrors.size() + globalErrors.size());

		String error;
		for (FieldError fieldError : fieldErrors) {
            error = fieldError.getField() + ", " + fieldError.getDefaultMessage();
            VndError e = new VndError(error, error);
            errorList.add(e);
        }
		
		for (ObjectError objectError : globalErrors) {
            error = objectError.getObjectName() + ", " + objectError.getDefaultMessage();
            VndError e = new VndError(error, error);
            errorList.add(e);
        }
		
		return new VndErrors(errorList);
	}
	
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(InvalidIncomingValueException.class)
	public ResponseEntity<VndErrors> invalidIncomingValuesException(InvalidIncomingValueException e) {
		return new ResponseEntity<VndErrors>(error(e, e.getMessage() + " "), HttpStatus.BAD_REQUEST);
	}
	
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity<VndErrors> dataNotFoundException(DataNotFoundException e) {
		return new ResponseEntity<VndErrors>(error(e, e.getMessage() + " "), HttpStatus.NOT_FOUND);
	}
	
	@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler(InvalidStateException.class)
	public ResponseEntity<VndErrors> unprocessableException(InvalidStateException e) {
		return new ResponseEntity<VndErrors>(error(e, e.getMessage() + " "), HttpStatus.UNPROCESSABLE_ENTITY);
	}

	private <E extends Exception> VndErrors error(E e, String logref) {
		String msg = Optional.of(e.getMessage()).orElse(e.getClass().getSimpleName());
		return new VndErrors(logref, msg);
	}
	
}