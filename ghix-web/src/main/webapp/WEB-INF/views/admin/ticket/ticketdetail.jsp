<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<script type="text/javascript">
$('.my-link').click(function(e) { e.preventDefault(); });
</script>
<!-- Cancel ticket JSP to add confirm dialog and cancel ticket functionality  -->

<%

String timeZone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);
%>

<!-- Cancel ticket JSP ends -->
<div class="gutter10">
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
                    <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                    <li><a href="<c:url value="/ticketmgmt/ticket/ticketlist/" />"><spring:message  code="lbl.viewTickets"/></a></li>
                    <li><spring:message  code="lbl_tk.summary"/></li>
                </ul><!--page-breadcrumb ends-->
                <%-- <c:out value="${TICKET_STATUS}"></c:out> --%>
               
        <div class="row-fluid">
			<h1>${tkmTicketsObj.subject}</h1>
			<div class="row-fluid">
				<jsp:include page="ticketQuickActionBar.jsp">
    				<jsp:param value="${tkmTicketsObj.id}" name="ticketId"/>
    				<jsp:param value="${tkmTicketsObj.tkmWorkflows.type}" name="ticketType"/>
                    <jsp:param value="${tkmTicketsObj.tkmWorkflows.category}" name="ticketCategory"/>
    			</jsp:include>
			</div>
		</div>
	</div>
    <div class="row-fluid">
    	<div class="span3" id="sidebar">	
            <c:set var="tkmTicketsObj" value="${tkmTicketsObj}" scope="request" />
    	<jsp:include page="ticketsummary.jsp">
    		<jsp:param value="${tkmTicketsObj.id}" name="ticketId"/>
            <jsp:param value="tkmTicketsObj" name="ticketCategory" />
            <jsp:param value="../ticketlist?pageNumber=${gotoPageNumber}" name="backButtonUrl"/>
    	</jsp:include>
		</div>
        <div class="span9" id="rightpanel">	
            <div style="font-size: 14px; color: red" id="tkmErrorDiv">
					<c:if test="${errorMsg != null && errorMsg != ''}">
						<p><c:out value="${errorMsg}"></c:out><p/>
						<c:remove var="errorMsg"/>
					</c:if>
				</div>	
		        <div class="header">
					<h4 class="pull-left" id="pageTitle"><spring:message  code="header.ticketSummary"/></h4>
					<c:choose>
		              	<c:when test="${TICKET_STATUS =='Completed' || TICKET_STATUS=='Canceled' || TICKET_STATUS=='Resolved'}"/>
		              	<c:otherwise>
		              	    <c:set var="encTicketid" ><encryptor:enc value="${tkmTicketsObj.id}" isurl="true"/> </c:set>
							<a class="btn btn-small pull-right" id="editBtnDiv" href="<c:url value="/ticketmgmt/ticket/edit/${encTicketid}"/>"><spring:message  code="btn_tkt.edit"/></a>
						</c:otherwise>
					</c:choose>
				</div>	
                <div class="row-fluid">
                	<form method="POST" action="#" name="frmIssuerAcctComProInfo" id="frmIssuerAcctComProInfo" class="form-horizontal">
                	<df:csrfToken/>
                        <div class="control-group margin0">
                            <label class="control-label"><spring:message  code="lbl.ticketNumber"/></label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.number} </strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label"><spring:message  code="label.subject"/></label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.subject} </strong></div>
                        </div><!--control-group ends-->                        
                        <div class="control-group margin0">
                            <label class="control-label"><spring:message  code="label.ticketType"/></label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.tkmWorkflows.type}</strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label">Created For</label>
                            <div class="controls paddingT5">
                            <c:set var="encryptedId" ><encryptor:enc value="${tkmTicketsObj.moduleId}" isurl="true"/> </c:set>
                           <c:choose>
                           
								<c:when test="${tkmTicketsObj.moduleName == 'HOUSEHOLD' }"> 
								
                            	<%-- <strong>${createdForName} </strong> --%>
                            	<a href="<c:url value="/crm/member/viewmember/${encryptedId}"/>"><strong>${createdForName} </strong></a>
                            	 </c:when>
                            	<c:otherwise>
                            	<strong>${tkmTicketsObj.role.firstName} ${tkmTicketsObj.role.lastName}</strong>
                            	<%-- <strong>${tkmTicketsObj.role.firstName} ${tkmTicketsObj.role.lastName}</strong> --%>
                            	</c:otherwise>
							</c:choose> 
                            </div>
                        </div><!--control-group ends-->
                        
                        
                         
                         <div class="control-group margin0">
                         	<label class="control-label"><spring:message  code="label.requester"/></label>
	                         <div class="controls paddingT5">
	                         	<strong>${tkmTicketsObj.creator.firstName} ${tkmTicketsObj.creator.lastName}</strong>
	                         </div>
                         </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label"><spring:message  code="lbl.dateOfRequest"/></label>
                            <div class="controls paddingT5"><strong><fmt:formatDate	value="${tkmTicketsObj.created}" pattern="MM-dd-yyyy hh:mm aa" timeZone="<%=timeZone%>" /></strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label"><spring:message  code="label.priority"/></label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.priority}</strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label"><spring:message  code="lbl.queueAssigned"/></label>
                            <div class="controls paddingT5"><strong>${tkmTicketsObj.tkmQueues.name}</strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label"><spring:message  code="lbl_tkt.status"/></label>
                            <div class="controls paddingT5"><strong><c:out value="${tkmTicketsObj.status}"/></strong></div>
                        </div><!--control-group ends-->
                        <div class="control-group margin0">
                            <label class="control-label"><spring:message  code="lbl_tkt.description"/></label>
                            <div class="controls paddingT5"><strong><textarea id="description" readonly style="width:425px; height:120px;background-color:white;cursor:default; font-weight:bold;"><c:out value="${tkmTicketsObj.description}"/></textarea></strong></div>
                        </div><!--control-group ends-->
                        
                    </form>
                </div>
        </div>
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->


<script type="text/javascript">

$(document).ready(function() {
	$("#ticketdetail").addClass("active navmenu");
});
</script>
