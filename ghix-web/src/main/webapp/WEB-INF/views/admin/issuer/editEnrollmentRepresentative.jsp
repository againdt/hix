<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="label.manageRepresentatives"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1 id="skip">${issuerObj.name}</h1>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                   	<li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>"><spring:message  code="pgheader.issuerDetails"/></a></li>
		            <li class="active"><a href="<c:url value="#"/>"><spring:message  code="pgheader.issuerRepresentative"/></a></li>
		             <c:if test="${exchangeType != 'PHIX' && exchangeState != 'ID' && exchangeState != 'MN'}">
		             	<li><a href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>"><spring:message  code="pgheader.financialInformation"/></a></li>
		             </c:if>
		             <li><a href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>"><spring:message  code="pgheader.update.companyProfile"/></a></li>
		             <li><a href="<c:url value="/admin/issuer/market/individual/profile/${encIssuerId}"/>"><spring:message  code="pgheader.individualMarketProfile"/></a></li>
		            <%--  <c:if test="${exchangeState != 'ID'}">
		             	<li><a href="<c:url value="/admin/issuer/market/shop/profile/${encIssuerId}"/>"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
		             </c:if> --%>
		             <li><a href="<c:url value="/admin/issuer/accreditationdocument/view/${encIssuerId}" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
		             <li><a href="<c:url value="/admin/issuer/certification/status/${encIssuerId}"/>"><spring:message  code="pgheader.certificationStatus"/></a></li>
		 			<li><a href="<c:url value="/admin/issuer/history/${encIssuerId}"/>"><spring:message  code="pgheader.issuerHistory"/></a></li>
		 			<li><a href="<c:url value="/admin/issuer/crossWalkStatus/${encIssuerId}"/>"><spring:message  code="pgheader.crossWalkStatus"/></a></li>
		 			<c:if test="${exchangeState == 'ID'}">
		 				<li><a href="<c:url value="/admin/issuer/displayNetworkTransparencyData/${encIssuerId}"/>"><spring:message code="pgheader.networkTransparencyStatus" /></a></li>
		 			</c:if>
		 			 <c:if test="${activeRoleName == 'OPERATIONS'}">
						<li><a href="<c:url value="/admin/issuer/issuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="pgheader.IssuerPaymentInfo"/></a></li>
					</c:if>
                </ul>
		</div>
			<!-- end of span3 -->
            <div class="span9" id="rightpanel">
           		<form class="form-horizontal" id="frmIssuerEditRep" name="frmIssuerEditRep" action="<c:url value="/admin/issuer/enrollmentrepresentative/update" />" method="POST">
           			<df:csrfToken/>
                	<input type="hidden" id="issuerRepId" name="issuerRepId" value="<encryptor:enc value="${representativeObj.id}"></encryptor:enc>"/>
                     
                     <div class="header">
                         <h4 class="pull-left"><spring:message code="label.repInfo"/></h4>
						 <input type="submit" class="btn btn-primary btn-small pull-right margin5-lr ie-saveBtn" value="Save" onclick="javascript:submitForm();"/>
                         <a class="btn btn-small pull-right" href='<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>'><spring:message  code="label.btnCancel"/></a>
                     </div>
                     <div class="gutter10">
     
	                      <div class="control-group">
	                          <label class="control-label"><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                          <div class="controls">
	                            <input type="text" name="firstName" id="firstName" value="${repUser.firstName}" class="input-large" maxlength="50">
	                              <c:if test="${isIssuerRepDuplicate == 'true'}">
	                              <div class="error">
	                                   <label class="error"><span><spring:message  code="err.duplicateRepresentative"/></span></label>
	                              </div> 
	                              </c:if>
	                            <div id="firstName_error" class=""></div>
	                          </div>
	                      </div>
	                      <div class="control-group">
	                          <label class="control-label"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
	                          <div class="controls"><input type="text" name="lastName" id="lastName" value="${repUser.lastName}" class="input-large" maxlength="50">
	                              <div id="lastName_error" class=""></div>
	                          </div>
	                      </div>
	                             
	                     <div class="control-group">
	                         <label class="control-label"><spring:message  code="label.title"/></label>
	                         <div class="controls"><input type="text" name="title" id="title" value="${representativeObj.title}" class="input-large" maxlength="15">	
	                             <div id="title_error" class=""></div>
	                         </div>
	                     </div>
	                             
						<div class="control-group">
	                                 <label class="control-label"><spring:message  code="label.phoneNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
	                            <input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="input-mini" /> 
								<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" /> 
								<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-small" /> 
	                            <input type="hidden" name="phone" id="phone" class="input-large">
	                            <div id="" class=""></div><div id="" class=""></div><div id="phone3_error" class=""></div>
							</div>
						</div>
			                            
						<div class="control-group">
								<label class="control-label"><spring:message  code="label.emailAddress"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<c:choose>
								<c:when test="${isFromUser eq 'USER'}">
									<div class="controls"><input type="text" name="email" id="email" value="${repUser.email}" class="input-large" readonly="true" maxlength="50">
										<div id="email_error" class=""></div>
									</div>
									</c:when>
								<c:otherwise>
									<div class="controls"><input type="text" name="email" id="email" value="${representativeObj.email}" class="input-large" <c:if test="${representativeObj.userRecord != null}"> readonly="true"</c:if> maxlength="50">
											<div id="email_error" class=""></div>
											<c:if test="${repDuplicateEmail == 'true'}">
                                        	<div class="error">
                                               <label class="error"><span><spring:message  code="err.duplicateEmail"/></span></label>
                                        	</div> 
                                        	</c:if>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
                    </div>
                </form>	
        	</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
<script type="text/javascript">

function submitForm(){
		$("#frmIssuerEditRep").submit();
}  


var validator = $("#frmIssuerEditRep").validate({ 
	rules : {
		firstName : {required : true},
		lastName : {required : true},
		phone1 : {required : true, digits: true},
		phone2 : {required : true, digits: true},
		phone3 : {required : true, digits: true, minlength:4, maxlength:4, phoneUS:true},
		email : { required : true, emailIdCheck: true}
	},
	messages : {
		firstName : { required : "<span><em class='excl'>!</em><spring:message  code='err.firstName' javaScriptEscape='true'/></span>"},
		lastName: { required : "<span><em class='excl'>!</em><spring:message  code='err.lastName' javaScriptEscape='true'/></span>"},
		phone3 : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
			digits : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
          minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>",
          maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneRequired' javaScriptEscape='true'/></span>"
  		},
		email : { required : "<span><em class='excl'>!</em><spring:message  code='err.email' javaScriptEscape='true'/></span>",
				  emailIdCheck : "<span><em class='excl'>!</em><spring:message  code='err.invalidEmail' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});


//phone number validation
$.validator.addMethod("phoneUS", function(value, element, param) {
	if($.browser.msie==true && $.browser.version=='8.0'){
		phoneNumber1 = $("#phone1").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone2").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone3").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone1").val().trim();
		phoneNumber2 = $("#phone2").val().trim();
		phoneNumber3 = $("#phone3").val().trim();
	}
	var phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
	
	if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length==10){
		$("#phone").val(phoneNumber1+phoneNumber2+phoneNumber3);
		return true;
	}else{
		if(isNaN(phoneNumber1) || phoneNumber1.length!=3){
			$('#phone1').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone1').removeClass('input-mini error').addClass('input-small valid');
		}
		if(isNaN(phoneNumber2) || phoneNumber2.length!=3){
			$('#phone2').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone2').removeClass('input-mini error').addClass('input-small valid');
		}
		if(isNaN(phoneNumber3) || phoneNumber3.length!=4){
			$('#phone3').removeClass('input-mini valid').addClass('input-small error');
		}else{
			$('#phone3').removeClass('input-mini error').addClass('input-small valid');
		}
		return false;
	}	
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

$.validator.addMethod("emailIdCheck", function(value, element) {
	var emailRegEx = new RegExp("^[_A-Za-z0-9\u002e_]+([A-Za-z0-9\u002e]+)@[A-Za-z0-9]+([\u002e][a-zA-Z]{2,})+$");
	if("" != value && !emailRegEx.test(value)){
		return false;
	}else{
		return true;
	}
});
</script>		
