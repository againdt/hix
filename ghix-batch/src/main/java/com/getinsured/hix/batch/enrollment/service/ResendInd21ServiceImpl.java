package com.getinsured.hix.batch.enrollment.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.admin.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.enrollment.SendUpdatedEnrolleeResponseDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleeUpdateSendStatusRepository;
import com.getinsured.hix.enrollment.service.EnrolleeAuditService;
import com.getinsured.hix.enrollment.service.EnrolleeService;
import com.getinsured.hix.enrollment.service.EnrollmentService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.EnrolleeUpdateSendStatus;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

@Service("resendInd21Service")
@Transactional
public class ResendInd21ServiceImpl implements ResendInd21Service {

	private static final Logger LOGGER = Logger.getLogger(ResendInd21ServiceImpl.class);

	@Autowired 
	private RestTemplate restTemplate;

	@Autowired 
	private EnrollmentService enrollmentService;

	@Autowired
	private EnrolleeService enrolleeService;
	
	@Autowired 
	private EnrolleeAuditService enrolleeAuditService;
	
	@Autowired 
	private UserService userService;

	@Autowired
	private JobService jobService;
	
	@Autowired private IEnrolleeUpdateSendStatusRepository enrolleeUpdateSendStatusRepository;

	@Value("#{configProp['enrollment.resendIND21FilePath']}")
	private String resendIND21File;

	public EnrollmentService getEnrollmentService() {
		return enrollmentService;
	}

	public void setEnrollmentService(EnrollmentService enrollmentService) {
		this.enrollmentService = enrollmentService;
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public String getResendIND21File() {
		return resendIND21File;
	}

	public void setResendIND21File(String resendIND21File) {
		this.resendIND21File = resendIND21File;
	}


	@Override
	public Map<Integer, String> sendCarrierUpdatedDataToAHBX(long jobExecutionId) throws Exception{
		LOGGER.info("ResendInd21Service :: sendCarrierUpdatedDataToAHBX SERVICE :: START");
		Map<Integer, String> errorMap = new HashMap<Integer, String>();
		List<String>  enrollmentIdList = null;
		try {
			// Read CSV files for enrollmentId's
			enrollmentIdList = readCSVFileToGetEnrollments();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.error("Error reading file " + getResendIND21File());
			throw new GIException("Error reading file " + getResendIND21File(),e);
		}

		Map<String,Object> reqParam = new HashMap<String,Object>();
		List<Map<String,Object>> planList = new ArrayList<Map<String,Object>>();
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
		if(enrollmentIdList != null){
			for(String enrollmentIdStr : enrollmentIdList){
				String batchJobStatus=null;
				if(jobService != null && jobExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
				}
				
				Integer enrollmentId;
				try{
					enrollmentId = Integer.parseInt(enrollmentIdStr);
					Enrollment enrollment = getEnrollmentService().findById(enrollmentId);
					// Don't send aborted enrollments
					if(enrollment != null && enrollment.getEnrollmentStatusLkp() != null && !enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED)){
						/*List<EnrolleeAud> enrolleeList = enrolleeAuditService.getCarrierUpdatedEnrollmentsForResendInd21(enrollmentId, user);*/
						List<Integer> enrolleeAudIdList = enrolleeAuditService.getEnrolleeAudIdForResendInd21(enrollmentId, user);
						if(enrolleeAudIdList != null){
							for(Integer enrolleeAudId : enrolleeAudIdList){
								try{
									List<EnrolleeAud> enrolleeAudList = enrolleeAuditService.getEnrolleeAudForCarrierUpdatedEnrollmentsByEnrolleeId(enrollmentId, user, enrolleeAudId);
									for(EnrolleeAud enrollee : enrolleeAudList){
										Map<String,Object> planMap = new HashMap<String,Object>();

										planMap.put(EnrollmentConstants.CASE_ID, enrollment.getHouseHoldCaseId());
										planMap.put(EnrollmentConstants.ENROLLMENT_ID, enrollment.getId());
										planMap.put(EnrollmentConstants.PLAN_ID, enrollment.getPlanId());
										planMap.put(EnrollmentConstants.CMSPLAN_ID, enrollment.getCMSPlanID());
										LOGGER.info("ResendInd21Service :: sendCarrierUpdatedDataToAHBX SERVICE : Enrollment not found for the enrolle updatedby carrier");

										planMap.put(EnrollmentConstants.ENROLLEE_ID, enrollee.getId());
										planMap.put(EnrollmentConstants.POLICY_ID, enrollee.getHealthCoveragePolicyNo());

										if(enrollee.getEffectiveStartDate()!=null){
											planMap.put(EnrollmentConstants.EFFECTIVE_START_DATE, DateUtil.dateToString(enrollee.getEffectiveStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
										}else{
											planMap.put(EnrollmentConstants.EFFECTIVE_START_DATE,"");
										}
										if(enrollee.getEnrolleeLkpValue()!=null 
												&& (enrollee.getEnrolleeLkpValue().getLookupValueCode()!=null 
												&& (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) 
														|| enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)))){
											if(enrollee.getEffectiveEndDate()!=null){
												planMap.put(EnrollmentConstants.EFFECTIVE_END_DATE, DateUtil.dateToString(enrollee.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
											}else{
												planMap.put(EnrollmentConstants.EFFECTIVE_END_DATE,"");
											}
										}
										planMap.put(EnrollmentConstants.STATUS, enrollee.getEnrolleeLkpValue().getLookupValueCode());
										planMap.put(EnrollmentConstants.MEMBER_ID, enrollee.getExchgIndivIdentifier());
										if(enrollee.getLastPremiumPaidDate()!=null){
											planMap.put(EnrollmentConstants.LAST_PREMIUM_PAID_DATE, DateUtil.dateToString(enrollee.getLastPremiumPaidDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
										}else{
											planMap.put(EnrollmentConstants.LAST_PREMIUM_PAID_DATE,"");
										}
										if(enrollee.getEnrollment() != null && enrollee.getEnrollment().getEnrollmentConfirmationDate() != null){
											planMap.put(EnrollmentConstants.ENROLLMENT_CONFIRMATION_DATE, DateUtil.dateToString(enrollee.getEnrollment().getEnrollmentConfirmationDate(), GhixConstants.REQUIRED_DATE_FORMAT));
										}
										else{
											planMap.put(EnrollmentConstants.ENROLLMENT_CONFIRMATION_DATE, "");
										}
										
										if(enrollee.getLastEventId()!=null && 
												(enrollee.getLastEventId().getEventTypeLkp()!=null 
												&& EnrollmentConstants.EVENT_TYPE_CANCELLATION.equalsIgnoreCase(enrollee.getLastEventId().getEventTypeLkp().getLookupValueCode()) )){
											
											if(enrollee.getLastEventId()!=null && enrollee.getLastEventId().getEventReasonLkp()!=null){
												planMap.put("maintenanceReasonCode", enrollee.getLastEventId().getEventReasonLkp().getLookupValueCode());
											}
										}
										LOGGER.info("ResendInd21Service :: sendCarrierUpdatedDataToAHBX SERVICE : IND 21 INPUT : "+planMap.toString());
										planList.add(planMap);
									}//End of Inner loop
								}
								catch(Exception ex){
									LOGGER.error("resendInd21 SERVICE : Failed for Enrollee :: " +enrolleeAudId +" Exception: ", ex);
									errorMap.put(enrolleeAudId,  ex.getMessage() != null ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
								}
							}//end of Outer loop
						}
					}
					else{
						LOGGER.info("Enrollment in Aborted state. Enrollment ID :- " + enrollmentIdStr);
					}
				}
				catch(NumberFormatException numberFormatException){
					LOGGER.error("ResendInd21 Failed for Enrollment :- " + enrollmentIdStr);
					LOGGER.error("Invalid Enrollment ID" + enrollmentIdStr);
				}
				catch (Exception e) {
					LOGGER.error("ResendInd21 Failed for Enrollment :- " + enrollmentIdStr);
					LOGGER.error(e.getMessage());
				}
			}
			reqParam.put("planList", planList);
			LOGGER.info("ResendInd21Service :: sendCarrierUpdatedDataToAHBX SERVICE : Calling IND21 ");
			String verifyResponse = restTemplate.postForObject(GhixEndPoints.AHBXEndPoints.ENROLLMENT_IND21_CALL_URL, reqParam, String.class);
			LOGGER.info("ResendInd21Service :: sendCarrierUpdatedDataToAHBX SERVICE : verifyResponse from IND21 = " + verifyResponse);

			XStream xStream = GhixUtils.getXStreamStaxObject();
			EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xStream.fromXML(verifyResponse);
			if(enrollmentResponse!=null && (enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList() !=null && enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList().size()>0)){
				List<SendUpdatedEnrolleeResponseDTO> sendUpdatedEnrolleeResponseDTOList =enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList();
				enrolleeService.saveCarrierUpdatedDataResponse(sendUpdatedEnrolleeResponseDTOList, null);
			}
		}
		LOGGER.info("ResendInd21Service :: sendCarrierUpdatedDataToAHBX SERVICE : END");
		return errorMap;
	}
	
	@Override
	@Transactional
	public void updateEnrolleeUpdateSendStatusForFailure(Map<Integer, String> errorMap){
		if(errorMap != null && !errorMap.isEmpty()){
			try{
				for(Integer enrolleeId : errorMap.keySet()){
					EnrolleeUpdateSendStatus enrolleeUpdateSendStatusFail = new EnrolleeUpdateSendStatus();
					Enrollee enrollee = new Enrollee();
					enrollee.setId(enrolleeId);
					enrolleeUpdateSendStatusFail.setEnrollee(enrollee);
					enrolleeUpdateSendStatusFail.setAhbxStatusCode("ERR01");
					enrolleeUpdateSendStatusFail.setAhbxStatusDesc("Unable to send update to AHBX : "+errorMap.get(enrolleeId));
					enrolleeUpdateSendStatusRepository.save(enrolleeUpdateSendStatusFail);
				}		
			}
			catch(Exception ex){
				LOGGER.error("Failed to update enrolleeUpdateSendStatus Exception: ", ex);
			}
		}
	}

	private List<String> readCSVFileToGetEnrollments() throws GIException{
		List<String>  enrollmentIdList = new ArrayList<String>();
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(getResendIND21File()));
			while ((line = br.readLine()) != null) {
				List<String> tempStringList = null;

				tempStringList = (Arrays.asList(line.split(cvsSplitBy)));
				enrollmentIdList.addAll(tempStringList);
			}

		} catch (Exception e) {
			throw new GIException("Error in ResendInd21Service :: readCSVFileToGetEnrollments SERVICE :"+e.getMessage(),e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					LOGGER.error(e.getMessage());
				}
			}
		}
		return enrollmentIdList;
	}
}
