<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<style>
#submitButton{
	margin: 7px !important;
}
</style>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/shopstyles.css"/>" />

<script type="text/javascript">
function submitForm(customEffectiveDate) {
	if(customEffectiveDate == 'YES'){
		$("#frmcoveragedate").attr('action',"<c:url value='/shop/employer/plandisplay/coveragestart'/>");
		$("#frmcoveragedate").attr('method',"POST");
	}else{
		$("#frmcoveragedate").attr('action',"<c:url value='/shop/employer/plandisplay/selectcoverage'/>");
		$("#frmcoveragedate").attr('method',"GET");
	}
	$("#frmcoveragedate").submit();
}
</script>
<style>
 #submitButton {
 	margin: 6px;
 }
</style>

<!--titlebar-->
<div class="gutter10">
<div class="row-fluid">
	<div class="span3" id="sidebar">
		<div class="accordion graysidebar" id="accordion2">
			<util:nav pageName="${page_name}"></util:nav>
		</div>
		<c:if test="${errorMsg == '' }">
			<util:selection pageName="${page_name}"></util:selection>
		</c:if>
	</div>
	<!--sidebar-->
	<div class="span9" id="rightpanel">
	<div class="header">
		<h4 id="skip" class="pull-left"><spring:message code = "label.gettingStarted"/></h4>
		<button class="btn" style="visibility:hidden"><i class="icon-chevron-left"></i> <spring:message code = "label.back"/></button>
		<%-- <input type="button" name="submitButton" id="submitButton" onClick="javascript:submitForm('${customEffectiveDate}');" class="btn btn-primary pull-right" value="<spring:message code = "label.next"/>" /> --%>
     	<a type="button" tabindex="0" name="submitButton" id="submitButton" onClick="javascript:submitForm('${customEffectiveDate}');" class="btn btn-primary pull-right"><spring:message code = "label.next"/></a>
     </div>
	<c:if test="${errorMsg != ''}">
		<p></p>
		<div class="row-fluid gutter10"> 
			<div style="font-size: 15px; color: red">
					&nbsp;<p><c:out value="${errorMsg}"></c:out><p/>
					<script type="text/javascript">$('#submitButton').hide(); </script>
				<br>
			</div>
		</div>
	</c:if>
<c:if test="${errorMsg == '' }">
		<div class="gutter10">
			<p><b><spring:message code = "label.selectPlanMsg"/></b></p>
			<p><spring:message code = "label.planGuideMsg"/></p>
			<ul>
				<li><spring:message code = "label.planGuideStep1"/></li>
				<li><spring:message code = "label.planGuideStep2"/></li>
			</ul>
            <p><b><spring:message code = "label.impText"/> </b></p>
            <p><spring:message code = "label.CheckInfo"/></p>
            <p><b><spring:message code = "label.helpMsg"/></b></p>
            <p><spring:message code = "label.helpMsgDesc"/>

            	
			<form id="frmcoveragedate" name="frmcoveragedate">
			<df:csrfToken/>
			<input type="hidden" name="pgname" value="coveragestart"/>    
				<c:if test="${dateList != null}" >
					<input type="hidden" name="coverageDateStart" id="coverageDateStart" value="${dateList[0]}" />
					<p> <spring:message code = "label.covEffectiveDt"/>:  <strong>${dateList[0]}</strong> </p>
				</c:if>
			</form>
		     <div id="activeEnrollment" class="modal hide fade" data-backdrop="static" tabindex="-1"  role="dialog" aria-labelledby="activeEnroll" aria-hidden="true">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
               </div>
               <div class="modal-body" id="activeEnrollmentMsg"><spring:message code = "label.covEffectiveMsg"/></div>
               <div class="modal-footer">
                 <input type="button" data-dismiss="modal" id="activeEnrollmentButton" class="btn btn-primary" value="OK" />
               </div>
             </div>
		</div>
	</c:if>
		<!--gutter10-->
	</div>
	<!--rightpanel-->
</div>
<!--row-fluid-->
</div>

<script type="text/javascript">
$(document).ready(function(){
	var isActiveEnrollExist = ${isActiveEnrollExist};
	if(isActiveEnrollExist == true){
		$("#activeEnrollment").modal('show');
	}
});
</script>