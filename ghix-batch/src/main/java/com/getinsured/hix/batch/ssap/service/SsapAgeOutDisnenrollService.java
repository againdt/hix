package com.getinsured.hix.batch.ssap.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import com.getinsured.iex.ssap.model.SsapApplication;


public interface SsapAgeOutDisnenrollService {
	
	public Map<String,String> disenrollQHP(Long applicationId);
	public void logData(Set<Future<Map<String,String>>> tasks);
	public List<Long> getSsapAppForAgeOut(Long coverageYear,String isFinancialAssistanceFlag,String applicationStatus);
}
