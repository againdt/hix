package com.getinsured.eligibility.at.sep.service;

import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.model.SepEvents;



public interface SepEventsService {
	
	List<SepEvents> findFinancialSepEvents();
	List<SepEvents> findNonFinancialSepEvents();
	Map<String, SepEvents> findFinancialSepEventsByCategory(String category);
	Map<String, SepEvents> findFinancialSepEventsByChangeType(String category);
	Map<String, SepEvents> findNonFinancialSepEventsByCategory(String category);
	Map<String, SepEvents> findNonFinancialSepEventsByChangeType(String changeType);
	SepEvents findSepEventById(Integer id);
	SepEvents findFinancialSepEventsByEventNameAndCategory(String name, String category);
}
