package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.FacilityContactInformationType;

/**
 * Wrapper class for FacilityContactInformationType
 * 
 * @author Nikhil Talreja
 * @since 05-Mar-2014
 */
public class FacilityContactInformationTypeWrapper implements JSONAware {

	private String personFullName;
	private String contactTelephoneNumber;
	private String contactFaxNumber;
	
	public FacilityContactInformationTypeWrapper(FacilityContactInformationType facilityContactInformationType){
		
		if(facilityContactInformationType == null){
			return;
		}
		
		this.personFullName = facilityContactInformationType.getPersonFullName();
		this.contactTelephoneNumber = facilityContactInformationType.getContactTelephoneNumber();
		this.contactFaxNumber = facilityContactInformationType.getContactFaxNumber();
	}

	public String getPersonFullName() {
		return personFullName;
	}

	public String getContactTelephoneNumber() {
		return contactTelephoneNumber;
	}

	public String getContactFaxNumber() {
		return contactFaxNumber;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("PersonFullName", this.personFullName);
		obj.put("ContactTelephoneNumber", this.contactTelephoneNumber);
		obj.put("ContactFaxNumber", this.contactFaxNumber);
		return obj.toJSONString();
	}
	
	
}
