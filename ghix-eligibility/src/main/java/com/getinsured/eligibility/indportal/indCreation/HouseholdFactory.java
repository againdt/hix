package com.getinsured.eligibility.indportal.indCreation;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class HouseholdFactory implements ApplicationContextAware{

	private IHousehold household;
	private static ApplicationContext applicationContext;
	
	public IHousehold getObject(String householdName){
		if("IND19Household".equalsIgnoreCase(householdName)){
			household = loadBean(householdName);
		}else if("IND57Household".equalsIgnoreCase(householdName)){
			household = loadBean(householdName);
		}else if("IND71Household".equalsIgnoreCase(householdName)){
			household = loadBean(householdName);
		}		
		return household;
	}
	
	private IHousehold loadBean(String householdName){
		IHousehold household = (IHousehold) applicationContext.getBean(householdName);
		return household;
	}
	

	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		applicationContext = appContext;
		
	}
	
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
}
