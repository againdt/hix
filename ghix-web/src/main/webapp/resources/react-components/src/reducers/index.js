import { combineReducers } from 'redux';
import app from './app';
import dashboard from './dashboard';
import isMultiHousehold from './multiHousehold';
import pageContainer from './pageContainerReducer';
import verificationData from './verificationDataReducer';
import issuersList from './issuersReducer';
import spinner from './spinnerReducer';
import messages from './messagesReducer';
import language from './languageReducer';

export default combineReducers({
    app,
    dashboard,
    isMultiHousehold,
    pageContainer,
    verificationData,
    issuersList,
    spinner,
    messages,
    language
})