<%@ include file="../include.jsp" %>
<div id="appscr66" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page16.specialCircumstances" text="Special Circumstances" />
			</h4>
		</div>
		<form id="appscr66Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.pageHeader.familyAndHousehold" text="Family and Household" /></h4>
			</div>
			<div class="gutter10">
				<div id="appscr66SpecialCircumstances" ></div>
			</div>
		</form>
	</div> 
</div>