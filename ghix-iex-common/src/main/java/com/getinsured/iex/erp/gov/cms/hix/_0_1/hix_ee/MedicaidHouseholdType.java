//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean;


/**
 * A data type for a
 * 				collection of persons to be treated as a household unit under a
 * 				state's Medicaid rules.
 * 
 * <p>Java class for MedicaidHouseholdType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MedicaidHouseholdType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hix.cms.gov/0.1/hix-ee}HouseholdType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}MedicaidHouseholdEffectivePersonQuantity" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}MedicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MedicaidHouseholdType", propOrder = {
    "medicaidHouseholdEffectivePersonQuantity",
    "medicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator"
})
public class MedicaidHouseholdType
    extends HouseholdType
{

    @XmlElement(name = "MedicaidHouseholdEffectivePersonQuantity")
    protected QuantityType medicaidHouseholdEffectivePersonQuantity;
    @XmlElement(name = "MedicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator")
    protected Boolean medicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator;

    /**
     * Gets the value of the medicaidHouseholdEffectivePersonQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link QuantityType }
     *     
     */
    public QuantityType getMedicaidHouseholdEffectivePersonQuantity() {
        return medicaidHouseholdEffectivePersonQuantity;
    }

    /**
     * Sets the value of the medicaidHouseholdEffectivePersonQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityType }
     *     
     */
    public void setMedicaidHouseholdEffectivePersonQuantity(QuantityType value) {
        this.medicaidHouseholdEffectivePersonQuantity = value;
    }

    /**
     * Gets the value of the medicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getMedicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator() {
        return medicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator;
    }

    /**
     * Sets the value of the medicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMedicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator(Boolean value) {
        this.medicaidHouseholdIncomeAboveHighestApplicableMAGIStandardIndicator = value;
    }

}
