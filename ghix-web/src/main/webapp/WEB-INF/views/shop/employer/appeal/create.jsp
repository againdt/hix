<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>
<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ajaxfileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<form class="form-horizontal" id="frmCreateAppeal" name="frmCreateAppeal"  enctype="multipart/form-data"  action="create"  method="POST">
<df:csrfToken/>
<input type="hidden" name="upload_docs" id="upload_docs" value="" >
<input type="hidden" name="appeal_mode" id="appeal_mode" value="${param.appeal_mode}" >
<input type="hidden" name="ticketId" id="ticketId" value="${param.ticketId}" >
<input type="hidden" name="isAppealReviewMode" id="isAppealReviewMode" value="${isAppealReviewMode}" > 
<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="#">&lt; <spring:message code="label.back" /></a></li>
			<li><a href="#">Account</a></li>
			<li>Manage Account Information</li>
		</ul>
		<!--page-breadcrumb ends-->
		<h1 id="skip">${employerName}</h1>
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
			<h4><spring:message code="label.youshouldknow"/></h4>
			</div>
			<div class="gutter10">
				<p><spring:message code="label.youshouldknowcomments"/></p>
			</div>
		</div>
		<!-- /.sidebar-->
		<div class="span9" id="rightpanel">
			<div class="row-fluid">
				<div class="header">
					<h4 class="pull-left">
						<c:choose>
							<c:when  test="${isAppealReviewMode == 'Y'}">
								<spring:message code="label.appealReviewHead"/>
							</c:when>	
							<c:otherwise>
								<spring:message code="label.appealHead"/>
							</c:otherwise>
						</c:choose>
					</h4>
					<a class="btn btn-primary btn-small pull-right"  href="#"  onClick="javascript:submitappeal();"><spring:message code="label.savebt" /></a>
					<a class="btn btn-small pull-right" href="#" onClick="javascript:goToPreviousPage();"><spring:message code="label.cancelButton" /></a> 
				</div>

					
						

						<div class="gutter10">
							<div class="control-group">
								<label class="control-label" for="comment_text"><spring:message code="label.comment" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="" height="" alt="Required!" /></label>
								<div class="controls paddingT5">
									<textarea class="input-xlarge" name="comments"
										id="comment_text" rows="4" cols="40" style="resize: none;" maxlength="1000"></textarea>
									<div id="comment_text_error" class="help-inline"></div>
								</div>
							</div>
							<!-- end of control-group-->

							<div class="control-group">
								
								<div class="controls paddingT5">
									<!-- File upload plugin starts -->
									<div id="file_upload_container">
										<input type="file" class="input-file" id="fileupload" onchange=""  name="files" />&nbsp;	
									</div>
									 
									<div id="fileupload_error" class="error" style="display:none;">File could not be uploaded. Selected file name already exists on the server.</div>
									<div id="uploaddiv" style="display:none;">Uploading..... Please wait</div>
			       						<div id="deletediv" style="display:none;">Deleting..... Please wait</div>
							                <!-- The table listing the files available for attaching/removing -->
			       							<div id="" class="files" >
			       								<table id="upload_files_container">
			       								
			       								</table>
			       							</div>
			       					
			       							
			       						<!-- File upload plugin ends -->  
			       						   
			       						

								</div>
							</div>
						<div class="control-group">
							<div class="controls">
								<label><spring:message code="label.extracomment"/></label>
							</div>
						</div>
							
					</div>
					
				</div>

			</div>
			<br>
			<div class="row-fluid"></div>
			<!--row-fluid-->
		</div>
		<!--rightpanel-->
	</div>
	<!--row-fluid-->
</form>
<!-- Setting up the file upload plugin -->
<script type="text/javascript">
var uploadedDocId;
var uploadedDocDtls='';
var input = $('#fileupload');
var fileDetails;
var rowno=0;
var responseText="";

function setAjaxPlugin(){
	$('#fileupload').ajaxfileupload({
		
	    'action': 'uploadAppealFiles',
	    'onComplete': function(response,xhr){
			if(isInvalidCSRFToken(xhr))	    				
				return;
	    	responseText=response;
	    	if(responseText){
	    		fileDetails=responseText.split('#');
	    		if(fileDetails.length == 3){
	        		uploadedDocId=fileDetails[0];
	            	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
	            	uploadedDocDtls+=uploadedDocId+'#';
	            	$('#upload_docs').val(uploadedDocDtls);
	            	rowno++;
	            	appendFileDetails();	
	        	}
	        	else{
	        		$('#fileupload_error').show();
	        	}
	    	}
	    	else{
	    		$('#fileupload_error').show();
	    	}
	    	
	    	$('#uploaddiv').hide();
	    },
	    'onStart': function() {
	    	$('#uploaddiv').show();
	    	$('#fileupload_error').hide();
	    },
	    'onCancel': function() {
	      
	    }
	  });
}

function goToPreviousPage(){
	deleteAllFileOnCancel();
	$.browser.chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()); 
	if($.browser.chrome){
		history.go(-2);
	}
	else{
		history.go(-1);	
	}
    return false;
	
}

function deleteAllFileOnCancel(){
	var subUrl = '<c:url value="/shop/employer/appeal/removeAppealFiles">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {uploadedDocId : uploadedDocDtls}, 
		dataType:'text',
   		success: function(response,xhr){
			if(isInvalidCSRFToken(xhr))	    				
				return;
		},error : function(e) {
			
		}
	});
}
function submitappeal(){
	if ($("#frmCreateAppeal").validate().form()){
		if($("#appeal_mode").val() != ''){
			$("#frmCreateAppeal").attr("action", $("#appeal_mode").val());	
		}
		$("#frmCreateAppeal").submit();
	}
}

function appendFileDetails(){
	orgHtml=$('#upload_files_container').html();
	strHtml=orgHtml+'<tr id="' +rowno  +'">'+
	'<td colspan="1">'+fileDetails[1]+'</td>'+
	'<td colspan="1">'+fileDetails[2]+' B</td>'+
	'<td colspan="1"><a class="btn btn-danger" href="javascript:deleteFile(\''+uploadedDocId+'\','+rowno+')">delete</a></td>'
	+'</tr>'
	$('#upload_files_container').html(strHtml);
	 
}

function deleteFile(fileId,rowid){
	$('#fileupload_error').hide();
	$('#deletediv').show();
	var subUrl = '<c:url value="/shop/employer/appeal/removeAppealFiles">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {uploadedDocId : fileId}, 
		dataType:'text',
		success: function(response,xhr){
			if(isInvalidCSRFToken(xhr))	    				
				return;
			var newText = '<input type="file" class="input-file" id="fileupload" onchange=""  name="files" />&nbsp;';
			$('#file_upload_container').html('');
			$('#file_upload_container').html(newText);
			 //$('#fileupload').replaceWith($('#fileupload').clone(true).val(''));
			 setAjaxPlugin();
			($('#'+rowid)).remove();	
			uploadedDocDtls = uploadedDocDtls.replace(fileId+'#', "");
			$('#upload_docs').val(uploadedDocDtls);
			$('#deletediv').hide();
		},error : function(e) {
			alert('error in deleting document.');
			$('#deletediv').hide();
		}
	});
}

var validator = $("#frmCreateAppeal").validate({ 
	rules : {
		comments : {required: true}
		      },
	messages : {
		comments : {
			required : "<span> <em class='excl'>!</em><spring:message code='label.validatecomment' javaScriptEscape='true'/></span>"
		},
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});

$(document).ready(function(){
	setAjaxPlugin();
});
</script>