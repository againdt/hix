/**
 * 
 */
package com.getinsured.iex.ssap.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;

import com.getinsured.eligibility.at.resp.service.SsapVerificationService;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.EligibilityProgramRepository;
import com.getinsured.hix.model.ssap.MultiHousehold;
import com.getinsured.hix.model.ssap.MultiHouseholdMember;
import com.getinsured.timeshift.sql.TSTimestamp;
import com.getinsured.timeshift.util.TSDate;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.ssap.integration.notices.QEPNonFinancialNotificationService;
import com.getinsured.eligibility.ssap.validation.service.QleValidationService;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.SsapPreferencesDTO;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.SsapUtil;
import com.google.gson.Gson;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

/**
 * @author karnam_s
 *
 */
@Service("SsapService")
public class SsapServiceImpl implements SsapService { 
	private static final String LOCATION_ID = "location_id";
	private static final String HOUSEHOLD_CASE_ID = "household_case_id";
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapServiceImpl.class);
	@Autowired SsapApplicationRepository ssapApplicationRepository;
	@Autowired SsapApplicantRepository ssapApplicantRepository;
	@Autowired SsapJsonBuilder ssapJsonBuilder;
	@Autowired ILocationRepository iLocationRepository;
	@Autowired CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired private LookupService lookupService;
	@Autowired GhixRestTemplate ghixRestTemplate;
	@Autowired private UserService userService;
	@Autowired private QLEValidationService qleValidationService;
	@Autowired private Gson platformGson;
	@Autowired private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired IUserRepository iUserRepository;
	@Autowired QEPNonFinancialNotificationService qepNonFinancialNotificationService;
	@Autowired
	private QleValidationService qleValidation;
	@Autowired
	private ReferralLceNotificationService referralLceNotificationService;
	@Autowired private SsapUtil ssapUtil;
	@Autowired
	private SsapVerificationService ssapVerificationService;
	@Autowired
	private EligibilityProgramRepository eligibilityProgramRepository;

	@PersistenceUnit
	private EntityManagerFactory emf;

	@SuppressWarnings("serial")
	private static final Set<String> INVALID_CANCELLED_CLOSED_APPLICATION_STATUS = new HashSet<String>() {
		{
			add(ApplicationStatus.CANCELLED.getApplicationStatusCode());
			add(ApplicationStatus.CLOSED.getApplicationStatusCode());
			add(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
		}
	};

	private final List<String> allVerifications = Arrays.asList("RESIDENCY_STATUS", "MEC_STATUS", "DEATH_STATUS",
			"SSN", "NAT_AMERICAN_STATUS", "CURRENT_INCOME", "NON_ESI_MEC_STATUS", "CITIZENSHIP", "VLP_STATUS", "INCARCERATION_STATUS");

	/* (non-Javadoc)
	 * If the status is EN close the previous enrolled application based on the application Id
	 * update the current application to EN
	 * 
	 * @see com.getinsured.iex.ssap.service.SsapApplicationService#updateSsapApplicationStatus(java.lang.String, long, java.sql.Timestamp)
	 */
	@Override
	@Transactional
	public int updateSsapApplicationStatus(String applicationStatus, long ssapApplicationId, Timestamp timeStamp) throws GIException {
		int updatedCount = 0;
		try {
			if(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(applicationStatus) || 
					ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode().equalsIgnoreCase(applicationStatus)) {
				
				List <SsapApplication> ssapApplication = ssapApplicationRepository.getApplicationsById(ssapApplicationId);
				BigDecimal cmrHouseoldId = ssapApplication.get(0).getCmrHouseoldId();
				SsapApplication previousApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(cmrHouseoldId, ssapApplication.get(0).getCoverageYear());
					 
				if(null != previousApplication  && previousApplication.getId() != ssapApplicationId) {
					LOGGER.debug("Previous Application ID in EN/PN state" + previousApplication.getId());		
					if(previousApplication.getApplicationDentalStatus() != null) {
						ssapApplicationRepository.updateApplicationAndDentalStatusById(ApplicationStatus.CLOSED.getApplicationStatusCode(), ApplicationStatus.CLOSED.getApplicationStatusCode(), previousApplication.getId(), new Timestamp(new TSDate().getTime()));
					}
					else {
						ssapApplicationRepository.updateApplicationStatusById(ApplicationStatus.CLOSED.getApplicationStatusCode(), previousApplication.getId(), new Timestamp(new TSDate().getTime()));
					}
				}else {
					LOGGER.debug("Previous Application is null for " + ssapApplicationId);
				}
				
				
			}
			
			updatedCount = ssapApplicationRepository.updateApplicationStatusById(applicationStatus, ssapApplicationId, new Timestamp(new TSDate().getTime()));
			
		}catch(Exception e) {
			LOGGER.error("Erro in updating the application status for applciationid:" + ssapApplicationId ,e);
			throw new GIException(e);
		}
		return updatedCount;
	}
	/**
	 * applicationMemb.getOnApplication()  Y|N
	 * applicationMemb.getApplyingForCoverage() Y|N 
	 * applicationMemb.getEligibilityStatus()   null|QHP|NONE|Not Applicable
	 *  applicationMemb.getStatus()              null| LCE_ADD| LCE_DELETE | CHANGE_IN_CITIZENSHIP_STATUS .....32 values
	 * ApplicationStatus.java(Enum) OPEN("OP"), SIGNED("SG"), SUBMITTED("SU"), ELIGIBILITY_RECEIVED("ER"), ENROLLED_OR_ACTIVE("EN"), CANCELLED("CC"), CLOSED("CL"), UNCLAIMED("UC"),PARTIALLY_ENROLLED("PN");
	 * 1) Can we receive other than EN status for list of given members. If yes, then shall we change app status accordingly
	 * 2) What is significance of ApplyingForCoverage/OnApplication/EligibilityStatus on this logic
	 */
	@Override
	@Transactional
	public int updateSsapAppStatusOnMemberStatus(List<String> exchgIndivIdList, String applicationStatus, long ssapApplicationId, Timestamp timestamp) throws GIException {
		
		LOGGER.debug("updateSsapAppStatusOnMemberStatus request "+ssapApplicationId+" "+applicationStatus);
		if("EN".equalsIgnoreCase(applicationStatus)) {
			if(null == exchgIndivIdList || exchgIndivIdList.isEmpty()) {
				LOGGER.error("Error: Member Id List can not be null/empty for application ID " + ssapApplicationId );
				throw new GIException("Error: Member Id List null/empty while updating application status. ");
			}
			LOGGER.debug("updateSsapAppStatusOnMemberStatus member guid "+Arrays.toString(exchgIndivIdList.toArray()));
			List <SsapApplicant> applicationMembrList =ssapApplicantRepository.findEligibleMembersBySsapApplicationId(ssapApplicationId)  ;
			
			SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);
			SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
			List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
			
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Below members are from the exchgIndivIdList");
				exchgIndivIdList.forEach(mem -> LOGGER.info(mem));
				
				LOGGER.info("Below members are from the applicationMembrList from the database");
				applicationMembrList.forEach(mem -> LOGGER.info(mem.getApplicantGuid()));
			}
			
			List <SsapApplicant> seekQHPapplicationMembrList = new ArrayList<SsapApplicant>();
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Checking for the SeekQHP flag through Loop");
			}
			for(SsapApplicant applicationMember: applicationMembrList) {
				 HouseholdMember householdContactApplicantJson = ssapUtil.getApplicantJson(members, applicationMember);
				 
				 if(LOGGER.isInfoEnabled()) {
					LOGGER.info(applicationMember.getApplicantGuid() + " : " + householdContactApplicantJson.isSeeksQhp());
				 }
				 
				 if(householdContactApplicantJson.isSeeksQhp()) {
					 seekQHPapplicationMembrList.add(applicationMember);
				 }
			}
			
			 for(SsapApplicant applicationMember: seekQHPapplicationMembrList) {
				if(     !exchgIndivIdList.contains(applicationMember.getApplicantGuid())   ) {
						applicationStatus = "PN";
						break;
				}
			 }
		}
		 
		 return updateSsapApplicationStatus( applicationStatus, ssapApplicationId,   timestamp);
	}
	
	/* (non-Javadoc)
	 * If the status is EN close the previous enrolled application based on the case Number
	 * Update the current application to EN
	 * 
	 * @see com.getinsured.iex.ssap.service.SsapService#updateSsapApplicationStatusByCaseNumber(java.lang.String, java.lang.String, java.sql.Timestamp)
	 */
	@Override
	@Transactional
	public int updateSsapApplicationStatusByCaseNumber(String applicationStatus, String caseNumber, Timestamp timeStamp,BigDecimal userId) throws GIException {
		int updatedCount = 1;
		
		try {
			SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumberId(caseNumber);
			if(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(applicationStatus)) {
				
				BigDecimal cmrHouseoldId = ssapApplication.getCmrHouseoldId(); 
				
				SsapApplication previousApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(cmrHouseoldId, ssapApplication.getCoverageYear());
				if(previousApplication != null) {
					LOGGER.debug("Previous Application ID in EN state:" + previousApplication.getId());
					ssapApplicationRepository.updateApplicationStatusById(ApplicationStatus.CLOSED.getApplicationStatusCode(), previousApplication.getId(), new Timestamp(new TSDate().getTime()),userId);
				}
			}
			
			
			if(!INVALID_CANCELLED_CLOSED_APPLICATION_STATUS.contains(ssapApplication.getApplicationStatus()) && 
					ApplicationStatus.CANCELLED.getApplicationStatusCode().equalsIgnoreCase(applicationStatus)){
				qleValidationService.discardApp(caseNumber,  ApplicationStatus.CANCELLED, userId.intValue());
			}
			else{
				ssapApplicationRepository.updateApplicationStatus(applicationStatus, caseNumber, new Timestamp(new TSDate().getTime()),userId);
			}
			
		}
		catch(Exception e) {
			LOGGER.error("Erro in updating the application status for applciation caseNumber:" + caseNumber ,e);
			throw new GIException(e);
		}
		return updatedCount;
	}
	
	
	/**
	 * Fetch the consumer Id from the ssap Application
	 * @param ssapApplicationId
	 * @return
	 */
	private BigDecimal getConsumerId(long ssapApplicationId) {
		List <SsapApplication> ssapApplication = ssapApplicationRepository.getApplicationsById(ssapApplicationId);
		return ssapApplication.get(0).getCmrHouseoldId();
	}
	
	/**
	 * Fetch the consumer Id from the ssap Application
	 * @param caseNumber
	 * @return
	 */
	private BigDecimal getConsumerId(String caseNumber) {
		SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumberId(caseNumber);
		return ssapApplication.getCmrHouseoldId();
	}
	
	@Override
	@Transactional
	public Map<String,List<Long>> updateApplicantDateOfBirth(List<Long> applicantIds){
		SsapApplicant ssapApplicant = null;
		SsapApplication ssapApplication = null;
		SingleStreamlinedApplication singleStreamlinedApplication = null;
		List<HouseholdMember> householdMembers = null;
		List<Long> successApplicant= new ArrayList<Long>();
		List<Long> failedApplicant= new ArrayList<Long>();
		Map<String,List<Long>> result = new HashMap<>();
		if(applicantIds == null){
			return null;
		}
		for(Long applicantId : applicantIds){
			try{
				ssapApplicant = ssapApplicantRepository.findSsapApplicantById(applicantId);
				ssapApplication = ssapApplicant.getSsapApplication();
				singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
				householdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
				if(householdMembers!= null){
					for(HouseholdMember householdMember : householdMembers){
						if(ssapApplicant.getApplicantGuid()!=null && householdMember.getApplicantGuid()!= null 
								&& ssapApplicant.getApplicantGuid().equals(householdMember.getApplicantGuid())){
							ssapApplicant.setBirthDate(householdMember.getDateOfBirth());
							ssapApplicant.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
							ssapApplicantRepository.saveAndFlush(ssapApplicant);
							LOGGER.info("update date of birth for applicantId:"+ssapApplicant.getId());
							successApplicant.add(applicantId);
							break;
						}
					}
				}
			}catch(Exception ex){
				LOGGER.error("Exception occured while updating date of birth for applicant:"+applicantId,ex);
				failedApplicant.add(applicantId);
			}
		}
		result.put("successApplicant",successApplicant);
		result.put("failedApplicant",failedApplicant);
		return result;
	}
	
	@Override
	public void updatePreferences(SsapPreferencesDTO ssapPreferencesDTO){
		final String language = "LANGUAGE";
		final String communicationMode = "PreferredContactMode";
		Household objHousehold = null;
		boolean updateLocationInDb = false;
		
		try{
		
		objHousehold = cmrHouseholdRepository.findOne((int)ssapPreferencesDTO.getHouseholdId());
		if(ssapPreferencesDTO.isUpdateMailingAddress()){
			boolean updateLocation = true;
			if((ssapPreferencesDTO.getPrefContactLocation().getAddress1() == null || ssapPreferencesDTO.getPrefContactLocation().getAddress1().trim().equals(""))
			   || (ssapPreferencesDTO.getPrefContactLocation().getCity()== null  || ssapPreferencesDTO.getPrefContactLocation().getCity().trim().equals(""))	
			   || (ssapPreferencesDTO.getPrefContactLocation().getState()== null || ssapPreferencesDTO.getPrefContactLocation().getState().trim().equals(""))
			   || (ssapPreferencesDTO.getPrefContactLocation().getZip()== null || ssapPreferencesDTO.getPrefContactLocation().getZip().trim().equals(""))){
				updateLocation = false;
			}
			
			if(updateLocation && objHousehold.getPrefContactLocation() == null || !(equalsIgnoreNullAndBlank(objHousehold.getPrefContactLocation().getAddress1(),ssapPreferencesDTO.getPrefContactLocation().getAddress1()) 
					&& equalsIgnoreNullAndBlank(objHousehold.getPrefContactLocation().getAddress2(),ssapPreferencesDTO.getPrefContactLocation().getAddress2())
					&& equalsIgnoreNullAndBlank(objHousehold.getPrefContactLocation().getCity(),ssapPreferencesDTO.getPrefContactLocation().getCity())
					&& equalsIgnoreNullAndBlank(objHousehold.getPrefContactLocation().getState(),ssapPreferencesDTO.getPrefContactLocation().getState())
					&& equalsIgnoreNullAndBlank(objHousehold.getPrefContactLocation().getCounty(),ssapPreferencesDTO.getPrefContactLocation().getCounty())
					&& equalsIgnoreNullAndBlank(objHousehold.getPrefContactLocation().getZip(),ssapPreferencesDTO.getPrefContactLocation().getZip())
					)){
				Location l = new Location();
				l.setAddress1(ssapPreferencesDTO.getPrefContactLocation().getAddress1());
				l.setAddress2(ssapPreferencesDTO.getPrefContactLocation().getAddress2());
				l.setCity(ssapPreferencesDTO.getPrefContactLocation().getCity());
				l.setState(ssapPreferencesDTO.getPrefContactLocation().getState());
				l.setZip(ssapPreferencesDTO.getPrefContactLocation().getZip());
				l.setCounty(ssapPreferencesDTO.getPrefContactLocation().getCounty());
				objHousehold.setPrefContactLocation(l);
				objHousehold.setUpdatedBy(ssapPreferencesDTO.getUpdatedBy());
				updateLocationInDb = true;
			}
		
		}
		
		if(ssapPreferencesDTO.isUpdatePreferences()){
			 LookupValue spokenLang =  lookupService.getlookupValueByTypeANDLookupValueCode(language, ssapPreferencesDTO.getPreferedSpokenLangages());
			 objHousehold.setPrefSpokenLang(spokenLang.getLookupValueId());
			 LookupValue writtenLang =  lookupService.getlookupValueByTypeANDLookupValueCode(language, ssapPreferencesDTO.getPreferedWrittenLanguages());
			 objHousehold.setPrefWrittenLang(writtenLang.getLookupValueId());
			 LookupValue contactMethod =  lookupService.getlookupValueByTypeANDLookupValueCode(communicationMode, ssapPreferencesDTO.getPreferedContactMethod());
			 objHousehold.setPrefContactMethod(contactMethod.getLookupValueId());
		}
		
		if(updateLocationInDb || ssapPreferencesDTO.isUpdatePreferences()){
			objHousehold = cmrHouseholdRepository.saveAndFlush(objHousehold);
		}
		
		if(updateLocationInDb && ssapPreferencesDTO.isTriggerMailingLocationApi()){
			updateEnrollmentMailingLocation(objHousehold,ssapPreferencesDTO);
		}
	
		}catch(Exception ex){
			LOGGER.error("Exception occured while updating mailing address for household:"+ssapPreferencesDTO.getHouseholdId(),ex);
			throw new GIRuntimeException(ex);
		}
	}
	
	private void updateEnrollmentMailingLocation(Household objHousehold,SsapPreferencesDTO ssapPreferencesDTO ){
		//call enrollment API to update location
		// Call Enrollment API to update Preferences details
		ResponseEntity<EnrollmentResponse> response = null;
		final Map<String, String> request = new HashMap<String, String>(2);
		request.put(HOUSEHOLD_CASE_ID, String.valueOf(objHousehold.getId()));
		request.put(LOCATION_ID, String.valueOf(objHousehold.getPrefContactLocation().getId()));
		
		response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_PREFERENCES_ENROLLMENT,
				ssapPreferencesDTO.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, 
				EnrollmentResponse.class, platformGson.toJson(request)); 
		
		if(response.getBody() != null && GhixConstants.RESPONSE_FAILURE.equals(response.getBody().getStatus())){
			LOGGER.error("Exception occured while calling updateMailByHouseHoldId :"+ssapPreferencesDTO.getHouseholdId());
			throw new GIRuntimeException("Exception occured while calling updateMailByHouseHoldId :"+ssapPreferencesDTO.getHouseholdId());
		}
		
	}
	
	private boolean equalsIgnoreNullAndBlank(String s1,String s2){
		if((s1== null || s1.trim().equals("")) && (s2 == null || s2.trim().equals(""))){
			return true;
		}
		if(s1 !=null && s2 != null && s1.trim().equals(s2.trim())){
			return true;
		}
		return false;
	}

	/**
	 * Updates SSAP Application Renewal Status to OTR based on case number.
	 * @param caseNumber
	 * @param otr
	 * @param timestamp
	 * @param userId
	 */
	@Override
	public void updateApplicationRenewalStatus(String caseNumber, RenewalStatus otr, Timestamp timestamp,
			BigDecimal userId) {
		
		SsapApplication application = ssapApplicationRepository.findByCaseNumberId(caseNumber);
		if(application != null) {
			
			if(otr.equals(application.getRenewalStatus())) {
				LOGGER.warn("SSAP Applcation id [" + application.getId() + "], case number ["  + caseNumber + "] is already set to Renewal Status [" + otr + "]");
				return;
			}
			
			application.setRenewalStatus(otr);
			application.setLastUpdateTimestamp(timestamp);
			application.setLastUpdatedBy(userId);
			ssapApplicationRepository.save(application);
		}
	}
	
	
	public void processQleValidation(String caseNumber){
		SsapApplicationEvent ssapApplicationEvent  = null;
		SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumberId(caseNumber);
		if(!ssapApplication.getApplicationStatus().equals(ApplicationStatus.CLOSED.getApplicationStatusCode())){
		boolean result = qleValidation.triggerQleValidation(caseNumber, getAdminUserId());
		//validation success and send the notice
		if(result){
				
			ssapApplicationEvent =  ssapApplicationEventRepository.findEventBySsapApplication(ssapApplication.getId());
			if(SsapApplicationEventTypeEnum.QEP.equals(ssapApplicationEvent.getEventType()) && 
            		ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equals(ssapApplication.getApplicationStatus())) {
                try { 
                	qepNonFinancialNotificationService.generateQEPGrantNoticeForNonFinancial(ssapApplication.getCaseNumber());
                } catch (Exception ex) {
                    LOGGER.error("Error sending QEP notice for casenumber : " + ssapApplication.getCaseNumber(), ex);
                }
            }
			}else{
				try {
					referralLceNotificationService.generateLceDocumentRequiredNotice(caseNumber);
				} catch (Exception e) {
					LOGGER.error("Exception occured while sending document required notice for -"+caseNumber,e);
				}
		}
	}
	}
	
	
	private Integer getAdminUserId(){
		return userService.findByUserName("exadmin@ghix.com").getId();
	}

    @Override
	@Transactional
	public List<MultiHousehold> getCMRHouseholdApplicationMembers(List<Integer> householdIds) {
		List<MultiHousehold> households = new ArrayList<>();

		if(householdIds != null && householdIds.size() > 0) {
			LOGGER.debug("getCMRHouseholdApplicationMembers::householdIds = " + householdIds.toString());
            try {
				Map<Integer, Integer> applicationMap = new HashMap<>();
				Map<String, Object> parameterMap = new HashMap<>();
				parameterMap.put("householdIds", householdIds);

				StringBuilder householdQuery = new StringBuilder();
				householdQuery.append("SELECT ssap_applications.cmr_houseold_id, ssap_applications.id, ssap_applicants.external_applicant_id, ssap_applicants.first_name, ");
				householdQuery.append("ssap_applicants.middle_name, ssap_applicants.last_name, ssap_applicants.name_suffix, ssap_applications.coverage_year, ssap_applications.creation_timestamp, ");
				householdQuery.append("ssap_applicants.person_type, CASE WHEN ssap_applicants.person_type LIKE 'PC%' THEN 1 ELSE 0  END AS applicant_order ");
				householdQuery.append("FROM ssap_applicants, ssap_applications, cmr_household ");
				householdQuery.append("WHERE cmr_household.id = ssap_applications.cmr_houseold_id AND ssap_applications.id = ssap_applicants.ssap_application_id AND ssap_applications.application_status ");
				householdQuery.append("IN ( :appStatus ) ");
				householdQuery.append("AND cmr_household.id in ( :householdIds ) AND ssap_applicants.applying_for_coverage = 'Y'");
				householdQuery.append("ORDER BY ssap_applications.creation_timestamp desc, ssap_applications.coverage_year desc, cmr_household.creation_timestamp desc, applicant_order desc;");

				List<String> activeAppStatus = new ArrayList<>();
				activeAppStatus.add("ER");
				activeAppStatus.add("PN");
				activeAppStatus.add("EN");
				parameterMap.put("appStatus", activeAppStatus);
				getMultiHouseholds(householdQuery.toString(), parameterMap, householdIds, applicationMap, households, true);

                if(!householdIds.isEmpty()) {
					LOGGER.debug("getCMRHouseholdApplicationMembers::householdIds not empty = " + householdIds.toString());

					List<String> closedAppStatus = new ArrayList<>();
					closedAppStatus.add("CL");
					parameterMap.put("appStatus", closedAppStatus);
					getMultiHouseholds(householdQuery.toString(), parameterMap, householdIds, applicationMap, households, false);
				}
            } catch(Exception ex) {
                LOGGER.error("Exceptin Occured in getCMRHouseholdApplicationMembers", ex);
            }
        } else {
			LOGGER.debug("getCMRHouseholdApplicationMembers::household ID list null or empty");
		}

		LOGGER.debug("getCMRHouseholdApplicationMembers::households = " + households.toString());
		return households;
	}

	@Override
	@Transactional
	public void updateHouseholdLocation(Location location, Integer cmrHouseholdId){
		cmrHouseholdRepository.updateHouseholdLocation(location, cmrHouseholdId);
	}

	/**
	 * Method to get Households and members based on query string
	 * Resulting Household list will contain each household and its members with only the most recent application ordered by most recent household
	 *
	 * @param queryString - Query string to get households
	 * @param parameterMap - Any parameters that need replacing in the query string (Ex: Household Ids)
	 * @param householdIds - List of household ids left to process
	 * @param applicationMap - Map<Household ID, Application ID> used to make sure we only take care of the most recent application for a household
	 * @param households
	 * @param active
	 */
	private void getMultiHouseholds(String queryString, Map<String, Object> parameterMap, List<Integer> householdIds,
													Map<Integer, Integer> applicationMap, List<MultiHousehold> households, boolean active) {
		try {
			Map<Integer, MultiHousehold> householdMap = new HashMap<>();
			List resultList = buildAndExecuteQuery(queryString, parameterMap);
			List<Integer> orderedHouseholds = new ArrayList<>();

			if(CollectionUtils.isEmpty(resultList)) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("No households found for IDs.");
				}
			} else {
				Iterator rsIterator = resultList.iterator();
				Object[] objArray = null;

				while(rsIterator.hasNext()) {
					objArray = (Object[]) rsIterator.next();

					Integer householdId = Integer.parseInt(objArray[0].toString());
					Integer applicationId = objArray[1] != null ? Integer.parseInt(objArray[1].toString()) : null;

					if(!applicationMap.containsKey(householdId)) {
						LOGGER.debug("getMultiHouseholds::app map did not have key, adding key");
						applicationMap.put(householdId, applicationId);
						orderedHouseholds.add(householdId);
					}

					LOGGER.debug("getMultiHouseholds::householdId = " + householdId);
					LOGGER.debug("getMultiHouseholds::applicationId = " + applicationId);
					if(applicationMap.get(householdId).equals(applicationId)) {
						List<MultiHouseholdMember> householdMemberList;
						MultiHousehold household;
						if (householdMap.containsKey(householdId)) {
							household = householdMap.get(householdId);
						} else {
							household = new MultiHousehold();
							household.setId(String.valueOf(householdId));
							household.setActive(active);
						}
						householdMemberList = household.getMembers();

						MultiHouseholdMember member = new MultiHouseholdMember();
						member.setFirstName(objArray[3] != null ? objArray[3].toString() : "");
						member.setMiddleName(objArray[4] != null ? objArray[4].toString() : "");
						member.setLastName(objArray[5] != null ? objArray[5].toString() : "");
						member.setSuffix(objArray[6] != null ? objArray[6].toString() : "");

						String applicationCreationDate = objArray[8] != null ? objArray[8].toString() : "";

						LOGGER.debug("getMultiHouseholds::member added to list = " + member.toString());
						householdMemberList.add(member);

						if(household.getApplicationCreationDate() == null) {
							household.setApplicationCreationDate(applicationCreationDate);
						}

						household.setMembers(householdMemberList);
						LOGGER.debug("getMultiHouseholds::multiHousehold being added to map = " + household.toString());
						householdMap.put(householdId, household);
					}
				}

				orderedHouseholds.forEach(householdId -> households.add(householdMap.get(householdId)));

				householdIds.removeAll(orderedHouseholds);
			}
		} catch(Exception ex) {
			LOGGER.error("Exception occurred in executeMultiHouseholdQuery", ex);
		}
	}

	private List buildAndExecuteQuery(String queryString, Map<String, Object> parameterMap) {
		EntityManager entityManager = null;
		List resultList = null;

		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(queryString);
			if(parameterMap != null) {
				parameterMap.entrySet().forEach(entry -> {
					query.setParameter(entry.getKey(), entry.getValue());
				});
			}

			resultList = query.getResultList();
		} catch(Exception ex) {
			LOGGER.error("Exception occurred in buildAndExecuteQuery", ex);
		} finally {
			// close the entity manager
			if(entityManager !=null && entityManager.isOpen()){
				entityManager.clear();
				entityManager.close();
			}
		}

		return resultList;
	}
	
	public String getApplicationStatusById(Long ssapApplicationId) {
		return ssapApplicationRepository.getApplicationStatusById(ssapApplicationId);
	}
	
	public void updateDentalApplicationStatusByCaseNumber(String caseNumber, TSTimestamp tsTimestamp,
			BigDecimal userId) {
		ssapApplicationRepository.updateDentalApplicationStatus(caseNumber, tsTimestamp, userId);
	}

	@Override
	public boolean cleanupApplicationApplicantVerifications(Long ssapApplicationId) {
		try {
			if (ssapApplicationId != null) {
				List<SsapApplicant> applicantList = ssapApplicantRepository.findBySsapApplicationId(ssapApplicationId);
				SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);

				if (applicantList != null && !applicantList.isEmpty() && ssapApplication != null) {
					for (SsapApplicant applicant : applicantList) {
						cleanupApplicantVerifications(applicant);
					}
					setApplicationStatusBasedOnVerifications(ssapApplication, applicantList);
					LOGGER.debug("Finished cleaning up application applicant verifications id {}", ssapApplicationId);
					return true;
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception cleaning up application {}", ssapApplicationId, ex);
		}

		LOGGER.debug("There was an issue cleaning up verifications returning false");
		return false;
	}

	private void cleanupApplicantVerifications(SsapApplicant applicant) throws GIException {
		List<String> applicableVerifications = getApplicableVerifications(applicant.getId());

		if (!applicableVerifications.isEmpty()) {
			List<String> notApplicableVerifications = new ArrayList<>(allVerifications);
			notApplicableVerifications.removeAll(applicableVerifications);
			for (String verification : notApplicableVerifications) {
				switch (verification) {
					case "RESIDENCY_STATUS":
						applicant.setResidencyStatus(null);
						break;
					case "MEC_STATUS":
						applicant.setMecVerificationStatus(null);
						break;
					case "DEATH_STATUS":
						applicant.setDeathStatus(null);
						break;
					case "SSN":
						applicant.setSsnVerificationStatus(null);
						break;
					case "NAT_AMERICAN_STATUS":
						applicant.setNativeAmericanVerificationStatus(null);
						break;
					case "CURRENT_INCOME":
						applicant.setIncomeVerificationStatus(null);
						break;
					case "NON_ESI_MEC_STATUS":
						applicant.setNonEsiMecVerificationStatus(null);
						break;
					case "CITIZENSHIP":
						applicant.setCitizenshipImmigrationStatus(null);
						break;
					case "VLP_STATUS":
						applicant.setVlpVerificationStatus(null);
						break;
					case "INCARCERATION_STATUS":
						applicant.setIncarcerationStatus(null);
						break;
					default:
						break;
				}
			}

			LOGGER.debug("Saving applicant with changes verifications {}", applicant.toString());
			ssapApplicantRepository.save(applicant);
		}
	}

	private void setApplicationStatusBasedOnVerifications(SsapApplication application, List<SsapApplicant> applicants) {
		boolean applicationConditional = false;
		for (SsapApplicant applicant : applicants) {
			HouseholdMember householdMember = getHouseholdMemberForApplicant(application, applicant);

			if (householdMember != null) {
				if (!ssapVerificationService.isApplicantVerified(application.getFinancialAssistanceFlag(), applicant, householdMember)) {
					applicationConditional = true;
					break;
				}
			}
		};

		if (applicationConditional) {
			application.setEligibilityStatus(EligibilityStatus.CAE);
		} else {
			application.setEligibilityStatus(EligibilityStatus.AE);
		}

		ssapApplicationRepository.save(application);
	}

	@Override
	public List<String> getApplicableVerifications(Long applicantId) throws GIException {
		List<String> applicableStatuses = new ArrayList<>();

		try {
			if (applicantId != null) {
				// Residency, MEC, Death, SSN, Native American should always be in the list
				applicableStatuses.add("RESIDENCY_STATUS");
				applicableStatuses.add("MEC_STATUS");
				applicableStatuses.add("DEATH_STATUS");
				applicableStatuses.add("SSN");
				applicableStatuses.add("NAT_AMERICAN_STATUS");

				SsapApplicant applicant = ssapApplicantRepository.findOne(applicantId);

				if (applicant != null && applicant.getSsapApplication() != null) {
					SsapApplication ssapApplication = applicant.getSsapApplication();

					if ("Y".equals(ssapApplication.getFinancialAssistanceFlag())) {
						List<EligibilityProgram> eligibilityPrograms = eligibilityProgramRepository.findBySsapApplicant(applicant);
						for (EligibilityProgram program : eligibilityPrograms) {
							if (program.getEligibilityType().equals("APTCEligibilityType")) {
								applicableStatuses.add("CURRENT_INCOME");
								applicableStatuses.add("NON_ESI_MEC_STATUS");
							}
						}
					}

					HouseholdMember householdMember = getHouseholdMemberForApplicant(ssapApplication, applicant);
					if (householdMember != null) {
						if (householdMember.getCitizenshipImmigrationStatus() != null) {
							if (householdMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null
									&& Boolean.TRUE == householdMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator()) {
								applicableStatuses.add("CITIZENSHIP");
							} else {
								applicableStatuses.add("VLP_STATUS");
							}
						}

						if (householdMember.getIncarcerationStatus() != null && householdMember.getIncarcerationStatus().getIncarcerationAsAttestedIndicator() != null
								&& Boolean.FALSE == householdMember.getIncarcerationStatus().getIncarcerationAsAttestedIndicator()) {
							applicableStatuses.add("INCARCERATION_STATUS");
						}
					} else {
						throw new GIException("Could not find household member for applicant");
					}
				} else {
					throw new GIException("Could not find applicant");
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Error getting Applicable Verifications for applicant {}", applicantId, ex);
			throw ex;
		}

		return applicableStatuses;
	}

	@Override
	public HouseholdMember getHouseholdMemberForApplicant(SsapApplication ssapApplication, SsapApplicant applicant) {
		final SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
		List<HouseholdMember> householdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();

		HouseholdMember householdMember = null;
		if (householdMembers != null && !householdMembers.isEmpty()) {
			Optional<HouseholdMember> member = householdMembers.stream().filter(hhm -> hhm.getApplicantGuid() != null &&
					hhm.getApplicantGuid().equals(applicant.getApplicantGuid())).findFirst();

			if (member.isPresent()) {
				householdMember = member.orElseGet(null);
			}
		}

		return householdMember;
	}
}
