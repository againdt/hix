package com.getinsured.hix.batch.enrollment.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyIRSBatchService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentIRSOut;


public class EnrollmentMonthlyIRSOutWriter implements ItemWriter<String> {
	int applicableMonth;
	int applicableYear;
	int partition;
	private EnrollmentIRSOut enrollmentIRSOut;
	private EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService;
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyIRSOutWriter.class);
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			applicableMonth=ec.getInt("month", 0);
			applicableYear=ec.getInt("year",0);
			partition =ec.getInt("partition");
		}
	}

	@Override
	public void write(List<? extends String> items) throws Exception {
		List<String> householdIdList= null;
		try{
			if(items!=null && items.size()>0){
				householdIdList= new ArrayList<String>(items);
				enrollmentMonthlyIRSBatchService.processIRSHousehold(householdIdList, applicableMonth, applicableYear, partition);
				
			}
			
		}catch(Exception e){
			LOGGER.error("Exception occurred in IndivEnrollmentIRSOutWriter: ", e);
			
		}
		}

	public EnrollmentIRSOut getEnrollmentIRSOut() {
		return enrollmentIRSOut;
	}

	public void setEnrollmentIRSOut(EnrollmentIRSOut enrollmentIRSOut) {
		this.enrollmentIRSOut = enrollmentIRSOut;
	}

	public EnrollmentMonthlyIRSBatchService getEnrollmentMonthlyIRSBatchService() {
		return enrollmentMonthlyIRSBatchService;
	}

	public void setEnrollmentMonthlyIRSBatchService(EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService) {
		this.enrollmentMonthlyIRSBatchService = enrollmentMonthlyIRSBatchService;
	}
}
