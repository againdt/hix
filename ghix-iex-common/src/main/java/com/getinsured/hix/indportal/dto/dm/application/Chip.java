package com.getinsured.hix.indportal.dto.dm.application;

public class Chip {
	 private Boolean stateHealthBenefitIndicator;
	 private String coverageEndedReasonType;
	 private Boolean coverageEndedIndicator;


	 // Getter Methods 

	 public Boolean getStateHealthBenefitIndicator() {
	  return stateHealthBenefitIndicator;
	 }

	 public Boolean getCoverageEndedIndicator() {
	  return coverageEndedIndicator;
	 }

	 // Setter Methods 

	 public void setStateHealthBenefitIndicator(Boolean stateHealthBenefitIndicator) {
	  this.stateHealthBenefitIndicator = stateHealthBenefitIndicator;
	 }

	 public void setCoverageEndedIndicator(Boolean coverageEndedIndicator) {
	  this.coverageEndedIndicator = coverageEndedIndicator;
	 }

	public String getCoverageEndedReasonType() {
		return coverageEndedReasonType;
	}

	public void setCoverageEndedReasonType(String coverageEndedReasonType) {
		this.coverageEndedReasonType = coverageEndedReasonType;
	}
	}
