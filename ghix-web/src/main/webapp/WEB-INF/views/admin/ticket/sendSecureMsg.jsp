<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<style type="text/css">
.modal .header{
	border-radius: 6px 6px 0 0 !important;
    padding: 10px !important;
}
.modal .dialogClose{
	margin: -4px !important;
    padding: 4px;
}
</style>

<input type="hidden" id="varTicketId" name="varTicketId"  value="<encryptor:enc value = "${ticketId}"/>" /> 

<!-- Send Message dialog starts -->
<form id="sendSecureTicketMessage" method="POST" enctype="multipart/form-data" action="<c:url value='/inbox/secureInboxSend'/>">
<df:csrfToken/>
<div id="sendSecureMsgDlg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="sendSecureMsgDlg" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="sendSecureMsgHeader">
		<div class="header">
			<h4 class="pull-left"> <spring:message code="header.sendMsg" /> </h4>
			<button aria-hidden="true" data-dismiss="modal" id="crossClose1"	class="close" title="x" type="button" onClick="cancelBtnClicked()"><spring:message code="btn.crossClose" /></button>
		</div>
	</div>

	<div class="modal-body">
				<input type="hidden" id="addressBook" name="addressBook">
				<input type="hidden" id="subject" name="subject">
				<input type="hidden" id="body" name="body">
				<input type="hidden" id="currentURL" name="currentURL">
				<input type="hidden" id="names" name="names">
		<div class="control-group">
			<label class="control-label span3 txt-right"><span><spring:message code="label_msg.to" />:</span></label>
			<div class="controls span8"><span id="ticTo" name="ticTo"/></div>
		</div>	
		<div class="control-group">
			<label class="control-label span3 txt-right"><span><spring:message code="label_msg.from" />:</span></label>
			<div class="controls span8"><span id="ticFrom" name="ticFrom"/></div>
		</div>	
		<div class="control-group">
			<label class="control-label span3 txt-right"><span><spring:message code="label_msg.subject" />:</span></label>
			<div class="controls span8"><span id="ticSubject" name="ticSubject"/></div>
		</div>	
		
			<br>
		<div class="control-group">
			<label class="control-label span3 txt-right"> <spring:message code="label_msg.fileUpload" />: </label>
			<div class="controls span8">
			
					<input type="file" class="input-file" id="fileupload"
								name="files[]">
			</div>
		</div>
		<div class="clear gutter10"></div>
		<div class="control-group">
			<label class="control-label span3 txt-right"> <spring:message code="label_ac.msgText" /> </label>
			<div class="controls span8">
				<textarea class="span" name="secureMsg_text" id="secureMsg_text" rows="4" cols="40" style="resize: none;" maxlength="4000"
					spellcheck="true" value=""></textarea>
				<div id="secureMsg_text_error"></div>
			</div>
		</div>
	</div>
	<div class="modal-footer clearfix">
		<%-- <button class="btn pull-left" id="cancelSecureMsgBtn" data-dismiss="modal" aria-hidden="true" onclick="cancelBtnClicked()"><spring:message code="button.cancel" /></button>
		<button class="btn btn-primary" id="sendMsgBtn"	onclick="javascript:sendSecureMsg();"><spring:message code="button.sendMsg" /></button> --%>
		<a href="#" class="btn btn-small pull-left"  data-dismiss="modal" aria-hidden="true" onclick="cancelBtnClicked()"><spring:message code="button.cancel" /></a>
		<a href="#" class="btn btn-small btn-primary pull-right"  id="sendMsgBtn"	onclick="javascript:sendSecureMsg();"><spring:message code="button.sendMsg" /></a>
	</div>
	<div id="errorMsg"><label id="secureErrLbl"></label></div>
	<div id="msgAjaxloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
    </div>
</div>
</form>
<!--Send Message dialog ends -->
<script>
	function showSendMessageDlg() {
		resetSendMsgDlg();
		enableDisableBtns(true);
		
		$("#msgAjaxloader").show();
		$('#sendSecureMsgDlg').modal('show');
		
		//Get the details of the message elements like 
		//To, From and ticSubject and enable the dialog to send a secure message.
		var ticketId = $("#varTicketId").val();
		var validateUrl = '<c:url value="/ticketmgmt/ticket/sendSecureMsg"></c:url>';
		
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				ticketId : ticketId
			},
			success: function(response,xhr){
				if(isInvalidCSRFToken(xhr))	    				
					return;
				var ticketDetails = JSON.parse(response);
				$('#ticFrom').text(ticketDetails.from);
				$('#ticTo').text(ticketDetails.to);
				var subj =  ticketDetails.num + ", " + ticketDetails.subject + " needs your attention.";
				$('#ticSubject').text(subj);
				$('#addressBook').val(ticketDetails.toUserId);
				enableDisableBtns(false);
				$("#msgAjaxloader").hide();
			},
			error : function(error) {
				enableDisableBtns(false);
				$("#msgAjaxloader").hide();
			}
		});	
	}
	
	function cancelBtnClicked() {	
		resetSendMsgDlg();
	}
	
	function enableDisableBtns(bDisable) {
		$("#cancelSecureMsgBtn").attr("disabled",bDisable);
		$("#sendMsgBtn").attr("disabled",bDisable);
		$("#crossClose1").attr("disabled",bDisable);
	}
	
	function resetSendMsgDlg() {
		$('#ticFrom').text('');
		$('#ticTo').text('');
		$('#ticSubject').text('');
		$("#secureMsg_text").val("");	
	}
	 
	function sendSecureMsg() {
		
		
		$('#subject').val($('#ticSubject').text());
		$('#body').val($("#secureMsg_text").val());
		var url=window.location.pathname;
		var pathName = url.substring( url.indexOf('/')+1, url.length);
		var convertedUrl = pathName.substring( pathName.indexOf('/'), pathName.length);
		
		$('#currentURL').val(convertedUrl);
		$('#names').val($('#ticTo').text());
		var messageData = $("#secureMsg_text").val();
		if(messageData == null || messageData.trim() == "" ) {
			alert ("Message data cannot be null");
		}
		else{
		 $('#sendSecureTicketMessage').submit();
		}

	}
	
	function getErroMsg() {
		if($('#ticFrom').text() == "" || $('#ticTo').text() == "" ||	$('#ticSubject').text() == "" ) {
			return "Data not sufficient to send a message";
		}
		
		var messageData = $("#secureMsg_text").val();
		if(messageData == null || messageData.trim() == "" ) {
			return "Message data cannot be null";
		}
	
		return null;
	}
	
	function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		  alert($('Session is invalid').text());
		  rv = true;
		 }
		return rv;
	}
	
</script>