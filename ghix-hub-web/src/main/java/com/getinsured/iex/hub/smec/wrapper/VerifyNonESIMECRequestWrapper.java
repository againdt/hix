package com.getinsured.iex.hub.smec.wrapper;

import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.NonESIMECRequestType;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.ObjectFactory;

public class VerifyNonESIMECRequestWrapper {

	private NonESIMECRequestType request = null;
	
	private NonESIMECIndividualTypeWrapper nonESIMECIndividualTypeWrapper;
	
	public VerifyNonESIMECRequestWrapper(){
		ObjectFactory objectFactory = new ObjectFactory();
		request = objectFactory.createNonESIMECRequestType();
	}
	
	public NonESIMECRequestType getSystemRepresentation()
	{
		if(nonESIMECIndividualTypeWrapper != null){
			request.setNonESIMECindividualInformation(nonESIMECIndividualTypeWrapper.getSystemRepresentation());
		}
		return request;
	}

	public void setNonESIMECIndividualTypeWrapper(NonESIMECIndividualTypeWrapper nonESIMECIndividualTypeWrapper){
		this.nonESIMECIndividualTypeWrapper = nonESIMECIndividualTypeWrapper;
	}
	
}
