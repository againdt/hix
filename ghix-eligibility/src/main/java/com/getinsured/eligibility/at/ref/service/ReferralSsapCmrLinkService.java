package com.getinsured.eligibility.at.ref.service;

import java.math.BigDecimal;
import java.util.Map;

import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * @author chopra_s
 * 
 */
public interface ReferralSsapCmrLinkService {

	Map<String, String> executeLinking(long referralActivation, int userId, int cmrId);

	boolean executeLinking(long enrolledApplicationId, long currentApplicationId, String authRep, String hhCaseId);

	Map<String, Object> executeOELinking(long currentApplicationId ,String  householdCaseId);

	Map<String, Object> executeOELinking(long currentApplicationId, String householdCaseId, String organizationIdentificationID);

	Map<String, Object> executeQELinking(long currentApplicationId,String householdCaseId);

	Map<String, Object> executeQELinking(long currentApplicationId, String householdCaseId, String organizationIdentificationID);

	boolean hasLinkUserEnable();

	Map<String, Object> executeRenewalLinking(long currentApplicationId, boolean linkApplication, String householdCaseId);
	
	Map<String, Object> executeRenewalLinking(long currentApplicationId, boolean linkApplication, String householdCaseId, String organizationalId);
	
	boolean executeLinkingWithNFCmr(long applicationId, int cmrId);
	
	BigDecimal findMatchingCmrHousehold(SsapApplicant primary, BigDecimal cmrhousehold);
}
