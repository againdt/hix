package com.getinsured.eligibility.externalnotices.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;

import com.getinsured.hix.externalnotice.util.ExternalNoticeConstants;
import com.getinsured.timeshift.TSCalendar;

public class ExternalNoticeUtil {
	
	public static Date getBeforeDayDate(Date date, int days) {
		Date prevDay = null;
		if (date != null) {
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, -days);
			cal.set(Calendar.HOUR_OF_DAY, ExternalNoticeConstants.TWENTY_THREE);
			cal.set(Calendar.MINUTE, ExternalNoticeConstants.FIFTY_NINE);
			cal.set(Calendar.SECOND, ExternalNoticeConstants.FIFTY_NINE);
			cal.set(Calendar.MILLISECOND, ExternalNoticeConstants.NINE_NINETY_NINE);
			prevDay = cal.getTime();
		}
		return prevDay;
	}
	
	public static <T> boolean isNotNullAndEmpty(T e){
		boolean isNotNUll=false;
		if((e!=null) && !(e.toString().isEmpty()) && !(e.toString().equalsIgnoreCase("null"))){
			isNotNUll=true;
		}
		return isNotNUll;
	}
	
	 public static String shortStackTrace(Exception e, int maxLength) {
		 if(e==null){
			 return "";
		 }
	     StringWriter writer = new StringWriter();
	     e.printStackTrace(new PrintWriter(writer));
	     String trace = writer.toString();
	     return trace.substring(0, Math.min(trace.length(), maxLength)-1);
	 }

}
