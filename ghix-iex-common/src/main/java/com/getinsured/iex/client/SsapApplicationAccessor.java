package com.getinsured.iex.client;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.getinsured.iex.dto.SsapApplicationResource;

public interface SsapApplicationAccessor {

	public List<SsapApplicationResource> getSsapApplicationsForCoverageYear(long cmrHouseholdId, long coverageYear) throws JsonParseException, JsonMappingException, IOException;
	public List<SsapApplicationResource> getSsapApplicationsForCoverageYearNoAppData(long cmrHouseholdId, long coverageYear)throws JsonParseException, JsonMappingException, IOException;
	public List<SsapApplicationResource> getSsapApplications(long cmrHouseholdId) throws JsonParseException, JsonMappingException, IOException;
	public SsapApplicationResource getSsapApplications(String caseNumber) throws Exception;
	public SsapApplicationResource getSsapApplicationsByCaseNumberId(String caseNumber) throws Exception;
			
}
