<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="expires" content="0">
    <title>${brandName}</title>

    <%--Configuration for root url !!!--%>
    <base href="" >

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="<c:url value="/resources/images"/>/${favicon}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
    <link href="<c:url value="/resources/angular/dst/src/assets/fonts/MyFontsWebfontsKit.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/angular/dst/dist/styles.bundle.css" />" rel="stylesheet">
	
	<script>
		window.dataLayer = window.dataLayer || [];
	</script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','${GTMContainerID}');</script>
    <!-- End Google Tag Manager -->

</head>
<body>
     <script>
        window.__theme = 'bs4';
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${GTMContainerID}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <%--Start getting configuration from jsp--%>
    <input type="hidden" id="jsp_csrfToken" value ='<df:csrfToken plainToken="true"/>'>
    <input type="hidden" id="jsp_logo" value="<c:url value="/resources/img"/>/${logo}" />
    <input type="hidden" id="jsp_brandName" value="${brandName}" />
    <input type="hidden" id="jsp_dstCancelRedirectUrl" value="${dstCancelRedirectUrl}" />
    <input type="hidden" id="jsp_dstSubmitRedirectUrl" value="${dstSubmitRedirectUrl}" />
    <input type="hidden" id="docRoot" value="<c:url value="/"  />" />
    <%--End getting configuration from jsp--%>

    <%--start of the angular app--%>
    <app-root>Loading...</app-root>
    <script type="text/javascript" src="<c:url value="/resources/angular/dst/dist/inline.bundle.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/angular/dst/dist/polyfills.bundle.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/angular/dst/dist/main.bundle.js"/>"></script>
    <%--end of the angular app--%>
    
    <script type="text/javascript" src="<c:url value="/resources/jquery-3.2.1.min.js"/>"></script>
    <!-- swtich between English and Spanish -->
    <script type="text/javascript">
        url = window.location.href;
        var english = "Your session has expired for security reasons. Sign in or refresh your search.";
        var spanish = "Su sesión ha caducado por razones de seguridad. Inicia sesión o actualiza tu búsqueda.";
        $(document).ready(function() {
        if(url.indexOf('&lang=')>0)
        {
            if(url.indexOf('&lang=en')>0){
                $("#warning").text("Your session has expired for security reasons. Sign in or refresh your search.");
                $('#footer-button').attr('aria-label',"press enter to go back to plan results");
            }
            if(url.indexOf('&lang=es')>0)
            {
                $("#warning").text("Su sesi\xF3n ha caducado por razones de seguridad. Inicia sesi\xF3n o actualiza tu b\xFAsqueda.");
                $('#footer-button').attr('aria-label',"pulse Intro para volver a planificar los resultados");
            }
        }else{
                $("#warning").text("Your session has expired for security reasons. Sign in or refresh your search.");
                $('#footer-button').attr('aria-label',"press enter to go back to plan results");
        }
        });
    </script>
    
    <%--Start session inactivity--%>
    <div id="activityModal" class="modal hide fade" tabindex="-1" role="alertdialog" aria-labelledby="timeout-warning">
            <div class="modal-dialog" role="document">
                <div class="modal-content"  >
                    <div class="modal-body" aria-live="assertive" id="timeout-warning">
                        <p id="warning" role="alert" >Your session has expired for security reasons. Sign in or refresh your search.</p>
                    </div>
                    <div class="modal-footer"  >
                        <button id="footer-button" class="btn btn-primary" aria-label="press enter to go back to plan results" onclick="goToCancel();">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!--set focus on modal when it's active -->
        <script type="text/javascript">
            $(document).ready(function() {
                console.log("execute here!");
                $('#activityModal').on('shown.bs.modal', function () {
                    $('#timeout-warning').focus();
                    console.log("focus!");
                  });
            });
        </script>
  	
      <script type="text/javascript" src="<c:url value="/resources/js/jquery-3.2.1.min.js" />"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>  
    <script type="text/javascript"> 
          var pingUrl = ""; 
          var authenticatedPage = "false"; 
          var extendSessionUrl = "/hix/preeligibility/session/ping"; 
          var clientInactivityThreshold = "${clientInactivityThreshold}"; 
          var popupThresholdValue = "${popupThresholdValue}"; 
          
          function goToCancel() {
        	  	window.location = $("#jsp_dstCancelRedirectUrl").val();
          }
          
          window.history.pushState(null, "", window.location.href);        
          window.onpopstate = function() {
              window.history.pushState(null, "", window.location.href);
          };
    </script>
    <script type="text/javascript" src="<c:url value="/resources/js/sessioninactivity.js" />"></script>
     <%--End session inactivity--%>
</body>
</html>