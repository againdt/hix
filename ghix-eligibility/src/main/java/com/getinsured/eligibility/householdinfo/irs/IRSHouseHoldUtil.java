package com.getinsured.eligibility.householdinfo.irs;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

import us.gov.treasury.irs.common.CompletePersonNameType;
import us.gov.treasury.irs.common.DependentGrpType;
import us.gov.treasury.irs.common.DependentPersonType;
import us.gov.treasury.irs.common.EPDPersonType;
import us.gov.treasury.irs.common.HouseholdType;
import us.gov.treasury.irs.common.IRSHouseholdGrpType;
import us.gov.treasury.irs.common.PersonAddressGrpType;
import us.gov.treasury.irs.common.PersonInformationType;
import us.gov.treasury.irs.common.PrimaryGrpType;
import us.gov.treasury.irs.common.SpouseGrpType;
import us.gov.treasury.irs.common.StateType;
import us.gov.treasury.irs.common.TaxHouseholdCoverageType;
import us.gov.treasury.irs.common.TaxHouseholdType;
import us.gov.treasury.irs.common.USAddressGrpType;

/**
 * @author jape_g
 *
 */
public class IRSHouseHoldUtil {
	private static final Logger LOGGER = Logger.getLogger(IRSHouseHoldUtil.class);
	@Autowired
	SsapApplicationRepository ssapApplicaitonRepository;
	@Autowired
	SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	@Qualifier("irsObjectFactory")
	us.gov.treasury.irs.msg.monthlyexchangeperiodicdata.ObjectFactory objectFactory;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	

	/**
	 * @param irsHouseholdClientRequest
	 * @return List of IRSHouseholdGrpType object
	 * @throws GIException
	 */
	public List<IRSHouseholdGrpType> getIrsHouseholdInfo(IRSHouseholdClientRequest irsHouseholdClientRequest) throws GIException {
		List<IRSHouseholdGrpType> irsHouseholdInfoList = new ArrayList<IRSHouseholdGrpType>();
		IRSHouseholdGrpType irsHouseholdClientResponse = new IRSHouseholdGrpType();
		TaxHouseholdType taxHouseholdresp =  new TaxHouseholdType();
		if (irsHouseholdClientRequest == null || irsHouseholdClientRequest.getHouseholdCaseId() == null) {
			LOGGER.error("IRS household information request empty");
		}
		try {
			Date benefitEffectiveDateFormat;
			Timestamp benefitEffectiveDate = null;
			int benefitEffectiveMonth = 0;
			int currentMonth;

			BigDecimal cmrHouseholdId = new BigDecimal(irsHouseholdClientRequest.getHouseholdCaseId());
			String requestMonth = irsHouseholdClientRequest.getMonth();
			String requestYear = irsHouseholdClientRequest.getYear();
			int requestedMonth = (Integer.valueOf(requestMonth));
			List<Long> ssapApplicationIdList = irsHouseholdClientRequest.getSsapApplicationList();
			Boolean copyPreviousMonthData, lCEvent = null, addHouseholEachmonth = null;
			String benefitEffective = irsHouseholdClientRequest.getBenefitEffectiveDate();
			if (!benefitEffective.isEmpty()) {
				benefitEffectiveDateFormat = DateUtil.StringToDate(benefitEffective, "yyyy-MM-dd");
				long benefitEffectiveTime = benefitEffectiveDateFormat.getTime();
				benefitEffectiveDate = new Timestamp(benefitEffectiveTime);
			}

			if (benefitEffectiveDate != null) {
				benefitEffectiveMonth = benefitEffectiveDate.getMonth();
			}

			List<SsapApplication> ssapList = this.getSsapApplicaitonRepository().findLatestUpdateByCmrHouseoldId(cmrHouseholdId, Long.valueOf(requestYear));
			LOGGER.info("Inside the getIrsHouseholdInfo API for cmrHouseholdId -->" + cmrHouseholdId);
			if (ssapList.size() > 0) {
				// loop through months

				for (currentMonth = 0; currentMonth < requestedMonth; currentMonth++) {
					copyPreviousMonthData = true; // flag to getPreviousMonth data
					TaxHouseholdCoverageType taxHouseholdcoverageresp = new TaxHouseholdCoverageType();
					// TaxHouseholdType taxHouseholdresp = new TaxHouseholdType();
					HouseholdType householdListEachMonth = null;
					Set<DependentGrpType> dependents = new HashSet<DependentGrpType>();
					PersonInformationType otherPerson = null;
					Timestamp createDate = null;
					Date date1 = getStartDateForMonth(currentMonth, Integer.valueOf(requestYear));
					long time1 = date1.getTime();
					Timestamp fromdate = new Timestamp(time1);
					Date date = getStartDateForNextMonth(currentMonth, Integer.valueOf(requestYear));
					long time = date.getTime();
					Timestamp todate = new Timestamp(time);

					// loop through application

					for (SsapApplication currentApplication : ssapList) { // go through whole list of apps
						HouseholdType household = null;
						createDate = currentApplication.getCreationTimestamp();
						String applicationType = currentApplication.getApplicationType();

						if (createDate != null && createDate.before(todate) && createDate.after(fromdate)) {

							// HIX-67113 if application's financial status changes take latest application
							Long changeInFinancialStatusApplicationId = findChangeInFinancialStatus(ssapList, fromdate, todate);
							copyPreviousMonthData = false;

							// lCEvent flag will show whether this life changing event happened in that particular month.
							if (applicationType.equals("SEP")) {

								lCEvent = true;
							} else {

								lCEvent = false;
							}

							if (!ssapList.isEmpty()) {

								if (changeInFinancialStatusApplicationId != null && changeInFinancialStatusApplicationId != 0) {
									if (changeInFinancialStatusApplicationId.equals(currentApplication.getId())) {
										household = populateApplicantInfo(currentApplication, currentMonth);
									}

								} else {
									household = populateApplicantInfo(currentApplication, currentMonth);
								}

								// Make household list for each month

								// For non - financial Application HIX-63998
								if (household != null) {
									if (currentApplication.getFinancialAssistanceFlag().equals("N")) {

										if (household.getPrimaryGrp() != null) {
											otherPerson = household.getPrimaryGrp().getPrimary();
										}

									} else {
										if (householdListEachMonth == null) {
											householdListEachMonth = new HouseholdType();
										}
										// set Primary Group
										if (household.getPrimaryGrp() != null) {
											householdListEachMonth.setPrimaryGrp(household.getPrimaryGrp());
										}

										// add spouse HIX-63999

										if (household.getSpouseGrp() != null) {

											householdListEachMonth.setSpouseGrp(household.getSpouseGrp());
										}

										// add Dependent Group

										if (household.getDependentGrp() != null && !household.getDependentGrp().isEmpty()) {

											for (DependentGrpType dependent : household.getDependentGrp()) {
												dependents.add(dependent);
											}
										}
									}
								}
							}
						}

					}

					if (!copyPreviousMonthData) {
						if (dependents != null && dependents.size() > 0) {
							householdListEachMonth.getDependentGrp().addAll(dependents);
						}
						if (householdListEachMonth != null) {
							taxHouseholdcoverageresp.setHousehold(householdListEachMonth);
						}
						taxHouseholdcoverageresp.setOtherRelevantAdult(otherPerson);
						taxHouseholdcoverageresp.setApplicableCoverageMonthNum("" + currentMonth);
						taxHouseholdresp.getTaxHouseholdCoverage().add(taxHouseholdcoverageresp);

					}

					/*
					 * when there is no application found for that particular month, then copy previous month's data
					 */

					if (copyPreviousMonthData) {

						// taxhousehold coverege is not empty
						if (taxHouseholdresp.getTaxHouseholdCoverage() != null && taxHouseholdresp.getTaxHouseholdCoverage().size() > 0) {

							// if it is LCE , then copy the application with EN status.
							if (lCEvent != null && lCEvent) {

								for (SsapApplication application : ssapList) {

									if (application.getApplicationStatus().equals("EN")) {

										HouseholdType household = populateApplicantInfo(application, currentMonth);

										// For non - financial Application HIX-63998
										if (application.getFinancialAssistanceFlag().equals("N") && household != null) {

											if (household.getPrimaryGrp() != null) {
												otherPerson = household.getPrimaryGrp().getPrimary();
											}
											taxHouseholdcoverageresp.setOtherRelevantAdult(otherPerson);
											household = removePrimaryGrp(household);
										} else if (household != null) {

											taxHouseholdcoverageresp.setHousehold(household);
										}

										taxHouseholdcoverageresp.setApplicableCoverageMonthNum("" + currentMonth);

										taxHouseholdresp.getTaxHouseholdCoverage().add(taxHouseholdcoverageresp);
										break;
									}

								}

								lCEvent = false;

							} else {
								// copy previous month data when it is not LCE event
								TaxHouseholdCoverageType taxHouseholdcoverge = getPreviousMonthData(taxHouseholdresp, currentMonth);

								taxHouseholdresp.getTaxHouseholdCoverage().add(taxHouseholdcoverge);

							}

						} else {

							// taxHouseholdCoverage is empty because application is created in future date and looping through earlier month
							/*
							 * This if will take care different year case condition. when application got created in November- December of 2014 and benefit effective date is 1-1-2015.
							 */
							if (!ssapList.isEmpty()) {
								if (irsHouseholdClientResponse.getTaxHousehold().size() == 0) {
									for (SsapApplication application : ssapList) {
										// this check is for different year case.

										if (application.getCreationTimestamp().before(todate)) {
											addHouseholEachmonth = true;
											HouseholdType household = populateApplicantInfo(application, currentMonth);

											// For non - financial Application HIX-63998
											if (application.getFinancialAssistanceFlag().equals("N") && household != null) {
												if (household.getPrimaryGrp() != null) {
													otherPerson = household.getPrimaryGrp().getPrimary();
												}

												household = removePrimaryGrp(household);
											} else if (household != null) {
												if (householdListEachMonth == null) {
													householdListEachMonth = new HouseholdType();
												}
												if (household.getPrimaryGrp() != null) {
													householdListEachMonth.setPrimaryGrp(household.getPrimaryGrp());
												}

												// add spouse HIX-63999

												if (household.getSpouseGrp() != null) {

													householdListEachMonth.setSpouseGrp(household.getSpouseGrp());
												}

												// add Dependent Group

												if (household.getDependentGrp() != null && !household.getDependentGrp().isEmpty()) {

													for (DependentGrpType dependent : household.getDependentGrp()) {
														dependents.add(dependent);
													}
												}

											}

										}
									}

									if (addHouseholEachmonth != null && addHouseholEachmonth) {
										if (dependents != null && dependents.size() > 0) {
											householdListEachMonth.getDependentGrp().addAll(dependents);
										}
										if (householdListEachMonth != null) {
											taxHouseholdcoverageresp.setHousehold(householdListEachMonth);
										}
										if (otherPerson != null) {
											taxHouseholdcoverageresp.setOtherRelevantAdult(otherPerson);
										}
										taxHouseholdcoverageresp.setApplicableCoverageMonthNum("" + currentMonth);
										taxHouseholdresp.getTaxHouseholdCoverage().add(taxHouseholdcoverageresp);

									}
								}
							}
						}
					}
				}

				// Skip Household Logic
				/*
				 * pull the application from database by ssap ID and use that application to create taxhouseholdCoverage object
				 */
			} else if (ssapApplicationIdList != null && !ssapApplicationIdList.isEmpty() && ssapApplicationIdList.size() > 0) {
				TaxHouseholdCoverageType taxHouseholdcoverageresp = new TaxHouseholdCoverageType();

				HouseholdType householdListEachMonth = new HouseholdType();
				Set<DependentGrpType> dependents = new HashSet<DependentGrpType>();
				PersonInformationType otherPerson = null;
				Boolean addHousehold = null;
				Boolean copyEarlierMonth = null;

				// loop through the month
				for (int month = 0; month < requestedMonth; month++) {
					addHousehold = false;
					copyEarlierMonth = true;

					// loop though the ssap Application List
					for (Long id : ssapApplicationIdList) {

						// get the SSAP Application for that particular Id.
						List<SsapApplication> ssapApplicationList = this.getSsapApplicaitonRepository().getApplicationsById(id);

						Timestamp createDate = null;
						Date date1 = getStartDateForMonth(month, Integer.valueOf(requestYear));
						long time1 = date1.getTime();
						Timestamp fromdate = new Timestamp(time1);
						Date date = getStartDateForNextMonth(month, Integer.valueOf(requestYear));
						long time = date.getTime();
						Timestamp todate = new Timestamp(time);
						if (ssapApplicationList.size() > 0) {

							// loop though the ssap apps
							for (SsapApplication ssapApp : ssapApplicationList) {
								createDate = ssapApp.getCreationTimestamp();
								if (createDate != null && createDate.before(todate) && createDate.after(fromdate)) {
									addHousehold = true;
									copyEarlierMonth = false;
									HouseholdType household = populateApplicantInfo(ssapApp, month);
									if (ssapApp.getFinancialAssistanceFlag().equals("N") && household != null) {

										if (household.getPrimaryGrp() != null) {
											otherPerson = household.getPrimaryGrp().getPrimary();
										}

									} else if (household != null) {

										if (householdListEachMonth == null) {
											householdListEachMonth = new HouseholdType();
										}
										// set Primary Group
										if (household.getPrimaryGrp() != null) {
											householdListEachMonth.setPrimaryGrp(household.getPrimaryGrp());
										}
										// add spouse HIX-63999

										if (household.getSpouseGrp() != null) {

											householdListEachMonth.setSpouseGrp(household.getSpouseGrp());
										}

										// add Dependent Group

										if (household.getDependentGrp() != null && !household.getDependentGrp().isEmpty()) {

											for (DependentGrpType dependent : household.getDependentGrp()) {
												dependents.add(dependent);
											}
										}
									}

								} else if (month > createDate.getMonth() - 1) {

								}

							}
						}
					}

					// add to response list
					if (addHousehold != null && addHousehold) {
						if (dependents != null && dependents.size() > 0) {
							householdListEachMonth.getDependentGrp().addAll(dependents);
						}
						if (householdListEachMonth != null && !householdListEachMonth.toString().isEmpty()) {
							taxHouseholdcoverageresp.setHousehold(householdListEachMonth);
						}
						if (otherPerson != null) {
							taxHouseholdcoverageresp.setOtherRelevantAdult(otherPerson);
						}
						taxHouseholdcoverageresp.setApplicableCoverageMonthNum("" + month);
						taxHouseholdresp.getTaxHouseholdCoverage().add(taxHouseholdcoverageresp);

					}

					if (copyEarlierMonth && !taxHouseholdresp.getTaxHouseholdCoverage().isEmpty()) {

						TaxHouseholdCoverageType taxHouseholdcoverge = getPreviousMonthData(taxHouseholdresp, month);
						taxHouseholdresp.getTaxHouseholdCoverage().add(taxHouseholdcoverge);

					}
				}
			}

			/*
			 * This section will take care, benefit coverage started and application created later. e.g.- Referral applications copy 1st object in the response list to those missing months.
			 */

			if (!taxHouseholdresp.getTaxHouseholdCoverage().isEmpty()) {
				int applicationCoverageMonth = Integer.parseInt(taxHouseholdresp.getTaxHouseholdCoverage().get(0).getApplicableCoverageMonthNum());

				if (applicationCoverageMonth != benefitEffectiveMonth) {

					for (int m = benefitEffectiveMonth; m < applicationCoverageMonth; m++) {

						TaxHouseholdCoverageType taxHousehold = getNextMonthData(taxHouseholdresp, m);
						taxHouseholdresp.getTaxHouseholdCoverage().add(taxHousehold);

					}
				}

			} else {

				TaxHouseholdCoverageType taxHouseholdcoverageresp = new TaxHouseholdCoverageType();
				HouseholdType household = null;

				// HIX-63239
				/*
				 * Fix for bug - IRS: Household id whose benefit started in January is not reported in February month's irs report.
				 */
				if (ssapList.size() > 0) {
					for (int m = 0; m < requestedMonth; m++) {
						Date date1 = getStartDateForMonth(m, Integer.valueOf(requestYear));
						long time1 = date1.getTime();
						Timestamp fromdate = new Timestamp(time1);
						Date date = getStartDateForNextMonth(m, Integer.valueOf(requestYear));
						long time = date.getTime();
						Timestamp todate = new Timestamp(time);
						Long changeInFinancialStatusApplicationId = findChangeInFinancialStatus(ssapList, fromdate, todate);

						if (changeInFinancialStatusApplicationId != null && changeInFinancialStatusApplicationId != 0) {
							for (SsapApplication ssapApp : ssapList) {
								if (ssapApp.getId() == changeInFinancialStatusApplicationId) {
									household = populateApplicantInfo(ssapApp, m);
								}
							}

						} else if (m == 0) {

							household = populateApplicantInfo(ssapList.get(0), benefitEffectiveMonth);

							// For non - financial Application HIX-63998
							if (ssapList.get(0).getFinancialAssistanceFlag().equals("N") && household != null) {

								PersonInformationType otherPerson = new PersonInformationType();

								if (household.getPrimaryGrp() != null) {
									otherPerson = household.getPrimaryGrp().getPrimary();
								}
								taxHouseholdcoverageresp.setOtherRelevantAdult(otherPerson);
								household = removePrimaryGrp(household);
							} else if (household != null) {
								taxHouseholdcoverageresp.setHousehold(household);
							}
							taxHouseholdcoverageresp.setApplicableCoverageMonthNum("" + m);
							taxHouseholdresp.getTaxHouseholdCoverage().add(taxHouseholdcoverageresp);

						} else if (!taxHouseholdresp.getTaxHouseholdCoverage().isEmpty()) {
							TaxHouseholdCoverageType taxHouseholdcoverge = getPreviousMonthData(taxHouseholdresp, m);

							taxHouseholdresp.getTaxHouseholdCoverage().add(taxHouseholdcoverge);

						}
					}
				}
			}

			/*
			 * Finally Create the response list from the Taxhouseld type object
			 */
			if (taxHouseholdresp != null) {
				// Sorting TaxHouseholdCoverageType list

				if (!taxHouseholdresp.getTaxHouseholdCoverage().isEmpty() && taxHouseholdresp.getTaxHouseholdCoverage().size() > 0) {
					Collections.sort(taxHouseholdresp.getTaxHouseholdCoverage(), new IRSHouseHoldListComparator());

					irsHouseholdInfoList = createIrsResponseList(taxHouseholdresp, cmrHouseholdId);
				}
			}

			LOGGER.info("Inside end of getIrsHouseholdInfo API for cmrHouseholdId -->" + cmrHouseholdId);

			LOGGER.info("IRS HOUSEHOLD INFO LIST for CMR_HOUSEHOLD_ID" + cmrHouseholdId + "---------> " + irsHouseholdInfoList);

		} catch (Exception e) {
			LOGGER.error(" Exception during irshousehold info request  " + e.getMessage(),e);
			throw new GIException("Exception during irshousehold info request creation, mapping", e );
		}
		
		return irsHouseholdInfoList;
	}

	 
	/**
	 * @param ssap
	 * @param fromdate
	 * @param todate
	 * @return ssapId of financial application
	 * gives the Financial application of that particular month if there is any change in application status
	 */
	private Long findChangeInFinancialStatus(List<SsapApplication> ssap, Timestamp fromdate, Timestamp todate) {
		long appId = 0;
		try {
			String FinancialStatus = null;
			Boolean changeInFinancialStatus = null;
			Timestamp creationDate = null;

			for (int k = 0; k < ssap.size(); k++) {
				creationDate = ssap.get(k).getCreationTimestamp();
				if (creationDate != null && creationDate.before(todate) && creationDate.after(fromdate)) {
					if (k == 0) {
						FinancialStatus = ssap.get(k).getFinancialAssistanceFlag();
						if (FinancialStatus.equals("Y")) {
							appId = ssap.get(k).getId();
						}
					} else if (FinancialStatus != null && FinancialStatus.equals(ssap.get(k).getFinancialAssistanceFlag())) {
						changeInFinancialStatus = false;

					} else {
						FinancialStatus = ssap.get(k).getFinancialAssistanceFlag();
						changeInFinancialStatus = true;
						if (FinancialStatus.equals("Y")) {
							appId = ssap.get(k).getId();
						}

					}
				}
			}
			if (changeInFinancialStatus != null && changeInFinancialStatus) {
				return appId;
			}

		}
		catch(Exception e){
			
		}
		return appId;
	}

	
	/**
	 * creates final response list
	 * @param taxHouseholdresp
	 * @param cmrHouseholdId
	 * @return List of objects IRSHouseholdGrpType
	 */
	private List<IRSHouseholdGrpType> createIrsResponseList(TaxHouseholdType taxHouseholdresp, BigDecimal cmrHouseholdId) {
		List<IRSHouseholdGrpType> irsHouseholdInfoList = new ArrayList<IRSHouseholdGrpType>();
		IRSHouseholdGrpType irsHouseholdClientResponse = new IRSHouseholdGrpType();
		try{
			
			if(taxHouseholdresp != null && cmrHouseholdId != null){
				irsHouseholdClientResponse.getTaxHousehold().add(taxHouseholdresp);
			irsHouseholdClientResponse.setIRSGroupIdentificationNum(String
					.valueOf(cmrHouseholdId));
			irsHouseholdInfoList
			.add(irsHouseholdClientResponse);
			
			}
		}catch(Exception e){
			LOGGER.error("Exception during genereateIrsResponseList, mapping"+e.getMessage(), e);
		}
		return irsHouseholdInfoList;
	}

	
	/**
	 * @param household
	 * @return HouseholdType
	 * It removes the Primary Grp from household Type when it is Non-Financial Application
	 */
	private HouseholdType removePrimaryGrp(HouseholdType household) {
		HouseholdType householdEachMonth = null;
		try{
		 householdEachMonth = new HouseholdType();
		 Set <DependentGrpType> dependentset= new HashSet<DependentGrpType> ();
		 
		 if(household != null){
			 if(household.getDependentGrp() != null && !household.getDependentGrp().isEmpty()){
					
					
					for(DependentGrpType dependent : household.getDependentGrp()){
						dependentset.add(dependent);
					}
			 }
			 householdEachMonth.setSpouseGrp(household.getSpouseGrp());
			 householdEachMonth.getDependentGrp().addAll(dependentset);
		 }
		
		
		}
		catch(Exception e){
			LOGGER.error("Exception during HouseholdInfor request creation, mapping"+e.getMessage(), e);
			
		}
		
		// TODO Auto-generated method stub
		return householdEachMonth;
	}

	
	/**
	 * @param ssapApplication
	 * @param month
	 * @return HouseholdType object
	 * This will return HouseholdType object for the particular month
	 * This method will fetch data from JSON, and enter into respective tags of householdType object.
	 */
	private HouseholdType populateApplicantInfo(SsapApplication ssaps, int month) {
		LOGGER.info("Populating Household Info for IRS Request");

		HouseholdType household = new HouseholdType();
		try {
			String ssapJson = null;
			if (ssaps != null) {
				ssapJson = ssaps.getApplicationData();
			}
			LOGGER.info(ssapJson);
			SingleStreamlinedApplication ssapApplication = null;

			// Dependent and household
			DependentGrpType members = new DependentGrpType();
			DependentPersonType member = null;
			CompletePersonNameType personName = null;

			// Spouse HIX-63999
			SpouseGrpType spouseGrp = new SpouseGrpType();
			EPDPersonType spouse = new EPDPersonType();

			// primary applicant
			PrimaryGrpType primaryApplicant = new PrimaryGrpType();
			PersonInformationType primary = null;
			CompletePersonNameType completePersonName = null;
			PersonAddressGrpType personAddress = new PersonAddressGrpType();
			USAddressGrpType address = null;
			StateType state = null;

			int spouseId = 0;
			if (ssapJson != null) {
				ssapApplication = ssapJsonBuilder.transformFromJson(ssapJson);
			}
			int primaryTaxpersonId = 1;
			// for financial application always add primary tax person in Primary GRP

			if (ssaps.getFinancialAssistanceFlag().equals("Y") && ssapApplication != null) {
				if (ssapApplication.getPrimaryTaxFilerPersonId() != null) {
					primaryTaxpersonId = ssapApplication.getPrimaryTaxFilerPersonId();

					// spouseID of primaryTaxfiler HIX-63999

					spouseId = getSpouseId(ssapApplication, primaryTaxpersonId);
				}
			}

			Map<String, String> uniqueMemberMap = new HashMap<String, String>();
			List<HouseholdMember> householdMembers = ((TaxHousehold) ssapApplication.getTaxHousehold().get(0)).getHouseholdMember();
			// List<JSONObject> taxHousehold = JsonPath.read(ssapJson, "$.singleStreamlinedApplication.taxHousehold[*].householdMember[*]");
			for (HouseholdMember householdMember : householdMembers) {
				int personId = householdMember.getPersonId();
				String firstName = householdMember.getName().getFirstName();
				String lastName = householdMember.getName().getLastName();
				String middleName = householdMember.getName().getMiddleName();
				String suffix = householdMember.getName().getSuffix();
				String ssn = householdMember.getSocialSecurityCard().getSocialSecurityNumber();
				Date dobirth = householdMember.getDateOfBirth();
				XMLGregorianCalendar dob = changeDateFormatToXMLGregorianCalendar(dobirth);
				String street1 = householdMember.getHouseholdContact().getHomeAddress().getStreetAddress1();
				String street2 = householdMember.getHouseholdContact().getHomeAddress().getStreetAddress2();
				String city = householdMember.getHouseholdContact().getHomeAddress().getCity();
				if ((householdMember.getHouseholdContact().getHomeAddress().getState() != null) && !(householdMember.getHouseholdContact().getHomeAddress().getState().toString().isEmpty())) {
					state = StateType.fromValue(householdMember.getHouseholdContact().getHomeAddress().getState());
				}
				String zip = householdMember.getHouseholdContact().getHomeAddress().getPostalCode();
				String countyCode = householdMember.getHouseholdContact().getHomeAddress().getCountyCode();
				String personGuId = householdMember.getApplicantGuid();
				boolean isSeekingCoverage = householdMember.getApplyingForCoverageIndicator();

				// if name on SSN card is different use that name
				if (householdMember.getSocialSecurityCard().getNameSameOnSSNCardIndicator() != null && !householdMember.getSocialSecurityCard().getNameSameOnSSNCardIndicator()) {
					firstName = householdMember.getSocialSecurityCard().getFirstNameOnSSNCard();
					lastName = householdMember.getSocialSecurityCard().getLastNameOnSSNCard();
					middleName = householdMember.getSocialSecurityCard().getMiddleNameOnSSNCard();
					suffix = householdMember.getSocialSecurityCard().getSuffixOnSSNCard();
				}
				if (uniqueMemberMap.containsKey(personGuId)) {
					// don't add person
				} else {
					if (primaryTaxpersonId == personId) {
						completePersonName = new CompletePersonNameType();
						if (firstName != null && firstName.trim().length() > 0) {
							completePersonName.setPersonFirstName(firstName);
						}

						if (lastName != null && lastName.trim().length() > 0) {
							completePersonName.setPersonLastName(lastName);
						}

						if (middleName != null && middleName.trim().length() > 0) {
							completePersonName.setPersonMiddleName(middleName);
						}
						if (!StringUtils.isEmpty(suffix)) {
							completePersonName.setSuffixName(suffix);
						}
						primary = new PersonInformationType();
						primary.setCompletePersonName(completePersonName);
						if (dob != null) {
							primary.setBirthDt(dob);
						}
						if (ssn != null && ssn.trim().length() > 0 && householdMember.getApplyingForCoverageIndicator()) {
							primary.setSSN(ssn);
						}
						address = new USAddressGrpType();
						if (street1 != null && !StringUtils.isEmpty(street1)) {
							address.setAddressLine1Txt(street1);
						}
						if (street2 != null && !StringUtils.isEmpty(street2)) {
							address.setAddressLine2Txt(street2);
						}
						if (city != null && !StringUtils.isEmpty(city)) {
							address.setCityNm(city);
						}
						if (state != null && !StringUtils.isEmpty(state.toString())) {
							address.setUSStateCd(state);
						}
						if (zip != null && !StringUtils.isEmpty(zip)) {
							address.setUSZIPCd(zip);
						}
						if (countyCode != null && !StringUtils.isEmpty(countyCode)) {
							address.setUSZIPExtensionCd(countyCode);
						}
						personAddress.setUSAddressGrp(address);
						primary.setPersonAddressGrp(personAddress);

						if (personGuId != null && personGuId.trim().length() > 0) {
							primary.setApplicantGuid(personGuId);
						}
						primary.setIsSeekingCoverage(isSeekingCoverage);
						primaryApplicant.setPrimary(primary);
						uniqueMemberMap.put(personGuId, "YES");

					} else if (ssaps.getFinancialAssistanceFlag().equals("Y")) {

						if (spouseId != 0 && spouseId == personId) {
							personName = new CompletePersonNameType();
							if (firstName != null && firstName.trim().length() > 0) {
								personName.setPersonFirstName(firstName);
							}
							if (lastName != null && lastName.trim().length() > 0) {
								personName.setPersonLastName(lastName);
							}
							if (middleName != null && middleName.trim().length() > 0) {
								personName.setPersonMiddleName(middleName);
							}
							spouse.setCompletePersonName(personName);
							if (dob != null) {
								spouse.setBirthDt(dob);
							}
							if (ssn != null && ssn.trim().length() > 0 && householdMember.getApplyingForCoverageIndicator()) {
								spouse.setSSN(ssn);
							}

							if (personGuId != null && personGuId.trim().length() > 0) {
								spouse.setApplicantGuid(personGuId);
							}
							spouse.setIsSeekingCoverage(isSeekingCoverage);
							spouseGrp.setSpouse(spouse);
							household.setSpouseGrp(spouseGrp);
							uniqueMemberMap.put(personGuId, "YES");

						} else {
							personName = new CompletePersonNameType();
							if (firstName != null && firstName.trim().length() > 0) {
								personName.setPersonFirstName(firstName);
							}
							if (lastName != null && lastName.trim().length() > 0) {
								personName.setPersonLastName(lastName);
							}
							if (middleName != null && middleName.trim().length() > 0) {
								personName.setPersonMiddleName(middleName);
							}
							member = new DependentPersonType();
							members = new DependentGrpType();
							member.setCompletePersonName(personName);
							if (dob != null) {
								member.setBirthDt(dob);
							}
							if (ssn != null && ssn.trim().length() > 0 && householdMember.getApplyingForCoverageIndicator()) {
								member.setSSN(ssn);
							}
							if (personGuId != null && personGuId.trim().length() > 0) {
								member.setApplicantGuid(personGuId);
							}
							member.setIsSeekingCoverage(isSeekingCoverage);
							members.setDependentPerson(member);
							household.getDependentGrp().add(members);
							uniqueMemberMap.put(personGuId, "YES");
						}
					}
				}
			}
			household.setPrimaryGrp(primaryApplicant);
		} catch (Exception e) {
			LOGGER.error("Exception during HouseholdInfor request creation, mapping" + e.getMessage(), e);
		}
		return household;
	}
	
	
	// get spouse id  HIX-63999	
		/**
		 * @param ssapApplication
		 * @param primaryTaxpersonId
		 * @return spouse Id
		 * 
		 */
	private int getSpouseId(SingleStreamlinedApplication ssapApplication, int primaryTaxpersonId) {

		int spouseId = 0;
		try {
			List<HouseholdMember> householdMembers = ((TaxHousehold) ssapApplication.getTaxHousehold().get(0)).getHouseholdMember();

			for (HouseholdMember householdMember : householdMembers) {
				if (householdMember.getPersonId() == primaryTaxpersonId) {
					for (BloodRelationship relation : householdMember.getBloodRelationship()) {
						if (relation.getIndividualPersonId().equals(String.valueOf(householdMember.getPersonId())) && relation.getRelation().equals("01")) {
							spouseId = Integer.valueOf(relation.getRelatedPersonId());

						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception during getSpouseId" + e.getMessage(), e);

		}
		return spouseId;
	}

	
	/**
	 * @param taxHouseholdType
	 * @param month
	 * @return TaxHouseholdCoverageType object
	 */
	private TaxHouseholdCoverageType getPreviousMonthData(TaxHouseholdType taxHouseholdType, int month) {

		LOGGER.info("Getting Previous month data Household Info for IRS Request taxHousehold type" + taxHouseholdType);

		// TaxHouseholdType taxHouseholdresp = new TaxHouseholdType();
		TaxHouseholdCoverageType taxHouseholdcoverageresp = new TaxHouseholdCoverageType();

		try {

			if (taxHouseholdType != null) {

				List<TaxHouseholdCoverageType> taxcoveragelist = taxHouseholdType.getTaxHouseholdCoverage();

				for (TaxHouseholdCoverageType taxHouseholdcoverage : taxcoveragelist) {
					if (taxHouseholdcoverage != null) {
						if (Integer.parseInt(taxHouseholdcoverage.getApplicableCoverageMonthNum()) == (month - 1)) {
							taxHouseholdcoverageresp.setHousehold(taxHouseholdcoverage.getHousehold());
							if (taxHouseholdcoverage.getOtherRelevantAdult() != null) {
								taxHouseholdcoverageresp.setOtherRelevantAdult(taxHouseholdcoverage.getOtherRelevantAdult());
							}
						}
					}

				}

			}

			taxHouseholdcoverageresp.setApplicableCoverageMonthNum("" + month);

		} catch (Exception e) {
			LOGGER.error("Exception during copying previous month data of HouseholdInfo" + e.getMessage(), e);

		}

		return taxHouseholdcoverageresp;
	}

      
	/**
	 * @param taxHouseholdType
	 * @param month
	 * @return TaxHouseholdCoverageType
	 */
	private TaxHouseholdCoverageType getNextMonthData(TaxHouseholdType taxHouseholdType, int month) {

		LOGGER.info("Getting Previous month data Household Info for IRS Request taxHousehold type" + taxHouseholdType);

		// TaxHouseholdType taxHouseholdresp = new TaxHouseholdType();
		TaxHouseholdCoverageType taxHouseholdcoverageresp = new TaxHouseholdCoverageType();

		try {

			if (taxHouseholdType != null) {

				List<TaxHouseholdCoverageType> taxcoveragelist = taxHouseholdType.getTaxHouseholdCoverage();
				if (taxcoveragelist != null && taxcoveragelist.size() > 0) {
					taxHouseholdcoverageresp.setHousehold(taxcoveragelist.get(0).getHousehold());
					if (taxcoveragelist.get(0).getOtherRelevantAdult() != null) {
						taxHouseholdcoverageresp.setOtherRelevantAdult(taxcoveragelist.get(0).getOtherRelevantAdult());
					}

				}
				
		
		}
		
		taxHouseholdcoverageresp.setApplicableCoverageMonthNum(""+month);
		
		} catch (Exception e){
			LOGGER.error("Exception during copying next month data of HouseholdInfo"+e.getMessage(), e);
			
		}
		
		
		// TODO Auto-generated method stub
		return taxHouseholdcoverageresp;
	}


       
      
	private XMLGregorianCalendar changeDateFormatToXMLGregorianCalendar(Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		XMLGregorianCalendar xmlGregorianDate = null;
		cal.setTime(date);
		try {
			xmlGregorianDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			LOGGER.error("Error converting to XMLGregorianCalendar Date" + e.getMessage(), e);
		}
		return xmlGregorianDate;
	}



	public Date getStartDateForNextMonth(int month, int year) {
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY, calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, month + 1);
		calenderObj.set(Calendar.YEAR, year);

		return calenderObj.getTime();
	}

	
	public static Date getStartDateForMonth(int month, int year) {
		Calendar calenderObj = TSCalendar.getInstance();
		calenderObj.set(Calendar.DATE, calenderObj.getMinimum(Calendar.DATE));
		calenderObj.set(Calendar.HOUR_OF_DAY, calenderObj.getMinimum(Calendar.HOUR_OF_DAY));
		calenderObj.set(Calendar.MINUTE, calenderObj.getMinimum(Calendar.MINUTE));
		calenderObj.set(Calendar.SECOND, calenderObj.getMinimum(Calendar.SECOND));
		calenderObj.set(Calendar.MONTH, month);
		calenderObj.set(Calendar.YEAR, year);

		return calenderObj.getTime();
	}

	public SsapApplicationRepository getSsapApplicaitonRepository() {
		return ssapApplicaitonRepository;
	}

	public void setSsapApplicaitonRepository(SsapApplicationRepository ssapApplicaitonRepository) {
		this.ssapApplicaitonRepository = ssapApplicaitonRepository;
	}

	public SsapApplicantRepository getSsapApplicantRepository() {
		return ssapApplicantRepository;
	}

	public void setSsapApplicantRepository(SsapApplicantRepository ssapApplicantRepository) {
		this.ssapApplicantRepository = ssapApplicantRepository;
	}
}
