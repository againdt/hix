package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.QhpReportDTO;
import com.getinsured.hix.enrollment.service.EnrolleeService;
import com.getinsured.hix.enrollment.service.EnrollmentService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.util.TSDate;

/**
 * Class is used to provide services for QHP Reports.
 */
@Service("qhpReportService")
public class QhpReportServiceImpl implements QhpReportService {

	private static final Logger LOGGER = LoggerFactory.getLogger(QhpReportServiceImpl.class);

	@Autowired
	private EnrollmentService enrollmentService;
	@Autowired
	private EnrolleeService enrolleeService;
	@Autowired
	private GIMonitorService giMonitorService;

	/**
	 * Method is used to get List of Enrollment-ID to generate QHP Reports by Coverage Year.
	 */
	@Override
	public List<Integer> getEnrollmentIdListByCoverageYearForQHPReports(Integer coverageYear) {

		List<Integer> enrollmentIdList = enrollmentService.getEnrollmentIdListByCoverageYearForQHPReports(coverageYear);
		if (CollectionUtils.isEmpty(enrollmentIdList)) {
			return null;
		}
		return enrollmentIdList;
	}

	/**
	 * Method is used to get Enrollment and Enrollee data to generate QHP Reports.
	 */
	@Override
	public List<QhpReportDTO> getEnrollmentAndEnrolleeDataToGenerateQHPReports(Integer enrollmentId) {

		List<QhpReportDTO> qhpReportList = enrolleeService.getEnrollmentAndEnrolleeDataToGenerateQHPReports(enrollmentId);

		if (CollectionUtils.isEmpty(qhpReportList)) {
			LOGGER.error("Enrollment data is not found to generate QHP Reports.");
		}
		return qhpReportList;
	}

	/**
	 * Method is used to store QHP Report data using QhpReportDTO class.
	 */
	@Override
	public boolean storeQHPReportData(List<QhpReportDTO> qhpReportList, Long batchJobExecutionId) {

		boolean qhpReportsStatus = false;

		if (CollectionUtils.isNotEmpty(qhpReportList)) {
			LOGGER.info("Number of QHP Reports data: {}", qhpReportList.size());
			// Insert data into QHP_REPORT table
			qhpReportsStatus = enrolleeService.storeQHPReportData(qhpReportList, batchJobExecutionId.intValue());
		}
		else {
			LOGGER.error("There is no data found to store in QHP Reports table.");
		}
		return qhpReportsStatus;
	}

	/**
	 * Method is used to clean old QHP Report data from DB by Job ID.
	 */
	@Override
	public boolean cleanOldQHPReportDataByJobId(Long batchJobExecutionId) {

		boolean hasCleanedOldData = false;

		if (null != batchJobExecutionId && 0 < batchJobExecutionId) {
			hasCleanedOldData = enrolleeService.cleanOldQHPReportDataByJobId(batchJobExecutionId.intValue());
		}
		else {
			LOGGER.error("Invalid QHP Reports - Batch Job Execution ID.");
		}
		return hasCleanedOldData;
	}

	/**
	 * Method is use to save and throws QHP Report Job Error.
	 */
	public void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException {
		giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_50012", new TSDate(), this.getClass().getName(),
				errorMessage, null, errorMessage, GIRuntimeException.Component.BATCH.getComponent(), null);
		throw new UnexpectedJobExecutionException(errorMessage);
	}
}
