package com.getinsured.iex.hub.vlp.servicehandler;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.GetCaseDetailsResponseType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ObjectFactory;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.vlp.wrapper.GetCaseDetailsRequestWrapper;
import com.getinsured.iex.hub.vlp.wrapper.GetCaseDetailsResponseWrapper;

public class GetCaseDetailsServiceHandler extends AbstractServiceHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GetCaseDetailsServiceHandler.class);
	private static ObjectFactory factory;
	
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
	}
	
	public Object getRequestpayLoad() throws HubServiceException {
		Object response = null;
		
		try{
			Object getCaseDetailsRequest = getNamespaceObjects().get(GetCaseDetailsRequestWrapper.class.getName());
			if(getCaseDetailsRequest == null){
				throw new HubServiceException("Request information not available");
			}
			GetCaseDetailsRequestWrapper getCaseDetailsWrapper = (GetCaseDetailsRequestWrapper)getCaseDetailsRequest;
			setJsonInput(getCaseDetailsWrapper.toJSONString());
			response =  getCaseDetailsWrapper.getCaseDetailsRequest();
		}
		catch(Exception e){
			throw new HubServiceException("Failed to generate the payload",e);
		}
	
		return response;
	}

	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		
		String message = null;
		try{
			JAXBElement<GetCaseDetailsResponseType> resp= (JAXBElement<GetCaseDetailsResponseType>)responseObject;
			GetCaseDetailsResponseType responsePayload = resp.getValue();
			GetCaseDetailsResponseWrapper response = new GetCaseDetailsResponseWrapper(responsePayload);

			if(response != null){
				message = response.toJSONString();
				LOGGER.debug("Response Payload " + message);
				if(response.getResponseMetadata() != null){
					this.setResponseCode(response.getResponseMetadata().getResponseCode());
				}
				//Case number
				if(response != null && response.getGetCaseDetailsResult() != null){
					this.getResponseContext().put("HUB_CASE_NUMBER", response.getGetCaseDetailsResult().getCaseNumber());
				}
				this.requestStatus = RequestStatus.SUCCESS;
			}
			
			LOGGER.debug("Response message " + message);
		}catch(ClassCastException ce){
			message = "Class in response: " + responseObject.getClass().getName()+" Expecting:"+ GetCaseDetailsResponseType.class.getName();
			this.requestStatus = RequestStatus.FAILED;
			throw new HubServiceException(message, ce);
		}
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
