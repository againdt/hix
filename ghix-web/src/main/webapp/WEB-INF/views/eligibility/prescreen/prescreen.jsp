<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Estimate Your Tax Credit For Healthcare Coverage.</title>
		<!-- required styles -->
		<link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700|Droid+Serif:400,400italic,700,700italic' rel='stylesheet'/>
		<link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet"/>
		<link href="<c:url value="/resources/css/bootstrap-responsive.css"/>" rel="stylesheet"/>
		<link href="<c:url value="/resources/css/styles.css"/>" rel="stylesheet"/>
		<link href="<c:url value="/resources/css/counter.css"/>" rel="stylesheet"/>
		<link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet"/>
		<link href="<c:url value="/resources/highslide/highslide.css"/>"  rel="stylesheet" type="text/css" />
		<link href="<c:url value="/resources/css/bootstrap-editable.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/css/custom-theme/jquery-ui-1.10.0.custom.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/css/custom-theme/jquery.ui.1.10.0.ie.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/css/ui.progress-bar.css"/>" rel="stylesheet">
		<!--[if IE 8]>
			<link href="<c:url value="/resources/css/ie-fixes.css"/>" rel="stylesheet">
		 <![endif]-->
		
		<script	src="<c:url value="/resources/highslide/highslide-full.packed.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery-phix.js"/>"></script>
		<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
		<script src="<c:url value="/resources/js/bootstrap-editable.js"/>"></script>
		<script src="<c:url value="/resources/js/flipcounter.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery.easing.min.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
		<script src="<c:url value="/resources/js/flipcounter.js"/>"></script>
		<script src="<c:url value="/resources/js/chart-plugins.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery-ui-slider.js"/>"></script>
		<script src="<c:url value="/resources/js/redactor/redactor.min.js"/>"></script>
		<script src="<c:url value="/resources/js/jmapping/jquery.metadata.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery.uniform.js"/>"></script>
		<script src="<c:url value="/resources/js/chosen.jquery.min.js"/>"></script>
		<script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery.timePicker.min.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery-ui.min.js"/>"></script>
		<script src="<c:url value="/resources/js/progress.js"/>"></script>
		<script src="<c:url value="/resources/js/loader.js"/>"></script>
		<script src="<c:url value="/resources/js/jquery.maskedinput.min.js"/>"></script>
		
		<!-- Backbone scripts -->
		<script src="<c:url value="/resources/js/underscore-min-phix.js"/>"></script>
		<script src="<c:url value="/resources/js/backbone.js"/>"></script>
		<script src="<c:url value="/resources/js/Backbone.ModelBinder.js"/>"></script>
		
		<!-- Custom scripts -->
		<script src="<c:url value="/resources/js/prescreen/prescreen-model-binder-view.js"/>"></script>
		<script src="<c:url value="/resources/js/prescreen/prescreen-profile.js"/>"></script>
		<script src="<c:url value="/resources/js/prescreen/prescreen-household.js"/>"></script>
		<script src="<c:url value="/resources/js/prescreen/prescreen-income.js"/>"></script>
		<script src="<c:url value="/resources/js/prescreen/prescreen-deductions.js"/>"></script>
		<script src="<c:url value="/resources/js/prescreen/prescreen-poller.js"/>"></script>
		<script src="<c:url value="/resources/js/prescreen/prescreen-plandisplay.js"/>"></script>
		<script src="<c:url value="/resources/js/prescreen/prescreen-common.js"/>"></script>
		
		<!-- UserVoice JavaScript SDK (only needed once on a page) -->
		<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/GxnA612fKYnZZXhKtbp5Q.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>
		
		<!-- A tab to launch the Classic Widget -->
		<script>
		UserVoice = window.UserVoice || [];
		UserVoice.push(['showTab', 'classic_widget', {
		  mode: 'feedback',
		  primary_color: '#00cc03',
		  link_color: '#007dbf',
		  forum_id: 211321,
		  tab_label: 'Feedback ',
		  tab_color: '#00cc14',
		  tab_position: 'bottom-right',
		  tab_inverted: false
		}]);
		</script>
		
		<script type="text/javascript">
			hs.graphicsDir = '${pageContext.request.contextPath}/resources/highslide/graphics/';
			hs.outlineType = 'rounded-white';
			hs.showCredits = false;
			hs.wrapperClassName = 'draggable-header';
			var smrtysttkn;
		</script>
		
		<style type="text/css">
		body {
			/* background: #DADDE0;
			background-image: linear-gradient(top bottom, #DADDE0, #8E8E8E); */
			background:#fff;
		}
		
		.no-close .ui-dialog-titlebar-close {
 			 display: none; /*Hide dialog box close button*/
		}
		</style>
		
	</head>
<body>
<!-- header -->
	<div id="header" class="navbar">
		<div class="navbar-inner">
			<a class="brand hidden-phone" href="prescreen"><img
				src="<c:url value="/resources/images/gi_logo.png"/>"
				alt="Get Insured Logo">The Getinsured Eligibility Estimator</a>
			<ul class="nav">
				<li>
					<a href="#"> 
						<i class="icon-large "></i>
					</a>
				</li>
			</ul>
			<ul class="nav pull-right">
				<li>
					<a href="#" class="clickpop" data-html="true" data-toggle="popover" data-placement="bottom"  data-content="					
					<p><small>The Getinsured Eligibility Estimator leverages its industry-tested quoting engine
																 to estimate 2014 premium costs and financial assistance for a given household. It is based on the Affordable Care Act (ACA) 
																 as signed into law in 2010 and supporting regulations issued by HHS and IRS. Based 
																 on the user's location and household composition, the estimator calculates eligibility 
																 for new financial assistance under the ACA 
																(premium tax credits and cost sharing reductions) in addition to eligibility for 
																Medicaid expansion and Medicare. It is highly customized to the user's inputs.</small></p><br/>
																<p><small> See the frequently 
																asked questions for more information about how the Getinsured Eligibility Estimator works 
																and the types of financial assistance available to you.</small></p>"
																><small>About the Estimator</small></a>
				</li>
				
				<!-- <li>
					<a href="#" onclick="return hs.htmlExpand(this, { contentId: 'aboutEstimator' } )"
					class="highslide"><small>About the Estimator</small></small></a>
				</li> -->
				<li>
					<a href="javascript:newPopup('<c:out value='${fbshare}' />');"><i class="icon-large icon-facebook"></i></a>
					<%-- <a href="<c:out value='${fbshare}' />" target="_blank"><i class="icon-large icon-facebook"></i></a> --%>
					<!-- <iframe
						src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fhttps://www.facebook.com/GetInsured&amp;layout=button_count&amp;show_faces=true&amp;width=450&amp;action=like&amp;colorscheme=light&amp;height=80"
						scrolling="no" frameborder="0"
						style="border: none; overflow: hidden; width: 100px; height: 25px; margin-top: 15px; margin-left: 15px;"
						allowTransparency="true"></iframe> -->

				</li>
				<li>
					<a href="javascript:newPopup('<c:out value='${twtrshare}' />');"><i class="icon-large icon-twitter"></i></a>
					<%-- <a href="<c:out value='${twtrshare}' />" target="_blank"><i class="icon-large icon-twitter"></i></a> --%>
				</li>
				<li>
					<a href="javascript:newPopup('<c:out value='${gplusshare}' />');"><i class="icon-large icon-google-plus"></i></a>
					<%-- <a href="<c:out value='${gplusshare}' />" target="_blank"><i class="icon-large icon-google-plus"></i></a> --%>
				</li>
				<li class="dropdown" style="display:none;"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> <i class="icon-large icon-comments-alt"></i>
				</a>
					<ul class="dropdown-menu dropdown-user-account">
						<li class="account-info gutter10">
							<h3>Chat with a Getinsured Expert</h3>
							<p>
								Or Call Toll Free <strong>866-602-8466</strong> for live help.
							</p>
						</li>
						<li class="account-footer">
							<div class="row-fluid">
								<div class=" span6 muted left gutter10">
									<small>Mon - Fri 8am to 8pm EST</small>
								</div>

								<div class="span4 offset2">
									<a href="#" class="btn btn-success">Begin Chat</a>
								</div>
							</div>
						</li>
					</ul>
				</li>
			</ul>
			
			<!-- highslide for aboutEstimator -->
												<div class="highslide-html-content" id="aboutEstimator">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>About the Getinsured Eligibility Estimator</h4>
																<p>The Getinsured Eligibility Estimator leverages its industry-tested quoting engine
																 to estimate 2014 premium costs and financial assistance for a given household. Based 
																 on the user's location and household composition, the estimator calculates eligibility 
																 for new financial assistance under the Affordable Care Act 
																(premium tax credits and cost sharing reductions) in addition to eligibility for 
																Medicaid and Medicare. It is highly customized to the user's inputs. See the frequently 
																asked questions for more information about how the Getinsured Eligibility Estimator works 
																and the types of financial assistance available to you. 
</p>													
																</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
			
			
		</div>
	</div>
	<!-- end header -->		
	
	<div id="left_layout">
		<div id="main_content" class="container-fluid">			
			<div class="row-fluid">
			
				<!-- Flip Counter and Sample Plan -->
				<div class="well widget counter" style="">
				<div id="flipCounterDiv">
					<h3 class="page-title">
						<i class="icon-dashboard"></i> Your Potential Monthly Credit 
					</h3>
					<div class="span" style="text-align:center">
						<div id="wrapper">
							<img src='<c:url value="/resources/images/digits-dash.png"/>' alt="" class="digitsDashImage">
							<div id="flip-counter" class="flip-counter"></div>
						</div>
						<div id="medicaidMessageNearCounter" class="medicaidMessageNearCounter alert alert-success" style="display:none">
							<a href="http://www.healthcare.gov/using-insurance/low-cost-care/medicaid/" target="_blank" style="color:inherit">Medicaid Eligible</a>
						</div>
					</div>

					<div class="span"
						style="display:none" id="showplans">
						<h3 class='left muted ribbon'>Sample Plan</h3>
						<!-- <a href="#" class=""
							style="padding: 0px; float: none; margin: 10px 10px 0px 10px;">
							Show Plans With This Credit <i class="icon icon-arrow-right"></i>
						</a> -->
					</div>
				</div>
					<div id="sampleplan" class="ta-center" style="display: none;">
						<article  class="total" id="showhealth">
							<header id="healthplan">
								<img id="planImg" src="" alt=" carrier logo">
								<p id="planName"></p>
							</header>
							
					
							<div class="planData" id="planData">
								<h4></h4>
								<p class="muted crossedAptc">was <span class="crossed benchmarkPremium">$400</span> before credit</p>
								<ul id="planDataList">
									<!-- <li>Doctor Visits: <span class="pull-right">$3</span></li>
									<li>Physicals:<span class="pull-right">$0</span></li>
									<li>Generic Drugs: <span class="pull-right">$3</span></li>
									<li>Max Out of Pocket: <span class="pull-right">$4,500</span></li>
									<li>Deductible: <span class="pull-right">$0</span></li> -->
								</ul>
							</div>
										
							
							</article>
							<article  class="ta-center">
							<!-- Medicaid -->
							<header id="medicaidplan">
								<img
									src="<c:url value="/resources/images/medicaid.jpg"/>"
									alt="logo">
							</header>
						</article>	
							<!-- Medicare -->
						<article  class="ta-center">	
							<header id="medicareplan">
								<img
									src="<c:url value="/resources/images/medicare.jpg"/>"
									alt="logo">
							</header>
						</article>
							
					</div>
				</div>
				<!-- End of Flip Counter and Sample Plan -->

				<div id="myCarousel" class="carousel">
					<div class="carousel-inner" id="slides">
						<!-- #home start -->
						<div id="item-0">
							<div class="span8">
								<div class="well widget content-l ">
									<div class="widget-content">
										<fieldset>
											<legend>Will you qualify for health reform subsidies?</legend>
										</fieldset>
										<blockquote style="color:#fff">
										<div class="span4 offset8">
											<p>Starting January 1, 2014, the Affordable Care Act will guarantee coverage for all Americans and 
											offer tax credits to reduce premiums for nearly 26 million consumers nationwide. Are you one of them?<br/><br/>
											In 3 quick steps, we'll tell you if you qualify and estimate what your monthly tax credit will be.</p>
											<!-- <p><a id="count0" class="btn btn-info btn-large right"
											href="#item-1" data-slide="next">Let's Get Started</a></p> -->
										</div>
										</blockquote>
									</div>

									<div class="widget-footer">
										<a id="count0" class="btn btn-inverse btn-large right"
											href="#item-1" data-slide="next">Let's Get Started</a>
									</div>

								</div>
							</div>

							<div class="span4">
								<!-- widget -->
								<div class="well widget box-shadow-right content-r special">
									<div class="widget-header">
										<h3 class="title">Special circumstances?</h3>
									</div>
									<ul>
										<li><span
											data-html="true" data-content="Individuals who are incarcerated cannot receive tax credits for themselves. 
											They can only apply for subsidized health plans on behalf of their children who are legal residents."
											data-placement="top" data-toggle="popover"
											data-container="body" class="haspopover">Incarcerated
												<i class="icon-question-sign"></i>
										</span></li>
										<li><span 
											data-html="true" data-content="Individuals who are not legal US residents cannot receive tax credits for themselves. 
											They can only apply for subsidized health plans on behalf of their children who are legal residents."
											data-placement="top" data-toggle="popover"
											data-container="body" class="haspopover">Not a legal resident 
												<i class="icon-question-sign"></i>
										</span></li>
										<li><span 
											data-html="true" data-content="In general, individuals who are offered affordable healthcare coverage 
											from their employer cannot receive tax credits. Employer coverage is affordable if the plan has at least 60% actuarial value
											 and the employee's share of premiums is less than 9.5% of the employee's household income."
											data-placement="top" data-toggle="popover"
											data-container="body" class="haspopover">Have coverage
												from an employer <i class="icon-question-sign"></i>
										</span></li>
									</ul>
								</div>
								
								<!--ask start FAQ for HOME TAB-->
								<div class="well widget content-r faq1 box-shadow-right">
									<div class="widget-header">
										<h3 class="title">
											<i class="icon-comment"></i> Frequently Asked Questions
										</h3>
									</div>
									<%-- <form id="askForm" name="askForm" class="form-search">
										<input type="text" class="input-medium span9"
											placeholder="What if I'm on Medicare?">
										<button class="btn" type="submit">Ask!</button>
									</form> --%>
									<div class="tabbable tabs-custom">
										<!-- <ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#topq-1"><i
													class="icon-comments"></i>&nbsp;Top Quesions</a></li>
											<li><a data-toggle="tab" href="#helpo-1"><i
													class="icon-thumbs-up"></i>&nbsp; Help Others</a></li>

										</ul>
 -->
										<div class="">
											<div id="topq-1" class="tab-pane fade in active">
												<ul class="qlist">
													<!-- <li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'financialAssistance' } )"
													class="highslide">What type of financial assistance is available to me for private insurance coverage?</a></li> -->
													
													<li><a href="#" class="clickpop" data-html="true" data-content="In general, there are two types of financial assistance paid by the federal government: advanced premium tax 
																credits and cost sharing reductions. Premium tax credits give you immediate savings on your monthly insurance 
																premium payments. Cost sharing reductions provide you 
																better benefits when you actually go and receive medical care."
																	data-placement="top" data-toggle="popover"
																	data-container="body">
												What type of financial assistance is available to me for private insurance coverage?</a></li>
														
													<!-- <li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'premiumTax' } )"
													class="highslide">When can I use my premium tax credit? </a></li>
													 -->
													<li><a href="#" class="clickpop" data-html="true" data-content="This October, you can start shopping health plans for the 2014 coverage year."
																	data-placement="top" data-toggle="popover"
																	data-container="body">
												When can I use my premium tax credit? </a></li>
													
													<!-- <li class="ta-right"><a href="#"><small>
																View all</small></a></li> -->
												</ul>
											</div>
											
												<!-- highslide for financialAssistance -->
												<div class="highslide-html-content" id="financialAssistance">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>What type of financial assistance is available to me for private insurance coverage?</h4>
																<p>In general, there are two types of financial assistance paid by the federal government: advanced premium tax 
																credits and cost sharing reductions. Premium tax credits give you immediate savings on your monthly insurance 
																premium payments. Cost sharing reductions provide you 
																better benefits when you actually go and receive medical care.</p>													</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
												
												<!-- highslide for premiumTax -->
												<div class="highslide-html-content" id="premiumTax">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>When can I use my premium tax credit?</h4>
																<p>This October, you can start shopping health plans for the 2014 coverage year. </p>													</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
						
													
											<!-- <div id="helpo-1" class="tab-pane fade">
												<ul class="qlist">
													<li><a href="#">Should I include my spouse if I'm
															getting divorced this year?</a></li>
													<li><a href="#"> Can I claim my mother as a
															dependent if she lives with me? </a></li>
													<li class="ta-right"><a href="#"><small>
																View all</small></a></li>
												</ul>
											</div> -->
										</div>
									</div>
								</div>
								<!--ask end-->
								
							</div>
							<input id="prescreenVersionName" name="prescreenVersionName" type="hidden" value="prescreen">							
						</div>
						<!-- #home end -->

						<!-- #profile -->
						<div id="item-1">
							<div class="span8">
								<div class="well widget content-l">
									<div class="widget-content">
										<form id="profileForm" name="profileForm">
											<fieldset>
												<legend class="row-fluid">
													<span class="haspopover" 
													data-html="true" data-content="We assume that you are the primary tax filer in your household. 
													The primary tax filer is the claiming person listed on your federal tax return in 2014."
															data-placement="right" data-toggle="popover"
															class="haspopover">Ok, let's do this!  <i class="icon-question-sign"></i></span>
												</legend>
												<!-- Hidden fields  -->
												<input id="prescreenRecordId" name="prescreenRecordId" type="hidden" value="0">
												<input id="currentTab" name="currentTab" type="hidden" value="2">
												<input id="medicaidFplValue" name="medicaidFplValue" type="hidden" value="0">
												<input id="benchmarkPlanPremium" name="benchmarkPlanPremium" type="hidden" value="0">
												<input id="errorMessage" name="errorMessage" type="hidden" value="">
												<input id="errorDescription" name="errorDescription" type="hidden" value="">
												
												<!-- Progress bar -->
												<div id="progress-wrap" style="display:none">
													<h5 class="gutter10">Importing your profile	information from Turbotax.com...</h5>
													<div id="progress_bar" class="ui-progress-bar ui-container">
														<div class="ui-progress">
															<span class="ui-label" style="display: none;">Importing
																<b class="value">0%</b>
															</span>
														</div>
													</div>
												</div>
												<!-- /Progress bar -->
												
												<div id="mask1">
													<!-- <label>
														<span
															data-html="true" data-content="We assume that you are the primary tax filer in your household. 
															If not, and you wish to proceed on behalf of the primary tax filer, 
															please enter that person's name. 
															The primary tax filer is the claiming person listed on your federal tax return in 2014."
															data-placement="right" data-toggle="popover"
															class="haspopover">Who is the Primary Tax Filer? (optional)  <i class="icon-question-sign"></i>
														</span>
													</label>  -->
													<!-- First Name -->
													<input id="claimerName" name="claimerName" type="text" placeholder="Primary Tax Filer" maxlength="30" style="display:none">
													<div id="claimerName_error"></div>

													<!-- <div class="popover fade bottom in" style="display: none">
														<div class="arrow"></div>
														<div class="popover-content">We assume that you are
															the primary tax filer in your household. If not, and you
															wish to proceed on behalf of the primary tax filer,
															please enter that person's name. The primary tax filer is
															the claiming person listed on your federal tax return in
															2014.</div>
													</div> -->
													
													<!-- Zip Code -->
													<label>What is your zip code?</label> 
													<input id="zipCode" name="zipCode" type="text"	placeholder="Zip" class="input-mini numbersOnlyField" maxlength="5">
													<img id="zipLoading" src="<c:url value="/resources/img/ajax_loader_blue_128.gif"/>" style="width:20px; display:none">
													<select id="county" name="county" style="margin-top: 8px; display:none">
														<option value="" selected="selected">Select county</option>
													</select>
													<select id="stateCode" name="stateCode" style="margin-top: 8px; display:none"></select>
													<!-- <input id="stateCode" name="stateCode" type="hidden"> -->
													<!-- <input id="county" name="county" type="hidden"> -->
													 
													<div id="zipCode_error"></div>	 
													<div id="stateCode_error"></div>
													<div id="county_error" style="display: none;" class="error alert alert-success">
														<label style="display: inline;" for="county">Please select county</label>
													</div>
													
													<!-- Is Married -->
													<div class="floatLeft">
													<label class="checkbox"  style="margin-top:20px">
														<div class="checker" id="uniform-undefined">
															<span>
																<input id="isMarried" name="isMarried" type="checkbox" class="fancy married-yes">
															</span>
														</div> 
													</label>
													</div>
													<label style="margin-top:20px">
													
														<span
															data-html="true" data-content="If you are married and filing taxes jointly, 
															either spouse can be the primary tax filer. 
															If you are married but do not file taxes jointly, then you cannot receive advanced premium tax credits."
															data-placement="right" data-toggle="popover"
															class="haspopover">Married? <i class="icon-question-sign"></i>
														</span>
														
													</label>
													
													<div class="popover fade bottom in" style="display: none">
														<div class="arrow"></div>
														<div class="popover-content">If you are married and
															filing taxes jointly, either spouse can be the primary
															tax filer. If you are married but do not file taxes
															jointly, then you cannot receive advanced premium tax
															credits.</div>
													</div>
													
													<!-- No. of dependents -->
													<label  style="margin-top:20px">
														<span 
															data-html="true" data-content="A tax dependent is typically a child or relative who you financially support. 
															If you use a 1040 form to file taxes, your dependents are listed on line 6c."
															data-placement="right" data-toggle="popover"
															class="haspopover">How many dependents will you
																claim on your 2014 tax filing? Do not include your
																spouse.<i class="icon-question-sign"></i>
														</span>
													</label> 
													<input type="button" class = "btn" id="numberOfDependentsDecrement" value="-" title="-">
													<input id="numberOfDependents" name="numberOfDependents" value="0" type="text" maxlength="2" placeholder="0" 
														class="input-mini numbersOnlyField ta-center">
													<input type="button" class = "btn" id="numberOfDependentsIncrement" value="+" title="+" >
													<div id="numberOfDependents_error"></div>	

													<div class="popover fade bottom in" style="display: none">
														<div class="arrow"></div>
														<div class="popover-content">A tax dependent is
															typically a child or relative who you financially
															support. If you use a 1040 form to file taxes, your
															dependents are listed on line 6c.</div>
													</div>
													
													<!-- Tax Household Income -->
													<label  style="margin-top:20px">
														<span
															data-html="true" data-content="Please consider the incomes of both yourself and the members of your household. 
															The more common types of income that you should include here are job, 
															self-employment, rental, unemployment income and any alimony you receive."
															data-placement="right" data-toggle="popover"
															class="haspopover">What is your expected household income in 2014? <i class="icon-question-sign"></i>
														</span>
													</label>

													<div class="popover fade bottom in" style="display: none">
														<div class="arrow"></div>
														<div class="popover-content">Please consider the
															incomes of both yourself and the members of your
															household. The more common types of income that you
															should include here are job, self-employment, rental,
															unemployment income and any alimony you receive.</div>
													</div>
													
													<div class="row-fluid sliderwrap">
														<label  style="margin-top:5px"> 
															<input id="taxHouseholdIncome" name="taxHouseholdIncome" type="text" maxlength="9" class="input-mini numbersOnlyField">
														</label>
														
														<div class="slider-r">
															<div id="eq1">
																<div id="taxHouseholdIncomeSlider" class="slider-info"></div>
															</div>
															
														</div>&nbsp;
														
													</div>
													<div id="taxHouseholdIncome_error"></div>
													
												</div>

											</fieldset>
										</form>
									</div>
									<!-- widget-content -->
									
									<div class="widget-footer">
										<a id="baseincome-p" class="btn btn-large left" href="#item-0"
											data-slide="prev">Previous</a> <a id="baseincome"
											class="btn btn-info btn-large right" href="#item-2"
											data-slide="next" onclick="javascript:generateHouseholdSection();">Next</a>
									</div>
									
								</div>
							</div>
							
							<!-- My Profile Questions FAQ YOUR PROFILE-->	
							<div class="span4">
								<div class="well widget content-r faq2 box-shadow-right">
									<div class="widget-header">
										<h3 class="title">
											<i class="icon-comment"></i> Frequently Asked Questions
										</h3>
									</div>
									<%-- <form id="askProfileQuestions" name="askProfileQuestions" class="form-search">
										<input type="text" class="input-medium span9"
											placeholder="What if I'm on Medicare?">
										<button class="btn" type="submit">Ask!</button>
									</form> --%>

									<div class="tabbable tabs-custom">
										<!-- <ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#topq-1"><i
													class="icon-comments"></i>&nbsp;Top Quesions</a></li>
											<li><a data-toggle="tab" href="#helpo-1"><i
													class="icon-thumbs-up"></i>&nbsp; Help Others</a></li>

										</ul> -->

										<div class="">
											<div id="topq-1" class="tab-pane fade in active">
												<ul class="qlist">
												
													<li><a href="#" class="clickpop" data-html="true" data-content="Yes, but you will need to disclose your divorce to the Exchange when the divorce becomes final."
																	data-placement="top" data-toggle="popover"
																	data-container="body">
												Should I include my spouse if I plan to get divorced in 2014?</a></li>
												<li><a href="#" class="clickpop" data-html="true" data-content="Your son or daughter can be claimed as a dependent child or dependent relative. 
																In general, dependent children need to live with you for at least six months out of the year, whereas dependent relatives do not have this requirement."
																	data-placement="top" data-toggle="popover"
																	data-container="body">
												Can I include my son or daughter who does not live with me?</a></li>
												
													<!-- <li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'spouseDivorce' } )"
													class="highslide">Should I include my spouse if I plan to get divorced in 2014?</a></li> -->
													<!-- <li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'sonDaughter' } )"
													class="highslide">Can I include my son or daughter who does not live with me?</a></li> -->
												</ul>
											</div>
											
												<!-- highslide for spouseDivorce -->
												<div class="highslide-html-content" id="spouseDivorce">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>Should I include my spouse if I plan to get divorced in 2014?</h4>
																<p>Yes, but you will need to disclose your divorce to the Exchange upon the divorce becoming final.</p>													
																</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
												
												<!-- highslide for sonDaughter -->
												<div class="highslide-html-content" id="sonDaughter">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>Can I include my son or daughter who does not live with me?</h4>
																<p>Your son or daughter can be claimed as a dependent child or dependent relative. 
																Dependent children need to live with you for at least six months out of the year, whereas dependent relatives do not have this requirement.</p>													</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
						
										
										</div>
										<!-- end of faq -->
									</div>
								</div>
							</div>
							<!-- My Profile Questions end -->
						</div>
						<!-- #profile end -->

						<!-- #household -->
						<div id="item-2">
							<div class="span8">
								<div class="well widget content-l">
								<form id="householdForm" name="householdForm">
									<div class="widget-content">
										<fieldset>
											<legend>Tell us more
												about your household </legend>
										</fieldset>
										<input id="effectiveDate" name="effectiveDate" type="hidden" value="<c:out value='${effectiveDate}'/>">
										<p>Click on each member of your household and enter their date of birth.</p>
										<label id="householdError" class="error alert alert-error" style="display:none">At least one member should seek coverage.</label>
										<div class="householdBox">
										<!--accordion-->
										<div id="householdAccordion" class="accordion">
											<div class="accordion-group" id="selfAccordion">
												<div class="accordion-heading">
													<i class="icon-user-1"></i> 
													
													<a href="#collapseOne" id="selfName"
														data-parent="#householdAccordion" data-toggle="collapse"
														class="accordion-toggle collapsed"> <i
														class="icon-chevron-right"></i>&nbsp; Primary Tax Filer 
														
													</a>
													<div id="selfDob_error"></div>
													
												</div>
												<div class="accordion-body collapse in" id="collapseOne">
													<div class="accordion-inner">
														<div class="row-fluid">
															<div class="span4">
																<label>Date of Birth</label> <input maxlength="10" id="selfDob" name="selfDob" type="text"
																	placeholder="mm/dd/yyyy" class="input-small dob">
																	
															</div>

															<div class="span3">
																<label>Pregnant?</label> <label class="radio"
																	style="display: inline-block">
																	<div class="radio pregnant" id="">
																		<span><input type="radio" class="fancy"
																			value="true" name="isSelfPregnant"
																			></span>
																	</div> Yes
																</label> <label class="radio" style="display: inline-block">
																	<div class="radio pregnant" id="uniform-optionsRadios2">
																		<span class="checked"><input type="radio"
																			class="fancy pregnant-no" value="false"
																			name="isSelfPregnant"
																			checked="checked"></span>
																	</div> No
																</label>
															</div>
															<div class="span7">
																<label> <span
																	data-html="true" data-content="The health reform law requires all Americans to have health insurance coverage. 
																	However, some members of your household may already have coverage through 
																	government programs such as Medicaid, CHIP, Medicare or TRICARE. "
																	data-placement="right" data-toggle="popover"
																	data-container="body" class="haspopover">Seeking Insurance
																		Coverage? <i class="icon-question-sign"></i>
																</span>
																</label>
																<div class="popover fade bottom in"
																	style="display: none">
																	<div class="arrow"></div>
																	<div class="popover-content">The health reform
																		law requires all Americans to have health insurance
																		coverage. However, some members of your household may
																		already have coverage through government programs such
																		as Medicaid, Medicare or TRICARE.</div>
																</div>
																<label class="radio" style="display: inline-block">
																	<div class="radio">
																		<span id="selfSeekingYes" class="checked"><input type="radio"
																			class="fancy seeking-yes" value="true"
																			name="isSelfSeekingCoverage" checked="checked"
																			style="opacity: 0;"></span>
																	</div> Yes
																</label> <label class="radio" style="display: inline-block">
																	<div class="radio">
																		<span id="selfSeekingNo"><input type="radio" class="fancy seeking-no"
																			value="false" name="isSelfSeekingCoverage"
																			style="opacity: 0;"></span>
																	</div> No
																</label>
															</div>
															<div class="span3 medicareEligible" style="display:none">
																<label class="alert alert-success"> 
																<span
																	data-html="true" data-content="In general, individuals who are 65 or older cannot seek 
																	premium tax credits for private plans because they are likely eligible for Medicare, 
																	a federally administered program for the elderly."
																	data-placement="right" data-toggle="popover"
																	data-container="body" class="haspopover"> Medicare Eligible <i class="icon-white icon-question-sign"></i>
																</span> 
<!-- 																	<span>Medicare Eligible &nbsp;<i class="icon-ok icon-white"></i></span> -->
																</label>
															</div>	
														</div>
													</div>
												</div>
											</div>
											<div class="accordion-group" id="spouseAccordionDiv" style="display:none">
												<div class="accordion-heading">
													<a href="#collapseTwo" data-parent="#householdAccordion"
														data-toggle="collapse" class="accordion-toggle"> <i
														class="icon-chevron-right"></i>&nbsp; Spouse
													</a>
													<div id="spouseDob_error"></div>
												</div>
												<div class="accordion-body collapse" id="collapseTwo"
												>
													<div class="accordion-inner">
														<div class="row-fluid">
															<div class="span4">
																<label>Date of Birth</label> <input id="spouseDob" name="spouseDob" type="text"
																	placeholder="mm/dd/yyyy" class="input-small dob">
																	
															</div>
															<div class="span3">
																<label>Pregnant?</label> <label class="radio"
																	style="display: inline-block">
																	<div class="radio pregnant">
																		<span id="spouseSeekingYes"><input type="radio" class="fancy"
																			value="true" name="isSpousePregnant"
																			style="opacity: 0;"></span>
																	</div> Yes
																</label> <label class="radio" style="display: inline-block">
																	<div class="radio pregnant" id="uniform-optionsRadios1">
																		<span id="spouseSeekingNo" class=""><input type="radio"
																			class="fancy pregnant-no" value="false"
																			name="isSpousePregnant"
																			style="opacity: 0;"></span>
																	</div> No
																</label>
															</div>
															<div class="span7">
																<label> <span
																	data-html="true" data-content="The health reform law requires all Americans to have 
																	health insurance coverage. However, some members of your household may already have 
																	coverage through government programs such as Medicaid, CHIP, Medicare or TRICARE. "
																	data-placement="right" data-toggle="popover"
																	data-container="body" class="haspopover">Seeking Insurance
																		Coverage? <i class="icon-question-sign"></i>
																</span>
																</label>
																<div class="popover fade bottom in"
																	style="display: none">
																	<div class="arrow"></div>
																	<div class="popover-content">The health reform
																		law requires all Americans to have health insurance
																		coverage. However, some members of your household may
																		already have coverage through government programs such
																		as Medicaid, Medicare or TRICARE.</div>
																</div>
																<label class="radio" style="display: inline-block">
																	<div class="radio">
																		<span class="checked"><input type="radio"
																			class="fancy seeking-yes" value="true"
																			name="isSpouseSeekingCoverage" style="opacity: 0;" checked="checked"></span>
																	</div> Yes
																</label> <label class="radio" style="display: inline-block">
																	<div class="radio">
																		<span><input type="radio" class="fancy seeking-no"
																			value="false" name="isSpouseSeekingCoverage"
																			style="opacity: 0;"></span>
																	</div> No
																</label>
															</div>
															<div class="span3 medicareEligible" style="display:none">
																<label class="alert alert-success"> 
																<span
																	data-html="true" data-content="In general, individuals who are 65 or older cannot seek 
																	premium tax credits for private plans because they are likely eligible for Medicare, 
																	a federally administered program for the elderly."
																	data-placement="right" data-toggle="popover"
																	data-container="body" class="haspopover"> Medicare Eligible <i class="icon-white icon-question-sign"></i>
																</span> 
<!-- 																	<span>Medicare Eligible &nbsp;<i class="icon-ok icon-white"></i></span> -->
																</label>
															</div>	
														</div>
													</div>
												</div>
											</div>
											<!-- Dependent Accordions will be dynamically added here -->
											
										</div>
										<!--accordion-->
										<div>
										<p>Is anyone in your household disabled?</p>
										<label class="radio" style="display: inline-block">
											<div class="radio" id="uniform-optionsRadios1">
												<span><input type="radio" class="fancy"
													value="true" name="isAnyMemberDisabled"
													style="opacity: 0;"></span>
											</div> Yes
										</label> <label class="radio" style="display: inline-block">
											<div class="radio" id="uniform-optionsRadios1">
												<span><input type="radio" class="fancy"
													value="false" id="disabled2" name="isAnyMemberDisabled"
													style="opacity: 0;" checked="checked"></span>
											</div> No
										</label>
										</div>
										</div><!-- .householdBox -->
									</div>
									</form>
									<div class="widget-footer">
										<a id="household1-p" class="btn btn-large left" href="#item-1"
											data-slide="prev">Previous</a> <a id="household1"
											class="btn btn-info btn-large right" href="#item-3"
											data-slide="next" onclick="">Next</a>

									</div>
								</div>
							</div>
				
							<!-- Household questions FAQs-->
							<div class="span4">
								<div class="well widget box-shadow-right content-r uncleSamDiv">
									<div class="widget-header">
										<h3 class="title uncleSam">
											In 2014, Uncle Sam does not expect you to pay more than <span
												class="uncleSamPercentage">X</span>% of your household
											income on premiums.
										</h3>
									</div>
								</div>
								<div class="well widget content-r  box-shadow-right questionDiv">
								<div class="widget-header">
										<h3 class="title">
											<i class="icon-comment"></i> Frequently Asked Questions
										</h3>
									</div>
								

								<div class="tabbable tabs-custom">
											<div id="topq-1" class="tab-pane fade in active">
												<ul class="qlist">
													<!-- <li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'govSponsored' } )"
													class="highslide">What if I am eligible for or already have government-sponsored coverage?</a></li> -->
													
													<li><a href="#" class="clickpop" data-html="true" data-content="Individuals who have access to government-sponsored coverage 
																cannot receive tax credits for 2014. However, they can still shop unsubsidized private plans that 
																will have improved coverage and benefits. Examples of 
																government-sponsored coverage include TRICARE, Medicaid, Medicare, VA and CHIP."
																	data-placement="top" data-toggle="popover"
																	data-container="body">
															What if I am eligible for or already have government-sponsored coverage?</a></li>
															
															<li><a href="#" class="clickpop" data-html="true" data-content="Individuals who are Native American or Alaska Native 
															may be entitled to special cost-sharing subsidies. However, there may be a verification process. This version of the estimator does not determine eligibility for such subsidies."
																	data-placement="top" data-toggle="popover"
																	data-container="body">
															What if someone in my household is Native American or Alaska Native?</a></li>
													
													<!-- <li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'native' } )"
													class="highslide">What if someone in my household is Native American or Alaska Native?</a></li> -->
<!-- 													<li class="ta-right"><a href="#"><small>
																View all</small></a></li> -->
												</ul>
											</div>
											
												<!-- highslide for govSponsored -->
												<div class="highslide-html-content" id="govSponsored">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>What if I am eligible for or already have government-sponsored coverage?</h4>
																<p>Unfortunately, individuals who have access to government-sponsored coverage 
																cannot receive tax credits for 2014. However, they can still shop unsubsidized private plans that 
																will have improved coverage and benefits. Examples of 
																government-sponsored coverage include TRICARE, Medicaid, Medicare, VA and CHIP.</p>													
																</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
												
												<!-- highslide for native -->
												<div class="highslide-html-content" id="native">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>What if someone in my household is Native American or Alaska Native?</h4>
																<p>Individuals who are Native American or Alaska Native are entitled to special cost-sharing benefits. However, 
																there may be a verification process. This version of the estimator does not determine 
																Native American cost sharing subsidies.</p>													
																</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
						
										
										</div>
										<!-- end of faq -->
										</div>					
							</div>
						</div>
						<!-- #household end -->
						
						<!-- #Income1 -->
						<div id="item-3">
							<div class="span8">
								<!-- widget -->
								<div class="well widget content-l">
									<!-- widget content -->
									<div class="widget-content">
										<form id="incomeForm" name="incomeForm">
											<fieldset>
												<legend>
													Your expected household income in 2014:<span class="right"
														id="editincome"> $&nbsp;
														<input id="refineincome" name="refineincome" size="10" maxlength="13" value="0" class="input-medium numbersOnlyField" type="text">
														<!-- &nbsp;<i class=" icon-edit" style="color: #999"></i> --></span>
														<div id="refineincome_error"></div>
												</legend>
												<p class="gutter10">For the most accurate calculation of your tax credit, please add all other income, such as social security benefits, rental and unemployment income. 
												If you have no other income to modify, just click next.</p>

												<div class="" id="calculateincome">
													<div class="incomeBox" id="calculateincomeBox">
														<!-- Incomes of all household member will be added here -->
													</div>
														<!-- Deductions -->
											<div class="deductionBox">
												<h3>Any Deductions?</h3>
												<div class="row-fluid">
													<div class="span6">
														<label>
															<span data-html="true" data-content="Alimony includes spousal support from a divorce but does not include child support." data-placement="right" data-toggle="popover" class="haspopover" href="#">Alimony Paid  <i class="icon-question-sign"></i></span>
														</label>
													</div><!-- .span2 -->
													<div class="popover fade top in" style="display:none">
														<div class="arrow"></div>
														<div class="popover-content">Alimony includes spousal support from a divorce but does not include child support.</div>
													</div><!-- .popover fade top in -->
												</div>
												<div class="row-fluid">
													<div class="span4">
														<input type="text" id="alimonyPaid" class="input-small numbersOnlyField" maxlength="9">
													</div><!-- .span2 -->
													
													<div class="span6 offset1">
															<div id="eq1">
																<div id="alimonyPaidSlider" class="slider-info" rel="0"></div>
															</div><!-- eq1 -->
													</div><!-- .span5 -->
												<div id="alimonyPaid_error"></div>													
												</div><!-- .row-fluid -->

												<div class="row-fluid">
													<div class="span6">
														<label>
															<span data-html="true" data-content="Generally, the amount you can deduct in 2014 is $2,500 or the amount of student loan interest you actually paid, whichever is less." data-placement="right" data-toggle="popover" class="haspopover" href="#">Student Loan Interest <i class="icon-question-sign"></i></span>
														</label>
													</div><!-- .span2 -->
													
													<div class="popover fade top in" style="display:none">
														<div class="arrow">
														</div>
														<div class="popover-content">Generally, the amount you can deduct in 2014 is $2,500 or the amount of student loan interest you actually paid, whichever is less.
														</div>
													</div><!-- .popover fade top in -->
												</div>
												<div class="row-fluid">
													<div class="span4">
														<input type="text" id="studentLoans" class="input-small numbersOnlyField" maxlength="9">
													</div><!-- .span2 -->
													
													<div class="span6 offset1">
															<div id="eq1">
																<div id="studentLoansSlider" class="slider-info" rel="0"></div>
															</div>
													</div><!-- .span5 -->
												<div id="studentLoans_error"></div>	
												</div><!-- .row-fluid -->
												
											</div><!-- .deductionBox -->
												</div>
											</fieldset>
											<!-- All the refine income pop ups will be added here -->
											<div id="incomePopUps" ></div>
										</form>
									</div>
									<!-- ./ widget content -->
									<div class="widget-footer">
										<a id="income2-p" class="btn btn-large left" href="#item-2"
											data-slide="prev">Previous</a> <a id="incomeCalculator"
											class="btn btn-info btn-large right" href="#item-5"
											data-slide="next">Next</a>
									</div>
								</div>
							</div>
							<!--span8-->
							<!-- FAQ for Income-->
							<div class="span4">
								<div class="well widget box-shadow-right content-r uncleSamDiv" id="incomeUncleSam">
									<div class="widget-header">
										<h3 class="title uncleSam">
											In 2014, Uncle Sam does not expect you to pay more than <span
												class="uncleSamPercentage">X</span>% of your household
											income on premiums.
										</h3>
									</div>
								</div>

								<div class="well widget content-r box-shadow-right questionDiv">
									<div class="widget-header">
										<h3 class="title">
											<i class="icon-comment"></i> Frequently Asked Questions
										</h3>
									</div>
									

									<div class="tabbable tabs-custom">
										<!-- <ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#topq-1"><i
													class="icon-comments"></i>&nbsp;Top Quesions</a></li>
											<li><a data-toggle="tab" href="#helpo-1"><i
													class="icon-thumbs-up"></i>&nbsp; Help Others</a></li>

										</ul> -->

										<div class="">
											<div id="topq-1" class="tab-pane fade in active">
												<ul class="qlist">
													<!-- <li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'sharedReductions' } )" class="highslide">
													How are the premium tax credits and cost sharing reductions calculated?</a></li> -->
													
													<li><a href="#" class="clickpop" data-html="true" data-content="Both are calculated on the basis of your household's 2014 income as a percentage of 
																2013 federal poverty level &#40;FPL&#41;. Household incomes up to 400&#37; of FPL may be eligible for premium 
																tax credits, and households income up to 250&#37; of FPL are eligible for cost sharing reductions. The 2013 FPL
																 is &#36;11,490 for an individual and &#36;23,550 for a family of four."
																	data-placement="top" data-toggle="popover"
																	data-container="body">
												How are the premium tax credits and cost sharing reductions calculated?</a></li>
												
												<li><a href="#" class="clickpop" data-html="true" data-content="In general, individuals who are offered affordable healthcare coverage from their employer cannot 
																receive tax credits. Employer coverage is 'affordable' if the plan has minimum value and the employee's share of 
																premiums is less than 9.5&#37; of the employee's household income."
																	data-placement="top" data-toggle="popover"
																	data-container="body">
												If I get coverage from my employer, can I receive premium tax credits?</a></li>
												
													<!-- <li><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'empTaxCredit' } )" class="highslide">
													If I get coverage from my employer, can I receive premium tax credits?</a></li> -->
<!-- 													<li class="ta-right"><a href="#"><small>
																View all</small></a></li> -->
												</ul>
											</div>
											
												<!-- highslide for sharedReductions -->
												<div class="highslide-html-content" id="sharedReductions">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>How are the premium tax credits and cost sharing reductions calculated?</h4>
																<p>Both are calculated on the basis of your family's 2014 income as a percentage of 
																2013 federal poverty level &#40;FPL&#41;. Households up to 400&#37; of FPL may be eligible for premium 
																tax credits, and households up to 250&#37; of FPL are eligible for cost sharing reductions. The 2013 federal poverty 
																level is &#36;11,490 for an individual and &#36;23,550 for a family of four.</p>													
																</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
												
												<!-- highslide for premiumTax -->
												<div class="highslide-html-content" id="empTaxCredit">
													<div class="highslide-header">
														<ul>
															<li class="highslide-move">
																<a href="#" onclick="return false">Move</a>
															</li>
															<li class="highslide-close">
																<a href="#" onclick="return hs.close(this)">Close</a>
															</li>
														</ul>
													</div>
													<div class="highslide-body">
														<h4>If I get coverage from my employer, can I receive premium tax credits?</h4>
																<p>In general, individuals who are offered affordable healthcare coverage from their employer cannot 
																receive tax credits. Employer coverage is "affordable" if the plan has minimum value and the employee's share of 
																premiums is less than 9.5&#37; of the employee's household income. </p>													
																</div>
												    <div class="highslide-footer">
												        <div>
												            <span class="highslide-resize" title="Resize">
												                <span></span>
												            </span>
												        </div>
												    </div>
												</div>
						
										
										</div>
										<!-- end of faq -->
									</div>
								</div>
							</div>
						</div>
						<!-- #Income1 end -->

						<!-- #Results-->
						<div id="item-5">
							<div class="span8">
								<div class="well widget content-l">
									<div class="widget-content">
									
									<fieldset class="no-margin-b" id="case1">
										<legend>Subsidy Likely!</legend>
									</fieldset>
									<fieldset class="no-margin-b" id="case3">
										<legend>Subsidy Unlikely</legend>
									</fieldset>
									<fieldset class="no-margin-b" id="case4">
										<legend>Medicare Eligible</legend>
									</fieldset>
									<fieldset class="no-margin-b" id="case5">
										<legend>You are Likely Eligible for Medicaid</legend>
									</fieldset>
									<fieldset class="no-margin-b" id="case6">
										<legend>Subsidy Likely!</legend>
									</fieldset>
									
									<div class="message-1" id="resultsTabContent" style="display:none;overflow-y:scroll;max-height: 400px ">
									
										<!-- APTC/CSR Top lines STARTS -->
										
										<p id="aptcLine" style=""><b>Save $<span
														id="aptcCase3Results"></span> per month on premiums!
												</b> Because of a tax discount there are Y plans available to
													you for $0 and Z plans under $100 per month.</p>
										<p id="csrLine" style=""><b>Plan Upgrade!</b> Because
													of cost sharing reductions, you are eligible for a plan
													upgrade, meaning you will pay less when you get care. The
													average household will pay X dollars, whereas you will
													never pay more than X dollars in 2014.</p>
											<!-- <p>
												<small><em> *Disclaimer The premium and
															subsidy amounts displayed are estimates only and the
															actual amounts of subsidy eligibility may differ. This
															Getinsured Eligibility Estimator is not intended to be
															relied upon for legal or tax advice. </em></small>
											</p> -->
										
										<!-- APTC/CSR Top lines ENDS -->

										<!-- Not APTC/CSR Eligible (>420% of FPL) STARTS -->
										<div class="results" id="case3Results">
											<p>You are unlikely to be eligible for financial assistance based on your income. You may continue to view
											unsubsidized plans or you may go through an advanced eligibility screener on the state exchange. Unsubsidized plans
											include all the plans that are offered on the state health exchange as well as additional plans offered by 
											leading insurance companies.</p> 
											<!-- <p>
												<small><em> *Disclaimer The premium and
															subsidy amounts displayed are estimates only and the
															actual amounts of subsidy eligibility may differ. This
															Getinsured Eligibility Estimator is not intended to be
															relied upon for legal or tax advice. </em></small>
											</p> -->
										</div>
										<!-- Not APTC/CSR Eligible (>420% of FPL) ENDS -->


										<!-- When all applicants in a household are 65 or over STARTS -->
										<div class="results" id="medicareResults">
											<p> Individuals who are 65 or older cannot seek tax credits on private plans because they are eligible for Medicare, 
											a federally administered progream for the elderly. 
											However, you can still shop for unsubsidized health plans from the industry's leading insurance carriers. 
											<a href="http://www.healthcare.gov/using-insurance/medicare-long-term-care/medicare/" target="_blank">Learn more about Medicare</a></p>				
											<!-- <p>
												<small><em> *Disclaimer The premium and
															subsidy amounts displayed are estimates only and the
															actual amounts of subsidy eligibility may differ. This
															Getinsured Eligibility Estimator is not intended to be
															relied upon for legal or tax advice. </em></small>
											</p> -->
										</div>	
										<!-- When all applicants in a household are 65 or over ENDS -->
										
										
										<!-- When FPL is less than medicaidFplValue% STARTS-->
										<div class="results" id="medicaidResults">
										<p> Medicaid is a state administered health program for low-income adults, their children and people with certain disabilities.
											Unfortunately, individuals who are eligible for Medicaid cannot recieve premium tax credits for private insurance.  
											
											<a href="http://www.healthcare.gov/using-insurance/low-cost-care/medicaid/" target="_blank">Learn more about Medicaid</a><br><br>
											
											If you prefer private insurance, catastrophic plans are available to you from industry leading carriers that
											satisfy your legal requirement to have insurance. Unsubsidized plans are also be available to you.
											
											</p>
											<!-- <p>
												<small><em> *Disclaimer The premium and
															subsidy amounts displayed are estimates only and the
															actual amounts of subsidy eligibility may differ. This
															Getinsured Eligibility Estimator is not intended to be
															relied upon for legal or tax advice. </em></small>
											</p> -->
											<a class="btn btn-large btn-info" href="${pageContext.request.contextPath}/eligibility/confirmMedicaid">Confirm Your Medicaid Eligibility</a>				
										</div>	
										<!-- When FPL is less than medicaidFplValue% ENDS-->
										
											<!-- Subsidized Plan result STARTS -->
											<div id="subsidizedPLanResults" class="alert alert-info" style="text-align:left">
											
												<div class="row-fluid line">
													<div class="span8">
														<h3 style="margin-top: 0">How do you want to shop for
															your healthcare plans?</h3>
														<p>
															<strong>Shop for Subsidized Plan <span
																class="color">(Recommended)</span></strong>
														</p>
													</div>
												
	
													<div class="span4 ta-right">
														<form id="shopPlan" name="shopPlan" method="POST" 
															action="${pageContext.request.contextPath}/plandisplay/saveIndividualPHIX">
														<input id="eligibilityData" name="eligibilityData" type="hidden">
															<a class="btn btn-large btn-info"
																href="#" 
																onclick="javascript:createPlanDisplayrequest('YES');document.getElementById('shopPlan').submit();">
																Shop for a Subsidized Plan <i class="icon-external-link"></i>
															</a>
														</form>	
													</div>
													
												</div>	
												
												<div class="row-fluid">
													<div class="span6">
														<p>
															<strong>Waive your Tax Credit</strong>
														</p>
														<ul>
															<li>Enables you to select from additional plans.</li>
														</ul>
													</div>
													<!-- .span6 -->
													<div class="span4 offset2">
														<a class="" href="subsidyWaiver">
															Waive Credit <i class="icon-external-link"></i>
														</a>
													</div>
												</div>
											</div>
											<!-- Subsidized Plan result ENDS -->
											
											<!-- UnSubsidized Plan result STARTS -->
											<div id="unsubsidizedPLanResults" class="alert alert-info" style="text-align:left">
											
												<div class="row-fluid line">
													<div class="span8">
														<h3 style="margin-top: 0">How do you want to shop for
															your healthcare plans?</h3>
														<p>
															<strong>Shop for Unsubsidized Plan <span
																class="color">(Recommended)</span></strong>
														</p>
													</div>
												
	
													<div class="span4 ta-right">
														<form id="shopPlan" name="shopPlan" method="POST" 
															action="${pageContext.request.contextPath}/plandisplay/saveIndividualPHIX">
														<input id="eligibilityData" name="eligibilityData" type="hidden">
															<a class="btn btn-large btn-info"
																href="#" 
																onclick="javascript:createPlanDisplayrequest('NO');document.getElementById('shopPlan').submit();">
																Shop for an Unsubsidized Plan <i class="icon-external-link"></i>
															</a>
														</form>	
													</div>
													
												</div>	
												
												<div class="row-fluid">
													<div class="span6">
														<p>
															<strong>Verify your eligibility anyways</strong>
														</p>
													<!--<ul>
															<li>Enables you to select from additional plans.</li>
														</ul>-->
													</div>
													<!-- .span6 -->
													<div class="span4 offset2">
														<a class="" href="#">
															Verify! <i class="icon-external-link"></i>
														</a>
													</div>
												</div>
											</div>
											<!-- UnSubsidized Plan result ENDS -->
											
											<!-- <ul class="gutter10">
												<li id="aptcLine" style=""><b>Save $<span
														id="aptcCase3Results"></span> per month on premiums!
												</b> Because of a tax discount there are Y plans available to
													you for $0 and Z plans under $100 per month.</li>
												<li id="csrLine" style=""><b>Plan Upgrade !</b> Because
													of cost sharing reductions, you are eligible for a plan
													upgrade, meaning you will pay less when you get care. The
													average household will pay X dollars, whereas you will
													never pay more than X dollars in 2014.</li>
											</ul> -->
											<br>
											
											<!-- APTC Eligible STARTS -->
											<div class="results" id="case1Results">
												<!-- <p> Based on your Pre-Eligibility screening, you are likely to be eligible for a subsidy, you may continue
												to an advanced eligibility screener on the Federal Exchange or you may view unsubsidized plans. Unsubsidized plans
												include all the plans that are offered on the state health exchange as well as additional plans offered by 
												leading insurance companies.</p> -->
											</div>
											<!-- APTC Eligible ENDS -->
											
											<!-- Not APTC Eligible, but CSR Eligible (APTC equation returns negative or 0, but medicaidFplValue-250% of FPL) starts -->
											<div class="results" id="case6Results">
												<!-- <p>Based on your Pre-Eligibility screening, you are likely to be eligible for a subsidy, you may continue
												to an advanced eligibility screener on the Federal Exchange or you may view unsubsidized plans. Unsubsidized plans
												include all the plans that are offered on the state health exchange as well as additional plans offered by 
												leading insurance companies.</p> -->
											</div>
											<!-- Not APTC Eligible, but CSR Eligible (APTC equation returns negative or 0, but medicaidFplValue-250% of FPL) ends -->
											
											<!-- When one but not all applicants are above 65 starts -->
											<div class="results" id="partialMedicareResults">
												<p><b>Medicare Eligible. </b>Someone in your household is 65 or over. 
												This makes them eligible for Medicare, a federally administered program for the elderly. 
												Individuals eligible for Medicare cannot seek tax credits for private insurance. 
												<a href="http://www.healthcare.gov/using-insurance/medicare-long-term-care/medicare/" target="_blank">About Medicare</a></p>
												<!-- <p>
												<small><em> *Disclaimer The premium and
															subsidy amounts displayed are estimates only and the
															actual amounts of subsidy eligibility may differ. This
															Getinsured Eligibility Estimator is not intended to be
															relied upon for legal or tax advice. </em></small>
											</p> -->
											</div>
											<!-- When one but not all applicants are above 65 starts ENDS -->
											
											<div class="gutter10" style="display:none">
												<div class="alert alert-info row-fluid">
													<div class="span6 border-r">
														<p id="emailTextOne" style="display:none">Would you like to be notified by email when your tax credit
															is available?</p>
														<p id="emailTextTwo" style="display:none">Would you like us to email you regarding important health reform developments?</p>	
														<form id="emailForm" name="emailForm" class="form-horizontal" action="#">
															<input id="email" name="email" class="input-medium emailField" type="text" placeholder="Your Email">
															<input id="resultsType" name="resultsType" type="hidden">
															<input id="submitEmail" name="submitEmail" type="button" value="Submit" title="Submit" class="btn btn-large btn-success margin-l" onclick="javacript:saveEmail();">
															<div id="emailError" style="display:none" class="alert alert-error">Enter a valid email address</div>
														</form>
													</div>
													
													<div class="span6">
														<p>
															Can't wait until October? Call us at <span
																class="text-error">866-602-8466</span> and one of our
															customer care representatives can discuss your options
															today.
														</p>
													</div>
												</div>
												<!-- <p>
													<small><em> *Disclaimer The premium and
																subsidy amounts displayed are estimates only and the
																actual amounts of subsidy eligibility may differ. This
																Getinsured Eligibility Estimator is not intended to be
																relied upon for legal or tax advice. </em></small>
												</p> -->
											</div>
											
											<div class="row-fluid" style="display:none">
												<h5>Here are some other important things to know about
													the Affordable Care Act</h5>
											</div>
											
											<div class="row-fluid" style="display:none">
												
												<div class="infoBox withIcon no-margin-l span3">
													<span
														data-html="true" data-content="All health insurance plans (except most sold before March 10, 2010) 
														must cover you even if you have a pre-existing health condition, such as asthma or diabetes. 
														You cannot be declined." data-container="body"
														data-placement="right" data-toggle="popover"
														class="haspopover"><p>
															Guaranteed <br /> Coverage <i class="icon-question-sign"></i>
														</p></span>
												</div>
												<div class="infoBox withIcon span3">
													<span
														data-html="true" data-content="Insurance companies are no longer permitted to set a 
														maximum dollar amount they pay for benefits during your lifetime. 
														This means that you get unlimited coverage while your policy is active." data-container="body"
														data-placement="right" data-toggle="popover"
														class="haspopover"><p>
															No Limits <br /> on Coverage <i
																class="icon-question-sign"></i>
														</p></span>
												</div>
												<div class="infoBox withIcon span3">

													<span
														data-html="true" data-content="Starting January 1, 2014, most adults over the age of 18 will be required 
														to carry insurance or pay a penalty. The penalty will be 1% of your annual income or $95, whichever is greater."
														data-placement="right" data-toggle="popover"
														class="haspopover"><p>
															Penalties for <br /> Being Uninsured <i
																class="icon-question-sign"></i>
														</p></span>
												</div>
												<div class="infoBox withIcon no-margin-r span3">

<!-- 													<span -->
<!-- 														data-html="true" data-content="Health insurance will become more predictable because all health plans must now cover certain services, known as Essential Health Benefits. These benefits fit into the following 10 categories: Ambulatory patient services, Emergency services, Hospitalization, Maternity and newborn care, Mental health and substance use disorder services, Prescription drugs, Rehabilitative and habilitative services and devices, Laboratory services, Preventive and wellness services and chronic disease management, Pediatric services, including dental and vision care" -->
<!-- 														data-placement="right" data-toggle="popover" -->
<!-- 														class="haspopover"><p> -->
<!-- 															Standartization <br /> of Benefits <i -->
<!-- 																class="icon-question-sign"></i> -->
<!-- 														</p></span> -->

												<span data-html="true" data-content="
														<p>Health insurance will become more predictable because all health plans must now cover certain services,
														known as Essential Health Benefits. These benefits fit into the following 10 categories:</p>
														<ul>
														<li>Ambulatory patient services </li>
														<li>Emergency services, </li>
														<li>Hospitalization, </li>
														<li>Maternity and newborn care, </li>
														<li>Mental health and substance use disorder services, </li>
														<li>Prescription drugs, </li>
														<li>Rehabilitative and habilitative services and devices, </li>
														<li>Laboratory services, </li>
														<li>Preventive and wellness services and chronic disease management, </li>
														<li>Pediatric services, including dental and vision care</li>
														</ul>
														" data-container="body" data-placement="right" data-toggle="popover" class="haspopover preT">
														<p>Standardization <br/> of Benefits <i class="icon-question-sign"></i></p>
												</span>

												</div>
											</div>
											<div class="row-fluid">
												<p class="margin-t"><small><em>
												*Disclaimer: The premium and subsidy amounts displayed are estimates only 
												and the actual amounts of subsidy eligibility may differ. This Getinsured Eligibility 
												Estimator is not intended to be relied upon for legal or tax advice.
												</em></small></p>
											</div>
										</div>
									</div>
									<div class="widget-footer">
										<a id="startover-p" class="btn btn-large left" href="#item-3"
											data-slide="prev">Previous</a> <a id="startover"
											class="btn btn-default btn-large right" href="#"
											>Start Over </a>
									</div>
								</div>
							</div>
							
						</div>
						<div id="errorDialog" title="" style="diplay:none"></div>
						<div id="resultsDialog" title="Calculating your results" style="diplay:none" class="ta-center">
							<img src="<c:url value="/resources/img/loader_greendots.gif"/>">
						</div>
						<!-- #Results -->
			</div>	
		</div>
	</div>
</div>
		<!-- sidebar -->
		<ul id="sidebar" class="nav nav-pills nav-stacked">
			<li class="active" id="sidenav0"><a class="sidenav"
				href="#item-0"> <i class="micon-home"></i> <span
					class="hidden-phone">Home</span>
			</a></li>
			<li id="sidenav1"><a class="sidenav" href="#item-1"> <i
					class="micon-location"></i> <span class="hidden-phone">Your
						Profile</span>
			</a></li>
			<li id="sidenav2"><a class="sidenav" href="#item-2"> <i
					class="micon-user-2"></i> <span class="hidden-phone">Household</span>
			</a></li>
			<li id="sidenav3"><a class="sidenav" href="#item-3"> <i
					class="micon-stats-up"></i> <span class="hidden-phone">Income</span>
			</a></li>
			<li id="sidenav5"><a class="sidenav" href="#item-5"> <i
					class="micon-star-3"></i> <span class="hidden-phone">Results</span>
			</a></li>
		</ul>
		<!-- end sidebar -->
	</div>
</body>

<!-- jQuery validation plugin -->
<script type="text/javascript">
	
	//Validation method for letters only validation
	$.validator.addMethod("lettersOnly", function(value, element) {
		return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
	}, "Field must contain only letters");
	
	//Validation method for income section
	$.validator.addMethod("incomeValidator", function(value, element) {
		
		if(!value || value == '$'){
			return true;
		}
		
		if(value.charAt(0) == '$'){
			
			var amount = value.substring(1);
			
			if(isNaN(parseFloat(amount,10))){
				//console.log(parseFloat(amount,10));
				return false;
			}
			
			return true;
		}
		return this.optional(element) || /^\d{1,9}(\.\d{1,2})?$/.test($.trim(value));
		
	},"Income amount should be a number");
	
	$.validator.addMethod("refineIncomeValidator", function(value, element) {
		
		var amount = value.replace(/,/g ,'');
			
		if(isNaN(parseFloat(amount,10))){
			return false;
		}
			
		return true;
		
	},"Income amount should be a number");
	
	//Validator metod for DOB
	$.validator.addMethod("dob", function(value, element) {
		
		var today = new Date();
		dob_arr = value.split('/');

		dob_mm = dob_arr[0];
		dob_dd = dob_arr[1];
		dob_yy = dob_arr[2];

		var birthDate = new Date();
		birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

		if ((today.getFullYear() - 100) > birthDate.getFullYear()) {return false;}

		if ((dob_dd != birthDate.getDate())
					|| (dob_mm - 1 != birthDate.getMonth())
					|| (dob_yy != birthDate.getFullYear())) {
				return false;
		}
		
		//medicareCheck(age, element);
		
		if (today.getTime() < birthDate.getTime()) {return false;}

		return true;

	}, "<span>Enter valid date of birth in MM/DD/YYYY format.</span>");
	
	//Validation method for checking age is less than 19 or more than 64
	$.validator.addMethod("ageLess19", function(value, element) {
		
		var today = new Date();
		
		if($('#effectiveDate').val()){
			var effective = $('#effectiveDate').val().split('/');
			today.setFullYear(effective[2], effective[0] - 1, effective[1]);
		}
		
		dob_arr = value.split('/');

		dob_mm = dob_arr[0];
		dob_dd = dob_arr[1];
		dob_yy = dob_arr[2];

		var birthDate = new Date();
		birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

		if ((today.getFullYear() - 100) > birthDate.getFullYear()) {return false;}

		if ((dob_dd != birthDate.getDate())
					|| (dob_mm - 1 != birthDate.getMonth())
					|| (dob_yy != birthDate.getFullYear())) {
				return false;
		}
		
		var age = Math.ceil(today.getTime() - birthDate.getTime()) / (1000 * 60 * 60 * 24 * 365);
		if(isNaN(age) || (age < 19 && age < 65)){
			return false;
		}
		
		return true;
	},  "Age should be more than 19");
	
	//Validation method for checking age is less than 26
	$.validator.addMethod("ageMore26", function(value, element) {
		
		var elementId = $(element).attr('id');
		$('#' + elementId + '_message').hide();
		
		var today = new Date();
		
		if($('#effectiveDate').val()){
			var effective = $('#effectiveDate').val().split('/');
			today.setFullYear(effective[2], effective[0] - 1, effective[1]);
		}
		
		dob_arr = value.split('/');

		dob_mm = dob_arr[0];
		dob_dd = dob_arr[1];
		dob_yy = dob_arr[2];

		var birthDate = new Date();
		birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

		if ((today.getFullYear() - 100) > birthDate.getFullYear()) {return false;}

		if ((dob_dd != birthDate.getDate())
					|| (dob_mm - 1 != birthDate.getMonth())
					|| (dob_yy != birthDate.getFullYear())) {
				return false;
		}
		
		var age = Math.ceil(today.getTime() - birthDate.getTime()) / (1000 * 60 * 60 * 24 * 365);
		if(isNaN(age) || (age > 26 && age < 65)){
			
			//Disable both radio buttons
			$(element).closest('.accordion-body').find('.seeking-yes').attr('disabled','disabled');
			$(element).closest('.accordion-body').find('.seeking-no').attr('disabled','disabled');
			
			//Un-check the Yes radio button
			$(element).closest('.accordion-body').find('.seeking-yes').removeAttr('checked');
			$(element).closest('.accordion-body').find('.seeking-yes').closest('span').removeClass("checked");
			
			//Check the No radio button
			$(element).closest('.accordion-body').find('.seeking-no').closest('span').addClass("checked");
			$(element).closest('.accordion-body').find('.seeking-no').trigger('change');
			$(element).closest('.accordion-body').find('.seeking-no').attr('checked','checked');
			
			$('#' + elementId + '_message').html('<span class="alert alert-error">Dependents must be under 26 for this version of the estimator</span>');
			$('#' + elementId + '_message').show();
		}
		else{
			//Enable both radio buttons
			$(element).closest('.accordion-body').find('.seeking-yes').removeAttr('disabled');
			$(element).closest('.accordion-body').find('.seeking-no').removeAttr('disabled');
			
			$('#' + elementId + '_message').hide();
			//medicareCheck(age, element);
		}
		
		return true;
	},  "Age should be less than 26");
	
		
	var validZip = false;
	var previousZip = '';
	var zipAjaxCallCompleted = false;
	
	//Function to validate zip code using smarty street
	function validateZip() {
		
		var zipcodeinp = $('#zipCode').val();
		var pathURL = $('#prescreenVersionName').val() + "/validateZip";
		
		if(zipcodeinp && zipcodeinp != previousZip){
			
			previousZip = zipcodeinp;
			
			$("#stateCode").val('');
			$("#county").val('');
			$('#county').hide();
			$('#county_error').hide();
			
			$('#zipLoading').show();
			$('#baseincome').attr('href','#');
			
			zipAjaxCallCompleted = false;
			
			$.post(pathURL, {
				zip : zipcodeinp,
			}, function(data) {
				
				if(data){
					
					$('#county').html('<option value="" selected="selected">Select county</option>');
					$('#stateCode').html('<option value="" selected="selected"></option>');
					
					var county = "";
					var state = "";
					$.each(data, function(index, value) { 	
						county=index;
						state=value;
						$("#stateCode").append('<option value="'+value+'">'+value+'</option>');
						$('#county').append('<option value="'+index+'">'+index+'</option>');
					});
					
					if($('#county option').length > 2){
						$('#county').show();
						$('#stateCode').val('');
					}
					else{
						$('#county').val(county);
						$('#stateCode').val(state);
					}
					
					
					$('#zipCode_error').hide();
					validZip = true;
				}
				else{
					$('#county').html('<option value="" selected="selected">Select county</option>');
					$('#stateCode').html('<option value="" selected="selected"></option>');
					$('#stateCode').removeAttr('value');
					$('#county').removeAttr('value');
					$('#county').hide();
					validZip = false;
				}
				
				$('#zipLoading').hide();
				$('#baseincome').attr('href','#item-2');
				
				zipAjaxCallCompleted = true;
				
			}).fail(function() {
				$('#zipLoading').hide();
				$('#baseincome').attr('href','#item-2');
				$('#county').hide();
				zipAjaxCallCompleted = true;
				validZip = false;
			});
		}
		
		
		return validZip;
	}
	
 	function validateCounty() {
		
		var county = $('#county').val();
		var state =  $('#stateCode').val();
		validateZip();
		
		if(!zipAjaxCallCompleted){
			$('#county_error').hide();
			return true;
		}
		
		if(county && state){
			$('#county_error').hide();
			return true;
		}
		
		$('#county_error').css('display','inline');
		return false;
		
	}
	
	function medicareCheck(age, element){
			
			if(age>=65){
				
				//Disable both radio buttons
				$(element).closest('.accordion-body').find('.seeking-yes').attr('disabled','disabled');
				$(element).closest('.accordion-body').find('.seeking-no').attr('disabled','disabled');
				
				//Un-check the Yes radio button
				$(element).closest('.accordion-body').find('.seeking-yes').removeAttr('checked');
				$(element).closest('.accordion-body').find('.seeking-yes').closest('span').removeClass("checked");
				
				//Check the No radio button
				$(element).closest('.accordion-body').find('.seeking-no').closest('span').addClass("checked");
				$(element).closest('.accordion-body').find('.seeking-no').trigger('change');
				$(element).closest('.accordion-body').find('.seeking-no').attr('checked','checked');
				
				//Show medicare eligible sign
				$(element).closest('.accordion-body').find('.medicareEligible').css("display", "inline");
				$('#medicaidMessageNearCounter').hide();
			}
			else if(age<65){
				
				//Skip this validation for Dependents above 26
				if($(element).attr('id').indexOf('dependent') !== -1 && age > 26){
					return ;
				}
				
				//Enable both radio buttons
				$(element).closest('.accordion-body').find('.seeking-yes').removeAttr('disabled');
				$(element).closest('.accordion-body').find('.seeking-no').removeAttr('disabled');
				
				//Un-check the no button
				$(element).closest('.accordion-body').find('.seeking-no').removeAttr('checked');
				$(element).closest('.accordion-body').find('.seeking-no').closest('span').removeClass("checked");
				
				//Check the yes button
				$(element).closest('.accordion-body').find('.seeking-yes').closest('span').addClass("checked");
				$(element).closest('.accordion-body').find('.seeking-yes').trigger('change');
				$(element).closest('.accordion-body').find('.seeking-yes').attr('checked','checked');
				
				//Hide medicare eligible sign
				$(element).closest('.accordion-body').find('.medicareEligible').css("display", "none");
			}
			return;
	}
	
	$("#profileForm")
			.validate(
					{
						ignore: [],
						rules : {
							claimerName : {
								lettersOnly : true
							},
							zipCode : {
								required : true,
								number : true,
								minlength : 3,
								maxlength : 5,
							},
							numberOfDependents : {
								number : true,
								min:0,
								max:19
							},

							taxHouseholdIncome : {
								incomeValidator : true
							}
						},
						messages : {
							claimerName : {
								lettersOnly : "<span>Your name should include only letters</span>"
							},
							zipCode : {
								required : "<span>Please enter your zip code</span>",
								number : "<span>Zip code must be 3-5 numeric characters</span>",
								minlength : "<span>Zip code must be 3-5 numeric characters</span>",
								maxlength : "<span>Zip code must be 3-5 numeric characters</span>"
							},
							numberOfDependents : {
								number : "No. of dependents should be a number",
								max:"Number of dependents cannot exceed 19."
							}
						},
						onkeyup: false,
						highlight : function(element, errorClass) {
							/*shows the error message on invalid input*/
							var elementId = $(element).attr('id');
							
							
							$('#' + elementId + '_error').show();
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							
							if (elementId == null || elementId == '') {
								elementId = element.attr('name');
							}
							
							
							error.appendTo($("#" + elementId + "_error"));
							
							$("#" + elementId + "_error")
									.attr('class', 'error alert alert-error');
						},
						success : function(error, element) {
							/*hides the error message on a valid input*/
							var elementId = $(element).attr('id');
							if (elementId == null || elementId == '') {
								elementId = $(element).attr('name');
							}
						
							
							$('#' + elementId + '_error').hide();
							
						}
	});
	
	$("#householdForm").validate(
			{
					rules: {
						selfDob:{required:true,dob:true,ageLess19:true},
						spouseDob:{required:true,dob:true,ageLess19:true}
					},
					messages : {
						selfDob:{required:"<span>Date of Birth is required</span>",ageLess19:"<span>Primary applicant should be above 19 years of age</span>"},
						spouseDob:{required:"<span>Date of Birth is required</span>",ageLess19:"<span>Spouse should be above 19 years of age</span>"}
					},
					onkeyup: false,
					highlight : function(element, errorClass) {
						/*shows the error message on invalid input*/
						var elementId = $(element).attr('id');
						$('#' + elementId + '_error').show();
						$('#' + elementId + '_message').hide();
					},
					errorClass : "error",
					errorPlacement : function(error, element) {
						var elementId = element.attr('id');
						if (elementId == null || elementId == '') {
							elementId = element.attr('name');
						}
						error.appendTo($("#" + elementId + "_error"));
						$("#" + elementId + "_error")
								.attr('class', 'error alert alert-error');
					},
					success : function(error, element) {
						/*hides the error message on a valid input*/
						var elementId = $(element).attr('id');
						if (elementId == null || elementId == '') {
							elementId = $(element).attr('name');
						}
						$('#' + elementId + '_error').hide();
					}
	});
	
	
	$("#incomeForm").validate({
			
			rules: {
				refineincome : {required:true,refineIncomeValidator:true}
			},
			
			messages: {
				refineincome : {required:"<span>Income is required</span>",refineIncomeValidator : "<span>Invalid income amount</span>",}
			},
			
			onkeyup: false,
			highlight : function(element, errorClass) {
				
				var elementId = $(element).attr('id');
				$('#' + elementId + '_error').show();
			},
			errorClass : "error",
			errorPlacement : function(error, element) {
				var elementId = element.attr('id');
				if (elementId == null || elementId == '') {
					elementId = element.attr('name');
				}
				error.appendTo($("#" + elementId + "_error"));
				$("#" + elementId + "_error")
						.attr('class', 'error alert alert-error');
			},
			success : function(error, element) {
				
				var elementId = $(element).attr('id');
				if (elementId == null || elementId == '') {
					elementId = $(element).attr('name');
				}
				$('#' + elementId + '_error').hide();
			}
	});
	
</script>

<script type="text/javascript">
  	
	ctx = "${pageContext.request.contextPath}";
	//Popup window code
	function newPopup(url) {
		popupWindow = window.open(
			url,'popUpWindow','height=300,width=300,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes'
		);
	}

	
  	//Import Profile from turbotax.com
	$(document).ready(function($) {
		
		smrtysttkn = "<c:out value='${smartystreettkn}' />";
		
		$("#mask2").hide(); 

		if(window.location.pathname == '/hix/eligibility/prescreen/'){
		window.location.pathname = '/hix/eligibility/prescreen';
		}
		$('.numbersOnlyField').keypress(function (e) { 
			if (e.which != 8 && e.which != 0 && (e.which<48 || e.which>57)){
				return false;
			}
		});
		
		$('#zipCode').keyup(function(){
			if($(this).val().length == 5){
				validateZip();
			}
		});
		
		
		$('#zipCode').blur(function(){
			
			if($('#zipCode').val().length == 3){
				$('#zipCode').val('00'+$('#zipCode').val());
				$('#zipCode').trigger('change');
			}
			else if($('#zipCode').val().length == 4){
				$('#zipCode').val('0'+$('#zipCode').val());
				$('#zipCode').trigger('change');
			}
			
			validateZip();
		});
		
		$('#county').change(function(){
			index = $(this).prop("selectedIndex");
			if(index == 0){
				$('#stateCode').val('');
			}
			else{
				$('#county_error').hide();
				var e = document.getElementById("stateCode");
				var state = e.options[index].value;
				$('#stateCode').val(state);
			}
			
		});
		
		//$("#taxHouseholdIncome").mask("$?99999999",{placeholder:''} );
		//$("#alimonyPaid").mask("$?99999999",{placeholder:''} );
		//$("#studentLoans").mask("$?99999999",{placeholder:''} );
		
		$('#refineincome').keypress(function(){
			
			if($(this).val().length > 10){
				return true;
			}
			
			if($(this).val().replace(/,/g ,'').length == 10){
				return false;
			}
		});
		
		$('#refineincome').blur(function(){
			
			if( $(this).valid() != 0 && $(this).val() != $("#taxHouseholdIncome").val().replace(/\\$/g, '')) {
				
				$('.incomeType').each(function(){
					$(this).val('$0');
				});

				$('.income-slider').each(function(){
					$(this).slider("value",0);
				});

				$('#alimonyPaid').val('$0');
				$("#alimonyPaidSlider").slider("value",0);

				$('#studentLoans').val('$0');
				$("#studentLoansSlider").slider("value",0);

				$('#jobIncome_self').val('$' + $(this).val().replace(/,/g , ''));
				$('#jobIncomeSlider_self').slider("value",$(this).val().replace(/,/g , ''));

				calculateRefinedIncome();
				calculateAPTC();
			}
		});
		
		
		//Prevent email form submission on enter
		$('#email').keypress(function(event) { 
			return event.keyCode != 13; 
		});
		
		$("#sidenav2 .sidenav").mouseleave(function(){
			$("#sidenav2 .sidenav").popover('hide');
			$('#sidenav2 .sidenav').data('popover').options.content = '';
		    });
		$("#sidenav2 .sidenav").popover({
			title: '<span type="button" title="x" id="close" class="close" onclick="$(&quot;#sidenav2 .sidenav&quot;).popover(&quot;hide&quot;);">x</span>',
			html: true,
			content: '',
			trigger:'none'
		});
		
		$("#sidenav3 .sidenav").mouseleave(function(){
			$("#sidenav3 .sidenav").popover('hide');
			$('#sidenav3 .sidenav').data('popover').options.content = '';
		    });
		$("#sidenav3 .sidenav").popover({
			title: '<span type="button" title="x" id="close" class="close" onclick="$(&quot;#sidenav3 .sidenav&quot;).popover(&quot;hide&quot;);">x</span>',
			html: true,
			content: '',
			trigger:'none'
		});
		
		$("#sidenav5 .sidenav").mouseleave(function(){
			$("#sidenav5 .sidenav").popover('hide');
			$('#sidenav5 .sidenav').data('popover').options.content = '';
		    });
		$("#sidenav5 .sidenav").popover({
			title: '<span type="button" title="x" id="close" class="close" onclick="$(&quot;#sidenav5 .sidenav&quot;).popover(&quot;hide&quot;);">x</span>',
			html: true,
			content: '',
			trigger:'none'
		});
		
		$("#importprofile").bind('click', function(){
			$("#mask1").hide();
			$("#progress-wrap").show('fast');
			
			setTimeout(function(){ 

				$('#progress-wrap').hide();
				$("#mask2").show();
				$("#importprofile").removeClass('btn-info');
				$("#importprofile").html('<i class=" icon-check"></i>&nbsp; Imported From Your Turbotax.com Account');

			}, 4000);

		});

						
		/* default radio selections */
		
		$('.pregnant-no').each(function(){
			$(this).closest('span').addClass("checked");
			
		});
		
		$('.rel-child').each(function(){
			$(this).closest('span').addClass("checked");
			
		});
		
		$('.seeking-yes').each(function(){
			$(this).closest('span').addClass("checked");
			
		});
		
		/* default radio selections end */
	
	
		$('#collapseOne').siblings('.accordion-heading').find('.accordion-toggle').find('i').addClass('icon-chevron-down');
		
		$('.accordion-body').on('show',function(){
			$(this).siblings('.accordion-heading').find('.accordion-toggle').
			find('i').addClass('icon-chevron-down');
		});
		$('.accordion-body').on('hide',function(){
			$(this).siblings('.accordion-heading').find('.accordion-toggle').
			find('i').removeClass('icon-chevron-down');
		});
		
		
		var items = $('#slides.carousel-inner'),index = 0,timeout = 0;
	
		/* vertical scroll */       
		
		function autoForward() {
			var children = items.children();
		
			index = index === children.length - 1 ? 0 : index + 1;
			
			animate(children.eq(index));
		}
		
		
		function startTimer() {
			if (timeout) {
				clearTimeout(timeout);
			}
			timeout = setTimeout(autoForward, 5000000);
		}
		
		
		function animate(item) {
			if (timeout) {
				clearTimeout(timeout);
			}
			
			items.stop(true).animate({
				top: -item.position().top
			}, 'slow', startTimer);
		}
	
	
		$('#sidebar.nav').find('a.sidenav').click(function() {
			var link = $(this),
			hash = link.attr('href').replace(/^.*#/, '#'),
			item = $(hash);
		
			index = item.index();
			
			$("#refineincome").changePolling('stop');
			
			//If user clicks on the same side nav button as they are on currently, no processing shud be done
			if((hash == '#item-1' && $('#currentTab').val() == '1') ||
					(hash == '#item-2' && $('#currentTab').val() == '2') ||
					(hash == '#item-3' && $('#currentTab').val() == '3') ||
					(hash == '#item-5' && $('#currentTab').val() == '4')){
				return false;
			}
			
			if(hash == '#item-1' && $('#currentTab').val() == '2'){
				if ($('#householdForm').valid() && !validateNoOfApplicants()) {
					return false;	
				}
			}
			
			if(hash == '#item-1' || hash == "#item-0"){
				$("#sampleplan").fadeOut(200);
				$("#showplans").fadeOut(200);
				$('#flipCounterDiv').show();
				$('#medicaidplan').hide();
				$('#medicareplan').hide();
				$('#healthplan').hide();
				$('#currentTab').val('1');
				$('#sidebar').find('li.active').removeClass('active');
				$(this).parent('li').addClass('active');
				animate(item);
				return false;
			}
		
			if (hash == '#item-2') {
				if ($('#profileForm').valid() && validateCounty()) {	
					generateHouseholdSection();
					$('#currentTab').val('2');
					$( "#resultsDialog" ).dialog({
		    			  dialogClass: "no-close",
		    			  autoOpen: false,
		    			  closeOnEscape: false,
		    			  modal: true,
		    			  draggable: false,
		    			  resizable: false,
		    		});
		    		$( "#resultsDialog" ).dialog("open");
				} 
				else {
					if(userFirstTimeNavigation){
						$('#sidenav2 .sidenav').data('popover').options.content = 'Since this is your first time building your household, you will need to fill out each tab in order.';
						$("#sidenav2 .sidenav").popover('show');
					}else{
						$('#sidenav2 .sidenav').data('popover').options.content = 'Please enter valid data in Profile tab to proceed.';
						$("#sidenav2 .sidenav").popover('show');
					}
					return false;
				}
			}
			if (hash == '#item-3') {
				generateHouseholdSection();
				if ($('#profileForm').valid() && $('#householdForm').valid() && validateNoOfApplicants()) {
					generateIncomeSection();
					$('img.digitsDashImage').hide();
					$('div#flip-counter').removeClass('hide');
					$('#currentTab').val('3');
					$( "#resultsDialog" ).dialog({
		    			  dialogClass: "no-close",
		    			  autoOpen: false,
		    			  closeOnEscape: false,
		    			  modal: true,
		    			  draggable: false,
		    			  resizable: false,
		    		});
		    		$( "#resultsDialog" ).dialog("open");
				} 
				else {
					if(userFirstTimeNavigation){
						$('#sidenav3 .sidenav').data('popover').options.content = 'Since this is your first time building your household, you will need to fill out each tab in order.';
						$("#sidenav3 .sidenav").popover('show');
					}else if(!$('#profileForm').valid()){
						$('#sidenav3 .sidenav').data('popover').options.content = 'Please enter valid data in Profile tab to proceed.';
						$("#sidenav3 .sidenav").popover('show');
					}else if(!$('#householdForm').valid() || !validateNoOfApplicants()){
						$('#sidenav3 .sidenav').data('popover').options.content = 'Please enter valid data in Household tab to proceed.';
						$("#sidenav3 .sidenav").popover('show');
					}
					return false;
				}
			}
			if (hash == '#item-5') {
				generateHouseholdSection();
				if ($('#profileForm').valid() && $('#householdForm').valid() && $('#incomeForm').valid() && validateNoOfApplicants()) {
					$( "#resultsDialog" ).dialog({
	    			  dialogClass: "no-close",
	    			  autoOpen: false,
	    			  closeOnEscape: false,
	    			  modal: true,
	    			  draggable: false,
	    			  resizable: false,
	    			});
	    			$( "#resultsDialog" ).dialog("open");
					
					$('#currentTab').val('4');
				} 
				else {
					if(userFirstTimeNavigation){
						$('#sidenav5 .sidenav').data('popover').options.content = 'Since this is your first time building your household, you will need to fill out each tab in order.';
						$("#sidenav5 .sidenav").popover('show');
					}else if(!$('#profileForm').valid()){
						$('#sidenav5 .sidenav').data('popover').options.content = 'Please enter valid data in Profile tab to proceed.';
						$("#sidenav5 .sidenav").popover('show');
					}else if(!$('#householdForm').valid() || !validateNoOfApplicants()){
						$('#sidenav5 .sidenav').data('popover').options.content = 'Please enter valid data in Household tab to proceed.';
						$("#sidenav5 .sidenav").popover('show');
					}
			
					return false;
				}
			}
			$('#sidebar').find('li.active').removeClass('active');
			$(this).parent('li').addClass('active');
			
			calculateAPTC();
			
			animate(item);
			return false;
		});
	
		$('.widget-footer').find('a').click(function() {
			var link = $(this),
			hash = link.attr('href').replace(/^.*#/, '#'),
			item = $(hash);
		
		
			index = item.index();
		
			if(hash == '#'){
				return false;
			}
			
			if (hash == '#item-1' || hash=='#item-0') {
				animate(item);
				$('#flipCounterDiv').show();
				$('#currentTab').val('1');
				return;
			}
			if (hash == '#item-2') {
				if ($('#profileForm').valid() && validateCounty()) {
					$( "#resultsDialog" ).dialog({
		    			  dialogClass: "no-close",
		    			  autoOpen: false,
		    			  closeOnEscape: false,
		    			  modal: true,
		    			  draggable: false,
		    			  resizable: false,
		    			});
		    			$( "#resultsDialog" ).dialog("open");
					animate(item);
					$('#currentTab').val('2');
				} else {
					return false;
				}
			}

			if (hash == '#item-3') {
				
				if(!validateNoOfApplicants()){
					return false;
				}
				
				if ($('#householdForm').valid() && validateNoOfApplicants()) {
					generateIncomeSection();
					$( "#resultsDialog" ).dialog({
		    			  dialogClass: "no-close",
		    			  autoOpen: false,
		    			  closeOnEscape: false,
		    			  modal: true,
		    			  draggable: false,
		    			  resizable: false,
		    			});
		    			$( "#resultsDialog" ).dialog("open");
					animate(item);
					$('#currentTab').val('3');
				} else {
					return false;
				}
			}
			
			if(hash == '#item-5'){
				
				if($('#incomeForm').valid()){
				
					$( "#resultsDialog" ).dialog({
		    			  dialogClass: "no-close",
		    			  autoOpen: false,
		    			  closeOnEscape: false,
		    			  modal: true,
		    			  draggable: false,
		    			  resizable: false,
		    		});
		    		$( "#resultsDialog" ).dialog("open");
					
					$('#currentTab').val('4');
				}
				else{
					return false;
				}
			}
			
			
			calculateAPTC();
			
			animate(item);
			return false;
		});
	
		startTimer();
		/* vertical scroll */   

	});
</script>

<!-- Tax Household income slider -->
<script>

	$(document).ready(function(){
	
		//Household Income Slider
		$("#taxHouseholdIncomeSlider").slider({
			range: "min",
			value: 0,
			min: 0,
			max: 150000,
			step: 1000,
			slide: function( event, ui ) {
				$("#taxHouseholdIncome_error").hide();
				$("#taxHouseholdIncome").val("$" + ui.value);
				//Setting income page as latest
				profileModel.set({
					isIncomePageLatest : false
				});
				
				//Clearing all other incomes
				$('.incomeType').each(function(){
					$(this).val('$0');
				});
				
				$('.income-slider').each(function(){
					$(this).slider("value",0);
				});
			}
		});
		
		$("#taxHouseholdIncomeSlider").slider( "value", 30000 );
		$("#taxHouseholdIncome").val("$" + $("#taxHouseholdIncomeSlider").slider("value"));
		
		$("#taxHouseholdIncome").change(function(){
			if($("#taxHouseholdIncome").valid() != 0){
				var income = Math.round($.trim($("#taxHouseholdIncome").val().replace(/\\$/g, '')));
				if(!isNaN(income)){
					$("#taxHouseholdIncomeSlider").slider('value', income);
					$("#taxHouseholdIncome").val('$' + income);
				}
				else{
					$("#taxHouseholdIncomeSlider").slider('value', 0);
					$("#taxHouseholdIncome").val('$');
				}
				//Setting income page latest as false
				profileModel.set({
					isIncomePageLatest : false
				});
				
				//Clearing all other incomes
				$('.incomeType').each(function(){
					$(this).val('$0');
				});
				
				$('.income-slider').each(function(){
					$(this).slider("value",0);
				});
			}
		});
		
	});	
</script>

<!-- Deduction section sliders -->
<script type="text/javascript">

	$(document).ready(function(){
		//Alimony Deduction slider
		$("#alimonyPaidSlider").slider({
			range: "min",
			value: 0,
			min: 0,
			max: 100000,
			step: 1000,
			slide: function( event, ui ) {
				$("#alimonyPaid_error").hide();
				$("#alimonyPaid").val( "$" + ui.value);
				calculateRefinedIncome();
				
			}
		});
		
		$("#alimonyPaid").val("$" + $("#alimonyPaidSlider").slider("value"));
		
		$("#alimonyPaid").change(function(){
			if($("#alimonyPaid").valid() != 0){
				var income = Math.round($.trim($("#alimonyPaid").val().replace(/\\$/g, ''))) ;
				income = Math.floor((income + 500) / 1000) * 1000;
				if(!isNaN(income)){		
					$("#alimonyPaid").val('$' + income);
					$("#alimonyPaidSlider").slider("value",income);
				}
				else{
					$("#alimonyPaid").val('$');
					$("#alimonyPaidSlider").slider("value",0);
				}
				calculateRefinedIncome();
				
			}
		});
		
		
		// Student Loan Deduction slider
		$("#studentLoansSlider").slider({
			range: "min",
			value: 0,
			min: 0,
			max: 5000,
			step: 100,
			slide: function( event, ui ) {
				$("#studentLoans_error").hide();
				$("#studentLoans").val( "$" + ui.value);
				calculateRefinedIncome();
				
			}
		});
		
		$("#studentLoans").val("$" + $("#studentLoansSlider").slider("value"));
		
		$("#studentLoans").change(function(){
			if($("#studentLoans").valid() != 0){
				var income = Math.round($.trim($("#studentLoans").val().replace(/\\$/g, '')));
				income = Math.floor((income + 50) / 100) * 100;
				if(!isNaN(income)){		
					$("#studentLoans").val('$' + income);
					$("#studentLoansSlider").slider("value",income);
				}
				else{
					$("#studentLoans").val('$');
					$("#studentLoansSlider").slider("value",income);
				}
				calculateRefinedIncome();
				
			}
		});
	});
</script>

<!-- Flip Counter -->
<script type="text/javascript">
$(document).ready(function(){			

	// Initialize a new counter
	myCounter = new flipCounter('flip-counter', {
		value:0, 
		inc:1, 
		pace:2, 
		auto:false
	});
	
	//Polling refined income text box
	$("#refineincome").changePolling({
	     interval: 2000,
	     autoStart: false
	});
	
	//Form will be reset when start over is clicked
	$("#startover").click(function(){
		location.reload(true);
	});

	$("#count0").click(function(){
		$('.highslide-html-content, table, #highslide-wrapper-0, .popover').fadeOut();
		$("#sampleplan").fadeOut(100);
		$("#importprofile").text("Import From TurboTax");
		$("#importprofile").addClass('btn-info');
		$("#mask2").hide();
		$("#mask1").show();
	
	});

	// Expand help
	$("a.expand").click(function(){
		$(this).parent().children(".toggle").slideToggle(200);
		return false;
	});
	
	$("#numberOfDependentsDecrement").click(function(){
		var numberDependents = 0;
		if($("#numberOfDependents").val()){
			numberDependents = parseInt($("#numberOfDependents").val());
		}		
		if(numberDependents>0){
		$("#numberOfDependents").val(numberDependents-1);
		}
	});
	
	$("#numberOfDependentsIncrement").click(function(){
		var numberDeps = 0;
		if($("#numberOfDependents").val()){
			numberDeps = parseInt($("#numberOfDependents").val());
		}
		if(numberDeps<19){
			numberDeps+=1;
		$("#numberOfDependents").val(numberDeps);
		}
	});

});
</script>

<script type="text/javascript">

$(document).ready(function(){
		
	if ($('#healthplan').is(':visible')){ 
		$('article.total').css({			
		'margin-top': '10px',
		'border' : '1px solid #CFCFCF',
		'background' : '#fff',
		'box-shadow' : '11px 1px 10px 2px rgba(150, 150, 150, .3)',
		'-webkit-box-shadow' : '1px 1px 10px 2px rgba(150, 150, 150, .3)',
		});
		$('#sampleplan').css({
		'height':'465px',
		});
	}
	
	
	$('div#flip-counter').addClass('hide');

	$('.haspopover').popover({
		trigger:'hover',
		delay: { show: 500}
	});
	

	$('.clickpop').popover({
		trigger:'click',
		title:'<a type="button" title="" id="close" class="close" onclick="$(&quot;.clickpop&quot;).popover(&quot;hide&quot;);">&times;</a>',
		delay: { show: 0}
	});
	
	
	$('html').click(function() {
		$('.clickpop').popover('hide');
		
	});
	$('.sidenav').click(function() {
		$('.clickpop').popover('hide');
		
	});
	$('.btn-large').click(function() {
		$('.clickpop').popover('hide');
		
	});
	$('.clickpop').click(function(event){
	    event.stopPropagation();
	});

	$("#uniform-refine1").find('span').removeClass('checked');

	$("#uniform-refine2").find('span').addClass('checked');

	$("#openAccodion3").click(function(){
		$("#accordion3").toggle('fast');

	});


	/* Footer navigation*/
	$("#count0").on("click", function(){
		$('#sidebar').find('li.active').removeClass('active');
		$("#sidebar").find("#sidenav1").addClass("active");
	});
	$("#baseincome").on("click", function(){
		
		if (!$('#profileForm').valid() || !validateCounty()) {
			return false;
		}
		
		$('#sidebar').find('li.active').removeClass('active');
		$("#sidebar").find("#sidenav2").addClass("active");
	});
	$("#baseincome-p").on("click", function(){
		$('#sidebar').find('li.active').removeClass('active');
		$("#sidebar").find("#sidenav0").addClass("active");
	});
	$("#household1").on("click", function(){
		
		if (!$('#householdForm').valid() || !validateNoOfApplicants()) {
			return false;
		}
		$('#sidebar').find('li.active').removeClass('active');
		$("#sidebar").find("#sidenav3").addClass("active");
		
	});
	$("#household1-p").on("click", function(){
		$('#sidebar').find('li.active').removeClass('active');
		$("#sidebar").find("#sidenav1").addClass("active");
	});
	$("#incomeCalculator").on("click", function(){
		
		if (!$('#incomeForm').valid()) {
			return false;
		}
		
		$("#refineincome").changePolling('stop');
		
		$('#sidebar').find('li.active').removeClass('active');
		$("#sidebar").find("#sidenav5").addClass("active");
	});
	$("#income2-p").on("click", function(){
		
		$("#refineincome").changePolling('stop');
		
		$('#sidebar').find('li.active').removeClass('active');
		$("#sidebar").find("#sidenav2").addClass("active");
		
	});
	
	$("#startover-p").on("click", function(){
		
		generateIncomeSection();
		
		$('#sidebar').find('li.active').removeClass('active');
		$("#sidebar").find("#sidenav3").addClass("active");
	});

	/* Footer navigation*/

});		
</script>
</html>	