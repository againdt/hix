package com.getinsured.iex.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the SSAP_APPLICATIONS database table.
 * 
 */

public class SsapApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;

	private String applicationData;

	private String applicationStatus;

	private String applicationType;

	private BigDecimal cmrHouseoldId;

	private BigDecimal createdBy;

	private Timestamp creationTimestamp;

	private Timestamp esignDate;

	private String esignFirstName;

	private String esignLastName;

	private String esignMiddleName;

	private String caseNumber;

	private BigDecimal lastNoticeId;

	private Timestamp lastUpdateTimestamp;

	private String source;
	
	private String eligibilityStatus;
	
	private String exchangeEligibilityStatus;

	private BigDecimal ssapApplicationSectionId;
	
	private String currentPageId;
	
	private int coverageYear;
	
	private String financialAssistanceFlag;
	
	private String exemptHouseHold;
	
	private Date eligibilityReceivedDate;
	private BigDecimal ehbAmount;
	private String csrLevel;
	private String nativeAmerican;
	private BigDecimal maximumAPTC;
	private BigDecimal electedAPTC;
	private BigDecimal availableAPTC;
	private BigDecimal lastUpdatedBy;
	private String allowEnrollment;

	private String eligibilityResponseType;
	private String externalApplicationId;
	
	private Long parentSsapApplicationId;
	
	public Long getParentSsapApplicationId() {
		return parentSsapApplicationId;
	}

	public void setParentSsapApplicationId(Long parentSsapApplicationId) {
		this.parentSsapApplicationId = parentSsapApplicationId;
	}
	
	public String getFinancialAssistanceFlag() {
		return financialAssistanceFlag;
	}

	public void setFinancialAssistanceFlag(String financialAssistanceFlag) {
		this.financialAssistanceFlag = financialAssistanceFlag;
	}

	public int getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(int coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getCurrentPageId() {
		return currentPageId;
	}

	public void setCurrentPageId(String currentPageId) {
		this.currentPageId = currentPageId;
	}

	private Timestamp startDate;
	
	@JsonManagedReference("ssapApplication-ssapApplicant")
	@JsonBackReference
	private List<SsapApplicant> ssapApplicants = new ArrayList<SsapApplicant>();

	private List<SsapApplicationEvent> ssapApplicationEvents;

	public SsapApplication() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getApplicationData() {
		return this.applicationData;
	}

	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}

	public String getApplicationStatus() {
		return this.applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}

	public void setExchangeEligibilityStatus(String exchangeEligibilityStatus) {
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}
	public String getApplicationType() {
		return this.applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public BigDecimal getCmrHouseoldId() {
		return this.cmrHouseoldId;
	}

	public void setCmrHouseoldId(BigDecimal cmrHouseoldId) {
		this.cmrHouseoldId = cmrHouseoldId;
	}

	public BigDecimal getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTimestamp() {
		return this.creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getEsignDate() {
		return this.esignDate;
	}

	public void setEsignDate(Timestamp esignDate) {
		this.esignDate = esignDate;
	}

	public String getEsignFirstName() {
		return this.esignFirstName;
	}

	public void setEsignFirstName(String esignFirstName) {
		this.esignFirstName = esignFirstName;
	}

	public String getEsignLastName() {
		return this.esignLastName;
	}

	public void setEsignLastName(String esignLastName) {
		this.esignLastName = esignLastName;
	}

	public String getEsignMiddleName() {
		return this.esignMiddleName;
	}

	public void setEsignMiddleName(String esignMiddleName) {
		this.esignMiddleName = esignMiddleName;
	}

	public String getCaseNumber() {
		return this.caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public BigDecimal getLastNoticeId() {
		return this.lastNoticeId;
	}

	public void setLastNoticeId(BigDecimal lastNoticeId) {
		this.lastNoticeId = lastNoticeId;
	}

	public Timestamp getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public BigDecimal getSsapApplicationSectionId() {
		return this.ssapApplicationSectionId;
	}

	public void setSsapApplicationSectionId(BigDecimal ssapApplicationSectionId) {
		this.ssapApplicationSectionId = ssapApplicationSectionId;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public List<SsapApplicant> getSsapApplicants() {
		return this.ssapApplicants;
	}

	public void setSsapApplicants(List<SsapApplicant> ssapApplicants) {
		this.ssapApplicants = ssapApplicants;
	}

	public SsapApplicant addSsapApplicant(SsapApplicant ssapApplicant) {
		getSsapApplicants().add(ssapApplicant);
		//ssapApplicant.setSsapApplication(this);

		return ssapApplicant;
	}

	public SsapApplicant removeSsapApplicant(SsapApplicant ssapApplicant) {
		getSsapApplicants().remove(ssapApplicant);
		//ssapApplicant.setSsapApplication(null);

		return ssapApplicant;
	}

	public List<SsapApplicationEvent> getSsapApplicationEvents() {
		return this.ssapApplicationEvents;
	}

	public void setSsapApplicationEvents(List<SsapApplicationEvent> ssapApplicationEvents) {
		this.ssapApplicationEvents = ssapApplicationEvents;
	}

	public SsapApplicationEvent addSsapApplicationEvent(SsapApplicationEvent ssapApplicationEvent) {
		getSsapApplicationEvents().add(ssapApplicationEvent);
		ssapApplicationEvent.setSsapApplication(this);

		return ssapApplicationEvent;
	}

	public SsapApplicationEvent removeSsapApplicationEvent(SsapApplicationEvent ssapApplicationEvent) {
		getSsapApplicationEvents().remove(ssapApplicationEvent);
		ssapApplicationEvent.setSsapApplication(null);

		return ssapApplicationEvent;
	}

	public String getExemptHouseHold() {
		return exemptHouseHold;
	}

	public void setExemptHouseHold(String exemptHouseHold) {
		this.exemptHouseHold = exemptHouseHold;
	}

	public Date getEligibilityReceivedDate() {
		return eligibilityReceivedDate;
	}

	public void setEligibilityReceivedDate(Date eligibilityReceivedDate) {
		this.eligibilityReceivedDate = eligibilityReceivedDate;
	}

	public BigDecimal getEhbAmount() {
		return ehbAmount;
	}

	public void setEhbAmount(BigDecimal ehbAmount) {
		this.ehbAmount = ehbAmount;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getNativeAmerican() {
		return nativeAmerican;
	}

	public void setNativeAmerican(String nativeAmerican) {
		this.nativeAmerican = nativeAmerican;
	}

	public BigDecimal getMaximumAPTC() {
		return maximumAPTC;
	}

	public void setMaximumAPTC(BigDecimal maximumAPTC) {
		this.maximumAPTC = maximumAPTC;
	}

	public BigDecimal getElectedAPTC() {
		return electedAPTC;
	}

	public void setElectedAPTC(BigDecimal electedAPTC) {
		this.electedAPTC = electedAPTC;
	}

	public BigDecimal getAvailableAPTC() {
		return availableAPTC;
	}

	public void setAvailableAPTC(BigDecimal availableAPTC) {
		this.availableAPTC = availableAPTC;
	}

	public BigDecimal getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getAllowEnrollment() {
		return allowEnrollment;
	}

	public void setAllowEnrollment(String allowEnrollment) {
		this.allowEnrollment = allowEnrollment;
	}

	public String getEligibilityResponseType() {
		return eligibilityResponseType;
	}

	public void setEligibilityResponseType(String eligibilityResponseType) {
		this.eligibilityResponseType = eligibilityResponseType;
	}

	public String getExternalApplicationId() {
		return externalApplicationId;
	}

	public void setExternalApplicationId(String externalApplicationId) {
		this.externalApplicationId = externalApplicationId;
	}

}