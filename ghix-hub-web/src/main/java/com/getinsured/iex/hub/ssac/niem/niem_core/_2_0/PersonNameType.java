
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for a combination of names and/or titles by which a person is known.
 * 
 * <p>Java class for PersonNameType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonNameType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonGivenName" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonMiddleName" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonSurName"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonMaidenName" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonFullName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonNameType", propOrder = {
    "personGivenName",
    "personMiddleName",
    "personSurName",
    "personMaidenName",
    "personFullName"
})
public class PersonNameType
    extends ComplexObjectType
{

    @XmlElement(name = "PersonGivenName", nillable = true)
    protected PersonNameTextType personGivenName;
    @XmlElement(name = "PersonMiddleName", nillable = true)
    protected PersonNameTextType personMiddleName;
    @XmlElement(name = "PersonSurName", required = true, nillable = true)
    protected PersonNameTextType personSurName;
    @XmlElement(name = "PersonMaidenName", nillable = true)
    protected PersonNameTextType personMaidenName;
    @XmlElement(name = "PersonFullName", nillable = true)
    protected PersonNameTextType personFullName;

    /**
     * Gets the value of the personGivenName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameTextType }
     *     
     */
    public PersonNameTextType getPersonGivenName() {
        return personGivenName;
    }

    /**
     * Sets the value of the personGivenName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameTextType }
     *     
     */
    public void setPersonGivenName(PersonNameTextType value) {
        this.personGivenName = value;
    }

    /**
     * Gets the value of the personMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameTextType }
     *     
     */
    public PersonNameTextType getPersonMiddleName() {
        return personMiddleName;
    }

    /**
     * Sets the value of the personMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameTextType }
     *     
     */
    public void setPersonMiddleName(PersonNameTextType value) {
        this.personMiddleName = value;
    }

    /**
     * Gets the value of the personSurName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameTextType }
     *     
     */
    public PersonNameTextType getPersonSurName() {
        return personSurName;
    }

    /**
     * Sets the value of the personSurName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameTextType }
     *     
     */
    public void setPersonSurName(PersonNameTextType value) {
        this.personSurName = value;
    }

    /**
     * Gets the value of the personMaidenName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameTextType }
     *     
     */
    public PersonNameTextType getPersonMaidenName() {
        return personMaidenName;
    }

    /**
     * Sets the value of the personMaidenName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameTextType }
     *     
     */
    public void setPersonMaidenName(PersonNameTextType value) {
        this.personMaidenName = value;
    }

    /**
     * Gets the value of the personFullName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameTextType }
     *     
     */
    public PersonNameTextType getPersonFullName() {
        return personFullName;
    }

    /**
     * Sets the value of the personFullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameTextType }
     *     
     */
    public void setPersonFullName(PersonNameTextType value) {
        this.personFullName = value;
    }

}
