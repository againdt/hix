package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;

import org.springframework.batch.core.UnexpectedJobExecutionException;

import com.getinsured.hix.batch.ssap.renewal.util.EligibilityResponse;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface RedetermineEligibilityService {

	void saveAndThrowsErrorLog(String errorMessage, String errorCode) throws UnexpectedJobExecutionException;

	List<Long> getSsapAppIdsForRenewalWithSgStatus(Long renewalYear, Long batchSize);

	List<SsapApplication> getSsapApplicationListByIds(List<Long> applicationIdList);

	EligibilityResponse invokeEligibilityEngine(long ssapApplicationId);

	String invokeOutboundAT(long ssapApplicationId);

}
