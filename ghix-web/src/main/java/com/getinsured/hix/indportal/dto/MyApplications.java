package com.getinsured.hix.indportal.dto;

import java.util.ArrayList;
import java.util.List;

public class MyApplications {

	private List<MyApplicationsEntry> currentApplications = new ArrayList<MyApplicationsEntry>(0);
	private List<MyApplicationsEntry> pastApplications = new ArrayList<MyApplicationsEntry>(0);
	private String coverageYear;
	private boolean insideOE;
	private boolean insideQEP;
	private String householdCaseId;
	private List<MyApplicationsEntry> queuedApplications = new ArrayList<MyApplicationsEntry>(0);
	private List<String> userPermissionList = new ArrayList<String>();


	public List<MyApplicationsEntry> getCurrentApplications() {
		return currentApplications;
	}

	public void setCurrentApplications(List<MyApplicationsEntry> currentApplications) {
		this.currentApplications = currentApplications;
	}

	public List<MyApplicationsEntry> getPastApplications() {
		return pastApplications;
	}

	public void setPastApplications(List<MyApplicationsEntry> pastApplications) {
		this.pastApplications = pastApplications;
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

	public boolean isInsideOE() {
		return insideOE;
	}

	public void setInsideOE(boolean insideOE) {
		this.insideOE = insideOE;
	}

	public boolean isInsideQEP() {
		return insideQEP;
	}

	public void setInsideQEP(boolean insideQEP) {
		this.insideQEP = insideQEP;
	}

	public String getHouseholdCaseId() {
		return householdCaseId;
	}

	public void setHouseholdCaseId(String householdCaseId) {
		this.householdCaseId = householdCaseId;
	}
	
	public List<MyApplicationsEntry> getQueuedApplications() {
		return queuedApplications;
	}

	public void setQueuedApplications(List<MyApplicationsEntry> queuedApplications) {
		this. queuedApplications = queuedApplications;
	}

	public List<String> getUserPermissionList() {
		return userPermissionList;
	}

	public void setUserPermissionList(List<String> userPermissionList) {
		this.userPermissionList = userPermissionList;
	}
	
}
