import { ProviderInfo } from './../../models/data.model';
import { trigger, transition, style ,animate} from '@angular/animations';
import { BROWSER_ANIMATIONS_PROVIDERS } from '@angular/platform-browser/animations/src/providers';
import { Component, OnInit, Input, Output, OnDestroy, EventEmitter } from '@angular/core';

@Component({
    selector: 'doctor-card',
    templateUrl: './doctor-card.component.html',
    styleUrls: ['./doctor-card.component.scss'],
    animations: [trigger('abc', [
        transition(':enter', [
            style({ opacity: 0 }),
            animate(500, style({ opacity: 1 }))
        ]),
        transition(':leave', [
            animate(500, style({ opacity: 0 }))
        ])
    ])]
})

export class DoctorCardComponent implements OnInit {
    @Input() doctor: ProviderInfo;
    @Output() destoryed = new EventEmitter<ProviderInfo>();
    @Input() public selectedType: string;
    constructor() { }

    ngOnInit() { }

    public destroy(event): void {
        this.destoryed.emit(this.doctor);
    }
}
