package com.getinsured.iex.hub.ssac.wrapper;


import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ssac.extension.SSACompositeIndividualRequestType;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory;
import com.google.gson.Gson;

public class RequestCitizenshipVerificationIndicatorWrapper implements JSONAware {
	private  boolean requestCitizenshipVerificationIndicator=false;
	private Boolean requestCitizenshipVerificationIndicatorFlag=null;
	private static ObjectFactory xsd;
	private static com.getinsured.iex.hub.ssac.extension.ObjectFactory extnFactory;
	
	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	
	public static RequestCitizenshipVerificationIndicatorWrapper fromJsonString(String jsonString) throws IOException {
		return gson.fromJson(jsonString,RequestCitizenshipVerificationIndicatorWrapper.class);
	}
	
	public RequestCitizenshipVerificationIndicatorWrapper() {
		xsd = new ObjectFactory();
		extnFactory = new com.getinsured.iex.hub.ssac.extension.ObjectFactory();
	}
	
	public RequestCitizenshipVerificationIndicatorWrapper(Boolean flag) {
		this.requestCitizenshipVerificationIndicatorFlag = flag;
		this.requestCitizenshipVerificationIndicator = this.requestCitizenshipVerificationIndicatorFlag.isValue();
	}
	
	public boolean isRequestCitizenshipVerificationIndicator() {
		return this.requestCitizenshipVerificationIndicator;
	}
	
	public void setRequestCitizenshipVerificationIndicator(boolean isVerifyFlag) {
		this.requestCitizenshipVerificationIndicator = isVerifyFlag;
		this.requestCitizenshipVerificationIndicatorFlag = xsd.createBoolean();
		this.requestCitizenshipVerificationIndicatorFlag.setValue(isVerifyFlag);
	}
	
	public Boolean getSystemRepresentation() {
		return this.requestCitizenshipVerificationIndicatorFlag;
	}
	

	public String toJSONString() {
		// TBD
		JSONObject obj = new JSONObject();
		//obj.put(SUR_NAME_KEY, this.surName);
		return obj.toJSONString();
	}
	
	public static void main(String[] args) throws JAXBException, IOException {
		//RequestCitizenshipVerificationIndicatorWrapper requestCitizenshipVerificationIndicator = fromJsonString("{\"person\":{\"surName\": \"test123\", \"ssn\": \"123456789\", \"birthDate\": \"2009-08-08\"},\"RequestCitizenshipVerificationIndicator\":\"false\"}");
		RequestCitizenshipVerificationIndicatorWrapper reqCitizenVerificationIndicator = fromJsonString("{\"requestCitizenshipVerificationIndicator\":\"true\"}");
		SSACompositeIndividualRequestType req = extnFactory.createSSACompositeIndividualRequestType();
		
		req.setRequestCitizenshipVerificationIndicator(reqCitizenVerificationIndicator.requestCitizenshipVerificationIndicatorFlag);
		JAXBElement<SSACompositeIndividualRequestType> ssaCompReq = extnFactory.createSSACompositeIndividualRequest(req);
		JAXBContext jc = JAXBContext.newInstance("com.getinsured.hub.ssac.extension");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(ssaCompReq, System.out);
		
	}
}
