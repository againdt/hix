import keyMirror from 'keymirror';

export const ActionTypes = keyMirror({
    // UI CHANGES
    CHANGE_ORIGIN_AMOUNT: null,
    CHANGE_DESTINATION_AMOUNT: null,
    CHANGE_ORIGIN_CURRENCY: null,
    CHANGE_DESTINATION_CURRENCY: null,

    // AJAX Calls
    REQUEST_CONVERSION_RATE: null,
    RECEIVED_CONVERSION_RATE_SUCCESS: null,
    RECEIVED_CONVERSION_RATE_FAILURE: null,

    REQUEST_FEES: null,
    RECEIVED_FEES_SUCCESS: null,

    RECEIVED_AJAX_CALL_FAILURE: null,

    REQUEST_DASHBOARD_INFO: null,
    RECEIVED_DASHBOARD_INFO_SUCCESS: null,
    RECEIVED_DASHBOARD_INFO_FAILURE: null,

    RECEIVED_MUTLTIHOUSEHOLD_SUCCESS: null,
    CHANGE_PAGE_CONTAINER: null,

    // Verification page
    RECEIVED_VERIFICATION_DATA_SUCCESS: null,
    RECEIVED_ISSUERS_DATA_SUCCESS: null,
    UPDATED_VERIFICATION_DATA: null,
    SELECT_APPLICANT_INDEX: null,
    FETCHING_ERROR: null,
});

export const VERIFICATION_DATA = {
    ISSUER_CODE: 'issuerCode',
    GROUP_NUMBER: 'groupNumber',
    POLICY_NUMBER: 'policyNumber',
    ACTION: 'action',
    ACTION_SUBMIT: 'SUBMIT',
    ACTION_SKIP: 'SKIP',
    ACTION_OVERRIDE: 'OVERRIDE'
};

export const PAGE_CONTAINERS = {
    DASHBOARD: 'DASHBOARD',
    VERIFICATION: 'VERIFICATION'
};

export const spinnerActionTypes = keyMirror({
    START_SPINNER: null,
    STOP_SPINNER: null,
    CLEAR_STATE: null
});

export const messagesActionTypes = keyMirror({
    SET_MESSAGES: null,
    CLEAR_STATE: null
});

export const languageActionTypes = keyMirror({
    SET_LANGUAGE: null,
    CLEAR_STATE: null
});

export const QEP = "QEP";
export const OE = "OE";
