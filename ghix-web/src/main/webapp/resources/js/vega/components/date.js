﻿define(['textbox'], function (txtbox) {
    var date = new baseComponent();    
    date.type = "date";
    date.getHTML = function (cgfObj) {
        var defCfg = {
            etype:"date"
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;
        var vm = this;
        vm.context.registerComponent(txtbox);        
        var textbox = vm.context.comp["textbox"].getHTML(compData);
        var $date = $(textbox);
        $date.find("input#" + compData.name).attr("placeholder", "MM/DD/YYYY").attr("kendo-date-picker", "").removeClass("k-textbox");
        return $date[0].outerHTML;
    };
    return date;
});