package com.getinsured.hix.broker.service;

import com.getinsured.hix.model.Employer;
import com.getinsured.hix.platform.util.exception.GIException;

public interface EmployerService {
	Employer findEmployerByExternalId(String externalId) throws GIException;
}