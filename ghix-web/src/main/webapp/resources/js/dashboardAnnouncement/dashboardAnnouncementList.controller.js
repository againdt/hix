(function(){
	'use strict';
	
	angular.module("dashboardAnnouncementApp").controller('DashboardAnnouncementListController', DashboardAnnouncementListController);

	DashboardAnnouncementListController.$inject=['$state', '$http', '$scope', '$sce'];
	
	function DashboardAnnouncementListController($state, $http, $scope, $sce){
		var vm = this;
		
		vm.loading = true;
		

		vm.currentPage = 1;
		$scope.setPage = function(pageNo) {
			vm.currentPage = pageNo;
		};
		  
		var response = $http.get('../announcements');
		response.success(function(data, status, headers, config) {
			vm.loading = false;
			vm.announcementList = data;
			
			angular.forEach(vm.announcementList, function(value, key){
				value.announcementText = $sce.trustAsHtml(value.announcementText);
			});
		});
		
		vm.editAnnouncement = editAnnouncement;
		vm.createAnnouncement = createAnnouncement;
		vm.orderByField = orderByField;
		
		function editAnnouncement(announcementId){
			$state.go("announcementEdit",{id:announcementId});
		}
		
		function createAnnouncement(announcementId){
			$state.go('announcement');
		}
		
		function orderByField(item){
			if(vm.orderField === 'status'){
				return item.status;
			}else{
				return new Date(item[vm.orderField]);
			}
		}
	}
		
})();