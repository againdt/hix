package com.getinsured.hix.provider.web;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnclarityNetworkTierAndId {

private static final Pattern VALID_PATTERN = Pattern.compile("[0-9]+|[A-Z0-9]+");
	
	public Map<String,String> getNetworkIdTierAndHiosInfo(String networkId, String networkTier){
		Map<String,String> parsedNetworkId = parseNetworkId(networkId);
		Map<String,String> parsedNetworkTier = parseNetworkTier(networkTier);
		Map<String,String> networkTierMap = new HashMap<String,String>();
		Set<String> keys = parsedNetworkId.keySet();
		for(String key:keys){
			if(key.contains("OTHER")){
				networkTierMap.put(parsedNetworkId.get(key), parsedNetworkTier.get("OTHER"));
			}else if(!key.contains("HIOS")){
				networkTierMap.put(parsedNetworkId.get(key), parsedNetworkTier.get(key));
			}else if(key.contains("HIOS")){
				networkTierMap.put(key,parsedNetworkId.get(key));
			}
		}
		return networkTierMap;
	}
	private  Map<String,String> parseNetworkId(String networkId) {
		int hiosCount = 1;
		int hiosNetworkIdCount = 1;
		int nonHiosNetworkIdCount = 1;
		String separatedNetworks[] = networkId.split("\\|");
		LinkedHashMap <String,String> chunks = new LinkedHashMap <String,String>();
		String hiosId= "";
		for(int j=0;j<separatedNetworks.length;j++){
			boolean isHiosFound = false;
			Matcher matcher = VALID_PATTERN.matcher(separatedNetworks[j]);
			while (matcher.find()) {
				String s = matcher.group();
				if(isHiosFound&&!s.matches("-?\\d+")){
					chunks.put(hiosId+s, hiosId+s);
					hiosNetworkIdCount++;
					isHiosFound = false;
				}else if(!s.matches("-?\\d+")){
					chunks.put("OTHER_"+nonHiosNetworkIdCount, s);
					nonHiosNetworkIdCount++;
				}
				if(s.matches("-?\\d+")){
					if(!chunks.containsValue(s)){
						chunks.put("HIOS_"+hiosCount, s);
						hiosId = s;
						hiosCount++;
					}else{
						hiosId = s;
					}
					isHiosFound = true;
				}

			}
		}
	    return chunks;
	}
	
	private  Map<String,String> parseNetworkTier(String networkTiers) {
		boolean isHiosFound = false;
		Map<String,String> chunks = new HashMap<String,String>();
		String separatedNetworks[] = networkTiers.split("\\|");
		String hiosId= "";
		for(int j=0;j<separatedNetworks.length;j++){
			String networkTier = separatedNetworks[j];
			if(networkTier.contains("_")){	
				String networkTierArray[] = networkTier.split("_");
				String networkId = networkTierArray[0];
				String networkTierValue = networkTierArray[1];
				Matcher matcher = VALID_PATTERN.matcher(networkTier);
				while (matcher.find()) {
					String s = matcher.group();
					if(isHiosFound&&!s.matches("-?\\d+")){
						chunks.put(hiosId+s, networkTierValue);
						//chunks.put(s, networkTierValue);	 
						isHiosFound = false;
					}else if(s.matches("-?\\d+")&&s.length()<3){
						chunks.put("OTHER", s);
					}
					if(s.matches("-?\\d+")){
						hiosId = s;
						isHiosFound = true;
					}

				}
			}else{
				chunks.put("OTHER", networkTier);
			}

		}
	    return chunks;
	}
	

}