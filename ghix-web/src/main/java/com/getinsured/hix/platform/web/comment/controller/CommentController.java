/**
 * 
 */
package com.getinsured.hix.platform.web.comment.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.platform.comment.service.CommentService;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;

/**
 * @author panda_p
 *
 */

@Controller
public class CommentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommentController.class);
	
	@Autowired private CommentTargetService commentTargetService;
	@Autowired private UserService userService;
	@Autowired private CommentService commentService;
	
	//Constants
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final String PAGE_TITLE = "page_title";	
	private static final String REDIRECT_CONFIRM_DELETE = "redirect:/platform/web/comment/controller/confirmdeletecommentPopup?commentId=";	

	/**
	 * Added by Pratap Kumar panda
	 * Edited by Nikhil Talreja
	 * 
	 * @param request - HTTP request object from the form
	 * @param model - Model Object
	 * @return String - redirection URL
	 */
	
	@RequestMapping(value="/platform/web/comment/controller/savecomment",method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'COMMENT_ACCESS_PERMISSION')")
	@ResponseBody public String saveComment(final HttpServletRequest request,final Model model){
		
		LOGGER.info("inside ajax call method=====================");
		
		HttpSession session = request.getSession();
		if(session == null){
			return "failure";
		}
		
		String targetId = request.getParameter("target_id");
		String targetName = request.getParameter("target_name");
		String commentText = request.getParameter("comment_text");
		
		//LOGGER.info("Commented by : " + request.getParameter("commented_by"));
		//LOGGER.info("Target id : " + targetId);
		//LOGGER.info("Target name : " + targetName);
		//LOGGER.info("Comment text : " + commentText);
		if(commentText != null) {
			commentText = StringEscapeUtils.unescapeHtml(commentText.trim());
		}
		
		try{
			LOGGER.info("TargetName.valueOf(targetName) : " +TargetName.valueOf(targetName));
			TargetName.valueOf(targetName);
		}catch (Exception e) {
			// TODO: handle exception
			LOGGER.info("Exception has occured",e);
			return "failure";
		}
		
				
		CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(Long.valueOf(targetId), TargetName.valueOf(targetName));
		if(commentTarget == null){
			commentTarget =  new CommentTarget();
			commentTarget.setTargetId(new Long(targetId));
			commentTarget.setTargetName(TargetName.valueOf(targetName));
		}
		
		List<Comment> comments = null;
		
		if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
			comments = new ArrayList<Comment>();
		}
		else{
			comments = commentTarget.getComments();
		}
		
		
		LOGGER.info("Retrieving logged in user's name");
		
		AccountUser accountUser = null;
		
		try{
			accountUser = userService.getLoggedInUser();
			
			if(accountUser==null){
				throw new InvalidUserException();
			}
		}
		catch(InvalidUserException e){
			LOGGER.info("Exception has occured",e);
			return "failure";
		}
		
		String commenterName = "";
		
		if(accountUser != null){
			commenterName += (accountUser.getFirstName() == null || accountUser.getFirstName().length() == 0 ) ? "" :  accountUser.getFirstName()+" ";
			commenterName += (accountUser.getLastName() == null || accountUser.getLastName().length() == 0 ) ? "" : accountUser.getLastName();
		}
		
		//Checking if user's name is blank
		if(commenterName == null || commenterName.trim().length() == 0){
			commenterName = "Anonymous User";
		}
		
		LOGGER.info("Commented by : " + commenterName);
		
		Comment comment = new Comment();
		//comment.setComentedBy(request.getParameter("commented_by").trim());
		
		//Setting logged in user's id
		LOGGER.info("Commenter's user Id : " + accountUser.getId());
		comment.setComentedBy(accountUser.getId());
		
		comment.setCommenterName(commenterName);
		
		//Fetching first 4000 char for comment text;
		int beginIndex = 0;
		int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
		commentText = commentText.substring(beginIndex, endIndex);
		//commentText = commentText.replaceAll("\\n|\\r", " ");
		comment.setComment(commentText);
		
		comment.setCommentTarget(commentTarget);
		
		comments.add(comment);
		commentTarget.setComments(comments);
		commentTargetService.saveCommentTarget(commentTarget);
		
		return "success";
	}
	
	@RequestMapping(value="/platform/web/comment/controller/editcomment",method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'COMMENT_ACCESS_PERMISSION')")
	@ResponseBody public String editComment(final HttpServletRequest request,final Model model){
		
		LOGGER.info("inside ajax call method=====================");
		
		HttpSession session = request.getSession();
		if(session == null){
			return "failure";
		}
		
		String commentId = request.getParameter("commentId");
		String commentText = request.getParameter("comment_text");
		
		//LOGGER.info("Comment id : " + commentId);
		//LOGGER.info("Comment text : " + commentText);
		if(commentText != null) {
			commentText = commentText.trim();
		}
		
		Comment c = commentService.findCommentById(Integer.valueOf(commentId));
		int beginIndex = 0;
		int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
		commentText = commentText.substring(beginIndex, endIndex);
		//commentText = commentText.replaceAll("\\n|\\r", " ");
		c.setComment(commentText);
		c = commentService.saveComment(c);
		
		return "success";
	}

	@RequestMapping(value="/platform/web/comment/controller/deletecomment/{id}",method = {RequestMethod.GET, RequestMethod.POST})
	@PreAuthorize("hasPermission(#model, 'COMMENT_ACCESS_PERMISSION')")
	@ResponseBody public String deleteComment(final HttpServletRequest request, final Model model, @PathVariable("id") Integer id){
		LOGGER.info("inside ajax call delete comment method=====================");
		commentService.deleteComment(id);
		return "success";

	}		
	
	@RequestMapping(value = "platform/web/comment/controller/confirmdeletecommentPopup", method = {RequestMethod.GET, RequestMethod.POST})
	@PreAuthorize("hasPermission(#model, 'COMMENT_ACCESS_PERMISSION')")
	public String confirmdeletecommentPopup(@RequestParam(value="commentId", required=false) String commentId, Model model, HttpServletRequest request) {
		model.addAttribute(PAGE_TITLE,"Getinsured Health Exchange: Comment Delete Confirmation");		
		return "platform/web/comment/confirmdeletecommentPopup";
	}
	
	@RequestMapping(value="/platform/web/comment/controller/confirmdeletecomment/{id}",method = {RequestMethod.GET, RequestMethod.POST})
	@PreAuthorize("hasPermission(#model, 'COMMENT_ACCESS_PERMISSION')")
	public String confirmDeleteComment(final HttpServletRequest request, final Model model, @PathVariable("id") Integer id){
		LOGGER.info("inside ajax call confirm Delete Comment method=====================");			
		return REDIRECT_CONFIRM_DELETE + id;
	}
}
