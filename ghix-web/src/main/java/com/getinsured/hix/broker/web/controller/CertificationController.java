package com.getinsured.hix.broker.web.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.agency.utils.AgencyUtils;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.agency.AgencyAssistantInfoDto;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.AgencySiteDto;
import com.getinsured.hix.dto.broker.BrokerSiteLocationDTO;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Broker.certification_status;
import com.getinsured.hix.model.BrokerResponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.AEEConfiguration.AEEConfigurationEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Handles requests for the application home page.
 */
@SuppressWarnings("unused")
@Controller
public class CertificationController {

	private static final int EIGHT = 8;

	private static final int SEVEN = 7;

	private static final int FOUR = 4;

	private static final int THREE = 3;

	private static final int ZERO = 0;

	private static final int TWELVE = 12;

	private static final String HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER = "hasPermission(#model, 'BROKER_VIEW_BROKER')";

	private static final String HAS_PERMISSION_MODEL_BROKER_ADD_BROKER = "hasPermission(#model, 'BROKER_ADD_BROKER')";

	private static final String FAX_NUMBER3 = "faxNumber3";

	private static final String FAX_NUMBER2 = "faxNumber2";

	private static final String FAX_NUMBER1 = "faxNumber1";

	private static final String BUSINESS_CONTACT_PHONE3 = "businessContactPhone3";

	private static final String BUSINESS_CONTACT_PHONE2 = "businessContactPhone2";

	private static final String BUSINESS_CONTACT_PHONE1 = "businessContactPhone1";

	private static final String ALTERNATE_PHONE3 = "alternatePhone3";

	private static final String ALTERNATE_PHONE2 = "alternatePhone2";

	private static final String ALTERNATE_PHONE1 = "alternatePhone1";

	private static final String PHONE3 = "phone3";

	private static final String PHONE2 = "phone2";

	private static final String PHONE1 = "phone1";

	private static final String N = "N";

	private static final String Y = "Y";

	private static final String LOCATION_MATCHING = "locationMatching";

	private static final String SWITCH_TO_MODULE_ID = "switchToModuleId";

	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";

	private static final String FALSE = "false";

	private static final String TRUE = "true";

	private static final String BROKER_OBJECT_FOR_LEFT_NAV = "brokerObjectForLeftNav";

	private static final String DESIGNATED_BROKER_PROFILE = "designatedBrokerProfile";

	private static final String STATELIST = "statelist";
	
	private static final String STATECODE = "stateCode";

	private static final String BROKER = "broker";

	private static final String MAILING_LOCATION_OBJ = "mailingLocationObj";

	private static final String LOCATION_OBJ = "locationObj";

	private static final String BROKER_RENEW = "broker/renew";

	private static final String PAGE_TITLE = "page_title";

	private static final String CERTIFICATION_STATUS = "certification_status";

	private static final Logger LOGGER = LoggerFactory.getLogger(CertificationController.class);

	private static final String PAYMENT_ID = "paymentId";
	private static final String DATE_FORMAT = "MM-dd-yyyy";
	private static final String UPDATEPROFILE = "updatedProfile";
	private static final String LAST_VISITED_URL = "lastVisitedURL";
	private static final String ALL_MAIL_NOTICES="allowMailNotice";
	private static final String LIST_OF_AGENCY_SITES = "agencySites";

	@Autowired
	private BrokerService brokerService;
	@Autowired
	private UserService userService;
	
	@Autowired
	private ZipCodeService zipCodeService;
	
	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	
	@Autowired
	SmartValidator smartValidator;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired 
	RestTemplate restTemplate;
	
	@Autowired
	GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	AgencyUtils agencyUtils;
	/**
	 * To validate license number for uniqueness
	 *
	 * @param licenseNumber
	 *            String
	 * @return String 'EXISTS' if number is already exists in db else string
	 *         'NEW'
	 */
	@RequestMapping(value = "/broker/certificationapplication/chkLicenseNumber")
	@PreAuthorize("hasPermission(#model, 'BROKER_EDIT_BROKER')")
	@ResponseBody
	public String chkLicenseNumber(@RequestParam String licenseNumber) {
		LOGGER.info("chkLicenseNumber: START");
		Broker brokerOb = brokerService.findBrokerByLicenseNumber(licenseNumber);

		if (brokerOb != null) {
			LOGGER.info("chkLicenseNumber: END");
			return "EXIST";
		}
		LOGGER.info("chkLicenseNumber: END");
		return "NEW";

	}

	/**
	 * Saves certification information page
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param locationObj
	 *            {@link Location}
	 * @param licenseRenewalDate
	 *            String of date
	 * @param mailingLocationObj
	 *            {@link Location}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param redirectAttr
	 *            {@link RedirectAttributes}
	 * @return URL for navigation to certification application page
	 * @throws CloneNotSupportedException 
	 */
	@RequestMapping(value = "/broker/certificationapplication", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
	public String brokerCertificationSubmit(@ModelAttribute(BROKER) Broker broker,BindingResult bindingResult,
			@ModelAttribute(LOCATION_OBJ) Location locationObj,
			@RequestParam(value = "licrenewaldate", required = false) String licenseRenewalDate,
			@ModelAttribute(MAILING_LOCATION_OBJ) Location mailingLocationObj, Model model,
			HttpServletRequest request, RedirectAttributes redirectAttr) throws GIRuntimeException  {
		long startTime = TimeShifterUtil.currentTimeMillis();
		String refParam="";
		String savedBrkId = "";
		LOGGER.info("brokerCertificationSubmit: START->"+startTime);

			try{
				
				decryptRequestParam(request);
				
				setBrokerStateForMailingLocation(broker);
				
				Broker brokerClone = null;
				try {
					brokerClone = (Broker) broker.clone();
				} catch (CloneNotSupportedException e) {
					LOGGER.error("CloneNotSupportedException occured in brokerCertificationSubmit", e);
					throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
				}
				AccountUser user = new AccountUser();
				String certificationStatus = request.getParameter("certificationStatus");
				String firstName = "";
				String lastName = "";
		
				
				user = userService.getLoggedInUser();
				if(null == request.getAttribute("newBrokerRegistration")){
					brokerClone.setUser(user);	
				}
				Broker brokerObj = null;
				brokerObj = retrieveBroker(request, user, false);
				
				if(!"Y".equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)) && !userService.hasUserRole(user, RoleService.AGENCY_MANAGER )    ){
					if(brokerObj != null && brokerObj.getId() != 0 && brokerObj.getCertificationStatus()!=null && !BrokerConstants.INCOMPLETE.equals(brokerObj.getCertificationStatus())){
						if(!brokerMgmtUtils.isBrokerCertified(brokerObj.getUser().getId(), false)){
							throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
						}
					}
				}
				
				brokerObj = retrieveAndPopulateBroker(broker, licenseRenewalDate, request, brokerClone, user, firstName, lastName);
				brokerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
				validateBrokerRegFirstStep(bindingResult, brokerObj);
				if(null != request.getAttribute("newBrokerRegistration") && (userService.hasUserRole(user, RoleService.AGENCY_MANAGER) || RoleService.AGENCY_MANAGER.equalsIgnoreCase(user.getActiveModuleName())) 
						|| userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2)){
					setAgencyInfoIntoBrokerObject(brokerObj, user,request);
				}
				
				
				Broker newSavedBroker = brokerService.saveBrokerWithLocation(brokerObj);
				/*if(null == brokerObj.getUser()){
					//This condition is for newly created brokers (through agency) which doesn't have user account
					AccountUser acUser= new AccountUser();
					acUser.setFirstName(brokerObj.getFirstName());
					acUser.setLastName(brokerObj.getLastName());
					brokerObj.setUser(acUser);
				}*/
				savedBrkId = Integer.toString(newSavedBroker.getId());
				LOGGER.debug("Broker saved with location:");
	
				/** Code to create the module user record for current user.*/  
				persistModuleUser(user, newSavedBroker);
	
				model.addAttribute(STATELIST, new StateHelper().getAllStates());
				model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
				model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
				model.addAttribute(BROKER, brokerObj);
				request.getSession().setAttribute(DESIGNATED_BROKER_PROFILE, brokerObj);
	
				getPaymentInformation(request, brokerObj);
				request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
			
				
				// request.getSession().removeAttribute("designatedBrokerProfile");
				if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER) || RoleService.AGENCY_MANAGER.equalsIgnoreCase(user.getActiveModuleName())  ){
					AgencyInformationDto resAgencyManObj =null;
					
					if(     request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
							&& request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
							&& ((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase(Y)      ){
						resAgencyManObj = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+user.getActiveModuleId() ,AgencyInformationDto.class);
						
					}else {
						resAgencyManObj = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/getAgencyForBroker/"+user.getId(),AgencyInformationDto.class);
						
					}
					if("CERTIFIED".equalsIgnoreCase(resAgencyManObj.getCertificationStatus().toString()) && ! certification_status.Incomplete.toString().equalsIgnoreCase(newSavedBroker.getCertificationStatus().toString())){
						//If agency Manager.. Broker certification is notIncomplete...and Aency anaer is certified
						if(null != request.getAttribute("newBrokerRegistration") || null != request.getAttribute("brokerId")){
							Map<String,String> paramMap= new HashMap<String,String>(3);
					        paramMap.put("brokerId", savedBrkId );
					        String encryptedValue = GhixAESCipherPool.encryptParameterMap(paramMap)  ;
							refParam = "?ref="+encryptedValue;
							model.addAttribute("activePage", "agentProfile"); 
						}
						return "redirect:/broker/viewcertificationinformation"+refParam;
					}
				}else if(userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2)){
					AgencyInformationDto resAgencyManObj =null;
					AgencyAssistantInfoDto agencyAssistantInfoDto = null;
					if(!StringUtils.isBlank(user.getRecordId())) {
						agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/"+user.getRecordId(), AgencyAssistantInfoDto.class);
						if(agencyAssistantInfoDto!=null && agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
							resAgencyManObj = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId()), AgencyInformationDto.class);
						}
					}
					if(resAgencyManObj!=null && resAgencyManObj.getCertificationStatus()!=null && "CERTIFIED".equalsIgnoreCase(resAgencyManObj.getCertificationStatus().toString()) && ! certification_status.Incomplete.toString().equalsIgnoreCase(newSavedBroker.getCertificationStatus().toString())){
						//If agency Manager.. Broker certification is notIncomplete...and Aency anaer is certified
						if(null != request.getAttribute("newBrokerRegistration") || null != request.getAttribute("brokerId")){
							Map<String,String> paramMap= new HashMap<String,String>(3);
					        paramMap.put("brokerId", savedBrkId );
					        String encryptedValue = GhixAESCipherPool.encryptParameterMap(paramMap)  ;
							refParam = "?ref="+encryptedValue;
							model.addAttribute("activePage", "agentProfile"); 
						}
						return "redirect:/broker/viewcertificationinformation"+refParam;
					}
				}
				
				if (   certificationStatus != null && !"".equals(certificationStatus)
						&& !certificationStatus.equalsIgnoreCase(certification_status.Pending.toString())
						&& !certificationStatus.equalsIgnoreCase(certification_status.Incomplete.toString())   ) {
					LOGGER.info("brokerCertificationSubmit: END");
					return "redirect:/broker/viewcertificationinformation";
				}
				long endTime = TimeShifterUtil.currentTimeMillis();
				LOGGER.info("brokerCertificationSubmit: END->"+endTime);
				LOGGER.info("brokerCertificationSubmit total duration ->"+(endTime-startTime));
			}catch(GIRuntimeException giruntimeexception){
	            LOGGER.error("GIRuntimeException occured while saving certification information page : ", giruntimeexception);
	            throw giruntimeexception;
			}catch(Exception exception){
				LOGGER.error("Exception occured while saving certification information page"+exception);
				throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
			
			
			if(null != request.getAttribute("newBrokerRegistration") || null != request.getAttribute("brokerId")){
				Map<String,String> paramMap= new HashMap<String,String>(3);
		        paramMap.put("brokerId", savedBrkId );
		        String encryptedValue = GhixAESCipherPool.encryptParameterMap(paramMap)  ;
				refParam = "?ref="+encryptedValue;
				model.addAttribute("activePage", "agentProfile"); 
			}
			

		return "redirect:/broker/buildprofile"+refParam;
	}

	private void validateBrokerRegFirstStep(BindingResult bindingResult,Broker brokerObj) throws Exception {
		List<Object> validationGroups = new ArrayList<Object>();
		validationGroups.add(Broker.BrokerRegistration.class);
		
		if(brokerObj.getBusinessContactPhoneNumber()!=null && !brokerObj.getBusinessContactPhoneNumber().trim().isEmpty())
		{
			validationGroups.add(Broker.BrokerBusinessContact.class);
		}
		
		if(brokerObj.getAlternatePhoneNumber()!=null && !brokerObj.getAlternatePhoneNumber().trim().isEmpty())
		{
			validationGroups.add(Broker.BrokerAlternateContact.class);
		}
		if("Fax".equalsIgnoreCase(brokerObj.getCommunicationPreference()) || (brokerObj.getFaxNumber()!=null && !brokerObj.getFaxNumber().trim().isEmpty())){
			validationGroups.add(Broker.BrokerFaxContact.class);
		}
		
		smartValidator.validate(brokerObj,bindingResult, validationGroups.toArray());
		if(bindingResult.hasErrors()){
			throw new Exception(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION); 	
		}
	}
	
	public static void fillRequestParamInModel(HttpServletRequest request,Model model){
		Enumeration<String> enumerationOfRequestParmeter = request.getParameterNames();
	     while(enumerationOfRequestParmeter.hasMoreElements()){
	    	 String parameterName = enumerationOfRequestParmeter.nextElement();
	    	 model.addAttribute(parameterName,request.getParameter(parameterName));
	     }
	}
	/** 
	 * Method to persist Module User record for an Agent.
	 * 
	 * @param user The current AccountUSer instance.
	 * @param newBroker The current Broker instance.
	 */
	private void persistModuleUser(AccountUser user, Broker newBroker) throws GIRuntimeException {
		try {
			if(null != user && null != user.getDefRole() && RoleService.BROKER_ROLE.equalsIgnoreCase(user.getDefRole().getName())) {
				Validate.notNull(newBroker, "No Broker record for current user found in repository.");
				ModuleUser moduleUser = userService.findModuleUser(newBroker.getId(), ModuleUserService.BROKER_MODULE, user);
				
				if (null == moduleUser) {
			        userService.createModuleUser(newBroker.getId(), ModuleUserService.BROKER_MODULE, user, true);
				}	
				else {
					LOGGER.info("Module User creation ignored for current user agent.");
				}
	         }
			else {
				LOGGER.info("Module User creation not done for current user agent.");
			}
		} 
		catch (Exception ex) {
			LOGGER.error("Exception occurred while updating module user for Agent", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}
	}

	/**
	 * Method to set state value of mailing location of a Broker.
	 * 
	 * @param broker The current Broker instance.
	 */
	private void setBrokerStateForMailingLocation(Broker broker) {
		if (broker.getLocation() != null && broker.getMailingLocation() != null && broker.getMailingLocation().getState() == null && broker.getLocation().getState() != null) {
				broker.getMailingLocation().setState(broker.getLocation().getState());
		}
	}

	/**
	 * Method to retrieve and populate Broker instance.
	 * 
	 * @param broker The Broker instance.
	 * @param licenseRenewalDate The Broker's license renewal date.
	 * @param request The current HttpServletRequest instance.
	 * @param brokerClone The cloned Broker instance.
	 * @param user The AccountUser instance.
	 * @param firstName The Broker's first name.
	 * @param lastName The Broker's last name.
	 * @return brokerObj = The modified Broker instance.
	 * @throws NumberFormatException The NumberFormatException instance.
	 * @throws ParseException The ParseException instance.
	 */
	private Broker retrieveAndPopulateBroker(Broker broker,
			String licenseRenewalDate, HttpServletRequest request,
			Broker brokerClone, AccountUser user, String firstName, String lastName)
			throws ParseException {
		Broker brokerObj = retrieveBroker(request, user, false);

		if (brokerObj == null) {

			LOGGER.debug("No existing Broker record found. Hence creating new record");

			if (!StringUtils.isBlank(user.getRecordId()) && null == request.getAttribute("newBrokerRegistration")) {
				brokerObj = retrieveBrokerDetailForUser(user);
			} else {
				brokerObj = new Broker();
			}
			if(null == request.getAttribute("newBrokerRegistration")){
				brokerObj.setUser(user);
			}
			
		}
		String phone = request.getParameter(PHONE1) + "-" + request.getParameter(PHONE2) + "-"
				+ request.getParameter(PHONE3);
		brokerObj.setContactNumber(phone);
		brokerObj.setAlternatePhoneNumber(brokerClone.getAlternatePhoneNumber());
		brokerObj.setBusinessContactPhoneNumber(brokerClone.getBusinessContactPhoneNumber());
		brokerObj.setFaxNumber(brokerClone.getFaxNumber());
		brokerObj.setCommunicationPreference(brokerClone.getCommunicationPreference());
		brokerObj.setPostalMailEnabled(brokerClone.getPostalMailEnabled());
		brokerObj.setPersonalEmailAddress(brokerClone.getPersonalEmailAddress());
		brokerObj.setNpn(brokerClone.getNpn());

		if( userService.hasUserRole( user, RoleService.AGENCY_MANAGER) || BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE) || !"Certified".equalsIgnoreCase( brokerObj.getCertificationStatus()) || ( request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
				&& request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
				&& ((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase(Y)  )   ){
			  
				setBrokerName(broker, firstName, lastName, brokerObj);
		}

		LOGGER.debug("Values set in new Broker object");

		if (brokerObj.getCertificationStatus() == null) {
			LOGGER.debug("Setting certification status as Incomplete");
			brokerObj.setCertificationStatus(certification_status.Incomplete.toString()); // Added
																							// to
																							// set
																							// the
																							// certification
																							// status
																							// to
																							// Incomplete
																							// when
																							// Broker
																							// Registration
																							// along
																							// with
																							// Payment
																							// Info
																							// is
																							// complete
			brokerObj.setApplicationDate(new TSDate());
		}

		if(userService.hasUserRole( user, RoleService.AGENCY_MANAGER) ||  BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE) || !"Certified".equalsIgnoreCase( brokerObj.getCertificationStatus()) || ( request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
				&& request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
				&& ((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase(Y))   ){
			
			setBrokerObj(broker, brokerObj, request,user);
			if (!EntityUtils.isEmpty(licenseRenewalDate)) {
				
					LOGGER.debug("Setting license renewal date");
					SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
					Date licenseRenDate = dateFormat.parse(licenseRenewalDate);
					brokerObj.setLicenseRenewalDate(licenseRenDate);
				
			}
		}
		return brokerObj;
	}

	/**
	 * Method to retrieve Broker details for broker identified by given User identifier.
	 * 
	 * @param user The current AccountUser instance.
	 * @return brokerObj The Broker instance.
	 */
	private Broker retrieveBrokerDetailForUser(AccountUser user) {
		Broker brokerObj;
		try {
			brokerObj = brokerService.getBrokerDetail(Integer.valueOf(user.getRecordId()));
		} catch (NumberFormatException nfe) {
			brokerObj = new Broker();
		}
		return brokerObj;
	}

	/**
	 * Getting payment information
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param brokerObj
	 *            {@link Broker}
	 * @return int paymentId
	 */
	private int getPaymentInformation(HttpServletRequest request, Broker brokerObj) {
		int paymentId = 0;
		
		if (BrokerMgmtUtils.isPaymentEnabled()) {
			LOGGER.debug("Retrieving payment detail for existing broker");
			if (brokerObj.getId() != 0) {
				PaymentMethods paymentMethodsObj = brokerService.getPaymentMethodDetailsForBroker(brokerObj.getId(),
						PaymentMethods.ModuleName.BROKER);
				if (paymentMethodsObj != null) {
					paymentId = paymentMethodsObj.getId();
				}
			}
			request.getSession().setAttribute("paymentObjId", paymentId);
		}
		return paymentId;
	}

	/**
	 * Set values from broker to brokerObj
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param brokerObj
	 *            {@link Broker}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param LoggedInUser  
	 */
	private void setBrokerObj(Broker broker, Broker brokerObj, HttpServletRequest request, AccountUser loggedInUser ) {

		LOGGER.info("setBrokerObj : START");

		if (broker != null) {
			populateBrokerWithLocationInfo(broker, brokerObj, request,loggedInUser);
			
			populateBrokerWithMailingLocationInfo(broker, brokerObj);
			
			brokerObj.setCompanyName(broker.getCompanyName());
			brokerObj.setLicenseNumber(broker.getLicenseNumber());
			if(!broker.getFederalEIN().contains("*")){
				brokerObj.setFederalEIN(broker.getFederalEIN());
			}
		}

		LOGGER.info("setBrokerObj : END");
	}

	/**
	 * Method to populate Broker instance with Broker's mailing location.
	 * 
	 * @param broker The original Broker instance.
	 * @param brokerObj The modified Broker instance.
	 */
	private void populateBrokerWithMailingLocationInfo(Broker broker,
			Broker brokerObj) {
		if (broker.getMailingLocation() != null && broker.getMailingLocation().getZip()!=null) {

			if((broker.getMailingLocation().getLat()==0.0)&&(broker.getMailingLocation().getLon()==0.0)){ 
				ZipCode zipCode=zipCodeService.findByZipCode(broker.getMailingLocation().getZip().toString()); 
				if(zipCode!=null){ 
					broker.getMailingLocation().setLat(zipCode.getLat());    
					broker.getMailingLocation().setLon(zipCode.getLon());            
				} 
			}
			//Remove URL encoded special char from Address
			if(broker.getMailingLocation() != null) {
				if(broker.getMailingLocation().getAddress1()!= null) {
					broker.getMailingLocation().setAddress1(URLDecodeString(broker.getMailingLocation().getAddress1()));
				}
				if(broker.getMailingLocation().getAddress2()!= null) {
					broker.getMailingLocation().setAddress2(URLDecodeString(broker.getMailingLocation().getAddress2()));
				}
				if(broker.getMailingLocation().getCity()!= null) {
					broker.getMailingLocation().setCity(URLDecodeString(broker.getMailingLocation().getCity()));
				}
			}
			
			brokerObj.setMailingLocation(broker.getMailingLocation());
			//COndition to patch disabled/readonly drop-down issue in case of agency Manager
			if(null == brokerObj.getMailingLocation().getState()){
				brokerObj.getMailingLocation().setState(brokerObj.getLocation().getState());
			}
		}
	}
	
	private String   URLDecodeString(String urlEncryptedString)   {
		String result = urlEncryptedString;
		result = StringEscapeUtils.unescapeHtml4(urlEncryptedString);
		
		return result;
	}

	/**
	 * Method to populate Broker with location information.
	 * 
	 * @param broker The persisted Broker instance.
	 * @param brokerObj The modified Broker instance.
	 * @param loggedInUser 
	 */
	private void populateBrokerWithLocationInfo(Broker broker, Broker brokerObj, HttpServletRequest request, AccountUser loggedInUser) {
		if (broker.getLocation() != null && broker.getLocation().getZip()!=null) {
		
			if((broker.getLocation().getLat()==0.0)&&(broker.getLocation().getLon()==0.0)){ 
				ZipCode zipCode=zipCodeService.findByZipCode(broker.getLocation().getZip().toString()); 
				if(zipCode!=null){ 
					broker.getLocation().setLat(zipCode.getLat());   
					broker.getLocation().setLon(zipCode.getLon());   
				} 
			} 
			
			//Remove URL encoded special char from Address
			if(broker.getLocation() != null) {
				if(broker.getLocation().getAddress1()!= null) {
					broker.getLocation().setAddress1(URLDecodeString(broker.getLocation().getAddress1()));
				}
				if(broker.getLocation().getAddress2()!= null) {
					broker.getLocation().setAddress2(URLDecodeString(broker.getLocation().getAddress2()));
				}
				if(broker.getLocation().getCity()!= null) {
					broker.getLocation().setCity(URLDecodeString(broker.getLocation().getCity()));
				}
			}

			brokerObj.setLocation(broker.getLocation());
		}else{
			//If logged in user is agency manager, get selected location from agency location and set location and mailingLocation for broker,  
			if(userService.hasUserRole( loggedInUser, RoleService.AGENCY_MANAGER)  ||  RoleService.AGENCY_MANAGER.equalsIgnoreCase(loggedInUser.getActiveModuleName()) 
					|| userService.hasUserRole( loggedInUser, RoleService.APPROVED_ADMIN_STAFF_L2)){
				if(request.getParameter("agencyLocation")!=null && !request.getParameter("agencyLocation").isEmpty()){
					String selectedAgencySiteId = ghixJasyptEncrytorUtil.decryptStringByJasypt(request.getParameter("agencyLocation"));
					BrokerSiteLocationDTO brokerSiteLocationDTO = new BrokerSiteLocationDTO();
					if(selectedAgencySiteId!=null){
							brokerSiteLocationDTO.setId(brokerObj.getId());
							brokerSiteLocationDTO.setSiteId(Long.parseLong(selectedAgencySiteId));
							String response = ghixRestTemplate.postForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/broker/location",brokerSiteLocationDTO,String.class);
							
							ObjectReader objRd=JacksonUtils.getJacksonObjectReaderForJavaType(BrokerResponse.class);
							BrokerResponse agencyInfo;
							try {
								agencyInfo = objRd.readValue(response);
							} catch (Exception e) {
								LOGGER.error("Exception occurred while converting String to JSOn Object", e);
								throw new GIRuntimeException(e , ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
							}  
							
							if(agencyInfo.getResponseCode()==200 ){
								Location loc=locationService.findById(agencyInfo.getLocationId());
								brokerObj.setLocation(loc);
							}
							
					}
					
				}
			}
		}
	}

	/**
	 * Setting broker name from broker to brokerObj
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param firstName
	 *            String
	 * @param lastName
	 *            String
	 * @param brokerObj
	 *            {@link Broker}
	 */
	private void setBrokerName(Broker broker, String firstName, String lastName, Broker brokerObj) {
		LOGGER.info("setBrokerName : START");

		String fname = firstName;
		String lname = lastName;
		if (broker != null) {
			fname = broker.getUser().getFirstName();
			lname = broker.getUser().getLastName();
		}

		if (fname != null && lname != null) {
			if(null != brokerObj.getUser()){
				brokerObj.getUser().setFirstName(fname.trim());
				brokerObj.getUser().setLastName(lname.trim());	
			}else{
				//This case will be invoked when broker object doesn't have user account
				brokerObj.setFirstName(fname.trim());
				brokerObj.setLastName(lname.trim());
			}
		}

		LOGGER.info("setBrokerName : END");
	}

	/**
	 * Redirect to certification information page
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to certification application page
	 */
	@RequestMapping(value = "/broker/viewcertificationinformation", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
	public String submitcertificationinformation(@ModelAttribute(BROKER) Broker broker, BindingResult result,
			Model model, HttpServletRequest request) {
		LOGGER.info("submitcertificationinformation : START");

		LOGGER.info("submitcertificationinformation : END");
		return "redirect:/broker/certificationapplication";
	}

	/**
	 * Display Certification Application page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param isCertified
	 *            Flag to check if broker is certified
	 * @param showconfirmation
	 *            Flag to decide if confirmation alert is to be shown
	 * @return URL for navigation to certification application page
	 */
	@RequestMapping(value = "/broker/certificationapplication", method = RequestMethod.GET)
	@GiAudit(transactionName = "BrokerCertification", eventType = EventTypeEnum.BROKER, eventName = EventNameEnum.PII_READ)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String certification(Model model, HttpServletRequest request,
			@RequestParam(value = "isCertified", required = false) String isCertified,
			@RequestParam(value = "showconfirmation", required = false) boolean showconfirmation,
			@RequestParam(value = "landingpage", required = false) boolean landingpage) throws GIRuntimeException {
		LOGGER.info("certification : START");
		model.addAttribute(PAGE_TITLE, "Certification Application");
		Broker brokerObj = null;
		AccountUser user = null;
		String returnURL = "";
		 
		try{
		try {
			user = userService.getLoggedInUser();
			
			decryptRequestParam(request);
			
			brokerObj = retrieveBroker(request, user, true);
			
			//HIX-42075-Start
			model.addAttribute(ALL_MAIL_NOTICES,DynamicPropertiesUtil.getPropertyValue(AEEConfigurationEnum.AGENT_ALLOWMAILNOTICES));
			//HIX-42075-End
			request.getSession().setAttribute(DESIGNATED_BROKER_PROFILE, brokerObj);
			if (brokerObj == null) {
				if (!StringUtils.isBlank(user.getRecordId()) && null == request.getAttribute("newBrokerRegistration") ) {
					brokerObj = retrieveBrokerDetailForUser(user);
				} else {
					brokerObj = new Broker();
					
					
					if(null != request.getAttribute("newBrokerRegistration")){
						setAgencyInfoIntoBrokerObject(brokerObj, user,request);
					}
				}
				if(null == request.getAttribute("newBrokerRegistration")){
					brokerObj.setUser(user);
				}
				
				request.getSession().setAttribute(DESIGNATED_BROKER_PROFILE, null);
			}
		} catch (InvalidUserException invalidUserException) {
			LOGGER.error("User not logged in", invalidUserException);
			throw new GIRuntimeException(invalidUserException, ExceptionUtils.getFullStackTrace(invalidUserException), 
					Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}
		
		 if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER) || RoleService.AGENCY_MANAGER.equalsIgnoreCase( user.getActiveModuleName())){
				
				setAgencySitesJsonToModel(model, brokerObj);
				model.addAttribute("isAgencyManager", true);
				model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isAgencyPaymentEnabled() );
				
				if(brokerObj.getUser() == null || brokerObj.getUser().getId() != user.getId()) {
					model.addAttribute("viewOtherBrokerInfo", true);
				}else {
					model.addAttribute("viewSelfBrokerInfo", true);
				}
				
		}else if(userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2) || RoleService.APPROVED_ADMIN_STAFF_L2.equalsIgnoreCase( user.getActiveModuleName())){
			
			setAgencySitesJsonToModel(model, brokerObj);
			model.addAttribute("isAgencyManager", false);
			model.addAttribute("isAdminStaffL2", true);
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isAgencyPaymentEnabled() );
			
			if(brokerObj.getUser() == null || brokerObj.getUser().getId() != user.getId()) {
				model.addAttribute("viewOtherBrokerInfo", true);
			}else {
				model.addAttribute("viewSelfBrokerInfo", true);
			}
			
		}else if(userService.hasUserRole(user, RoleService.ADMIN_ROLE) || userService.hasUserRole(user, RoleService.BROKER_ADMIN_ROLE) || userService.hasUserRole(user, RoleService.OPERATIONS_ROLE)){
			model.addAttribute("isAdminUser", true);
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
			model.addAttribute("isAgencyManager", false);
			model.addAttribute(LIST_OF_AGENCY_SITES+"JsonString","[]" );
		}else if(userService.hasUserRole(user, RoleService.BROKER_ROLE)){
			model.addAttribute("isAgencyManager", false);
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
			model.addAttribute(LIST_OF_AGENCY_SITES+"JsonString","[]" );
		}else{
			model.addAttribute("isAgencyManager", false);
			model.addAttribute("isAdminUser", false);
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());
			model.addAttribute(LIST_OF_AGENCY_SITES+"JsonString","[]" );
		}
		
		model.addAttribute(STATELIST, brokerMgmtUtils.filterStates());
		
		request.getSession().setAttribute("designateBroker", null);

		int paymentId = getPaymentInformation(request, brokerObj);
		model.addAttribute(PAYMENT_ID, paymentId);
		
		request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
		
		
		
		GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter("isCertified", isCertified),
				new GiAuditParameter("Certification Status", brokerObj.getCertificationStatus()));
		
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		model.addAttribute("exchangeName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		model.addAttribute("stateCode", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
		
		if ((isCertified != null && isCertified.equalsIgnoreCase(Boolean.TRUE.toString())) || (brokerObj.getCertificationStatus() == null || brokerObj.getCertificationStatus().equalsIgnoreCase(certification_status.Incomplete.toString()))) {
			setModelAttributes(model, brokerObj); //HIX-103038  HIX-102756 federal ein code shuffle
			LOGGER.info("certification : END");
			if(null == brokerObj.getUser()){
				//This case for Agency Manager resume broker creation flow. 
				brokerObj.setUser(new AccountUser());
				brokerObj.getUser().setFirstName(brokerObj.getFirstName());
				brokerObj.getUser().setLastName(brokerObj.getLastName());
			}
			model.addAttribute("activePage", "agentInfo");
			model.addAttribute("showOKicon", false);
			return "broker/certificationapplication";
		}

		
		returnURL = deduceReturnURL(request, showconfirmation, brokerObj,model);
		setModelAttributes(model, brokerObj);//HIX-103038  HIX-102756 federal ein code shuffle
		if(null == brokerObj.getUser()){
			//This condition is for newly created brokers (through agency) which doesn't have user account
			AccountUser acUser= new AccountUser();
			acUser.setFirstName(brokerObj.getFirstName());
			acUser.setLastName(brokerObj.getLastName());
			brokerObj.setUser(acUser);
		}
		}catch(GIRuntimeException giruntimeexception){
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while displaying certification"));
            LOGGER.error("GIRuntimeException occured while displaying Certification Application page : ", giruntimeexception);
            throw giruntimeexception;
		}
		catch(Exception exception)
		{
			LOGGER.error("Exception occured while displaying Certification Application page : ", exception);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while displaying certification"));
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}
		return returnURL;
	}

	private void decryptRequestParam(HttpServletRequest request) {
		if(null !=request.getParameter("ref")){
			String reference = request.getParameter("ref");
			Map<String, String> encParams = GhixAESCipherPool.decryptParameterMap(reference);
		    for(Entry<String, String> entrySet:encParams.entrySet()){
			  request.setAttribute( entrySet.getKey(), entrySet.getValue()); 
		    }
		}
		
		//Added condition to avoid  menu link encryption..
		if(null != request.getParameter("newBrokerRegistration")){
			 request.setAttribute( "newBrokerRegistration", "true"); 
		}
	}

	public void setAgencyInfoIntoBrokerObject(Broker brokerObj, AccountUser user,HttpServletRequest request)
			throws IOException, JsonProcessingException {
		String apiParam="";
		  if(user.getActiveModuleId() == 0){
		   Long agencyId=agencyUtils.retrieveAgencyMgrForBrokerId(user);
		   apiParam  = Long.toString(agencyId);
		  }else{
			  if (request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
	                    && request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
	                    && (  (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase("Y")) {
				  apiParam= user.getActiveModuleId()+"";
			  }else {
				  apiParam= user.getActiveModuleId()+"?useModuleUser=true";  
			  }
			  
			  
		  }
		  String jsonStr=ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+ apiParam , String.class);
		ObjectReader objRd=JacksonUtils.getJacksonObjectReaderForJavaType(AgencyInformationDto.class);
		AgencyInformationDto agencyInfo=objRd.readValue(jsonStr);
		
		if(agencyInfo!=null && agencyInfo.getCertificationStatus()!=null && !"CERTIFIED".equalsIgnoreCase(agencyInfo.getCertificationStatus().toString())){
			throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
		}
		
		brokerObj.setCompanyName(agencyInfo.getBusinessLegalName());
		brokerObj.setFederalEIN(agencyInfo.getFederalTaxId());
		brokerObj.setAgencyId(Long.parseLong(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyInfo.getId())));
	}
	
	public void setAgencySitesJsonToModel(Model model, Broker brokerObj){
		//HIX-100338 - Populate agency locations
		List<AgencySiteDto> agencySiteDtos = null;
		if(brokerObj.getAgencyId()!=null){
			ResponseEntity<List<AgencySiteDto>> response = restTemplate.exchange(GhixEndPoints.GHIX_BROKER_URL+"agency/site/list/"+brokerObj.getAgencyId(), HttpMethod.GET, null, new ParameterizedTypeReference<List<AgencySiteDto>>() {
            });
			if(response != null){
				agencySiteDtos = response.getBody();
			}
		}
		model.addAttribute(LIST_OF_AGENCY_SITES, agencySiteDtos);

		JsonArray jsonArray = new JsonArray();
		for (Iterator iterator = agencySiteDtos.iterator(); iterator.hasNext();) {
			AgencySiteDto agencySiteDto = (AgencySiteDto) iterator.next();
			
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("id", agencySiteDto.getId());
			jsonObject.addProperty("siteType", agencySiteDto.getSiteType());
			
			if(agencySiteDto.getLocation()!= null) {
				jsonObject.addProperty("address1", agencySiteDto.getLocation().getAddress1());
				jsonObject.addProperty("address2", agencySiteDto.getLocation().getAddress2());
				jsonObject.addProperty("city", agencySiteDto.getLocation().getCity());
				jsonObject.addProperty("state", agencySiteDto.getLocation().getState());
				jsonObject.addProperty("zip", agencySiteDto.getLocation().getZip());
				jsonObject.addProperty("lat", agencySiteDto.getLocation().getLat());
				jsonObject.addProperty("lon", agencySiteDto.getLocation().getLon());
			}else {
				String stringNull = null;
				jsonObject.addProperty("address1",stringNull);
				jsonObject.addProperty("address2",stringNull);
				jsonObject.addProperty("city", stringNull);
				jsonObject.addProperty("state", stringNull);
				jsonObject.addProperty("zip", stringNull);
				jsonObject.addProperty("lat", stringNull);
				jsonObject.addProperty("lon",stringNull);
			}
			
			jsonArray.add(jsonObject);
		}
		
		
		model.addAttribute(LIST_OF_AGENCY_SITES+"JsonString",jsonArray.toString() );
	}

	/**
	 * Method to deduce return URL for 'certification' operation.
	 * 
	 * @param request The current HttpServletRequest instance.
	 * @param showconfirmation The confirmation display flag.
	 * @param brokerObj The Broker instance.
	 * @return returnURL The return URL.
	 */
	private String deduceReturnURL(HttpServletRequest request,
			boolean showconfirmation, Broker brokerObj,Model model) {
		String returnURL;
		if (brokerObj != null && brokerObj.getCertificationStatus() != null && brokerObj.getCertificationStatus().equalsIgnoreCase(certification_status.Certified.toString())) {
			LOGGER.info("certification : START");			
			String lastVisitedURL = (String)request.getSession().getAttribute(LAST_VISITED_URL);
			request.getSession().removeAttribute(LAST_VISITED_URL);
			request.removeAttribute(LAST_VISITED_URL);
			if(lastVisitedURL != null) {
				returnURL = "redirect:"+lastVisitedURL;
			}
			else{
				returnURL = "redirect:/broker/dashboard";
			}
			model.asMap().clear();
			
		}else if(null == request.getAttribute("newBrokerRegistration") && userService.hasUserRole(brokerObj.getUser(), RoleService.AGENCY_MANAGER)){
			model.addAttribute("activePage", "agentInfo");
			returnURL = "broker/viewcertificationinformation";
		}else{
			if (showconfirmation) {
				request.getSession().setAttribute(UPDATEPROFILE, TRUE);
			} else {
				request.getSession().setAttribute(UPDATEPROFILE, FALSE);
			}
			LOGGER.info("certification: END");
			returnURL =  "broker/certificationstatus";
		}
		return returnURL;
	}

	/**
	 * Setting model attributes
	 *
	 * @param model
	 *            {@link Model}
	 * @param brokerObj
	 *            {@link Broker}
	 */
	private void setModelAttributes(Model model, Broker brokerObj) {
		LOGGER.info("setModelAttributes : START");

		if (brokerObj != null) {
			if(!BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
				formatFederalEIN(brokerObj);
			}
			model.addAttribute(BROKER, brokerObj);
			model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
			model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());
			getContactNumber(model, brokerObj);
			if (brokerObj.getLocation() != null && brokerObj.getMailingLocation() != null) {
				int comparisonResult = brokerObj.getLocation().compareTo(brokerObj.getMailingLocation());
				model.addAttribute(LOCATION_MATCHING, comparisonResult == 0 ? Y : N);
			}
		}
		LOGGER.info("setModelAttributes : END");
	}

	/**
	 * Displays readonly certification information page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param isCertified
	 *            Flag to check if broker is certified
	 * @return URL for navigation to view certification information page
	 */
	@RequestMapping(value = "/broker/viewcertificationinformation", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String viewcertificationinformation(Model model, HttpServletRequest request,
			@RequestParam(value = "isCertified", required = false) String isCertified) throws GIRuntimeException{
		LOGGER.info("viewcertificationinformation : START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: View Certification Information");
		Broker brokerObj = new Broker();
		AccountUser user = null;

		try{
		try {
			decryptRequestParam(request);
			
			user = userService.getLoggedInUser();

			brokerObj = retrieveBroker(request, user, false);
			
			agencyUtils.compareAgencyId(user, brokerObj);

		} catch (InvalidUserException ex) {
			LOGGER.error("InvalidUserException User may not be logged in", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);


		}

		if (brokerObj == null) {
			brokerObj = new Broker();
			brokerObj.setUser(user);

		}
		String allowMailNotice = DynamicPropertiesUtil.getPropertyValue("agent.allowMailNotices");
		String state = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		model.addAttribute(ALL_MAIL_NOTICES,allowMailNotice);
		model.addAttribute(STATECODE,state);
		model.addAttribute(STATELIST, new StateHelper().getAllStates());
		 
		
		model.addAttribute(BROKER, brokerObj);
		model.addAttribute(LOCATION_OBJ, brokerObj.getLocation());
		model.addAttribute(MAILING_LOCATION_OBJ, brokerObj.getMailingLocation());

		getContactNumber(model, brokerObj);
		if (brokerObj.getLocation() != null && brokerObj.getMailingLocation() != null) {
			int comparisonResult = brokerObj.getLocation().compareTo(brokerObj.getMailingLocation());
			model.addAttribute(LOCATION_MATCHING, comparisonResult == 0 ? Y : N);
		}

		int paymentId = getPaymentInformation(request, brokerObj);

		model.addAttribute(PAYMENT_ID, paymentId);
		request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
		if (userService.hasUserRole(user, RoleService.AGENCY_MANAGER) || RoleService.AGENCY_MANAGER.equalsIgnoreCase(user.getActiveModuleName())){
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isAgencyPaymentEnabled());
			model.addAttribute("isAgencyManager", true);
			model.addAttribute("isAdminStaffL2", false);
			
			AgencyInformationDto resAgencyManObj;
			if(request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
					&& request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
					&& ((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase(Y)) {
				resAgencyManObj=  ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/information/view/"+brokerObj.getAgencyId(),AgencyInformationDto.class);
				
			}else {
				resAgencyManObj = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/getAgencyForBroker/"+user.getId(),AgencyInformationDto.class);
			}
			
			if(null != resAgencyManObj){
				model.addAttribute("agency", resAgencyManObj);	
			}
			if(brokerObj.getUser() == null || brokerObj.getUser().getId() != user.getId()) {
				model.addAttribute("viewOtherBrokerInfo", true);
			}else {
				model.addAttribute("viewSelfBrokerInfo", true);
			}
		}else if(userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2) || RoleService.APPROVED_ADMIN_STAFF_L2.equalsIgnoreCase(user.getActiveModuleName())){
			model.addAttribute("isAgencyManager", false);
			model.addAttribute("isAdminStaffL2", true);
			AgencyInformationDto resAgencyManObj = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"agency/getAgencyForBroker/"+user.getId(),AgencyInformationDto.class);
			if(null != resAgencyManObj){
				model.addAttribute("agency", resAgencyManObj);	
			}
			if(brokerObj.getUser() == null || brokerObj.getUser().getId() != user.getId()) {
				model.addAttribute("viewOtherBrokerInfo", true);
			}else {
				model.addAttribute("viewSelfBrokerInfo", true);
			}
		}else{
			model.addAttribute("isAgencyManager", false);
			model.addAttribute("isAdminStaffL2", false);
			model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());	
		}
		
		//Shuffled block to prevent *  into DB 
			if(!BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
				formatFederalEIN(brokerObj);
			}
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		}catch(GIRuntimeException giexception){
            LOGGER.error("GIRuntimeException occured while diaplying readonly certification information page : ", giexception);
            throw giexception;
		} catch(Exception exception){
            LOGGER.error("Exception occured while viewing certification information page : ", exception);
            throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		if(null == brokerObj.getUser()){
			//This condition is for newly created brokers (through agency) which doesn't have user account
			AccountUser acUser= new AccountUser();
			acUser.setFirstName(brokerObj.getFirstName());
			acUser.setLastName(brokerObj.getLastName());
			brokerObj.setUser(acUser);
		}
		if(null != brokerObj.getUser() && null != brokerObj.getAgencyId()   ) {
			model.addAttribute("ROLECHANGEALLOWED","TRUE");
			if (userService.hasUserRole(brokerObj.getUser(), RoleService.AGENCY_MANAGER)) {
				model.addAttribute("CURRENTROLE","AGENCYMANAGER");
			}else {
				model.addAttribute("CURRENTROLE","BROKER");
			}
		}

		LOGGER.info("viewcertificationinformation : END");

		return "broker/viewcertificationinformation";

	}
	
	 
	
	private void formatFederalEIN(Broker broker){
		if(broker!=null && broker.getFederalEIN()!=null){
			StringBuilder sb = new StringBuilder();
			String federalEIN = broker.getFederalEIN();
			sb.append("***-**-"+federalEIN.substring(federalEIN.length()- FOUR, federalEIN.length()));
			broker.setFederalEIN(sb.toString());			
		} 
	}

	/**
	 * Setting contact number to model object
	 *
	 * @param model
	 *            {@link Model}
	 * @param brokerObj
	 *            {@link Broker}
	 */
	private void getContactNumber(Model model, Broker brokerObj) {
		LOGGER.info("getContactNumber : START");

		if (brokerObj.getContactNumber() != null && brokerObj.getContactNumber().length()==TWELVE) {
			model.addAttribute(PHONE1, brokerObj.getContactNumber().substring(ZERO, THREE));
			model.addAttribute(PHONE2, brokerObj.getContactNumber().substring(FOUR, SEVEN));
			model.addAttribute(PHONE3, brokerObj.getContactNumber().substring(EIGHT, TWELVE));
		}else{
			LOGGER.debug("CertificationController -getContactNumber(): Unsupported format for the current Contact Number.");
		}

		if (brokerObj.getAlternatePhoneNumber() != null && brokerObj.getAlternatePhoneNumber().length()==TWELVE) {
			model.addAttribute(ALTERNATE_PHONE1, brokerObj.getAlternatePhoneNumber().substring(ZERO, THREE));
			model.addAttribute(ALTERNATE_PHONE2, brokerObj.getAlternatePhoneNumber().substring(FOUR, SEVEN));
			model.addAttribute(ALTERNATE_PHONE3, brokerObj.getAlternatePhoneNumber().substring(EIGHT, TWELVE));
		}

		if (brokerObj.getBusinessContactPhoneNumber() != null && brokerObj.getBusinessContactPhoneNumber().length()==TWELVE ) {
			model.addAttribute(BUSINESS_CONTACT_PHONE1, brokerObj.getBusinessContactPhoneNumber().substring(ZERO, THREE));
			model.addAttribute(BUSINESS_CONTACT_PHONE2, brokerObj.getBusinessContactPhoneNumber().substring(FOUR, SEVEN));
			model.addAttribute(BUSINESS_CONTACT_PHONE3, brokerObj.getBusinessContactPhoneNumber().substring(EIGHT, TWELVE));
		}
		if (brokerObj.getFaxNumber() != null && brokerObj.getFaxNumber().length()==TWELVE ) {
			model.addAttribute(FAX_NUMBER1, brokerObj.getFaxNumber().substring(ZERO, THREE));
			model.addAttribute(FAX_NUMBER2, brokerObj.getFaxNumber().substring(FOUR, SEVEN));
			model.addAttribute(FAX_NUMBER3, brokerObj.getFaxNumber().substring(EIGHT, TWELVE));
		}

		LOGGER.info("getContactNumber : END");
	}

	// @RequestMapping(value = "/certificationstatus", method =
	// RequestMethod.GET)
	// @PreAuthorize("hasPermission(#model, 'ASSIST_WRITE')")
	/**
	 * Displays certification status page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param showconfirmation
	 *            Flag to decide if confirmation alert to be shown
	 * @return URL for navigation certification status page
	 */
	@RequestMapping(value = "/broker/certificationstatus", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String certificationstatus(Model model, HttpServletRequest request,
			@RequestParam(value = "showconfirmation", required = false) boolean showconfirmation) throws GIRuntimeException {
		LOGGER.info("certificationstatus : START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Certification Status");
		Broker brokerObj = new Broker();
		try {
			
			decryptRequestParam(request);
			
			AccountUser user = userService.getLoggedInUser();

			brokerObj = retrieveBroker(request, user, false);

			agencyUtils.compareAgencyId(user, brokerObj);
			
			if (brokerObj != null) {

				model.addAttribute(CERTIFICATION_STATUS, brokerObj.getCertificationStatus());
			} else if (brokerObj == null) {
				brokerObj = new Broker();
				brokerObj.setUser(user);
			}

			model.addAttribute(BROKER, brokerObj);

			int paymentId = getPaymentInformation(request, brokerObj);

			model.addAttribute(PAYMENT_ID, paymentId);
			request.getSession().setAttribute(BROKER_OBJECT_FOR_LEFT_NAV, brokerObj);
			if (userService.hasUserRole(user, RoleService.AGENCY_MANAGER) || RoleService.AGENCY_MANAGER.equalsIgnoreCase(user.getActiveModuleName()) ){
				model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isAgencyPaymentEnabled());
				model.addAttribute("isAgencyManager", true);
				model.addAttribute("isAdminStaffL2", false);
				if(brokerObj.getUser() == null || brokerObj.getUser().getId() != user.getId()) {
					model.addAttribute("viewOtherBrokerInfo", true);
				}else {
					model.addAttribute("viewSelfBrokerInfo", true);
				}
			}else if (userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2) || RoleService.APPROVED_ADMIN_STAFF_L2.equalsIgnoreCase(user.getActiveModuleName()) ){
				model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isAgencyPaymentEnabled());
				model.addAttribute("isAgencyManager", false);
				model.addAttribute("isAdminStaffL2", true);
				if(brokerObj.getUser() == null || brokerObj.getUser().getId() != user.getId()) {
					model.addAttribute("viewOtherBrokerInfo", true);
				}else {
					model.addAttribute("viewSelfBrokerInfo", true);
				}
			}else{
				model.addAttribute("isAgencyManager", false);
				model.addAttribute("isAdminStaffL2", false);
				model.addAttribute(BrokerConstants.IS_PAYMENT_ENABLED, BrokerMgmtUtils.isPaymentEnabled());	
			}
			

			// Fix for issue HIX-14818: if certification status is pending &
			// showconfirmation is true then show Agent registration
			// confirmation pop-up else not
			if (brokerObj != null && brokerObj.getCertificationStatus() != null
					&& brokerObj.getCertificationStatus().equalsIgnoreCase(certification_status.Pending.toString())) {
				if (showconfirmation) {
					request.getSession().setAttribute(UPDATEPROFILE, TRUE);
				} else {
					request.getSession().setAttribute(UPDATEPROFILE, FALSE);
				}
			} else {
				request.getSession().setAttribute(UPDATEPROFILE, FALSE);
			}
			
			if(null == brokerObj.getUser()){
				//This condition is for newly created brokers (through agency) which doesn't have user account
				AccountUser acUser= new AccountUser();
				acUser.setFirstName(brokerObj.getFirstName());
				acUser.setLastName(brokerObj.getLastName());
				brokerObj.setUser(acUser);
			}
			
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch (InvalidUserException ex) {
			LOGGER.error("Invalid User Exception occurred in certificationstatus method", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}catch(GIRuntimeException giexception){
            LOGGER.error("GIRuntimeException occured in certificationstatus method while displaying certification status page : ", giexception);
            throw giexception;
    	 }catch(Exception exception){
            LOGGER.error("Exception occured while displaying certification status page : ", exception);
            throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("certificationstatus : END");

		return "broker/certificationstatus";
	}

	/**
	 * Method to retrieve Broker instance based on user's identifier or module identifier.
	 * 
	 * @param request The current HttpServletRequest instance.
	 * @param user The current AccountUser instance.
	 * @return brokerObj The Broker instance.
	 * @throws NumberFormatException The NumberFormatException instance.
	 */
	private Broker retrieveBroker(HttpServletRequest request, AccountUser user, boolean setActiveModuleId) {
		Broker brokerObj=null;
		
		LOGGER.warn(" IS_USER_SWITCH_TO_OTHER_VIEW: "+request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)
				+" : SWITCH_TO_MODULE_ID : "+request.getSession().getAttribute(SWITCH_TO_MODULE_ID)
				+" : IS_USER_SWITCH_TO_OTHER_VIEW : "+request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW));
		
//		LOGGER.warn(" Logged In User Role : "+user.getDefRole().getName());
		
		if (userService.hasUserRole(user, RoleService.AGENCY_MANAGER) || RoleService.AGENCY_MANAGER.equalsIgnoreCase(user.getActiveModuleName())) {
			Validate.notNull(user);
			if(null != request.getAttribute("newBrokerRegistration")){
				//Control comes here when AgencyMangercreates new agent.
				if(null != request.getAttribute("brokerId")){
					brokerObj = brokerService.getBrokerDetail( Integer.parseInt((String)request.getAttribute("brokerId")));
					
				}
			}else if(null != request.getAttribute("brokerId")){
				brokerObj = brokerService.getBrokerDetail( Integer.parseInt((String)request.getAttribute("brokerId")));
			}else{
				//Control comes here when AgencyManger-Agent is being created. 
				brokerObj = brokerService.findBrokerByUserId(user.getId());
			}
			
//			
//			if(setActiveModuleId && brokerObj != null && user!= null && user.getActiveModuleId()==0 ){
//				//user.setActiveModuleId(brokerObj.getId());
//			}
		}
		
		else if (userService.hasUserRole(user, RoleService.BROKER_ROLE)) {
			Validate.notNull(user);
			
			brokerObj = brokerService.findBrokerByUserId(user.getId());
			
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				if(null==brokerObj && user.getRecordId() != null){
					brokerObj = brokerService.getBrokerDetail(Integer.valueOf(user.getRecordId()));
					
					if (brokerObj!=null) {
						if(null== brokerObj.getUser()){
							AccountUser user1 = userService.findById(user.getId());
							brokerObj.setUser(user1);
							brokerService.saveBrokerWithLocation(brokerObj);
							
							persistModuleUser(user1, brokerObj);
						}
					} 
				}
			}
			
			if(setActiveModuleId && brokerObj != null && user!= null && user.getActiveModuleId()==0 ){
				user.setActiveModuleId(brokerObj.getId());
			}
		} else if (userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2) || RoleService.APPROVED_ADMIN_STAFF_L2.equalsIgnoreCase(user.getActiveModuleName())) {
			Validate.notNull(user);
			if(null != request.getAttribute("newBrokerRegistration")){
				//Control comes here when APPROVED_ADMIN_STAFF_L2 creates new agent.
				if(null != request.getAttribute("brokerId")){
					brokerObj = brokerService.getBrokerDetail( Integer.parseInt((String)request.getAttribute("brokerId")));
					
				}
			}else if(null != request.getAttribute("brokerId")){
				brokerObj = brokerService.getBrokerDetail( Integer.parseInt((String)request.getAttribute("brokerId")));
			}
		}else if(request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null
				&& request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null
				&& ((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase(Y)) {
			if(null != request.getAttribute("brokerId")){
				brokerObj = brokerService.getBrokerDetail( Integer.parseInt((String)request.getAttribute("brokerId")));
			}else {
				brokerObj = brokerService.getBrokerDetail(Integer.parseInt((String) request.getSession().getAttribute(
						SWITCH_TO_MODULE_ID)));
			}
			
		} else {
			throw new GIRuntimeException(null,"Invalid User", Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return brokerObj;
	}

	/**
	 * Displays broker settings
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to payment information page
	 */
	@RequestMapping(value = "/broker/settings", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String brokerSettings(Model model, HttpServletRequest request) {
		LOGGER.info("brokerSettings : START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Settings");
		Broker brokerObj = new Broker();
		try {
			AccountUser user = userService.getLoggedInUser();
			brokerObj = brokerService.findBrokerByUserId(user.getId());

			if (brokerObj != null) {

				model.addAttribute(CERTIFICATION_STATUS, brokerObj.getCertificationStatus());
			} else if (brokerObj == null) {
				brokerObj = new Broker();
				brokerObj.setUser(user);
			}

			model.addAttribute(BROKER, brokerObj);
		} catch (InvalidUserException ex) {
			LOGGER.error("Invalid User Exception occurred", ex);
		}
		LOGGER.info("brokerSettings : END");

		return "broker/paymentinfo";
	}

	/**
	 * Displays compaints feedback page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/broker/complaintsfeedback", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String complaintsfeedback(Model model, HttpServletRequest request) {
		LOGGER.info("complaintsfeedback : START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Complaints and Feedback");
		LOGGER.info("complaintsfeedback : END");
		return "broker/complaintsfeedback";
	}

	/**
	 * Saves complaint feedback
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/broker/complaintsfeedback", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
	public String complaintsFeedbackSave(@ModelAttribute(BROKER) Broker broker, BindingResult result, Model model,
			HttpServletRequest request) {

		LOGGER.info("complaintsFeedbackSave : START");

		LOGGER.info("complaintsFeedbackSave : END");

		return "redirect:/broker/complaintsfeedback";
	}

	/**
	 * Display certification contract page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/broker/certificationcontract", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_VIEW_BROKER)
	public String contract(Model model, HttpServletRequest request) {
		LOGGER.info("contract : START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Certification Contract");
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException ex) {
			LOGGER.error("User not logged in", ex);
			return "redirect:/account/user/login";
		}
		model.addAttribute("Date", new TSDate().toString());
		LOGGER.info("contract : END");
		return "broker/certificationcontract";
	}

	/**
	 * Saves certification contract page
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to certification status page
	 * @throws CloneNotSupportedException 
	 */
	@RequestMapping(value = "/broker/certificationcontract", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
	public String brokerContractSubmit(@ModelAttribute(BROKER) Broker broker, BindingResult result, Model model,
			HttpServletRequest request) throws CloneNotSupportedException {
		LOGGER.info("brokerContractSubmit : START");
		Broker brkObj = (Broker) broker.clone();
		try {
			AccountUser user = userService.getLoggedInUser();
			brkObj.setUser(user);
			Broker brokerObj = brokerService.findBrokerByUserId(user.getId());
			brokerObj.setEsignature(brkObj.getEsignature());
			brokerObj.setApplicationDate(brkObj.getApplicationDate());
			brokerObj.setApplicationDate(new TSDate());
			// model.addAttribute("broker", brokerObj);
			brokerService.saveBrokerCertification(brokerObj, null);
		} catch (InvalidUserException ex) {
			LOGGER.error("Exception occurred.", ex);
		}
		LOGGER.info("brokerContractSubmit : END");

		return "redirect:/broker/certificationstatus";
	}

	/**
	 * Displays broker renew page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/broker/certificationstatus", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_BROKER_ADD_BROKER)
	public String renewCertification(Model model, HttpServletRequest request) throws GIRuntimeException{
		LOGGER.info("renewCertification : START");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Certification Status Renew");
		Broker brokerObj = new Broker();
		try {
			AccountUser user = userService.getLoggedInUser();
			brokerObj = brokerService.findBrokerByUserId(user.getId());
			//brokerObj.setLastUpdatedBy(userService.getLoggedInUser().getId());
			model.addAttribute(BROKER, brokerObj);
			model.addAttribute(CERTIFICATION_STATUS, brokerObj.getCertificationStatus());
		} catch (InvalidUserException ex) {
			LOGGER.error("InvalidUserException occurred.", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		}catch(Exception exception){
            LOGGER.error("Exception occured while displaying broker renew page: ", exception);
            throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("renewCertification : END");
		return BROKER_RENEW;
	}
}
