<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<link rel="stylesheet" href="//cdn.rawgit.com/angular-ui/bower-ui-grid/master/ui-grid.min.css">
<style>
.ui-grid-pager-control input {
    height: 14px;
}
.ui-grid-pager-control .ui-grid-pager-max-pages-number {
    vertical-align: middle;
}
</style>

<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/angular-animate.1.5.6.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/angular-touch.1.5.6.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/ui-grid.min.4.0.2.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/csv.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/pdfmake.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/vfs_fonts.js" />'></script>

<script src="<gi:cdnurl value="/resources/js/lce/ui-bootstrap-tpls-0.11.0.js"/>"></script>
<script src="<gi:cdnurl value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js"/>"></script>
<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js"/>"></script>
<script src='<c:url value="/resources/js/reports/enrollment/cmsxmlfiletransmissionlog.js" />'></script> 

<div class="gutter10" ng-app="cmsXmlFileTransmissionLog">
	<div class="row-fluid">
		<h1>CMS XML File Transmission Logs</h1>
	</div>
	<div  ng-controller="CMSXMLFileTransmissionCtrl as vm">
		<div ui-grid="gridOptions" style="height: 575px" class="grid" ui-grid-pagination ui-grid-resize-columns ui-grid-exporter></div>
	</div>
	<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
</div>