(function(){
	_.templateSettings = {
		    interpolate: /\<\@\=(.+?)\@\>/gim,
		    evaluate: /\<\@(.+?)\@\>/gim
		};
	window.App = {};
	App.collections = {};
	App.models = {};
	App.views = {};
	App.backbone = {};
	
	// Defer initialization until doc ready.
	$(function(){
		var collection = new App.collections.Plans();
		var cartCollection = new App.collections.Carts();
		var planModel = new App.models.Plan();
		var favoriteModel = new App.models.Favorite();
		App.views.plans = new App.views.Plans({collection:collection,planModel:planModel,cartCollection:cartCollection,favoriteModel:favoriteModel});
		new App.views.Pagination({collection:collection});
		new App.views.CompareView({collection:collection});
		new App.views.SortView({collection:collection});
	});

})();