/**
 * viewValidAddressListNew is a javascript function. This performs AJAX call to validate address and refreshes the content on JSP.
 * 
 * <p> Address Modal gets openup only for matching records returned from actual web service.
 * 
 * Developer needs to invoke this from address section of JSP or modal by passing below params. 
 * <p> All are mandatory. 
 *  
 * @param address1, value in the form
 * @param address2, value in the form
 * @param city, value in the form
 * @param state, value in the form
 * @param zip, value in the form
 * 
 * @param model_address1, value fetched from appServer(db)
 * @param model_address2, value fetched from appServer(db)
 * @param model_city, value fetched from appServer(db)
 * @param model_state, value fetched from appServer(db)
 * @param model_zip, value fetched from appServer(db)
 * 
 * @param ids - Should be ids for address1, address2, city, state, zip fields from JSP and be ~ separated. 
 * 				Also there should be hidden fields ids for lat, lon, county and rdi.
 * 				see example:- address1_business~address2_business~city_business~state_business~zip_business~lat_business~lon_business~rdi_business~county_business
 */
function viewValidAddressListNew(address1, address2, city, state, zip, model_address1, model_address2, model_city, model_state, model_zip,ids) {
	var enteredAddress = address1+","+address2+","+city+","+state+","+zip;
		
	var imgpath = '/hix/resources/img/ajax_loader_blue_128.gif';
	
	$('#addressProgressPopup, .modal-backdrop').remove(); //remove wait popup
	/*lock the screen and inform user about validation 'Please wait... while we validate your address...'*/
	$('<div class="modal popup-address" id="addressProgressPopup"><div class="modal-header" style="border-bottom:0; "><h4 class="margin0">Please wait... while we validate your address...</h4></div><div class="modal-body center" style="max-height:470px; padding: 10px 0px;"><img src=\"' + imgpath + '\"></div></div>').modal({backdrop:"static",keyboard:false});
	
	if (!address1 || !city || !state || !zip){
		//console.log("conditional statement");

		$('#check-address-error').remove();
		$('#addressProgressPopup, .modal-backdrop').remove(); //remove wait popup
		//alert(" Inside !address1 || !city || !state || !zip " );
		var checkAddressError = '<div id="check-address-error" class="modal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-header clearfix"><h3 class="pull-left">Check Your Address</h3>'+
			'<button type="button" class="close closeModal">&times;</button></div>'+
			'<div class="modal-body"><div class="errorAddress">Please provide complete address.</div></div>'+
			'<div class="modal-footer"><input type="button" class="btn errorAddressBtn" aria-hidden="true" id="back_to_input_modal" value="OK" /></div></div><div class="modal-backdrop fade in"></div>';
		if($('#frmworksites').is(':visible')){
			$('#frmworksites').append(checkAddressError);
			$('#form-input-data').hide();
			$('#addressProgressPopup').remove(); //remove wait popup
		}else{
			$('body').append(checkAddressError);
			$('#addressProgressPopup').remove(); //remove wait popup
			$('#check-address-error').addClass('modal popup-address');
		}
		application.back_button_to_input_screen();
	}else{
		/*check whether model and page values are same. If same, donot call web service*/
		if (model_address1 == address1 && model_address2 == address2 && model_city == city && model_state == state && model_zip == zip){
			//no ChangeOfAddress, ignore this call...
			$('#addressProgressPopup, .modal-backdrop').remove();	//remove wait popup	
		}else{		
			$.ajax({
				url: "/hix/platform/validateaddressnew",
			    data: {	enteredAddress: enteredAddress, ids: ids},
			    
			    success: function(data){
			   		var href = '/hix/platform/address/viewvalidaddressnew';
			   		var retVal=data.split("~");
			   		var idxx=ids.split("~");
			   		$('#addressProgressPopup').modal('hide');//HIX-103363
			   		$('#addressProgressPopup').remove();	//remove wait popup
			   		$('#addressIFrame').remove(); //remove any present modal 
			   		
			   		//reset hidden information if any it holds....
			   		$('#'+idxx[5]).val(parseFloat(0.00));
		   			$('#'+idxx[6]).val(parseFloat(0.00));
		   			$('#'+idxx[7]).val("");
		   			$('#'+idxx[8]).val("");
			   		
		   			
			   		if(data=="SUCCESS"){
			   			//console.log('SUCCESS');
			   			$('#suggestion-box').remove();
			   			$('#form-input-data').hide();
			   			var successModalData = '<div id="suggestion-box" class="modal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-hidden="true">'+
			   			'<div class="modal-header clearfix"><h3 class="pull-left">Check Your Address</h3><button type="button" class="close closeModal">&times;</button></div>'+
			   				'<div class="modal-body"><div class="suggestedAddress"><iframe id="modalData" src="' + href + '" ></iframe></div></div></div><div class="modal-backdrop fade in"></div>';
			   			
			   			if($('#frmworksites').is(':visible')){
			   				$('#frmworksites').append(successModalData);
				   			application.iframeHeight();
				   			
				   			/*$('#modalData')[0].contentWindow.onload = function () {
				   				application.back_button_from_iframe(this);
				   			};*/
			   			}else{
			   				$('#check-address-error').remove();
			   				$('body').append(successModalData);
			   				$('#suggestion-box').addClass('modal popup-address');
			   			}
			   			application.iframeHeight();
			   			
			   			// $('#modalData')[0].contentWindow.onload = function () {
			   			// 	application.back_button_from_iframe(this);
			   			// 	application.modal_cross_button(this);
			   			// };
			   			application.back_button_to_input_screen();
			   			$('#addressProgressPopup').remove(); //remove wait popup
			   		}else if(data=="IGNORE"){
			   			//do nothing...
			   			//console.log('IGNORE');
			   		}else if(retVal[0]=="FAILURE"){
			   			//console.log('FAILURE');
			   			application.address_failure_message(retVal[1]);
			   			$('#addressProgressPopup').remove(); //remove wait popup
			   		}else if(retVal[0]=="MATCH_FOUND"){
			   			//console.log('MATCH FOUND aaaaa',retVal[1]);
			   			application.match_found_proper_address();
			   			//$('.modal-backdrop').hide();
			   			// As MATCH_FOUND, go ahead and update hidden information without opening address light-box....
			   			$('#'+idxx[5]).val(parseFloat(retVal[1]));
			   			$('#'+idxx[6]).val(parseFloat(retVal[2]));
			   			$('#'+idxx[7]).val(retVal[3]);
			   			$('#'+idxx[8]).val(retVal[4]);
						if($('.brokerAddressValidation').is(':visible')){
			   				application.remove_modal();
			   			}else if($('#frmeditemployee').is(':visible')){
			   				application.remove_modal();
			   			}else if($('#frmaddemployee').is(':visible')){
			   				application.remove_modal();
			   			}
			   		}
			    }
			});
		}
	}
	
}
