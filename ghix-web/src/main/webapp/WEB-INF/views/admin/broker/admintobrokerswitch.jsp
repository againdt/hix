<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 

<form name="dialogForm" id="dialogForm"	action="<c:url value="/admin/broker/dashboard" />" novalidate="novalidate">
 <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="broker"/>" />
 <input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${switchToModuleId}"/>" />
 <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${switchToResourceName}" />

	<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
		<div class="markCompleteHeader">
			<div class="header">
					<h4 class="margin0 pull-left"><spring:message code="label.ViewAgentAccount"/></h4>
					<button aria-hidden="true" data-dismiss="modal" id="crossClose"
						class="dialogClose" title="x" type="button">x</button>
			</div>
		</div>

		<div class="modal-body">
			<div class="control-group">
				<div class="controls">
					<spring:message code="label.bkr.AgentViewPopup"/> ${currSwitchToResourceFullName}.<br /> 
					<spring:message code="label.bkr.AgentSwitchPopup"/><br /> 
					<spring:message code="label.bkr.ProceedtoAgentView"/>
				</div>
			</div>
		</div>
		<div class="modal-footer clearfix">
			<input class="pull-left" type="checkbox" id="checkAgentView" name="checkAgentView">
			<div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/></div>
			<button class="btn btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.employee.cancle"/></button>
			<button class="btn btn-primary" type="submit"><spring:message code="label.bkr.agentView"/></button>

		</div>
	</div>
</form>