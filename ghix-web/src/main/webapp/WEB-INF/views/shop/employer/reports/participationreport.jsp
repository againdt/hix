<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="gutter10">
  <div class="row-fluid">
    <h1 id="skip">${employer.name }</h1>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <h4 class="graydrkbg">Participation Report</h4>
        <ul class="nav nav-list">
          <li><a href="<c:url value="summaryview"/>">Summary View</a></li>
          <li class="active"><a href="#">Detail View</a></li>
        </ul>
      </div>
      <!-- /.sidebar-->
      <div class="span9" id="sidebar">
        <div class="row-fluid">
          <div class="header"><h4 class="graydrkbg gutter10">Participation Report</h4></div>
        
          <c:if test="${errorMsg != ''}">
				<p></p>
				<div class="row-fluid"> 
					<div class="alert alert-danger">
							&nbsp;<p class="txt-center margin0"><c:out value="${errorMsg}"></c:out><p/>
						<br>
					</div>
				</div>
			</c:if>
		<c:if test="${infoMsg != ''}">
				<p></p>
				<div class="row-fluid"> 
					<div class="alert">
							&nbsp;<p class="txt-center margin0"><c:out value="${infoMsg}"></c:out><p/>
						<br>
					</div>
				</div>
			</c:if>
	 
			
          <c:if test="${errorMsg == '' }">
	         <!--   	<p class="txt-center margin0">${employer.name } Participation: ${employeedetails.participationrate}%</p> 
	            <div id="container" style="width:680px; height: 400px; margin: 0 auto"></div>-->
	            
	            
	             <div class="gutter10">
          	<div class="status_require_table">
          		<ul>
          			<li>
          				<div class="span7"><strong>Status</strong></div>
          				<div class="span4">${requirement_status }</div>
          			</li>
          			<li>
          				<div class="span7"><strong>${employer.name } Participation Rate</strong></div>
          				<div class="span4">${employeedetails.participationrate}%</div>
          			</li>
          			<li>
          				<div class="span7"><strong>Minimum Participation Required</strong></div>
          				<div class="span4">${participation_required }%</div>
          			</li>
          			<li>
          				<div class="span7"><strong>Eligible Employees</strong></div>
          				<div class="span4">${employeedetails.eligiblity}</div>
          			</li>
          			<li>
          				<div class="span7"><strong>Enrolled</strong></div>
          				<div class="span4">${employeedetails.enrolled}</div>
          			</li>
          			<li>
          				<div class="span7"><strong>Waived: Qualifying coverage</strong></div>
          				<div class="span4">${employeedetails.waived_qualified }</div>
          			</li>
          			<li>
          				<div class="span7"><strong>Waived: No qualified coverage</strong></div>
          				<div class="span4">${employeedetails.waived_unqualified }</div>
          			</li>
          			<li>
          				<div class="span7"><strong>No response</strong></div>
          				<div class="span4">${employeedetails.noresponse }</div>
          			</li>
          		</ul>
          	</div> 
	            	<div class="emp_enroll_table marginTop20">
          		<ul>
          			<li>
          				<div class="span7"><strong>Employee Name</strong></div>
          				<div class="span4"><strong>Enrollment Status</strong></div>
          			</li>

					<c:forEach var = "employeeApp" items ="${employeeAppList}" >
						<li>
          					<div class="span7">${employeeApp.name}</div>
          					<div class="span4">${employeeApp.statusNotes}</div>
          				</li>
					</c:forEach>
          		</ul>
          	</div>
            </c:if>  
          </div>
          <!--gutter10--> 
        </div>
        <!--row-fluid--> 
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<!--gutter10-->

