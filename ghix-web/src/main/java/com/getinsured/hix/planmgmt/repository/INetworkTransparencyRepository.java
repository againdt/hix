package com.getinsured.hix.planmgmt.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.NetworkTransparency;

/**
 *-----------------------------------------------------------------------------
 * HIX-84043 Load and persist Network transparency rating.
 *-----------------------------------------------------------------------------
 *
 * Repository is used to retrieve & persist data in Network Transparency table.
 * @author Bhavin Parmar
 * @since July 25, 2016
 */
public interface INetworkTransparencyRepository extends JpaRepository<NetworkTransparency, Integer> {

	@Query("SELECT nwTrans FROM NetworkTransparency nwTrans WHERE nwTrans.isDeleted='N' and nwTrans.hisoIssuerId =:hisoIssuerId and nwTrans.applicableYear =:applicableYear")
	List<NetworkTransparency> getAllNetworkTransparencyDataforIssuer(@Param("hisoIssuerId") String hisoIssuerId, @Param("applicableYear") Integer applicableYear);
}
