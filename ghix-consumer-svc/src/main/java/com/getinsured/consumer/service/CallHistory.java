package com.getinsured.consumer.service;

import com.getinsured.hix.consumer.history.ConsumerHistoryBase;



public class CallHistory extends ConsumerHistoryBase {
	private MainData mainData;
	private CallMetaData metadata;
	private String type;
	
	/**
	 * @param mainData
	 * @param metadata
	 * @param type
	 */
	public CallHistory(MainData mainData, CallMetaData metadata, String type) {
		super(metadata.getDate());
		this.mainData = mainData;
		this.metadata = metadata;
		this.type = type;
	}
	
	public CallHistory(){
		
	}
 
	public MainData getMainData() {
		return mainData;
	}

	public void setMainData(MainData mainData) {
		this.mainData = mainData;
	}

	public CallMetaData getMetadata() {
		return metadata;
	}

	public void setMetadata(CallMetaData metadata) {
		this.metadata = metadata;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
