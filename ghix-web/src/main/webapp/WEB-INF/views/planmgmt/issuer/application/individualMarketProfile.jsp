<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="row-fluid">
    <div class="gutter10">
    	<ul class="page-breadcrumb">
        	<li><a href="<c:url value="/planmgmt/issuer/profile/update/company"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.newIssuerApplication"/></a></li>
            <li><spring:message  code="pgheader.step"/> 4</li>
        </ul><!--page-breadcrumb ends-->
        <h1><issuerLogo:view issuerName="" /><spring:message  code="pgheader.newIssuerApplication"/></h1>
    </div>
</div>
		<div class="row-fluid">
        	<div class="gutter10">
                <div class="span3" id="sidebar">
                    <div class="header">
                        <h4 class="margin0"><spring:message  code="pgheader.steps"/></h4>
                    </div>
                    <!--  beginning of side bar -->
                    <ol class="nav nav-list nav-steps">
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.addRepresentative"/></a></li>
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.issuerApplication"/></a></li>
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.financialInformation"/></a></li>
                        <li class="active"><a href="#"><spring:message  code="pgheader.updateProfile"/></a></li>
                        <li class="list-style-none">
                            <ul class="sub-menu marginT-10">
                                <li><a href="#"><span class="subtask-completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.update.companyProfile"/></a></li>
                                <li><a href="#" class="submenu-active"><spring:message  code="pgheader.update.inidividualMarketProfile"/></a></li>
                                <li><a href="#"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                            </ul>
                        </li>
                        <li><a href="#"><spring:message  code="pgheader.signApplication"/></a></li>
                    </ol>
                    <!-- end of side bar -->
                </div>
                <!-- end of span3 -->
                <div class="span9" id="rightpanel">
                    <div class="header">
                        <h4 class="span10"><spring:message  code="pgheader.step"/> 4: <spring:message  code="pgheader.update.inidividualMarketProfile"/></h4>
                    </div>
                    <div class="gutter10">
            
                    <form class="form-horizontal" id="frmIssuerIndvdlMarktProfInfo" name="frmIssuerIndvdlMarktProfInfo" action="<c:url value="/planmgmt/issuer/profile/update/market/save" />" method="POST">
                        <input type="hidden" id="market" name="market" value="individual"/>
                        <input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/>
                        <df:csrfToken/>
                        <div class="control-group">
                            <label class="control-label" for="cust-serv-phone"><spring:message  code="label.customerServicePhone"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls span4">
                                <input type="text" name="customerServicePhone" id="customerServicePhone" class="input-large" value="${issuerObj.indvCustServicePhone}"/>
                                <div id="customerServicePhone_error" class=""></div>
                            </div>
                            <label class="control-label span2" for="cust-ext"><spring:message  code="label.phone.ext"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="customerServiceExt" id="customerServiceExt" class="input-small marginL10" value="${issuerObj.indvCustServicePhoneExt}"/>
                                <div id="customerServiceExt_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="cust-toll-free"><spring:message  code="label.customerServiceTollFreeNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="custServiceTollFreeNumber" id="custServiceTollFreeNumber" class="input-large" value="${issuerObj.indvCustServiceTollFree}"/>
                                <div id="custServiceTollFreeNumber_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="cust-serv-tty"><spring:message  code="label.customerServiceTTY"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="custServiceTTY" id="custServiceTTY" class="input-large" value="${issuerObj.indvCustServiceTTY}"/>
                                <div id="custServiceTTY_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="cust-website-url"><spring:message  code="label.consumerFacingWebSiteURL"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="facingWebSite" id="facingWebSite" class="input-large" value="${issuerObj.indvSiteUrl}"/>
                                <div id="facingWebSite_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="form-actions paddingLR20">
                            <a href='<c:url value="/planmgmt/issuer/profile/update/company"/>' class="btn"><spring:message  code="label.back"/></a>
                            <button type="submit" class="btn btn-primary"><spring:message  code="label.next"/></button>
                        </div>
                    </form>
                </div>
            </div><!--  end of span9 -->
        	</div>
		</div><!-- end row-fluid -->
<script type="text/javascript">
var validator = $("#frmIssuerIndvdlMarktProfInfo").validate({ 
		rules : {
			customerServicePhone : {required : false, number : true, minlength : 10, maxlength : 10},
			customerServiceExt : {required : false, number : true, maxlength : 11},
			custServiceTollFreeNumber : {required : false, number : true, minlength : 10, maxlength : 10},
			custServiceTTY : { required : false, number : true, minlength : 10, maxlength : 10},
			facingWebSite : {required : false, checkurl:true}
		},
		messages : {
			customerServicePhone : { required : "<span><em class='excl'>!</em><spring:message  code='err.customerServicePhone' javaScriptEscape='true'/></span>"},
			customerServiceExt: { required : "<span><em class='excl'>!</em><spring:message  code='err.customerServiceExt' javaScriptEscape='true'/></span>"},
			custServiceTollFreeNumber : { required : "<span><em class='excl'>!</em><spring:message  code='err.custServiceTollFreeNumber' javaScriptEscape='true'/></span>"},
			custServiceTTY : { required : "<span><em class='excl'>!</em><spring:message  code='err.custServiceTTY' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});
	
jQuery.validator.addMethod("checkurl", function(value, element) {
	//Should validate only if a value has been entered
	if(value == '') {
		return true;
	}
		// now check if valid url
		return /^(http:\/\/||www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/.test(value);
	}
);
</script>	
