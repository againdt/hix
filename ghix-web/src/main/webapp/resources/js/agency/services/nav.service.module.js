(function (){
    angular.module('navServiceModule',[])
    .service('NavService', function() {
        var agencyCertificationStatus = $("#agencyCertificationStatus").val();
        return {
            "SideNavOne" : [
                {
                    "name" : "Agency Information",
                    "link" : "accsetup.agencyInformation",
                    "disableLink": false,
                    "showOKicon": false
                },
                {
                    "name" : "Location and Hours",
                    "link" : "accsetup.locationHour",
                    "disableLink": true,
                    "showOKicon": false
                },
                {
                    "name" : "Agency Manager Information",
                    "link" : "./../hix/broker/certificationapplication",
                    "disableLink": true,
                    "showOKicon": false
                },
                {
                    "name" : "Public Profile",
                    "link" : "./../hix/broker/buildprofile",
                    "disableLink": true,
                    "showOKicon": false
                },
                {
                    "name" : "Document Upload",
                    "link" : "accsetup.upload",
                    "disableLink": true,
                    "showOKicon": false
                },
                {
                    "name" : "Certification Status",
                    "link" : "accsetup.certificationstatus",
                    "disableLink": true,
                    "showOKicon": false
                }

            ],
            "SideNavTwo" : [
                {
                    "name" : "Agency Information",
                    "link" : "accsetup.agencyInformation"
                },
                {
                    "name" : "Location and Hours",
                    "link" : "accsetup.locationHour"
                },
                {
                    "name" : "Document Upload",
                    "link" : "accsetup.upload"
                },
                {
                    "name" : "Certification Status",
                    "link" : "accsetup.certificationstatus"
                }
            ],
            "SideNavThree" : [
                {
                    "name" : "Agency Information",
                    "link" : "accsetup.agencyInformation"
                },
                {
                    "name" : "Location and Hours",
                    "link" : "accsetup.locationHour"
                },
                {
                    "name" : "Certification Status",
                    "link" : "accsetup.admincertificationstatus"
                }
            ],
            "SideNavFour" : [
                {
                    "name" : "Admin Staff Information",
                    "link" : "adminstaff.addstaff",
                    "showOKicon": false,
                    "disableLink": false
                },
                {
                    "name" : "Approval Status",
                    "link" : "adminstaff.approvalstatus",
                    "showOKicon": false,
                    "disableLink": true
                },
                {
                    "name" : "Status",
                    "link" : "adminstaff.status",
                    "showOKicon": false,
                    "disableLink": true
                }
            ],

            "CertificationStatus" : agencyCertificationStatus
        };
    });
})();
