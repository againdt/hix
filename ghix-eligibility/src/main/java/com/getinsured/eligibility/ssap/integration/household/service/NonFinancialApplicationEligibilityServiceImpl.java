package com.getinsured.eligibility.ssap.integration.household.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getinsured.eligibility.util.EligibilityConstants;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.at.resp.service.SsapVerificationService;
import com.getinsured.eligibility.at.resp.si.handler.helper.ApplicantEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.NativeAmericanEligibilityHelper;
import com.getinsured.eligibility.common.dto.SsapApplicationUpdateDTO;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.eligibility.ssap.integration.notices.SsapIntegrationNotificationService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;

@Component(value="nonFinancialApplicationEligibilityService")
public class NonFinancialApplicationEligibilityServiceImpl implements NonFinancialApplicationEligibilityService {
    
	private static final String NON_FINANCIAL_APPLICATION = "N";
	private static final String ELIBILITY_INDICATOR_TRUE = "TRUE";
    private static final String EXCHANGE_ELIGIBILITY_TYPE = "ExchangeEligibilityType";
    private static final String CSR_ELIGIBILITY_TYPE = "CSREligibilityType";
    private static final String ENROLLMENT_ALLOWED = "Y";
    private static final String APPLICANT_SEEKING_COVERAGE = "Y";
    private static final String APPLICANT_ON_APPLICATION = "Y";
    private static final Logger LOGGER = Logger.getLogger(NonFinancialApplicationEligibilityServiceImpl.class);
    private static final String ELIBILITY_INDICATOR_FALSE = "FALSE";
    private static final String ENROLLMENT_DISALLOWED = NON_FINANCIAL_APPLICATION;
    private static final String COVERAGE_START_DATE_STR = "01/01/2020 00:00:00";
    private static final String COVERAGE_END_DATE_STR = "12/31/2020 23:59:59";
    private static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
    private static final Date COVERAGE_END_DATE;
    private static final Date COVERAGE_START_DATE;
    
    

    
	@Value("#{configProp['nativeAmerican.default.cslevel'] != null ? configProp['nativeAmerican.default.cslevel'] : 'CS3'}")
	public String nativeAmericanHouseholdCSLevel;

    static { 
        try {
            COVERAGE_START_DATE = new SimpleDateFormat(DATE_FORMAT).parse((COVERAGE_START_DATE_STR));
            COVERAGE_END_DATE = new SimpleDateFormat(DATE_FORMAT).parse((COVERAGE_END_DATE_STR));
        } catch (java.text.ParseException e) {
            throw new GIRuntimeException("Unable to parse the coverage start and end date to be used for non-financial eligibility");
        }
    }
    
	@Autowired
    private NativeAmericanEligibilityHelper nativeAmericanEligibilityHelper;

    @Autowired
    private SsapApplicantRepository ssapApplicantRepository;

    @Autowired
    private SsapApplicationRepository ssapApplicationRepository;
    
    @Autowired
    private IEligibilityProgram eligibilityProgramRepository;

    
    @Autowired 
    private RestTemplate restTemplate;
    
    @Autowired 
    private SsapIntegrationNotificationService ssapIntegrationNotificationService; 

    @Autowired 
    private SsapVerificationService ssapVerificationService;
    
    @Autowired
    private ApplicantEligibilityHelper applicantEligibilityHelper;
    
    @Override
    public SsapApplicationUpdateDTO determineEligibility(SsapApplication ssapApplication) throws ParseException {

    	SsapApplicationUpdateDTO ssapApplicationUpdate =  null;
    	
        if(NON_FINANCIAL_APPLICATION.equals(ssapApplication.getFinancialAssistanceFlag())) {
            LOGGER.debug(new StringBuilder().append("NonFinancial application status service activator - checking status for application id: ").append(ssapApplication.getId()));

            ssapApplicationUpdate = updateApplicationStatus(ssapApplication);

            // Invoke NativeAmericanHelper to mark the application as Native American if all applicants are native american
            String hhNativeAmerican = nativeAmericanEligibilityHelper.process(ssapApplication.getCaseNumber());
            ssapApplicationUpdate.setNativeAmerican(hhNativeAmerican);
            
            if("Y".equals(ssapApplicationUpdate.getNativeAmerican())) {
            	ssapApplicationUpdate.setCsrLevel(nativeAmericanHouseholdCSLevel);
            } else {
            	ssapApplicationUpdate.setCsrLevel(null);
            }
            
            List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplication.getId());
            for(SsapApplicant ssapApplicant:ssapApplicants){
            	ssapApplicant.setCsrLevel(ssapApplicationUpdate.getCsrLevel());
            }
            ssapApplicantRepository.save(ssapApplicants);
        }
        
        return ssapApplicationUpdate;
    }

    
	@Override
    public boolean isVerificationDone(List<SsapApplicant> ssapApplicants, SingleStreamlinedApplication ssapJson) {
        boolean allApplicantsVerified = true;

        for(SsapApplicant applicant : ssapApplicants) {
        	HouseholdMember householdMember = getHouseholdMember(applicant.getApplicantGuid(), ssapJson);
			if(!ssapVerificationService.isApplicantVerified(NON_FINANCIAL_APPLICATION, applicant, householdMember)) {
        		allApplicantsVerified = false;
        	}
        }

        return allApplicantsVerified;
    }

    private HouseholdMember getHouseholdMember(String applicantGuid, SingleStreamlinedApplication ssapJson) {
        TaxHousehold household = ssapJson.getTaxHousehold().get(0);
        List<HouseholdMember> jsonApplicants = household.getHouseholdMember();
		HouseholdMember householdMember = null;
		
    	for (HouseholdMember member : jsonApplicants) {
			if(member.getApplicantGuid().equals(applicantGuid)) {
				householdMember = member;
			}
		}
		return householdMember;
	}


	@Override
    public void sendInitialEligibilityNotification(String caseNumber) {
        try {
            restTemplate.getForEntity(GhixEndPoints.EligibilityEndPoints.ELIGIBILITY_NOTIFICATION_URL + caseNumber, String.class);
        } catch (Exception exception) {
            LOGGER.error("Error invoking notification service for sending eligibility notifications", exception);
        } 
    }

    @Override
    public void sendUpdatedEligibilityNotification(String caseNumber) {
        try {
            restTemplate.getForEntity(GhixEndPoints.EligibilityEndPoints.ELIGIBILITY_NOTIFICATION_UPDATED_URL + caseNumber, String.class);
        } catch (Exception exception) {
            LOGGER.error("Error invoking notification service for sending eligibility notifications", exception);
        } 
    }
    

    @Override
    public void sendAdditionalInfoEligibilityNotification(String caseNumber) throws Exception {
        ssapIntegrationNotificationService.generateAdditionalInformationDocument(caseNumber);
    }
    
    private SsapApplicationUpdateDTO updateApplicationStatus(SsapApplication ssapApplication) throws ParseException {
        LOGGER.debug(new StringBuilder().append("NonFinancial application status application id: " + ssapApplication.getId()));
        Date currentDate = new TSDate();
        List<SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();
        SsapApplicationUpdateDTO ssapApplicationUpdate = new SsapApplicationUpdateDTO();
        List<EligibilityProgram> eligibilityProgramList = new ArrayList<EligibilityProgram>();
        
        boolean isHHNA = setNativeamericanStatus(ssapApplication);
        
        boolean isAnyoneSeekingCoverage = false;
        
    	String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

        Map<Long, Boolean> applicantIncarcerationSelfAttestation = getIncarcerationSelfAttestation(ssapApplication); 
        for(SsapApplicant applicant : ssapApplicants) {
            if(APPLICANT_SEEKING_COVERAGE.equals(applicant.getApplyingForCoverage()) && APPLICANT_ON_APPLICATION.equals(applicant.getOnApplication())) {
            	isAnyoneSeekingCoverage = true;
                boolean applicantIncarcerationSelfAttestationInd = false;
                if (applicantIncarcerationSelfAttestation.containsKey(applicant.getPersonId())) {
                    applicantIncarcerationSelfAttestationInd = applicantIncarcerationSelfAttestation.get(applicant.getPersonId());
                    if(applicantIncarcerationSelfAttestationInd) {
                        applicant.setEligibilityStatus(ExchangeEligibilityStatus.NONE.toString());
                    } else {
                    	if("ID".equals(stateCode)) {
                    		if(applicant.getEligibilityStatus()==null || !ExchangeEligibilityStatus.MEDICAID.toString().equalsIgnoreCase(applicant.getEligibilityStatus())){
                        		applicant.setEligibilityStatus(ExchangeEligibilityStatus.QHP.toString());
                        	}
                    	} else {
                    		applicant.setEligibilityStatus(ExchangeEligibilityStatus.QHP.toString());
                    	}
                    }
                }
                
                LOGGER.debug(new StringBuilder().append("Adding program eligibility for applicant id: ").append(applicant.getId()));

                List<EligibilityProgram> programs = eligibilityProgramRepository.getApplicantEligibilities(applicant.getId());
                if(null != programs && !programs.isEmpty()) {
                    // Update existing eligibilities if already present.
                    for(EligibilityProgram program : programs) {
                    	if("ID".equals(stateCode)) {
                    		if(applicant.getEligibilityStatus()==null || 
                    				!ExchangeEligibilityStatus.MEDICAID.toString().equalsIgnoreCase(applicant.getEligibilityStatus())){
                    			 program.setEligibilityType(isHHNA ? CSR_ELIGIBILITY_TYPE : EXCHANGE_ELIGIBILITY_TYPE);
                    			 program.setEligibilityIndicator(ELIBILITY_INDICATOR_TRUE);
                        	}
                    	} else {
                    		 program.setEligibilityType(isHHNA ? CSR_ELIGIBILITY_TYPE : EXCHANGE_ELIGIBILITY_TYPE);
                    		 program.setEligibilityIndicator(ELIBILITY_INDICATOR_TRUE);
                    	}
                       
                        // Set QHP Eligibility based on applicant's is self-attested incarceration status 
                    	if(applicantIncarcerationSelfAttestationInd){
                    		 program.setEligibilityIndicator(ELIBILITY_INDICATOR_FALSE);
                    	}
                        program.setEligibilityStartDate(COVERAGE_START_DATE);
                        program.setEligibilityDeterminationDate(currentDate);
                        program.setEligibilityEndDate(COVERAGE_END_DATE);
                        program.setSsapApplicant(applicant);
                        eligibilityProgramList.add(program);
                        eligibilityProgramRepository.save(program); 
                    }

                } else {
                    // Insert a new record if not already present.
                    EligibilityProgram eligibilityProgramDO = new EligibilityProgram();
                    eligibilityProgramDO.setEligibilityType(isHHNA ? CSR_ELIGIBILITY_TYPE : EXCHANGE_ELIGIBILITY_TYPE);
                    // Set QHP Eligibility based on applicant's is self-attested incarceration status 
                    eligibilityProgramDO.setEligibilityIndicator(applicantIncarcerationSelfAttestationInd ? ELIBILITY_INDICATOR_FALSE : ELIBILITY_INDICATOR_TRUE);
                    eligibilityProgramDO.setEligibilityStartDate(COVERAGE_START_DATE);
                    eligibilityProgramDO.setEligibilityDeterminationDate(currentDate);
                    eligibilityProgramDO.setEligibilityEndDate(COVERAGE_END_DATE);
                    eligibilityProgramDO.setSsapApplicant(applicant);
                    eligibilityProgramList.add(eligibilityProgramDO);
                    eligibilityProgramRepository.save(eligibilityProgramDO);
                    
                    if (isHHNA) {
                    	//HIX-115921 Adding exchange eligibility in case of ID Native American household
                    	if("ID".equals(stateCode)) {
                    		EligibilityProgram exchangeEligibilityProgramDO = new EligibilityProgram();
                    		exchangeEligibilityProgramDO.setEligibilityType(EXCHANGE_ELIGIBILITY_TYPE);
                    		// Set QHP Eligibility based on applicant's is self-attested incarceration status 
                    		exchangeEligibilityProgramDO.setEligibilityIndicator(applicantIncarcerationSelfAttestationInd ? ELIBILITY_INDICATOR_FALSE : ELIBILITY_INDICATOR_TRUE);
                    		exchangeEligibilityProgramDO.setEligibilityStartDate(COVERAGE_START_DATE);
                    		exchangeEligibilityProgramDO.setEligibilityDeterminationDate(currentDate);
                    		exchangeEligibilityProgramDO.setEligibilityEndDate(COVERAGE_END_DATE);
                    		exchangeEligibilityProgramDO.setSsapApplicant(applicant);
                    		eligibilityProgramList.add(exchangeEligibilityProgramDO);
                    		eligibilityProgramRepository.save(exchangeEligibilityProgramDO);
                    	}
                    }
                    
                }

            }
        }
        ssapApplicantRepository.save(ssapApplicants); 
        applicantEligibilityHelper.setSsapApplicationStartDate(ssapApplication, eligibilityProgramList);
        
        // Allow enrollment if any one of the applicants seeking coverage is not incarcerated
        if(enrollmentAllowed(applicantIncarcerationSelfAttestation)) {
            if(ApplicationStatus.SIGNED.getApplicationStatusCode().equals(ssapApplication.getApplicationStatus())) {
            	ssapApplicationUpdate.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
            }
            ssapApplicationUpdate.setEligibilityStatus(EligibilityStatus.CAE);
            ssapApplicationUpdate.setExchangeEligibilityStatus(extractEligibilityStatus(ssapApplicants));
            if("OE".equals(ssapApplication.getApplicationType())) {
            	ssapApplicationUpdate.setAllowEnrollment(ENROLLMENT_ALLOWED);
            }
        } else {
        	if(!isAnyoneSeekingCoverage && "SEP".equals(ssapApplication.getApplicationType())) {
        		ssapApplicationUpdate.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
        	} else {
        		ssapApplicationUpdate.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
        	}
        	
        	ssapApplicationUpdate.setEligibilityStatus(EligibilityStatus.DE);
        	ssapApplicationUpdate.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);
        	ssapApplicationUpdate.setAllowEnrollment(ENROLLMENT_DISALLOWED);
        }
        ssapApplicationUpdate.setEligibilityReceivedDate(currentDate);
        return ssapApplicationUpdate;
    }
    
    private ExchangeEligibilityStatus extractEligibilityStatus(List<SsapApplicant> ssapApplicants){
    	ExchangeEligibilityStatus exchangeEligibilityStatus = ExchangeEligibilityStatus.NONE;
    	Set<String> applicationEligibility = new HashSet<>();
    	 for(SsapApplicant applicant : ssapApplicants) {
    		 applicationEligibility.add(applicant.getEligibilityStatus());
    	 }
    	 
    	 if(applicationEligibility.contains(ExchangeEligibilityStatus.QHP.toString())){
    		 exchangeEligibilityStatus = ExchangeEligibilityStatus.QHP;
    	 } else if(applicationEligibility.contains(ExchangeEligibilityStatus.MEDICAID.toString())){
    		 exchangeEligibilityStatus = ExchangeEligibilityStatus.MEDICAID;
    	 }
    	 
    	 return exchangeEligibilityStatus;
    }
    
    private boolean enrollmentAllowed(Map<Long, Boolean> applicantIncarcerationSelfAttestation) {
        Collection<Boolean> values = applicantIncarcerationSelfAttestation.values();
        for (Boolean incarcerationSelfAttestation : values) {
            if(!incarcerationSelfAttestation) {
                return true;
            }
        }
        return false;
    }

    private Map<Long, Boolean> getIncarcerationSelfAttestation(SsapApplication ssapApplication) throws ParseException {
        Map<Long, Boolean> incarcerationSelfAttestation = new HashMap<>();
        JSONParser parser = new JSONParser();
        String ssapJson = ssapApplication.getApplicationData();
        JSONObject ssapJSONObject = (JSONObject) parser.parse(ssapJson); 
        JSONArray taxHouseholds = (JSONArray) ((JSONObject) ssapJSONObject
                .get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION))
                .get(SSAPIntegrationConstants.TAX_HOUSEHOLD);

            JSONObject household = (JSONObject) taxHouseholds.get(0);
            JSONArray householdMembers = (JSONArray) household.get(SSAPIntegrationConstants.HOUSEHOLD_MEMBER);
            for(Object householdMember : householdMembers) {
                JSONObject member = (JSONObject) householdMember;
                if(null != member.get("applyingForCoverageIndicator") && Boolean.valueOf(member.get("applyingForCoverageIndicator").toString())) {
                	/*  
                	 *  HIX-51958 - Pending Disposition Eligibility  
                	 *  First check if incarcerationStatusIndicator is true. If true then, also check if incarcerationPendingDispositionIndicator is true. 
                	 *  If incarcerationPendingDispositionIndicator is true, then in this case the applicant should not be treated as incarcerated.
                	 *  In this case hub verification would be done for this applicant.
                	 */ 
                	boolean incarcerationStatusIndicator = false;
                    JSONObject incarcerationStatus = (JSONObject) member.get("incarcerationStatus");
                    if(null != incarcerationStatus && Boolean.valueOf(incarcerationStatus.get("incarcerationStatusIndicator").toString())) {
                    	incarcerationStatusIndicator = true;
                    	Object incarcerationPendingIndicator = incarcerationStatus.get("incarcerationPendingDispositionIndicator");
						if(null != incarcerationPendingIndicator && Boolean.valueOf(incarcerationPendingIndicator.toString())) {
							incarcerationStatusIndicator = false;
						}
                    }
                    incarcerationSelfAttestation.put(Long.valueOf(member.get(SSAPIntegrationConstants.PERSON_ID).toString()), incarcerationStatusIndicator);
                }
            }
        return incarcerationSelfAttestation;
    }

    private boolean setNativeamericanStatus(SsapApplication ssapApplication) {
        boolean isHouseholdNativeAmerican = true;
        for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
            if ("Y".equals(applicant.getApplyingForCoverage()) && !"Y".equals(applicant.getNativeAmericanFlag())){
                isHouseholdNativeAmerican = false;
                break;
            }
        }
        return isHouseholdNativeAmerican;
    }


	@Override
	public boolean checkIfEligibilityIsToBeDenied(String caseNumber) throws Exception {
		Boolean eligibilityDenied = false;
		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);
		if(!ssapApplications.isEmpty()) {
			SsapApplication ssapApplication = ssapApplications.get(0);
			eligibilityDenied = enrollmentAllowed(getIncarcerationSelfAttestation(ssapApplication));
		}
		return eligibilityDenied;
	}

    @Override
    public String updateEligibilityStatus(SsapApplication ssapApplication, SingleStreamlinedApplication ssapJSON) {

            if(EligibilityStatus.CAE.equals(ssapApplication.getEligibilityStatus()) || EligibilityStatus.PE.equals(ssapApplication.getEligibilityStatus())){
                if(isVerificationDone(ssapApplication.getSsapApplicants(), ssapJSON)) {
                    ssapApplication.setEligibilityStatus(EligibilityStatus.AE);
                    ssapApplication.setEligibilityReceivedDate(new TSDate());
                    ssapApplicationRepository.save(ssapApplication);
                    return EligibilityConstants.SUCCESS;
                }else {
                    ssapApplication.setEligibilityStatus(EligibilityStatus.CAE);
                    ssapApplication.setEligibilityReceivedDate(new TSDate());
                    ssapApplicationRepository.save(ssapApplication);
                    return EligibilityConstants.SUCCESS;
                }
            }

            return EligibilityConstants.FAILURE;
        }
}
