/**
 * 
 */
package com.getinsured.hix.entity.enrollmententity.history;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;

/**
 * @author dhumal_m
 * 
 */
@Service
public class EnrollmentEntityHistoryRendererImpl implements HistoryRendererService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEntityHistoryRendererImpl.class);

	public static final int UPDATE_DATE = 1;
	public static final int PREVIOUS_STATUS = 2;
	public static final int NEW_STATUS = 3;
	public static final int COMMENTS = 4;
	public static final int ATTACHMENT = 5;
	public static final int DATE = 1;
	public static final int FILE_NAME = 2;
	public static final int REMOVE = 3;
	public static final String TIMESTAMP_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String UPDATED_DATE_COL = "updatedOn";
	public static final String COMMENT_COL = "comments";
	public static final String ATTACHMENT_COL = "documentId";
	public static final String DOCUMENTID_EE = "documentIdEE";
	public static final String STATUS_COL = "registrationStatus";
	public static final String PREVIOUS_STATUS_COL = "previousRegistrationStatus";
	public static final String NEW_STATUS_COL = "newRegistrationStatus";
	public static final String REPOSITORY_NAME = "IEnrollmentEntityRepository";
	public static final String MODEL_NAME = "com.getinsured.hix.model.entity.EnrollmentEntity";
	public static final String APOSTROPHE_STRING = "apostrophes";
	public static final String APOSTROPHE = "'";
	public static final String NO_COMMENTS = "No Comments";
	private static final String COMMENT_TEXT_PRE = "<a onclick=\"getComment('";
	private static final String COMMENT_TEXT_POST = "');\" href='#modalBox' data-toggle='modal'>View Comment</a>";
	public static final String HASH_SEP = "#";
	public static final String LINE_SEP = "line.separator";

	/**
	 * Process data to view in audit table
	 */
	@Override
	public List<Map<String, Object>> processData(List<Map<String, String>> data, List<String> compareColumns,
			List<Integer> columnsToDisplay) {
		
		LOGGER.info("processData: START");

		List<Map<String, String>> orderedData = new ArrayList<Map<String, String>>(data);
		SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_PATTERN1);
		Collections.sort(orderedData, new Comparator<Map<String, String>>() {

			@Override
			public int compare(Map<String, String> map1, Map<String, String> map2) {
				String lastUpdateTime1 = map1.get(UPDATED_DATE_COL); 
				String lastUpdateTime2 = map2.get(UPDATED_DATE_COL);
				if(lastUpdateTime1 == null) {
					return 1;
				}
				if(lastUpdateTime2 == null) {
					return -1;
				}
				Date lastUpdateTimeStamp1 = null;
				Date lastUpdateTimeStamp2 = null;
				try {
					lastUpdateTimeStamp1 = sdf.parse(lastUpdateTime1); 
					lastUpdateTimeStamp2 = sdf.parse(lastUpdateTime2);
				} catch (ParseException e) {}
				
				if(lastUpdateTimeStamp1 !=null &&  lastUpdateTimeStamp2 !=null ) {
					return lastUpdateTimeStamp2.compareTo(lastUpdateTimeStamp1); 
				}else {
					return lastUpdateTime2.compareTo(lastUpdateTime1); 
				}
				
			}
		});
		int size = orderedData.size();
		Map<String, String> firstElement = null;
		Map<String, String> secondElement = null;
		boolean allSameCertificationStatusRecords = true;
		List<Map<String, Object>> processedData = new ArrayList<Map<String, Object>>();
		{
			firstElement = orderedData.get(0);
		}
		for (int i = 0; i < size - 1; i++) {
			firstElement = orderedData.get(i);
			secondElement = orderedData.get(i + 1);
			for (String keyColumn : compareColumns) {
				/*
				 * to incorporate same status record in Certification history
				 * when comments added
				 */
				if (!isEqual(firstElement, secondElement, keyColumn)) {

					processedData.add(formMap(firstElement, secondElement, columnsToDisplay));
					allSameCertificationStatusRecords = false;

				} else {
					if (!firstElement.get(COMMENT_COL).isEmpty()) {
						processedData.add(formMap(firstElement, secondElement, columnsToDisplay));
						allSameCertificationStatusRecords = false;
					}
				}

			}

		}

		if (size == 1 || allSameCertificationStatusRecords) {
			
				processedData.add(formMap(firstElement, firstElement, columnsToDisplay));
			
		}
		 if(null != processedData && processedData.size() > 1 && hasDuplicateRecords(processedData)) {
	 	 	 	
			 processedData = filterDuplicatesRecords(processedData);
		

	               }
		
		LOGGER.info("processData: END");
		
		return processedData;
	}
	
	/**
     * Method to check if the processed data collection has duplicates or not.
	     * 
         * @param origProcessedData The original un-filtered collection.
         * @return hasDuplicate The duplicate presence flag.
         */
    private boolean hasDuplicateRecords(List<Map<String, Object>> origProcessedData) {
	                boolean hasDuplicate = false;
	                Map<String, Object> firstElem = null;
	                Map<String, Object> secondElem = null;
	                
	                for(int i=0; (i<origProcessedData.size()) && (i+1 < origProcessedData.size()); i++) {
	                        firstElem = origProcessedData.get(i);
	                        secondElem = origProcessedData.get(i + 1);
	                        
	                        if(((firstElem.get(PREVIOUS_STATUS_COL).equals(secondElem.get(PREVIOUS_STATUS_COL))) && 
	            					(firstElem.get(NEW_STATUS_COL).equals(secondElem.get(NEW_STATUS_COL))) &&	
	            					((null != firstElem.get(COMMENT_COL) && null != secondElem.get(COMMENT_COL) && firstElem.get(COMMENT_COL).equals(secondElem.get(COMMENT_COL))) ||
	            							(null == firstElem.get(COMMENT_COL) && null == secondElem.get(COMMENT_COL))) && 
	            						((null != firstElem.get(ATTACHMENT_COL) && null != secondElem.get(ATTACHMENT_COL) && firstElem.get(ATTACHMENT_COL).equals(secondElem.get(ATTACHMENT_COL))) ||
	            								(null == firstElem.get(ATTACHMENT_COL) && null == secondElem.get(ATTACHMENT_COL)))) || (firstElem.get(PREVIOUS_STATUS_COL).equals(firstElem.get(NEW_STATUS_COL)) && 
	            										 (((null == firstElem.get(COMMENT_COL) && null == secondElem.get(COMMENT_COL)) || 
	            												 (null != firstElem.get(COMMENT_COL) && null != secondElem.get(COMMENT_COL) && firstElem.get(COMMENT_COL).equals(secondElem.get(COMMENT_COL)))) || 
	            												 	((null == firstElem.get(ATTACHMENT_COL) && null == secondElem.get(ATTACHMENT_COL)) || 
	            												 		(null != firstElem.get(ATTACHMENT_COL) && null != secondElem.get(ATTACHMENT_COL) && firstElem.get(ATTACHMENT_COL).equals(secondElem.get(ATTACHMENT_COL)))))) ) {
	                                hasDuplicate = true;
	                                break;
                     }
             }
	                
	                return hasDuplicate;
	        }
    
    /**
         * Filter duplicate resultant status records.
         * 
         * @param processedData The non-filtered processed data collection.
         * @return filteredData The non-filtered processed data collection.
         */
        private List<Map<String, Object>> filterDuplicatesRecords(
                        List<Map<String, Object>> processedData) {
                do {
                        List<Map<String, Object>> filteredData = new ArrayList<Map<String, Object>>();
                        Map<String, Object> firstElem = null;
                        Map<String, Object> secondElem = null;
                        int i = 0;
                        
                        while((i<processedData.size()) && (i+1 < processedData.size())) {
                                firstElem = processedData.get(i);
                                secondElem = processedData.get(i + 1);
                                
                                if(firstElem.get(PREVIOUS_STATUS_COL).equals(firstElem.get(NEW_STATUS_COL)) && 
               						 (((null == firstElem.get(COMMENT_COL) && null == secondElem.get(COMMENT_COL)) || 
               								 (null != firstElem.get(COMMENT_COL) && null != secondElem.get(COMMENT_COL) && firstElem.get(COMMENT_COL).equals(secondElem.get(COMMENT_COL)))) || 
               								 	((null == firstElem.get(ATTACHMENT_COL) && null == secondElem.get(ATTACHMENT_COL)) || 
               								 		(null != firstElem.get(ATTACHMENT_COL) && null != secondElem.get(ATTACHMENT_COL) && firstElem.get(ATTACHMENT_COL).equals(secondElem.get(ATTACHMENT_COL))))) ) {
               					//Ignore redundant record
               					i = i + 1;
               				}
               				else if((firstElem.get(PREVIOUS_STATUS_COL).equals(secondElem.get(PREVIOUS_STATUS_COL))) && 
               						(firstElem.get(NEW_STATUS_COL).equals(secondElem.get(NEW_STATUS_COL))) &&	
               						((null != firstElem.get(COMMENT_COL) && null != secondElem.get(COMMENT_COL) && firstElem.get(COMMENT_COL).equals(secondElem.get(COMMENT_COL))) ||
               								(null == firstElem.get(COMMENT_COL) && null == secondElem.get(COMMENT_COL))) && 
               							((null != firstElem.get(ATTACHMENT_COL) && null != secondElem.get(ATTACHMENT_COL) && firstElem.get(ATTACHMENT_COL).equals(secondElem.get(ATTACHMENT_COL))) ||
               									(null == firstElem.get(ATTACHMENT_COL) && null == secondElem.get(ATTACHMENT_COL)))) {
                                        filteredData.add(firstElem);
                                        i = i + 2;
                                }
                                else {
                                        filteredData.add(firstElem);
                                        i = i + 1;
                                }                               
                        } 
                        
                        if(((i+1) == processedData.size())) {
                                filteredData.add(processedData.get(i));
                        }
                        
                        processedData = filteredData;
                } while(hasDuplicateRecords(processedData));
                
                return processedData;
        }
        
	/**
	 * Compares values of maps firstElement and secondElement for key keyColumn.
	 */
	private boolean isEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		
		return firstElement.get(keyColumn).equals(secondElement.get(keyColumn));
	}

	/**
	 * Forms map for given key. The newly formed map contains key-value pairs
	 * based on the list provided as columnsToDisplay.
	 */
	private Map<String, Object> formMap(Map<String, String> firstMap, Map<String, String> secondMap,
			List<Integer> columnsToDisplay) {
		
		LOGGER.info("formMap: START");
		
		Map<String, Object> values = new HashMap<String, Object>();
		for (int colId : columnsToDisplay) {
			switch (colId) {
			case UPDATE_DATE:
				String dt = firstMap.get(UPDATED_DATE_COL);
				SimpleDateFormat sdf = new SimpleDateFormat();
				sdf.applyPattern(TIMESTAMP_PATTERN1);
				try {
					values.put(UPDATED_DATE_COL, sdf.parse(dt));
				} catch (ParseException e) {
					LOGGER.error("Unable to parse Updated Date " + dt + " " , e);
				}
				break;

			case PREVIOUS_STATUS:
				String previousStatus = secondMap.get(STATUS_COL);
				values.put(PREVIOUS_STATUS_COL, previousStatus);
				break;

			case NEW_STATUS:
				String newStatus = firstMap.get(STATUS_COL);
				values.put(NEW_STATUS_COL, newStatus);
				break;

			case COMMENTS:
				updateMapForComment(firstMap, values);
				break;
			
			case ATTACHMENT:
				String doc = firstMap.get(ATTACHMENT_COL);
				if (!EntityUtils.isEmpty(doc)) {
				    values.put(ATTACHMENT_COL, firstMap.get(ATTACHMENT_COL));
				} else {
				    values.put(ATTACHMENT_COL, null);
				}
				break;
			    }
				
			}

		LOGGER.info("formMap: END");
		
		return values;
	}

	/**
	 * @param firstMap
	 * @param values
	 */
	private void updateMapForComment(Map<String, String> firstMap,
			Map<String, Object> values) {
		String comments = firstMap.get(COMMENT_COL);
		if (comments != null && comments.length() > 0) {
			comments = comments.replace(System.getProperty(LINE_SEP), HASH_SEP);

			String cmnt = comments.replace(APOSTROPHE, APOSTROPHE_STRING);

			cmnt = StringEscapeUtils.escapeHtml(cmnt);
			values.put(COMMENT_COL, COMMENT_TEXT_PRE + cmnt + COMMENT_TEXT_POST);
		}

		else {
			values.put(COMMENT_COL, NO_COMMENTS);
		}
	}


	/**
	 * process Data to arrange in List<Map<String, Object>>
	 */
	public List<Map<String, Object>> processDataForDocument(List<Map<String, String>> entityHistoryData,
			List<String> compareCols, List<Integer> displayCols) {
		
		LOGGER.info("processDataForDocument: START");
		
		List<Map<String, String>> orderedData = new ArrayList<Map<String, String>>(entityHistoryData);
		Collections.reverse(orderedData);
		int size = orderedData.size();
		Map<String, String> firstElement = null;

		List<Map<String, Object>> processedData = new ArrayList<Map<String, Object>>();
		{
			firstElement = orderedData.get(0);
		}
		for (int i = 0; i < size; i++) {
			firstElement = orderedData.get(i);
			for (String keyColumn : compareCols) {
				processedData.add(formMapForDocument(firstElement, displayCols));
			}

		}
		
		LOGGER.info("processDataForDocument: END");

		return processedData;
	}

	/**
	 * Forms map for given key. The newly formed map contains key-value pairs
	 * based on the list provided as columnsToDisplay.
	 */
	private Map<String, Object> formMapForDocument(Map<String, String> firstElement, List<Integer> displayCols) {
		
		LOGGER.info("formMapForDocument: START");
		
		Map<String, Object> values = new HashMap<String, Object>();
		for (int colId : displayCols) {
			switch (colId) {
			case DATE:
				String dt = firstElement.get("Date");
				SimpleDateFormat sdf = new SimpleDateFormat();
				sdf.applyPattern(TIMESTAMP_PATTERN1);
				try {
					values.put("Date", sdf.parse(dt));
				} catch (ParseException e) {
					LOGGER.error("Unable to parse Updated Date " + dt + " " , e);
				}
				break;
			case FILE_NAME:
				String fileName = firstElement.get("FileName");
				values.put("FileName", fileName);
				break;

			case REMOVE:
				String documentIdEE = firstElement.get("remove");
				values.put("documentIdEE", documentIdEE);
				break;
			}

		}

		LOGGER.info("formMapForDocument: END");
		
		return values;
	}

}
