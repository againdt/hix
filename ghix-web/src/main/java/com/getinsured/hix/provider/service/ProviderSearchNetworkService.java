package com.getinsured.hix.provider.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.Facility;
import com.getinsured.hix.model.FacilityAddress;
import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Physician;
import com.getinsured.hix.model.PhysicianAddress;
import com.getinsured.hix.model.Provider;
import com.getinsured.hix.model.ProviderNetwork;
import com.getinsured.hix.model.Specialty;

public interface ProviderSearchNetworkService {

	boolean uploadFile(String fileLocation, MultipartFile mfile, String fileName);

	/* Facilities services */

	List<List> getFacilities(String filePath);

	List<FacilityAddress> getFacilitiesAddress(String filePath);

	Facility getFacilityByDataSourceRefId(String dataSourceRefId, int networkId)
			throws Exception;

	List<Specialty> getFacilitiesSpecialty(String filePath);

	FacilityAddress getFacilityAddress(String county, String address,
			int facilityId) throws Exception;

	boolean isFacilityAddressExists(String county, String address,
			int facilityId) throws Exception;

	void saveFacilities(List<Facility> facilities) throws Exception;

	Facility saveFacility(Facility facility) throws Exception;

	FacilityAddress saveFacilityAddress(FacilityAddress facilityAddress)
			throws Exception;

	/* Physicians services */

	Physician getPhysicianByDataSourceRefId(String dataSourceRefId,
			int networkId) throws Exception;

	List<List> getPhysicians(String filePath);

	List<PhysicianAddress> getPhysiciansAddress(String filePath);

	List<Specialty> getPhysiciansSpecialty(String filePath);

	boolean isPhysicianAddressExists(String county, String address,
			int physicianId) throws Exception;

	PhysicianAddress getPhysicianAddress(String county, String address,
			int physicianId) throws Exception;

	void savePhysicians(List<Physician> physicians);

	Physician savePhysician(Physician physician) throws Exception;

	PhysicianAddress savePhysicianAddress(PhysicianAddress physicianAddress)
			throws Exception;

	/* Provider services */

	Provider getProvider(String type, String name, String datasourceRefId)
			throws Exception;

	boolean isProviderExists(String type, String name, String datasourceRefId)
			throws Exception;

	void saveProvider(Provider provider) throws Exception;

	/* Provider Network services */

	void saveProviderNetwork(ProviderNetwork providerNetwork) throws Exception;

	/* Network services */

	List<Network> getAllProviderNetwork();

	boolean isNetworkExists(String type, String netName, int issuerId)
			throws Exception;

	Network getNetwork(int id) throws Exception;

	Network saveNetwork(Network network) throws Exception;

	List<Network> getProviderNetworkByIssuerId(int issuerId);
	
	Page<Network> getProviderNetworkByIssuerId(int issuerId, Pageable pageable);
	
	void deleteInBatch(List<Network> entities);
	
	/* Specialty services */

	Specialty getSpecialty(String name, String groupName) throws Exception;

	boolean isSpecialtyExists(String name, String groupName) throws Exception;

	Specialty saveSpecialty(Specialty specialty) throws Exception;
	
	Object[] getProvNetDetailsById(int providerNetworkId);
	
	Specialty getSpecialtyByCode(String specialtyCode);
	
	List<Network> getNetworkByNetworkId(String networkId, String hiosId);

}
