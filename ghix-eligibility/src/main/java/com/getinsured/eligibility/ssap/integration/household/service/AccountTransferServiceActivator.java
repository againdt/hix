package com.getinsured.eligibility.ssap.integration.household.service;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;

@Component(value="accountTransferServiceActivator")
public class AccountTransferServiceActivator {

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountTransferServiceActivator.class);

	@SuppressWarnings("rawtypes")
	@ServiceActivator
	public Message receive(Message<String> message) throws UnsupportedEncodingException, ParseException {
		String payload = message.getHeaders().get(SSAPIntegrationConstants.SSAP_JSON_HEADER).toString();
		Map<String, String> rulePayload = new HashMap<String, String>();
		rulePayload.put("flowAction", "callState");
		rulePayload.put("load", payload);
		Message<Map<String, String>> ruleMessage = MessageBuilder.withPayload(rulePayload).setHeader("Content-Type", "application/x-www-form-urlencoded").build();
		LOGGER.debug(">>>>>>>>> Sending to account transfer - " + payload);
		return ruleMessage;
	}

}
