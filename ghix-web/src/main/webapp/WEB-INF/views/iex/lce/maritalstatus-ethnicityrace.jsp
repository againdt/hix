<script type="text/ng-template" id="maritalstatus-ethnicityrace.jsp">
  <div class="gutter10" ng-controller="ethnicityRace"  ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
    <h3><spring:message code="label.lce.ethnicityandrace"/></h3>

    <div class="alert alert-info margin20-t">
      <p><spring:message code="label.lce.ethp1"/></p>
    </div>
    <form class="form-horizontal" name="ethnicityraceForm" novalidate>
      <div id="hispaniclatinospanish">
        <div>
            <spring:message code="label.lce.is"/>
            <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
            <spring:message code="label.lce.ofhispanic"/>
        </div>
        <div class="margin10-l">
          <label class="radio-inline" for="ethnicityYes">
            <input type="radio" id="ethnicityYes" name="ethnicityRadioBtn"  value="Yes" ng-value="true" ng-model="householdMember.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator"> <spring:message code="label.lce.yes"/>
          </label>
          <label class="radio-inline" for="ethnicityNo">
            <input type="radio" id="ethnicityNo" name="ethnicityRadioBtn"  value="No" ng-value="false" ng-model="householdMember.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator" ng-click="uncheckAllEthnicities();"> <spring:message code="label.lce.no"/>
              </label>
        </div>
        <div class="margin10-l">
          <span class="validation-error-text-container" ng-if="submitted && ethnicityraceForm.ethnicityRadioBtn.$error.required"><spring:message code="label.lce.selectvalidoption"/> </span>
         </div>
      </div><!-- Is spouse Hispanic, Latino, or Spanish origin?  -->

      <!-- Ethnicity -->
      <div ng-if="householdMember.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator" id="ethnicity" class="margin30-t">
        <div>
          <spring:message code="label.lce.ethnicity"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
          <spring:message code="label.lce.checkallthatapply"/>
        </div>
        <div class="margin10-l" ng-repeat="ethnicity in ethnicities">
          <label class="checkbox inline">
            <input type="checkbox" id="ETHNICITY{{$index}}" name="ethincities" class="native" ng-checked="ethnicity.checked" ng-required="householdMember.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator && householdMember.ethnicityAndRace.ethnicity.length==0" ng-click="updateEthnicity(ethnicity)" ng-model="ethnicity.value"  ng-true-value="{{ethnicity.label}}"/> {{ethnicity.label}}
          </label>
        </div>

        <div class="margin10-l margin10-t" ng-if="ethnicities[3].value == 'Other'">
          <label for="otherEthnicityText">
			<span class="aria-hidden">otherEthnicityText text field</span>
            <input type="text" id="otherEthnicityText" name="otherEthnicityText" ng-model="tempEthnicityHolder" ng-required="ethnicities[3].value == 'Other'" class="span4" placeholder="<spring:message code='label.lce.enterotherethnicity'/>" onchange="updateOtherEthnicity();">
          </label>
        </div>
        <!-- Error -->
        <div class="margin30-l margin10-t">
          <div class="validation-error-text-container" ng-if="submitted && ethnicityraceForm.ethincities.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></div>
          <div class="validation-error-text-container" ng-if="submitted && ethnicityraceForm.otherEthnicityText.$error.required"><spring:message code="label.lce.error1"/></div>
        </div>
      </div><!-- Ethnicity -->

      <!-- Race -->
      <div id="race" class="margin30-t">
        <div>Race (check all that apply)</div>
        <div class="margin10-l" ng-repeat="race in races" >
          <label class="checkbox inline">
            <input type="checkbox"  name="{{$index}}"  id="RACE{{$index}}" ng-checked="race.checked"  ng-click="updateRace(race)" ng-model="race.value" ng-true-value="{{race.label}}"/> {{race.label}}
          </label>
        </div>
        <div class="margin10-l margin10-t" ng-if="races[14].value == 'Other'">
          <label for="otherRaceText">
			<span class="aria-hidden">otherRaceText text field</span>
              <input type="text" id="otherRaceText" ng-model="tempRaceHolder" name="otherRaceText" ng-required="races[14].value == 'Other'" onchange="updateOtherRace();" class="span4" placeholder="<spring:message code='label.lce.enterotherrace'/>">
            </label>
        </div>
        <!-- Error -->
        <div class="margin30-l margin10-t">
          <div class="validation-error-text-container" ng-if="submitted && ethnicityraceForm.otherRaceText.$error.required"><spring:message code='label.lce.error2'/></div>
        </div>
      </div><!-- Race -->
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a role="button" tabindex="0" ng-if="ethnicityraceForm.$valid" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a role="button" tabindex="0" ng-if="!ethnicityraceForm.$valid" class="btn btn-primary" ng-click="$parent.submitted=true" ><spring:message code="label.lce.next"/></a>
    </dl>

  </div>
</script>

<script>
  $('a.btn-primary').attr('tabindex', '0');
  var updateOtherEthnicity = function() {
    var scope = angular.element($("#ethnicityYes")).scope();
    var loopVar=0;
     for(loopVar=0  ; loopVar <  scope.selectedEthnicities.length ; loopVar++){
       if(scope.selectedEthnicities[loopVar].code === '000-0'){
         scope.selectedEthnicities[loopVar].label=$('#otherEthnicityText').val();
       }
     }
  };
  var updateOtherRace= function() {
    var scope = angular.element($("#ethnicityYes")).scope();
    var loopVar=0;
     for(loopVar=0  ; loopVar <  scope.selectedraces.length ; loopVar++){
       if(scope.selectedraces[loopVar].code === '000-0'){
         scope.selectedraces[loopVar].label=$('#otherRaceText').val();
       }
     }
  };
</script>