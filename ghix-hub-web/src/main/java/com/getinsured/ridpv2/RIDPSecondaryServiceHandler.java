package com.getinsured.ridpv2;


import javax.xml.bind.JAXBElement;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.RequestPayloadType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ResponsePayloadType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.SecondaryRequestInformationType;
import com.getinsured.ridpv2.wrapper.RIDPSecondaryRequestWrapper;
import com.getinsured.ridpv2.wrapper.ResponsePayloadWrapper;
import com.getinsured.ridpv2.RIDPNamespaceMapper;

public class RIDPSecondaryServiceHandler extends AbstractServiceHandler{

	private static com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ObjectFactory extnFactory = new com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ObjectFactory();
	private static com.getinsured.ridpv2.cms.dsh.ridp.exchange._1.ObjectFactory exchFactory = new com.getinsured.ridpv2.cms.dsh.ridp.exchange._1.ObjectFactory();
	private Jaxb2Marshaller marshaller;
	
	public Object getRequestpayLoad() throws HubServiceException {
		if(getJsonInput() == null){
			throw new HubServiceException("Can not generate the payload, expecting JSON input");
		}
		return this.getPayLoadFromJSONInput(null, getJsonInput());
	}
	
	public Object getPayLoadFromJSONInput(String jsonSectionKey, String jsonInputString) throws HubServiceException {
		RIDPSecondaryRequestWrapper ridpSecondaryRequestWrapper = RIDPSecondaryRequestWrapper.fromJSON(jsonInputString);
		RequestPayloadType value = extnFactory.createRequestPayloadType();
		JAXBElement<SecondaryRequestInformationType> secondaryRequestJaxb = extnFactory.createSecondaryRequest(ridpSecondaryRequestWrapper.getSystemRepresentation());
		value.setRequest(secondaryRequestJaxb);
		return exchFactory.createRequest(value);
	}

	public String handleResponse(Object responseObject)
			throws HubServiceException {
		@SuppressWarnings("unchecked")
		JAXBElement<ResponsePayloadType> response = (JAXBElement<ResponsePayloadType>)responseObject;
		ResponsePayloadType responsePayload = response.getValue();
		ResponsePayloadWrapper responseWrapper = new ResponsePayloadWrapper(responsePayload);
		this.requestStatus = RequestStatus.SUCCESS;
		return responseWrapper.toJSONString();
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		return "H1.1";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.ridpv2.cms.dsh.ridp.exchange._1",new RIDPNamespaceMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.ridpv2.cms.dsh.ridp.exchange._1",new RIDPNamespaceMapper());
		}
		return this.marshaller;
	}

}
