package com.getinsured.eligibility.at.resp.si.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


public class EligibilityProgram {

	private Long id;

	private String eligibilityType;

	private String eligibilityIndicator;

	private Date eligibilityStartDate;

	private Date eligibilityEndDate;

	private Date eligibilityDeterminationDate;

	private String ineligibleReason;

	private String establishingCategoryCode;

	private String establishingStateCode;

	private String establishingCountyName;

	private BigDecimal maxAPTCAmount;

	private BigDecimal maxStateSubsidyAmount;

	private String csrLevel;

	private Long ssapApplicantId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEligibilityType() {
		return eligibilityType;
	}

	public void setEligibilityType(String eligibilityType) {
		this.eligibilityType = eligibilityType;
	}

	public String getEligibilityIndicator() {
		return eligibilityIndicator;
	}

	public void setEligibilityIndicator(String eligibilityIndicator) {
		this.eligibilityIndicator = eligibilityIndicator;
	}

	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}

	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}

	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}

	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}

	public Date getEligibilityDeterminationDate() {
		return eligibilityDeterminationDate;
	}

	public void setEligibilityDeterminationDate(Date eligibilityDeterminationDate) {
		this.eligibilityDeterminationDate = eligibilityDeterminationDate;
	}

	public String getIneligibleReason() {
		return ineligibleReason;
	}

	public void setIneligibleReason(String ineligibleReason) {
		this.ineligibleReason = ineligibleReason;
	}

	public String getEstablishingCategoryCode() {
		return establishingCategoryCode;
	}

	public void setEstablishingCategoryCode(String establishingCategoryCode) {
		this.establishingCategoryCode = establishingCategoryCode;
	}

	public String getEstablishingStateCode() {
		return establishingStateCode;
	}

	public void setEstablishingStateCode(String establishingStateCode) {
		this.establishingStateCode = establishingStateCode;
	}

	public String getEstablishingCountyName() {
		return establishingCountyName;
	}

	public void setEstablishingCountyName(String establishingCountyName) {
		this.establishingCountyName = establishingCountyName;
	}

	public Long getSsapApplicantId() {
		return ssapApplicantId;
	}

	public void setSsapApplicantId(Long ssapApplicantId) {
		this.ssapApplicantId = ssapApplicantId;
	}


	@Override
	public int hashCode(){

		return new HashCodeBuilder()
		.append(eligibilityType)
		.toHashCode();
	}

	@Override
	public boolean equals(final Object obj){

		if(obj instanceof EligibilityProgram){

			final EligibilityProgram other = (EligibilityProgram) obj;

			return new EqualsBuilder().append(eligibilityType, other.eligibilityType)
					.isEquals();

		} else{
			return false;
		}
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

	public BigDecimal getMaxAPTCAmount() {
		return maxAPTCAmount;
	}

	public void setMaxAPTCAmount(BigDecimal maxAPTCAmount) {
		this.maxAPTCAmount = maxAPTCAmount;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public BigDecimal getMaxStateSubsidyAmount() {
		return maxStateSubsidyAmount;
	}

	public void setMaxStateSubsidyAmount(BigDecimal maxStateSubsidyAmount) {
		this.maxStateSubsidyAmount = maxStateSubsidyAmount;
	}
}
