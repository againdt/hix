package com.getinsured.hix.indportal.dto.dm.application.result;

import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;

public class ApplicationData {

	private String jsonSource;
	
	private AccountTransferRequestPayloadType requestObj;
	
	private String requestXML;
	
	private String responseXML;

	private String errorMessage;
	
	public String getJsonSource() {
		return jsonSource;
	}

	public void setJsonSource(String jsonSource) {
		this.jsonSource = jsonSource;
	}

	public AccountTransferRequestPayloadType getRequestObj() {
		return requestObj;
	}

	public void setRequestObj(AccountTransferRequestPayloadType requestObj) {
		this.requestObj = requestObj;
	}

	public String getRequestXML() {
		return requestXML;
	}

	public void setRequestXML(String requestXML) {
		this.requestXML = requestXML;
	}

	public String getResponseXML() {
		return responseXML;
	}

	public void setResponseXML(String responseXML) {
		this.responseXML = responseXML;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
