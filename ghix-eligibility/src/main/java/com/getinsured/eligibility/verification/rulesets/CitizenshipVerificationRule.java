package com.getinsured.eligibility.verification.rulesets;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Encapsulates Citizenship Verification Rule methods
 */
@Component
public class CitizenshipVerificationRule {

	private static final Logger LOGGER = LoggerFactory.getLogger(CitizenshipVerificationRule.class);
	
	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	public static void main(String[] asdf) throws Exception {
		CitizenshipVerificationRule citizenshipVerificationRule = new CitizenshipVerificationRule();
		//citizenshipVerificationRule.verifyCitizenship(SSAPUtils.loadSSAPJSON(), SSAPUtils.loadHUBJSON());
	}
	
	/**
	 * This rule will verify Citizenship Status of Household Members
	 * 
	 * @param ssapJson
	 * @param hubResponseJson
	 * @throws Exception 
	 */
	public boolean verifyCitizenship(String ssapJson, String hubResponseJson, String pPersonId) throws Exception {
		LOGGER.info("Verify Citizenship Rule : Starts");
		
		String status = null;
		boolean verificationStatus = false;
		JsonArray householdMembers;
		JsonObject householdMember;
		String personId;
		boolean personChecked = false;
		JsonObject singleStreamlinedApplication;

		try {
			singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(ssapJson);
			
			if(!SSAPUtils.isEmpty(hubResponseJson)){
				if(!SSAPConstants.NO_SSN.equalsIgnoreCase(hubResponseJson)){
					householdMembers = SSAPUtils.getFilteredHouseholdMembers(singleStreamlinedApplication);
					
					Iterator<JsonElement> iterator = householdMembers.iterator();
					while (iterator.hasNext()) {
						householdMember = iterator.next().getAsJsonObject();
						personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
						
						try {
							if(personId.equals(pPersonId)){
								String personUSCitizenIndicator = SSAPUtils.searchHubResponseJson( 
											SSAPConstants.PERSON_US_CITIZEN_INDICATOR, hubResponseJson);
									
								if(!SSAPUtils.isEmpty(personUSCitizenIndicator)){
									if(personUSCitizenIndicator.equals(SSAPConstants.TRUE)){
										status = SSAPConstants.VERIFIED;
									} else {
										status = SSAPConstants.NOT_VERIFIED;
									}
								}
								personChecked = true;
							} 
						
							if(personChecked)break;
						} catch(Exception exception){
							LOGGER.error("Exception occurred while Verifying Citizenship : Person : "+pPersonId, exception);
							String rule = SSAPConstants.RULES.CITIZENSHIP_VERIFICATION_RULE.toString();
							giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
									hubResponseJson, null, null, null, rule), rule));
						}
					}
				} else {
					status = SSAPConstants.NOT_VERIFIED;
				}
			} else {
				status = SSAPConstants.PENDING;
			}
			
			if(!SSAPUtils.isEmpty(status)){
				verificationStatus = verificationRulesService.updateVerificationStatus(Long.valueOf(SSAPUtils.getApplicationIdFromSsapJson(singleStreamlinedApplication)),
						Long.valueOf(pPersonId), status, SSAPConstants.CITIZENSHIP_VERIFICATION_RULE);
				verificationStatus = true;
			} 
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying Citizenship : ",exception);
			throw exception;
		}
		
		LOGGER.info("Verify Citizenship Rule : Ends ");
		
		return verificationStatus;
	}
}
