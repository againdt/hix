import React, { Fragment, useState } from 'react';
import { connect } from 'react-redux';
import ReactHtmlParser from 'react-html-parser';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const InstructionalText = props => {

    const {
        Component,
        language,
        instructions,
        styleType,
        replacements = {}
    } = props;

    const [isShown, toggleView] = useState(false);

    const handleClick = (e) => {
        e.preventDefault();
        toggleView(isShown => !isShown);
    };

    const text = (() => {

        let extracted = instructions && instructions[language] && instructions[language][Component.props['data-instructions']];

        if (extracted) {
            Object.keys(replacements).forEach(rpl => {
                extracted = extracted.replace(`[${rpl}]`, replacements[rpl]);
            });
            extracted = ReactHtmlParser(extracted);
        }
        return extracted;
    })();

    /**
     * Returns appropriate class depending on where instructionalText is used.
     * @param styleType
     * @returns {string}
     */
    const getClassName = (styleType = 'PARAGRAPH') => {
        const STYLE_TYPES = {
            PARAGRAPH: 'PARAGRAPH',
            MODAL: 'MODAL',
        };

        let className = 'instruction_link';

        switch (styleType) {
        case STYLE_TYPES.PARAGRAPH: {
            className += ' instruction_link_p';
            break;
        }
        case STYLE_TYPES.MODAL: {
            className += ' usa-width-one-third txt-right';
            break;
        }
        }
        return className;
    };

    const link = React.cloneElement(
        <a
            href="#"
            onClick={handleClick}
            className={getClassName(styleType)}
            id="instructionalLink"
            key={Component.props['data-instructions']}
        >
            Learn more
        </a>
    );

    const WrappedComponent = React.cloneElement(Component, {

        ...(React.isValidElement(Component.props.children) ?
            {
                children: React.cloneElement(Component.props.children, {
                    children:
                        [
                            ...(Component.props.children.props.children ? Component.props.children.props.children : []),
                            link
                        ]
                })
            } :
            {
                children:
                    [
                        Component.props.children,
                        link
                    ]
            })
    });

    return (
        <Fragment >
            {WrappedComponent}
            {
                <div className={`${isShown ? 'text-expand' : 'text-collapse'}`}>
                    <div className={`${isShown ? 'usa-alert usa-alert-info alert--info expandable' : 'hidden'}`}>
                        <p>{text}</p>
                        <a className="expandable__btn--close" href="#" onClick={handleClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                    </div>
                </div>


            }
        </Fragment>
    );
};

const mapStateToProps = state => ({
    instructions: state.instructions,
    language: state.language
});

export default connect(mapStateToProps)(InstructionalText);