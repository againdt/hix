package com.getinsured.hix.indportal.dto;

import java.util.TreeSet;

public class MemberEligibilityDetails {

	private String firstName;
	private String MiddleName;
	private String lastName;
	private long personId;
	private boolean eligibilityStatus;
	private String suffix;
	
	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	
	private TreeSet<MemberEligibilityStatus> memberEligibilities;

	public TreeSet<MemberEligibilityStatus> getMemberEligibilities() {
		return memberEligibilities;
	}

	/*public void setMemberEligibilities(
			Set<EligibilityProgramDTO> memberEligibilities) {
		TreeSet<MemberEligibilityStatus> eligibleMemberSet = new TreeSet<MemberEligibilityStatus>(new MemberEligibilityStatusComparator());
		for(EligibilityProgramDTO epd: memberEligibilities){
			MemberEligibilityStatus mes = new MemberEligibilityStatus();
			if(epd.getEligibilityStatus()!=null) {
				mes.setEligibilityStatus(epd.getEligibilityStatus().getDescription());
			}

			if(StringUtils.isNotEmpty(epd.getEligibilityStatus().getDescription()) && StringUtils.isNotEmpty(ApplicantEligibilityStatus.QHP.getDescription()) && ApplicantEligibilityStatus.QHP.getDescription().equalsIgnoreCase(epd.getEligibilityStatus().getDescription())){
				mes.setEligibilityStatus("Medical and Dental Insurance");
			}
			eligibleMemberSet.add(mes);
		}
	
		this.memberEligibilities=eligibleMemberSet;
	}*/
	
	

	public void setMemberEligibilities(TreeSet<MemberEligibilityStatus> memberEligibilities) {
		this.memberEligibilities = memberEligibilities;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return MiddleName;
	}

	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MemberEligibilityDetails [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", personId=");
		builder.append(personId);
		builder.append(", memberEligibilities=");
		builder.append(memberEligibilities);
		builder.append("]");
		return builder.toString();
	}

	public boolean getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(boolean eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	
}
