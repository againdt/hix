package com.getinsured.eligibility.at.ref.transform;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.Ethnicity;
import com.getinsured.iex.ssap.EthnicityAndRace;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
public class CompareJsonTransformer {
	public static void transformDataFromJson(CompareApplicationDTO compareApplicationDTO, boolean demographic) {
		try {
			String applicationJSON = compareApplicationDTO.getApplicationData();
			JSONObject jsonObj = new JSONObject(applicationJSON);
			JSONObject ssapJsonObj = jsonObj.getJSONObject(SSAPConstants.SINGLE_STREAMLINED_APPLICATION);
			JSONArray taxHouseHold = ssapJsonObj.getJSONArray(SSAPConstants.TAX_HOUSEHOLD);
			JSONObject householdMember;
			JSONArray householdMembers = ((JSONArray) ((JSONObject) taxHouseHold.get(0)).get(SSAPConstants.HOUSEHOLD_MEMBER));

			List<CompareApplicantDTO> compareApplicants = compareApplicationDTO.getApplicants();
			for (CompareApplicantDTO compareApplicantDTO : compareApplicants) {
				householdMember = householdMember(compareApplicantDTO.getApplicantGuid(), householdMembers);
				if (householdMember != null) {
					transformAddress(householdMember, compareApplicantDTO, demographic);
					if (demographic) {
						transformEthnicityAndRace(householdMember, compareApplicantDTO);
						transformPhone(householdMember, compareApplicantDTO);
						transformWrittenAndSpokenLanguage(householdMember, compareApplicantDTO);
					} else {
						transformCitizenshipIndicator(householdMember, compareApplicantDTO);
						transformSeekQHP(householdMember, compareApplicantDTO);
						if (householdMember.getInt(SSAPConstants.PERSON_ID) == ReferralConstants.PRIMARY) {
							transformBloodRelationship(householdMember, compareApplicantDTO);
						}
					}

				}
			}
		} catch (JSONException e) {
			throw new GIRuntimeException(e);
		}
	}

	private static void transformCitizenshipIndicator(JSONObject householdMember, CompareApplicantDTO compareApplicantDTO) throws JSONException {
		{
			JSONObject citizenShipImmigrationStatus = (JSONObject) householdMember.get(SSAPConstants.CITIZENSHIP_IMMIGRATION_STATUS);
			if (citizenShipImmigrationStatus != null && citizenShipImmigrationStatus.get(SSAPConstants.CITIZENSHIP_STATUS_INDICATOR) != null) {
				compareApplicantDTO.setCitizenShipStatus(Boolean.parseBoolean(citizenShipImmigrationStatus.get(SSAPConstants.CITIZENSHIP_STATUS_INDICATOR).toString()));
			}
		}

	}
	
	private static void transformSeekQHP(JSONObject householdMember, CompareApplicantDTO compareApplicantDTO) throws JSONException {
		{
			if(householdMember.get(SSAPConstants.SEEKS_QHP)!=null){
				compareApplicantDTO.setSeekQHP(Boolean.parseBoolean(householdMember.get(SSAPConstants.SEEKS_QHP).toString()));
			}
		}
    }

	private static void transformWrittenAndSpokenLanguage(JSONObject householdMember, CompareApplicantDTO compareApplicantDTO) throws JSONException {
		{
			JSONObject householdContact = (JSONObject) householdMember.get(SSAPConstants.HOUSEHOLD_CONTACT);
			if (householdContact != null) {
				JSONObject contactPreferences = (JSONObject) householdContact.get(SSAPConstants.CONTACT_PREFERENCES);
				if (contactPreferences != null && contactPreferences.get(SSAPConstants.PREFERRED_SPOKEN_LANGUAGE) != null && contactPreferences.get(SSAPConstants.PREFERRED_WRITTEN_LANGUAGE) != null) {
					compareApplicantDTO.setSpokenLanguage(String.valueOf(contactPreferences.get(SSAPConstants.PREFERRED_SPOKEN_LANGUAGE)));
					compareApplicantDTO.setWrittenLanguage(String.valueOf(contactPreferences.get(SSAPConstants.PREFERRED_WRITTEN_LANGUAGE)));
				}
			}
		}

	}
	
	private static void transformPhone(JSONObject householdMember, CompareApplicantDTO compareApplicantDTO) throws JSONException {
		JSONObject householdContact = (JSONObject) householdMember.get(SSAPConstants.HOUSEHOLD_CONTACT);
		JSONObject phone = (JSONObject) householdContact.get(SSAPConstants.PHONE);
		if (phone != null) {
			if (phone.get(SSAPConstants.PHONE_NUMBER) != null) {
				compareApplicantDTO.getPhone().setPhoneNumber(ReferralUtil.convertToValidString(phone.get(SSAPConstants.PHONE_NUMBER)));
			}
			if (phone.get(SSAPConstants.PHONE_EXTENSION) != null) {
				compareApplicantDTO.getPhone().setPhoneExtension(ReferralUtil.convertToValidString(phone.get(SSAPConstants.PHONE_EXTENSION)));
			}
			if (phone.get(SSAPConstants.PHONE_TYPE) != null) {
				compareApplicantDTO.getPhone().setPhoneType(ReferralUtil.convertToValidString(phone.get(SSAPConstants.PHONE_TYPE)));
			}
		}
		JSONObject otherPhone = (JSONObject) householdContact.get(SSAPConstants.OTHER_PHONE);
		if (phone != null) {
			if (otherPhone.get(SSAPConstants.PHONE_NUMBER) != null) {
				compareApplicantDTO.getOtherPhone().setPhoneNumber(ReferralUtil.convertToValidString(otherPhone.get(SSAPConstants.PHONE_NUMBER)));
			}
			if (otherPhone.get(SSAPConstants.PHONE_EXTENSION) != null) {
				compareApplicantDTO.getOtherPhone().setPhoneExtension(ReferralUtil.convertToValidString(otherPhone.get(SSAPConstants.PHONE_EXTENSION)));
			}
			if (otherPhone.get(SSAPConstants.PHONE_TYPE) != null) {
				compareApplicantDTO.getOtherPhone().setPhoneType(ReferralUtil.convertToValidString(otherPhone.get(SSAPConstants.PHONE_TYPE)));
			}
		}
	}

	private static void transformEthnicityAndRace(JSONObject householdMember, CompareApplicantDTO compareApplicantDTO) throws JSONException {

		JSONObject ethnicityAndRaceJSON = (JSONObject) householdMember.get(SSAPConstants.ETHNICITY_AND_RACE);

		EthnicityAndRace ethnicityAndRace = compareApplicantDTO.getEthnicityAndRace();

		if (ReferralUtil.convertToValidString(ethnicityAndRaceJSON.get(SSAPConstants.HISPANICLATINOSPANISHORIGININDICATOR)) != null) {
			ethnicityAndRace.setHispanicLatinoSpanishOriginIndicator(Boolean.parseBoolean(ReferralUtil.convertToValidString(ethnicityAndRaceJSON.get(SSAPConstants.HISPANICLATINOSPANISHORIGININDICATOR))));
		}

		JSONArray raceList = ethnicityAndRaceJSON.getJSONArray(SSAPConstants.RACE);

		Race race;
		int arraySize = ReferralUtil.sizeForJSONArray(raceList);
		for (int i = 0; i < arraySize; i++) {
			JSONObject raceJSON = (JSONObject) raceList.get(i);
			race = new Race();
			if (raceJSON.get(SSAPConstants.CODE) != null) {
				race.setCode(ReferralUtil.convertToValidString(raceJSON.get(SSAPConstants.CODE)));
			}
			if (raceJSON.get(SSAPConstants.LABEL) != null) {
				race.setLabel(ReferralUtil.convertToValidString(raceJSON.get(SSAPConstants.LABEL)));
			}
			if (raceJSON.get(SSAPConstants.OTHER_LABEL) != null) {
				race.setOtherLabel(ReferralUtil.convertToValidString(raceJSON.get(SSAPConstants.OTHER_LABEL)));
			}
			ethnicityAndRace.getRace().add(race);
		}
		JSONArray ethnicityList = ethnicityAndRaceJSON.getJSONArray(SSAPConstants.ETHNICITY);
		Ethnicity ethnicity;
		arraySize = ReferralUtil.sizeForJSONArray(ethnicityList);

		for (int i = 0; i < arraySize; i++) {
			JSONObject ethnicityJSON = (JSONObject) ethnicityList.get(i);
			ethnicity = new Ethnicity();
			if (ethnicityJSON.get(SSAPConstants.CODE) != null) {
				ethnicity.setCode(ReferralUtil.convertToValidString(ethnicityJSON.get(SSAPConstants.CODE)));
			}
			if (ethnicityJSON.get(SSAPConstants.LABEL) != null) {
				ethnicity.setLabel(ReferralUtil.convertToValidString(ethnicityJSON.get(SSAPConstants.LABEL)));
			}
			if (ethnicityJSON.get(SSAPConstants.OTHER_LABEL) != null) {
				ethnicity.setOtherLabel(ReferralUtil.convertToValidString(ethnicityJSON.get(SSAPConstants.OTHER_LABEL)));
			}
			ethnicityAndRace.getEthnicity().add(ethnicity);
		}
	}

	private static void transformAddress(JSONObject householdMember, CompareApplicantDTO compareApplicantDTO, boolean demographic) throws JSONException {

		JSONObject householdContact = (JSONObject) householdMember.get(SSAPConstants.HOUSEHOLD_CONTACT);

		JSONObject homeAddress = (JSONObject) householdContact.get(SSAPConstants.HOME_ADDRESS);
		if (demographic) {
			JSONObject mailingAddress = (JSONObject) householdContact.get(SSAPConstants.MAILING_ADDRESS);
			if (mailingAddress != null) {
				compareApplicantDTO.setMailingAddress(address(mailingAddress));
			}
		}
		if (homeAddress != null) {
			compareApplicantDTO.setPrimaryAddress(address(homeAddress));
		}

	}

	private static Address address(JSONObject jaddress) throws JSONException {
		Address address = new Address();

		if (jaddress.get("city") != null) {
			address.setCity(jaddress.get("city").toString());
		}
		if (jaddress.get("postalCode") != null) {
			address.setPostalCode(jaddress.get("postalCode").toString());
		}
		if (jaddress.get("state") != null) {
			address.setState(jaddress.get("state").toString());
		}
		if (jaddress.get("streetAddress1") != null) {
			address.setStreetAddress1(jaddress.get("streetAddress1").toString());
		}
		// added string null equals ignore case because jaddress.get("streetAdress2") from JSONObject is "null".
		if (jaddress.get("streetAddress2") != null && !"null".equalsIgnoreCase(jaddress.get("streetAddress2").toString())) {
			address.setStreetAddress2(jaddress.get("streetAddress2").toString());
		}
		if (jaddress.get("county") != null) {
			address.setCounty(jaddress.get("county").toString());
		}
		if (jaddress.get("primaryAddressCountyFipsCode") != null) {
			address.setPrimaryAddressCountyFipsCode(jaddress.get("primaryAddressCountyFipsCode").toString());
		}

		return address;
	}

	private static JSONObject householdMember(String applicantGuid, JSONArray householdMembers) throws JSONException {
		JSONObject returnOj = null;

		for (int i = 0; i < householdMembers.length(); i++) {
			if (StringUtils.equalsIgnoreCase(((((JSONObject) householdMembers.get(i)).get("applicantGuid"))).toString(), applicantGuid)) {
				returnOj = (JSONObject) householdMembers.get(i);
				break;
			}
		}
		return returnOj;
	}

	private static void transformBloodRelationship(JSONObject householdMember, CompareApplicantDTO compareApplicantDTO) throws JSONException {

		JSONArray bloodRelationshipJSONList = householdMember.getJSONArray(SSAPConstants.BLOOD_RELATIONSHIPS);
		final int arraySize = ReferralUtil.sizeForJSONArray(bloodRelationshipJSONList);
		BloodRelationship bloodRelationship;
		JSONObject bloodRelationshipJSON;
		for (int i = 0; i < arraySize; i++) {
			bloodRelationshipJSON = (JSONObject) bloodRelationshipJSONList.get(i);
			bloodRelationship = new BloodRelationship();
			bloodRelationship.setIndividualPersonId(ReferralUtil.convertToValidString(bloodRelationshipJSON.get(SSAPConstants.INDIVIDUAL_PERSONID)));
			bloodRelationship.setRelatedPersonId(ReferralUtil.convertToValidString(bloodRelationshipJSON.get(SSAPConstants.RELATED_PERSONID)));
			bloodRelationship.setRelation(ReferralUtil.convertToValidString(bloodRelationshipJSON.get(SSAPConstants.RELATION)));
			if (ReferralUtil.convertToValidString(bloodRelationshipJSON.get(SSAPConstants.TEXT_DEPENDENCY)) != null) {
				bloodRelationship.setTextDependency(Boolean.parseBoolean(ReferralUtil.convertToValidString(bloodRelationshipJSON.get(SSAPConstants.TEXT_DEPENDENCY))));
			}
			compareApplicantDTO.getBloodRelationships().add(bloodRelationship);
		}
	}

}
