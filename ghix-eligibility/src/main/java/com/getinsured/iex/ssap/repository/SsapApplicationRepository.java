package com.getinsured.iex.ssap.repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.iex.ssap.model.SsapApplication;

@RestResource(path="SsapApplication")
public interface SsapApplicationRepository extends JpaRepository<SsapApplication, Long> ,CustomSsapApplicationRepository{

	@RestResource(path="find-by-source")
	public SsapApplication findSsapApplicationBySource(@Param("source") String id);

	/**
	 * This query has been modified to ignore Queued and DeQueued applications so that existing functionality remains unaffected.
	 * These two statuses were introduced as part of MN dev - multiple eligibility spans.
	 * @param cmrHouseoldId
	 * @return
	 */
	@RestResource(path="findByCmrHouseoldId")
	@Query(" from SsapApplication where cmrHouseoldId = :cmrHouseoldId and applicationStatus not in ('QU' ,'DQ') ")
	//public SsapApplication findByCmrHouseoldId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId);
	public List<SsapApplication> findByCmrHouseoldId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId);

	@RestResource(path="findByMaxIdAndCmrHouseoldId")
	@Query("from SsapApplication where id = (SELECT MAX(id) from SsapApplication where cmrHouseoldId = :cmrHouseoldId and applicationStatus in ('EN'))")
	public SsapApplication findByMaxIdAndCmrHouseoldId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId);
	
	@Query("from SsapApplication ssapApplication where ssapApplication.externalApplicationId = :externalApplicationId")
	public List<SsapApplication> findLatestByExternalAppId(@Param("externalApplicationId") String externalApplicationId,  Pageable pageable);

	@Query("from SsapApplication ssapApplication where ssapApplication.externalApplicationId = :externalApplicationId and ssapApplication.creationTimestamp >= :creationTimestamp")
	public List<SsapApplication> findLatestByExternalAppIdCreationTime(@Param("externalApplicationId") String externalApplicationId, @Param("creationTimestamp") Timestamp creationTime, Pageable pageable);
	@RestResource(path="findByCmrHouseoldIdAndApplicationStatusAndMaxId")
	@Query("from SsapApplication where id = ( SELECT MAX(id) from SsapApplication where cmrHouseoldId = :cmrHouseoldId and applicationStatus = :status)")
	public SsapApplication findByCmrHouseoldIdAndApplicationStatusAndMaxId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("status") String status);

	@RestResource(path="findLatestUpdateByCmrHouseoldId")
	@Query("Select ssapapplication from SsapApplication as ssapapplication where cmrHouseoldId = :cmrHouseoldId and applicationStatus in ('EN' ,'CL') and coverageYear = :requestYear ")
	public List<SsapApplication> findLatestUpdateByCmrHouseoldId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("requestYear") Long requestYear);

	@Query("Select count(id) from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear and applicationStatus not in ( :statusList) and financialAssistanceFlag = 'N' ")
	public long countOfNonFinancialAppByCmrAndApplicationStatus(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear, @Param("statusList") List<String> statusList);

	@Query("Select count(id) from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear and applicationStatus not in ( :statusList) ")
	public long countOfApplicationByCmrAndApplicationStatus(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear, @Param("statusList") List<String> statusList);

	/**
	 * HQL-JPA query to return List<SsapApplication> for Id.
	 *
	 * This helps in leveraging LAZY behavior as default
	 * for SsapApplication and  SsapApplicant entities.
	 *
	 * @param ssapApplicantId
	 * @return List<SsapApplication> including all linked SsapApplicant objects
	 */
	@RestResource(path="getApplicationsById")
	@Query("Select application " +
			" FROM SsapApplication as application "+
			" inner join fetch application.ssapApplicants as ssapApplicants"+
			" where application.id = :id ")
	List<SsapApplication> getApplicationsById(@Param("id") Long id);

	@RestResource(path="findByCaseNumber")
	@Query("Select application " +
			" FROM SsapApplication as application "+
			" inner join fetch application.ssapApplicants as ssapApplicants"+
			" where application.caseNumber = :caseNumber ")
	List<SsapApplication> findByCaseNumber(@Param("caseNumber") String caseNumber);

	@RestResource(path="findByCmrHouseoldIdAndCoverageYear")
	@Query("from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear order by creationTimestamp desc")
	public List<SsapApplication> findByCmrHouseoldIdAndCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);

	@RestResource(path="findEnrolledSsapApplicationForCoverageYear")
	@Query("from SsapApplication where cmrHouseoldId = :cmrHouseoldId and applicationStatus = 'EN' and coverageYear = :coverageYear")
	public SsapApplication findEnrolledSsapApplicationForCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);
	 
	@Query("Select application from SsapApplication as application where application.cmrHouseoldId = :cmrHouseoldId and application.applicationStatus in ( 'EN' , 'PN' ) and application.coverageYear = :coverageYear and application.creationTimestamp = ( select max(creationTimestamp ) FROM  SsapApplication  where  cmrHouseoldId = :cmrHouseoldId and  applicationStatus in ( 'EN' , 'PN' ) and  coverageYear = :coverageYear  ) ")
	public  SsapApplication findLatestEnPnSsapApplicationForCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);
	
	@RestResource(path="findByCmrHouseoldIdAndCaseNumber")
	public SsapApplication findByCmrHouseoldIdAndCaseNumber(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("caseNumber") String caseNumber);

	@RestResource(path="findByCaseNumberId")
	@Query("Select application " +
			" FROM SsapApplication as application "+
			" where application.caseNumber = :caseNumber ")
	public SsapApplication findByCaseNumberId(@Param("caseNumber") String caseNumber);

	@RestResource(path="find-by-id-cmr-null")
	@Query("from SsapApplication where id = :id and cmrHouseoldId is null")
	public SsapApplication findSsapApplicationByIdCmrNull(@Param("id") Long id);

	@RestResource(path="updateApplicationStatusByCaseNumber")
	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationStatus = :applicationStatus where caseNumber = :caseNumber")
	void updateApplicationStatus(@Param("applicationStatus") String applicationStatus, @Param("caseNumber") String caseNumber);

	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationStatus = :applicationStatus, lastUpdateTimestamp = :lastUpdateTimestamp where caseNumber = :caseNumber")
	void updateApplicationStatus(@Param("applicationStatus") String applicationStatus, @Param("caseNumber") String caseNumber, @Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);

	@RestResource(path="find-by-externalApplicationId")
	List<SsapApplication> findByExternalApplicationId(@Param("externalApplicationId") String externalApplicationId);

	@RestResource(path="findByCaseNumberOne")
	@Query("Select application " +
			" FROM SsapApplication as application "+
			" where application.caseNumber = :caseNumber ")
	List<SsapApplication> findByCaseNumberOne(@Param("caseNumber") String caseNumber);

	@RestResource(path="fetchCaseNumberById")
	@Query("select caseNumber FROM SsapApplication as application where application.id = :id ")
	String fetchCaseNumberById(@Param("id") long id);

	@RestResource(path="findByExternalApplicantId")
	@Query("Select application " +
			" FROM SsapApplication as application " +
			" INNER JOIN application.ssapApplicants as applicant"+
			" where applicant.externalApplicantId = :externalApplicantId"
			+ " and applicant.personId = 1 and application.financialAssistanceFlag = 'Y' ")
	List<SsapApplication> findByExternalApplicantId(@Param("externalApplicantId") String externalApplicantId);

	@RestResource(path="findByAppId")
	@Query("Select application " +
			" FROM SsapApplication as application "+
			" inner join fetch application.ssapApplicants as ssapApplicants"+
			" where application.id = :id ")
	List<SsapApplication> findByAppId(@Param("id") Long id);

	@RestResource(path="findAndLoadApplicantsByAppId")
	@Query("Select application " +
			" FROM SsapApplication as application "+
			" inner join fetch application.ssapApplicants as ssapApplicants"+
			" where application.id = :id ")
	SsapApplication findAndLoadApplicantsByAppId(@Param("id") Long id);

	@RestResource(path="updateApplicationStatusByApplicationId")
	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationStatus = :applicationStatus where id = :ssapApplicationId")
	int updateApplicationStatusById(@Param("applicationStatus") String applicationStatus, @Param("ssapApplicationId") long ssapApplicationId);

	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationStatus = :applicationStatus, lastUpdateTimestamp = :lastUpdateTimestamp where id = :ssapApplicationId")
	int updateApplicationStatusById(@Param("applicationStatus") String applicationStatus, @Param("ssapApplicationId") long ssapApplicationId, @Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);

	@Modifying
	@Transactional
	@Query("update SsapApplication set effectiveDate = :effectiveDate where id = :ssapApplicationId")
	int updateApplicationEffectiveDateById(@Param("ssapApplicationId") long ssapApplicationId, @Param("effectiveDate") Date effectiveDate);

	@Query("Select application " +
			" FROM SsapApplication as application "+
			" where application.id IN :idList and application.applicationStatus = :applicationStatus")
	List<SsapApplication> findByApplicationStatusAndId(@Param("idList") List<Long> idList,@Param("applicationStatus") String applicationStatus );

	@Query("from SsapApplication where cmrHouseoldId = :cmrHouseoldId and applicationStatus = :applicationStatus and coverageYear = :coverageYear")
	List<SsapApplication> findSsapByCmrHouseholdIdAppstatus(@Param("cmrHouseoldId") BigDecimal bigDecimal,@Param("applicationStatus") String applicationStatus, @Param("coverageYear") long coverageYear);

	//@Modifying
	//@Transactional
	//@Query("update SsapApplication ssap set  ssap.exemptHousehold = :exemptHouseHold where ssap.caseNumber = :caseNumber")
	//void updateSsapExempt(@Param("exemptHouseHold") String exemptHouseHold, @Param("caseNumber") String caseNumber);

	@Modifying
	@Transactional
	@Query("update SsapApplication ssap set  ssap.exemptHousehold = :exemptHouseHold, ssap.lastUpdateTimestamp = :lastUpdateTimestamp where ssap.caseNumber = :caseNumber")
	void updateSsapExempt(@Param("exemptHouseHold") String exemptHouseHold, 
			@Param("caseNumber") String caseNumber, 
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);

	@Modifying
	@Transactional
	@Query("update SsapApplication ssap "
			+ "set ssap.cmrHouseoldId = :cmrHouseoldId, "
			+ "ssap.applicationStatus = :applicationStatus, "
			+ "ssap.lastUpdateTimestamp = :lastUpdateTimestamp "
			+ "where ssap.id = :applicationId")
	int updateCmrHouseholdIdAndApplicationStatusById(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, 
			@Param("applicationStatus") String applicationStatus, 
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp, 
			@Param("applicationId") Long applicationId);
	
	@Modifying
	@Transactional
	@Query("update SsapApplication ssap "
			+ "set ssap.cmrHouseoldId = :cmrHouseoldId, "
			+ "ssap.lastUpdateTimestamp = :lastUpdateTimestamp "
			+ "where ssap.id = :applicationId")
	int updateCmrHouseholdIdById(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, 
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp, 
			@Param("applicationId") Long applicationId);

	@Modifying
	@Transactional
	@Query("update SsapApplication ssap set ssap.applicationData = :ssapJsonString, ssap.lastUpdateTimestamp = :lastUpdateTimestamp where ssap.id = :id")
	void updateSsapObjectData(@Param("ssapJsonString") String ssapJsonString, 
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp,
			@Param("id") long id);

	@Query("update SsapApplication ssap set "
			+ "ssap.cmrHouseoldId = :cmrHouseoldId, "
			+ "ssap.nativeAmerican = :hhNativeAmerican, "
			+ "ssap.csrLevel = :csrLevel, "
			+ "ssap.applicationStatus = :status, "
			+ "ssap.eligibilityStatus = :eligibilityStatus, "
			+ "ssap.exchangeEligibilityStatus = :exchangeEligibilityStatus, "
			+ "ssap.allowEnrollment = :allowEnrollment, "
			+ "ssap.eligibilityReceivedDate = :eligibilityReceivedDate, "
			+ "ssap.lastUpdateTimestamp = :lastUpdateTimestamp "
			+ "where ssap.id = :ssapApplicationId")
	int updatePassiveEnrollmentApplication(@Param("ssapApplicationId") long ssapApplicationId, 
			@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, 
			@Param("hhNativeAmerican") String hhNativeAmerican,
			@Param("csrLevel") String csrLevel,
			@Param("status") String status,
			@Param("eligibilityStatus") EligibilityStatus eligibilityStatus,
			@Param("exchangeEligibilityStatus") ExchangeEligibilityStatus exchangeEligibilityStatus,
			@Param("allowEnrollment") String allowEnrollment,
			@Param("eligibilityReceivedDate") Date eligibilityReceivedDate,
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);

	@Query("Select count(application.id) " +
			" FROM SsapApplication as application "+
			" where application.applicationStatus not in ('CC', 'CL') and application.id !=:id "+
			" and (application.cmrHouseoldId = :cmrId or application.id in (select ssapApplicationId from ReferralActivation where cmrHouseholdId = :cmrId)) "+ 
			" and application.id in (select ssapApplication.id from SsapApplicant where ssapApplication.id=application.id and personId =1 and externalApplicantId is not null and externalApplicantId != :externalId)")
	long getCountofApplicationsForCMRIdandMedicaidId(@Param("id") long id, @Param("cmrId") BigDecimal cmrId,@Param("externalId") String externalid);


	@Query("Select count(application.id) " +
			" FROM SsapApplication as application INNER JOIN application.ssapApplicants as applicant "+
			" where application.applicationStatus not in ('CC', 'CL') and application.id !=:id and application.cmrHouseoldId = :cmrId  "+ 
			" and applicant.personId =1 and applicant.externalApplicantId is not null and applicant.externalApplicantId != :externalId)")
	long countofApplicationswithDiffMedicaidIdFromSSAPApplication(@Param("id") long id, @Param("cmrId") BigDecimal cmrId,@Param("externalId") String externalid);


	@Query("Select count(application.id) " +
			" FROM SsapApplication as application INNER JOIN application.ssapApplicants as applicant "+
			" where application.applicationStatus not in ('CC', 'CL') and application.id !=:id and application.id in (select ssapApplicationId from ReferralActivation where cmrHouseholdId = :cmrId) "+ 
			" and applicant.personId =1 and applicant.externalApplicantId is not null and applicant.externalApplicantId != :externalId)")
	long countofApplicationswithDiffMedicaidIdFromReferralActivation(@Param("id") long id, @Param("cmrId") int cmrId,@Param("externalId") String externalid);

	@Query("Select application.cmrHouseoldId " +
			" FROM SsapApplication as application INNER JOIN application.ssapApplicants as applicant "+
			" where application.cmrHouseoldId is not null and application.cmrHouseoldId != 0 and applicant.personId = 1 and applicant.externalApplicantId is not null and applicant.externalApplicantId = :externalId and application.financialAssistanceFlag = 'Y')")
	Set<BigDecimal> cmrHouseholdForMedicaidId(@Param("externalId") String externalid);

	@Query("Select count(application.cmrHouseoldId) " +
			" FROM SsapApplication as application  "+
			" where application.cmrHouseoldId is not null and application.cmrHouseoldId = :cmrHouseoldId and application.applicationStatus not in ('CC', 'CL') and application.financialAssistanceFlag = 'N' )")
	long applicationCountForCmrHousehold(@Param("cmrHouseoldId")BigDecimal cmrHouseoldId);

	@Query("Select application.cmrHouseoldId " +
			" FROM SsapApplication as application INNER JOIN application.ssapApplicants as applicant "+
			" where application.cmrHouseoldId is not null and application.cmrHouseoldId != 0 and applicant.personId = 1 and applicant.externalApplicantId is not null and applicant.externalApplicantId = :externalId)")
	Set<BigDecimal> allCmrHouseholdForMedicaidId(@Param("externalId") String externalid);

	@Query("Select application " +
			" FROM SsapApplication as application " +
			" INNER JOIN application.ssapApplicants as applicant"+
			" where applicant.externalApplicantId = :externalApplicantId and application.coverageYear = :coverageYear "
			+ " and applicant.personId = 1 and application.financialAssistanceFlag = 'Y' order by application.creationTimestamp desc")
	List<SsapApplication> findByExternalApplicantIdForQE(@Param("externalApplicantId") String externalApplicantId, @Param("coverageYear") long coverageYear);


	@RestResource(path="countOfApplicationsByStatus")
	@Query("select count(*) from SsapApplication sa where sa.cmrHouseoldId = :cmrHouseoldId and sa.coverageYear = :coverageYear and sa.applicationType = :applicationType and sa.applicationStatus in :applicationStatus and sa.financialAssistanceFlag = :financialAssistanceFlag")
	long countOfApplicationsByStatusAndCoverageYear(@Param("cmrHouseoldId")BigDecimal cmrHouseoldId,@Param("applicationType")String applicationType,@Param("coverageYear") long coverageYear,
			@Param("applicationStatus")List<String> applicationStatus,@Param("financialAssistanceFlag") String financialAssistanceFlag);

	/**
	 * This query has been modified to ignore Queued and DeQueued applications so that existing functionality remains unaffected.
	 * These two statuses were introduced as part of MN dev - multiple eligibility spans.
	 * @param externalApplicantId
	 * @return
	 */
	@Query("Select application " +
			" FROM SsapApplication as application " +
			" INNER JOIN application.ssapApplicants as applicant"+
			" where applicant.externalApplicantId = :externalApplicantId"
			+ " and application.applicationStatus not in ('QU' ,'DQ')  "
			+ " and applicant.personId = 1 order by application.creationTimestamp desc")
	List<SsapApplication> findByExternalApplicantIdSortDesc(@Param("externalApplicantId") String externalApplicantId);

	@Query("select s.id from SsapApplication s where s.coverageYear = :coverageYear and s.applicationStatus = 'EN' ")
	public List<Long> getEnrolledSsapApplicationsByCoverageYear(@Param("coverageYear") long coverageYear);

	@Query("select s.id from SsapApplication s where s.coverageYear = :coverageYear and s.applicationStatus IN (:applicationStatusList) and s.renewalStatus is null order by s.creationTimestamp desc ")
	public List<Long> getEnrolledSsapApplicationsByCoverageYearWithPageable(@Param("coverageYear") long coverageYear,
			@Param("applicationStatusList") List<String> applicationStatusList, Pageable pageable);

	@Query("select s.id from SsapApplication s where s.coverageYear = :coverageYear and s.applicationStatus IN (:applicationStatusList) order by s.creationTimestamp desc ")
	public List<Long> getSsapApplicationsByCoverageYearAndApplicationStatusList(@Param("coverageYear") long coverageYear,
			@Param("applicationStatusList") List<String> applicationStatusList, Pageable pageable);
	
	@Query("select s.id from SsapApplication s where s.coverageYear = :coverageYear and s.applicationStatus = 'EN' and s.renewalStatus = 'OTR' ")
	public List<Long> getOTRApplicationsByCoverageYear(@Param("coverageYear") long coverageYear);
	
	@Query("select s.id from SsapApplication s where s.coverageYear = :coverageYear and s.applicationStatus IN (:applicationStatusList) and s.renewalStatus = 'OTR' order by s.creationTimestamp desc ")
	public List<Long> getOTRApplicationsByCoverageYearWithPageable(@Param("coverageYear") long coverageYear,
			@Param("applicationStatusList") List<String> applicationStatusList, Pageable pageable);
	
	@Query("select s.id from SsapApplication s where s.coverageYear = :coverageYear and s.applicationStatus = 'EN' and s.cmrHouseoldId = :cmrHouseoldId")
	public Long getEnrolledSsapApplicationIdByCoverageYear(@Param("coverageYear") long coverageYear,@Param("cmrHouseoldId")BigDecimal cmrHouseoldId );
	
	@Query("select max(s.id) from SsapApplication s where s.coverageYear = :coverageYear and "
			+ "(s.applicationStatus = 'EN' or s.applicationStatus = 'PN' or (s.applicationStatus = 'ER' and s.applicationDentalStatus = 'EN')) "
			+ "and s.cmrHouseoldId = :cmrHouseoldId")
	public Long getHealthOrDentalEnrolledApplicationByCoverageYearAndCmrId(@Param("coverageYear") long coverageYear,@Param("cmrHouseoldId")BigDecimal cmrHouseoldId );
	
	@Query("select ssap from SsapApplication ssap "
			+ "where ssap.financialAssistanceFlag = 'N' and "
			+ "ssap.applicationStatus IN :openApplicationStatuses and "
			+ "ssap.esignDate < :expiryDateForCompletingVerifications")
	List<SsapApplication> getOpenNonFinancialApplicationsWithEsignDateLessThan(
			@Param("openApplicationStatuses") List<String> openApplicationStatuses,
			@Param("expiryDateForCompletingVerifications") Date expiryDateForCompletingVerifications);

	@Query("select ssap from SsapApplication ssap "
			+ "where ssap.financialAssistanceFlag = 'N' and "
			+ "ssap.eligibilityStatus = 'CAE' and "
			+ "ssap.applicationStatus IN :openApplicationStatuses and "
			+ "ssap.esignDate > :expiryDateForCompletingVerifications")
	List<SsapApplication> getNonFinancialAppsWithExchangeEligStatusAsCAEAndEsignDateGreaterThan(
			@Param("openApplicationStatuses") List<String> openApplicationStatuses,
			@Param("expiryDateForCompletingVerifications") Date expiryDateForCompletingVerifications);

	@Query("select ssap from SsapApplication ssap "
			+ "where ssap.financialAssistanceFlag = 'N' and "
			+ "ssap.eligibilityStatus = 'CAE' and "
			+ "ssap.applicationStatus IN :openApplicationStatuses and "
			+ "ssap.esignDate < :expiryDateForCompletingVerifications")
	List<SsapApplication> getNonFinancialAppsWithExchangeEligStatusAsCAEAndEsignDateLessThan(
			@Param("openApplicationStatuses") List<String> openApplicationStatuses,
			@Param("expiryDateForCompletingVerifications") Date expiryDateForCompletingVerifications);


	@Query("select ssap from SsapApplication ssap where ssap.id in " +
			"(" +
			"   select distinct sap.id from SsapApplication sap, SsapApplicant sapc " +
			"   where sap.id = sapc.ssapApplication.id " +
			"   and" +
			"   (   " +
			"       sap.financialAssistanceFlag = 'N' and " +
			"       sapc.applyingForCoverage = 'Y' and " +
			"       sap.applicationStatus in :openApplicationStatuses and" +
			"       ((" +
			"           (sapc.citizenshipImmigrationStatus is null or sapc.citizenshipImmigrationStatus = 'PENDING') and " +
			"           (sapc.vlpVerificationStatus is null or sapc.vlpVerificationStatus = 'PENDING')" +
			"       ) " +
			"       or" +
			"       (sapc.deathStatus is null or sapc.deathStatus = 'PENDING') or " +
			"       (sapc.incarcerationStatus is null or sapc.incarcerationStatus = 'PENDING') or" +
			"       (sapc.ssnVerificationStatus is null or sapc.ssnVerificationStatus = 'PENDING'))" +
			"   )" +
			")" +
			"and ssap.esignDate between :startDate and :endDate")
	List<SsapApplication> findApplicationsToRetry(@Param("startDate") Date startDate, @Param("endDate") Date endDate,  
			@Param("openApplicationStatuses") List<String> openApplicationStatuses);

	@Query("select ssap from SsapApplication ssap "
			+ "where ssap.esignDate between :startDate and :endDate "
			+ "and ssap.applicationStatus IN :openApplicationStatuses and ssap.eligibilityStatus = 'CAE' ")
	List<SsapApplication> findApplicationsWithEsignDateWithinRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
			@Param("openApplicationStatuses") List<String> openApplicationStatuses);

	@Query("select ssap.id from SsapApplication ssap "
			+ "where ssap.esignDate between :startDate and :endDate "
			+ "and ssap.applicationStatus IN :openApplicationStatuses")
	List<Long> findApplicationIdsWithEsignDateWithinRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
																   @Param("openApplicationStatuses") List<String> openApplicationStatuses);

	@Query(" select ssap from SsapApplication ssap "
			+ "where ssap.financialAssistanceFlag = :financialAssistance  and "
			+ "ssap.applicationStatus IN :openApplicationStatuses ")
	List<SsapApplication> getOpenApplications(@Param("financialAssistance") String financialAssistance,
			@Param("openApplicationStatuses") List<String> openApplicationStatuses);

	@Query("Select application " +
			" FROM SsapApplication as application "+
			" inner join fetch application.ssapApplicants as ssapApplicants"+
			" where ssapApplicants.birthDate >:startDate and ssapApplicants.birthDate < :endDate " )
	List<SsapApplication> getApplicantsBetween(@Param("startDate") Date startDate,@Param("endDate") Date endDate);

	@Query("from SsapApplication where id = (SELECT MAX(id) from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear and financialAssistanceFlag = :financialAssistance)")
	public SsapApplication findByMaxIdCoverageYearAndCmrHouseoldId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear, @Param("financialAssistance") String financialAssistance);

	@Query("from SsapApplication where coverageYear = :coverageYear and ((financialAssistanceFlag='N' and applicationStatus in ('OP', 'ER')) or (financialAssistanceFlag='Y' and applicationStatus='ER') ) and cmrHouseoldId is not null  and applicationType='OE' and renewalStatus not in ('TO_CONSIDER','NOT_TO_CONSIDER','AUTO_ENROLMENT','MANUAL_ENROLMENT','ERROR')")
	public List<SsapApplication> getRenewedSsapApplicationsByCoverageYear(@Param("coverageYear") long coverageYear,  Pageable pageable);

	@Query("from SsapApplication where coverageYear = :coverageYear and applicationStatus = 'ER' and financialAssistanceFlag in :financialAssistenceFlag and (renewalStatus = 'TO_CONSIDER' or dentalRenewalStatus = 'TO_CONSIDER')   ")
	public List<SsapApplication> getAutoRenewalApplicationsByCoverageYear(@Param("coverageYear") long coverageYear, @Param("financialAssistenceFlag") List<String> financialAssistenceFlag ,  Pageable pageable);
	
	@Query("from SsapApplication WHERE coverageYear = :coverageYear AND applicationStatus ='ER' and applicationType='OE' and cmrHouseoldId IS NOT NULL AND id NOT IN (Select ssapApplicationId FROM RenewalApplication)")
	public List<SsapApplication> getAutoRenewalApplicationsForNextCoverageYear(@Param("coverageYear") long coverageYear,Pageable pageable);
	
	@Query("Select id from SsapApplication WHERE coverageYear = :coverageYear AND applicationStatus ='ER' and applicationType='OE' and cmrHouseoldId IS NOT NULL AND id NOT IN (Select ssapApplicationId FROM RenewalApplication)")
	public List<Long> getSsapApplicationIdsForRenewal(@Param("coverageYear") long coverageYear,Pageable pageable);
	
	@Query("Select id from SsapApplication WHERE coverageYear = :coverageYear AND applicationStatus ='ER' and applicationType='OE' and cmrHouseoldId IS NOT NULL AND id NOT IN (Select ssapApplicationId FROM RenewalApplication WHERE ssapApplicationId IS NOT NULL) and MOD(ID, :serverCount)=:serverName ORDER BY ID DESC")
	public List<Long> getSsapApplicationIdsByServerName(@Param("coverageYear") long coverageYear, @Param("serverCount") int serverCount,  @Param("serverName") int serverName, Pageable pageable);
	
	@Query("from SsapApplication WHERE id = :id AND coverageYear = :coverageYear AND applicationStatus ='ER' and applicationType='OE' and cmrHouseoldId IS NOT NULL")
	public SsapApplication findByIdAndCoverageYear(@Param("coverageYear") long coverageYear,@Param("id") long id);
	
	@Query("from SsapApplication WHERE coverageYear = :coverageYear AND applicationStatus = 'ER' and applicationType='OE' and cmrHouseoldId IS NOT NULL AND id IN (Select ssapApplicationId FROM RenewalApplication WHERE householdRenewalStatus = 'ERROR')")
	public List<SsapApplication> getAutoRenewalApplicationsForNextCoverageYearWithErrors(@Param("coverageYear") long coverageYear,Pageable pageable);

	@Query("Select id from SsapApplication WHERE coverageYear = :coverageYear AND applicationStatus = 'ER' and applicationType='OE' and cmrHouseoldId IS NOT NULL AND id IN (Select ssapApplicationId FROM RenewalApplication WHERE householdRenewalStatus = 'ERROR') and MOD(ID, :serverCount)=:serverName ORDER BY ID DESC")
	public List<Long> getSsapApplicationIdsForErrorProcessing(@Param("coverageYear") long coverageYear, @Param("serverCount") int serverCount,  @Param("serverName") int serverName);

	@Query("FROM SsapApplication WHERE id IN (:applicationIdList)")
	List<SsapApplication> getSsapApplicationListByIds(@Param("applicationIdList") List<Long> applicationIdList);
	
	@Query("Select application "
			+ " FROM SsapApplication as application "
			+ " INNER JOIN fetch application.ssapApplicants as applicant"
			+ " where applicant.applicantGuid = :applicantGuid"
			+ " and applicant.personId = 1 "
			+ " and application.applicationType in ('OE', 'QEP') and application.eligibilityStatus in ('AE', 'AX') "
			+ " order by application.coverageYear desc")
	List<SsapApplication> findbyApplicantGuidForPrimary(@Param("applicantGuid") String applicantGuid);
	
	@Query("Select application "
			+ " FROM SsapApplication as application "
			+ " INNER JOIN fetch application.ssapApplicants as applicant"
			+ " where applicant.externalApplicantId = :externalApplicantId"
			+ " and applicant.personId = 1 and application.financialAssistanceFlag = 'Y' "
			+ " and application.applicationStatus = 'UC' and application.applicationType in ('OE', 'QEP') and application.eligibilityStatus = 'AE' "
			+ " order by application.coverageYear desc")
	List<SsapApplication> findbyMedicaidIdForPrimary(@Param("externalApplicantId") String externalApplicantId);

	@Query("Select application " +
			" FROM SsapApplication as application "+
			" INNER JOIN fetch application.ssapApplicants as applicant"+
			" where application.id = :ssapApplicationId and applicant.personId = 1 order by application.creationTimestamp desc" )
	public List<SsapApplication> findByApplicationIdForPrimary(@Param("ssapApplicationId") Long ssapApplicationId);


	//@Query("from SsapApplication where id = (SELECT MAX(id) from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear and applicationStatus not in ('CL', 'CC'))")
	@Query("select count(*) from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear and applicationStatus not in ('CL', 'CC'))")
	public Long findOpenApplicationByCoverageYearAndCmrHouseoldId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);

	@Query("from SsapApplication where id = (SELECT MAX(id) from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear and applicationStatus = 'EN')")
	public SsapApplication findEnrolledApplicationByMaxIdCoverageYearAndCmrHouseoldId(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);		

	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationStatus = :applicationStatus, lastUpdateTimestamp = :lastUpdateTimestamp, lastUpdatedBy = :lastUpdatedBy where caseNumber = :caseNumber")
	void updateApplicationStatus(@Param("applicationStatus") String applicationStatus, @Param("caseNumber") String caseNumber, @Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp, @Param("lastUpdatedBy") BigDecimal lastUpdatedBy);

	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationStatus = :applicationStatus, lastUpdateTimestamp = :lastUpdateTimestamp, lastUpdatedBy = :lastUpdatedBy where id = :ssapApplicationId")
	int updateApplicationStatusById(@Param("applicationStatus") String applicationStatus, @Param("ssapApplicationId") long ssapApplicationId, @Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp, @Param("lastUpdatedBy") BigDecimal lastUpdatedBy);

	@Query("select id from SsapApplication where renewalStatus = :renewalStatus and source = :applicationType and coverageYear = :coverageYear and eligibilityStatus = :eligibilityStatus and applicationStatus = :applicationStatus")
	List<Long> findSsapApplicationIdsByRenewalStatus(
			@Param("renewalStatus") RenewalStatus renewalStatus,
			@Param("applicationType") String applicationType,
			@Param("coverageYear") long coverageYear,
			@Param("eligibilityStatus") EligibilityStatus eligibilityStatus,
			@Param("applicationStatus") String applicationStatus);


	@Modifying
	@Transactional
	@Query("update SsapApplication set nativeAmerican = :nativeAmerican, lastUpdateTimestamp = :lastUpdateTimestamp where id = :ssapApplicationId")
	int updateNativeAmericanEligibilityStatus(@Param("nativeAmerican") String nativeAmerican, @Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp,  @Param("ssapApplicationId") long ssapApplicationId);

	/*@Query(value="select erSsap.id as currentApplicationId, enSsap.id  as enrolledApplicationId,enSsap.CMR_HOUSEOLD_ID as cmrHouseholdId from SSAP_APPLICATIONS erSsap join SSAP_APPLICATIONS enSsap on enSsap.CMR_HOUSEOLD_ID = erSsap.CMR_HOUSEOLD_ID join SSAP_APPLICATION_EVENTS erSae on erSsap.id = erSae.SSAP_APPLICATION_ID join enrollment e on e.SSAP_APPLICATION_ID = enSsap.id join PLAN_HEALTH pHealth on e.PLAN_ID = pHealth.PLAN_ID where erSsap.APPLICATION_TYPE = 'SEP' and erSsap.APPLICATION_STATUS = 'ER' and trunc(erSae.ENROLLMENT_END_DATE) < trunc(sysdate) and enSsap.APPLICATION_STATUS = 'EN' and pHealth.COST_SHARING <> erSsap.CSR_LEVEL and pHealth.COST_SHARING <> 'CS1' and erSsap.coverage_year = enSsap.coverage_year and enSsap.financial_assistance_flag = 'Y' and erSsap.financial_assistance_flag = 'Y'",nativeQuery=true)
	List<Object[]> findCSPlanChangedApplications();

	@Query(value = "select distinct erSsap.id as currentApplicationId, enSsap.id  as enrolledApplicationId,enSsap.CMR_HOUSEOLD_ID as cmrHouseholdId from SSAP_APPLICATIONS erSsap join SSAP_APPLICATIONS enSsap on enSsap.CMR_HOUSEOLD_ID = erSsap.CMR_HOUSEOLD_ID join SSAP_APPLICATION_EVENTS erSae on erSsap.id = erSae.SSAP_APPLICATION_ID join SSAP_APPLICANTS erApplicant on erApplicant.SSAP_APPLICATION_ID = erSsap.ID where erSsap.APPLICATION_TYPE = 'SEP' and erSsap.APPLICATION_STATUS = 'ER' and trunc(erSae.ENROLLMENT_END_DATE) < trunc(sysdate) and erSsap.coverage_year = enSsap.coverage_year and enSsap.APPLICATION_STATUS = 'EN' and erApplicant.STATUS = 'UPDATED_ZIP_COUNTY' and enSsap.financial_assistance_flag = 'Y' and erSsap.financial_assistance_flag = 'Y'",nativeQuery=true)
	List<Object[]> findCSGeoLocaleChangedApplications();*/

	@Query("select erSsap.id as currentApplicationId, enSsap.id  as enrolledApplicationId, enSsap.cmrHouseoldId as cmrHouseholdId from SsapApplication erSsap, SsapApplication enSsap, SsapApplicationEvent erSae, Enrollment e, PlanHealth pHealth where enSsap.cmrHouseoldId = erSsap.cmrHouseoldId and erSsap.id = erSae.ssapApplication.id and e.ssapApplicationid = enSsap.id and e.planId = pHealth.plan.id and erSsap.applicationType = 'SEP' and erSsap.applicationStatus = 'ER' and trunc(erSae.enrollmentEndDate) < trunc(:enrollmentEndDate) and enSsap.applicationStatus = 'EN' and (pHealth.costSharing <> erSsap.csrLevel or (pHealth.costSharing is null and erSsap.csrLevel is not null) or (pHealth.costSharing is not null and erSsap.csrLevel is null)) and pHealth.costSharing <> 'CS1' and erSsap.coverageYear = enSsap.coverageYear")
	List<Object[]> findCSPlanChangedApplications_Oracle( @Param("enrollmentEndDate") Timestamp enrollmentEndDate);

	@Query("SELECT DISTINCT erSsap.id AS currentApplicationId, enSsap.id AS enrolledApplicationId, enSsap.cmrHouseoldId  AS cmrHouseholdId FROM SsapApplication erSsap, SsapApplication enSsap, SsapApplicationEvent erSae, SsapApplicant erApplicant WHERE enSsap.cmrHouseoldId = erSsap.cmrHouseoldId and erSsap.id = erSae.ssapApplication.id and erApplicant.ssapApplication.id = erSsap.id and erSsap.applicationType = 'SEP' AND erSsap.applicationStatus = 'ER' AND trunc(erSae.enrollmentEndDate) < trunc(:enrollmentEndDate) AND erSsap.coverageYear = enSsap.coverageYear AND enSsap.applicationStatus = 'EN' AND erApplicant.status = 'UPDATED_ZIP_COUNTY' AND enSsap.financialAssistanceFlag = 'Y' AND erSsap.financialAssistanceFlag = 'Y'")
	List<Object[]> findCSGeoLocaleChangedApplications_Oracle( @Param("enrollmentEndDate") Timestamp enrollmentEndDate);


	@Query("Select erSsap.id as currentApplicationId, erSsap.caseNumber, erSae.enrollmentStartDate, erSae.enrollmentEndDate, erSsap.coverageYear "
			+ " FROM SsapApplication as erSsap "
			+ " LEFT JOIN erSsap.ssapApplicationEvents as erSae"
			+ " where erSsap.applicationType in ('QEP', 'SEP') and erSsap.applicationStatus = 'ER' "
			+ " and ((trunc(erSsap.creationTimestamp) < trunc(:creationDate) and erSae.enrollmentEndDate is null) "
			+ " or (trunc(erSae.enrollmentEndDate) < trunc(:enrollmentEndDate))) ")
	List<Object[]> findOverDueSEPQEP_Oracle( @Param("creationDate") Timestamp creationDate, @Param("enrollmentEndDate") Timestamp enrollmentEndDate);

	
	@Query("select erSsap.id as currentApplicationId, enSsap.id  as enrolledApplicationId, enSsap.cmrHouseoldId as cmrHouseholdId from SsapApplication erSsap, SsapApplication enSsap, SsapApplicationEvent erSae, Enrollment e, PlanHealth pHealth where enSsap.cmrHouseoldId = erSsap.cmrHouseoldId and erSsap.id = erSae.ssapApplication.id and e.ssapApplicationid = enSsap.id and e.planId = pHealth.plan.id and erSsap.applicationType = 'SEP' and erSsap.applicationStatus = 'ER' and date(date_trunc('day',erSae.enrollmentEndDate)) < date(date_trunc('day',cast(:enrollmentEndDate as timestamp))) and enSsap.applicationStatus = 'EN' and (pHealth.costSharing <> erSsap.csrLevel or (pHealth.costSharing is null and erSsap.csrLevel is not null) or (pHealth.costSharing is not null and erSsap.csrLevel is null)) and pHealth.costSharing <> 'CS1' and erSsap.coverageYear = enSsap.coverageYear")
	List<Object[]> findCSPlanChangedApplications_Postgres( @Param("enrollmentEndDate") Timestamp enrollmentEndDate);

	@Query("SELECT DISTINCT erSsap.id AS currentApplicationId, enSsap.id AS enrolledApplicationId, enSsap.cmrHouseoldId  AS cmrHouseholdId FROM SsapApplication erSsap, SsapApplication enSsap, SsapApplicationEvent erSae, SsapApplicant erApplicant WHERE enSsap.cmrHouseoldId = erSsap.cmrHouseoldId and erSsap.id = erSae.ssapApplication.id and erApplicant.ssapApplication.id = erSsap.id and erSsap.applicationType = 'SEP' AND erSsap.applicationStatus = 'ER' AND date(date_trunc('day',erSae.enrollmentEndDate)) < date(date_trunc('day',cast(:enrollmentEndDate as timestamp))) AND erSsap.coverageYear = enSsap.coverageYear AND enSsap.applicationStatus = 'EN' AND erApplicant.status = 'UPDATED_ZIP_COUNTY' AND enSsap.financialAssistanceFlag = 'Y' AND erSsap.financialAssistanceFlag = 'Y'")
	List<Object[]> findCSGeoLocaleChangedApplications_Postgres( @Param("enrollmentEndDate") Timestamp enrollmentEndDate);
	
	@Query("Select erSsap.id as currentApplicationId, erSsap.caseNumber, erSae.enrollmentStartDate, erSae.enrollmentEndDate, erSsap.coverageYear "
			+ " FROM SsapApplication as erSsap "
			+ " LEFT JOIN erSsap.ssapApplicationEvents as erSae"
			+ " where erSsap.applicationType in ('QEP', 'SEP') and erSsap.applicationStatus = 'ER' "
			+ " and ((date(date_trunc('day',erSsap.creationTimestamp)) < date(date_trunc('day',cast(:creationDate as timestamp))) and erSae.enrollmentEndDate is null) "
			+ " or (date(date_trunc('day',erSae.enrollmentEndDate)) < date(date_trunc('day',cast(:enrollmentEndDate as timestamp))))) ")
	List<Object[]> findOverDueSEPQEP_Postgres( @Param("creationDate") Timestamp creationDate, @Param("enrollmentEndDate") Timestamp enrollmentEndDate);
	
	@Query("Select erSsap.id as currentApplicationId from SsapApplication as erSsap where erSsap.caseNumber = :casenumber and erSsap.applicationStatus = 'ER' ")
	Object[] findApplicationByCaseNumberAndERStatus(@Param("casenumber") String casenumber);
	
	/**
	 * This query has been modified to ignore Queued and DeQueued applications so that existing functionality remains unaffected.
	 * These two statuses were introduced as part of MN dev - multiple eligibility spans.
	 * @param householdId
	 * @return
	 */
	@Query("Select application "
			+ " FROM SsapApplication as application"
			+ " LEFT JOIN fetch application.ssapApplicants as applicant"
			+ " WHERE application.cmrHouseoldId = :householdId and application.applicationStatus not in ('QU' ,'DQ') "
			+ " ORDER BY application.creationTimestamp DESC")
	List<SsapApplication> findLatestSsapApplicationByHouseholdId(@Param("householdId") BigDecimal householdId);
	
	@Query("Select application "
			+ " FROM SsapApplication as application"
			+ " INNER JOIN fetch application.ssapApplicants as applicant"
			+ " where application.applicationStatus = 'UC' "
			+ " and application.applicationType in ('OE', 'QEP') and applicant.personId = 1 "
			+ " and applicant.firstName = :firstName and applicant.lastName = :lastName and applicant.birthDate = :birthDate and applicant.ssn = :ssn"
			+ " order by application.coverageYear desc ")
	List<SsapApplication> findMatchingApplication(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("birthDate") Date birthDate , @Param("ssn") String ssn);

	@Query("Select application "
			+ " FROM SsapApplication as application , Household as household "
			+ " WHERE application.cmrHouseoldId =  household.id AND  household.householdCaseId = :hhCaseId ORDER BY application.creationTimestamp DESC")
	public List<SsapApplication> findByHHCaseId(@Param("hhCaseId") String hhCaseId);
	
	@RestResource(path="getApplicationIdByCaseNumber")
	@Query("Select id FROM SsapApplication as application where application.caseNumber = :caseNumber ")
	public Long getApplicationIdByCaseNumber(@Param("caseNumber") String caseNumber);
	
	
	/**
	 * This query has been modified to ignore Queued and DeQueued applications so that existing functionality remains unaffected.
	 * These two statuses were introduced as part of MN dev - multiple eligibility spans.
	 * @param hhId
	 * @param coverageYear
	 * @return
	 */
	@Query("Select application "
			+ " FROM SsapApplication as application , Household as household "
			+ " WHERE application.cmrHouseoldId =  household.id AND  application.cmrHouseoldId = :hhId "
			+ " AND application.applicationStatus not in ('QU' ,'DQ') "
			+ " AND application.coverageYear= :coverageYear ORDER BY application.creationTimestamp DESC")
	public List<SsapApplication> findByHHIdAndCoverageYear(@Param("hhId") BigDecimal hhId,@Param("coverageYear") Long coverageYear);

	/**
	 * This query has been modified to ignore Queued and DeQueued applications so that existing functionality remains unaffected.
	 * These two statuses were introduced as part of MN dev - multiple eligibility spans.
	 * @param hhCaseId
	 * @param coverageYear
	 * @return
	 */
	@Query("Select application "
			+ " FROM SsapApplication as application , Household as household "
			+ " WHERE application.cmrHouseoldId =  household.id AND  household.householdCaseId = :hhCaseId "
			+ " AND application.applicationStatus not in ('QU' ,'DQ') "
			+ " AND application.coverageYear= :coverageYear ORDER BY application.creationTimestamp DESC")
	public List<SsapApplication> findByHHCaseIdAndCoverageYear(@Param("hhCaseId") String hhCaseId,@Param("coverageYear") Long coverageYear);

	public SsapApplication findById(long id);
	
	@Query("Select applicationData FROM SsapApplication WHERE id=:id")
	public String getApplicationDataById(@Param("id") Long householdId);	
	@Query(" SELECT EE.externalApplicantId, EE.firstName, EE.lastName, EE.birthDate, CMR.householdCaseId, APP.coverageYear "
			+ " FROM SsapApplicant AS EE, Household AS CMR, SsapApplication AS APP where EE.ssapApplication = APP.id "
			+ " AND CMR.id = APP.cmrHouseoldId  AND EE.personType in ('PTF','PC_PTF') AND APP.id = :ssapApplicationId")
	public Object[] getSsapApplicationDetails(@Param("ssapApplicationId")long ssapApplicationId);
	
	@Query("Select application from SsapApplication as application where application.cmrHouseoldId = :cmrHouseoldId and application.applicationStatus = 'ER' and application.coverageYear = :coverageYear and application.creationTimestamp > ( select max(creationTimestamp ) FROM  SsapApplication  where  cmrHouseoldId = :cmrHouseoldId and  applicationStatus in ( 'EN' , 'PN' ) and  coverageYear = :coverageYear  ) ")
	public  SsapApplication findErAfterLatestEnPnSsapApplicationForCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);
	
	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationDentalStatus = :applicationStatus, lastUpdateTimestamp = :lastUpdateTimestamp where id = :ssapApplicationId")
	int updateDentalApplicationStatusById(@Param("applicationStatus") String applicationStatus, @Param("ssapApplicationId") long ssapApplicationId, @Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);
	
	@Query(   " SELECT US.id "
			+ " FROM  AccountUser AS US "
			+ " where US.extnAppUserId = :externalAssisterId ")
	public Integer getUserIdForExternalAssisterId(@Param("externalAssisterId")String externalAssisterId);
	
	@Query("Select application from SsapApplication  as application where  application.applicationStatus = 'ER' AND application.applicationDentalStatus = 'EN' "
			+ " and application.id != :ssapApplicationId  and application.cmrHouseoldId = :cmrHouseoldId and application.coverageYear = :coverageYear ")
	public  SsapApplication findOnlyDentalEnrolledSsapApplicationForCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear,@Param("ssapApplicationId") long ssapApplicationId);
	
	@Query("Select applicationStatus from SsapApplication where id=:ssapApplicationId")
	public String getApplicationStatusById(@Param("ssapApplicationId") Long ssapApplicationId);
	
	@Query("Select caseNumber from SsapApplication where id=:id")
	public String findCaseNumberById(@Param("id") Long applicationId);
	
	@Query("Select application from SsapApplication as application where application.cmrHouseoldId = :cmrHouseoldId and application.applicationStatus in ( 'EN' , 'PN' ) and application.coverageYear = :coverageYear and application.creationTimestamp = ( select max(creationTimestamp ) FROM  SsapApplication  where  cmrHouseoldId = :cmrHouseoldId and  applicationStatus in ( 'EN' , 'PN' ) and  coverageYear = :coverageYear  ) ")
	public SsapApplication findEnPnSsapApplicationForCoverageYear(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear);

	@Query("Select id from SsapApplication WHERE coverageYear = :coverageYear AND applicationStatus ='SG' and cmrHouseoldId IS NOT NULL")
	public List<Long> getSsapAppIdsForRenewalWithSgStatus(@Param("coverageYear") long coverageYear,Pageable pageable);
	
	@Query("Select application from SsapApplication as application WHERE application.source = :source AND application.cmrHouseoldId = :cmrHouseoldId order by application.creationTimestamp desc")
	public List<SsapApplication> getAppsBySourceAndHousehold(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("source") String source);

	@Query("SELECT sa.id " +
			"FROM SsapApplication sa " +
			"WHERE sa.applicationStatus = :applicationStatus AND sa.coverageYear = :coverageYear " +
			"ORDER BY sa.id")
	Page<Long> findAllEligibleSsapApplication(@Param("coverageYear") long coverageYear,
											  @Param("applicationStatus") String applicationStatus,
											  Pageable pageable);
	
	@Query("Select application.cmrHouseoldId "
			+ " FROM SsapApplication as application "
			+ " left JOIN application.ssapApplicants as applicant"
			+ " where applicant.id = :applicantId")
	BigDecimal getHouseholdIdByApplicantId(@Param("applicantId") Long applicantId);
	
	
	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationStatus = :applicationStatus, applicationDentalStatus = :applicationDentalStatus, lastUpdateTimestamp = :lastUpdateTimestamp where id = :ssapApplicationId")
	int updateApplicationAndDentalStatusById(@Param("applicationStatus") String applicationStatus,
			@Param("applicationDentalStatus") String applicationDentalStatus,
			@Param("ssapApplicationId") long ssapApplicationId,
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);

	@Query("SELECT DISTINCT application.id "
			+ "FROM SsapApplication as application, "
			+ "SsapApplicant as applicant, "
			+ "EligibilityProgram as ep "
			+ "WHERE application.id = applicant.ssapApplication "
			+ "AND applicant.id = ep.ssapApplicant.id "
			+ "AND application.coverageYear = :coverageYear "
			+ "AND application.id NOT IN (SELECT outbound.ssapApplicationId FROM OutboundATApplication AS outbound) "
			+ "AND application.applicationStatus IN (:applicationStatusList) "
			+ "AND ep.eligibilityType IN ('AssessedCHIPEligibilityType','AssessedMedicaidMAGIEligibilityType', 'AssessedMedicaidNonMAGIEligibilityType') "
			+ "AND ep.eligibilityIndicator = 'TRUE' ")	
	List<Long> getSsapApplIDsByCoverageYearAndApplStatusAndEligStatus(@Param("coverageYear") long coverageYear,
			@Param("applicationStatusList") List<String> applicationStatusList, Pageable pageable);
	
	@Modifying
	@Transactional
	@Query("update SsapApplication set applicationDentalStatus = null , lastUpdateTimestamp = :lastUpdateTimestamp, lastUpdatedBy = :lastUpdatedBy where caseNumber = :caseNumber")
	void updateDentalApplicationStatus(@Param("caseNumber") String caseNumber, @Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp, @Param("lastUpdatedBy") BigDecimal lastUpdatedBy);
	
	@Query("Select id from SsapApplication where cmrHouseoldId = :cmrHouseoldId and coverageYear = :coverageYear and applicationStatus in (:statusList) and id <> :appId ")
	public List<Long> getApplicatonIdForCMRIdandStatus(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear, @Param("statusList") List<String> statusList, @Param("appId") Long appId );
}
