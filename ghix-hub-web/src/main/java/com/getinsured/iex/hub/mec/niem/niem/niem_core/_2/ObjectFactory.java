//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.02.26 at 10:06:56 AM PST 
//


package com.getinsured.iex.hub.mec.niem.niem.niem_core._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.iex.hub.mec.niem.niem.fbi._2.SEXCodeType;
import com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.String;



/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.getinsured.iex.hub.mec.niem.niem.niem_core._2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StartDate_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "StartDate");
    private final static QName _PersonBirthDate_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonBirthDate");
    private final static QName _Date_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "Date");
    private final static QName _PersonGivenName_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonGivenName");
    private final static QName _DateRepresentation_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "DateRepresentation");
    private final static QName _PersonName_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonName");
    private final static QName _IdentificationID_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "IdentificationID");
    private final static QName _PersonSexCode_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonSexCode");
    private final static QName _PersonSex_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonSex");
    private final static QName _PersonMiddleName_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonMiddleName");
    private final static QName _EndDate_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "EndDate");
    private final static QName _PersonSSNIdentification_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonSSNIdentification");
    private final static QName _PersonNameSuffixText_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonNameSuffixText");
    private final static QName _PersonSurName_QNAME = new QName("http://niem.gov/niem/niem-core/2.0", "PersonSurName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.getinsured.iex.hub.mec.niem.niem.niem_core._2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TextType }
     * 
     */
    public TextType createTextType() {
        return new TextType();
    }

    /**
     * Create an instance of {@link PersonNameType }
     * 
     */
    public PersonNameType createPersonNameType() {
        return new PersonNameType();
    }

    /**
     * Create an instance of {@link IdentificationType }
     * 
     */
    public IdentificationType createIdentificationType() {
        return new IdentificationType();
    }

    /**
     * Create an instance of {@link PersonNameTextType }
     * 
     */
    public PersonNameTextType createPersonNameTextType() {
        return new PersonNameTextType();
    }

    /**
     * Create an instance of {@link DateType }
     * 
     */
    public DateType createDateType() {
        return new DateType();
    }

    /**
     * Create an instance of {@link DateRangeType }
     * 
     */
    public DateRangeType createDateRangeType() {
        return new DateRangeType();
    }

    /**
     * Create an instance of {@link NumericType }
     * 
     */
    public NumericType createNumericType() {
        return new NumericType();
    }

    /**
     * Create an instance of {@link AmountType }
     * 
     */
    public AmountType createAmountType() {
        return new AmountType();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link PercentageType }
     * 
     */
    public PercentageType createPercentageType() {
        return new PercentageType();
    }

    /**
     * Create an instance of {@link ProperNameTextType }
     * 
     */
    public ProperNameTextType createProperNameTextType() {
        return new ProperNameTextType();
    }

    /**
     * Create an instance of {@link QuantityType }
     * 
     */
    public QuantityType createQuantityType() {
        return new QuantityType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "StartDate")
    public JAXBElement<DateType> createStartDate(DateType value) {
        return new JAXBElement<DateType>(_StartDate_QNAME, DateType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonBirthDate")
    public JAXBElement<DateType> createPersonBirthDate(DateType value) {
        return new JAXBElement<DateType>(_PersonBirthDate_QNAME, DateType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Date }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "Date", substitutionHeadNamespace = "http://niem.gov/niem/niem-core/2.0", substitutionHeadName = "DateRepresentation")
    public JAXBElement<Date> createDate(Date value) {
        return new JAXBElement<Date>(_Date_QNAME, Date.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonNameTextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonGivenName")
    public JAXBElement<PersonNameTextType> createPersonGivenName(PersonNameTextType value) {
        return new JAXBElement<PersonNameTextType>(_PersonGivenName_QNAME, PersonNameTextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "DateRepresentation")
    public JAXBElement<Object> createDateRepresentation(Object value) {
        return new JAXBElement<Object>(_DateRepresentation_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonName")
    public JAXBElement<PersonNameType> createPersonName(PersonNameType value) {
        return new JAXBElement<PersonNameType>(_PersonName_QNAME, PersonNameType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "IdentificationID")
    public JAXBElement<String> createIdentificationID(String value) {
        return new JAXBElement<String>(_IdentificationID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SEXCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonSexCode", substitutionHeadNamespace = "http://niem.gov/niem/niem-core/2.0", substitutionHeadName = "PersonSex")
    public JAXBElement<SEXCodeType> createPersonSexCode(SEXCodeType value) {
        return new JAXBElement<SEXCodeType>(_PersonSexCode_QNAME, SEXCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonSex")
    public JAXBElement<Object> createPersonSex(Object value) {
        return new JAXBElement<Object>(_PersonSex_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonNameTextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonMiddleName")
    public JAXBElement<PersonNameTextType> createPersonMiddleName(PersonNameTextType value) {
        return new JAXBElement<PersonNameTextType>(_PersonMiddleName_QNAME, PersonNameTextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "EndDate")
    public JAXBElement<DateType> createEndDate(DateType value) {
        return new JAXBElement<DateType>(_EndDate_QNAME, DateType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonSSNIdentification")
    public JAXBElement<IdentificationType> createPersonSSNIdentification(IdentificationType value) {
        return new JAXBElement<IdentificationType>(_PersonSSNIdentification_QNAME, IdentificationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonNameSuffixText")
    public JAXBElement<TextType> createPersonNameSuffixText(TextType value) {
        return new JAXBElement<TextType>(_PersonNameSuffixText_QNAME, TextType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonNameTextType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://niem.gov/niem/niem-core/2.0", name = "PersonSurName")
    public JAXBElement<PersonNameTextType> createPersonSurName(PersonNameTextType value) {
        return new JAXBElement<PersonNameTextType>(_PersonSurName_QNAME, PersonNameTextType.class, null, value);
    }

}
