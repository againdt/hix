package com.getinsured.ssap.model.financial;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Describes IFSV Applicant Verification details.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonRootName("TaxReturn")
public class IfsvApplicantVerification {
  @JsonProperty("TaxReturnYear")
  private int taxReturnYear;

  @JsonProperty("TaxReturnTaxableSocialSecurityBenefitsAmount")
  private long taxableSsnBenefits;

  @JsonProperty("TaxReturnTotalExemptionsQuantity")
  private int totalExcemptionsQuantity;

  @JsonProperty("TaxReturnMAGIAmount")
  private long magiAmount;

  @JsonProperty("TaxReturnFilingStatusCode")
  private int filingStatusCode;

  // TODO: Add all fields

  public int getTaxReturnYear() {
    return taxReturnYear;
  }

  public void setTaxReturnYear(int taxReturnYear) {
    this.taxReturnYear = taxReturnYear;
  }

  public long getTaxableSsnBenefits() {
    return taxableSsnBenefits;
  }

  public void setTaxableSsnBenefits(long taxableSsnBenefits) {
    this.taxableSsnBenefits = taxableSsnBenefits;
  }

  public int getTotalExcemptionsQuantity() {
    return totalExcemptionsQuantity;
  }

  public void setTotalExcemptionsQuantity(int totalExcemptionsQuantity) {
    this.totalExcemptionsQuantity = totalExcemptionsQuantity;
  }

  public long getMagiAmount() {
    return magiAmount;
  }

  public void setMagiAmount(long magiAmount) {
    this.magiAmount = magiAmount;
  }

  public int getFilingStatusCode() {
    return filingStatusCode;
  }

  public void setFilingStatusCode(int filingStatusCode) {
    this.filingStatusCode = filingStatusCode;
  }
}
