//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for
 * 				attestations made when submitting a Single Streamlined Form (SSF).
 * 			
 * 
 * <p>Java class for SSFAttestationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSFAttestationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SSFAttestationCollectionsAgreementIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SSFAttestationMedicaidObligationsIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SSFAttestationNonPerjuryIndicator"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SSFAttestationNotIncarceratedIndicator" maxOccurs="unbounded"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SSFAttestationPrivacyAgreementIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SSFAttestationPendingChargesIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SSFAttestationInformationChangesIndicator"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}SSFAttestationApplicationTermsIndicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSFAttestationType", propOrder = {
    "ssfAttestationCollectionsAgreementIndicator",
    "ssfAttestationMedicaidObligationsIndicator",
    "ssfAttestationNonPerjuryIndicator",
    "ssfAttestationNotIncarceratedIndicator",
    "ssfAttestationPrivacyAgreementIndicator",
    "ssfAttestationPendingChargesIndicator",
    "ssfAttestationInformationChangesIndicator",
    "ssfAttestationApplicationTermsIndicator"
})
public class SSFAttestationType
    extends ComplexObjectType
{

    @XmlElement(name = "SSFAttestationCollectionsAgreementIndicator")
    protected Boolean ssfAttestationCollectionsAgreementIndicator;
    @XmlElement(name = "SSFAttestationMedicaidObligationsIndicator")
    protected Boolean ssfAttestationMedicaidObligationsIndicator;
    @XmlElement(name = "SSFAttestationNonPerjuryIndicator")
    protected Boolean ssfAttestationNonPerjuryIndicator;
    @XmlElement(name = "SSFAttestationNotIncarceratedIndicator")
    protected List<Boolean> ssfAttestationNotIncarceratedIndicator;
    @XmlElement(name = "SSFAttestationPrivacyAgreementIndicator")
    protected Boolean ssfAttestationPrivacyAgreementIndicator;
    @XmlElement(name = "SSFAttestationPendingChargesIndicator")
    protected Boolean ssfAttestationPendingChargesIndicator;
    @XmlElement(name = "SSFAttestationInformationChangesIndicator")
    protected Boolean ssfAttestationInformationChangesIndicator;
    @XmlElement(name = "SSFAttestationApplicationTermsIndicator")
    protected Boolean ssfAttestationApplicationTermsIndicator;

    /**
     * Gets the value of the ssfAttestationCollectionsAgreementIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSFAttestationCollectionsAgreementIndicator() {
        return ssfAttestationCollectionsAgreementIndicator;
    }

    /**
     * Sets the value of the ssfAttestationCollectionsAgreementIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSFAttestationCollectionsAgreementIndicator(Boolean value) {
        this.ssfAttestationCollectionsAgreementIndicator = value;
    }

    /**
     * Gets the value of the ssfAttestationMedicaidObligationsIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSFAttestationMedicaidObligationsIndicator() {
        return ssfAttestationMedicaidObligationsIndicator;
    }

    /**
     * Sets the value of the ssfAttestationMedicaidObligationsIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSFAttestationMedicaidObligationsIndicator(Boolean value) {
        this.ssfAttestationMedicaidObligationsIndicator = value;
    }

    /**
     * Gets the value of the ssfAttestationNonPerjuryIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSFAttestationNonPerjuryIndicator() {
        return ssfAttestationNonPerjuryIndicator;
    }

    /**
     * Sets the value of the ssfAttestationNonPerjuryIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSFAttestationNonPerjuryIndicator(Boolean value) {
        this.ssfAttestationNonPerjuryIndicator = value;
    }

    /**
     * Gets the value of the ssfAttestationNotIncarceratedIndicator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ssfAttestationNotIncarceratedIndicator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSSFAttestationNotIncarceratedIndicator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Boolean }
     * 
     * 
     */
    public List<Boolean> getSSFAttestationNotIncarceratedIndicator() {
        if (ssfAttestationNotIncarceratedIndicator == null) {
            ssfAttestationNotIncarceratedIndicator = new ArrayList<Boolean>();
        }
        return this.ssfAttestationNotIncarceratedIndicator;
    }

    /**
     * Gets the value of the ssfAttestationPrivacyAgreementIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSFAttestationPrivacyAgreementIndicator() {
        return ssfAttestationPrivacyAgreementIndicator;
    }

    /**
     * Sets the value of the ssfAttestationPrivacyAgreementIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSFAttestationPrivacyAgreementIndicator(Boolean value) {
        this.ssfAttestationPrivacyAgreementIndicator = value;
    }

    /**
     * Gets the value of the ssfAttestationPendingChargesIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSFAttestationPendingChargesIndicator() {
        return ssfAttestationPendingChargesIndicator;
    }

    /**
     * Sets the value of the ssfAttestationPendingChargesIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSFAttestationPendingChargesIndicator(Boolean value) {
        this.ssfAttestationPendingChargesIndicator = value;
    }

    /**
     * Gets the value of the ssfAttestationInformationChangesIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSFAttestationInformationChangesIndicator() {
        return ssfAttestationInformationChangesIndicator;
    }

    /**
     * Sets the value of the ssfAttestationInformationChangesIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSFAttestationInformationChangesIndicator(Boolean value) {
        this.ssfAttestationInformationChangesIndicator = value;
    }

    /**
     * Gets the value of the ssfAttestationApplicationTermsIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSFAttestationApplicationTermsIndicator() {
        return ssfAttestationApplicationTermsIndicator;
    }

    /**
     * Sets the value of the ssfAttestationApplicationTermsIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSFAttestationApplicationTermsIndicator(Boolean value) {
        this.ssfAttestationApplicationTermsIndicator = value;
    }

}
