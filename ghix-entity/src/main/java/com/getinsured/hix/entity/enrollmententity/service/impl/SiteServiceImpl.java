package com.getinsured.hix.entity.enrollmententity.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.enrollmententity.repository.ISiteRepository;
import com.getinsured.hix.entity.enrollmententity.service.SiteService;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.Site;

/**
 * Implements {@link SiteService} to perform Enrollment Entity Site related
 * operations like find,save,update etc.
 */
@Service("subSiteService")
@Transactional
public class SiteServiceImpl implements SiteService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteServiceImpl.class);
	
	@Autowired
	private ISiteRepository iSiteRepository;

	/**
	 * @see SiteService#findByEESiteId(int)
	 */
	@Override
	@Transactional
	public Site findByEESiteId(int siteId) {
		
		LOGGER.info("findByEESiteId: START");
		LOGGER.info("findByEESiteId: END");
		
		
		return iSiteRepository.findSiteById(siteId);
	}
	
	/**
	 * @see SiteService#saveSite(Site)
	 */	
	@Override
	@Transactional
	public Site saveSite(Site site) {
		
		LOGGER.info("saveSite: START");
		
	//set physical location as Null if user has not provided physical location details
			if (validateSite(site))
			{
				site.setPhysicalLocation(null);
			}
		LOGGER.info("saveSite: END");
		
		return iSiteRepository.save(site);
	}

	/**
	 * @param site
	 * @return
	 */
	private boolean validateSite(Site site) {
		
		if(site.getPhysicalLocation() == null) {
			return false;
		}
		else if(EntityUtils.isEmpty(site.getPhysicalLocation().getAddress1()) && EntityUtils.isEmpty(site.getPhysicalLocation().getAddress2()) && EntityUtils.isEmpty(site.getPhysicalLocation().getCity())) {
			return false;
		}
		else {
			return EntityUtils.isEmpty(site.getPhysicalLocation().getState()) && site.getPhysicalLocation().getZip() == null && EntityUtils.isEmpty(site.getPhysicalLocation().getCounty());
		}
	}

	/**
	 * @see SiteService#findSiteByEntity(EnrollmentEntity)
	 */	
	@Override
	public List<Site> findSiteByEntity(EnrollmentEntity enrollmentEntity) {
		
		LOGGER.info("findSiteByEntity: START");
		LOGGER.info("findSiteByEntity: END");
		
		
		return iSiteRepository.findSiteByEntity(enrollmentEntity);
	}

	/**
	 * @see SiteService#findByEnrollmentEntityAndSiteType(int)
	 */	
	@Override
	public List<Site> findByEnrollmentEntityAndSiteType(int enrollmentEntityId, String siteType) {
		
		LOGGER.info("findByEnrollmentEntityAndSiteType: START");
		LOGGER.info("findByEnrollmentEntityAndSiteType: END");
		
		return iSiteRepository.findByEnrollmentEntityAndSiteType(enrollmentEntityId, siteType);
	}
}
