package com.getinsured.hix.agency.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.agency.assistant.AgencyAssistantAud;

public interface IAgencyAssistantAudRepository extends JpaRepository<AgencyAssistantAud, Long> {

	@Query(nativeQuery=true, 
			value= "select distinct a.id,a.last_update_date,a.approval_status,c.COMMENT_TEXT,a.rev from AGENCY_ASSISTANT_AUD a "
					+ "left join comments c on c.id=a.COMMENTS_ID "
					+ "where a.id = :agencyAssistantId and a.approval_status is not null order by REV ASC")
	List<Object[]> getAgencyAssistantApprovalHistory(@Param("agencyAssistantId")long agencyAssistantId);
	
	@Query(nativeQuery=true, 
			value= "select distinct a.id,a.LAST_UPDATE_DATE,a.status, c.COMMENT_TEXT, a.rev from AGENCY_ASSISTANT_AUD a "
					+ "left join comments c on c.id=a.ACTIVITY_COMMENTS_ID "
					+ "where a.id = :agencyAssistantId and a.status is not null order by REV ASC")
	List<Object[]> getAgencyAssistantStatusHistory(@Param("agencyAssistantId")long agencyAssistantId);

}
