package com.getinsured.hix.planmgmt.restwebservice;

import junit.framework.TestCase;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.platform.util.exception.GIException;

public class PlanMgmtRestServiceTest extends TestCase {

	private static final Logger logger = LoggerFactory.getLogger(PlanMgmtRestServiceTest.class);
	
	
	private RestTemplate restTemplate;
	
	private static final String REST_SERVICE_URL = "http://localhost:8080/hix/planMgmtService/tranferPlanDTO";

	@Test
	public void testTranferPlanDTO() throws GIException{
		String acknowledgement = "true";
		String jsonRequestObj = "jsonRequestObj";
		

		FileSystemXmlApplicationContext applicationContext = new FileSystemXmlApplicationContext(
				"E:\\CodeBase\\ghix\\ghix-web\\src\\main\\resources\\applicationContext.xml");
		restTemplate = (RestTemplate )applicationContext.getBean("restTemplate");
		
		acknowledgement = restTemplate.postForObject(REST_SERVICE_URL, jsonRequestObj, String.class);
		assertEquals(acknowledgement,"true");
		
	}
}
