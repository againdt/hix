package com.getinsured.eligibility.at.ref.si;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.mes.service.QueueProcessingService;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.eligibility.at.ref.service.ReferralEligibilityDecisionService;
import com.getinsured.eligibility.at.ref.service.ReferralProcessingService;
import com.getinsured.eligibility.at.ref.service.ReferralSsapCmrLinkService;
import com.getinsured.eligibility.at.ref.service.RenewalDecisionService;
import com.getinsured.eligibility.at.ref.service.nonfinancial.ReferralNonFinancialService;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.server.endpoint.ATContextHolder;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AssisterType;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 *
 */
@Component("renewalReferralProcessingService")
@Scope("singleton")
public class RenewalReferralProcessingService {
	private static final Logger LOGGER = Logger.getLogger(RenewalReferralProcessingService.class);

	@Autowired
	@Qualifier("referralProcessingService")
	private ReferralProcessingService referralProcessingService;

	@Autowired
	@Qualifier("referralSsapCmrLinkService")
	private ReferralSsapCmrLinkService referralSsapCmrLinkService;

	@Autowired
	@Qualifier("renewalDecisionService")
	private RenewalDecisionService renewalDecisionService;

	@Autowired
	@Qualifier("referralRenNFDeterminationService")
	private ReferralNonFinancialService referralRenNFDeterminationService;

	@Autowired
	@Qualifier("referralRenNFConversionService")
	private ReferralNonFinancialService referralRenNFConversionService;
	
	@Autowired
	private ExternalAssisterService externalAssisterService;
	
	@Autowired
	private QueueProcessingService queueProcessingService;

	private String REN_DECISION_HEADER = "renDecisionFlow";

	private String REN_NF_CONV_DECISION_HEADER = "rennfConversionFlow";

	public String processReferral(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		AccountTransferRequestDTO accountTransferRequest = null;
		long ssapApplicationId = 0;
		try {
			LOGGER.info("RenewalReferralProcessingService processReferral starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			
			// Retrieve request object from message which was set in step routing step.
			accountTransferRequest = (AccountTransferRequestDTO)referralResponse.getData().get(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID, accountTransferRequest.getApplicationsWithSameId());

			ssapApplicationId = referralProcessingService.executeRenewalReferral(accountTransferRequest);
			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, ssapApplicationId);
			
			referralResponse.getData().put(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID, referralProcessingService.getOrganizationIdentification(accountTransferRequest.getAccountTransferRequestPayloadType()));
			
			// set the HouseholdCaseId in ReferralResponse 
			referralProcessingService.setExternalHouseholdCaseIdInReferralResponse(referralResponse,accountTransferRequest);
			
			// set mes procecssing required fields in referral response 
			referralResponse.getData().put(ReferralProcessingConstants.SPAN_INFO_DTO,queueProcessingService.populateATQueueInfoRequestDTO(accountTransferRequest));

			// config for MN
			final String hasLinkUserStr =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
			// for MN if Assister tag is passed then set it in the map
			if (null != hasLinkUserStr && "TRUE".equalsIgnoreCase(hasLinkUserStr)) {
				if (null != accountTransferRequest.getAccountTransferRequestPayloadType().getAssister()) {
					referralResponse.getData().put(ReferralConstants.ASSISTER_INFO,
							accountTransferRequest.getAccountTransferRequestPayloadType().getAssister());
				}
			}
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_CREATING_REFERRAL_APPLICATION + accountTransferRequest.getGiwsPayloadId());
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

			ATContextHolder.setAccountTransferRequestPayloadType(accountTransferRequest.getAccountTransferRequestPayloadType()); /* set in Thread Local */

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_CREATING_REFERRAL_APPLICATION).append(accountTransferRequest.getGiwsPayloadId()).append(ReferralProcessingConstants.REASON)
			        .append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
		} finally {
			Map<String, Object> data = referralResponse.getData();
			data.put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, accountTransferRequest.getGiwsPayloadId());
			data.put(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID, accountTransferRequest.getCompareToApplicationId());
			data.put(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID, accountTransferRequest.getEnrolledApplicationId());
			data.put(ReferralProcessingConstants.KEY_RENEWAL_COMPARED_TO_APPLICATION_ID, accountTransferRequest.getRenewalCompareToApplicationId());
			data.put(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY, accountTransferRequest.getAccountTransferCategory());
			// Remove request object from message as further steps won't need it. It will be recreated when required.
			data.remove(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
		}

		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("RenewalReferralProcessingService processReferral ends - Response is - " + response);

		return response;
	}

	public String processLinkCmr(Message<String> message) {

		ReferralResponse referralResponse = new ReferralResponse();
		try {
			LOGGER.info("RenewalReferralProcessingService processLinkCmr starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			boolean linkApplication = true;
			String hhCaseId = (String) input.getData().get(ReferralConstants.HOUSE_HOLD_CASE_ID)    ;
			
			Map<String, Object> mpData = null;
			
			if (referralSsapCmrLinkService.hasLinkUserEnable() && null != input.getData().get(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID)) {
				mpData = referralSsapCmrLinkService.executeRenewalLinking((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), linkApplication,hhCaseId,
						input.getData().get(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID).toString());
			}
			else {
				mpData = referralSsapCmrLinkService.executeRenewalLinking((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), linkApplication,hhCaseId);
			}
			
			// call External Assister Designate/De-designate API
			final String hasLinkUserStr = DynamicPropertiesUtil
					.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
			if (null != hasLinkUserStr && "TRUE".equalsIgnoreCase(hasLinkUserStr)) {
				AssisterType assisterInfo = (AssisterType) input.getData().get(ReferralConstants.ASSISTER_INFO);
				if(null != assisterInfo) {
					externalAssisterService.callExternalAssisterDesignateAPI(assisterInfo,(int) mpData.get(ReferralConstants.HOUSEHOLD_ID));	
				} else {
					externalAssisterService.callExternalAssisterDeDesignateAPI((int) mpData.get(ReferralConstants.HOUSEHOLD_ID));
				}
				
			}
			referralResponse.getData().put(ReferralProcessingConstants.REFERRAL_AUTOLINKING, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.AUTO_LINK)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.MULTIPLE_CMR)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.NON_FINANCIAL_CMR)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID, ReferralUtil.convertToInt(mpData.get(ReferralProcessingConstants.NON_FINANCIAL_CMR_ID)));

			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LINK_CMR + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LINK_CMR).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LINK_CMR);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("RenewalReferralProcessingService processLinkCmr ends - Response is - " + response);

		return response;
	}

	public Message<String> executeDecision(Message<String> message) throws Exception {
		String headerValue = ReferralEligibilityDecisionService.ERROR_HANDLER;
		ReferralResponse referralResponse = new ReferralResponse();
		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);
		try {
			LOGGER.info("RenewalReferralProcessingService executeDecision starts for " + erpResponse.getSsapApplicationPrimaryKey());
			final boolean blnComplete = renewalDecisionService.execute(erpResponse.getSsapApplicationPrimaryKey(), erpResponse.isCmrAutoLinking(), erpResponse.isMultipleCmr(), erpResponse.getCompareEnrolledApplicationId(),
			        erpResponse.getRenewalCompareToApplicationId());
			headerValue = blnComplete ? ReferralEligibilityDecisionService.SUCCESS_HANDLER : ReferralEligibilityDecisionService.NF_LINK_HANDLER;
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_RENEWAL_DECISION + erpResponse.getApplicationID());
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_RENEWAL_DECISION).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_RENEWAL_DESCISION);
		} finally {
			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, erpResponse.getSsapApplicationPrimaryKey());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, erpResponse.getGiWsPayloadId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID, erpResponse.getCompareToApplicationId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_RENEWAL_COMPARED_TO_APPLICATION_ID, erpResponse.getRenewalCompareToApplicationId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID, erpResponse.getCompareEnrolledApplicationId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID, erpResponse.getApplicationsWithSameId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATION_EXTENSION, erpResponse.getApplicationExtension());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY, erpResponse.getAccountTransferCategory());
			referralResponse.getData().put(ReferralProcessingConstants.REFERRAL_AUTOLINKING, erpResponse.isCmrAutoLinking());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE, erpResponse.isMultipleCmr());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR, erpResponse.isNonFinancialCmr());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID, erpResponse.getNonFinancialCmrId());
			referralResponse.getData().put(ReferralProcessingConstants.PROCESSOR_REQUESTER, erpResponse.getRequester());
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("RenewalReferralProcessingService executeDecision ends for " + erpResponse.getSsapApplicationPrimaryKey() + ", headerValue - " + headerValue);
		
		Message<String> resp =  MessageBuilder.withPayload(response).copyHeadersIfAbsent(message.getHeaders()).setHeaderIfAbsent(REN_DECISION_HEADER, headerValue).setHeader("contentType", "application/xml").setHeader("Content-Length", response.length()  ).setHeader("content-length", response.length() ).build();

		return resp;
	}

	public Message<String> determineNonFinancialFlow(Message<String> message) {
		String headerValue = ReferralEligibilityDecisionService.ERROR_HANDLER;
		ReferralResponse referralResponse = new ReferralResponse();
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("RenewalReferralProcessingService determineNonFinancialFlow starts ");
			}
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			final NFProcessDTO nfProcessDTO = createNFProcessDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (boolean) input.getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR), (int) input.getData()
			        .get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID),(boolean) input.getData().get(ReferralProcessingConstants.REFERRAL_AUTOLINKING));
			final boolean blnComplete = referralRenNFDeterminationService.execute(nfProcessDTO);
			headerValue = blnComplete ? ReferralEligibilityDecisionService.SUCCESS_HANDLER : ReferralEligibilityDecisionService.NF_CONVERSION_HANDLER;
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NF_MATCHING_CMR_ID, nfProcessDTO.getMatchingCmrId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NF_APPLICATION_ID, nfProcessDTO.getNfApplicationId());

			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_REN_NF_DETERMINATION + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_REN_NF_DETERMINATION).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_REN_NF_DETERMINATION);
		}
		final String response = EligibilityUtils.marshal(referralResponse);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("RenewalReferralProcessingService determineNonFinancialFlow ends for " + response + ", headerValue - " + headerValue);
		}
		Message<String> resp =  MessageBuilder.withPayload(response).copyHeadersIfAbsent(message.getHeaders()).setHeaderIfAbsent(REN_NF_CONV_DECISION_HEADER, headerValue).setHeader("contentType", "application/xml").setHeader("Content-Length", response.length()  ).setHeader("content-length", response.length() ).build();

		return resp;
	}

	public String convertNonFinancialProcess(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		try {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("RenewalReferralProcessingService convertNonFinancialProcess starts ");
			}
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			final NFProcessDTO nfProcessDTO = createNFProcessDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (boolean) input.getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR), (int) input.getData()
			        .get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID), (int) input.getData().get(ReferralProcessingConstants.KEY_NF_MATCHING_CMR_ID), (long) input.getData().get(ReferralProcessingConstants.KEY_NF_APPLICATION_ID),(boolean) input.getData().get(ReferralProcessingConstants.REFERRAL_AUTOLINKING));

			referralRenNFConversionService.execute(nfProcessDTO);

			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_REN_NF_CONVERSION + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_REN_NF_CONVERSION).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_REN_NF_CONVERSION);
		}
		final String response = EligibilityUtils.marshal(referralResponse);
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("RenewalReferralProcessingService convertNonFinancialProcess ends for " + response);
		}
		return response;

	}

	private NFProcessDTO createNFProcessDTO(long currentApplicationId, boolean nfCmr, int nfCmrId, boolean isAutoLink) {
		NFProcessDTO nfProcessDTO = new NFProcessDTO();
		nfProcessDTO.setCurrentApplicationId(currentApplicationId);
		nfProcessDTO.setNonFinancialCmr(nfCmr);
		nfProcessDTO.setNfCmrId(nfCmrId);
		nfProcessDTO.setAutoLink(isAutoLink);
		return nfProcessDTO;
	}

	private NFProcessDTO createNFProcessDTO(long currentApplicationId, boolean nfCmr, int nfCmrId, int matchingCmrId, long nfApplicationId, boolean isAutoLink) {
		NFProcessDTO nfProcessDTO = createNFProcessDTO(currentApplicationId, nfCmr, nfCmrId,isAutoLink);
		nfProcessDTO.setMatchingCmrId(matchingCmrId);
		nfProcessDTO.setNfApplicationId(nfApplicationId);
		return nfProcessDTO;
	}
}
