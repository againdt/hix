export const OTHER_RACE_CODE = '2131-1';

export default [
    {
        label: 'American Indian or Alaska Native',
        code: '1002-5',
        otherLabel:	null
    },
    {
        label: 'Asian Indian',
        code: '2029-7',
        otherLabel:	null
    },
    {
        label: 'Black or African American',
        code: '2054-5',
        otherLabel:	null
    },
    {
        label: 'Chinese',
        code: '2034-7',
        otherLabel: null
    },
    {
        label: 'Filipino',
        code: '2036-2',
        otherLabel: null
    },
    {
        label: 'Guamanian or Chamorro',
        code: '2086-7',
        otherLabel: null
    },
    {
        label: 'Japanese',
        code: '2039-6',
        otherLabel: null
    },
    {
        label: 'Korean',
        code: '2040-4',
        otherLabel: null
    },
    {
        label: 'Native Hawaiian',
        code: '2079-2',
        otherLabel: null
    },
    {
        label: 'Other Asian',
        code: '2028-9',
        otherLabel: null
    },
    {
        label: 'Other Pacific Islander',
        code: '2500-7',
        otherLabel: null
    },
    {
        label: 'Samoan',
        code: '2080-0',
        otherLabel: null
    },
    {
        label: 'Vietnamese',
        code: '2047-9',
        otherLabel: null
    },
    {
        label: 'White or Caucasian',
        code: '2106-3',
        otherLabel: null
    },
    {
        label: '',
        code: OTHER_RACE_CODE,
        otherLabel: null
    }
];