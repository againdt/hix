package com.getinsured.iex.hub.mec.servicehandler;

import java.util.Iterator;

import javax.xml.bind.JAXBElement;

import org.json.simple.parser.ParseException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ApplicantRequestType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ApplicantResponseSetType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ESIMECRequestPayloadType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ESIMECResponsePayloadType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.InsuranceApplicantRequestType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.RequestPersonType;
import com.getinsured.iex.hub.mec.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.hub.mec.wrapper.ApplicantResponseSetWrapper;
import com.getinsured.iex.hub.mec.wrapper.InsuranceApplicantWrapper;
import com.getinsured.iex.hub.mec.wrapper.LocationStateWrapper;
import com.getinsured.iex.hub.mec.wrapper.PersonWrapper;
import com.getinsured.iex.hub.mec.wrapper.ResponseMetadataWrapper;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ssap.iex.HouseholdMember;
import com.getinsured.iex.hub.platform.ssap.iex.SSAP;
import com.getinsured.iex.hub.platform.ssap.iex.SSAPTaxHouseHold;

public class MECApplicantServiceHandler extends AbstractServiceHandler {

	private static com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ObjectFactory extnFactory = new com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ObjectFactory();
	private static com.getinsured.iex.hub.mec.cms.dsh.vesim.exchange._1.ObjectFactory exchFactory = new com.getinsured.iex.hub.mec.cms.dsh.vesim.exchange._1.ObjectFactory();

	public Object getRequestpayLoad() throws HubServiceException {
		HubServiceException he = null;
		Object response = null;
		try {
			response = getPayLoadFromJSONInput("householdMember",
					getJsonInput());
		} catch (HubServiceException e) {
			he = e;
		} catch (HubMappingException e) {
			he = new HubServiceException("Failed to generate the payload", e);
		}
		if (he != null) {
			throw he;
		}
		return response;
	}

	public Object getPayLoadFromJSONInput(String jsonSectionKey, String jsonStr)
			throws HubServiceException, HubMappingException {

		JAXBElement<ESIMECRequestPayloadType> payloadElement = null;
		SSAP ssap = null;
		try{
			ssap = new SSAP(getJsonInput());
		}catch(ParseException ex){
			listJSONKeys();
			throw new HubMappingException("Failed to process input data ["+ex.getMessage()+"]", ex);
		}
		
	
		ESIMECRequestPayloadType x = extnFactory.createESIMECRequestPayloadType();

		this.setServiceName("MEC");
		HouseholdMember houseHoldMember = null;
		SSAPTaxHouseHold taxHousehold = null;
		
		Iterator<SSAPTaxHouseHold> householdCursor = ssap.houseHoldIterator();
		while(householdCursor.hasNext()){
				taxHousehold = householdCursor.next();
				Iterator<HouseholdMember> memberCursor = taxHousehold.iterator();
				
				while(memberCursor.hasNext()){
						houseHoldMember = memberCursor.next();
	
				ApplicantRequestType iReq = extnFactory
						.createApplicantRequestType();
	
				PersonWrapper personWrapper = new PersonWrapper();
				this.extractSSAPData(houseHoldMember, personWrapper);
				this.extractSSAPData(houseHoldMember.getSsnInfo(), personWrapper);
				
				RequestPersonType requestPersonType = personWrapper.getSystemRepresentation();
				iReq.setRequestPerson(requestPersonType);
	
				InsuranceApplicantRequestType insuranceApplicantRequestType = extnFactory.createInsuranceApplicantRequestType();
				InsuranceApplicantWrapper insuranceApplicanttWrapper = new InsuranceApplicantWrapper();
	
				this.extractSSAPData(houseHoldMember, insuranceApplicanttWrapper);
				insuranceApplicantRequestType.setInsuranceApplicantRequestedCoverage(insuranceApplicanttWrapper.getSystemRepresentation());
				iReq.setInsuranceApplicantRequest(insuranceApplicantRequestType);
	
				LocationStateWrapper locationStateWrapper = new LocationStateWrapper();
				this.extractSSAPData(houseHoldMember.getHomeAddress(), locationStateWrapper);
	
				iReq.setPersonLocationState(locationStateWrapper.getSystemRepresentation());
				x.getApplicantRequest().add(iReq);
			}
		}
		payloadElement = exchFactory.createESIMECRequest(x);

		return payloadElement;
	}

	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject) {
		String response = null;

		if (responseObject != null) {
			
			JAXBElement<ESIMECResponsePayloadType> esiMecResponseJaxb = (JAXBElement<ESIMECResponsePayloadType>) responseObject;
			ESIMECResponsePayloadType esiMecResponseType = esiMecResponseJaxb
					.getValue();
			
			try {
				JAXBElement<ApplicantResponseSetType> responseSetJaxb = (JAXBElement<ApplicantResponseSetType>) esiMecResponseType
						.getResponse();
				ApplicantResponseSetType responseSet = responseSetJaxb.getValue();
				ApplicantResponseSetWrapper responseSetWrapper = new ApplicantResponseSetWrapper(
						responseSet);
				response = responseSetWrapper.toJSONString();
				this.requestStatus = RequestStatus.SUCCESS;
			} catch (ClassCastException e) {
				JAXBElement<ResponseMetadataType> responseSetJaxb = (JAXBElement<ResponseMetadataType>) esiMecResponseType.getResponse();
				ResponseMetadataType responseMetaData = (ResponseMetadataType)( responseSetJaxb.getValue());
				if (responseMetaData != null) {
					ResponseMetadataWrapper metadata = new ResponseMetadataWrapper(responseMetaData);
					this.setResponseCode(metadata.getResponseCode());
					response = metadata.toJSONString();
				}
				this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;

			}
		}
		return response;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
