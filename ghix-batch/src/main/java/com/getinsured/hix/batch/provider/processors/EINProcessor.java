package com.getinsured.hix.batch.provider.processors;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EINProcessor implements ProviderDataFieldProcessor {
    private ValidationContext context;
    private int index;
    private Logger logger = LoggerFactory.getLogger(EINProcessor.class);

    @Override
    public void setIndex(int idx) {
        this.index = idx;
    }

    @Override
    public int getIndex() {
        return this.index;
    }

    @Override
    public void setValidationContext(ValidationContext validationContext) {
        this.context = validationContext;
    }

    @Override
    public ValidationContext getValidationContext() {
        return this.context;
    }

    @Override
    public Object process(Object objToBeValidated)
            throws ProviderValidationException, InvalidOperationException {
        String s;
        int intEIN;
        try {
            s = (String) objToBeValidated;
            String strEIN = s.trim();
            if (strEIN.length() != 9) {
                throw new ProviderValidationException("Validation of EIN failed. Length of EIN is not 9.");
            }
            intEIN = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            throw new ProviderValidationException("Validation of EIN failed. It seems to contain non-digit characters.");
        }
        return intEIN;
    }
}
