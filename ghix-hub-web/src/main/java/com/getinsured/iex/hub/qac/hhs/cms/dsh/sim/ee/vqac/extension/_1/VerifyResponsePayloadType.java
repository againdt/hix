//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.03.29 at 06:16:44 PM PDT 
//


package com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.hub.qac.niem.niem.structures._2.ComplexObjectType;



/**
 * <p>Java class for VerifyResponsePayloadType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyResponsePayloadType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element ref="{http://hix.cms.gov/0.1/hix-core}ResponseMetadata"/>
 *           &lt;element ref="{http://vqac.ee.sim.dsh.cms.hhs.gov/extension/1.0}EDSResponseSet"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyResponsePayloadType", propOrder = {
    "responseMetadata",
    "edsResponseSet"
})
public class VerifyResponsePayloadType
    extends ComplexObjectType
{

    @XmlElement(name = "ResponseMetadata", namespace = "http://hix.cms.gov/0.1/hix-core")
    protected ResponseMetadataType responseMetadata;
    @XmlElement(name = "EDSResponseSet")
    protected EDSResponseSetType edsResponseSet;

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

    /**
     * Gets the value of the edsResponseSet property.
     * 
     * @return
     *     possible object is
     *     {@link EDSResponseSetType }
     *     
     */
    public EDSResponseSetType getEDSResponseSet() {
        return edsResponseSet;
    }

    /**
     * Sets the value of the edsResponseSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link EDSResponseSetType }
     *     
     */
    public void setEDSResponseSet(EDSResponseSetType value) {
        this.edsResponseSet = value;
    }

}
