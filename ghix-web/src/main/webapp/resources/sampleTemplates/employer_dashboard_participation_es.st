<style>
	#container .highcharts-container{height: auto !important; overflow-x: visible!important; left:-15px!important; } 
	.pie-Descrip{margin:-75px 0 0 0}
</style>
<script type="text/javascript" src="/hix/resources/js/highcharts.js"></script>
<script type="text/javascript" src="/hix/resources/js/shop/piechart.js"></script>


	<div class="well-step">
		<div class="clearfix">
			<h4>What&#039;s Next?</h4>
			<div class="gutter10-lr">
				<div id="complete-oep" class="alert">
					<p>Extremos de inscripcion abierta $enrollEndDate$. Si usted cumple con el $participation_required$% requisito de participacion, recibira una factura en $invoicedate$.</p>
					$showWaivedText$            
				</div>	
			</div><!-- End -->
		</div> <!-- End /.blurb --> 
		
		<div class="txt-center">
			<h5 class="header-secondary"><b>Resumen Participacion</b></h5>
			<p>Participacion requerida: $participation_required$%</p>
			<p>Participacion del Empleador: $participationrate$%</p>
		</div> <!-- End /.blurb -->
		<div  id="step-progress">        
			<div id="progress-bar">
				
				<div id="container"></div>
			</div> <!--/End Progress-bar-->
		</div><!--/End step-progress-->
		
		  
	</div><!--End /.well-step-->

<script type="text/javascript">
showPieChart($enrolled$, $waived_qualified$, $waived_unqualified$, $noresponse$);
</script>