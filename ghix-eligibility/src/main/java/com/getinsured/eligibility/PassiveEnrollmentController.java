package com.getinsured.eligibility;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.passive.enrollment.service.PassiveEnrollmentNotice;
import com.getinsured.eligibility.passive.enrollment.service.PassiveEnrollmentService;

@Controller
@RequestMapping("/passive/enrollment/")
public class PassiveEnrollmentController {

	private static final Logger LOGGER = Logger.getLogger(PassiveEnrollmentController.class);

	@Autowired
	private PassiveEnrollmentService passiveEnrollmentService;

	@Autowired
	private PassiveEnrollmentNotice passiveEnrollmentNotice;

	@Autowired private IntegrationLogService integrationLogService;


	@RequestMapping(value = "/process", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> processPassiveSSAP(@RequestBody String ssapJson) {
		Long ssapApplicationId = null;
		try {
			ssapApplicationId = passiveEnrollmentService.execute(ssapJson);
		} catch (Exception e) {
			String errorPrintStack = ExceptionUtils.getFullStackTrace(e);
			LOGGER.error("Error executing passive enrollment - " + errorPrintStack);
			return new ResponseEntity<String>(errorPrintStack, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(ssapApplicationId.toString(), HttpStatus.OK);
	}

	@RequestMapping(value = "/notifications/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> generateNotification(@PathVariable("caseNumber") String caseNumber) {

		String result = generateNotice(caseNumber);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	private String generateNotice(String caseNumber) {
		String result;
		try {
			String ecmId = passiveEnrollmentNotice.generateNoticeInInbox(caseNumber);
			result = "Passive Notification generated for case Number - " + caseNumber + " document id - " + ecmId;
		} catch (Exception e) {
			String message = "Error occured while generating Passive Notification for case Number - " + caseNumber +  " - " + ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(message);
			result = "Passive Notification not generated for case Number - " + caseNumber;
		}
		return result;
	}


}