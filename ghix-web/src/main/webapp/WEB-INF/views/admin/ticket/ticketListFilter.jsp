<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page isELIgnored="false"%>
<div class="clearfix">

	<div class="span3">
		<div class="control-group pull-left">
			<label for="status" class="control-label"><spring:message code="lbl_searchTkt.subject"/></label>
			<div class="controls">
				<input class="input-large" name="ticketSubject" type="text" id="ticketSubject" onfocus="javascript:fetchTicketSubjects();" value="${searchCriteria.ticketSubject}" >
			</div> <!-- end of controls-->
		</div>
	</div>
	
	<div class="span3">
		<div class="control-group pull-left">
			<label for="status" class="control-label"><spring:message code="label_tkt.requestedBy"/></label>
			<div class="controls">
				<input class="input-large" name="requestedByName" type="text" id="requestedByName" onkeyup="javascript:fetchRequesters();" value="${searchCriteria.requestedByName}" >
				<input type ="hidden" name="requestedBy" id="requestedBy" value="${searchCriteria.requestedBy}">
				<div class="requestedByNameLoader pull-right">
				  				<img style="float:right;" src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="20" height="20" />
				  				</div>		
			</div>
	
		</div>
	</div>
	
	<div class="span3">
		<div class="control-group pull-left ">
			<label for="planLevel" class="control-label"><spring:message code="lbl_searchTkt.queue"/></label>
			<div class="controls multiSelectControlContainer-ticket-search">
			  <c:out value="${searchCriteria.ticketQueues}"/>
				<select id="ticketQueues" name="ticketQueues" class="select-large customMultiDD" multiple="multiple">
					
					<c:forEach var="assigned" items="${ticketQueues}">
					<option value="${assigned.name}" 
						<c:forEach var="item" items="${searchCriteria.ticketQueues}">
						  <c:if test="${item eq assigned.name}">
						     selected="selected" 
						  </c:if>
						</c:forEach>
						>
						${assigned.name}
					</option>
				</c:forEach>
				</select>
			</div>
		</div>
	</div>	
	
	<div class="span3">
		<div class="control-group pull-left multiSelectControlContainer-ticket-search">
			<label for="" class="control-label"><spring:message code="label.priority"/></label>
			<div class="controls">
				<select name="ticketPriority" id="ticketPriority"  class="select-large customMultiDD" multiple="multiple">
					
					<option value="Critical" <c:if test="${searchCriteria.ticketPriority == 'Critical'}"> selected="selected" </c:if>><spring:message code="option_tktPriority.critical"/></option>
					<option value="High" <c:if test="${searchCriteria.ticketPriority == 'High'}"> selected="selected" </c:if>><spring:message code="option_tktPriority.high"/></option>
					<option value="Medium"<c:if test="${searchCriteria.ticketPriority == 'Medium'}"> selected="selected" </c:if>><spring:message code="option_tktPriority.medium"/></option>
					<option value="Low"<c:if test="${searchCriteria.ticketPriority == 'Low'}"> selected="selected" </c:if>><spring:message code="option_tktPriority.low"/></option>
				</select>
			</div>
		</div>
	</div>
	

	
</div><!--first row ends-->

<div class="clearfix">

	<div class="span3">
		<div class="control-group pull-left">
			<label for="" class="control-label"><spring:message code="lbl_tkt.assignee"/></label>
			<div class="controls">
				<input class="input-large" name="assignee" type="text" id="assignee" onfocus="javascript:fetchAssigneeList();" value="${searchCriteria.assignee}" >
				<input type ="hidden" name="assigneeId" id="assigneeId" value="${searchCriteria.assigneeId}" >	
			</div>
			<div id="assignee_error"></div>
		</div>
	</div>
	
	<div class="span3">
		<div class="control-group pull-left ">
			<label for="status" class="control-label"><spring:message code="lbl.ticketNumber"/></label>
			<div class="controls">
				<input class="input-large" name="ticketNumber" type="text" id="ticketNumber" onfocus="javascript:fetchTicketNumbers();" value="${searchCriteria.ticketNumber}" >
			</div> <!-- end of controls-->
		</div>
	</div>	
	
	<div class="span3">
		<div class="control-group pull-left ">
			<label for="submittedAfter" class="control-label"><spring:message code="lbl_searchTkt.submittedAfter"/> </label>
			<div class="input-append date date-picker" id="date" data-date="">
				<input class="input-date" type="text" name="submittedAfter" id="submittedAfter" value="${searchCriteria.submittedAfter}" pattern="MM/DD/YYYY" placeholder="MM/DD/YYYY" /> 
				<span class="add-on"><i class="icon-calendar"></i></span>
			</div>
			<div id="submittedAfter_error"></div>
		</div>
	</div>	
	
	<div class="span3">
		<div class="control-group pull-left ">
			<label for="submittedBefore" class="control-label"><spring:message code="lbl_searchTkt.submittedBefore"/></label>
			<div class="input-append date date-picker" id="date" data-date="">
				<input class="input-date" type="text" name="submittedBefore" id="submittedBefore" value="${searchCriteria.submittedBefore}" pattern="MM/DD/YYYY" placeholder="MM/DD/YYYY" /> 
				<span class="add-on"><i class="icon-calendar"></i></span>
			</div>
			<span id="submittedBefore_error"></span>
		</div>
	</div>
	
	
	<div class="clearfix">
	
	<c:set var="selectedTicketStatusList" value="${searchCriteria.ticketStatus}"/>
	<div class="span3">
		<div class="control-group pull-left ">
			<label for="" class="control-label"><spring:message code="lbl_searchTkt.tktStatus"/></label>
			<div class="controls">
				<select class="select-large" id ="ticketStatus" name="ticketStatus"  multiple size="4">
					<option value="Any" <c:if test="${fn:contains(searchCriteria.ticketStatus,'Any')}"> selected="selected" </c:if>><spring:message code="option_tkt.any"/></option>
					<option value="UnClaimed" <c:if test="${ fn:contains(searchCriteria.ticketStatus,'UnClaimed')}"> selected="selected" </c:if>><spring:message code="option_tktStatus.unclaimed"/></option>
					<option value="Open" <c:if test="${ fn:contains(searchCriteria.ticketStatus, 'Open') }"> selected="selected" </c:if>><spring:message code="option_tktStatus.open"/></option>
					<option value="Resolved"  <c:if test="${fn:contains(searchCriteria.ticketStatus,'Resolved')}"> selected="selected" </c:if>><spring:message code="option_tktStatus.resolved"/></option>
					<option value="Canceled" <c:if test="${fn:contains(searchCriteria.ticketStatus,'Canceled')}"> selected="selected" </c:if>><spring:message code="option_tktStatus.canceled"/></option>
					<option value="Restarted" <c:if test="${fn:contains(searchCriteria.ticketStatus,'Restarted')}"> selected="selected" </c:if>>Restarted</option>
					<option value="Reopened" <c:if test="${fn:contains(searchCriteria.ticketStatus,'Reopened')}"> selected="selected" </c:if>>Reopened</option>
				</select>
			</div>
		</div>
	</div>	
	
	<div class="span3">
		<div class="control-group pull-left ">
		<label for="" class="control-label"><spring:message code="lbl_searchTkt.taskStatus"/></label>
		<div class="controls">
			<select name="taskStatus" id="taskStatus"  class="select-large">
   				<option value="" <c:if test="${searchCriteria.taskStatus == ''}"> selected="selected" </c:if>><spring:message code="option_tkt.any"/></option>	
   				<option value="<encryptor:enc value="UnClaimed"/>"<c:if test="${searchCriteria.taskStatus == 'UnClaimed'}"> selected="selected" </c:if>><spring:message code="option_tktStatus.unclaimed"/></option>
   				<option value="<encryptor:enc value="Claimed"/>" <c:if test="${searchCriteria.taskStatus == 'Claimed'}"> selected="selected" </c:if>><spring:message code="option_tkt.statusClaimed"/></option>
   				<option value="<encryptor:enc value="Canceled"/>" <c:if test="${searchCriteria.taskStatus == 'Canceled'}"> selected="selected" </c:if>><spring:message code="option_tktStatus.canceled"/></option>
   			</select>
		</div>
		</div>
	</div>	
	
		<div class="span3">
			<div id="userRoleDiv" class="control-group pull-left">
				<label class="control-label required">Created For Role </label>
				<div class="controls">
					<div class="pull-left">
					<select class="select-large" name="userRole.id"
						id="userRole" onchange="javascript:checkUserRole();">
						<%-- <c:if test="${flow == 'edit'}">
							<option value='${ticket.userRole.id}'>${ticket.userRole.id} </option>
							SELECTED
						</c:if> --%>
						<option value="">Select</option>
						<c:forEach var="role" items="${roleList}">								
							<option value="<c:out value="${role.id}" />">
								<c:out value="${role.label}" />
							</option>
							
						</c:forEach>
						
							
					</select>
					<div id="userRole_error"></div>
					</div>
				</div>
			</div>
		</div>	
		<!-- end of control-group -->
		
		<div class="span3">
			<div id="requesterDiv" class="control-group pull-left ">
				<label class="control-label required">Created For</label>
				
				
						<div class="controls">
				  			<div class="pull-left">
				  				<input class="input-large" type="text" name="requester" id="requester" onkeyup="javascript:getCreatedFor();" value="${searchCriteria.requester}"  disabled="disabled">
				  				<input type ="hidden" name="roleId" id="roleId" value="${searchCriteria.roleId}">
				  				<div id="requester_error"></div>
				  			</div>	
				  				<div class="requesterLoader pull-left">
				  				<img style="float:right;" src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="20" height="20" />
				  				</div>
						</div>
						<div class="controls" style="display:none;">
			  				<input class="input-large " name="role.id"
							id="roleId" >
							<div id="roleId_error"></div>
						</div>
			  		
				
	 		</div><!-- end of control-group -->
	 	</div>
	 	
		
</div>

<div class="clearfix">
	<div class="span3">	

			<div class="control-group pull-left ">
				<label for="status" class="control-label">Comments</label>
				<div class="controls">
					<input class="input-large" name="ticketComments" type="text" id="ticketComments" value="" >
				</div> <!-- end of controls-->
			</div>
	</div>
	
	<div class="span3">	
		<div class="control-group pull-left" style="margin-top: 24px">
			<label><input value="true" type="checkbox" id="archiveFlagValue" name="archivedCheckbox">&nbsp;&nbsp;&nbsp;Only Archived Tickets </label>
		</div>
	</div>
	
	<div class="span3 pull-right ">
			<input type="submit" class="btn btn-primary input-large" value="Search" title="Click to Search" style="margin-top: 20px">
	</div>
		
</div>
	
</div><!-- second row ends -->

