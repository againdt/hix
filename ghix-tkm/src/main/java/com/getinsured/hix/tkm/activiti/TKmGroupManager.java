package com.getinsured.hix.tkm.activiti;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.AbstractManager;
import org.activiti.engine.impl.persistence.entity.AbstractEntityManager;
import org.activiti.engine.impl.persistence.entity.Entity;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.GroupEntityManager;
import org.activiti.engine.impl.persistence.entity.data.DataManager;
import org.apache.commons.lang.StringUtils;

import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.tkm.dto.TkmIdentityGroupDto;
import com.getinsured.hix.tkm.service.TkmQueuesService;

public class TKmGroupManager extends AbstractManager implements GroupEntityManager  {

	public TKmGroupManager(ProcessEngineConfigurationImpl processEngineConfiguration,TkmQueuesService tkmQueuesService) {
		
		super(processEngineConfiguration);
		this.tkmQueuesService = tkmQueuesService;
		// TODO Auto-generated constructor stub
	}

	private TkmQueuesService tkmQueuesService;
	
	

	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
		// TODO Auto-generated method stub
		List<Group> groupList = new ArrayList<Group>();
		GroupQueryImpl groupQuery = (GroupQueryImpl) query;

		if (StringUtils.isNotEmpty(groupQuery.getId())) {
			Group singleGroup = findById(groupQuery.getId());
			groupList.add(singleGroup);
			return groupList;

		} else if (StringUtils.isNotEmpty(groupQuery.getName())) {

			Group singleGroup = findByName(groupQuery.getName());
			groupList.add(singleGroup);

			return groupList;

		} else if (StringUtils.isNotEmpty(groupQuery.getUserId())) {

			return findGroupsByUser(groupQuery.getUserId());

		} else {

			// TODO: get all groups from your identity domain and convert them
			// to List<Group>
			return Collections.emptyList();

		} // TODO: you can add other search criteria that will allow extended
			// support using the Activiti engine API
	}

	public long findGroupCountByQueryCriteria(GroupQueryImpl query) {

		return findGroupByQueryCriteria(query, null).size();
	}


	public GroupEntity findById(String groupId) {
		TkmIdentityGroupDto queuesDTO = null;
		TkmQueues tkmQueues = tkmQueuesService.findById(Integer
				.parseInt(groupId));
		if (checkNotNull(tkmQueues)) {
			queuesDTO = new TkmIdentityGroupDto(
					tkmQueuesService.findById(Integer.parseInt(groupId)));
		}
		return queuesDTO;
	}

	private boolean checkNotNull(TkmQueues tkmQueues) {
		if (null != tkmQueues&&null != tkmQueues.getId()&&null != tkmQueues.getName()) {
					return true;
		}
		return false;
	}

	private Group findByName(String name) {
		TkmIdentityGroupDto tkmQueuesDto = null;
		TkmQueues tkmQueues = tkmQueuesService.findByName(name);
		if (checkNotNull(tkmQueues)) {
			tkmQueuesDto = new TkmIdentityGroupDto(tkmQueues);
		}
		return tkmQueuesDto;
	}


	public List<Group> findGroupsByUser(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	protected DataManager<Entity> getDataManager() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GroupEntity create() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(GroupEntity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insert(GroupEntity entity, boolean fireCreateEvent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public GroupEntity update(GroupEntity entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GroupEntity update(GroupEntity entity, boolean fireUpdateEvent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(GroupEntity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(GroupEntity entity, boolean fireDeleteEvent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Group createNewGroup(String groupId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GroupQuery createNewGroupQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Group> findGroupsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isNewGroup(Group group) {
		// TODO Auto-generated method stub
		return false;
	}


}
