package com.getinsured.iex.im.wso2;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.im.wso2.wrappers.UpdateCredentialsWrapper;

public class UpdatePasswordRequestHandler extends AbstractServiceHandler {
	boolean handleResponseCalled = false;
	@Override
	public Object getRequestpayLoad() throws HubServiceException,
			HubMappingException {
		JSONParser parser = new JSONParser();
		try {
			JSONObject authJsonrequest = (JSONObject) parser.parse(this.getJsonInput());
			String userName = (String) authJsonrequest.get("userName");
			String password = (String) authJsonrequest.get("password");
			String oldPassword = (String) authJsonrequest.get("oldPassword");
			UpdateCredentialsWrapper wrapper = new UpdateCredentialsWrapper();
			wrapper.setOldCredendial(oldPassword);
			wrapper.setNewCredential(password);
			wrapper.setUserName(userName);
			return wrapper.getSystemRepresentation();
		} catch (ParseException e) {
			throw new HubServiceException("Failed to construct the payload from request input");
		}
	}

	@Override
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		// TODO Auto-generated method stub
		handleResponseCalled = true;
		return "Success";
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(this.handleResponseCalled){
			return RequestStatus.SUCCESS;
		}
		return RequestStatus.FAILED;
	}

}
