package com.getinsured.hix.entity.assister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.entity.AssisterCertificationNumber;

/**
 * Spring-JPA repository that encapsulates methods that interact with the AssisterCertificationNumber
 * table
 */
public interface IAssisterCertificationNumberRepository extends
		RevisionRepository<AssisterCertificationNumber, Integer, Integer>, JpaRepository<AssisterCertificationNumber, Integer> {

	/**
	 * Fetches the AssisterCertificationNumber record against the given Assister Id
	 * 
	 * @param assisterId
	 *            the Assister id
	 * @return the AssisterCertificationNumber record against the Assister Id
	 */
	AssisterCertificationNumber findByAssisterId(Integer assisterId);

	/**
	 * Fetches the AssisterCertificationNumber record against the given Id
	 * 
	 * @param assisterCertificationNumber
	 *            the AssisterCertificationNumber id
	 * @return AssisterCertificationNumber record against the given Id
	 */
	AssisterCertificationNumber findById(long assisterCertificationNumber);

}