package com.getinsured.iex.im.wso2.wrappers;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.getinsured.iex.im.wso2.carbon.um.ws.service.ObjectFactory;
import com.getinsured.iex.im.wso2.carbon.um.ws.service.SetUserClaimValues;
import com.getinsured.iex.im.wso2.carbon.user.mgt.common.xsd.ClaimValue;
import com.getinsured.iex.im.wso2.exceptions.CalimValueNotSupported;

public class SetMultipleUserClaimsWrapper implements JSONAware {
	private static final String FALSE = "false";
	private static final String TRUE = "true";
	private static final String N = "N";
	private static final String NO = "NO";
	private static final String Y = "Y";
	private static final String YES = "YES";
	public static final String SECURITY_QUESTION_URI="http://wso2.org/claims/secquestion";
	public static final String SECURITY_QUESTION_ANS_URI = "http://wso2.org/claims/secanswer";
	public static final String PREF_COMMUNICATION_METHOD_URI = "http://wso2.org/claims/prefMethComm";
	public static final String RIDP_FLAG_URI="http://wso2.org/claims/ridpflag";
	public static final String USERNAME="http://wso2.org/claims/userName";
	public static final String PASSWORD_TIMESTAMP="http://wso2.org/claims/passwordTimestamp";
	public static final String PASSWORD_IDENTITY_TIMESTAMP="http://wso2.org/claims/identity/passwordTimestamp";
	public static final String ACCOUNT_TYPE_CODE = "http://wso2.org/claims/accountTypeCode";
	
	private static HashSet<String> supportedClaims= new HashSet<>();
	private static final QName userQName  = new QName("http://service.ws.um.carbon.wso2.org", "userName");
	private static final QName profileQName = new QName("http://service.ws.um.carbon.wso2.org", "profileName");
	private static final QName claimUriQName = new QName("http://service.ws.um.carbon.wso2.org", "claimURI");
	private static final QName claimValueQName = new QName("http://service.ws.um.carbon.wso2.org", "claimValue");
	
	static{
		supportedClaims.add(PREF_COMMUNICATION_METHOD_URI);
		supportedClaims.add(SECURITY_QUESTION_URI);
		supportedClaims.add(SECURITY_QUESTION_ANS_URI);
		supportedClaims.add(RIDP_FLAG_URI);
		supportedClaims.add(USERNAME);
		supportedClaims.add(PASSWORD_TIMESTAMP);
		supportedClaims.add(PASSWORD_IDENTITY_TIMESTAMP);
		supportedClaims.add(ACCOUNT_TYPE_CODE);
	}
	
	private static final ObjectFactory factory = new ObjectFactory();
	private String userName;
	private String profileName;
	private SetUserClaimValues setClaimReq;
	private HashSet<String> claimsAdded = new HashSet<String>(10);
	
	
	public SetMultipleUserClaimsWrapper(){
		setClaimReq = factory.createSetUserClaimValues();
	}
	
	public void setTargetUser(String userName, String profileName){
		this.userName = userName;
		this.profileName = profileName;
		if(this.profileName == null){
			this.profileName = "default";
		}
		this.setClaimReq.setUserName(new JAXBElement<String>(userQName, String.class,this.userName));
		this.setClaimReq.setProfileName(new JAXBElement<String>(profileQName, String.class,this.profileName));
	}
	
	public void setUserClaim(String claimUri, String value) throws CalimValueNotSupported{
		
		if(claimUri == null || value == null || this.claimsAdded.contains(claimUri)){
			return;
		}
		String claimValue = value;
		boolean isClaimPresent = false;
		String supportedClaim = null;
		
		if(null != supportedClaims){
			Iterator<String> iterator = supportedClaims.iterator();
			while(iterator.hasNext()) {
				supportedClaim = iterator.next();
				if(supportedClaim.equalsIgnoreCase(claimUri)) {
					isClaimPresent = true;
					break;
				} 
			}
		}
		
		if(!isClaimPresent) {
			throw new CalimValueNotSupported(claimUri);
		} 
		
		claimUri = supportedClaim;
		
		if (supportedClaim.equalsIgnoreCase("http://wso2.org/claims/ridpflag")) {
			if (value.equalsIgnoreCase(YES) || value.equalsIgnoreCase(Y)) {
				claimValue = TRUE;
			} else if (value.equalsIgnoreCase(NO) || value.equalsIgnoreCase(N)) {
				claimValue = FALSE;
			}
		} else {
			claimValue = value;
		}
		List<ClaimValue> list = this.setClaimReq.getClaims();
		ClaimValue cv  = new ClaimValue();
		cv.setClaimURI(new JAXBElement<String>(claimUriQName, String.class,supportedClaim));
		cv.setValue(new JAXBElement<String>(claimValueQName, String.class,claimValue));
		list.add(cv);
		this.claimsAdded.add(claimUri);
	}
	
	public Object getSystemRepresentation(){
		return this.setClaimReq;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("USER_NAME", this.userName);
		obj.put("PROFILE", this.profileName);
		JSONArray claimsList = new JSONArray();
		if(this.setClaimReq != null){
			List<ClaimValue> claims = this.setClaimReq.getClaims();
			for(ClaimValue cv: claims){
				JSONObject claim = new JSONObject();
				claim.put("CLAIM_URI", cv.getClaimURI().getValue());
				claim.put("CLAIM_VALUE", cv.getValue().getValue());
				claimsList.add(claim);
			}
			
		}
		obj.put("CLAIMS", claimsList);
		return obj.toJSONString();
	}

	public static SetMultipleUserClaimsWrapper fromJson(String jsonString){
		SetMultipleUserClaimsWrapper wrapper = new SetMultipleUserClaimsWrapper();
		JSONParser parser = new JSONParser();
		try{
			JSONObject root = (JSONObject) parser.parse(jsonString);
			String userName = (String) root.get("USER_NAME");
			String profile = (String)root.get("PROFILE");
			wrapper.setTargetUser(userName, profile);
			JSONArray claimsList = (JSONArray) root.get("CLAIMS");
			@SuppressWarnings("rawtypes")
			Iterator i = claimsList.iterator();
			while(i.hasNext()){
				JSONObject claim = (JSONObject) i.next();
				wrapper.setUserClaim((String)claim.get("CLAIM_URI"), (String)claim.get("CLAIM_VALUE"));
			}
			return wrapper;
		}catch(Exception e){
			return null;
		}
	}
}
