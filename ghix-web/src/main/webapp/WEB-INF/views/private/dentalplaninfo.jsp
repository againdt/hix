<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link rel="stylesheet" href="<c:url value="/resources/css/tiles.css" />" type="text/css">
<link media="screen,print" type="text/css" rel="stylesheet" href="/hix/resources/css/bootstrap-responsive.css">
<link media="screen,print" type="text/css" rel="stylesheet" href="/hix/resources/css/ghixcustom.css">
<script type="text/javascript">
  var plan_display = new Array();
  
  plan_display['pd.label.title.removeDentalPlan'] = "<spring:message code='pd.label.title.removeDentalPlan' javaScriptEscape='true'/>";
  plan_display['pd.label.title.showCompare'] = "<spring:message code='pd.label.title.showCompare' javaScriptEscape='true'/>";
  plan_display['pd.label.title.hideCompare'] = "<spring:message code='pd.label.title.hideCompare' javaScriptEscape='true'/>";
  plan_display['pd.label.title.removeDental'] = "<spring:message code='pd.label.title.removeDental' javaScriptEscape='true'/>";  
  plan_display['pd.label.selectOneMember'] = "<spring:message code='pd.label.selectOneMember' javaScriptEscape='true'/>";
  plan_display['pd.label.adultDentalSelection'] = "<spring:message code='pd.label.adultDentalSelection' javaScriptEscape='true'/>";
  plan_display['pd.label.selectOnePlan'] = "<spring:message code='pd.label.selectOnePlan' javaScriptEscape='true'/>";
  plan_display['pd.label.dentalPlanAlreadyAdded'] = "<spring:message code='pd.label.dentalPlanAlreadyAdded' javaScriptEscape='true'/>";
  plan_display['pd.label.removeThisPlan'] = "<spring:message code='pd.label.removeThisPlan' javaScriptEscape='true'/>";
  
  plan_display['pd.label.selectPlanAlreadyInCart'] = "<spring:message code='pd.label.selectPlanAlreadyInCart' javaScriptEscape='true'/>";
  plan_display['pd.label.goToCart'] = "<spring:message code='pd.label.goToCart' javaScriptEscape='true'/>";
  plan_display['pd.label.title.addPlan'] = "<spring:message code='pd.label.title.addPlan' javaScriptEscape='true'/>";
</script>

<script type="text/javascript">

function buildObject(benefit){
	var benefitObj = new Object();
	var benefitAttr = benefit.replace(/{|}/gi,"").split(",");
	for (var benefitName in benefitAttr){
		var val = benefitAttr[benefitName].split("=");
		benefitObj[$.trim(val[0])] = val[1];
	}
	return benefitObj;
}
</script>

<style>
body #container-wrap {
	margin-top: 0;
}
body #container-wrap .container {
	margin-bottom: 30px;
}
.table thead th strong {
	font-weight: 400;
}
.table thead th p {
	line-height: 40px;
	padding: 0;
	margin: 0;
	color: #333;
}
.table thead th.span3 {
	padding-bottom: 0;
}
</style>

<div class="row-fluid">
  
    <h3>${individualPlan.issuer} &nbsp; - &nbsp; ${individualPlan.name}</h3>
    
     <c:if  test="${(( fn:length(individualPlan.providerLink) != 0 && individualPlan.providerLink != null) || (fn:length(individualPlan.sbcUrl) != 0 && individualPlan.sbcUrl != null)) ||
     ( (fn:length(individualPlan.planBrochureUrl) != 0 && individualPlan.planBrochureUrl != null) || (fn:length(individualPlan.formularyUrl) != 0 && individualPlan.formularyUrl != null) ) }">
      <table class="table">
      <thead>
        <tr>
          <th class="span5"><p><strong><spring:message code="pd.label.title.informationFrom"/></strong></p></th>
        </tr> 
      </thead>
      <tbody>
      <c:choose>
        <c:when test="${individualPlan.providerLink != null && fn:length(individualPlan.providerLink) != 0}">
        	<tr><td><a href="${individualPlan.providerLink}"><spring:message code="pd.label.link.providerDirectoryUrl"/></a></td></tr>
        </c:when>
      </c:choose>
      <c:choose>
        <c:when test="${individualPlan.sbcUrl != null && fn:length(individualPlan.sbcUrl) != 0}">
        	<tr><td><a href="${individualPlan.sbcUrl}"><spring:message code="pd.label.link.sbcUrl"/></a></td></tr>
        </c:when>
      </c:choose>
      <c:choose>
        <c:when test="${individualPlan.planBrochureUrl != null && fn:length(individualPlan.planBrochureUrl) != 0}">
        	<tr><td><a href="${individualPlan.planBrochureUrl}"><spring:message code="pd.label.link.planBrochureUrl"/></a></td></tr>
        </c:when>
      </c:choose>
      <c:choose>
        <c:when test="${individualPlan.formularyUrl != null && fn:length(individualPlan.formularyUrl) != 0}">
        	<tr><td><a href="${individualPlan.formularyUrl}"><spring:message code="pd.label.link.formularyUrl"/></a></td></tr>
        </c:when>
      </c:choose>
      </tbody>
	</table>
    </c:if>
    
    <table class="table">
        <thead>
          <tr>
          	<th colspan="2" class="span5"><p><strong><spring:message code="pd.label.title.summary"/></strong></p></th>
          </tr> 
        </thead>
        <tbody>
          <tr>
            <td><spring:message code="pd.label.planName"/></td>
            <td>${individualPlan.name}</td>
          </tr>
          <tr>
            <td><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.producttype" htmlEscape="true"/>">
            <spring:message code="pd.label.commontext.productType"/></a></td>
            <td>
            <c:choose>
            <c:when test="${(individualPlan.networkType == 'PPO')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.ppo" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'HMO')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hmo" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'POS')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.pos" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'DHMO')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dhmo" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'DPPO')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dppo" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'HSA')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hsa" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:otherwise>
            	<c:choose>
	            <c:when test="${(individualPlan.networkType == null) || (individualPlan.networkType == '')}">
	              <td><script><spring:message code="pd.label.commontext.notAvailable"/></script></td>
	            </c:when>
 		    </c:choose>
            </c:otherwise>
 		  </c:choose>
            </td>
          </tr>
           <tr>
            <td><spring:message code="pd.label.planTier"/></td>
            <td>${individualPlan.level}</td>
          </tr>
        </tbody>
    </table>
    
    <table class="table">
        <thead>
          <tr>
            <th colspan="2"><p><strong><spring:message code="pd.label.commontext.deductible"/> &amp; <spring:message code="pd.label.icon.children.dentalDeductible.part2"/></strong></p></th>
          </tr> 
        </thead>
        <tbody>
          <tr>
            <td><spring:message code="pd.label.children.dentalDeductible.link1"/> (<spring:message code="pd.label.commontext.family"/>)</td>
                <c:choose>
              	  <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL != null)}">
              	    <c:choose>
                	  <c:when test="${(individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != '') || (individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Fly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Fly != '') || (individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkFly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkFly != '') || (individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly != '')}">
                  		<td><c:if test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != ''}">
                		  $<fmt:formatNumber value="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
                  		</c:if>
		                <c:if test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Fly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Fly != ''}">
		                  $<fmt:formatNumber value="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Fly}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
		                </c:if>
		                <c:if test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkFly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkFly != ''}">
		                  $<fmt:formatNumber value="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
		                </c:if>
		                <c:if test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly != ''}">
		                  $<fmt:formatNumber value="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
		                </c:if></td>             	
                	  </c:when>
                	  <c:otherwise><td><script><spring:message code="pd.label.commontext.dental.notApp"/></script></td></c:otherwise>
              		</c:choose>
              	  </c:when>
              	  <c:otherwise><td><spring:message code="pd.label.commontext.notAvailable"/></td></c:otherwise>
                </c:choose>
          </tr>
          <tr>
            <td><spring:message code="pd.label.children.dentalDeductible.link1"/> (<spring:message code="pd.label.commontext.individual"/>)</td>
            <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL != null)}">
              <c:choose>
                <c:when test="${(individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != '') || (individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Ind != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Ind != '') || (individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkInd != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkInd != '') || (individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd != '')}">
                  <td><c:if test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != ''}">
                	$<fmt:formatNumber value="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
                  </c:if>
                  <c:if test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Ind != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Ind != ''}">
                	$<fmt:formatNumber value="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkTier2Ind}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
                  </c:if>
                  <c:if test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkInd != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkInd != ''}">
                	$<fmt:formatNumber value="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.outNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
                  </c:if>
                  <c:if test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd != ''}">
                	$<fmt:formatNumber value="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
                  </c:if></td>             	
                </c:when>   
                <c:otherwise><td><script><spring:message code="pd.label.commontext.dental.notApp"/></script></td></c:otherwise>
              </c:choose>
            </c:when>
            <c:otherwise><td><script><spring:message code="pd.label.commontext.notAvailable"/></script></td></c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td><spring:message code="pd.label.dentalDeductible.link2"/> (<spring:message code="pd.label.commontext.family"/>)</td>
                <c:choose>
                  <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_MEDICAL != null)}">
                    <c:choose>
                      <c:when test="${(individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkFly != null && individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkFly != '') || (individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Fly != null && individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Fly != '') || (individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkFly != null && individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkFly != '') || (individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly != null && individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly != '')}">
                        <td><c:if test="${individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkFly != null && individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkFly != ''}">
               		      $<fmt:formatNumber value="${individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
               	        </c:if>
	               	    <c:if test="${individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Fly != null && individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Fly != ''}">
	               		  $<fmt:formatNumber value="${individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Fly}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
	               	    </c:if>
	               	    <c:if test="${individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkFly != null && individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkFly != ''}">
	               		  $<fmt:formatNumber value="${individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
	               	    </c:if>
	               	    <c:if test="${individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly != null && individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly != ''}">
	               		  $<fmt:formatNumber value="${individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
	               	    </c:if></td>
                	  </c:when>
                	  <c:otherwise><td><script><spring:message code="pd.label.commontext.notApp"/></script></td></c:otherwise>
              		</c:choose>
            	  </c:when>
            	 <c:otherwise><td><script><spring:message code="pd.label.commontext.notAvailable"/></script></td></c:otherwise>
          		</c:choose>
          </tr>
          <tr>
            <td><spring:message code="pd.label.dentalDeductible.link2"/> (<spring:message code="pd.label.commontext.individual"/>)</td>
            <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_MEDICAL != null)}">
              <c:choose>
                <c:when test="${(individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkInd != null && individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkInd != '') || (individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Ind != null && individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Ind != '') || (individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkInd != null && individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkInd != '') || (individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd != null && individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd != '')}">
                  <td><c:if test="${individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkInd != null && individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkInd != ''}">
               		$<fmt:formatNumber value="${individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
               	  </c:if>
               	  <c:if test="${individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Ind != null && individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Ind != ''}">
               		$<fmt:formatNumber value="${individualPlan.planCosts.MAX_OOP_MEDICAL.inNetworkTier2Ind}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
               	  </c:if>
               	  <c:if test="${individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkInd != null && individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkInd != ''}">
               		$<fmt:formatNumber value="${individualPlan.planCosts.MAX_OOP_MEDICAL.outNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
               	  </c:if>
               	  <c:if test="${individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd != null && individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd != ''}">
               		$<fmt:formatNumber value="${individualPlan.planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
               	  </c:if></td>               	  
                </c:when>  
                <c:otherwise><td><small><spring:message code="pd.label.commontext.notApp"/></small></td></c:otherwise>
              </c:choose>
            </c:when>
            <c:otherwise><td><small><spring:message code="pd.label.commontext.notAvailable"/></small></td></c:otherwise>
            </c:choose>
          </tr>
          <c:if test="${stateCode == 'CA'}">
          <tr>
            <td><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultDeductible" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultDeductible"/></a>  (<spring:message code="pd.label.commontext.family"/>)</td>
            <c:choose>
            <c:when test="${(individualPlan.optionalDeductible != null && fn:length(individualPlan.optionalDeductible) gt 0 && individualPlan.optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] != null)}">
              <c:set var="OPTDEDUCTIBLE" value="${individualPlan.optionalDeductible['DEDUCTIBLE_DENTAL_ADULT']}"/>
              <c:choose>
              	<c:when test="${(OPTDEDUCTIBLE.inNetworkFly != null && OPTDEDUCTIBLE.inNetworkFly != '') || (OPTDEDUCTIBLE.inNetworkTier2Fly != null && OPTDEDUCTIBLE.inNetworkTier2Fly != '') || (OPTDEDUCTIBLE.outNetworkFly != null && OPTDEDUCTIBLE.outNetworkFly != '') || (OPTDEDUCTIBLE.combinedInOutNetworkFly != null && OPTDEDUCTIBLE.combinedInOutNetworkFly != '')}">
            <td>
				    <c:if test="${OPTDEDUCTIBLE.inNetworkFly != null && OPTDEDUCTIBLE.inNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
               	    </c:if>
	               	<c:if test="${OPTDEDUCTIBLE.inNetworkTier2Fly != null && OPTDEDUCTIBLE.inNetworkTier2Fly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkTier2Fly}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
					</c:if>
					<c:if test="${OPTDEDUCTIBLE.outNetworkFly != null && OPTDEDUCTIBLE.outNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.outNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
					</c:if>
					<c:if test="${OPTDEDUCTIBLE.combinedInOutNetworkFly != null && OPTDEDUCTIBLE.combinedInOutNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.combinedInOutNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
	               	</c:if>
	              </td>
               	  </c:when>
				<c:otherwise><td><small><spring:message code="pd.label.commontext.notApp"/></small></td></c:otherwise>
               	  </c:choose>
               	  </c:when>
            <c:otherwise><td><small><spring:message code="pd.label.commontext.notAvailable"/></small></td></c:otherwise>
               	  </c:choose>
          </tr>
          <tr>
            <td><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultDeductible" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultDeductible"/></a>  (<spring:message code="pd.label.commontext.individual"/>)</td>
               	  <c:choose>
            <c:when test="${(individualPlan.optionalDeductible != null && fn:length(individualPlan.optionalDeductible) gt 0 && individualPlan.optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] != null)}">
              <c:set var="OPTDEDUCTIBLE" value="${individualPlan.optionalDeductible['DEDUCTIBLE_DENTAL_ADULT']}"/>
              <c:choose>
              	<c:when test="${(OPTDEDUCTIBLE.inNetworkInd != null && OPTDEDUCTIBLE.inNetworkInd != '') || (OPTDEDUCTIBLE.inNetworkTier2Ind != null && OPTDEDUCTIBLE.inNetworkTier2Ind != '') || (OPTDEDUCTIBLE.outNetworkInd != null && OPTDEDUCTIBLE.outNetworkInd != '') || (OPTDEDUCTIBLE.combinedInOutNetworkInd != null && OPTDEDUCTIBLE.combinedInOutNetworkInd != '')}">
                  <td><c:if test="${OPTDEDUCTIBLE.inNetworkInd != null && OPTDEDUCTIBLE.inNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.inNetworkTier2Ind != null && OPTDEDUCTIBLE.inNetworkTier2Ind != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkTier2Ind}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.outNetworkInd != null && OPTDEDUCTIBLE.outNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.outNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.combinedInOutNetworkInd != null && OPTDEDUCTIBLE.combinedInOutNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.combinedInOutNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
               	  </c:if></td>               	  
                </c:when>  
                <c:otherwise><td><small><spring:message code="pd.label.commontext.notApp"/></small></td></c:otherwise>
              </c:choose>
            </c:when>
            <c:otherwise><td><small><spring:message code="pd.label.commontext.notAvailable"/></small></td></c:otherwise>
            </c:choose>
          </tr>
          
          <tr>
            <td><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultAnnualBenefitLimit" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultAnnualBenefitLimit"/></a>  (<spring:message code="pd.label.commontext.family"/>)</td>
            <c:choose>
            <c:when test="${(individualPlan.optionalDeductible != null && fn:length(individualPlan.optionalDeductible) gt 0 && individualPlan.optionalDeductible['Adult Annual Benefit Limit'] != null)}">
              <c:set var="OPTDEDUCTIBLE" value="${individualPlan.optionalDeductible['Adult Annual Benefit Limit']}"/>
              <c:choose>
              	<c:when test="${(OPTDEDUCTIBLE.inNetworkFly != null && OPTDEDUCTIBLE.inNetworkFly != '') || (OPTDEDUCTIBLE.inNetworkTier2Fly != null && OPTDEDUCTIBLE.inNetworkTier2Fly != '') || (OPTDEDUCTIBLE.outNetworkFly != null && OPTDEDUCTIBLE.outNetworkFly != '') || (OPTDEDUCTIBLE.combinedInOutNetworkFly != null && OPTDEDUCTIBLE.combinedInOutNetworkFly != '')}">
                  <td>
				    <c:if test="${OPTDEDUCTIBLE.inNetworkFly != null && OPTDEDUCTIBLE.inNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
               	    </c:if>
	               	<c:if test="${OPTDEDUCTIBLE.inNetworkTier2Fly != null && OPTDEDUCTIBLE.inNetworkTier2Fly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkTier2Fly}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
					</c:if>
					<c:if test="${OPTDEDUCTIBLE.outNetworkFly != null && OPTDEDUCTIBLE.outNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.outNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
					</c:if>
					<c:if test="${OPTDEDUCTIBLE.combinedInOutNetworkFly != null && OPTDEDUCTIBLE.combinedInOutNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.combinedInOutNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
	               	</c:if>
	              </td>
               	  </c:when>
				<c:otherwise><td><small><spring:message code="pd.label.commontext.notApp"/></small></td></c:otherwise>
               	  </c:choose>
               	  </c:when>
            <c:otherwise><td><small><spring:message code="pd.label.commontext.notAvailable"/></small></td></c:otherwise>
               	  </c:choose>
          </tr>
          <tr>
            <td><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultAnnualBenefitLimit" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultAnnualBenefitLimit"/></a>  (<spring:message code="pd.label.commontext.individual"/>)</td>
               	  <c:choose>
            <c:when test="${(individualPlan.optionalDeductible != null && fn:length(individualPlan.optionalDeductible) gt 0 && individualPlan.optionalDeductible['Adult Annual Benefit Limit'] != null)}">
              <c:set var="OPTDEDUCTIBLE" value="${individualPlan.optionalDeductible['Adult Annual Benefit Limit']}"/>
              <c:choose>
              	<c:when test="${(OPTDEDUCTIBLE.inNetworkInd != null && OPTDEDUCTIBLE.inNetworkInd != '') || (OPTDEDUCTIBLE.inNetworkTier2Ind != null && OPTDEDUCTIBLE.inNetworkTier2Ind != '') || (OPTDEDUCTIBLE.outNetworkInd != null && OPTDEDUCTIBLE.outNetworkInd != '') || (OPTDEDUCTIBLE.combinedInOutNetworkInd != null && OPTDEDUCTIBLE.combinedInOutNetworkInd != '')}">
                  <td><c:if test="${OPTDEDUCTIBLE.inNetworkInd != null && OPTDEDUCTIBLE.inNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.inNetworkTier2Ind != null && OPTDEDUCTIBLE.inNetworkTier2Ind != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkTier2Ind}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.outNetworkInd != null && OPTDEDUCTIBLE.outNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.outNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.combinedInOutNetworkInd != null && OPTDEDUCTIBLE.combinedInOutNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.combinedInOutNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
               	  </c:if></td>               	  
                </c:when>  
                <c:otherwise><td><small><spring:message code="pd.label.commontext.notApp"/></small></td></c:otherwise>
              </c:choose>
            </c:when>
            <c:otherwise><td><small><spring:message code="pd.label.commontext.notAvailable"/></small></td></c:otherwise>
            </c:choose>
          </tr>
          
          <tr>
            <td><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultOutOfPocketMax" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultOutOfPocketMax"/></a>  (<spring:message code="pd.label.commontext.family"/>)</td>
            <c:choose>
            <c:when test="${(individualPlan.optionalDeductible != null && fn:length(individualPlan.optionalDeductible) gt 0 && individualPlan.optionalDeductible['Adult Out-of-pocket maximum'] != null)}">
              <c:set var="OPTDEDUCTIBLE" value="${individualPlan.optionalDeductible['Adult Out-of-pocket maximum']}"/>
              <c:choose>
              	<c:when test="${(OPTDEDUCTIBLE.inNetworkFly != null && OPTDEDUCTIBLE.inNetworkFly != '') || (OPTDEDUCTIBLE.inNetworkTier2Fly != null && OPTDEDUCTIBLE.inNetworkTier2Fly != '') || (OPTDEDUCTIBLE.outNetworkFly != null && OPTDEDUCTIBLE.outNetworkFly != '') || (OPTDEDUCTIBLE.combinedInOutNetworkFly != null && OPTDEDUCTIBLE.combinedInOutNetworkFly != '')}">
                  <td>
				    <c:if test="${OPTDEDUCTIBLE.inNetworkFly != null && OPTDEDUCTIBLE.inNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
               	    </c:if>
	               	<c:if test="${OPTDEDUCTIBLE.inNetworkTier2Fly != null && OPTDEDUCTIBLE.inNetworkTier2Fly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkTier2Fly}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
					</c:if>
					<c:if test="${OPTDEDUCTIBLE.outNetworkFly != null && OPTDEDUCTIBLE.outNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.outNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
					</c:if>
					<c:if test="${OPTDEDUCTIBLE.combinedInOutNetworkFly != null && OPTDEDUCTIBLE.combinedInOutNetworkFly != ''}">
					  $<fmt:formatNumber value="${OPTDEDUCTIBLE.combinedInOutNetworkFly}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
	               	</c:if>
	              </td>
               	  </c:when>
				<c:otherwise><td><small><spring:message code="pd.label.commontext.notApp"/></small></td></c:otherwise>
               	  </c:choose>
               	  </c:when>
            <c:otherwise><td><small><spring:message code="pd.label.commontext.notAvailable"/></small></td></c:otherwise>
               	  </c:choose>
          </tr>
          <tr>
            <td><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.deductible.adultOutOfPocketMax" htmlEscape="false"/>"><spring:message code="pd.label.deductible.adultOutOfPocketMax"/></a>  (<spring:message code="pd.label.commontext.individual"/>)</td>
               	  <c:choose>
            <c:when test="${(individualPlan.optionalDeductible != null && fn:length(individualPlan.optionalDeductible) gt 0 && individualPlan.optionalDeductible['Adult Out-of-pocket maximum'] != null)}">
              <c:set var="OPTDEDUCTIBLE" value="${individualPlan.optionalDeductible['Adult Out-of-pocket maximum']}"/>
              <c:choose>
              	<c:when test="${(OPTDEDUCTIBLE.inNetworkInd != null && OPTDEDUCTIBLE.inNetworkInd != '') || (OPTDEDUCTIBLE.inNetworkTier2Ind != null && OPTDEDUCTIBLE.inNetworkTier2Ind != '') || (OPTDEDUCTIBLE.outNetworkInd != null && OPTDEDUCTIBLE.outNetworkInd != '') || (OPTDEDUCTIBLE.combinedInOutNetworkInd != null && OPTDEDUCTIBLE.combinedInOutNetworkInd != '')}">
                  <td><c:if test="${OPTDEDUCTIBLE.inNetworkInd != null && OPTDEDUCTIBLE.inNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.inNetwork" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.inNetworkTier2Ind != null && OPTDEDUCTIBLE.inNetworkTier2Ind != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.inNetworkTier2Ind}" pattern = "0"/> (<spring:message code="pd.label.title.tier2Network" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.outNetworkInd != null && OPTDEDUCTIBLE.outNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.outNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.outOfNetwork" />)<br/>
               	  </c:if>
               	  <c:if test="${OPTDEDUCTIBLE.combinedInOutNetworkInd != null && OPTDEDUCTIBLE.combinedInOutNetworkInd != ''}">
               		$<fmt:formatNumber value="${OPTDEDUCTIBLE.combinedInOutNetworkInd}" pattern = "0"/> (<spring:message code="pd.label.title.combinedAndOutOfNetwork" />)
               	  </c:if></td>               	  
                </c:when>  
                <c:otherwise><td><small><spring:message code="pd.label.commontext.notApp"/></small></td></c:otherwise>
              </c:choose>
            </c:when>
            <c:otherwise><td><small><spring:message code="pd.label.commontext.notAvailable"/></small></td></c:otherwise>
            </c:choose>
          </tr>
          
          </c:if>
        </tbody>
      </table>
<script type="text/javascript">
	
	function displayBenefit(benefit){
		var benefitObj = buildObject(benefit);
		
		var str = "<p>";
		if(benefitObj.displayVal)
		{
		str += benefitObj.displayVal;
		}
		str += "</p>";
		document.write(str);
	}
	
	function displaySubToDeduct(benefit){
		var benefitObj = buildObject(benefit);
		
		var str = "";
		if(benefitObj.AppliesNetwk && benefitObj.AppliesNetwk != "" && benefitObj.AppliesNetwk != "Not Applicable"){
			str += "<i class='has-deductible-"+benefitObj.AppliesNetwk+"'></i>";
		} 
		else{
			str += "<p><spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/></p>";
		} 
		document.write(str);
	}
		  
</script>

	<div class="table mobile-table">
		<div class="row header"> 	
			<div class="cell header-name"><p><strong><spring:message code="pd.label.benefits"/></strong></p></div>  
			<div  class="cell header-name xs-hide"><p><strong><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a></strong></p></div>
    
			<div  class="cell header-name xs-hide"><p><strong><a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a></strong></p></div>
		</div>        
 
        <div class="row-detail clearfix">
		   <div class="cell column-name"><spring:message code="pd.label.dentalBenefit.braces"/>(<spring:message code="pd.label.kids"/>)</div>
		   <div class="details cell vertical-align">
		  		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
		  		<p><script>displayBenefit("${individualPlan.planTier1.ORTHODONTIA_CHILD}")</script></p>
		   </div>
          
		 	<div class="details cell vertical-align">
		 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
		   		<p><script>displayBenefit("${individualPlan.planOutNet.ORTHODONTIA_CHILD}")</script></p>
		 	</div>
	  	</div>              
          
          <c:if test="${stateCode != 'CA'}">
        <div class="row-detail clearfix">
		   <div class="cell column-name"><spring:message code="pd.label.dentalBenefit.braces"/>(<spring:message code="pd.label.adults"/>)</div>
		   <div class="details cell vertical-align">
		  		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
		  		<p><script>displayBenefit("${individualPlan.planTier1.ORTHODONTIA_ADULT}")</script></p>
		   </div>
		  	 
		 	<div class="details cell vertical-align">
		 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
		   		<p><script>displayBenefit("${individualPlan.planOutNet.ORTHODONTIA_ADULT}")</script></p>
		 	</div>
	  	 </div>              
          </c:if>
          
          
         <div class="row-detail clearfix">
		   <div class="cell column-name"><spring:message code="pd.label.dentalBenefit.majorDentalCare"/> (<spring:message code="pd.label.kids"/>)</div>
		   <div class="details cell vertical-align">
		  		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
		  		<p><script>displayBenefit("${individualPlan.planTier1.MAJOR_DENTAL_CARE_CHILD}")</script></p>
		   </div>
	 
		 	<div class="details cell vertical-align">
		 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
		   		<p><script>displayBenefit("${individualPlan.planOutNet.MAJOR_DENTAL_CARE_CHILD}")</script></p>
		 	</div>
	  	 </div>              
       
       
         <div class="row-detail clearfix">
		   <div class="cell column-name"><spring:message code="pd.label.dentalBenefit.majorDentalCare"/> (<spring:message code="pd.label.adults"/>)</div>
		   <div class="details cell vertical-align">
		  	<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
		  	<p><script>displayBenefit("${individualPlan.planTier1.MAJOR_DENTAL_CARE_ADULT}")</script></p>
		   </div>
		   
		 	<div class="details cell vertical-align">
		 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
		   		<p><script>displayBenefit("${individualPlan.planOutNet.MAJOR_DENTAL_CARE_ADULT}")</script></p>
		 	</div>
	  	 </div>              
        
        
         <div class="row-detail clearfix">
		   <div class="cell column-name"><spring:message code="pd.label.dentalBenefit.checkups.child"/> (<spring:message code="pd.label.kids"/>)</div>
		   <div class="details cell vertical-align">
		  	<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
		  	<p><script>displayBenefit("${individualPlan.planTier1.DENTAL_CHECKUP_CHILDREN}")</script></p>
		   </div>
	
		 	<div class="details cell vertical-align">
		 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
		   		<p><script>displayBenefit("${individualPlan.planOutNet.DENTAL_CHECKUP_CHILDREN}")</script></p>
		 	</div>
	  	 </div>              
         
         <c:if test="${stateCode != 'CA'}">
         <div class="row-detail clearfix">
			   <div class="cell column-name"><spring:message code="pd.label.dentalBenefit.checkups.adult"/></div>
			   <div class="details cell vertical-align">
			  	<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
			  	<p><script>displayBenefit("${individualPlan.planTier1.ACCIDENTAL_DENTAL}")</script></p>
			   </div>
		
			 	<div class="details cell vertical-align">
			 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
			   		<p><script>displayBenefit("${individualPlan.planOutNet.ACCIDENTAL_DENTAL}")</script></p>
			 	</div>
	  	  </div>                       
          </c:if>
          
           <div class="row-detail clearfix">
			   <div class="cell column-name"><spring:message code="pd.label.dentalBenefit.fillings"/> (<spring:message code="pd.label.kids"/>)</div>
			   <div class="details cell vertical-align">
			  	<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
			  	<p><script>displayBenefit("${individualPlan.planTier1.BASIC_DENTAL_CARE_CHILD}")</script></p>
			   </div>
			 
			 	<div class="details cell vertical-align">
			 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
			   		<p><script>displayBenefit("${individualPlan.planOutNet.BASIC_DENTAL_CARE_CHILD}")</script></p>
			 	</div>
	  	   </div>                   
          
          <div class="row-detail clearfix">
			   <div class="cell column-name"><spring:message code="pd.label.dentalBenefit.fillings"/> (<spring:message code="pd.label.adults"/>)</div>
			   <div class="details cell vertical-align">
			  	<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
			  	<p><script>displayBenefit("${individualPlan.planTier1.BASIC_DENTAL_CARE_ADULT}")</script></p>
			   </div>
		 
			 	<div class="details cell vertical-align">
			 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
			   		<p><script>displayBenefit("${individualPlan.planOutNet.BASIC_DENTAL_CARE_ADULT}")</script></p>
			 	</div>
	  	   </div>          
          
          
          <div class="row-detail clearfix">
			   <div class="cell column-name"><spring:message code="pd.label.children.dental.link9"/></div>
			   <div class="details cell vertical-align">
			  	<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.inNetwork"/></p>
			  	<p><script>displayBenefit("${individualPlan.planTier1.RTN_DENTAL_ADULT}")</script></p>
			   </div>
 
			 	<div class="details cell vertical-align">
			 		<p class="mobile-show" style="float:left; width:50%"><spring:message code="pd.label.title.outOfNetwork"/></p>
			   		<p><script>displayBenefit("${individualPlan.planOutNet.RTN_DENTAL_ADULT}")</script></p>
			 	</div>
	  	   </div>          
     </div>
    
	  
  	</div>
  	<!--rightpanel--> 



<script type="text/javascript" src="<c:url value="/resources/js/tmpl.js" />"></script> 
<script type="text/javascript">
$(document).ready(function(){
	
	var hasno = $('td').find('i');	
	$(hasno).parent('td').addClass('txt-center');
	
	  $('a[rel=tooltip]').tooltip();

    
	$(".collapse").collapse();
	
	$("input").each(function(i){ 
		$(this).attr('checked', false);
	});//input end

	$('a.btn').popover({
		placement:'left',
		
	});//popover 
	 
    $('span.icon-ok').parents('li').css('list-style', 'none');
	
});

</script>