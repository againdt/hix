<script type="text/ng-template" id="incarceration.jsp">
 <div class="control-group" ng-controller="incarcerationController">
    <h3>
        <spring:message code="label.lce.entereventdetails"/>
    </h3>
    <form class="form-horizontal" id="incarcerationForm" name="incarcerationForm" novalidate>
        <spring:message code="label.lce.indicateIncarStatus"/>
        <div class="gutter10">
            <!-- <h4>Family and Household Summary</h4> -->
            <table id="" class="table table-striped" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold" role="presentation">
                <tbody>
                    <tr>
                        <td>
                            <spring:message code="label.lce.firstname"/>
                        </td>
                        <td>
                            <spring:message code="label.lce.lastname"/>
                        </td>
                        <td>
                            <spring:message code="label.lce.relationship"/>
                        </td>
                        <td>
                            <spring:message code="label.lce.seekingcoverage"/>
                        </td>
                        <td><spring:message code="label.lce.incarceratedNoQ"/></td>
                        <td><spring:message code="label.lce.eventdate"/></td>
                    </tr>
                    <tr ng-repeat="householdMember in taxHousehold.householdMember | filter: activeDeathFilter  track by $index">
                        <td>{{householdMember.name.firstName}}</td>
                        <td>{{householdMember.name.lastName}}</td>
                        <td>
                            {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                        </td>
                        <td>
                            <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
                            <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
                        </td>
                        <td>
                            <a href class="btn btn-primary btn-small" ng-if="jsonObject.singleStreamlinedApplication.taxHousehold[0].householdMember[getOriginalIndex(householdMember,taxHousehold.householdMember)].incarcerationStatus.incarcerationStatusIndicator" ng-click="jsonObject.singleStreamlinedApplication.taxHousehold[0].householdMember[getOriginalIndex(householdMember,taxHousehold.householdMember)].incarcerationStatus.incarcerationStatusIndicator=!jsonObject.singleStreamlinedApplication.taxHousehold[0].householdMember[getOriginalIndex(householdMember,taxHousehold.householdMember)].incarcerationStatus.incarcerationStatusIndicator;eventInfo.eventSubCategory='NOT_INCARCERATED'"><spring:message code="label.lce.yes"/></a>
                            <a href class="btn btn-secondary btn-small" ng-if="!jsonObject.singleStreamlinedApplication.taxHousehold[0].householdMember[getOriginalIndex(householdMember,taxHousehold.householdMember)].incarcerationStatus.incarcerationStatusIndicator" ng-click="jsonObject.singleStreamlinedApplication.taxHousehold[0].householdMember[getOriginalIndex(householdMember,taxHousehold.householdMember)].incarcerationStatus.incarcerationStatusIndicator=!jsonObject.singleStreamlinedApplication.taxHousehold[0].householdMember[getOriginalIndex(householdMember,taxHousehold.householdMember)].incarcerationStatus.incarcerationStatusIndicator;eventInfo.eventSubCategory='INCARCERATION'"><spring:message code="label.lce.no"/></a>
                        </td>
                        <td>
                            <div ng-if="ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember[getOriginalIndex(householdMember,taxHousehold.householdMember)].incarcerationStatus.incarcerationStatusIndicator != householdMember.incarcerationStatus.incarcerationStatusIndicator" style="width: 112px; position: relative; left: -10px;"  ng-model="dummyBinding">
                                <div id="incarcerationDate{{$index}}"  dynamic-name="$index" placeholder="incarcerationDate"  ng-required="ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember[getOriginalIndex(householdMember,taxHousehold.householdMember)].incarcerationStatus.incarcerationStatusIndicator != householdMember.incarcerationStatus.incarcerationStatusIndicator " class="input-append" ng-model="eventInfoArrayTemp[getOriginalIndex(householdMember,taxHousehold.householdMember)].dateOfChange" datetimez  check-after="0" >
                                    <label for="incarcerationDate{{$index}}"></label>
                                    <input type="text" id="incarcerationDate{{$index}}" class="span10" ng-model="eventInfoArrayTemp[getOriginalIndex(householdMember,taxHousehold.householdMember)].dateOfChange" placeholder="MM/DD/YYYY">
                                    <span class="add-on">
                                    <i class="icon-calendar"></i>
                                    </span>
                                </div>
                                <div class=" margin5-t">
                                    <span class="validation-error-text-container" for="incarcerationDate{{$index}}" ng-if="submitted && $eval('incarcerationForm.incarcerationDate'+$eval('$index')+'.$error.required')"><spring:message code="label.lce.enterValidDate"/></span>
                                    <span class="validation-error-text-container" for="incarcerationDate{{$index}}" ng-if="submitted && $eval('incarcerationForm.incarcerationDate'+$eval('$index')+'.$error.checkAfter')"><spring:message code="label.lce.futureNotAllowed"/></span>
                                    
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- gutter10 -->
    </form>
    <dl class="dl-horizontal pull-right">
        <input type="button" value="<spring:message code='label.lce.back'/>" class="btn btn-secondary" onclick="history.go(-1);return false;" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
        <a id="next" ng-if="incarcerationForm.$valid" class="btn btn-primary" ng-click="updateChangedHouseHold();updateApplyingCoverage();next();" ng-href="" >
            <spring:message code="label.lce.next"/>
        </a>
        <a ng-if="!incarcerationForm.$valid" class="btn btn-primary" ng-click="$parent.submitted=true;">
            <spring:message code="label.lce.next"/>
        </a>
    </dl>
</div>
<!-- gutter10 -->
</script>