(function (views) {
	views.Filter = Backbone.View.extend({
		
		events : {
			'click a.filter': 'filter',
			'click a.showAll': 'showAll'
		},
		
		el : '#filter',
		initialize : function () {
			_.bindAll (this, 'render');
			var self = this;

			self.tmpl = _.template($('#tmpFilter').html());
			self.collection.bind('reset', this.render);

		},
		
		render : function () {
            var self;
            self = this;

			var html = this.tmpl(self.collection.info());
			$(this.el).html(html);
		},
		
		getFilterRules: function () {
			var rules = new Object();
			
			var planTypeObj = new Object();
			planTypeObj['type'] = 'equalTo';
			planTypeObj['field'] = 'level';
			planTypeObj['value'] = $('#planType').val();
			rules['planType'] = planTypeObj;

			var nameObj = new Object();
			nameObj['type'] = 'pattern';
			nameObj['field'] = 'name';
			nameObj['value'] = $('#planName').val();
			rules['planName'] = nameObj;
			
			var issuerObj = new Object();
			issuerObj['type'] = 'oneOf';
			issuerObj['field'] = 'issuer';
			issuerObj['value'] = new Array();
			_.each(this.collection.issuerList, function(issuer){
				if($("#issuer_"+issuer.replace(/[\s\.?]/g,"_")).is(':checked')){
					issuerObj['value'].push(issuer);
				}
			});
			
			rules['issuer'] = issuerObj;
			
			return rules;
		},
		
		filter: function (e) {
			e.preventDefault();
			
			var rules = this.getFilterRules();
			this.collection.page = 1;
			this.collection.setFieldFilter(rules);
			this.collection.loadIssuers();
			this.collection.preserveOtherOptions();
		},
		
		showAll: function (e) {
			e.preventDefault();
			this.collection.page = 1;
			this.collection.resetFieldFilter();
			this.collection.loadIssuers();
			this.collection.preserveOtherOptions();
		}
	});
})(App.views);