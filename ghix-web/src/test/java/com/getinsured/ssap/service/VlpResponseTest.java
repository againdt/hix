package com.getinsured.ssap.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.ssap.model.hub.vlp.response.VlpResponse;

/**
 * Tests VLP response JSON mappings.
 *
 * @author Yevgen Golubenko
 * @since 2/27/19
 */
@RunWith(MockitoJUnitRunner.class)
public class VlpResponseTest {
  private static final Logger log = LoggerFactory.getLogger(VlpResponseTest.class);

  private static final String JSON_NO_NULLS = "vlp/vlp_response_success_nonulls.json";
  private static final String JSON_NO_NULLS_MULTIPLE = "vlp/vlp_response_success_nonulls_multiple.json";
  private static final String JSON_EXCEPTION_EXAMPLE = "vlp/exception_example.json";

  private ObjectMapper om = new ObjectMapper();

  @Test
  public void vlpResponseWithoutNulls() throws Exception {
    ClassPathResource classPathResource = new ClassPathResource(JSON_NO_NULLS);
    if (classPathResource.exists()) {
      VlpResponse verificationResponse = om.readValue(classPathResource.getFile(), VlpResponse.class);
      log.info("Read class from JSON: {}", verificationResponse);
      Assert.assertNotNull("Unable to create class from json object", verificationResponse);
    } else {
      Assert.fail("Unable to load JSON resource for testing: " + JSON_NO_NULLS);
    }
  }

  @Test
  public void vlpResponseWithoutNullsMultiple() throws Exception {
    ClassPathResource classPathResource = new ClassPathResource(JSON_NO_NULLS_MULTIPLE);
    if (classPathResource.exists()) {
      VlpResponse verificationResponse = om.readValue(classPathResource.getFile(), VlpResponse.class);
      log.info("Read class from JSON: {}", verificationResponse);
      Assert.assertNotNull("Unable to create class from json object", verificationResponse);
    } else {
      Assert.fail("Unable to load JSON resource for testing: " + JSON_NO_NULLS_MULTIPLE);
    }
  }

  @Test
  public void vlpResponseException() throws Exception {
    ClassPathResource classPathResource = new ClassPathResource(JSON_EXCEPTION_EXAMPLE);
    if (classPathResource.exists()) {
      VlpResponse verificationResponse = om.readValue(classPathResource.getFile(), VlpResponse.class);
      log.info("Read class from JSON: {}", verificationResponse);
      Assert.assertNotNull("Unable to create class from json object", verificationResponse);
    } else {
      Assert.fail("Unable to load JSON resource for testing: " + JSON_EXCEPTION_EXAMPLE);
    }
  }
}
