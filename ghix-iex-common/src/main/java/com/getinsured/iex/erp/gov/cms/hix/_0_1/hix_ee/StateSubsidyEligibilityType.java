package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A data type for an
 * 				Advance Premium Tax Credit (APTC) eligibility determination outcome.
 *
 *
 * <p>Java class for APTCEligibilityType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="APTCEligibilityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hix.cms.gov/0.1/hix-ee}EligibilityType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}APTC" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StateSubsidyEligibilityType", propOrder = {
        "stateSubsidy"
})
public class StateSubsidyEligibilityType
        extends EligibilityType
{

    @XmlElement(name = "StateSubsidy")
    protected StateSubsidyCalculationType stateSubsidy;

    /**
     * Gets the value of the aptc property.
     *
     * @return
     *     possible object is
     *     {@link StateSubsidyCalculationType }
     *
     */
    public StateSubsidyCalculationType getStateSubsidy() {
        return stateSubsidy;
    }

    /**
     * Sets the value of the aptc property.
     *
     * @param value
     *     allowed object is
     *     {@link StateSubsidyCalculationType }
     *
     */
    public void setStateSubsidy(StateSubsidyCalculationType value) {
        this.stateSubsidy = value;
    }

}