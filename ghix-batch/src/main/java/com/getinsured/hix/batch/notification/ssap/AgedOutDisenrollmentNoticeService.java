package com.getinsured.hix.batch.notification.ssap;

import java.util.List;

import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface AgedOutDisenrollmentNoticeService {

	public List<String> processAgeOutDependents(String caseNumbers);
}