(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .controller('searchResultsCtrl', ['$scope', '$http', '$state', '$timeout', 'searchNotifyingService', 'searchService', 'uiGridConstants', 'searchReconciliation', 'searchPaginationService', 'dataStorageService', 'clearSearchResultsService',
            function ($scope, $http, $state, $timeout, searchNotifyingService, searchService, uiGridConstants, searchReconciliation, searchPaginationService, dataStorageService, clearSearchResultsService) {
            var searchResults = {};

            var paginationOptions = {
                currentPage: 1,
                pageSize: 15,
                sortByField: null,
                sort: null,
                sortOrder: null
            };

            $scope.currentPage = 1;
            $scope.pageSize = paginationOptions.pageSize;

            $scope.gridOptionsForSearchResults = {
                paginationPageSizes: [$scope.pageSize, $scope.pageSize * 2, $scope.pageSize * 3],
                paginationPageSize: paginationOptions.pageSize,
                minRowsToShow: paginationOptions.pageSize * 1,
                useExternalPagination: true,
                useExternalSorting: true,
                enableSorting: true,
                enableColumnMenus: false,
                columnDefs: [
                    { field: 'enrollmentId', displayName: 'Policy ID'.toUpperCase(), cellTemplate:"<a href=\"\" ng-click=\"grid.appScope.showDisrepancyDetail(row.entity)\"> {{row.entity.enrollmentId}} </a>"},
                    // { field: 'issuerEnrollmentId', displayName: 'Issuer ID'.toUpperCase()  }, //totalNumberOfFiles
                    { field: 'subscriberIdentifier', displayName: 'Subscriber ID'.toUpperCase()  }, //totalEnrlCntInFile
                    { field: 'subscriberName', displayName: 'Name'.toUpperCase()  }, //totalEnrlInHix
                    { field: 'lastReconcileDate', displayName: 'Reconciliation Date'.toUpperCase() }, //totalEnrlDiscrepancyCount
                    //{ field: 'discrepancyStatuses', displayName: 'Status'.toUpperCase() , enableSorting: false , cellTemplate:'<span style="display:block; font-size: 10px; padding: 0px; margin: 0px;" ng-repeat="item in row.entity[col.field]">{{item.status}} : {{item.count}}</span>'}, //enrollmentFailedCount
                    { field: 'numberOfTimeReconciled', displayName: 'Reconciliation Count'.toUpperCase() }, // Kuldeep to add new field
                    { field: 'benefitEffectiveDate', displayName: 'Coverage Date'.toUpperCase() }, //enrlMissinginHixCount
                    { name: 'discrepancyStatuses' ,  displayName: 'Status', enableColumnMenu: false,  enableSorting: false , cellTemplate:'<a href=\"\" ng-click=\"grid.appScope.showDisrepancyDetail(row.entity)\"> <label style=\"float:left\" ng-repeat=\"item in row.entity[col.field]\" ng-class=\"{\'openBg\': item.status === \'OPEN\',\'holdBg\': item.status === \'HOLD\', \'resolvedBg\': item.status === \'RESOLVED\',\'autofixedBg\': item.status === \'AUTOFIXED\'}\">{{item.count}}</label></a>' }
                ],
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                        if (sortColumns.length == 0) {
                            paginationOptions.sort = null;
                            paginationOptions.sortOrder = null;
                        } else {
                            paginationOptions.sortOrder = sortColumns[0].sort.direction;
                            paginationOptions.sortByField = sortColumns[0].field;
                            paginationOptions.currentPage = 1;
                        }
                        searchPaginationService.notify(paginationOptions);
                        if($scope.gridApi.pagination.getPage() > 1){
                            $scope.gridApi.pagination.seek(1);
                        }
                    });

                    gridApi.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
                        paginationOptions.currentPage = newPage;
                        paginationOptions.pageSize = pageSize;
                        $scope.gridOptionsForSearchResults.virtualizationThreshold =  $scope.gridOptionsForSearchResults.paginationPageSize * 2;
                        $scope.pageSize = pageSize;
                        $scope.currentPage = newPage;
                        $scope.totalPage = Math.ceil($scope.gridOptionsForSearchResults.totalItems / $scope.pageSize);
                        searchPaginationService.notify(paginationOptions);

                    });
                }
            };

            $scope.showDisrepancyDetail = function (enrollementObj) {
                searchReconciliation.setEnrollment(enrollementObj);
                dataStorageService.setItem("enrollementData", enrollementObj);
                $state.go('detail');
            };

            $scope.getTotalResultsCount = function () {
                return $scope.gridOptionsForSearchResults.totalItems
            };

            clearSearchResultsService.subscribe($scope, function searchResultsAvailable() {
                $timeout(function() {
                    $scope.gridOptionsForSearchResults.totalItems = 0;
                    $scope.gridOptionsForSearchResults.data = [];
                },0);
            });

            searchNotifyingService.subscribe($scope, function searchResultsAvailable() {
                searchResults = searchService.getSearchResultsData().enrlDiscrepancyDataDTOList;
                for(var i = 0; i < searchResults.length; i++){
                    //searchResults[i].subscriberName = searchResults[i].subscriberFirstName + " " + searchResults[i].subscriberLastName;
                    // searchResults[i].discrepancyStatusDataMap = JSON.stringify(searchResults[i].discrepancyStatusDataMap).replace(/[{}]/g,"");
                    searchResults[i].discrepancyStatuses = [];

                    var keys = Object.keys(searchResults[i].discrepancyStatusDataMap);
                    for (var k=0; k<keys.length; k++) {
                        searchResults[i].discrepancyStatuses.push({"status": keys[k] , "count": searchResults[i].discrepancyStatusDataMap[keys[k]] });
                    }
                }

                $timeout(function() {
                    $scope.gridOptionsForSearchResults.totalItems = searchService.getSearchResultsData().totalRecordCount;
                $scope.gridOptionsForSearchResults.data = searchResults;
                },0);
            });

            // $scope.gridOptionsForSearchResults.data = searchResults;
        }]);
})();