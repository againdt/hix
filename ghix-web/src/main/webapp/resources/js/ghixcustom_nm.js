$(document).ready(function(){
	/*HIX-17077*/
	//$('h1').parent('div').addClass('titlebar');
	
	//add .header to thead tr
    $('.table thead tr').addClass('header');
    
    
    $('.clearForm').click(function(){
        $(this).parents("form").find("input").not(':button,:hidden').val("");
        $('label.error').hide();
	});
    
   $('#rightpanel .graydrkbg').find('.btn').addClass('btn-small');
    
    //HIX-14907
   // $('a[title="Account"]').text('My Information'); commenting this as part of HIX-24608
	
	//HIX-22629
	$('a[title="Account"]').append(' <i class="caret"></i>');	
	
	/*HIX-18823 Remove Modal on press of escape button */
	/*$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			e.preventDefault();
			if ($('#editdependent, #einModal, #worksite').is(':visible')){
				e.preventDefault();
			}else{
				$('.modal-backdrop, .modal-backdrop.in').remove();
				$('#addressIFrame, .addressErrModal, #modal').remove();
			}
		}  
	});*/
	
	/*$('.modalClose').live('click',function(){
		$('#modal, .modal-backdrop').remove();
		window.location.reload();
	});*/
	
	
	/*HIX-25202 fixed as per changed requirement*/
	$('#hidekeep-currentplnTab').hide();
	
	/* HIX-35709 */
	$('#ticketStatus').removeAttr('multiple').removeAttr('size');
});
