package com.getinsured.hix.indportal.dto;

public class IndPortalAddlnRegDetails {
	
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zipcode;
	private String phoneNumber;
	private String prefSpokenLang;
	private String prefWrittenLang;
	private String prefCommunication;
	private String dob;
	
	
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPrefSpokenLang() {
		return prefSpokenLang;
	}
	public void setPrefSpokenLang(String prefSpokenLang) {
		this.prefSpokenLang = prefSpokenLang;
	}
	public String getPrefWrittenLang() {
		return prefWrittenLang;
	}
	public void setPrefWrittenLang(String prefWrittenLang) {
		this.prefWrittenLang = prefWrittenLang;
	}
	public String getPrefCommunication() {
		return prefCommunication;
	}
	public void setPrefCommunication(String prefCommunication) {
		this.prefCommunication = prefCommunication;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}

}
