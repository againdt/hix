package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.ssap.renewal.service.OutboundService;
import com.getinsured.hix.batch.ssap.renewal.util.OutboundPartitionerParams;

/**
 * Outbound Reader class is used to get read SSAP Application ID from Partitioner.
 * 
 * @since September 12, 2019
 */
public class OutboundReader implements ItemReader<Long> {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutboundReader.class);
	List<Long> readSsapApplicationIdList;
	private int partition;
	private int loopCount;
	private int startIndex;
	private int endIndex;

	private OutboundService outboundService;
	private OutboundPartitionerParams outboundPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			partition = executionContext.getInt("partition");
			startIndex = executionContext.getInt("startIndex");
			endIndex = executionContext.getInt("endIndex");
			LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);

			if (null != outboundPartitionerParams && CollectionUtils.isNotEmpty(outboundPartitionerParams.getRenewalApplicationIdList())) {

				if (startIndex < outboundPartitionerParams.getRenewalApplicationIdList().size()
						&& endIndex <= outboundPartitionerParams.getRenewalApplicationIdList().size()) {
					readSsapApplicationIdList = new CopyOnWriteArrayList<Long>(outboundPartitionerParams.getRenewalApplicationIdList().subList(startIndex, endIndex));
				}
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("readSsapApplicationList is null: {}", CollectionUtils.isEmpty(readSsapApplicationIdList));
			}
		}
	}

	@Override
	public Long read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		boolean readerFlag = false;
		Long readerSsapApplicationID = null;

		if (CollectionUtils.isNotEmpty(readSsapApplicationIdList)) {

			if (loopCount < readSsapApplicationIdList.size()) {
				loopCount++;
				readerFlag = true;
				readerSsapApplicationID = readSsapApplicationIdList.get(loopCount - 1);
			}
		}

		if (!readerFlag) {
			LOGGER.warn("Unable to find SSAP Application.");
		}
		return readerSsapApplicationID;
	}

	public OutboundService getOutboundService() {
		return outboundService;
	}

	public void setOutboundService(OutboundService outboundService) {
		this.outboundService = outboundService;
	}

	public OutboundPartitionerParams getOutboundPartitionerParams() {
		return outboundPartitionerParams;
	}

	public void setOutboundPartitionerParams(OutboundPartitionerParams outboundPartitionerParams) {
		this.outboundPartitionerParams = outboundPartitionerParams;
	}
}
