package com.getinsured.eligibility.at.ref.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chopra_s
 * 
 */
public class EnrollmentAttributesDTO {

	private long ssapApplicationId;

	private long healthPlanId;

	private String healthPlanLevel;

	private long healthEnrollmentId;

	private String healthSubscriberId;

	private Float healthNetPremiumAmt;
	
	private List<String> healthEnrollees = new ArrayList<String>();

	private long dentalPlanId;

	private long dentalEnrollmentId;

	private String dentalSubscriberId;
	
	private Float dentalNetPremiumAmt;

	private List<String> dentalEnrollees = new ArrayList<String>();
	
	private String costSharing;

	public String getCostSharing() {
		return costSharing;
	}

	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}

	public Float getHealthNetPremiumAmt() {
		return healthNetPremiumAmt;
	}

	public void setHealthNetPremiumAmt(Float healthNetPremiumAmt) {
		this.healthNetPremiumAmt = healthNetPremiumAmt;
	}

	public Float getDentalNetPremiumAmt() {
		return dentalNetPremiumAmt;
	}

	public void setDentalNetPremiumAmt(Float dentalNetPremiumAmt) {
		this.dentalNetPremiumAmt = dentalNetPremiumAmt;
	}

	public List<String> getHealthEnrollees() {
		return healthEnrollees;
	}

	public void setHealthEnrollees(List<String> healthEnrollees) {
		this.healthEnrollees = healthEnrollees;
	}

	public List<String> getDentalEnrollees() {
		return dentalEnrollees;
	}

	public void setDentalEnrollees(List<String> dentalEnrollees) {
		this.dentalEnrollees = dentalEnrollees;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public long getHealthPlanId() {
		return healthPlanId;
	}

	public void setHealthPlanId(long healthPlanId) {
		this.healthPlanId = healthPlanId;
	}

	public String getHealthPlanLevel() {
		return healthPlanLevel;
	}

	public void setHealthPlanLevel(String healthPlanLevel) {
		this.healthPlanLevel = healthPlanLevel;
	}

	public long getHealthEnrollmentId() {
		return healthEnrollmentId;
	}

	public void setHealthEnrollmentId(long healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}

	public String getHealthSubscriberId() {
		return healthSubscriberId;
	}

	public void setHealthSubscriberId(String healthSubscriberId) {
		this.healthSubscriberId = healthSubscriberId;
	}

	public long getDentalPlanId() {
		return dentalPlanId;
	}

	public void setDentalPlanId(long dentalPlanId) {
		this.dentalPlanId = dentalPlanId;
	}

	public long getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}

	public void setDentalEnrollmentId(long dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}

	public String getDentalSubscriberId() {
		return dentalSubscriberId;
	}

	public void setDentalSubscriberId(String dentalSubscriberId) {
		this.dentalSubscriberId = dentalSubscriberId;
	}

}
