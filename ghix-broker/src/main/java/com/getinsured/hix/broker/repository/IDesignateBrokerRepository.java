package com.getinsured.hix.broker.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.model.entity.DesignateAssister;

/**
 * Repository for CRUD operations on {@link DesignateBroker}
 * 
 */
public interface IDesignateBrokerRepository extends JpaRepository<DesignateBroker, Integer> {

	

	/**
	 * Finds {@link DesignateBroker} based on external employer id coming from
	 * AHBX.
	 * 
	 * @param extEmployerId
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findDesigBrokerByExternalEmployerId(int extEmployerId);

	/**
	 * Finds {@link DesignateBroker} based on external individual id coming from
	 * AHBX.
	 * 
	 * @param extIndividualId
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findDesigBrokerByExternalIndividualId(int extIndividualId);
	
	/**
	 * 
	 * @param individualId
	 * @return
	 */
	DesignateBroker findDesigBrokerByIndividualId(Integer individualId);

	/**
	 * Finds {@link DesignateAssister} based on external individual id and agent
	 * id coming from AHBX.
	 * 
	 * @param agent
	 *            Id
	 * @param externalIndividualId
	 * @return {@link DesignateAssister}
	 */
	@Query("FROM DesignateBroker d where d.brokerId = :brokerId AND d.externalIndividualId = :externalIndividualId")
	DesignateBroker findDesigBrokerByAgentAndIndividualId(@Param(BrokerConstants.BROKER_ID) int brokerId,
			@Param("externalIndividualId") int externalIndividualId);

	/**
	 * Finds {@link DesignateBroker} based on external employer id and agent id
	 * coming from AHBX.
	 * 
	 * @param agent
	 *            Id
	 * @param externalEmployerId
	 * @return {@link DesignateBroker}
	 */
	@Query("FROM DesignateBroker d where d.brokerId = :brokerId AND d.externalEmployerId = :externalEmployerId")
	DesignateBroker findDesigBrokerByAgentAndEmployerId(@Param(BrokerConstants.BROKER_ID) int brokerId,
			@Param("externalEmployerId") int externalEmployerId);

	/**
	 * Finds {@link DesignateBroker} based on external employer id and agent id
	 * coming from AHBX and status
	 * 
	 * @param brokerId
	 * @param externalEmployerId
	 * @param status
	 * @return {@link DesignateBroker}
	 */
	@Query("FROM DesignateBroker d where d.brokerId = :brokerId AND d.externalEmployerId = :externalEmployerId AND d.status = :status")
	DesignateBroker findDesigBrokerByAgentAndEmployerIdAndStatus(@Param(BrokerConstants.BROKER_ID) int brokerId,
			@Param("externalEmployerId") int externalEmployerId, @Param("status") Status status);

	/**
	 * Finds {@link DesignateBroker} based on external individual id and agent id
	 * coming from AHBX and status
	 * 
	 * @param brokerId
	 * @param externalIndividualId
	 * @param status
	 * @return {@link DesignateBroker}
	 */
	@Query("FROM DesignateBroker d where d.brokerId = :brokerId AND d.externalIndividualId = :externalIndividualId AND d.status = :status")
	DesignateBroker findDesigBrokerByAgentAndIndividualAndStatus(@Param(BrokerConstants.BROKER_ID) int brokerId,
			@Param("externalIndividualId") int externalIndividualId, @Param("status") Status status);
	
	
	DesignateBroker findBrokerByEmployerId(int employerId);
	
	/**
	 * Finds {@link DesignateBroker} based on  employer id and  status
	 * 
	 * @param employerId
	 * @param status
	 * @return {@link DesignateBroker}
	 */
	@Query("FROM DesignateBroker d where d.employerId = :employerId AND d.status = :status")
	DesignateBroker findDesigBrokerByEmployerIdAndStatus(@Param("employerId") int employerId, @Param("status") Status status);
	
	@Query("FROM DesignateBroker d where d.brokerId = :brokerId")
	List<DesignateBroker> findByBrokerId(@Param("brokerId") int brokerId);
	
	@Modifying
	@Transactional
	@Query("update DesignateBroker db set db.brokerId = :targetBrokerId, db.updated = :updated, db.updatedBy = :updatedBy where db.brokerId = :brokerId")
	int updateDesignateBrokerByBrokerId(@Param("brokerId") int brokerId, @Param("targetBrokerId") int targetBrokerId, 
			@Param("updated") Date updated, @Param("updatedBy") int updatedBy);
	
	@Query("FROM DesignateBroker d where d.brokerId = :brokerId and d.updated > :updated")
	List<DesignateBroker> findByBrokerIdAndUpdatedTime(@Param("brokerId") int brokerId, @Param("updated") Date updated);

}