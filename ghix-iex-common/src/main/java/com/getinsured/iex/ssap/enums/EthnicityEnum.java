
package com.getinsured.iex.ssap.enums;


/**
 * 
 * @author raguram_p
 *
 */

public enum EthnicityEnum {

    CUBAN("2182-4","Cuban"),
    MEXICAN_OR_MEXAMER_OR_CHICANO("2148-5","Mexican, Mexican American or Chicano/a"),
    PUERTO_RICAN("2180-8","Puerto Rican"),
    OTHER("0000-0","Other");

    
    
    private String code = null;
    private String label=null;
    
    private EthnicityEnum(String code,String label)
    {
    	this.code = code;
    	this.label=label;
    }
    
  
    
    public static String value(String v) {
		if (v != null) {
			final EthnicityEnum data = readValue(v);
			if (data != null) {
				return data.getCode();
			}
		}
		return null;
	}

	public static EthnicityEnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static EthnicityEnum readValue(String v) {
		EthnicityEnum data = null;
		for (EthnicityEnum c : EthnicityEnum.class.getEnumConstants()) {
			if (c.getLabel().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	

}
