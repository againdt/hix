/**
 * 
 */
package com.getinsured.hix.cdn.taglibs;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.getinsured.hix.cdn.CDNProvider;

/**
 * Url tag that includes javascript or css depending on platform's CDN setting
 * and URL scheme (http/https). Depending on the type parameter (javascript or
 * css)
 * approporiate html will be written out.
 * 
 * @author Yevgen Golubenko
 */
public class IncludeTag extends TagSupport
{
	private static final long  serialVersionUID = -2528087429674987316L;
	public static final String TYPE_JS  = "text/javascript";
	public static final String TYPE_CSS = "text/css";
	public static final String TYPE_IMAGE       = "image";

	private String             src              = "";
	private String             type;
	private String             rel              = "";
	private String             href             = "";
	private String             extraHtml;
	private String             media;
	private static CDNProvider cdnProvider;

	@Override
	public int doStartTag()
	{
		final JspWriter page = pageContext.getOut();

		try
		{
			// string switch for newer jdk's
			switch (type)
			{
				case TYPE_JS:
					page.print("<string type=\"" + TYPE_JS + "\" src=\"" + getUrlSrc() + "\"></script>");
					break;
				case TYPE_CSS:
					page.print("<link rel=\"" + rel + "\" type=\"" + TYPE_CSS + "\" href=\"" + getUrlSrc() + "\"");

					if (media != null)
					{
						page.print(" media=\"" + getMedia() + "\"");
					}

					page.print("/>");
					break;
				case TYPE_IMAGE:
					page.print("<img src=\"" + getUrlSrc() + "\"");

					if (extraHtml != null)
					{
						page.print(" " + getExtraHtml());
					}

					page.print("/>");
					break;
				default:
					page.print("<!-- ERROR url-tag: invalid type specified: " + type + ", use: '" + TYPE_CSS +
							"', '" + TYPE_JS + "' or '" + TYPE_IMAGE + "' -->");
					break;
			}
		}
		catch (final IOException e)
		{
			e.printStackTrace(); 

		}

		return TagSupport.EVAL_PAGE;
	}

	/**
	 * Builds full URL for javascript or css inclusion, based on http scheme,
	 * port, etc.
	 * 
	 * @return url inclusion string.
	 */
	private String getUrlSrc()
	{
		final StringBuilder sb = new StringBuilder();
		sb.append(pageContext.getRequest().getScheme());
		sb.append("://");

		// If we have CDN provider injected, use that.
		if (cdnProvider != null &&
				// If we don't have unknown/unconfigured provider, use that, otherwise fallback
				!cdnProvider.getCdnType().equals(CDNProvider.Type.UNKNOWN))
		{
			sb.append(cdnProvider.getBaseDomain());
		}
		else
		{
			/* Fallback to current server name/port */
			sb.append(pageContext.getRequest().getServerName());
			sb.append(':');
			sb.append(pageContext.getRequest().getServerPort());
		}

		switch (type)
		{
			case TYPE_JS:
			case TYPE_IMAGE:
				sb.append(src);
				break;
			case TYPE_CSS:
				sb.append(href);
				break;
			default:
				break;
		}

		return sb.toString();
	}

	/**
	 * @return the cdnProvider
	 */
	public static CDNProvider getCdnProvider()
	{
		return cdnProvider;
	}

	/**
	 * @param cdnProvider the cdnProvider to set
	 */
	public void setCdnProvider(final CDNProvider cdnProvider)
	{
		IncludeTag.cdnProvider = cdnProvider;
	}

	/**
	 * @return the src
	 */
	public String getSrc()
	{
		return src;
	}

	/**
	 * @param src the src to set
	 */
	public void setSrc(final String src)
	{
		this.src = src;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(final String type)
	{
		this.type = type;
	}

	/**
	 * @return the rel
	 */
	public String getRel()
	{
		return rel;
	}

	/**
	 * @param rel the rel to set
	 */
	public void setRel(final String rel)
	{
		this.rel = rel;
	}

	/**
	 * @return the href
	 */
	public String getHref()
	{
		return href;
	}

	/**
	 * @param href the href to set
	 */
	public void setHref(final String href)
	{
		this.href = href;
	}

	/**
	 * @return the extraHtml
	 */
	public String getExtraHtml()
	{
		return extraHtml;
	}

	/**
	 * @param extraHtml the extraHtml to set
	 */
	public void setExtraHtml(final String extraHtml)
	{
		this.extraHtml = extraHtml;
	}

	/**
	 * @return the media
	 */
	public String getMedia()
	{
		return media;
	}

	/**
	 * @param media the media to set
	 */
	public void setMedia(final String media)
	{
		this.media = media;
	}

}
