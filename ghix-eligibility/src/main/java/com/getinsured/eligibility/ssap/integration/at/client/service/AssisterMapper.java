package com.getinsured.eligibility.ssap.integration.at.client.service;

import net.minidev.json.JSONObject;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAugmentationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AssisterType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.iex.util.ReferralUtil;

public class AssisterMapper {
	public AssisterType createAssister(JSONObject singleStreamlinedApplication){
	
		if(AccountTransferUtil.checkBoolean(singleStreamlinedApplication.get("getHelpIndicator")) 
				&& singleStreamlinedApplication.get("helpType") != null){
			JSONObject helper = null;
			String brokerOrAssister = singleStreamlinedApplication.get("helpType").toString();
			if("ASSISTER".equalsIgnoreCase(brokerOrAssister)){
				helper = (JSONObject)singleStreamlinedApplication.get("assister");
			}else if("BROKER".equalsIgnoreCase(brokerOrAssister)){
				helper = (JSONObject)singleStreamlinedApplication.get("broker");
			}else{
				return null;
			}
			
			return createHelper(helper, brokerOrAssister);
		}else{
			return null;
		}
	}	
	private AssisterType createHelper(JSONObject helper,String brokerOrAssister){
		AssisterType assiter = AccountTransferUtil.insuranceApplicationObjFactory.createAssisterType();
		PersonType assiterPerson = AccountTransferUtil.hixCoreFactory.createPersonType();
		assiter.setId("Assister");
		assiterPerson.getPersonName().add(createHelperName(helper));
		assiterPerson.setPersonAugmentation(addHelperId(helper,brokerOrAssister));
		assiter.setRolePlayedByPerson(assiterPerson);
		return assiter;
	}
	
	private PersonNameType createHelperName(JSONObject helper){
		
		String givenName = (String) helper.get("assisterFirstName");    
		String surName = (String) helper.get("assisterLastName");
		
		PersonNameType assiterPersonNameType =AccountTransferUtil.niemCoreFactory.createPersonNameType();
		if(ReferralUtil.isNotNullAndEmpty(givenName)){
			assiterPersonNameType.setPersonGivenName(AccountTransferUtil.createPersonNameTextType(givenName));
		}
		if(ReferralUtil.isNotNullAndEmpty(surName)){
			assiterPersonNameType.setPersonSurName(AccountTransferUtil.createPersonNameTextType(surName));
		}
		
		return assiterPersonNameType;
	}
	
	private PersonAugmentationType addHelperId(JSONObject helper,String brokerOrAssister){
		PersonAugmentationType assiterPersonAugmentationType = AccountTransferUtil.hixCoreFactory.createPersonAugmentationType();
		IdentificationType identificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String stringIdentificationID = AccountTransferUtil.basicFactory.createString();
		
		if("BROKER".equalsIgnoreCase(brokerOrAssister)){
			if(helper.get("brokerFederalTaxIdNumber")!= null  && StringUtils.isNotBlank(helper.get("brokerFederalTaxIdNumber").toString())){
				stringIdentificationID.setValue(helper.get("brokerFederalTaxIdNumber").toString());
				identificationType.setIdentificationID(stringIdentificationID);
				assiterPersonAugmentationType.getPersonIdentification().add(identificationType);
			}
		}else if("ASSISTER".equalsIgnoreCase(brokerOrAssister) && helper.get("assisterID") != null && StringUtils.isNotBlank(helper.get("assisterID").toString()) ){
				stringIdentificationID.setValue(helper.get("assisterID").toString());
				identificationType.setIdentificationID(stringIdentificationID);
				assiterPersonAugmentationType.getPersonIdentification().add(identificationType);
		}	
		return assiterPersonAugmentationType;
	}
	
}
