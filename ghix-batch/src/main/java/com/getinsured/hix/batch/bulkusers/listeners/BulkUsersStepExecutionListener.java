package com.getinsured.hix.batch.bulkusers.listeners;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.hix.batch.bulkusers.common.MergeCsvSplitFiles;

public class BulkUsersStepExecutionListener implements StepExecutionListener {

	private static final Logger logger = LoggerFactory.getLogger(BulkUsersStepExecutionListener.class);

	private MergeCsvSplitFiles mergeCsvSplitFiles;
	private String startTime = "";
	

	@Override
	public void beforeStep(StepExecution stepExecution) {
		ExitStatus exitStatus = stepExecution.getExitStatus();
		
		this.startTime = CommonUtil.dateToString();
		stepExecution.getExecutionContext().put("jobProcessId", CommonUtil.getJobProcessId(stepExecution.getJobExecution()));
		
		logger.info("Bulk Users Creation Job is started. --> [ Total No. Of All CSV Files Records Status --> [ "
				+ "\"Read Count\" : " + stepExecution.getReadCount() + ", \"Write Count\" : " + stepExecution.getWriteCount() 
				+ ", \"Start Time\" : " + this.startTime + ", \"End Time\" : " + "" 
				+ ", \"Exit Status\" : " + exitStatus.getExitCode() + ", \"ExitStatusDescription\" : " + exitStatus.getExitDescription() 
				+ " , \"jobProcessId\" : "+ CommonUtil.getJobProcessId(stepExecution.getJobExecution())
				+ ", Job Execution Id : "+ stepExecution.getJobExecution().getJobId()
				+ ", Job Instance Id : "+ stepExecution.getJobExecution().getId()
				+ ", Job Name : "+stepExecution.getJobExecution().getJobInstance().getJobName()
				+ " ]");
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		ExitStatus exitStatus = null;
		exitStatus = stepExecution.getExitStatus();
		String jobProcessId = CommonUtil.getJobProcessId(stepExecution.getJobExecution());
		try {
			stepExecution.getReadCount();
			mergeCsvSplitFiles.mergeFiles(jobProcessId);
		} catch (IOException e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			logger.error("afertStep() jobProcessId : "+jobProcessId+"   --> Exception Details : " + exceptionAsString);
		}
		
		logger.info("Bulk Users Creation Job is ended. --> [ Total No. Of All CSV Files Records Status --> [ "
				+ "\"Read Count\" : " + stepExecution.getReadCount() + ", \"Write Count\" : " + stepExecution.getWriteCount() 
				+ ", \"Start Time\" : " + this.startTime + ", \"End Time\" : " + CommonUtil.dateToString()
				+ ", \"Exit Status\" : " + exitStatus.getExitCode() + ", \"ExitStatusDescription\" : " + exitStatus.getExitDescription() 
				+ " , \"jobProcessId\" : "+ jobProcessId
				+ ", Job Execution Id : "+ stepExecution.getJobExecution().getJobId()
				+ ", Job Instance Id : "+ stepExecution.getJobExecution().getId()
				+ ", Job Name : "+stepExecution.getJobExecution().getJobInstance().getJobName()
				+ " ]");
		
		
		if(stepExecution.getFailureExceptions() != null && stepExecution.getFailureExceptions().size()>0) {
			return ExitStatus.FAILED;
		}
		return ExitStatus.COMPLETED;
	}

	public MergeCsvSplitFiles getMergeCsvSplitFiles() {
		return mergeCsvSplitFiles;
	}

	public void setMergeCsvSplitFiles(MergeCsvSplitFiles mergeCsvSplitFiles) {
		this.mergeCsvSplitFiles = mergeCsvSplitFiles;
	}

}
