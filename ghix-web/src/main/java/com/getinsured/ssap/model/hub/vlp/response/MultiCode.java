package com.getinsured.ssap.model.hub.vlp.response;

import java.util.Arrays;

/**
 * Muti-code, one of following:
 * <ul>
 *   <li>Y - Yes</li>
 *   <li>N - No</li>
 *   <li>P - Pending</li>
 *   <li>X - Not Applicable</li>
 * </ul>
 * @author Yevgen Golubenko
 * @since 2/27/19
 */
public enum MultiCode
{
  /**
   * Yes
   */
  Y,

  /**
   * No
   */
  N,

  /**
   * Pending
   */
  P,

  /**
   * Not Applicable.
   */
  X,

  /**
   * Unknown
   */
  UNKNOWN;

  public static MultiCode fromValue(String value) {
    return Arrays.stream(MultiCode.values()).filter(rc -> rc.name().equals(value)).findFirst().orElse(MultiCode.UNKNOWN);
  }
}
