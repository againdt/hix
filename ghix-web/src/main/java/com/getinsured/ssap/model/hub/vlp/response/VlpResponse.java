package com.getinsured.ssap.model.hub.vlp.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.getinsured.ssap.model.hub.vlp.VlpActionResult;

/**
 * Describes VLP response.
 * <p>
 *   VLP Responses come with weird JSON structure, so I use custom
 *   deserializer to produce more or less logical representation of that JSON
 *   to be used afterwards.
 * </p>
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = VlpResponseDeserializer.class)
public class VlpResponse
{
  private ValidationErrorCode validationErrorCode = ValidationErrorCode.NONE;
  private ResponseCode responseCode = ResponseCode.NONE;
  private List<IndividualDetails> individualDetails;

  @JsonProperty("RetriveStep2ResponseTypeResponseSet")
  private Object stepTwoResponseType;

  @JsonProperty("ResponseMetadata")
  private ResponseMetadata responseMetadata;

  @JsonIgnore
  private VlpActionResult actionResult;

  @JsonIgnore
  private String remoteUrl;

  /**
   * Returns validation error codes if any.
   * @return {@link ValidationErrorCode} object.
   */
  public ValidationErrorCode getValidationErrorCode()
  {
    return validationErrorCode;
  }

  public void setValidationErrorCode(final ValidationErrorCode validationErrorCode)
  {
    this.validationErrorCode = validationErrorCode;
  }

  /**
   * Returns {@link ResponseCode} object for this response.
   * @return {@link ResponseCode} object.
   */
  public ResponseCode getResponseCode()
  {
    return responseCode;
  }

  public void setResponseCode(final ResponseCode responseCode)
  {
    this.responseCode = responseCode;
  }

  /**
   * Returns list of {@link IndividualDetails} from this response.
   * @return list of {@link IndividualDetails}.
   */
  public List<IndividualDetails> getIndividualDetails()
  {
    return individualDetails;
  }

  public void setIndividualDetails(final List<IndividualDetails> individualDetails)
  {
    this.individualDetails = individualDetails;
  }

  public VlpActionResult getActionResult()
  {
    return actionResult;
  }

  public void setActionResult(final VlpActionResult actionResult)
  {
    this.actionResult = actionResult;
  }

  public String getRemoteUrl()
  {
    return remoteUrl;
  }

  public void setRemoteUrl(final String remoteUrl)
  {
    this.remoteUrl = remoteUrl;
  }

  public Object getStepTwoResponseType() {
    return stepTwoResponseType;
  }

  public void setStepTwoResponseType(Object stepTwoResponseType) {
    this.stepTwoResponseType = stepTwoResponseType;
  }

  public ResponseMetadata getResponseMetadata() {
    return responseMetadata;
  }

  public void setResponseMetadata(ResponseMetadata responseMetadata) {
    this.responseMetadata = responseMetadata;
  }

  @Override
  public String toString() {
    return "VlpResponse{" +
      "validationErrorCode=" + validationErrorCode +
      ", responseCode=" + responseCode +
      ", individualDetails=" + individualDetails +
      ", stepTwoResponseType=" + stepTwoResponseType +
      ", responseMetadata=" + responseMetadata +
      ", actionResult=" + actionResult +
      ", remoteUrl='" + remoteUrl + '\'' +
      '}';
  }
}
