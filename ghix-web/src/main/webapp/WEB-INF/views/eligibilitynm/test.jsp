<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Build your household</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ghixcustom.css">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ghix-blue-theme.css">
<link href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ms-custom.css">
<link type="text/css" rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/eligibilitynm/css/planView-v2.css" type="text/css">
<link rel="stylesheet" href="../resources/eligibilitynm/css/planViewStyles-v2.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/datepicker.css" />
<style type="text/css">
.row-fluid .span12 {
	width:99.9%;
}
.clear {
	clear:both;
	display:block;
}
.form-vertical hr {
	margin: 10px 0;
	clear: both;
	display: block;
}
h4 {
	font-size:18px;
	line-height:30px;
	padding:10px 0 0;
}
h5 {
	padding:10px 0 0;
}
input.no-margin {
	margin:0;
}
input.txt-center {
	text-align:center;
}
</style>
</head>

<body>

<div class="row-fluid" id="main">
    <div class="row-fluid" id="titlebar">
      <div class="span3">
        <ul class="nav nav-pills">
          <li> <a href="e1-1-start-app">Back </a> </li>
        </ul>
      </div>
      <div class="span9 pull-left">
        <h3>Start Your Application</h3>
      </div>
    </div>
    <!--titlebar-->
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li><a href="e1-1-start-app">Start Your Application <i class="icon-chevron-right"></i></a></li>
          <li><a href="e3-build-your-household">Build Your Household <i class="icon-chevron-right"></i></a></li>
          <li><a href="e4-household-income">Your Household Income <i class="icon-chevron-right"></i></a></li>
          <li><a href="e5-additional-questions">Additional Questions <i class="icon-chevron-right"></i></a></li>
          <li><a href="e6-review-declare-file">Review, Declare &amp; File <i class="icon-chevron-right"></i></a></li>
        </ul>
      </div>
      <!--sidebar-->
      <div class="span9 columns gutter10" id="rightpanel">
        <div class="gutter10">
          <div class="span4">
            <h4>Enter your information</h4>
            <p> Etiam in justo elit, et tristique nulla. Praesent molestie ultrices lacus, ultricies pretium purus porttitor eu. Nam imperdiet purus et nisl vehicula non tempor risus blandit. Donec ornare tempor lacus id tincidunt. Curabitur fringilla rutrum elit a auctor. Phasellus facilisis, dui a lobortis luctus, ipsum dolor consequat nulla, euismod laoreet tellus urna eu ligula. Donec at lorem diam, sed rutrum massa. Mauris sed turpis orci. Nullam non risus orci. Etiam et sapien nulla, a malesuada odio. Quisque vel ligula vitae mi vulputate suscipit id vel nisl. Aenean bibendum ultrices neque, at dapibus urna tristique vel. Duis at nibh et nisl vestibulum suscipit. Sed tincidunt dapibus ligula, sed ultrices dui auctor et. Integer lorem lectus, lobortis et lobortis sit amet, placerat sed quam. Pellentesque urna mauris, ultrices vitae placerat sed, gravida id nisl. </p>
          </div>
          <div class="span4">
            <h4>See your results</h4>
            <p> Etiam in justo elit, et tristique nulla. Praesent molestie ultrices lacus, ultricies pretium purus porttitor eu. Nam imperdiet purus et nisl vehicula non tempor risus blandit. Donec ornare tempor lacus id tincidunt. Curabitur fringilla rutrum elit a auctor. Phasellus facilisis, dui a lobortis luctus, ipsum dolor consequat nulla, euismod laoreet tellus urna eu ligula. Donec at lorem diam, sed rutrum massa. Mauris sed turpis orci. Nullam non risus orci. Etiam et sapien nulla, a malesuada odio. Quisque vel ligula vitae mi vulputate suscipit id vel nisl. Aenean bibendum ultrices neque, at dapibus urna tristique vel. Duis at nibh et nisl vestibulum suscipit. Sed tincidunt dapibus ligula, sed ultrices dui auctor et. Integer lorem lectus, lobortis et lobortis sit amet, placerat sed quam. Pellentesque urna mauris, ultrices vitae placerat sed, gravida id nisl. </p>
          </div>
          <div class="span4">
            <h4>Find health care plans</h4>
            <p> Etiam in justo elit, et tristique nulla. Praesent molestie ultrices lacus, ultricies pretium purus porttitor eu. Nam imperdiet purus et nisl vehicula non tempor risus blandit. Donec ornare tempor lacus id tincidunt. Curabitur fringilla rutrum elit a auctor. Phasellus facilisis, dui a lobortis luctus, ipsum dolor consequat nulla, euismod laoreet tellus urna eu ligula. Donec at lorem diam, sed rutrum massa. Mauris sed turpis orci. Nullam non risus orci. Etiam et sapien nulla, a malesuada odio. Quisque vel ligula vitae mi vulputate suscipit id vel nisl. Aenean bibendum ultrices neque, at dapibus urna tristique vel. Duis at nibh et nisl vestibulum suscipit. Sed tincidunt dapibus ligula, sed ultrices dui auctor et. Integer lorem lectus, lobortis et lobortis sit amet, placerat sed quam. Pellentesque urna mauris, ultrices vitae placerat sed, gravida id nisl. </p>
          </div>
          <div class="privacypolicy">
            <h3>Before you continue, please read our Privacy Policy</h3>
            <p>You are about to apply for health coverage and, if you choose, for help paying for your health coverage</p>
            <p>We may access personal information from various government sources, such as the Social Security Administration (SSA), Department of Homeland Security (DHS), and - if you choose to apply for help paying for your health coverage - the Internal Revenue Service (IRS).  We will obtain and use information available to us under federal law to verify the health programs available through the Exchange.</p>
            <p>We are committed to protecting your personal information. Please review our Privacy Policy for further information.</p>
            <form class="form-vertical">
              <div class="pull-left">
                <h3>Privacy Policy</h3>
              </div>
              <div class="pull-right"><a class="btn"><i class="icon-print"></i> Print</a> <a class="btn"><i class="icon-download-alt"></i> Download</a></div>
              <hr class="clear">
              <div class="control-group">
                <div class="controls">
                  <textarea rows="5" class="span12" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tortor nulla, cursus mattis imperdiet ut, elementum id eros. Sed tempus molestie sapien, eget bibendum metus mattis et. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla nec sollicitudin eros. Nunc euismod pellentesque sapien nec tempor. Aliquam mi orci, viverra ac scelerisque a, mattis eu quam. Etiam lacus arcu, bibendum non tincidunt sit amet, mollis vel leo. Duis sit amet erat augue, ut dictum neque.

Nunc in eros aliquam nisl viverra tincidunt. Mauris semper ipsum eros, et consectetur risus. Nunc ullamcorper vestibulum aliquam. Aliquam posuere sagittis neque, in varius ante dapibus et. Proin libero risus, cursus quis commodo et, egestas id nibh. Quisque lacus orci, suscipit vitae dignissim a, dapibus a metus. Nullam a ipsum id ante euismod semper. Nam adipiscing massa diam. Etiam pharetra augue in ante iaculis ac fermentum leo ultrices. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce elementum ligula ligula. Praesent eu faucibus sapien. Vestibulum mollis ornare felis, tristique semper neque commodo vitae. Sed hendrerit, quam et gravida tincidunt, erat arcu tincidunt est, quis posuere diam orci et augue. Nullam at congue sapien. Sed tortor eros, sollicitudin eu blandit quis, rutrum at nulla.

Duis dictum, lectus et ultrices rhoncus, sem urna ornare dui, nec pretium massa enim vitae lorem. Sed adipiscing, lacus sit amet semper blandit, tellus velit aliquet augue, consequat consectetur sem ante eu urna. Phasellus sapien nunc, aliquam vitae placerat et, adipiscing tristique orci. Suspendisse porttitor nisl dictum libero condimentum ornare. Integer sed semper lacus. Ut non ipsum ac sapien tristique volutpat. Quisque sagittis lacinia lacus id pellentesque. Ut et erat lectus. Duis velit leo, pulvinar in venenatis ac, volutpat in ante. Mauris eleifend orci eu nisi tincidunt suscipit. Etiam fermentum turpis vitae turpis egestas scelerisque. Etiam ac lacinia mauris.

Phasellus dolor massa, interdum eu vehicula in, mattis a lorem. Vestibulum molestie dolor sit amet neque ultrices facilisis. Morbi massa arcu, molestie vitae ullamcorper a, interdum at magna. Curabitur purus felis, egestas eu mattis at, mollis id elit. Duis fermentum dui non eros imperdiet et venenatis nisl vulputate. Donec volutpat est eu eros congue elementum. Nunc ultrices blandit ipsum id laoreet. Sed et sapien id purus sodales ultricies in sit amet tortor. Duis pharetra sollicitudin urna at pharetra. Vestibulum dignissim nisi ut dui fringilla sodales vel at nunc.

Nullam vel augue et justo mattis eleifend id in enim. Sed tincidunt molestie lectus, blandit mattis metus lacinia et. Sed id magna non augue mattis consectetur nec id est. Fusce mauris mauris, fermentum ac interdum interdum, faucibus at mi. Vestibulum adipiscing nibh nunc. Aliquam erat volutpat. Integer in orci dolor. Nunc suscipit neque id mi consectetur eu fermentum dolor elementum. Quisque eu quam tellus. Suspendisse auctor, velit eget convallis euismod, erat magna facilisis nulla, at pretium diam diam at ipsum. Nulla pretium mi ut ante adipiscing venenatis. Mauris magna diam, luctus vitae ullamcorper eu, vulputate id arcu. Donec justo diam, cursus quis placerat vel, porttitor eu sem. Maecenas vehicula, urna id rhoncus imperdiet, nisi lorem ullamcorper ipsum, sed ultrices quam diam vel justo. In ut ipsum sed ipsum bibendum placerat.
                                </textarea>
                </div>
              </div>
              <div class="control-group clearfix">
                <div class="controls">
                  <label class="checkbox">
                    <input type="checkbox" value="">
                    I have read the Privacy Policy and understand that my information will be shared only for the purpose of determining my eligibility. </label>
                </div>
                <a class="btn btn-primary pull-right" href="#">Accept &amp; Continue</a> </div>
            </form>
            <div class="primary-contact">
              <h3>Primary Contact</h3>
              <p>If you are seeking coverage for yourself or others in your household, please enter your contact information. If you are helping someone apply who is not a member of your household, please <a href="#">click here</a>. If you would like to designate an authorized representetive, please <a href="#">click here</a>.</p>
              <form class="form-vertical">
                <h4>Name</h4>
                <hr>
                <div class="span4">
                  <label class="control-label" for="inputfName">First Name</label>
                  <div class="controls">
                    <input type="text" id="inputfName" placeholder="Eli">
                  </div>
                </div>
                <div class="span4">
                  <label class="control-label" for="inputmName">Middle Name</label>
                  <div class="controls">
                    <input type="text" id="inputmName" placeholder="M">
                  </div>
                </div>
                <div class="span4">
                  <label class="control-label" for="inputlName">Last Name</label>
                  <div class="controls">
                    <input type="text" id="inputlName" placeholder="Manning">
                  </div>
                </div>
                <div class="span4 clearfix">
                  <label class="control-label" for="inputSuffix">Suffix</label>
                  <div class="controls">
                    <input type="text" id="inputSuffix" class="input-small" placeholder="Suffix">
                  </div>
                </div>
                <h4 class="clear">Residential Address</h4>
                <hr>
                <div class="span6">
                  <label class="control-label" for="inputStreet">Street address</label>
                  <div class="controls">
                    <input type="text" id="inputStreet" class="span12" placeholder="1437 San Pedro Vista Road">
                  </div>
                </div>
                <div class="span3">
                  <label class="control-label" for="inputApt">Apt.</label>
                  <div class="controls">
                    <input type="text" id="inputApt" class="span9" placeholder="Apartment 207">
                  </div>
                </div>
                <div class="span3">
                  <label class="control-label" for="inputCity">City</label>
                  <div class="controls">
                    <input type="text" id="inputCity"  class="span12" placeholder="San Francisco">
                  </div>
                </div>
                <div class="span3">
                  <label class="control-label" for="inputState">State</label>
                  <div class="controls">
                    <select name="inputState" class="span12" id="inputState">
                      <option value="" selected="selected">Select a State</option>
                      <option value="AL">Alabama</option>
                      <option value="AK">Alaska</option>
                      <option value="AZ">Arizona</option>
                      <option value="AR">Arkansas</option>
                      <option value="CA" selected>California</option>
                      <option value="CO">Colorado</option>
                      <option value="CT">Connecticut</option>
                      <option value="DE">Delaware</option>
                      <option value="DC">District Of Columbia</option>
                      <option value="FL">Florida</option>
                      <option value="GA">Georgia</option>
                      <option value="HI">Hawaii</option>
                      <option value="ID">Idaho</option>
                      <option value="IL">Illinois</option>
                      <option value="IN">Indiana</option>
                      <option value="IA">Iowa</option>
                      <option value="KS">Kansas</option>
                      <option value="KY">Kentucky</option>
                      <option value="LA">Louisiana</option>
                      <option value="ME">Maine</option>
                      <option value="MD">Maryland</option>
                      <option value="MA">Massachusetts</option>
                      <option value="MI">Michigan</option>
                      <option value="MN">Minnesota</option>
                      <option value="MS">Mississippi</option>
                      <option value="MO">Missouri</option>
                      <option value="MT">Montana</option>
                      <option value="NE">Nebraska</option>
                      <option value="NV">Nevada</option>
                      <option value="NH">New Hampshire</option>
                      <option value="NJ">New Jersey</option>
                      <option value="NM">New Mexico</option>
                      <option value="NY">New York</option>
                      <option value="NC">North Carolina</option>
                      <option value="ND">North Dakota</option>
                      <option value="OH">Ohio</option>
                      <option value="OK">Oklahoma</option>
                      <option value="OR">Oregon</option>
                      <option value="PA">Pennsylvania</option>
                      <option value="RI">Rhode Island</option>
                      <option value="SC">South Carolina</option>
                      <option value="SD">South Dakota</option>
                      <option value="TN">Tennessee</option>
                      <option value="TX">Texas</option>
                      <option value="UT">Utah</option>
                      <option value="VT">Vermont</option>
                      <option value="VA">Virginia</option>
                      <option value="WA">Washington</option>
                      <option value="WV">West Virginia</option>
                      <option value="WI">Wisconsin</option>
                      <option value="WY">Wyoming</option>
                    </select>
                  </div>
                </div>
                <div class="span3">
                  <label class="control-label" for="inputZip">Zip</label>
                  <div class="controls">
                    <input type="text" id="inputZip" class="span9" placeholder="Zip">
                  </div>
                </div>
                <div class="span9">
                  <div class="controls">
                    <label class="checkbox">
                      <input type="checkbox">
                      No fixed address </label>
                    <label class="checkbox">
                      <input type="checkbox">
                      Mailing address is different from residential address </label>
                  </div>
                </div>
                <h4 class="clear">Email</h4>
                <hr>
                <div class="span5">
                  <label class="control-label" for="inputEmail">Email</label>
                  <div class="controls">
                    <input type="text" id="inputEmail" placeholder="eli.manning@gmail.com">
                    <p><a href="e0-sign-in">Don't have an email address?</a></p>
                  </div>
                </div>
                <div class="span4">
                  <label class="control-label" for="inputrEmail">Re-enter email</label>
                  <div class="controls">
                    <input type="text" class="span12" id="inputrEmail" placeholder="eli.manning@gmail.com">
                    <p>&nbsp;</p>
                  </div>
                </div>
                <div class="span5">
                  <label class="control-label" for="inputHomephone">Home Phone</label>
                  <div class="controls">
                    <input type="text" class="span12" id="inputHomephone" placeholder="(415) 123-467">
                  </div>
                </div>
                <div class="span6">
                  <label class="control-label" for="inputCellphone">Cell Phone</label>
                  <div class="controls">
                    <input type="text" id="inputCellphone" placeholder="(415) 867-5309">
                  </div>
                </div>
                <h4 class="clear">Preferences</h4>
                <hr>
                <div class="span5">
                  <label class="control-label" for="contactpref">Preferred method of contact</label>
                  <div class="controls">
                    <select name="contactpref" class="span12" id="contactpref">
                      <option value="email" selected>Email</option>
                      <option value="phone">Phone</option>
                      <option value="Mail">Mail</option>
                    </select>
                  </div>
                </div>
                <div class="span9">
                  <label class="control-label" for="inputAlerts">Recieve alerts electronically?</label>
                  <div class="controls">
                    <label class="checkbox">
                      <input type="checkbox" id="inputAlerts">
                      via text messaging <strong>(415) 867-5309</strong> </label>
                    <label class="checkbox">
                      <input type="checkbox">
                      via email at email <strong>eli.manning@gmail.com</strong> </label>
                  </div>
                </div>
                <h5 class="clear">Prefered Language</h5>
                <hr>
                <div class="span5">
                  <label class="control-label" for="preferedspoken">Preferred spoken language</label>
                  <div class="controls">
                    <select name="preferedspoken" class="span12" id="preferedspoken">
                      <option value="English" selected>English</option>
                      <option value="Spanish">Spanish</option>
                      <option value="Cantonese">Cantonese</option>
                      <option value="French">French</option>
                    </select>
                  </div>
                </div>
                <div class="span5">
                  <label class="control-label" for="preferedwritten">Prefered written language</label>
                  <div class="controls">
                    <select name="preferedwritten" class="span12" id="preferedwritten">
                      <option value="English"selected>English</option>
                      <option value="Spanish">Spanish</option>
                      <option value="Cantonese">Cantonese</option>
                      <option value="French">French</option>
                    </select>
                  </div>
                </div>
                <h5 class="clear">Are you applying for health coverage for yourself?</h5>
                <hr>
                <label class="radio inline">
                  <input type="radio" name="Coverageforyourself" id="yourself" value="option1">
                  Yes </label>
                <label class="radio inline">
                  <input type="radio" name="Coverageforyourself" id="someoneelse" value="option2" checked>
                  No </label>
                <h5 class="clear">Would you like help paying for health coverage for anyone in your household?</h5>
                <label class="radio inline">
                  <input type="radio" name="coveragehelp" id="coveragehelp" value="Yes">
                  Yes </label>
                <label class="radio inline">
                  <input type="radio" name="coveragehelp" id="nocoveragehelp" value="No">
                  No </label>
                <h5 class="alert-success gutter10 qualify"> <a href="#qualify-coverage" role="modal-trigger" data-toggle="modal">See if you Qualify</a></h5>
                <button class="clear btn btn-primary pull-right">Save</button>
              </form>
              <!-- Modal -->
              <div id="qualify-coverage" class="hide modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                  <button type="button" title="" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 id="myModalLabel">Would you like help paying for your health coverage?</h3>
                </div>
                <div class="modal-body">
                  <!-- tabs-->

                  <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="home">
                      <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</p>
                    </div>
                    <div class="tab-pane fade" id="profile">
                      <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia yr, vero magna velit sapiente labore stumptown. Vegan fanny pack odio cillum wes anderson 8-bit, sustainable jean shorts beard ut DIY ethical culpa terry richardson biodiesel. Art party scenester stumptown, tumblr butcher vero sint qui sapiente accusamus tattooed echo park.</p>
                    </div>
                  </div>
                  <!-- .tabs-->
                </div>
                <div class="modal-footer">
                 <ul id="myTab">
                    <li class="active"><a href="#home" data-toggle="tab">Home</a></li>
                    <li><a href="#profile" data-toggle="tab">Profile</a></li>
                  </ul> <button class="btn" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary">Save changes</button>
                </div>
              </div>
              <!-- #modal -->
            </div>
          </div>
        </div>
        <!--gutter10-->
      </div>
      <!--rightpanel-->
    </div>
    <!--row-fluid-->
</div>
<!--main-->


<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script>
<script src="../resources/eligibilitynm/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="../resources/eligibilitynm/js/bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-tab.js"></script>
<script>
$(document).ready(function(){
$('.qualify').hide();
$('#sidebar').affix();
$("input[name='coveragehelp']").change(function(){
	$('.qualify').fadeToggle(this.value == "Yes");
});
$('#myTab a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});
});
</script>
</body>
</html>

</body>
</html>
