package com.getinsured.hix.referral.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.referral.exception.ReferralServiceException;
import com.getinsured.hix.referral.service.ReferralActivationService;
import com.getinsured.hix.referral.util.WebReferralConstants;
import com.getinsured.hix.referral.util.WebReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Controller
@RequestMapping("/referral")
public class ReferralCsrController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralCsrController.class);
	private static final String REDIRECT_ACCOUNT_USER_LOGINFAILED = "redirect:/account/user/loginfailed";

	@Autowired
	private UserService userService;

	@Autowired
	@Qualifier("referralActivationService")
	private ReferralActivationService referralActivationService;

	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	private boolean isAllowed(Model model, HttpServletRequest request) {
		AccountUser user = null;
		HashMap<String, Object> errorMsg = new HashMap<String, Object>();

		try {
			user = userService.getLoggedInUser();
			if (user == null) {
				errorMsg.put("message", "User not authorized.");
				model.addAttribute("authfailed", errorMsg.get("message"));
				request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
				return false;
			}
		} catch (InvalidUserException ex) {
			LOGGER.error("User not logged in");
			errorMsg.put("message", "User not logged in.");
			model.addAttribute("authfailed", errorMsg.get("message"));
			request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
			return false;
		}
		return true;
	}

	@GiAudit(transactionName = "CSR Linking", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/csrflow/{ssapApplication}/{cmrHousehold}", "/csrflow/{ssapApplication}/{cmrHousehold}/" }, method = RequestMethod.GET)
	public String csrFlow(@PathVariable("ssapApplication") String ssapApplication, @PathVariable("cmrHousehold") String cmrHousehold, Model model, HttpServletRequest request) throws InvalidUserException {
		try {
			final int cmrHouseholdId = toDecrypt(cmrHousehold);
			final int ssapApplicationId = toDecrypt(ssapApplication);
			

			LOGGER.debug("CSR Referral Flow Starts for Ssap Id - " + ssapApplicationId + " and Cmr Id " + cmrHouseholdId);
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			final HttpSession httpSession = request.getSession();

			httpSession.setAttribute(WebReferralConstants.REFERRAL_FROM_CSR, "Y");

			final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationForSsap(ssapApplicationId);
			GiAuditParameterUtil.add(new GiAuditParameter("referralActivation", referralActivation),new GiAuditParameter("referralCmrToLink", cmrHouseholdId),new GiAuditParameter("referralSsapToLink", ssapApplicationId));
			
			if (referralActivation == null) {
				LOGGER.error("No data found for Referral for Ssap Id - " + ssapApplicationId + " and Cmr Id " + cmrHouseholdId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "No data found for Referral for Ssap Id - " + ssapApplicationId + " and Cmr Id " + cmrHouseholdId));
				return "redirect:/indportal";
			}

			if (referralActivation.isActivationComplete() || referralActivation.isCancelled()) {
				LOGGER.error("Data not proper. Workflow Status not valid");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Data not proper. Workflow Status not valid"));
				return "redirect:/indportal";
			}

			if (!referralActivation.canStartSecurity() && referralActivation.getCmrHouseholdId() != cmrHouseholdId) {
				LOGGER.error("Cmr household data not proper for Id - " + referralActivation.getId());
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Cmr household data not proper for Id - " + referralActivation.getId()));
				return "redirect:/indportal";
			}
			
			final int userId = ((AccountUser) request.getSession().getAttribute("sUser")).getId();

			final String reDirectUrl = navigateTo(referralActivation, cmrHouseholdId, userId);

			LOGGER.debug("CSR Referral Flow Ends");

			return reDirectUrl;
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ csrFlow  " + e.getMessage());
		    throw new GIRuntimeException(ReferralServiceException.ReferralErrors.REFERRAL_CSR_LINKING_EXCEPTION.getId(), e, null, Severity.HIGH);
		}
	}

	private String navigateTo(ReferralActivation referralActivation, int cmrHouseholdId, int user) {
		final String encryptedRefId = toEncrypt(referralActivation.getId());
		if (referralActivation.canStartSecurity()) {
			final int referralActivationStatus = referralActivationService.bindUserCmrToReferralActivation(referralActivation.getSsapApplicationId(), cmrHouseholdId, user);
			if (referralActivationStatus == WebReferralConstants.FAILURE) {
				LOGGER.error("Referral Binding Failed ");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Referral Binding Failed"));
				return "redirect:/indportal";
			}
			else if (referralActivationStatus == WebReferralConstants.USER_LINKED_TO_DIFFERENT_EXTERNAL_ID) {
				LOGGER.warn("Referral USER LINKED TO DIFFERENT EXTERNAL ID ");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Referral USER LINKED TO DIFFERENT EXTERNAL ID "));
				return "referral/linkuser/medicaididissue";
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			return "redirect:/referral/security/verify/" + encryptedRefId;
		} else {
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			return WebReferralUtil.navigateTo(referralActivation, encryptedRefId);
		}
	}

	private String toEncrypt(final long idToEncrypt) {
		return ghixJasyptEncrytorUtil.encryptStringByJasypt("" + idToEncrypt);
	}

	private int toDecrypt(String strToDecrypt) {
		return Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(strToDecrypt));
	}

}
