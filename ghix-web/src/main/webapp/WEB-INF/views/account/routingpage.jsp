<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="form-wrapper">
<div class="row-fluid">
<div class="rightpanel" ng-app="communicationPreferencesApp" >
	<%@ include file="../iex/indportal/mypreferences.jsp" %>
	<div ng-include="'mypreferences'"></div>
</div>
    </div>
    </div>
    </div>

<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>

<script src="<c:url value="/resources/angular/mask.js" />"></script> 
<script src="<c:url value="/resources/js/moment.min.js" />"></script> 
<script src='<c:url value="/resources/js/preferences/communicationPreferences.app.js" />'></script>
<script src='<c:url value="/resources/js/preferences/communicationPreferences.controller.js" />'></script>
<script src='<c:url value="/resources/js/preferences/communicationPreferences.directive.js" />'></script>
<script src='<c:url value="/resources/js/preferences/communicationPreferences.service.js" />'></script>

<script src='<c:url value="/resources/js/addressValidation.module.js" />'></script>

<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js"/>"></script>
