<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isErrorPage="true" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" 
	  xmlns:th="http://www.thymeleaf.org">
<head>
</head>
<body>
<div>
	<br/>
	<%-- <h3>Application Error</h3>
	<div >
		<span >
			Cause:  ${exception.getMessage()}
		</span>
		<BR/>
		<BR/>
		<span>
			Click here for detailed Error Stack Trace:
			<details>
				<c:forEach items="${exception.stackTrace}" var="element">
		    				<c:out value="${element}" />
				</c:forEach>
			</details>
		</span>
			<p>
				<b>Please contact the support team.</b>
			</p>
		<BR/> --%>
		
		<h3>Application Error</h3>
		<table width="100%" style="border: 1px solid red;">
			<tr valign="top">
				<td width="10%">Error:</td>
				<td><c:out value="${pageContext.exception}" /></td>
			</tr>
			<tr valign="top">
				<td>URI:</td>
				<td><c:out value="${pageContext.errorData.requestURI}" /></td>
			</tr>
			<tr valign="top">
				<td>Status code:</td>
				<td>${pageContext.errorData.statusCode}</td>
			</tr>
			<tr valign="top">
				<td>Stack trace:</td>

				<td><div>
						<details>
							<c:forEach var="trace"
								items="${pageContext.exception.stackTrace}">
								<p>${trace}</p>
							</c:forEach>
						</details>
					</div></td>
			</tr>
		</table>
		<p>
			<b>Please contact the support team.</b>
		</p>
</div>
</body>
</html>