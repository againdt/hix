package com.getinsured.ahbx.platform.ecm.webclient;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.getinsured.ahbx.platform.address.Request;
import com.getinsured.ahbx.platform.address.Response;
import com.getinsured.ahbx.platform.ecm.CheckinFile;
import com.getinsured.ahbx.platform.ecm.CheckinFileResponse;
import com.getinsured.ahbx.platform.ecm.Content;
import com.getinsured.ahbx.platform.ecm.Datasource;
import com.getinsured.ahbx.platform.ecm.DocumentStream;
import com.getinsured.ahbx.platform.ecm.GetFile;
import com.getinsured.ahbx.platform.ecm.GetFileResponse;
import com.getinsured.ahbx.platform.ecm.Image;
import com.getinsured.ahbx.platform.ecm.KeyValue;
import com.getinsured.ahbx.platform.ecm.MapWrapper;
import com.getinsured.ahbx.platform.notices.GetStaticNotice;
import com.getinsured.ahbx.platform.notices.GetStaticNoticeResponse;





public class TestAHBXECM {
	@Test
	public void testCreateContentStringStringByteArray() {

		try {
			
			//Content con = new Content();
			//con.setContentId("1");
			
			CheckinFileResponse checkin = new CheckinFileResponse();
			checkin.setReturn("return");
			
			Datasource ds = new Datasource();
			ds.setDescription("desc");
			
			GetFileResponse getresponse = new GetFileResponse();
			
			KeyValue key = new KeyValue();
			
			CheckinFile checkFile = new CheckinFile();
			checkFile.setDataAsXML("Val");
			
			MapWrapper mapwrap = new MapWrapper();
			
			GetFile getFile = new GetFile();
			getFile.setDocId("1");
			
			Image img = new Image();
			img.setBitDepth(1);
			
			Request req = new Request();
			//req.setId("1");
			
			Response res = new Response();
			res.setId("1");
			
			GetStaticNotice getStatNot= new GetStaticNotice();
			getStatNot.setDataAsXML("q");
			
			GetStaticNoticeResponse getResp = new GetStaticNoticeResponse();
			getResp.setReturn("23");
			
			DocumentStream docStream = new DocumentStream();
			docStream.setEncoding("enc");
			
			//System.out.println(con.toString());
			System.out.println(checkin.toString());
			System.out.println(ds.toString());
			System.out.println(getresponse.toString());
			System.out.println(key.toString());
			System.out.println(mapwrap.toString());
			System.out.println(checkFile.toString());
			System.out.println(getFile.toString());
			System.out.println(img.toString());
			System.out.println(req.toString());
			System.out.println(res.toString());
			System.out.println(getStatNot.toString());
			System.out.println(getResp.toString());
			System.out.println(docStream.toString());
			
			//assertNotNull(con.toString());
			assertNotNull(checkin.toString());
			assertNotNull(ds.toString());
			assertNotNull(getresponse.toString());
			assertNotNull(key.toString());
			assertNotNull(mapwrap.toString());
			assertNotNull(checkFile.toString());
			assertNotNull(getFile.toString());
			assertNotNull(img.toString());
			assertNotNull(req.toString());
			assertNotNull(res.toString());
			assertNotNull(getStatNot.toString());
			assertNotNull(getResp.toString());
			assertNotNull(docStream.toString());


		} catch (Exception e) {
			fail(e.getMessage());
		}

	}

}
