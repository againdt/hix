import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';

// Localization for English and Spanish
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

addLocaleData([...en, ...es]);

/**
 *
 * @param {Object} props
 * @returns {React.Component}
 *
 * Here we inject in pre-built component <IntlProvider/> messages
 * (they go from Redux Store on successful download from server)
 * and make <IntlProvider/> to update every time when we switch languages
 */
const ConnectedIntlProvider = props => {
  const { children, language, messages } = props;

  const currentMessages = messages && messages[language];

  return (
    <IntlProvider locale={language} messages={currentMessages}>
      {children}
    </IntlProvider>
  );
};

const mapStateToProps = state => ({
  key: state.language, // Changing of language causes changing of the Key. It invokes update for <ConnectedIntlProvider/>
  language: 'en', // Hardcoding to en until we know how we wish to handle language changes
  messages: state.messages
});

export default connect(mapStateToProps)(ConnectedIntlProvider);
