<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />

	
<script type="text/javascript">
$(document).ready(function() {	
	$("#assisterInformation").removeClass("link");
	$("#assisterInformation").addClass("active");
});
	function highlightThis(val){
		
		var currUrl = window.location.href;
		var newUrl="";
		/* Check if Query String parameter is set or not */
		if(currUrl.indexOf("?", 0) > 0) { /* If Yes */
			if(currUrl.indexOf("?lang=", 0) > 0) {/* Check if locale is already set without querystring param  */
				 newUrl = "?lang="+val;
			} else if(currUrl.indexOf("&lang=", 0) > 0) { /*  Check if locale is already set with querystring param  */
				newUrl = currUrl.substring(0, currUrl.length-2)+val; 
			} else { 
				newUrl = currUrl + "&lang="+val;
			}
		} else { /* If No  */
			newUrl = currUrl + "?lang="+val;
		}
		window.location = newUrl;
	}
</script>
	
	
		<div class="row-fluid">
				<div class="gutter10-lr">
				<jsp:include page="/WEB-INF/views/entity/activationLinkResult.jsp" />
					<div class="l-page-breadcrumb hide">
						<!--start page-breadcrumb -->
						<div class="row-fluid">
							<ul class="page-breadcrumb">
							 <c:set var="encAssisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
							 <input type="hidden" id="status" name="status" value="<c:url value="/entity/entityadmin/assisterstatus?assisterId=${encAssisterId}"/>">
	  						 <input type="hidden" id="certificationStatus" name="certificationStatus" value="<c:url value="/entity/assisteradmin/certificationstatus?assisterId=${encAssisterId}"/>">
							  <c:set var="encEntityId" ><encryptor:enc value="${assister.entity.id}" isurl="true"/> </c:set>
								<li><a href="#">&lt; <spring:message code="label.assister.back"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><spring:message code="label.assister.assisters"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><spring:message code="label.assister.manage"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/viewassisterinformation?assisterId=${encAssisterId}&entityId=${encEntityId}" />">${assister.firstName} ${assister.lastName}</a></li>
							</ul>
							<!--page-breadcrumb ends-->
						</div><!-- end of .row-fluid -->
					</div><!--l-page-breadcrumb ends-->
					
					
					<div class="row-fluid">
						      <h1><class name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
					</div>
					<div class="row-fluid">
						<c:choose>
							<c:when test="${showCECLeftnavigation== 'true'}">
							<div id="sidebar" class="span3">
								<ul class="nav nav-list">
								
													<li class="active"><a href="#"><spring:message code="label.assister.certifiedCounselor"/></a></li>
													<li><a href="/hix/entity/entityadmin/assisterprofile?assisterId=${encAssisterId}"><spring:message code="label.assister.profile"/></a></li>
													<c:choose>	
														<c:when test="${loggedUser=='entityAdmin' }">
															<li><a href="/hix/entity/assisteradmin/certificationstatus?assisterId=${encAssisterId}&entityId=${encEntityId}">Certification Status</a></li>
														</c:when>
														</c:choose>
													<li><a href="/hix/entity/entityadmin/assisterstatus?assisterId=${encAssisterId}&entityId=${encEntityId}"><spring:message code="label.assister.status"/></a></li>
												
												</ul>
									</div><!-- end of span3 -->	
							</c:when>
							
							<c:otherwise>
								<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
							</c:otherwise>
		
						</c:choose>
		
		<div class="span9">
								<div class="header">
									<h4 class="pull-left"><spring:message code="label.assister.certifiedCounselor"/></h4>
									<!-- Fix for #HIX-16228: replaced assisterMenu to assisterMenuLogin -->
									<c:choose>
										<c:when test="${CA_STATE_CODE}">
											<c:if test="${loggedUser =='entityAdmin'}"> 
												<a class="btn btn-small pull-right" type="button" href="<c:url value="/entity/assister/editassisterinformation?assisterId=${encAssisterId}&entityId=${encEntityId}" />"><spring:message code="label.assister.edit"/></a>
										   </c:if> 
										</c:when>
										<c:otherwise>
											<c:if test="${loggedUser != 'assisterLogin' || loggedUser =='entityAdmin' || assister.certificationStatus == 'Certified'}"> 
												<a class="btn btn-small pull-right" type="button" href="<c:url value="/entity/assister/editassisterinformation?assisterId=${encAssisterId}&entityId=${encEntityId}" />"><spring:message code="label.assister.edit"/></a>
										   </c:if> 
										</c:otherwise>
									</c:choose>
									
								</div>
								<form class="form-horizontal" id="frmviewCertification" name="frmviewCertification" action="viewcertificationinformation" method="POST">
								<df:csrfToken/>
								<div class="gutter10">
									<table class="table table-border-none">
										<tbody>
											<tr>
												<td class="span4 txt-right" scope="row"><spring:message code="label.assister.Firstname"/></td>
												<td><strong>${assister.firstName}</strong></td>
											</tr>
											<tr>
												<td class="span4 txt-right" scope="row"><spring:message code="label.assister.Lastname"/></td>
												<td><strong>${assister.lastName}</strong></td>
											</tr>
												<tr>
												<td class="span4 txt-right"><spring:message code="label.assister.emailAddress"/></td>
												<td><strong>${assister.emailAddress}</strong></td>
											</tr>
											
											<tr>
												<td class="span4 txt-right" scope="row"><spring:message code="label.assister.phoneNumber"/></td>
												<td>
													<strong> 
														<c:choose>
															<c:when test="${assister.primaryPhoneNumber!=null}">
																<c:set var="primaryPhoneNumber" value="${assister.primaryPhoneNumber}"></c:set>
																<c:set var="formattedPrimaryPhoneNumber" value="(${fn:substring(primaryPhoneNumber,0,3)})${fn:substring(primaryPhoneNumber,3,6)}-${fn:substring(primaryPhoneNumber,6,10)} "></c:set>
																${formattedPrimaryPhoneNumber}
															</c:when>
															<c:otherwise>
																${assister.primaryPhoneNumber}
															</c:otherwise>											
														</c:choose>																				
													</strong>
												</td>
											</tr>
										
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.secondaryPhoneNumber" /></td>
									<td>
									<strong> 
										<c:choose>
											<c:when test="${assister.secondaryPhoneNumber!=null && not empty assister.secondaryPhoneNumber}">
												<c:set var="secondaryPhoneNumber" value="${assister.secondaryPhoneNumber}"></c:set>
												<c:set var="formattedSecondaryPhoneNumber" value="(${fn:substring(secondaryPhoneNumber,0,3)})${fn:substring(secondaryPhoneNumber,3,6)}-${fn:substring(secondaryPhoneNumber,6,10)} "></c:set>
												${formattedSecondaryPhoneNumber}
											</c:when>
											<c:otherwise>
												${assister.secondaryPhoneNumber}
											</c:otherwise>
										</c:choose>
									</strong>
									</td>
								</tr>
	
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.preferredMethodofCommunication" /></td>
									<td><strong>${assister.communicationPreference}</strong></td>
								</tr>
								
								<c:choose>
						 						<c:when test="${ showPostalMailOption == 'true'  }">
						 								<tr>
															<td class="span4 txt-right" scope="row"><spring:message code="label.assister.receiveNotices" /> </td> 
															<td>
																<strong>
																	<c:if test="${assister.postalMail!=null && assister.postalMail == 'Y' }" > <spring:message code="label.assister.Yes" /></c:if>
																	<c:if test="${assister.postalMail == null || assister.postalMail == 'N' ||  assister.postalMail == 'n'}" > <spring:message code="label.assister.No" /> </c:if>
																</strong>
															</td>
														</tr>
						 						</c:when>
						 		</c:choose>
								
								
								
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.isCertifiedAssister"/></td>
									<c:if test="${assister.certificationNumber==null || assister.certificationNumber == ''}">
										<td><strong><spring:message code="label.assister.No" /></strong></td>
									</c:if>
									<c:if test="${assister.certificationNumber!=null && assister.certificationNumber != ''}">
										<td><strong><spring:message code="label.assister.Yes" /></strong></td>
									</c:if>
								</tr>
								
								<c:if test="${assister.certificationNumber!=null && assister.certificationNumber != ''}">
									<tr>
										<td class="span4 txt-right" scope="row"><spring:message code="label.assister.assisterCertification"/> #</td>
										<td><strong><fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/></strong></td>
									</tr>
								</c:if>								
								<c:if test="${assister.primaryAssisterSite!=null}">
									<tr>
										<td class="span4 txt-right" scope="row"><spring:message code="label.assister.primarySite" /></td>
										<td><strong>${assister.primaryAssisterSite.siteLocationName}</strong></td>
									</tr>
								</c:if>	
								<c:if test="${assister.secondaryAssisterSite!=null}">
									<tr>
										<td class="span4 txt-right" scope="row"><spring:message code="label.assister.secondarySite" /></td>
										<td><strong>${assister.secondaryAssisterSite.siteLocationName}</strong></td>
									</tr>
								</c:if>
										</tbody>
									</table>
								</div>
		
					<div class="header">
						<h4><spring:message code="label.assister.mailingAddress"/></h4>
					</div>
						<table class="table table-border-none">
							<tbody>
								<tr>
									<td class="span4 txt-right" scope="row"><spring:message code="label.assister.streetAddress1"/></td>
									<td><strong>${assister.mailingLocation.address1}</strong></td>
								</tr>
								
									<tr>
										<td class="span4 txt-right" scope="row"><spring:message code="label.assister.suite" /></td>
										<td><strong>${assister.mailingLocation.address2}</strong></td>
									</tr>
								
								<tr>
									<td class="span4 txt-right"><spring:message code="label.assister.city"/></td>
									<td><strong>${assister.mailingLocation.city}</strong></td>
								</tr>
								
								<tr>
									<td class="span4 txt-right"><spring:message code="label.assister.state"/></td>
									<td><strong>${assister.mailingLocation.state}</strong></td>
								</tr>
	
								<tr>
									<td class="span4 txt-right"><spring:message code="label.assister.zipCode"/></td>
									<td><strong>${assister.mailingLocation.zip}</strong></td>
								</tr>
							</tbody>
						</table>
						<div class="gutter10">
				<div class="header">
					<h4><spring:message code="label.assister.profileInformation"/></h4>
				</div>
					
					<table class="table table-border-none">
						<tr>
							<td class="span4 txt-right" scope="row"><spring:message code="label.assister.spokenLanguagesSupport"/></td>
							<td class="comma"><strong>${assisterLanguages.spokenLanguages}</strong></td>
						</tr>
						<tr>
							<td class="span4 txt-right" scope="row"><spring:message code="label.assister.writtenLanguagesSupport"/></td>
							<td class="comma"><strong>${assisterLanguages.writtenLanguages}</strong></td>
						</tr>
						<tr>
							<td class="span4 txt-right" scope="row"><spring:message code="label.assister.education"/></td>
							<td class="comma"><strong>${assister.education}</strong></td>
						</tr>					
						<tr>
						 <c:set var="encAssisterIdUrl" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
							<td class="span4 txt-right"><spring:message code="label.assister.photo" /></td>
							<td><spring:url value="/entity/assisteradmin/photo/${encAssisterIdUrl}" var="photoUrl" /><img src="${photoUrl}" alt='<spring:message code="label.assister.profileImgOfAssister" />'
										class="profilephoto thumbnail" />
									</td>
						</tr>
					</table>
				</div>
					</form>
				</div>
			</div>
		</div>

</div>
		<script type="text/javascript">
$(function(){$("#getAssistance").click(function(e){e.preventDefault();var href=$(this).attr('href');if(href.indexOf('#')!=0){$('<div id="searchBox" class="modal bigModal" data-backdrop="static"><div class="searchModal-header gutter10-lr"><button type="button" onclick="window.location.reload()" class="close" data-dismiss="modal" aria-hidden="true">�<\/button><\/div><div class=""><iframe id="search" src="'+href+'" class="searchModal-body"><\/iframe><\/div><\/div>').modal();}});});function closeSearchLightBox(){$("#searchBox").remove();window.location.reload();}
		function closeSearchLightBoxOnCancel(){$("#searchBox").remove();}
		
		$(document).ready(function() {
			$("#assisters").removeClass("link");
			$("#assisters").addClass("active");

		});
		</script>
		<script type="text/javascript"> 
		/* Comma separated value in language text (HIX - 33824) */ 
		$(".comma strong").text(function(i, val) {
		    return val.replace(/,/g, ", ");
		});
		
if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,194,new Date().getTime(),"","","","",""]);
		</script>
