package com.getinsured.hix.util;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;

public class DelegateRespTest extends BaseTest
{
	@Autowired DelegationResponseUtils delegateResponseUtils;
	private static final Logger LOGGER = Logger.getLogger(DelegateRespTest.class);
	
	
	@Test
	public void testValidateIssuerResponse()
	{
		try
		{
			LOGGER.info("======================== Test Started ==========================");
			/*Map<String, Object> criteria = new HashMap<String, Object>();
			criteria.put("userRoleType", "CARRIER");
			//Default 1 is sent for testing
			criteria.put("individualCaseId", 1);*/
			//LOGGER.info("Criteria: "+criteria);
			DelegationRequest delegationRequest = new DelegationRequest();
			delegationRequest.setRecordId(1);
			delegationRequest.setRecordType("CARRIER");
			DelegationResponse issuerValidationResponse = delegateResponseUtils.validateIssuerResponse(delegationRequest);
			
			LOGGER.info("Validation data: "+issuerValidationResponse.getStatus()+" "+issuerValidationResponse.getResponseCode());
			assertTrue(issuerValidationResponse != null);
			
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
