/**
 * 
 */
package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.ssap.renewal.service.ToConsiderService;
import com.getinsured.hix.batch.ssap.renewal.util.ToConsiderPartitionerParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * ToConsiderWriter class is used to execute ToConsider Logic For Renewal .
 * 
 * @since July 19, 2020
 */
public class ToConsiderWriter implements ItemWriter<SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToConsiderWriter.class);
	private long jobExecutionId = -1;

	private JobService jobService;
	private ToConsiderService toConsiderService;
	private ToConsiderPartitionerParams toConsiderPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		jobExecutionId = stepExecution.getJobExecution().getId();
	}

	@Override
	public void write(List<? extends SsapApplication> ssapApplicationListToWriter) throws Exception {

		String batchJobStatus=null;
		if (jobService != null && jobExecutionId != -1) {
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}

		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {
			throw new UnexpectedJobExecutionException(EnrollmentConstants.BATCH_STOP_MSG);
		}

		if (CollectionUtils.isNotEmpty(ssapApplicationListToWriter)) 
		{
			Long renewalYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
			
			for(SsapApplication ssapApplication : ssapApplicationListToWriter)
			{
				try 
				{
					toConsiderService.executeToConsiderLogicForRenewal(ssapApplication, renewalYear, toConsiderPartitionerParams.getServerName());
				} 
				catch (Exception e) {
					String errorMessage = "Error in executing ToConsider in writer."+ "\n" + ExceptionUtils.getFullStackTrace(e);
					LOGGER.error(errorMessage);
					toConsiderService.logToGIMonitor(errorMessage,"RENEWALBATCH_50007");
				}
			}
			
			LOGGER.info("Successfully executed ToConsider For Renewal year: {} and applicationIds: {} ",renewalYear,toConsiderPartitionerParams.getSsapApplicationIdList());
		}
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public ToConsiderService getToConsiderService() {
		return toConsiderService;
	}

	public void setToConsiderService(ToConsiderService toConsiderService) {
		this.toConsiderService = toConsiderService;
	}

	public ToConsiderPartitionerParams getToConsiderPartitionerParams() {
		return toConsiderPartitionerParams;
	}

	public void setToConsiderPartitionerParams(
			ToConsiderPartitionerParams toConsiderPartitionerParams) {
		this.toConsiderPartitionerParams = toConsiderPartitionerParams;
	}

}
