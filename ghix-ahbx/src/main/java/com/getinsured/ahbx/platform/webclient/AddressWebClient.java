package com.getinsured.ahbx.platform.webclient;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.ahbx.platform.address.Request;
import com.getinsured.ahbx.platform.address.Response;
import com.getinsured.ahbx.platform.mapper.AddressMapper;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformResponse;
import com.getinsured.hix.platform.webclient.SoapWebClient;
import com.getinsured.ahbx.util.AHBXUtils;
import com.getinsured.ahbx.util.GhixAhbxConstants;

/**
 * 
 * Class to invoke AHBX SOAP Service.
 * 
 * @author Ekram Ali Kazi
 *
 */
@Component
public class AddressWebClient {

	private static final Logger LOGGER = Logger.getLogger(AddressWebClient.class);

	@Value("#{configProp['verifyAddressWSDL']}")
	private String wsdlUrl;

	@Autowired private WebServiceTemplate webServiceTemplate;
	@Autowired private SoapWebClient<Response> soapWebClient;
	

	public String sendRequest(PlatformRequest platformRequest){

		/**
		 * Initiate Platform Response Object, to be returned to the caller
		 */
		PlatformResponse platformResponse = new PlatformResponse();
		platformResponse.startResponse();
		platformResponse.setAppID(wsdlUrl);

		if (platformRequest.getAddress() == null){
			// this should never happen... handled boundary condition...
			platformResponse.setModuleStatusCode("P-A105");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg("Incoming address is blank");
			platformResponse.endResponse();

			return AHBXUtils.marshal(platformResponse);
		}

		/**
		 * Populate AHBX Request Object
		 */
		Request ahbxRequest = formAHBXRequest(platformRequest.getAddress());

		//LOGGER.info("AHBX OEDQ Address request - " + ahbxRequest);
		Response ahbxResponse = null;
		try{
			webServiceTemplate.setDefaultUri(wsdlUrl);
			//ahbxResponse = (Response) webServiceTemplate.marshalSendAndReceive(ahbxRequest);
			ahbxResponse = soapWebClient.send(ahbxRequest, wsdlUrl, "SHARED-SERVICES-OEDQ", webServiceTemplate);
		}catch(WebServiceIOException wsIOE){
			LOGGER.error(wsIOE);
			platformResponse.setModuleStatusCode("P-A101");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(wsIOE.getMessage());
		}catch(SoapFaultClientException sfce){
			LOGGER.error(sfce);
			LOGGER.error("Fault Code : " + sfce.getFaultCode());
			LOGGER.error("Fault Reason : " + sfce.getFaultStringOrReason());
			LOGGER.error("Fault Role : " + sfce.getSoapFault().getFaultActorOrRole());
			platformResponse.setModuleStatusCode("P-A102");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(sfce.getMessage());
		}catch(Exception e) {
			LOGGER.error(e);
			platformResponse.setModuleStatusCode("P-A103");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(e.getMessage());
		}

		//LOGGER.info("AHBX OEDQ Address response - " + ahbxResponse);

		/**
		 * If AHBX Response found, populate Platform Response Object, in this case validAddressList
		 */
		if (ahbxResponse != null){
			List<Location> validAddressList = processAHBXResponse(ahbxResponse);
			platformResponse.setModuleStatusCode("P-A000");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
			platformResponse.setValidAddressList(validAddressList);
		}

		platformResponse.endResponse();
		/**
		 * Marshal and send response back to the caller
		 */
		return AHBXUtils.marshal(platformResponse);
	}

	private List<Location> processAHBXResponse(Response ahbxResponse) {
		return AddressMapper.map(ahbxResponse);
	}

	private Request formAHBXRequest(Location incomingAddress) {
		return AddressMapper.map(incomingAddress);
	}

}