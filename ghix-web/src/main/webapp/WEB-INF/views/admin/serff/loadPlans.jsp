
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<%--<link rel="stylesheet"  type="text/css" href="../../resources/css/bootstrap.css"/>--%>
<%--<link rel="stylesheet"  type="text/css" href="../../resources/css/bootstrap.css"/>--%>
<link rel="stylesheet"  type="text/css" href="../../resources/css/ghixcustom.css"/>

<script src='<c:url value="/resources/js/planMgmt/loadPlans.js" />'></script>

<script type="text/javascript">
	$(document).ready(function() {
		var scope = angular.element(document.getElementById("rightpanel")).scope();

		if (scope) {
			scope.$apply(function () {
				// Set respective exchange State Code. 
				scope.exchangeState = "${exchangeState}";
				// Set suffix value of all Templates.
				scope.NETWORK_SUFFIX = "${NETWORK_SUFFIX}";
				scope.BENEFITS_SUFFIX = "${BENEFITS_SUFFIX}";
				scope.RATE_SUFFIX = "${RATE_SUFFIX}";
				scope.SERVICE_AREA_SUFFIX = "${SERVICE_AREA_SUFFIX}";
				scope.DRUG_SUFFIX = "${DRUG_SUFFIX}";
				scope.BUSINESS_RULE_SUFFIX = "${BUSINESS_RULE_SUFFIX}";
				scope.UNIFIED_RATE_SUFFIX = "${UNIFIED_RATE_SUFFIX}";
			});
		}
	});
</script>

<style>
	ul.unstyled li{
		padding:10px;
	}
</style>

<div class="row-fluid" id="main">
	<div class="gutter10">
		<div class="row-fluid">
			<h1 id="">Load Health and Dental Plans</h1>
		</div><!-- end of .row-fluid -->

		<div class="row-fluid">
			<div id="sidebar" class="span3">
				<div class="header"><h4 class="margin0">Plan Load Process</h4></div>
				<ul class="unstyled">
					<li><b>Step 1: </b>Select the Issuer that you want to load plans for.</li>
					<li><b>Step 2: </b>Choose the required files for the Issuer.</li>

					<c:choose>
						<c:when test="${exchangeState == 'WA' || exchangeState == 'CT'}">
							<li><b>Step 3: </b>Load Prescription Template Only.</li>
							<li><b>Step 4: </b>Click on <b>Save</b> to proceed or <b>Cancel</b> to abort the process.</li>
						</c:when>
						<c:otherwise>
							<li><b>Step 3: </b>Click on <b>Save</b> to proceed or <b>Cancel</b> to abort the process.</li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div><!-- end of .span3 -->

			<div class="span9" id="rightpanel" ng-app="myApp" ng-controller="validateCtrl">
				<div class="header" style="height:40px"></div>
				<form name="frmUploadPlans" ng-model="frmUploadPlans" action="${pageContext.request.contextPath}/admin/serff/uploadPlans" enctype="multipart/form-data" method="POST">
					<df:csrfToken />
					<div class="control-group">
						<c:if test="${uploadStatus != null}">
							<div style="color: red">${uploadStatus}<br /><br /></div>
						</c:if>
						<c:if test="${uploadSucess != null}">
							<div style="color: green">${uploadSucess}<br /><br /></div>
						</c:if>

						<label for="carrier" class="control-label"><b>Step 1:</b> Select Issuer&nbsp;<span style="color:red">*</span>&nbsp;&nbsp;</label>
						<select name="carrier" ng-model="carrier" onchange="$('#carrierText').val(this.options[this.selectedIndex].text);" required carrier-validation>
							<option value="" selected="selected">Select carrier</option>
							<c:if test="${issuerList != null}">
								<c:forEach var="issuer" items="${issuerList}">
									<c:if test="${issuer.hiosIssuerId != null}">
										<option value="${issuer.hiosIssuerId}">[${issuer.hiosIssuerId}] ${issuer.shortName}</option>
									</c:if>
								</c:forEach>
							</c:if>
						</select>
						<span style="color:red" ng-show="frmUploadPlans.carrier.$dirty && frmUploadPlans.carrier.$invalid">
							<span ng-show="frmUploadPlans.carrier.$error.required">Issuer not selected.</span>
							<span ng-show="frmUploadPlans.carrier.$error.carrierValidation">Invalid Issuer HIOS ID selected.</span>
						</span>
						<input type="hidden" id="carrierText" name="carrierText" />
					</div>

					<div class="control-group">
						<label class="control-label" for="fileUploadPlans"><b>Step 2:</b> Select XML Templates&nbsp;<span style="color:red">*</span></label>
						<div class="controls">
							<c:choose>
								<c:when test="${exchangeState == 'MN'}">
									<input type="file" class="filestyle" multiple="multiple" id="fileUploadPlans" ng-model="fileUploadPlans" name="fileUploadPlans" data-rule-required="true" accept=".xml,.pdf" data-classButton="btn" files-validation />
								</c:when>
								<c:otherwise>
									<input type="file" class="filestyle" multiple="multiple" id="fileUploadPlans" ng-model="fileUploadPlans" name="fileUploadPlans" data-rule-required="true" accept=".xml" data-classButton="btn" files-validation />
								</c:otherwise>
							</c:choose>
							<br /><span style="color:red" ng-show="frmUploadPlans.fileUploadPlans.$dirty && frmUploadPlans.fileUploadPlans.$invalid">
								<span id="fileValid" name="fileValid" ng-show="frmUploadPlans.fileUploadPlans.$error.filesValidation"></span>
							</span>
						</div>
					</div>

					<c:if test="${exchangeState == 'WA' || exchangeState == 'CT'}">
					<div class="control-group">
						<div id = "pnD">
							<label class="pull-left" for="pndCheckbox"><b>Step 3:</b> Load Prescription Template Only </label>
							&nbsp; &nbsp;
							<input type="checkbox" ng-model="pndCheckbox.selected" name="pndCheckbox" ng-change="resetSelectedFiles()" class="pull-left" />
						</div>
					</div>
					</c:if>

					<div class="control-group">
						<div class="span2">
							<input class="btn offset2" type="reset" ng-click="resetForm()" value="Cancel" />
						</div>
						<div class="span2">
							<input class="btn btn-primary offset1" type="submit" ng-model="saveBtn" name="saveBtn" value="Save" ng-disabled="invalidCarrier || invalidFileList" />
						</div>
						<c:if test="${exchangeState == 'WA' || exchangeState == 'CT'}">
						<div class="span2">
							<a href="${pageContext.request.contextPath}/admin/serff/executeBatch?sourceScreen=loadPlans">
								<input class="btn btn-primary offset1" type="button" id="executeBatch" name="executeBatch" value="Upload" <c:if test="${hasWaitingPlansToLoad == false}">disabled="disabled"</c:if> />
							</a>
						</div>
						</c:if>
					</div>
				</form>
			</div><!-- end of .span9 -->
		</div><!-- end of .row-fluid -->
	</div> <!-- end of .gutter10 -->
</div><!--end of row-fluid Main-->

