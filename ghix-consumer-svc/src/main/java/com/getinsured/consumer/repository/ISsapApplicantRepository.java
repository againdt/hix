package com.getinsured.consumer.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.ssap.model.SsapApplicant;

@Repository
public interface ISsapApplicantRepository extends JpaRepository<SsapApplicant, Long>{

	@Query("SELECT appl FROM SsapApplicant appl WHERE appl.ssapApplication.id = :ssapAppId AND personId=1 ")
	SsapApplicant findByPrimaryApplicant(@Param("ssapAppId") long ssapAppId);

	@Query("select apl.applicantGuid from SsapApplicant apl, SsapApplication sap "+
                "where sap.id = apl.ssapApplication.id and apl.personId=1 and sap.cmrHouseoldId=:householdId order by apl.lastUpdateTimestamp desc")
	List<String> findApplicantAccessCodes(@Param("householdId") BigDecimal householdId);

	
}
