package com.getinsured.iex.hub.apc.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.apc.cms.dsh.aptc.extension._1.APTCResponsePayloadType;
import com.getinsured.iex.hub.apc.cms.dsh.aptc.extension._1.ErrorMessageType;

public class APTCResponsePayloadTypeWrapper implements JSONAware{

    private IRSResponseTypeWrapper irsResponseWrapper;
    private List<ErrorMessageTypeWrapper> errorMessageWrapperDetail;
    
    public APTCResponsePayloadTypeWrapper(APTCResponsePayloadType aPTCResponsePayloadType)
    {
    	if(aPTCResponsePayloadType != null)
    	{
	    	irsResponseWrapper = new IRSResponseTypeWrapper(aPTCResponsePayloadType.getIRSResponse());
	    	
	    	List<ErrorMessageType> errorMessageDetail = aPTCResponsePayloadType.getErrorMessageDetail();
	    	int size = errorMessageDetail.size();
	    	
	    	if(size > 0)
	    	{
	    		errorMessageWrapperDetail = new ArrayList<ErrorMessageTypeWrapper>();
	    	}
	    	
	    	for(int i = 0; i < size; i++)
	    	{
	    		ErrorMessageTypeWrapper errorMessageTypeWrapper = new ErrorMessageTypeWrapper(errorMessageDetail.get(i));
	    		errorMessageWrapperDetail.add(errorMessageTypeWrapper);
	    	}
    	}
    }
    public String getResponseCodes()
    {
    	String errorResponseCodes = "";
    	int size = errorMessageWrapperDetail.size();
    	for(int i = 0; i < size; i++)
    	{
    		errorResponseCodes += errorMessageWrapperDetail.get(i).getResponseCode();
    		
    		if(i < (size-1))
    		{
    			errorResponseCodes += ", ";
    		}
    	}
    	return errorResponseCodes;
    }
    
    @SuppressWarnings("unchecked")
	@Override
	public java.lang.String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("IRSResponse", this.irsResponseWrapper);
		
		JSONArray errorMessageResponse = null;
		
		if(this.errorMessageWrapperDetail != null && !this.errorMessageWrapperDetail.isEmpty()){
			errorMessageResponse = new JSONArray();
			for(ErrorMessageTypeWrapper data : this.errorMessageWrapperDetail){
				errorMessageResponse.add(data);
			}
		}
		obj.put("ErrorMessageDetail", errorMessageResponse);
		return obj.toJSONString();
	}
}
