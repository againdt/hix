package com.getinsured.hix.agency.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.agency.repository.IAgencySiteRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.agency.AgencySite;
import com.getinsured.hix.model.agency.AgencySiteHour;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Service("agencySiteService")
public class AgencySiteServiceImpl implements AgencySiteService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AgencySiteServiceImpl.class);
	
	@Autowired
	private IAgencySiteRepository iAgencySiteRepository;
	@Autowired
	private UserService userService;
	
	
	
	@Transactional(rollbackOn=Exception.class)
	@Override
	public AgencySite saveOrUpdate(AgencySite agencySite) {
		try {
			if (agencySite.getAgencySiteHours() != null) {
				for (AgencySiteHour agencySiteHour : agencySite.getAgencySiteHours()) {
				agencySiteHour.setAgencySite(agencySite);
			}
		}

			AccountUser user = userService.getLoggedInUser();
			if (user != null) {
				if (agencySite.getId() == 0) {
					agencySite.setCreatedBy(Long.valueOf(user.getId()));
				} else {
					agencySite.setLastUpdatedBy(Long.valueOf(user.getId()));
				}
			}
			
			if(0 != agencySite.getId()){
				AgencySite tempFetchedSite =	iAgencySiteRepository.findById(agencySite.getId());	
				agencySite.setCreatedBy(tempFetchedSite.getCreatedBy());
				agencySite.setCreationTimestamp(tempFetchedSite.getCreationTimestamp());
				if(null != agencySite.getLocation()){
					agencySite.getLocation().setId(tempFetchedSite.getLocation().getId());	
				}
			}
			
		return iAgencySiteRepository.save(agencySite);
		} catch (Exception e) {
			LOGGER.error("Error while upating site information for agency :" + agencySite.getId(), e);
			throw new GIRuntimeException("Error while upating site information for agency+" + agencySite.getId(), e);
		}
		
	}
	
	@Override
	public AgencySite findSiteById(Long siteId){
		return iAgencySiteRepository.findById(siteId);
	}
	
	
	@Override
	public List<AgencySite> getSiteByAgencyId(Long agencyId) {
		return iAgencySiteRepository.findByAgencyId(agencyId);
	}
	
	
}
