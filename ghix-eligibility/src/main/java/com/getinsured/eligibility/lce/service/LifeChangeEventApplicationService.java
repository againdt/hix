package com.getinsured.eligibility.lce.service;

import java.math.BigDecimal;
import java.util.List;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface LifeChangeEventApplicationService {

	Boolean isValidLCEApplication(SsapApplication ssapApplication);
	
	SsapApplication getEnrolledSsapApplicationByMaxIdAndCmrHouseoldId(BigDecimal cmrHouseoldId,long coverageYear);
	
	SsapApplication saveSsapSepApplication(SsapApplication ssapApplication, boolean updateJson) throws Exception;

	SsapApplication cloneSsapApplicationFromParent(String ssapJson, SsapApplication currentApplication, SsapApplication parentApplication, SepRequestParamDTO sepRequestParamDTO, String applicationType, String applicationSource, String applicationStatus, Long renewalYear);
	
	void closeSsapApplication(SsapApplication ssapApplication);

	List<Long> getEnrolledSsapApplicationsByCoverageYear(long coverageYear);
	
	SsapApplication findByMaxIdCoverageYearAndCmrHouseoldId(BigDecimal cmrHouseoldId, long coverageYear, String isFinancial);

	List<SsapApplication> getRenewedSsapApplicationsByCoverageYear(Long renewalYear,Long batchSize);
	
	
}
