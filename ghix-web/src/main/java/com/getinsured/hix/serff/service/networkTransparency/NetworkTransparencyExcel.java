package com.getinsured.hix.serff.service.networkTransparency;

/**
 *-----------------------------------------------------------------------------
 * HIX-84043 Load and persist Network transparency rating.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to persist data in Network Transparency table.
 * @author Bhavin Parmar
 * @since July 25, 2016
 */
public class NetworkTransparencyExcel {

	private static final int NUMBER_TYPE = 1;
	private static final int STRING_TYPE = 2;

	public enum NetworkTransparencyExcelRowEnum {
		PLAN_YEAR("Plan Year", "PLAN_YEAR", 1, NUMBER_TYPE),
		ISSUER_NAME("Issuer Name", "ISSUER_NAME", 1, STRING_TYPE),
		HIOS_ISSUER_ID("HIOS Issuer ID", "HIOS_ISSUER_ID", 1, STRING_TYPE);

		private String rowName;
		private String rowConstName;
		private int rowIndex;
		private int dataType;

		private NetworkTransparencyExcelRowEnum(String name, String columnConstName, int index, int dataType) {
			this.rowName = name;
			this.rowConstName = columnConstName;
			this.rowIndex = index;
			this.dataType = dataType;
		}

		public String getRowName() {
			return rowName;
		}

		public String getRowConstName() {
			return rowConstName;
		}

		public int getRowIndex() {
			return rowIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum NetworkTransparencyExcelColumnEnum {
		NETWORK_ID("Network ID", "NETWORK_ID", 0, STRING_TYPE),
		PLAN_HIOS_ID("Plan HIOS ID", "PLAN_HIOS_ID", 1, STRING_TYPE),
		COUNTY("County", "COUNTY", 2, STRING_TYPE),
		RATING("Rating", "RATING", 3, STRING_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private NetworkTransparencyExcelColumnEnum(String name, String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}
}
