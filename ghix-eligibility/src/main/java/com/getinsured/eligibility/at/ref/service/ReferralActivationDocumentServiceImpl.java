package com.getinsured.eligibility.at.ref.service;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.exception.ReferralException;
import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.repository.IReferralActivationDocumentRepository;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.referraltask.ReferralTasks;

/**
 * @author chopra_s
 * 
 */
@Component("referralActivationDocumentService")
@Scope("singleton")
public class ReferralActivationDocumentServiceImpl implements ReferralActivationDocumentService {

	private static final String COMMENTS = "comments";
	private static final String ACCEPTED = "accepted";
	private static final String CMR_DOCUMENT_ID = "cmrDocumentId";
	private static final Logger LOGGER = Logger.getLogger(ReferralActivationDocumentServiceImpl.class);
	private static final String SUCCESS_RESPONSE = "{\"responseCode\": \"HS000000\"}";
	private static final String REFERRAL_ACTIVATION = "REFERRAL_ACTIVATION";
	private static final String DOCUMENT_APPROVED = "Y";

	@Autowired
	@Qualifier("ireferralActivationDocumentRepository")
	private IReferralActivationDocumentRepository ireferralActivationDocumentRepository;

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;

	@Autowired
	@Qualifier("referralDocumentTriggerMailService")
	private ReferralDocumentTriggerMailService referralTriggerMailService;

	@Autowired
	@Qualifier("referralTasks")
	private ReferralTasks referralTasks;
	
	@Override
	public String updateDocumentStatus(final String jsonInput) throws ReferralException {

		try {
			final JSONObject requestJson = parseJson(jsonInput);
			final ConsumerDocument consumerDocument = updateConsumerDocument(requestJson);
			processReferalActivation(consumerDocument);
		} catch (ReferralException e) {
			throw e;
		}

		return SUCCESS_RESPONSE;

	}

	private ConsumerDocument updateConsumerDocument(JSONObject requestJson) {
		// Save the document to the database
		final Long consumerDocumentId = Long.valueOf(requestJson.get(CMR_DOCUMENT_ID).toString());
		ConsumerDocument consumerDocument = ireferralActivationDocumentRepository.findOne(consumerDocumentId);
		consumerDocument.setAccepted(requestJson.get(ACCEPTED).toString());
		consumerDocument.setComments(requestJson.get(COMMENTS).toString());
		consumerDocument = ireferralActivationDocumentRepository.save(consumerDocument);
		return consumerDocument;

	}

	private void processReferalActivation(final ConsumerDocument consumerDocument) {
		if (ReferralUtil.isValidString(consumerDocument.getTargetName()) && consumerDocument.getTargetName().equals(REFERRAL_ACTIVATION)) {
			final ReferralActivation referralActivation = referralActivationRepository.findOne(consumerDocument.getTargetId());
			if (referralActivation.isCsrVerificationPending()) {
				if (ReferralUtil.isValidString(consumerDocument.getAccepted()) && consumerDocument.getAccepted().equals(DOCUMENT_APPROVED)) {
					LOGGER.info("Setting Workflow Status for Referral Activation id " + consumerDocument.getTargetId() + " as " + ReferralActivationEnum.RIDP_PENDING);
					referralActivation.setLastUpdatedOn(new TSDate());
					referralActivation.setWorkflowStatus(ReferralActivationEnum.RIDP_PENDING);
					referralActivationRepository.save(referralActivation);
					referralTasks.processRidpAfterDocumentApproval(consumerDocument.getTargetId());
				} else {
					LOGGER.info("Setting Workflow Status for Referral Activation id " + consumerDocument.getTargetId() + " as " + ReferralActivationEnum.SECURITY_FAILED);
					referralActivation.setLastUpdatedOn(new TSDate());
					referralActivation.setWorkflowStatus(ReferralActivationEnum.SECURITY_FAILED);
					referralActivationRepository.save(referralActivation);
				}
				//commented as per Hix-56115
				//referralTriggerMailService.triggerDocumentStatusMail(consumerDocument, referralActivation);
			} else {
				LOGGER.warn("Setting Workflow Status for Referral Activation id " + consumerDocument.getTargetId() + " failed as Status not proper");
			}

		} else {
			StringBuilder debugMessage = new StringBuilder();
			debugMessage.append("Not marking ReferralActivation status as " + ReferralActivationEnum.RIDP_PENDING + " as received document with accepted flag: ");
			debugMessage.append(consumerDocument.getAccepted());
			debugMessage.append(" for ");
			debugMessage.append(consumerDocument.getTargetName());
			debugMessage.append(" with id: ");
			debugMessage.append(consumerDocument.getTargetId());
			LOGGER.debug(debugMessage);
		}

	}

	private JSONObject parseJson(String jsonInput) throws ReferralException {
		// Extract the required fields from Json
		JSONObject requestJson = null;
		String errorMessage = null;
		JSONParser parser = null;
		try {
			parser = new JSONParser();
			requestJson = (JSONObject) parser.parse(jsonInput);
		} catch (ParseException parseException) {
			errorMessage = "Error parsing input json";
			LOGGER.error(errorMessage, parseException);
			throw new ReferralException(errorMessage);
		}

		return requestJson;
	}

	@Override
	public Integer sendNotificationToUploadDoc(long referralId) {

		if (referralId > 0) {
			final ReferralActivation referralActivation = referralActivationRepository.findOne(referralId);
			final Integer status = referralTriggerMailService.sendNotificationToUploadDoc(referralActivation);
			return status;
		}
		return null;

	}

}
