package com.getinsured.hix.account.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

public class UserChangePasswordView extends TagSupport {
	
	private static final long serialVersionUID = 1L;
	private String redirectURL;
	private String submitRedirectURL;
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	private ApplicationContext applicationContext;
	
	
	private JspWriter out;
	//onblur='javascript:checkUserPassword(this);'
	private String template ="<form class='form-horizontal' id='pwdchangeform' name='pwdchangeform' action='appContextPath/account/user/passwordchange' method='POST' autocomplete='off'>"
			+"<input type='hidden' id='submitRedirectURL1' name='submitRedirectURL' value='submitRedirectURLValue' disabled='true'>"
			+"<input type='hidden' id='email' name='email' value=''>"
			+"<df:csrfToken/>"
			+"<div class='modal-body'>"
			+"<div class='form-group'>"
			+"<label for='currentpswd' class='required col-lg-2 control-label'>Current Password <img src='appContextPath/resources/images/requiredAsterix.png' width='' height='' alt='Required!' /></label>"
			+"<div class='span7'>"
			+"<input type='password' class='form-control span10' id='currentpswd' name='currentpswd' onkeypress='resetErrorMsg(event, this)' value=''/>"
			+"<div id='currentpswd_error' class=''></div>"
			+"</div>"
			+"</div>"
			+"<div class='form-group'>"
			+"<label for='newpassword' class='required col-lg-2 control-label'>New Password <img src='appContextPath/resources/images/requiredAsterix.png' width='' height='' alt='Required!' /></label>"
			+"<div class='span7'>"
			+"<input type='password' class='form-control span10' id='newpassword' name='newpassword' size='30' onkeypress='resetErrorMsg(event, this)'/>"
			+"<div id='newpassword_error' class=''></div>"
			+"</div>"
			+"</div>"
			+"<div class='form-group'>"
			+"<label for='confirmpassword' class='required col-lg-2 control-label'>Confirm New Password <img src='appContextPath/resources/images/requiredAsterix.png' width='' height='' alt='Required!' /></label>"
			+"<div class='span7'>"
			+"<input type='password' class='form-control span10' id='confirmpassword' name='confirmpassword' size='30' onkeypress='resetErrorMsg(event, this)'/>"
			+"<div id='confirmpassword_error' class=''></div>"
			+"</div>"
			+"</div>"
			+"</div>"
			+"<div class='modal-footer'>"
			+"        <button class='btn pull-left 'id='cancelbutton1' name='cancelbutton' onClick=\"return goToPage(appContextPath/redirectURL);\">Cancel</button>    "
			+"		  <input type='button'  name='changePwdSave' id='changePwdSave' aria-hidden='true' onClick='javascript:checkUserPassword();' value='Save'	class='btn btn-primary pull-right' style='margin:0px;' />"
			+"</div>"
			+"</form>";
	
	
	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	@Override
	public int doStartTag() throws JspException {
		this.setProperties();
		try {
				String temp = template;
				String contextPath = ((HttpServletRequest)pageContext.getRequest()).getContextPath();
				
				if(null!=submitRedirectURL){
					applicationContext = RequestContextUtils.getWebApplicationContext(
							pageContext.getRequest(),
							pageContext.getServletContext()
							);
					ghixJasyptEncrytorUtil  = (GhixJasyptEncrytorUtil)applicationContext.getBean("ghixJasyptEncrytorUtil");
					String url =ghixJasyptEncrytorUtil.encryptStringByJasypt(submitRedirectURL.trim());
					temp = temp.replace("submitRedirectURLValue", url);
				}
				else{
					temp=temp.replace("<input type='hidden' id='submitRedirectURL1' name='submitRedirectURL' value='submitRedirectURLValue' disabled='true'>", "");
				}
				String token = (String) ((HttpServletRequest) pageContext.getRequest()).getSession().getAttribute("csrftoken");
				temp = temp.replace("<df:csrfToken/>", "<input type='hidden' id='csrftoken' name='csrftoken' value='"+token+"'>");
				temp = temp.replace("appContextPath/redirectURL", "'"+contextPath+redirectURL.trim()+"'");
				temp = temp.replace("appContextPath", contextPath);
	            this.out.println(temp);
	        } catch (Exception ex) {
	            throw new JspException("Error in UserChangePasswordView tag", ex);
	        }
	    return SKIP_BODY;
	}
	
	private void setProperties(){
    	this.out     = pageContext.getOut();
    	
    	
	}

	public String getSubmitRedirectURL() {
		return submitRedirectURL;
	}

	public void setSubmitRedirectURL(String submitRedirectURL) {
		this.submitRedirectURL = submitRedirectURL;
	}
	
}
