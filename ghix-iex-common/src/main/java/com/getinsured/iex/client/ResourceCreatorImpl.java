package com.getinsured.iex.client;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.eligibility.model.SepEvents.Financial;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.iex.dto.*;
import com.getinsured.iex.util.SsapUtil;

/**
 * @author karnam_s
 */
@Component
public class ResourceCreatorImpl implements ResourceCreator{
	private static final String IEX_LCE_REST_SAVE_SSAP_APPLICATION_EVENT = "saveSsapApplicationEvent";
	private static final String IEX_LCE_REST_SAVE_SSAP_APPLICANT_EVENT = "saveSsapApplicantEvent";
	private static final String IEX_LCE_REST_FIND_SSAP_APPLICANT_EVENTS = "findSsapApplicantEvents";
	
	@Autowired private RestTemplate restTemplate;
	private static final Logger LOGGER = Logger.getLogger(ResourceCreatorImpl.class);
	
	@Override
	public SsapApplicationResource getSsapApplicationById(long id) {
		String url =   GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + "{id}";
		
		Map<String, Long> uriParams = new HashMap<>();
		uriParams.put("id", id);
		ResponseEntity<Resource<SsapApplicationResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, null, 
					new ParameterizedTypeReference<Resource<SsapApplicationResource>>() {}, 
					uriParams);
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			Resource<SsapApplicationResource> resource =responseEntity.getBody();
			return  resource.getContent();
		}
		else {
			//  TODO: handle this later (???)
			return null;
		}
	}

	@Override
	public String getSsapApplicationApplicantById(long id) {
		String url =   GhixEndPoints.SsapEndPoints.SSAP_APPLICANTS_DATA_URL + "{id}";
		
		Map<String, Long> uriParams = new HashMap<>();
		uriParams.put("id", id);
		ResponseEntity<Resource<SsapApplicantResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, null, 
					new ParameterizedTypeReference<Resource<SsapApplicantResource>>() {}, 
					uriParams);

		String ssapApplicantLink = "";
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			ssapApplicantLink = responseEntity.getHeaders().getLocation().getPath();
		}
		return  ssapApplicantLink;
	}
	@Override
	public String saveSsapApplication(SsapApplication ssapApplication) throws JsonGenerationException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL;
		long ssapApplicationID = 0;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplication.class); 
		
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();
		ssapApplication.setSsapApplicants(null);
		
		String jsonString = writer.writeValueAsString(ssapApplication);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Collections.singletonList(new MediaType("application","json")));

		String ssapApplicationLink = "";
		ResponseEntity<Resource<SsapApplicationResource>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jsonString, requestHeaders), new ParameterizedTypeReference<Resource<SsapApplicationResource>>() {},String.class);
		if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
			ssapApplicationLink = responseEntity.getHeaders().getLocation().getPath();
		}
		return  ssapApplicationLink;
	}
	
	@Override
	public String updateSsapApplication(SsapApplication ssapApplication, long ssapID) throws JsonGenerationException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL+"{id}";
		ssapApplication.setSsapApplicants(null);
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplication.class);
		String jsonString = writer.writeValueAsString(ssapApplication);
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);
		Map<String, Object> uriParams = new HashMap<>();
		uriParams.put("id", ssapID);
		 
		ResponseEntity<Resource<SsapApplicationResource>> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,new ParameterizedTypeReference<Resource<SsapApplicationResource>>() {},uriParams);
		// TODO: why?!!?
		return responseEntity.getHeaders().getLocation().toString(); 
	}
	
	/**
	 * Get all the applicants for a given application.
	 *
	 * @see com.getinsured.iex.client.ResourceCreator#getApplicants(long)
	 */
	@Override
	public Map<String,SsapApplicantResource> getApplicantsWithGuidAsKey(long applicationId) throws Exception {
		Map <String,SsapApplicantResource> applicants = null;
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_APPLICANTS_DATA_URL;
			
			Map<String, Long> uriParams = new HashMap<>();
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.ALL);
			requestHeaders.setAccept(mediaTypeList);
			HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
			uriParams.put("id", applicationId);
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
			
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				applicants = new HashMap<>();
				
				List lst = (List)jobj.get("ssapApplicants");

				for (Object o : lst)
				{
					Map map = (Map) o;
					JSONObject jmap = new JSONObject(map);
					String jsonString = jmap.toJSONString();
					ObjectMapper mapper = new ObjectMapper();
					mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
					SsapApplicantResource ssapApplicantResource = mapper.readValue(jsonString, SsapApplicantResource.class);
					if (!("N".equalsIgnoreCase(ssapApplicantResource.getOnApplication())))
					{
						applicants.put(ssapApplicantResource.getApplicantGuid(), ssapApplicantResource);
					}
				}
			}
			
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while getting the applicants for applicationId::" + applicationId, e);
			throw new Exception(e);
		}
		return applicants;
	}
	
	@Override
	public Map<Long,SsapApplicantResource> getApplicants(long applicationId) throws Exception {
		Map <Long,SsapApplicantResource> applicants = null;
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_APPLICANTS_DATA_URL;
			
			Map<String, Long> uriParams = new HashMap<>();
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.ALL);
			requestHeaders.setAccept(mediaTypeList);
			HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
			uriParams.put("id", applicationId);
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
			
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				applicants = new HashMap<>();
				
				List lst = (List)jobj.get("ssapApplicants");

				for (Object o : lst)
				{
					Map map = (Map) o;
					JSONObject jmap = new JSONObject(map);
					String jsonString = jmap.toJSONString();
					ObjectMapper mapper = new ObjectMapper();
					mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
					SsapApplicantResource ssapApplicantResource = mapper.readValue(jsonString, SsapApplicantResource.class);
					if (!("N".equalsIgnoreCase(ssapApplicantResource.getOnApplication())))
					{
						applicants.put(ssapApplicantResource.getPersonId(), ssapApplicantResource);
					}
				}
			}
			
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while getting the applicants for applicationId::" + applicationId, e);
			throw new Exception(e);
		}
		return applicants;
	}
	
	
	@Override
	public Map<Long,SsapApplicantResource> getApplicantsByApplicationId(long applicationId) throws Exception {
		Map <Long,SsapApplicantResource> applicants = null;
		long applicantId = 0;
		try {
			String baseUrl = GhixEndPoints.GHIX_SSAP_SVC_URL;
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_APPLICANTS_DATA_URL;
			Map<String, Long> uriParams = new HashMap<>();
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.ALL);
			requestHeaders.setAccept(mediaTypeList);
			HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
			uriParams.put("id", applicationId);
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
			
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				applicants = new HashMap<>();
				
				List lst = (List)jobj.get("ssapApplicants");

				for (Object o : lst)
				{
					Map map = (Map) o;
					JSONObject jmap = new JSONObject(map);
					String jsonString = jmap.toJSONString();
					ObjectMapper mapper = new ObjectMapper();
					mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
					SsapApplicantResource ssapApplicantResource = mapper.readValue(jsonString, SsapApplicantResource.class);

					String[] array = ssapApplicantResource._links.toString().split(",");
					for (String s : array)
					{
						if (s.contains("self"))
						{
							String sd = s.substring(s.lastIndexOf("/") + 1);
							applicantId = Long.valueOf(sd.substring(0, sd.lastIndexOf("}")));
							break;
						}

					}
					if (!("N".equalsIgnoreCase(ssapApplicantResource.getOnApplication())))
					{
						applicants.put(applicantId, ssapApplicantResource);
					}
				}
			}
			
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while getting the applicants for applicationId::" + applicationId, e);
			throw new Exception(e);
		}
		return applicants;
	}
	
	/**
	 * Creates a new SsapApplicant using POST
	 *
	 * @param ssapApplicant
	 * @param ssapApplicationLink
	 * @throws Exception
	 */
	@Override
    public String createSsapApplicant(SsapApplicantResource ssapApplicant, String ssapApplicationLink) throws Exception {
		String applicantLink = null;
		try {
			
			ssapApplicant.setSsapApplication(ssapApplicationLink);
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicantResource.class);
			String applicantJson = writer.writeValueAsString(ssapApplicant);
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANTS_DATA_URL;
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			
			ResponseEntity<Resource<SsapApplicantResource>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(applicantJson, requestHeaders), new ParameterizedTypeReference<Resource<SsapApplicantResource>>() {},String.class);
			if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
				applicantLink = responseEntity.getHeaders().getLocation().getPath();
			}
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while creating the applicant", e);
			throw new Exception(e);
		}
		
		return applicantLink;
	}
	
	/**
	 * Updates the SsapApplicant using PUT
	 * @see com.getinsured.iex.client.ResourceCreator#updateSsapApplicant(com.getinsured.iex.dto.SsapApplicantResource)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateSsapApplicant(SsapApplicantResource ssapApplicantResource) throws Exception {
		try {
			Object links = ssapApplicantResource.get_links();
			JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
			Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANTS_DATA_URL+"{id}";
			String selfHref= (self.get("href")==null)?"": self.get("href");
			if("".equals(selfHref)) {
				LOGGER.error("The end point to update the applicant is incorrect or not defined");
				throw new Exception("The end point to update the applicant is incorrect or not defined");
			}
			long ssapApplicantId = getNumerIDAtEndOfLink(selfHref);
			Map<String, Object> uriParams = new HashMap<>();
			uriParams.put("id", ssapApplicantId);
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType( SsapApplicantResource.class);
			String jsonString = writer.writeValueAsString(ssapApplicantResource);
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(mediaTypeList);
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);
		 
			restTemplate.exchange(url, HttpMethod.PUT, requestEntity,new ParameterizedTypeReference<Resource<SsapApplicantResource>>() {},uriParams);
		}
		catch(JsonGenerationException jge) {
			LOGGER.error("Exception in writing value as a string", jge);
			throw new Exception(jge);
		}
		catch(JsonMappingException jme) {
			LOGGER.error("Exception in writing value as a string", jme);
			throw new Exception(jme);
		}
		catch(IOException ioe) {
			LOGGER.error("Exception in writing value as a string", ioe);
			throw new Exception(ioe);
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while updating the applicant", e);
			throw new Exception(e);
		}
	}

	@Override
	public SsapApplicationResource getSsapApplicationByCmrHouseoldId(
			long cmrHouseoldId) throws JsonParseException, JsonMappingException, IOException {
		String baseUrl = GhixEndPoints.GHIX_SSAP_SVC_URL;
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + "search/findByCmrHouseoldId?cmrHouseoldId={cmrHouseoldId}";
		Map<String, Long> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("cmrHouseoldId", cmrHouseoldId);
		
		ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
		
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
		
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				List lst = (List)jobj.get("ssapApplications");
				Map map = (Map)lst.get(0);
				JSONObject jmap = new JSONObject(map);
				String jsonString = jmap.toJSONString();
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicationResource.class);

				return reader.readValue(jsonString );
			}	
			
		}
		
		return null;
	}
	
	@Override
	public SsapApplicationResource getSsapApplicationByMaxIdAndCmrHouseoldId(
			long cmrHouseoldId) throws JsonParseException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + "search/findByMaxIdAndCmrHouseoldId?cmrHouseoldId={cmrHouseoldId}";
		Map<String, Long> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("cmrHouseoldId", cmrHouseoldId);
		
		ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
		
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
		
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				List lst = (List)jobj.get("ssapApplications");
				Map map = (Map)lst.get(0);
				JSONObject jmap = new JSONObject(map);
				String jsonString = jmap.toJSONString();
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicationResource.class);

				return reader.readValue(jsonString);
			}	
			
		}
		
		return null;
	}

	@Override
	public SsapApplicationEvent saveSsapapplicationEvent(SsapApplicationEvent ssapApplicationEvent) throws JsonGenerationException, JsonMappingException, IOException, RestClientException {
		//String baseUrl = GhixEndPoints.GHIX_SSAP_SVC_URL;
		//String url =   baseUrl + IEX_LCE_REST_SAVE_SSAP_APPLICATION_EVENT;
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + IEX_LCE_REST_SAVE_SSAP_APPLICATION_EVENT; 
		// Set the Content-Type header
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application","json"));
		HttpEntity<SsapApplicationEvent> requestEntity = new HttpEntity<>(ssapApplicationEvent, requestHeaders);

		// Make the HTTP POST request, marshaling the request to JSON, and the response to a String
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
		String result = responseEntity.getBody();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicationEvent.class);
		return reader.readValue(result  );		
	}

	@Override
	public SsapApplicantEvent saveSsapapplicantEvent(SsapApplicantEvent ssapApplicantEvent) throws JsonGenerationException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + IEX_LCE_REST_SAVE_SSAP_APPLICANT_EVENT;
		
		// Set the Content-Type header
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application","json"));
		HttpEntity<SsapApplicantEvent> requestEntity = new HttpEntity<>(ssapApplicantEvent, requestHeaders);



		// Make the HTTP POST request, marshaling the request to JSON, and the response to a String
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
		String result = responseEntity.getBody();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicantEvent.class);
		return reader.readValue(result);
		
	}

	@Override
	public List<SsapApplicantEvent> findSsapApplicantEventsByApplicationEvent(SsapApplicationEvent ssapAplicationEvent) throws JsonParseException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + IEX_LCE_REST_FIND_SSAP_APPLICANT_EVENTS;
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application","json"));
		HttpEntity<SsapApplicationEvent> requestEntity = new HttpEntity<>(ssapAplicationEvent, requestHeaders);

		// Make the HTTP POST request, marshaling the request to JSON, and the response to a String
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
		String result = responseEntity.getBody();
		if(result != null) {
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicantEvent[].class);
		
		SsapApplicantEvent[] ssapApplicantEvents = reader.readValue(result);
		
		return Arrays.asList(ssapApplicantEvents);
	}
		return null;
	}
	
	@Override
	public List<SsapApplicant> getSsapApplicantsByApplicationId(long applicationId) throws JsonParseException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "findSsapApplicants";
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application","json"));
		SsapApplication ssapApplication = new SsapApplication();
		ssapApplication.setId(applicationId);
		HttpEntity<SsapApplication> requestEntity = new HttpEntity<>(ssapApplication, requestHeaders);

		// Make the HTTP POST request, marshaling the request to JSON, and the response to a String
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
		String result = responseEntity.getBody();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicant[].class);
		SsapApplicant[] ssapApplicants = reader.readValue(result );
		
		return Arrays.asList(ssapApplicants);
	}
	
	@Override
	public String saveLCESsapApplicant(SsapApplicantResource ssapApplicant) throws Exception {
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "SsapApplicant/";
		long ssapApplicationID = 0;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicantResource.class); 

		String jsonString = writer.writeValueAsString(ssapApplicant);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Collections.singletonList(new MediaType("application","json")));

		String ssapApplicationLink = "";
		ResponseEntity<Resource<SsapApplicantResource>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jsonString, requestHeaders), new ParameterizedTypeReference<Resource<SsapApplicantResource>>() {},String.class);
		if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
			ssapApplicationLink = responseEntity.getHeaders().getLocation().getPath();
		}

		return  ssapApplicationLink;
	}	
	
	@Override
	public SsapApplicationResource findByCmrHouseoldIdAndApplicationStatusAndMaxId(
			long cmrHouseoldId, String status) throws JsonParseException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL +
				"SsapApplication/search/findByCmrHouseoldIdAndApplicationStatusAndMaxId?cmrHouseoldId={cmrHouseoldId}&status={status}";
		Map<String, Object> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("cmrHouseoldId", cmrHouseoldId);
		uriParams.put("status", status);
		
		ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
		
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				List lst = (List)jobj.get("ssapApplications");
				Map map = (Map)lst.get(0);
				JSONObject jmap = new JSONObject(map);
				String jsonString = jmap.toJSONString();
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicationResource.class);

				return reader.readValue(jsonString);
			}	
		}
		return null;
	}
	
	@Override
	public SsapApplicationEventResource findSsapApplicationEventByApplicationId(long applicationId, int lookupValueId) throws JsonParseException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL +
				"SsapApplicationEvent/search/findBySssapApplicationId?applicationId={applicationId}&lookupValueId={lookupValueId}";
		Map<String, Object> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("applicationId", applicationId);
		uriParams.put("lookupValueId", lookupValueId);
		
		ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
					uriParams);
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				List lst = (List)jobj.get("ssapApplicationEvents");
				Map map = (Map)lst.get(0);
				JSONObject jmap = new JSONObject(map);
				String jsonString = jmap.toJSONString();
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicationEventResource.class);

				return reader.readValue(jsonString);
			}	
		}
		
		return null;
	}
	
	@Override
	public String createSsapApplicationEvent(SsapApplicationEventResource ssapApplicationEventResource, String ssapApplicationEventLink) throws JsonGenerationException, JsonMappingException, IOException {
		String link = null;
		
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "SsapApplicationEvent/";
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicationEventResource.class); 
		
		ssapApplicationEventResource.setSsapApplication(ssapApplicationEventLink);
		String jsonString = writer.writeValueAsString(ssapApplicationEventResource);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Collections.singletonList(new MediaType("application","json")));

		ResponseEntity<Resource<SsapApplicationEventResource>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jsonString, requestHeaders), new ParameterizedTypeReference<Resource<SsapApplicationEventResource>>() {},String.class);
		if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
			link = responseEntity.getHeaders().getLocation().getPath();
		}

		return  link;
	}
	

	@Override
	public String updateSsapApplicationEvent(SsapApplicationEventResource ssapApplicationEventResource, long ssapApplicationEventId) throws Exception {
	
		try {
			Object links = ssapApplicationEventResource.get_links();
			JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
			Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
			String selfHref= (self.get("href")==null)?"": self.get("href");
			URI myUri = null;
			if("".equals(selfHref)) {
				throw new Exception("The end point to update the applicant is incorrect or not defined");
			}
			else {
				myUri = URI.create(selfHref);
			}

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicationEventResource.class);
			String jsonString = writer.writeValueAsString(ssapApplicationEventResource);
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(mediaTypeList);
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);
		 
			ResponseEntity<Resource<SsapApplicationEventResource>> responseEntity = restTemplate.exchange(selfHref, HttpMethod.PUT, requestEntity,new ParameterizedTypeReference<Resource<SsapApplicationEventResource>>() {},Collections.EMPTY_MAP);
			return responseEntity.getHeaders().getLocation().toString();
		}
		catch(JsonGenerationException jge) {
			LOGGER.error("Exception in writing value as a string", jge);
			throw new Exception(jge);
		}
		catch(JsonMappingException jme) {
			LOGGER.error("Exception in writing value as a string", jme);
			throw new Exception(jme);
		}
		catch(IOException ioe) {
			LOGGER.error("Exception in writing value as a string", ioe);
			throw new Exception(ioe);
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while updating the applicant", e);
			throw new Exception(e);
		}
	}
	
	@Override
	public Map<Long,SsapApplicantResource> getApplicantEvents(long applicationEventId) throws Exception {

		Map <Long,SsapApplicantResource> applicantEvents = null;
		long applicantEventId = 0;
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL +
					"SsapApplicantEvent/search/findBySsapApplicationEventId?applicationEventId={applicationEventId}";
			Map<String, Long> uriParams = new HashMap<>();
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.ALL);
			requestHeaders.setAccept(mediaTypeList);
			HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
			uriParams.put("applicationEventId", applicationEventId);
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
			
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				applicantEvents = new HashMap<>();
				
				List lst = (List)jobj.get("ssapApplicants");

				for (Object o : lst)
				{
					Map map = (Map) o;
					JSONObject jmap = new JSONObject(map);
					String jsonString = jmap.toJSONString();
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicantResource.class);

					SsapApplicantResource ssapApplicantEventResource = reader.readValue(jsonString);

					String link = getLink(ssapApplicantEventResource);
					applicantEventId = Long.valueOf(link.substring(link.lastIndexOf("/") + 1));
					applicantEvents.put(applicantEventId, ssapApplicantEventResource);
				}
			} else {
				applicantEvents = new HashMap<>();
			}
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while gettign the applicants for applicantEventId::" + applicantEventId, e);
			throw new Exception(e);
		}
		return applicantEvents;
	}
	
	@Override
	public String createSsapApplicantEvent(SsapApplicantEventResource SsapApplicantEventResource) throws JsonGenerationException, JsonMappingException, IOException {
		String link = null;
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "SsapApplicantEvent"; 
		
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicantEventResource.class); 
		
		String jsonString = writer.writeValueAsString(SsapApplicantEventResource);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Collections.singletonList(new MediaType("application","json")));

		ResponseEntity<Resource<SsapApplicantEventResource>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jsonString, requestHeaders), new ParameterizedTypeReference<Resource<SsapApplicantEventResource>>() {},String.class);
		if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
			link = responseEntity.getHeaders().getLocation().getPath();
		}

		return  link;
	}
	
	public static String getLink(SsapApplicantResource ssapApplicantResource) throws Exception{
		String link = null;
		try {
			Object links = ssapApplicantResource.get_links();
			
			if(links!=null){
				JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
				Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
				String selfHref= (self.get("href")==null)?"": self.get("href");
				URI myUri = null;
				if("".equals(selfHref)) {
					throw new Exception("The end point to update the applicant is incorrect or not defined");
				}
				else {
					myUri = URI.create(selfHref);
					link = myUri.toString();
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching SsapApplicant ID from Json : " , exception);
			throw exception;
		}
		
		return link;
	}
	
	@Override
	public SsapApplicantResource getSsapApplicantByApplicationAndPersonId(long applicationId, long personId) throws Exception {
		SsapApplicantResource ssapApplicantResource = null;
		try{
			String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "SsapApplicant/search/findBySsapApplicationIdAndPersonId?applicationId={applicationId}&personId={personId}";

			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.ALL);
			requestHeaders.setAccept(mediaTypeList);
			HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
			Map<String, Long> uriParams = new HashMap<>();
			uriParams.put("applicationId", applicationId);
			uriParams.put("personId", personId);
			
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
			
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				
				List lst = (List)jobj.get("ssapApplicants");

				if(lst != null && lst.size() > 0)
				{
					Map map = (Map) lst.get(0);
					JSONObject jmap = new JSONObject(map);
					String jsonString = jmap.toJSONString();
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicantResource.class);

					ssapApplicantResource = reader.readValue(jsonString);
				}
			}
		}catch(Exception exception){
			LOGGER.error("Exception occurred while invoking getSsapApplicantByApplicationAndPersonId() ", exception);
		}
		
		return ssapApplicantResource;
	}
	
	@Override
	public void updateSsapApplicant(long applicantId) throws Exception {

		try{
			String url = GhixEndPoints.ELIGIBILITY_URL + "lce/updateSsapApplicant?id={id}";

			restTemplate.postForObject(url, null, String.class, applicantId);
			
		}catch(Exception exception){
			LOGGER.error("Exception occurred while invoking updateSsapApplicant() ", exception);
		}
	}

	@Override
	public SsapApplicantResource getSsapApplicantById(long id) {
		String url =   GhixEndPoints.SsapEndPoints.SSAP_APPLICANTS_DATA_URL + "{id}";
		
		Map<String, Long> uriParams = new HashMap<>();
		uriParams.put("id", id);
		ResponseEntity<Resource<SsapApplicantResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, null, 
					new ParameterizedTypeReference<Resource<SsapApplicantResource>>() {}, 
					uriParams);
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			Resource<SsapApplicantResource> resource =responseEntity.getBody();
			return  resource.getContent();
		}
		else {
			//  handle this later
			return null;
		}
	}

	/**
	 * Fetch Ssap Verification based in ID
	 *
	 * @param id
	 * @return SsapVerificationResource
	 * @throws Exception
	 */
	@Override
	public SsapVerificationResource getSsapVerificationById(long id) {
		
		try{
			String url =   GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "SsapVerification/{id}";
			
			Map<String, Long> uriParams = new HashMap<>();
			uriParams.put("id", id);
			ResponseEntity<Resource<SsapVerificationResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, null, 
						new ParameterizedTypeReference<Resource<SsapVerificationResource>>() {}, 
						uriParams);
			if (responseEntity.getStatusCode() == HttpStatus.OK) {
				Resource<SsapVerificationResource> resource =responseEntity.getBody();
				return  resource.getContent();
			}
		} catch(Exception exception){
			LOGGER.error("Exception occurred while fetching SsapVerification : " , exception);
			throw exception;
		}
		
		return null;
	}
	
	/**
	 * Creates a new Ssap Verification
	 *
	 * @param ssapVerification
	 * @param ssapApplicantLink
	 * @return verificationLink
	 * @throws Exception
	 */
	@Override
	public String createSsapVerification(SsapVerificationResource ssapVerification, String ssapApplicantLink) throws Exception {
		String verificationLink = null;
		try {
			
			ssapVerification.setSsapApplicant(ssapApplicantLink);
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapVerificationResource.class);
			String json = writer.writeValueAsString(ssapVerification);
			String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "SsapVerification/";
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			
			ResponseEntity<Resource<SsapVerificationResource>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(json, requestHeaders), new ParameterizedTypeReference<Resource<SsapVerificationResource>>() {},String.class);
			if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
				verificationLink = responseEntity.getHeaders().getLocation().getPath();
			}
		}
		catch(Exception exception) {
			LOGGER.error("Exception occurred while creating the SSAP Verification", exception);
			throw new Exception(exception);
		}
		
		return verificationLink;
	}
	
	/**
	 * Updates Ssap Verification
	 *
	 * @param ssapVerificationResource
	 * @return verificationLink
	 * @throws Exception
	 */
	@Override
	public String updateSsapVerification(SsapVerificationResource ssapVerificationResource) throws Exception {
	
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "SsapVerification/{id}";
			Object links = ssapVerificationResource.get_links();
			JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
			Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
			String selfHref= (self.get("href")==null)?"": self.get("href");
			if("".equals(selfHref)) {
				throw new Exception("The end point to update the verification is incorrect or not defined");
			}
			long ssapVerificationId = getNumerIDAtEndOfLink(selfHref);
			Map<String, Object> uriParams = new HashMap<>();
			uriParams.put("id", ssapVerificationId);

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapVerificationResource.class);
			String jsonString = writer.writeValueAsString(ssapVerificationResource);
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(mediaTypeList);
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);
		 
			ResponseEntity<Resource<SsapVerificationResource>> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					new ParameterizedTypeReference<Resource<SsapVerificationResource>>() {},uriParams);
			return responseEntity.getHeaders().getLocation().toString();
		} catch(Exception exception) {
			LOGGER.error("Exception occurred while updating the Verification ", exception);
			throw new Exception(exception);
		}
	}
	
	/**
	 * Updates Ssap Application
	 *
	 * @param ssapApplicationResource
	 * @return ssapApplicationLink
	 * @throws Exception
	 */
	@Override
	public String updateSsapApplication(SsapApplicationResource ssapApplicationResource) throws Exception {
	
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL+"{id}";
			
			Object links = ssapApplicationResource.get_links();
			JSONObject jsonObjectLinks = new JSONObject((Map<String,String>)links);
			Map<String,String> self = (Map<String,String>)jsonObjectLinks.get("self");
			String selfHref= (self.get("href")==null)?"": self.get("href");
			if("".equals(selfHref)) {
				throw new Exception("The end point to update the SSAP Application is incorrect or not defined");
			}
			long ssapApplicationId = getNumerIDAtEndOfLink(selfHref);
			Map<String, Object> uriParams = new HashMap<>();
			uriParams.put("id", ssapApplicationId);

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicationResource.class);
			String jsonString = writer.writeValueAsString(ssapApplicationResource);
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(mediaTypeList);
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);
		 
			ResponseEntity<Resource<SsapApplicationResource>> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					new ParameterizedTypeReference<Resource<SsapApplicationResource>>() {},uriParams);
			
			return responseEntity.getHeaders().getLocation().toString();
		} catch(Exception exception) {
			LOGGER.error("Exception occurred while updating the SSAP Application ", exception);
			throw new Exception(exception);
		}
	}
	
	@Override
	public SsapApplicationEventResource findSsapApplicationEventBySsapApplicationId(long applicationId) 
			throws JsonParseException, JsonMappingException, IOException {

		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + 
				"SsapApplicationEvent/search/findEventBySsapApplication?applicationId={applicationId}";
		Map<String, Object> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("applicationId", applicationId);
		
		
		ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
					uriParams);
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				List lst = (List)jobj.get("ssapApplicationEvents");
				Map map = (Map)lst.get(0);
				JSONObject jmap = new JSONObject(map);
				String jsonString = jmap.toJSONString();
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicationEventResource.class);

				return reader.readValue(jsonString);
			}	
		}
		
		return null;
	}
	
	private long getNumerIDAtEndOfLink(String link) {
		long returnvalue = 0;
		Pattern patternForNumberInLink = Pattern.compile("\\d+");
		Matcher matcher = patternForNumberInLink.matcher(link);
		while (matcher.find( ))
		{
			returnvalue = Long.parseLong(matcher.group());     
		                
		}
		return returnvalue;
	}
	
	@Override
	public Map<Long,SsapApplicantResource> getActiveApplicants(long applicationId) throws Exception {
		Map <Long,SsapApplicantResource> applicants = null;
		
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_APPLICANTS_DATA_URL;
			
			Map<String, Long> uriParams = new HashMap<>();
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.ALL);
			requestHeaders.setAccept(mediaTypeList);
			HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
			uriParams.put("id", applicationId);
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
			
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				applicants = new HashMap<>();
				
				List lst = (List)jobj.get("ssapApplicants");

				for (Object o : lst)
				{
					Map map = (Map) o;
					JSONObject jmap = new JSONObject(map);
					String jsonString = jmap.toJSONString();
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicantResource.class);

					SsapApplicantResource ssapApplicantResource = reader.readValue(jsonString);

					//This block will be removed and above block will be used once it is confirmed 
					//status column will always hold value once applicants are created by SSAP/LCE
					if (ssapApplicantResource.getStatus() != null && "LCE_DELETE".equals(ssapApplicantResource.getStatus()))
					{

					}
					else
					{
						applicants.put(ssapApplicantResource.getPersonId(), ssapApplicantResource);
					}
				}
			}
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while gettign the applicants for applicationId::" + applicationId, e);
			throw new Exception(e);
		}
		return applicants;
	}
	
	@Override
	public SsapApplicantEventResource findSsapApplicantEventByApplicantId(long applicantId) {
		
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANT_EVENT_DATA_URL + "search/findBySsapApplicantId?applicantId={applicantId}";
		Map<String, Long> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("applicantId", applicantId);
		
		try {
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
						uriParams);
			
			if (responseEntity.getStatusCode() == HttpStatus.OK) {
				if(responseEntity.getBody().getContent().get_embedded()!=null){
					Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
					JSONObject jobj = new JSONObject(resp);
					List list = (List)jobj.get("ssapApplicantEvents");
					Map map = (Map)list.get(0);
					JSONObject jmap = new JSONObject(map);
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicantEventResource.class);
					return reader.readValue(jmap.toJSONString());
				}	
			}
		} catch(Exception exception){
			exception.printStackTrace();
		}

		return null;
	}
	
	@Override
	public SsapApplicantEventResource findByApplicationEventIdAndPersonIdAndSepEventId(long applicationEventId, long personId, int sepEventId) throws Exception {
		
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANT_EVENT_DATA_URL + "search/findByApplicationEventIdAndPersonIdAndSepEventId?" +
				"applicationEventId={applicationEventId}&personId={personId}&sepEventId={sepEventId}";
		Map<String, Object> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("applicationEventId", applicationEventId);
		uriParams.put("personId", personId);
		uriParams.put("sepEventId", sepEventId);
		
		try {
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
						uriParams);
	
			if (responseEntity.getStatusCode() == HttpStatus.OK) {
				if(responseEntity.getBody().getContent().get_embedded()!=null){
					Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
					JSONObject jobj = new JSONObject(resp);
					List list = (List)jobj.get("ssapApplicantEvents");
					Map map = (Map)list.get(0);
					JSONObject jmap = new JSONObject(map);
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicantEventResource.class);
					return reader.readValue(jmap.toJSONString());
				}	
			}
		} catch(Exception exception){
			LOGGER.error("Exception occurred while fetching SsapApplicantEvent by application id, person id and sepEvent Id:", exception);
			throw new Exception(exception);
		}

		return null;
	}
	
	@Override
	public void updateSsapApplicantEvent(SsapApplicantEventResource ssapApplicantEventResource, long ssapApplicantEventId) throws Exception {
	
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANT_EVENT_DATA_URL+"{id}";
			
			Map<String, Object> uriParams = new HashMap<>();
			uriParams.put("id", ssapApplicantEventId);

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicantEventResource.class);
			String jsonString = writer.writeValueAsString(ssapApplicantEventResource);
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(mediaTypeList);
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);
		 
			restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					new ParameterizedTypeReference<Resource<SsapApplicantEventResource>>() {},uriParams);
		} catch(Exception exception) {
			LOGGER.error("Exception occurred while updating the SSAP Application ", exception);
			throw new Exception(exception);
		}
	}
	
	@Override
	public SepEventsResource findByEventNameCategoryAndFinancial(String name, String category, Financial financial ) 
			throws JsonParseException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + 
				"SepEvents/search/findByEventNameCategoryAndFinancial?name={name}&category={category}&financial={financial}";

		Map<String, Object> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("name", name);
		uriParams.put("category", category);
		uriParams.put("financial", financial);
		
		ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
		
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
		
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				List list = (List)jobj.get("sepEventss");
				Map map = (Map)list.get(0);
				JSONObject jmap = new JSONObject(map);
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SepEventsResource.class);
				return reader.readValue(jmap.toJSONString());
			}	
			
		}
		return null;
	}
	
	@Override
	public String findSepEventByFinancial(Financial financial) 
			throws JsonParseException, JsonMappingException, IOException {

		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + "SepEvents/search/findByFinancial?financial={financial}";

		Map<String, Financial> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("financial", financial);
		
		try {
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
						uriParams);
			
			if (responseEntity.getStatusCode() == HttpStatus.OK) {
				if(responseEntity.getBody().getContent().get_embedded()!=null){
					Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
					JSONObject jmap = new JSONObject(resp);

					return jmap.toJSONString();
				}	
			}
		} catch(Exception exception){
			exception.printStackTrace();
		}
		
		return null;
	}
	
	@Override
	public void deleteRecordById(String tableName, long id) {
		String url = null;
		Map<String, Long> uriParams = new HashMap<>();
		uriParams.put("id", id);
		
		try {
			if(!SsapUtil.isEmpty(tableName)){
				SsapUtil.SSAP_TABLE_NAMES event = SsapUtil.SSAP_TABLE_NAMES.valueOf(tableName);
				switch(event){
				    case SSAP_APPLICATIONS :
				    	url =   GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + "{id}";
				    	break; 
				    case SSAP_APPLICANTS :
				    	url =   GhixEndPoints.SsapEndPoints.SSAP_APPLICANTS_DATA_URL + "{id}";
				    	break; 
				    case SSAP_APPLICATION_EVENTS :
				    	url =   GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_EVENT_DATA_URL + "{id}";
				    	break; 
				    case SSAP_APPLICANT_EVENTS :
				    	url =   GhixEndPoints.SsapEndPoints.SSAP_APPLICANT_EVENT_DATA_URL + "{id}";
				    	break; 
				}
				
				restTemplate.delete(url, uriParams);
			}
		} catch(Exception exception){
			LOGGER.error("Exception occurred while invoking deleteRecordById() ", exception);
		}
	}
	
	@Override
	public List<SepEventsResource> findEventsByApplicationId(long applicationId) throws Exception {
		
		List<SepEventsResource> sepEventsResources = new ArrayList<>();
		
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANT_EVENT_DATA_URL + "search/findEventsByApplicationId?applicationId={applicationId}";
		Map<String, Long> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("applicationId", applicationId);
		
		try {
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
						uriParams);
			
			if (responseEntity.getStatusCode() == HttpStatus.OK) {
				if(responseEntity.getBody().getContent().get_embedded()!=null){
					Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
					JSONObject jobj = new JSONObject(resp);
										
					List lst = (List)jobj.get("sepEventss");

					for (Object o : lst)
					{
						Map map = (Map) o;
						JSONObject jmap = new JSONObject(map);
						String jsonString = jmap.toJSONString();
						ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SepEventsResource.class);

						SepEventsResource sepEventsResource = reader.readValue(jsonString);
						sepEventsResources.add(sepEventsResource);
					}
				} 
			}
		} catch(Exception exception){
			LOGGER.error("Exception occurred while fetching SsapApplicantEvents by SsapApplication Id:", exception);
			throw new Exception(exception);
		}

		return sepEventsResources;
	}
	
	@Override
	public SsapApplicantEventResource findMinAndMaxEnrollmentStartDateByApplicationEventId(long applicationEventId, String type) throws Exception {
		String url;
		if("MIN".equals(type)){
			url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANT_EVENT_DATA_URL + "search/findMinEnrollmentStartDate?applicationEventId={applicationEventId}";
		} else {
			url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANT_EVENT_DATA_URL + "search/findMaxEnrollmentEndDate?applicationEventId={applicationEventId}";
		}
		
		Map<String, Object> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("applicationEventId", applicationEventId);
		
		try {
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
						uriParams);
	
			if (responseEntity.getStatusCode() == HttpStatus.OK) {
				if(responseEntity.getBody().getContent().get_embedded()!=null){
					Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
					JSONObject jobj = new JSONObject(resp);
					List list = (List)jobj.get("ssapApplicantEvents");
					Map map = (Map)list.get(0);
					JSONObject jmap = new JSONObject(map);
					ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicantEventResource.class);
					return reader.readValue(jmap.toJSONString());
				}	
			}
		} catch(Exception exception){
			LOGGER.error("Exception occurred while fetching SsapApplicantEvent by application id, person id and sepEvent Id:", exception);
			throw new Exception(exception);
		}

		return null;
	}
	
	@Override
	public SsapApplicationResource updateSsapApplicationById(long id, SsapApplicationResource ssapApplicationResource) throws Exception {
	
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL+"{id}";
			
			Map<String, Object> uriParams = new HashMap<>();
			uriParams.put("id", id);

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicationResource.class);
			String jsonString = writer.writeValueAsString(ssapApplicationResource);
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(mediaTypeList);
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);
		 
			restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					new ParameterizedTypeReference<Resource<SsapApplicationResource>>() {},uriParams);
			
			return getSsapApplicationById(id);
		} catch(Exception exception) {
			LOGGER.error("Exception occurred while updating the SSAP Application ", exception);
			throw new Exception(exception);
		}
	}
	
	@Override
	public List<SsapApplicantResource> findApplicantsByApplicationEventAndSepEventIds(long applicationEventId, Set<Integer> sepEventsIds) throws Exception {
		
		List<SsapApplicantResource> ssapApplicantResources;
		Map <String,SsapApplicantResource> applicantResourceMap= new LinkedHashMap<>();
		for(Integer id:sepEventsIds){
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICANT_EVENT_DATA_URL + "search/findByApplicationEventIdAndSepEventId?applicationEventId={applicationEventId}&sepEventId={sepEventId}";
			Map<String, Object> uriParams = new HashMap<>();
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.ALL);
			requestHeaders.setAccept(mediaTypeList);
			HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
			uriParams.put("applicationEventId", applicationEventId);
			uriParams.put("sepEventId", id);
			
			try {
				ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
						restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
							new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
							uriParams);
				
				if (responseEntity.getStatusCode() == HttpStatus.OK) {
					if(responseEntity.getBody().getContent().get_embedded()!=null){
						Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
						JSONObject jobj = new JSONObject(resp);
											
						List lst = (List)jobj.get("ssapApplicants");

						for (Object o : lst)
						{
							Map map = (Map) o;
							JSONObject jmap = new JSONObject(map);
							String jsonString = jmap.toJSONString();
							ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicantResource.class);

							SsapApplicantResource ssapApplicantResource = reader.readValue(jsonString);
							applicantResourceMap.put(ssapApplicantResource.getApplicantGuid(), ssapApplicantResource);
						}
					} 
				}
			} catch(Exception exception){
				LOGGER.error("Exception occurred while fetching SsapApplicantEvents by SsapApplication Id:", exception);
				throw new Exception(exception);
			}
		}
		ssapApplicantResources= new ArrayList<>(applicantResourceMap.values());
		return ssapApplicantResources;
	}
	
	@Override
	public SsapApplicationEventResource getSsapApplicationEventById(long id) {
		
		String url =   GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_EVENT_DATA_URL + "{id}";
		
		Map<String, Long> uriParams = new HashMap<>();
		uriParams.put("id", id);
		ResponseEntity<Resource<SsapApplicationEventResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, null, 
					new ParameterizedTypeReference<Resource<SsapApplicationEventResource>>() {}, 
					uriParams);
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			Resource<SsapApplicationEventResource> resource =responseEntity.getBody();
			return  resource.getContent();
		}
		else {
			return null;
		}
	}
	
	@Override
	public Map<Long,SsapApplicantResource> getAllApplicants(long applicationId) throws Exception {
		Map <Long,SsapApplicantResource> applicants = null;
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_APPLICANTS_DATA_URL;
			
			Map<String, Long> uriParams = new HashMap<>();
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.ALL);
			requestHeaders.setAccept(mediaTypeList);
			HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
			uriParams.put("id", applicationId);
			ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
					restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
						new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
			
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				applicants = new HashMap<>();
				
				List lst = (List)jobj.get("ssapApplicants");

				for (Object o : lst)
				{
					Map map = (Map) o;
					JSONObject jmap = new JSONObject(map);
					String jsonString = jmap.toJSONString();
					ObjectMapper mapper = new ObjectMapper();
					mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
					SsapApplicantResource ssapApplicantResource = mapper.readValue(jsonString, SsapApplicantResource.class);
					applicants.put(ssapApplicantResource.getPersonId(), ssapApplicantResource);
				}
			}
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while gettign the applicants for applicationId::" + applicationId, e);
			throw new Exception(e);
		}
		
		return applicants;
	}
	
	@Override
	public SsapApplicationEventResource updateSsapApplicationEventById(long id, SsapApplicationEventResource ssapApplicationEventResource) throws Exception {
	
		try {
			String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_EVENT_DATA_URL+"{id}";
			
			Map<String, Object> uriParams = new HashMap<>();
			uriParams.put("id", id);

			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicationEventResource.class);
			String jsonString = writer.writeValueAsString(ssapApplicationEventResource);
			HttpHeaders requestHeaders = new HttpHeaders();
			List<MediaType> mediaTypeList = new ArrayList<>();
			mediaTypeList.add(MediaType.APPLICATION_JSON);
			requestHeaders.setAccept(mediaTypeList);
			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<?> requestEntity = new HttpEntity<Object>(jsonString, requestHeaders);
		 
			restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
					new ParameterizedTypeReference<Resource<SsapApplicationEventResource>>() {},uriParams);
			
		} catch(Exception exception) {
			LOGGER.error("Exception occurred while updating the SSAP Application ", exception);
			throw new Exception(exception);
		}
		
		return null;
	}
	
	@Override
	public String saveSsapApplication(SsapApplicationResource ssapApplicationResource) throws JsonGenerationException, JsonMappingException, IOException {
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(SsapApplicationResource.class); 
		
		String jsonString = writer.writeValueAsString(ssapApplicationResource);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Collections.singletonList(new MediaType("application","json")));

		String ssapApplicationLink = "";
		ResponseEntity<Resource<SsapApplicationResource>> responseEntity = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(jsonString, requestHeaders), new ParameterizedTypeReference<Resource<SsapApplicationResource>>() {},String.class);
		if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
			ssapApplicationLink = responseEntity.getHeaders().getLocation().getPath();
		}
		return  ssapApplicationLink;
	}

	@Override
	public long countOfApplicationsByStatus(BigDecimal cmrHouseoldId,String applicationType) {
		long countOfApplications = 0;
		String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL+"/search/countOfApplicationsByStatus?cmrHouseoldId={cmrHouseoldId}&applicationType={applicationType}";
		Map<String, Object> uriParams = new HashMap<>();
		uriParams.put("cmrHouseoldId", cmrHouseoldId);
		uriParams.put("applicationType", applicationType);
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, uriParams);
		
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
		
			if(responseEntity.getBody().getContent().get_embedded()!=null){
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jobj = new JSONObject(resp);
				countOfApplications  = ((List)jobj.get("ssapApplications")).size();
			}	
			
		}
		return countOfApplications;
	}
	
	@Override
	public String findByEventChangeTypeAndFinancial(String changeType, Financial financial, String displayOnUI) 
			throws JsonParseException, JsonMappingException, IOException {
		
		String url = GhixEndPoints.SsapEndPoints.SSAP_DATA_ACCESS_URL + 
				"SepEvents/search/findByEventChangeTypeAndFinancial?changeType={changeType}&financial={financial}&displayOnUI={displayOnUI}";

		Map<String, Object> uriParams = new HashMap<>();
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);
		uriParams.put("changeType", changeType);
		uriParams.put("financial", financial);
		uriParams.put("displayOnUI", displayOnUI);
		
		ResponseEntity<Resource<EmbeddedResource>> responseEntity = 
				restTemplate.exchange(url, HttpMethod.GET, requestEntity, 
					new ParameterizedTypeReference<Resource<EmbeddedResource>>() {}, 
					uriParams);
		
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			if(responseEntity.getBody().getContent().get_embedded()!=null) {
				Map resp = (Map)responseEntity.getBody().getContent().get_embedded();
				JSONObject jmap = new JSONObject(resp);

				return jmap.toJSONString();
			}	
		}
		return null;
	}
}
