$(document).ready(function(){
	
	//add .header to thead tr
    $('.table thead tr').addClass('header');
    
    $('.clearForm').click(function(){
        $(this).parents("form").find("input").not(':button,:hidden').val("");
        $('label.error').hide();
	});
    
   $('#rightpanel .graydrkbg').find('.btn').addClass('btn-small');
    
	
	//HIX-22629
	$('a[title="Account"]').append(' <i class="caret"></i>');	
	
	/*HIX-25202 fixed as per changed requirement*/
	$('#hidekeep-currentplnTab').hide();
	
	/* HIX-35709 */
	$('#ticketStatus').removeAttr('multiple').removeAttr('size');
	
	var viewportHeight = $(window).height();
 	//console.log(viewportHeight,'ghixcustom')
 	if (viewportHeight < 650) {
 		$('.container').append('<style>.modal { top: 5%; height: auto;} .modal.fade.in {top: 5%;} iframe#search {min-height: 585px} .modal-body {height: auto;} .popup-address {margin-top:0% } div#addressIFrame.modal.popup-address.in  {height: auto;} div#addressIFrame.modal.popup-address.in .modal-body {height: auto}</style>');
 	 }else{
 		$('.container').append('<style>.modal { top: 5%; height: auto;} iframe#search {min-height: 595px} .modal-body {height: auto;}</style> ');
 	 }
 	
 	
 	
 	
 	
	
	var language = $('#lang').val() || 'en';
	
	if(language === 'en') {
		$('#language-select').text('Espa\xF1ol');
	} else {
		$('#language-select').text('English');
	}
				
	var url = window.location.href;
	
	if(url.indexOf('#/') > 0) {
		url = url.substring(0, url.indexOf('#/'));
	}
	
	if(url.indexOf('lang') > 0) {
		url = url.substring(0, url.indexOf('lang') - 1);
	}

	if(url.indexOf('?') < 0){
		url += '?';
	} else if(url.indexOf('?') > 0 && url[url.length-1] != '&'){
		url += '&';
	}

	$('#language-select').click(function(){
		
		if(language === 'en'){
			language = 'es';
			
		}else{
			language = 'en';
		}
		
		url += 'lang=' + language;
		
		$.ajax({ 
			type : "GET", 
			url : "/hix/private/changeLang?lang=" + language, 
			success : function(response) {
				window.location.href = url;
			},
			error : function(e) {
				alert('Please try again later!');
			}
		});
		
	});
});
