package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ExpenseType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.FrequencyType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAugmentationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonContactInformationAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PregnancyStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.TribalAugmentationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExpenseCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExpenseCategoryCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.fips_6_4._2.USCountyCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AddressType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ContactInformationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.FullTelephoneNumberType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonLanguageType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ProperNameTextType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.StreetType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.StructuredAddressType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TelephoneNumberType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType;
import com.getinsured.iex.ssap.financial.type.Frequency;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.util.ReferralUtil;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Component
public class PersonTypeMapper {
	private static final String kEY_AMERICAN_INDIAN_ALASKA_NATIVE = "americanIndianAlaskaNative";
	private static final String kEY_HOUSEHOLD_CONTACT = "householdContact";
	private static final String kEY_PHONE = "phone";
	private static final String OTHER_PHONE = "otherPhone";
	//private static String lABEL = "label";
	private static String cODE = "code";
	private static String ethnicityOtherLabel = "otherLabel";
	private static String raceLabel = "label";
	private static Logger lOGGER = Logger.getLogger(PersonTypeMapper.class);
	private PersonType person;
	
	@Autowired private SsapApplicantRepository ssapApplicant;
	
	public static final Map<com.getinsured.iex.ssap.financial.type.Frequency, String> incomeFrequencyTypeToAppDataJson;
	static{
		incomeFrequencyTypeToAppDataJson = new HashMap<com.getinsured.iex.ssap.financial.type.Frequency, String>();
		incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.WEEKLY, com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType.WEEKLY.value());
    	incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.BIWEEKLY, com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType.BI_WEEKLY.value());
    	incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.MONTHLY, com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType.MONTHLY.value());
    	incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.QUARTERLY, com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType.QUARTERLY.value());
    	incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.YEARLY , com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType.ANNUALLY.value());
    	incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.BIMONTHLY, com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType.SEMI_MONTHLY.value());
    	incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.HOURLY, com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType.HOURLY.value());
    	incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.ONCE, "Once");
    	incomeFrequencyTypeToAppDataJson.put(com.getinsured.iex.ssap.financial.type.Frequency.DAILY, com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType.DAILY.value());
	}
	
	private PersonNameType createPersonName(JSONObject personNameObject ){
		// person name
		PersonNameType name = AccountTransferUtil.niemCoreFactory.createPersonNameType();
		if(ReferralUtil.isNotNullAndEmpty((String)personNameObject.get("firstName"))){
			name.setPersonGivenName(AccountTransferUtil.createPersonNameTextType((String) personNameObject.get("firstName")));
		}
		if(ReferralUtil.isNotNullAndEmpty((String)personNameObject.get("middleName"))){
			name.setPersonMiddleName(AccountTransferUtil.createPersonNameTextType((String) personNameObject.get("middleName")));
		}
		if(ReferralUtil.isNotNullAndEmpty((String)personNameObject.get("lastName") )){
			name.setPersonSurName(AccountTransferUtil.createPersonNameTextType((String) personNameObject.get("lastName")));
		}
		if(ReferralUtil.isNotNullAndEmpty((String)personNameObject.get("suffix"))){
			name.setPersonNameSuffixText(AccountTransferUtil.createPersonNameTextType((String) personNameObject.get("suffix")));
		}
		return name;
	}
	
	private DateType createPersonDateOfBirth(String dob){
		DateType dateTypeDOB = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date dateDOB = AccountTransferUtil.basicFactory.createDate();
		dateDOB.setValue(AccountTransferUtil.stringToXMLGregorianCalendar(dob));
		dateTypeDOB.setDate(dateDOB);
		return dateTypeDOB;
	}
	private IdentificationType createPersonSSNID(String ssn){
		String ssnNumber = StringUtils.remove(ssn, "-");
		IdentificationType ssnIdentificationType = null;
		if(StringUtils.isNotBlank(ssnNumber)){
			ssnIdentificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
			ssnIdentificationType.setIdentificationID(AccountTransferUtil.createString(ssnNumber));
		}else{
			lOGGER.debug("passed ssn is blank");
			//handle later
		}
		return ssnIdentificationType;
	} 
	
	private PersonContactInformationAssociationType createHomeAddress(JSONObject householdMemberJSON, JSONObject primaryHouseHoldContact,boolean mailingAddressSameAsHomeAddressIndicator ){
		PersonContactInformationAssociationType personContactInformationAssociation = AccountTransferUtil.hixCoreFactory.createPersonContactInformationAssociationType();
		//TODO:set associationBeginDate
		/*DateType associationBeginDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date associationBeginDate = AccountTransferUtil.basicFactory.createDate();
		associationBeginDateType.setDate(associationBeginDate);
		personContactInformationAssociation.setAssociationBeginDate(associationBeginDateType);*/
		
		ContactInformationType contactInformationType = AccountTransferUtil.niemCoreFactory.createContactInformationType();
		AddressType addressType =AccountTransferUtil.niemCoreFactory.createAddressType();
		StructuredAddressType structuredAddressType = AccountTransferUtil.niemCoreFactory.createStructuredAddressType();
		StreetType streetType1 = AccountTransferUtil.niemCoreFactory.createStreetType();
		TextType streetTextType = AccountTransferUtil.niemCoreFactory.createTextType();
		
		JSONObject houseHoldContact = (JSONObject)householdMemberJSON.get(kEY_HOUSEHOLD_CONTACT);
		
		JSONObject homeAddress = null;
		//homeAddressIndicator
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeType contactInformationCategoryCodeType = AccountTransferUtil.hixTypeFactory.createContactInformationCategoryCodeType();
		
		if(mailingAddressSameAsHomeAddressIndicator == false){
			contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.HOME);
		}else if(mailingAddressSameAsHomeAddressIndicator == true){
			personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
			contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.MAILING);
		}else{
			personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
			contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.MAILING);
		}
		
		personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
		
		personContactInformationAssociation.setContactInformationCategoryCode(contactInformationCategoryCodeType);
		
		if(AccountTransferUtil.checkBoolean(householdMemberJSON.get("livesWithHouseholdContactIndicator"))){
			homeAddress =(JSONObject)((JSONObject)primaryHouseHoldContact.get(kEY_HOUSEHOLD_CONTACT)).get("homeAddress");
        } else if (AccountTransferUtil.checkBoolean(householdMemberJSON.get("livesAtOtherAddressIndicator"))) {
            homeAddress = (JSONObject)((JSONObject) householdMemberJSON.get("otherAddress")).get("address");
        } else {
            homeAddress = (JSONObject) householdMemberJSON.get("homeAddress");
        }
		String streetAddress1 = (String) homeAddress.get("streetAddress1");
		String unitNumber = (String) homeAddress.get("streetAddress2");
		String city = (String) homeAddress.get("city");
		String state = (String) homeAddress.get("state");
		String postalCodestr = (String) homeAddress.get("postalCode");
		String  countystr = (String) homeAddress.get("county");
		String countyCode = (String) homeAddress.get("primaryAddressCountyFipsCode");
		if(StringUtils.isBlank(countyCode)){
			countyCode = (String) homeAddress.get("countyCode");
		}
		
		streetTextType.setValue(streetAddress1);
		streetType1.setStreetFullText(streetTextType);
		structuredAddressType.setLocationStreet(streetType1);
		if(StringUtils.isNotBlank(unitNumber)){
			TextType streetTextType2 = AccountTransferUtil.niemCoreFactory.createTextType();
			streetTextType2.setValue(unitNumber);
			structuredAddressType.setAddressSecondaryUnitText(streetTextType2);
		}	
		
		ProperNameTextType properNameTextType = AccountTransferUtil.niemCoreFactory.createProperNameTextType();
		properNameTextType.setValue(city);
		structuredAddressType.setLocationCityName(properNameTextType);
		
		ProperNameTextType countyName = AccountTransferUtil.niemCoreFactory.createProperNameTextType();
		if (StringUtils.isNotBlank(countystr)) {
		    countyName.setValue(countystr);
		    structuredAddressType.setLocationCountyName(countyName);
		}
		
		USCountyCodeType USCountCodeType = AccountTransferUtil.niemFipsFactory.createUSCountyCodeType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String tempCountyCode = AccountTransferUtil.basicFactory.createString();
		if (StringUtils.isNotBlank(countyCode)) {
			tempCountyCode.setValue(countyCode.substring(countyCode.length() - 3,countyCode.length()));
			USCountCodeType.setValue(countyCode.substring(countyCode.length() - 3,countyCode.length()));
			structuredAddressType.setLocationCountyCode(tempCountyCode);
		}
		
		USStateCodeType uSStateCodeType = AccountTransferUtil.statesObjFactory.createUSStateCodeType();
		uSStateCodeType.setValue(USStateCodeSimpleType.fromValue(state));
		if(uSStateCodeType.getValue() != null){
			structuredAddressType.setLocationStateUSPostalServiceCode(uSStateCodeType);
		}
		
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String postalCode = AccountTransferUtil.basicFactory.createString();
		postalCode.setValue(postalCodestr);
		structuredAddressType.setLocationPostalCode(postalCode);
		
		addressType.setStructuredAddress(structuredAddressType);
		contactInformationType.setContactMailingAddress(addressType);
		personContactInformationAssociation.setContactInformation(contactInformationType);
		return personContactInformationAssociation;
	}
	
	private PersonContactInformationAssociationType createPersonPhoneNumberAssociation(JSONObject householdMemberJSON){
		PersonContactInformationAssociationType personContactInformationAssociation = AccountTransferUtil.hixCoreFactory.createPersonContactInformationAssociationType();
		
		/*DateType sigDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date sigDate = AccountTransferUtil.basicFactory.createDate();
		sigDateType.setDate(sigDate);
		personContactInformationAssociation.setAssociationBeginDate(sigDateType);*/
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeType contactInformationCategoryCodeType = AccountTransferUtil.hixTypeFactory.createContactInformationCategoryCodeType();
		contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.MOBILE);
		JSONObject houseHoldContact = (JSONObject)householdMemberJSON.get(kEY_HOUSEHOLD_CONTACT);
		personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
		personContactInformationAssociation.setContactInformationCategoryCode(contactInformationCategoryCodeType);
		String phoneNumStr =(String) ((JSONObject) houseHoldContact.get(kEY_PHONE)).get("phoneNumber");
		if(StringUtils.isNotBlank(phoneNumStr)){
			ContactInformationType contactInformationType = AccountTransferUtil.niemCoreFactory.createContactInformationType();
			TelephoneNumberType telephoneNumberType = AccountTransferUtil.niemCoreFactory.createTelephoneNumberType();
			FullTelephoneNumberType fullPhone = AccountTransferUtil.niemCoreFactory.createFullTelephoneNumberType();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String phonenumber = AccountTransferUtil.basicFactory.createString();
			phoneNumStr = StringUtils.remove(phoneNumStr, "-");
			phoneNumStr = StringUtils.remove(phoneNumStr, "(");
			phoneNumStr = StringUtils.remove(phoneNumStr, ")");
			phonenumber.setValue(phoneNumStr);
			fullPhone.setTelephoneNumberFullID(phonenumber);
			if( ((JSONObject) houseHoldContact.get(kEY_PHONE)).get("phoneExtension") != null){
				fullPhone.setTelephoneSuffixID(AccountTransferUtil.createString(((JSONObject) houseHoldContact.get(kEY_PHONE)).get("phoneExtension").toString()));
			}
			telephoneNumberType.setFullTelephoneNumber(fullPhone);
			contactInformationType.setContactTelephoneNumber(telephoneNumberType);
			personContactInformationAssociation.setContactInformation(contactInformationType);
		}
		
		String otherPhoneNumStr =(String) ((JSONObject) houseHoldContact.get(OTHER_PHONE)).get("phoneNumber");
		if(StringUtils.isBlank(phoneNumStr) && StringUtils.isNotBlank(otherPhoneNumStr)){
			ContactInformationType contactInformationType = AccountTransferUtil.niemCoreFactory.createContactInformationType();
			TelephoneNumberType telephoneNumberType = AccountTransferUtil.niemCoreFactory.createTelephoneNumberType();
			FullTelephoneNumberType fullPhone = AccountTransferUtil.niemCoreFactory.createFullTelephoneNumberType();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String phonenumber = AccountTransferUtil.basicFactory.createString();
			otherPhoneNumStr = StringUtils.remove(otherPhoneNumStr, "-");
			otherPhoneNumStr = StringUtils.remove(otherPhoneNumStr, "(");
			otherPhoneNumStr = StringUtils.remove(otherPhoneNumStr, ")");
			phonenumber.setValue(otherPhoneNumStr);
			fullPhone.setTelephoneNumberFullID(phonenumber);
			if(((JSONObject) houseHoldContact.get(OTHER_PHONE)).get("phoneExtension") != null ){
				fullPhone.setTelephoneSuffixID(AccountTransferUtil.createString(((JSONObject) houseHoldContact.get(OTHER_PHONE)).get("phoneExtension").toString()));
			}
			telephoneNumberType.setFullTelephoneNumber(fullPhone);
			contactInformationType.setContactTelephoneNumber(telephoneNumberType);
			personContactInformationAssociation.setContactInformation(contactInformationType);
		}
		return personContactInformationAssociation;
	}
	private PersonContactInformationAssociationType createPersonEmailAssociation(JSONObject contactPreferences){
		PersonContactInformationAssociationType personContactInformationAssociation = AccountTransferUtil.hixCoreFactory.createPersonContactInformationAssociationType();
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeType contactInformationCategoryCodeType = AccountTransferUtil.hixTypeFactory.createContactInformationCategoryCodeType();
		contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.MAILING);
		personContactInformationAssociation.setContactInformationCategoryCode(contactInformationCategoryCodeType);
		/*DateType sigDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date sigDate = AccountTransferUtil.basicFactory.createDate();
		sigDateType.setDate(sigDate);
		personContactInformationAssociation.setAssociationBeginDate(sigDateType);*/
		
		personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
		String emailaddress = (String) contactPreferences.get("emailAddress");
		if(StringUtils.isNotBlank(emailaddress)){
			ContactInformationType contactInformationType = AccountTransferUtil.niemCoreFactory.createContactInformationType();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String emailStr = AccountTransferUtil.basicFactory.createString();
			emailStr.setValue(emailaddress);
			contactInformationType.setContactEmailID(emailStr);
			personContactInformationAssociation.setContactInformation(contactInformationType);
		}
		return personContactInformationAssociation;
	}
		
	private PersonLanguageType createPreferredLanguage(String language,boolean writes,boolean reads){

			PersonLanguageType personLanguageType = AccountTransferUtil.niemCoreFactory.createPersonLanguageType();
			TextType languageName =  AccountTransferUtil.niemCoreFactory.createTextType();
			languageName.setValue(language);
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean writesLang = AccountTransferUtil.basicFactory.createBoolean();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean readsLang = AccountTransferUtil.basicFactory.createBoolean();
			writesLang.setValue(writes);
			personLanguageType.setPersonWritesLanguageIndicator(writesLang);
			readsLang.setValue(reads);
			personLanguageType.setPersonSpeaksLanguageIndicator(readsLang);
			personLanguageType.getLanguageName().add(languageName);
			return personLanguageType;
	}
	private List<TextType> getEthnicityList(JSONObject ethnicityAndRace){
		List<TextType> ethnicityList = new ArrayList<TextType>();
		if(ethnicityAndRace.get("hispanicLatinoSpanishOriginIndicator")!= null){
			if((java.lang.Boolean) ethnicityAndRace.get("hispanicLatinoSpanishOriginIndicator")){
				//List<String> ethnicities = (List<String>) ((JSONObject) ethnicityAndRace.get("ethnicities")).get("ethnicity");
				List<?> ethnicities = (JSONArray) ethnicityAndRace.get("ethnicity");
				for (Object ethnicity : ethnicities) {
					 JSONObject ethnicityJson = (JSONObject) ethnicity;
					if(ethnicityJson.get(PersonTypeMapper.ethnicityOtherLabel) != null && StringUtils.isNotEmpty( ethnicityJson.get(PersonTypeMapper.ethnicityOtherLabel).toString())){
					//if(ethnicityJson.get(PersonTypeMapper.LABEL) != null && StringUtils.isNotEmpty( ethnicityJson.get(PersonTypeMapper.LABEL).toString())){	
						TextType ethnicityTextType = AccountTransferUtil.niemCoreFactory.createTextType();
						//ethnicityTextType.setValue(ethnicityJson.get(PersonTypeMapper.LABEL).toString());
						ethnicityTextType.setValue(ethnicityJson.get(PersonTypeMapper.ethnicityOtherLabel).toString());
						ethnicityList.add(ethnicityTextType);
					}	
				}
				
			}
			
		}
		return ethnicityList;
	}
	
	private List<TextType> getRaceList(JSONObject ethnicityAndRace){
		List<TextType> raceList = new ArrayList<TextType>();
		TextType raceText = AccountTransferUtil.niemCoreFactory.createTextType();
		//List<String> races = (List<String>) ((JSONObject)ethnicityAndRace.get("races")).get("race");
		List<?> races = (JSONArray) ethnicityAndRace.get("race");
		if(races.isEmpty()){
			//to be removed when ssap UI and Delloite incorprates the change code
			raceText = AccountTransferUtil.niemCoreFactory.createTextType();
			raceText.setValue("Unknown");
			raceList.add(raceText);
		}else{
			for (Object race: races) {
				JSONObject raceJSON = (JSONObject) race;
				//if(raceJSON.get(PersonTypeMapper.LABEL) != null && StringUtils.isNotBlank(raceJSON.get(PersonTypeMapper.LABEL).toString()) ){
				if(raceJSON.get(PersonTypeMapper.raceLabel) != null && StringUtils.isNotBlank(raceJSON.get(PersonTypeMapper.raceLabel).toString()) ){
					raceText = AccountTransferUtil.niemCoreFactory.createTextType();
					//raceText.setValue(raceJSON.get(PersonTypeMapper.LABEL).toString());
					raceText.setValue(raceJSON.get(PersonTypeMapper.raceLabel).toString());
					raceList.add(raceText);
					raceText = null;
				}
			}
		}
		return raceList;
	}
	public PersonType createPerson(JSONObject householdMemberJSON, JSONObject primaryHouseHold, Map<Long, Map<String, 
			VerificationMetadataType>> verificationMetadatas, Long ssapId,List<JSONObject> houseHolds, AccountTransferRequestPayloadType returnObj) {
		
		person = AccountTransferUtil.hixCoreFactory.createPersonType();
		
		String personId = householdMemberJSON.get("personId").toString();
		String applicantGuid = (String) householdMemberJSON.get("applicantGuid");
		// Id
		person.setId(applicantGuid+"P"+ personId);
		// date of birth	
		person.setPersonBirthDate(this.createPersonDateOfBirth((String) householdMemberJSON.get("dateOfBirth")));
		//name
		person.getPersonName().add(this.createPersonName((JSONObject) householdMemberJSON.get("name")));
		// SSN
		String ssnNumber = (String) ((JSONObject) householdMemberJSON.get("socialSecurityCard")).get("socialSecurityNumber");
		IdentificationType personSSNID = this.createPersonSSNID(ssnNumber);
		
		// Set verification metadata for SSN if available
        Map<String, VerificationMetadataType> personVerificationMetadata = verificationMetadatas.get(Long.valueOf(personId));
        if(null != personSSNID) {
            VerificationMetadataWrapper.setMetadata(personVerificationMetadata, personSSNID, VerificationCategoryCodeSimpleType.SSN);
            person.getPersonSSNIdentification().add(personSSNID);
        }
		
		PersonAugmentationType personAugmentationType = AccountTransferUtil.hixCoreFactory.createPersonAugmentationType();
		//address
		personAugmentationType.getPersonContactInformationAssociation().add(this.createHomeAddress(householdMemberJSON,primaryHouseHold, false));
		personAugmentationType.getPersonContactInformationAssociation().add(this.createHomeAddress(householdMemberJSON, primaryHouseHold, true));
		
		List<PersonContactInformationAssociationType> personContactInformationAssociationTypeList = personAugmentationType.getPersonContactInformationAssociation();
		boolean mailingAddress = false;
		boolean homeAddress = false;
		List<PersonContactInformationAssociationType> personContactInformationAssociationTypeNewList = new ArrayList<PersonContactInformationAssociationType>();
		for (PersonContactInformationAssociationType personContactInformationAssociationType : personContactInformationAssociationTypeList) {
			if(mailingAddress == false && personContactInformationAssociationType.getContactInformationCategoryCode().getValue() == com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.MAILING ){
				mailingAddress = true;
				personContactInformationAssociationTypeNewList.add(personContactInformationAssociationType);
			}
			if(homeAddress == false && personContactInformationAssociationType.getContactInformationCategoryCode().getValue() == com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.HOME ){
				homeAddress = true;
				personContactInformationAssociationTypeNewList.add(personContactInformationAssociationType);
			}
		}
		personAugmentationType.setPersonContactInformationAssociation(personContactInformationAssociationTypeNewList);
		personAugmentationType.getPersonContactInformationAssociation().add(this.createPersonPhoneNumberAssociation(householdMemberJSON));
		// contactPreferences:emailAddress
		JSONObject contactPreferences = (JSONObject)((JSONObject) householdMemberJSON.get(kEY_HOUSEHOLD_CONTACT)).get("contactPreferences");
		
		//preferred language
		if (personId.equals("1")) {
			personAugmentationType.getPersonContactInformationAssociation().add(this.createPersonEmailAssociation(contactPreferences));

			String speaks = (String) contactPreferences.get("preferredWrittenLanguage");
			String writes =	(String) contactPreferences.get("preferredSpokenLanguage");
			if(StringUtils.isNotBlank(speaks) && StringUtils.isNotBlank(writes) ){
				if(speaks.equalsIgnoreCase(writes)){
					personAugmentationType.getPersonPreferredLanguage().add(createPreferredLanguage(speaks,true,true));
				}else{
					personAugmentationType.getPersonPreferredLanguage().add(createPreferredLanguage(speaks,true,false));
					personAugmentationType.getPersonPreferredLanguage().add(createPreferredLanguage(writes,false,true));
				}
			}
		}else{
			personAugmentationType.getPersonContactInformationAssociation().add(this.createPersonEmailAssociation(contactPreferences));

		}

		personAugmentationType.setPersonMarriedIndicator(AccountTransferUtil.addBoolean(householdMemberJSON.get("marriedIndicator")));
		//TODO: set default marriedindicator hardcoded
		//personAugmentationType.setPersonMarriedIndicatorCode(AccountTransferUtil.createTextType("R"));

		person.getPersonEthnicityText().addAll(getEthnicityList((JSONObject)householdMemberJSON.get("ethnicityAndRace")));
		
		person.getPersonRaceText().addAll(getRaceList((JSONObject)householdMemberJSON.get("ethnicityAndRace")));
		
		JSONObject specialCircumstances = (JSONObject) householdMemberJSON.get("specialCircumstances");
		
		PregnancyStatusType pregnancyStatusType = AccountTransferUtil.hixCoreFactory.createPregnancyStatusType();
		pregnancyStatusType.setStatusIndicator(AccountTransferUtil.addBoolean(specialCircumstances.get("pregnantIndicator")));
		
		QuantityType babyQuantity = AccountTransferUtil.niemCoreFactory.createQuantityType();
		if(null != specialCircumstances.get("numberBabiesExpectedInPregnancy") && NumberUtils.isNumber(specialCircumstances.get("numberBabiesExpectedInPregnancy").toString())) {
		    babyQuantity.setValue(new BigDecimal(specialCircumstances.get("numberBabiesExpectedInPregnancy").toString()));
		    pregnancyStatusType.setPregnancyStatusExpectedBabyQuantity(babyQuantity);
		    personAugmentationType.setPersonPregnancyStatus(pregnancyStatusType);
		}else{
			 babyQuantity.setValue(new BigDecimal("0"));
			 pregnancyStatusType.setPregnancyStatusExpectedBabyQuantity(babyQuantity);
			 personAugmentationType.setPersonPregnancyStatus(pregnancyStatusType);
		}
		
		
		if(householdMemberJSON.get("externalId") != null){
			String externalId = householdMemberJSON.get("externalId").toString();
			IdentificationType identificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
			identificationType.setIdentificationID(AccountTransferUtil.createString(externalId));
			personAugmentationType.setPersonMedicaidIdentification(identificationType);
			personAugmentationType.setPersonCHIPIdentification(identificationType);
		}else{
			IdentificationType identificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
			identificationType.setIdentificationID(AccountTransferUtil.createString(applicantGuid));
			personAugmentationType.setPersonMedicaidIdentification(identificationType);
			personAugmentationType.setPersonCHIPIdentification(identificationType);
		}
		

		if(householdMemberJSON.get("expenses") != null) {
			List<?> expenses = (JSONArray) householdMemberJSON.get("expenses");
			
			for (Object expense : expenses) {
				JSONObject expenseObj = (JSONObject) expense;
				if(expenseObj != null){
					ExpenseCategoryCodeSimpleType expenseCategoryCode = null;
					if("STUDENT_LOAN_INTEREST".equalsIgnoreCase((String)expenseObj.get("type"))) {
						expenseCategoryCode = ExpenseCategoryCodeSimpleType.STUDENT_LOAN_INTEREST;
					}
					if("ALIMONY".equalsIgnoreCase((String)expenseObj.get("type"))) {
						expenseCategoryCode = ExpenseCategoryCodeSimpleType.ALIMONY;
					}
					
					ExpenseType studentLoanExpense = createExpenseFor(expenseCategoryCode, expenseObj.get("amount"), expenseObj.get("frequency"), null);
					if(null != studentLoanExpense) {
						personAugmentationType.getPersonExpense().add(studentLoanExpense); 
					}
				}
			}
		}

		JSONObject citizenshipImmigrationStatus = (JSONObject) householdMemberJSON.get("citizenshipImmigrationStatus");
		person.getPersonUSCitizenIndicator().add(addUSCitizenIndicator(personVerificationMetadata, citizenshipImmigrationStatus));
		
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean uSNaturalizedCitizenIndicatorBoolean = AccountTransferUtil.basicFactory.createBoolean();

		if(citizenshipImmigrationStatus.get("naturalizedCitizenshipIndicator") != null){
			uSNaturalizedCitizenIndicatorBoolean.setValue((java.lang.Boolean) citizenshipImmigrationStatus.get("naturalizedCitizenshipIndicator"));
			personAugmentationType.getUSNaturalizedCitizenIndicator().add(uSNaturalizedCitizenIndicatorBoolean);
		}

		personAugmentationType.getPersonUSVeteranIndicator().add(AccountTransferUtil.addBoolean(citizenshipImmigrationStatus.get("honorablyDischargedOrActiveDutyMilitaryMemberIndicator")));
		
		
		List<JSONObject> houseHoldIncome = (List<JSONObject>) householdMemberJSON.get("incomes");
		if(houseHoldIncome != null){
				personAugmentationType.getPersonIncome().addAll(new IncomeMapper().mapIncomeData(houseHoldIncome, personVerificationMetadata, householdMemberJSON));
		}
		person.setPersonAugmentation(personAugmentationType);
		person.setPersonSexText(setMaleFemale(householdMemberJSON));

		person.setTribalAugmentation(createTribalAugmentation(householdMemberJSON));
		return person;
	}

	protected com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean addUSCitizenIndicator(
			Map<String, VerificationMetadataType> personVerificationMetadata,
			JSONObject citizenshipImmigrationStatus) {
		// naturalizedCitizenshipIndicator
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean attestedCitizenIndicatorBoolean = AccountTransferUtil.basicFactory.createBoolean();
		if(citizenshipImmigrationStatus.get("citizenshipStatusIndicator")!= null){
			attestedCitizenIndicatorBoolean.setValue((Boolean) citizenshipImmigrationStatus.get("citizenshipStatusIndicator"));
		}
		// Set verification metadata for citizenship if available.
        /*if(personVerificationMetadata != null) {
            VerificationMetadataType citizenShipMetadata = personVerificationMetadata.get(VerificationCategoryCodeSimpleType.CITIZENSHIP.value());
            if(citizenShipMetadata != null) {
                attestedCitizenIndicatorBoolean.getMetadata().add(citizenShipMetadata);
            }
        }*/
		return attestedCitizenIndicatorBoolean;
	}
	
    private ExpenseType createExpenseFor(ExpenseCategoryCodeSimpleType expenseCategoryCode, Object expenseAmount, Object expenseFrequency, String userProvidedExpenseCategoryText) {
        ExpenseType expenseType = null;
        if(null != expenseAmount && null != expenseFrequency && NumberUtils.isNumber(expenseAmount.toString())) {
            expenseType = AccountTransferUtil.hixCoreFactory.createExpenseType();
            ExpenseCategoryCodeType expenseCategoryCodeType = AccountTransferUtil.hixTypeFactory.createExpenseCategoryCodeType();
            if(null != expenseCategoryCode) {
                expenseCategoryCodeType.setValue(expenseCategoryCode);
                expenseType.setExpenseCategoryCode(expenseCategoryCodeType);
            } else {
                // if expense category is null, then this is "other" type deductions in Ssap, 
                // so we populate the description about what is the deduction for as entered by the user
                TextType expenseCategoryText = AccountTransferUtil.niemCoreFactory.createTextType();
                expenseCategoryText.setValue(userProvidedExpenseCategoryText);
                expenseType.setExpenseCategoryText(expenseCategoryText);
            }
            AmountType amountType = AccountTransferUtil.niemCoreFactory.createAmountType();
            amountType.setValue(new BigDecimal(expenseAmount.toString()).divide(BigDecimal.valueOf(100)));
            expenseType.setExpenseAmount(amountType);
            FrequencyType frequencyType = AccountTransferUtil.hixCoreFactory.createFrequencyType();
            FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
            frequencyCodeType.setValue(incomeFrequencyTypeToAppDataJson.get(Frequency.valueOf(expenseFrequency.toString())));
            frequencyType.setFrequencyCode(frequencyCodeType);
            expenseType.setExpenseFrequency(frequencyType);
        }
        return expenseType;
    }
	
	private TextType setMaleFemale(JSONObject householdMemberJSON){
		TextType sexTextType = AccountTransferUtil.niemCoreFactory.createTextType();
		if(StringUtils.isNotBlank((String) householdMemberJSON.get("gender"))){
			
			if(((String) householdMemberJSON.get("gender")).equalsIgnoreCase("Female")){
				sexTextType.setValue("Female");
			}else{
				sexTextType.setValue("Male");
			}
		}
		return sexTextType;
	}
	
	private TribalAugmentationType createTribalAugmentation(JSONObject householdMemberJSON){
	
		JSONObject specialCircumstances = (JSONObject) householdMemberJSON.get("specialCircumstances");
		TribalAugmentationType tribalAugmentationType = AccountTransferUtil.hixCoreFactory.createTribalAugmentationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean indianOrAlaskaNativeIndicator = AccountTransferUtil.basicFactory.createBoolean();
		indianOrAlaskaNativeIndicator.setValue(AccountTransferUtil.checkBoolean( specialCircumstances.get("americanIndianAlaskaNativeIndicator")));
		tribalAugmentationType.setPersonAmericanIndianOrAlaskaNativeIndicator(indianOrAlaskaNativeIndicator);
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean recognisedTribalIndicator = AccountTransferUtil.basicFactory.createBoolean();
		recognisedTribalIndicator.setValue(AccountTransferUtil.checkBoolean(((JSONObject) householdMemberJSON.get(kEY_AMERICAN_INDIAN_ALASKA_NATIVE)).get("memberOfFederallyRecognizedTribeIndicator")));
		tribalAugmentationType.setPersonRecognizedTribeIndicator(recognisedTribalIndicator);
				
		if(((JSONObject)householdMemberJSON.get(kEY_AMERICAN_INDIAN_ALASKA_NATIVE)).get("tribeName") != null && 
				StringUtils.isNotEmpty(((JSONObject)householdMemberJSON.get(kEY_AMERICAN_INDIAN_ALASKA_NATIVE)).get("tribeName").toString())){
			ProperNameTextType personTribeName=AccountTransferUtil.niemCoreFactory.createProperNameTextType();
			personTribeName.setValue((java.lang.String)((JSONObject)householdMemberJSON.get(kEY_AMERICAN_INDIAN_ALASKA_NATIVE)).get("tribeName"));
			tribalAugmentationType.setPersonTribeName(personTribeName);
		}
		tribalAugmentationType.setLocationStateUSPostalServiceCode(null);
		return tribalAugmentationType;
	}
}
