//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.29 at 08:34:31 AM IST 
//


package com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.vci.gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for Base Compensation Information
 * 
 * <p>Java class for BaseCompensationInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseCompensationInfoType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vci.dsh.cms.gov/extension/1.0}PayRate" minOccurs="0"/>
 *         &lt;element ref="{http://vci.dsh.cms.gov/extension/1.0}PayFrequencyCode" minOccurs="0"/>
 *         &lt;element ref="{http://vci.dsh.cms.gov/extension/1.0}PayFrequencyMessage" minOccurs="0"/>
 *         &lt;element ref="{http://vci.dsh.cms.gov/extension/1.0}PayPeriodFrequencyCode" minOccurs="0"/>
 *         &lt;element ref="{http://vci.dsh.cms.gov/extension/1.0}PayPeriodFrequencyMessage" minOccurs="0"/>
 *         &lt;element ref="{http://vci.dsh.cms.gov/extension/1.0}AnnualizedIncome" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseCompensationInfoType", propOrder = {
    "payRate",
    "payFrequencyCode",
    "payFrequencyMessage",
    "payPeriodFrequencyCode",
    "payPeriodFrequencyMessage",
    "annualizedIncome"
})
public class BaseCompensationInfoType
    extends ComplexObjectType
{

    @XmlElement(name = "PayRate")
    protected PayRateRestrictionType payRate;
    @XmlElement(name = "PayFrequencyCode")
    protected PayFrequencyCodeType payFrequencyCode;
    @XmlElement(name = "PayFrequencyMessage")
    protected PayFrequencyMessageCodeType payFrequencyMessage;
    @XmlElement(name = "PayPeriodFrequencyCode")
    protected PayPeriodFrequencyCodeType payPeriodFrequencyCode;
    @XmlElement(name = "PayPeriodFrequencyMessage")
    protected PayPeriodFrequencyMessageCodeType payPeriodFrequencyMessage;
    @XmlElement(name = "AnnualizedIncome")
    protected AmountRestrictionType annualizedIncome;

    /**
     * Gets the value of the payRate property.
     * 
     * @return
     *     possible object is
     *     {@link PayRateRestrictionType }
     *     
     */
    public PayRateRestrictionType getPayRate() {
        return payRate;
    }

    /**
     * Sets the value of the payRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayRateRestrictionType }
     *     
     */
    public void setPayRate(PayRateRestrictionType value) {
        this.payRate = value;
    }

    /**
     * Gets the value of the payFrequencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link PayFrequencyCodeType }
     *     
     */
    public PayFrequencyCodeType getPayFrequencyCode() {
        return payFrequencyCode;
    }

    /**
     * Sets the value of the payFrequencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayFrequencyCodeType }
     *     
     */
    public void setPayFrequencyCode(PayFrequencyCodeType value) {
        this.payFrequencyCode = value;
    }

    /**
     * Gets the value of the payFrequencyMessage property.
     * 
     * @return
     *     possible object is
     *     {@link PayFrequencyMessageCodeType }
     *     
     */
    public PayFrequencyMessageCodeType getPayFrequencyMessage() {
        return payFrequencyMessage;
    }

    /**
     * Sets the value of the payFrequencyMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayFrequencyMessageCodeType }
     *     
     */
    public void setPayFrequencyMessage(PayFrequencyMessageCodeType value) {
        this.payFrequencyMessage = value;
    }

    /**
     * Gets the value of the payPeriodFrequencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link PayPeriodFrequencyCodeType }
     *     
     */
    public PayPeriodFrequencyCodeType getPayPeriodFrequencyCode() {
        return payPeriodFrequencyCode;
    }

    /**
     * Sets the value of the payPeriodFrequencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayPeriodFrequencyCodeType }
     *     
     */
    public void setPayPeriodFrequencyCode(PayPeriodFrequencyCodeType value) {
        this.payPeriodFrequencyCode = value;
    }

    /**
     * Gets the value of the payPeriodFrequencyMessage property.
     * 
     * @return
     *     possible object is
     *     {@link PayPeriodFrequencyMessageCodeType }
     *     
     */
    public PayPeriodFrequencyMessageCodeType getPayPeriodFrequencyMessage() {
        return payPeriodFrequencyMessage;
    }

    /**
     * Sets the value of the payPeriodFrequencyMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link PayPeriodFrequencyMessageCodeType }
     *     
     */
    public void setPayPeriodFrequencyMessage(PayPeriodFrequencyMessageCodeType value) {
        this.payPeriodFrequencyMessage = value;
    }

    /**
     * Gets the value of the annualizedIncome property.
     * 
     * @return
     *     possible object is
     *     {@link AmountRestrictionType }
     *     
     */
    public AmountRestrictionType getAnnualizedIncome() {
        return annualizedIncome;
    }

    /**
     * Sets the value of the annualizedIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountRestrictionType }
     *     
     */
    public void setAnnualizedIncome(AmountRestrictionType value) {
        this.annualizedIncome = value;
    }

}
