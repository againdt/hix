<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<style>
#waiving-reason label:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}
</style>

<script type="text/javascript">

/**
 *  Waiver Modal
 */
 
$(document).ready(function(){

	$('input[name=optionsWaive]:radio').click(function(){
		
		if($(this).is(":checked") && $(this).val()=='Not covered by another plan and do not wish to enroll') {
			// Check if there is no existing alert message
			if ($('p#deny-insurance').length < 1){
				//Then add alert msg
	    		$('label[for=optionsWaive5]').after ('<p id="deny-insurance" class="alert-error"><small>Not having health insurance may subject you to tax penalities due to non-compliance with the individual mandate of the Affordable Care Act.</small></p>');
			}
		}
		// if the radio button other than value=option5 is clicked
		else {
		// Check if there is no existing alert message
			if ($('p#deny-insurance').length >= 1){
				$('p#deny-insurance').remove();
			}
		}
	});
	
});

function validateESignName(){
	var empName  = '${employee.name}';
	if($('#esignName').val().toUpperCase().replace(/\s+/g, '') != (empName.toUpperCase().replace(/\s+/g, ''))){
		alert('Employee ESignature should match Employee\'s firstname and lastname');
		return false;
	}else{
		return true;
	}
}

function validateDetails(){
	waiveOption = "";
	$(':radio[name="optionsWaive"]').each(function() {
		if(this.checked)	 
			waiveOption = this.value;
	});
	
	if(waiveOption !=""){
		var isChecked = $('#attestation').attr('checked') ? true : false;
		var isESign = validateESignName();
		if(isChecked == true && isESign == true){
				updateWaiveCoverage($("#employeeApplicationId").val(),waiveOption);
		}else if(isESign == true && isChecked ==false){
			alert("Please read and check the attestation.");
			return false;
		}else if(isESign ==false){
			return false;
		}
	}else{
		alert("Please select one reason.");	
		return false;
	}
	return false;
}

function updateWaiveCoverage(elem,waiveOption){
	if(waiveOption !=""){
		window.location.href ='/hix/shop/employee/waivecoverage/'+elem + '&SN='+waiveOption + '&ES=' + $("#esignName").val()+".";
	}
}

</script>

<div id="waive-emp-coverage" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;width:650px">
   <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     <h3 id="myModalLabel">Waive Employer Coverage</h3>
   </div>
                
   <div class="modal-body" style="height:320px">
	    <h4>Please select the reason for waiving employer-sponsered coverage.</h4>
        <div class="control-group" id="waiving-reason" >
           	<div class="controls">
            	 <label class="radio" for="optionsWaive1" >
				 <input type="radio" name="optionsWaive" id="optionsWaive1" value="Covered by another employer's plan" checked>
				I am covered by another employer's plan
				</label>
			</div>
			<div class="controls">
				<label class="radio" for="optionsWaive2">
				<input type="radio" name="optionsWaive" id="optionsWaive2" value="Covered by Individual Health Exchange plan">
				I am covered by an Individual Health Exchange plan
				</label>
			</div>
			<div class="controls">
				<label class="radio" for="optionsWaive3">
				<input type="radio" name="optionsWaive" id="optionsWaive3" value="Covered by government plan (e.g. Medicare)">
				I am covered by a Government plan (e.g. Medicare, Medicaid, etc.)
				</label>
			</div>
			<div class="controls">
				<label class="radio" for="optionsWaive4">
				<input type="radio" name="optionsWaive" id="optionsWaive4" value="Covered as dependent on someone else's plan">
				I am covered as a dependent under someone else's plan
				</label>
			</div>
			<div class="controls">
				<label class="radio" for="optionsWaive5">
				<input type="radio" name="optionsWaive" id="optionsWaive5" value="Not covered by another plan and do not wish to enroll">
				I am not covered by another plan and I choose not to have health insurance
				</label>
			</div>								
		</div> <!-- /waiving-reason -->						
		<div class="control-group" id="employee-esign">
			<label class="control-label pull-left" for="esignName">Employee's e-Signature <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10px" height="10px" alt="Required!" /></label>
			<div class="controls">
				<input type="text" class="input-xlarge" id="esignName" name="esignName" size="30" placeholder="John Doe">
				<p class="esign-text" style="margin-right: 55px;"><small>Type your full name here as your electronic signature.</small></p>
				<div id="esignName_error"></div>
			</div>
		</div>
		<div class="control-group clearfix" id="employee-esign-date">
			<label class="control-label pull-left" for="today_date">Today&#39;s Date</label>
			<div class="controls">
				<input type="text" readonly class="input-small" maxlength="8" id="today_date" name="today_date">
			</div>	
		</div>
		<div class="control-group" id="employee-attestation">
			<input type="checkbox" id="attestation" name ="attestation">
			<label  class="control-label pull-left" for="attestation">I attest that I wish to waive my employer provided coverage, and I understand that I will be unable to enroll until the next open enrollment period unless I experience a qualifying event</label>					
		</div>
	</div><!-- /modal-body -->
	<div class="modal-footer">
		<input type="hidden" name="employeeApplicationId" id="employeeApplicationId" value="${param.employeeApplicationId}"/>
	            			<button class="btn pull-left" data-dismiss="modal" aria-hidden="true">Cancel</button>						
	          			<button class="btn btn-primary pull-right" name="waiveSubmit" id="waiveSubmit" aria-hidden="true" alt="Submit" onclick="validateDetails();">Submit</button>
	</div>
</div><!-- /#waive-emp-coverage -->

<script type="text/javascript">
var d = new Date();
$('#today_date').val((d.getMonth()+1) +'/'+ d.getDate() + '/' + d.getFullYear());
</script>