import React, { Fragment } from 'react';

const IncomeItem = props => {

    const {
        amount,
        design,
        frequency,
        sourceName,
        type
    } = props;

    const getClassName = design => {
        if(design) {
            return `usa-width-one-third${design ? ` ${design}` : ''}`;
        } else {
            return 'usa-width-one-third';
        }
    };

    return (
        <div
            className={`usa-width-one-whole review-document${design ? ` ${design}` : ''}`}
        >
            <span className={getClassName(design)}>
                <span className={design ? '' : 'bold'}>
                    { type }
                </span>
                {
                    sourceName &&
                    <Fragment>
                        <span>
                           ({ sourceName })
                        </span>
                    </Fragment>
                }
            </span>
            <span className={getClassName(design)}>
                { amount }
            </span>
            <span className={getClassName(design)}>
                { frequency }
            </span>
        </div>
    );
};

export default IncomeItem;