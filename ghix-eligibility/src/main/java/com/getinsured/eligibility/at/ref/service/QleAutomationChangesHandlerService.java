package com.getinsured.eligibility.at.ref.service;



import java.util.Date;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.qlevalidation.QLEAutomationChangesHandlerDetermination;
import com.getinsured.eligibility.qlevalidation.QLEAutomationChangesHandlerDetermination.AllChangesHandlerType;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;


@Service("qleAutomationChangesHandlerService")
@Scope("singleton")
public class QleAutomationChangesHandlerService extends LceAllChangesHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(QleAutomationChangesHandlerService.class);

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;
	
	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired
	@Qualifier("enrollmentUpdateAdapter")
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;
	
	@Autowired private Gson platformGson;

	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("QleAutomationChangesHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		
		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId());
		
		final AllChangesHandlerType handlerToInvoke 
			= QLEAutomationChangesHandlerDetermination.handlerToInvoke(lceProcessRequestDTO.getEnrolledApplicationAttributes(), ssapApplicationEvent);
		
		LOGGER.info("Handler to invoke - " + handlerToInvoke);
		
		
		if (AllChangesHandlerType.SINGLE.equals(handlerToInvoke)) {
			LOGGER.info("Invoking case " + AllChangesHandlerType.SINGLE + " handler for " + currentApplication.getId());
			handleSingle(currentApplication, lceProcessRequestDTO);
		} 
		else if (AllChangesHandlerType.PURE_REMOVE.equals(handlerToInvoke)){
			LOGGER.info("Invoking case " + AllChangesHandlerType.PURE_REMOVE + " handler for " + currentApplication.getId());
			handlePureRemove(currentApplication, lceProcessRequestDTO);
		} else if (AllChangesHandlerType.CS_CHANGE.equals(handlerToInvoke)){
			LOGGER.info("Invoking case " + AllChangesHandlerType.CS_CHANGE + " handler for " + currentApplication.getId());
			handleCSAutomation(currentApplication, lceProcessRequestDTO);
		} 
		else {
			triggerSEPEmail(currentApplication, ssapApplicationEvent);
		}
		
		/**
		 * In MN, we need to support only dental enrollment. In such cases the application status is in ER and application dental status is EN.
		 * Whenever all changes handler is invoked and manual flow is determined, we We run into situation where 2 applications are in ER status. 
		 * Following method is called to specifically handle this scenario after 
		 * All changes handler and QLE handler execution completes.
		 * 
		 * */
		super.closePreviousERApplication(currentApplication,lceProcessRequestDTO);
	}
	
	private void triggerSEPEmail(SsapApplication currentApplication, SsapApplicationEvent ssapApplicationEvent) {
		if (StringUtils.equals(currentApplication.getFinancialAssistanceFlag(), ReferralConstants.Y)){
			if (StringUtils.equals(ssapApplicationEvent.getKeepOnly(), ReferralConstants.Y)){
				triggerSepEventKeepOnlyNotice(currentApplication);
			} else {
				triggerSEPEmail(currentApplication);
			}
		} 
	}
	
	@SuppressWarnings("unused")
	private void handleCSAutomation(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		final SsapApplication enrolledApplication = loadApplication(lceProcessRequestDTO.getEnrolledApplicationId());
		String previousDentalApplicationStatus = enrolledApplication.getApplicationDentalStatus();
		String previousApplicationStatus = enrolledApplication.getApplicationStatus();
		boolean isCSAutomationAllowed = isAddCase(currentApplication) ? super.isAutoAddAllowed(lceProcessRequestDTO) : super.isAutoCSChangeAllowed(lceProcessRequestDTO);
		if(isCSAutomationAllowed){
			Date coverageStartDate = enrollmentUpdateAdapter.getCoverageStartDate(currentApplication, ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
			final AptcUpdate aptcUpdate = populateAptcUpdateRequest(currentApplication, lceProcessRequestDTO.getEnrolledApplicationId(),coverageStartDate);
			String changePlan = "Y";
			if (aptcUpdate == null) {
				LOGGER.info("APTC redistribution failure - " + currentApplication.getId());
				processMixed(currentApplication, true, lceProcessRequestDTO);
			} else {
				final boolean status = ind71GCall(lceProcessRequestDTO.getEnrolledApplicationId(), lceProcessRequestDTO.getCurrentApplicationId(),currentApplication.getCoverageYear(), aptcUpdate,changePlan) ;
				if (status) {
					LOGGER.info("IND71G update successful for  " + currentApplication.getId());
					processCSSuccess(currentApplication.getId(), lceProcessRequestDTO.getEnrolledApplicationId());
					if(isAddCase(currentApplication) && "CA".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))) {
						triggerAutomationAddRemoveNotice(currentApplication.getCaseNumber());
					}else {
						triggerAutomationCSNotice(currentApplication.getCaseNumber());
					}
				} else {
					LOGGER.info("IND71G update failed for  " + currentApplication.getId());
					processCSFailure(currentApplication, lceProcessRequestDTO);
				}
				
			}
		}else {
			handleManual(currentApplication); //TODO: check is status == ER
		}
	}

	@SuppressWarnings("unused")
	private void handlePureRemove(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Handle " + AllChangesHandlerType.PURE_REMOVE + " case for " + currentApplication.getId());
		String changePlan = "N";
		Date coverageStartDate = enrollmentUpdateAdapter.getCoverageStartDate(currentApplication, ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		handleAutomation(currentApplication, lceProcessRequestDTO,coverageStartDate,changePlan);
	}

	

	private void handleSingle(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Handle " + AllChangesHandlerType.SINGLE + " case for " + currentApplication.getId());
		String changePlan = "Y";
		if(isAutoAddAllowed(lceProcessRequestDTO)){
			Date coverageStartDate = enrollmentUpdateAdapter.getCoverageStartDate(currentApplication, ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
			handleAutomation(currentApplication, lceProcessRequestDTO,coverageStartDate,changePlan);
		}
	}

	@SuppressWarnings("unused")
	private void handleMixed(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Handle " + AllChangesHandlerType.MIXED + " case for " + currentApplication.getId());
		processMixed(currentApplication, true, lceProcessRequestDTO);
	}

	@SuppressWarnings("unused")
	private void handleDenial(SsapApplication currentApplication) {
		LOGGER.info("Handle " + AllChangesHandlerType.DENIAL + " case for " + currentApplication.getId());
		processDenial(currentApplication.getCaseNumber());
	}

	@SuppressWarnings("unused")
	private void handleManual(SsapApplication currentApplication) {
		updateAllowEnrollment(currentApplication, Y);
		triggerUserActionEmail(currentApplication);
	}

	private void triggerUserActionEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger User Action Email - Notice LCE2");
		try {
			referralLceNotificationService.generateFinancialLCENotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE2").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());

		}
	}	
	
}
