package com.getinsured.ahbx.broker.service;

import com.getinsured.hix.model.ExternalEmployer;

/**
 * Encapsulates service layer method calls for External Employer 
 */
public interface ExternalEmployerService {
	
	/**
	 * Saves external employer when external employer bob detail is fetched from AHBX
	 * 
	 * @param externalEmployer
	 * @return ExternalEmployer
	 */
	ExternalEmployer save(ExternalEmployer externalEmployer);
	
	/**
	 * Finds external employer based on employer case id(same as external employer id)
	 * 
	 * @param externalEmpId
	 * @return ExternalEmployer
	 * 
	 */
	ExternalEmployer findByExternalEmployerId(long externalEmpId);
	
	/**
	 * Updates external employer when external employer bob status is fetched from AHBX
	 * 
	 * @param externalEmployer
	 * @return ExternalEmployer
	 */
	ExternalEmployer update(ExternalEmployer externalEmployer);
}