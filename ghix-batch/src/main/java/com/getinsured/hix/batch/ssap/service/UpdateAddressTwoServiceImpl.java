package com.getinsured.hix.batch.ssap.service;

import com.getinsured.hix.enrollment.service.AdminUpdateService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service("updateAddressTwoService")
public class UpdateAddressTwoServiceImpl implements UpdateAddressTwoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAddressTwoServiceImpl.class);
    private static final String RESPONSE_SUCCESS = "SUCCESS";

    @Autowired
    private GhixRestTemplate restTemplate;

    @Autowired
    private AdminUpdateService adminUpdateService;

    @Autowired
    private SsapApplicationRepository ssapApplicaitonRepository;
    
    @Autowired
    private SsapApplicantRepository ssapApplicantRepository;

    @Override
    public void processUpdateAddressTwo(String ssapAppIdsFilePath, String absPathWithNameAndExt) throws GIException {
        try {
            Path path = Paths.get(absPathWithNameAndExt);
            String absolutePath = path.getParent().toString() + File.separator + "applicationProcessStatus.csv";
            FileReader filereader = new FileReader(ssapAppIdsFilePath);
            FileWriter fileWriter = new FileWriter(absolutePath);
            CSVReader csvReader = new CSVReader(filereader);
            CSVWriter csvWriter = new CSVWriter(fileWriter);
            String[] nextRecord;

            // Reading data line by line if no delimiter is specified
            while ((nextRecord = csvReader.readNext()) != null) {
                for (String record : nextRecord) {
                    try {
                        if (record != null && !record.isEmpty()) {
                            Long ssapAppId = Long.valueOf(record);
                            SsapApplication ssapApplication = ssapApplicaitonRepository.findOne(ssapAppId);

                            String url = GhixEndPoints.ELIGIBILITY_URL + "/application/updateAddressTwo/" + ssapAppId + "?path=" + absPathWithNameAndExt;
                            String response = restTemplate.getForObject(url, String.class);

                            if (RESPONSE_SUCCESS.equals(response)) {
                                LOGGER.debug("processUpdateAddressTwo::update address two was success for app id {}", ssapAppId);
                                if (ssapApplication != null) {
                                    List<SsapApplicant> applicants =  ssapApplicantRepository.findBySsapApplicationId(ssapApplication.getId());
                                    if (RESPONSE_SUCCESS.equals(adminUpdateService.updateAddressTwo(ssapApplication.getCmrHouseoldId().longValue(), applicants))) {
                                        LOGGER.debug("processUpdateAddressTwo::successful update of enrollments for app id {}", ssapAppId);
                                        csvWriter.writeNext(new String[]{ record, "SUCCESS"});
                                    }
                                } else {
                                    LOGGER.error("processUpdateAddressTwo::ssap app or applicants list was null for id {}", ssapAppId);
                                    csvWriter.writeNext(new String[]{ record, "FAILURE" });
                                }
                            } else {
                                LOGGER.error("processUpdateAddressTwo::call to updateAddressTwo failed for app id {}", ssapAppId);
                                csvWriter.writeNext(new String[]{ record, "FAILURE" });
                            }
                        } else {
                            LOGGER.error("processUpdateAddressTwo::could not parse line into long for ssap app id {}", record);
                            csvWriter.writeNext(new String[]{ record, "FAILURE" });
                        }
                    } catch (Exception ex) {
                        LOGGER.error("processUpdateAddressTwo::error updating address two for ssapAppId {}", record, ex);
                        csvWriter.writeNext(new String[]{ record, "FAILURE" });
                    }
                }
            }

            try {
                csvReader.close();
                csvWriter.close();
            } catch (IOException ioe) {
                LOGGER.error("processUpdateAddressTwo::error closing csv reader/writer", ioe);
            }
        } catch (Exception ex) {
            LOGGER.error("processUpdateAddressTwo::error processing ssap apps to update address two", ex);
        }
    }
}
