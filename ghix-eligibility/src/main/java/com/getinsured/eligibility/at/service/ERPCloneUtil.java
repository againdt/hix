package com.getinsured.eligibility.at.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.model.SsapVerification;
import com.getinsured.iex.ssap.model.SsapVerificationGiwspayload;
import com.getinsured.iex.ssap.repository.IConsumerDocumentRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapVerificationRepository;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.iex.util.SsapUtil;

@Component("erpCloneUtil")
public class ERPCloneUtil {

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private SsapApplicantEventRepository ssapApplicantEventRepository;

	@Autowired private SsapVerificationRepository ssapVerificationRepository;
	@Autowired private IConsumerDocumentRepository cmrDocument;

	@Autowired private SsapJsonBuilder ssapJsonBuilder;
	@Autowired private SsapUtil ssapUtil;
	
	private final String SSAPAPPLICANT_GUID_KEY = "SSAP_APPLICANT_DISPLAY_ID";
	private final String SSAPAPPLICATION_GUID_KEY = "SSAP_APPLICATION_DISPLAY_ID";

	private static final Logger LOGGER = Logger.getLogger(ERPCloneUtil.class);

	@Transactional(rollbackFor = Exception.class)
	public SsapApplication clone(SsapApplication ssapApplication) {

		SsapApplication clonedApp = ssapApplicationRepository.findOne(ssapApplication.getId());

		List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(clonedApp);
		List<SsapApplicant> originalSsapApplicants = ssapApplicantRepository.findBySsapApplication(clonedApp);
		List<SsapApplicationEvent> ssapAppEvents = ssapApplicationEventRepository.findBySsapApplication(clonedApp);

		/* modify */

		clonedApp.setApplicationStatus(ApplicationStatus.UNCLAIMED.getApplicationStatusCode());
		clonedApp.setEligibilityStatus(EligibilityStatus.PE);
		clonedApp.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);
		clonedApp.setCaseNumber(ssapUtil.getNextSequenceFromDB(SSAPAPPLICATION_GUID_KEY));
		clonedApp.setSsapApplicationEvents(null);
		//clonedApp.setParentSsapApplicationId(clonedApp.getId());
		clonedApp.setId(0);
		/* update json */

		List<SsapApplicant> clonedSsapApplicants = new ArrayList<>();
		List<SsapApplicantEvent> ssapApplicantEventsAll = new ArrayList<>();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			long applicantID = ssapApplicant.getId();

			List<SsapVerification> verifications = ssapVerificationRepository.getVerifications(applicantID);
			List<SsapApplicantEvent> ssapApplicantEvents = ssapApplicantEventRepository.findBySsapApplicant(ssapApplicant);

			SsapApplicant clonedApplicant = ssapApplicant;
			clonedApplicant.setId(0);
			clonedApplicant.setApplicantGuid(ssapUtil.getNextSequenceFromDB(SSAPAPPLICANT_GUID_KEY));
			clonedApplicant.setSsapApplication(clonedApp);

			// handle verifications for each applicant...
			for (SsapVerification ssapVerification : verifications) {
				ssapVerification.setId(0);
				ssapVerification.setSsapApplicant(clonedApplicant);
				List<SsapVerificationGiwspayload> verificationGiWsPayloads = ssapVerification.getSsapVerificationGiwspayloads();
				for (SsapVerificationGiwspayload ssapVerificationGiwspayload : verificationGiWsPayloads) {
					ssapVerificationGiwspayload.setId(0);
					ssapVerificationGiwspayload.setSsapVerification(ssapVerification);
				}
				ssapVerification.setSsapVerificationGiwspayloads(verificationGiWsPayloads);
			}
			clonedApplicant.setSsapVerifications(verifications);

			// handle ApplicantEvent for each applicant...
			for (SsapApplicantEvent ssapApplicantEvent : ssapApplicantEvents) {
				ssapApplicantEvent.setId(0);
				ssapApplicantEvent.setSsapApplicant(clonedApplicant);
				// for each ApplicantEvent, there is an associated SsapApplicationEvent... as of here there is no information of new SsapApplicationEvent
				// So delay setting of cloned SsapApplicationEvent and maintain original SsapApplicationEvent...
				// and refresh SsapAplicationEvent down..
			}
			clonedApplicant.setSsapApplicantEvents(ssapApplicantEvents);
			ssapApplicantEventsAll.addAll(ssapApplicantEvents); // collect all ApplicantEvent for this cloned app

			clonedSsapApplicants.add(clonedApplicant);
		}

		for (SsapApplicationEvent appEvent : ssapAppEvents) {
			// refresh SsapAplicationEvent
			for (SsapApplicantEvent ssapApplicantEvent : ssapApplicantEventsAll) {
				if (ssapApplicantEvent.getSsapAplicationEvent().getId() == appEvent.getId()){
					ssapApplicantEvent.setSsapAplicationEvent(appEvent);
				}
			}
			appEvent.setId(0);
			appEvent.setSsapApplication(clonedApp);
		}
		clonedApp.setSsapApplicationEvents(ssapAppEvents);

		clonedApp.setSsapApplicants(clonedSsapApplicants);
		clonedApp = ssapApplicationRepository.save(clonedApp);

		/*Handle CMR Documents*/
		List<ConsumerDocument> cmrDocuments = new ArrayList<>();
		for (SsapApplicant ssapApplicant : originalSsapApplicants) {

			for (SsapApplicant clonedApplicant : clonedApp.getSsapApplicants()) {
				if (ssapApplicant.getPersonId() == clonedApplicant.getPersonId()){
					List<ConsumerDocument> cmrDocumentsApplicant = cmrDocument.findByTargetNameAndTargetId("SSAP_APPLICANTS", ssapApplicant.getId());
					for (ConsumerDocument consumerDocument : cmrDocumentsApplicant) {
						consumerDocument.setId(null);
						consumerDocument.setTargetId(clonedApplicant.getId());
					}
					cmrDocuments.addAll(cmrDocumentsApplicant);
					break;
				}
			}

		}

		if (cmrDocuments.size() > 0){
			cmrDocument.save(cmrDocuments);
		}

		clonedApp = updateJson(clonedApp);

		return clonedApp;
	}

	private SsapApplication updateJson(SsapApplication clonedApp) {
		List<SsapApplication> updateApps = ssapApplicationRepository.findByAppId(clonedApp.getId());
		if (updateApps != null && updateApps.size() > 0){
			clonedApp = updateApps.get(0);
		} else {
			throw new GIRuntimeException("No Ssap Application found!");
		}
		String clonedJson = ssapJsonBuilder.modifyJsonForClonedApp(clonedApp);
		clonedApp.setApplicationData(clonedJson);
		return ssapApplicationRepository.save(clonedApp);
	}

}
