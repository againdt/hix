package com.getinsured.hix.planmgmt.service.jpa;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.RatingArea;
import com.getinsured.hix.planmgmt.repository.RatingAreaRepository;
import com.getinsured.hix.planmgmt.service.RatingAreaService;
import com.getinsured.hix.platform.util.SecurityUtil;

@Service("ratingAreaService")
@Repository
@Transactional
public class RatingAreaServiceImpl implements RatingAreaService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RatingAreaServiceImpl.class);
	@Autowired private RatingAreaRepository ratingAreaRepository;

	@Override
	public RatingArea getByRatingAreaID(String ratingArea) {
		RatingArea returningRatingArea = null;
		LOGGER.debug("ratingAreaIDForRatingArea: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingArea)));
		returningRatingArea = ratingAreaRepository.getRatingAreaByName(ratingArea.trim());
		if (returningRatingArea != null) {
			LOGGER.debug("returningRatingAreaFound: " + SecurityUtil.sanitizeForLogging(String.valueOf(returningRatingArea.getRatingArea())));
			return returningRatingArea;
		} else {
			return null;
		}
	}

	@Override
	public RatingArea getByRatingAreaAndState(@NotNull String ratingArea, @NotNull String state) {
		RatingArea returningRatingArea = null;
		returningRatingArea = ratingAreaRepository.getRatingAreaByNameAndState(ratingArea.trim(),state.trim());
		if (returningRatingArea != null) {
			return returningRatingArea;
		} else {
			LOGGER.warn("Not found for input ratind area : "+ SecurityUtil.sanitizeForLogging(String.valueOf(ratingArea)) +" and state :"+ SecurityUtil.sanitizeForLogging(String.valueOf(state)));
			return null;
		}
	}

}