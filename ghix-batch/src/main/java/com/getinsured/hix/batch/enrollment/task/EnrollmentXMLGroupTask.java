package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.enrollment.service.GroupInstallationService;
import com.getinsured.hix.platform.util.exception.GIException;

public class EnrollmentXMLGroupTask implements Tasklet {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentXMLGroupTask.class);
	private GroupInstallationService groupInstallationService;
	private String regenerate;
	private String employer_Id;
	private String employer_enrollment_Id;
	private String issuer_Id;
	private String groupAction;
	
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws GIException {
		LOGGER.info("EnrollmentXMLGroupTask.execute : START :: ");
		try{
		groupInstallationService.getGroupInstallationXML(getRegenerate(),getEmployer_enrollment_Id(),getIssuer_Id(), getGroupAction());
		}catch(Exception e){
			LOGGER.error("enrollmentXMLGroupJob :: failed to execute"+ e.toString(),e);
			throw new GIException("enrollmentXMLGroupJob :: failed to execute"+ e.toString(), e);
		}
		LOGGER.info("EnrollmentXMLGroupTask.execute : END :: ");
		return RepeatStatus.FINISHED;
	}

	public GroupInstallationService getGroupInstallationService() {
		return groupInstallationService;
	}

	public void setGroupInstallationService(
			GroupInstallationService groupInstallationService) {
		this.groupInstallationService = groupInstallationService;
	}

	public String getRegenerate() {
		return regenerate;
	}

	public void setRegenerate(String regenerate) {
		this.regenerate = regenerate;
	}

	public String getEmployer_Id() {
		return employer_Id;
	}

	public void setEmployer_Id(String employer_Id) {
		this.employer_Id = employer_Id;
	}

	public String getIssuer_Id() {
		return issuer_Id;
	}

	public void setIssuer_Id(String issuer_Id) {
		this.issuer_Id = issuer_Id;
	}

	public String getGroupAction() {
		return groupAction;
	}

	public void setGroupAction(String groupAction) {
		this.groupAction = groupAction;
	}

	public String getEmployer_enrollment_Id() {
		return employer_enrollment_Id;
	}

	public void setEmployer_enrollment_Id(String employer_enrollment_Id) {
		this.employer_enrollment_Id = employer_enrollment_Id;
	}

	
}
