package com.getinsured.hix.broker.service;
import com.getinsured.hix.model.ExternalIndividual;

/**
 * Encapsulates service layer method calls for External Individual
 */
public interface ExternalIndividualService {

	/**
	 * Finds external individual based on individual case id
	 * 
	 * @see ExternalIndividualService#findByExternalIndividualID(long)
	 * 
	 * @param externalIndId
	 * @return ExternalIndividual
	 * 
	 */

	ExternalIndividual findByExternalIndividualID(long externalIndId);
}
