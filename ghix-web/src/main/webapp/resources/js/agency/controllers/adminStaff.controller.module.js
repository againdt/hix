(function() {
'use strict';

angular.module('adminStaffCtrlModule', ['translateApp'])
.controller('AdminStaffCtrl', AdminStaffCtrl);

AdminStaffCtrl.$inject = ['$http', '$stateParams', '$location', '$scope', '$state', 'dataStorageService', '$translate', 'StatusService'];

    function AdminStaffCtrl($http, $stateParams, $location, $scope, $state, dataStorageService, $translate, StatusService) {
        var vm = this;
        vm.init = init;
        vm.agencyCertificationStatus = StatusService.CertificationStatus;
        vm.currentUserRole = StatusService.CurrentUserRole;
        vm.isAdminRole = StatusService.IsAdminRole;
        vm.saveData = saveData;
        vm.getData = getData;
        vm.cancel = cancel;
        vm.edit = edit;
        vm.nextPage = nextPage;
        vm.setAssistantRole = setAssistantRole;
        vm.getSelectedLocation = getSelectedLocation;
        vm.editMode = false;
        vm.primaryPhoneprimaryPhone1 = vm.primaryPhone2 = vm.primaryPhone3 = '';
        vm.businessPhone1 = vm.businessPhone2 = vm.businessPhone3 = '';
        vm.registrationFlow = true;
        vm.adminStaffData = {};

        function init() {
            if ((vm.agencyCertificationStatus === 'PENDING' || vm.agencyCertificationStatus === 'SUSPENDED' || vm.agencyCertificationStatus === 'TERMINATED') && vm.currentUserRole === 'AGENCY_MANAGER') {
                $state.go("viewagentlist");
            }
            dataStorageService.setItem('adminStaffRole', true);
            vm.agencyId =  $("#agencyId").val() || dataStorageService.getItem('agencyId');
            vm.isNewAdminStaffRegistrationFlow = $location.search().newAdminStaffRegistration;
            if (vm.isNewAdminStaffRegistrationFlow === undefined) {
                vm.isNewAdminStaffRegistrationFlow = dataStorageService.getItem('isNewAdminStaffRegistrationFlow');
            }
            vm.registrationComplete = $stateParams.registrationComplete || dataStorageService.getItem('registrationComplete');
            if(vm.registrationComplete) {
                dataStorageService.setItem('registrationComplete', vm.registrationComplete);
            }
            if($stateParams.viewAdminStaffFlow){
                vm.viewAdminStaffFlow = $stateParams.viewAdminStaffFlow;
            } else {
                if(vm.isNewAdminStaffRegistrationFlow) {
                    vm.viewAdminStaffFlow = false;
                } else {
                    vm.viewAdminStaffFlow = dataStorageService.getItem('viewAdminStaffFlow');
                }
            }
            vm.showLocationAddressFlag = false;
            vm.preferredCommunicationList = ['Phone','Email','Mail'];
            vm.assistantRoleList = ['Level1', 'Level2'];
            vm.businessAddress = [];
            vm.flowFlag = {};

            $scope.$watch(function(){
                return $location.search();
            }, function(newParams, oldParams){
                if (newParams.newAdminStaffRegistration && newParams !== oldParams) {
                    $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
                    vm.assistantId = null
                    vm.isNewAdminStaffRegistrationFlow = true;
                    vm.businessLegalName = vm.adminStaffData.businessLegalName;
                    getData();
                    vm.adminStaffData = {};
                    vm.adminStaffData.businessLegalName = vm.businessLegalName;
                    vm.primaryPhone1 = vm.primaryPhone2 = vm.primaryPhone3 = '';
                    vm.businessPhone1 = vm.businessPhone2 = vm.businessPhone3 = '';
                    vm.businessAddress = [];
                    vm.showLocationAddressFlag =false;
                    updateFlowFlag();
                }
            }, true);
            getData();
            updateFlowFlag();
        }
        function setAssistantRole() {
            if(vm.isNewAdminStaffRegistrationFlow) {
                vm.assistantRole = 'Level1';
            } else {
                vm.assistantRole = vm.adminStaffData.assistantRole;
            }
            return vm.assistantRole;
        }
        function getData() {
            if(vm.isNewAdminStaffRegistrationFlow) {
                vm.assistantId = null;
                var promise = $http.get('/hix/agency/staff/information/view');
            } else {
                vm.assistantId = $stateParams.assistantId || dataStorageService.getItem('assistantId');

                var promise = $http.get('/hix/agency/staff/information/view/' + vm.assistantId);
            }
            promise.success(function(response){
                vm.loader = false;
                vm.adminStaffData = response;
                dataStorageService.setItem('assistantId', vm.adminStaffData.id);
                dataStorageService.setItem('viewAdminStaffFlow', vm.viewAdminStaffFlow);
                vm.communicationPref = vm.adminStaffData.communicationPref;
                if(vm.adminStaffData.assistantRole === null) {
                    vm.adminStaffData.assistantRole = 'Level1';
                } else {
                    vm.assistantRole = vm.adminStaffData.assistantRole;
                }
                if(vm.adminStaffData.primaryContactNumber) {
                    vm.primaryPhone1 = vm.adminStaffData.primaryContactNumber.substring(0, 3);
                    vm.primaryPhone2 = vm.adminStaffData.primaryContactNumber.substring(3, 6);
                    vm.primaryPhone3 = vm.adminStaffData.primaryContactNumber.substring(6, 10);
                } else {
                    vm.primaryPhone1 = vm.primaryPhone2 = vm.primaryPhone3 = '';
                }
                if(vm.adminStaffData.businessContactNumber) {
                    vm.businessPhone1 = vm.adminStaffData.businessContactNumber.substring(0, 3);
                    vm.businessPhone2 = vm.adminStaffData.businessContactNumber.substring(3, 6);
                    vm.businessPhone3 = vm.adminStaffData.businessContactNumber.substring(6, 10);
                } else {
                    vm.businessPhone1 = vm.businessPhone2 = vm.businessPhone3 = '';
                }
                if (vm.adminStaffData.businessAddress !== null) {
                    if(Object.keys(vm.adminStaffData.businessAddress).length > 0 ) {
                        if (vm.adminStaffData.businessAddress.id) {
                                angular.forEach(vm.adminStaffData.agencySites, function (value, key) {
                                    if (value.location.id === vm.adminStaffData.businessAddress.id) {
                                        vm.adminStaffData.agencySites.siteLocationName = value.siteLocationName;
                                        vm.selectedSiteId = value.id;
                                        vm.showLocationAddressFlag = true;
                                        vm.businessAddress.push(value.location);
                                    }
                                });
                            }
                    }
                }
                if (vm.adminStaffData.correspondenceAddress !== null) {
                    if(Object.keys(vm.adminStaffData.correspondenceAddress).length > 0 ) {
                        if (vm.adminStaffData.correspondenceAddress.id) {
                            vm.adminStaffData.correspondenceAddress.addressLine1 = vm.adminStaffData.correspondenceAddress.address1;
                            vm.adminStaffData.correspondenceAddress.addressLine2 = vm.adminStaffData.correspondenceAddress.address2;
                            vm.adminStaffData.correspondenceAddress.zipcode = vm.adminStaffData.correspondenceAddress.zip;
                        }
                    }
                }
            })
            promise.error(function(response){
                vm.loader = false;
            })
        }

          function getSelectedLocation(loactionName, loactionId) {
              vm.selectedLocation = loactionName;
              vm.businessAddress = [];
              angular.forEach(vm.adminStaffData.agencySites, function (value, key) {
                  if (value.siteLocationName === vm.selectedLocation) {
                      vm.businessAddress.push(value.location);
                      vm.selectedSiteId = value.id;
                      vm.showLocationAddressFlag = true;
                  }
              });
          }
          vm.siteId = vm.selectedSiteId;
          function saveData() {
          	vm.loader = true;
          	vm.editMode = false;
              var primaryContactNumber = '';
              var businessContactNumber = '';
              vm.selectedStaffLevel = vm.adminStaffData.staffLevel;
              vm.adminStaffData.businessAddress = {};
              if(vm.primaryPhone1 !== undefined && vm.primaryPhone2 !== undefined && vm.primaryPhone3 !== undefined){
                  primaryContactNumber = "" + vm.primaryPhone1 + vm.primaryPhone2 + vm.primaryPhone3;
              }
              if(vm.businessPhone1 !== undefined && vm.businessPhone2 !== undefined && vm.businessPhone3 !== undefined){
                  businessContactNumber = "" + vm.businessPhone1 + vm.businessPhone2 + vm.businessPhone3;
              }
              //vm.adminStaffData.agencySites.siteLocationName
              var data = {
                  id: vm.assistantId || vm.adminStaffData.id,
                  firstName: vm.adminStaffData.firstName,
                  lastName: vm.adminStaffData.lastName,
                  primaryContactNumber: primaryContactNumber,
                  businessContactNumber: businessContactNumber,
                  personalEmailAddress: vm.adminStaffData.personalEmailAddress,
                  businessEmailAddress: vm.adminStaffData.businessEmailAddress,
                  communicationPref: vm.communicationPref,
                  businessLegalName: vm.adminStaffData.businessLegalName,
                  businessAddress : {
                      id: vm.businessAddress[0].id,
                      address1: vm.businessAddress[0].address1,
                      address2: vm.businessAddress[0].address2,
                      city: vm.businessAddress[0].city,
                      state: vm.businessAddress[0].state,
                      zip: vm.businessAddress[0].zip,
                      lat: vm.businessAddress[0].lat,
                      lon: vm.businessAddress[0].lon
                  },
                  correspondenceAddress : {
                      id: null || vm.adminStaffData.correspondenceAddress.id,
                      address1: vm.adminStaffData.correspondenceAddress.addressLine1,
                      address2: vm.adminStaffData.correspondenceAddress.addressLine2,
                      city: vm.adminStaffData.correspondenceAddress.city,
                      state: vm.adminStaffData.correspondenceAddress.state,
                      zip: vm.adminStaffData.correspondenceAddress.zipcode,
                      lat: vm.adminStaffData.correspondenceAddress.lat,
                      lon: vm.adminStaffData.correspondenceAddress.lon
                  },
                  assistantRole: vm.assistantRole,
                  agencyId: vm.agencyId || vm.adminStaffData.agencyId,
                  siteId: vm.selectedSiteId
              };

              var promise = $http.post('/hix/agency/staff/information', data);
              promise.success(function(response){
                  vm.loader = false;
                  vm.editMode = false;
                  vm.adminStaffData = response;
                  if(response.statusCode === '200'){
                      dataStorageService.setItem('isNewAdminStaffRegistrationFlow', false);
                      dataStorageService.setItem('registrationComplete', true);
                      vm.responseId = response.id;
                      if(vm.isAdminRole || !vm.isNewAdminStaffRegistrationFlow) {
                           $state.go ("adminstaff.addstaff", {assistantId: vm.responseId, registrationComplete: true });
                      } else {
                          $('#successfullyCompletedApplication').modal();
                      }
                  }
              })
              promise.error(function(response){
                  vm.loader = false;
              })
          }
          function cancel() {
              vm.editMode = !vm.editMode;
              vm.getData();
          }
          function nextPage() {
              $state.go ("adminstaff.approvalstatus", {assistantId: vm.responseId, registrationComplete: true });
              $('#successfullyCompletedApplication').modal('hide');
          }

          function updateFlowFlag(){

              //for admin flow
              var isAdminRole = $("#isAdminRole").val();

              //for agency flow
              var certificationStatus = $('#agencyCertificationStatus').val();

              if (isAdminRole === 'true') {
                  vm.flowFlag.adminFlow = true;
                  vm.editMode = false;
                  $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
                  $scope.$parent.vm.sideNavListFour[1].disableLink = false;
                  $scope.$parent.vm.sideNavListFour[2].disableLink = false;
              }
              if (vm.currentUserRole === 'AGENCY_MANAGER') {
                  vm.flowFlag.adminFlow = false;
                  vm.editMode = true;
                  if (vm.registrationComplete && !vm.isNewAdminStaffRegistrationFlow) {
                      vm.editMode = false;
                      if(vm.viewAdminStaffFlow) {
                          $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
                      } else {
                          $scope.$parent.vm.sideNavListFour[0].showOKicon = true;
                      }

                      $scope.$parent.vm.sideNavListFour[1].disableLink = false;
                      $scope.$parent.vm.sideNavListFour[2].disableLink = false;
                  } else {
                      vm.editMode = true;
                      $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
                      $scope.$parent.vm.sideNavListFour[1].disableLink = true;
                      $scope.$parent.vm.sideNavListFour[2].disableLink = true;
                  }
              }
              if (vm.currentUserRole === 'APPROVEDADMINSTAFFL1' || vm.currentUserRole === 'APPROVEDADMINSTAFFL2') {
                  vm.flowFlag.adminFlow = false;
                  vm.editMode = false;
                  $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
                  $scope.$parent.vm.sideNavListFour[1].disableLink = false;
                  $scope.$parent.vm.sideNavListFour[2].disableLink = false;
              }
          }

          function edit(){
              vm.editMode = !vm.editMode;
              vm.adminStaffDataCopy = angular.copy(vm.adminStaffData);
          }

      }

  })();
