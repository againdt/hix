 <!--
 ~ Copyright (c) 2005-2014, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 ~
 ~ WSO2 Inc. licenses this file to you under the Apache License,
 ~ Version 2.0 (the "License"); you may not use this file except
 ~ in compliance with the License.
 ~ You may obtain a copy of the License at
 ~
 ~    http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied.  See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
 -->

<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.Constants" %>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.net.URI" %>
<%@ page import="java.net.URISyntaxException" %>
<%@ page import="java.util.Enumeration" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%!
public String getHost(String url) {
	if (url == null || url.isEmpty()) {
		return null;
	}
	try {
		return new URI(url).getHost();
	} catch (URISyntaxException ex) {
		return null;
	}
}
%>

<%
	//YHI Aliases
	Map<String,String> serviceProviderUrlLookup = new HashMap<String,String>();
	serviceProviderUrlLookup.put("YHIDEMO","https://iddemo.ghixqa.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHI_QA","https://idqa.ghixqa.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHI_QA2","https://idqa.getinsured.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHISIT","https://idsit.ghixqa.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHIUAT","https://iduat.getinsured.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHIE2E1","https://ide2e.ghixqa.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHICARRIER","https://idcarrier.getinsured.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHIQA_STAGE","https://idqa-stage.ghixqa.com/hix/account/user/login");
	serviceProviderUrlLookup.put("IDSTAGE","https://idstage.ghixtest.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHI","https://idahohix.yourhealthidaho.org/hix/account/user/login");
	serviceProviderUrlLookup.put("YHISEC","https://yhisec.getinsured.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHIPWW_DEV","https://idssodev.ghixqa.com/hix/account/user/login");
	serviceProviderUrlLookup.put("IDSSODEV","https://idssodev.ghixqa.com/hix/account/user/login");
	serviceProviderUrlLookup.put("YHISSODEV","https://idssodev.ghixqa.com/hix/account/user/login");
	serviceProviderUrlLookup.put("IDALINK","https://idalink.idaho.gov/login");
	serviceProviderUrlLookup.put("IDALINK-DEV","https://idalinkd.dhw.state.id.us/login");
	serviceProviderUrlLookup.put("IDALINK-I","https://idalinki.dhw.state.id.us/login");
	serviceProviderUrlLookup.put("IDALINKQUAL","https://idalinkq.dhw.state.id.us/login");
	serviceProviderUrlLookup.put("IDALINKQF","https://idalinkqf.dhw.state.id.us/login");
	serviceProviderUrlLookup.put("YHIDEV","https://localhost:8080/hix/");
	serviceProviderUrlLookup.put("IDALINKEZRA","http://idalink-local:8080/login");

	Map<String,String[]> defaultUrlLookup = new HashMap<String,String[]>();
	defaultUrlLookup.put("localhost", new String[] {"YHISSODEV", "IDALINKEZRA"});
	defaultUrlLookup.put("sbmssod", new String[] {"IDALINK-I","YHISSODEV","YHISIT"});
	defaultUrlLookup.put("sbmssoq", new String[] {"IDSTAGE", "IDALINKQUAL","YHIE2E1"});
	defaultUrlLookup.put("sbmsso", new String[] {"YHI", "IDALINK"});

	String serverName = CharacterEncoder.getSafeText(request.getServerName());
	String[] splitServerName = serverName.split("\\.", 2);
	if (splitServerName.length > 1) {
		serverName = splitServerName[0];
	}

	String[] defaultUrls = defaultUrlLookup.get(serverName);
	if (defaultUrls == null) {
		defaultUrls = defaultUrlLookup.get("sbmsso");
	}
	
	String relyingParty = null;

	//Determine relyingParty from referrer.
    String referrerHost = getHost(CharacterEncoder.getSafeText(request.getHeader("Referer")));
	if (referrerHost != null) {
		for (String key : serviceProviderUrlLookup.keySet()) {
			String knownHost = getHost(serviceProviderUrlLookup.get(key));
			if (referrerHost.equalsIgnoreCase(knownHost)) {
				relyingParty = key;
				break;
			}
		}
	}

	//Determine relyingParty from request parameters
	if (relyingParty == null) {
		relyingParty = CharacterEncoder.getSafeText(request.getParameter("relyingParty"));
	}

	//Determine relyingParty from session attribute
	if (relyingParty == null && session.getAttribute("relyingParty") != null) {
         relyingParty = session.getAttribute("relyingParty").toString();
    }

	//Determine relyingParty from cookie
	if (relyingParty == null) {
		Cookie[] cookies = request.getCookies();
		if( cookies != null ){
			for (Cookie cookie : cookies){
				if ("relyingParty".equals(CharacterEncoder.getSafeText(cookie.getName()))) {
					relyingParty = CharacterEncoder.getSafeText(cookie.getValue());
					break;
				}
			}
		}
	}

	String errorCode = CharacterEncoder.getSafeText(request.getParameter("errorCode"));
	String redirectUrl = null;
	
	//Check for relying party
	if(relyingParty != null){
		relyingParty = relyingParty.toUpperCase();
		redirectUrl = serviceProviderUrlLookup.get(relyingParty);
		//Relying party is something else other than YHI
		if(redirectUrl == null){
			redirectUrl = "https://idalink.idaho.gov/login";
		}
	}
	
	//Account locked - Redirect to login page
	if (errorCode != null && errorCode.contains("17003")) {
		String requestUrl = request.getQueryString();
		response.sendRedirect("login.do?"+requestUrl+"&authenticators=BasicAuthenticator:LOCAL");
	}
	
    String stat = CharacterEncoder.getSafeText((String) request.getParameter(Constants.STATUS));
	if (stat == null || stat.isEmpty()) {
        stat = "Authentication Error";
	}
    String statusMessage = CharacterEncoder.getSafeText( (String) request.getParameter(Constants.STATUS_MSG));
    if (statusMessage == null || statusMessage.isEmpty()){
        statusMessage = "Something went wrong with the authentication process.";
    }
    
    session.invalidate();
%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<title><%=stat%></title>
		<style>

@font-face {
    font-family: 'Gudea';
    src: url('assets_idalink/fonts/gudea-regular-webfont.eot');
    src: url('assets_idalink/fonts/gudea-regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('assets_idalink/fonts/gudea-regular-webfont.woff') format('woff'),
         url('assets_idalink/fonts/gudea-regular-webfont.ttf') format('truetype'),
         url('assets_idalink/fonts/gudea-regular-webfont.svg#gudearegular') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'Gudea';
    src: url('assets_idalink/fonts/gudea-bold-webfont.eot');
    src: url('assets_idalink/fonts/gudea-bold-webfont.eot?#iefix') format('embedded-opentype'),
         url('assets_idalink/fonts/gudea-bold-webfont.woff') format('woff'),
         url('assets_idalink/fonts/gudea-bold-webfont.ttf') format('truetype'),
         url('assets_idalink/fonts/gudea-bold-webfont.svg#gudeabold') format('svg');
    font-weight: bold;
    font-style: normal;

}

* {
	margin: 0;
	padding: 0;
	font-family: 'Gudea', sans-serif;
}

.header{
	border-bottom: 2px solid #999999;
	min-height: 60px;
	text-align: right;
	background: #f5f5f5;
	padding-right: 50px;
}

.content{
	max-width: 870px;
	margin: 20px auto;
}

h1{
	color: #489c12;
	font-weight: normal;
	font-size: 24px;
	margin-bottom: 20px;
	line-height: 30px;
}

p {
	margin-bottom: 15px;
	line-height: 18px;
}

.generic-button{
	border: 0px;
	color:white;
	height: 33px;
	width: 130px;
	font-size: 15px;
	font-weight: bold;
	font-family: sans-serif;
	text-shadow: none;
	background: #3d8fb7;
	background: -moz-linear-gradient(top, #3d8fb7 0%, #3d8fb7 50%, #004f8a 51%, #004f8a 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#3d8fb7), color-stop(50%,#3d8fb7), color-stop(51%,#004f8a), color-stop(100%,#004f8a)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #3d8fb7 0%,#3d8fb7 50%,#004f8a 51%,#004f8a 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #3d8fb7 0%,#3d8fb7 50%,#004f8a 51%,#004f8a 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #3d8fb7 0%,#3d8fb7 50%,#004f8a 51%,#004f8a 100%); /* IE10+ */
	background: linear-gradient(to bottom, #3d8fb7 0%,#3d8fb7 50%,#004f8a 51%,#004f8a 100%); /* W3C */
	-webkit-border-radius: 5px; 
	border-radius: 5px; 
}

.yhi-button {
	border: 0px;
	background: #0081a0;
	border-radius: 5px; 
	color:white;
	height: 33px;
	width: 130px;
	font-size: 15px;
	font-weight: bold;
	font-family: sans-serif;
	text-shadow: none;
}

.idalink-button {
	border: 0px;
	color:white;
	height: 33px;
	width: 130px;
	font-size: 15px;
	font-weight: bold;
	font-family: sans-serif;
	text-shadow: none;
	background: #3d8fb7;
	background: -moz-linear-gradient(top, #92b63a 0%, #92b63a 50%, #548a00 51%, #548a00 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#92b63a), color-stop(50%,#92b63a), color-stop(51%,#548a00), color-stop(100%,#548a00)); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, #92b63a 0%,#92b63a 50%,#548a00 51%,#548a00 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, #92b63a 0%,#92b63a 50%,#548a00 51%,#548a00 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, #92b63a 0%,#92b63a 50%,#548a00 51%,#548a00 100%); /* IE10+ */
	background: linear-gradient(to bottom, #92b63a 0%,#92b63a 50%,#548a00 51%,#548a00 100%); /* W3C */
	-webkit-border-radius: 5px; 
	border-radius: 5px; 
}

input[type='submit'], button{
	cursor: pointer;
	line-height: 33px;
}

input[type='submit'] span, button span {
	background: url(assets_idalink/img/next.png) no-repeat center center;
	display: inline-block;
	margin-left: 5px;
	height: 18px;
	width: 17px;
	top: 3px;
	position: relative;

}

.logos {
	max-width: 7000px;
	margin: 20px auto;
	clear:both;
}



.logos form {
	float: left;
	width: 49%;
	text-align: center;
	padding: 20px 0;
}

.yhi-form {
	border-right: 1px solid #999999;
}


		</style>
	</head>
	<body>
		<div class="header">
			<img src="assets_idalink/img/logos.jpg" alt="Your Health Idaho / idalink" />
		</div>

		<div class="content">
			<h1><%=stat%></h1>
			<p><%=statusMessage%></p>

		<% if (relyingParty == null) { %>
		<div class="logos">
			<!-- Option B, one button to YHI, one button to idalink -->
			<form action="<%=serviceProviderUrlLookup.get(defaultUrls[0])%>" class="yhi-form">
				<p><img src="assets_idalink/img/yhi-retry.jpg" alt="Your Health Idaho" /></p>
				<p><button class="yhi-button">LOGIN</button></p>
			</form>
			<form action="<%=serviceProviderUrlLookup.get(defaultUrls[1])%>">
				<p><img src="assets_idalink/img/idalink-retry.jpg" alt="idalink" /></p>
				<p><button class="idalink-button">Login<span></span></button></p>

			</form>
		</div>
		<% } else { %>
			<!-- Option A, one dynamic button sending you to wherever you came from -->
			<form action="<%=redirectUrl%>">
				<p>
					<button class="generic-button">Try Again <span></span></button>
				</p>
			</form>
		<% } %>
	</body>
</html>
