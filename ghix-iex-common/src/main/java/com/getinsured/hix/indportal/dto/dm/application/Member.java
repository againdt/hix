package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;

public class Member {
	 private String personTrackingNumber;
	 private String indianStatus;
	 private String indianStatusReason;
	 private String nonEscMecStatus;
	 private String nonEscMecStatusReason;
	 private String qhpStatus;
	 private String qhpStatusReason;
	 private String ssnStatus;
	 private String ssnStatusReason;
	 private String medicaidLawfulPresenceStatus;
	 private String medicaidLawfulPresenceStatusReason;
	 private Boolean under100FplWithLawfulPresenceDmiIndicator;
	 private String aptcStatus;
	 private String aptcStatusReason;
	 private Boolean secretaryHardshipExemptionIndicator;
	 private String citizenshipStatus;
	 private String citizenshipStatusReason;
	 private String citizenshipDataEventStatus;
	 private Boolean citizenshipDataFoundIndicator;
	 private String preliminaryMedicaidStatus;
	 private String preliminaryMedicaidStatusReason;
	 private String preliminaryChipStatus;
	 private String preliminaryChipStatusReason;
	 private String preliminaryAptcStatus;
	 private String preliminaryAptcStatusReason;
	 private String preliminaryEmergencyMedicaidStatus;
	 private String preliminaryEmergencyMedicaidStatusReason;
	 private String dependentChildCoveredStatus;
	 private String dependentChildCoveredStatusReason;
	 private String chipStateHealthBenefitStatus;
	 private String chipStateHealthBenefitStatusReason;
	 private String chipWaitingPeriodStatus;
	 private String chipWaitingPeriodStatusReason;
	 private String emergencyMedicaidStatus;
	 private String emergencyMedicaidStatusReason;
	 private String unbornChildChipStatus;
	 private String unbornChildChipStatusReason;
	 private String transferApplicantToStateStatus;
	 private String transferApplicantToStateStatusReason;
	 private String qhpLawfulPresenceStatus;
	 private String qhpLawfulPresenceStatusReason;
	 private String csrStatus;
	 private String csrStatusReason;
	 private String csrVariant;
	 private String medicaidStatus;
	 private String medicaidStatusReason;
	 private String chipStatus;
	 private String chipStatusReason;
	 private MedicaidChipStandard medicaidChipStandard;
	 private String escMecStatus;
	 private String escMecStatusReason;
	 private String incarcerationStatus;
	 private String incarcerationStatusReason;
	 private Income income;
	 private MedicaidHouseholdComposition medicaidHouseholdComposition;
	 private String medicaidNonMagiReferralStatus;
	 private String medicaidNonMagiReferralStatusReason;
	 private QhpResidency qhpResidency;
	 private String medicaidResidencyStatus;
	 private String medicaidResidencyStatusReason;
	 private ArrayList < Object > eligibleSEP;
	 private String fiveYearBarStatus;
	 private String fiveYearBarStatusReason;
	 private ResidencyAddress residencyAddress;
	 


	 // Getter Methods 

	 public String getPersonTrackingNumber() {
	  return personTrackingNumber;
	 }

	 public String getIndianStatus() {
	  return indianStatus;
	 }

	 public String getIndianStatusReason() {
	  return indianStatusReason;
	 }

	 public String getNonEscMecStatus() {
	  return nonEscMecStatus;
	 }

	 public String getNonEscMecStatusReason() {
	  return nonEscMecStatusReason;
	 }

	 public String getQhpStatus() {
	  return qhpStatus;
	 }

	 public String getQhpStatusReason() {
	  return qhpStatusReason;
	 }

	 public String getSsnStatus() {
	  return ssnStatus;
	 }

	 public String getSsnStatusReason() {
	  return ssnStatusReason;
	 }

	 public String getMedicaidLawfulPresenceStatus() {
	  return medicaidLawfulPresenceStatus;
	 }

	 public String getMedicaidLawfulPresenceStatusReason() {
	  return medicaidLawfulPresenceStatusReason;
	 }

	 public Boolean getUnder100FplWithLawfulPresenceDmiIndicator() {
	  return under100FplWithLawfulPresenceDmiIndicator;
	 }

	 public String getAptcStatus() {
	  return aptcStatus;
	 }

	 public String getAptcStatusReason() {
	  return aptcStatusReason;
	 }

	 public Boolean getSecretaryHardshipExemptionIndicator() {
	  return secretaryHardshipExemptionIndicator;
	 }

	 public String getCitizenshipStatus() {
	  return citizenshipStatus;
	 }

	 public String getCitizenshipStatusReason() {
	  return citizenshipStatusReason;
	 }

	 public String getCitizenshipDataEventStatus() {
	  return citizenshipDataEventStatus;
	 }

	 public String getPreliminaryMedicaidStatus() {
	  return preliminaryMedicaidStatus;
	 }

	 public String getPreliminaryMedicaidStatusReason() {
	  return preliminaryMedicaidStatusReason;
	 }

	 public String getPreliminaryChipStatus() {
	  return preliminaryChipStatus;
	 }

	 public String getPreliminaryChipStatusReason() {
	  return preliminaryChipStatusReason;
	 }

	 public String getPreliminaryAptcStatus() {
	  return preliminaryAptcStatus;
	 }

	 public String getPreliminaryAptcStatusReason() {
	  return preliminaryAptcStatusReason;
	 }

	 public String getPreliminaryEmergencyMedicaidStatus() {
	  return preliminaryEmergencyMedicaidStatus;
	 }

	 public String getPreliminaryEmergencyMedicaidStatusReason() {
	  return preliminaryEmergencyMedicaidStatusReason;
	 }

	 public String getDependentChildCoveredStatus() {
	  return dependentChildCoveredStatus;
	 }

	 public String getDependentChildCoveredStatusReason() {
	  return dependentChildCoveredStatusReason;
	 }

	 public String getChipStateHealthBenefitStatus() {
	  return chipStateHealthBenefitStatus;
	 }

	 public String getChipStateHealthBenefitStatusReason() {
	  return chipStateHealthBenefitStatusReason;
	 }

	 public String getChipWaitingPeriodStatus() {
	  return chipWaitingPeriodStatus;
	 }

	 public String getChipWaitingPeriodStatusReason() {
	  return chipWaitingPeriodStatusReason;
	 }

	 public String getEmergencyMedicaidStatus() {
	  return emergencyMedicaidStatus;
	 }

	 public String getEmergencyMedicaidStatusReason() {
	  return emergencyMedicaidStatusReason;
	 }

	 public String getUnbornChildChipStatus() {
	  return unbornChildChipStatus;
	 }

	 public String getUnbornChildChipStatusReason() {
	  return unbornChildChipStatusReason;
	 }

	 public String getQhpLawfulPresenceStatus() {
	  return qhpLawfulPresenceStatus;
	 }

	 public String getQhpLawfulPresenceStatusReason() {
	  return qhpLawfulPresenceStatusReason;
	 }

	 public String getCsrStatus() {
	  return csrStatus;
	 }

	 public String getCsrStatusReason() {
	  return csrStatusReason;
	 }

	 public String getCsrVariant() {
	  return csrVariant;
	 }

	 public String getMedicaidStatus() {
	  return medicaidStatus;
	 }

	 public String getMedicaidStatusReason() {
	  return medicaidStatusReason;
	 }

	 public String getChipStatus() {
	  return chipStatus;
	 }

	 public String getChipStatusReason() {
	  return chipStatusReason;
	 }

	 public MedicaidChipStandard getMedicaidChipStandard() {
	  return medicaidChipStandard;
	 }

	 public String getEscMecStatus() {
	  return escMecStatus;
	 }

	 public String getEscMecStatusReason() {
	  return escMecStatusReason;
	 }

	 public String getIncarcerationStatus() {
	  return incarcerationStatus;
	 }

	 public String getIncarcerationStatusReason() {
	  return incarcerationStatusReason;
	 }

	 public Income getIncome() {
	  return income;
	 }

	 public MedicaidHouseholdComposition getMedicaidHouseholdComposition() {
	  return medicaidHouseholdComposition;
	 }

	 public String getMedicaidNonMagiReferralStatus() {
	  return medicaidNonMagiReferralStatus;
	 }

	 public String getMedicaidNonMagiReferralStatusReason() {
	  return medicaidNonMagiReferralStatusReason;
	 }

	 public QhpResidency getQhpResidency() {
	  return qhpResidency;
	 }

	 public String getMedicaidResidencyStatus() {
	  return medicaidResidencyStatus;
	 }

	 public String getMedicaidResidencyStatusReason() {
	  return medicaidResidencyStatusReason;
	 }

	 public String getFiveYearBarStatus() {
	  return fiveYearBarStatus;
	 }

	 public String getFiveYearBarStatusReason() {
	  return fiveYearBarStatusReason;
	 }

	 public ResidencyAddress getResidencyAddress() {
	  return residencyAddress;
	 }

	 // Setter Methods 

	 public void setPersonTrackingNumber(String personTrackingNumber) {
	  this.personTrackingNumber = personTrackingNumber;
	 }

	 public void setIndianStatus(String indianStatus) {
	  this.indianStatus = indianStatus;
	 }

	 public void setIndianStatusReason(String indianStatusReason) {
	  this.indianStatusReason = indianStatusReason;
	 }

	 public void setNonEscMecStatus(String nonEscMecStatus) {
	  this.nonEscMecStatus = nonEscMecStatus;
	 }

	 public void setNonEscMecStatusReason(String nonEscMecStatusReason) {
	  this.nonEscMecStatusReason = nonEscMecStatusReason;
	 }

	 public void setQhpStatus(String qhpStatus) {
	  this.qhpStatus = qhpStatus;
	 }

	 public void setQhpStatusReason(String qhpStatusReason) {
	  this.qhpStatusReason = qhpStatusReason;
	 }

	 public void setSsnStatus(String ssnStatus) {
	  this.ssnStatus = ssnStatus;
	 }

	 public void setSsnStatusReason(String ssnStatusReason) {
	  this.ssnStatusReason = ssnStatusReason;
	 }

	 public void setMedicaidLawfulPresenceStatus(String medicaidLawfulPresenceStatus) {
	  this.medicaidLawfulPresenceStatus = medicaidLawfulPresenceStatus;
	 }

	 public void setMedicaidLawfulPresenceStatusReason(String medicaidLawfulPresenceStatusReason) {
	  this.medicaidLawfulPresenceStatusReason = medicaidLawfulPresenceStatusReason;
	 }

	 public void setUnder100FplWithLawfulPresenceDmiIndicator(Boolean under100FplWithLawfulPresenceDmiIndicator) {
	  this.under100FplWithLawfulPresenceDmiIndicator = under100FplWithLawfulPresenceDmiIndicator;
	 }

	 public void setAptcStatus(String aptcStatus) {
	  this.aptcStatus = aptcStatus;
	 }

	 public void setAptcStatusReason(String aptcStatusReason) {
	  this.aptcStatusReason = aptcStatusReason;
	 }

	 public void setSecretaryHardshipExemptionIndicator(Boolean secretaryHardshipExemptionIndicator) {
	  this.secretaryHardshipExemptionIndicator = secretaryHardshipExemptionIndicator;
	 }

	 public void setCitizenshipStatus(String citizenshipStatus) {
	  this.citizenshipStatus = citizenshipStatus;
	 }

	 public void setCitizenshipStatusReason(String citizenshipStatusReason) {
	  this.citizenshipStatusReason = citizenshipStatusReason;
	 }

	 public void setCitizenshipDataEventStatus(String citizenshipDataEventStatus) {
	  this.citizenshipDataEventStatus = citizenshipDataEventStatus;
	 }

	 public void setPreliminaryMedicaidStatus(String preliminaryMedicaidStatus) {
	  this.preliminaryMedicaidStatus = preliminaryMedicaidStatus;
	 }

	 public void setPreliminaryMedicaidStatusReason(String preliminaryMedicaidStatusReason) {
	  this.preliminaryMedicaidStatusReason = preliminaryMedicaidStatusReason;
	 }

	 public void setPreliminaryChipStatus(String preliminaryChipStatus) {
	  this.preliminaryChipStatus = preliminaryChipStatus;
	 }

	 public void setPreliminaryChipStatusReason(String preliminaryChipStatusReason) {
	  this.preliminaryChipStatusReason = preliminaryChipStatusReason;
	 }

	 public void setPreliminaryAptcStatus(String preliminaryAptcStatus) {
	  this.preliminaryAptcStatus = preliminaryAptcStatus;
	 }

	 public void setPreliminaryAptcStatusReason(String preliminaryAptcStatusReason) {
	  this.preliminaryAptcStatusReason = preliminaryAptcStatusReason;
	 }

	 public void setPreliminaryEmergencyMedicaidStatus(String preliminaryEmergencyMedicaidStatus) {
	  this.preliminaryEmergencyMedicaidStatus = preliminaryEmergencyMedicaidStatus;
	 }

	 public void setPreliminaryEmergencyMedicaidStatusReason(String preliminaryEmergencyMedicaidStatusReason) {
	  this.preliminaryEmergencyMedicaidStatusReason = preliminaryEmergencyMedicaidStatusReason;
	 }

	 public void setDependentChildCoveredStatus(String dependentChildCoveredStatus) {
	  this.dependentChildCoveredStatus = dependentChildCoveredStatus;
	 }

	 public void setDependentChildCoveredStatusReason(String dependentChildCoveredStatusReason) {
	  this.dependentChildCoveredStatusReason = dependentChildCoveredStatusReason;
	 }

	 public void setChipStateHealthBenefitStatus(String chipStateHealthBenefitStatus) {
	  this.chipStateHealthBenefitStatus = chipStateHealthBenefitStatus;
	 }

	 public void setChipStateHealthBenefitStatusReason(String chipStateHealthBenefitStatusReason) {
	  this.chipStateHealthBenefitStatusReason = chipStateHealthBenefitStatusReason;
	 }

	 public void setChipWaitingPeriodStatus(String chipWaitingPeriodStatus) {
	  this.chipWaitingPeriodStatus = chipWaitingPeriodStatus;
	 }

	 public void setChipWaitingPeriodStatusReason(String chipWaitingPeriodStatusReason) {
	  this.chipWaitingPeriodStatusReason = chipWaitingPeriodStatusReason;
	 }

	 public void setEmergencyMedicaidStatus(String emergencyMedicaidStatus) {
	  this.emergencyMedicaidStatus = emergencyMedicaidStatus;
	 }

	 public void setEmergencyMedicaidStatusReason(String emergencyMedicaidStatusReason) {
	  this.emergencyMedicaidStatusReason = emergencyMedicaidStatusReason;
	 }

	 public void setUnbornChildChipStatus(String unbornChildChipStatus) {
	  this.unbornChildChipStatus = unbornChildChipStatus;
	 }

	 public void setUnbornChildChipStatusReason(String unbornChildChipStatusReason) {
	  this.unbornChildChipStatusReason = unbornChildChipStatusReason;
	 }

	 public void setQhpLawfulPresenceStatus(String qhpLawfulPresenceStatus) {
	  this.qhpLawfulPresenceStatus = qhpLawfulPresenceStatus;
	 }

	 public void setQhpLawfulPresenceStatusReason(String qhpLawfulPresenceStatusReason) {
	  this.qhpLawfulPresenceStatusReason = qhpLawfulPresenceStatusReason;
	 }

	 public void setCsrStatus(String csrStatus) {
	  this.csrStatus = csrStatus;
	 }

	 public void setCsrStatusReason(String csrStatusReason) {
	  this.csrStatusReason = csrStatusReason;
	 }

	 public void setCsrVariant(String csrVariant) {
	  this.csrVariant = csrVariant;
	 }

	 public void setMedicaidStatus(String medicaidStatus) {
	  this.medicaidStatus = medicaidStatus;
	 }

	 public void setMedicaidStatusReason(String medicaidStatusReason) {
	  this.medicaidStatusReason = medicaidStatusReason;
	 }

	 public void setChipStatus(String chipStatus) {
	  this.chipStatus = chipStatus;
	 }

	 public void setChipStatusReason(String chipStatusReason) {
	  this.chipStatusReason = chipStatusReason;
	 }

	 public void setMedicaidChipStandard(MedicaidChipStandard medicaidChipStandardObject) {
	  this.medicaidChipStandard = medicaidChipStandardObject;
	 }

	 public void setEscMecStatus(String escMecStatus) {
	  this.escMecStatus = escMecStatus;
	 }

	 public void setEscMecStatusReason(String escMecStatusReason) {
	  this.escMecStatusReason = escMecStatusReason;
	 }

	 public void setIncarcerationStatus(String incarcerationStatus) {
	  this.incarcerationStatus = incarcerationStatus;
	 }

	 public void setIncarcerationStatusReason(String incarcerationStatusReason) {
	  this.incarcerationStatusReason = incarcerationStatusReason;
	 }

	 public void setIncome(Income incomeObject) {
	  this.income = incomeObject;
	 }

	 public void setMedicaidHouseholdComposition(MedicaidHouseholdComposition medicaidHouseholdCompositionObject) {
	  this.medicaidHouseholdComposition = medicaidHouseholdCompositionObject;
	 }

	 public void setMedicaidNonMagiReferralStatus(String medicaidNonMagiReferralStatus) {
	  this.medicaidNonMagiReferralStatus = medicaidNonMagiReferralStatus;
	 }

	 public void setMedicaidNonMagiReferralStatusReason(String medicaidNonMagiReferralStatusReason) {
	  this.medicaidNonMagiReferralStatusReason = medicaidNonMagiReferralStatusReason;
	 }

	 public void setQhpResidency(QhpResidency qhpResidencyObject) {
	  this.qhpResidency = qhpResidencyObject;
	 }

	 public void setMedicaidResidencyStatus(String medicaidResidencyStatus) {
	  this.medicaidResidencyStatus = medicaidResidencyStatus;
	 }

	 public void setMedicaidResidencyStatusReason(String medicaidResidencyStatusReason) {
	  this.medicaidResidencyStatusReason = medicaidResidencyStatusReason;
	 }

	 public void setFiveYearBarStatus(String fiveYearBarStatus) {
	  this.fiveYearBarStatus = fiveYearBarStatus;
	 }

	 public void setFiveYearBarStatusReason(String fiveYearBarStatusReason) {
	  this.fiveYearBarStatusReason = fiveYearBarStatusReason;
	 }

	 public void setResidencyAddress(ResidencyAddress residencyAddressObject) {
	  this.residencyAddress = residencyAddressObject;
	 }

	public String getTransferApplicantToStateStatus() {
		return transferApplicantToStateStatus;
	}

	public void setTransferApplicantToStateStatus(String transferApplicantToStateStatus) {
		this.transferApplicantToStateStatus = transferApplicantToStateStatus;
	}

	public String getTransferApplicantToStateStatusReason() {
		return transferApplicantToStateStatusReason;
	}

	public void setTransferApplicantToStateStatusReason(String transferApplicantToStateStatusReason) {
		this.transferApplicantToStateStatusReason = transferApplicantToStateStatusReason;
	}
	
	public ArrayList<Object> getEligibleSEP() {
		return eligibleSEP;
	}

	public void setEligibleSEP(ArrayList<Object> eligibleSEP) {
		this.eligibleSEP = eligibleSEP;
	}

	public Boolean getCitizenshipDataFoundIndicator() {
		return citizenshipDataFoundIndicator;
	}

	public void setCitizenshipDataFoundIndicator(Boolean citizenshipDataFoundIndicator) {
		this.citizenshipDataFoundIndicator = citizenshipDataFoundIndicator;
	}
	}