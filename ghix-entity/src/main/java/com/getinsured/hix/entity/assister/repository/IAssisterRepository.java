package com.getinsured.hix.entity.assister.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EnrollmentEntity;

/**
 * Spring-JPA repository that encapsulates methods that interact with the Assister
 * table
 */
public interface IAssisterRepository extends JpaRepository<Assister, Integer>, RevisionRepository<Assister, Integer, Integer>{
	/**
	 * Fetches the Assister record against the given Assister Id
	 * 
	 * @param assisterId
	 *            the Assister id
	 * @return the Assister record against the Assister Id
	 */
	Assister findById(int assisterId);
	
	/**
	 * Fetches the list of Assisters for the given Enrollment Entity
	 * 
	 * @param entity
	 *            the given Enrollment Entity
	 * @return the list of Assisters against the Enrollment Entity
	 */
	List<Assister> findByEntity(EnrollmentEntity entity);
	
	/**
	 * Fetches the Assister for the given User
	 * 
	 * @param userId
	 *            the given User
	 * @return the Assister against the given User
	 */
	@Query("SELECT assister from Assister assister, "
			+ "ModuleUser mu where assister.id = mu.moduleId "
			+ "and upper(mu.moduleName) = upper('assister') and mu.user.id =  :userId")
	Assister findByUserId(@Param("userId") Integer userId); 
	
	/**
	 * Fetches the Assister for the given User
	 * 
	 * @param userId
	 *            the given User
	 * @return the Assister against the given User
	 */
	@Query("SELECT assister from Assister assister, "
			+ "ModuleUser mu where assister.id = mu.moduleId "
			+ "and upper(mu.moduleName) = upper('assister') and mu.user.id =  :userId")
	Assister findAssisterByUserId(@Param("userId") Integer userId);
	
	@Query("FROM Assister a where a.user.id = :userId")
	Assister findAssisterRecordByUserId(@Param("userId") Integer userId);
	
	/**
	 * Fetches the list of Assisters for the given Enrollment Entity
	 * 
	 * @param entityId
	 *            the given Enrollment Entity Id
	 * @return the list of Assisters against the Enrollment Entity
	 */
	@Query("FROM Assister a where a.entity.id = :entityId")
	List<Assister> findByEntityId(@Param("entityId") Integer entityId);
	
	/**
	 * Fetches the list of Assisters for the given Enrollment Entity and Site
	 * 
	 * @param entityId
	 *            the given Enrollment Entity Id
	 * @param siteId
	 *            the given Site Id          
	 * @return the list of Assisters against the Enrollment Entity and Site
	 */
	@Query("FROM Assister a where a.entity.id = :entityId and a.certificationStatus = 'Certified' and a.status = 'Active' and (a.primaryAssisterSite.id = :siteId or a.secondaryAssisterSite.id = :siteId)")
	List<Assister> findByEntityIdAndSiteId(@Param("entityId") Integer entityId, @Param("siteId") Integer siteId);
	
	/**
	 * 
	 * @param emailAddress
	 * @return
	 */
	List<Assister> findByEmailAddress(String emailAddress);
	
	@Query(nativeQuery=true,
			value= "  SELECT a.id, a.first_name, a.last_name, e.entity_name, a.assister_number, a.recertification_date,"
					+ "	a.certification_status, l.address1, l.address2, l.city, l.state, l.zip, a.primary_phone_number, a.email_address,"
					+ "a.communication_pref,count(CASE WHEN d.status = 'Pending' THEN assister_id END) as pending_designations,"
					+ "count(CASE WHEN d.status = 'Active' THEN assister_id END) as active_designations,"
					+ "count(CASE WHEN d.status = 'Inactive' THEN assister_id END) as inactive_designations FROM ee_assisters a "
					+ "LEFT JOIN ee_locations l on l.id = a.mailing_location_id "
					+ "LEFT JOIN ee_designate_assisters d on d.assister_id = a.id "
					+ "LEFT JOIN ee_entities e on e.id = a.ee_entity_id"
					+ "	GROUP BY a.id, a.first_name, a.last_name, e.entity_name, a.assister_number, a.recertification_date, "
					+ "a.certification_status, l.address1, l.address2, l.city, l.state, l.zip, a.primary_phone_number, "
					+ "a.email_address,	a.communication_pref ORDER BY a.id asc")
	List<Object[]> getAssisterExportList();
}
