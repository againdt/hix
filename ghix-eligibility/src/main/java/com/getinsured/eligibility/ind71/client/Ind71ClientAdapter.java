package com.getinsured.eligibility.ind71.client;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.benchmark.service.BenchmarkPlanService;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.ind19.util.service.EnrollmentsExtractionService;
import com.getinsured.eligibility.model.SepEvents.ChangeType;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.SoapHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.DisenrollMembers;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.DisenrollMembers.DisenrollMember;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.DisenrollMembers.DisenrollMember.Enrollments;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member.Relationship;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.ResponsiblePerson;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalResponse;
import com.getinsured.hix.webservice.plandisplay.autorenewal.EnrollmentVal;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ObjectFactory;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ProductType;
import com.getinsured.hix.webservice.plandisplay.autorenewal.RoleTypeVal;
import com.getinsured.hix.webservice.plandisplay.autorenewal.YesNoVal;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.LanguageEnum;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.iex.util.SsapUtil;

@Component("ind71ClientAdapter")
public class Ind71ClientAdapter {

	private static final Logger LOGGER = Logger.getLogger(Ind71ClientAdapter.class);
	
	private static final String EXCEPTION = "Exception - ";
	private static final String NEW_LINE = "\n";

	private static final String PRIMARY_PERSON_ID = "1";
	private static final String PRIMARY_INFORMATION_NOT_FOUND = "Primary information not found!";
	private static final String NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION = "No Application Events found for application!";
	private static final String EMPTY_RESPONSE_FROM_IND71_API = "Empty response from IND71 API";
	private static final String UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID = "Unable to find application for ssapApplicationId - ";
	
	private static final String THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR = "There are no enrolled SSAPs for this household for coverage year - ";
	private static final String EXISTING_MEDICAL_ENROLLMENT_COULD_NOT_BE_RETRIEVED_FOR_THE_APPLICATION_WITH_CASENUMBER = "Existing medical enrollment could not be retrieved for the application with casenumber ";
	private static final String GETTING_ENROLLMENT_DETAILS_FOR_LINKED_ENROLLED_APPLICATION = "Getting enrollment details for linked enrolled application - ";
	private static final String ERROR_COMPUTING_COVERAGE_START_DATE_CURRENT_APPLICATION = "Error computing coverage start date current application - ";

	private static final String RESPONSE_CODE = "Response Code";

	private static final String US_CITIZEN = "1";
	private static final String NON_US_CITIZEN = "3";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private CoverageStartDateService coverageStartDateService;

	@Autowired
	private EnrollmentsExtractionService enrollmentsExtractionService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	private IntegrationLogService integrationLogService;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private BenchmarkPlanService benchmarkPlanService;

	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;

	@Autowired Ind71WebClient ind71WebClient;
	
	@Autowired private SsapUtil ssapUtil;
	
	@Autowired private AptcHistoryRepository aptcHistoryRepository;
	
	

	public String execute(Long ssapApplicationId, List<String> healthEnrollees, List<String> dentalEnrollees) {

		StringBuilder finalPayload = new StringBuilder();

		String status = GhixConstants.RESPONSE_FAILURE;
		AutoRenewalRequest request = null;
		try {
			request = formRequest(ssapApplicationId, healthEnrollees, dentalEnrollees);
			AutoRenewalResponse response = ind71WebClient.invoke(request);
			if (response == null) {
				finalPayload.append(EMPTY_RESPONSE_FROM_IND71_API);
				throw new GIRuntimeException(EMPTY_RESPONSE_FROM_IND71_API);
			}
			if ("200".equalsIgnoreCase(response.getResponseCode())) {
				status = GhixConstants.RESPONSE_SUCCESS;
			} else {
				/* Donot throw exception as we have persisted events */			
			}
			finalPayload.append(NEW_LINE).append(RESPONSE_CODE).append(response.getResponseCode());
			
		} catch (Exception e) {
			if (request != null){
				try {
					String requestPayload = SoapHelper.marshal(request);
					finalPayload.append(NEW_LINE).append("Request - ").append(requestPayload);
				} catch (JAXBException e1) {
					/* Donot throw exception as we have persisted events. This is only for logging invalid request */
				}
			}
			finalPayload.append(NEW_LINE).append(EXCEPTION).append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		} finally {
			logData(ssapApplicationId, finalPayload.toString(), status);
		}

		return status;
	}


	private AutoRenewalRequest formRequest(Long ssapApplicationId, List<String> healthEnrollees, List<String> dentalEnrollees) {

		SsapApplication currentApplication = validate(ssapApplicationId);

		List<String> enrollees = new ArrayList<String>();
		if (CollectionUtils.isNotEmpty(healthEnrollees)) {
			enrollees.addAll(healthEnrollees);
		}
		if(CollectionUtils.isNotEmpty(dentalEnrollees)) {
			enrollees.addAll(dentalEnrollees);
		}
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = getEnrollmentDetails(currentApplication, enrollees);
		Date coverageDate = computeCoverageStartDate(currentApplication.getId(), enrollmentsByTypeMap);

		EnrollmentShopDTO healthEnrollmentShopDTO = enrollmentsByTypeMap.get(HEALTH);

		if(healthEnrollmentShopDTO != null && ReferralUtil.isCoverageDatePastEnrollmentDate(coverageDate, healthEnrollmentShopDTO.getCoverageEndDate())) {
			throw new GIRuntimeException(ReferralConstants.COVERAGE_DATE_IS_PAST_ENROLLMENT_DATE + ReferralConstants.COVERAGE_DATE_STR_VAL + coverageDate + ReferralConstants.CURRENT_ENROLLMENT_END_DATE_STR_VAL + healthEnrollmentShopDTO.getCoverageEndDate());
		}

		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();

		SsapApplicant responsiblePersonApplicantDB = ssapUtil.getPrimaryTaxFilerApplicantFromApplication(currentApplication);
		HouseholdMember responsiblePersonApplicantJson = ssapUtil.getApplicantJson(members, responsiblePersonApplicantDB);

		
		SsapApplicant householdContactApplicantDB = ssapUtil.getPrimaryContactApplicantFromApplication(currentApplication);
		HouseholdMember householdContactApplicantJson = ssapUtil.getApplicantJson(members, householdContactApplicantDB);
		
		
//		SsapApplicant householdContactApplicantDB = ssapUtil.getPrimaryTaxFilerApplicantFromApplication(currentApplication);
//		HouseholdMember householdContactApplicantJson = ssapUtil.getApplicantJson(members, householdContactApplicantDB);
//		
		if (responsiblePersonApplicantDB == null || responsiblePersonApplicantJson == null) {
			throw new GIRuntimeException(PRIMARY_INFORMATION_NOT_FOUND);
		}

		Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId());
		if (ssapApplicationEvent == null) {
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}

		// populate applicant event map to draw enrollment ADD, TERM, OTHER sections
		Map<String, SsapApplicantEvent> ssapApplicantEventMap = new HashMap<String, SsapApplicantEvent>();
		List<SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();
		for (SsapApplicantEvent ssapApplicantEvent : applicantEvents) { 
			ssapApplicantEventMap.put(ssapApplicantEvent.getSsapApplicant().getApplicantGuid(), ssapApplicantEvent);
		}

		Household household = cmrHouseholdRepository.findOne(currentApplication.getCmrHouseoldId().intValue());

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.ADD.name().equals(applicantEvent.getSepEvents().getChangeType())) {

				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						newlyEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				newlyEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (!ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.REMOVE.name().equals(applicantEvent.getSepEvents().getChangeType())) {

				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						newlyIneigibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				newlyIneligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		Map<String, SsapApplicant> changeEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.OTHER.name().equals(applicantEvent.getSepEvents().getChangeType())) {
				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						changeEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				changeEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		// HLT
		AutoRenewalRequest request;
		try {
			request = populateHealthEnrollment(newlyEligibleApplicantDBRowMap, newlyIneligibleApplicantDBRowMap, changeEligibleApplicantDBRowMap, 
					newlyEligibleApplicantJsonRowMap, newlyIneigibleApplicantJsonRowMap, changeEligibleApplicantJsonRowMap,  
					responsiblePersonApplicantJson, ssapApplicantEventMap,  household, healthEnrollees, currentApplication, healthEnrollmentShopDTO, 
					coverageDate, enrollmentsByTypeMap,householdContactApplicantJson);
		} catch (GIException | ParseException e) {
			throw new GIRuntimeException("Error forming AutoRenewalRequest object" , ExceptionUtils.getStackTrace(e));
		}


		return request;
	}

	private AutoRenewalRequest populateHealthEnrollment(Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap,
			Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap,
			Map<String, SsapApplicant> otherEligibleApplicantDBRowMap,
			Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap,
			Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap,
			Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap, HouseholdMember responsiblePersonApplicantJson,
			Map<String, SsapApplicantEvent> applicantEventMap, Household household, List<String> healthEnrollees,
			SsapApplication currentApplication, EnrollmentShopDTO healthEnrollmentShopDTO, Date coverageDate, 
			Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, HouseholdMember householdContactApplicantJson) throws GIException, ParseException {
		
		ObjectFactory factory = new ObjectFactory();

		AutoRenewalRequest autoRenewalRequest = factory.createAutoRenewalRequest();
		com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household specialEnrollmentHousehold = factory.createAutoRenewalRequestHousehold();
		autoRenewalRequest.setHousehold(specialEnrollmentHousehold);
		
		SsapApplicant primaryApplicant= ssapUtil.getPrimaryContactApplicantFromApplication(currentApplication);
		
		if (responsiblePersonApplicantJson == null){
			throw new GIRuntimeException("responsiblePersonApplicantJson cannot be null!");
		}
		
		/* productType = Health */
		specialEnrollmentHousehold.setProductType(ProductType.H);
		
		/* responsiblePerson section */
		ResponsiblePerson responsiblePerson = getResponsiblePerson(factory,responsiblePersonApplicantJson );
		specialEnrollmentHousehold.setResponsiblePerson(responsiblePerson);

		/* houseHoldContact section */
		HouseHoldContact houseHoldContact = getHouseHoldContact(factory, householdContactApplicantJson);
		specialEnrollmentHousehold.setHouseHoldContact(houseHoldContact);
		
		/* individual section */
		Individual individual = getIndividual(factory, currentApplication, healthEnrollmentShopDTO, coverageDate);
		specialEnrollmentHousehold.setIndividual(individual);
		
		/* member section */
		Members members = populateAutoSpecialMembers(primaryApplicant, newlyEligibleApplicantDBRowMap,
				otherEligibleApplicantDBRowMap, newlyEligibleApplicantJsonRowMap, changeEligibleApplicantJsonRowMap,
				responsiblePersonApplicantJson, applicantEventMap, household, healthEnrollees, currentApplication,
				healthEnrollmentShopDTO, factory);
		specialEnrollmentHousehold.setMembers(members);
		
		/* disenrollMembers section */
		if (newlyIneligibleApplicantDBRowMap.size() > 0){
			DisenrollMembers disenrollMembers = getRemoveMembers(newlyIneligibleApplicantDBRowMap, newlyIneigibleApplicantJsonRowMap, responsiblePersonApplicantJson,
					applicantEventMap, household, healthEnrollees, currentApplication, healthEnrollmentShopDTO,
					coverageDate, factory);
			
			if (!disenrollMembers.getDisenrollMember().isEmpty()){ // HIX-90288
				specialEnrollmentHousehold.setDisenrollMembers(disenrollMembers);
			}
		}
		
		
		specialEnrollmentHousehold.setApplicationId(currentApplication.getId());
		
		/* do we care about userRoleType and userRoleId ??*/
		getDesignatedBrokerOrAssister(specialEnrollmentHousehold, household);
		
		return autoRenewalRequest;
	}


	private void getDesignatedBrokerOrAssister(
			com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household specialEnrollmentHousehold, Household household) {
		try {
			ResponseEntity<DesignateBroker> brokerDesignationResponse = 
					ghixRestTemplate.exchange(GhixEndPoints.BrokerServiceEndPoints.GET_INDIVIDUAL_DESIGNATION, 
							ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, DesignateBroker.class, 
							household.getId());
			if(brokerDesignationResponse != null) {
				DesignateBroker designateBroker = brokerDesignationResponse.getBody();
				if(designateBroker != null && designateBroker.getStatus().toString().equalsIgnoreCase("Active")) {						
					specialEnrollmentHousehold.setUserRoleId(String.valueOf(designateBroker.getBrokerId()));
					specialEnrollmentHousehold.setUserRoleType(RoleTypeVal.AGENT);
				} else {
					ResponseEntity<DesignateAssister> assisterDesignationResponse = 
							ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEntityEndPoints.GET_INDIVIDUAL_DESIGNATION, 
									ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, DesignateAssister.class, 
									household.getId());
					
					if(assisterDesignationResponse != null) {
						DesignateAssister designateAssister = assisterDesignationResponse.getBody();
						if(designateAssister != null && designateAssister.getStatus().toString().equalsIgnoreCase("Active")) {
							specialEnrollmentHousehold.setUserRoleId(String.valueOf(designateAssister.getAssisterId()));
							specialEnrollmentHousehold.setUserRoleType(RoleTypeVal.ASSISTER);
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception finding designated assister or broker", e);
		}
	}
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;


	private DisenrollMembers getRemoveMembers(Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap,
			Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap, HouseholdMember responsiblePersonApplicantJson,
			Map<String, SsapApplicantEvent> applicantEventMap, Household household, List<String> healthEnrollees,
			SsapApplication currentApplication, EnrollmentShopDTO healthEnrollmentShopDTO, Date coverageDate,
			ObjectFactory factory) {
		
		DisenrollMembers members = factory.createAutoRenewalRequestHouseholdDisenrollMembers();
		for (String newIneligibleApplicantGuid : newlyIneligibleApplicantDBRowMap.keySet()) {
			if (healthEnrollees.contains(newIneligibleApplicantGuid)){
				
				SsapApplicant memberDB = newlyIneligibleApplicantDBRowMap.get(newIneligibleApplicantGuid);
				//HouseholdMember memberJson = newlyIneigibleApplicantJsonRowMap.get(newIneligibleApplicantGuid);
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newIneligibleApplicantGuid);
				
				
				DisenrollMember member = factory.createAutoRenewalRequestHouseholdDisenrollMembersDisenrollMember();
				member.setMemberId(memberDB.getApplicantGuid());

				DateTime terminationDate = new DateTime(coverageDate);
				if (ReferralConstants.ONE != ssapApplicantEvent.getSepEvents().getType()) {
					terminationDate = terminationDate.minusDays(ReferralConstants.ONE);
				}
				member.setTerminationDate(formWSDate(new Date(terminationDate.getMillis())));
				
				if ("DEATH".equals(ssapApplicantEvent.getSepEvents().getName())){
					member.setDeathDate(formWSDate(ssapApplicantEvent.getEventDate()));
				}
				member.setTerminationReasonCode(ssapApplicantEvent.getSepEvents().getMrcCode());
				
				Enrollments enrollments = factory.createAutoRenewalRequestHouseholdDisenrollMembersDisenrollMemberEnrollments();
				enrollments.getEnrollmentId().add(String.valueOf(healthEnrollmentShopDTO.getEnrollmentId()));
				
				member.setEnrollments(enrollments);
				
				members.getDisenrollMember().add(member);
			}
		}
		
		return members;
	}


	private Members populateAutoSpecialMembers(SsapApplicant primaryApplicant, Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap,
			Map<String, SsapApplicant> otherEligibleApplicantDBRowMap,
			Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap,
			Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap, HouseholdMember responsiblePersonApplicantJson,
			Map<String, SsapApplicantEvent> applicantEventMap, Household household, List<String> healthEnrollees,
			SsapApplication currentApplication, EnrollmentShopDTO healthEnrollmentShopDTO, ObjectFactory factory) {
		
		Members members = factory.createAutoRenewalRequestHouseholdMembers();

		Map<String, String> personToGuidMap = populatePersonToGuidMap(currentApplication);
		
		// add members
		for (String newApplicantGuid : newlyEligibleApplicantDBRowMap.keySet()) {

			SsapApplicant memberDB = newlyEligibleApplicantDBRowMap.get(newApplicantGuid);
			HouseholdMember memberJson = newlyEligibleApplicantJsonRowMap.get(newApplicantGuid);
			SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newApplicantGuid);

			Member member = populateMember( factory,
					memberDB, memberJson, 
					responsiblePersonApplicantJson, ssapApplicantEvent, household, 
					currentApplication, "ADD", healthEnrollmentShopDTO, personToGuidMap );

			members.getMember().add(member);
		}

		// other eligible members
		for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
			if (healthEnrollees.contains(otherEligibleApplicantGuid)){
				SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
				HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(otherEligibleApplicantGuid);


				Member member = populateMember( factory,
						memberDB, memberJson, 
						responsiblePersonApplicantJson, ssapApplicantEvent, household, 
						currentApplication, "NOCHANGE", healthEnrollmentShopDTO, personToGuidMap);

				members.getMember().add(member);
			}
		}
		return members;
	}


	private Map<String, String> populatePersonToGuidMap(SsapApplication currentApplication) {
		
		Map<String, String> personToGuidMap = new HashMap<>();
		List<SsapApplicant> applicants = currentApplication.getSsapApplicants();
		
		for (SsapApplicant ssapApplicant : applicants) {
			personToGuidMap.put(String.valueOf(ssapApplicant.getPersonId()), ssapApplicant.getApplicantGuid());
		}
		return personToGuidMap;
	}


	private Member populateMember(ObjectFactory factory, SsapApplicant memberDB, HouseholdMember memberJson,
			HouseholdMember responsiblePersonApplicantJson, SsapApplicantEvent ssapApplicantEvent,
			Household household, SsapApplication currentApplication, String type, EnrollmentShopDTO healthEnrollmentShopDTO, Map<String, String> personToGuidMap) {

		Member member = factory.createAutoRenewalRequestHouseholdMembersMember();

		member.setMemberId(memberDB.getApplicantGuid());
		member.setMaintenanceReasonCode(ssapApplicantEvent.getSepEvents().getMrcCode());
		
		member.setNewPersonFLAG(YesNoVal.N);
		if (StringUtils.equalsIgnoreCase("ADD", type)) {
			member.setNewPersonFLAG(YesNoVal.Y); 
		}
		
		member.setFirstName(memberDB.getFirstName());
		member.setLastName(memberDB.getLastName());
		member.setMiddleName(memberDB.getMiddleName());
		member.setSuffix(memberDB.getNameSuffix());
		
		member.setDob(formWSDate(memberDB.getBirthDate()));
		member.setSsn(memberDB.getSsn());
		
		populateTobaccoUsageDetails( member, memberDB);
		
		String primaryPhone = null, secondaryPhone = null;
		if (!StringUtils.isEmpty(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) && !StringUtils.isEmpty(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
			secondaryPhone = responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber())) {
			primaryPhone = responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		}
		
		if (StringUtils.isEmpty(primaryPhone)) {
			primaryPhone = household.getPhoneNumber();
		}
		member.setPrimaryPhone(primaryPhone);
		member.setSecondaryPhone(secondaryPhone);

		populateLocationDetails(member, memberJson, responsiblePersonApplicantJson);
		populateGenderDetails(member, memberDB);
		
		populateMaritalStatus(member, memberDB);
		
		if(memberJson.getCitizenshipImmigrationStatus()!=null) {
			if(memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator()) {
				member.setCitizenshipStatusCode(US_CITIZEN);
			}else{
				member.setCitizenshipStatusCode(NON_US_CITIZEN);
			}
		}
		
		String languageSpokenLkpStr = languageSpoken(memberJson);
		member.setSpokenLanguageCode(languageSpokenLkpStr);
		
		String languageWrittenLkpStr = languageWritten(memberJson);
		member.setWrittenLanguageCode(languageWrittenLkpStr);
		
		populateMailingAddress(memberJson, member);
		
		member.setResponsiblePersonRelationship(populateRelationshipToHCP(memberJson, "" + memberDB.getPersonId(), responsiblePersonApplicantJson));
		
		if (memberDB.getPersonId() == 1){
			if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
					&& "Email".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())) {
				member.setPreferredEmail(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getEmailAddress()));
			}
			if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
					&& "TextMessage".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())
					&& responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null ) {
				member.setPreferredPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()));
			}
		}
		
		member.setExistingMedicalEnrollmentID(String.valueOf(healthEnrollmentShopDTO.getEnrollmentId()));
		
		populateRelationship(factory, memberDB, memberJson, responsiblePersonApplicantJson, member, personToGuidMap);

		populateRaces(factory, memberJson, member);
		
		member.setFinancialHardshipExemption("Y".equals(memberDB.getHardshipExempt()) ? YesNoVal.Y : YesNoVal.N);
		
		member.setCatastrophicEligible(YesNoVal.N);
		
		//member.setChildOnlyPlanEligibile(YesNoVal.N);
		member.setExternalMemberId(memberDB.getExternalApplicantId());
		return member;

	}


	private void populateRaces(ObjectFactory factory, HouseholdMember memberJson, Member member) {
		if(memberJson.getEthnicityAndRace() != null) {
			//List<Ethnicity> ethnicities = memberJson.getEthnicityAndRace().getEthnicity();
			List<Race> races = memberJson.getEthnicityAndRace().getRace();

			if(races != null) {
				for (Race race : races) {
					com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member.Race ind71Race = factory.createAutoRenewalRequestHouseholdMembersMemberRace();
					/*if(race.getCode().equalsIgnoreCase("0000-0")){
						continue;
					}*/
					if(race.getCode().equalsIgnoreCase("2131-1")){
						ind71Race.setDescription(race.getOtherLabel());
					}
					ind71Race.setRaceEthnicityCode(race.getCode());
					member.getRace().add(ind71Race);
				}
			}
			
			/*if(ethnicities != null) {
				for (Ethnicity race : ethnicities) {
					com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member.Race ind71Race = factory.createAutoRenewalRequestHouseholdMembersMemberRace();
					if(race.getCode().equalsIgnoreCase("0000-0")){
						continue;
					}
					if(race.getCode().equalsIgnoreCase("2131-1")){
						ind71Race.setDescription(race.getOtherLabel());
					}
					ind71Race.setRaceEthnicityCode(race.getCode());
					member.getRace().add(ind71Race);
				}
			}*/
		}
	}


	private void populateRelationship(ObjectFactory factory, SsapApplicant memberDB, HouseholdMember memberJson,
			HouseholdMember responsiblePersonApplicantJson, Member member, Map<String, String> personToGuidMap) {
		List<BloodRelationship> bloodRelatioshipList = responsiblePersonApplicantJson.getBloodRelationship();
		String personId = String.valueOf(memberDB.getPersonId());
		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId())) {
				Relationship ind71Relationship = factory.createAutoRenewalRequestHouseholdMembersMemberRelationship();
				
				String relatedMemberId = personToGuidMap.get(bloodRelationship.getRelatedPersonId());
				ind71Relationship.setMemberId(relatedMemberId);
				ind71Relationship.setRelationshipCode(bloodRelationship.getRelation());
				
				member.getRelationship().add(ind71Relationship);
			}
		}
		
	}


	private void populateMailingAddress(HouseholdMember memberJson, Member member) {
		Address mailingAddress = memberJson.getHouseholdContact().getMailingAddress();

		member.setMailingAddress1(mailingAddress.getStreetAddress1());
		member.setMailingAddress2(mailingAddress.getStreetAddress2());
		member.setMailingCity(mailingAddress.getCity());
		member.setMailingState(mailingAddress.getState());
		member.setMailingZip(ssapUtil.getFiveDigitZipCode(mailingAddress.getPostalCode()));
	}


	private void populateMaritalStatus(Member member, SsapApplicant memberDB) {
		String maritalStatusLkpStr = memberDB.getMarried();
		if ("Yes".equalsIgnoreCase(memberDB.getMarried()) || "true".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = "M";
		}else if("No".equalsIgnoreCase(memberDB.getMarried()) || "false".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = "R";
		}
		member.setMaritalStatusCode(maritalStatusLkpStr);
	}

	private static final String HEALTH = "health";

	private Individual getIndividual(ObjectFactory factory,SsapApplication ssapApplication, 
			EnrollmentShopDTO enrollment, Date coverageDate) throws GIException, ParseException {
		
		Individual individual = factory.createAutoRenewalRequestHouseholdIndividual();
		
		if(ssapApplication.getCmrHouseoldId() != null) {
			individual.setHouseholdCaseId(ssapApplication.getCmrHouseoldId().longValue());
		}
		
		individual.setCoverageStartDate(formWSDate(coverageDate));
		individual.setFinancialEffectiveDate(formWSDate(coverageDate)); // TODO:
		//individual.setPlanId(getPlanId(enrollment, HEALTH ));
		individual.setEnrollmentType(EnrollmentVal.S);
		
		if(ssapApplication.getMaximumAPTC() != null) {
			individual.setAptc(ssapApplication.getMaximumAPTC().floatValue());
		}
		
		individual.setCsr(ssapApplication.getCsrLevel() != null ? ssapApplication.getCsrLevel() : "CS1");
		//individual.setEhbAmount(getEHBAmount(ssapApplication.getCaseNumber(), coverageDate));
		
		AptcHistory aptcHistory = aptcHistoryRepository.findLatestSlcspAmount(ssapApplication.getId()); 
		if(aptcHistory != null && aptcHistory.getSlcspAmount() != null) {
			individual.setEhbAmount(new Float(aptcHistory.getSlcspAmount().toString()));
			}

		return individual;
	}

	@SuppressWarnings("unused")
	private String getPlanId(EnrollmentShopDTO enrollment, String enrollmentType) {
		return enrollment != null ? String.valueOf(enrollment.getPlanId()) : null;
	}

	private Map<String, EnrollmentShopDTO> getEnrollmentDetails(SsapApplication currentApplication, List<String> enrollees) {

		SsapApplication enrolledApplication = getEnrolledSsapForCoverageYearAndHousehold(currentApplication);

		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>(0);
		try {
			enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveEnrollmentsForApplicants(enrolledApplication.getId(), enrollees);
		} catch (GIException e) {
			throw new GIRuntimeException(GETTING_ENROLLMENT_DETAILS_FOR_LINKED_ENROLLED_APPLICATION + enrolledApplication.getCaseNumber());
		}
		if(enrollmentsByTypeMap == null || enrollmentsByTypeMap.isEmpty() || null == enrollmentsByTypeMap.get(HEALTH)){			
			SsapApplication currentErSsap  = ssapApplicationRepository.findErAfterLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
			if(currentErSsap == null){
				throw new GIRuntimeException(THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR + currentApplication.getCoverageYear());
			}
			try{
				enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveEnrollmentsForApplicants(currentErSsap.getId(), enrollees);
			}catch (GIException e) {
				throw new GIRuntimeException(GETTING_ENROLLMENT_DETAILS_FOR_LINKED_ENROLLED_APPLICATION + enrolledApplication.getCaseNumber());
			}
			
			if(enrollmentsByTypeMap == null || enrollmentsByTypeMap.isEmpty() || null == enrollmentsByTypeMap.get(HEALTH)){
				throw new GIRuntimeException(EXISTING_MEDICAL_ENROLLMENT_COULD_NOT_BE_RETRIEVED_FOR_THE_APPLICATION_WITH_CASENUMBER+ currentErSsap.getCaseNumber());
			}			
		}
		return enrollmentsByTypeMap;
	}

	public SsapApplication getEnrolledSsapForCoverageYearAndHousehold(SsapApplication currentApplication) {
		SsapApplication enrolledApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
		if(enrolledApplication == null){
			throw new GIRuntimeException(THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR + currentApplication.getCoverageYear());
		}
		return enrolledApplication;
	}

	private Timestamp computeCoverageStartDate(Long currentApplicationId, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap) {
		try {
			return coverageStartDateService.computeCoverageStartDate(BigDecimal.valueOf(currentApplicationId), 
					enrollmentsByTypeMap, 
					false,false);
		} catch (GIException e) {
			throw new GIRuntimeException(ERROR_COMPUTING_COVERAGE_START_DATE_CURRENT_APPLICATION + currentApplicationId);
		}
	}


	private ResponsiblePerson getResponsiblePerson(ObjectFactory factory, HouseholdMember responsiblePersonApplicantJson) {
		
		ResponsiblePerson responsiblePerson = factory.createAutoRenewalRequestHouseholdResponsiblePerson();

		responsiblePerson.setResponsiblePersonId(responsiblePersonApplicantJson.getApplicantGuid());
		
		responsiblePerson.setResponsiblePersonFirstName(responsiblePersonApplicantJson.getName().getFirstName());
		responsiblePerson.setResponsiblePersonLastName(responsiblePersonApplicantJson.getName().getLastName());
		responsiblePerson.setResponsiblePersonMiddleName(responsiblePersonApplicantJson.getName().getMiddleName());
		responsiblePerson.setResponsiblePersonSuffix(responsiblePersonApplicantJson.getName().getSuffix());
		responsiblePerson.setResponsiblePersonSsn(responsiblePersonApplicantJson.getSocialSecurityCard().getSocialSecurityNumber());
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null) {
			responsiblePerson.setResponsiblePersonPrimaryPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) != null ? responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber() : nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber()));
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone() != null) {
			responsiblePerson.setResponsiblePersonSecondaryPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber()));
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
				&& "Email".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())) {
			responsiblePerson.setResponsiblePersonPreferredEmail(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getEmailAddress()));
		}
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null  
				&& "TextMessage".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())
				&& responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null) {
			responsiblePerson.setResponsiblePersonPreferredPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()));
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress() != null) { 
			responsiblePerson.setResponsiblePersonHomeAddress1(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getStreetAddress1()));
			responsiblePerson.setResponsiblePersonHomeAddress2(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getStreetAddress2()));
			responsiblePerson.setResponsiblePersonHomeCity(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getCity()));
			responsiblePerson.setResponsiblePersonHomeState(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getState()));
			responsiblePerson.setResponsiblePersonHomeZip(ssapUtil.getFiveDigitZipCode(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getPostalCode()));
		}

		responsiblePerson.setResponsiblePersonType("QD"); // ???

		return responsiblePerson;
	}

	private String nullCheckedValue(String value) {
		if(StringUtils.isBlank(value)) {
			return null;
		}
		return value;
	}
	private HouseHoldContact getHouseHoldContact(ObjectFactory factory, HouseholdMember responsiblePersonApplicantJson) {

		HouseHoldContact houseHoldContact = factory.createAutoRenewalRequestHouseholdHouseHoldContact();
		
		houseHoldContact.setHouseHoldContactId(responsiblePersonApplicantJson.getApplicantGuid());
		
		houseHoldContact.setHouseHoldContactFirstName(responsiblePersonApplicantJson.getName().getFirstName());
		houseHoldContact.setHouseHoldContactLastName(responsiblePersonApplicantJson.getName().getLastName());
		houseHoldContact.setHouseHoldContactMiddleName(nullCheckedValue(responsiblePersonApplicantJson.getName().getMiddleName()));
		houseHoldContact.setHouseHoldContactSuffix(responsiblePersonApplicantJson.getName().getSuffix());

		if(responsiblePersonApplicantJson.getSocialSecurityCard() != null) {
			houseHoldContact.setHouseHoldContactFederalTaxIdNumber(responsiblePersonApplicantJson.getSocialSecurityCard().getSocialSecurityNumber());
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null) {
			houseHoldContact.setHouseHoldContactPrimaryPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) != null ? responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber() : nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber()));
		}
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone() != null) {
			houseHoldContact.setHouseHoldContactSecondaryPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber()));
		}
		
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
				&& "Email".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())) {
			houseHoldContact.setHouseHoldContactPreferredEmail(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getEmailAddress()));
		}
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences() != null 
				&& "TextMessage".equalsIgnoreCase(responsiblePersonApplicantJson.getHouseholdContact().getContactPreferences().getPreferredContactMethod())
				&& responsiblePersonApplicantJson.getHouseholdContact().getPhone() != null) {
			houseHoldContact.setHouseHoldContactPreferredPhone(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()));
		}
			
		if(responsiblePersonApplicantJson.getHouseholdContact() != null && responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress() != null) {
			houseHoldContact.setHouseHoldContactHomeAddress1(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getStreetAddress1()));
			houseHoldContact.setHouseHoldContactHomeAddress2(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getStreetAddress2()));
			houseHoldContact.setHouseHoldContactHomeCity(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getCity()));
			houseHoldContact.setHouseHoldContactHomeState(nullCheckedValue(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getState()));
			houseHoldContact.setHouseHoldContactHomeZip(ssapUtil.getFiveDigitZipCode(responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getPostalCode()));
		}

		return houseHoldContact;
	}

	private void populateLocationDetails(Member member, HouseholdMember memberJson, HouseholdMember responsiblePersonApplicantJson) {

		Address homeAddress = memberJson.getHouseholdContact().getHomeAddress();

		member.setCountyCode(getCounty(memberJson, responsiblePersonApplicantJson));
		member.setHomeAddress1(nullCheckedValue(homeAddress.getStreetAddress1()));
		member.setHomeAddress2(nullCheckedValue(homeAddress.getStreetAddress2()));
		member.setHomeCity(nullCheckedValue(homeAddress.getCity()));
		member.setHomeState(nullCheckedValue(homeAddress.getState()));
		member.setHomeZip(ssapUtil.getFiveDigitZipCode(homeAddress.getPostalCode()));
	}
	
	private String getCounty(HouseholdMember applicantJsonRow, HouseholdMember responsiblePersonApplicantJson) {
		String countyCode = responsiblePersonApplicantJson.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null){ 
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()){
				countyCode = applicantJsonRow.getOtherAddress().getAddress().getCounty(); 
			}
		}
		return countyCode;
	}

	private void populateGenderDetails(Member member, SsapApplicant memberDB) {
		String genderLkpStr = "Male".equalsIgnoreCase(memberDB.getGender()) ? "M" : "F";
		member.setGenderCode(genderLkpStr);
	}

	private String populateRelationshipToHCP(HouseholdMember memberJson, String personId, HouseholdMember responsiblePersonApplicantJson) {
		String relatioshipToHCPStr = null;

		List<BloodRelationship> bloodRelatioshipList = responsiblePersonApplicantJson.getBloodRelationship();
		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId()) && String.valueOf(responsiblePersonApplicantJson.getPersonId()).equalsIgnoreCase(bloodRelationship.getRelatedPersonId())) {
				relatioshipToHCPStr = bloodRelationship.getRelation();
			}
		}
		return relatioshipToHCPStr;
	}

	private String languageSpoken(HouseholdMember memberJson) {
		String languageSpoken = null;
		String languageSpokenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageSpoken = memberJson.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage();
		}

		if(StringUtils.isNotBlank(languageSpoken)) {
			languageSpokenCode = LanguageEnum.getCode(languageSpoken);								
		}
		return languageSpokenCode;
	}

	private String languageWritten(HouseholdMember memberJson) {
		String languageWritten = null;
		String languageWrittenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageWritten = memberJson.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage();
		}

		if(StringUtils.isNotBlank(languageWritten)) {
			languageWrittenCode = LanguageEnum.getCode(languageWritten);								
		}
		return languageWrittenCode;
	}

	private void populateTobaccoUsageDetails(Member member, SsapApplicant memberDB) {
		YesNoVal tobaccoUsage = YesNoVal.N;
		if (memberDB.getTobaccouser() != null) {
			tobaccoUsage = StringUtils.equalsIgnoreCase(ReferralConstants.Y, memberDB.getTobaccouser()) ? YesNoVal.Y : YesNoVal.N;
		}
		else{
			tobaccoUsage = null;
		}
		member.setTobacco(tobaccoUsage);
	}
	

	private SsapApplication validate(Long ssapApplicationId) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException(UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID + ssapApplicationId);
		}

		SsapApplication currentApplication = ssapApplications.get(0);

		SsapApplicationEvent currentApplicationEvent = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(ssapApplicationId);

		if (currentApplicationEvent == null) {
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}
		return currentApplication;
	}

	

	private static final String WS_DATE_FORMAT = "MM/dd/yyyy";

	private static final String IND71 = "IND71";

	private static String formWSDate(Date date) {
		SimpleDateFormat WS_SIMPLE_DATE_FORMATTER = new SimpleDateFormat(WS_DATE_FORMAT);
		return WS_SIMPLE_DATE_FORMATTER.format(date);
	}
	
	
	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, IND71, status, ssapApplicationId, null);
	}

}
