package com.getinsured.iex.hub.ssac.wrapper;

import java.io.IOException;

import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.google.gson.Gson;

public class RequestTitleIIMonthlyIncomeVerificationIndicatorWrapper {
	private  boolean titleIIMonthlyIncomeVerificationRequested;
	private Boolean requestTitleIIMonthlyIncomeVerificationIndicator;
	
	private static com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory xsdFactory;
	//private static com.getinsured.iex.hub.ssac.extension.ObjectFactory extnFactory;
	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	public RequestTitleIIMonthlyIncomeVerificationIndicatorWrapper(Boolean reqTitleIIIncomeVerification) {
		this.requestTitleIIMonthlyIncomeVerificationIndicator = reqTitleIIIncomeVerification;
		this.titleIIMonthlyIncomeVerificationRequested = requestTitleIIMonthlyIncomeVerificationIndicator.isValue();
	}
	
	
	public RequestTitleIIMonthlyIncomeVerificationIndicatorWrapper() {
		xsdFactory = new com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.ObjectFactory();
		//extnFactory = new com.getinsured.iex.hub.ssac.extension.ObjectFactory();
	}
	
	public void setTitleIIMonthlyIncomeVerificationRequested(boolean isVerifyFlag) {
		this.titleIIMonthlyIncomeVerificationRequested = isVerifyFlag;
		if(requestTitleIIMonthlyIncomeVerificationIndicator == null){
			requestTitleIIMonthlyIncomeVerificationIndicator = xsdFactory.createBoolean();
		}
		requestTitleIIMonthlyIncomeVerificationIndicator.setValue(isVerifyFlag);
		this.titleIIMonthlyIncomeVerificationRequested = isVerifyFlag;
	}
	
	public boolean isTitleIIMonthlyIncomeVerificationRequested() {
		if(this.requestTitleIIMonthlyIncomeVerificationIndicator == null){
			return false;
		}
		return this.titleIIMonthlyIncomeVerificationRequested;
	}
	
	public Boolean getSystemRepresentation() {
		return this.requestTitleIIMonthlyIncomeVerificationIndicator;
	}
	
	public static RequestTitleIIMonthlyIncomeVerificationIndicatorWrapper 
		fromJsonString(String jsonString) throws IOException {
		
		return gson.fromJson(jsonString, RequestTitleIIMonthlyIncomeVerificationIndicatorWrapper.class);
		
	}
	
	/*public static void main(String[] args) throws JAXBException, IOException {
		RequestTitleIIMonthlyIncomeVerificationIndicatorWrapper wrapper = fromJsonString("{\"titleIIMonthlyIncomeVerificationRequested\":\"true\"}");
		SSACompositeIndividualRequestType req = extnFactory.createSSACompositeIndividualRequestType();
		req.setRequestTitleIIMonthlyIncomeVerificationIndicator(wrapper.requestTitleIIMonthlyIncomeVerificationIndicator);
		JAXBElement<SSACompositeIndividualRequestType> ssaCompReq = extnFactory.createSSACompositeIndividualRequest(req);
		JAXBContext jc = JAXBContext.newInstance("com.getinsured.hub.ssac.extension");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(ssaCompReq, System.out);
	}*/
}
