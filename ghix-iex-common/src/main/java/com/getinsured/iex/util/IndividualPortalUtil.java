package com.getinsured.iex.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;

@Component
public class IndividualPortalUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualPortalUtil.class);
	public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";

	public boolean isInsideOEEnrollmentWindow(int coverageYear) {
		  Date currentDate = new TSDate();
		  boolean status = false;
		  Date oeStart = null;
		  Date oeEnd = null;
		  int prevYear=0;
		  int currYear=0;
		try {
			String previousYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
			String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
			if(StringUtils.isNumeric(previousYear)){
				prevYear = Integer.parseInt(previousYear);
			}
			if(StringUtils.isNumeric(currentYear)){
				currYear = Integer.parseInt(currentYear);
			}
			if (coverageYear <= prevYear) {
				return false;
			}
			if (coverageYear == currYear) {
				oeStart = new SimpleDateFormat(SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE));
				oeEnd = new SimpleDateFormat(SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE));
				Calendar calendar = TSCalendar.getInstance();
				calendar.setTime(oeEnd);
				calendar.set(Calendar.HOUR_OF_DAY, 23);
				calendar.set(Calendar.MINUTE, 59);
				calendar.add(Calendar.SECOND, 59);
				oeEnd = calendar.getTime();
	  
				if ((currentDate.equals(oeStart) || currentDate.equals(oeEnd)) || (currentDate.after(oeStart) && currentDate.before(oeEnd))) {
					status = true;
				}
			}
			
		} catch (ParseException e) {
			  LOGGER.error("Exception occured while checking the current with open enrollment start date : ", e);
			  throw new GIRuntimeException("Exception occured while checking the current with open enrollment start date :");
		  }
		  return status;
	 }
	
	public boolean isInsideOEWindow(int coverageYear) {
		  Date currentDate = new TSDate();
		  boolean status = false;
		  Date oeStart = null;
		  Date oeEnd = null;
		  int prevYear=0;
		  int currYear=0;
		try {
			String previousYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
			String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
			if(StringUtils.isNumeric(previousYear)){
				prevYear = Integer.parseInt(previousYear);
			}
			if(StringUtils.isNumeric(currentYear)){
				currYear = Integer.parseInt(currentYear);
			}
			if (coverageYear <= prevYear) {
				return false;
			}
			if (coverageYear == currYear) {
				oeStart = new SimpleDateFormat(SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE));
				oeEnd = new SimpleDateFormat(SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE));
				Calendar calendar = TSCalendar.getInstance();
				calendar.setTime(oeEnd);
				calendar.set(Calendar.HOUR_OF_DAY, 23);
				calendar.set(Calendar.MINUTE, 59);
				calendar.add(Calendar.SECOND, 59);
				oeEnd = calendar.getTime();
	  
				if ((currentDate.equals(oeStart) || currentDate.equals(oeEnd)) || (currentDate.after(oeStart) && currentDate.before(oeEnd))) {
					status = true;
				}
			}
			
		} catch (ParseException e) {
			  LOGGER.error("Exception occured while checking the current with open enrollment start date : ", e);
			  throw new GIRuntimeException("Exception occured while checking the current with open enrollment start date :");
		  }
		  return status;
	 }
	
	public boolean isDateInsideOEWindow(Date date) {
		boolean status = false;
		Date oeStart = null;
		Date oeEnd = null;
		try {
			oeStart = new SimpleDateFormat(SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE));
			oeEnd = new SimpleDateFormat(SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE));
			Calendar calendar = TSCalendar.getInstance();
			calendar.setTime(oeEnd);
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.add(Calendar.SECOND, 59);
			oeEnd = calendar.getTime();

			if ((date.equals(oeStart) || date.equals(oeEnd)) || (date.after(oeStart) && date.before(oeEnd))) {
				status = true;
			}

		} catch (ParseException e) {
			LOGGER.error("Exception occured while checking the current date with in OE Scenario ", e);
			throw new GIRuntimeException("Exception occured while checking the current date with in OE Scenario :");
		}
		return status;
	}
}
