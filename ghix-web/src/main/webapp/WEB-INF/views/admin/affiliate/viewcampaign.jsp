<%@page import="java.util.ArrayList"%>
<%@page import="antlr.collections.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.util.*"   %>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
.dob label {display: inline;}
</style>





<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>

<script type="text/javascript" src="<:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/highlight.min.js" />"></script>
<form class="form-vertical" id="frmviewcampaign" name="frmviewcampaign" action="<c:url value="/admin/affiliate/viewcampaign" />" method="POST">
<div class="gutter10">
	<div class="row-fluid">
	        <ul class="page-breadcrumb">
              <c:if test="${viewaffiliate == 'viewaffiliate' }">
                <li><a href="<c:url value="/admin/affiliate/viewaffiliate/${affiliate.affiliateId}" />">&lt; <spring:message  code="label.back"/></a></li>
				<li><a href="<c:url value="/admin/affiliate/manage" />">Manage Affiliate</a></li>
			    <li><a href="<c:url value="/admin/affiliate/viewaffiliate/${affiliate.affiliateId}" />">View Affiliate</a></li>
              </c:if>
              <c:if test="${viewaffiliate == null }">
               <li><a href="<c:url value="/admin/affiliate/managecampaign"/>">&lt; <spring:message  code="label.back"/></a></li>
             		<li><a href="<c:url value="/admin/affiliate/managecampaign"/>">Manage Campaign</a></li>
              </c:if>
             <li>View Campaign</li>
        </ul><!--page-breadcrumb ends--></div>
        
        <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
      </div>
<legend class="removemember" id="">View Campaign Information</legend>
	<table class="table table-condensed table-border-none">
		<tbody>
			<tr>
				<td class="txt-right">Affiliate Id</td>
				<td><strong>${affiliate.affiliateId}</strong></td>
				
				<td class="txt-right">Flow Name</td>
				<td><strong>${affiliateFlow.flowName}</strong></td>
				
				<td class="txt-right ">Account Executive</td>
				<td><strong>${affiliate.accountExecutive}</strong></td>
			</tr>
			<tr>
				
				<td class="txt-right ">Affiliate Company</td>
				<td><strong>${affiliate.companyName}</strong></td>
				
				<td class="txt-right ">Affiliate Manager</td>
				<td><strong>${affiliate.managerName}</strong></td>
				
				<td class="txt-right ">Campaign Name</td>
				<td><strong>${campaign.campaignName}</strong></td>
			</tr>
			<tr>
				
				
				<td class="txt-right ">Start Date</td>
				<td><strong>${startDate}</strong></td>
				
				<td class="txt-right ">End Date</td>
				<td><strong>${endDate}</strong></td>
				
				<td class="txt-right ">Amount</td>
				<td><strong>${campaign.amount}</strong></td>
				
			</tr>
		</tbody>
	</table>
	<!--row-fluid-->
</div>
</form>
<!--gutter10-->
<script type="text/javascript">
	   $('#frmviewcampaign').parsley( 'addListener', {
			onFormSubmit: function (isFormValid, event, ParsleyForm) {
				// if field is not visible, do not apply Parsley validation!
				if(isFormValid === "true"){
					var url='<c:url value="/admin/affiliate/viewcampaign" />';
					 $.post(url,function() {
						location.href='<c:url value="/admin/affiliate/viewcampaign" />'
					 });
				}
			} 
		}); 
		

</script>

    