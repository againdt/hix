(function (backbone) {
	/**
	 * @class
	 * Cart
	 */
	backbone.Cart = {
		/**  how many items to show per page */
		perPage : 5,
		
		/** page to start off on */
		page : 1,
	
		/**
		 *
		 */
		initialize: function(){

			this.resetProps();
		},
		
		resetProps: function(){
			this.cartSummary = [];
			this.cartSummary.planCount = 0;
			this.cartSummary.platinum = 0;
			this.cartSummary.gold = 0;
			this.cartSummary.silver = 0;
			this.cartSummary.bronze = 0;
			this.cartSummary.bronze = 0;
			this.cartSummary.catastrophic = 0;
			
			this.cartItems = [];
		},
		
		setCartSummary: function (models) {
			var self = this;
			if(!models){
				models = self.models;
			}
			self.cartSummary['planCount'] = models.length;
			if(models.length){
				_.each(models, function(model){
					self.cartItems.push(model.get("planId"));
				});
			}else{
				self.resetProps();
			}
			self.reset(models);
		},
		
		removeSelectedItems: function (itemId,groupId) {
			var self = this
			var updatedModels = [];

			_.each(self.models, function(model){
				if(itemId == model.get("planId") && groupId == model.get("groupId")){
					model.set({id: model.get("orderItemId")});
					model.destroy({
						success : function () {
							 window.location = "shopCart";
						}});
				}else{
					updatedModels.push(model);
				}
			});
			
			this.resetProps();
			this.setCartSummary(updatedModels);
		},
		
		getTotalPremium: function () {
			var self = this;
			models = self.models;
			var totalPremium = 0;
			if(models.length){
				_.each(models, function(model){
					totalPremium +=  model.get("premium");
				});
			}
			return totalPremium;
		},
		
		getTotalAptc: function () {
			var self = this;
			models = self.models;
			var totalAptc = 0;
			if(models.length){
				_.each(models, function(model){
					totalAptc +=  model.get("aptc");
				});
			}
			return totalAptc;
		},
		
		getTotalContri: function () {
			var self = this;
			models = self.models;
			var totalContri = 0;
			if(models.length){
				_.each(models, function(model){
					totalContri +=  model.get("totalContribution");
				});
			}
			return totalContri;
		},
		
		getEhb: function () {
			var self = this;
			var groupsEhb = new Object();

			_.each(self.models, function(cartModel){
				if(groupsEhb["groupEhb_"+cartModel.get("groupId")]){
					groupsEhb["groupEhb_"+cartModel.get("groupId")] = groupsEhb["groupEhb_"+cartModel.get("groupId")] + cartModel.get("ehb"); 
				}else{
					groupsEhb["groupEhb_"+cartModel.get("groupId")] = cartModel.get("ehb") ;
				}
			});

			return groupsEhb;
		},
	};
	
})(App.backbone);