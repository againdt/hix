package com.getinsured.hix.tkm.repository;

import java.util.List;

import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.TkmDocuments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.consumer.Household;

@Repository
public interface ITicketRepository extends JpaRepository<TkmTickets, Integer>, RevisionRepository<TkmTicketTasks, Integer, Integer>
{
	@Query("SELECT DISTINCT(t.subject) FROM TkmTickets t WHERE t.subject IS NOT NULL")
	List<String> getDistinctSubject();
	
	@Query("SELECT DISTINCT(t.number) FROM TkmTickets t WHERE t.number IS NOT NULL")
	List<String> getDistinctNumbers();
	
	@Query("select tt from TkmTickets tt" 
			  +" where tt.id in (SELECT distinct(t.ticket.id) FROM TkmTicketTasks t WHERE t.status in ('UnClaimed','Claimed') " +
			  "  and t.queue.id in(:id) or t.assignee.id=:assigneeId ) order by tt.id DESC")
	List<TkmTickets> getTkmTicketListByGroupIdAndAssigneeId(@Param("id") List<Integer> groupIds, @Param("assigneeId") Integer assigneeId);
	
	@Query("SELECT t FROM TkmTickets t WHERE t.id= :id ORDER BY t.updated ASC")
	List<TkmTickets> getTkmTicketsHistory(@Param("id") int id);

	@Query("SELECT DISTINCT(t.role) FROM TkmTickets t WHERE t.role IS NOT NULL")
	List<AccountUser> getDistinctRequesters();
	
	@Query("select DISTINCT(tt.subject) from TkmTickets tt" 
			  +" where tt.id in (SELECT distinct(t.ticket.id) FROM TkmTicketTasks t WHERE t.status in ('UnClaimed') " +
			  "  and t.queue.id in(:id) or t.assignee.id=:assigneeId ) order by tt.subject DESC")  
	List<String> getTkmTicketSubjectListByGroupIdAndAssigneeId(@Param("id") List<Integer> groupIds, @Param("assigneeId") Integer assigneeId);
	
	@Query("SELECT tkt FROM TkmTickets tkt,TkmTicketTasks tsk where tkt.id = tsk.ticket.id AND tsk.activitiTaskId = :activitiTaskId")
	List<TkmTickets> getTkmTicketsByActivitiTaskId(@Param("activitiTaskId")String activitiTaskId);
	
/*	@Query("SELECT tkt.priority,count(*) as no_of_tickets FROM TkmTickets tkt where tkt.isArchived = 'N' group by tkt.priority order by tkt.priority ")
	List<Object[]> getTkmTicketsByPriority();*/
	@Query("SELECT tkt.priority,count(*) as no_of_tickets FROM TkmTickets tkt WHERE EXISTS ( FROM TkmTicketTasks tt WHERE tt.ticket.id = tkt.id) AND tkt.isArchived = 'N' AND tkt.status IN ('UnClaimed', 'Open', 'Restarted', 'Reopened') group by tkt.priority order by tkt.priority ")
    List<Object[]> getTkmTicketsByPriority();

	
	/*@Query("SELECT twf.type, count(*) as no_of_tickets FROM TkmTickets tkt JOIN tkt.tkmWorkflows twf where tkt.isArchived = 'N' group by twf.type order by twf.type ")
	List<Object[]> getTkmTicketsByType();*/
    
    @Query("SELECT twf.type, count(*) as no_of_tickets FROM TkmTickets tkt JOIN tkt.tkmWorkflows twf WHERE EXISTS ( FROM TkmTicketTasks tt WHERE tt.ticket.id = tkt.id) AND tkt.isArchived = 'N' AND tkt.status IN ('UnClaimed', 'Open', 'Restarted', 'Reopened') group by twf.type order by twf.type ")
	List<Object[]> getTkmTicketsByType();


	@Query("SELECT count(*) FROM TkmTickets tkt WHERE tkt.userRole.id = :roleId and tkt.moduleId= :moduleId")
	long findByModuleIdAndUserRoleId(@Param("moduleId") Integer moduleId, @Param("roleId") int roleId );

	TkmTickets findByNumber(String ticNum);
	
	@Query("SELECT tt FROM TkmTickets tt WHERE tt.tkmWorkflows.category in ('Complaint','Request') and (tt.tkmExtractedtBatchFlag ='N' or tt.tkmExtractedtBatchFlag IS NULL)  order by tt.created DESC")
	List<TkmTickets> getUnprocessedTkmTickets();
	
	@Transactional
	@Modifying
	@Query("UPDATE TkmTickets tt set tkmExtractedtBatchFlag='Y' where tt.id in(:id)")
	void updateExtractedTicketBatchFlag(@Param("id") List<Integer> ticketId);
	
	
	@Query("SELECT hh FROM Household hh WHERE hh.id = :moduleId ")
	Household getNameCreatedFor(@Param("moduleId") int moduleId);
	
	
	//@Query("SELECT hh FROM Household hh,TkmTickets tt WHERE tt.moduleName ='HOUSEHOLD' hh.firstName LIKE ('% :reqName ') OR hh.lastName LIKE ('% :reqName ') ")
	@Query("SELECT hh FROM Household hh WHERE hh.id IN (SELECT DISTINCT(t.moduleId) FROM TkmTickets t WHERE t.moduleName='HOUSEHOLD')")
	List<Household> getCreatedForIndv();

	@Query("SELECT DISTINCT(t.role) FROM TkmTickets t WHERE t.userRole.id = :roleId")
	List<AccountUser> getCreatedForUser(@Param("roleId")Integer roleId);
	
	@Query("SELECT DISTINCT(t.role.id),t.role.firstName,t.role.lastName FROM TkmTickets t WHERE t.userRole.id = :roleId")
	List<Object[]> getCreatedForUser1(@Param("roleId")Integer roleId);
	
	@Query("SELECT hh FROM Household hh WHERE hh.user.id = :houseHoldUserId")
	List<Household> getHouseholdByuserId(@Param("houseHoldUserId")Integer houseHoldUserId);
	
	@Query("SELECT DISTINCT(t.role.id),t.role.firstName,t.role.lastName FROM TkmTickets t WHERE t.userRole.id = :roleId and  LOWER(t.role.firstName) like  '%'||LOWER(:firstName)||'%' ")
	List<Object[]> getCreatedForUser1(@Param("firstName")String firstName,@Param("roleId")Integer roleId);
	
	@Query("SELECT DISTINCT(t.role.id),t.role.firstName,t.role.lastName FROM TkmTickets t WHERE LOWER(t.role.firstName) like  '%'||LOWER(:firstName)||'%' ")
	List<Object[]> getRequesters(@Param("firstName")String firstName);
	
	@Query("SELECT hh.id,hh.firstName,hh.lastName FROM Household hh WHERE hh.id IN (SELECT DISTINCT(t.moduleId) FROM TkmTickets t WHERE t.moduleName='HOUSEHOLD') " +
			" and LOWER(hh.firstName) like  '%'||LOWER(:firstName)||'%' ")
	List<Object[]> getCreatedForIndv1(@Param("firstName")String firstName);

	@Query("SELECT tt FROM TkmTickets tt WHERE tt.subModuleId = :subModuleId")
	TkmTickets findBySubModuleId(@Param("subModuleId") Integer subModuleId);
	
	@Query("SELECT tt FROM TkmTickets tt WHERE tt.subModuleId = :subModuleId and tt.subModuleName = :subModuleName")
	TkmTickets findBySubModuleIdAndName(@Param("subModuleId") Integer subModuleId,@Param("subModuleName") String subModuleName);

	@Query("SELECT td FROM TkmDocuments td WHERE td.ticket = :ticket")
	List<TkmDocuments> findDocumentsFromTicketID(@Param("ticket") TkmTickets ticket);

	@Query("SELECT doc FROM ConsumerDocument doc WHERE doc.ecmDocumentId = :documentID")
	ConsumerDocument findConsumerDocumentsByECMID(@Param("documentID") String documentID);

	@Query("SELECT tt FROM TkmTickets tt WHERE tt.id in (SELECT tdoc.ticket.id FROM TkmDocuments tdoc, ConsumerDocument cmrdoc WHERE tdoc.documentPath = cmrdoc.ecmDocumentId " +
			"AND cmrdoc.targetId = :applicantID AND cmrdoc.documentCategory = :documentCategory) AND tt.status IN (:validStatuses)")
	List<TkmTickets> findExistingTicket(@Param("applicantID") Long applicantID, @Param("documentCategory") String documentCategory, @Param("validStatuses") List<String> validStatuses);
}
