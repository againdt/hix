/**
 * File 'IFormularyRepository' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 18, 2013 6:33:34 PM
 */
package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Formulary;

/**
 * Repository class 'IFormularyRepository' to handle persistence of 'Formulary' records.
 *
 * @author santanu
 * @version 1.0
 * @since Sept 27, 2013
 *
 */
public interface IFormularyRepository extends JpaRepository<Formulary, Integer> {
	
	Formulary findById(int id);
}
