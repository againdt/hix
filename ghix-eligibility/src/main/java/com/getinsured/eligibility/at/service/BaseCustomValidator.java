package com.getinsured.eligibility.at.service;

import java.util.ArrayList;
import java.util.Date;

import com.getinsured.timeshift.util.TSDate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonContactInformationAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxFilerType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxReturnType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.StructuredAddressType;
import com.getinsured.iex.erp.org.nmhix.ssa.person.StateCodeEnum;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 *
 */
public abstract class BaseCustomValidator implements CustomValidator {

	private static final String ADDRESS_PATTERN = "[0-9a-zA-Z .'#,/\\u005C-]+";
	
	private static final String CITY_PATTERN = "[0-9a-zA-Z .'#,\\u005C-]+";

	@Autowired
	@Qualifier("zipCodeService")
	private ZipCodeService zipCodeService;

	private static final String MAIL = "Mail";

	private static final Matcher ADDRESS_REGEX = Pattern.compile(ADDRESS_PATTERN).matcher("");

	private static final Matcher CITY_REGEX = Pattern.compile(CITY_PATTERN).matcher("");

	@Override
	public List<String> validate(AccountTransferRequestPayloadType request) {
		List<String> errorList = new ArrayList<>();
		processValidation(request, errorList);
		return errorList;
	}

	abstract void validateEligibilityDetails(AccountTransferRequestPayloadType request, List<String> errorList);

	abstract boolean isZipValidationApplicable(String state);

	private void processValidation(AccountTransferRequestPayloadType request, List<String> errorList) {
		validateApplicationDates(request, errorList);
		final PersonType primaryTaxFiler =extractPrimaryTaxFilerReference(request);
		final List<String> insuranceApplicants = extractInsuranceApplicantIds(request);
		validateAllMemberData(request, errorList, insuranceApplicants);
		validateAddressForAllMembers(request, errorList);
		validatePrimaryTaxFiler(request, primaryTaxFiler, errorList);
		validateEligibilityDetails(request, errorList);
		validateMedicaidCHIPUnique(request, errorList);

	}

	/**
	 * added to match ind19 validation -pravin
	 *
	 * @param request
	 * @param errorList
	 */
	private void validateAddressForAllMembers(AccountTransferRequestPayloadType request, List<String> errorList) {

		List<PersonType> persons = request.getPerson();

		for (PersonType personType : persons) {
			validateAddress(personType, errorList);
		}

	}

	private void validateAddress(PersonType applicant, List<String> errorList) {

		if (applicant.getPersonAugmentation() != null) {
			String contactCode;
			final List<PersonContactInformationAssociationType> listPersonContactInformationAssociation = applicant
					.getPersonAugmentation().getPersonContactInformationAssociation();
			final int size = ReferralUtil.listSize(listPersonContactInformationAssociation);
			PersonContactInformationAssociationType personContactInformationAssociationType = null;
			StructuredAddressType structuredAddress;
			for (int i = 0; i < size; i++) {
				personContactInformationAssociationType = listPersonContactInformationAssociation.get(i);
				if (personContactInformationAssociationType.getContactInformationCategoryCode() != null
						&& personContactInformationAssociationType.getContactInformationCategoryCode()
								.getValue() != null) {
					contactCode = ReferralUtil.validString(personContactInformationAssociationType
							.getContactInformationCategoryCode().getValue().value());

					if (ContactInformationCategoryCodeSimpleType.HOME.value().equals(contactCode)
							|| ContactInformationCategoryCodeSimpleType.MAILING.value().equals(contactCode)) {
						if (personContactInformationAssociationType.getContactInformation() != null) {
							if (personContactInformationAssociationType.getContactInformation()
									.getContactMailingAddress() != null
									&& personContactInformationAssociationType.getContactInformation()
											.getContactMailingAddress().getStructuredAddress() != null) {
								structuredAddress = personContactInformationAssociationType.getContactInformation()
										.getContactMailingAddress().getStructuredAddress();
								if (structuredAddress.getLocationStreet() != null
										&& structuredAddress.getLocationStreet().getStreetFullText() != null
										&& structuredAddress.getLocationStreet().getStreetFullText()
												.getValue() != null) {
									if (!(structuredAddress.getLocationStreet().getStreetFullText().getValue()
											.length() <= ReferralProcessingConstants.ADDRESS_LINE_1_MAX_LENGTH
											&& ADDRESS_REGEX.reset(structuredAddress.getLocationStreet()
													.getStreetFullText().getValue()).matches())) {
										errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA,
												new Object[] { contactCode + " Address 1 ", applicant.getId() }));
									}
								}
								if (structuredAddress.getAddressSecondaryUnitText() != null
										&& structuredAddress.getAddressSecondaryUnitText().getValue() != null) {
									if (!(structuredAddress.getAddressSecondaryUnitText().getValue()
											.length() <= ReferralProcessingConstants.ADDRESS_LINE_2_MAX_LENGTH
											&& ADDRESS_REGEX
													.reset(structuredAddress.getAddressSecondaryUnitText().getValue())
													.matches())) {
										errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA,
												new Object[] { contactCode + " Address 2 ", applicant.getId() }));
									}
								}

								if (structuredAddress.getLocationCityName() != null
										&& structuredAddress.getLocationCityName().getValue() != null) {
									if (!(structuredAddress.getLocationCityName().getValue()
											.length() <= ReferralProcessingConstants.CITY_MAX_LENGTH
											&& CITY_REGEX.reset(structuredAddress.getLocationCityName().getValue())
													.matches())) {
										errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA,
												new Object[] { contactCode + " City ", applicant.getId() }));
									}
								}

								if (!(structuredAddress.getLocationStateUSPostalServiceCode() != null
										&& structuredAddress.getLocationStateUSPostalServiceCode()
												.getValue() != null)) {
									errorList.add(ReferralUtil.formatMessage(MSG_INVALID_DATA,
											new Object[] { contactCode + " State ", applicant.getId() }));
								}

							}
						}
					}
				}
			}
		}

	}

	private void validateMedicaidCHIPUnique(AccountTransferRequestPayloadType request, List<String> errorList) {
		String identificationId = null;
		List<PersonType> personList = request.getPerson();
		boolean idValid = true;
		List<String> ids = new ArrayList<String>();
		for (PersonType personType : personList) {
			identificationId = null;
			if (personType.getPersonAugmentation() != null
					&& personType.getPersonAugmentation().getPersonMedicaidIdentification() != null && personType
							.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID() != null) {
				identificationId = personType.getPersonAugmentation().getPersonMedicaidIdentification()
						.getIdentificationID().getValue();
			}

			if (identificationId == null) {
				if (personType.getPersonAugmentation() != null
						&& personType.getPersonAugmentation().getPersonCHIPIdentification() != null && personType
								.getPersonAugmentation().getPersonCHIPIdentification().getIdentificationID() != null) {
					identificationId = personType.getPersonAugmentation().getPersonCHIPIdentification()
							.getIdentificationID().getValue();
				}
			}

			if (identificationId != null) {
				if (ids.indexOf(identificationId) != -1) {
					idValid = false;
				} else {
					ids.add(identificationId);
				}
			}
		}
		if (!idValid) {
			errorList.add(MSG_ID_DUPLICATE);
		}
	}

	private void validateApplicationDates(AccountTransferRequestPayloadType request, List<String> errorList) {
		boolean blnSignatureDate = false;
		if (request.getInsuranceApplication().getApplicationSubmission() != null
				&& request.getInsuranceApplication().getApplicationSubmission().getActivityDate() != null
				&& request.getInsuranceApplication().getApplicationSubmission().getActivityDate().getDate() != null) {
			blnSignatureDate = true;
		}

		if (!blnSignatureDate) {
			errorList.add(
					ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "Application Submission Date" }));
		} else {
			if (!isValidDate(
					request.getInsuranceApplication().getApplicationSubmission().getActivityDate().getDate())) {
				errorList.add(
						ReferralUtil.formatMessage(MSG_INVALID_DATE, new Object[] { "Application Submission Date" }));
			}
		}

		boolean blnStartDate = false;
		if (request.getInsuranceApplication().getApplicationCreation() != null
				&& request.getInsuranceApplication().getApplicationCreation().getActivityDate() != null
				&& request.getInsuranceApplication().getApplicationCreation().getActivityDate().getDate() != null) {
			blnStartDate = true;
		}

		if (!blnStartDate) {
			errorList
					.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { "Application Creation Date" }));
		} else {
			if (!isValidDate(request.getInsuranceApplication().getApplicationCreation().getActivityDate().getDate())) {
				errorList.add(
						ReferralUtil.formatMessage(MSG_INVALID_DATE, new Object[] { "Application Creation Date" }));
			}
		}
	}

	static boolean isValidDate(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date source) {
		try {
			source.getValue().toGregorianCalendar(); // If the Date Data from source is invalid this method throws a
														// nullpointerexception
		} catch (Exception pe) {
			return false;
		}

		return true;
	}

	static DateTime getDate(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date source) {
		return new DateTime(source.getValue().toGregorianCalendar().getTime());
	}

	static boolean isFutureDate(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date source) {

		DateTime eventDate = new DateTime(source.getValue().toGregorianCalendar().getTime());
		DateTime currentDate = new DateTime(new TSDate());

		return eventDate.isAfter(currentDate) ? true : false;
	}

	private void validatePrimaryTaxFiler(AccountTransferRequestPayloadType request, PersonType primaryTaxFiler,
			List<String> errorList) {
		// added business validation for null PTF 
		if(null == primaryTaxFiler) {
			// For IDAHO, accept and close ATs that come without PTF add a state check
			String stateCode = DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			if (!stateCode.equalsIgnoreCase("ID")) {
				errorList.add(
						ReferralUtil.formatMessage(MSG_MANDATORY_FIELD, new Object[] { PRIMARYTAXFILER }));
			}			
		} else {
			if (null!=primaryTaxFiler && primaryTaxFiler.getPersonAugmentation() != null) {
				String contactCode;
				boolean mailingAddressPresent = false;
				final List<PersonContactInformationAssociationType> listPersonContactInformationAssociation = primaryTaxFiler
						.getPersonAugmentation().getPersonContactInformationAssociation();
				final int size = ReferralUtil.listSize(listPersonContactInformationAssociation);
				PersonContactInformationAssociationType personContactInformationAssociationType = null;
				final boolean[] homeValid = { false, false, false, false, false };
				final boolean[] mailingValid = { false, false, false, false, false };

				for (int i = 0; i < size; i++) {
					personContactInformationAssociationType = listPersonContactInformationAssociation.get(i);
					if (personContactInformationAssociationType.getContactInformationCategoryCode() != null
							&& personContactInformationAssociationType.getContactInformationCategoryCode()
									.getValue() != null) {
						contactCode = ReferralUtil.validString(personContactInformationAssociationType
								.getContactInformationCategoryCode().getValue().value());

						if (contactCode.equals(ContactInformationCategoryCodeSimpleType.HOME.value())) {
							validateAddressForPrimaryTaxFiler(personContactInformationAssociationType, primaryTaxFiler,
									errorList, ContactInformationCategoryCodeSimpleType.HOME.value(), homeValid);
						} else if (contactCode.equals(ContactInformationCategoryCodeSimpleType.MAILING.value())) {
							mailingAddressPresent = true;
							validateAddressForPrimaryTaxFiler(personContactInformationAssociationType, primaryTaxFiler,
									errorList, ContactInformationCategoryCodeSimpleType.MAILING.value(), mailingValid);
						}
					}
				}

				populateErrorList(homeValid, errorList, primaryTaxFiler.getId(),
						ContactInformationCategoryCodeSimpleType.HOME.value());

				if (mailingAddressPresent) {
					populateErrorList(mailingValid, errorList, primaryTaxFiler.getId(),
							ContactInformationCategoryCodeSimpleType.MAILING.value());
				}

				if (request.getInsuranceApplication().getSSFPrimaryContact().getSSFPrimaryContactPreferenceCode() != null
						&& request.getInsuranceApplication().getSSFPrimaryContact().getSSFPrimaryContactPreferenceCode()
								.getValue() != null) {
					if (request.getInsuranceApplication().getSSFPrimaryContact().getSSFPrimaryContactPreferenceCode()
							.getValue().value().equals(MAIL) && !mailingAddressPresent) {
						populateErrorList(mailingValid, errorList, primaryTaxFiler.getId(),
								ContactInformationCategoryCodeSimpleType.MAILING.value());

					}
				}
			}
		}
		
	}

	private void validateAddressForPrimaryTaxFiler(
			PersonContactInformationAssociationType personContactInformationAssociationType,
			PersonType primaryApplicant, List<String> errorList, String addressType, boolean[] homeValid) {

		boolean blnvalidateZip = false;
		boolean blnZipValidationApplicable = addressType
				.equalsIgnoreCase(ContactInformationCategoryCodeSimpleType.HOME.value());
		StructuredAddressType structuredAddress;
		String stateUSPostalServiceCode = "";
		if (personContactInformationAssociationType.getContactInformation() != null) {
			if (personContactInformationAssociationType.getContactInformation().getContactMailingAddress() != null
					&& personContactInformationAssociationType.getContactInformation().getContactMailingAddress()
							.getStructuredAddress() != null) {
				structuredAddress = personContactInformationAssociationType.getContactInformation()
						.getContactMailingAddress().getStructuredAddress();
				if (structuredAddress.getLocationStreet() != null
						&& structuredAddress.getLocationStreet().getStreetFullText() != null
						&& structuredAddress.getLocationStreet().getStreetFullText().getValue() != null) {
					homeValid[0] = true;
				}

				if (structuredAddress.getLocationStateUSPostalServiceCode() != null
						&& structuredAddress.getLocationStateUSPostalServiceCode().getValue() != null) {
					homeValid[1] = true;
					stateUSPostalServiceCode = structuredAddress.getLocationStateUSPostalServiceCode().getValue()
							.value();
					// blnZipValidationApplicable =
					// isZipValidationApplicable(ReferralUtil.enumValueAsString(StateCodeEnum.class,
					// structuredAddress.getLocationStateUSPostalServiceCode().getValue().value()));
				}

				if (structuredAddress.getLocationCityName() != null
						&& structuredAddress.getLocationCityName().getValue() != null) {
					homeValid[2] = true;
				}

				if (structuredAddress.getLocationPostalCode() != null
						&& structuredAddress.getLocationPostalCode().getValue() != null) {
					homeValid[3] = true;
				}

				String stateCode = DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				if (addressType.equals(ContactInformationCategoryCodeSimpleType.MAILING.value())
						&& !StringUtils.equalsIgnoreCase(stateCode, stateUSPostalServiceCode)) {
					homeValid[4] = true;
				} else if (structuredAddress.getLocationCountyCode() != null
						&& structuredAddress.getLocationCountyCode().getValue() != null) {

					String countyCode = structuredAddress.getLocationCountyCode().getValue();
					if (StringUtils.isBlank(countyCode) || StringUtils.length(countyCode) != 3) {
						errorList.add(MSG_INVALID_COUNTY);
					}
					homeValid[4] = true;
					
				}
   
			}
		}
	}

	private void populateErrorList(boolean[] homeValid, List<String> errorList, String applicantId,
			String categoryCode) {
		if (!homeValid[0]) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { categoryCode + " Address 1", applicantId }));
		}

		if (!homeValid[1]) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { categoryCode + " State", applicantId }));
		}

		if (!homeValid[2]) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { categoryCode + " City", applicantId }));
		}

		if (!homeValid[3]) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { categoryCode + " Postal Code", applicantId }));
		}
		if (!homeValid[4]) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { categoryCode + " County Code", applicantId }));
		}
	}

	@SuppressWarnings("unused")
	private void validateRelationshipData(PersonType primaryTaxFiler, Map<String, Integer> persons,
			List<String> errorList) {
		if (primaryTaxFiler.getPersonAugmentation() != null
				&& primaryTaxFiler.getPersonAugmentation().getPersonAssociation() != null) {
			final List<PersonAssociationType> personAssociationType = primaryTaxFiler.getPersonAugmentation()
					.getPersonAssociation();
			final int size = ReferralUtil.listSize(personAssociationType);
			persons.put(primaryTaxFiler.getId(), 1); // Done for Primary
			for (int j = 0; j < size; j++) {
				if (personAssociationType.get(j).getFamilyRelationshipCode() != null
						&& personAssociationType.get(j).getFamilyRelationshipCode().getValue() != null) {
					persons.put(
							((PersonType) personAssociationType.get(j).getPersonReference().get(0).getRef()).getId(),
							1);
				}
			}
		}

		if (persons.containsValue(0)) {
			errorList.add(MSG_RELATIONSHIP);
		}

	}

	private Map<String, Integer> validateAllMemberData(AccountTransferRequestPayloadType request,
			List<String> errorList, List<String> insuranceApplicants) {
		final Map<String, Integer> persons = new HashMap<String, Integer>();
		final List<PersonType> listPersonType = request.getPerson();
		final int size = ReferralUtil.listSize(listPersonType);
		PersonType person = null;
		List<String> ssn = new ArrayList<String>();
		boolean ssnValid = true;
		String tempSsn;
		for (int i = 0; i < size; i++) {
			person = listPersonType.get(i);

			persons.put(person.getId(), 0);

			validateMemberIdentity(person, errorList, insuranceApplicants);

			if (person.getPersonSSNIdentification() != null
					&& ReferralUtil.listSize(person.getPersonSSNIdentification()) != 0) {
				if (person.getPersonSSNIdentification().get(0).getIdentificationID() != null
						&& person.getPersonSSNIdentification().get(0).getIdentificationID().getValue() != null) {
					tempSsn = person.getPersonSSNIdentification().get(0).getIdentificationID().getValue();
					if (ssn.indexOf(tempSsn) != -1) {
						ssnValid = false;
					} else {
						ssn.add(tempSsn);
					}
				}
			}
		}

		if (!ssnValid) {
			errorList.add(MSG_SSN_DUPLICATE);
		}
		return persons;
	}

	private List<String> extractInsuranceApplicantIds(AccountTransferRequestPayloadType source) {
		List<String> insuranceApplicantIdsList = new ArrayList<String>();
		final List<InsuranceApplicantType> insuranceApplicantList = source.getInsuranceApplication()
				.getInsuranceApplicant();
		final int size = ReferralUtil.listSize(insuranceApplicantList);
		InsuranceApplicantType insuranceApplicant;
		PersonType personType;
		for (int i = 0; i < size; i++) {
			insuranceApplicant = insuranceApplicantList.get(i);
			if (insuranceApplicant.getRoleOfPersonReference() != null) {
				if (insuranceApplicant.getRoleOfPersonReference().getRef() instanceof PersonType) {
					personType = (PersonType) insuranceApplicant.getRoleOfPersonReference().getRef();
					insuranceApplicantIdsList.add(personType.getId());
				}
			}
		}
		return insuranceApplicantIdsList;
	}

	private void validateMemberIdentity(PersonType person, List<String> errorList, List<String> insuranceApplicants) {
		boolean blnFName = false;
		boolean blnLName = false;
		boolean blnDob = false;
		boolean blnMarried = false;
		boolean blnGender = false;

		if (person.getPersonName() != null && ReferralUtil.listSize(person.getPersonName()) != 0) {
			blnFName = person.getPersonName().get(0).getPersonGivenName() != null
					&& person.getPersonName().get(0).getPersonGivenName().getValue() != null;
			blnLName = person.getPersonName().get(0).getPersonSurName() != null
					&& person.getPersonName().get(0).getPersonSurName().getValue() != null;
		}

		blnDob = person.getPersonBirthDate() != null && person.getPersonBirthDate().getDate() != null
				&& person.getPersonBirthDate().getDate().getValue() != null;

		blnGender = person.getPersonSexText() != null && person.getPersonSexText().getValue() != null;

		if (insuranceApplicants.indexOf(person.getId()) != -1) {
			blnMarried = person.getPersonAugmentation() != null
					&& person.getPersonAugmentation().getPersonMarriedIndicator() != null;
		} else {
			blnMarried = true;
		}

		if (!blnFName) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { "First Name", person.getId() }));
		}

		if (!blnLName) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { "Last Name", person.getId() }));
		}

		if (!blnDob) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { "Date of Birth", person.getId() }));
		}

		if (!blnGender) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { "Gender", person.getId() }));
		}

		if (!blnMarried) {
			errorList.add(ReferralUtil.formatMessage(MSG_MANDATORY_FIELD_APPLICANT,
					new Object[] { "Married Indicator", person.getId() }));
		}
	}

	private PersonType extractPrimaryApplicantId(AccountTransferRequestPayloadType source) {
		return (PersonType) source.getInsuranceApplication().getSSFPrimaryContact().getRoleOfPersonReference().getRef();
	}

	private PersonType extractPrimaryTaxFilerReference(AccountTransferRequestPayloadType request) {
		final int size = ReferralUtil.listSize(request.getTaxReturn());
		if (size != 0) {
			final TaxReturnType taxReturnType = request.getTaxReturn().get(0);
			if (taxReturnType.getTaxHousehold() == null) {
				return null;
			}
			final TaxHouseholdType taxHouseholdType = taxReturnType.getTaxHousehold();

			final List<?> taxFilerList = taxHouseholdType.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent();
			final int tfSize = ReferralUtil.listSize(taxFilerList);
			JAXBElement<?> taxFilerType = null;

			// Primary Taxfiler & Dependents
			for (int i = 0; i < tfSize; i++) {
				taxFilerType = (JAXBElement<?>) taxFilerList.get(i);
				if (taxFilerType != null) {
					if (taxFilerType.getName().getLocalPart().equals(PRIMARYTAXFILER)) {
						JAXBElement<TaxFilerType> primaryTaxFiler = (JAXBElement<TaxFilerType>) taxFilerType;
						if (primaryTaxFiler.getValue() != null
								&& primaryTaxFiler.getValue().getRoleOfPersonReference() != null) {
							return ((PersonType) primaryTaxFiler.getValue().getRoleOfPersonReference().getRef());
						}
					}
				}
			}
		}
		return null;
	}
	
	
}
