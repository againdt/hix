package com.getinsured.ssap.exception;

/**
 * Types of Invalid States for SSAP.
 *
 * @author Yevgen Golubenko
 * @since 1/3/19
 */
public enum InvalidStateType implements ExceptionType
{
  /**
   * Unspecified exception type.
   */
  NOT_SPECIFIED,

  /**
   * Means that SSAP already started for given household and coverage year.
   */
  APPLICATION_ALREADY_STARTED_FOR_THIS_HOUSEHOLD_AND_YEAR,

  /**
   * Means that SSAP's household id is not the same as id of the current user, thus
   * we can't modify SSAP.
   */
  PERMISSION_DENIED_LOGGEDIN_USER_ID_AND_HOUSEHOLD_ID_DONT_MATCH,

  /**
   * Means that SEP application cannot be modified by CSR.
   */
  SPECIAL_ENROLLMENT_APPLICATION_CANNOT_BE_MODIFIED_BY_CSR,

  /**
   * Means that SSAP with Enrolled or Active status cannot be modified by CSR.
   */
  ENROLLED_OR_ACTIVE_SSAP_CANNOT_BE_MODIFIED_BY_CSR,

  /**
   * Means that SSAP was already E-Signed and cannot be modified again.
   */
  APPLICATION_ALREADY_ESIGNED,

  /**
   * Unable to update applicant for given SSAP
   */
  CANT_UPDATE_APPLICANT_INFO,

  /**
   * Unable to create applicant for given SSAP
   */
  CANT_CREATE_APPLICANT_INFO,

  /**
   * SSAP doesn't have any tax households.
   */
  APPLICATION_HAS_NO_TAX_HOUSEHOLDS,

  /**
   * No application found for given GUID/CaseNumber
   */
  NO_APPLICATION_FOUND_FOR_GIVEN_GUID_OR_CASENUMBER,

  /**
   * Application not found for given id.
   */
  APPLICATION_NOT_FOUND_FOR_GIVEN_ID,

  /**
   * Application JSON is invalid.
   */
  APPLICATION_JSON_INVALID;
}
