<!-- 
 @author raja : viewIssuerDetails.jsp
 @UserStory : HIX-1807
 @Jira-id : HIX-4186
 -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" ></script>
<div class="gutter10">
 <c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/manageissuer"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
        	<li><spring:message  code="pgheader.issuerDetails"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
	<div class="centered_all">
		<div class="span3">
			<img alt="logo ${issuerObj.name}" src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>" class="resize-img img_center_h" />
		</div>
		<div class="span9">
			<h1 id="skip">${issuerObj.name}</h1>
		</div>
	</div>
	</div>
	<div class="row-fluid">
			<div class="span3" id="sidebar">
			<div class="header">
               <h4><spring:message  code="pgheader.issuerAbout"/></h4>
            </div>
			<jsp:include page="issuerDetailsLeftNav.jsp">
        			<jsp:param name="pageName" value="issuerDetails"/>
			</jsp:include>
						
	            <!--  show date picker start here -->
	            <div class="margin10">
	                 <strong><spring:message  code="label.effectiveStartDate"/></strong>
	                 <fmt:formatDate pattern="MM/dd/yyyy" value="${sysDate}" var="systemDate" />
					 <div class="input-append date date-picker" id="date" data-date-format="mm/dd/yyyy">
	                     <input class="span10" type="text" id="effectiveStartDate" name="effectiveStartDate" pattern="MM/DD/YYYY">
	                	 <span class="add-on"><i class="icon-calendar"></i></span>
					 </div>
	            </div>
	            <!--  show date picker end here -->
             	<div class="margin10">
             	   	 <a class="btn btn-primary btn-addpayment" id="redirectTo" onclick="checkeffectiveDate();" target="_blank"><spring:message  code="pgheader.viewConsumerShopping"/></a> 
                </div>
                		
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.issuerDetails"/></h4>
                    <%-- <c:if test="${exchangeType != 'PHIX'}"> remove condition as per HIX-17647 --%>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/details/edit/${encIssuerId}"/>"><spring:message  code="label.edit"/></a>
                    <%-- </c:if> --%>
                </div>
				
					<form class="form-horizontal" id="frmIssuerDetailsInfo" name="frmIssuerDetailsInfo" action="#" method="POST">
					<df:csrfToken/>
					<div class="profile">
						<table class="table table-border-none verticalThead">							
								<tr>
									<th class="txt-right span4"><spring:message  code="label.name"/></th>
									<td><strong>${issuerObj.name}</strong></td>
								</tr>
								<tr>
									<th class="txt-right span4"><spring:message  code="label.shortName"/></th>
									<td><strong>${issuerObj.shortName}</strong></td>
								</tr>							
								<tr>
									<th class="txt-right"><spring:message  code="label.NAICCompanyCode"/></th>
									<td><strong>${issuerObj.naicCompanyCode}</strong></td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.NAICGroupCode"/></th>
									<td><strong>${issuerObj.naicGroupCode}</strong>
									</td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.FederalEmployerId"/></th>
									<td><strong>${issuerObj.federalEin}</strong></td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.HIOSIssuerId"/></th>
									<td><strong>${issuerObj.hiosIssuerId}</strong></td>
								</tr>
								<%-- <tr>
									<th class="txt-right span4"><spring:message  code="label.txn834Version"/></th>
									<td><strong>${issuerObj.txn834Version}</strong></td>
								</tr>
								<tr>
									<th class="txt-right span4"><spring:message  code="label.txn820Version"/></th>
									<td><strong>${issuerObj.txn820Version}</strong></td>
								</tr> --%>
							</tbody>
						</table><br />
                        <div class="header">
                            <h4><spring:message  code="pgheader.issuerAddress"/></h4>
                        </div>
                        <table class="table table-border-none verticalThead">
                            <tr>
                                <th class="txt-right span4"><spring:message  code="label.addressLine1"/></th>
                                <td><strong>${issuerObj.addressLine1}</strong></td>
                            </tr>
                            <tr>
                                <th class="txt-right span4"><spring:message  code="label.addressLine2"/></th>
                                <td><strong>${issuerObj.addressLine2}</strong></td>
                            </tr>
                            <tr>
                                <th class="txt-right"><spring:message  code="label.city"/></th>
                                <td><strong>${issuerObj.city}</strong>
                                </td>
                            </tr>
                            <tr>
                                <th class="txt-right"><spring:message  code="label.state"/></th>
                                <td><strong>${issuerObj.state}</strong></td>
                            </tr>
                            <tr>
                                <th class="txt-right"><spring:message  code="label.zipCode"/></th>
                                <td><strong>${issuerObj.zip}</strong></td>
                            </tr>
                           <!-- don't show start application button except issuer status = 'register' --> 
                           
                            <!-- tr>
                                <td class="txt-right span4">&nbsp;</td>
                                <td><a href='<c:url value="/admin/issuer/financial/info/${issuerObj.id}"/>' class="btn btn-primary"><spring:message  code="label.next"/></a></td>
                            </tr -->
                             
						</table>
					</div>			
				</form>
			
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();	
		var replaceExpr = /html"\>/gi;
	$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
				
	$('.date-picker').datepicker({
		startDate: '+'+ '${systemDate}' + 'd',
		autoclose: true,
	    format: 'mm/dd/yyyy'
	});	
		
	});
function checkeffectiveDate(){
	var effectiveStartDate = $('#effectiveStartDate').val();
	if(isDate(effectiveStartDate)){
		if(isValidDate(effectiveStartDate)){
			if(effectiveStartDate != ''){           
				$.ajax({
					url: "<c:url value='/setEffectiveDate/anonymous'/>",
					data: {"effectiveStartDate": effectiveStartDate, "csrftoken": $("#csrftoken").val(), "issuerId": ${issuerObj.id}},
					type: "POST",
					success: function(response){
					if(response == "sessionExpire"){
						resultResp = response;
						window.location.href = "${pageContext.request.contextPath}" + "/account/user/login?confirmId=Y";
					}else{
						resultResp = response;
						if('${exchangeState}'.toUpperCase() == 'CA'){
							window.open("${pageContext.request.contextPath}" + "/private/quoteform#/");
						}else{
							window.open("${pageContext.request.contextPath}" + "/preeligibility#/");
						}
					}
				},
				error: function (response){
					resultResp = response;
					alert("Technical Issue Occured for Effective Start Date");
					window.location.href = "${pageContext.request.contextPath}" + "/account/user/login?confirmId=Y";
					}
				});
			}else{
				alert("Please Select Effective Start Date");
				$('#redirectTo').attr('href',"#");
				$('#redirectTo').attr('target',"_top");
			}
		}else{
			alert("Effective Date is always Current Date or Future Date");
			$('#redirectTo').attr('href',"#");
			$('#redirectTo').attr('target',"_top");			
		}
	}else{
		alert("Enter Valid Effective Date");
		$('#redirectTo').attr('href',"#");
		$('#redirectTo').attr('target',"_top");
	}
} 

function isValidDate(effectiveStartDate){
	var effectiveStartDateArr = effectiveStartDate.split('/');
	var sysDate =  '${systemDate}';
	var sysDateArr = sysDate.split('/');
	
	var mydate = new Date(effectiveStartDateArr[2], effectiveStartDateArr[0]-1, effectiveStartDateArr[1]);
	var currentDate = new Date(sysDateArr[2], sysDateArr[0]-1, sysDateArr[1]);
	
	if(currentDate <= mydate){
		return true;
	}else{
		return false;
	}
}


function isDate(txtDate)
{
  var currVal = txtDate;
  if(currVal == '')
    return false;
  
  //Declare Regex  
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (dtArray == null)
     return false;
 
  //Checks for mm/dd/yyyy format.
  dtMonth = dtArray[1];
  dtDay= dtArray[3];
  dtYear = dtArray[5];

  if(dtMonth < 1)
      return false;
  else if(dtMonth > 12)
	  return false;
  else if (dtDay < 1)
      return false;
  else if(dtDay > 31)
	  return false;
  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
      return false;
  else if (dtMonth == 2)
  {
     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
     if (dtDay> 29 || (dtDay ==29 && !isleap))
          return false;
  }
  return true;
}

</script>

