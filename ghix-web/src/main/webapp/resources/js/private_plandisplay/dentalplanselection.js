(function(){
	_.templateSettings = {
		    interpolate: /\<\@\=(.+?)\@\>/gim,
		    evaluate: /\<\@(.+?)\@\>/gim
		};
	window.App = {};
	App.collections = {};
	App.models = {};
	App.views = {};
	App.backbone = {};
	
	// Defer initialization until doc ready.
	$(function(){
		var collection = new App.collections.dentalPlans();
		var cartCollection = new App.collections.Carts();
		var cartModel = new App.models.Cart();
		App.views.dentalPlans = new App.views.dentalPlans({collection:collection,cartModel:cartModel,cartCollection:cartCollection});
	});

})();