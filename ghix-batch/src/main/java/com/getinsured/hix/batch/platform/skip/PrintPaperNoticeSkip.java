package com.getinsured.hix.batch.platform.skip;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component("printPaperNoticeSkip")
public class PrintPaperNoticeSkip {
	
	
	
	private Map<String,String> skippedNoticeMap=null;
	private static final String format = "yyyy.MM.dd HH:mm:ss z";
	
	public PrintPaperNoticeSkip() {
		skippedNoticeMap= Collections.synchronizedMap(new HashMap<String, String>());
	}

	public void putToSkippedHouseholdMap(String key, String value){
		skippedNoticeMap.put(key, value);
	}
	public void putAllToSkippedNoticeMap(Map<String,String> failedNoticeMap){
		this.skippedNoticeMap.putAll(failedNoticeMap);
	}	
	
	public Map<String,String> getSkippedNoticeMap(){
		return skippedNoticeMap;
	}
	
	public void clearSkippedNoticeMap(){
		this.skippedNoticeMap.clear();
	}
	
	public String toString(){
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String dateStr = formatter.format(new Date(System.currentTimeMillis()));
		Set<Entry<String, String>> entrySet = this.skippedNoticeMap.entrySet();
		Iterator<Entry<String, String>> cursor = entrySet.iterator();
		Entry<String, String> entry;
		StringBuilder builder = new StringBuilder(1024);
		int skipCount = 0;
		builder.append("--------------- NOTICES SKIPPED ON "+dateStr+" ---------------------\n");
		while(cursor.hasNext()){
			skipCount++;
			builder.append("[SKIPPED RECORD-"+skipCount+"]\n");
			entry = cursor.next();
			builder.append("\t\tNotice ID:"+entry.getKey()+"\n");
			builder.append("\t\tError:"+entry.getValue()+"\n");
		}
		builder.append("--------------------------------------------------------------------\n");
		builder.trimToSize();
		return  builder.toString();
		
	}
}
