
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">


<div class="row-fluid" >
    <div class="row-fluid" id="titlebar">
      <div class="span3">
        <a class="btn btn-link" href="e1-1-start-app">Back </a>
      </div>
      <div class="span9 pull-left">
        <h1>Review &amp; Sign</h1>
      </div>
    </div>
    <!--titlebar-->
    <div class="row-fluid" id="main">
      <div class="span3 gutter10" id="sidebar">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
        <li> <a href="e1-1-start-app"> <i class="icon-ok"></i> Start Your Application</a></li>
          <li><a href="e3-build-your-household">  <i class="icon-ok"></i> Build Your Household</a></li>
          <li><a href="e4-household-income"> <i class="icon-ok"></i> Your Household Income </a></li>
          <li><a href="e5-additional-questions"> <i class="icon-ok"></i> Additional Questions </a></li>
          <li class="active"><a href="e6-review-declare-file"> <i class="icon-ok"></i> Review &amp; Sign </a></li>
        </ul>
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
      <img src="../resources/eligibilitynm/img/build.png" alt="header image" class="img-polaroid">
        <div class="margin-right"> <!-- gutter10  -->
           
          <div class="span8">
            <h4>Coming up in this section1</h4>
            <p>In this section, you will review your application for completeness and accuracy. Once you finish your review, you can give your e-signature and file your application. In most cases, you will receive a final eligibility determination within minutes.</p>
          </div>
          <div class="span4">
            <h5>You may need</h5>
            <hr>
            <p> <i class="icon-ok"></i> All previously required documents.</p>
            <p> <i class="icon-time"></i> Estimated time needed to complete this section: 10 minutes.</p>
          </div>
           
            <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#review1">Next</a> 
	                  <a class="btn edit secondary">Next</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
           
           
           <div id="review1" class="section">
              <div class="opacity">
              <div class="header">
               <h4>Review Application </h4>
               </div>
               
              <p class="gutter10">Carefully review the information on this page to ensure its accuracy. Make changes if necessary. Once everything is correct, please sign and file your application.</p>

              <table class="table">
                <tr>
                  <td width="43%">Name<br>
                  <strong>Stan Jacobs</strong></td>

                  <!--<td width="53%"><br><a href="#">Edit name</a></td>-->
                  <td width="53%"><br><p class="blue">Edit name</p></td>
                  <td width="4%"></td>
                </tr>
                <tr>
                  <td>Residential Address<br>
                    <strong>1971 Homer Ave.<br>
					Boise, ID 83725</strong></td>

                  <td>Home Phone<br>
                    <strong>(208) 555-1212</strong><br>
                    Mobile Phone<br>
                    <strong>(208) 555-1212</strong></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Preferred method of contact<br>
                    <strong>Email</strong></td>

                  <td>Receive alerts electronically?<br>
                    <strong>Email  </strong></td>  <td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Spoken language<br>
                    <strong>English</strong></td>

                  <td>Written language<br>
                    <strong>English</strong></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Applying for Health Coverage?<br>
                    <strong>No</strong></td>

                  <td>Applying for help paying for coverage?<br>
                    <strong>Yes</strong></td><td><i class="icon-pencil"></i></td>
                </tr>
              </table>
              <h4>Household members</h4>
              <table class="table">
                <tr>
                  <td width="42%">Name<br>
                  <strong>Janet Jacobs</strong></td>
                  <!--<td width="54%"><a href="#">Delete Person</a><br>
                  <a href="#">Edit name</a></td><td width="4%"></td>-->
                  <td width="54%" class="blue">Delete Person<br>
                  Edit name</td><td width="4%"></td>
                </tr>
                <tr>
                  <td>Date of birth<br>
                    <strong>06/24/1985</strong><br>
                    Applying for coverage<br>
                  <strong>Yes</strong></td>
                  <td>Relationship to primary contact<br>
                    <strong>Spouse</strong><br></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Citizenship<br>
                    <strong>No</strong></td>
                  <td>Legal Immigration status<br>
                    <strong>Yes</strong><br>
                    Alien Registration Number<br>
                  <strong> Green Card - A 200 345 678</strong></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Sex<br>
                    <strong>Female</strong><br>
                    Ethnicity<br>
                    <strong>Not Hispanic or Latino</strong></td>
                  <td>Race<br>
                    <strong>White</strong></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Pregnant<br>
                  <strong>No</strong></td>
                  <td>Parent of applying child<br>
                    <strong>Joe Jacobs</strong></td><td><i class="icon-pencil"></i></td>
                </tr><tr>
                  <td colspan="2">Is Janet Jacobs a member of a federally recognized American Indian/Alaskan Native tribe?

<br>
                  <strong>No</strong></td>
                  <td><i class="icon-pencil"></i></td>
                </tr>
              </table>
              <table class="table">
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td></td>
                </tr>
                <tr>
                  <td width="42%">Name<br>
                    <strong>Joe Jacobs</strong></td>
                  <!--<td width="54%"><a href="#">Delete Person</a><br>
                  <a href="#">Edit name</a></td><td width="4%"></td>-->
                  <td width="54%" class="blue">Delete Person<br>
                  Edit name</td><td width="4%"></td>
                </tr>
                <tr>
                  <td>Date of birth<br>
                    <strong>04/05/2011</strong><br>
                    Applying for coverage<br>
                  <strong>Yes</strong></td>
                  <td>Relationship to primary contact<br>
                    <strong>Child</strong><br></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Citizenship<br>
                    <strong>Yes</strong><br>
                    Social Security Number<br>
                    <strong> XXX-XX-2974</strong></td>
                  <td>&nbsp;</td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Sex<br>
                    <strong>Male</strong><br>
                    Ethnicity<br>
                    <strong>Not Hispanic or Latino</strong></td>
                  <td>Race<br>
                    <strong>White</strong></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td colspan="2">Is Joe Jacobs a member of a federally recognized American Indian/Alaskan Native tribe?

<br>
                  <strong>No</strong></td>
                  <td><i class="icon-pencil"></i></td>
                </tr>
              </table>
              <table class="table">
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td></td>
                </tr>
                <!--<tr>
                  <td width="61%">Name<br>
                    <strong>Stan Jacobs</strong></td>
                  <td width="35%"><br>
                  <a href="#">Edit name</a></td><td width="4%"></td>
                  <td width="35%" class="blue"><br>
                  Edit name</td><td width="4%"></td>
                </tr>-->
                <tr>
                  <td width="43%">Name<br>
                  <strong>Stan Jacobs</strong></td>

                  <!--<td width="53%"><br><a href="#">Edit name</a></td>-->
                  <td width="53%"><br><p class="blue">Edit name</p></td>
                  <td width="4%"></td>
                </tr>
                <tr>
                  <td>Date of birth<br>
                    <strong>07/28/1982</strong><br>
                    Applying for coverage<br>
                    <strong>No</strong></td>
                  <td><br></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td> Social Security Number<br>
                    <strong> XXX-XX-3265</strong></td>
                  <td>&nbsp;</td><td><i class="icon-pencil"></i></td>
                </tr>
              </table>
              <h4>Tax Filing Status</h4>

              <table class="table">
                <tr>
                  <td width="36%">Stan Jacobs</td>
                  <td width="62%"><strong>Primary Filer</strong></td><td width="2%"><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Janet Jacobs</td>
                  <td><strong>Spouse</strong></td><td><i class="icon-pencil"></i></td>
                </tr>
                <tr>
                  <td>Joe Jacobs</td>
                  <td><strong>Dependent of Stan Jacobs</strong></td>
                  <td><i class="icon-pencil"></i></td>
                </tr>
              </table>




               <h4>Household Address</h4>
              <table class="table">
                <tr>
                  <td width="97%">Residential Address<br>
                     <strong>1971 Homer Ave.<br>
					Boise, ID 83725</strong></td>
<td width="3%"><i class="icon-pencil"></i></td>
                </tr>
              </table>

              <h4>Household income</h4>
              <table class="table">
                <tr>
                  <td width="48%">Current Monthly Income<br>
                    <strong>$4,416.00</strong></td>
                  <td width="50%">2014 Expected Income<br>
                  <strong>$53,000.00</strong></td>
                  <td width="2%"><i class="icon-pencil"></i></td>
                </tr>
              </table>

              <h4>Sign and Submit</h4>
              <div class="control-group">
              	<label class="control-label">No one applying for health coverage on this application is incarcerated (detained or jailed). </label>
              		<div class="controls">
		              <label class="checkbox inline">
		              	<input type="checkbox" checked>
		              Agree </label>
		            <label class="checkbox inline">
		             	 <input type="checkbox" >
		              Disagree </label>
             		</div>
              </div>
              
               <div class="control-group">
              	<label class="control-label">To make it easier to determine my eligibility for help paying for health coverage in future years, I agree to allow the Marketplace to use income data, including information from tax returns, for the next 5 years (the maximum number of years allowed). The Marketplace will send me a notice, let me make any changes, and I can opt out at any time. </label>
              		<div class="controls">
		              <label class="checkbox inline">
		              	<input type="checkbox" checked>
		              Agree </label>
		            <label class="checkbox inline">
		             	 <input type="checkbox" >
		              Disagree </label>
             		</div>
              </div>
              
              <div class="control-group">
              	<label class="control-label">I know that I must tell the program I'll be enrolled in if information I listed on this application changes. I know I can make changes in "My account" in the Marketplace or by calling 1-800-XXX-XXXX. I understand that a change in my information could affect my eligibility for member(s) of my household. </label>
              		<div class="controls">
		              <label class="checkbox inline">
		              	<input type="checkbox" checked>
		              Agree </label>
		            <label class="checkbox inline">
		             	 <input type="checkbox" >
		              Disagree </label>
             		</div>
              </div>
              
              <div class="control-group">
              <label class="control-label">I'm signing this application under penalty of perjury, which means I've provided true answers to all of the questions to the best of my knowledge. I know that I may be subject to penalties under federal law if I intentionally provide false or untrue information. </label>
              		<div class="controls">
		              <label class="checkbox inline">
		              	<input type="checkbox" checked>
		              Agree </label>
		            <label class="checkbox inline">
		             	 <input type="checkbox" >
		              Disagree </label>
             		</div>
              </div>
              </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#review2">Next</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #household .section -->
              
           
           
           

            <div id="review2" class="section">
            <div class="opacity">
            <div class="control-group span5">
            	<label class="control-label">Applicant's E-Signature</label>
            	<div class="controls">
            		<input type="text" class="input-large">
            	</div>
            </div>
           
            <div class="span5">
              <label class="control-label" for="inputDate">Today's date</label>
              <div class="controls">
                <div class="input-append date" data-date="01-01-2013" data-date-format="mm-dd-yyyy">
                  <input class="input-small" size="16" type="text" value="01-01-2013">
                  <span class="add-on"><i class="icon-calendar"></i></span> </div>
              </div>
            </div>

             </div><!-- opacity -->
	             
            <div class="well well-small clear">
              <a type="submit" class="btn btn-primary offset9" href="e9-confirmation">File your Application</a>
            </div>
	            </div><!-- #review2 .section -->   
                
             
             
           </div>
    <!--gutter10-->
  </div>
  <!--rightpanel-->
      
</div>
<!--main-->
</div>

<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script> -->
<!-- <script src="../resources/eligibilitynm/js/jquery-1.7.2.min.js" type="text/javascript"></script> -->
<!-- <script src="../resources/eligibilitynm/js/bootstrap.js" type="text/javascript"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/jquery-ui-1.8.22.custom.min.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/jquery.multiselect.js"></script> -->
<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script> -->
<!-- <script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-modal.js"></script> -->

<script>
    $(document).ready(function () {
        $(".flip").click(function () {
            $(".mailingAddress").fadeToggle();
        });
        $('input, textarea').placeholder();
    });
    $(".next").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        var hash = $(this.hash);
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(hash).closest('.section').show();
        $(currentsection).next().addClass('activediv');
        $(hash).prev('div').removeClass('activediv');
        $('html,body').delay(300).animate({
            scrollTop: $(this.hash).offset().top - 100
        }, 500)
        $(hash).delay(700).prev('div').find('.edit').show();
        $(hash).delay(700).prev('div').find('.next').hide();
    });
    $(".edit").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 1.0
        });
        $(currentsection).find('.cancel, .save').show();
        $('.activediv').animate({
            opacity: 0.45
        });
        $(currentsection).find('.edit').hide();
    });
    $(".save").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $(currentsection).nextAll('.activediv').offset().top - 100
        }, 500)
    });
    $(".cancel").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $('div').nextAll('.activediv').offset().top - 100
        }, 500)
    });
</script>
<script>
$(document).ready(function(){
$('.qualify').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});
$('.date').datepicker();
});
</script>
