<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/user-changeinfo-view" prefix="userInfoView"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<style>
  .dl-horizontal dt {
    font-weight: normal;
  }
  .dl-horizontal dd {
    font-weight: bold;
  }
  #account-settings dt,
  #account-settings dd {margin-bottom: 15px; }
  
  #acc-settings-info dt {
    width: 160px;    
  }
 #acc-settings-info dd {
    margin-left: 180px;
    font-weight: bold;
  }  
  #change-email label, #change-email input,
  #change-password label, #change-password input { margin: 10px 0;}
  #change-questions .form-group{ 
    float: left;
    margin: 5px 0;
  }
</style>
<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />


<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${employee.name}<span></h1>
  <div class="row-fluid">
<%--     <div class="span3" id="sidebar">
      <util:myStuff employee="${employee}"></util:myStuff>
      <util:quickLinks employee="${employee}"></util:quickLinks>
    </div> --%><!--/span3-->
    <div class="span9" id="rightpanel">
    	<div class="header">
      		<h4 class="pull-left">Account Settings</h4> 
      	</div>     
        <div>
          <h4 class="span8">Password Settings
          <a id="pwdChange" href="#change-password" data-toggle="modal" class="btn btn-small pull-right">Change Your Password</a>
          </h4>
		</div>
		<div>	
          <h4 class="span8">Security Questions
          <a href="#change-questions" data-toggle="modal" class="btn btn-small pull-right">Change Your Security Questions</a>
          </h4>
        </div>
       </div>  <!--/rightpanel--> 
  </div><!--/row-fluid-->        
</div><!--/main .gutter10-->

<!-- Modal Code Begins Here-->
<div id="change-password" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="password-modal" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="password-modal">Change Your Password</h3>
    </div>
      <userInfoView:changePassword redirectURL="/shop/employee/spplanselection/accountsettings">
      </userInfoView:changePassword>
</div><!--/.modal change password-->

<div id="change-questions" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="questions-modal" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="questions-modal">Change Your Security Questions</h3>
    </div>
     <userInfoView:securityQuestions redirectURL="/shop/employee/spplanselection/accountsettings"></userInfoView:securityQuestions>
</div><!--/.modal change password-->


<jsp:include page="../waive_employer_coverage.jsp" >
	<jsp:param name="employeeId" value="${employee.id}" />
</jsp:include>
<script type="text/javascript" src="<c:url value="/resources/js/user/userinfo.js" />"></script>
<script>
$(document).ready(function(){
	$('.clearForm').click(function(){
        $(this).parents("form").find("input").val("");
	});
  $('div[rel=popover]').popover({trigger:'hover'});
  $('div[rel=popover]').popover({trigger:'focus'}); 
  
	$('#pwdChange').click(function(){
		$('label.error').remove();
		$("input[type=password]").val("");
	});
  
});
$(".inactive").click(function(e) {
	e.preventDefault();
});
</script>