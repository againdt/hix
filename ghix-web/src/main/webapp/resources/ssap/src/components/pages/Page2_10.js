import React, {Fragment, useEffect, useState} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import _ from 'lodash';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import {changeData} from '../../actions/actionCreators';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import ETHNICITY_LIST, {OTHER_ETHNICITY_CODE} from '../../constants/ethnicityList';
import INPUT_TYPES from '../../constants/inputTypes';
import RACES_LIST, {OTHER_RACE_CODE} from '../../constants/racesList';
import VALIDATION_TYPES from '../../constants/validationTypes';

// Components

// ActionCreators

// Utils

// Constants


/**
 *
 * @param {Object} props
 * @returns {React.Component} - Page 'Ethnicities and Races'
 *
 */
const Page2_10 = props => {

    const {
        data,
        householdMembers
    } = props;

    const {
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        ethnicityAndRace : {
            hispanicLatinoSpanishOriginIndicator
        },
        name
    } = currentHhm ? currentHhm : { ethnicityAndRace: {}, name: {} };

    // Local State of Functional Component
    // Here we only handle state of all checkboxes if they are checked or not

    const [ localState, setLocalState ] = useState({
        ethnicities: currentHhm ? prepareValuesForState('ethnicities'): [],
        races:  currentHhm ? prepareValuesForState('races') : []
    });

    const {
        ethnicities,
        races
    } = localState;

    // Each of the following useEffects is invoked in case if related to it property in Local State has been updated
    // As a result we update JSON in Redux Store

    useEffect(() => {
        const newState = { ...data };
        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .ethnicityAndRace
            .ethnicity = localState.ethnicities
                .map(ethnicity => ({ ...ethnicity }))
                .filter(ethnicity => ethnicity[ethnicity.code])
                .map(ethnicity => _.omit(ethnicity, [ethnicity.code, 'ethnicityLabel']));
        props.actions.changeData(newState);
    }, [localState.ethnicities]);

    useEffect(() => {
        const newState = { ...data };
        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .ethnicityAndRace
            .race = localState.races
                .map(race => ({ ...race }))
                .filter(race => race[race.code])
                .map(race => _.omit(race, [race.code, 'raceLabel']));
        props.actions.changeData(newState);
    }, [localState.races]);

    // Here we get initialState for Local State

    function prepareValuesForState(type) {

        switch(type) {

        case 'ethnicities':

            return ETHNICITY_LIST.map(ethnicity => {

                const matchingEthnicity = currentHhm
                    .ethnicityAndRace
                    .ethnicity
                    .find(eth => eth.code === ethnicity.code);

                return {
                    ...ethnicity,
                    label: ethnicity.code === OTHER_ETHNICITY_CODE ?
                        matchingEthnicity && matchingEthnicity.label : ethnicity.label,
                    [ethnicity.code]: !!matchingEthnicity
                };
            });

        case 'races':

            return RACES_LIST.map(race => {

                const matchingRace = currentHhm
                    .ethnicityAndRace
                    .race
                    .find(r => r.code === race.code);

                return {
                    ...race,
                    label: race.code === OTHER_RACE_CODE ?
                        matchingRace && matchingRace.label : race.label,
                    [race.code]: !!matchingRace
                };
            });
        }
    }

    // Here we update JSON in Redux store

    const onFieldChange = newData => {
        const newState = { ...data };

        // Here we check <hispanicLatinoSpanishOriginIndicator> and if its value is false
        // clean up <ethnicity> Array

        if(newData.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator === false) {
            newData.ethnicityAndRace.ethnicity = [];
        }
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = newData;
        props.actions.changeData(newState);

        // Updating of Local State: uncheck all ethnicities

        setLocalState(state => ({
            ...state,
            ethnicities: state.ethnicities.map(ethnicity => ({
                ...ethnicity,
                [ethnicity.code]: false
            }))
        }));
    };

    // Updating of Local State: check or uncheck certain ethnicity

    const onFieldChange_ethnicity = newData => {

        setLocalState(state => ({
            ...state,
            ethnicities: state.ethnicities.map(ethnicity => ethnicity.code === newData.code ?
                {
                    ...newData,
                    label: ethnicity.code === OTHER_ETHNICITY_CODE ? newData.ethnicityLabel : newData.label
                } : ethnicity
            )}));
    };

    // Updating of Local State: check or uncheck certain race

    const onFieldChange_race = newData => {
        setLocalState(state => ({
            ...state,
            races: state.races.map(race => race.code === newData.code ?
                {
                    ...newData,
                    label: race.code === OTHER_RACE_CODE ? newData.raceLabel : newData.label
                } : race
            )}));
    };

    return (
        <div id="fhEthinicity" className="fade-in">
            <Fragment>
                <div className="subsection">
                    <div className="usa-width-one-whole">
                        <div className="usa-alert usa-alert-info alert--info">
                            <div className="usa-alert-body" >
                                <p className="usa-alert-text">
                                    <FormattedMessage
                                        id="ssap.ethnicityandrace.msg.one"
                                        defaultMessage="Optional: These questions are optional, and you do not need to answer them
                                                to apply for health insurance. If you choose to answer them, Nevada Health Link
                                                will use this information to get a better understanding of the demographics
                                                and health needs of Nevadans. This information will also be shared with
                                                the Department of Health and Human Services to support a broader understanding
                                                of health needs across the U.S. population."
                                    />
                                </p>
                            </div>
                        </div>
                    </div>
                    <Form>
                        <UniversalInput
                            legend={{
                                legendText: <span>
                                    Is &nbsp;
                                    <strong>
                                        {normalizeNameOfPerson(name)} &nbsp;
                                    </strong>
                                    of Hispanic, Latino, or Spanish origin?
                                </span>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole'],
                                showRequiredIcon: false
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'hispanicLatinoSpanishOriginIndicator',
                                value: hispanicLatinoSpanishOriginIndicator,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                currentData: householdMembers[member],
                                fields: ['ethnicityAndRace']

                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />

                        {/********************************************************************
                         Begin - If User is #hispanicLatinoSpanishOriginIndicator (Yes)
                         **********************************************************************/}

                        {

                            hispanicLatinoSpanishOriginIndicator &&

                            <Fragment>
                                <div className="usa-width-one-whole margin-t-30">
                                    <fieldset className="usa-fieldset-inputs usa-sans">
                                        <legend className="gi-checkbox margin-b-10"><strong>Ethnicity </strong>(check all that apply)</legend>
                                        <ul className="usa-unstyled-list">
                                            {
                                                ethnicities.map(ethnicity => {
                                                    return (
                                                        <Fragment key = {ethnicity.code}>
                                                            <li>
                                                                <UniversalInput
                                                                    label={{
                                                                        text: ethnicity.code !== OTHER_ETHNICITY_CODE ? ethnicity.label : 'Other'
                                                                    }}
                                                                    inputData={{
                                                                        isInputRequired: false,
                                                                        classes:['layout--01'],
                                                                        type: INPUT_TYPES.CHECKBOX,
                                                                        name: ethnicity.code,
                                                                        checked: ethnicity[ethnicity.code],
                                                                        value: ethnicity[ethnicity.code],
                                                                        currentData: ethnicity,
                                                                        fields: []
                                                                    }}
                                                                    inputActions={{
                                                                        onFieldChange: onFieldChange_ethnicity
                                                                    }}
                                                                />
                                                            </li>
                                                            {
                                                                ethnicity.code === OTHER_ETHNICITY_CODE && ethnicity[ethnicity.code] &&
                                                                <li>
                                                                    <UniversalInput
                                                                        label={{
                                                                            text: 'Please Specify',
                                                                            labelClasses: ['spl__width--01']
                                                                        }}
                                                                        inputData={{
                                                                            isInputRequired: false,
                                                                            type: INPUT_TYPES.REGULAR_INPUT,
                                                                            name: 'ethnicityLabel',
                                                                            helperClasses: ['margin-t-0'],
                                                                            value: ethnicity.label,
                                                                            placeholder: 'Enter other ethnicity',
                                                                            validationType: VALIDATION_TYPES.NOT_EMPTY,
                                                                            currentData: ethnicity,
                                                                            fields: []
                                                                        }}
                                                                        inputActions={{
                                                                            onFieldChange: onFieldChange_ethnicity
                                                                        }}
                                                                    />
                                                                </li>
                                                            }
                                                        </Fragment>
                                                    );
                                                })
                                            }
                                        </ul>
                                    </fieldset>
                                </div>
                            </Fragment>
                        }


                        {/****************************************************************
                         End - If User is #hispanicLatinoSpanishOriginIndicator (Yes)
                         ******************************************************************/}

                        <Fragment>
                            <div className="usa-width-one-whole margin-t-30">
                                <fieldset className="usa-fieldset-inputs usa-sans">
                                    <legend className="gi-checkbox margin-b-10"><strong>Race</strong> (check all that apply)</legend>
                                    <ul className="usa-unstyled-list">
                                        {
                                            races.map(race => {
                                                return (
                                                    <Fragment key = {race.code}>
                                                        <li>
                                                            <UniversalInput
                                                                label={{
                                                                    text:  race.code !== OTHER_RACE_CODE ? race.label : 'Other'
                                                                }}
                                                                inputData={{
                                                                    isInputRequired: false,
                                                                    type: INPUT_TYPES.CHECKBOX,
                                                                    classes:['layout--01'],
                                                                    name: race.code,
                                                                    checked: race[race.code],
                                                                    value: race[race.code],
                                                                    currentData: race,
                                                                    fields: []
                                                                }}
                                                                inputActions={{
                                                                    onFieldChange: onFieldChange_race
                                                                }}
                                                            />
                                                        </li>
                                                        {
                                                            race.code === OTHER_RACE_CODE && race[race.code] &&
                                                            <li>
                                                                <UniversalInput
                                                                    label={{
                                                                        text: 'Please Specify',
                                                                        labelClasses: ['spl__width--01']
                                                                    }}
                                                                    inputData={{
                                                                        isInputRequired: false,
                                                                        type: INPUT_TYPES.REGULAR_INPUT,
                                                                        name: 'raceLabel',
                                                                        helperClasses: ['margin-t-0'],
                                                                        value: race.label,
                                                                        placeholder: 'Enter other race',
                                                                        validationType: VALIDATION_TYPES.NOT_EMPTY,
                                                                        currentData: race,
                                                                        fields: []
                                                                    }}
                                                                    inputActions={{
                                                                        onFieldChange: onFieldChange_race
                                                                    }}
                                                                />
                                                            </li>
                                                        }
                                                    </Fragment>
                                                );
                                            })
                                        }
                                    </ul>
                                </fieldset>
                            </div>
                        </Fragment>

                    </Form>
                </div>
            </Fragment>
        </div>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_10.defaultProps = {
    name: 'Page2_10'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_10);
