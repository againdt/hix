package com.getinsured.hix;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class IFrameTestController {
	
	@RequestMapping(value = "/iframe/iframehome", method = RequestMethod.GET)
	public String iFrameParent(Model model) {
		model.addAttribute("page_title",
				"iFrame Parent");
		return "iframe/iframehome";
	}
	
}
