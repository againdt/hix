package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.DS2019DocumentID27Type;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;

/**
 * Wrapper Class for DS2019DocumentID27Type
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class DS2019DocumentID27Wrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DS2019DocumentID27Wrapper.class);
	
	private ObjectFactory factory;
	private DS2019DocumentID27Type ds2019DocumentID27Type;
	
	private String i94Number;
	private String sevisid;
	private String expiryDate;
	protected PassportCountryWrapper passportCountry;

	private static final String I94_NUMBER_KEY = "I94Number";
	private static final String SEVIS_ID_KEY = "SEVISID";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	private static final String PASSPORT_COUNTRY_KEY = "PassportCountry";
	

	public DS2019DocumentID27Wrapper(){
		this.factory = new ObjectFactory();
		this.ds2019DocumentID27Type = factory.createDS2019DocumentID27Type();
	}
	
	public void setI94Number(String i94Number) throws HubServiceException {
		
		if(i94Number == null){
			throw new HubServiceException("No i94 number provided");
		}
		
		this.i94Number = i94Number;
		this.ds2019DocumentID27Type.setI94Number(this.i94Number);
	}
	
	public String getI94Number() {
		return i94Number;
	}
	
	public void setSevisid(String sevisid) throws HubServiceException{

		if(sevisid == null){
			throw new HubServiceException("No SEVIS ID provided");
		}
		
		this.sevisid = sevisid;
		this.ds2019DocumentID27Type.setSEVISID(this.sevisid);
		
	}
	
	public String getSevisid() {
		return sevisid;
	}

	public void setExpiryDate(String expiryDate) {
		
		try{
			this.ds2019DocumentID27Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting DS 2019 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	
	public DS2019DocumentID27Type getDS2019DocumentID27Type(){
		return this.ds2019DocumentID27Type;
	}
	
	public PassportCountryWrapper getPassportCountry() {
		return passportCountry;
	}

	public void setPassportCountry(PassportCountryWrapper passportCountry) {
		this.passportCountry = passportCountry;
	}

	
	/**
	 * Converts the JSON String to DS2019DocumentID27Wrapper object
	 * 
	 * @param jsonString - String representation for DS2019DocumentID27Wrapper
	 * @return I20DocumentID26Wrapper Object
	 */
	public static DS2019DocumentID27Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create DS2019DocumentID27Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		DS2019DocumentID27Wrapper wrapper = new DS2019DocumentID27Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(I94_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.i94Number = (String)tmpObj;
			wrapper.ds2019DocumentID27Type.setI94Number(wrapper.i94Number);
		}
		
		tmpObj = obj.get(SEVIS_ID_KEY);
		if(tmpObj != null){
			wrapper.sevisid = (String)tmpObj;
			wrapper.ds2019DocumentID27Type.setSEVISID(wrapper.sevisid);
		}
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.ds2019DocumentID27Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating DS2019DocumentID27Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		tmpObj = obj.get(PASSPORT_COUNTRY_KEY);
		if(tmpObj != null){
			
			try {
				wrapper.setPassportCountry(PassportCountryWrapper.fromJSONObject((JSONObject)tmpObj));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating DS2019DocumentID27Wrapper from JSON, Invalid expiration date",e);
			}
		}

		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static DS2019DocumentID27Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		DS2019DocumentID27Wrapper wrapper = new DS2019DocumentID27Wrapper();
		wrapper.setI94Number((String) jsonObj.get(I94_NUMBER_KEY));
		wrapper.setSevisid((String) jsonObj.get(SEVIS_ID_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
	
}
