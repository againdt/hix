(function (views) {
	views.dentalPlans = Backbone.View.extend({
		events : {
			'click a.addToCart': 'addToCart',
			'click a.showAll': 'showAll',
			'click a.validateCart': 'validateCart',
			'click a.goToDentalPlan': 'goToDentalPlan',
			'click a.details': 'details'
		},
		el : '#mainSummary',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'loadAll', 'addOne','addToCart','showAll','validateCart','goToDentalPlan', 'details');
			var self = this;
			
			$('.heading').hide();
			$('.showAll').hide();
			$('.fewer').hide();
			$('.removeDental').bind('click', self.addToCart);
			$('.removeDental').hide();
			self.collection.perPage = 3;
			self.collection.fetch({
				success : function () {
					if(self.collection.length==0){
						$('#mainSummary').empty();
						$('#mainSummary').hide();
						$('.heading').hide();
						$('.showAll').hide();
					}else{
						$('.heading').show();
						if(self.collection.length>3){
							$('.showAll').show();
						}
						
						self.collection.updateCartSummary();
					}
					
					$.ajax({
						  type: "GET",
							url: 'getDentalQuotes?key='+(Math.random()*10)+'&quotingGroup='+$('#quotingGroup').val()
					    }).done(function( response ) {
					    	$.each(response,function(index,value){
					    		if(value != null){
					    			if(index=='title'){
					    				$("#title").html(value);
					    			}
					    			if(index=='subHeading'){
					    				$("#subHeading").html(value);
					    			}
					    			if(index=='bodyMessage'){
					    				$("#bodyMessage").html(value);
					    			}
					    			if(index=='missingDentalBenifit' && value=='true')
					    			{
					    				$('#subHeadingStaticText').hide();
					    			}
					    			if(index=='subHeading2'){
					    				$("#subHeading2").html(value);
					    			}
					    		}
					    	});
					    });
					
					self.collection.setSort('planScore', 'desc');
					self.collection.pager();
					self.loadAll();
					/*show/hide cart buttons depending on individual type*/
					if($('#indType').val() == 'ANONYMOUS'){
						$(".addToCart").hide();
					}
										
				},
				silent:true
			});
			
			self.collection.cartSummary.fetched = false;
			self.cartCollection = options.cartCollection;
			var currentGroupId = $('#currentGroupId').val();
			
			self.cartCollection.fetch({
				success : function () {
					self.cartCollection.setCartSummary(false,currentGroupId);
					self.collection.cartSummary.fetched = true;
				}
			});
			
			self.cartModel = options.cartModel;
			$('.showAll').bind('click', this.showAll);
			$('.validateCart').bind('click', this.validateCart);
			$('.goToDentalPlan').bind('click', this.goToDentalPlan);
			$('.details').bind('click', this.details);
			
			self.collection.bind('reset', self.addAll);
		},

		addAll : function () {
			var self = this;
			$('#mainSummary').empty();
			self.collection.each (self.addOne);
		},
		
		loadAll : function () {
			var self = this;
			self.collection.loadIssuers();
			self.loadCartSummary();
			self.collection.updateCompareSummary();
		},
		
		showAll: function (e) {
			this.collection.perPage = 100;
			//var currentSort = e.target.id;
			//this.collection.setSort(currentSort, 'asc');	
			//this.collection.preserveOtherOptions();
			this.collection.pager();
			this.collection.info();
			this.collection.updateCartSummary();
			$('.showAll').hide();
			$('.fewer').show();
			
		},
		
		addOne : function (model) {
			var self = this;
			var pgrmType = $('#PGRM_TYPE').val();
			model.set({"PGRM_TYPE":pgrmType});			
			var providerLinkOnOff = $('#providerLinkOnOff').val();
			model.set({"providerLinkOnOff":providerLinkOnOff});
			var viewSummary = new Summary({model:model});
			$('#mainSummary').append(viewSummary.render());				
		},	
		
		loadCartSummary : function () {
			var self = this;
			var fetchedVal = self.collection.cartSummary.fetched;
			if(fetchedVal == false){
				$('#planCount').html("(Loading...)");
				setTimeout(function(){self.loadCartSummary();}, 1000);
				return;
			}else{
				self.collection.loadCartSummary(self.cartCollection.cartSummary,self.cartCollection.cartItems,self.cartCollection.models);
			}
		},		
		
		addToCart: function (e) {
			var self = this;
			var planModel = "";
			var exchangeType = $('#exchangeType').val();
			var multipleItemsEntryOnOff = $('#multipleItemsEntryOnOff').val();
			var aptcForHP = $('#aptcForHP').val();
			var discountedPrice = $('#discountedPrice').val();

			//$('#taxCredit').text(aptcForHP);
			//$('#monthlyTotal').text(discountedPrice);
			
			if($('#quotingGroup').val()=="FAMILY" ||  $('#quotingGroup').val()==null)
			{
				$("#myTab_WAC_Children > a").attr("href","#");
				$("#myTab_WAC_Adults > a").attr("href","#"); 
				$("#myTab_WAC_Children").attr("class","grayed");
				$("#myTab_WAC_Adults").attr("class","grayed");
				
				$("#myTab_WAC_Children > a").attr( "rel", "tooltip" );
				$("#myTab_WAC_Adults > a").attr( "rel", "tooltip" );
				
			}
			else if($('#quotingGroup').val()=="ADULT")
			{
				$("#myTab_WAC_Family > a").attr("href","#");
				$("#myTab_WAC_Children > a").attr("href","#");
				$("#myTab_WAC_Family").attr("class","grayed");
				$("#myTab_WAC_Children").attr("class","grayed");
				
				$("#myTab_WAC_Family > a").attr( "rel", "tooltip" );
				$("#myTab_WAC_Children > a").attr( "rel", "tooltip" );
			}
			else if($('#quotingGroup').val()=="CHILD")
			{
				$("#myTab_WAC_Family > a").attr("href","#");
				$("#myTab_WAC_Adults > a").attr("href","#");
				$("#myTab_WAC_Family").attr("class","grayed");
				$("#myTab_WAC_Adults").attr("class","grayed");
				
				$("#myTab_WAC_Family > a").attr( "rel", "tooltip" );
				$("#myTab_WAC_Adults > a").attr( "rel", "tooltip" );
			}
			
			
			
			
			if(e.target.id.indexOf('cart_') >= 0){
				itemId = e.target.id.replace("cart_","");
			}else{
				itemId = e.target.id.replace("plan_","");
			}

			if($("#"+e.target.id).attr("class").indexOf('addedItem') >= 0){
				if($("#cart_"+itemId).length){
					$("#cart_"+itemId).attr("class","addToCart btn btn-primary");
					$("#cart_"+itemId).html('Select');
					$("#myTab_WAC_Children > a").attr("href","dental?quotingGroup=CHILD");
					$("#myTab_WAC_Adults > a").attr("href","dental?quotingGroup=ADULT"); 
					$("#myTab_WAC_Family > a").attr("href","dental?quotingGroup=FAMILY");
					
					$("#myTab_WAC_Children").attr("class","");
					$("#myTab_WAC_Adults").attr("class","");
					$("#myTab_WAC_Family").attr("class","");
					
					$("#myTab_WAC_Children >a").removeAttr( "rel" );
					$("#myTab_WAC_Adults >a").removeAttr( "rel" );
					$("#myTab_WAC_Family >a").removeAttr( "rel" );
				}
				
				self.collection.emptyCart(self.cartCollection.models);
				planModel = self.collection.searchPlan(this.cartCollection.models, itemId);
				orgId = planModel.id;
				$('#dentalTemplate').fadeOut(250);
				$('#tempText').fadeIn(250);
				//$('#taxCredit').text("$"+aptcForHP.toFixed(2));
				//$('#monthlyTotal').text("$"+discountedPrice.toFixed(2));
				
				planModel.set({id: planModel.get("orderItemId")});
				planModel.destroy({
					success : function () {
						planModel.set({id: orgId});
						self.cartCollection.remove(planModel);
						self.collection.updateCart(self.cartCollection.models);
					}
				});
				
				
				self.collection.removeFromCart(itemId, planModel.get("level").toLowerCase());
				this.collection.updateCartSummary();
				
				$('#cartmessageplanremove').fadeIn(250,function() {
					setTimeout(function(){
						$('#cartmessageplanremove').hide();
					},1500);
				});
				
				$('.removeDental').hide();
				
				$('#taxCredit').text(aptcForHP);
				$('#monthlyTotal').text(parseFloat(discountedPrice).toFixed(2));
			}
			else{
					
					var dentalPlanAdded = self.collection.dentalPlanAdded(self.cartCollection.models);
					if(dentalPlanAdded == true){
						this.collection.updateCartSummary();
						alert(plan_display['pd.label.dentalPlanAlreadyAdded']);
						return false;
					}
					
					if( jQuery.inArray(itemId, this.collection.cartItems) === -1 ) {
						$("#"+itemId).attr("class","addToCart addedItem btn btn-primary");
						$("#"+itemId).attr("title",plan_display['pd.label.removeThisPlan']);//added to fav
						 if(!(exchangeType == 'STATE')){
							 $("#cart").show();
							 $('#cart-box').show();
						 }
						planModel = self.collection.searchPlan(this.collection.models, itemId);
						
						this.cartModel.save({id: planModel.get("planId"), premium: planModel.get("premiumAfterCredit"), premiumBeforeCredit: planModel.get("premiumBeforeCredit"), planDetailsByMember: planModel.get("planDetailsByMember"), aptc: planModel.get("aptc"),qoutingGroup: $('#quotingGroup').val(), contributionData: planModel.get("contributionData"),encodedPremium: planModel.get("encodedPremium")},{
							success : function (model,resp) {
								var newModel = new App.models.Cart({id: planModel.id, name: planModel.get("name"), planType: planModel.get("planType"), 
									level: planModel.get("level"), premium: planModel.get("premium"), issuer: planModel.get("issuer"), 
									issuerText: planModel.get("issuerText"), networkType: planModel.get("networkType"), deductible: planModel.get("deductible"),
									premiumBeforeCredit: planModel.get("premiumBeforeCredit"), aptc: planModel.get("aptc"), premiumAfterCredit: planModel.get("premiumAfterCredit"),
									groupId: planModel.get("groupId"), groupName: planModel.get("groupName"), groupMembers: planModel.get("groupMembers"), 
									planId: planModel.get("planId"), ehb: planModel.get("ehb"), orderItemId:model.id, issuerLogo: planModel.get("issuerLogo"), qoutingGroup: $('#quotingGroup').val(), contributionData: planModel.get("contributionData")});
								
								self.cartCollection.add(newModel);
								
								self.collection.emptyCart(self.cartCollection.models);
								self.collection.updateCart(self.cartCollection.models);
								//$('.removeFromCart').bind('click', self.addToCart);
								$('.checkOut').bind('click', self.checkOut);
								
								var id = $(resp).attr('id');
								var result = $(resp).attr('result');								
					        	/*if(id != 'FAIL' && result == 'SUCCESS')	{
					        		$('.removeDental').show();
					        		$('#tempText').fadeOut(250);
									$('#dentalTemplate').attr("class","dental-added");
									$('#dentalPlanName').text(planModel.get("name"));
									$('#dentalPlanPrice').text("$"+planModel.get("premiumAfterCredit").toFixed(2));
									$('.removeFromCart').attr("id","cart_" + planModel.get("planId") + "_" + planModel.get("groupId"));
									
									$('#dentalTemplate').fadeIn(250);
									
									var totalAPTC = aptcForHP + planModel.get("aptc");
									var totalMonthlyTotal = discountedPrice + planModel.get("premiumAfterCredit");
									
									$('#taxCredit').text("$"+totalAPTC.toFixed(2));
									$('#monthlyTotal').text("$"+totalMonthlyTotal.toFixed(2));
					        	}*/
							}
						});
						
						this.collection.setCartSummary(itemId, planModel.get("level").toLowerCase());
						
						$('.cartmessageplan').fadeIn(250,function() {
							setTimeout(function(){
								$('.cartmessageplan').hide();
							},1500);
						});
					}else{
						alert(plan_display['pd.label.selectPlanAlreadyInCart']);
					}
				
				this.collection.updateCartSummary();
			}
			
			//this.collection.updateCartSummary();
			$('#holdcart a').removeClass('disabled').bind('click', this.showCart);
			$('#holdcart a').text(plan_display['pd.label.goToCart']);
					
		},
		
		showCart : function () {
			var self = this;
			self.collection.emptyCart(self.cartCollection.models);
			//$('#cart-box').show();
			//self.collection.updateCart(self.cartCollection.models);
			$('.removeFromCart').bind('click', self.addToCart);
			window.location = "showcart";
			//$('.checkOut').bind('click', this.checkOut);
		},
		
		goToDentalPlan: function () {
			var self = this;
			self.collection.perPage = 3;
			self.collection.pager();
			self.collection.info();
			self.collection.updateCartSummary();
			//self.collection.emptyCart(self.cartCollection.models);
			//$('.removeFromCart').bind('click', self.addToCart);
			//window.location = "dental";
			$('.showAll').show();
			$('.fewer').hide();
		},
		
		validateCart : function(){
			$("#showcart").submit();
		},
		details : function (e){
			if(e.target.id.indexOf('detail_') >= 0){
				planId = e.target.id.replace("detail_","");
			}
			if($("#detail_"+planId).text() == 'Details'){
				$("#detail_"+planId).text('Close');
			}else{
				$("#detail_"+planId).text('Details');
			}
		}
		
	});
	
	var Summary = Backbone.View.extend({
		template: _.template($('#resultItemTemplateMainSummary').html()),
		
		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},
		
		render : function () {
			return this.template(this.model.toJSON());
		}
	});
})(App.views);