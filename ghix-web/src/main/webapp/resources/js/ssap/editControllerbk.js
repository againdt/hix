
var editFlag = false;
var editMemberNumber = 0;
var cancleFlag = true;
var loginUserID = "";

//	Edit functionality of Household Information

function appscr58EditController(i)
{
	editFlag = true;
	
	editMemberNumber = i;
	
	$('#contBtn').text('Update');
	$('#countinue_button_div').removeClass("Continue_button");
	$('#countinue_button_div').addClass("ContinueForUpdate_button");
	$('#cancelButtonCancel').show();
	$('#back_button_div').hide();
	

	if(i == 1)
	{
		goToPageById("appscr53");
		$('#contBtn').attr('onclick', 'appscr58EditUdate(1),'+true+'');
	}
	else
	{		
		$('#appscr57FirstName'+i).focus();
		goToPageById("appscr57");
		$('#contBtn').attr('onclick', 'appscr58EditUdate('+i+'),'+true+'');
		
		blockOtherFields(i);
	}
}

function enableOtherFields()
{
	for(var j=1; j<=noOfHouseHold; j++)
	{
		$('#appscr57FirstName'+j).attr("disabled",false);
		$('#appscr57MiddleName'+j).attr("disabled",false);
		$('#appscr57LastName'+j).attr("disabled",false);
		$('#appscr57Suffix'+j).attr("disabled",false);
		$('#appscr57DOB'+j).attr("disabled",false);
		
		$('#appscr57DOBVerificationStatus'+j).attr("disabled",false);
		$('input[name=appscr57Sex'+j+']:radio').attr("disabled",false);
		$('input[name=appscr57Tobacco'+j+']:radio').attr("disabled",false);
	}
}

function blockOtherFields(i)
{
	for(var j=1; j<=noOfHouseHold; j++)
	{
		if(j==i)
		{
			$('#appscr57FirstName'+j).focus();
			continue;
		}
		else
		{
			$('#appscr57FirstName'+j).attr("disabled",true);
			$('#appscr57MiddleName'+j).attr("disabled",true);
			$('#appscr57LastName'+j).attr("disabled",true);
			$('#appscr57Suffix'+j).attr("disabled",true);
			$('#appscr57DOB'+j).attr("disabled",true);
			
			$('#appscr57DOBVerificationStatus'+j).attr("disabled",true);
			$('input[name=appscr57Sex'+j+']:radio').attr("disabled",true);
			$('input[name=appscr57Tobacco'+j+']:radio').attr("disabled",true);
		}
	}
}

function appscr58EditCancel()
{
	if(!cancleFlag)
	{
		$('#addAnotherHouseHold'+noOfHouseHold).hide();
		$('#cancelButtonCancel').hide();
		$('#back_button_div').show();
		noOfHouseHold--;
	
		$('#numberOfHouseHold').text(noOfHouseHold);
		$('#noOfPeople53').val(noOfHouseHold);
		cancleFlag = true;
	}
	else
	{
		$('#back_button_div').show();
		$('#contBtn').text('Continue');
		$('#countinue_button_div').addClass("Continue_button");
		$('#countinue_button_div').removeClass("ContinueForUpdate_button");
		$('#cancelButtonCancel').hide();
		enableOtherFields();
		editFlag = false;
	}
		goToPageById("appscr58");
	
}

function appscr58EditCancle()
{
	$('#countinue_button_div').show();
	$('#back_button_div').show();
	$('#editUdateOrCancle_Div').hide();
	
	goToPageById("appscr58");
	editFlag = false;
}

function appscr58EditUdate(i,updateFlag)
{
	$('#contBtn').text('Continue');
	$('#countinue_button_div').removeClass("ContinueForUpdate_button");
	$('#countinue_button_div').addClass("Continue_button");
	$('#cancelButtonCancel').hide();
	$('#back_button_div').show();
	
	cancleFlag = true;
	if(i == 1)
	{
		if(updateFlag!=false)
		{	backData53(); 
			saveData53();
		}
		$('#appscr57FirstNameTD'+i).text($('#firstName').val());
		$('#appscr57MiddleNameTD'+i).text($('#middleName').val());
		$('#appscr57LastNameTD'+i).text($('#lastName').val());
		$('#identifire'+i).text($('#firstName').val());
		$('#appscr57SuffixTD'+i).text($('#appscr53Suffix').val());
		$('#appscr57DOBTD'+i).text($('#dateOfBirth').val());
		
		$('#appscr57FirstName'+i).val($('#firstName').val());
		$('#appscr57MiddleName'+i).val($('#middleName').val());
		$('#appscr57LastName'+i).val($('#lastName').val());
		$('#appscr57Suffix'+i).val($('#appscr53Suffix').val());
		$('#appscr57DOB'+i).val($('#dateOfBirth').val());
		$('#appscr57DOBVerificationStatus'+i).val($('#dobVerificationStatus').val());
	}
	else
	{
		if(updateFlag!=false)
		{	backData57();
			generateArray57();
			saveData57();
		}
		$('#appscr57FirstNameTD'+i).text($('#appscr57FirstName'+i).val());
		$('#appscr57MiddleNameTD'+i).text($('#appscr57MiddleName'+i).val());
		$('#appscr57LastNameTD'+i).text($('#appscr57LastName'+i).val());
		$('#identifire'+i).text($('#appscr57FirstName'+i).val());
		$('#appscr57SuffixTD'+i).text($('#appscr57Suffix'+i).val());
		$('#appscr57DOBTD'+i).text($('#appscr57DOB'+i).val());
		
	}
	$('#appscr57DOBVerificationTD'+i).text($('#appscr57DOBVerificationStatus'+i).val());
	$('#appscr57emailAddressTD'+i).text($('#emailAddress').val());
	
	$('#appscr57TobaccoTD'+i).text($('input[name=appscr57Tobacco'+i+']:radio:checked').val());
	$('#appscr57SexTD'+i).text($('input[name=appscr57Sex'+i+']:radio:checked').val());
	$('#appscr57IsChildTD'+i).text(updateChildRelatedDetails(i,$('#appscr57DOB'+i).val()));
	$('#appscr57IsInfantTD'+i).text(updateInfantRelatedDetails(i,$('#appscr57DOB'+i).val()));
	
	addAppscr58Detail();
	
	if(editFlag)
	{
		goToPageById("appscr58");
		enableOtherFields();
	}
	editFlag = false;
}


//	Edit functionality of NonFinancialDetails

function appscr67EditController(i)
{
	editMemberNumber = i;
	nonFinancialHouseHoldDone = i;
	
	if(!haveDataFlag)
	{
		editFlag = true;
		$('#back_button_div').hide();
	}
	
	var editHouseHoldgender 				= $('#appscrHouseHoldgender57TD'+i).text();
	var editHouseHoldHavingSSN  			= $('#appscr57HouseHoldHavingSSNTD'+i).text();
	var editHouseHoldSSN 					= $('#appscr57HouseHoldSSNTD'+i).text();
	var editsameNameInSSNCardIndicator		= $('#appscr57SameNameInSSNCardIndicatorTD'+i).text();
	var editfirstNameOnSSNCard				= $('#appscr57FirstNameOnSSNCardTD'+i).text();
	var editmiddleNameOnSSNCard				= $('#appscr57MiddleNameOnSSNCardTD'+i).text();
	var editlastNameOnSSNCard				= $('#appscr57LastNameOnSSNCardTD'+i).text();
	var editsuffixOnSSNCard					= $('#appscr57SuffixOnSSNCardTD'+i).text();
	var editnotAvailableSSNReason			= $('#appscr57notAvailableSSNReasonTD'+i).text();
	var editUScitizen 						= $('#appscr57UScitizenTD'+i).text();
	var editlivedIntheUSSince1996Indicator 	= $('#appscr57livedIntheUSSince1996IndicatorTD'+i).text();
	var editnaturalizedCitizenshipIndicator	= $('#appscr57naturalizedCitizenshipIndicatorTD'+i).text();
	
	var editcertificateType 				= $('#appscr57certificateTypeIndicatorTD'+i).text();
	var editcertificateAlignNumber 			= $('#appscr57naturalizedCitizenshipAlignNumberTD'+i).text();
	var editcertificateNumber 				= $('#appscr57certificateNumberTD'+i).text();
	
	var editaliegnNumber 					= $('#appscr57alienNoTD'+i).text();
	var editi94Number 						= $('#appscr57i94NoTD'+i).text();
	var editForeignPassportNumber 			= $('#appscr57foreignPassNoTD'+i).text();
	var editForeignPassportCountry			= $('#appscr57foreignPassCountryTD'+i).text();
	var editForeignPassportExpiration		= $('#appscr57foreignPassExpirationTD'+i).text();
	var editSevisID 						= $('#appscr57sevisIDTD'+i).text();
	
	
	var editdocumentSameNameIndicator 		= $('#appscr57documentSameNameIndicatorTD'+i).text();
	var editdocumentFirstName 				= $('#appscr57documentFirstNameTD'+i).text();
	var editdocumentLastName 				= $('#appscr57documentMiddleNameTD'+i).text();
	var editdocumentMiddleName 				= $('#appscr57documentLastNameTD'+i).text();
	var editdocumentSuffix 					= $('#appscr57documentSuffixTD'+i).text();
	
	var editethnicityIndicator				= $('#appscr57EthnicityIndicatorTD'+i).text();
	var editethnicityCuban					= $('#appscr57EthnicityCubanTD'+i).text();
	var editethnicityMexicanMexican			= $('#appscr57EthnicityMexicanMexicanTD'+i).text();
	var editethnicityPuertoRican			= $('#appscr57EthnicityPuertoRicanTD'+i).text();
	var editethnicityOther					= $('#appscr57EthnicityOtherTD'+i).text();
	var editethnicityText					= $('#appscr57EthnicityTextTD'+i).text();
	var editraceAmericanIndian				= $('#appscr57RaceAmericanIndianTD'+i).text();
	var editraceAsianIndian					= $('#appscr57RaceAsianIndianTD'+i).text();
	var editraceBlackOrAfricanAmerican		= $('#appscr57RaceBlackOrAfricanAmericanTD'+i).text();
	var editraceChinese						= $('#appscr57RaceChineseTD'+i).text();
	var editraceFilipino					= $('#appscr57RaceFilipinoTD'+i).text();
	var editraceGuamanianOrChamorro			= $('#appscr57RaceGuamanianOrChamorroTD'+i).text();
	var editraceJapanese					= $('#appscr57RaceJapaneseTD'+i).text();
	var editraceKorean						= $('#appscr57RaceKoreanTD'+i).text();
	var editraceNativeHawaiian				= $('#appscr57RaceNativeHawaiianTD'+i).text();
	var editraceOtherAsian					= $('#appscr57RaceOtherAsianTD'+i).text();
	var editraceOtherPacificIslander		= $('#appscr57RaceOtherPacificIslanderTD'+i).text();
	var editraceSamoan						= $('#appscr57RaceSamoanTD'+i).text();
	var editraceVietnamese					= $('#appscr57RaceVietnameseTD'+i).text();
	var editraceWhiteOrCaucasian			= $('#appscr57RaceWhiteOrCaucasianTD'+i).text();
	var editraceOther						= $('#appscr57RaceOtherTD'+i).text();
	var editraceOtherText					= $('#appscr57RaceOtherTextTD'+i).text();
	
	var ssn1 = editHouseHoldSSN.split("-")[0];
	var ssn2 = editHouseHoldSSN.split("-")[1];
	var ssn3 = editHouseHoldSSN.split("-")[2];
	
	var houseHoldname = $('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
	
	resetAddNonFinancialDetails();
	
	$('.nameOfHouseHold').text(houseHoldname);
	
	if(editHouseHoldgender == 'male')
	{
		$('#appscr61GenderMaleID').prop('checked',true);
		apppscr60SetGenderInSSAP();
	
	}
	else
	{
		$('#appscr61GenderFemaleID').prop('checked',true);
		apppscr60SetGenderInSSAP();
		
	}
	
	if(editHouseHoldHavingSSN == 'yes')
	{
		$('#socialSecurityCardHolderIndicatorYes').prop('checked',true);
		askForSSNNoPage61();
	}
	else
	{
		$('#socialSecurityCardHolderIndicatorNo').prop('checked',true);
		
		validation('appscr61');
		askForSSNNoPage61();
	}
	
	$('#ssn1').val(ssn1);
	$('#ssn2').val(ssn2);
	$('#ssn3').val(ssn3);
	
	if(editsameNameInSSNCardIndicator == "yes")
		$('#fnlnsSameIndicatorYes').prop("checked", true);
	else
	{
	
		$('#fnlnsSameIndicatorNo').prop("checked", true);
		$('#firstNameOnSSNCard').val(editfirstNameOnSSNCard);
		$('#middleNameOnSSNCard').val(editmiddleNameOnSSNCard);
		$('#lastNameOnSSNCard').val(editlastNameOnSSNCard);
		$('#suffixOnSSNCard').val(editsuffixOnSSNCard);
	}
	
	hasSameSSNNumberP61();
	
	if(editnotAvailableSSNReason == 0)
		$('#reasonableExplanationForNoSSN').val('');
	else
		$('#reasonableExplanationForNoSSN').val(editnotAvailableSSNReason);
	
	if(editUScitizen == 'no')
	{
		$('#UScitizenIndicatorNo').prop('checked',true);
		isUSCitizen();
	}
	else
	{
		$('#UScitizenIndicatorYes').prop('checked',true);
		isUSCitizen();
	}
	
	if(editlivedIntheUSSince1996Indicator == 'yes')
		$('#62Part_livedIntheUSSince1996IndicatorYes').prop('checked',true);
	else
		$('#62Part_livedIntheUSSince1996Indicator').prop('checked',true);
	
	if(editnaturalizedCitizenshipIndicator == 'yes')
		$('#naturalizedCitizenshipIndicatorYes').prop('checked',true);
	else
		$('#naturalizedCitizenshipIndicatorNo').prop('checked',true);
	
	if(editcertificateType == 'CitizenshipCertificate')
	{
		$('#naturalizedCitizenNaturalizedIndicator2').prop('checked',true);
		$('#citizenshipAlignNumber').val(editcertificateAlignNumber);
		$('#appscr62p1citizenshipCertificateNumber').val(editcertificateNumber);
		
		$(".appscr62Part1HideandShowFillingDetails").eq(1).show();
		$(".appscr62Part1HideandShowFillingDetails").eq(0).hide();
	}
	else
	{
		$('#naturalizedCitizenNaturalizedIndicator').prop('checked',true);
		$('#naturalizationAlignNumber').val(editcertificateAlignNumber);
		$('#naturalizationCertificateNumber').val(editcertificateNumber);
		
		$(".appscr62Part1HideandShowFillingDetails").eq(0).show();
		$(".appscr62Part1HideandShowFillingDetails").eq(1).hide();
	}
	
	$('#naturalCitizen_eligibleImmigrationStatus').prop('checked');
	
	
	if(editaliegnNumber!=" "){
		$('#DocAlignNumber').val(editaliegnNumber);
	} 
	if(editi94Number!=" "){
		$('#DocI-94Number').val(editi94Number);
	} 
	if(editForeignPassportNumber!=" "){
		$('#passportDocNumber').val(editForeignPassportNumber);
	} 
	if(editForeignPassportCountry!="0"){
		$('#appscr62P1County').val(editForeignPassportCountry);
	} 	
	if(editForeignPassportExpiration!="0"){
		$('#passportExpDate').val(editForeignPassportExpiration);
	} 
	if(editSevisID!="0"){
		$('#sevisIDNumber').val(editSevisID);
	} 
	
	
	
	
	
	if(editdocumentSameNameIndicator == 'no')
	{
		$('#62Part2_UScitizen').prop('checked',true);
		$('#documentFirstName').val(editdocumentFirstName);
		$('#documentMiddleName').val(editdocumentMiddleName);
		$('#documentLastName').val(editdocumentLastName);
		$('#documentSuffix').val(editdocumentSuffix);
		
		displayOrHideSameNameInfoPage62();
	}
	else
	{
		$('#62Part2_UScitizenYes').prop('checked',true);
		$('#documentFirstName').val('');
		$('#documentMiddleName').val('');
		$('#documentLastName').val('');
		$('#documentSuffix').val('0');
		
		displayOrHideSameNameInfoPage62();
	}

	if(editethnicityIndicator == "yes" || editethnicityIndicator == "Yes")
		$('#checkPersonNameLanguageYes').prop("checked", true);
	else
		$('#checkPersonNameLanguageNo').prop("checked", true);
	checkLanguage();
	
	if(editethnicityCuban == "true" || editethnicityCuban == "True")
		document.getElementById('ethnicity_cuban').checked = true;
	
	if(editethnicityMexicanMexican == "true" || editethnicityMexicanMexican == "True")
		document.getElementById('ethnicity_maxican').checked = true;
	
	if(editethnicityPuertoRican == "true" || editethnicityPuertoRican == "True")
		document.getElementById('ethnicity_puertoRican').checked = true;
	
	/*if(editethnicityOther == "true" || editethnicityPuertoRican == "True")
		document.getElementById('ethnicity_other').checked = true;
	else
		document.getElementById('ethnicity_other').checked = false;*/
	
	/*$('#other_ethnicity').val(editethnicityText);*/
	
	
	if(editraceAmericanIndian == "true" || editraceAmericanIndian == "True")
		document.getElementById('americanIndianOrAlaskaNative').checked = true;
	
	if(editraceAsianIndian == "true" || editraceAsianIndian == "True")
		document.getElementById('asianIndian').checked = true;
	
	if(editraceBlackOrAfricanAmerican == "true" || editraceBlackOrAfricanAmerican == "True")
		document.getElementById('BlackOrAfricanAmerican').checked = true;
	
	if(editraceChinese == "true" || editraceChinese == "True")
		document.getElementById('chinese').checked = true;
	
	if(editraceFilipino == "true" || editraceFilipino == "True")
		document.getElementById('filipino').checked = true;
	
	if(editraceGuamanianOrChamorro == "true" || editraceGuamanianOrChamorro == "True")
		document.getElementById('guamanianOrChamorro').checked = true;
	
	if(editraceJapanese == "true" || editraceJapanese == "True")
		document.getElementById('japanese').checked = true;
	
	if(editraceKorean == "true" || editraceKorean == "True")
		document.getElementById('korean').checked = true;
	
	if(editraceNativeHawaiian == "true" || editraceNativeHawaiian == "True")
		document.getElementById('nativeHawaiian').checked = true;
	
	if(editraceOtherAsian == "true" || editraceOtherAsian == "True")
		document.getElementById('otherAsian').checked = true;
	
	if(editraceOtherPacificIslander == "true" || editraceOtherPacificIslander == "True")
		document.getElementById('otherPacificIslander').checked = true;
	
	if(editraceSamoan == "true" || editraceSamoan == "True")
		document.getElementById('samoan').checked = true;
	
	if(editraceVietnamese == "true" || editraceVietnamese == "True")
		document.getElementById('vietnamese').checked = true;
	
	if(editraceWhiteOrCaucasian == "true" || editraceWhiteOrCaucasian == "True")
		document.getElementById('whiteOrCaucasian').checked = true;
	
	/*if(editraceOther == "true" || editraceOther == "True")
		document.getElementById('raceOther').checked = true;
	else
		document.getElementById('raceOther').checked = false;*/
	
	/*$('#other_race').val(editraceOtherText);*/
	

	
	if(!haveDataFlag)
		goToPageById('appscr61');
	
}


function appscr67EditControllerUpdateValues(i)
{
	$('#appscrHouseHoldgender57TD'+i).text($('input[name=appscr61_gender]:radio:checked').val());
	$('#appscr57HouseHoldHavingSSNTD'+i).text($('input[name=socialSecurityCardHolderIndicator]:radio:checked').val());
	
	var ssnNumber = $('#ssn1').val()+"-"+$('#ssn2').val()+"-"+$('#ssn3').val();
	$('#appscr57HouseHoldSSNTD'+i).text(ssnNumber);
	
	$('#appscr57notAvailableSSNReasonTD'+i).text($('#reasonableExplanationForNoSSN').val());
	$('#appscr57UScitizenTD'+i).text($('input[name=UScitizen]:radio:checked').val());
	$('#appscr57livedIntheUSSince1996IndicatorTD'+i).text($('input[name=livedIntheUSSince1996Indicator]:radio:checked').val());
	$('#appscr57naturalizedCitizenshipIndicatorTD'+i).text($('input[name=naturalizedCitizenshipIndicator]:radio:checked').val());
	
	$('#back_button_div').show();
}




function appscr74EditController(i)
{
	editMemberNumber = i;
	
	if(!haveDataFlag)
	{
		editFlag = true;	
		$('#back_button_div').hide();
	}
	
	
	
	var editIrsIncome 							  = $('#appscrirsIncome57TD'+i).text();
	
	var editIncomesStatus 						  = $('#incomesStatus'+i).text();	
	var editJobIncome 							  = $('#appscrFinancial_Job57TD'+i).text();
	var editJobFrequency 						  = $('#appscrFinancial_JobFrequency57TD'+i).text();
	var editEmployerName						  = $('#appscrFinancial_JobEmployerName57TD'+i).text();
	var editHoursOrDays							  = $('#appscrFinancial_JobHoursOrDays57TD'+i).text();
	var editSelfEmploymentIncome 				  = $('#appscrFinancial_Self_Employment57TD'+i).text();
	var editEmploymentTypeOfWork				  = $('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+i).text();
	var editSocialSecurityBenefitsIncome 		  = $('#appscrFinancial_SocialSecuritybenefits57TD'+i).text();
	var editSocialSecurityBenefitsIncomeFrequency = $('#appscrFinancial_SocialSecuritybenefitsFrequency57TD'+i).text();
	var editUnemploymentIncome 					  = $('#appscrFinancial_Unemployment57TD'+i).text();
	var editUnemploymentIncomeFrequency 		  = $('#appscrFinancial_UnemploymentFrequency57TD'+i).text();
	var editUnemploymentStateGovernment			  = $('#appscrFinancial_UnemploymentStateGovernment57TD'+i).text();
	var editunemploymentIncomeDate				  = $('#appscrFinancial_UnemploymentDate57TD'+i).text();
	var editRetirementPensionIncome 			  = $('#appscrFinancial_Retirement_pension57TD'+i).text();
	var editRetirementPensionIncomeFrequency 	  = $('#appscrFinancial_Retirement_pensionFrequency57TD'+i).text();
	var editCapitalgainsIncome 					  = $('#appscrFinancial_Capitalgains57TD'+i).text();
	var editInvestmentIncome 					  = $('#appscrFinancial_InvestmentIncome57TD'+i).text();
	var editInvestmentIncomeFrequency 			  = $('#appscrFinancial_InvestmentIncomeFrequency57TD'+i).text();
	var editRentalOrRoyaltyIncome 				  = $('#appscrFinancial_RentalOrRoyaltyIncome57TD'+i).text();
	var editRentalOrRoyaltyIncomeFrequency 		  = $('#appscrFinancial_RentalOrRoyaltyIncomeFrequency57TD'+i).text();
	var editFarmingOrFishingIncome 				  = $('#appscrFinancial_FarmingOrFishingIncome57TD'+i).text();
	var editFarmingOrFishingIncomeFrequency 	  = $('#appscrFinancial_FarmingOrFishingIncomeFrequency57TD'+i).text();
	var editAlimonyReceivedIncome 				  = $('#appscrFinancial_AlimonyReceived57TD'+i).text();
	var editAlimonyReceivedIncomeFrequency 		  = $('#appscrFinancial_AlimonyReceivedFrequency57TD'+i).text();
	var editOtherIncome 						  = $('#appscrFinancial_OtherIncome57TD'+i).text();
	var editOtherIncomeFrequency 				  = $('#appscrFinancial_OtherIncomeFrequency57TD'+i).text();
	var editOtherTypeIncome						  = $('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+i).text();
	
	var editExpectedIncome 						  = $('#appscrexpectedIncome57TD'+i).text();
	var editIrsHouseHoldExplanationTextArea 	  = $('#appscr57HouseHoldExplanationTextAreaTD'+i).text();
	
	var editHouseHoldalimonyAmount 				  = $('#appscr57HouseHoldalimonyAmountTD'+i).text();
	var editHouseHoldalimonyFrequency 			  = $('#appscr57HouseHoldalimonyFrequencyTD'+i).text();
	var editHouseHoldstudentLoanAmount 			  = $('#appscr57HouseHoldstudentLoanAmountTD'+i).text();
	var editHouseHoldstudentLoanFrequency 		  = $('#appscr57HouseHoldstudentLoanFrequencyTD'+i).text();
	var editHouseHolddeductionsType 			  = $('#appscr57HouseHolddeductionsTypeTD'+i).text();
	var editHouseHolddeductionsAmount 			  = $('#appscr57HouseHolddeductionsAmountTD'+i).text();
	var editHouseHolddeductionsFrequency 		  = $('#appscr57HouseHolddeductionsFrequencyTD'+i).text();
	
	var editHouseHoldbasedOnInput 				  = $('#appscr57HouseHoldbasedOnInputTD'+i).text();
	
	var editstopWorking							  = $("#appscrStopWorking57TD"+i).html();
	var editworkAt								  =	$("#appscrWorkAt57TD"+i).html();
	var editdecresedAt							  = $("#appscrDecresedAt57TD"+i).html();
	var editsalaryAt							  = $("#appscrSalaryAt57TD"+i).html();
	var editsesWorker							  = $("#appscrSesWorker57TD"+i).html();
	var editexplainText							  = $("#appscrExplainText57TD"+i).html();
	var editotherJointIncome					  = $("#appscrOtherJointIncome57TD"+i).html();
	
	var incomeStatusFlag = false;
	
	resetPage69Values();
	resetPage71Values();
	resetPage72DeductionValues();
	
	
	var houseHoldname = $('#appscr57FirstNameTD'+editMemberNumber).text()+" "+$('#appscr57MiddleNameTD'+editMemberNumber).text()+" "+$('#appscr57LastNameTD'+editMemberNumber).text();
	  
	  $('.nameOfHouseHold').text(houseHoldname);
	
	
	if(editIrsIncome != 0)
	{
		$('#irsIncome2013').val(editIrsIncome);
		
		irsIncomePrevYear();
		
		$('#irsIncomeLabel').text( $('#appscrirsIncome57TD'+i).text());
	}
	
	if(editIncomesStatus == "Yes" || editIncomesStatus == "yes")
	{
		$('#appscr70radios4IncomeYes').prop("checked",true);
		selectIncomesCheckBoxes();
		
		if(editJobFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
		
			document.getElementById('Financial_Job').checked = true;
			$('#Financial_Job_EmployerName_id').val(editEmployerName);
			$('#Financial_Job_HoursOrDays_id').val(editHoursOrDays);
			$('#Financial_Job_id').val(editJobIncome);
			$('#Financial_Job_select_id').val(editJobFrequency);
			showDiductionsFields();
		}
		else
		{
			document.getElementById('Financial_Job').checked = false;
			$('#Financial_Job_EmployerName_id').val('');
			$('#Financial_Job_HoursOrDays_id').val('');
			$('#Financial_Job_Div').hide();
		}
		
		if(editSelfEmploymentIncome != "0" && editSelfEmploymentIncome != "0.0")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Self_Employment').checked = true;
			$('#Financial_Self_Employment_TypeOfWork').val(editEmploymentTypeOfWork);
			$('#Financial_Self_Employment_id').val(editJobIncome);
		}
		else
		{
			document.getElementById('Financial_Self_Employment').checked = false;
			$('#Financial_Self_Employment_TypeOfWork').val('');
			$('#Financial_Self_Employment_Div').hide();
		}
		
		if(editSocialSecurityBenefitsIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_SocialSecuritybenefits').checked = true;
			$('#Financial_SocialSecuritybenefits_id').val(editSocialSecurityBenefitsIncome);
			$('#Financial_SocialSecuritybenefits_select_id').val(editSocialSecurityBenefitsIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_SocialSecuritybenefits').checked = false;
			$('#Financial_SocialSecuritybenefits_Div').hide();
		}
		
		if(editUnemploymentIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Unemployment').checked = true;
			$('#Financial_Unemployment_StateGovernment').val(editUnemploymentStateGovernment);
			$('#Financial_Unemployment_id').val(editUnemploymentIncome);
			$('#unemploymentIncomeDate').val(editunemploymentIncomeDate);
			$('#Financial_Unemployment_select_id').val(editUnemploymentIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_Unemployment').checked = false;
			$('#Financial_Unemployment_StateGovernment').val('');
			$('#unemploymentIncomeDate').val('');
			$('#Financial_Unemployment_Div').hide();
		}
		
		if(editRetirementPensionIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Retirement_pension').checked = true;
			$('#Financial_Retirement_pension_id').val(editRetirementPensionIncome);
			$('#Financial_Retirement_pension_select_id').val(editRetirementPensionIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_Retirement_pension').checked = false;
			$('#Financial_Retirement_pension_Div').hide();
		}
		
		if(editCapitalgainsIncome != "0" && editCapitalgainsIncome != "0.0")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Capitalgains').checked = true;
			$('#Financial_Capitalgains_id').val(editCapitalgainsIncome);
		}
		else
		{
			document.getElementById('Financial_Capitalgains').checked = false;
			$('#Financial_Capitalgains_Div').hide();
		}
		
		if(editInvestmentIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_InvestmentIncome').checked = true;
			$('#Financial_InvestmentIncome_id').val(editInvestmentIncome);
			$('#Financial_InvestmentIncome_select_id').val(editInvestmentIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_InvestmentIncome').checked = false;
			$('#Financial_InvestmentIncome_Div').hide();
		}
		
		if(editRentalOrRoyaltyIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_RentalOrRoyaltyIncome').checked = true;
			$('#Financial_RentalOrRoyaltyIncome_id').val(editRentalOrRoyaltyIncome);
			$('#Financial_RentalOrRoyaltyIncome_select_id').val(editRentalOrRoyaltyIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_RentalOrRoyaltyIncome').checked = false;
			$('#Financial_RentalOrRoyaltyIncome_Div').hide();
		}
		
		if(editFarmingOrFishingIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_FarmingOrFishingIncome').checked = true;
			$('#Financial_FarmingOrFishingIncome_id').val(editFarmingOrFishingIncome);
			$('#Financial_FarmingOrFishingIncome_select_id').val(editFarmingOrFishingIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_FarmingOrFishingIncome').checked = false;
			$('#Financial_FarmingOrFishingIncome_Div').hide();
		}
		
		if(editAlimonyReceivedIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_AlimonyReceived').checked = true;
			$('#Financial_AlimonyReceived_id').val(editAlimonyReceivedIncome);
			$('#Financial_AlimonyReceived_select_id').val(editAlimonyReceivedIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_AlimonyReceived').checked = false;
			$('#Financial_AlimonyReceived_Div').hide();
		}
		
		if(editOtherIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_OtherIncome').checked = true;
			$('#Financial_OtherTypeIncome').val(editOtherTypeIncome);
			$('#Financial_OtherIncome_id').val(editOtherIncome);
			$('#Financial_OtherIncome_select_id').val(editOtherIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_OtherIncome').checked = false;
			$('#Financial_OtherTypeIncome').val('');
			$('#Financial_OtherIncome_Div').hide();
		}
	}
	else
	{
		$('#appscr70radios4IncomeNo').prop("checked",true);
		selectIncomesCheckBoxes();
		
		hideDiductionsFields();
	}
	
	if(incomeStatusFlag == false)
	{
		$('#appscr70radios4IncomeNo').prop("checked",true);
		selectIncomesCheckBoxes();
		
		hideDiductionsFields();
	}
	
	if(editExpectedIncome != 0)
	{
		/*$('#expectedIncomeByHouseHoldMember').val(editExpectedIncome);*/
		$('#appscr69ExpectedIncome').val(editExpectedIncome);
	}
	
	if(editIrsHouseHoldExplanationTextArea != "")
	{
		$('#explanationTextArea').val(editIrsHouseHoldExplanationTextArea);
	}
	
	
	
	resetPage72DeductionValues();
	
	if(editHouseHoldalimonyFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72alimonyPaid').checked = true;
		$('#alimonyAmountInput').val(editHouseHoldalimonyAmount);
		$('#alimonyFrequencySelect').val(editHouseHoldalimonyFrequency);
		$('#appscr72alimonyPaidTR').show();
	}
		
	if(editHouseHoldstudentLoanFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72studentLoanInterest').checked = true;
		$('#studentLoanInterestAmountInput').val(editHouseHoldstudentLoanAmount);
		$('#studentLoanInterestFrequencySelect').val(editHouseHoldstudentLoanFrequency);
		$('#appscr72studentLoanInterestTR').show();
	}
	
	if(editHouseHolddeductionsFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72otherDeduction').checked = true;
		$('#deductionTypeInput').val(editHouseHolddeductionsType);
		$('#deductionAmountInput').val(editHouseHolddeductionsAmount);
		$('#deductionsFrequencySelect').val(editHouseHolddeductionsFrequency);
		$('#appscr72otherDeductionTR').show();
	}
	
	if(editHouseHoldbasedOnInput !="0" && editHouseHoldbasedOnInput != "0.0")
	{
		$('#basedOnAmountInput').val(editHouseHoldbasedOnInput);
	}
	
	if(editworkAt == "Yes" || editworkAt == "yes"){
		$('#appscr73WorkingIndicatorYes').prop("checked",true);
	}
	else{
		$('#appscr73WorkingIndicatorNo').prop("checked",true);
	}
	
	if(editdecresedAt == "Yes" || editdecresedAt == "yes"){
		$('#appscr73DecresedAtYes').prop("checked",true);
	}
	else{
		$('#appscr73DecresedAtNo').prop("checked",true);
	}
	
	if(editstopWorking == "Yes" || editstopWorking == "yes"){
		$('#appscr73stopWorkingIndicatorYes').prop("checked",true);
	}
	else{
		$('#appscr73stopWorkingIndicatorNo').prop("checked",true);
	}
	
	if(editsalaryAt == "Yes" || editsalaryAt == "yes"){
		$('#appscr73SalaryAtYes').prop("checked",true);
	}else{
		$('#appscr73SalaryAtNo').prop("checked",true);
	}
	
	if(editsesWorker == "Yes" || editsesWorker == "yes"){
		$('#appscr73SesWorkerYes').prop("checked",true);
	}else{
		$('#appscr73SesWorkerNo').prop("checked",true);
	}
	if(editotherJointIncome == "Yes" || editotherJointIncome == "yes"){
		$('#appscr73OtherJointIncomeYes').prop("checked",true);
	}else{
		$('#appscr73OtherJointIncomeNo').prop("checked",true);
	}
	
	
	$('#appscr73ExplanationTaxtArea').val(editexplainText);
	
	if(!haveDataFlag)
		goToPageById("appscr69");
	
}

function appscr74EditUpdateValues(i)
{

	$('#appscrirsIncome57TD'+i).text($('#irsIncome').val());
	
	$('#incomesStatus'+i).text($('input[name=appscr70radios4Income]:radio:checked').val());
	
	if($('input[name=appscr70radios4Income]:radio:checked').val() == 'Yes')
	{
		if($('#Financial_Job').is(":checked") == true)
		{
			$('#appscrFinancial_JobEmployerName57TD'+i).text($('#Financial_Job_EmployerName_id').val());
			$('#appscrFinancial_JobHoursOrDays57TD'+i).text($('#Financial_Job_HoursOrDays_id').val());
			$('#appscrFinancial_Job57TD'+i).text($('#Financial_Job_id').val());
			$('#appscrFinancial_JobFrequency57TD'+i).text($('#Financial_Job_select_id').val());
		}
		else
		{
			$('#appscrFinancial_JobEmployerName57TD'+i).text('');
			$('#appscrFinancial_JobHoursOrDays57TD'+i).text('');
			$('#appscrFinancial_Job57TD'+i).text(0);
			$('#appscrFinancial_JobFrequency57TD'+i).text("SelectFrequency");
		}
		
		if($('#Financial_Self_Employment').is(":checked") == true)
		{
			$('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+i).text($('#Financial_Self_Employment_TypeOfWork').val());
			$('#appscrFinancial_Self_Employment57TD'+i).text($('#Financial_Self_Employment_id').val());
		}
		else
		{
			$('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+i).text('');
			$('#appscrFinancial_Self_Employment57TD'+i).text(0);
		}
		
		if($('#Financial_SocialSecuritybenefits').is(":checked") == true)
		{
			$('#appscrFinancial_SocialSecuritybenefits57TD'+i).text($('#Financial_SocialSecuritybenefits_id').val());
			$('#appscrFinancial_SocialSecuritybenefitsFrequency57TD'+i).text($('#Financial_SocialSecuritybenefits_select_id').val());
		}
		else
		{
			$('#appscrFinancial_SocialSecuritybenefits57TD'+i).text(0);
			$('#appscrFinancial_SocialSecuritybenefitsFrequency57TD'+i).text("SelectFrequency");
		}
		
		if($('#Financial_Unemployment').is(":checked") == true)
		{
			$('#appscrFinancial_UnemploymentStateGovernment57TD'+i).text($('#Financial_Unemployment_StateGovernment').val());
			$('#appscrFinancial_Unemployment57TD'+i).text($('#Financial_Unemployment_id').val());
			$('#appscrFinancial_UnemploymentFrequency57TD'+i).text($('#Financial_Unemployment_select_id').val());
		}
		else
		{
			$('#appscrFinancial_UnemploymentStateGovernment57TD'+i).text('');
			$('#appscrFinancial_Unemployment57TD'+i).text(0);
			$('#appscrFinancial_UnemploymentFrequency57TD'+i).text("SelectFrequency");
		}
		
		if($('#Financial_Retirement_pension').is(":checked") == true)
		{
			$('#appscrFinancial_Retirement_pension57TD'+i).text($('#Financial_Retirement_pension_id').val());
			$('#appscrFinancial_Retirement_pensionFrequency57TD'+i).text($('#Financial_Retirement_pension_select_id').val());
		}
		else
		{
			$('#appscrFinancial_Retirement_pension57TD'+i).text(0);
			$('#appscrFinancial_Retirement_pensionFrequency57TD'+i).text("SelectFrequency");
		}
		
		if($('#Financial_Capitalgains').is(":checked") == true)
		{
			$('#appscrFinancial_Capitalgains57TD'+i).text($('#Financial_Capitalgains_id').val());
		}
		else
		{
			$('#appscrFinancial_Capitalgains57TD'+i).text(0);
		}
		
		if($('#Financial_InvestmentIncome').is(":checked") == true)
		{
			$('#appscrFinancial_InvestmentIncome57TD'+i).text($('#Financial_InvestmentIncome_id').val());
			$('#appscrFinancial_InvestmentIncomeFrequency57TD'+i).text($('#Financial_InvestmentIncome_select_id').val());
		}
		else
		{
			$('#appscrFinancial_InvestmentIncome57TD'+i).text(0);
			$('#appscrFinancial_InvestmentIncomeFrequency57TD'+i).text("SelectFrequency");
		}
		
		if($('#Financial_RentalOrRoyaltyIncome').is(":checked") == true)
		{
			$('#appscrFinancial_RentalOrRoyaltyIncome57TD'+i).text($('#Financial_RentalOrRoyaltyIncome_id').val());
			$('#appscrFinancial_RentalOrRoyaltyIncomeFrequency57TD'+i).text($('#Financial_RentalOrRoyaltyIncome_select_id').val());
		}
		else
		{
			$('#appscrFinancial_RentalOrRoyaltyIncome57TD'+i).text(0);
			$('#appscrFinancial_RentalOrRoyaltyIncomeFrequency57TD'+i).text("SelectFrequency");
		}
		
		if($('#Financial_FarmingOrFishingIncome').is(":checked") == true)
		{
			$('#appscrFinancial_FarmingOrFishingIncome57TD'+i).text($('#Financial_FarmingOrFishingIncome_id').val());
			$('#appscrFinancial_FarmingOrFishingIncomeFrequency57TD'+i).text($('#Financial_FarmingOrFishingIncome_select_id').val());
		}
		else
		{
			$('#appscrFinancial_FarmingOrFishingIncome57TD'+i).text(0);
			$('#appscrFinancial_FarmingOrFishingIncomeFrequency57TD'+i).text("SelectFrequency");
		}
		
		if($('#Financial_AlimonyReceived').is(":checked") == true)
		{
			$('#appscrFinancial_AlimonyReceived57TD'+i).text($('#Financial_AlimonyReceived_id').val());
			$('#appscrFinancial_AlimonyReceivedFrequency57TD'+i).text($('#Financial_AlimonyReceived_select_id').val());
		}
		else
		{
			$('#appscrFinancial_AlimonyReceived57TD'+i).text(0);
			$('#appscrFinancial_AlimonyReceivedFrequency57TD'+i).text("SelectFrequency");
		}
		
		if($('#Financial_OtherIncome').is(":checked") == true)
		{
			$('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+i).text($('#Financial_OtherTypeIncome').val());
			$('#appscrFinancial_OtherIncome57TD'+i).text($('#Financial_OtherIncome_id').val());
			$('#appscrFinancial_OtherIncomeFrequency57TD'+i).text($('#Financial_OtherIncome_select_id').val());
		}
		else
		{
			$('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+i).text('');
			$('#appscrFinancial_OtherIncome57TD'+i).text(0);
			$('#appscrFinancial_OtherIncomeFrequency57TD'+i).text("SelectFrequency");
		}
		
		/*if($('#expectedIncomeByHouseHoldMember').val() == '')
			$('#appscrexpectedIncome57TD'+i).text(0);
		else
			$('#appscrexpectedIncome57TD'+i).text($('#expectedIncomeByHouseHoldMember').val());*/
		
		$('#appscr57HouseHoldExplanationTextAreaTD'+i).text($('#explanationTextArea').val());	
		
		if($('#appscr72alimonyPaid').is(":checked") == true)
		{
			$('#appscr57HouseHoldalimonyAmountTD'+i).text($('#alimonyAmountInput').val());
			$('#appscr57HouseHoldalimonyFrequencyTD'+i).text($('#alimonyFrequencySelect').val());
		}
		else
		{
			$('#appscr57HouseHoldalimonyAmountTD'+i).text(0);
			$('#appscr57HouseHoldalimonyFrequencyTD'+i).text("SelectFrequency");
		}
		
		if($('#appscr72studentLoanInterest').is(":checked") == true)
		{
			$('#appscr57HouseHoldstudentLoanAmountTD'+i).text($('#studentLoanInterestAmountInput').val());
			$('#appscr57HouseHoldstudentLoanFrequencyTD'+i).text($('#studentLoanInterestFrequencySelect').val());
		}
		else
		{
			$('#appscr57HouseHoldstudentLoanAmountTD'+i).text(0);
			$('#appscr57HouseHoldstudentLoanFrequencyTD'+i).text("SelectFrequency");
		}
	
		if($('#appscr72otherDeduction').is(":checked") == true)
		{
			$('#appscr57HouseHolddeductionsTypeTD'+i).text($('#deductionTypeInput').val());
			$('#appscr57HouseHolddeductionsAmountTD'+i).text($('#deductionAmountInput').val());
			$('#appscr57HouseHolddeductionsFrequencyTD'+i).text($('#deductionsFrequencySelect').val());
		}
		else
		{
			$('#appscr57HouseHolddeductionsTypeTD'+i).text($('#deductionTypeInput').val());
			$('#appscr57HouseHolddeductionsAmountTD'+i).text(0);
			$('#appscr57HouseHolddeductionsFrequencyTD'+i).text("SelectFrequency");
		}
		
		if($('#basedOnAmountInput').val() == "")
			$('#appscr57HouseHoldbasedOnInputTD'+i).text(0);
		else
			$('#appscr57HouseHoldbasedOnInputTD'+i).text($('#basedOnAmountInput').val());
	}
	else
	{
		$('#appscrFinancial_JobEmployerName57TD'+i).text('');
		$('#appscrFinancial_JobHoursOrDays57TD'+i).text('');
		$('#appscrFinancial_Job57TD'+i).text(0);
		$('#appscrFinancial_JobFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+i).text('');
		$('#appscrFinancial_Self_Employment57TD'+i).text(0);
	
		$('#appscrFinancial_SocialSecuritybenefits57TD'+i).text(0);
		$('#appscrFinancial_SocialSecuritybenefitsFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrFinancial_UnemploymentStateGovernment57TD'+i).text('');
		$('#appscrFinancial_Unemployment57TD'+i).text(0);
		$('#appscrFinancial_UnemploymentFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrFinancial_Retirement_pension57TD'+i).text(0);
		$('#appscrFinancial_Retirement_pensionFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrFinancial_Capitalgains57TD'+i).text(0);
		
		$('#appscrFinancial_InvestmentIncome57TD'+i).text(0);
		$('#appscrFinancial_InvestmentIncomeFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrFinancial_RentalOrRoyaltyIncome57TD'+i).text(0);
		$('#appscrFinancial_RentalOrRoyaltyIncomeFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrFinancial_FarmingOrFishingIncome57TD'+i).text(0);
		$('#appscrFinancial_FarmingOrFishingIncomeFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrFinancial_AlimonyReceived57TD'+i).text(0);
		$('#appscrFinancial_AlimonyReceivedFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+i).text('');
		$('#appscrFinancial_OtherIncome57TD'+i).text(0);
		$('#appscrFinancial_OtherIncomeFrequency57TD'+i).text("SelectFrequency");
		
		$('#appscrexpectedIncome57TD'+i).text(0);
		
		$('#appscr57HouseHoldExplanationTextAreaTD'+i).text($('#explanationTextArea').val());
		
		$('#appscr57HouseHoldalimonyAmountTD'+i).text(0);
		$('#appscr57HouseHoldalimonyFrequencyTD'+i).text("SelectFrequency");
		
		$('#appscr57HouseHoldstudentLoanAmountTD'+i).text(0);
		$('#appscr57HouseHoldstudentLoanFrequencyTD'+i).text("SelectFrequency");
		
		$('#appscr57HouseHolddeductionsTypeTD'+i).text($('#deductionTypeInput').val());
		$('#appscr57HouseHolddeductionsAmountTD'+i).text(0);
		$('#appscr57HouseHolddeductionsFrequencyTD'+i).text("SelectFrequency");
		
		$('#appscr57HouseHoldbasedOnInputTD'+i).text(0);
		
		resetTotalInputsOrSelectboxesforEdit();
	}
	
	
	
	$('#appscrFinancial_JobEmployerName57TD'+financialHouseHoldDone).text($('#Financial_Job_EmployerName_id').val());
	$('#appscrFinancial_JobHoursOrDays57TD'+financialHouseHoldDone).text($('#Financial_Job_HoursOrDays_id').val());
	$('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+financialHouseHoldDone).text($('#Financial_Self_Employment_TypeOfWork').val());
	$('#appscrFinancial_UnemploymentStateGovernment57TD'+financialHouseHoldDone).text($('#Financial_Unemployment_StateGovernment').val());
	$('#appscrFinancial_UnemploymentDate57TD'+financialHouseHoldDone).text($('#unemploymentIncomeDate').val());
	$('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+financialHouseHoldDone).text($('#Financial_OtherTypeIncome').val());
	
	
	$('#appscrStopWorking57TD'+financialHouseHoldDone).text($('input[name=stopWorking]:radio:checked').val());
	$('#appscrWorkAt57TD1'+financialHouseHoldDone).text($('input[name=workAt]:radio:checked').val());
	$('#appscrDecresedAt57TD'+financialHouseHoldDone).text($('input[name=decresedAt]:radio:checked').val());
	$('#appscrSalaryAt57TD'+financialHouseHoldDone).text($('input[name=salaryAt]:radio:checked').val());
	$('#appscr57HouseHoldExplanationTextAreaTD'+financialHouseHoldDone).text($('#explanationTextArea').val());
	$('#appscrSesWorker57TD'+financialHouseHoldDone).text($('input[name=sesWorker]:radio:checked').val());
	$('#appscrExplainText57TD'+financialHouseHoldDone).text($('#appscr73ExplanationTaxtArea').val());
	$('#appscrOtherJointIncome57TD'+financialHouseHoldDone).text($('input[name=OtherJointIncome]:radio:checked').val());
	
	
}

function resetTotalInputsOrSelectboxesforEdit()
{
	$('#Financial_Job_EmployerName_id').val('');
	$('#Financial_Job_HoursOrDays_id').val(0);
	$('#Financial_Job_id').val(0);
	$('#Financial_Job_select_id').val('SelectFrequency');
	$('#Financial_Self_Employment_TypeOfWork').val('');
	$('#Financial_Self_Employment_id').val(0);
	$('#Financial_SocialSecuritybenefits_id').val(0);
	$('#Financial_SocialSecuritybenefits_select_id').val('SelectFrequency');
	$('#Financial_Unemployment_StateGovernment').val('');
	$('#Financial_Unemployment_id').val(0);
	$('#Financial_Unemployment_select_id').val('SelectFrequency');
	$('#Financial_Retirement_pension_id').val(0);
	$('#Financial_Retirement_pension_select_id').val('SelectFrequency');
	$('#Financial_Capitalgains_id').val(0);
	$('#Financial_InvestmentIncome_id').val(0);
	$('#Financial_InvestmentIncome_select_id').val('SelectFrequency');
	$('#Financial_RentalOrRoyaltyIncome_id').val(0);
	$('#Financial_RentalOrRoyaltyIncome_select_id').val('SelectFrequency');
	$('#Financial_FarmingOrFishingIncome_id').val(0);
	$('#Financial_FarmingOrFishingIncome_select_id').val('SelectFrequency');
	$('#Financial_AlimonyReceived_id').val(0);
	$('#Financial_AlimonyReceived_select_id').val('SelectFrequency');
	$('#Financial_OtherTypeIncome').val('');
	$('#Financial_OtherIncome_id').val(0);
	$('#Financial_OtherIncome_select_id').val('SelectFrequency');
	/*$('#expectedIncomeByHouseHoldMember').val(0);*/
	$('#explanationTextArea').val('');
	$('#alimonyAmountInput').val(0);
	$('#alimonyFrequencySelect').val('SelectFrequency');
	$('#studentLoanInterestAmountInput').val(0);
	$('#studentLoanInterestFrequencySelect').val('SelectFrequency');
	$('#deductionTypeInput').val('');
	$('#deductionAmountInput').val(0);
	$('#deductionsFrequencySelect').val('SelectFrequency');
	$('#basedOnAmountInput').val(0);
}

// Additional Questions Editing

function appscr75AEditController(i)
{
	
	
	goToPageById("appscr75A");
}


//	Edit functionality Conditions calling form navigation.js

function editFunctionalityConditions()
{

	if(currentPage == "appscr75A")
		totalIncomeShowMethod();	
	
	if(editFlag == true && currentPage == "appscr62Part1")
	{
		$('#back_button_div').hide();
	}
	
	if(editFlag == true && currentPage == "appscr70")
	{
		$('#back_button_div').hide();
	}
	
}


var updateTo='';
var flagToUpdateFromResult=false;

/* 
 * @returns no of Household 
 */
function checkStillUpdateRequired()
{
	updateTo = $('input[name=editRadioGroup]:radio:checked').val();
	
	
	flagToUpdateFromResult=false;
	var noOfHouseHold = parseInt($('#numberOfHouseHold').text());
	
	for(var memberId=1; memberId <= noOfHouseHold; memberId++ )
		{
				var check = $('#'+updateTo+memberId).is(':checked');
				if(check)
				{
					$('#finalEditDiv').hide();	
				switch(updateTo)
				{
				case "HouseHoldDetailsUpdate":
					flagToUpdateFromResult = true;
					
					fetchDataHouseholdUpdate(memberId);
					$('#'+updateTo+memberId).prop('checked', false);
					editMemberNumber=memberId;
					goToPageById("appscr60");
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					setNonFinancialPageProperty(memberId);
					
					break;
					
				case "nonFinancialUpdate":  
					
					
					flagToUpdateFromResult = true;
					$('#'+updateTo+memberId).prop('checked', false);
					appscr67EditController(memberId);
					goToPageById("appscr61");
					editMemberNumber=memberId;
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					setNonFinancialPageProperty(memberId);
					
					break;
					
					
				case "financialDetailsUpdate":	
					
					flagToUpdateFromResult = true;
					$('#'+updateTo+memberId).prop('checked', false);
					
					appscr74EditController(memberId);
					editMemberNumber=memberId;
					goToPageById("appscr69");
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					
					setNonFinancialPageProperty(memberId);
					
					break;
					
				
				
				}
				
				break;
			}
		}

	return flagToUpdateFromResult;

}


function fetchDataHouseholdUpdate(targetIndex) {
	
	var houseHoldIndex = targetIndex;

	getSpouse(houseHoldIndex);
	var martialStatus=$("#appscr57isMarriedTD"+houseHoldIndex).text();
	var spouseID=$("#appscr57spouseIdTD"+houseHoldIndex).text();	
	var isPayingJointly=$("#appscr57payJointlyTD"+houseHoldIndex).text();
	var  isTaxFiller=$("#appscr57isTaxFilerTD"+houseHoldIndex).text();
	if(martialStatus.toLowerCase()=="yes"||martialStatus.toLowerCase()=="true")
		{
			$("#marriedIndicatorYes").prop('checked',true);
		}
	else if(martialStatus.toLowerCase()=="no"||martialStatus.toLowerCase()=="false") {
			$("#marriedIndicatorNo").prop('checked',true);
	}
	if(isPayingJointly.toLowerCase()=="yes"||isPayingJointly.toLowerCase()=="true"){
		$("#planOnFilingJointFTRIndicatorYes").prop('checked',true);
	}
	else if(isPayingJointly.toLowerCase()=="no"||isPayingJointly.toLowerCase()=="false") {
		$("#planOnFilingJointFTRIndicatorNo").prop('checked',true);
	}
	if (isTaxFiller.toLowerCase()=="yes"||isTaxFiller.toLowerCase()=="true") {
		$("#planToFileFTRIndicatorYes").prop('checked',true);
		
	} else if (isTaxFiller.toLowerCase()=="no"||isTaxFiller.toLowerCase()=="false") { {
		$("#planToFileFTRIndicatorNo").prop('checked',true);
	}
	for ( var hNum = 1; hNum <= noOfHouseHold; hNum++) {
		if($("#HouseHold"+hNum).is(":checked")){
			if ($("#HouseHold"+hNum)==spouseID) {
				$("#HouseHold"+hNum).prop('checked',true);
				break;
			}
		}
		
		}
	}

	
	
}
