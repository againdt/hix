<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema" targetNamespace="http://ahbx.accenture.com/AHBXValidation"
	xmlns:tns="http://ahbx.accenture.com/AHBXValidation"
	elementFormDefault="qualified">

	<simpleType name="StringFixed10">
		<annotation>
			<documentation>
				This field is used where we required alpha numeric
				values which should be length = 10.
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="10" fixed="true" />
		</restriction>
	</simpleType>

	<simpleType name="StringLength10">
		<annotation>
			<documentation>
				This field is used where we required alpha numeric
				values which should be length = 10.
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="[a-zA-Z0-9]{1,10}"></pattern>
			<minLength value="1" />
			<maxLength value="10" />
		</restriction>
	</simpleType>

	<simpleType name="LongLength10">
		<annotation>
			<documentation>
				This field is used where we required numeric values
				which have at max 10
				digits.
			</documentation>
		</annotation>
		<restriction base="long">
			<minInclusive value="1"></minInclusive>
			<maxInclusive value="9999999999"></maxInclusive>
		</restriction>
	</simpleType>
	
	<simpleType name="StringLength16">
		<annotation>
			<documentation>
				This field is used where we required alpha numeric
				values which should be length = 16.
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="[a-zA-Z0-9]{1,16}"></pattern>
			<minLength value="1" />
			<maxLength value="16" />
		</restriction>
	</simpleType>
	
	<simpleType name="acceptanceStatusValue">
		<annotation>
			<documentation>
				This field is used to denote the status values 
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="Pending" />
			<enumeration value="Accepted" />
			<enumeration value="Declined" />
		</restriction>
	</simpleType>

	<simpleType name="AccountNumber">
		<annotation>
			<documentation>
				This type is used wherever Account Number
				element is
				required with the
				max length restriction as 17.
			</documentation>
		</annotation>
		<restriction base="string">
		<minLength value="1" />
			<maxLength value="17" />
		</restriction>
	</simpleType>
	<simpleType name="LicenseNumber">
		<annotation>
			<documentation>
				This type is used wherever License Number element is
				required with the max length restriction as 7.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="7"></maxLength>
		</restriction>
	</simpleType>
	<simpleType name="CertificationCode">
		<annotation>
			<documentation>
				This type is used wherever Certification element is
				required with the possible values.
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="2"></length>
			<enumeration value="PE"></enumeration>
			<enumeration value="WI"></enumeration>
			<enumeration value="EL"></enumeration>
			<enumeration value="CE"></enumeration>
			<enumeration value="DE"></enumeration>
			<enumeration value="TV"></enumeration>
			<enumeration value="TC"></enumeration>
			<enumeration value="DC"></enumeration>
			<enumeration value="WR"></enumeration>
			<enumeration value="DT"></enumeration>
			<enumeration value="DB"></enumeration>
			<enumeration value="DI"></enumeration>
			<enumeration value="DS"></enumeration>
			<enumeration value="DM"></enumeration>
			<enumeration value="DA"></enumeration>
			<enumeration value="ED"></enumeration>
			<enumeration value="EF"></enumeration>
			<enumeration value="EM"></enumeration>
			<enumeration value="IC"></enumeration>
			<enumeration value="DP"></enumeration>
			<enumeration value="DF"></enumeration>
		</restriction>
	</simpleType>

	<simpleType name="RegistrationCode">
		<annotation>
			<documentation>
				This type is used wherever Registration element is
				required with the possible values.
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="2"></length>
			<enumeration value="PE"></enumeration>
			<enumeration value="WE"></enumeration>
			<enumeration value="DO"></enumeration>
			<enumeration value="DQ"></enumeration>
			<enumeration value="DE"></enumeration>
			<enumeration value="DC"></enumeration>
			<enumeration value="DU"></enumeration>
			<enumeration value="RE"></enumeration>
			<enumeration value="AC"></enumeration>
			<enumeration value="DT"></enumeration>
			<enumeration value="DF"></enumeration>
			<enumeration value="DR"></enumeration>
			<enumeration value="DM"></enumeration>
		</restriction>
	</simpleType>

	<simpleType name="UserType">
		<annotation>
			<documentation>
				This field is used to denote the different user types
				available in our
				application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="Broker"></enumeration>
			<enumeration value="Carrier"></enumeration>
			<enumeration value="Navigator individual">
			</enumeration>
			<enumeration value="Navigator with enrollment entity"></enumeration>
		</restriction>
	</simpleType>

	<simpleType name="RecordType">
		<annotation>
			<documentation>
				This field is used to denote the different user types
				available in our application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="Agent"></enumeration>
			<enumeration value="Assister"></enumeration>
			<enumeration value="Entity"></enumeration>
		</restriction>
	</simpleType>

	<simpleType name="PlanTierIDType">
		<annotation>
			<documentation>
	This field is used to denote the different plan Tier
	Types available in our application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="CATASTROPHIC" />
			<enumeration value="BRONZE" />
			<enumeration value="SILVER" />
			<enumeration value="LOW" />
			<enumeration value="GOLD" />
			<enumeration value="HIGH" />
			<enumeration value="PLATINUM" />
		</restriction>
	</simpleType>

	<simpleType name="StartEndDate">
		<restriction base="string">
			<pattern value="(0?[1-9]|1[012])(0?[1-9]|[12][0-9]|3[01])((20)\d\d)">
			</pattern>
		</restriction>
	</simpleType>

	<simpleType name="StringLength50">
		<annotation>
			<documentation>
				This type is used wherever Name/Alphanumeric
				element
				required with the
				max length restriction as 50.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="50"></maxLength>
		</restriction>
	</simpleType>
	
	<simpleType name="StringLength100">
		<annotation>
			<documentation>
				This type is used wherever Name/Alphanumeric
				element
				required with the
				max length restriction as 100.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="100"></maxLength>
		</restriction>
	</simpleType>

    <simpleType name="StringLength255">
		<annotation>
			<documentation>
				This type is used wherever Name/Alphanumeric
				element
				required with the
				max length restriction as 255.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="255"></maxLength>
		</restriction>
	</simpleType>
	
	<simpleType name="Date">
		<annotation>
			<documentation>
				This type is used where ever date required.
				Padding
				zero in the left side is not mandatory for date month. (mm/dd/yyyy)
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/[\d]{4}"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="EINType">
		<annotation>
			<documentation>
				This specify the EIN format of US. (length=9)
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="9" fixed="true" />
		</restriction>
	</simpleType>

	<simpleType name="StateEINType">
		<annotation>
			<documentation>
				This specify the EIN format of US.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="20"></maxLength>
		</restriction>
	</simpleType>

	<simpleType name="AddressLineType">
		<annotation>
			<documentation>
				This type used to specify the limit of the each
				address line should
				follow.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="25"></maxLength>
		</restriction>
	</simpleType>

	<simpleType name="CityType">
		<annotation>
			<documentation>
				This type used to specify the limit of the each
				City
				field should
				follow.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="30"></maxLength>
		</restriction>
	</simpleType>

	<simpleType name="StateType">
		<annotation>
			<documentation>
				This type used to specify the format of the
				State field
				should follow. (2 letter)
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="2"></maxLength>
			<pattern value="[a-zA-Z]*"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="ZipCodeType">
		<annotation>
			<documentation>
				This type is used wherever zipcode
				element is required
				with the pattern as aaaaa-bbbb
				(where first 5 elements are
				mandatory).
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="[\d]{5}(-[\d]{4})?"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="PhoneNumberType">
		<annotation>
			<documentation>
				This type is used to specify the format of the
				phone
				number (10 digits).
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="[\d]{10}"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="StringFixed6">
		<annotation>
			<documentation>
				This field is used where ever we required alphanumeric
				values and length of data fixed to 6 letters.
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="6" fixed="true"></length>
		</restriction>
	</simpleType>

	<simpleType name="YNFlag">
		<annotation>
			<documentation>
				This Flag field is used to specify the Y/N values
				in
				both cases.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="1"></maxLength>
			<pattern value="[yYnN]"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="TrueFalseFlag">
		<annotation>
			<documentation>
				This Flag field is used to specify the True/false
				values in
				both cases.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="True"></enumeration>
			<enumeration value="true"></enumeration>
			<enumeration value="False"></enumeration>
			<enumeration value="false"></enumeration>
		</restriction>
	</simpleType>

	<simpleType name="RecordIndicator">
		<annotation>
			<documentation>
				This Flag field is used to specify the New/Update
				values in
				both cases.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="New"></enumeration>
			<enumeration value="Update"></enumeration>
			<enumeration value="new"></enumeration>
			<enumeration value="update"></enumeration>
		</restriction>
	</simpleType>

	<simpleType name="ResponseCodeType">
		<annotation>
			<documentation>
				This type used to specify the response code format
				which is yet to be
				confirmed. (Temp: it will be Integer without
				any
				restriction)
			</documentation>
		</annotation>
		<restriction base="int"></restriction>
	</simpleType>

	<simpleType name="ResponseDescriptionType">
		<annotation>
			<documentation>
				This type used to specify the response description
				format which is yet to be
				confirmed. (Temp: it will be Alphanumeric
				without any restriction)
			</documentation>
		</annotation>
		<restriction base="string"></restriction>
	</simpleType>

	<simpleType name="AccountType">
		<restriction base="string">
			<enumeration value="Savings"></enumeration>
			<enumeration value="savings"></enumeration>
			<enumeration value="Checking"></enumeration>
			<enumeration value="checking"></enumeration>
		</restriction>
	</simpleType>

	<simpleType name="StringFixed4">
		<annotation>
			<documentation>
				This field is used where ever we required alphanumeric
				values and length of data fixed to 6 letters.
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="4" fixed="true"></length>
		</restriction>
	</simpleType>

	<simpleType name="responseValidInvalid">
		<annotation>
			<documentation>
				This type used to specify the response values as VALID
				or INVALID
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="VALID" />
			<enumeration value="INVALID" />
			<enumeration value="FAILURE" />
		</restriction>
	</simpleType>

	<simpleType name="StringFixed2">
		<annotation>
			<documentation>
				This field is used where ever we required alphanumeric
				values and length of data fixed to 2 letters.
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="2" fixed="true"></length>
		</restriction>
	</simpleType>

	<simpleType name="NumericFixed4">
		<annotation>
			<documentation>
				This field is used where ever we required numeric
				values and length of data fixed to 4 digits.
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="[\d]{4}"></pattern>
		</restriction>
	</simpleType>
	<simpleType name="StringLength80">
		<annotation>
			<documentation>
				This field is used where we required a String
				which should be length be max 80.
			</documentation>
		</annotation>
		<restriction base="string">
			<minLength value="1" />
			<maxLength value="80" />
		</restriction>
	</simpleType>
	
	<simpleType name="UserRecordType">
		<annotation>
			<documentation>
				This field is used to denote the different record
				types
				available in our
				application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="Assister"></enumeration>
			<enumeration value="AssisterEnrollmentEntity"></enumeration>
			<enumeration value="Issuer"></enumeration>
			<enumeration value="Agent"></enumeration>
		</restriction>
	</simpleType>
	<simpleType name="LongLength5">
		<annotation>
			<documentation>
				This field is used where we required numeric values
				which have at max 5
				digits.
			</documentation>
		</annotation>
		<restriction base="long">
			<minInclusive value="1"></minInclusive>
			<maxInclusive value="99999"></maxInclusive>
		</restriction>
	</simpleType>
	<simpleType name="LongLength2">
		<annotation>
			<documentation>
				This field is used where we required numeric values
				which have at max 2
				digits.
			</documentation>
		</annotation>
		<restriction base="long">
			<minInclusive value="1"></minInclusive>
			<maxInclusive value="99"></maxInclusive>
		</restriction>
	</simpleType>
	<simpleType name="EntityType">
		<annotation>
			<documentation>
				This field is used to denote the different entity
				types
				available in our
				application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="In-Person Assistance" />
			<enumeration value="Navigator Organization" />
		</restriction>
	</simpleType>
	
	<simpleType name="orgType">
		<annotation>
			<documentation>
				This field is used to denote the different organization
					types
					available in our
					application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="School" />
			<enumeration value="Faith-based organization" />
			<enumeration value="Community Clinic" />
			<enumeration value="Community-Based organization" />
			<enumeration value="Public Partnership" />
			<enumeration value="Private Partnership" />
			<enumeration value="Agents" />
		</restriction>
	</simpleType>
	
	<simpleType name="receivePaymentType">
		<annotation>
			<documentation>
				This type used to specify the request receive payments values as VALID
				or INVALID
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="Yes" />
			<enumeration value="No" />
		</restriction>
	</simpleType>
	
	<simpleType name="AgentAssisterType">
		<annotation>
			<documentation>
				This field is used to denote the different record
				types
				available in our
				application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="Assister"></enumeration>
			<enumeration value="Agent"></enumeration>
		</restriction>
	</simpleType>
	
	<simpleType name="AcceptanceStatusType">
		<restriction base="string">
			<enumeration value="C">
				<annotation>
					<documentation>It refers to status as "Accepted"
					</documentation>
				</annotation>
			</enumeration>
			<enumeration value="D">
				<annotation>
					<documentation>It refers to status as "Declined"
					</documentation>
				</annotation>
			</enumeration>
		</restriction>
	</simpleType>
	<simpleType name="EnrollmentTypeValue">
		<restriction base="string">
			<enumeration value="I">
				<annotation>
					<documentation>It refers to Initial Enrollment
					</documentation>
				</annotation>
			</enumeration>
			<enumeration value="A">
				<annotation>
					<documentation>It refers to Renewal
					</documentation>
				</annotation>
			</enumeration>
			<enumeration value="S">
				<annotation>
					<documentation>It refers to Special Enrollment
					</documentation>
				</annotation>
			</enumeration>
			<enumeration value="N">
				<annotation>
					<documentation>It refers to Shop Enrollment
					</documentation>
				</annotation>
			</enumeration>
		</restriction>
	</simpleType>
			
	<simpleType name="applicationStatus">
		<annotation>
			<documentation>
				This field is used to denote the different 
					application status.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="WITHDRAWN" />
			<enumeration value="COMPLETED " />
			<enumeration value="SUBMITTED" />
			<enumeration value="IN PROGRESS" />
			<enumeration value="CLOSED - NOT COMPLETED" />
			<enumeration value="RENEWAL" />
			<enumeration value="RAC-WITHDRAWN" />
			<enumeration value="TERMINATED" />
		</restriction>
	</simpleType>
	
	<simpleType name="maintenanceReasonCodeVal">
		<annotation>
			<documentation>
				This field is used to denote the different 
					maintenance reason codes.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="07" />
			<enumeration value="14 " />
			<enumeration value="28" />
			<enumeration value="59" />
			<enumeration value="AI" />
			<enumeration value="EC" />
		</restriction>
	</simpleType>	

<simpleType name="booleanType">	
		<restriction base="string">
			<enumeration value="Yes" />
			<enumeration value="No" />
		</restriction>
	</simpleType>
	 <simpleType name="EligibilityStatusType">
		<restriction base="string">
			<enumeration value="Eligible" />
			<enumeration value="Ineligible" />
			<enumeration value="Conditional Eligible" />			
		</restriction>
	</simpleType>
	
		<simpleType name="EnrollmentStatusType">
		<restriction base="string">
			<enumeration value="CANCEL" />
			<enumeration value="TERM" />
			<enumeration value="CONFIRM" />
		</restriction>
	</simpleType>
</schema>