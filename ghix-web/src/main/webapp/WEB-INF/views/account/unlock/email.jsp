<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.util.Properties"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%!private static ApplicationContext ctx = null;%>
<%!private static Properties prop = null;%>
<%
        if (ctx == null) {
                ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
        }
        if (prop == null) {
                prop = (Properties) ctx.getBean("configProp");
        }
%>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="gutter10">
	<h1>Unlock Your Account</h1>
	<%-- <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	</div> --%>
	<div class="row-fluid">
	
		<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">You should know</h4>
			</div>
			<ul class="nav nav-list">
			    <li>To unlock your account you will need your email, answers to security questions and certain miscellaneous personal information.</li>
		    </ul>
		</div>
		
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left">Step 1: Enter your email address</h4>
			</div>
			<form class="form-horizontal gutter10" id="emailToUnlockAcc" name="emailToUnlockAcc" action="<c:url value="/account/unlock/email"/>" method="POST" onsubmit="formSubmit()">
				<df:csrfToken/>
				<div class="control-group">
					<label for="emailAddress" class="control-label"><spring:message code="label.userEmail"/></label>
						<div class="controls">
							<input type="text"  id="emailAddress" name="emailAddress" class="xlarge" size="30" value="${userEmailAddress}">	
							<div class="error help-inline" id="emailAddress_error"></div>
							<div id="emailAddress_valid" style="display:none;"><label class="error"><span> <em class="excl">!</em><spring:message  code="label.validateEmailAddress"/></span></label></div>													
							
							<c:if test="${!empty errorMsg}">
								<div class="alert alert-error alert-error-custom">
									<c:out value="${errorMsg}"></c:out>
								</div>
							</c:if>
						
						</div>
				</div>
				
				<c:if test="${hideCaptcha != 'true'}">
					<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %> 
					<div id="captcha">
						<h4><spring:message code="label.proveYouAreNotARobot" /></h4>
						<fieldset>
							<div class="control-group">
								<label class="control-label"><spring:message code="label.securityCode" /><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<div class="g-recaptcha" data-sitekey="<%=prop.getProperty("Recaptcha.siteKey")%>" data-callback="reCapchaFilled"></div>
									
									<input type="hidden" name="hiddenRecaptcha" id="hiddenRecaptcha">
									<div class="error help-inline" id="hiddenRecaptcha_error"></div>
									
									<div id="securityCode_error" class=" error help-inline" style="color: red">
										${captchaError}
									</div>
								</div>
								
							</div>
						</fieldset>
					</div>
					<!--/#captcha -->
				</c:if>					
				
				<div class="control-group">
					<div class="controls">							
						<input type="submit" value="<spring:message  code='label.brkContinue'/>" class="btn btn-primary btn-large" title="<spring:message  code='label.brkContinue'/>">
					</div>
				</div>		
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function validateEmail(sEmail) {
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		if (filter.test(sEmail)) {
		    return true;
		}
		else {
		    return false;
		}
	}
	
	/*Jquery validator to validate either required email address and invalid email address*/
	jQuery.validator.addMethod("validateLogin", function(value,
				element, param) {
			 var sEmail = $('#emailAddress').val();
			 if (!validateEmail(sEmail)){
				 return false;
			 }
			 return true;
		});


	var validator = $("#emailToUnlockAcc").validate({ 
		ignore: ".ignore",
		rules : {
			emailAddress: { 
				required : true,
				validateLogin:true	
			},
			hiddenRecaptcha: {
	            required: function () {
	                if(grecaptcha.getResponse() === '') {
	                	return true;
	                }else {
	                	return false;
	                }
	            }
	        }
		},
		messages : {
			emailAddress: { 
				required : "<span> <em class='excl'>!</em>Email address is required.</span>",
				validateLogin:"<span> <em class='excl'>!</em>Please enter valid email address.</span>"
			},
			hiddenRecaptcha: {
		        required: "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Please confirm that you are not a robot.</span>"
		    }
		},
		onkeyup: false,
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		}
	});
	
	function reCapchaFilled() {
		$('#hiddenRecaptcha_error, #securityCode_error').hide();
	}

	function formSubmit() {
		$('#securityCode_error').hide();
	}
</script>
