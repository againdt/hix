import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Form from './Form';

// Action creators

import { changeData } from '../../actions/actionCreators';

// Constants

import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import UniversalInput from '../../lib/UniversalInput';


const Page1_03_E1 = props => {

    const {
        data,
        data: {
            singleStreamlinedApplication: {
                applyingForFinancialAssistanceIndicator
            }
        }
    } = props;

    const currentData = data
        .singleStreamlinedApplication;

    const onFieldChange = newData => {
        const newState = {...data };
        newState.singleStreamlinedApplication = newData;
        props.actions.changeData(newState);
    };

    return (
        <Fragment>
            <div id="saHelpPayingForCoverage" className="fade-in">
                <div className="subsection">
                    <h3 className="usa-heading-alt">
                        You may be eligible for a free or low-cost plan, or tax credit to help pay your monthly premiums</h3>

                    <Form>
                        <UniversalInput
                            legend={{
                                legendText: 'Do you want to find out if you can get help paying for health coverage?'
                            }}
                            label={{
                                text: DROPDOWN_OPTIONS.YES_NO,
                                labelClass: ['usa-width-one-whole'],
                                ulClass: ['usa-width-one-whole layout--02'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'applyingForFinancialAssistanceIndicator',
                                value: applyingForFinancialAssistanceIndicator,
                                options: DROPDOWN_OPTIONS.FLOW_TYPE,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: []

                            }}
                            errorClasses={['margin-l-0']}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page1_03_E1.defaultProps = {
    name: 'Page1_03_E1'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page1_03_E1);
