package com.getinsured.hix.admin.util;

import java.math.BigDecimal;

/**
 * This class is the plan management report record type for Data-wise statistics
 * HIX-26726
 * 
 * @author Nikhil Talreja
 * @since 08 January, 2013
 *
 */

public class PlanManagmentReportsRecord {
	
	private String date;
	private BigDecimal createdCount;
	private BigDecimal startDateCount;
	private BigDecimal endDateCount;
	private BigDecimal updationCount;
	private BigDecimal certifiedDateCount;
	private BigDecimal enrollmentEffectiveDateCount;
	
	public PlanManagmentReportsRecord() {
	}
	
	public PlanManagmentReportsRecord(String date, BigDecimal createdCount,
			BigDecimal startDateCount, BigDecimal endDateCount,
			BigDecimal updationCount, BigDecimal certifiedDateCount,
			BigDecimal enrollmentEffectiveDateCount) {
		this.date = date;
		this.createdCount = createdCount == null ? BigDecimal.ZERO : createdCount;
		this.startDateCount = startDateCount == null ? BigDecimal.ZERO : startDateCount;
		this.endDateCount = endDateCount == null ? BigDecimal.ZERO : endDateCount;
		this.updationCount = updationCount == null ? BigDecimal.ZERO : updationCount;
		this.certifiedDateCount = certifiedDateCount == null ? BigDecimal.ZERO : certifiedDateCount;
		this.enrollmentEffectiveDateCount = enrollmentEffectiveDateCount == null ? BigDecimal.ZERO : enrollmentEffectiveDateCount;
	}

	@Override
	public String toString() {
		return "PlanManagmentReportsRecord [date=" + date + ", createdCount="
				+ createdCount + ", startDateCount=" + startDateCount
				+ ", endDateCount=" + endDateCount + ", updationCount="
				+ updationCount + ", certifiedDateCount=" + certifiedDateCount
				+ ", enrollmentEffectiveDateCount="
				+ enrollmentEffectiveDateCount + "]";
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public BigDecimal getCreatedCount() {
		return createdCount;
	}
	public void setCreatedCount(BigDecimal createdCount) {
		this.createdCount = createdCount;
	}
	public BigDecimal getStartDateCount() {
		return startDateCount;
	}
	public void setStartDateCount(BigDecimal startDateCount) {
		this.startDateCount = startDateCount;
	}
	public BigDecimal getEndDateCount() {
		return endDateCount;
	}
	public void setEndDateCount(BigDecimal endDateCount) {
		this.endDateCount = endDateCount;
	}
	public BigDecimal getUpdationCount() {
		return updationCount;
	}
	public void setUpdationCount(BigDecimal updationCount) {
		this.updationCount = updationCount;
	}

	public BigDecimal getCertifiedDateCount() {
		return certifiedDateCount;
	}

	public void setCertifiedDateCount(BigDecimal certifiedDateCount) {
		this.certifiedDateCount = certifiedDateCount;
	}

	public BigDecimal getEnrollmentEffectiveDateCount() {
		return enrollmentEffectiveDateCount;
	}

	public void setEnrollmentEffectiveDateCount(
			BigDecimal enrollmentEffectiveDateCount) {
		this.enrollmentEffectiveDateCount = enrollmentEffectiveDateCount;
	}
	
}
