package com.getinsured.hix.platform.web.emailstat.controller;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.getinsured.hix.platform.emailstat.service.IEmailEventsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//import com.getinsured.hix.admin.util.AdminUserAccountActivationEmail;
//import com.getinsured.hix.model.Notice;
//import com.getinsured.hix.platform.repository.NoticeRepository;

@Controller
@RequestMapping(value="/webhook/emailstat")
public class EmailStatController {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailStatController.class);
	private @Autowired IEmailEventsService emailEventsService;
	
	//test code 
	//private @Autowired AdminUserAccountActivationEmail adminUserAccountActivationEmail;
	//private @Autowired NoticeRepository noticeRepository;
	//test code ends
	
	@RequestMapping(
		      headers = {"content-type=application/json"},
		      method = RequestMethod.POST, value = "/sendgrid")
	@ResponseBody
	public String webHookSendgrid(HttpServletRequest request) {
		LOGGER.debug("Received hit from sendgrid : remote host ="+request.getRemoteHost());
		
		String jsonBody = "";
		try {
			 jsonBody = IOUtils.toString( request.getInputStream());
			 LOGGER.debug("Received hit from sendgrid : data ="+jsonBody);
			 this.processEmailStat(jsonBody);
		} catch (Exception e) {
			LOGGER.error("Error in retreiving request", e);
		}
		
		return "success";
	}
	
	@Async
	private void processEmailStat(String jsonBody) {
		Gson gson = new Gson();
		Type type = new TypeToken<ArrayList<Map<String, String>>>() {}.getType();
		List<Map<String, Object>> sendGridRequest = null;
		
		try {
			sendGridRequest =  gson.fromJson(jsonBody , type);
			
			for (Map<String, Object> emailStat : sendGridRequest) {
				if( emailEventsService.validateEmailEvent(emailStat) ) {
					emailEventsService.saveEmailEvent(emailStat);
				}else{
					LOGGER.debug("Validation failed for the sendgrid event: "+emailStat.toString());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in processing request", e);
		}
		
	}

/*	@RequestMapping( method = RequestMethod.GET, value = "/sendTestMail")
	@ResponseBody
	public String sendTestMail(HttpServletRequest request) {
		String emailTpl = "<html><body><p>test email sent from sendgrid.<br/>To Test open : Only read;<br/>" +
			       "To Test Click : <a href='https://www.google.co.in'>Click here</a><br/></p></body></html>";
		Notice noticeObj = noticeRepository.findById(307147);
		noticeObj.setToAddress("ghixhaneetest@yopmail.com");
		noticeObj.setEmailBody(emailTpl);
		
		HashMap<String, String> emailStatData = new HashMap<String, String> ();
		emailStatData.put("TenantID", "1");
		emailStatData.put("ClientID", "5");
		emailStatData.put("LeadID", "105");
		
		adminUserAccountActivationEmail.sendEmail(noticeObj, emailStatData);
		return "success";
	}*/

}
