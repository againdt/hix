<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%
    String caseNumber=  request.getParameter("caseNumber") == null ? "" : request.getParameter("caseNumber");
    String applicationType =  request.getParameter("applicationType") == null ? "" : request.getParameter("applicationType");
    String coverageYear =  request.getParameter("coverageYear") == null ? "" : request.getParameter("coverageYear");
    request.getSession().setAttribute("ssapVisited", "Y");
    String ssapFlowVersion = request.getAttribute("ssapFlowVersion") == null ? "" : request.getAttribute("ssapFlowVersion").toString();
    String ssapURL = "/ssap?caseNumber=" + caseNumber + "&applicationType=" + applicationType + "&coverageYear=" + coverageYear;

    if (StringUtils.isNotBlank(ssapFlowVersion) && ssapFlowVersion.equals("2.0")) {
        ssapURL = "/newssap/start";
        if (!caseNumber.isEmpty()) {
            ssapURL += "?caseNumber=" + caseNumber;
        } else {
            ssapURL += "?applicationType=" + applicationType + "&coverageYear=" + coverageYear;
        }
    }
%>
<c:set var="caseNumber" value="<%=caseNumber%>" />
<c:set var="applicationType" value="<%=applicationType%>" />
<c:set var="coverageYear" value="<%=coverageYear%>" />
<c:set var="ssapURL" value="<%=ssapURL%>" />

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ridp.css"/>" />

<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>

<script src="<c:url value="/resources/angular/jquery.maskedinput.min.js"/>"></script>

<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js"/>"></script>

<script type="text/javascript" src="<c:url value="/resources/js/ridp.js" />"></script>

<style type="text/css">
    legend{
        margin-bottom: 0px !important;
    }
    /*  #rightpanel{
       padding-left: 30px !important;
     } */
</style>

<div ng-app="ridpApp" ng-controller="mainController">

    <script type="text/ng-template" id="verifyidentity.temp.html">
        <div class="header">
            <h4><spring:message code="label.ridp.verifyidentity.header"/></h4>

        </div>
        <div class="gutter10">

            <div class="row-fluid margin20-b">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-10">
                    <p><spring:message code="label.ridp.verifyidentity.beforeContinue"/></p>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                    <a href="javascript:void(0)" ng-click="getStarted()" class="btn btn-primary btn_res pull-right" id="ridp-start"><spring:message code="label.ridp.verifyidentity.start"/></a>
                </div>
            </div>

            <div class="alert alert-info" id="ridp-info">
                <p class="txt-bold"><spring:message code="label.ridp.verifyidentity.whyVerifyQ"/></p>
                <p><spring:message code="label.ridp.verifyidentity.whyVerifyA1"/></p>
                <p class="margin20-t txt-bold"><spring:message code="label.ridp.verifyidentity.howItWorks"/></p>
                <p>
                <ul>
                    <li><spring:message code="label.ridp.verifyidentity.howItWorksli1"/></li>
                    <li><spring:message code="label.ridp.verifyidentity.howItWorksli2"/></li>
                    <li><spring:message code="label.ridp.verifyidentity.howItWorksli3"/></li>
                </ul>
                </p>
            </div>
        </div>
    </script>



    <script type="text/ng-template" id="contactinfo.temp.html">
        <div ng-show="loading" id="loading-spinner"><img alt="Loading" src="<c:url value="/resources/images/loader_gray.gif" />"></div>
        <div ng-show="!loading">
            <div ng-show="errorMessages.errorMsg">
                <div class="alert alert-error">{{errorMessages.errorMsg}}</div>
            </div>

            <div class="header">
                <h4><spring:message code="label.ridp.contactinfo.header"/></h4>
            </div>
            <div class="gutter10">
                <div class="alert alert-info margin10-t">
                    <p><spring:message code="label.ridp.contactinfo.tellMe"/></p>
                    <p><spring:message code="label.ridp.contactinfo.important"/></p>
                </div>

                <div ng-if="userInfo.allowPaperVerification && statecode !='ID'">
                    <a class="btn btn-primary" ng-click="doPaperVerification()"><spring:message code="label.ridp.contactinfo.paper.verification"/></a>
                </div>

                <form novalidate name="contactForm" class="form-horizontal" ng-submit="submitContactForm(contactForm,userInfo)">
                    <div class="control-group">
                        <label for="firstName" class="control-label"><spring:message code="label.ridp.contactinfo.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <input type="text" id="firstName" name="personGivenName" class="xlarge" size="30" ng-model="userInfo.personGivenName" ng-pattern="/^[a-zA-Z ,.'-]{1,50}$/" required display-invalid/>

                            <div id="firstName_error" err-msg="<spring:message code='label.ridp.pattern.firstName'/>" pattern-err-directive label-for="firstName" ng-show="contactForm.personGivenName.$error.pattern"></div>

                            <div id="firstName_error" err-msg="<spring:message code='label.ridp.required'/>" label-for="firstName" required-err-directive ng-show="contactForm.personGivenName.$dirty && contactForm.personGivenName.$error.required">
                            </div>

                        </div>
                    </div>

                    <div class="control-group">
                        <label for="middleName" class="control-label"><spring:message code="label.ridp.contactinfo.middleName"/></label>
                        <div class="controls">
                            <input type="text" id="middleName" name="personMiddleName" class="xlarge" size="30" ng-model="userInfo.personMiddleName" ng-pattern="/^[a-zA-Z ,.'-]{1,50}$/"/>

                            <div id="middleName_error" err-msg="<spring:message code='label.ridp.pattern.middleName'/>" label-for="middleName" pattern-err-directive ng-show="contactForm.personMiddleName.$dirty && contactForm.personMiddleName.$error.pattern"></div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="lastName" class="control-label"><spring:message code="label.ridp.contactinfo.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <input type="text" id="lastName" name="personSurName" class="xlarge" size="30" ng-model="userInfo.personSurName" ng-pattern="/^[a-zA-Z ,.'-]{1,50}$/" ng-maxlength="50" required display-invalid/>

                            <div id="lastName_error" label-for="lastName" err-msg="<spring:message code='label.ridp.pattern.lastName'/>" pattern-err-directive ng-show="contactForm.personSurName.$error.pattern || contactForm.personSurName.$error.maxlength"></div>
                            <div id="lastName_error" label-for="lastName" err-msg="<spring:message code='label.ridp.required'/>" required-err-directive ng-show="contactForm.personSurName.$dirty && contactForm.personSurName.$error.required"></div>



                        </div>
                    </div>

                    <div class="control-group">
                        <label for="suffix" class="control-label"><spring:message code="label.ridp.contactinfo.suffix"/></label>
                        <div class="controls">
                            <select id="suffix" class="input-medium" name="personNameSuffixText" ng-model="userInfo.personNameSuffixText" >
                                <option value=""></option>
                                <option value="SR"><spring:message code="label.ridp.contactinfo.sr"/></option>
                                <option value="JR"><spring:message code="label.ridp.contactinfo.jr"/></option>
                                <option value="II"><spring:message code="label.ridp.contactinfo.ii"/></option>
                                <option value="III"><spring:message code="label.ridp.contactinfo.iii"/></option>
                                <option value="IV"><spring:message code="label.ridp.contactinfo.iv"/></option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="dob" class="control-label"><spring:message code="label.ridp.contactinfo.dob"/></label>
                        <div class="controls">

                            <input class="input-medium" data-inputmask="'alias': 'mm/dd/yyyy'" type="text" id="dob" ng-focus name="personBirthDate" ng-model="userInfo.personBirthDate" dob-err ng-pattern="/^\d{1,2}/\d{1,2}/\d{4}$/">

                            <div id="dob_error" label-for="dob"  dob-err-directive err-msg="<spring:message code='label.ridp.dobErr'/>" ng-show="contactForm.personBirthDate.$dirty && contactForm.personBirthDate.$error.dobErr"></div>
                            <div id="dob_error" pattern-err-directive label-for="dob"  err-msg="<spring:message code='label.ridp.pattern.dob'/>" ng-show="contactForm.personBirthDate.$dirty && contactForm.personBirthDate.$error.pattern && !contactForm.personBirthDate.$focused"></div>


                        </div>
                    </div>

                    <div class="control-group">
                        <label for="ssn" class="control-label"><spring:message code="label.ridp.contactinfo.ssn"/></label>
                        <div class="controls">
                            <input type="text" name="personSSNIdentification" id="ssn" class="input-medium" ng-focus ng-model="userInfo.personSSNIdentification" ng-pattern="/^\d{3}-?\d{2}-?\d{4,}$/"/>
                            <div id="ssn_error" pattern-err-directive label-for="ssn"  err-msg="<spring:message code='label.ridp.pattern.ssn'/>" ng-show="contactForm.personSSNIdentification.$dirty && contactForm.personSSNIdentification.$error.pattern && !contactForm.personSSNIdentification.$focused">
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="address1" class="control-label"><spring:message code="label.ridp.contactinfo.address"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <input type="text" class="input-large" size="30" id="address1" name="address1" maxlength="68" ng-model="userInfo.address1" required ng-pattern="/^[A-Za-z0-9\.\-\s]{1,68}$/"/>

                            <div id="address1_error"  required-err-directive label-for="address1" err-msg="<spring:message code='label.ridp.required'/>" ng-show="contactForm.address1.$dirty && contactForm.address1.$error.required">
                            </div>

                            <div id="address1_error"  label-for="address1" pattern-err-directive err-msg="<spring:message code='label.ridp.pattern.address'/>" ng-show="contactForm.address1.$dirty && contactForm.address1.$error.pattern">
                            </div>
                        </div>
                    </div>


                    <div class="control-group">
                        <label for="city" class="control-label"><spring:message code="label.ridp.contactinfo.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <input type="text" class="input-large" id="city" name="locationCityName" maxlength="38" required ng-model="userInfo.locationCityName" ng-pattern="/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/" ng-maxlength="38"/>

                            <div id="city_error" label-for="city" required-err-directive err-msg="<spring:message code='label.ridp.required'/>" ng-show="contactForm.locationCityName.$dirty && contactForm.locationCityName.$error.required">
                            </div>
                            <div id="city_error" label-for="city" pattern-err-directive err-msg="<spring:message code='label.ridp.pattern.city'/>" ng-show="(contactForm.locationCityName.$dirty && (contactForm.locationCityName.$error.pattern || contactForm.locationCityName.$error.maxlength))"></div>

                        </div>
                    </div>

                    <div class="control-group">
                        <label for="state" class="control-label"><spring:message code="label.ridp.contactinfo.state"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <select id="state" ng-change="stateSelect()" class="input-large" name="locationStateUSPostalServiceCode" required ng-model="userInfo.locationStateUSPostalServiceCode" ng-options="state.value as state.displayName for state in states.state">

                            </select>

                            <div id="state_error" required-err-directive err-msg="<spring:message code='label.ridp.required'/>" label-for="state" ng-show="contactForm.locationStateUSPostalServiceCode.$dirty && contactForm.locationStateUSPostalServiceCode.$error.required">
                            </div>

                        </div>
                    </div>

                    <div class="control-group">
                        <label for="zip" class="control-label"><spring:message code="label.ridp.contactinfo.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                        <div class="controls">
                            <input type="text" class="input-medium" id="zip" name="locationPostalCode" maxlength="5" required ng-model="userInfo.locationPostalCode" ng-focus ng-pattern="/^\d{5}$/"/>

                            <div id="zip_error" required-err-directive label-for="zip" err-msg="<spring:message code='label.ridp.required'/>" ng-show="(contactForm.locationPostalCode.$dirty && contactForm.locationPostalCode.$error.required)">
                            </div>

                            <div id="zip_error" label-for="zip" pattern-err-directive err-msg="<spring:message code='label.ridp.pattern.zipCode'/>" ng-show="contactForm.locationPostalCode.$dirty && contactForm.locationPostalCode.$error.pattern && !contactForm.locationPostalCode.$focused"></div>

                        </div>
                    </div>

                    <div class="control-group">
                        <label for="phone" class="control-label" ><spring:message code="label.ridp.contactinfo.phone"/></label>
                        <div class="controls">
                            <input type="text" class="input-medium" name="fullTelephoneNumber" id="phone" ng-focus ng-model="userInfo.fullTelephoneNumber" ng-pattern="/^\d{3}-?\d{3}-?\d{4,}$/"/>
                            <div id="phone_error" pattern-err-directive label-for="phone"  err-msg="<spring:message code='label.ridp.pattern.phone'/>" ng-show="contactForm.fullTelephoneNumber.$dirty && contactForm.fullTelephoneNumber.$error.pattern && !contactForm.fullTelephoneNumber.$focused"></div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <input type="submit" value="<spring:message code='label.ridp.contactinfo.continue'/>" ng-disabled="contactForm.$invalid" class="btn btn-primary pull-right"/>
                    </div>
                </form>

            </div>
            <div id="paperVerificationModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to mark the applicant as verified based on the paper application?</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" ng-click="confirmPaperVerification()">OK</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
    </script>

    <script type="text/ng-template" id="identityquestions.temp.html">
        <div ng-show="loading" id="loading-spinner"><img alt="Loading" src="<c:url value="/resources/images/loader_gray.gif" />"></div>
        <div ng-show="!loading">
            <div ng-show="errorMessages.errorMsg">
                <div class="alert alert-error">{{errorMessages.errorMsg}}</div>
            </div>

            <div class="header">
                <h4><spring:message code="label.ridp.identityquestions.header"/></h4>
            </div>
            <div class="gutter10">
                <p><spring:message code="label.ridp.identityquestions.answerQuestion"/></p>
                <p class="alert alert-info"><spring:message code="label.ridp.identityquestions.note"/></p>
                <form novalidate method="post" name="questionForm" ng-submit="submitQuestionForm(questionForm)">
                    <div class="control-group" ng-repeat="question in questionsData.verificationResponse.questions" >
                        <fieldset>
                            <legend class="control-label">{{question.questionNumber}}. {{question.question}}</legend>
                            <div class="controls">
                                <label class="radio" for="{{question.questionNumber}}-{{choice.answerChoice.choiceIndex}}-identityquestions" ng-repeat="choice in question.choices">
                                    <input type="radio" id="{{question.questionNumber}}-{{choice.answerChoice.choiceIndex}}-identityquestions" required ng-model="answer[question.questionNumber]" name="{{question.questionNumber}}" value="{{choice.answerChoice.choiceIndex}}">{{choice.answerChoice.choiceText}}
                                </label>
                            </div>
                        </fieldset>
                    </div>
                    <div class="form-actions">
                        <input type="submit" value="<spring:message code='label.ridp.identityquestions.submit'/>" class="btn btn-primary pull-right"/>
                    </div>
                    <form>
            </div>
        </div>
    </script>

    <script type="text/ng-template" id="identitynotverify.temp.html">
        <div ng-show="loading" id="loading-spinner"><img alt="Loading" src="<c:url value="/resources/images/loader_gray.gif" />"></div>
        <div ng-show="!loading">
            <div class="header">
                <h4><spring:message code="label.ridp.identitynotverify.title"/></h4>
            </div>
            <div class="alert alert-error margin20-t">
                <spring:message code="label.ridp.identitynotverify.header"/>
            </div>
            <div class="gutter10">
                <div class="control-group">
                    <div class="controls">
                        <p><spring:message code="label.ridp.identitynotverify.canNotSubmit"/></p>

                        <p><spring:message code="label.ridp.identitynotverify.callThe"/> {{FARSRes.verificationResponse.serviceContactName}} <spring:message code="label.ridp.identitynotverify.helpDesk"/></p>

                        <p><spring:message code="label.ridp.identitynotverify.call"/> {{FARSRes.verificationResponse.serviceContactCallCenterNumber}} <spring:message code="label.ridp.identitynotverify.useTheCode"/></p>

                        <div class="alert alert-info txt-center">
                            <p class="margin10-t" ><spring:message code="label.ridp.identitynotverify.codeIs"/> <span id="ridp-code">{{FARSRes.verificationResponse.dhsReferenceNumber}}</span></p>
                        </div>
                    </div>
                </div>

                <div ng-show="errorMessages.HE200006">
                    <div class="alert alert-error"><spring:message code="label.ridp.identitynotverify.numberExpired"/> <a href="javascript:void(0)" ng-click='clearDHSReferenceNumber()'><spring:message code="label.ridp.identitynotverify.here"/></a> <spring:message code="label.ridp.identitynotverify.restartProcess"/></div>
                </div>

                <div ng-show="errorMessages.HE200045">
                    <div class="alert alert-error">{{FARSRes.verificationResponse.serviceContactName}} <spring:message code="label.ridp.identitynotverify.stillUnresolved"/> <a href="javascript:void(0)" ng-click='clearDHSReferenceNumber()'><spring:message code="label.ridp.identitynotverify.here"/></a>.</div>
                </div>

                <div class="control-group">
                    <div class="controls center">
                        <a class="btn btn-primary" href="javascript:void(0)" ng-click="identityVerify()"><spring:message code="label.ridp.identitynotverify.haveVerified"/></a>
                    </div>
                </div>

                <div ng-show="errorMessages.errorMsg">
                    <div class="alert alert-error" ng-bind-html="errorMessages.errorMsg"></div>
                </div>

                <div ng-if="FARSRes.verificationResponse.allowManualVerification">
                    <spring:message code="label.ridp.goto.manual.verification"/> <a href="javascript:void(0)" ng-click="gotoManualVerification()"><spring:message code="label.ridp.identitynotverify.here"/></a>
                </div>

            </div>
        </div>
    </script>

    <script type="text/ng-template" id="verificationcomplete.temp.html">
        <div class="alert alert-success">
            <p><strong><spring:message code="label.ridp.verificationcomplete.header"/></strong></p>
        </div>

        <div class="gutter10">
            <p><spring:message code="label.ridp.verificationcomplete.fillOutAndSubmit"/></p>
            <a href="<c:url value="${ssapURL}"/>" class="btn btn-primary pull-right"><spring:message code="label.ridp.verificationcomplete.continue"/></a>

        </div>

    </script>

    <script type="text/ng-template" id="uploaddocument.temp.html">
        <div ng-show="loading" id="loading-spinner"><img alt="Loading" src="<c:url value="/resources/images/loader_gray.gif" />"></div>
        <div ng-show="!loading">
            <div ng-show="errorMessages.errorMsg">
                <div class="alert alert-error">{{errorMessages.errorMsg}}</div>
            </div>
            <h4><spring:message code="label.ridp.uploaddocument.header"/></h4>

            <div class="alert alert-error"><h5><spring:message code="label.ridp.uploaddocument.identityNotVerified"/></h5> <spring:message code="label.ridp.uploaddocument.canNotSubmit"/></div>
            <p><spring:message code="label.ridp.uploaddocument.reviewAndSend"/></p>

            <form novalidate class="form-horizontal" name="upLoadForm">
                <div class="control-group gutter10">
                    <label for="documentType" class="control-label"><spring:message code="label.ridp.uploaddocument.documentType"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                    <div class="controls">
                        <select class="span11" id="documentType" required name="doctype" ng-model="document.doctype" ng-options="type for type in documents.type"><option value="" selected><spring:message code="label.ridp.uploaddocument.select"/></option></select>
                        <div id="documentType_error" err-msg="<spring:message code='label.ridp.required'/>" required-err-directive label-for="documentType" ng-show="(upLoadForm.doctype.$dirty && upLoadForm.doctype.$error.required)"></div>
                    </div>

                </div>

                <div class="control-group">
                    <label for="manualVerificationFile" class="control-label"><spring:message code="label.ridp.uploaddocument.upload"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                    <div class="controls gutter10-lr">

                        <div class="btn btn primary fileUpload">
                            <span><spring:message code="label.ridp.uploaddocument.upload"/></span>
                            <input type="file" class="upload" id="manualVerificationFile" file-directive="verificationFile" valid-file ng-model="verificationFile" required name="manualVerificationFile">
                        </div>
                        <label for="uploadFile" class="aria-hidden"><spring:message code="label.ridp.uploaddocument.upload"/></label>
                        <input id="uploadFile" style="line-height: 16px;" type="text" disabled="disabled" placeholder="<spring:message code='label.ridp.uploaddocument.chooseFile'/>">
                        <div id="manualVerificationFile_error" err-msg="<spring:message code='label.ridp.required'/>" required-err-directive label-for="manualVerificationFile" ng-show="(upLoadForm.manualVerificationFile.$dirty && upLoadForm.manualVerificationFile.$error.required)"></div>
                    </div>

                </div>

                <div class="form-actions">
                    <button class="btn btn-primary pull-right" style="margin-top:20px" ng-click="upload()" ng-disabled="upLoadForm.$invalid"><spring:message code="label.ridp.uploaddocument.submit"/></button>
                </div>
            </form>
        </div>
    </script>


    <script type="text/ng-template" id="uploaddocument-process.temp.html">
        <h4><spring:message code="label.ridp.uploaddocument-process.header"/></h4>

        <div class="alert alert-info"><h5><spring:message code="label.ridp.uploaddocument-process.inProcess"/></h5> <spring:message code="label.ridp.uploaddocument-process.isBeingProcessed"/></div>

        <table class="table table-striped margin10-t" role="presentation">
            <thead>
            <tr>
                <th class="span4"><spring:message code="label.ridp.uploaddocument-process.document"/></th>
                <th class="span3"><spring:message code="label.ridp.uploaddocument-process.status"/></th>
                <th class="span5"><spring:message code="label.ridp.uploaddocument-process.notes"/></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="document in documentsUploaded">
                <td>{{document.documentType}} <span class="doc-type">{{document.documentName}}</span></td>
                <td><span class="alert">{{document.status}}</span></td>
                <td>{{document.comments}}</td>
            </tr>
            </tbody>
        </table>

    </script>

    <script type="text/ng-template" id="uploaddocument-reject.temp.html">
        <div ng-if="errorMessages.errorMsg">
            <div class="alert alert-error">{{errorMessages.errorMsg}}</div>
        </div>

        <h4><spring:message code="label.ridp.uploaddocument-reject.header"/></h4>

        <div class="alert alert-error"><h5><spring:message code="label.ridp.uploaddocument-reject.documentReject"/></h5> <spring:message code="label.ridp.uploaddocument-reject.checkInbox"/></div>

        <form novalidate class="form-horizontal" name="upLoadForm">
            <div class="control-group gutter10">
                <label for="documentType" class="control-label"><spring:message code="label.ridp.uploaddocument-reject.documentType"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                <div class="controls">
                    <select class="span11" id="documentType" required name="doctype" ng-model="document.doctype" ng-options="type for type in documents.type"><option value="" selected><spring:message code="label.ridp.uploaddocument-reject.select"/></option></select>
                    <div id="documentType_error" required-err-directive err-msg="<spring:message code='label.ridp.required'/>" label-for="documentType" ng-show="(upLoadForm.doctype.$dirty && upLoadForm.doctype.$error.required)"></div>
                </div>

            </div>

            <div class="control-group">
                <label for="manualVerificationFile" class="control-label"><spring:message code="label.ridp.uploaddocument-reject.upload"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                <div class="controls gutter10-lr">

                    <div class="btn btn primary fileUpload">
                        <span><spring:message code="label.ridp.uploaddocument-reject.upload"/></span>
                        <input type="file" class="upload" id="manualVerificationFile" file-directive="verificationFile" valid-file ng-model="verificationFile" required name="manualVerificationFile">
                    </div>

                    <input id="uploadFile" style="line-height: 16px;" type="text" disabled="disabled" placeholder="<spring:message code='label.ridp.uploaddocument-reject.chooseFile'/>">
                    <div id="manualVerificationFile_error" required-err-directive err-msg="<spring:message code='label.ridp.required'/>" label-for="manualVerificationFile" ng-show="(upLoadForm.manualVerificationFile.$dirty && upLoadForm.manualVerificationFile.$error.required)"></div>
                </div>

            </div>

            <div class="form-actions">
                <button class="btn btn-primary pull-right" style="margin-top:20px" ng-click="upload()" ng-disabled="upLoadForm.$invalid"><spring:message code="label.ridp.uploaddocument-reject.sumit"/></button>
            </div>
        </form>

        <table class="table table-striped margin10-t" role="presentation">
            <thead>
            <tr>
                <th class="span4"><spring:message code="label.ridp.uploaddocument-reject.document"/></th>
                <th class="span3"><spring:message code="label.ridp.uploaddocument-reject.status"/></th>
                <th class="span5"><spring:message code="label.ridp.uploaddocument-reject.notes"/></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="document in documentsUploaded">
                <td>{{document.documentType}} <span class="doc-type">{{document.documentName}}</span></td>
                <td><span class="alert">{{document.status}}</span></td>
                <td>{{document.comments}}</td>
            </tr>
            </tbody>
        </table>

    </script>






    <script type="text/ng-template" id="technicalerror.temp.html">

        <div class="gutter10">
            <p><spring:message code="label.ridp.technicalerror.error"/></p>
            <p class="txt-center">
                <br/>
                <a id="backApplication" class="btn btn-primary" href="<c:url value="${ssapURL}"/>"><spring:message code="label.ridp.technicalerror.tryagain"/></a>
                &nbsp; OR &nbsp;
                <a href="javascript:void(0)" ng-click="gotoManualVerification()" class="btn btn-primary"><spring:message code="label.ridp.technicalerror.go.manual"/></a>

            </p>
            <p><br/><spring:message code="label.ridp.technicalerror.manual.error"/></p>
        </div>
    </script>

    <script type="text/ng-template" id="maximumattempts.temp.html">
        <div class="alert alert-error">
            <p><strong><spring:message code="label.ridp.maximumattempts.header"/></strong></p>
        </div>

        <div class="gutter10">

            <p><spring:message code="label.ridp.maximumattempts.24hour"/></p>


            <a href="<c:url value="${ssapURL}"/>" class="btn btn-primary pull-left"><spring:message code="label.ridp.maximumattempts.resume"/></a>
        </div>
    </script>

    <div class="gutter10">
        <div class="row-fluid margin20-b">
            <a href="<c:url value="${ssapURL}"/>"><i class="icon-circle-arrow-left"></i> <spring:message code="label.ridp.backApplication"/></a>
        </div>
        <div class="row-fluid">
            <div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message code="label.ridp.sidebar.header"/></h4>
                </div>
                <ul class="nav nav-list" id="ridp-nav">
                    <li id="ridp-nav-start"><spring:message code="label.ridp.sidebar.menu1"/></li>
                    <li id="ridp-nav-contact"><spring:message code="label.ridp.sidebar.menu2"/></li>
                    <li id="ridp-nav-question"><spring:message code="label.ridp.sidebar.menu3"/></li>
                    <li id="ridp-nav-experian" class="hide"><spring:message code="label.ridp.sidebar.menu4"/></li>
                    <li id="ridp-nav-manual" class="hide"><spring:message code="label.ridp.sidebar.menu5"/></li>
                    <li id="ridp-nav-finish"><spring:message code="label.ridp.sidebar.menu6"/></li>
                </ul>
            </div>
            <div class="span9 ridp-rightpanel" id="rightpanel" >
                <div ng-view></div>

            </div>
        </div>
    </div>
</div>

<script>
    ridpApp.factory("documentsFactory", function() {
        return {
            type:[
                "<spring:message code='label.ridp.manuUploadDocument1'/>",
                "<spring:message code='label.ridp.manuUploadDocument2'/>",
                "<spring:message code='label.ridp.manuUploadDocument3'/>",
                "<spring:message code='label.ridp.manuUploadDocument4'/>",
                "<spring:message code='label.ridp.manuUploadDocument5'/>",
                "<spring:message code='label.ridp.manuUploadDocument6'/>",
                "<spring:message code='label.ridp.manuUploadDocument7'/>",
                "<spring:message code='label.ridp.manuUploadDocument8'/>",
                "<spring:message code='label.ridp.manuUploadDocument9'/>",
                "<spring:message code='label.ridp.manuUploadDocument10'/>",
                "<spring:message code='label.ridp.manuUploadDocument11'/>"
            ]
        };


    });




    ridpApp.factory("statesFactory", function() {
        return {
            state: [{

                displayName: '<spring:message code="label.ridp.albama"/>',
                value:  'AL'
            },{
                displayName: '<spring:message code="label.ridp.alaska"/>',
                value: 'AK'
            },{
                displayName: '<spring:message code="label.ridp.arizona"/>',
                value: 'AZ'
            },{
                displayName: '<spring:message code="label.ridp.arkansas"/>',
                value: 'AR'
            },{
                displayName: '<spring:message code="label.ridp.california"/>',
                value: 'CA'
            },{
                displayName: '<spring:message code="label.ridp.colorado"/>',
                value: 'CO'
            },{
                displayName: '<spring:message code="label.ridp.connecticut"/>',
                value: 'CT'
            },{
                displayName: '<spring:message code="label.ridp.delaware"/>',
                value: 'DE'
            },{
                displayName: '<spring:message code="label.ridp.districtOfColumbia"/>',
                value: 'DC'
            },{
                displayName: '<spring:message code="label.ridp.florida"/>',
                value: 'FL'
            },{
                displayName: '<spring:message code="label.ridp.georgia"/>',
                value: 'GA'
            },{
                displayName: '<spring:message code="label.ridp.hawaii"/>',
                value: 'HI'
            },{
                displayName: '<spring:message code="label.ridp.idaho"/>',
                value: 'ID'
            },{
                displayName: '<spring:message code="label.ridp.illinois"/>',
                value: 'IL'
            },{
                displayName: '<spring:message code="label.ridp.indiana"/>',
                value: 'IN'
            },{
                displayName: '<spring:message code="label.ridp.iowa"/>',
                value: 'IA'
            },{
                displayName: '<spring:message code="label.ridp.kansas"/>',
                value: 'KS'
            },{
                displayName: '<spring:message code="label.ridp.kentucky"/>',
                value: 'KY'
            },{
                displayName: '<spring:message code="label.ridp.louisiana"/>',
                value: 'LA'
            },{
                displayName: '<spring:message code="label.ridp.maine"/>',
                value: 'ME'
            },{
                displayName: '<spring:message code="label.ridp.maryland"/>',
                value: 'MD'
            },{
                displayName: '<spring:message code="label.ridp.massachusetts"/>',
                value: 'MA'
            },{
                displayName: '<spring:message code="label.ridp.michigan"/>',
                value: 'MI'
            },{
                displayName: '<spring:message code="label.ridp.minnesota"/>',
                value: 'MN'
            },{
                displayName: '<spring:message code="label.ridp.mississippi"/>',
                value: 'MS'
            },{
                displayName: '<spring:message code="label.ridp.missouri"/>',
                value: 'MO'
            },{
                displayName: '<spring:message code="label.ridp.montana"/>',
                value: 'MT'
            },{
                displayName: '<spring:message code="label.ridp.nebraska"/>',
                value: 'NE'
            },{
                displayName: '<spring:message code="label.ridp.nevada"/>',
                value: 'NV'
            },{
                displayName: '<spring:message code="label.ridp.newHampshire"/>',
                value: 'NH'
            },{
                displayName: '<spring:message code="label.ridp.newJersey"/>',
                value: 'NJ'
            },{
                displayName: '<spring:message code="label.ridp.newMexico"/>',
                value: 'NM'
            },{
                displayName: '<spring:message code="label.ridp.newYork"/>',
                value: 'NY'
            },{
                displayName: '<spring:message code="label.ridp.northCarolina"/>',
                value: 'NC'
            },{
                displayName: '<spring:message code="label.ridp.northDakota"/>',
                value: 'ND'
            },{
                displayName: '<spring:message code="label.ridp.ohio"/>',
                value: 'OH'
            },{
                displayName: '<spring:message code="label.ridp.oklahoma"/>',
                value: 'OK'
            },{
                displayName: '<spring:message code="label.ridp.oregon"/>',
                value: 'OR'
            },{
                displayName: '<spring:message code="label.ridp.pennsylvania"/>',
                value: 'PA'
            },{
                displayName: '<spring:message code="label.ridp.rhodeIsland"/>',
                value: 'RI'
            },{
                displayName: '<spring:message code="label.ridp.southCarolina"/>',
                value: 'SC'
            },{
                displayName: '<spring:message code="label.ridp.southDakota"/>',
                value: 'SD'
            },{
                displayName: '<spring:message code="label.ridp.tennessee"/>',
                value: 'TN'
            },{
                displayName: '<spring:message code="label.ridp.texas"/>',
                value: 'TX'
            },{
                displayName: '<spring:message code="label.ridp.utah"/>',
                value: 'UT'
            },{
                displayName: '<spring:message code="label.ridp.vermont"/>',
                value: 'VT'
            },{
                displayName: '<spring:message code="label.ridp.virginia"/>',
                value: 'VA'
            },{
                displayName: '<spring:message code="label.ridp.washington"/>',
                value: 'WA'
            },{
                displayName: '<spring:message code="label.ridp.westVirginia"/>',
                value: 'WV'
            },{
                displayName: '<spring:message code="label.ridp.wisconsin"/>',
                value: 'WI'
            },{

                displayName: '<spring:message code="label.ridp.wyoming"/>',
                value: 'WY'
            }]
        };
    });



</script>
