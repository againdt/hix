package com.getinsured.hix.batch.ssap.service;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.sql.TSTimestamp;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("cleanApplicationVerificationsService")
public class CleanApplicationVerificationsServiceImpl implements CleanApplicationVerificationsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CleanApplicationVerificationsServiceImpl.class);
    private static final String SCHEDULED_DATE_FORMAT = "yyyy-MM-dd";
    private final List<String> openApplicationStatuses = Arrays.asList(
            ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),
            ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode(),
            ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode());

    @Autowired
    SsapApplicationRepository ssapApplicationRepository;

    @Autowired
    SsapApplicantRepository applicantRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public void cleanVerificationsAndUpdateApplicationEligibility(String applicationCreationDate) throws GIException {
        try {
            LOGGER.debug("applicationCreationDate = {}", applicationCreationDate);
            Date creationDate;
            if(StringUtils.isBlank(applicationCreationDate)) {
                TSTimestamp timestamp = new TSTimestamp();
                creationDate = new Date(timestamp.getTime());
            } else {
                creationDate = DateUtil.StringToDate(applicationCreationDate, SCHEDULED_DATE_FORMAT);
                creationDate = setEndOfDay(creationDate);
            }

            LOGGER.debug("getting ssap applications for date {}", creationDate);
            List<Long> ssapApplicationIds = ssapApplicationRepository.findApplicationIdsWithEsignDateWithinRange(
                    subtractOneDay(creationDate), creationDate, openApplicationStatuses);

            if (ssapApplicationIds != null && !ssapApplicationIds.isEmpty()) {
                for (Long applicationId : ssapApplicationIds) {
                    ResponseEntity<Boolean> response = restTemplate.exchange(
                            GhixEndPoints.EligibilityEndPoints.SSAP_APP_CLEANUP_VERIFICATIONS + "/" + applicationId,
                            HttpMethod.GET,
                            null,
                            Boolean.class);

                    Boolean successfulCleanup = false;
                    if (response != null && response.getStatusCode().equals(HttpStatus.OK) && response.getBody() != null) {
                        successfulCleanup = response.getBody();
                    }

                    if (!successfulCleanup) {
                        LOGGER.error("failed to cleanup ssap application with id {}", applicationId);
                    } else if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("successful cleanup of ssap application with id {}", applicationId);
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error("exception cleaning up eligibilities for applicants");
        }
    }

    private Date setEndOfDay(Date date) {
        Calendar calendar = TSCalendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    private Date subtractOneDay(Date date) {
        Calendar calendar = TSCalendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime();
    }
}
