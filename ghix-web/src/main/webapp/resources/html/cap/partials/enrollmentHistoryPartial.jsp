<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<div class="header margin20-b">
	<h4>Enrollment
		<span ng-show="years" class="dropdown coverage-year coverage-year_dropdown pull-right" style="margin-top:-5px;">
			<select class="input-small" ng-model="selectedYear" ng-options="year for year in years" ng-change="changeYears()"></select>
		</span>
		<span ng-show="years" class="pull-right">Coverage Year&nbsp;</span>
	</h4>
</div>
<div ng-if="enrollments.length === 0" class="noEnrollment">
	<p>No enrollment found.</p>
</div>
<div class="gutter10" ng-if="enrollments.length !== 0" style="position: relative">
	<div ng-if="localData.rightPanelLoading" class="loading-img">
		<img alt="loading" src="<c:url value="/resources/images/loader_gray.gif" />">
	</div>
	
	<div ng-if="!localData.rightPanelLoading" ng-repeat="enrollment in enrollments | orderBy: sortCreationTimestamp" ng-form name="enrollment.form">
		<div class="well-step enrollment-history">
	    <div class="row-fluid">
	      <div id="enrollmentimage" class="span5 center" >
	      	<img ng-src='<c:url value="/admin/issuer/company/profile/logo/view/{{enrollment.issuerId}}"/>' alt='{{enrollment.carrierName}} logo'>
	        <p ><small>{{enrollment.carrierName | uppercase}}

					</small></p>
	      </div>
	
	      <div class="span7">
	          <h5 class="center">{{enrollment.planName | uppercase}}</h5>
	          <p ng-if="enrollment.renewalFlag === 'Y'"><span class="enrollment-renewal">Renewal</span> -- Previous Enrollment ID: {{enrollment.priorEnrollmentId}}</p>
	      </div>
	
	    </div>
		
		<div class="row-fluid margin10-t">
	      <table class="table table-condensed span5">
	        <tr>
	          <th>Enrollment Status:</th>
	          <td>{{enrollment.enrollmentStatus}}</td>
	        </tr>
	        <tr ng-if="enrollment.enrollmentConfirmationDate !== undefined">
	          <th>Confirmation Date:</th>
	          <td>{{enrollment.enrollmentConfirmationDate | uiDateFormat}}</td>
	        </tr>
	      </table>
	      <table class="table table-condensed span7">
	        <tr>
	          <th>Effective Date:</th> 
	          <td>
	          	{{enrollment.enrollmentBenefitEffectiveStartDateUI}}
	          	 - 
	          	{{enrollment.enrollmentBenefitEffectiveEndDateUI}}
	          </td>
	        </tr>
	      </table>
		</div>
		
	    <div class="row-fluid">
	      <table class="table table-condensed span5">
	        <caption class="table-caption">Monthly Payment</caption>
	        <tr>
	          <th>Premium Amount</th><td>{{enrollment.grossPremiumAmt | currency}}</td>    
	        </tr> 
	          
			<tr ng-if="enrollment.aptcAmount === undefined">
	          <th>Elected Federal APTC Amount</th><td>$0</td>
	        </tr>
	        
	        <tr ng-if="enrollment.aptcAmount !== undefined">
	          <th>Elected Federal APTC Amount</th><td>{{enrollment.aptcAmount | currency}}</td>
	        </tr>

			<tr ng-if="(stateCode == 'CA') && enrollment.stateSubsidyAmount !== undefined">
				<th>CA Premium Subsidy</th><td>{{enrollment.stateSubsidyAmount | currency}}</td>
			</tr>

	        <tr ng-if="enrollment.csrAmount !== undefined">
	          <th>CSR Amount</th><td>{{enrollment.csrAmount}}</td>        
	        </tr>
			<tr>
			  <th>Net Premium</th><td>{{enrollment.netPremiumAmt | currency}}</td> 
	        </tr>               
	      </table>
	
	      <table class="table table-condensed span7">
	        <caption class="table-caption">Enrollment ID's</caption>
	        <tr><th>Exchange Assigned Policy ID</th><td>{{enrollment.enrollmentId}}</td></tr>
	        <tr><th>CMS Plan ID</th><td>{{enrollment.CMSPlanID}}</td></tr>        
	        <tr><th>Transaction ID</th><td>{{enrollment.transactionId}}</td></tr>           
	      </table>
	    </div>
	
	
	    <table class="table table-condensed dateTable">
	      <tr>
	        <th>Submitted Date:</th> <td>{{enrollment.creationTimestamp | uiDateFormat}}</td>
	        <th>Submitted by:</th> <td>{{enrollment.submittedByUserName}} ({{enrollment.submittedByRoleName}})</td>
	      </tr>     
	      <tr>
	        <th>Last Update Date:</th> <td>{{enrollment.lastUpdateDate | uiDateFormat}}</td>
	        <th>Last Update By:</th> <td>{{enrollment.lastUpdatedUserName}} ({{enrollment.lastUpdatedRoleName}})</td>
	        <!-- <td></td> -->
	      </tr>       
	    </table>

	    <div class="txt-right">
	    	<a ng-show="memberEnrlmntPrmHist" href="javascript:void(0)" ng-click="goToPremiumHistoryPage(enrollment)" class="btn btn-primary btn-small margin10-r">SHOW PREMIUM HISTORY</a>
	    	
	    	<span>
		    	<a href="javascript:void(0)" ng-show="memberEnrlmnt834hist" ng-click="overrideRecentHistory(enrollment)" class="btn btn-primary btn-small margin10-r">RESEND 834 HISTORY</a>
		    	<a href="javascript:void(0)" ng-show="memberEnrlmntLtst834" ng-click="overrideLatestTransaction(enrollment)" class="btn btn-primary btn-small margin10-r">RESEND LATEST 834 TRANSACTION</a>
	    	</span>
	    </div>
	    
	    <div class="enrollmentCsrActionDiv">
		<!-- CSR MENU -->
		<br/>

	    <div data-toggle="collapse" data-target="#enrollee{{$index}}" collapse-icon-plus class="margin10-b margin30-t inline-block">
   	      <a ng-show="memberEnrlmntAddnlinfo" href="javascript:void(0)" ng-click="getAllSubcriberEvents(enrollment)" class="enrollment__link"><i class="icon-plus-sign"></i>Additional Information</a>
	    </div>
	
	
	    <div id="enrollee{{$index}}" class="collapse">
		 <table class="table table-condensed dateTable collapse" stlye="white-space:nowrap;">
	      <tr>
	        <th>Rating Area:</th> <td>{{enrollment.ratingArea}}</td>
	        <th>Rating Area Effective Date:</th> <td ng-if="enrollment.ratingAreaEffectiveDate">{{enrollment.ratingAreaEffectiveDate | uiDateFormat}}</td>
	      </tr>     
	      <tr>
	        <th>Agent Name:</th> <td>{{enrollment.agentName}}</td>
	        <th>Agent TPA Number:</th> <td>{{enrollment.agentTpaNumber}}</td>
	      </tr>
	      <tr>
	        <th ng-if="enrollment.homeAddress1">Home Address:</th> <td ng-if="enrollment.homeAddress1">{{enrollment.homeAddress1 | titlecase}}, <span ng-if="enrollment.homeAddress2">{{enrollment.homeAddress2 | titlecase}},</span> {{enrollment.homeCity | titlecase}}, {{enrollment.homeState | uppercase}}, {{enrollment.homeZipcode}} ({{enrollment.homeCountyCode}})</td>
	        <th ng-if="enrollment.mailingAddress1">Mailing Address:</th> <td ng-if="enrollment.mailingAddress1">{{enrollment.mailingAddress1 | titlecase}}, <span ng-if="enrollment.mailingAddress2">{{enrollment.mailingAddress2 | titlecase}},</span> {{enrollment.mailingCity | titlecase}}, {{enrollment.mailingState | uppercase}}, {{enrollment.mailingZipcode}} <span ng-if="enrollment.mailingCountyCode">({{enrollment.mailingCountyCode}})</span></td>
	      </tr>
	      <tr ng-if="enrollment.ptfLastName">
	      <th>Primary Tax Filer:</th> <td>{{enrollment.ptfFirstName}} {{enrollment.ptfMiddleName}} {{enrollment.ptfLastName}}</td>
	      </tr>
	    </table>
	    	<h5>Enrollees ({{enrollment.selfCount}} primary, {{enrollment.spouseCount}} spouse, {{enrollment.dependentCount}} dependent)</h5>
			<table class="table table-condensed">
			  <thead>
	            <tr>
	              <th>Type</th><th>Name</th><th style="padding-left:0" nowrap>Gender</th><th>DOB</th><th style="padding-left:0">Tobacco</th><th>SSN</th><th>Coverage Dates</th><th>Member ID</th>
	            </tr>
	          </thead>
	
	          <tbody>
	          	<tr ng-if="enrollment.enrolleeDataDtoList.length === 0"><td colspan="6">No enrollee found.</td></tr>
	            <tr ng-if="enrollment.enrolleeDataDtoList.length !== 0" ng-repeat="enrollee in enrollment.enrolleeDataDtoList | orderBy: relationshipOrderBy" ng-init="enrolleeIndex = $index">
	              <td nowrap>{{enrollee.relationshipToSubscriber}} ({{enrollee.relationshipToSubscriberCode}})</td>
	              <td>	
	              	{{enrollee.firstName}} {{enrollee.middleName}} {{enrollee.lastName}}
	              </td>
	              <td nowrap>
	              	{{enrollee.genderLookupLabel}}
	              </td>
	              <td nowrap>
	              	{{enrollee.birthDate | uiDateFormat}}
	              </td>
	              <td>
	              	<span ng-switch="enrollee.tobaccoStatusCode"><span ng-switch-when="T">Yes</span><span ng-switch-when="N">No</span><span ng-switch-when="U">N/A</span></span>
	              </td>
	              <td nowrap>
	              	{{enrollee.taxIdentificationNumber | ssnFormat}}
	              </td>
	              <td nowrap>
	              	{{enrollee.enrolleeEffectiveStartDateUI}}
	              	- 
	              	{{enrollee.enrolleeEffectiveEndDateUI}}
	              </td nowrap>
	              <td>{{enrollee.memberId}}</td>             
	            </tr>
	          </tbody>
	      </table>
	      
	      <div ng-if="enrollmentHistoryLoading" class="loading-img">
	        <img alt="loading" src="<c:url value="/resources/images/loader_gray.gif" />">
	      </div>
	      <div ng-if="!enrollmentHistoryLoading" >
	        <h5>Enrollment History</h5>
	        <table class="table table-condensed">
		        <thead>
		          <tr>
		            <th class="span3">Time</th>
		            <th class="span3">Event</th>
		            <th class="span3">Maintanance Reason</th>
		            <th class="span3">Created By</th>
		          </tr>
		        </thead>
		        
				<tbody>
					<tr>
						<td colspan="4">
							<div style="max-height: 145px;overflow-y: scroll;overflow-x: hidden;">
								<table class="table table-condensed">
									<tr ng-if="enrollment.enrollmentHistoryList === 'null' || enrollment.enrollmentHistoryList.length === 0"><td colspan="3">No enrollment history found.</td></tr>
									<tr ng-if="enrollment.enrollmentHistoryList !== 'null' && enrollment.enrollmentHistoryList.length > 0" ng-repeat="event in enrollment.enrollmentHistoryList | reverse">
					             		<td>{{event.eventCreationDate | date:'MM-dd-yyyy HH:mm:ss'}}</td>
					              		<td ng-if="memberEnrlmntPrmHist" ng-click="getEnrollmentSnapshotData(enrollment, event.subscriberEventId, event.eventCreationDate)"><a data-target="#enrollmentSnapshotDialog" role="button" data-toggle="modal">{{event.enrollmentEvent}}</a></td>
					              		<td ng-if="!memberEnrlmntPrmHist">{{event.enrollmentEvent}}</td>
					              		<td>{{event.enrollmentEventReason}}</td>
					              		<td>{{event.eventCreatedBy}} ({{event.userRole}})</td>
									</tr>
								</table>
							
							</div>
						</td>
						
					</tr>
		        </tbody>
	        </table>
	      </div>
	      		<form name="dialogForm" id="dialogForm" novalidate="novalidate" class="width:100%" >
		
			<div id="enrollmentSnapshotDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="enrollmentSnapshotDialog" aria-hidden="true" style="width: 90%">

				<div ng-if="enrollmentSnapshotData" class="loading-img">
					<img alt="loading" src="<c:url value="/resources/images/loader_gray.gif" />">
				</div>

				<div class="enrollmentSnapshotDialogHeader">
			    	<div class="header">
	    		        <h4 class="margin0 pull-left">Enrollment Snapshot as of {{eventCreationDate | date : "MMMM d, y,hh:mm a"}}</h4>
	            		<button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
	        		</div>
	    		</div>
	    		
	    		<div ng-repeat="enrollment in enrollmentDataDTOList" style="margin-left: 35px">
    				<div class="row">
    					<div class="col-md-6"><b><u>About this Transaction</u></b></div>
    					<div class="col-md-6"><b><u>Enrollment IDs</u></b></div>
    				</div>
    				<div class="row">
    					<div class="col-md-3">Subscriber Event</div>
    					<div class="col-md-3">{{enrollment.eventType ? enrollment.eventType : NA}}</div>
    					<div class="col-md-3">Exchange Policy Id</div>
    					<div class="col-md-3">{{enrollment.enrollmentId ? enrollment.enrollmentId : NA}}</div>
					</div>
    				<div class="row">
    					<div class="col-md-3">Subscriber Maintenance Reason</div>
    					<div class="col-md-3">{{enrollment.eventReason ? enrollment.eventReason : NA}}</div>
    					<div class="col-md-3">CMS Plan Id</div>
    					<div class="col-md-3">{{enrollment.CMSPlanID ? enrollment.CMSPlanID : NA}}</div>
					</div>
    				<div class="row">
    					<div class="col-md-3">Date</div>
    					<div class="col-md-9">{{enrollment.eventCreationDate ? enrollment.eventCreationDate : NA}}</div>
					</div>
    				<div class="row">
    					<div class="col-md-3">Created By</div>
    					<div class="col-md-9">{{enrollment.eventCreatedBy}} ({{enrollment.userRole}})</div>
					</div>
    				<div class="row">
    					<div class="col-md-6"><b><u>Plan Information</u></b></div>
    					<div class="col-md-6"><b><u>Monthly Payment</u></b></div>
					</div>
    				<div class="row">
    					<div class="col-md-3">Carrier</div>
    					<div class="col-md-3">{{enrollment.carrierName ? enrollment.carrierName : NA}}</div>
    					<div class="col-md-3">Gross Premium Amount</div>
    					<div class="col-md-3">{{enrollment.grossPremiumAmt ? enrollment.grossPremiumAmt : NA}}</div>
					</div>
    				<div class="row">
    					<div class="col-md-3">Plan Name</div>
    					<div class="col-md-3">{{enrollment.planName ? enrollment.planName : NA}}</div>
    					<div class="col-md-3">Elected APTC Amount</div>
    					<div class="col-md-3">{{enrollment.aptcAmount ? enrollment.aptcAmount : NA}}</div>
					</div>
    				<div class="row">
    					<div class="col-md-3">Effective Dates</div>
    					<div class="col-md-3">{{enrollment.enrollmentBenefitEffectiveStartDate | uiDateFormat}} - {{enrollment.enrollmentBenefitEffectiveEndDate | uiDateFormat}}</div>
    					<div class="col-md-3">Net Premium</div>
    					<div class="col-md-3">{{enrollment.netPremiumAmt ? enrollment.netPremiumAmt : NA}}</div>
					</div>
    				<div class="row">
    					<div class="col-md-3">Coverage Tier</div>
    					<div class="col-md-3">{{enrollment.planLevel ? enrollment.planLevel : NA}}</div>
						<div ng-if="(stateCode == 'CA') && (enrollment.stateSubsidyAmount)">
							<div class="col-md-3">CA Premium Subsidy</div>
							<div class="col-md-3">{{enrollment.stateSubsidyAmount ? enrollment.stateSubsidyAmount : NA}}</div>
						</div>
					</div>
	    		</div>
	    		<div class="enrollmentSnapshotDialog__household">
	    			<div class="col-md-12"><b><u>About this Household</u></b></div>
	    			<div class="col-md-2"><u>Name</u></div>
   					<div class="col-md-2"><u>Member ID</u></div>
   					<div class="col-md-3"><u>Coverage Dates</u></div>
   					<div class="col-md-2"><u>Member Event Type</u></div>
   					<div class="col-md-3"><u>Member Maintenance Reason</u></div>
	   				<div ng-repeat="enrollee in enrollees">
	  					<div class="col-md-2">{{enrollee.firstName}} {{enrollee.lastName}}</div>
	  					<div class="col-md-2">{{enrollee.memberId}}</div>
	  					<div class="col-md-3">{{enrollee.effectiveStartDate | uiDateFormat}}  - {{enrollee.effectiveEndDate | uiDateFormat}}</div>
	  					<div class="col-md-2">{{enrollee.eventType ? enrollee.eventType : NA}}</div>
	  					<div class="col-md-3">{{enrollee.eventReason ? enrollee.eventReason : NA}}</div>
	   				</div>
	    		</div>
			</div>
		</form>
	      
	    </div>
	   	
	   	<div ng-if="-1 < userPermissionsList.indexOf('MYAPP_ENRLMNT_ACTIONS')">
			<div class="csrActionBorderDiv" style="margin-left:-20px;margin-right:-5px" ng-if="((-1 < userPermissionsList.indexOf('MYAPP_ENRLMNT_OVRRD_ENRL_TO_CONFIRM')) && (enrollment.enrollmentStatus == 'PENDING') && !enrollment.enrollmentConfirmationDate) || ((-1 < userPermissionsList.indexOf('MYAPP_REINST_ENROLLMENT')) && (enrollment.enrollmentStatus == 'CANCEL'  || enrollment.enrollmentStatus == 'TERM'))">
				<div class="csrActionBtnDiv" style="margin-right: 21px;" ng-click="enrollmentCsrAction($event)" data-toggle="tooltip" rel="tooltip" data-placement="top" data-original-title="<strong><spring:message code="indportal.enrollments.overrideFunctions" javaScriptEscape="true"/></strong>" data-html="true" tabindex="-1">
					<div class="gutter5">
						<i class="icon-gear csrActionGear"></i>
						<span><spring:message code="indportal.portalhome.actions" javaScriptEscape="true"/></span>
					</div>
				</div>
			</div>
		</div>
		<!-- CSR MENU -->

		<!-- CSR OPTIONS -->
		<div class="csrButtonDiv" style="display:none">
			<div class="center csrBtns">
				<div ng-if="-1 < userPermissionsList.indexOf('MYAPP_ENRLMNT_OVRRD_ENRL_TO_CONFIRM')">
					<a ng-if ="(enrollment.enrollmentStatus == 'PENDING' || enrollment.enrollmentStatus == 'TERM') && !enrollment.enrollmentConfirmationDate" ng-click="openOverrideEnrollStatus(enrollment.ssapCaseNumber,'overrideEnrollment', enrollment)" id="OverrideEnrollStatus" class="btn"><spring:message code="indportal.enrollments.overrideEnrollmentStatus" javaScriptEscape="true"/></a>
				</div>
				{{myEnrollment.enrollmentStatus}}
				<div ng-if="-1 < userPermissionsList.indexOf('MYAPP_REINST_ENROLLMENT')">
					<a ng-if ="enrollment.enrollmentStatus == 'CANCEL'  || enrollment.enrollmentStatus == 'TERM'" ng-click="openCSR(enrollment, 'reinstate')" id="reinstateenrollment" data-autamation-id="reinstateenrollment" class="btn"><spring:message code="indportal.portalhome.reinstateenrollment" javaScriptEscape="true"/></a>
				</div>
			</div>

		</div>
		<!-- CSR OPTIONS -->
	  </div>    
	</div>
</div>
     <div id="overrideRecentHistoryModal" class="modal hide fade in" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">
            
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelAction(modalForm)">&times;</button>
              <h3 id="myModalLabel">
                  <span>Resend 834 History</span>
              </h3>
            </div>
              <div class="modal-body" ng-show ="showRH">
				<form name="overrideRecentHistoryModalForm" novalidate>
					<p ng-class="{fadeFont: fadeFont}">Please specify the reason for resending the 834 History for this customer.This will help keep track of updates to the customer's record.</p>

					<span class="oRReason">Specify the reason for override. <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></span>
					<textarea class="overrideText" ng-model="modalForm.overrideComment" placeholder="Please explain the change you are making and why" maxlength="4000" required></textarea>
					<div>
						<a class="btn btn-secondary pull-right " data-dismiss="modal" ng-click="cancelAction(modalForm)">CANCEL</a> 
						<a class="btn btn-primary pull-right cntBtn" ng-click="submitOverrideRHComment(modalForm.overrideComment)" ng-disabled="overrideRecentHistoryModalForm.$invalid">CONTINUE</a>
						<span class="pull-left">{{4000 - modalForm.overrideComment.length}} left</span>
					</div>
				</form>
			</div> 

            <div ng-show ="submitingResendRH">
              <div class="modal-body">
                <p><img style="width:40px;display:block;margin:auto;" alt="loading" src="<c:url value="/resources/images/loader_gray.gif" />"></p>
              </div>
            </div>

            <!--successModal-->
          <div ng-show ="successRH">
            <div class="modal-body">
              <h3 >Submission Successful!</h3>
              <p> Press the 'OK" button to go back to Enrollment History.</p>
            </div>
      
             <div class="modal-footer">
                <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelAction()">OK</a>
             </div>
          </div>
            <!--successModal-->
            <!--faliureModal--> 
          <div ng-show ="failureRH">
            <div class="modal-body">
              <h3 >Submission Failure!</h3>
              <p> Press the 'OK" button to go back to Enrollment History.</p>
            </div>
      
             <div class="modal-footer">
                <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelAction()">OK</a>
             </div>
          </div>
          <!--faliureModal-->
        </div>
      </div>
    </div>

<!-- overrideLatestTransaction Modal-->
     <div  id="overrideLatestTransactionModal" class="modal hide fade in" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">
            
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelAction()">&times;</button>
              <h3 id="myModalLabel">
                  <span>Resend latest 834 transaction</span>
              </h3>
            </div>
            <div ng-show="showRT">
              <div class="modal-body">
					<form name="overrideLatestTransactionModalForm" novalidate>
						<p ng-class="{fadeFont: fadeFont}">Please specify the reason for resending the latest 834 transaction for this customer.This will help keep track of updates to the customer's record.</p>
						<span class="oRReason">Specify the reason for override. <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></span> <br>
						<textarea class="overrideText" ng-model="modalForm.overrideComment" placeholder="Please explain the change you are making and why" required maxlength="4000"></textarea>
						<div>
							<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelAction()">CANCEL</a> <a class="btn btn-primary pull-right cntBtn" ng-click="submitOverrideRTComment(modalForm.overrideComment)"
								ng-disabled="modalForm.overrideComment.length < 1">CONTINUE</a>
							<span class="pull-left">{{4000 -
								modalForm.overrideComment.length}} left</span>
						</div>
					</form>
				</div>
            </div>

            <div ng-show ="submitingResendRT">
              <div class="modal-body">
                <p><img style="width:40px;display:block;margin:auto;" alt="loading" src="<c:url value="/resources/images/loader_gray.gif" />"></p>
              </div>
            </div>
            <!--successModal-->
          <div ng-show ="successRT">
            <div class="modal-body">
              <h3 >Submission Successful!</h3>
              <p> Press the 'OK" button to go back to Enrollment History.</p>
            </div>
      
             <div class="modal-footer">
                <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelAction()">OK</a>
             </div>
          </div>
            <!--successModal-->
           <!--faliureModal--> 
          <div ng-show ="failureRT">
            <div class="modal-body">
              <h3 >Submission Failure!</h3>
              <p> Press the 'OK" button to go back to Enrollment History.</p>
            </div>
      
             <div class="modal-footer">
                <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelAction()">OK</a>
             </div>
          </div>
          <!--faliureModal-->
        </div>
      </div>
    </div>
<!-- overrideLatestTransaction Modal-->  
<!-- CSR override enrollment Modal-->
<div ng-show="modalForm.csr" ng-cloak>
	<div modal-show="modalForm.csr" class="modal fade" data-keyboard="false" data-backdrop="static" id="overrideEnrollmentModal">
		<div class="modal-dialog">
			<div class="modal-content">
            	<div class="modal-header">
              		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelCsrAction()">&times;</button>
              		<h3 id="myModalLabel">
						<span>{{currentCsr.header}}</span>
			  		</h3>
            	</div>
			 <div ng-show ="csrInputOverride || csrInputTermDate || SPEopened || ReinstateenrollmentData.showReinstateEnrollmentOptions || changeCovStart || activefailuremsg">
				<div class="modal-body">
					<p ng-class="{fadeFont: fadeFont}">{{currentCsr.content}}</p>
        		</div>

        		<div class="modal-body" >
					<span class="oRReason"><spring:message code="indportal.portalhome.viewoverridehistorycontent" javaScriptEscape="true"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
            		<br>
            			<textarea class="overrideText" ng-model="modalForm.overrideComment" placeholder="Please explain the change you are making and why"></textarea>
            		<br>
            		<div>
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()" ng-disabled = "csrInputTermDate"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
                		<a class="btn btn-primary pull-right" ng-click="modalForm.overrideComment.length > 0 && reinstateEnrollment(modalForm.overrideComment)" class="" ng-disabled = "csrInputTermDate || modalForm.overrideComment.length < 1"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
                		<span class="pull-left">{{maxLength - modalForm.overrideComment.length}} <spring:message code="indportal.portalhome.charactersleft" javaScriptEscape="true"/></span>
             		</div>
				</div>
    		</div>

				<!-- first modal body-->
				<div ng-show="modalForm.overrideEnrollmentDetails">
            		<div class="modal-body">
						<p ng-class="{fadeFont: fadeFont}">{{currentCsr.content}}</p>

						<div class="margin10-t">
							<span class="oRReason"><spring:message code="indportal.portalhome.viewoverridehistorycontent" javaScriptEscape="true"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
							<span class="pull-right">{{maxLength - modalForm.overrideComment.length}} <spring:message code="indportal.portalhome.charactersleft" javaScriptEscape="true"/></span>
						</div>
                		<textarea class="overrideText" ng-model="modalForm.overrideComment" placeholder="<spring:message code="indportal.enrollments.explainWhy" javaScriptEscape="true"/>"></textarea>

						<form class="form-horizontal margin10-t">

							<p class="alert alert-info">
								<spring:message code="indportal.enrollments.note" javaScriptEscape="true"/>
							</p>

							<div class="control-group">
								<label class="control-label" for="issuerAssignedPolicy"><spring:message code="indportal.enrollments.issuerPolicyID" javaScriptEscape="true"/></label>
								<div class="controls">
									<input type="text" id="issuerAssignedPolicy" class="input-medium" ng-model="overrideEnrollmentData.issuerAssignedPolicyId" ng-trim="true">
								</div>
							</div>

							<table class="table table-condensed csrOverrideMemberTable">
								<caption class="table-caption"><spring:message code="indportal.enrollments.coveredMembers" javaScriptEscape="true"/></caption>
								<thead>
									<tr>
										<th>Member</th><th style="width: 80px"><spring:message code="indportal.enrollments.subscriber" javaScriptEscape="true"/></th><th><spring:message code="indportal.enrollments.issuerAssignedMemberID" javaScriptEscape="true"/></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="familymember in overrideEnrollmentData.familyMembers">
										<td>{{familymember.name}}
											
										</td>
										<td class="text-center">
											<i class="icon-ok" ng-if="familymember.relationship=='Self'">
										</td>
										<td>
											<input type="text" ng-model="familymember.issuerAssignedMemberId" class="input-medium" ng-trim="true">
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>

					<div class="modal-footer">
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">
							<spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/>
						</a>
						<button class="btn btn-primary pull-right" ng-click="getOverrideEnrollmentResult(modalForm.overrideComment,overrideEnrollmentData)" ng-disabled = "LastPremiumPaidDateErr || modalForm.overrideComment === undefined || modalForm.overrideComment.length < 1">
							<spring:message code="indportal.enrollments.overrideStatusConfirmed" javaScriptEscape="true"/>
						</button>

                	</div>
				</div>
				<!-- end first modal body-->

				<!-- second modal body-->
				<div ng-show="modalForm.overrideEnrollmentSuccessful">
            		<div class="modal-body">
						<h3><spring:message code="indportal.portalhome.csrsubmissionsucceed" javaScriptEscape="true"/></h3>
			    		<p> <spring:message code="indportal.portalhome.csroktogoback" javaScriptEscape="true"/></p>
                	</div>

					<div class="modal-footer">
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="refresh()">
							<spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/>
						</a>
                	</div>
				</div>
				<!-- end third modal body-->

				<!-- fail modal body-->
				<div ng-show="modalForm.subResult === 'failure'">
					<div class="modal-body" ng-if="reinstated != 'REINSTATED_ERROR' && reinstated != 'SUBSEQUENT_APP_ERROR'">
						<h3><spring:message code="indportal.portalhome.csrsubmissionfailtitle" javaScriptEscape="true"/></h3>
						<p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
						<p><spring:message code="indportal.portalhome.csrsubmissionfailcontent2" javaScriptEscape="true"/></p>					
					</div>
					<div class="modal-body" ng-if="reinstated == 'REINSTATED_ERROR'">
						<h3><spring:message code="indportal.portalhome.csrsubmissionfailtitle" javaScriptEscape="true"/></h3>
						<p><spring:message code="indportal.portalhome.reinstatederror" javaScriptEscape="true"/></p>
					</div>
					<div class="modal-body" ng-if="reinstated == 'SUBSEQUENT_APP_ERROR'">
						<h3><spring:message code="indportal.portalhome.csrsubmissionfailtitle" javaScriptEscape="true"/></h3>
						<p><spring:message code="indportal.portalhome.reinstatederror2" javaScriptEscape="true"/></p>
					</div>
					<div class="modal-footer">
						<a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">
							<spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/>
						</a>
                	</div>
				</div>
				<!--end fail modal body-->
            </div>
		</div>
	</div>
</div>
<!-- CSR Modal-->
