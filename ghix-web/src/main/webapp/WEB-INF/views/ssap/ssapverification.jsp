<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>

<head>



<link href="resources/css/ssap/tooltip.css" rel="stylesheet" type="text/css">
<link href="resources/css/ssap/opentip.css" rel="stylesheet" type="text/css" />
<link href="resources/css/ssap/reDeveloped.css" rel="stylesheet" type="text/css">
<link href="resources/css/bootstrap_NM.css" rel="stylesheet" type="text/css">

<script language="javascript">
function goToSsap() {
	document.location.href="ssap?source=verification";
}






</script>
</head>
<body>

<html>	

	<c:forEach items="${applicants}" var="applicant">
	
	<div class="form-horizontal gutter10"><h4>Verification Results for: ${applicant.firstName}</h4></div>
	<table class="table chk-plan">
	<tr>
		<td class="col1">MEC Verification</td>
		<td class="col2">${applicant.mecVerificationStatus}</td>
	</tr>
	
	<tr>
		<td class="col1">VLP Verification</td>
		<td class="col2">${applicant.vlpVerificationStatus}</td>
	</tr>
	
	<tr>
		<td class="col1">SSN Verification</td>
		<td class="col2">${applicant.ssnVerificationStatus}</td>
	</tr>
	
	<tr>
		<td class="col1">Income Verification</td>
		<td class="col2">${applicant.incomeVerificationStatus}</td>
	</tr>
	
	<tr>
		<td class="col1">Death Verification</td>
		<td class="col2">${applicant.deathStatus}</td>
	</tr>
	
	<tr>
		<td class="col1">Residency Verification</td>
		<td class="col2">${applicant.residencyStatus}</td>
	</tr>
	
	</table>
	
	</c:forEach>
	
	<div class="form-actions noMargin clearfix">
     		
     		
		    <div class="Continue_button" id="countinue_button_div">
		    	<a id="contBtn"  onClick="goToSsap();" class="btn btn-primary pull-right"><spring:message code="ssap.main.continue" text="Continue"/></a>
		    </div>
            
            
           
	</div>
</html>
</body>