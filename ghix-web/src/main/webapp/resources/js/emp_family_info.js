function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
}
jQuery.validator.addMethod("ssncheck", function(value, element, param) {
	var elementName = element.name;
  	if( elementName.indexOf('applicant') >= 0){ ssn1 = $("#applicant_ssn1").val(); ssn2 = $("#applicant_ssn2").val(); ssn3 = $("#applicant_ssn3").val(); }
  	if( elementName.indexOf('spouse') >= 0){ ssn1 = $("#spouse_ssn1").val(); ssn2 = $("#spouse_ssn2").val(); ssn3 = $("#spouse_ssn3").val(); }
  	if( elementName.indexOf('child') >= 0){
  		var childName = elementName.substr(0,7);
  		ssn1 = $("#"+childName+"_ssn1").val(); ssn2 = $("#"+childName+"_ssn2").val(); ssn3 = $("#"+childName+"_ssn3").val();
  	}
  	if( (ssn1 == "" || ssn2 == "" || ssn3 == "")  || (isNaN(ssn1)) || (isNaN(ssn2)) || (isNaN(ssn3))  ){ return false; }
  	return true;
});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
	var elementName = element.name;
  	if( elementName.indexOf('applicant') >= 0){ dob_mm = $("#applicant_dob_mm").val(); dob_dd = $("#applicant_dob_dd").val(); dob_yy = $("#applicant_dob_yy").val(); }
  	if( elementName.indexOf('spouse') >= 0){ dob_mm = $("#spouse_dob_mm").val(); dob_dd = $("#spouse_dob_dd").val(); dob_yy = $("#spouse_dob_yy").val(); }
  	if( elementName.indexOf('child') >= 0){
  		var childName = elementName.substr(0,7);
  		dob_mm = $("#"+childName+"_dob_mm").val(); dob_dd = $("#"+childName+"_dob_dd").val(); dob_yy = $("#"+childName+"_dob_yy").val();
  	}
  	if( (dob_mm == "" || dob_dd == "" || dob_yy == "")  || (isNaN(dob_mm)) || (isNaN(dob_dd)) || (isNaN(dob_yy))  ){ return false; }
  		
	var today=new Date()
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
	if(today.getTime() < birthDate.getTime()){ return false; }
	return true;
});

var family_info = {
	line_index:    0,
	child_numb:    0,
	applicant_row:
'<fieldset id="family_info_FID">\n'+
'<Legend id="family_legend_FID" class="open togglelink">Employee: John Smith</legend>\n'+
'<div class="profile">\n'+
	'<div class="clearfix">\n'+
		'<input type="hidden" id="relation_FID" name="relation[]" value="applicant">\n' +
		'<label for="xlInput3">First Name</label>\n'+  
		'<div class="input">\n'+
			'<input type="text" size="30" name="applicant_first_name" id="applicant_first_name" class="xlarge">\n'+
		'</div>\n'+
		'<div id="applicant_first_name_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label for="xlInput3" >Last Name</label>\n'+
		'<div class="input">\n'+
		       '<input type="text" size="30"name="applicant_last_name" id="applicant_last_name" class="xlarge">\n'+
		'</div>\n'+
		'<div id="applicant_last_name_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
	'<label>Date of Birth</label>\n'+
		'<div class="input">\n'+
			'<div class="inline-inputs">\n'+
				'<input type="text" placeholder="MM" onkeyup="shiftbox(this,\'applicant_dob_dd\')" name="applicant_dob_mm" id="applicant_dob_mm" maxlength="2" class="micro">\n'+
				'<input type="text" placeholder="DD" onkeyup="shiftbox(this,\'applicant_dob_yy\')" name="applicant_dob_dd" id="applicant_dob_dd" maxlength="2" class="micro">\n'+
				'<input type="text" placeholder="YYYY" name="applicant_dob_yy" id="applicant_dob_yy" maxlength="4" size="4">\n'+
			'</div>\n'+
		'</div>\n'+
		'<div id="applicant_dob_yy_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label for="normalSelect">Gender</label>\n'+
		'<div class="input">\n'+
			'<select name="applicant_gender" id="applicant_gender">\n'+
				'<option>Male</option>\n'+
				'<option>Female</option>\n'+
			'</select>\n'+
		'</div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label>Social Security Number</label>\n'+
		'<div class="input">\n'+
			'<div class="inline-inputs">\n'+
				'<input type="text" onkeyup="shiftbox(this,\'applicant_ssn2\')" name="applicant_ssn1" id="applicant_ssn1" maxlength="3" class="micro">\n'+
				'<input type="text" onkeyup="shiftbox(this,\'applicant_ssn3\')" name="applicant_ssn2" id="applicant_ssn2" maxlength="2" class="micro">\n'+
				'<input type="text" name="applicant_ssn3" id="applicant_ssn3" maxlength="4" class="micro">\n'+
			'</div>\n'+
		'</div>\n'+
		'<div id="applicant_ssn3_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
	'<label for="xlInput3" >Email Id</label>\n'+
		'<div class="input">\n'+
			'<input type="text" size="30" name="email" id="email" class="xlarge">\n'+
		'</div>\n'+
		'<div id="email_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
	'<label for="xlInput3" >Phone</label>\n'+
		'<div class="input">\n'+
			'<input type="text" maxlength="10" size="30" name="phone" id="phone" class="xlarge">\n'+
		'</div>\n'+
		'<div id="phone_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
	'<label for="xlInput3" >Address</label>\n'+
		'<div class="input">\n'+
			'<input type="text" size="30" name="address" id="address" class="xlarge">\n'+
		'</div>\n'+	
		'<div id="address_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
	'<label for="xlInput3" >Yearly Income</label>\n'+
		'<div class="input">\n'+
			'<input type="text" size="30" name="income" id="income" class="xlarge">\n'+
		'</div>\n'+
		'<div id="income_error" class=""></div>\n'+
	'</div>\n'+
'</fieldset>\n',

spouse_row:
'<fieldset id="family_info_FID">\n'+
'<legend id="family_legend_FID" class="open togglelink">Your Spouse</legend>\n'+
'<div class="profile" style="display:block">\n'+
	'<div class="clearfix">\n'+
		'<a  class="right btn small danger" href="javascript:family_info.delete_line(\'FID\', \'spouse\' )" id="remove">Remove Spouse</a>\n' +
		'<input type="hidden" id="relation_FID" name="relation[]" value="spouse">\n' +
		'<label for="xlInput3">First Name</label>\n'+  
		'<div class="input">\n'+
			'<input type="text" size="30" name="spouse_first_name" id="spouse_first_name" class="xlarge">\n'+
		'</div>\n'+
		 '<div id="spouse_first_name_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label for="xlInput3" >Last Name</label>\n'+
		'<div class="input">\n'+
			'<input type="text" size="30" name="spouse_last_name" id="spouse_last_name" class="xlarge">\n'+
		'</div>\n'+
		'<div id="spouse_last_name_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label>Date of Birth</label>\n'+
		'<div class="input">\n'+
			'<div class="inline-inputs">\n'+
				'<input type="text" placeholder="MM" onkeyup="shiftbox(this,\'spouse_dob_dd\')" name="spouse_dob_mm" id="spouse_dob_mm" maxlength="2" class="micro">\n'+
				'<input type="text" placeholder="DD" onkeyup="shiftbox(this,\'spouse_dob_yy\')" name="spouse_dob_dd" id="spouse_dob_dd" maxlength="2" class="micro">\n'+
				'<input type="text" placeholder="YYYY" name="spouse_dob_yy" id="spouse_dob_yy" maxlength="4" size="4">\n'+
			'</div>\n'+
		'</div>\n'+
		'<div id="spouse_dob_yy_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label for="normalSelect">Gender</label>\n'+
		'<div class="input">\n'+
			'<select id="spouse_gender" name="spouse_gender">\n'+
				'<option>Male</option>\n'+
				'<option>Female</option>\n'+
			'</select>\n'+
		'</div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label>Social Security Number</label>\n'+
			'<div class="input">\n'+
				'<div class="inline-inputs">\n'+
					'<input type="text" onkeyup="shiftbox(this,\'spouse_ssn2\')" name="spouse_ssn1" id="spouse_ssn1" " maxlength="3" class="micro">\n'+
					'<input type="text" onkeyup="shiftbox(this,\'spouse_ssn3\')" name="spouse_ssn2" id="spouse_ssn2"  maxlength="2" class="micro">\n'+
					'<input type="text" name="spouse_ssn3" id="spouse_ssn3" maxlength="4" class="micro">\n'+
				'</div>\n'+
			'</div>\n'+
			'<div id="spouse_ssn3_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label id="optionsCheckboxes">Please check all those that are applicable to you:</label>\n'+
		'<div class="input">\n'+
		'<ul class="inputs-list">\n'+
			'<li>\n'+
				'<label>\n'+
					'<input type="checkbox" value="yes" name="spouse_smoker"  id="spouse_smoker">\n'+
					'<span>Smoker</span>\n'+
				'</label>\n'+
			'</li>\n'+
			'<li>\n'+
				'<label>\n'+
					'<input type="checkbox" value="yes"  name="spouse_pregnant"  id="spouse_pregnant">\n'+
					'<span>Pregnant</span>\n'+
				 '</label>\n'+
			'</li>\n'+
			'<li>\n'+
				'<label>\n'+
					'<input type="checkbox" value="yes"  name="spouse_medicalconditions"  id="spouse_medicalconditions">\n'+
					'<span>Emergency Medical Condition</span>\n'+
				'</label>\n'+
			'</li>\n'+
			'<li>\n'+
				'<label>\n'+
					'<input type="checkbox"  value="yes"  name="spouse_nativeamerican"  id="spouse_nativeamerican">\n'+
					'<span>Native American Indian</span>\n'+
				'</label>\n'+
				'<label>\n'+
					'<input type="checkbox" value="yes"  name="spouse_disabled"  id="spouse_disabled">\n'+
					'<span>Disabled or Blind</span>\n'+
				'</label>\n'+
			'</li>\n'+
		'</ul>\n'+
		'</div>\n'+
	'</div>\n'+
'</div>\n'+
'</fieldset>\n',

children_row:
'<fieldset id="family_info_FID">\n'+
'<legend id="family_legend_FID" class="open togglelink">Child</legend>\n'+
'<div class="profile" style="display:block">\n'+
	'<div class="clearfix">\n'+
		'<input type="hidden" id="relation_FID" name="relation[]" value="child_CN">\n' +
		'<a  class="right btn small danger" href="javascript:family_info.delete_line(\'FID\', \'child\' )" id="remove">Remove Child</a>\n' +
		'<label for="xlInput3">First Name</label>\n'+  
		'<div class="input">\n'+
			'<input type="text" size="30" name="child_CN_first_name" id="child_CN_first_name" class="xlarge">\n'+
		'</div>\n'+
		'<div id="child_CN_first_name_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label for="xlInput3" >Last Name</label>\n'+
		'<div class="input">\n'+
			'<input type="text" size="30" name="child_CN_last_name" id="child_CN_last_name" class="xlarge">\n'+
		'</div>\n'+
		'<div id="child_CN_last_name_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label>Date of Birth</label>\n'+
		'<div class="input">\n'+
			'<div class="inline-inputs">\n'+
				'<input type="text" placeholder="MM" onkeyup="shiftbox(this,\'child_CN_dob_dd\')" name="child_CN_dob_mm" id="child_CN_dob_mm" maxlength="2" class="micro">\n'+
				'<input type="text" placeholder="DD" onkeyup="shiftbox(this,\'child_CN_dob_yy\')" name="child_CN_dob_dd" id="child_CN_dob_dd" maxlength="2" class="micro">\n'+
				'<input type="text" placeholder="YYYY" name="child_CN_dob_yy" id="child_CN_dob_yy" maxlength="4" class="micro">\n'+
			'</div>\n'+
		'</div>\n'+
		'<div id="child_CN_dob_yy_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label for="normalSelect">Gender</label>\n'+
		'<div class="input">\n'+
			'<select id="child_CN_gender" name="child_CN_gender">\n'+
				'<option>Male</option>\n'+
				'<option>Female</option>\n'+
			'</select>\n'+
		'</div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label>Social Security Number</label>\n'+
			'<div class="input">\n'+
				'<div class="inline-inputs">\n'+
					'<input type="text" onkeyup="shiftbox(this,\'child_CN_ssn2\')" name="child_CN_ssn1" id="child_CN_ssn1" " maxlength="3" class="micro">\n'+
					'<input type="text" onkeyup="shiftbox(this,\'child_CN_ssn3\')" name="child_CN_ssn2" id="child_CN_ssn2"  maxlength="2" class="micro">\n'+
					'<input type="text" name="child_CN_ssn3" id="child_CN_ssn3" maxlength="4" size="4">\n'+
				'</div>\n'+
			'</div>\n'+
			'<div id="child_CN_ssn3_error" class=""></div>\n'+
	'</div>\n'+
	'<div class="clearfix">\n'+
		'<label id="optionsCheckboxes">Please check all those that are applicable to you:</label>\n'+
		'<div class="input">\n'+
		'<ul class="inputs-list">\n'+
			'<li>\n'+
				'<label>\n'+
					'<input type="checkbox" value="1" name="child_CN_smoker"  id="child_CN_smoker">\n'+
					'<span>Smoker</span>\n'+
				'</label>\n'+
			'</li>\n'+
			'<li>\n'+
				'<label>\n'+
					'<input type="checkbox" value="1"  name="child_CN_pregnant"  id="child_CN_pregnant">\n'+
					'<span>Pregnant</span>\n'+
				 '</label>\n'+
			'</li>\n'+
			'<li>\n'+
				'<label>\n'+
					'<input type="checkbox" value="1"  name="child_CN_medicalconditions"  id="child_CN_medicalconditions">\n'+
					'<span>Emergency Medical Condition</span>\n'+
				'</label>\n'+
			'</li>\n'+
			'<li>\n'+
				'<label>\n'+
					'<input type="checkbox"  value="1"  name="child_CN_nativeamerican"  id="child_CN_nativeamerican">\n'+
					'<span>Native American Indian</span>\n'+
				'</label>\n'+
				'<label>\n'+
					'<input type="checkbox" value="1"  name="child_CN_disabled"  id="child_CN_disabled">\n'+
					'<span>Disabled or Blind</span>\n'+
				'</label>\n'+
			'</li>\n'+
		'</ul>\n'+
		'</div>\n'+
	'</div>\n'+
'</div>\n'+
'</fieldset>\n',
	
	add_applicant: function(){
		this.line_index++
		var row = this.applicant_row 
		row = row.replace(/FID/g, this.line_index);
		$('#family_info_data').append(row);
		$("#family_legend_"+this.line_index).click(function () {
	        $(this).next().slideToggle("slow");
	    });
		
		jQuery("#applicant_first_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter your first name.</span>" } } );
		jQuery("#applicant_last_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter your last name.</span>" } } );
		jQuery("#email").rules("add", { required: true, email: true,
										messages: {
											required: "<span> <em class='excl'>!</em> Please enter your email address.</span>",
											email: "<span> <em class='excl'>!</em> Please enter a valid email address.</span>" 
										} 
		});
		jQuery("#phone").rules("add", { required: true, number:true,
										messages: {
											required: "<span> <em class='excl'>!</em> Please enter your phone number.</span>",
											number: "<span> <em class='excl'>!</em> Please enter numeric value in phone number.</span>"
										} 
		});
		jQuery("#address").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter your address.</span>" } } );
		jQuery("#applicant_dob_yy").rules("add", { dobcheck: true, messages: { dobcheck: "<span> <em class='excl'>!</em> Please enter valid date of birth.</span>" } });
		jQuery("#applicant_ssn3").rules("add", { ssncheck: true, messages: { ssncheck: "<span> <em class='excl'>!</em> Please enter valid SSN number.</span>" }  });
		
		addmask('applicant','');

	},
	
	add_spouse: function() {
		this.line_index++
		var row = this.spouse_row 
		row = row.replace(/FID/g, this.line_index);
		$('#family_info_data').append(row);
		$("#family_legend_"+this.line_index).click(function () {
	        $(this).next().slideToggle("slow");
	    });
		if($(('spouse_cell_id'))){
			document.getElementById('spouse_cell_id').innerHTML = ""
		}
		jQuery("#spouse_first_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your spouse's first name.</span>" } } );
		jQuery("#spouse_last_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your spouse's last name.</span>" } } );
		jQuery("#spouse_dob_yy").rules("add", { dobcheck: true, messages: { dobcheck: "<span> <em class='excl'>!</em> Please enter valid date of birth.</span>" }  });
		jQuery("#spouse_ssn3").rules("add", { ssncheck: true, messages: { ssncheck: "<span> <em class='excl'>!</em> Please enter valid SSN number.</span>" }  });
		
		addmask('spouse','');
	},

		   				   
	add_children: function() {
		this.line_index++;
		this.child_numb++;
		var row = this.children_row 
		var childCN = this.child_numb;
		row = row.replace(/FID/g, this.line_index);
		row = row.replace(/CN/g, this.child_numb)
		$('#family_info_data').append(row);
		$("#family_legend_"+this.line_index).click(function () {
	        $(this).next().slideToggle("slow");
	    });
		jQuery("#child_"+childCN+"_first_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your child's first name.</span>" } } );
		jQuery("#child_"+childCN+"_last_name").rules("add", { required: true, messages: {required: "<span> <em class='excl'>!</em> Please enter Your child's last name.</span>" } } );
		jQuery("#child_"+childCN+"_dob_yy").rules("add", { dobcheck: true, messages: { dobcheck: "<span> <em class='excl'>!</em> Please enter valid date of birth.</span>" } });
		jQuery("#child_"+childCN+"_ssn3").rules("add", { ssncheck: true, messages: { ssncheck: "<span> <em class='excl'>!</em> Please enter valid SSN number.</span>" }  });
		
		addmask('child',childCN);
	},

	delete_line: function(index, member_type){
		if(confirm('Are you sure you want to delete this record ?')){
			$('#family_info_'+index).remove();
			$('#relation_'+index).remove();
			if(member_type == 'spouse'){
				document.getElementById('spouse_cell_id').innerHTML = '<input type="button" class="btn small primary" onclick="javascript:family_info.add_spouse();" id="add_spouse" name="add_spouse" value="Add Spouse">'
			}
		}
	}	 
}

function addmask(rel, childNum) {
	if((rel == 'applicant' ||  rel == 'spouse') && childNum == ''){ 
		$('#'+rel+'_dob_mm').mask('99',{placeholder:""});
		$('#'+rel+'_dob_dd').mask('99',{placeholder:""});
		$('#'+rel+'_dob_yy').mask('9999',{placeholder:""});
		$('#'+rel+'_ssn1').mask('999',{placeholder:""});
		$('#'+rel+'_ssn2').mask('99',{placeholder:""});
		$('#'+rel+'_ssn3').mask('9999',{placeholder:""});
	}else{
		for(var i=1;i<= childNum;i++){
			var childName = "child_"+ i;
			$('#'+childName+'_dob_mm').mask('99',{placeholder:""});
			$('#'+childName+'_dob_dd').mask('99',{placeholder:""});
			$('#'+childName+'_dob_yy').mask('9999',{placeholder:""});
			$('#'+childName+'_ssn1').mask('999',{placeholder:""});
			$('#'+childName+'_ssn2').mask('99',{placeholder:""});
			$('#'+childName+'_ssn3').mask('9999',{placeholder:""});
		}
	}
}
