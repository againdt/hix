package com.getinsured.hix.externalassister.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.externalassister.DesignateAssisterRequest;
import com.getinsured.hix.dto.externalassister.DesignateAssisterResponse;
import org.apache.log4j.Logger;

import com.getinsured.hix.externalassister.repository.IExternalAssisterDesignateRepository;
import com.getinsured.hix.externalassister.repository.IExternalAssisterRepository;
import com.getinsured.hix.externalassister.util.ExternalAssisterRestUtil;
import com.getinsured.hix.externalassister.util.ExternalBrokerConstants;
import com.getinsured.hix.externalassister.util.ExternalBrokerUtils;
import com.getinsured.hix.model.ExternalAssister;
import com.getinsured.hix.model.ExternalAssisterDesignate;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;

import static com.getinsured.hix.externalassister.util.ExternalBrokerUtils.isNotNullAndEmpty;


@Service
public class ExternalAssisterServiceImpl implements ExternalAssisterService{
	
	private IExternalAssisterDesignateRepository iExternalAssisterDesignateRepository;

	private IExternalAssisterRepository iExternalAssisterRepository;

	private ExternalAssisterRestUtil externalAssisterRestUtil;

	private Gson platformGson;

	@Autowired
	public ExternalAssisterServiceImpl(IExternalAssisterDesignateRepository iExternalAssisterDesignateRepository,
			IExternalAssisterRepository iExternalAssisterRepository, ExternalAssisterRestUtil externalAssisterRestUtil,
			Gson platformGson) {
		this.iExternalAssisterDesignateRepository = iExternalAssisterDesignateRepository;
		this.iExternalAssisterRepository = iExternalAssisterRepository;
		this.externalAssisterRestUtil = externalAssisterRestUtil;
		this.platformGson = platformGson;
	}

	private static final Logger LOGGER = Logger.getLogger(ExternalAssisterService.class);
	
	@Override
	@Transactional
	public DesignateAssisterResponse designator(DesignateAssisterRequest designateAssisterRequest)  {
		ExternalAssister externalAssister =  null;
		ExternalAssisterDesignate	externalAssisterDesignate =  null;
		DesignateAssisterResponse designateAssisterResponse =null;
		if (isNotNullAndEmpty(designateAssisterRequest.getExternalAssisterId())) {
			externalAssister =iExternalAssisterRepository.getByExternalAssisterId(designateAssisterRequest.getExternalAssisterId());
		}
		
		if (isNotNullAndEmpty(designateAssisterRequest.getHouseholdId())) {
			externalAssisterDesignate =iExternalAssisterDesignateRepository.getByActiveHouseholdId(designateAssisterRequest.getHouseholdId());
		}
		
		if (isNotNullAndEmpty(designateAssisterRequest.getExternalAssisterId())) {
			 designateAssisterResponse= designateRedesignateAssister(designateAssisterRequest, externalAssister, externalAssisterDesignate);
		}
		
		if (!isNotNullAndEmpty(designateAssisterRequest.getExternalAssisterId())) {
			 designateAssisterResponse= deDesignateAssister(externalAssisterDesignate);
		}

		return designateAssisterResponse;

	}

	private DesignateAssisterResponse designateRedesignateAssister(DesignateAssisterRequest designateAssisterRequest, ExternalAssister assister, ExternalAssisterDesignate ead) {
		
		DesignateAssisterResponse designateAssisterResponse = new DesignateAssisterResponse();
		boolean assisterUpdate = false;
		
		try {
			
		if (assister != null && designateAssisterRequest.getExternalAssisterId() != null) {

			if (assister.getName() != null && designateAssisterRequest.getAssisterName() != null && !assister.getName().equalsIgnoreCase(designateAssisterRequest.getAssisterName())) {
				assister.setName(designateAssisterRequest.getAssisterName());
				assisterUpdate = true;
			}
			if (assister.getAgentNpn() != null && designateAssisterRequest.getAgentNpn() != null && !assister.getAgentNpn().equalsIgnoreCase(designateAssisterRequest.getAgentNpn())) {
				assister.setAgentNpn(designateAssisterRequest.getAgentNpn());
				assisterUpdate = true;
			}
			if (assister.getFederalTaxId() != null && designateAssisterRequest.getFederalTaxId() != null && !assister.getFederalTaxId().equalsIgnoreCase(designateAssisterRequest.getFederalTaxId())) {
				assister.setFederalTaxId(designateAssisterRequest.getFederalTaxId());
				assisterUpdate = true;
			}
			if (assister.getType() != null && designateAssisterRequest.getAssisterType() != null && !assister.getType().equalsIgnoreCase(designateAssisterRequest.getAssisterType())) {
				assister.setType(designateAssisterRequest.getAssisterType());
				assisterUpdate = true;
			}
			Integer userId = iExternalAssisterRepository.getUserIdForExternalAssisterId(designateAssisterRequest.getExternalAssisterId());
			if ((isNotNullAndEmpty(userId) && assister.getUserId() != null && !assister.getUserId().equals(userId)) || (userId != null && assister.getUserId() == null)) {
				assister.setUserId((userId));
				assisterUpdate = true;
			}
			if (assisterUpdate) {
				assister = iExternalAssisterRepository.saveAndFlush(assister);
			}
		}

		if (assister == null) {
			
		assister = new ExternalAssister();
		assister.setName(designateAssisterRequest.getAssisterName());
		assister.setAgentNpn(designateAssisterRequest.getAgentNpn());
		assister.setFederalTaxId(designateAssisterRequest.getFederalTaxId());
		assister.setType(designateAssisterRequest.getAssisterType());
		assister.setExternalAssisterId(designateAssisterRequest.getExternalAssisterId());
		Integer userId = iExternalAssisterRepository.getUserIdForExternalAssisterId(designateAssisterRequest.getExternalAssisterId());
		if(isNotNullAndEmpty(userId)){
			assister.setUserId(userId);
		}
		
		assister = iExternalAssisterRepository.saveAndFlush(assister);
		}
		
		if (ead != null) {
			ead.setActive("N");
			iExternalAssisterDesignateRepository.saveAndFlush(ead);
		}

		if (assister != null) {
			ExternalAssisterDesignate externalAssisterDesignate = new ExternalAssisterDesignate();
			externalAssisterDesignate.setActive("Y");
			externalAssisterDesignate.setExternalAssisterId(assister.getId());
			externalAssisterDesignate.setCmrHouseHoldId(designateAssisterRequest.getHouseholdId());

			externalAssisterDesignate = iExternalAssisterDesignateRepository
					.saveAndFlush(externalAssisterDesignate);
		}
		if (isNotNullAndEmpty(assister.getAgentNpn())) {
			designateAssisterResponse.setAgentNpn(assister.getAgentNpn());
		}
		if (isNotNullAndEmpty(assister.getName())) {
			designateAssisterResponse.setAssisterName(assister.getName());
		}
		if (isNotNullAndEmpty(assister.getExternalAssisterId())) {
			designateAssisterResponse.setExternalAssisterId(assister.getExternalAssisterId());
		}
		if (isNotNullAndEmpty(assister.getFederalTaxId())) {
			designateAssisterResponse.setFederalTaxId(assister.getFederalTaxId());
		}
		if (isNotNullAndEmpty(designateAssisterRequest.getHouseholdId())) {
			designateAssisterResponse.setHouseholdId(designateAssisterRequest.getHouseholdId());
		}
		if (isNotNullAndEmpty(assister.getUserId())) {
			designateAssisterResponse.setAssisterUserId(assister.getUserId());
		} else {
			designateAssisterResponse.setWarningMsg(" externalAssisterId Not Provisioned for :" + assister.getExternalAssisterId());
		}
		if (isNotNullAndEmpty(assister.getId())) {
			designateAssisterResponse.setId(assister.getId());
		}
		designateAssisterResponse.setIsActive("Y");
		
		if(assister.getType().equalsIgnoreCase(ExternalBrokerConstants.EXTERNAL_ASSISTER)) {
			EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO = new EnrollmentBrokerUpdateDTO();
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentBrokerUpdateDTO.setAgentBrokerName(assister.getName());
			enrollmentBrokerUpdateDTO.setAssisterBrokerId(assister.getId());
			enrollmentBrokerUpdateDTO.setRoleType(ExternalBrokerConstants.AGENT_ROLE);
			enrollmentBrokerUpdateDTO.setStateLicenseNumber(assister.getAgentNpn());
			enrollmentBrokerUpdateDTO.setHouseHoldCaseId(new Integer(designateAssisterRequest.getHouseholdId()).toString());
			if(isNotNullAndEmpty(assister.getFederalTaxId())) {
			enrollmentBrokerUpdateDTO.setStateEINNumber(assister.getFederalTaxId());
			}
			enrollmentBrokerUpdateDTO.setAddremoveAction(ExternalBrokerConstants.ACTION_ADD);
			enrollmentBrokerUpdateDTO.setMarketType(ExternalBrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL);
			enrollmentRequest.setEnrollmentBrokerUpdateData(enrollmentBrokerUpdateDTO);
			EnrollmentResponse enrollmentResponse = externalAssisterRestUtil.updateEnrollmentDetails(enrollmentRequest);
			if(!isNotNullAndEmpty(enrollmentResponse) || enrollmentResponse.getErrCode() != ExternalBrokerConstants.ERROR_CODE_200 || !enrollmentResponse.getErrMsg().equalsIgnoreCase(ExternalBrokerConstants.MSG_ENROLLMENT_UPDATE_SUCCESS)) {
				throw new GIException("Enrollment Update Failed");
			}
			
		}
		else {
			
			EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO = new EnrollmentBrokerUpdateDTO();
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentBrokerUpdateDTO.setHouseHoldCaseId(new Integer(designateAssisterRequest.getHouseholdId()).toString());
			enrollmentBrokerUpdateDTO.setAssisterBrokerId(assister.getId());
			enrollmentBrokerUpdateDTO.setRoleType(ExternalBrokerConstants.AGENT_ROLE);
			enrollmentBrokerUpdateDTO.setAddremoveAction(ExternalBrokerConstants.ACTION_REMOVE);
			enrollmentBrokerUpdateDTO.setMarketType(ExternalBrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL);
			enrollmentRequest.setEnrollmentBrokerUpdateData(enrollmentBrokerUpdateDTO);
			EnrollmentResponse enrollmentResponse = externalAssisterRestUtil.updateEnrollmentDetails(enrollmentRequest);
			
			if(!isNotNullAndEmpty(enrollmentResponse) || enrollmentResponse.getErrCode() != ExternalBrokerConstants.ERROR_CODE_200 || !enrollmentResponse.getErrMsg().equalsIgnoreCase(ExternalBrokerConstants.MSG_ENROLLMENT_UPDATE_SUCCESS)) {
				throw new GIException("Enrollment Update Failed");
			}
			
		}
		designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_200);
		designateAssisterResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);	
		}
		catch(GIException ex)
		{
			designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_206);
			designateAssisterResponse.setErrMsg("Failed to update broker details in enrollment module: "+ ExternalBrokerUtils.shortenedStackTrace(ex, 4));
			designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		catch(Exception ex) {
			LOGGER.error("Exception caught in designate and redesignate broker details: ",ex);
			designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_209);
			designateAssisterResponse.setErrMsg("Failed to designate or redesignate broker "+ ExternalBrokerUtils.shortenedStackTrace(ex, 4));
			designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}

		return designateAssisterResponse;
	}
	
	private DesignateAssisterResponse deDesignateAssister(ExternalAssisterDesignate externalAssister) {
		DesignateAssisterResponse designateAssisterResponse = new DesignateAssisterResponse();
		try {
			if(isNotNullAndEmpty(externalAssister)) {
				externalAssister.setActive("N");
				iExternalAssisterDesignateRepository.saveAndFlush(externalAssister);
				designateAssisterResponse.setExternalAssisterId(new Integer(externalAssister.getExternalAssisterId()).toString());
				designateAssisterResponse.setHouseholdId(externalAssister.getCmrHouseHoldId());
				designateAssisterResponse.setIsActive("N");
				
				EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO = new EnrollmentBrokerUpdateDTO();
				EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
				enrollmentBrokerUpdateDTO.setHouseHoldCaseId(new Integer(externalAssister.getCmrHouseHoldId()).toString());
				enrollmentBrokerUpdateDTO.setAssisterBrokerId(externalAssister.getExternalAssisterId());
				enrollmentBrokerUpdateDTO.setRoleType(ExternalBrokerConstants.AGENT_ROLE);
				enrollmentBrokerUpdateDTO.setAddremoveAction(ExternalBrokerConstants.ACTION_REMOVE);
				enrollmentBrokerUpdateDTO.setMarketType(ExternalBrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL);
				enrollmentRequest.setEnrollmentBrokerUpdateData(enrollmentBrokerUpdateDTO);
				EnrollmentResponse enrollmentResponse = externalAssisterRestUtil.updateEnrollmentDetails(enrollmentRequest);
				
				if(!isNotNullAndEmpty(enrollmentResponse) || enrollmentResponse.getErrCode() != ExternalBrokerConstants.ERROR_CODE_200 || !enrollmentResponse.getErrMsg().equalsIgnoreCase(ExternalBrokerConstants.MSG_ENROLLMENT_UPDATE_SUCCESS)) {
					throw new GIException("Enrollment Update Failed");
				}
				designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_200);
				designateAssisterResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);	
			}
		}
		catch(GIException ex)
		{
			designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_212);
			designateAssisterResponse.setErrMsg("Failed to update broker details in enrollment module: "+ ExternalBrokerUtils.shortenedStackTrace(ex, 4));
			designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		catch(Exception ex) {
			LOGGER.error("Exception caught in designate and redesignate broker details: ",ex);
			designateAssisterResponse.setErrCode(ExternalBrokerConstants.ERROR_CODE_210);
			designateAssisterResponse.setErrMsg("Failed to designate or dedesignate broker "+ ExternalBrokerUtils.shortenedStackTrace(ex, 4));
			designateAssisterResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}

		return designateAssisterResponse;
		
	}

	@Override
	@Transactional
	public DesignateAssisterResponse getAssisterDetailForhouseholdId(int householdId) {
		List<Object[]> externalAssister =iExternalAssisterDesignateRepository.getExternalBrokerDetails(householdId);
		DesignateAssisterResponse designateAssisterResponse = new DesignateAssisterResponse();
		if (externalAssister != null && !externalAssister.isEmpty() && externalAssister.get(0).length == 9) {
			if (isNotNullAndEmpty(externalAssister.get(0)[6])) {
				designateAssisterResponse.setAgentNpn(externalAssister.get(0)[6].toString());
			}
			if (isNotNullAndEmpty(externalAssister.get(0)[3])) {
				designateAssisterResponse.setAssisterName(externalAssister.get(0)[3].toString());
			}
			if (isNotNullAndEmpty(externalAssister.get(0)[4])) {
				designateAssisterResponse.setAssisterType(externalAssister.get(0)[4].toString());
			}
			if (isNotNullAndEmpty(externalAssister.get(0)[0])) {
				designateAssisterResponse.setExternalAssisterId(externalAssister.get(0)[0].toString());
			}
			if (isNotNullAndEmpty(externalAssister.get(0)[5])) {
				designateAssisterResponse.setFederalTaxId(externalAssister.get(0)[5].toString());
			}
			if (isNotNullAndEmpty(externalAssister.get(0)[1])) {
				designateAssisterResponse.setHouseholdId((Integer) (externalAssister.get(0)[1]));
			}
			if (isNotNullAndEmpty(externalAssister.get(0)[7])) {
				designateAssisterResponse.setAssisterUserId((Integer) (externalAssister.get(0)[7]));
			}
			if (isNotNullAndEmpty(externalAssister.get(0)[8])) {
				designateAssisterResponse.setId((Integer) (externalAssister.get(0)[8]));
			}
			designateAssisterResponse.setIsActive("Y");
		}
		return designateAssisterResponse;
	}

	@Override
	@Transactional
	public List<DesignateAssisterResponse> householdsForBroker(DesignateAssisterRequest designateAssisterRequest) {
		List<Object[]> externalAssister =iExternalAssisterDesignateRepository.getExternalBrokerDetailsForUserId(designateAssisterRequest.getExternalUserId(),designateAssisterRequest.getExternalAssisterId());
		List<DesignateAssisterResponse>designateAssisterResponseList = new ArrayList<DesignateAssisterResponse>();
		if(externalAssister != null && !externalAssister.isEmpty()){
			for (Object[] externalAssisterData : externalAssister) {
				DesignateAssisterResponse designateAssisterResponse = new DesignateAssisterResponse();
				if (isNotNullAndEmpty(externalAssisterData[6])) {
					designateAssisterResponse.setAgentNpn(externalAssisterData[6].toString());
				}
				if (isNotNullAndEmpty(externalAssisterData[3])) {
					designateAssisterResponse.setAssisterName(externalAssisterData[3].toString());
				}
				if (isNotNullAndEmpty(externalAssisterData[4])) {
					designateAssisterResponse.setAssisterType(externalAssisterData[4].toString());
				}
				if (isNotNullAndEmpty(externalAssisterData[0])) {
					designateAssisterResponse.setExternalAssisterId(externalAssisterData[0].toString());
				}
				if (isNotNullAndEmpty(externalAssisterData[5])) {
					designateAssisterResponse.setFederalTaxId(externalAssisterData[5].toString());
				}
				if (isNotNullAndEmpty(externalAssisterData[1])) {
					designateAssisterResponse.setHouseholdId((Integer) (externalAssisterData[1]));
				}
				if (isNotNullAndEmpty(externalAssisterData[7])) {
					designateAssisterResponse.setAssisterUserId((Integer) (externalAssisterData[7]));
				}
				designateAssisterResponse.setIsActive("Y");
				designateAssisterResponseList.add(designateAssisterResponse);
				}
		}
			return designateAssisterResponseList;
	}
	
	
	@Override
	@Transactional
	public String getExternalUserIdsForBroker(String externalAssisterId) {
		List<String> externalUsersList =iExternalAssisterDesignateRepository.getUsersForExternalAssisterId(externalAssisterId);
		Map<String,Object> returnMap= new HashMap<String,Object>();
		returnMap.put("externalAssisterId",externalAssisterId);
		returnMap.put("externalUserIds",new ArrayList<String>());
		if(externalUsersList != null && !externalUsersList.isEmpty()){
			returnMap.put("externalAssisterId",externalAssisterId);
			returnMap.put("externalUserIds",externalUsersList);
						
		}
			return platformGson.toJson(returnMap); 
	}
	
	
	@Override
	@Transactional
	public DesignateAssisterResponse getAssisterDetails(String externalAssisterId) {
		DesignateAssisterResponse designateAssisterResponse = new DesignateAssisterResponse();
		ExternalAssister externalAssister =iExternalAssisterRepository.getByExternalAssisterId(externalAssisterId);
		if (externalAssister != null) {
			if (isNotNullAndEmpty(externalAssister.getId())) {
				designateAssisterResponse.setId(externalAssister.getId());
			}
			if (isNotNullAndEmpty(externalAssister.getAgentNpn())) {
				designateAssisterResponse.setAgentNpn(externalAssister.getAgentNpn());
			}
			if (isNotNullAndEmpty(externalAssister.getName())) {
				designateAssisterResponse.setAssisterName(externalAssister.getName());
			}
			if (isNotNullAndEmpty(externalAssister.getType())) {
				designateAssisterResponse.setAssisterType(externalAssister.getType());
			}
			if (isNotNullAndEmpty(externalAssister.getExternalAssisterId())) {
				designateAssisterResponse.setExternalAssisterId(externalAssister.getExternalAssisterId());
			}
			if (isNotNullAndEmpty(externalAssister.getFederalTaxId())) {
				designateAssisterResponse.setFederalTaxId(externalAssister.getFederalTaxId());
			}
			if (isNotNullAndEmpty(externalAssister.getUserId())) {
				designateAssisterResponse.setAssisterUserId(externalAssister.getUserId());
			}
		}
		return designateAssisterResponse;
	}
}
