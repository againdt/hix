package com.getinsured.eligibility.at.ref.dozzer.converter;

import org.dozer.DozerConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.iex.util.ReferralUtil;


/**
 * @author pravin
 * 
 */
@Component("dozzerStringTrimmer")
@Scope("singleton")
public class GlobalStringTrimmer extends
DozerConverter<com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String, String> {

public GlobalStringTrimmer() {
super(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String.class,
		String.class);
}

@Override
public com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String convertFrom(
	String arg0,
	com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String arg1) {
return null;
}

@Override
public String convertTo(
	com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String source,
	String dest) {
if (source != null) {
	return ReferralUtil.removeExtraSpace(source.getValue());
}
return null;
}

}
