
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "insurancePolicyExpirationDate",
    "insurancePolicyEffectiveDate"
})
public class Insurance implements Serializable
{

    @JsonProperty("insurancePolicyExpirationDate")
    private String insurancePolicyExpirationDate;
    @JsonProperty("insurancePolicyEffectiveDate")
    private String insurancePolicyEffectiveDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 8676076064445359572L;

    @JsonProperty("insurancePolicyExpirationDate")
    public String getInsurancePolicyExpirationDate() {
        return insurancePolicyExpirationDate;
    }

    @JsonProperty("insurancePolicyExpirationDate")
    public void setInsurancePolicyExpirationDate(String insurancePolicyExpirationDate) {
        this.insurancePolicyExpirationDate = insurancePolicyExpirationDate;
    }

    @JsonProperty("insurancePolicyEffectiveDate")
    public String getInsurancePolicyEffectiveDate() {
        return insurancePolicyEffectiveDate;
    }

    @JsonProperty("insurancePolicyEffectiveDate")
    public void setInsurancePolicyEffectiveDate(String insurancePolicyEffectiveDate) {
        this.insurancePolicyEffectiveDate = insurancePolicyEffectiveDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("insurancePolicyExpirationDate", insurancePolicyExpirationDate).append("insurancePolicyEffectiveDate", insurancePolicyEffectiveDate).append("additionalProperties", additionalProperties).toString();
    }

}
