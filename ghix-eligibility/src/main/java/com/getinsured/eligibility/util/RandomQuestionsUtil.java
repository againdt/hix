/**
 * 
 */
package com.getinsured.eligibility.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author Biswakesh
 * 
 */
@Component
public class RandomQuestionsUtil {
	private static final Logger LOGGER = Logger
			.getLogger(RandomQuestionsUtil.class);
	private static final int START = 4;
	private static final int END = 12;
	
	@Autowired
	private ILocationRepository iLocationRepository;

	public Map<Integer,String> generateRandomList(SsapApplicant ssapApplicant, int noOfApplicants) {
		Map<Integer,String> randomQuestionsMap = null;
		Map<Integer, String> ssapMap = null;
		Random random = null;
		
		try {
			ssapMap = convertSsapApplicantModelToMap(ssapApplicant,noOfApplicants);
			randomQuestionsMap = new ConcurrentHashMap<Integer,String>();
			
			random = new Random();
			for(int i=1;i<=3;i++) {
				randomQuestionsMap.put(i, ssapMap.get(i));
			}
			
			for (int idx = 0; idx < 2; idx++) {
				int randomInt = -1;
				String entity = null;
				
				do {	
					randomInt = getRandomInteger(START, END, random);
					entity = ssapMap.get(randomInt);
				} while(StringUtils.isEmpty(entity) || randomQuestionsMap.containsKey(randomInt));
				
				randomQuestionsMap.put(randomInt,entity);
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE GENERATING RANDOM QST LST: ", ex);
		}

		return randomQuestionsMap;
	}

	private Integer getRandomInteger(int aStart, int aEnd, Random aRandom) {
		if (aStart > aEnd) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		}
		long range = (long) aEnd - (long) aStart + 1;
		long fraction = (long) (range * aRandom.nextDouble());
		int randomNumber = (int) (fraction + aStart);
		return randomNumber;
	}
	
	/**
	 * 
	 * @param ssapApplicant
	 * @return
	 */
	private Location getLocation(SsapApplicant ssapApplicant) {
		Location location = null;
		
		if (ssapApplicant.getOtherLocationId() != null
				&& ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId()
						.intValue())) {
			location = iLocationRepository.findOne(ssapApplicant
					.getOtherLocationId().intValue());
		}
		
		return location;
	}
	
	/**
	 * 
	 * @param ssapApplicant
	 * @return
	 */
	private Map<Integer,String> convertSsapApplicantModelToMap(SsapApplicant ssapApplicant, int noOfApplicants) {
		String zipCode = null;
		String county = null;
		Location location = getLocation(ssapApplicant);
		
		Map<Integer, String> applicantMap = new HashMap<Integer,String>();
		
		applicantMap.put(1, ssapApplicant.getFirstName());
		applicantMap.put(2, ssapApplicant.getLastName());
		applicantMap.put(3, ssapApplicant.getBirthDate().toString());
		//Set 4,11,12 to Empty. we should not display ssn, Medicaid id, case number in security Question page. HIX-78120
		applicantMap.put(4, StringUtils.EMPTY);
		
		
		if(null != location) {
			zipCode = location.getZip();
			county = location.getCounty();
		}

		applicantMap.put(5, zipCode);
		applicantMap.put(6, ssapApplicant.getGender());
		applicantMap.put(7, ssapApplicant.getEmailAddress());
		applicantMap.put(8, ssapApplicant.getPhoneNumber());
		applicantMap.put(9, county);
	
		applicantMap.put(10, String.valueOf(noOfApplicants));
		applicantMap.put(11,  StringUtils.EMPTY);
		applicantMap.put(12, StringUtils.EMPTY);
		return applicantMap;
	}
}
