<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page import="java.util.Calendar"%>
<%@ page isELIgnored="false"%>

<style>
	h1 img {
	    height: auto;
	    width: auto;
	}
</style>

<script type="text/javascript" >

	function doSubmitfun(formName, url) {

		if (isValidateForm() && formName && url) {
			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
	}

	function isValidateForm() {

		var flag = true;
		var loForm = document.forms["frmUploadPlans"];

		if (loForm) {

			var laFields = new Array();
			var li = 0;
			var nodeFileNode = loForm["fileUploadExcel"];

			if (nodeFileNode.files.length == 0) {
				laFields[li++] = "Please select a CrossWalk excel file to upload.";
			}
			else {
				var fileName = nodeFileNode.value;
				var ext =fileName.split('.').pop();
				var XLSX = "xlsx" ;
				var XLS ="xls";

				if (ext!=XLSX && ext!=XLS) {
					laFields[li++] = "File type must be Excel.";
				}
			}

			if (laFields.length > 0) {

				if(laFields.length > 1) {

					for (var fi = 0; fi < laFields.length; fi++) {
						laFields[fi] = "- " + laFields[fi];
					}
				}
		       	alert(laFields.join('\n'));
		       	flag = false;
		    }
		}
		return flag;
	}
</script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>

<div class="gutter10">
	<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/></c:set>

	<div class="row-fluid">
		<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/manageissuer"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
        	<li><spring:message  code="pgheader.uploadNetworkTransparency"/></li>
        </ul><!--page-breadcrumb ends-->		
		<h1 id="skip"><img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img"/>${issuerObj.name}</h1>
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
               <h4><spring:message  code="pgheader.issuerAbout"/></h4>
            </div>
			<jsp:include page="issuerDetailsLeftNav.jsp">
        			<jsp:param name="pageName" value="netTrans"/>
			</jsp:include>
		</div>
		<!-- end of span3 -->
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message  code="pgheader.networkTransparencyStatus"/></h4>
				<%-- <c:if test="${exchangeType != 'PHIX'}"> remove condition as per HIX-17647 --%>
				
				<a class="btn btn-small pull-right" href="javascript:doSubmitfun('frmUploadPlans', '${pageContext.request.contextPath}/admin/issuer/processAndPersistNetworkTransExcel/${encIssuerId}');" class="btn btn-primary offset1" type="submit" id="save"><spring:message  code="label.save"/></a>
				<a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/displayNetworkTransparencyData/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a>
				<%-- </c:if> --%>
			</div>

			<div class="gutter10">
				<div class="row-fluid">
					<div class="span9" id="rightpanel">
						<c:if test="${uploadSuccessNetworkTrans != null}">
							<div align="left" style="width:100%; font-size: 10pt; color: green; font-weight: bold;">
								<c:out value="${uploadSuccessNetworkTrans}"/>
							</div>
							<br/>
						</c:if>
						<c:if test="${uploadFailedNetworkTrans != null}">
							<div align="left" style="width:100%; font-size: 10pt; color: red; font-weight: bold;">
								<c:out value="${uploadFailedNetworkTrans}"/>
							</div>
							<br/>
						</c:if>

						<form:form id="frmUploadPlans" name="frmUploadPlans" action="${pageContext.request.contextPath}/admin/issuer/processAndPersistNetworkTransExcel/${issuerObj.id}" modelAttribute="UploadExcel" enctype="multipart/form-data" method="POST">
							<df:csrfToken/>
							<div style="width: 30%; float:left">
								<label class="control-label" for="filePlanAndBenefits"><spring:message  code="label.selectYear"/> 
									<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
								</label>
							</div>

							<div style="width: 70%; float:right">
								<c:set var="currentYear" ><%=Calendar.getInstance().get(Calendar.YEAR)%></c:set>
								<select size="1" id="field_name" name="field_name" style="width :25%">
									<option value="${currentYear + 1}" selected>${currentYear + 1}</option>
									<option value="${currentYear}">${currentYear}</option>
								</select>
							</div>

							<input type="hidden" id="field_name" name="field_name" />
							</br> </br>

							<div style="width: 35%; float:left">
								<label class="control-label" for="filePlanAndBenefits"><spring:message  code="label.uploadNetworkTransparency"/>
									<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
								</label>
							</div>

							<div style="width: 65%; float:right">
								<input type="file" class="filestyle" id="fileUploadExcel" name="fileUploadExcel" data-classButton="btn">
							</div>

							<!-- <div class="control-group">
								<label class="control-label" for="filePlanAndBenefits"><b>Step 2:</b>  </label>
								<div class="controls">
								</div>
							</div> -->

							<!--  <div class="gutter10">
								<a href="javascript:document.forms['frmUploadPlans'].reset()" class="btn offset2" id="cancel">Cancel</a>
			            	</div> -->
						</form:form>
					</div><!-- end of .span9 -->
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	</div><!--  end of span9 -->
</div><!-- end row-fluid -->
		