package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;

/**
 * @author ajinkya_m
 * @since 19/03/2013
 * 
 */
public class EnrollmentCancelTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCancelTask.class);
	
	private String employerIdNumber;
	private EnrollmentBatchService enrollmentBatchService;
	private EnrollmentEmailUtils enrollmentEmailUtils;
	
	public EnrollmentCancelTask() {
		super();
	}

	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext){
		LOGGER.info(" EnrollmentCancelTask.execute : START");
		EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
		enrollmentBatchEmailDTO.setFileLocation(GhixBatchConstants.xmlShopReconExtractPath);
			
		try{
				enrollmentBatchService.updateShopEnrollemntToCancelled(Integer.parseInt(getEmployerIdNumber()));
			}
			catch(Exception e){
				LOGGER.info("EnrollmentCancelTask.execute : FAIL "+e.getCause());
				enrollmentBatchEmailDTO.setErrorCode(201);
				enrollmentBatchEmailDTO.setErrorMessage(e.getMessage());
				enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
			}
		LOGGER.info(" EnrollmentCancelTask.execute : END");	
		return RepeatStatus.FINISHED;
	}

	public String getEmployerIdNumber() {
		return employerIdNumber;
	}

	public void setEmployerIdNumber(String employerIdNumber) {
		this.employerIdNumber = employerIdNumber;
	}

	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(
			EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}
	public EnrollmentEmailUtils getEnrollmentEmailUtils() {
		return enrollmentEmailUtils;
	}

	public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
		this.enrollmentEmailUtils = enrollmentEmailUtils;
	}	
}
