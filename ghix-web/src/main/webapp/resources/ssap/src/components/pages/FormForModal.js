import React, { Fragment, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// ActionCreators

import {
    addContext,
    changePage,
    clearFormSubmission,
    deleteForm,
    removeContext,
    saveData,
    submitForm,
    validateForm
} from '../../actions/actionCreators';

// Constants

import CONFIG from '../../.env';
import ENDPOINTS from '../../constants/endPointsList';



const FormForModal  = props => {

    // Props from Parent Component (usually it is Page)

    const {
        topLevelValidity,
    } = props;

    // Props from Redux Store

    const {
        console: {
            isFormValidationShown,
            isInvalidInputsShown
        },
        data: {
            singleStreamlinedApplication,
            singleStreamlinedApplication: {
                ssapApplicationId,
                maxAchievedPage
            }
        },
        form: {
            FormForModal: {
                isEligiblyForSave,
                isFormSubmitted,
                registeredInputs
            }
        },
        environment: {
            impersonation
        },
        page
    } = props;


    useEffect(() => {
        props.actions.addContext(FormForModal, 'FormForModal');
        return () => {
            props.actions.deleteForm('FormForModal');
            props.actions.removeContext('FormForModal');
        };
    }, []);

    // Up to date primitive of registered inputs values to add as dependency for watching

    const registeredInputsToString = registeredInputs.map(input => input.valueOf()).toString();

    useEffect(() => {
        props.actions.validateForm(checkIfEligible(), 'FormForModal');
    }, [registeredInputsToString, topLevelValidity]);

    useEffect(() => {
        isFormSubmitted &&  isEligiblyForSave && props.actions.clearFormSubmission('FormForModal');
    }, [isEligiblyForSave]);

    const getBody = destinationPage => {

        let pageForSave;

        switch(destinationPage) {
        case 'increase':
            pageForSave = page + 1; break;
        case 'decrease':
            pageForSave = page - 1; break;
        default:
            pageForSave = destinationPage; break;
        }

        return {
            'application': JSON.stringify({
                ...singleStreamlinedApplication,
                currentPage: pageForSave,
                maxAchievedPage: maxAchievedPage < pageForSave ? pageForSave : maxAchievedPage
            }),
            'applicationId': ssapApplicationId,
            'csrftoken': window.csrf_token,
            'csrOverride': impersonation
        };
    };

    FormForModal.onFormSubmitForValidationOnly = callbacks => {
        props.actions.submitForm('FormForModal');
        if(isEligiblyForSave) {
            callbacks && callbacks.forEach(callback => {
                callback();
            });
        }
    };

    FormForModal.onFormSubmit = (destinationPage, callbacks) => {
        props.actions.submitForm('FormForModal');
        if (isEligiblyForSave) {
            if(CONFIG.ENV.DEVELOPMENT) {
                alert('Form is eligible for saving...');
                props.actions.changePage(destinationPage);
                callbacks && callbacks.forEach(callback => {
                    callback();
                });
            }
            if(CONFIG.ENV.PRODUCTION) {
                console.log('Saving...');
                props.actions.saveData(
                    callbacks || [],
                    `${window.location.origin}${ENDPOINTS.SAVE_DATA_ENDPOINT}${window.csrf_token}`,
                    getBody(destinationPage)
                );
            }
        } else {
            // props.actions.clearFormSubmission();
        }
    };

    FormForModal.onFormExit = callbacks => {
        registeredInputs.forEach(input => {
            const [ onFieldChange, getNewData, indexInArray, fields ] = input.onFormExit;
            if(input.isInputTouched) {
                onFieldChange(getNewData(input.initialInputValue), indexInArray, fields, true);
            }
        });
        callbacks && callbacks.forEach(callback => {
            callback();
        });
    };

    const checkIfEligible = () => {
        if(registeredInputs.length === 0) {
            return true;
        }
        const isApprovedByTopLevel = topLevelValidity === undefined ? true : topLevelValidity;
        let result = true;
        registeredInputs.forEach(input => {
            if (input.inputIsValid === false || (input.isInputRequired && (input.inputValue === '' || input.inputValue === null))) {
                isInvalidInputsShown && console.log(input);
                result = false;
            }
        });
        return {
            isEligiblyForSave: isApprovedByTopLevel && result,
            isFormOnTopLevelValid: isApprovedByTopLevel,
            areAllInputsValid: result,
        };
    };

    // Consoles start

    if (registeredInputs.length > 0) {
        isFormValidationShown && console.log('FormForModal...... Top level validity: ', topLevelValidity);
        isFormValidationShown && console.log('FormForModal...... Registered inputs: ',
            registeredInputs.map(input => ({
                valid: input.inputIsValid,
                rqd: input.isInputRequired,
                name: input.inputName,
            })));
    }

    isFormValidationShown && console.log('FormForModal...... eligible: ', registeredInputs.length === 0 ? true : isEligiblyForSave);

    // Consoles end

    const updateReactChildren = children => {
        return React.Children.map(children, child => {
            if(!React.isValidElement(child)) {
                return child;
            }
            let childProps = {};
            if(React.isValidElement(child)
                && (typeof child.props.inputData === 'object' || typeof child.props.currentData === 'object')) {
                childProps = {
                    parentForm: {
                        name: 'FormForModal'
                    }
                };
            }
            childProps.children = updateReactChildren(child.props.children);
            return React.cloneElement(child, childProps);
        });
    };

    return (
        <Fragment>
            { updateReactChildren(props.children) }
        </Fragment>
    );
};

const mapStateToProps = state => ({
    console: state.console,
    data: state.data,
    form: state.form,
    environment: state.headerLinks,
    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        addContext,
        changePage,
        deleteForm,
        clearFormSubmission,
        removeContext,
        saveData,
        submitForm,
        validateForm
    }, dispatch)
});

FormForModal.defaultProps = {
    name: 'FormForModal'
};

export default connect(mapStateToProps, mapDispatchToProps)(FormForModal);
