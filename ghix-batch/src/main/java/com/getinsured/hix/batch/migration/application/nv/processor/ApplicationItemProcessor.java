package com.getinsured.hix.batch.migration.application.nv.processor;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import com.getinsured.batch.util.JsonUtil;
import com.getinsured.hix.batch.migration.application.nv.mapper.AccountTransferMapperBatch;
import com.getinsured.hix.batch.migration.application.nv.mapper.AccountTransferUtil;
import com.getinsured.hix.indportal.dto.dm.application.ApplicationDTO;
import com.getinsured.hix.indportal.dto.dm.application.ApplicationJSONDTO;
import com.getinsured.hix.indportal.dto.dm.application.Member;
import com.getinsured.hix.indportal.dto.dm.application.result.ApplicationData;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class ApplicationItemProcessor implements ItemProcessor<String, ApplicationData>, ApplicationContextAware {
	private final static Logger LOGGER = Logger.getLogger(ApplicationItemProcessor.class);
	
	private String doNotOverwrite;

	private static ApplicationContext applicationContext;
	private Gson mapper = new GsonBuilder().disableHtmlEscaping().create();
	private AccountTransferMapperBatch accountTransferMapper;

	private static JAXBContext jc = null;
	private static Marshaller marshaller = null;
	private static final String LOGGERSTMT1 ="Reading Application JSON :";
	private static final String AccountMapperBeanName = "accountTransferMapperBatch";
	private static final String JSON_START = "{\"application\":{";
	private static final String JSON_END = "}}";
	private static final String APPLICATION_START = "\"applications\":{";
	private static final String SORT_BY_VALUE = "creationTimestamp";
	
	private SsapApplicationRepository ssapApplicationRepository;
	
	static {
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.erp.gov.cms.dsh.at.extension._1");
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);

		} catch (JAXBException e) {
			LOGGER.error("Error while initializing JAXBContext", e);
		}

	}

	public ApplicationData process(String result) throws Exception {
		ApplicationData data = new ApplicationData();
		data.setJsonSource(result);
		if (!StringUtils.isEmpty(result)) {
			
			if(StringUtils.startsWithIgnoreCase(result, APPLICATION_START)) {
				result = result.substring(APPLICATION_START.length(), result.length()-1);
			}
			else if (StringUtils.endsWithIgnoreCase(result, ",")) {
				result = result.substring(0, result.length() - 1);
			}
			result = JSON_START + result + JSON_END;
		}
		try {
			LOGGER.info(LOGGERSTMT1+result);
			JsonReader jsonReader = mapper.newJsonReader(new InputStreamReader(new ByteArrayInputStream(result.getBytes())));
			ApplicationJSONDTO obj = mapper.fromJson(jsonReader, ApplicationJSONDTO.class);
			PageRequest pageRequest = new PageRequest(0, 1, new Sort(Sort.Direction.DESC, SORT_BY_VALUE));
			ssapApplicationRepository = (SsapApplicationRepository) applicationContext.getBean("ssapApplicationRepository");
			List<SsapApplication> applicationList = ssapApplicationRepository.findLatestByExternalAppId(obj.getApplication().values().iterator().next().getResult().getInsuranceApplicationIdentifier().toString(), pageRequest);
			boolean processApplication = true;
			if("true".equalsIgnoreCase(doNotOverwrite) && applicationList != null && applicationList.size() > 0) {
				processApplication = false;
			}
			if(processApplication) {
			accountTransferMapper = (AccountTransferMapperBatch) applicationContext.getBean(AccountMapperBeanName);
			for(Entry<String, ApplicationDTO> dto :obj.getApplication().entrySet())
			{
				for(Entry<String,Member> member : dto.getValue().getResult().getComputed().getMembers().entrySet())
				{
					if(StringUtils.isEmpty(member.getValue().getPersonTrackingNumber()))
					{
						StringBuffer sb = new StringBuffer(UUID.randomUUID().toString());
						sb.deleteCharAt(8);
						sb.deleteCharAt(12);
						sb.deleteCharAt(16);
						sb.deleteCharAt(20);
						member.getValue().setPersonTrackingNumber(sb.toString().substring(0,28));
					}
				}
			}
			AccountTransferRequestPayloadType accountTrf = accountTransferMapper.populateAccountTransferRequest(obj);
				
			if(applicationList != null && applicationList.size() > 0) {
				SsapApplication ssapApplication = applicationList.get(0);
				final SingleStreamlinedApplication applicationData = JsonUtil.getSingleStreamlinedApplicationFromJson(ssapApplication.getApplicationData());
				if(applicationData.getIncomeConsentChanged() != null) {
					QuantityType noOfYears = AccountTransferUtil.neimCore2fctry.createQuantityType();
					noOfYears.setValue(BigDecimal.valueOf(5));
					if(!applicationData.getAgreeToUseIncomeData()) {
						noOfYears.setValue(new BigDecimal(applicationData.getNumberOfYearsAgreed()));
					}
					accountTrf.getInsuranceApplication().setInsuranceApplicationCoverageRenewalYearQuantity(noOfYears);
				}
					if(applicationData.getRenewalConsentChanged() != null && applicationData.getRenewalConsentChanged()) {
					accountTrf.getInsuranceApplication().setInsuranceApplicationTaxReturnAccessIndicator(AccountTransferUtil.addBoolean(applicationData.getConsentAgreement()));
				}
			}
			data.setRequestObj(accountTrf);
			}
			//System.out.println(responseString);
		} catch (Exception e) {
			data.setErrorMessage(ExceptionUtils.getFullStackTrace(e).replaceAll(System.lineSeparator(), " "));
			LOGGER.error("Unable to read next JSON object" + e);
			//throw new ParseException("Unable to read next JSON object", e);
		}
		return data;
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		applicationContext = context;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setDoNotOverwrite(String doNotOverwrite) {
		this.doNotOverwrite = doNotOverwrite;
	}
}
