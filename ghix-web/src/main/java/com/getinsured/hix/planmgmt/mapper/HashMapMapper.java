package com.getinsured.hix.planmgmt.mapper;

import java.util.HashMap;
import java.util.List;

import com.getinsured.hix.platform.file.mapper.Mapper;

/**
 * Mapper class for converting the data files benefit data to key-value paired HashMap.
 */
public class HashMapMapper extends Mapper<HashMap<String, String>> 
 {	
	public HashMap<String, String> mapData(final List<String> data, final List<String> columns) 
	{
		HashMap<String, String> benefit = new HashMap<String, String>();
		int index = 0;
		for (String column : columns) 
		{
			benefit.put(column, data.get(index));
			index++;
		}
		return benefit;
	}
}
