package com.getinsured.hix.indportal.dto.dm.application;

public class EnrollmentGroupPlan {
	 private Float allocatedAptcAmount;
	 private String insurancePlanIdentifier;
	 private String enrollmentGroupPlanType;
	 private String insurancePlanName;
	 private Float monthlyPolicyPremiumAmount;


	 // Getter Methods 

	 public Float getAllocatedAptcAmount() {
	  return allocatedAptcAmount;
	 }

	 public String getInsurancePlanIdentifier() {
	  return insurancePlanIdentifier;
	 }

	 public String getEnrollmentGroupPlanType() {
	  return enrollmentGroupPlanType;
	 }

	 public String getInsurancePlanName() {
	  return insurancePlanName;
	 }

	 public Float getMonthlyPolicyPremiumAmount() {
	  return monthlyPolicyPremiumAmount;
	 }

	 // Setter Methods 

	 public void setAllocatedAptcAmount(Float allocatedAptcAmount) {
	  this.allocatedAptcAmount = allocatedAptcAmount;
	 }

	 public void setInsurancePlanIdentifier(String insurancePlanIdentifier) {
	  this.insurancePlanIdentifier = insurancePlanIdentifier;
	 }

	 public void setEnrollmentGroupPlanType(String enrollmentGroupPlanType) {
	  this.enrollmentGroupPlanType = enrollmentGroupPlanType;
	 }

	 public void setInsurancePlanName(String insurancePlanName) {
	  this.insurancePlanName = insurancePlanName;
	 }

	 public void setMonthlyPolicyPremiumAmount(Float monthlyPolicyPremiumAmount) {
	  this.monthlyPolicyPremiumAmount = monthlyPolicyPremiumAmount;
	 }
	}