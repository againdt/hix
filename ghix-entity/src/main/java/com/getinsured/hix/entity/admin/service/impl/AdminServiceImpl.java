package com.getinsured.hix.entity.admin.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.admin.service.AdminService;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;

/**
 * Implements {@link AdminService} to perform Entity Admin related operations
 * like find,save,update etc.
 */
@Service("adminService")
@Transactional
public class AdminServiceImpl implements AdminService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminServiceImpl.class);
	
	@Autowired
	private ObjectFactory<QueryBuilder> delegateFactory;

	private static final int ZERO = 0;
	private static final int TEN = 10;

	/**
	 * @see AdminService#findAssister(Assister)
	 * 
	 * @param assister
	 *            {@link Assister}
	 * 
	 * @return list of Assisters
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Assister> findAssister(Assister assister) {
		
		LOGGER.info("findAssister: START");
		
		QueryBuilder<Assister> assisterQuery = delegateFactory.getObject();
		assisterQuery.buildObjectQuery(Assister.class);

		String firstName = (assister != null) ? assister.getFirstName() : "";
		if (firstName != null && firstName.length() > 0) {
			assisterQuery.applyWhere("firstname", firstName.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		
		LOGGER.info("findAssister: END");
		
		return assisterQuery.getRecords(ZERO, TEN);
	}
}
