package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;

public class MedicaidChipStandard {
	 private Integer medicaidStandardPercent;
	 private String medicaidStandardBasisType;
	 private Integer chipStandardPercent;
	 private String chipStandardBasisType;
	 private String adultGroupCategoryStatus;
	 private String childCaretakerDeprivedStatus;
	 private String parentCaretakerCategoryStatus;
	 private ArrayList<String> parentCaretakerChildList;


	 // Getter Methods 

	 public Integer getMedicaidStandardPercent() {
	  return medicaidStandardPercent;
	 }

	 public String getMedicaidStandardBasisType() {
	  return medicaidStandardBasisType;
	 }

	 public Integer getChipStandardPercent() {
	  return chipStandardPercent;
	 }

	 public String getAdultGroupCategoryStatus() {
	  return adultGroupCategoryStatus;
	 }

	 public String getChildCaretakerDeprivedStatus() {
	  return childCaretakerDeprivedStatus;
	 }

	 public String getParentCaretakerCategoryStatus() {
	  return parentCaretakerCategoryStatus;
	 }

	 // Setter Methods 

	 public void setMedicaidStandardPercent(Integer medicaidStandardPercent) {
	  this.medicaidStandardPercent = medicaidStandardPercent;
	 }

	 public void setMedicaidStandardBasisType(String medicaidStandardBasisType) {
	  this.medicaidStandardBasisType = medicaidStandardBasisType;
	 }

	 public void setChipStandardPercent(Integer chipStandardPercent) {
	  this.chipStandardPercent = chipStandardPercent;
	 }

	 public void setAdultGroupCategoryStatus(String adultGroupCategoryStatus) {
	  this.adultGroupCategoryStatus = adultGroupCategoryStatus;
	 }

	 public void setChildCaretakerDeprivedStatus(String childCaretakerDeprivedStatus) {
	  this.childCaretakerDeprivedStatus = childCaretakerDeprivedStatus;
	 }

	 public void setParentCaretakerCategoryStatus(String parentCaretakerCategoryStatus) {
	  this.parentCaretakerCategoryStatus = parentCaretakerCategoryStatus;
	 }

	public ArrayList<String> getParentCaretakerChildList() {
		return parentCaretakerChildList;
	}

	public void setParentCaretakerChildList(ArrayList<String> parentCaretakerChildList) {
		this.parentCaretakerChildList = parentCaretakerChildList;
	}

	public String getChipStandardBasisType() {
		return chipStandardBasisType;
	}

	public void setChipStandardBasisType(String chipStandardBasisType) {
		this.chipStandardBasisType = chipStandardBasisType;
	}
	}
