package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.AutoEnrollService;
import com.getinsured.hix.batch.ssap.renewal.util.AutoEnrollPartitionerParams;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Auto Enroll Partitioner class is used to get read Renewal Application IDs from database and do Partitioner.
 * 
 * @since July 19, 2019
 */
@Component("ssapAutoEnrollPartitioner")
@Scope("step")
public class AutoEnrollPartitioner implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutoEnrollPartitioner.class);
	
	private static final String STATE_CODE = "CA";

	private Integer serverCount;
	private Integer serverName;
	private Long batchSize;
	private Long renewalYear;
	private String processErrorRecords;
	private String autoEnrollCommitInterval;
	private AutoEnrollService autoEnrollService;
	private BatchJobExecutionService batchJobExecutionService;
	private AutoEnrollPartitionerParams autoEnrollPartitionerParams;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		int servName = 0;
		int servCount = 1;
		Map<String, ExecutionContext> partitionMap = null;
		StringBuffer errorMessage = new StringBuffer();
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		try {
			//As per HIX-118466, for CA multiple batch instances of same batch job can run in parallel
			if (!hasRunningBatchSizeOne() && !STATE_CODE.equalsIgnoreCase(stateCode)) {
				errorMessage.append(RenewalUtils.EMSG_RUNNING_BATCH);
				return partitionMap;
			}
			
			String defaultBatchSize = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);
			renewalYear = Long.valueOf(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));

			if (null == batchSize || 0l == batchSize) {
				batchSize = Long.valueOf(defaultBatchSize);
			}

			if (!validateParams(errorMessage)) {
				return partitionMap;
			}
			autoEnrollPartitionerParams.setRenewalYear(renewalYear);

			if (processErrorRecords != null && processErrorRecords.equalsIgnoreCase("Y")) {
				autoEnrollPartitionerParams.setProcessErrorRecordFlag(true);
			}
			else {
				autoEnrollPartitionerParams.setProcessErrorRecordFlag(false);
			}

			List<Long> partitionerRenewalApplicationIdList;
			
			if(STATE_CODE.equalsIgnoreCase(stateCode)) {
				if(serverCount==null || serverCount <= 0 || serverName==null || serverName>=serverCount){
					throw new GIException("Invalid Server Count or Server Name");
				}
				servName = serverName;
				servCount = serverCount;
			} 
			
			partitionerRenewalApplicationIdList = autoEnrollService.getAutoRenewalApplIdListByServerName(autoEnrollPartitionerParams.getProcessErrorRecordFlag().get(), 
					renewalYear, servCount, servName, batchSize);
			
			autoEnrollPartitionerParams.setServerName(servName);
			
			partitionMap = new HashMap<String, ExecutionContext>();
			
			if (CollectionUtils.isNotEmpty(partitionerRenewalApplicationIdList)) {

				LOGGER.info("Number of Renewal Application-ID to generate Auto Enroll: {}", partitionerRenewalApplicationIdList.size());
				autoEnrollPartitionerParams.clearRenewalApplicationIdList();
				autoEnrollPartitionerParams.addAllToRenewalApplicationIdList(partitionerRenewalApplicationIdList);

				int maxRenewalApplicationsCommitInterval = 1;
				int size = partitionerRenewalApplicationIdList.size();

				if (StringUtils.isNumeric(this.autoEnrollCommitInterval)) {
					maxRenewalApplicationsCommitInterval = Integer.valueOf(this.autoEnrollCommitInterval.trim());
				}

				int numberOfRenewalApplicationIdToCommit = size / maxRenewalApplicationsCommitInterval;
				if (size % maxRenewalApplicationsCommitInterval != 0) {
					numberOfRenewalApplicationIdToCommit++;
				}

				int firstIndex = 0;
				int lastIndex = 0;

				for (int i = 0; i < numberOfRenewalApplicationIdToCommit; i++) {
					firstIndex = i * maxRenewalApplicationsCommitInterval;
					lastIndex = (i + 1) * maxRenewalApplicationsCommitInterval;

					if (lastIndex > size) {
						lastIndex = size;
					}
					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition", i);
					partitionMap.put("partition - " + i, value);
				}
			}
			else {
				String message = "Renewal Application data is not found to generate Auto Enrolls.";
				Integer giMonitorId = autoEnrollService.logToGIMonitor(message,"RENEWALBATCH_50011");
				LOGGER.info("Message: {} - MonitorId: {}", message, giMonitorId);
			}
		}
		catch (Exception ex) {
			errorMessage.append("AutoEnrollPartitioner failed to execute : ").append(ex.getMessage());
			LOGGER.error(errorMessage.toString(), ex);
		}
		finally {

			if (0 < errorMessage.length()) {
				autoEnrollService.saveAndThrowsErrorLog(errorMessage.toString());
			}
		}
		return partitionMap;
	}

	private boolean validateParams(StringBuffer errorMessage) {

		boolean hasValidParams = true;

		if (null == batchSize || 0l == batchSize) {
			errorMessage.append("Invalid Batch Size: ");
			errorMessage.append(batchSize);
			hasValidParams = false;
		}

		if (null == renewalYear || 0l == renewalYear) {

			if (!hasValidParams) {
				errorMessage.append(", ");
			}
			errorMessage.append("Invalid Current Coverage Year: ");
			errorMessage.append(renewalYear);
			hasValidParams = false;
		}

		if (!hasValidParams) {
			LOGGER.error(errorMessage.toString());
		}
		return hasValidParams;
	}

	/**
	 * Method is used to get Running Batch List.
	 */
	private boolean hasRunningBatchSizeOne() {

		boolean hasRunningBatchSizeOne = false;

		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob(AutoEnrollService.AUTO_ENROLL_JOB_NAME);
		if (batchExecutionList != null && batchExecutionList.size() == 1) {
			hasRunningBatchSizeOne = true;
		}
		return hasRunningBatchSizeOne;
	}

	public Long getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(Long batchSize) {
		this.batchSize = batchSize;
	}

	public String getProcessErrorRecords() {
		return processErrorRecords;
	}

	public void setProcessErrorRecords(String processErrorRecords) {
		this.processErrorRecords = processErrorRecords;
	}

	public String getAutoEnrollCommitInterval() {
		return autoEnrollCommitInterval;
	}

	public void setAutoEnrollCommitInterval(String autoEnrollCommitInterval) {
		this.autoEnrollCommitInterval = autoEnrollCommitInterval;
	}

	public AutoEnrollService getAutoEnrollService() {
		return autoEnrollService;
	}

	public void setAutoEnrollService(AutoEnrollService autoEnrollService) {
		this.autoEnrollService = autoEnrollService;
	}

	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	public AutoEnrollPartitionerParams getAutoEnrollPartitionerParams() {
		return autoEnrollPartitionerParams;
	}

	public void setAutoEnrollPartitionerParams(AutoEnrollPartitionerParams autoEnrollPartitionerParams) {
		this.autoEnrollPartitionerParams = autoEnrollPartitionerParams;
	}

	public Integer getServerCount() {
		return serverCount;
	}

	public void setServerCount(Integer serverCount) {
		this.serverCount = serverCount;
	}

	public Integer getServerName() {
		return serverName;
	}

	public void setServerName(Integer serverName) {
		this.serverName = serverName;
	}
	
	
}
