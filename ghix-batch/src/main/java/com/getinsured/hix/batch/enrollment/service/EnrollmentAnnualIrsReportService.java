/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public interface EnrollmentAnnualIrsReportService {
	
	/**
	 * Get enrollments which were effectuated atleast once in the year passed as input
	 * @param year
	 * @return List of enrollment Ids
	 * @throws GIException
	 */
	List<Integer> getUniqueEnrollmentIds(int year) throws GIException;
	
	/**
	 * Returns unique household ids, enrollments for which were effectuated atleast once in the year passed as input
	 * @author negi_s
	 * @since 20-08-2015
	 * @return List<String> unique household ids
	 */
	List<String> getUniqueHouseHolds(int year) throws GIException;

	/**
	 * Process enrollment for the annual IRS reporting
	 * @param enrollmentId
	 * @param applicableYear
	 * @param isFreshRun 
	 * @param failedHouseholdMap
	 * @return EnrollmentAnnualIrsPolicyDTO
	 * @throws GIException 
	 */
	Enrollment1095 processAnnualIrsEnrollment(Integer enrollmentId, Boolean isFreshRun, Map<String, String> failedEnrollmentMap) throws GIException;

	/**
	 * 
	 * @param applicableYear
	 * @return
	 */
	List<Integer> getFreshEnrollment1095IdsFromStaging(int applicableYear);

	/**
	 * 
	 * @param applicableYear
	 * @return
	 */
	boolean isFreshRunForYear(int applicableYear);
	
	/**
	 * 
	 * @param lastJobRunDate
	 * @param applicableYear
	 * @return
	 */
	List<Integer> getEnrollmentIdsUpdatedAfterDate(Date lastJobRunDate, int applicableYear);
	
	/**
	 * Save to staging table
	 * @param enrollment1095
	 * @throws GIException 
	 */
	void saveToEnrollmentStagingTable(Enrollment1095 enrollment1095) throws GIException;

	/**
	 * Send list for processing
	 * @param enrollment1095List
	 * @param applicableYear
	 * @param partition
	 * @throws GIException 
	 */
	void sendEnrollment1095sForProcessing(List<Integer> enrollment1095List, int applicableYear, int partition) throws GIException;
	
	/**
	 * Get list of years for which fresh XML needs to be extracted
	 * @return List<Integer> count
	 */
	List<Integer> getCoverageYearListForFreshRun();

	/**
	 * Get original batch id list
	 * @return
	 */
	List<String> getOriginalBatchIdListFromStaging();

	/**
	 * get correction list ids
	 * @param batchId
	 * @param batchCategoryCode
	 * @return
	 */
	List<Integer> getCorrectionEnrollment1095ByBatchIdAndCategoryCode(String batchId, String batchCategoryCode);
	
	/**
	 * Get Coverage Year for batch Id
	 * @param batchId
	 * @return
	 */
	Integer getCoverageYearOfOriginalBatchId(String batchId);
	
	/**
	 * Get Coverage Year for batch Id
	 * @param jobName
	 * @return List of BatchJobExecution of running job
	 */
	List<BatchJobExecution> getRunningBatchList(String jobName);

	/**
	 * Get void enrollments by original batch id (First void submission)
	 * @param batchId
	 * @return List<Integer>
	 */
	List<Integer> getInitialVoidEnrollment1095ByBatchId(String batchId);
	
	/**
	 * Get void enrollments by original batch id and batchCategoryCode
	 * @param batchId
	 * @param batchCategoryCodeList
	 * @return List<Integer> 
	 */
	List<Integer> getVoidEnrollment1095ByBatchIdAndCategoryCode(String batchId,	List<String> batchCategoryCodeList);
	
	/**
	 * Set void indicator for cancelled enrollments
	 * @return boolean flag
	 */
	boolean setVoidIndicatorForCancelledEnrollment();
	
	/**
	 * Get correction list from staging table IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ
	 * @param batchId
	 * @param coverageYear
	 * @param batchCategoryCodeList
	 * @param resubmitFlag 
	 * @return List<Integer>
	 */
	List<Integer> getCorrectionListFromStaging(String batchId, Integer coverageYear, List<String> batchCategoryCodeList, Boolean resubmitFlag);
	
	/**
	 * Get records marked for regeneration 
	 * @param batchId
	 * @param batchCategoryCode
	 * @return List<Integer>
	 */
	List<Integer> getRegenerateEnrollment1095ByBatchIdAndCategoryCode(String batchId, String batchCategoryCode);
	
	
	void validateEnrollment1095Records();

}
