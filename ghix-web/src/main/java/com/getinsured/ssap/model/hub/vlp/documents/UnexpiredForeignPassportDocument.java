package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * Unexpired Foreign Passport Document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class UnexpiredForeignPassportDocument extends DhsDocument
{
  public UnexpiredForeignPassportDocument()
  {
    super.setDocumentType(DocumentType.UNEXPIRED_FOREIGN_PASSPORT);
  }

  @JsonProperty("I94Number")
  private String i94Number;

  @JsonProperty("PassportNumber")
  private String passportNumber;

  @JsonProperty("CountryOfIssuance")
  private String countryOfIssuance;

  @JsonProperty("SEVISID")
  private String sevisId;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
    return notBlank(this.passportNumber) &&
            notBlank(this.countryOfIssuance) &&
              this.docExpirationDate != null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final UnexpiredForeignPassportDocument document = new UnexpiredForeignPassportDocument();

    document.setI94Number(validValue(citizenshipDocument.getI94Number()));
    document.setPassportNumber(validValue(citizenshipDocument.getForeignPassportOrDocumentNumber()));
    document.setCountryOfIssuance(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getI94Number()
  {
    return i94Number;
  }

  public void setI94Number(final String i94Number)
  {
    this.i94Number = i94Number;
  }

  public String getPassportNumber()
  {
    return passportNumber;
  }

  public void setPassportNumber(final String passportNumber)
  {
    this.passportNumber = passportNumber;
  }

  public String getCountryOfIssuance()
  {
    return countryOfIssuance;
  }

  public void setCountryOfIssuance(final String countryOfIssuance)
  {
    this.countryOfIssuance = countryOfIssuance;
  }

  public String getSevisId()
  {
    return sevisId;
  }

  public void setSevisId(final String sevisId)
  {
    this.sevisId = sevisId;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", UnexpiredForeignPassportDocument.class.getSimpleName() + "[", "]")
        .add("i94Number='" + i94Number + "'")
        .add("passportNumber='" + passportNumber + "'")
        .add("countryOfIssuance='" + countryOfIssuance + "'")
        .add("sevisId='" + sevisId + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
