<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix='fn' uri='http://java.sun.com/jsp/jstl/functions' %>

<div class="gutter10">
    <div class="row-fluid">
      <div class="span8">
        <h3 id="skip"></h3>
      </div>
	
    </div>
  <div class="row-fluid">
            <div class="span3" id="sidebar">
                
      	</div>
                <div class="span6">
                            
<div id="container" style="width:680px; height: 400px; margin: 0 auto">
<h3 id="skip">Submitting a Paper Application for <c:out value="${exchangeName}"/></h3> <br>
To submit a paper application by mail, please follow these steps:<br><br>
<ol>
<li>Print out the <a href="<c:url value="/resources/sampleTemplates/sample_employer_app.pdf"/>" style="text-decoration:underline;" target="_blank">Employer application.</a> </li>
<li>Print out one copy of the <a href="<c:url value="/resources/sampleTemplates/sample_employee_app.pdf"/>" style="text-decoration:underline;" target="_blank">Employee application</a> for each employee to which you will offer health coverage.   Distribute to your employees.</li>
<li>When the Employer application and all your Employee applications are filled out and signed, put them all into a single envelope and mail to the following address:
<br/>
<c:if test="${statecode == 'MS'}">
 <c:out value="${callcenterAddress1}"/> <br/> <c:out value="${callcenterAddress2}"/><br/>
  <c:out value="${callcenterCityName}"/>, <c:out value="${callcenterStateCode}"/> <c:out value="${callcenterPinCode}"/><br/>
</c:if>

<c:if test="${statecode == 'NM'}">
 <c:out value="${exchangeName}"/><br/>
 <c:out value="${exchangeAddress1}"/> <c:out value="${exchangeAddress2}"/><br/>
  <c:out value="${cityName}"/>, <c:out value="${stateName}"/> <c:out value="${pinCode}"/><br/>
  </c:if>
</li>
<li>Once your application is processed, you will receive an email with further instructions.</li>
</ol>
<br><br>   
</div>      </div><!--span9-->
                        </div> 
     </div> <!-- row-fluid -->
 <!-- gutter -->