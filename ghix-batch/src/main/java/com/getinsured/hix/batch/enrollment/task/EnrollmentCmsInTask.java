package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentCmsOutService;

public class EnrollmentCmsInTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCmsInTask.class);
	private EnrollmentCmsOutService enrollmentCmsOutService;
	private JobService jobService;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		if(null != enrollmentCmsOutService){
			LOGGER.trace(" EnrollmentCmsInTask calling EnrollmentCmsOutService ");
			
			Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			
			enrollmentCmsOutService.processCmsInFile(jobId, jobService);
		}else{
			LOGGER.error(" enrollmentCmsOutService is null ");
			throw new RuntimeException("enrollmentCmsOutService is null") ;
		}
		return RepeatStatus.FINISHED;
	}
	
	public EnrollmentCmsOutService getEnrollmentCmsOutService() {
		return enrollmentCmsOutService;
	}

	public void setEnrollmentCmsOutService( EnrollmentCmsOutService enrollmentCmsOutService ) {
		this.enrollmentCmsOutService = enrollmentCmsOutService;
	}
	
	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

}
