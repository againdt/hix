package com.getinsured.iex.hub.apc.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.apc.cms.dsh.aptc.extension._1.ErrorMessageType;

public class ErrorMessageTypeWrapper  implements JSONAware{

    private ResponseMetadataWrapper responseMetadata;
    private String xPathContent;

    public ErrorMessageTypeWrapper(ErrorMessageType errorMessageType)
	{
    	if(errorMessageType != null)
    	{
	    	xPathContent = errorMessageType.getXPathContent().getValue();
	    	responseMetadata = new ResponseMetadataWrapper(errorMessageType.getResponseMetadata());
    	}
	}
    
    public String getResponseCode()
    {
    	return responseMetadata.getResponseCode();
    }
    
    @SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata", this.responseMetadata);
		obj.put("XPathContent", this.xPathContent);
		return obj.toJSONString();
	}
}
