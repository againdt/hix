package com.getinsured.iex.referral;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

public class ReferralLinkingOverride {

	private boolean isOverride;
	private PrimaryApplicant referralApplicant;
	private PrimaryApplicant householdApplicant;
	private boolean isSameHH;
	
	 public ReferralLinkingOverride() {
	   
	 }
	
	public ReferralLinkingOverride(PrimaryApplicant referralApplicant, PrimaryApplicant householdApplicant) {
		this.referralApplicant = referralApplicant;
		this.householdApplicant = householdApplicant;
		if(householdApplicant.getDateOfBirth() != null && referralApplicant.getDateOfBirth() != null){
			this.isSameHH = referralApplicant.equals(householdApplicant);
			this.isOverride = !referralApplicant.equals(householdApplicant);
		
		}
	}

	public boolean isOverride() {
		return isOverride;
	}

	public PrimaryApplicant getReferralApplicant() {
		return referralApplicant;
	}

	public PrimaryApplicant getHouseholdApplicant() {
		return householdApplicant;
	}

	public void setOverride(boolean isOverride) {
		this.isOverride = isOverride;
	}

	public void setReferralApplicant(PrimaryApplicant referralApplicant) {
		this.referralApplicant = referralApplicant;
	}

	public void setHouseholdApplicant(PrimaryApplicant householdApplicant) {
		this.householdApplicant = householdApplicant;
	}
	
	public boolean isSameHH() {
		return isSameHH;
	}

	public void setSameHH(boolean isSameHH) {
		this.isSameHH = isSameHH;
	}
	
	/* test method */
	/*
	public static void main(String[] args) {
		DateTime dateTime = new DateTime(2000, 04, 18,0,0);
		DateTime dateTime2 = new DateTime(2000, 04, 19,0,0);
		
		PrimaryApplicant referralApplicant = new PrimaryApplicant("John", "Doe", new Date(dateTime.getMillis()), "12345");
		PrimaryApplicant householdApplicant = new PrimaryApplicant("John", "Doe", new Date(dateTime2.getMillis()), "12345");
		ReferralLinkingOverride rlo = new ReferralLinkingOverride(referralApplicant, householdApplicant);
		System.out.println(rlo.isOverride);
		System.out.println(rlo.getHouseholdApplicant());
		System.out.println(rlo.getReferralApplicant());
		
		
		PrimaryApplicant referralApplicant2 = new PrimaryApplicant("John", "Doe", new Date(dateTime.getMillis()), "12345");
		PrimaryApplicant householdApplicant2 = new PrimaryApplicant("John", "Doe", new Date(dateTime.getMillis()), "12345");
		ReferralLinkingOverride rlo2 = new ReferralLinkingOverride(referralApplicant2, householdApplicant2);
		System.out.println(rlo2.isOverride);
		System.out.println(rlo2.getHouseholdApplicant());
		System.out.println(rlo2.getReferralApplicant());
		
		
		PrimaryApplicant referralApplicant3 = new PrimaryApplicant("john", "Doe", new Date(dateTime.getMillis()), "12345");
		PrimaryApplicant householdApplicant3 = new PrimaryApplicant("John", "doe", new Date(dateTime.getMillis()), "12345");
		ReferralLinkingOverride rlo3 = new ReferralLinkingOverride(referralApplicant3, householdApplicant3);
		System.out.println(rlo3.isOverride);
		System.out.println(rlo3.getHouseholdApplicant());
		System.out.println(rlo3.getReferralApplicant());
	}
	*/
	
}
