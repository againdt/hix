package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for middle Name
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class MiddleNameValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MiddleNameValidator.class);
	
	/** 
	 * Validations for middle name
	 * String between 1-50 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String middleName = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			middleName = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(middleName.length() < VLPServiceConstants.MIDDLE_NAME_MIN_LEN
				|| middleName.length() > VLPServiceConstants.MIDDLE_NAME_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.MIDDLE_NAME_LEN_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
