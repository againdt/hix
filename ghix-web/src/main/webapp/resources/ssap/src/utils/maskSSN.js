import React from 'react';

export default value => {
    if (value && value.length !== 9) {
        return <span className="red">Invalid SSN!</span>;
    } else {
        return `***-**-${value && value.slice(5)}`;
    }
};