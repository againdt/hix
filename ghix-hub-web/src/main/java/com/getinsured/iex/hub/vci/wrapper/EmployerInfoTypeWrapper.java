package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.EmployerInfoType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.EmployerInfoType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class EmployerInfoTypeWrapper implements JSONAware{
	
    private OrganizationTypeWrapper employerOrganization;
    private StructuredAddressTypeWrapper employerAddress;
	
    private static final String ORG_KEY = "EmployerOrganization";
    private static final String ADDR_KEY = "EmployerAddress";
    
	public EmployerInfoTypeWrapper(EmployerInfoType employerInfoType){
		
		if(employerInfoType == null){
			return;
		}
		
		this.employerOrganization = new OrganizationTypeWrapper(employerInfoType.getEmployerOrganization());
		this.employerAddress = new StructuredAddressTypeWrapper(employerInfoType.getEmployerAddress());
	}

	public OrganizationTypeWrapper getEmployerOrganization() {
		return employerOrganization;
	}

	public StructuredAddressTypeWrapper getEmployerAddress() {
		return employerAddress;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ORG_KEY, this.employerOrganization);
		obj.put(ADDR_KEY, this.employerAddress);
		return obj.toJSONString();
	}
    
}
