package com.getinsured.hix.batch.referral;

import java.text.SimpleDateFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.hix.batch.referral.service.LceReminderNotice;
import com.getinsured.hix.batch.referral.service.PendingLCEDTO;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class SEPGrantedReminderNotificationTask  extends BaseLCEReminderNotificationTask { 

	
	private final static int DEFAULT_BUFFER_DAYS = 2;
	private final static int STRING_BUILDER_SIZE =50;
	private LceReminderNotice lceReminderNotice;
	
	@Value("#{configProp['database.type']}")
	private String dbType;
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SEPGrantedReminderNotificationTask.class);

	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception 
	{	
		int interval_days = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_RECURRINGNOTICE_SEPGRANTED_FREQUENCY));
		int buffer_days_to_retry = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_RECURRINGNOTICE_SEPGRANTED_BUFFER));

		if (buffer_days_to_retry <= 0) {
			buffer_days_to_retry = DEFAULT_BUFFER_DAYS;
		}

		final List<PendingLCEDTO> records = pendingAppDetailsforLCE(interval_days, buffer_days_to_retry);

		if (records != null) {
			for (PendingLCEDTO pendingLCEDTO : records) {
				LOGGER.debug("Data For PendingLCEDTO" + pendingLCEDTO.getSsapApplicationId());
					checkAndSendNotice(pendingLCEDTO,interval_days, buffer_days_to_retry);
			}
		}
		targetDate = null;
		applicationId = null;
		return RepeatStatus.FINISHED;
	}

	private void checkAndSendNotice(PendingLCEDTO pendingLCEDTO,
			int interval_days, int buffer_days_to_retry) {
		try {
			if(pendingLCEDTO.getApplicationValidationStatus()==ApplicationValidationStatus.PENDING){
				pendingLCEDTO.setNotificationName("Document Required for Life Change Event - EE061");
			}else{
				if ("Y".equalsIgnoreCase(pendingLCEDTO.getKeepOnly())) {
					pendingLCEDTO.setNotificationName("SEP Keep Only Event Notice");
				} else {
					pendingLCEDTO.setNotificationName("SEP Event Notice");
				}
			}

			if (!lceReminderNotice.isNoticeSent(pendingLCEDTO, interval_days,
					buffer_days_to_retry)) {
				LOGGER.debug("Document generation for "
						+ pendingLCEDTO.getSsapApplicationId());
				if(pendingLCEDTO.getApplicationValidationStatus()==ApplicationValidationStatus.PENDING){
					generateSEPNotice(pendingLCEDTO.getCaseNumber(),
							"EE061LCEDocumentRequiredNotice");
				}else{
					if ("Y".equalsIgnoreCase(pendingLCEDTO.getKeepOnly())) {
						generateSEPNotice(pendingLCEDTO.getCaseNumber(),
								"SEPKeepOnlyEvent");
					} else {
	
						generateSEPNotice(pendingLCEDTO.getCaseNumber(), "SEPEvent");
	
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Notification not generated for "
					+ pendingLCEDTO.getCaseNumber() + " " + e);
		}

	}
	private List<PendingLCEDTO> pendingAppDetailsforLCE(int interval_days, int buffer_days_to_retry) throws Exception {
		if(dbType == null || dbType == ""){
			dbType = GhixBatchConstants.DB_TYPE;
		}
		SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String curDate = sDateFormat.format(targetDate);
		StringBuilder query = new StringBuilder(STRING_BUILDER_SIZE);
		
		if("ORACLE".equalsIgnoreCase(dbType)){
			query.append("Select  application.id,application.case_number,events.creation_Timestamp,application.cmr_houseold_id, ");
			query.append(" trunc((to_date('" + curDate + "','dd-mm-yyyy') - to_date(to_char(events.creation_Timestamp,'dd-mm-yyyy'),'dd-mm-yyyy'))/" + interval_days + ",0) ");
			query.append(" ,events.KEEP_ONLY, application.validation_status from ssap_applications  application, ssap_application_events events where application.financial_Assistance_Flag = 'Y' and application.application_Status = 'ER' and application.application_type in ('SEP','QEP') ");
			query.append(" and application.id = events.ssap_application_id and 	application.eligibility_status != 'DE'  and 	events.enrollment_end_date >= to_date('"+curDate+"','dd-mm-yyyy') ");
			query.append(" and (to_date('" + curDate + "','dd-mm-yyyy') - to_date(to_char(events.creation_Timestamp,'dd-mm-yyyy'),'dd-mm-yyyy')) >=  " + interval_days);
			query.append(" and MOD((to_date('" + curDate + "','dd-mm-yyyy') - to_date(to_char(events.creation_Timestamp,'dd-mm-yyyy'),'dd-mm-yyyy'))," + interval_days + ") between 0 and  " + buffer_days_to_retry);
			
		}else if("POSTGRESQL".equalsIgnoreCase(dbType)){
			query.append("Select  application.id,application.case_number,events.creation_Timestamp,application.cmr_houseold_id, ");
			query.append("cast(trunc(extract(day from (to_date('" + curDate + "','dd-mm-yyyy') - events.creation_Timestamp))/" + interval_days +") as numeric) ");
			query.append(" ,events.KEEP_ONLY, application.validation_status from ssap_applications  application, ssap_application_events events where application.financial_Assistance_Flag = 'Y' and application.application_Status = 'ER' and application.application_type in ('SEP','QEP') ");
			query.append(" and application.id = events.ssap_application_id and 	application.eligibility_status != 'DE'  and events.enrollment_end_date >= to_date('"+curDate+"','dd-mm-yyyy') ");
			query.append(" and cast(extract(day from (to_date('" + curDate + "','dd-mm-yyyy') - events.creation_Timestamp)) as int) >=   " + interval_days); 
			query.append(" and mod(cast(extract(day from (to_date('" + curDate + "','dd-mm-yyyy') - events.creation_Timestamp)) as int)," + interval_days + ") between 0 and  " + buffer_days_to_retry);
		}else {
			LOGGER.error("Unknown db type found in configuration.properties: " + dbType );
			throw new GIRuntimeException("Unknown db type found in configuration.properties: " + dbType);
		}
		
		if (applicationId != null) {
			query.append(" and application.id =  ");
			query.append(applicationId.longValue());
		}

		LOGGER.info("Executing Query \n\n " + query);

		return lceReminderNotice.appDetailsForRecurringLCESEPNotice(query.toString());
	}
	private String generateSEPNotice(String caseNumber,String noticetype) throws Exception {
		return lceReminderNotice.generate(caseNumber,noticetype);
	}

	public LceReminderNotice getLceReminderNotice() {
		return lceReminderNotice;
	}

	public void setLceReminderNotice(LceReminderNotice lceReminderNotice) {
		this.lceReminderNotice = lceReminderNotice;
	}
}
