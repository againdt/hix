package com.getinsured.hix.consumer.repository;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.Enrollee;


@Repository
@Transactional
public interface IHouseholdRepository extends JpaRepository<Household, Integer>{

	
	/**
	 * Fetches the Household for a given User
	 * @param userId 
	 * @return Household associated with that user
	 */
	@Query("FROM Household hh where hh.user.id = :userId")
	Household findByUserId(@Param("userId") Integer userId); 
	
	@Query("FROM Household hh where hh.ssn = :ssn")
	Household findBySSN(@Param("ssn") String ssn);
	
	@Query("FROM Household hh where hh.ssn = :ssn")
	List<Household> findHouseholdsBySSN(@Param("ssn") String ssn);
	
	@Query("FROM Household hh where hh.ssn = :ssn and hh.birthDate =  :birthDate")
	Household findBySSNAndBirthDate(@Param("ssn") String ssn,
			@Param("birthDate") Date birthDate);	
	
	@Query("FROM Household hh where hh.email = :email")
	Household findByEmailAddress(@Param("email") String email);
		
	Household findById(Integer Id);
}
