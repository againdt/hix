import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';
import _ from 'lodash';

// ActionCreators

import {
    changePage,
    hideModal,
    scrollToElement,
    setException,
    showModal,
    submitApplication,
    submitForm
} from '../actions/actionCreators';

//Utils

import { getCurrentHouseholdMember } from '../utils/getCurrentHouseholdMember';
import normalizeNameOfPerson from '../utils/normalizeNameOfPerson';

// Constants

import CONFIG from '../.env';
import ENDPOINTS from '../constants/endPointsList';
import EXCEPTIONS from '../constants/exceptionList';
import MODAL_TYPES from '../constants/modalTypes';
import { getButtons } from '../constants/buttonsConfiguration';
import { PAGES } from '../constants/pagesList';



/**
 *
 * @param {Object}props
 * @returns {React.Component} - Button with assigned to it actions on click depending on situation
 *
 */
const ButtonContainer  = props => {

    let {
        // These come from directly from Page or other Component

        args,
        customButtonClassName,
        isDisabled = false,
        type,

        // These come from Redux store

        adjustedFlow,
        contexts,
        data: {
            singleStreamlinedApplication: {
                isRidpVerified,
            }
        },
        exception,
        exception: {
            exceptionType,
            exceptionPages: [ destinationPage, departurePage ]
        },
        form: {
            Form
        },
        householdMembers,
        methods,
        messages,
        page

    } = props;

    const {
        hhmIndex: currentHhmIndex,
        currentHhm,
    } = getCurrentHouseholdMember(adjustedFlow, page, householdMembers);

    const _pp = (pageName, hhmIndex = undefined) => {
        return adjustedFlow.findIndex(page => {
            return hhmIndex !== undefined ?
                page.pageName === pageName && page.hhm === hhmIndex :
                page.pageName === pageName;
        }) ;
    };

    const getActions = () => {

        const {
            changePage,
            hideModal,
            scrollToElement,
            setException,
            showModal
        } = props.actions;

        const exc = (exception => exception ? exception : null)(exceptionType);

        const handleRIDP = () => {
            return () => {
                isRidpVerified ? changePage(_pp(PAGES.SIGN_AND_SUBMIT_PAGE)) :
                    CONFIG.ENV.PRODUCTION ?
                        window.location = `${window.location.origin}${ENDPOINTS.RIDP_ENDPOINT}` :
                        console.log(`RIDP failed, we are redirected to: ${window.location.origin}${ENDPOINTS.RIDP_ENDPOINT}`);
            };
        };

        const handleHouseholdCheck = () => {
            return () => {
                if(householdMembers.every(hhm => !hhm.applyingForCoverageIndicator)) {
                    showModal(
                        MODAL_TYPES.MANDATORY_SEEKING_FOR_COVERAGE_MODAL,
                        MODAL_TYPES.MANDATORY_SEEKING_FOR_COVERAGE_MODAL,
                        [],
                        null
                    );
                } else {
                    const memberList = [];
                    householdMembers.forEach((hhm) => {
                        let age = moment().diff(hhm.dateOfBirth, 'years');
                        if (age >= 65 && hhm.applyingForCoverageIndicator) {
                            memberList.push(normalizeNameOfPerson(hhm.name));
                        }
                    });
                    if(memberList.length > 0) {
                        if(Form.isEligiblyForSave) {
                            showModal(
                                MODAL_TYPES.INFORM_MEDICARE_ELIGIBLE_MODAL,
                                MODAL_TYPES.INFORM_MEDICARE_ELIGIBLE_MODAL,
                                [memberList],
                                [
                                    () => contexts.Form.onFormSubmit('increase'),
                                    () => hideModal(MODAL_TYPES.INFORM_MEDICARE_ELIGIBLE_MODAL)
                                ]
                            );
                        } else {
                            contexts.Form.onFormSubmit('increase');
                        }

                    } else {
                        contexts.Form.onFormSubmit('increase');
                    }
                }
            };
        };

        const handleMultiTaxHouseholds = () => {
            return () => {
                methods.Page2_07_E2_mod1.handleSaveClick.apply(this, [
                    () => contexts.Form.onFormSubmit('increase')
                ]);
            };
        };

        const lawfullyPresentCheck = () => {

            return () => {
                const {
                    citizenshipStatusIndicator,
                    eligibleImmigrationDocumentType
                } = currentHhm.citizenshipImmigrationStatus;
                if(!citizenshipStatusIndicator && !Object.values(eligibleImmigrationDocumentType[0]).some(doc => doc)){
                    showModal(
                        MODAL_TYPES.INFORM_LAWFULLY_PRESENT_REQUIRED_FOR_COVERAGE_MODAL,
                        MODAL_TYPES.INFORM_LAWFULLY_PRESENT_REQUIRED_FOR_COVERAGE_MODAL,
                        [],
                        [
                            () => contexts.Form.onFormSubmit('increase'),
                            () => hideModal(MODAL_TYPES.INFORM_LAWFULLY_PRESENT_REQUIRED_FOR_COVERAGE_MODAL)
                        ],
                    );
                } else {
                    contexts.Form.onFormSubmit('increase');
                }
            };
        };

        // Here we assign needed ActionCreators depending on (checking in order):

        // 1. Type of configuring button
        // 2. Page for which we take button
        // 3. Other condition that should be true

        // Sometimes we do not use ActionCreators, but method from Component via passing needed context

        switch(type) {

        case 'leftButton':
            return () => {
                if(exc && page === destinationPage) {
                    switch(true) {
                    case exc === EXCEPTIONS.EDITING_PRIMARY_HOUSEHOLD_CONTACT:
                    case exc === EXCEPTIONS.EDITING_HOUSEHOLD_MEMBER:
                    case exc === EXCEPTIONS.EDITING_HOUSEHOLD_MEMBER_ADDRESS:
                        contexts.Form.onFormExit([() => changePage(_pp(PAGES.HOUSEHOLD_SUMMARY_PAGE))]);
                        break;

                    case exc === EXCEPTIONS.ADDING_HOUSEHOLD_MEMBER_FROM_HOUSEHOLD_SECTION:
                        contexts.Form.onFormExit([() => changePage(_pp(PAGES.TAX_HOUSEHOLD_COMPOSITION_PAGE))]);
                        break;

                    case exc === EXCEPTIONS.REVIEW_HOUSEHOLD_SECTION:
                        contexts.Form.onFormExit([() => changePage(_pp(PAGES.FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE))]);
                        break;

                    case exc === EXCEPTIONS.EDITING_INCOMES_AND_DEDUCTIONS:
                        contexts.Form.onFormExit([() => changePage(_pp(PAGES.PERSONAL_INCOME_SUMMARY_PAGE, currentHhmIndex))]);
                        break;

                    case exc === EXCEPTIONS.REVIEW_INCOME_SECTION:
                        contexts.Form.onFormExit([() => changePage(_pp(PAGES.INCOME_INFORMATION_SUMMARY_PAGE))]);
                        break;

                    case exc === EXCEPTIONS.EDITING_ADDITIONAL_INFORMATION:
                        contexts.Form.onFormExit([() => changePage(_pp(PAGES.ADDITIONAL_INFORMATION_SUMMARY_PAGE))]);
                        break;

                    case exc === EXCEPTIONS.FIXING_TAX_RELATIONSHIP:
                        contexts.Form.onFormExit([() => changePage(_pp(PAGES.TAX_HOUSEHOLD_COMPOSITION_PAGE))]);
                        break;

                    case exc === EXCEPTIONS.REVIEW_BEFORE_SUBMIT:
                        contexts.Form.onFormExit([() => changePage(_pp(PAGES.FINAL_APPLICATION_REVIEW_PAGE))]);
                        break;

                    case exc === EXCEPTIONS.MANAGE_SPOUSE:
                        contexts.Form.onFormExit([() => changePage(departurePage)]);
                    }
                } else {
                    if(contexts.Form) {
                        contexts.Form.onFormExit([() => changePage('decrease')]);
                    } else {
                        changePage('decrease');
                    }
                }
            };

        case 'middleButton':
            switch(page) {
            default: return () => console.log('Button\'s actions haven\'t got because of wrong PAGE!');
            }

        case 'rightButton':
            switch(page) {
            case _pp(PAGES.FINAL_APPLICATION_REVIEW_PAGE):
                return handleRIDP();

            case _pp(PAGES.SIGN_AND_SUBMIT_PAGE):
                return () => methods.Page3_17.handleSubmit.apply(this, args);

            case _pp(PAGES.ABOUT_YOUR_HOUSEHOLD_PAGE):
                return handleHouseholdCheck();

            case _pp(PAGES.CITIZENSHIP_IMMIGRATION_STATUS_PAGE, currentHhmIndex):
                return lawfullyPresentCheck();

            case _pp(PAGES.TAX_HOUSEHOLD_COMPOSITION_PAGE):
                return handleMultiTaxHouseholds();

            default:
                return () => {
                    switch(true) {
                    case exc === EXCEPTIONS.EDITING_PRIMARY_HOUSEHOLD_CONTACT:
                        contexts.Form.onFormSubmit('increase');
                        break;

                    case exc === EXCEPTIONS.REVIEW_HOUSEHOLD_SECTION && _pp(PAGES.ETHNICITY_AND_RACE_PAGE) === page:
                        contexts.Form.onFormSubmit(_pp(PAGES.AMERICAN_INDIAN_ALASKA_NATIVE_PAGE));
                        break;

                    default:
                        if(contexts.Form) {
                            contexts.Form.onFormSubmit('increase');
                        } else {
                            changePage('increase');
                        }
                    }
                };
            }

        case 'editButton_secondary':

            switch(page) {

            case _pp(PAGES.HOUSEHOLD_SUMMARY_PAGE):
                return () => {
                    const [ type, hhmIndex, payload ] = [...args];
                    switch(type){
                    case 'editMember':
                        scrollToElement.apply(this, [payload]);

                        setException(
                            EXCEPTIONS.EDITING_HOUSEHOLD_MEMBER,
                            [ _pp(PAGES.ABOUT_YOUR_HOUSEHOLD_PAGE) ],
                            true
                        );
                        break;
                    case 'editAddress':

                        setException(
                            hhmIndex === 0 ?
                                EXCEPTIONS.EDITING_PRIMARY_HOUSEHOLD_CONTACT :
                                EXCEPTIONS.EDITING_HOUSEHOLD_MEMBER_ADDRESS,
                            [
                                hhmIndex === 0 ?
                                    _pp(PAGES.PRIMARY_CONTACT_INFORMATION_PAGE) :
                                    _pp(PAGES.OTHER_ADDRESSES_PAGE)
                            ],
                            true
                        );
                    }
                };


            case _pp(PAGES.FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE):
                return () => {
                    const [
                        hhmIndex
                    ] = args;

                    setException(
                        EXCEPTIONS.REVIEW_HOUSEHOLD_SECTION,
                        [ _pp(PAGES.PERSONAL_INFORMATION_PAGE, hhmIndex) ],
                        _pp(PAGES.PERSONAL_INFORMATION_PAGE, hhmIndex)
                    );
                };

            case _pp(PAGES.FINAL_APPLICATION_REVIEW_PAGE):
                return () => {
                    const [
                        hhmIndex,
                        destinationPage
                    ] = args;

                    setException(
                        EXCEPTIONS.REVIEW_BEFORE_SUBMIT,
                        [ _pp(destinationPage, hhmIndex) ],
                        _pp(destinationPage, hhmIndex)
                    );
                };

            case _pp(PAGES.SIGN_AND_SUBMIT_PAGE):
                return () => methods.Page3_17.handleEditClick.apply(this, args);

            case _pp(PAGES.PERSONAL_INCOME_SUMMARY_PAGE, currentHhmIndex):
                return () => {
                    const [
                        hhmIndex
                    ] = args;

                    setException(
                        EXCEPTIONS.EDITING_INCOMES_AND_DEDUCTIONS,
                        [ _pp(PAGES.INCOME_SOURCES_PAGE, hhmIndex) ],
                        _pp(PAGES.INCOME_SOURCES_PAGE, hhmIndex)
                    );
                };

            case _pp(PAGES.INCOME_INFORMATION_SUMMARY_PAGE):
                return () => {
                    const [
                        hhmIndex
                    ] = args;

                    setException(
                        EXCEPTIONS.REVIEW_INCOME_SECTION,
                        [ _pp(PAGES.INCOME_SOURCES_PAGE, hhmIndex) ],
                        _pp(PAGES.INCOME_SOURCES_PAGE, hhmIndex)
                    );
                };

            case _pp(PAGES.ADDITIONAL_INFORMATION_SUMMARY_PAGE):
                return () => {
                    const [
                        hhmIndex
                    ] = args;

                    setException(
                        EXCEPTIONS.EDITING_ADDITIONAL_INFORMATION,
                        [ _pp(PAGES.OTHER_HEALTH_COVERAGE_PAGE, hhmIndex) ],
                        _pp(PAGES.OTHER_HEALTH_COVERAGE_PAGE, hhmIndex)
                    );
                };

            case _pp(PAGES.INCOME_SOURCES_PAGE, currentHhmIndex):
                return () => methods.Page3_32_E1.handleIncomeSourceEditing.apply(this, args);

            case _pp(PAGES.DEDUCTION_SOURCES_PAGE, currentHhmIndex):
                return () => methods.Page3_34_E1.handleDeductionSourceEditing.apply(this, args);
            }
            break;

        case 'deleteButton':
            switch(page) {
            case _pp(PAGES.ABOUT_YOUR_HOUSEHOLD_PAGE):
                return () => methods.Page1_05.handleMemberDeletion.apply(this, args);
            }
            break;

        case 'addEmployerCoverageDetailButton':
            return () => methods.Page4_63.handleAddEmployerDetailsClick.apply(this, args);

        case 'addIncomeSourceButton':
            return () => methods.Page3_32_E1.handleAddIncomeSourceClick.apply(this, args);

        case 'addDeductionSourceButton':
            return () => methods.Page3_34_E1.handleAddDeductionSourceClick.apply(this, args);

        case 'removeButton':
            switch(page) {
            case _pp(PAGES.INCOME_SOURCES_PAGE, currentHhmIndex):
                return () => methods.Page3_32_E1.handleIncomeSourceDeletion.apply(this, args);
            case _pp(PAGES.DEDUCTION_SOURCES_PAGE, currentHhmIndex):
                return () => methods.Page3_34_E1.handleDeductionSourceDeletion.apply(this, args);
            } break;

        case 'addPersonButton':
            switch(page) {
            case _pp(PAGES.ABOUT_YOUR_HOUSEHOLD_PAGE):
                return () => methods.Page1_05.handleAddingNewPerson.apply(this, args);

            case _pp(PAGES.TAX_HOUSEHOLD_COMPOSITION_PAGE, currentHhmIndex):
                return () => {
                    setException(
                        EXCEPTIONS.ADDING_HOUSEHOLD_MEMBER_FROM_HOUSEHOLD_SECTION,
                        [ _pp(PAGES.ABOUT_YOUR_HOUSEHOLD_PAGE) ],
                        true
                    );
                };
            } break;

        case 'fixHouseholdMemberButton':
            return () => {
                setException(
                    EXCEPTIONS.ADDING_HOUSEHOLD_MEMBER_FROM_HOUSEHOLD_SECTION,
                    [ _pp(PAGES.ABOUT_YOUR_HOUSEHOLD_PAGE) ],
                    true
                );
            };

        case 'fixRelationshipButton':
            return () => {
                setException(
                    EXCEPTIONS.MANAGE_SPOUSE,
                    [ _pp(PAGES.RELATIONSHIPS_PAGE), _pp(PAGES.MARITAL_STATUS, currentHhmIndex) ],
                    true
                );
            };

        case 'updateInformationButton':
            switch(page) {
            case _pp(PAGES.TAX_HOUSEHOLD_COMPOSITION_PAGE):
                return () => {
                    setException(
                        EXCEPTIONS.FIXING_TAX_RELATIONSHIP,
                        [ _pp(PAGES.ABOUT_YOUR_HOUSEHOLD_PAGE) ],
                        true
                    );
                };
            case _pp(PAGES.MARITAL_STATUS, currentHhmIndex):
                return () => {
                    setException(
                        EXCEPTIONS.MANAGE_SPOUSE,
                        [ _pp(PAGES.ABOUT_YOUR_HOUSEHOLD_PAGE), _pp(PAGES.MARITAL_STATUS, currentHhmIndex) ],
                        true
                    );
                };
            } break;

        case 'printButton':
            return () => methods.Page3_16.handlePrint.apply(this, args);

        case 'downloadButton':
            return () => methods.Page3_16.handleDownload.apply(this, args);

        default: return () => console.log('Buttons actions haven\'t got because of wrong TYPE!');
        }
    };

    const {
        isButton,
        className,
        isShown,
        i18n
    } = getButtons(exception, page, type, adjustedFlow);

    let uniqueId = _.uniqueId('id');

    return (
        <Fragment>
            {
                isButton &&

                    <Button
                        className={`${className} ${customButtonClassName}`}
                        disabled={ isDisabled }
                        id={`Page_${page}_${type}_${uniqueId}`}
                        name={ type }
                        onClick={ getActions() }
                        style={{ display: isShown ? '' : 'none' }}
                    >
                        { messages ? <FormattedMessage id={i18n} /> : i18n }
                    </Button>
            }
        </Fragment>
    );
};

const mapStateToProps = state => ({
    adjustedFlow: state.adjustedFlow,
    contexts: state.contexts,
    data: state.data,
    exception: state.exception,
    flow: state.flow,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    methods: state.methods,
    messages: state.messages,
    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changePage,
        hideModal,
        scrollToElement,
        setException,
        showModal,
        submitApplication,
        submitForm
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ButtonContainer);
