package com.getinsured.hix.batch.provider.processors.ca;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailFormatProcessor implements ProviderDataFieldProcessor{

	private ValidationContext context;
	private int index;
	
	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

	@Override
	public Object process(Object objToBeValidated) throws ProviderValidationException, InvalidOperationException {
		String maxObj = context.getNamedConstraintField("max");
		if (maxObj == null) {
			throw new ProviderValidationException("Email max length not set. Please check your metadata file.");
		}

		int max = Integer.parseInt(maxObj);
		String email = (String) objToBeValidated;
		if (email.length() > max) {
			throw new ProviderValidationException("Email validation failed. Email is longer than accepted.");
		}

		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher matcher =  pattern.matcher(email);
		if (!matcher.matches()) {
			throw new ProviderValidationException("Email validation failed. Pattern mismatch.");
		}
		return email;
	}

}
