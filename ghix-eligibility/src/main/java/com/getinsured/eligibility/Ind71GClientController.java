package com.getinsured.eligibility;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.resp.service.SsapApplicantService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.ind71G.client.Ind71GClientAdapter;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.indportal.dto.AppGroupRequest;
import com.getinsured.hix.indportal.dto.AppGroupResponse;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.client.SsapApplicationAccessor;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;

@RestController
@Validated
@RequestMapping("/ind71g")
public class Ind71GClientController{
	private static Logger lOGGER = Logger.getLogger(Ind71GClientController.class);
	
	@Autowired
	@Qualifier("ind71GClientAdapter")
	private Ind71GClientAdapter ind71GClientAdapter;
	@Autowired private SsapApplicationRepository SsapApplication;
	@Autowired private SsapApplicantService ssapApplicantService;
	@Autowired private SsapApplicationAccessor ssapApplicationAccessor;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private CoverageStartDateService coverageStartDateService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private Gson platformGson;
	@Autowired private SsapEnrolleeHandler ssapEnrolleeHandler;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	
	@RequestMapping(value = "/processRequest", method = RequestMethod.GET)
    public String processAutoUpdateRequest(HttpServletRequest request){
		lOGGER.info("Process auto update request starts for application : " + request.getParameter("applicationId") );
		Long applicationId = Long.valueOf(request.getParameter("applicationId").trim());
		String ind71GResponse = "";
		try {
			SsapApplication ssapApplication = SsapApplication.findById(applicationId);
			CompareMainDTO compareMainDTO = populateCompareMainDTO(ssapApplication);
			Long enrolledApplicationId = compareMainDTO.getEnrolledApplication().getId();
			if(enrolledApplicationId == null){
				lOGGER.info("Previous application not found for - " + applicationId);
				ind71GResponse = "Previous application not found";
				return ind71GResponse;
			}
			
			Timestamp covgDate = null;
			covgDate = coverageStartDateService.computeCoverageStartDateIncludingTermMembers(new BigDecimal(applicationId), false, false, enrolledApplicationId);
			AptcUpdate aptcUpdate = this.populateAptcUpdateRequest(ssapApplication, enrolledApplicationId, covgDate);
			
			SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
			String enrollmentResponseStr = getEnrollmentsByMember(enrolledApplication.getSsapApplicants(),String.valueOf(ssapApplication.getCoverageYear()));
			if(StringUtils.isNotBlank(enrollmentResponseStr)) {
				EnrollmentResponse enrollmentResponse = platformGson.fromJson(enrollmentResponseStr, EnrollmentResponse.class);
				if(enrollmentResponse != null) {
					ind71GResponse = executeIND71G(applicationId,aptcUpdate,enrollmentResponse,null);
				}else {
					lOGGER.info("Member enrollment mapping call failure - " + applicationId);
					ind71GResponse = "Member enrollment mapping call failure - " + applicationId;
				}
			}else {
				lOGGER.info("Member enrollment mapping call failure - " + applicationId);
				ind71GResponse = "Member enrollment mapping call failure - " + applicationId;
			}
			return ind71GResponse;
		} catch (Exception e) {
			lOGGER.info("Exception occured during ind71g process request "+e.getMessage());
			ind71GResponse = "Exception occured during ind71g process request "+e.getMessage();
		}
		
		return ind71GResponse;
	}
	
	
	private String executeIND71G(long ssapApplicationId, AptcUpdate aptcUpdate, EnrollmentResponse enrollmentResponse,String isChangePlan) {
		String ind71GResponse = GhixConstants.RESPONSE_FAILURE;
		try {
			ind71GResponse = ind71GClientAdapter.execute(ssapApplicationId, aptcUpdate, enrollmentResponse,isChangePlan);
		} catch (Exception e) {
			lOGGER.info("Error during ind71G execution" + e.getMessage());
			return ind71GResponse; 
		}
		return ind71GResponse;
	}
	
	private CompareMainDTO populateCompareMainDTO(SsapApplication ssapApplication) {
		CompareMainDTO compareMainDTO = new CompareMainDTO();
		String externalApplicantId = null;

		compareMainDTO.setEnrolledApplication(new CompareApplicationDTO());
		List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(ssapApplication);

		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (ssapApplicant.getPersonId() == ReferralConstants.ONE) {
				externalApplicantId = ssapApplicant.getExternalApplicantId();
				break;
			}
		}
		List<SsapApplication> ssapApplications = ssapApplicantService.findSsapApplicationForExternalApplicantId(externalApplicantId);
		for (SsapApplication ssapApplicationTemp : ssapApplications) {
			if (ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplicationTemp.getApplicationStatus())
			 || ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode().equalsIgnoreCase(ssapApplicationTemp.getApplicationStatus()) ){
				compareMainDTO.getEnrolledApplication().setId(ssapApplicationTemp.getId());
				List<SsapApplicant> ssapApplicantLists = ssapApplicantRepository.findBySsapApplication(ssapApplicationTemp);
				
				for (SsapApplicant ssapApplicant : ssapApplicantLists) {
					CompareApplicantDTO compareApplicantDTO = new CompareApplicantDTO();
					compareApplicantDTO.setApplicantGuid(ssapApplicant.getApplicantGuid());
					compareMainDTO.getEnrolledApplication().getApplicants().add(compareApplicantDTO);
				}
				break;
			}else if(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(ssapApplicationTemp.getApplicationStatus())){
				if(this.isActiveEnrollmentByAppId(ssapApplicationTemp.getId())){
					compareMainDTO.getEnrolledApplication().setId(ssapApplicationTemp.getId());
					List<SsapApplicant> ssapApplicantLists = ssapApplicantRepository.findBySsapApplication(ssapApplicationTemp);
					
					for (SsapApplicant ssapApplicant : ssapApplicantLists) {
						CompareApplicantDTO compareApplicantDTO = new CompareApplicantDTO();
						compareApplicantDTO.setApplicantGuid(ssapApplicant.getApplicantGuid());
						compareMainDTO.getEnrolledApplication().getApplicants().add(compareApplicantDTO);
					}
					break;
				}
			}
		}
		return compareMainDTO;
	}
	
	public AptcUpdate populateAptcUpdateRequest(SsapApplication currentApplication, long enrolledApplicationId, Date finEffectiveDate) {
		AptcUpdate aptcUpdate = new AptcUpdate();
		String appGroupResponseJson = null;
		AppGroupResponse appGroupResponse = null;
		try {
			AppGroupRequest appGroupRequest = new AppGroupRequest();
        	appGroupRequest.setApplicationId(currentApplication.getId());
        	
        	if(finEffectiveDate == null) {
        		throw new GIRuntimeException("APTC redistribution financial Effective date can not be null : "+ currentApplication.getId());
			}
        	
        	getUpdatedDateBasedonEnrollment(enrolledApplicationId, finEffectiveDate, aptcUpdate);
        	if(aptcUpdate.getAptcEffectiveDate()!=null) {
        		finEffectiveDate = aptcUpdate.getAptcEffectiveDate();
        	}
        	String coverageStartDate = new SimpleDateFormat("MM/dd/yyyy").format(finEffectiveDate);
        	
        	appGroupRequest.setCoverageStartDate(coverageStartDate);
        	
        	try{
        		/*
        		 * this had to be changed as when called from web the logged in user is lost
        		 * this is done same as when called from LCE auto flow we pass exadmin user   
        		 */
        		appGroupResponseJson = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+ "eligibility/api/getAppGroups/", ReferralConstants.EXADMIN_USERNAME, 
    					HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, appGroupRequest).getBody();
    			aptcUpdate.setAptcEffectiveDate(finEffectiveDate);
    		}catch(Exception e){
    			lOGGER.error("Exception occured for calling getAppGroups api",e);
    			throw new GIRuntimeException("Error occoured while getting aptc redistribution: " + appGroupResponseJson);
    		}
        	try{
        		appGroupResponse = platformGson.fromJson(appGroupResponseJson, AppGroupResponse.class);
        		aptcUpdate.setAppGroupResponse(appGroupResponse);
    		}catch(Exception e){
    			throw new GIRuntimeException("Invalid response in APTC redistribution for application id : " + currentApplication.getId());
    		}
    	} catch (Exception e) {
    		lOGGER.info("Error occured during AptcUpdate " + e.getMessage());
		}
		return aptcUpdate;
	}
	
	private void getUpdatedDateBasedonEnrollment(long enrolledApplicationId, Date finEffectiveDate, AptcUpdate aptcUpdate) {
		final Map<String, List<EnrollmentShopDTO>> enrollmentDetails = ssapEnrolleeHandler.fetchMultipleEnrollmentID(enrolledApplicationId);
		
		List<EnrollmentShopDTO> healthList = enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO);
		if(enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO)!=null) {
			healthList.addAll(enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO));
		}
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if(healthList!=null) {
				for(EnrollmentShopDTO health : healthList) {
					String healthEnrollmentStartDate = df.format(health.getCoverageStartDate());
					if(StringUtils.isNotEmpty(healthEnrollmentStartDate) && isAptcStartDateBeforeEnrollmentStartDate(finEffectiveDate, healthEnrollmentStartDate)){
						finEffectiveDate = df.parse(healthEnrollmentStartDate);
					}
				}
			}
			aptcUpdate.setAptcEffectiveDate(finEffectiveDate);
			aptcUpdate.setEnrollmentList(healthList);
		}catch (Exception e) {
			lOGGER.error("Exception occured while parsing new financial effective date and enrollment start date",e);
			throw new GIRuntimeException("Exception occured while parsing new financial effective date and enrollment start date: " + enrolledApplicationId );
		}
	}
	
	public boolean isAptcStartDateBeforeEnrollmentStartDate(Date aptcEffectiveDate, String enrollmentEffectiveStartDate) throws ParseException{
		boolean isAptcStartDateBeforeEnrollmentStartDate = false;
		
		SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
		LocalDate aptcStartDate = new DateTime(aptcEffectiveDate.getTime()).toLocalDate();
		LocalDate enrollmentStartDate = new DateTime(mmddyyyy.parseObject(enrollmentEffectiveStartDate)).toLocalDate();
		if(aptcStartDate.isBefore(enrollmentStartDate)){
			isAptcStartDateBeforeEnrollmentStartDate = true;
		}
		return isAptcStartDateBeforeEnrollmentStartDate;
	}
	
	public SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}
	
	public String getEnrollmentsByMember(List<SsapApplicant> ssapApplicants, String coverageYear) {
		List<String> applicantGuidList = new ArrayList<String>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidList.add(ssapApplicant.getApplicantGuid());
		}
		
		List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
		enrollmentStatusList.add(EnrollmentStatus.PENDING);
		enrollmentStatusList.add(EnrollmentStatus.CONFIRM);
		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setMemberIdList(applicantGuidList);
		enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
		
		List<String> coverageYearList = new ArrayList<String>();
		coverageYearList.add(coverageYear);
		enrollmentRequest.setCoverageYearList(coverageYearList);
		String enrollmentResponseStr = ghixRestTemplate.exchange(EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_MEMBERID_LIST, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
	
		return enrollmentResponseStr;
	}
	
	private boolean isActiveEnrollmentByAppId(Long enrolledApplicationId){
		Map<String,Integer> enrollmentCountMap = new HashMap<String,Integer>(5);
		EnrollmentResponse enrollmentResponse = null;
		String jsonResponse = "";
		try{
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setSsapApplicationId(enrolledApplicationId);	
			jsonResponse = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID, 
					"",HttpMethod.POST, 
					MediaType.APPLICATION_JSON, 
					String.class, platformGson.toJson(enrollmentRequest))).getBody();
			if(lOGGER.isInfoEnabled()){
				lOGGER.info("*****************************************Received response from enrollemnt GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID" + jsonResponse);
			}
			enrollmentResponse = platformGson.fromJson(jsonResponse, EnrollmentResponse.class);
			if(enrollmentResponse != null){
				if(StringUtils.equalsIgnoreCase(enrollmentResponse.getStatus(),GhixConstants.RESPONSE_SUCCESS)){
					if(enrollmentResponse.getEnrollmentCountMap() != null){
						enrollmentCountMap = enrollmentResponse.getEnrollmentCountMap();
					}
				}
				else{
					lOGGER.error("Received Error Response from enrollemnt GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID, Error Code:" + enrollmentResponse.getErrCode()+", Error Message:"+enrollmentResponse.getErrMsg());
				}
			}
		}catch(Exception e){
			lOGGER.error("Exception occured for calling GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID api"+ ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException("Error occoured while getting health and dental enrollment count " + jsonResponse);
		}

		boolean flag = false;
		if(enrollmentCountMap != null && enrollmentCountMap.size()>0){
			int healthCount = enrollmentCountMap.get(ReferralConstants.HEALTH) != null ? enrollmentCountMap.get(ReferralConstants.HEALTH) : 0;
			int dentalCount = enrollmentCountMap.get(ReferralConstants.DENTAL) != null ? enrollmentCountMap.get(ReferralConstants.DENTAL) : 0;
			if(healthCount > 0 ||  dentalCount > 0){
				flag = true;
			}
		}	
	return flag;
		
	}
	
}
	