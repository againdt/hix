<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="row">
	<div class="span4">
		<h2>User Administration</h2>
		<p></p>
	</div>
	<div class="span12">
	<form class="form-stacked" id= "frmcontact" name="frmcontact" action="edit"  method="post">
			<df:csrfToken/>
			<fieldset>
				<legend>Edit Contact Information</legend>
				<div class="">
					<div class="clearfix">
						<label for="firstName" class="required">First Name</label>
						<div class="input">
							<input type="text" name="firstName" id="firstName" value="admin" class="xlarge" size="60">
						</div>					
					</div>
					<div class="clearfix">
						<label for="lastName" class="required">Last Name</label>
						<div class="input">
							<input type="text" name="lastName" id="lastName" value="admin" class="xlarge" size="60">
						</div>					
					</div>
					<div class="clearfix">
						<label for="title" class="required">Title or Designation <a href="#" data-original-title="Title" data-content="What is your title or designation." rel="popover"><span	class="label">?</span> </a></label>
						<div class="input">
							<input type="text" name="title" id="title" value="Admin" class="xlarge">
						</div>					
					</div>
					<div class="clearfix">
						<label for="email" class="required">Email Address <a href="#" data-original-title="Email Address" data-content="This email address will be used for logging into your account." rel="popover"><span class="label">?</span> </a></label>
						<div class="input">
							<input type="text" name="email" id="email" value="admin@exchange.com" class="xlarge" readonly="readonly" size="30">
						</div>					
					</div>
					<div class="clearfix">
						<label for="phone1" class="required">Contact Number</label>
						<div class="input">
							<input type="text" name="phone1" id="phone1" value="650" size="3" maxlength="3" class="micro" onKeyUp="shiftbox(this,&#39;phone2&#39;)">
							<input type="text" name="phone2" id="phone2" value="123" size="3" maxlength="3" class="micro" onKeyUp="shiftbox(this,&#39;phone3&#39;)">
							<input type="text" name="phone3" id="phone3" value="3333" size="4" maxlength="4" class="micro">						
						</div>					
					</div>
					<div class="clearfix">
						<label for="fax1" class="required">Fax Number</label>
						<div class="input">
							<input type="text" name="fax1" id="fax1" value="650" size="3" maxlength="3" class="micro" onKeyUp="shiftbox(this,&#39;fax2&#39;)">
							<input type="text" name="fax2" id="fax2" value="111" size="3" maxlength="3" class="micro" onKeyUp="shiftbox(this,&#39;fax3&#39;)">
							<input type="text" name="fax3" id="fax3" value="5055" size="4" maxlength="4" class="micro">						
						</div>					
					</div>
					<div class="clearfix">
						<label for="address" class="required">Address <a href="#" data-original-title="Address" data-content="Your exact address as it appears on the EIN." rel="popover"><span class="label">?</span></a></label>
				        <div class="input">
							<input type="text" name="address" id="address" value="ADDRESS 123" class="xlarge" size="30">
						</div>					
					</div>
					<div class="clearfix row">
						<div class="span2" style="width:75px;">
						    <label for="zip" class="required">Zip Code</label>
					        <div class="input">
								<input type="text" name="zip" id="zip" value="94025" class="mini" size="5" onBlur="populateDate(this.value)" maxlength="5">
							</div>						
						</div>
						<div class="span3">
							<label for="city" class="required">City</label>
							<div class="input">
								<input type="text" name="city" id="city" value="MENLO PARK" readonly="readonly" class="medium" size="30">
							</div>						
						</div>
						<div class="span1">
							<label for="state" class="required">State</label>
							<div class="input">
								<input type="text" name="state" id="state" value="CA" readonly="readonly" class="micro" size="30">
							</div>						
						</div>
					</div>
					<div class="clearfix">
						<dt id="communicationPref-label"><label class="required">How would you like to receive your confirmation?</label></dt>
						<ul class="inputs-list"><li>
							<label for="communicationPref-Email"><input type="radio" name="communicationPref" id="communicationPref-Email" value="Email"> Email</label>
							<label for="communicationPref-Mail"><input type="radio" name="communicationPref" id="communicationPref-Mail" value="Mail" checked="checked"> Mail</label></li>
						</ul>					
					</div>
					<div class="actions">
						<input type="submit" name="submit" id="submit" value="Submit" class="btn primary" title="Submit">
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<div class="notes" style="display:none">
		<div class="row">
			<div class="span">

		      <p>If the Employer chooses to authorize an employee or a Navigator/Broker, 
		      a new user account is being created for this representative.  
		      The Employer may remove any Authorized Representatives using the Account 
		      Administration page accessible from the Employer Dashboard.
		      </p>
		    </div>
	    </div>
	</div>	
</div>