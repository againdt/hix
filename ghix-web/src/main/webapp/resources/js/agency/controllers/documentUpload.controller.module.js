(function() {
'use strict';

angular.module('documentUploadCtrlModule', [])
.controller('DocumentUploadCtrl', DocumentUploadCtrl);

	DocumentUploadCtrl.$inject = ['$http', '$stateParams', '$state', '$scope' ,'fileUpload', '$window', 'dataStorageService', '$translate'];

	function DocumentUploadCtrl($http, $stateParams, $state, $scope, fileUpload, $window, dataStorageService, $translate) {
        var vm = this;

        vm.init = init;
        vm.uploadFile = uploadFile;
        vm.nextPage = nextPage;
        vm.backPage = backPage;
        vm.initRemoveDocumentModel = initRemoveDocumentModel;
        vm.removeDoument = removeDoument;
        vm.completedApplication = completedApplication;

        function init(){
        	vm.loader = true;
            vm.invalidFileType = false;

        	//for admin flow
        	vm.isAdminFlow = $("#isAdminRole").val();

        	//for agency flow
        	vm.certificationStatus = $('#agencyCertificationStatus').val();

			if(vm.isAdminFlow) {
				vm.agencyId =  dataStorageService.getItem('encryptedAgencyId');
			} else {
				vm.agencyId = $("#agencyId").val() || dataStorageService.getItem('agencyId');
			}

        	if(vm.isAdminFlow !== 'true'){
        		$scope.$parent.vm.sideNavListOne[0].disableLink = false;
                $scope.$parent.vm.sideNavListOne[1].disableLink = false;
                $scope.$parent.vm.sideNavListOne[2].disableLink = false;
                $scope.$parent.vm.sideNavListOne[3].disableLink = false;
                $scope.$parent.vm.sideNavListOne[4].disableLink = false;
                $scope.$parent.vm.sideNavListOne[0].showOKicon = true;
                $scope.$parent.vm.sideNavListOne[1].showOKicon = true;
                $scope.$parent.vm.sideNavListOne[2].showOKicon = true;
                $scope.$parent.vm.sideNavListOne[3].showOKicon = true;
                $scope.$parent.vm.sideNavListOne[4].showOKicon = true;
        	}


        	if(!vm.agencyId && vm.isAdminFlow){
        		$state.go('manageAgencies');
        	}

        	var promise = $http.get('/hix/agency/documents/' + vm.agencyId);

        	promise.success(function(response){
        		vm.loader = false;
                vm.documentUploadData = response;
            });

            promise.error(function(response){
            	vm.loader = false;
                console.log (response);
            });
        }

        function uploadFile() {
        	vm.loader = true;
			vm.invalidFileType = false;
            var file = vm.file.name
            var ext = file.split('.').pop().toLowerCase();
            if($.inArray(ext, ['gif','png','jpg','jpeg', 'doc', 'docx', 'pdf']) == -1) {
                vm.invalidFileType = true;
                vm.loader = false;
                vm.file = "";
            } else {
                var fields = [
                    {'name': 'agencyId', 'data': vm.agencyId},
                    {'name': 'documentType', 'data': vm.file.type}
                ];

                var promise = fileUpload.uploadFileToUrl('document', vm.file, fields, '/hix/agency/document/upload');

                promise.success(function(response){
                    vm.loader = false;
                    vm.invalidFileType = false;
                    if(response.status === 'SUCCESS'){
                        $('#fileUploadSuccessModal').modal();
                        vm.file = '';
                        vm.documentUploadData.unshift(response);
                    }

                });

                promise.error(function(response){
                    vm.loader = false;
                });
            }

        };


        function backPage(){
            $window.location.href = "/hix/broker/buildprofile";
        };

        function completedApplication(){
            var promise = $http.get('/hix/agency/certification/view/'+ vm.agencyId);
            promise.then(function(response){
                if(response.status == '200'){
                    $('#successfullyCompletedApplication').modal();
                    //$state.go("accsetup.certificationstatus", {refreshFlag: true });
                }
            })
            .catch(function(error){
                console.log (error);
            });
        }

        function nextPage() {
            $state.go("accsetup.certificationstatus", {refreshFlag: true });
        }
        function initRemoveDocumentModel(documentRemoveId){
        	vm.documentRemoveId = documentRemoveId;
        	$('#removeDocumentConfirmationModal').modal();
        }

        function removeDoument(){
        	 var promise = $http.post('/hix/agency/document/delete/' + vm.documentRemoveId);
             promise.then(function(response){
            	 if(response.status === 200){
            		 vm.documentUploadData = vm.documentUploadData.filter(function(document){
            			 return document.documentId !== vm.documentRemoveId;
            		 });
            	 }
             })
             .catch(function(error){
                 console.log (error);
             });
        }

    }

})();
