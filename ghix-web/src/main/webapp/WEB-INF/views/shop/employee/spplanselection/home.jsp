<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util" prefix="util"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo" %>
<%@ taglib prefix="spnav" uri="/WEB-INF/tld/employee-spenroll-nav" %>
<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />
<style type="text/css">
.dialogCSS{background-color: white}
#emp-contribution.well {
	padding: 10px;
}
.dl-horizontal dt {overflow:visible;}
#plan-summary .dl-horizontal dt, #emp-contribution .dl-horizontal dt { width: 196px!important;}
#plan-summary .dl-horizontal dd, #emp-contribution .dl-horizontal dd { margin-left: 195px !important;}
.datepicker-dropdown {
    position: fixed;
}
</style>


<div class="gutter10">
	<c:if test="${UpcomingCoverageAvailable=='Y'}">
		<div class="row-fluid bubble-nav-shop">
		   <ul class="nav nav-tabs">
				<li id="currentEnrollmentTab"  <c:if test="${selectedTab=='currentCoverage' }" > class="active" </c:if> ><a href="<c:url value='/shop/employee/home/${currentApplicationId}'/>" >Current Coverage</a></li>
				<li id="upcomingEnrollmentTab"  <c:if test="${selectedTab=='upcomingCoverage' }" > class="active"  </c:if> ><a href="<c:url value='/shop/employee/home/${upcomingApplicationId}'/>" >Upcoming Coverage</a></li>
			</ul>
			<div class="alert">
				<span <c:if test="${selectedTab == 'upcomingCoverage' }" > class="hide"</c:if> id="currentEnrollmentAlert">The information below is related to your current coverage which ends on <strong>${coverageEndDate}</strong>.</span>
				<span <c:if test="${selectedTab == 'currentCoverage' }" > class="hide"</c:if> id="upcomingEnrollmentAlert">The information below is related to your upcoming coverage which begins on <strong>${effectiveDate}</strong>.</span>
			</div>
		</div>
	</c:if>
	<div class="row-fluid" id="titlebar">
  		<h1>Welcome &nbsp;<span id="user-name">${employee.name}</span></h1>
  	</div>
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <util:myStuff employeeApplication="${employeeApplication}"></util:myStuff>
      <util:quickLinks employeeApplication="${employeeApplication}"></util:quickLinks>
    </div><!--/span3-->
    <div class="span9" id="rightpanel">
      <div class="header">
      	<h4>What's Next        
	      	<c:if test="${not empty sp_enroll_end}">
	      		<span class=" pull-right">Special Enrollment: Complete By ${sp_enroll_end}</span>    
	      	</c:if></h4>
      </div>
      <div class="gutter10">
          <div class="row-fluid">
	          	<!-- TLD for employee special enrollment -->
			  	<spnav:reportChanges employeeApplication="${employeeApplication}" spEnrollEnd="${sp_enroll_end}"></spnav:reportChanges>
	          <div class="row-fluid">
	           	<!-- TLD for employer contribution and plan summary -->
      		     <util:employerContribution employeeApplication="${employeeApplication}"></util:employerContribution>
	      		 <util:planSummary employeeApplication="${employeeApplication}" isSpEnrollment="${isSpEnrollment}"></util:planSummary>
      		 
	          </div><!-- .row-fluid -->
  	  </div>
  		</div><!--/row-fluid-->        
	</div><!--/main .gutter10-->
</div>
</div>
                

			<!--federally recognized tribe LIGHTBOX Success  -->
	<div id="nativeamrerror" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="nativeamrerror" aria-hidden="true">
		<div class="modal-header">
			<button id="nativeamrerrorCrossClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			<h3 id="nativeamrerrorLabel">Report Change in Circumstances</h3>
		</div>
		<div class="modal-body" id="nativeamrerrorBody">
			<p>You have made the changes please come again on next month.</p>
		</div>
		<div class="modal-footer">					
			<button class="btn btn-primary" id="nativeamrerrorClose">Close</button>
		</div>
	</div>		
			
	<!-- temporary Modal for notifying Sp enrollment is not yet ready. Starts here -->
	<div class="modal fade hide" id="blockSPEnrollment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	      </div>
	      <div class="modal-body">
	        <div class="row-fluid">
	        	<p>Special enrollment functionality is not available now. Please check back next week for reporting your qualifying event.</p>
	        	<p>It is very important that you report a qualifying event like birth, marriage etc within 30 days of the event to qualify for special enrollment in your employer coverage.</p>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary"  data-dismiss="modal">Ok</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->		
	<!-- temporary Modal for notifying Sp enrollment is not yet ready. ends here -->
						
							
<script type="text/javascript">
$(document).ready(function(){
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			return false;
		}
	 });
});

$('#qualified-event-box').click(function() {
	
	var isnativeAmr = '${nativeAmr}'; 
	var isAddEventAllowed = '${isAddEventAllowed}'; 
	var employeeStatus='${employeeApplication.status}';
	
	if(isnativeAmr == 'true' && employeeStatus == 'NOT_ENROLLED'){
		if(isAddEventAllowed == 'true'){
			window.location.href = "<c:url value='/shop/employee/spplanselection/addnativeamremployeeevent/${employeeApplication.id}'/>";
		}else{
			$('#nativeamrerror').modal('show');
		}
	}else{
			window.location.href = "<c:url value='/shop/employee/spplanselection/reportchanges/${employeeApplication.id}'/>";
	}
     
});

$('#nativeamrerrorClose').click(function() {
	   	$('#nativeamrerror').modal('hide'); 
  });  


function gotoNext(pageNum){
	if(pageNum=='1'){
		location.href="<c:url value='/shop/employee/editemployee/${employeeApplication.id}' />";
	}
	else if(pageNum=='2'){
		location.href="<c:url value='/shop/employee/spplanselection/spenrollverifydependent/${employeeApplication.id}' />";
	}
	else if(pageNum=='3'){
		location.href="<c:url value='/shop/employee/employeeselectplan/${employeeApplication.id}' />";
	}
	else if(pageNum=='5'){
		location.href="<c:url value='/shop/employee/spplanselection/myplans/${employeeApplication.id}' />";
	}
	/* else if(pageNum=='4'){
		location.href="<c:url value='/shop/employee/verifyemployee' />";
	}
	 */
}


/* $('.add-on').click(function(){
	$('.datePicker').focus();
}); */
function imgSize(){
    $(".carrierlogo").each(function(){
        var width = $(this).width();
        var height = $(this).height();
        var maxWidth = 160;
		var maxHeight = 60;
		
		// Check if the current width is larger than the max
	    if(width > maxWidth){
	        ratio = maxWidth / width; // get ratio for scaling image
	        $(this).width(maxWidth); // Set new width
	        $(this).height((height * ratio)); // Scale height based on ratio
	        height = height * ratio; // Reset height to match scaled image
	    	width = width * ratio; // Reset width to match scaled image
	    }
		
		// Check if current height is larger than max
		if(height > maxHeight){                                     
		  ratio = maxHeight / height; // get ratio for scaling image
		  $(this).height(maxHeight); // Set new height
		  $(this).width((width * ratio)); // Scale width based on ratio    
		}
	    
		$(this).css('display','block');
    });
}
function loadData(val, appCode){
	location.href=	'/hix/shop/employee/home/'+appCode;
} 
</script>
