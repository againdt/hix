package com.getinsured.iex.im.wso2;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;

import com.getinsured.iex.im.wso2.carbon.um.ws.service.IsExistingUserResponse;
import com.getinsured.iex.im.wso2.wrappers.IsExistingUserResponseWrapper;
import com.getinsured.iex.im.wso2.wrappers.IsExistingUserWrapper;

public class IsExistingUserServiceHandler extends AbstractServiceHandler {

	private IsExistingUserResponseWrapper responseWrapper;

	@Override
	public Object getRequestpayLoad() throws HubServiceException,
			HubMappingException {
		JSONParser parser = new JSONParser();
		try {
			JSONObject jsonrequest = (JSONObject) parser.parse(this.getJsonInput());
			String userName = (String) jsonrequest.get("userName");
			IsExistingUserWrapper request = new IsExistingUserWrapper();
			request.setUserName(userName);
			return request.getSystemRepresentation();
		} catch (ParseException e) {
			throw new HubServiceException("Failed to construct the payload from request input");
		}
	}

	@Override
	public String handleResponse(Object response) throws HubServiceException {
		if(!(response instanceof IsExistingUserResponse)){
			throw new HubServiceException("Invalid response received expected "+IsExistingUserResponse.class.getName()+" Received:"+response.getClass().getName());
		}
		responseWrapper= new IsExistingUserResponseWrapper((IsExistingUserResponse) response);
		boolean success = responseWrapper.isExistingUser();
		return Boolean.toString(success);
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(this.responseWrapper == null){
			return RequestStatus.FAILED;
		}
		return RequestStatus.SUCCESS;
	}

	

}
