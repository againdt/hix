package com.getinsured.eligibility.at.ref.dto;

import java.math.BigDecimal;

/**
 * @author chopra_s
 * 
 */
public class APTCUpdateRequest {
	private long enrolledApplicationId;
	
	private long currentApplicationId;

	private BigDecimal maximumAPTC;
	
	public long getCurrentApplicationId() {
		return currentApplicationId;
	}

	public void setCurrentApplicationId(long currentApplicationId) {
		this.currentApplicationId = currentApplicationId;
	}

	public long getEnrolledApplicationId() {
		return enrolledApplicationId;
	}

	public void setEnrolledApplicationId(long enrolledApplicationId) {
		this.enrolledApplicationId = enrolledApplicationId;
	}

	public BigDecimal getMaximumAPTC() {
	    return maximumAPTC;
    }

	public void setMaximumAPTC(BigDecimal maximumAPTC) {
	    this.maximumAPTC = maximumAPTC;
    }
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("APTCUpdateRequest [currentApplicationId=").append(currentApplicationId)
				.append(", enrolledApplicationId=").append(enrolledApplicationId)
				.append(", maximumAPTC=").append(maximumAPTC)
				.append("]");
		return builder.toString();
	}
	
}
