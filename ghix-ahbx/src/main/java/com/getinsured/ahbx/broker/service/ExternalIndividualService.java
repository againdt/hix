package com.getinsured.ahbx.broker.service;
import com.getinsured.hix.model.ExternalIndividual;

/**
 * Encapsulates service layer method calls for External Individual
 */
public interface ExternalIndividualService {

	/**
	 * Saves external individual when external individual bob detail is fetched
	 * from AHBX
	 * 
	 * @see ExternalIndividualService#save(ExternalIndividual)
	 * 
	 * @param externalIndividual
	 * @return ExternalIndividual
	 */

	ExternalIndividual save(ExternalIndividual externalIndividual);

	/**
	 * Finds external individual based on individual case id
	 * 
	 * @see ExternalIndividualService#findByExternalIndividualID(long)
	 * 
	 * @param externalIndId
	 * @return ExternalIndividual
	 * 
	 */

	ExternalIndividual findByExternalIndividualID(long externalIndId);

	/**
	 * Updates external individual when external individual bob status is
	 * fetched from AHBX
	 * 
	 * @see ExternalIndividualService#update(ExternalIndividual)
	 * 
	 * @param externalIndividual
	 * @return ExternalIndividual
	 */

	ExternalIndividual update(ExternalIndividual externalIndividual);
}
