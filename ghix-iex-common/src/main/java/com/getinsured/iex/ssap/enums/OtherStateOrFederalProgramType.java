package com.getinsured.iex.ssap.enums;

/**
 * Describes other state or federal program.
 *
 * @author Yevgen Golubenko
 * @since 5/14/19
 */
public enum OtherStateOrFederalProgramType {
  CHIP("CHIP"),
  COBRA_COVERAGE("Cobra Coverage"),
  EMPLOYER_INSURANCE("Employer Insurance"),
  MEDICAID("Medicaid"),
  MEDICARE("Medicare"),
  NONE("None Of the Above"),
  OTHER_COVERAGE("Other Coverage"),
  PEACE_CORPS("Peace Corps"),
  RETIRE_HEALTH_BENEFITS("Retire Health Benefits"),
  TRICARE("TRICARE"),
  VETERANS_AFFAIRS_HCP("Veterans Affairs (VA) Health Care Program");

  private String label;

  OtherStateOrFederalProgramType(String label) {
    this.label = label;
  }

  public String getLabel() {
    return this.label;
  }
}
