package com.getinsured.iex.ssap.financial.type;

/**
 * Describes all possible tax filing statuses that
 * any person can have.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-07
 */
public enum TaxFilingStatus {
  /**
   * Tax filing status is not specified (e.g. if person doesn't file taxes)
   */
  UNSPECIFIED,

  /**
   * Tax filing status with IRS is Single
   */
  SINGLE,

  /**
   * Tax filing status with IRS is the Head of Household
   */
  HEAD_OF_HOUSEHOLD,

  /**
   * Tax filing status with IRS is Married, Filing Jointly.
   */
  FILING_JOINTLY,

  /**
   * Tax filing status with IRS is Married, Filing Separately.
   */
  FILING_SEPARATELY,

  /**
   * Tax filing status with IRS is Qualifying Widower (we don't currently handle this case).
   */
  QUALIFYING_WIDOWER;
}
