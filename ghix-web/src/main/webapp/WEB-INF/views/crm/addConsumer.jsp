<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/addressutils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker-new.js" />"></script>

<script type="text/javascript">
<%
String afterSave = (String)request.getAttribute("afterSave"); 
%>
function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
}

function submitAddConsumer(){
	 if( $("#frmaddconsumer").validate().form() ) {
	  var ssn = $("#ssn1").val()+ "-" +$("#ssn2").val()+ "-" +$("#ssn3").val();
	  var phone = $("#phone1").val()+ "-" +$("#phone2").val()+ "-" +$("#phone3").val();
	 
	  $('#ssn').val(ssn);
	 
	  $("#contactNumber").val(phone);
	
		 $('input:button').attr("disabled", true);
		 $("#frmaddconsumer").submit();

	 }
	 

	}

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
	 var dob = $("#birthDate").val();
	 var today = getCurrentDate() ;
	 if (new Date(dob) >= new Date(today)) {
	       return false;
	 }
	 return true;
});

function getCurrentDate(){
	var currentDate = new Date();
   var year = currentDate.getFullYear();
   var currMonth = currentDate.getMonth()+1;
   var month = (currMonth > 9 ) ? currMonth : '0' + currMonth; 
   var day = currentDate.getDate();

   var today = month + '/' + day + '/' + year ;
   
   return today;
}

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	 phone1 = $("#phone1").val(); 
	 phone2 = $("#phone2").val(); 
	 phone3 = $("#phone3").val(); 

	if( 
			(phone1 == "" || phone2 == "" || phone3 == "")  || 
			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)
	  ){ 
		return false; 
	}
	return true;
});

jQuery.validator.addMethod("ssncheck", function(value, element, param) {
 	 ssn1 = $("#ssn1").val(); 
 	 ssn2 = $("#ssn2").val(); 
 	 ssn3 = $("#ssn3").val(); 
 
 	if(ssn1.length != 0 || ssn2.length != 0 || ssn3.length != 0) {
 	    if( (ssn1 == "" || ssn2 == "" || ssn3 == "")  || 
 	           (isNaN(ssn1)) || (isNaN(ssn2)) || (isNaN(ssn3)) ||
 	           (ssn1.length != 3 || ssn2.length != 2 || ssn3.length != 4)
 	          ){ 
 	       
 	       		return false; 
 	     	} 		
 	}
 	
 	return true;
});

jQuery.validator.addMethod("zipcheck", function(value, element, param) {
  	elementId = element.id; 
	if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) ){
  		return false;	
  	}return true;
});
function getCountyList(eIndex, zip) {
  
	var subUrl =  '<c:url value="/shop/employer/manage/addCounties"/>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {zipCode : zip}, 
		dataType:'json',
		success : function(response) {
			populateCounties(response, eIndex);
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}

function populateCounties(response, eIndex) {

		//var count = Object.keys(response).length;
		//$('#county'+eIndex).find('option').remove();
	    var count = $.map(response, function(key, value) { return value; }).length;
	    
	     $('#county'+eIndex).empty();
	     if(count == 0){
	    	 $('#county'+eIndex+'_div').hide();
	    	 if(eIndex == '_Home'){
	    		 $("input[name='eligLead.countyCode']").remove();
	    	 }else if(eIndex == '_Ws'){
	    		 $("input[name='locations[0].location.county']").remove();
	    	 }
	     }
		$.each(response, function(key, value) {
			if(count == 1){
				
				$('#county'+eIndex+'_div').hide();
				if(eIndex == '_Home'){
					
					$("input[name='eligLead.countyCode']").remove();
					
					$('#county'+eIndex+'_div').append('<input type="text" name="eligLead.countyCode" id="county'+eIndex+'"  value="'+value+'" />');	
                    
				}else if(eIndex == '_Ws'){
					$("input[name='locations[0].location.county']").remove();
					$('#county'+eIndex+'_div').append('<input type="hidden" name="locations[0].location.county" id="county'+eIndex+'"  value="'+value+'" />');					
				}
				$('#countycode'+eIndex).val(value);
			}else if(count > 1){
				if ($('#county'+eIndex).is(":hidden") == true){
					if(eIndex == '_Home'){
						$("input[name='eligLead.countyCode']").remove();	
					}else if(eIndex == '_Ws'){
						$("input[name='locations[0].location.county']").remove();
					}
			    }
				$('#county'+eIndex+'_div').show();
				$('#county'+eIndex).prepend('<option value="'+value+'">'+ key +'</option>');
				$('#countycode'+eIndex).val(value);	
			}
		});
	} // end populateCounties


</script>
<div class="gutter10"  id="addConsumerDiv">
<div class="row-fluid">
<ul class="breadcrumb txt-left">
  <li><a href="<c:url value="/crm/consumer/searchConsumer" />" > &lt; Back</a> <span class="divider">|</span></li>
  <li><a href="<c:url value="/crm/consumer/newconsumer" />">Consumer</a> <span class="divider">/</span></li>
  <li><a href="<c:url value="/crm/consumer/searchConsumer" />">Manage List </a><span class="divider">/</span></li>
  <li class="active">Add Consumer</li>
</ul>
</div>
<!--titlebar-->
<div class="row-fluid">
	<h1 id="skip"><spring:message  code="label.createConsumer"/></h1>
</div>
<div class="row-fluid">
    <div class="row-fluid">
      <div class="span3" id="sidebar">
      	<div class="header">
        	<h4> <spring:message  code="label.aboutConsumer"/> </h4>
        </div>
        <div class="gutter10">
          <p><spring:message  code="label.consumerInfo1"/> </p>  
          &nbsp;&nbsp; 
          <p><spring:message  code="label.consumerInfo2"/> </p>            
        </div>
      </div>
      
      
      <!--sidebar-->
      <div class="span9" id="rightpanel">
      	<div class="header">
			<h4><spring:message  code="label.consumerInfo"/></h4>
		</div>
		<!--titlebar-->
		<div class="gutter10">
			<h4 class="lightgray"><spring:message  code="label.personal"/></h4>
			<form class="form-horizontal" id="frmaddconsumer" name="frmaddconsumer" action="<c:url value="/crm/consumer/newconsumer/addconsumerSubmit" />" method="post"  >
			<df:csrfToken/>
				<input type="hidden" name="ssn" id="ssn"/>
				<input type="hidden" name="phoneNumber" id="contactNumber"/> 
				
				<input type="hidden" id="clickedBtnname" name="clickedBtnname"/>
				
				<div class="control-group">
					<label class="control-label" for="firstName"><spring:message  code='label.emplFirstName'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" id="firstName" name="firstName" maxlength="100" value="${household.firstName}" />
						<span id="firstName_error"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="lastName"><spring:message  code='label.emplLastName'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" id="lastName" name="lastName"  maxlength="100" value="${household.lastName}"/>
						<span id="lastName_error"></span>
					</div>
				</div>
				
			 <div class="control-group">
					<label class="control-label" for="dob"><spring:message  code='label.dob'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<div class="input-append date date-picker" data-date="" data-date-format="mm/dd/yyyy">
                 			<input class="span10" type="text" name="birthDate"  id="birthDate"  readonly="readonly" value="${dob}">
                  			<span class="add-on"><i class="icon-calendar"></i></span>
                  		</div>
						<span id="birthDate_error"></span>
					</div>
				</div>
	
				<%-- <div class="control-group">
					<label class="control-label" for="gender"><spring:message  code='label.gender'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<label class="radio inline"> <input type="radio" name="gender" id="gender" value="MALE"> Male </label>
						<label class="radio inline"> <input type="radio" name="gender" id="gender" value="FEMALE"> Female </label>
						<span id="gender_error"></span>
					</div>
				</div> --%>
				
				<div class="control-group" >
					<label class="control-label" for="smoker"><spring:message  code='label.tobacoUser'/> </label>
					<div class="controls">
					
						<label class="radio inline"> <input type="radio" name="smoker" id="smoker" value="YES"  <c:if test="${isTobaccoUser == 'YES'}"> checked="checked" </c:if> > Yes </label>
						<label class="radio inline"> <input type="radio" name="smoker"  id="smoker" value="NO" <c:if test="${isTobaccoUser == 'NO'}"> checked="checked" </c:if> > No </label>
						<span id="smoker_error"></span>
					</div>
				</div>
						
				<%-- <fieldset>
				<label class="control-label"><spring:message  code='label.taxidnumber'/> <a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message  code='label.taxidnumbertooltip'/>" 
							class="info"><i class="icon-question-sign"></i>
						</a>
					</label>
				<div class="control-group">
					<div class="controls">
						<label for="ssn1" class="hidden">&nbsp;</label>
						<input type="text" class="span2 inline" name="ssn1" id="ssn1"  maxlength="3" onKeyUp="shiftbox(this, 'ssn2')"/>
						
						<label for="ssn2" class="hidden">&nbsp;</label>
						<input type="text" class="span1 inline" name="ssn2" id="ssn2"  maxlength="2" onKeyUp="shiftbox(this, 'ssn3')"/>
						
						<label for="ssn3" class="hidden">&nbsp;</label>
						<input type="text" class="span2 inline" name="ssn3" id="ssn3"  maxlength="4"/>
							<span id="ssn3_error"></span>
					</div>
				</div>
				</fieldset> --%>
				
				
				<%-- <div class="control-group">
					<label class="control-label" for="isMarried"><spring:message  code='label.fullTimeStudent'/></label>
					<div class="controls">
						<label class="radio inline"> <input type="radio" name="fullTimeStudent" id="fullTimeStudent" value="YES"> Yes </label>
						<label class="radio inline"> <input type="radio" name="fullTimeStudent" id="fullTimeStudent" value="NO" > No </label>
					</div>
				</div> --%>
				
				<!-- <div class="control-group">
					<label class="control-label" for="type">Type</label>
					<div class="controls">
						<select size="1" class="input-medium" name="typeOfMember" id="memberType">
							 <option value="PRIMARY">Primary</option>
							  <option value="SECONDARY">Secondary</option>
						</select>
					</div>
				</div> -->
				
				
					<div class="control-group">
					<label class="control-label" for="affiliate">Affiliate </label>
					<div class="controls">
						<%-- <select size="1" class="input-medium" name="household.affiliate" id="affiliate">
							 <option value="">Select</option>
							 <c:forEach var="affiliate" items="${affiliateList}">
						    	<option id="${affiliate.accountExecutive}" <c:if test="${affiliate.accountExecutive == household.affiliate}"> SELECTED </c:if> value="${affiliate.accountExecutive}">${affiliate.accountExecutive}</option>
							</c:forEach>
						</select> --%>
							<input type="text" id="affiliate" name="eligLead.affiliateId"  class="span3" maxlength="80" disabled="disabled" value="None">
							<span id="affiliateSBEId_error"></span>
					</div>
				</div>

					
				
				
				<div id="affiliateHouseholdID">
					<%-- <div class="control-group">
						<label class="control-label" for="householdId"><spring:message  code='label.affiliateHouseholdID'/> </label>
						<div class="controls">
							<input type="text" id="affiliateHouseID" name="household.affiliateHouseID" class="span3" maxlength="80"/>
							<span id="householdId_error"></span>
						</div>
					</div> --%>
					<div class="control-group">
						<label class="control-label" for="ffmSBEHouseholdID"><spring:message  code='label.affiliateSBEId'/> 
						</label>
						<div class="controls">
							<input type="text" id="ffmSBEHouseholdID" name="ffmSBEHouseholdID"  class="span3" maxlength="80">
							<span id="affiliateSBEId_error"></span>
						</div>
					</div>
				</div>
				
				<h4 class="lightgray"><spring:message  code='label.contact'/></h4>
				<div class="control-group">
					<label class="control-label" for="email"><spring:message  code='label.emplEmail'/></label>
					<div class="controls">
						<input type="text" name="email" id="email" value="${household.email}"/>
						<span id="email_error"></span>
					</div>
				</div>
				
				<fieldset>
					<label class="control-label"><spring:message  code='label.homePhone'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<div class="control-group">
					
					<div class="controls">
						<label for="phone1" class="hidden">&nbsp;</label>
						<input type="text" class="span2 inline" name="phone1" id="phone1" maxlength="3" onKeyUp="shiftbox(this, 'phone2')" value="${phone1}"/>
						
						<label for="phone2" class="hidden">&nbsp;</label>
						<input type="text" class="span2 inline" name="phone2" id="phone2" maxlength="3" onKeyUp="shiftbox(this, 'phone3')" value="${phone2}"/>
						
						<label for="phone3" class="hidden">&nbsp;</label>
						<input type="text" class="span2 inline" name="phone3" id="phone3" maxlength="4" value="${phone3}"/>
						<span id="phone3_error"></span>
					</div>
				</div>
				</fieldset>
				 
				<%-- <div class="control-group">
					<label class="control-label" for="address1_Home"><spring:message  code='label.homeAddress'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<input type="hidden" name="address2_Home" id="address2_Home" >
							<input type="hidden" name="lat" id="lat_Home" value="0.0"  />
							<input type="hidden" name="lon" id="lon_Home" value="0.0"  />	
							<input type="hidden" name="rdi" id="rdi_Home" value="" />
							<input type="hidden" name="contactLocation.countycode" id="countycode_Home" value="" />
					<div class="controls">
						<input type="text" name="contactLocation.address1" id="address1_Home"/>
						<input type="hidden" id="address1_Home_hidden" name="address1_Home_hidden" >
						<span id="address1_Home_error"></span>
						
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="city_Home"><spring:message  code='label.emplCity'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" name="contactLocation.city" id="city_Home"/>
						<input type="hidden" id="city_Home_hidden" name="city_Home_hidden">
						<span id="city_Home_error"></span>
						
					</div>	
				</div> --%>
				<div class="control-group">
					<label class="control-label" for="state_Home"><spring:message  code='label.emplState'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<select size="1" class="input-medium" name="state" id="state_Home">
							 <option value="">Select</option>
							 <c:forEach var="state" items="${statelist}">
						    	<option id="${state.code}" <c:if test="${state.code == defaultStateCode}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
							</c:forEach>
						</select>
						<input type="hidden" id="state_Home_hidden" name="state_Home_hidden" >
						<span id="state_Home_error"></span>
						
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="zip_Home"><spring:message  code='label.zip'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" name="eligLead.zipCode" id="zip_Home" class="input-small zipCode" maxlength="5" value="${household.eligLead.zipCode}"/>
						<input type="hidden" id="eligLeadId" name="eligLeadId" value="${eligLeadId}"/>
						<input type="hidden" value="0.0" id="zip_Home_hidden" name="zip_Home_hidden" />
						<span id="zip_Home_error"></span>
						
					</div>
				</div>
				
				<div class="control-group" id="county_Home_div" style="display:none;">
					<label for="county" class="control-label required"><spring:message  code='label.county'/></label>
					<div class="controls">
						<select size="1" class="input-large" name="eligLead.countyCode" id="county_Home">
						</select>
						<div id="county_Home_error"></div>		
					</div>
				</div>
				
				<div class="form-actions">
					
					<a  href="/hix/crm/consumer/manageConsumers" name="cancelButton" id="cancelButton" class="btn" /><spring:message  code='label.cancelButton'/></a>
        			<div  class="pull-right"><input type="submit" name="mainSubmitButton" id="mainSubmitButton" onClick="javascript:submitAddConsumer();" class="btn btn-primary" value="<spring:message  code='label.save'/>" /></div>
        		</div>
			</form>
			
			
		</div><!--gutter10-->
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
</div><!--gutter10-->

 
<!--gutter10-->


<script type="text/javascript">

$('#frmaddconsumer').validate({
	rules: {
		firstName : {required: true},
	    lastName : {required: true},
	    birthDate :{required : true, date: true, dobcheck: true},
	  
		/*ssn3 : { ssncheck : true,  remote:{ url:"<c:url value='/shop/employer/manage/isUniqueSSN' />",
			   async:false,
											data: {  ssn: function() { return $("#ssn1").val()+ "-" +$("#ssn2").val()+ "-" +$("#ssn3").val();} , 
													 employeeId: function() { return $("#id").val();}  
	 											  }
 									}
	 	} ,*/
	 	 
		 phone3 : {phonecheck : true} ,
		
		 'state' :  {required: true},
		 'eligLead.zipCode' :  { required : true, zipcheck:true},
	},
	messages : {
		firstName : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>"},
		lastName  : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>"} ,		  
		birthDate :{required :  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>" , 
				date:  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>",
				dobcheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateempdob' javaScriptEscape='true'/></span>"},
		/* gender : {required: "<span> <em class='excl'>!</em><spring:message code='label.validategender' javaScriptEscape='true'/></span>"},
		smoker : {required: "<span> <em class='excl'>!</em><spring:message code='label.validatetobaccouser' javaScriptEscape='true'/></span>"} , */
		/* ssn3: {  ssncheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempdigit' javaScriptEscape='true'/></span>",
				remote: "<span> <em class='excl'>!</em><spring:message code='label.validateemptaxid' javaScriptEscape='true'/></span>"} , */
	   /*  email : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateemailadr' javaScriptEscape='true'/></span>", 
	    						email: "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>"}, */
	     phone3 : { phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"} ,
	   /*   'contactLocation.address1' :  {required: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddr' javaScriptEscape='true'/></span>"},
			'contactLocation.city' :  {required: "<span> <em class='excl'>!</em><spring:message code='label.validatecity' javaScriptEscape='true'/></span>"}, */
			'state' :  {required: "<span> <em class='excl'>!</em><spring:message code='label.validatestate' javaScriptEscape='true'/></span>"},
			'eligLead.zipCode' :  {required: "<span> <em class='excl'>!</em><spring:message code='label.validateempzip' javaScriptEscape='true'/></span>", 
			  zipcheck: "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"}  
	},
	errorClass : "error",
	errorPlacement : function(error, element) {
		var elementId = element.attr('id');
		if ($("#" + elementId + "_error").html() != null) {
			$("#" + elementId + "_error").html('');
		}
		error.appendTo($("#" + elementId + "_error"));
		$("#" + elementId + "_error")
				.attr('class', 'error');
	}
	});


</script>
<script type="text/javascript">
$(document).ready(function(){
	var afterSave= '<%=afterSave%>';
	 if(afterSave=='afterSave'){ 
	        $('#markCompleteDialog').modal({
	            backdrop: true 
	        });
    } 
	
	$('.date-picker').datepicker({
	autoclose:true,
	forceParse: false
	}).on('changeDate', function(ev) {
		$('#dob_error').hide();
	});
	
	
	$('.info').tooltip();
	
	$('.zipCode').focusout(function(e) {
		var startindex = (e.target.id).indexOf("_");
		var index = (e.target.id).substring(startindex,(e.target.id).length);
		var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
		var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
		address1_e=address1_e+index;
		address2_e=address2_e+index;
		city_e=city_e+index;
		state_e=state_e+index;
		zip_e=zip_e+index;
		lat_e=lat_e+index;
		lon_e=lon_e+index;
		rdi_e+=index;
		county_e+=index;
		var model_address1 = address1_e + '_hidden' ;
		var model_address2 = address2_e + '_hidden' ;
		var model_city = city_e + '_hidden' ;
		var model_state = state_e + '_hidden' ;
		var model_zip = zip_e + '_hidden' ;
		
		var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e;
		if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
			
			if(($('#'+ address2_e).val())==="Address Line 2"){
				$('#'+ address2_e).val('');
			}	
			
			
			
			/* viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
				$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
				idsText);	 */
		}
		
		getCountyList(index, $('#'+ zip_e).val());
		
	});
	
});


</script>

<form name="dialogForm" id="dialogForm" method="POST"   novalidate="novalidate" action="<c:url value="/crm/consumer/dashboard" />">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left"></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Consumer Information was saved. Going to view new consumer account
                    Press OK to Continue or Cancel to Exit
				</div>
	</div>
  </div>
  <div class="modal-footer clearfix">
    <button class="btn btn-primary" type="submit">OK</button>
    <input type="hidden" name="switchToModuleName" id="switchToModuleName" value='<encryptor:enc value="individual"/>' /> 
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value='<encryptor:enc value="${household.id}"/>' />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.eligLead.name}" />
    <a data-role="button"  class="btn btn-primary" href="/hix/crm/consumer/manageConsumers">Cancel</a>
  </div>
</div>
</form>
