const inputHouseholds = ["regression/APTC_2HH"]

before(function () {
    cy.runBeforeBlock()
})

inputHouseholds.forEach( file => {
    describe(`CIC49: Enroll all Disenrolls Dental through My Enrollments page - ${file}`, function() {
        it (`Init household info with SSN, DOB with checks for ${file}`, function() {
            cy.ServerInfo = this.serverInfo
            cy.ServerConfig = this.serverConfig.body
            cy.ServerTime = this.serverTime.body
            cy.initHousehold(file)
        })
        it(`Creates Account Transfer for ${file}`, function () {
            cy.createAccountTransfer(cy.household)
        })
        it(`Login to  ` + cy.MN.URL, function () {
            cy.login(cy.h.user, cy.h.pass)
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Enrolls household into Medical and Dental - ${file}`, function () {
            cy.enrollIntoMedicalAndDental();

        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Disenrolls from Dental through My Enrollments page - ${file}`, function () {
            cy.disenrollDentalMyEnrollments();
            cy.h.appStatus = "med_full_enroll"
        })
        it(`Verify dashboard`, function () {
            cy.verifyDashboard(cy.h)
        })
        it(`Logout`, function () {
            cy.logout()
        })
    })
})









