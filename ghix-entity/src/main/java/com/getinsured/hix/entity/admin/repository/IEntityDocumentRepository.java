package com.getinsured.hix.entity.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.entity.EntityDocuments;

/**
 * Spring-JPA repository that encapsulates methods that interact with the BrokerDocuments
 * table
 */
public interface IEntityDocumentRepository extends RevisionRepository<EntityDocuments, Integer, Integer>,
		JpaRepository<EntityDocuments, Integer> {

}
