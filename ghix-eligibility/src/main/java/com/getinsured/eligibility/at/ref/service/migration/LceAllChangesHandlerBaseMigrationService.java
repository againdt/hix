package com.getinsured.eligibility.at.ref.service.migration;



import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.getinsured.eligibility.at.ref.common.LceAllChangesHandlerDetermination;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapter;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterRequest;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterResponse;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;

public abstract class LceAllChangesHandlerBaseMigrationService extends LceProcessHandlerBaseMigrationService implements LceProcessHandlerService {
	
	private static final String APP_EVENT_ADD_APPLICANT = "ADD_APPLICANT";
	private static final String APP_EVENT_REMOVE_APPLICANT = "REMOVE_APPLICANT";
	@Autowired
	@Qualifier("planAvailabilityAdapter")
	private PlanAvailabilityAdapter planAvailabilityAdapter;

	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private AppEventService appEventService;
	
	PlanAvailabilityAdapterResponse executePlanAvailability(LCEProcessRequestDTO lceProcessRequestDTO) {
		PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest = new PlanAvailabilityAdapterRequest();
		planAvailabilityAdapterRequest.setSsapApplicationId(lceProcessRequestDTO.getCurrentApplicationId());
		planAvailabilityAdapterRequest.setHealthEnrollees(lceProcessRequestDTO.getEnrolledApplicationAttributes().getHealthEnrollees());
		planAvailabilityAdapterRequest.setDentalEnrollees(lceProcessRequestDTO.getEnrolledApplicationAttributes().getDentalEnrollees());
		planAvailabilityAdapterRequest.setHealthEnrollmentId(lceProcessRequestDTO.getEnrolledApplicationAttributes().getHealthEnrollmentId());
		planAvailabilityAdapterRequest.setDentalEnrollmentId(lceProcessRequestDTO.getEnrolledApplicationAttributes().getDentalEnrollmentId());
		planAvailabilityAdapterRequest.setSsapApplicationId(lceProcessRequestDTO.getCurrentApplicationId());

		PlanAvailabilityAdapterResponse response = null;
		try {
			response = planAvailabilityAdapter.execute(planAvailabilityAdapterRequest);
		} catch (Exception e) {
			/* eat and send failure response to avoid event rollback */
		}
		if (response == null) {
			response = new PlanAvailabilityAdapterResponse();
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return response;
	}

	void processCSFailure(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		processMixed(currentApplication, false, lceProcessRequestDTO);
	}

	void processSingleSuccess(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		final SsapApplication enrolledApplication = loadApplication(lceProcessRequestDTO.getEnrolledApplicationId());
		closeSsapApplication(enrolledApplication);
		updateCurrentAppToEN(currentApplication);
		logAddRemoveApplicantEvent(currentApplication,enrolledApplication);
		updateAllowEnrollment(currentApplication, Y);
		if (LceAllChangesHandlerDetermination.hasAddMember(currentApplication.getSsapApplicants())) {
			ssapApplicationEventMigrationService.setChangePlan(lceProcessRequestDTO.getCurrentApplicationId());
		}
		
	}

	void processSingleFailure(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		updateCurrentAppToER(currentApplication);
		updateAllowEnrollment(currentApplication, Y);
	}

	void processMixed(SsapApplication currentApplication, boolean email57, LCEProcessRequestDTO lceProcessRequestDTO) {
		updateCurrentAppToER(currentApplication);
		updateAllowEnrollment(currentApplication, Y);
	}

	void handlePendingValidation(SsapApplication currentApplication) {
		updateAllowEnrollment(currentApplication, Y);
		updateCurrentAppToER(currentApplication);
	}

	private void logAddRemoveApplicantEvent(SsapApplication currentApplication, SsapApplication enrolledApplication){
		EventInfoDto eventInfoDto = new EventInfoDto();
		Map<String, String> mapEventParam = new HashMap<String, String>();
		
		//event type and category
		LookupValue lookupValue = null;
		
		String addedApplicantNames="";
		String removedApplicantNames="";
		for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {
			if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
				addedApplicantNames = ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName() ;
			}
			else if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
				removedApplicantNames = ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName() ;
			}
			
		}
		if(addedApplicantNames.length() > 1){
			lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APP_EVENT_TYPE,APP_EVENT_ADD_APPLICANT);
			mapEventParam.put("Added Applicants", addedApplicantNames);
		}
		else if(removedApplicantNames.length()> 1){
			lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APP_EVENT_TYPE,APP_EVENT_REMOVE_APPLICANT);
			mapEventParam.put("Removed Applicants",removedApplicantNames);
		}
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleId(currentApplication.getCmrHouseoldId().intValue());
		eventInfoDto.setModuleName(GhixRole.INDIVIDUAL.toString());
		appEventService.record(eventInfoDto, mapEventParam);
	}
	
	}
