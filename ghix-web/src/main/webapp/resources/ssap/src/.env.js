const PRODUCTION = 'production';
const DEVELOPMENT = 'development';

const ROUTES = {
    [PRODUCTION]: {
        SSAP: 'hix/newssap/start',
    },
    [DEVELOPMENT]: {
        SSAP: '/'
    }
};

const isProduction = process.env.NODE_ENV === PRODUCTION;
const isDevelopment = process.env.NODE_ENV === DEVELOPMENT;
const MODE = isProduction ? PRODUCTION : DEVELOPMENT;

export default {
    ENV: {
        PRODUCTION: isProduction,
        DEVELOPMENT: isDevelopment
    },
    MODE: MODE,
    ROUTES: {
        ...ROUTES[MODE]
    }
};