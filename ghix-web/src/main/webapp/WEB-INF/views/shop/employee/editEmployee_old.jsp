<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript">
function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
}

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	var elementName = element.name;
  	 phone1 = $("#phone1").val(); 
  	 phone2 = $("#phone2").val(); 
  	 phone3 = $("#phone3").val(); 
  
  	if( (phone1 == "" || phone2 == "" || phone3 == "")  || (isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3))  ){ return false; }
  	return true;
});

jQuery.validator.addMethod("faxcheck", function(value, element, param) {
	var elementName = element.name;
  	 fax1 = $("#fax1").val(); 
  	 fax2 = $("#fax2").val(); 
  	 fax3 = $("#fax3").val(); 
  
  	if( (isNaN(fax1)) || (isNaN(fax2)) || (isNaN(fax3))  ){ return false; }
  	//if any of the three box is not empty, then other box should not be empty
  	if(  (fax1 != "" || fax2 != "" || fax3 != "") &&  (fax1 == "" || fax2 == "" || fax3 == "") ){ return false; }
  	return true;
});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
	  var today=new Date();
	dob_mm = $("#birthDateMM").val();  dob_dd = $("#birthDateDD").val();  dob_yy = $("#birthDateYY").val(); 
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
	if(today.getTime() < birthDate.getTime()){ return false; }
	return true;
});

jQuery.validator.addMethod("isAgeGreater", function(value, element, param) {
	dob_mm = $("#birthDateMM").val();  dob_dd = $("#birthDateDD").val();  dob_yy = $("#birthDateYY").val(); 
	var today=new Date()
	var birthDate=new Date();
	year = parseInt(dob_yy)+ parseInt(param);
	birthDate.setFullYear(year ,dob_mm - 1,dob_dd);
	if(today.getTime() > birthDate.getTime()){ return false; }
	return true;
});

jQuery.validator.addMethod("isAgeLess", function(value, element, param) {
	dob_mm = $("#birthDateMM").val();  dob_dd = $("#birthDateDD").val();  dob_yy = $("#birthDateYY").val(); 
	var today=new Date()
	var birthDate=new Date();
	year = parseInt(dob_yy)+ parseInt(param);
	birthDate.setFullYear(year ,dob_mm - 1,dob_dd);
	if(today.getTime() <= birthDate.getTime()){ return false; }
	return true;
});

jQuery.validator.addMethod("ssncheck", function(value, element, param) {
	var elementName = element.name;
  	 ssn1 = $("#ssn1").val(); 
  	 ssn2 = $("#ssn2").val(); 
  	 ssn3 = $("#ssn3").val(); 
  
  	if( (ssn1 == "" || ssn2 == "" || ssn3 == "")  || (isNaN(ssn1)) || (isNaN(ssn2)) || (isNaN(ssn3))  ){ return false; }
  	return true;
});

function validateForm(){
	if( $("#frmeditemp").validate().form() ) {
		var ssn = $("#ssn1").val()+ "-" +$("#ssn2").val()+ "-" +$("#ssn3").val();
		$("#ssn").val(ssn);
		var phone = $("#phone1").val()+ "-" +$("#phone2").val()+ "-" +$("#phone3").val();
		$("#contactnumber").val(phone);
		var dob = $("#birthDateMM").val()+ "/" +$("#birthDateDD").val()+ "/" +$("#birthDateYY").val();
		$("#birthdate").val(dob);
		if($("#fax1").val() != "")
		{
			var fax = $("#fax1").val()+ "-" +$("#fax2").val()+ "-" +$("#fax3").val();
			$("#fax").val(fax);
		}
	 	$("#frmeditemp").submit();
	}
}
</script>


<div class="container">




	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	</div>
	
	<div class="row-fluid">	
	<c:if test="${errorMsg == ''}">	
		<div class="span3">
			<div class="gutter10">
				<h3>Employee Contact Information</h3>
				<p>Please enter here the name and contact information for an executive
					officer of your company, such as an owner or CEO. This person should
					be authorized to act on behalf of the company and should be
					authorized to make health benefit decisions on the company&#39;s
					behalf.</p>
			</div>
		</div>
		
	<div class="span9">
		<div class="gutter10">
			<div class="page-header">
				<h1><spring:message  code="label.yourContactInfo"/></h1>
			</div>
		</div>
	
	
	<form class="form-horizontal gutter10" id="frmeditemp" name="frmeditemp" action="../editEmployeeSubmit" method="POST">
          <df:csrfToken/>
		<input type="hidden" name="id" id="id" value="${id}">
		<input type="hidden" name="employer.id" id="employerid" value="${employerid}">
		<input type="hidden" name="person.ssn" id="ssn" value="">
		<input type="hidden" name="person.contactNumber" id="contactnumber" value="">
		<input type="hidden" name="person.fax" id="fax" value="">
		<input type="hidden" name="person.dob" id="birthdate" value="">
		<input type="hidden" name="error" id="error" value="${error} ">
		
				
							
								<div class="control-group">
									<label class="control-label required" for="firstName" ><spring:message  code="label.firstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input class="input-large" type="text"  name="person.firstName" id="firstName" value="${employee.person.firstName}" size="35">
										<div id="firstName_error"></div>	
									</div>
								</div>
								<div class="control-group">
									<label for="LastName" class="control-label required"><spring:message  code="label.lastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input class="input-large" type="text"  name="person.lastName" id="lastName" value="${employee.person.lastName}" size="20">
										<div id="lastName_error"></div>
									</div>
								</div>
								<div class="control-group">
									<label for="suffix" class="control-label required"><spring:message  code="label.emplTitle"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input class="input-large" type="text" name="person.suffix" id="suffix" value="${employee.person.suffix}">	
										<div id="suffix_error"></div>	
									</div>
								</div>


							<c:if test="condition"></c:if>
							<c:choose>
							   <c:when test="${employee.person.dob != null && employee.person.dob != ''}">
							   		<c:set var="dob" value="${employee.person.dob}" />
									<c:set var="dateParts" value="${fn:split(dob,'-')}"/>
									
									<c:set var="dobMM" value="${dateParts[1]}"/>
									<c:set var="dobDD" value="${dateParts[2]}"/>
									<c:set var="dobYY" value="${dateParts[0]}"/>
							   </c:when>
							</c:choose>
							
								<div class="control-group">
									<label for="birthDate" class="control-label required"><spring:message  code="label.birthDate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" name="birthDateMM" id="birthDateMM" value="${dobMM}" class="input-mini" size="2" maxlength="2" placeholder="MM" onKeyUp="shiftbox(this,'birthDateDD')" > 
										<input type="text" name="birthDateDD" id="birthDateDD" value="${dobDD}" class="input-mini" size="2" maxlength="2" placeholder="DD" onKeyUp="shiftbox(this,'birthDateYY')" >
										<input type="text" name="birthDateYY" id="birthDateYY" value="${dobYY}" class="input-mini" size="4" maxlength="4" placeholder="YYYY" >
										<div id="birthDateYY_error"></div>
									</div>
								</div>
								<div class="control-group">
									<label for="gender" class="control-label required"><spring:message  code="label.gender"/></label> 
									<div class="controls">
										<select  name="person.gender" id="gender" class="input-small">
										    <option ${employee.person.gender == 'Male' ? 'selected="selected"' : ''} value="Male">Male</option>
											<option ${employee.person.gender == 'Female' ? 'selected="selected"' : ''} value="Female">Female</option>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label for="smoker" class="control-label required"><spring:message  code="label.smoker"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label> 
									<div class="controls">
										<select  name="person.smoker" id="smoker" class="input-mini">
										    <option ${fn:contains(employee.person.smoker, 'Y') ? 'selected="selected"' : ''} value="Y">Yes</option>
											<option ${fn:contains(employee.person.smoker, 'N') ? 'selected="selected"' : ''} value="N">No</option>
										</select>
										<div id="gender_error"></div>
									</div>
								</div>

							<c:if test="condition"></c:if>
							<c:choose>
								   <c:when test="${employee.person.ssn != null && employee.person.ssn != ''}">
								   		<c:set var="ssn" value="${employee.person.ssn}" />
										<c:set var="ssnParts" value="${fn:split(ssn,'-')}"/>
									
								   		<c:set var="ssn1" value="${ssnParts[0]}" />
										<c:set var="ssn2" value="${ssnParts[1]}" />
										<c:set var="ssn3" value="${ssnParts[2]}" />
								   </c:when>
							</c:choose>
							
								<div class="control-group">
									<label for="ssn" class="control-label required"><spring:message  code="label.ssn"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" name="ssn1" id="ssn1" value="${ssn1}" size="3" maxlength="3" class="input-mini" onKeyUp="shiftbox(this, 'ssn2')">
										<input type="text" name="ssn2" id="ssn2" value="${ssn2}" size="3" maxlength="2" class="input-mini" onKeyUp="shiftbox(this, 'ssn3')">
										<input type="text" name="ssn3" id="ssn3" value="${ssn3}" size="4" maxlength="4" class="input-mini">
										<div id="ssn3_error"></div>	
									</div>
								</div>
							
							<c:if test="condition"></c:if>
							<c:choose>
								   <c:when test="${employee.person.contactNumber != null && employee.person.contactNumber != ''}">
								   		<c:set var="phone" value="${employee.person.contactNumber}" />
										<c:set var="phoneParts" value="${fn:split(phone,'-')}"/>
									
								   		<c:set var="phone1" value="${phoneParts[0]}" />
										<c:set var="phone2" value="${phoneParts[1]}" />
										<c:set var="phone3" value="${phoneParts[2]}" />
								   </c:when>
							</c:choose>
							
								<div class="control-group">
									<label for="phone1" class="control-label required"><spring:message  code="label.emplPrimaryContact"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a href="#" data-original-title="Primary Number" data-content="<spring:message  code="label.emplPrimaryContactTip"/>" rel="popover"><span class="label">?</span> </a> </label>
									<div class="controls">
										<input type="text" name="phone1" id="phone1" value="${phone1}" size="3" maxlength="3" class="input-mini" onKeyUp="shiftbox(this, 'phone2')">
										<input type="text" name="phone2" id="phone2" value="${phone2}" size="3" maxlength="3" class="input-mini" onKeyUp="shiftbox(this, 'phone3')">
										<input type="text" name="phone3" id="phone3" value="${phone3}" size="4" maxlength="4" class="input-mini">
										<div id="phone3_error"></div>
									</div>
								</div>

							<c:if test="condition"></c:if>
							<c:choose>
								   <c:when test="${employee.person.fax != null && employee.person.fax != ''}">
								   		<c:set var="fax" value="${employee.person.fax}" />
										<c:set var="faxParts" value="${fn:split(fax,'-')}"/>
									
								   		<c:set var="fax1" value="${faxParts[0]}" />
										<c:set var="fax2" value="${faxParts[1]}" />
										<c:set var="fax3" value="${faxParts[2]}" />
								   </c:when>
							</c:choose>
							
								<div class="control-group">
									<label for="fax1" class="control-label required"><spring:message  code="label.emplFaxNo"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" name="fax1" id="fax1" value="${fax1}" size="3" maxlength="3" class="input-mini" onKeyUp="shiftbox(this, 'fax2')">
										<input type="text" name="fax2" id="fax2" value="${fax2}" size="3" maxlength="3" class="input-mini" onKeyUp="shiftbox(this, 'fax3')">
										<input type="text" name="fax3" id="fax3" value="${fax3}" size="4" maxlength="4" class="input-mini">	
										<div id="fax3_error"></div>
									</div>
								</div>
							

										<c:forEach items="${empLocationsList}" var="emplocationobj">
											
												<div class="control-group">
													<label class="control-label required"><spring:message  code="label.employerLocation"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
													<div class="controls">
														<input type="radio" name="employerLocation.id" id="emplocationid" size="4" 
											         value="${emplocationobj.id}" ${emplocationobj.id == employee.employerLocation.id ? 'checked="checked"' : ''}  maxlength="4" class="">
									        		     <div id="emplocationid_error"></div>
											        </div>
											    </div>
											    <div class="control-group">
												    <div class="controls">
												          	<input type="text" readonly="readonly" value="${emplocationobj.location.city}" />
												    </div>
												  </div>
												  <div class="control-group">
												    <div class="controls">
												          	<input type="text" readonly="readonly" value="${emplocationobj.location.state}" class="input-mini" />
												    </div>
												  </div>
												  <div class="control-group">
												    <div class="controls">
												            <input type="text" readonly="readonly" value="${emplocationobj.location.zip}" class="input-mini" />
												    </div>
												  </div>
										      
										</c:forEach>

								<div class="control-group">
									<label for="address1" class="control-label required"><spring:message  code="label.homeAddress1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" value="${employee.person.location.address1}" name="person.location.address1" id="address1" class="input-large">	
									<div id="address1_error"></div>
									</div>	
								</div>
								<div class="control-group">
									<label for="address2" class="control-label required"><spring:message  code="label.homeAddress2"/></label>
									<div class="controls">
										<input type="text" value="${employee.person.location.address2}" name="person.location.address2" id="address2" class="input-large">
									</div>	
								</div>
								<div class="control-group">
									<label for="city" class="control-label required"><spring:message  code="label.emplCity"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" value="${employee.person.location.city}" name="person.location.city" id="city" class="input-large">
										<div id="city_error"></div>	
									</div>	
								</div>

								<div class="control-group">
									<label for="state" class="control-label required"><spring:message  code="label.emplState"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<select size="1"  id="state" name="person.location.state" path="statelist">
											 <option value="">Select</option>
											 <c:forEach var="state" items="${statelist}">
											 	<option <c:if test="${state.code == employee.person.location.state}"> SELECTED </c:if> value="<c:out value="${state.code}" />">
		                                            <c:out value="${state.name}" />
		                                        </option>
											 </c:forEach>
										</select>
										<div id="state_error"></div>
									</div>
								</div>
								<div class="control-group">
									<label for="zip" class="control-label required"><spring:message  code="label.emplZip"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" value="${employee.person.location.zip}" name="person.location.zip" id="zip" size="5" maxlength="5" class="input-mini">
										<div id="zip_error"></div>	
									</div>	
								</div>
								<div class="control-group">
									<label for="email" class="control-label required"><spring:message  code="label.emplEmail"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a href="#" data-original-title="Email Address"  data-content="<spring:message  code="label.emplEmailTip"/>" rel="popover"><span class="label">?</span> </a> </label>
									<div class="controls">
										<input type="text" value="${employee.person.user.email}" name="person.user.email" id="email" readonly="readonly" class="input-large" size="30">
										<div id="email_error"></div>
									</div>
								</div>
								<div class="control-group" id="communicationPref-label">
									<label class="control-label required"><spring:message  code="label.emplConfirmType"/></label>
									<div class="controls span2">
										<label class="radio" for="communicationPref-Email"><input type="radio" ${employee.person.user.communicationPref == 'Email' ? 'checked="checked"' : ''} name="person.user.communicationPref" id="communicationPref-Email" value="Email"><spring:message  code="label.emplConfirmEmail"/></label>
									</div>
									<div class="controls span2">
										<label class="radio" for="communicationPref-Mail"><input type="radio" ${employee.person.user.communicationPref == 'Mail' ? 'checked="checked"' : ''} name="person.user.communicationPref" id="communicationPref-Mail" value="Mail"> <spring:message  code="label.emplConfirmMail"/></label>	
									</div>
								</div>	
								<div class="control-group">
									<div class="controls">
									<label class="checkbox" for="nativeAmrTribe"><input type="checkbox" ${employee.nativeAmrTribe == 'Y' ? 'checked="checked"' : ''} name="nativeAmrTribe" id="nativeAmrTribe"  value="Y">
							    		<spring:message  code="label.nativeAmrTribe"/></label>
									</div>
								</div>
								<div class="control-group">
									<label for="hrsPerWeek" class="control-label required"><spring:message  code="label.hrsPerWeek"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" name="hrsPerWeek" value="${employee.hrsPerWeek}" id="hrsPerWeek" class="input-mini" size="60">
										<div id="hrsPerWeek_error"></div>
									</div>
								</div>
								<div class="control-group">
									<label for="typeOfCoverage" class="control-label required"><spring:message  code="label.typeOfCoverage"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" name="typeOfCoverage" value="${employee.typeOfCoverage}" id="typeOfCoverage" class="input-large" size="60">
										<div id="typeOfCoverage_error"></div>
									</div>
								</div>
								<div class="control-group">
									<label for="typeOfEmployee" class="control-label required"><spring:message  code="label.typeOfEmployee"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<select  name="typeOfEmployee" id="typeOfEmployee">
											<option ${employee.typeOfEmployee == '' ? 'selected="selected"' : ''} value="">Select</option>
											<option ${employee.typeOfEmployee == 'FULLTIME' ? 'selected="selected"' : ''} value="FULLTIME">FULLTIME</option>
											<option ${employee.typeOfEmployee == 'PARTTIME' ? 'selected="selected"' : ''} value="PARTTIME">PARTTIME</option>
											<option ${employee.typeOfEmployee == 'SEASONAL' ? 'selected="selected"' : ''} value="SEASONAL">SEASONAL</option>
										</select>
										<div id="typeOfEmployee_error"></div>
									</div>
								</div>
								<div class="control-group">
									<label for="income" class="control-label required"><spring:message  code="label.income"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
									<div class="controls">
										<input type="text" name="yearlyIncome" value="${employee.yearlyIncome}" id="yearlyIncome" class="input-large" size="60">
										<div id="yearlyIncome_error"></div>
									</div>
								</div>
			
			<div class="form-actions">
				<input type="button" name="continue" id="continue" onClick="javascript:validateForm();" value="<spring:message  code="label.emplContinue"/>" class="btn btn-primary"/>			
			</div>
		</form>
	</div>
</c:if>
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
	
					<p>The prototype showcases three scenarios (A, B and C) dependant on
						a particular Employer's eligibility to use the SHOP Exchange.</p>
				</div>
			</div>
		</div>

	</div> <!-- row-fluid -->


</div>  <!-- container -->

<script type="text/javascript">
var validator = $("#frmeditemp").validate({ 
	rules : {'person.firstName' : { required : true},
			'person.lastName' : { required : true},
			'person.suffix' : { required : true},
			phone3 : {phonecheck : true},
			fax3 : {faxcheck : true},
			'person.location.address1' : {required : true},
			'person.location.city': {required : true},
			'person.location.state': {required : true},
			'person.location.zip': {required : true, number: true},
			birthDateYY : { required : true,dobcheck : true, isAgeLess : 18, isAgeGreater:65},
			ssn3 : { ssncheck : true},
			hrsPerWeek : { required : true,number : true},
			typeOfCoverage : { required : true},
			typeOfEmployee : { required : true},
			yearlyIncome : { required : true,number : true}
	},
	messages : {
		'person.firstName' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>"},
		'person.lastName': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>"},
		'person.suffix': { required : "<span> <em class='excl'>!</em><spring:message code='label.validateTitle' javaScriptEscape='true'/></span>"},
		phone3: { phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNo' javaScriptEscape='true'/></span>"},
		fax3: { faxcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateFaxNo' javaScriptEscape='true'/></span>"},
		'person.location.address1': {required: "<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
		'person.location.city': {required: "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		'person.location.state': {required: "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		'person.location.zip': {required: "<span> <em class='excl'>!</em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
								     number: "<span> <em class='excl'>!</em> <spring:message code='label.validateZipSyntax' javaScriptEscape='true'/>" 
									},
		birthDateYY: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateBirthDate' javaScriptEscape='true'/></span>",
			           dobcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateBirthDateSyntax' javaScriptEscape='true'/></span>",
			           isAgeLess : "<span> <em class='excl'>!</em><spring:message code='label.validateEmployeeMinAge' javaScriptEscape='true'/></span>",
				       isAgeGreater : "<span> <em class='excl'>!</em><spring:message code='label.validateEmployeeMaxAge' javaScriptEscape='true'/></span>"
			         },
		ssn3: {  ssncheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSSN' javaScriptEscape='true'/></span>"},
		hrsPerWeek: {  required : "<span> <em class='excl'>!</em><spring:message code='label.validateHrsPerWeek' javaScriptEscape='true'/></span>",
		               number : "<span> <em class='excl'>!</em><spring:message code='label.validateHrsPerWeekSyntax' javaScriptEscape='true'/></span>"
		            },
		typeOfCoverage: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateTypeofCoverage' javaScriptEscape='true'/></span>"},
		typeOfEmployee: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateTypeofEmp' javaScriptEscape='true'/></span>"},
		yearlyIncome: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateIncome' javaScriptEscape='true'/></span>",
                        number : "<span> <em class='excl'>!</em><spring:message code='label.validateIncomeSyntax' javaScriptEscape='true'/></span>"
                      }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});
</script>