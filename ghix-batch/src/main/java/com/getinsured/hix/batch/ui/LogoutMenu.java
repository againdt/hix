package com.getinsured.hix.batch.ui;


import org.springframework.batch.admin.web.resources.BaseMenu;
import org.springframework.stereotype.Component;
 
@Component
public class LogoutMenu extends BaseMenu {
  private static final int ORDER = 7;
  public LogoutMenu() {
    super("/j_spring_security_logout", "Logout", ORDER);
  }
}