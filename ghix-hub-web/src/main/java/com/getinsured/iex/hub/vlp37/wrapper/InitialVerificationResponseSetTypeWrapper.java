package com.getinsured.iex.hub.vlp37.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationIndividualResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
/**
 * Wrapper class for gov.hhs.cms.dsh.sim.ee.vlp.DHSResponseFields
 * 
 * @author Nikhil Talreja
 * @since 10-Feb-2014
 * 
 */
public class InitialVerificationResponseSetTypeWrapper implements JSONAware{
	
    private InitialVerificationResponseSetType initVerifResponseSetType;
	
	private List<InitialVerificationIndividualResponseTypeWrapper> initVerifIndividualResponse = new ArrayList<InitialVerificationIndividualResponseTypeWrapper>();
	
	private static final String RESPONSE_KEY = "InitVerifIndividualResponse";
	
	public InitialVerificationResponseSetTypeWrapper(InitialVerificationResponseSetType initVerifResponseSetType){
		
		for(InitialVerificationIndividualResponseType response : initVerifResponseSetType.getInitialVerificationIndividualResponse()){
			this.initVerifIndividualResponse.add(new InitialVerificationIndividualResponseTypeWrapper(response));
		}
		
		ObjectFactory factory = new ObjectFactory();
		this.initVerifResponseSetType = factory.createInitialVerificationResponseSetType();
	}
	
	public InitialVerificationResponseSetType getInitialVerificationResponseSetType() {
		return initVerifResponseSetType;
	}

	public void setInitialVerificationResponseSetType(
			InitialVerificationResponseSetType initVerifResponseSetType) {
		this.initVerifResponseSetType = initVerifResponseSetType;
	}

	public List<InitialVerificationIndividualResponseTypeWrapper> getInitVerifIndividualResponse() {
		return initVerifIndividualResponse;
	}

	public void setAgency3InitVerifIndividualResponse(
			List<InitialVerificationIndividualResponseTypeWrapper> initVerifIndividualResponse) {
		this.initVerifIndividualResponse = initVerifIndividualResponse;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		JSONArray initVerifIndividualResponses = new JSONArray();
		if(this.initVerifIndividualResponse != null){
			for(InitialVerificationIndividualResponseTypeWrapper data : this.initVerifIndividualResponse){
				initVerifIndividualResponses.add(data);
			}
		}
		obj.put(RESPONSE_KEY,initVerifIndividualResponses);
		return obj.toJSONString();
	}
}
