<%@ include file="datasource.jsp" %>
<%@ page import = "java.util.*" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>


<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "CREATION_TIMESTAMP <= systimestamp";
		
	}
%>

<sql:query dataSource="jdbc/ghixDS" var="result1">
SELECT CSR, COUNT(CSR) AS TOTAL 
FROM ELIG_LEAD 
WHERE <%=whereClause%>
and csr is not null
group by csr order by csr
</sql:query> 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>"/>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Lead - CSR Results</title>


  
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load('visualization', '1', {packages: ['corechart']});

      function drawVisualization() {
              // Create and populate the data table.
              var data = google.visualization.arrayToDataTable([
                ['CSR', 'Total'],
		<c:forEach var="row" items="${result1.rows}">
		['<c:out value="${row.csr}"/>',<c:out value="${row.total}"/>],
		</c:forEach>
              ]);
            
              // Create and draw the visualization.
              new google.visualization.PieChart(document.getElementById('visualization')).
                  draw(data, {title:"CSR Results", pieSliceText:"percentage"});
      }
    

    google.setOnLoadCallback(drawVisualization);
</script>
<script type="text/javascript">
	
$(function() {
	$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
});

$(function() {
	$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
});

function setWhereClause(type){
	
	var whereClause = '';
	var filterStatement = '';
	
	//Step 1: Get the Dates
	var date1 = document.getElementById('date1').value;
	var date2 = document.getElementById('date2').value;

	if (date1 && date2) {

		if (whereClause) {
			whereClause = whereClause
					+ "and CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
					+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
			

		} else {
			whereClause = whereClause
					+ " CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
					+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
		}
		
		filterStatement = filterStatement + ' Start Date : ' + date1
		+ ' End date : ' + date2;
		
	} else {
		if (date1 && !date2) {
			if (whereClause) {

				whereClause = whereClause
					+ " and CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and systimestamp ";
				
			} else {
				whereClause = whereClause
						+ " CREATION_TIMESTAMP between to_timestamp('"
						+ date1
						+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and systimestamp ";
				+ date1 + "'";
				
			}
			
			filterStatement = filterStatement + ' Start Date : ' + date1;
			
		} else {

			if (!date1 && date2) {
				if (whereClause) {
					whereClause = whereClause
							+ " and CREATION_TIMESTAMP <= to_timestamp('"
									+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					
				} else {
					whereClause = whereClause
							+ " CREATION_TIMESTAMP <=  to_timestamp('"
									+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					
				}
				
				filterStatement = filterStatement + ' End Date : ' + date2;
			}
			
			
		}

	}
	if (!whereClause) {
		whereClause = "CREATION_TIMESTAMP <= systimestamp";
	}
	
	document.getElementById('whereClause').value = whereClause;
	document.getElementById('filterStatement').value = filterStatement;
}

</script>
</head>
<body style="font-family: Arial;border: 0 none;">
<h4>CSR Results - Shows statistics for the type of Cost Sharing Reductions applicable to households</h4>
<form method="post" action="pie_csr_results">
	<table border="1" style="border-color:#C0C0C0">
		<tbody>
		<tr><td>
				Date Start:	<input type="text" id="date1" name="date1" size="10" maxlength="10"/> 
				End: <input type="text" id="date2" name="date2" size="10" maxlength="10"/>
			</td>
			<td>
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause();"/>
				<input id="whereClause" name="whereClause" type="hidden"/>
				<input id="filterStatement" name="filterStatement" type="hidden"/>
			</td>
		</tr>
		</tbody>
	</table>
</form>
<br>
<%
	if (request.getParameter("filterStatement") != null) {
		out.println("Search Criteria : " + request.getParameter("filterStatement"));
	}
%>
<table>
<tr>
<td><div id="visualization" style="width: 600px; height: 600px;"></div></td>
<td><br><br><br>
<b>CSR Results</b>
<display:table name="${result1.rows}"  requestURI="pie_csr_results" id="table1" export="true" >
	<display:column property="csr" title="CSR" sortable="false" />
	<display:column property="total" title="Total" sortable="false" />
</display:table>

</td>
</tr>
</table>

</body>
</html>

