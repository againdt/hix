<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>

<jsp:useBean id="now" class="java.util.Date"/>
<div class="row-fluid">
	<div class="gutter10">
		<div class="">
			<h1>Make A Payment</h1>
		</div>
	</div>
</div>

<!-- <div class="row-fluid">

		<div class="span3 gutter10">
			<div class="gray">


				<h4 class="margin0">Step 1</h4>
				
					<h4>Step 2</h4>
					<h4>Step 3</h4>
				
			</div>
 -->

<div class="row-fluid">
		<div class="span3" id="sidebar">
		<h4>Steps</h4>
	<ul class="nav nav-list">
		<li>View Invoice & Pay</li>
		<li>Review Payment</li>
		<li class="active">Confirm Payment</li>
	</ul>
	</div>
	


    <div class="span9 gutter10">


	<form class="form-horizontal gutter10" id="fromreviewpayment"
				name="fromreviewpayment" action="" method="POST">
            <df:csrfToken/>
				<div class="" id="success" style="display: none">
					<!--<div class="control-group">
							<h5>Payment submitted. The confirmation details are as shown.</h5>
						</div>
						<div class="control-group">
							<label class="control-label required">Pay To</label>
						<%-- 	<label class="control-label">${invoice.invoiceNumber}</label> --%>
							<label class="control-label">${invoice.employerInvoices.invoiceNumber}</label>
						</div>
						<div class="control-group">
							<label class="control-label required">Confirm Number</label>
							<label class="control-label">${invoice.confirmNumber}</label>
						</div>
						<div class="control-group">
							<label class="control-label required">Payment Amount</label>
						<%-- 	<label class="control-label">$ ${invoice.totalAmountDue}</label> --%>
							<label class="control-label">$ ${invoice.amount}</label>
						</div>
						<div class="control-group">
							<label class="control-label required">Payment Date</label>
							<label class="control-label"><fmt:formatDate value="${now}" pattern="MM/dd/yyyy" /></label>
						</div>
						<div class="control-group">
							<label class="control-label required">Payment Method</label>
							<c:choose>
									<c:when
											test='${not empty paymentdetail.financialInfo.bankInfo}'>
											<label class="control-label">${paymentdetail.financialInfo.bankInfo.bankName}<br>
												   ${paymentdetail.financialInfo.bankInfo.accountNumber}<br>
												   ${paymentdetail.financialInfo.bankInfo.routingNumber}<br>
												   ${paymentdetail.financialInfo.bankInfo.accountType}
											</label>
									</c:when>
									<c:when
											test='${not empty paymentdetail.financialInfo.creditCardInfo}'>
											<label class="control-label">${paymentdetail.financialInfo.creditCardInfo.nameOnCard}<br>
												   ${paymentdetail.financialInfo.creditCardInfo.cardNumber}<br>
												   ${paymentdetail.financialInfo.creditCardInfo.cardType}
											</label>
									</c:when>
									<c:otherwise>
											<label class="control-label">${paymentdetail.financialInfo.address1}<br>
												   ${paymentdetail.financialInfo.city}<br>
												   ${paymentdetail.financialInfo.state}<br>
												   ${paymentdetail.financialInfo.zipcode}
											</label>
									</c:otherwise>
							</c:choose>
							
						</div>   -->							
						<div class="control-group">
								<h5>Payment submitted. The confirmation details are as shown.</h5>
						</div>
						<table class="table table-border-none">
							<tbody>	
								<tr>
									<td class="control-group txt-right">
										<label class="control-label required">Pay To</label>
									<%-- 	<label class="control-label">${invoice.invoiceNumber}</label> --%>
									</td>
									<td class="control-group txt-left">
										<strong><label class="control-label">${invoice.employerInvoices.invoiceNumber}</strong></label>
									</td>
								</tr>
								<tr>
									<td class="control-group txt-right">
										<label class="control-label required">Confirm Number</label>
									</td>
									<td class="control-group txt-left">	
										<strong><label class="control-label">${invoice.confirmNumber}</strong></label>
									</td>
								</tr>								
								<tr>	
									<td class="control-group txt-right">
										<label class="control-label required">Payment Amount</label>
									<%-- 	<label class="control-label">$ ${invoice.totalAmountDue}</label> --%>
									</td>
									<td class="control-group txt-left">
										<strong><label class="control-label">$ ${invoice.amount}</strong></label>
									</td>
								</tr>	
								<tr>
									<td class="control-group txt-right">
										<label class="control-label required">Payment Date</label>
									</td>
									<td class="control-group txt-left">	
										<strong><label class="control-label"><fmt:formatDate value="${now}" pattern="MM/dd/yyyy" /></strong></label>
									</td>
								</tr>	
								<tr>
									<td class="control-group txt-right">
										<label class="control-label required">Payment Method</label>
									</td>
									<td class="control-group txt-left autoWidthDetails">
										<strong>
										<c:choose>
												<c:when
														test='${not empty paymentdetail.financialInfo.bankInfo}'>
														<!-- <label class="control-label"> -->
														<table>
														<tr>
														<th><label class="control-label required" style="text-align: left">Account Number </label></th>
														<th><label class="control-label required" style="text-align: left">Routing Number </label></th>
														<th><label class="control-label required" style="text-align: left">AccountType </label></th>
														</tr>
														<tr>
														<td>${paymentdetail.financialInfo.bankInfo.accountNumber}</td>
														<td>${paymentdetail.financialInfo.bankInfo.routingNumber}</td>
														<td>
														<c:choose>
														<c:when test="${paymentdetail.financialInfo.bankInfo.accountType =='C'}">Checking
														</c:when>
														<c:otherwise>Saving</c:otherwise></c:choose>
														
														<%-- ${paymentdetail.financialInfo.bankInfo.accountType} --%></td>
														</tr>
														<%-- <tr>
														<td><label class="control-label">Bank Name </label>
														${paymentdetail.financialInfo.bankInfo.bankName}</td></tr> --%>
														<%-- <tr>
														<td>
														<label class="control-label">Account Number </label>
														${paymentdetail.financialInfo.bankInfo.accountNumber}</td></tr>
														<tr>
														<td>
														<label class="control-label">Routing Number </label>
														${paymentdetail.financialInfo.bankInfo.routingNumber}</td></tr>
														<tr>
														<td>
														<label class="control-label">AccountType </label>
														${paymentdetail.financialInfo.bankInfo.accountType}</td></tr> --%>
														</table>
														<!-- </label> -->
												</c:when>
												<c:when
														test='${not empty paymentdetail.financialInfo.creditCardInfo}'>
														<!-- <label class="control-label"> -->
														<table>
														<tr>
														<th><label class="control-label required" style="text-align: left">Name On Card </label></th>
														<th><label class="control-label required" style="text-align: left">Card Number </label></th>
														<th><label class="control-label required" style="text-align: left">Card Type </label></th>
														</tr>
														<tr>
														<td>${paymentdetail.financialInfo.creditCardInfo.nameOnCard}</td>
														<td>${paymentdetail.financialInfo.creditCardInfo.cardNumber}</td>
														<td>${paymentdetail.financialInfo.creditCardInfo.cardType}</td>
														</tr>
														<%-- <tr><td>
														<label class="control-label">Name On Card </label>
														${paymentdetail.financialInfo.creditCardInfo.nameOnCard}</td></tr>
														<tr><td>
														<label class="control-label">Card Number </label>
														${paymentdetail.financialInfo.creditCardInfo.cardNumber}</td></tr>
														<tr><td>
														<label class="control-label">Card Type </label>
														${paymentdetail.financialInfo.creditCardInfo.cardType}</td></tr> --%>
														</table>
														<!-- </label> -->
												</c:when>
												<c:otherwise>
														<label class="control-label">
														<table>
														<tr><td>
														<label class="control-label">Street Address1 </label>
														${paymentdetail.financialInfo.address1}</td></tr>
														<tr><td>
														<label class="control-label">City </label>
														${paymentdetail.financialInfo.city}</td></tr>
														<tr><td>
														<label class="control-label">State </label>
														${paymentdetail.financialInfo.state}</td></tr>
														<tr><td>
														<label class="control-label">Zip Code </label>
														${paymentdetail.financialInfo.zipcode}</td></tr>
														</table>
														</label>
												</c:otherwise>
										</c:choose>	
										</strong>
									</td> 
								</tr>		
							</tbody>							
						</table>
					
				</div>
				<div class="" id="error" style="display: none">
					<div class="control-group">
						<h5>Error occured on payment processing. The details are as shown.</h5>
					</div>
					<div class="control-group">
						<label class="control-label required">Error</label>
						<label class="control-label">${error}</label>
					</div>
					
				</div>
				
				
			<!-- end: this only shows when you come from 'edit payment' -->
			
				<div class="form-actions">
				    <a class="btn btn-primary"
						href="<c:url value="/shop/employer/viewpayment"/>">View Payment History</a>
				</div>
			</form>
	</div>
</div>



<div class="row-fluid">
	
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C)
					dependant on a particular Employer's eligibility to use the SHOP
					Exchange.</p>
			</div>
		</div>
	</div>

</div>
<!-- row-fluid -->

<script>

$(document).ready(function() {

	// only in case of edit radio button div has to be hidden
	var myerror = "${error}";
	if (myerror == '') {
		$('#success').attr('style', 'display:block');
		$('#error').attr('style', 'display:none');
		$('#paymentmethod').attr('placeholder', 'Chase Bank Checking');
		
	} 
	else {
		$('#success').attr('style', 'display:none');
		$('#error').attr('style', 'display:block');
		
	}
});

</script>
