package com.getinsured.hix.batch.mes.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.eligibility.at.mes.service.BatchApplicationProcessingService;



public class QueuedApplicationProcessingTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(QueuedApplicationProcessingTask.class);

	BatchApplicationProcessingService batchApplicationProcessingService;
	
	public BatchApplicationProcessingService getBatchApplicationProcessingService() {
		return batchApplicationProcessingService;
	}

	public void setBatchApplicationProcessingService(BatchApplicationProcessingService batchApplicationProcessingService) {
		this.batchApplicationProcessingService = batchApplicationProcessingService;
	}
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		LOGGER.info("QueuedApplicationProcessingTask.execute : START");

		// call the Eligibility Service for queued application processing
		batchApplicationProcessingService.executeQueuedApplication();

		LOGGER.info("QueuedApplicationProcessingTask.execute : END");

		return RepeatStatus.FINISHED;
	}

	

}
