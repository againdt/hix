package com.getinsured.iex.hub.vlp.service;

/**
 * Class to hold constants for the VLP service operations
 * 
 * @author Nikhil Talreja
 * @since 14-Jan-2014
 *
 */
public final class VLPServiceConstants {
	
	private VLPServiceConstants(){
		
	}
	
	public static final String VLP_SERVICE_DATE_FORMAT = "yyyy-MM-dd";
	
	//Constants for validations
	public static final int ALIEN_NUMBER_MIN_LEN = 9;
	public static final int ALIEN_NUMBER_MAX_LEN = 9;
	
	public static final int CIT_NUMBER_MIN_LEN = 7;
	public static final int CIT_NUMBER_MAX_LEN = 12;
	
	public static final int I94_NUMBER_MIN_LEN = 11;
	public static final int I94_NUMBER_MAX_LEN = 11;
	
	public static final int SEVIS_ID_MIN_LEN = 10;
	public static final int SEVIS_ID_MAX_LEN = 10;
	
	//Used for card number and receipt number validations
	public static final int CARD_NUMBER_MIN_LEN = 13;
	public static final int CARD_NUMBER_MAX_LEN = 13;
	public static final String CARD_NUM_REGEX = "[a-zA-Z]{3}+\\d{10}";
	
	public static final int VISA_NUMBER_MIN_LEN = 8;
	public static final int VISA_NUMBER_MAX_LEN = 8;
	
	public static final int PASSPORT_NUMBER_MIN_LEN = 6;
	public static final int PASSPORT_NUMBER_MAX_LEN = 12;
	
	public static final String COUNTRY_CODE_REGEX = "[a-zA-Z]{1,5}";
	
	public static final int NAT_NUMBER_MIN_LEN = 7;
	public static final int NAT_NUMBER_MAX_LEN = 12;
	
	public static final int DOC_DESC_MIN_LEN = 1;
	public static final int DOC_DESC_MAX_LEN = 35;
	
	public static final int FIRST_NAME_MIN_LEN = 1;
	public static final int FIRST_NAME_MAX_LEN = 50;
	
	public static final int LAST_NAME_MIN_LEN = 1;
	public static final int LAST_NAME_MAX_LEN = 50;
	
	public static final int MIDDLE_NAME_MIN_LEN = 1;
	public static final int MIDDLE_NAME_MAX_LEN = 50;
	
	public static final int DOC_FIRST_NAME_MIN_LEN = 1;
	public static final int DOC_FIRST_NAME_MAX_LEN = 25;
	
	public static final int DOC_LAST_NAME_MIN_LEN = 1;
	public static final int DOC_LAST_NAME_MAX_LEN = 40;
	
	public static final int DOC_MIDDLE_NAME_MIN_LEN = 1;
	public static final int DOC_MIDDLE_NAME_MAX_LEN = 25;
	
	public static final int AKA_MIN_LEN = 1;
	public static final int AKA_MAX_LEN = 40;
	
	public static final String FIVE_YEAR_BAR_REGEX = "true|false|TRUE|FALSE";
	
	public static final int CASE_NUMBER_MIN_LEN = 15;
	public static final int CASE_NUMBER_MAX_LEN = 15;
	public static final String CASE_NUM_REGEX = "[0-9]{13}+[A-Z]{2}";
	
	//Validation error messages
	public static final String NON_NUMERIC_ERROR_MESSAGE = "This field should be a number";
	public static final String DATE_ERROR_MESSAGE = "Invalid Date";
	
	//Constants to avoid duplication
	private static final String AND = " and ";
	private static final String CHARS = " characters";
	private static final String DIGITS = " digits";
	
	public static final String ALIEN_NUMBER_LEN_ERROR_MESSAGE = "Alien Number should be exactly "+ALIEN_NUMBER_MIN_LEN+DIGITS;
	public static final String CITI_NUMBER_LEN_ERROR_MESSAGE = "Citizenship Number should be between "+CIT_NUMBER_MIN_LEN+AND+CIT_NUMBER_MAX_LEN+DIGITS;
	public static final String I94_NUMBER_LEN_ERROR_MESSAGE = "I94 Number should be exactly "+I94_NUMBER_MAX_LEN+DIGITS;
	public static final String SEVIS_ID_LEN_ERROR_MESSAGE = "SEVIS Id should be exactly "+SEVIS_ID_MIN_LEN+DIGITS;
	public static final String CARD_NUMBER_ERROR_MESSAGE = "Card/Receipt number should be alphanumeric with first 3 characters alphabets and the remaining 10 numeric characters";
	public static final String VISA_NUMBER_LEN_ERROR_MESSAGE = "Visa Number should be exactly "+VISA_NUMBER_MIN_LEN+CHARS;
	public static final String PASSPORT_NUMBER_LEN_ERROR_MESSAGE = "Passport Number should be between "+PASSPORT_NUMBER_MIN_LEN+AND+PASSPORT_NUMBER_MAX_LEN+CHARS;
	public static final String COUNTRY_CODE_ERROR_MESSAGE = "Country of issuance should be between 1-5 letters";
	public static final String NAT_NUMBER_LEN_ERROR_MESSAGE = "Naturalization Number should be between "+NAT_NUMBER_MIN_LEN+AND+NAT_NUMBER_MAX_LEN+DIGITS;
	public static final String DOC_DESC_ERROR_MESSAGE = "Document description should be between "+DOC_DESC_MIN_LEN+AND+DOC_DESC_MAX_LEN+CHARS;
	public static final String INVALID_OBJECT_TYPE_FOR_PASSPORT_ERROR_MESSAGE = "Passport details should be of type : ";
	public static final String PASSPORT_COUNTRY_MISSING_ERROR_MESSAGE = "Passport's country of issuance is missing";
	public static final String FIRST_NAME_REQUIRED_ERROR_MESSAGE = "First name is required";
	public static final String FIRST_NAME_LEN_ERROR_MESSAGE = "First name should be between "+FIRST_NAME_MIN_LEN+AND+FIRST_NAME_MAX_LEN+CHARS;
	public static final String LAST_NAME_REQUIRED_ERROR_MESSAGE = "Last name is required";
	public static final String LAST_NAME_LEN_ERROR_MESSAGE = "Last name should be between "+LAST_NAME_MIN_LEN+AND+LAST_NAME_MAX_LEN+CHARS;
	public static final String MIDDLE_NAME_LEN_ERROR_MESSAGE = "Middle name should be between "+MIDDLE_NAME_MIN_LEN+AND+MIDDLE_NAME_MAX_LEN+CHARS;
	public static final String DATE_OF_BIRTH_REQUIRED_ERROR_MESSAGE = "Date of birth is required";
	public static final String DATE_OF_BIRTH_FUTURE_ERROR_MESSAGE = "Date of birth cannot be a future date";
	public static final String AKA_LEN_ERROR_MESSAGE = "AKA(Also known as) should be between "+AKA_MIN_LEN+AND+AKA_MAX_LEN+CHARS;
	public static final String COVERAGE_DATE_REQUIRED_ERROR_MESSAGE = "Coverage Start date is required";
	public static final String FIVE_YEAR_BAR_REQUIRED_ERROR_MESSAGE = "Fiver year bar applicable indicator is required";
	public static final String FIVE_YEAR_BAR_ERROR_MESSAGE = "Fiver year bar applicable indicator should be " +FIVE_YEAR_BAR_REGEX ;
	public static final String CASE_NUMBER_REQUIRED_ERROR_MESSAGE = "Case number is required";
	public static final String CASE_NUMBER_ERROR_MESSAGE = "Case number should be alphanumeric with first 13 characters digits and the remaining 2 upper case letters";
	public static final String DOC_FIRST_NAME_LEN_ERROR_MESSAGE = "Doc First name should be between "+DOC_FIRST_NAME_MIN_LEN+AND+DOC_FIRST_NAME_MAX_LEN+CHARS;
	public static final String DOC_LAST_NAME_LEN_ERROR_MESSAGE = "DocLast name should be between "+DOC_LAST_NAME_MIN_LEN+AND+DOC_LAST_NAME_MAX_LEN+CHARS;
	public static final String DOC_MIDDLE_NAME_LEN_ERROR_MESSAGE = "Doc Middle name should be between "+DOC_MIDDLE_NAME_MIN_LEN+AND+DOC_MIDDLE_NAME_MAX_LEN+CHARS;
	
	/*
	 * ENUM to determine the type of DHS ID type
	 * 
	 * author - Nikhil Talreja
	 * since - 13-Feb-2014
	 */
	public static enum VLP_DHS_ID_TYPES {
		I327,
		I551,
		I571,
		I766,
		CertOfCit,
		NatrOfCert,
		MacReadI551,
		TempI551,
		I94Document,
		I94UnexpForeignPassport,
		UnexpForeignPassport,
		I20,
		DS2019,
		OtherCase1,
		OtherCase2
	}
	
	public static String getAvailableDHSIdTypes(){
		return "I327,I551,I571,I766,CertOfCit,NatrOfCert,MacReadI551,TempI551,I94Document,I94UnexpForeignPassport,UnexpForeignPassport,I20,DS2019,OtherCase1,OtherCase2";
	}
}
