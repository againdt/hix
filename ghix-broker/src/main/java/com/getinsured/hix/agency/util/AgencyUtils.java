package com.getinsured.hix.agency.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component
public final class AgencyUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AgencyUtils.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired 
	private CommentTargetService commentTargetService;
	
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final String TARGET_NAME = "AGENCY_ASSISTANT";
		
	
	public Comment saveComments(String targetId, String commentText) {
		List<Comment> comments = null;
		AccountUser accountUser = null;
		String commenterName = "";
		CommentTarget commentTarget = null;
		
		try {
			commentTarget = commentTargetService.findByTargetIdAndTargetType(Long.valueOf(targetId), TargetName.valueOf(TARGET_NAME));
			if(commentTarget == null){
				commentTarget =  new CommentTarget();
				commentTarget.setTargetId(Long.valueOf(targetId));
				commentTarget.setTargetName(TargetName.valueOf(TARGET_NAME));
			}
			
			if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
				comments = new ArrayList<Comment>();
			} else{
				comments = commentTarget.getComments();
			}

			accountUser = userService.getLoggedInUser();
				
			String firstName = accountUser.getFirstName();
			String lastName  = accountUser.getLastName();
			commenterName += (StringUtils.isBlank(firstName)) ? "" :  firstName+" ";
			commenterName += (StringUtils.isBlank(lastName)) ? "" : lastName;
			
			//Checking if user's name is blank
			if(StringUtils.isBlank(commenterName)){
				commenterName = "Anonymous User";
			}
			
			Comment comment = new Comment();
			comment.setComentedBy(accountUser.getId());
			comment.setCommenterName(commenterName);
			
			//Fetching first 4000 char for comment text;
			int beginIndex = 0;
			int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
			commentText = commentText.substring(beginIndex, endIndex);
			comment.setComment(commentText);
			
			comment.setCommentTarget(commentTarget);
			
			comments.add(comment);
			commentTarget.setComments(comments);
			commentTarget = commentTargetService.saveCommentTarget(commentTarget);
			List<Comment> list = commentTarget.getComments();
			Collections.sort(list);
			
			return list.get(0);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while saving comments.", ex);
			throw new GIRuntimeException("Exception occured while saving comments.", ex);
		}
	}
}
