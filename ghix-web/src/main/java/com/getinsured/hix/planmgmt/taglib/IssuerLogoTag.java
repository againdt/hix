package com.getinsured.hix.planmgmt.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.SecurityUtil;

public class IssuerLogoTag extends TagSupport {
	
	private String issuerId;
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerLogoTag.class);
	
	private static final long serialVersionUID = 1;
	private static final String TEMPLATE = "<ISSUERLOGO>";
	

	private ApplicationContext applicationContext;
	private IssuerService issuerService;
	private UserService userService;
	private PlanMgmtService planMgmtService;
	@Override
    public int doStartTag() throws JspException {
		LOGGER.info("Issuer Id is "+ SecurityUtil.sanitizeForLogging(String.valueOf(issuerId)));
		applicationContext = RequestContextUtils.getWebApplicationContext(
				pageContext.getRequest(),
				pageContext.getServletContext()
				);
		try{
		issuerService = applicationContext.getBean("issuerService",IssuerService.class);
		if (issuerId == null || issuerId.length()==0)
		{
			String issuerName = "";
			LOGGER.info("Issue name not provided in the Taglib,fetching from logged in user.");
			if (issuerService.isIssuerRepLoggedIn()) 
			{	
				userService = applicationContext.getBean("userService",UserService.class);
			try 
			{
				issuerName = issuerService.getIssuerNameByRepUserId(userService.getLoggedInUser().getId());
			} 
			catch (InvalidUserException e)
			{
				LOGGER.error("No user logged in");
			}
			LOGGER.info("Logged in Issuer name is "+ SecurityUtil.sanitizeForLogging(String.valueOf(issuerName)));
			}
		}
		}catch(Exception e){
			LOGGER.error("Issuer LOGO Error"+e);
		}
		
		try{			
			AccountUser user = userService.getLoggedInUser();
		 	Issuer currUserIssuerObj = issuerService.getIssuer(user);
			//String encodedFileName = issuerService.getIssuerLogoByName(getIssuerName());//
			String messageStart = "<div><img class='resize-img' src=\'"+pageContext.getServletContext().getContextPath()+"/admin/issuer/company/profile/logo/hid/";
			String messageEnd =		"' /></div>";
			JspWriter out = pageContext.getOut();
			String logoURL = String.valueOf(currUserIssuerObj.getHiosIssuerId()); //getIssuerLogo(encodedFileName);
			
			if (logoURL != null && logoURL.trim().length()>0)
			{
				logoURL = messageStart + logoURL + messageEnd;				
				out.println(IssuerLogoTag.TEMPLATE.replaceAll("<ISSUERLOGO>", logoURL));
			}
			else
			{
				out.println(IssuerLogoTag.TEMPLATE.replaceAll("<ISSUERLOGO>", ""));
			}
		}
		catch (Exception e) {
			LOGGER.error("Error while decrypting issuer logo file name.");
        }
        return SKIP_BODY;
	}
	
	public String getIssuerLogo(String encodedFileName){
		String logo = "";
		try{
		planMgmtService = applicationContext.getBean("planmgmtService",PlanMgmtService.class);
		String logoPath = planMgmtService.getLogoPath();
		
		if (encodedFileName!=null) {
			logo = planMgmtService.getActualLogoName(encodedFileName);
			if(!logo.isEmpty()){
				logo = logoPath + logo;
			}				
		}
		}catch(Exception e){
			LOGGER.error("Issuer Logo error"+e);
		}
		return logo;	
	}

}
