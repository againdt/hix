import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import {injectIntl} from 'react-intl';

import SummaryItem from './SummaryItem';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Components

// Utils


const ApplyingForHealthCoverageSummary = props => {

    const {
        additionalElement,
        bloodRelationships,
        householdMembers,
        intl,
        relations: {
            loadedRelations,
            loadedRelationsWholeList
        }
    } = props;

    return (
        <div className="table-res-wrap">
            <div className="summary__block" id="householdMemberSummary">
                <div className="usa-grid"/>
                <SummaryItem
                    textCode="label.name"
                    value={['Relationship', 'Date of Birth', 'Seeking Coverage']}
                    valueCode={['label.relationship', 'label.dob', 'label.seekingCoverage']}
                    classes2={{
                        spanClass1: 'usa-width-one-third first-cell',
                        spanClass: 'usa-width-one-sixth txt-center',
                        divClass: ''
                    }}

                />
                <div className="usa-grid usa-width-one-whole divider"/>
                {
                    householdMembers.map((member, i) => {
                        const {
                            applyingForCoverageIndicator,
                            name,
                            dateOfBirth
                        } = member;
                        // console.log('bloodRelationships', bloodRelationships, loadedRelationsWholeList, loadedRelations);
                        const relationToHeadOfHousehold = bloodRelationships
                            .find(rel => rel.relatedPersonId === '1' && rel.individualPersonId === String(i + 1));

                        const getRelation = () => {
                            const resolvedRelations = loadedRelationsWholeList && loadedRelationsWholeList.length > 0 ? loadedRelationsWholeList : loadedRelations;
                            let res = resolvedRelations
                                .find(lr => lr.code === (relationToHeadOfHousehold && relationToHeadOfHousehold.relation));
                            return res ? res.text : '';
                        };

                        const relationTxt = getRelation();
                        const intlRelationTxt = relationTxt === 'Self' ? intl.formatMessage({id: 'label.self'}) : relationTxt;

                        return (
                            <SummaryItem
                                key={`${name.firstName}_${i}`}
                                category={normalizeNameOfPerson(name)}
                                value={[
                                    intlRelationTxt,
                                    moment(dateOfBirth).format('MM/DD/YYYY'),
                                    applyingForCoverageIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})
                                ]}
                                classes2={{
                                    spanClass1: 'usa-width-one-third first-cell',
                                    spanClass: 'usa-width-one-sixth txt-center',
                                    divClass: ''
                                }}
                                additionalElement={
                                    additionalElement ?
                                        React.cloneElement(additionalElement, { args: ['editMember', i, `household-member-${i}`] }) :
                                        null
                                }
                                additionalElementClassName={'usa-width-one-sixth summary-button'}
                            />
                        );
                    })
                }
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    relations: state.relations
});

const IntlApp = injectIntl(ApplyingForHealthCoverageSummary);

export default connect(mapStateToProps)(IntlApp);
