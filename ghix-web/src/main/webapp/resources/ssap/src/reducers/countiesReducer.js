import { countiesActionTypes } from '../constants/actionTypes';
import { PAGES } from '../constants/pagesList';

const initialState = {
    [PAGES.PRIMARY_CONTACT_INFORMATION_PAGE]: [],
    [PAGES.OTHER_ADDRESSES_PAGE]: []
};

export default (state = initialState, action) => {
    switch(action.type) {
    case countiesActionTypes.SET_COUNTIES:
        return action.state ?
            {
                ...state,
                [action.state]: action.counties
            } :
            {
                ...state,
                ...action.counties
            };
    case countiesActionTypes.SET_COUNTY_FOUND_BY_STATE_AND_ZIPCODE:
        return {
            ...state,
            [action.pageName]: action.payload
        };
    case countiesActionTypes.REMOVE_COUNTY:
        return {
            ...state,
            [PAGES.OTHER_ADDRESSES_PAGE]: state[PAGES.OTHER_ADDRESSES_PAGE].filter(county => {
                const [ key ] = Object.entries(county)[0];
                return key !== String(action.index);
            })
        };
    case countiesActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};