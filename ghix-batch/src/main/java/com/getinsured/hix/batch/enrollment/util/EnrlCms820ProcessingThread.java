package com.getinsured.hix.batch.enrollment.util;

import java.io.File;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;

import com.getinsured.hix.batch.enrollment.service.EnrlCms820Service;

public class EnrlCms820ProcessingThread implements Callable<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlCms820ProcessingThread.class);

	private String filePath = null;
	private Long jobId = null;
	private JobService jobService = null;
	private StepExecution stepExecution = null;
	private File archivePath = null;
	private EnrlCms820Service enrlCms820Service;

	public EnrlCms820ProcessingThread(String filePath, Long jobId, JobService jobService, StepExecution stepExecution,
			File archivePath, EnrlCms820Service enrlCms820Service) {
		super();
		this.filePath = filePath;
		this.jobId = jobId;
		this.jobService = jobService;
		this.stepExecution = stepExecution;
		this.archivePath = archivePath;
		this.enrlCms820Service = enrlCms820Service;
	}

	@Override
	public String call() throws Exception {
		try {
			return enrlCms820Service.populateCms820Data(filePath, jobId, jobService, stepExecution, archivePath);
		} catch (Exception e) {
			LOGGER.error("Exception occurred in EnrlCms820ProcessingThread: ", e);
		}
		return null;
	}

}
