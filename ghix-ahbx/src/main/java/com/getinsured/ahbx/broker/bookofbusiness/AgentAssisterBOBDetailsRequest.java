//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.30 at 02:39:50 PM IST 
//


package com.getinsured.ahbx.broker.bookofbusiness;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for agentAssisterBOBDetailsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="agentAssisterBOBDetailsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="recordId" type="{http://ahbx.accenture.com/AHBXValidation}LongLength10" minOccurs="0"/>
 *         &lt;element name="recordType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="designationStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pageNumber" type="{http://ahbx.accenture.com/AHBXValidation}LongLength5" minOccurs="0"/>
 *         &lt;element name="pageSize" type="{http://ahbx.accenture.com/AHBXValidation}LongLength5" minOccurs="0"/>
 *         &lt;element name="firstName" type="{http://ahbx.accenture.com/AHBXValidation}StringLength50" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://ahbx.accenture.com/AHBXValidation}StringLength50" minOccurs="0"/>
 *         &lt;element name="applicationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="applicationStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eligibilityStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currentStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nextStep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dueDate" type="{http://ahbx.accenture.com/AHBXValidation}Date" minOccurs="0"/>
 *         &lt;element name="issuerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="applicationYear" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enrlCounselorFname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enrlCounselorLname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestSentFrmDt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestSentToDt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="activeSinceFrmDt" type="{http://ahbx.accenture.com/AHBXValidation}Date" minOccurs="0"/>
 *         &lt;element name="activeSinceToDt" type="{http://ahbx.accenture.com/AHBXValidation}Date" minOccurs="0"/>
 *         &lt;element name="inactiveSinceFrmDt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inactiveSinceToDt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sortOrder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sortBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enrollmentStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="individualIds">
 *         &lt;simpleType>
 *           &lt;list itemType="{http://www.w3.org/2001/XMLSchema}long" />
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agentAssisterBOBDetailsRequest", propOrder = {
    "recordId",
    "recordType",
    "designationStatus",
    "pageNumber",
    "pageSize",
    "firstName",
    "lastName",
    "applicationType",
    "applicationStatus",
    "eligibilityStatus",
    "currentStatus",
    "nextStep",
    "dueDate",
    "issuerName",
    "applicationYear",
    "enrlCounselorFname",
    "enrlCounselorLname",
    "requestSentFrmDt",
    "requestSentToDt",
    "activeSinceFrmDt",
    "activeSinceToDt",
    "inactiveSinceFrmDt",
    "inactiveSinceToDt",
    "sortOrder",
    "sortBy",
    "enrollmentStatus"
})
public class AgentAssisterBOBDetailsRequest {

    protected Long recordId;
    protected String recordType;
    protected String designationStatus;
    @XmlSchemaType(name = "long")
    protected Integer pageNumber;
    @XmlSchemaType(name = "long")
    protected Integer pageSize;
    protected String firstName;
    protected String lastName;
    protected String applicationType;
    protected String applicationStatus;
    protected String eligibilityStatus;
    protected String currentStatus;
    protected String nextStep;
    protected String dueDate;
    protected String issuerName;
    protected String applicationYear;
    protected String enrlCounselorFname;
    protected String enrlCounselorLname;
    protected String requestSentFrmDt;
    protected String requestSentToDt;
    protected String activeSinceFrmDt;
    protected String activeSinceToDt;
    protected String inactiveSinceFrmDt;
    protected String inactiveSinceToDt;
    protected String sortOrder;
    protected String sortBy;
    protected String enrollmentStatus;
    @XmlAttribute(name = "individualIds")
    protected List<Long> individualIds;

    /**
     * Gets the value of the recordId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRecordId() {
        return recordId;
    }

    /**
     * Sets the value of the recordId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRecordId(Long value) {
        this.recordId = value;
    }

    /**
     * Gets the value of the recordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * Sets the value of the recordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordType(String value) {
        this.recordType = value;
    }

    /**
     * Gets the value of the designationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesignationStatus() {
        return designationStatus;
    }

    /**
     * Sets the value of the designationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesignationStatus(String value) {
        this.designationStatus = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageNumber(Integer value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPageSize(Integer value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the applicationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationType() {
        return applicationType;
    }

    /**
     * Sets the value of the applicationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationType(String value) {
        this.applicationType = value;
    }

    /**
     * Gets the value of the applicationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationStatus() {
        return applicationStatus;
    }

    /**
     * Sets the value of the applicationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationStatus(String value) {
        this.applicationStatus = value;
    }

    /**
     * Gets the value of the eligibilityStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEligibilityStatus() {
        return eligibilityStatus;
    }

    /**
     * Sets the value of the eligibilityStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEligibilityStatus(String value) {
        this.eligibilityStatus = value;
    }

    /**
     * Gets the value of the currentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentStatus() {
        return currentStatus;
    }

    /**
     * Sets the value of the currentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentStatus(String value) {
        this.currentStatus = value;
    }

    /**
     * Gets the value of the nextStep property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextStep() {
        return nextStep;
    }

    /**
     * Sets the value of the nextStep property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextStep(String value) {
        this.nextStep = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDueDate(String value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the issuerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerName() {
        return issuerName;
    }

    /**
     * Sets the value of the issuerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerName(String value) {
        this.issuerName = value;
    }

    /**
     * Gets the value of the applicationYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationYear() {
        return applicationYear;
    }

    /**
     * Sets the value of the applicationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationYear(String value) {
        this.applicationYear = value;
    }

    /**
     * Gets the value of the enrlCounselorFname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnrlCounselorFname() {
        return enrlCounselorFname;
    }

    /**
     * Sets the value of the enrlCounselorFname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnrlCounselorFname(String value) {
        this.enrlCounselorFname = value;
    }

    /**
     * Gets the value of the enrlCounselorLname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnrlCounselorLname() {
        return enrlCounselorLname;
    }

    /**
     * Sets the value of the enrlCounselorLname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnrlCounselorLname(String value) {
        this.enrlCounselorLname = value;
    }

    /**
     * Gets the value of the requestSentFrmDt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestSentFrmDt() {
        return requestSentFrmDt;
    }

    /**
     * Sets the value of the requestSentFrmDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestSentFrmDt(String value) {
        this.requestSentFrmDt = value;
    }

    /**
     * Gets the value of the requestSentToDt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestSentToDt() {
        return requestSentToDt;
    }

    /**
     * Sets the value of the requestSentToDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestSentToDt(String value) {
        this.requestSentToDt = value;
    }

    /**
     * Gets the value of the activeSinceFrmDt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveSinceFrmDt() {
        return activeSinceFrmDt;
    }

    /**
     * Sets the value of the activeSinceFrmDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveSinceFrmDt(String value) {
        this.activeSinceFrmDt = value;
    }

    /**
     * Gets the value of the activeSinceToDt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveSinceToDt() {
        return activeSinceToDt;
    }

    /**
     * Sets the value of the activeSinceToDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveSinceToDt(String value) {
        this.activeSinceToDt = value;
    }

    /**
     * Gets the value of the inactiveSinceFrmDt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInactiveSinceFrmDt() {
        return inactiveSinceFrmDt;
    }

    /**
     * Sets the value of the inactiveSinceFrmDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInactiveSinceFrmDt(String value) {
        this.inactiveSinceFrmDt = value;
    }

    /**
     * Gets the value of the inactiveSinceToDt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInactiveSinceToDt() {
        return inactiveSinceToDt;
    }

    /**
     * Sets the value of the inactiveSinceToDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInactiveSinceToDt(String value) {
        this.inactiveSinceToDt = value;
    }

    /**
     * Gets the value of the sortOrder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortOrder() {
        return sortOrder;
    }

    /**
     * Sets the value of the sortOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortOrder(String value) {
        this.sortOrder = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortBy(String value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the enrollmentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnrollmentStatus() {
        return enrollmentStatus;
    }

    /**
     * Sets the value of the enrollmentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnrollmentStatus(String value) {
        this.enrollmentStatus = value;
    }

    /**
     * Gets the value of the individualIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the individualIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIndividualIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getIndividualIds() {
        if (individualIds == null) {
            individualIds = new ArrayList<Long>();
        }
        return this.individualIds;
    }

    public void setIndividualIds(List<Long> individualIds) {
		this.individualIds = individualIds;
	}

}
