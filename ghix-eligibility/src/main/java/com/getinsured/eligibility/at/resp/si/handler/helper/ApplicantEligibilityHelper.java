package com.getinsured.eligibility.at.resp.si.handler.helper;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicationService;
import com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.at.resp.si.handler.mapper.EligibilityProgramMapper;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.CHIPEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.MedicaidMAGIEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.MedicaidNonMAGIEligibilityType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

@Component
public class ApplicantEligibilityHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicantEligibilityHelper.class);

	private static final String NONE = "NONE";
	private static final String QHP = "QHP";
	private static final String CSR = "CSR";
	private static final String APTC = "APTC";
	private static final String MEDICAID = "MEDICAID";
	private static final Set<String> NATIVE_AMERICAN_CSR = new HashSet<>();
	private static final Set<String> NON_NATIVE_AMERICAN_CSR = new HashSet<>();
	private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	
	
	static {
		NATIVE_AMERICAN_CSR.add("CS2"); NATIVE_AMERICAN_CSR.add("CS3");
	}
	static{
		NON_NATIVE_AMERICAN_CSR.add("CS4"); NON_NATIVE_AMERICAN_CSR.add("CS5"); NON_NATIVE_AMERICAN_CSR.add("CS6");
	}

	@Autowired private IEligibilityProgram iEligibilityProgram;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private GIWSPayloadRepository giwsPayloadRepository;
	@Autowired private SsapApplicationService ssapApplicationService;

	public List<com.getinsured.eligibility.model.EligibilityProgram> processEligibility(TaxHouseholdMember atTaxPerson, TaxHouseholdMember ssapApplicant, Integer giWsPayloadId, Map<String, Object> hhLevelEliDetails) {

		Long ssapApplicantId = ssapApplicant.getPersonInfo().getPrimaryKey();

		// re-bind entity with sessions...
		SsapApplicant ssapApplicantDO = ssapApplicantRepository.findOne(ssapApplicantId);
		List<com.getinsured.eligibility.model.EligibilityProgram> eliModel = iEligibilityProgram.getEligibilities(ssapApplicantId);

		Map<String, com.getinsured.eligibility.model.EligibilityProgram> eligibilitiesDOMap = new HashMap<>();
		for (com.getinsured.eligibility.model.EligibilityProgram eligibiltyProgram : eliModel) {
			eligibilitiesDOMap.put(eligibiltyProgram.getEligibilityType(), eligibiltyProgram);
		}

		
		String applicantCSLevel = null;
		boolean isExchangeElig = false;
		boolean isAPTCElig = false;
		boolean isStateSubsidyElig = false;
		boolean isNativeAmerican = ReferralConstants.Y.equalsIgnoreCase(ssapApplicantDO.getNativeAmericanFlag());
		List<com.getinsured.eligibility.model.EligibilityProgram> eliModelModified = new ArrayList<>();
		
		AccountTransferRequestPayloadType accountTransferRequest = this.extractSoapBodyFromGiWsPayload(giWsPayloadId);
		if("NV".equalsIgnoreCase(stateCode) && accountTransferRequest != null && (this.isNVInboundATRequest(accountTransferRequest) == true)){
			com.getinsured.eligibility.model.EligibilityProgram eligibilityProgram = this.updateInboundATRequest(ssapApplicantDO, accountTransferRequest);
			eliModelModified.add(eligibilityProgram);
		}else{
			Map<String, EligibilityProgram> atTaxMemberEligibilities = atTaxPerson.getEligibilityProgramMap();
			for (String eligibilityProgramIndicatorType : atTaxMemberEligibilities.keySet()) {
				Long id = null;

				EligibilityProgram eligibilityProgram = atTaxMemberEligibilities.get(eligibilityProgramIndicatorType);

				com.getinsured.eligibility.model.EligibilityProgram eligibilityDO = eligibilitiesDOMap.get(eligibilityProgramIndicatorType);

				com.getinsured.eligibility.model.EligibilityProgramGiwspayload eligibilityProgramGiwspayloadDO = null;
				if (eligibilityDO != null) {
					id = eligibilityDO.getId();

					if (!eligibilityDO.getEligibilityProgramGiwspayload().isEmpty()){
						eligibilityProgramGiwspayloadDO = eligibilityDO.getEligibilityProgramGiwspayload().get(0);
					}
				}
				if (null == eligibilityProgramGiwspayloadDO) {
					eligibilityProgramGiwspayloadDO = new com.getinsured.eligibility.model.EligibilityProgramGiwspayload();
				}
				
				if("ExchangeEligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType()) && Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())) {
					isExchangeElig = true;
				}
				
				if("APTCEligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType()) && Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())) {
					isAPTCElig=true;
					hhLevelEliDetails.put("APTCProgram", eligibilityProgram);
				}

				// Extract Max APTC amount and CSR Level..
				BigDecimal maxAPTCAmt = (BigDecimal) hhLevelEliDetails.get(EligibilityConstants.MAX_APTC_AMT);
				if (maxAPTCAmt == null){
					hhLevelEliDetails.put(EligibilityConstants.MAX_APTC_AMT, eligibilityProgram.getMaxAPTCAmount());
				}

				if("StateSubsidyEligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType()) && Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())) {
					isStateSubsidyElig=true;
					hhLevelEliDetails.put("StateSubsidyProgram", eligibilityProgram);
				}

				BigDecimal maxStateSubsidyAmt = (BigDecimal) hhLevelEliDetails.get(EligibilityConstants.MAX_STATE_SUBSIDY_AMT);
				if (maxStateSubsidyAmt == null){
					hhLevelEliDetails.put(EligibilityConstants.MAX_STATE_SUBSIDY_AMT, eligibilityProgram.getMaxStateSubsidyAmount());
				}

				if("CSREligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType()) && Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())) {
					applicantCSLevel = eligibilityProgram.getCsrLevel();
					/*if (NATIVE_AMERICAN_CSR.contains(eligibilityProgram.getCsrLevel())){
						nativeAmericanCSRLevels = eligibilityProgram.getCsrLevel();
					} else if (NON_NATIVE_AMERICAN_CSR.contains(eligibilityProgram.getCsrLevel())){
						nonNativeAmericanCSRLevels = eligibilityProgram.getCsrLevel();
					}*/
				}

				if("CSREligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType())
						&& isNativeAmerican
						&& !Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())){
					applicantCSLevel = ReferralConstants.CS3;
					eligibilityProgram.setEligibilityIndicator("TRUE");
					eligibilityProgram.setIneligibleReason(null);
				}
				
				eligibilityDO = EligibilityProgramMapper.map(eligibilityProgram, ssapApplicantDO, id);

				// Link eligibilityDO with GiWsPayload object with join table eligibilityProgramGiwspayloadDO
				eligibilityProgramGiwspayloadDO.setEligibilityProgram(eligibilityDO);
				eligibilityProgramGiwspayloadDO.setGiWsPayloadId(giWsPayloadId);

				List<com.getinsured.eligibility.model.EligibilityProgramGiwspayload> eligibilityProgramGiwspayloadDOList = new ArrayList<>();
				eligibilityProgramGiwspayloadDOList.add(eligibilityProgramGiwspayloadDO);

				eligibilityDO.setEligibilityProgramGiwspayload(eligibilityProgramGiwspayloadDOList);

				eliModelModified.add(eligibilityDO);
			}
		}
		//String csrLevel = (String) hhLevelEliDetails.get(EligibilityConstants.CSR_LEVEL);
		/**
		 * Set CSR Level for application in hhLevelEliDetails map
		 * Non Native CSR takes precedence
		 * Check whether we have set csrLevel in hhLevelEliDetails map
		 */
		/*if (!StringUtils.isEmpty(csrLevel)){
			if (!NON_NATIVE_AMERICAN_CSR.contains(csrLevel)){
				if (!StringUtils.isEmpty(nonNativeAmericanCSRLevels)){
					hhLevelEliDetails.put(EligibilityConstants.CSR_LEVEL, nonNativeAmericanCSRLevels);
				}
			}
		} else {
			if (!StringUtils.isEmpty(nonNativeAmericanCSRLevels)){
				hhLevelEliDetails.put(EligibilityConstants.CSR_LEVEL, nonNativeAmericanCSRLevels);
			} else if (!StringUtils.isEmpty(nativeAmericanCSRLevels)) {
				hhLevelEliDetails.put(EligibilityConstants.CSR_LEVEL, nativeAmericanCSRLevels);
			}
		}*/
		
		Boolean exchangeEligibilityEnable = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ELIGIBILITY_ENABLE));
		Set<String> hhCSlevel = (HashSet<String>)hhLevelEliDetails.get("HHCSLevel");
		
		if (exchangeEligibilityEnable) {
			if (isExchangeElig) {
				if(isAPTCElig) {
					Boolean isApplicantAPTCElig = (Boolean) hhLevelEliDetails.get("APTCElig");
					if (isApplicantAPTCElig == null) {
						hhLevelEliDetails.put("APTCElig", Boolean.TRUE);
					}
				}
				if (!StringUtils.isEmpty(applicantCSLevel)) {
					hhCSlevel.add(applicantCSLevel);
				}
			}
		} else {
			if (isAPTCElig) {
				Boolean isApplicantAPTCElig = (Boolean) hhLevelEliDetails.get("APTCElig");
				if (isApplicantAPTCElig == null) {
					hhLevelEliDetails.put("APTCElig", Boolean.TRUE);
				}
			}
			if (!StringUtils.isEmpty(applicantCSLevel)) {
				hhCSlevel.add(applicantCSLevel);
			}
		}

		if(isExchangeElig && isStateSubsidyElig) {
			Boolean isApplicantStateSubsidyElig = (Boolean) hhLevelEliDetails.get("StateSubsidyElig");
			if(isApplicantStateSubsidyElig==null) {
				hhLevelEliDetails.put("StateSubsidyElig",Boolean.TRUE);
			}
		}
		
		
		boolean nonQhpIndicator = false;
		
		@SuppressWarnings("unchecked")
		List<Map<String,Boolean>> qhpListMap = (List<Map<String,Boolean>>)hhLevelEliDetails.get("nonQHPListMap");
		
		if (qhpListMap != null && !qhpListMap.isEmpty()) {
			for (Map<String, Boolean> qhpMap : qhpListMap) {
				if (qhpMap.containsKey(ssapApplicantDO.getExternalApplicantId())) {
					nonQhpIndicator = qhpMap.get(ssapApplicantDO.getExternalApplicantId()) != null
							? qhpMap.get(ssapApplicantDO.getExternalApplicantId())
							: false;
				}
			}
		}
	
		calculateApplicantLevelEligibility(ssapApplicantDO, eliModelModified, applicantCSLevel);
		
		Long ssapApplicationId = ssapApplicantDO.getSsapApplication().getId();
		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByAppId(ssapApplicationId);
		SsapApplication ssapApplication = !ssapApplicationList.isEmpty() ? ssapApplicationList.get(0) : null;
		setSsapApplicationStartDate(ssapApplication, eliModelModified);
		
		boolean isExchangeEligibilityTypePresent = false;
		String applicationType = ssapApplication.getApplicationType() != null ? ssapApplication.getApplicationType() : null;

		for (com.getinsured.eligibility.model.EligibilityProgram elgProgram : eliModelModified) {
			if (elgProgram.getEligibilityType().equalsIgnoreCase("ExchangeEligibilityType")) {
				isExchangeEligibilityTypePresent = true;
				break;
			}
		}

		if (!isExchangeEligibilityTypePresent) {
			Boolean isAptcEligibile = false;
			Boolean isCSREligibile = false;
			Boolean isExchangeEligible = false;
			Date eligibilityEndDate = null;
			Date eligibilityStartDate = null;

			for (com.getinsured.eligibility.model.EligibilityProgram eligibilityProgram : eliModelModified) {
				if ("CSREligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType())
						&& Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())) {
					isCSREligibile = true;
					eligibilityEndDate = eligibilityProgram.getEligibilityEndDate();
					eligibilityStartDate = eligibilityProgram.getEligibilityStartDate();

				} else if (("APTCEligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType())
						&& Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator()))) {
					isAptcEligibile = true;
					eligibilityEndDate = eligibilityProgram.getEligibilityEndDate();
					eligibilityStartDate = eligibilityProgram.getEligibilityStartDate();
				}
				if (eligibilityEndDate == null && eligibilityProgram.getEligibilityEndDate() != null
						&& eligibilityProgram.getEligibilityStartDate() != null) {
					eligibilityEndDate = eligibilityProgram.getEligibilityEndDate();
					eligibilityStartDate = eligibilityProgram.getEligibilityStartDate();
				}
			}

			if (isAptcEligibile || isCSREligibile) {
				isExchangeEligible = true;
			} else {
				isExchangeEligible = false;
			}

			com.getinsured.eligibility.model.EligibilityProgram newEligibilityProgram = new com.getinsured.eligibility.model.EligibilityProgram();
			newEligibilityProgram.setEligibilityType("ExchangeEligibilityType");
			newEligibilityProgram.setEligibilityIndicator(isExchangeEligible.toString().toUpperCase());
			newEligibilityProgram.setEligibilityStartDate(eligibilityStartDate);
			newEligibilityProgram.setEligibilityEndDate(eligibilityEndDate);
			newEligibilityProgram.setEligibilityDeterminationDate(null);
			newEligibilityProgram.setSsapApplicant(ssapApplicantDO);
			eliModelModified.add(newEligibilityProgram);
		}
			
		return iEligibilityProgram.save(eliModelModified);
	}

	private void calculateApplicantLevelEligibility( SsapApplicant ssapApplicantDO, List<com.getinsured.eligibility.model.EligibilityProgram> eliModelModified
			, String applicantCSLevel) {

		//String applicantCSRLevel = null;
		//Removed if condition for HIX-109720
		//if(ReferralConstants.N.equals(ssapApplicantDO.getSsapApplication().getFinancialAssistanceFlag())) {
			//ssapApplicantDO.setCsrLevel(null);
		//}else {
			/*if (!StringUtils.isEmpty(nonNativeAmericanCSRLevels)){
				applicantCSRLevel = nonNativeAmericanCSRLevels;
			} else if (!StringUtils.isEmpty(nativeAmericanCSRLevels)) {
				applicantCSRLevel = nativeAmericanCSRLevels;
			}*/
			ssapApplicantDO.setCsrLevel(applicantCSLevel);
		//}
		ssapApplicantDO.setEligibilityStatus(determineApplicantLevelEligibility(eliModelModified));
		ssapApplicantRepository.save(ssapApplicantDO);
	}

	private static String determineApplicantLevelEligibility(List<com.getinsured.eligibility.model.EligibilityProgram> eliModelModified) {

		Set<String> applicantLevelEligibility =	EligibilityStatusHelper.prepareApplicantEligibilitySet(eliModelModified);
		
		applicantLevelEligibility = checkExchangeEligibility(eliModelModified, applicantLevelEligibility);

		if (applicantLevelEligibility.contains(APTC) || applicantLevelEligibility.contains(CSR) || applicantLevelEligibility.contains(QHP)){
			return QHP;
		} else if (applicantLevelEligibility.contains(MEDICAID)){
			return MEDICAID;
		} else {
			return NONE;
		}
	}

	private static Set<String> checkExchangeEligibility(List<com.getinsured.eligibility.model.EligibilityProgram> eligibilityList, Set<String> eligibilities){
		Set<String> applicationEligibility = new HashSet<>();

		for (com.getinsured.eligibility.model.EligibilityProgram ep : eligibilityList) {
			String type = EligibilityStatusHelper.readEligibilityType(ep.getEligibilityType());
			if (StringUtils.equalsIgnoreCase(type, QHP) && StringUtils.equalsIgnoreCase(ep.getEligibilityIndicator(), EligibilityConstants.FALSE)){
				for(String eligibility:eligibilities){
					if (!StringUtils.equalsIgnoreCase(eligibility, QHP) && !StringUtils.equalsIgnoreCase(eligibility, APTC) && !StringUtils.equalsIgnoreCase(eligibility, CSR)){
						applicationEligibility.add(eligibility);
					}
				}
				return applicationEligibility;
			} 
		}
		
		return eligibilities;
	}
	
	public void setSsapApplicationStartDate(SsapApplication ssapApplication, List<com.getinsured.eligibility.model.EligibilityProgram> programEligibilityList) {
		
		if(ssapApplication != null && !programEligibilityList.isEmpty()) {
			
			Boolean exchangeEligibilityEnable = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ELIGIBILITY_ENABLE));
						
			Date currentDate = new TSDate();

			List<Date> eligibilityStartDateList = new ArrayList<Date>();	
			
			for (com.getinsured.eligibility.model.EligibilityProgram eligibilityProgram : programEligibilityList) {
				if (exchangeEligibilityEnable) {
					if ("ExchangeEligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType())
							&& Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())) {
						eligibilityStartDateList.add(eligibilityProgram.getEligibilityStartDate());
					} else {
						if ("APTCEligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType())
								&& Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())) {
							eligibilityStartDateList.add(eligibilityProgram.getEligibilityStartDate());
						}
					}
				} else {
					if ("APTCEligibilityType".equalsIgnoreCase(eligibilityProgram.getEligibilityType())
							&& Boolean.parseBoolean(eligibilityProgram.getEligibilityIndicator())) {
						eligibilityStartDateList.add(eligibilityProgram.getEligibilityStartDate());
					}
				}
			}
					
			Date minDate = eligibilityStartDateList != null && !eligibilityStartDateList.isEmpty() ? Collections.min(eligibilityStartDateList) : null;
			
			if (minDate != null) {
				ssapApplication.setStartDate(new Timestamp(minDate.getTime()));
			} else if(ssapApplication.getStartDate() == null) {
				ssapApplication.setStartDate(new Timestamp(currentDate.getTime()));
			}			
		}
		ssapApplicationRepository.save(ssapApplication);
	}
	
	private com.getinsured.eligibility.model.EligibilityProgram  updateInboundATRequest(SsapApplicant ssapApplicantDO, AccountTransferRequestPayloadType accountTransferRequest){
		com.getinsured.eligibility.model.EligibilityProgram  eligibilityProgram = null;
		if(this.exchangeEligibilityIndicator(accountTransferRequest) == true){
			eligibilityProgram = this.createExchangeEligibilityType(true, ssapApplicantDO);
		}else{
			eligibilityProgram = this.createExchangeEligibilityType(false, ssapApplicantDO);
		}
		return eligibilityProgram;
	}
	
	private boolean exchangeEligibilityIndicator(AccountTransferRequestPayloadType accountTransferRequest) {
		boolean createExchangeEligibility = true;
		 List<InsuranceApplicantType>  insuranceApplicantTypes = accountTransferRequest.getInsuranceApplication().getInsuranceApplicant();
		 if(insuranceApplicantTypes.size() > 0 ){
			 for (InsuranceApplicantType insuranceApplicantType : insuranceApplicantTypes) {
				 if(insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility() != null){
					 for (EligibilityType eligibilityType : insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility()) {
						if(eligibilityType instanceof MedicaidMAGIEligibilityType || 
						   eligibilityType instanceof MedicaidNonMAGIEligibilityType ||
						   eligibilityType instanceof CHIPEligibilityType
							){
							if(eligibilityType.getEligibilityIndicator().isValue() == true){
								createExchangeEligibility = false;
								break;
							}
						}
					}
				 }
			}
		 }
		return createExchangeEligibility;
	}
	
	
	
	private  com.getinsured.eligibility.model.EligibilityProgram createExchangeEligibilityType(boolean statusIndicator, SsapApplicant ssapApplicantDO){
		com.getinsured.eligibility.model.EligibilityProgram newEligibilityProgram = new com.getinsured.eligibility.model.EligibilityProgram();
		
		Calendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_MONTH, 60);
		Date today60 = cal.getTime(); 
		
		newEligibilityProgram.setEligibilityType("ExchangeEligibilityType");
		newEligibilityProgram.setEligibilityIndicator(String.valueOf(statusIndicator).toUpperCase());
		newEligibilityProgram.setEligibilityStartDate(new Date());
		newEligibilityProgram.setEligibilityEndDate(today60);
		newEligibilityProgram.setEligibilityDeterminationDate(new Date());
		newEligibilityProgram.setSsapApplicant(ssapApplicantDO);
		
		return newEligibilityProgram;
	}

	private boolean isNVInboundATRequest(AccountTransferRequestPayloadType accountTransferRequest){
		boolean isNvInitiatedAT = false;
		List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes = accountTransferRequest.getInsuranceApplication().getApplicationIdentification();
		for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
			if( tIdentificationType.getIdentificationID() != null && tIdentificationType.getIdentificationCategoryText() != null && "Exchange".equalsIgnoreCase(tIdentificationType.getIdentificationCategoryText().getValue())){
				isNvInitiatedAT = true;
			}
		}
		return isNvInitiatedAT;
	}
	
	private AccountTransferRequestPayloadType extractSoapBodyFromGiWsPayload(Integer giWsPayloadId){
		GIWSPayload giwsPayload = giwsPayloadRepository.findOne(giWsPayloadId);
		AccountTransferRequestPayloadType accountTransferRequest = null;
		try {
			if(!"NV".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)))
			{
				String requestBody = ReferralUtil.extractSoapBoady(giwsPayload.getRequestPayload());
				accountTransferRequest = ReferralUtil.unmarshal(requestBody);
			}
			else
			{
				accountTransferRequest = ReferralUtil.getAccountTransferRequestObject(giwsPayload.getRequestPayload());
			}
			return accountTransferRequest;
		} catch (Exception e) {
			LOGGER.error("Error occured while extracting AT request body for " + giWsPayloadId);
			return null;
		}
	}
}