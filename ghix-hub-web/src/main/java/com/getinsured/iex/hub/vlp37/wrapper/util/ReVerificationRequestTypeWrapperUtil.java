package com.getinsured.iex.hub.vlp37.wrapper.util;

import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.wrapper.ReVerificationRequestTypeWrapper;


public final class ReVerificationRequestTypeWrapperUtil {
	
	private ReVerificationRequestTypeWrapperUtil(){
		
	}
	
	private static final String FIRSTNAME_KEY = "FirstName";
	private static final String MIDDLENAME_KEY = "MiddleName";
	private static final String LASTNAME_KEY = "LastName";
	private static final String DATEOFBIRTH_KEY = "DateOfBirth";
	private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
	private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	
	public static void addNameToWrapper(JSONObject obj, ReVerificationRequestTypeWrapper wrapper) throws HubServiceException{
		
		Object param = null;
		param = obj.get(FIRSTNAME_KEY);
		if(param != null){
			wrapper.setFirstName((String)param);
		}
		
		param = obj.get(MIDDLENAME_KEY);
		if(param != null){
			wrapper.setMiddleName((String)param);
		}
		
		param = obj.get(LASTNAME_KEY);
		if(param != null){
			wrapper.setLastName((String)param);
		}
		
		param = obj.get(DATEOFBIRTH_KEY);
		if(param != null){
			try {
				wrapper.setDateOfBirth((String)param);
			} catch (Exception e) {
				throw new HubServiceException(e);
			}
		}
	}
	
	public static void addIndicatorsToWrapper(JSONObject obj, ReVerificationRequestTypeWrapper wrapper){
		
		Object param = null;
		param = obj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY);
		if(param != null){
			wrapper.setFiveYearBarApplicabilityIndicator((Boolean) param);
		}
		
		param = obj.get(REQUESTERCOMMENTSFORHUB_KEY);
		if(param != null){
			wrapper.setRequesterCommentsForHub((String)param);
		}
	}
	
}
