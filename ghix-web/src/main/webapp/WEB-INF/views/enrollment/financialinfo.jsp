<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript">
function redirectForm(paymentType){
	$('#paymentType').val(paymentType);
	$('#frmcreatefinancialinfo').submit();
}
</script>


<div class="row">		
	<div class="span4">
		<h2>Financial Information</h2>
	</div>
	<form action="../../financialinfo/createfinancialinfo" id="frmcreatefinancialinfo" method="post">
		<input type="hidden" name="redirectUrl" id="redirectUrl"  value="/enrollment/financialinfo/${applicationId}" />
		<input type="hidden" name="paymentType" id="paymentType"  value="" />
	</form>
	
	
	<div class="span12">
		<form class="form-stacked" id="frmfinancial" name="frmfinancial" method="post">
			<input type="hidden" name="applicationId" id="applicationId"  value="${applicationId}" />
			<input type="hidden" name="personId" id="personId"  value="${personId}" />
			<input type="hidden" name="oneTimeShowDiv" id="oneTimeShowDiv" value="${oneTimeShowDiv}" />
			<input type="hidden" name="recurringShowDiv" id="recurringShowDiv" value="${recurringShowDiv}" />
			
			<fieldset class="form-stacked">
			<br></br>
				<h3><spring:message  code="label.yourFinancialInfo"/></h3>
			</fieldset>
			
			<div id="existingFinancialInfo">
				<div id="bankDetailsDiv">
				<c:if test='${bankDataSize > 0}' >
					<p><font style="font-size:16px;">Bank Details</font></p>
					<br>
					<table>
			 			<tr style="font-weight:bold;font-size:13px;">
			 				<td><b>Bank name</b></td>
			 				<td><b>Account number</b></td>
			 				<td><b>Routing number</b></td>
			 				<c:if test='${oneTimeBankPayOption == true}'>
							   <td><b>OneTime</b></td>
							</c:if>
							<c:if test='${recurringBankPayOption == true}'>
							   <td><b>Recurring</b></td>
							</c:if>
			 			</tr>
					<c:set var="count" value="0" scope="page" />
					<c:forEach items="${banks}"  var="bankObj">
						 <c:set var="count" value="${count + 1}" scope="page"/>
							  <div id="onetime_bankDetails_${bankObj.value.id}_data">
							  		<c:set var="accountNumber" value="${bankObj.value.accountNumber}"/>
							  		<c:set var="routingNumber" value="${bankObj.value.routingNumber}"/>
						 			<tr>
						 				<td>${bankObj.value.bankName}</td>
						 				<td>${fn:substring(accountNumber, fn:length(accountNumber) - 4, fn:length(accountNumber))}</td>
										<td>${fn:substring(routingNumber, fn:length(routingNumber) - 4, fn:length(routingNumber))}</td>	
										<c:if test='${oneTimeBankPayOption == true}'>
											<td><input type="radio" name="oneTime" id="oneTime" value="${bankObj.key}">&nbsp;</td>
										</c:if>
										<c:if test='${recurringBankPayOption == true}'>
											<td><input type="radio" name="recurring" id="recurring" value="${bankObj.key}">&nbsp;</td>
										</c:if>							 				
						 			</tr>
							 </div>
					</c:forEach>
					</table>
				</c:if>
					<input type="button" name="addNewBank" id="addNewBank" onClick="javascript:redirectForm('Bank');" value="Add New Bank Details" title="Add New Bank Details" class="btn primary"/>
					<br></br>
				</div>
				
				<div id="creditCardDetailsDiv">
					<c:if test='${creditCardSize > 0}' >
						<p><font style="font-size:16px;">CreditCard Details</font></p>
						<br>
						<table>
				 			<tr style="font-weight:bold;font-size:13px;">
				 				<td><b>Card type</b></td>
				 				<td><b>Card number</b></td>
				 				<td><b>Expiration date</b></td>
				 				<c:if test='${oneTimeCreditPayOption == true}'>
				 					<td><b>OneTime</b></td>
				 				</c:if>
				 				<c:if test='${recurringCreditPayOption == true}'>
				 					<td><b>Recurring</b></td>
				 				</c:if>
				 			</tr>
						<c:set var="count" value="0" scope="page" />
						<c:forEach items="${creditCards}" var="creditCardObj">
							 <c:set var="count" value="${count + 1}" scope="page"/>
								 <div id="onetime_creditCardDetails_${creditCardObj.value.id}_data" style="display:none">
								 		<c:set var="cardNumber" value="${creditCardObj.value.cardNumber}"/>
							 			<tr>
							 				<td>${creditCardObj.value.cardType}</td>
							 				<td>${fn:substring(cardNumber, fn:length(cardNumber) - 4, fn:length(cardNumber))}</td>
											<td>${creditCardObj.value.expirationMonth}/${creditCardObj.value.expirationYear}</td>
											<c:if test='${oneTimeCreditPayOption == true}'>
												<td><input type="radio" name="oneTime" id="oneTime" value="${creditCardObj.key}">&nbsp;</td>
											</c:if>
											<c:if test='${recurringCreditPayOption == true}'>
												<td><input type="radio" name="recurring" id="recurring" value="${creditCardObj.key}">&nbsp;</td>
											</c:if>								 				
							 			</tr>
								 </div>
						</c:forEach> 
						</table>
					</c:if>
					<input type="button" name="addNewCreditCard" id="addNewCreditCard"  onClick="javascript:redirectForm('CreditCard');" value="Add New Credit Card Details" title="Add New Credit Card Details" class="btn primary"/>
				    <br></br>
				</div>
			</div>
			<div id="oneTimeErrorDiv"></div><br><br><br>
			<div id="recurringErrorDiv"></div><br><br><br>
	<fieldset>
		<div class="actions">
			<input type="submit" name="continuebutton" id="continuebutton" value="<spring:message  code="label.emplContinue"/>" class="btn primary" title="<spring:message  code="label.emplContinue"/>"/>&nbsp;&nbsp;
		</div>
   </fieldset>
</form>
	</div>
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C) dependant on
					a particular Employer's eligibility to use the SHOP Exchange.</p>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

$("#continuebutton").click(function() {
	var error = "";
	var isError = false;
	$("#oneTimeErrorDiv").html('');
	$("#recurringErrorDiv").html('');
	
	if($('input:radio[name=oneTime]').length == 0 && $('input:radio[name=recurring]').length == 0){
		isError = true;
		error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please add your bank/creditcard financial information.</span></label>";
		$("#oneTimeErrorDiv").html(error);
	}
	if ($('input:radio[name=oneTime]').length > 0) {
		if($('input:radio[name=oneTime]:checked').val() == undefined){
			error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please select onetime.</span></label>";
			$("#oneTimeErrorDiv").html(error);
			isError = true;
		}
	}
	if ($('input:radio[name=recurring]').length > 0) {
		if($('input:radio[name=recurring]:checked').val() == undefined){
			error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please select recurring.</span></label>";
			$("#recurringErrorDiv").html(error);
			isError = true;
		}
	}
	
	if(isError == true){
		return false;
	}else{
		return true;
	}
});
</script>