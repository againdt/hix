package com.getinsured.hix.indportal.dto.dm.application;

public class NonMagi {
	 private Boolean blindOrDisabledIndicator;
	 private Boolean longTermCareIndicator;


	 // Getter Methods 

	 public Boolean getBlindOrDisabledIndicator() {
	  return blindOrDisabledIndicator;
	 }

	 public Boolean getLongTermCareIndicator() {
	  return longTermCareIndicator;
	 }

	 // Setter Methods 

	 public void setBlindOrDisabledIndicator(Boolean blindOrDisabledIndicator) {
	  this.blindOrDisabledIndicator = blindOrDisabledIndicator;
	 }

	 public void setLongTermCareIndicator(Boolean longTermCareIndicator) {
	  this.longTermCareIndicator = longTermCareIndicator;
	 }
	}
