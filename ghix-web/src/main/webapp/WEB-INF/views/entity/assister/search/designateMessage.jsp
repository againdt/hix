<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<style>
body {background: #fff !important;}
</style>

	<div class="container">
		<div class="row-fluid">
			<div class="span9">     
				<ul>
					<li class="nav-header">
						<spring:message code="label.assister.designatemessage.header"/>
					</li>
				</ul>
			</div>

			<div class="span9"> 

				<div class="alert alert-info">
					<p><spring:message code="label.assister.designatemessage.msg"/></p>
				</div><!-- .alert -->
				<a href="/hix/entity/locateassister/searchentities" class="btn btn-primary"><spring:message code="label.assister.designatemessage.btn"/></a>
			</div><!-- end of span9 -->
		</div><!--  end of .row-fluid -->
	</div><!-- end of .container -->
