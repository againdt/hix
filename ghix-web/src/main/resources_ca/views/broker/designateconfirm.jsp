<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>

<div id="broker-password-confirm">
	<div class="gutter10">
		<div class="row-fluid">
			<div class="span12">
	
					<h1><spring:message  code="label.brkcongratulations"/></h1>
	
				<div class="row-fluid">
					<div class="alert alert-info span12">
						<p class="pull-left"><spring:message  code="label.brkagentdesignationcompleted"/> ${designatedBrokerProfile.user.firstName}&nbsp;${designatedBrokerProfile.user.lastName}.</p>			
						<!-- <a href="../broker/search" class="btn btn-primary pull-right">Back to Search</a> -->
					</div><!-- .alert -->
				</div><!--  end of .row-fluid -->
			</div><!-- end of span12 -->
		</div><!--  end of .row-fluid -->	
		
		<div style="margin-top:330px;">		
			<a href="<c:url value="/broker/search"/>" class="btn btn-primary pull-left"><spring:message  code="label.brkbacktosearch"/></a>
			<input name="close" id="closeIFrame" type="button" onClick="window.close();" value="<spring:message  code='label.button.close'/>" title="<spring:message  code='label.button.close'/>" class="btn btn-primary pull-right" />
		</div>
	</div>
</div>

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->	

