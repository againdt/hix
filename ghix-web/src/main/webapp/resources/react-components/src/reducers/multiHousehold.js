import { ActionTypes as types } from '../constants/constants';

const INITIAL_STATE = false;

const multiHousehold = (state=INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case (types.RECEIVED_MUTLTIHOUSEHOLD_SUCCESS):
            return payload;
        case (types.RECEIVED_MUTLTIHOUSEHOLD_FAILURE):
            return payload;

        default:
            return state;
    }
};

export default multiHousehold;