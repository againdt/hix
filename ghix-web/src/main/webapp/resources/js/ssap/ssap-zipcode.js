/*template XHTML for address worksite*/
var zipIdFocused="";
var application = {
	/*remove the modal*/
	remove_modal:function(){

		/*console.log('remove_modal');*/
		$('#modal,.modal-backdrop,#addressProgressPopup,#check-address-error').remove();
		if($('#primary-contact-information-form').is(':visible')){
			$('#modal, .modal-backdrop').remove();
			$('#check-address-error, #address-failure-box, #suggestion-box, #addressProgressPopup').remove();
		}
	},
	/*reset county on all the callback from validAddressNew function*/
	resetCountyOnAllCallback : function(){
		var zipIdTarget = zipIdFocused;

		/*console.log(zipIdTarget,'get zipIdFocused value remove modal');*/
		
		var	formId = $(parent.document.forms).attr('id'),
			parentElement = jQuery('#'+zipIdTarget).parents('.addressBlock').find('.zipCode'),
			zipCodeVal = parentElement.val(),
			zipId = parentElement.attr('id'),
			indexValue = zipId.split('_');
		
		/*console.log(formId,'-----',parentElement,'-----1-----',zipCodeVal,'---2----',zipId,'----3---',indexValue);*/
		//jQuery('#'+zipIdTarget).parents('.addressBlock').find('.control-group .controls').find('input:text').eq(0).focus();
		
		application.populateCounty(zipId, indexValue, zipCodeVal);
	},
	setValueInFormForSplEnroll:function(){
		$('#suggestion-box').hide();
		$('#form-input-data').show();
	},
	/*return back to the input FORM div after after error from the SAME PAGE*/
	back_button_to_input_screen : function(zipIdFocus){
		application.modal_cross_button();
		$('#back_to_input_modal').bind('click',function(){
			/*console.log('close this modal');*/
			$('#check-address-error, #address-failure-box,#addressProgressPopup').hide();
			application.remove_modal();
			application.resetCountyOnAllCallback();
			//jQuery('#'+zipIdFocused).parents('.addressBlock').find('.control-group .controls').find('input:text').eq(0).focus();
		});
	},
	/*return back to the input FORM div from IFRAME WINDOW*/
	/*back_button_from_iframe:function(modalId){
		$(modalId.document).find('#back_button_from_iframe').bind('click',function(){
			if($('#editdependent').is(':visible')){
				$('input#firstName').focus();
				$('#check-address-error, #address-failure-box, #suggestion-box').hide();
				$('#form-input-data').show();
				if($('#address1_0').length){
					$('#address1_0').focus();
				}
			}else{
				$('#check-address-error, #address-failure-box, #suggestion-box').hide();
				application.remove_modal();
			}
		});
	},*/
	back_button_from_iframe_worksite:function(modalId){
		/*console.log("new zip code iframe window", modalId, $(modalId.document),$(modalId.document).find('#back_button_from_iframe'));*/
		
		/*var formTextBoxId = jQuery('#form-input-data').find('input[type="text"]:first').attr('id');*/
		/*console.log('formTextBoxIdformTextBoxId----',formTextBoxId,$(modalId.document));*/
		
		$(modalId.document).find('#back_button_from_iframe').bind('click',function(e){
			application.remove_modal();
			application.resetCountyOnAllCallback();
		});
	},
	modal_cross_button:function(modalId){
		/*console.log('modal cross button', modalId, $(modalId.document), $('.closeModal'));*/
		$('.closeModal').bind('click',function(e){
			application.remove_modal();
			application.resetCountyOnAllCallback();
		});
	},
	populateCounty: function(zipid, indexValue, zipCodeVal){
		/*console.log(zipid, indexValue, zipCodeVal,'zipcode js function',$('#'+zipid).val());*/
		if($('#'+zipid).val().length >= 5){
			/*console.log('zip code lenght greater than 5 ' + indexValue.length);
			console.log('index value before',indexValue[1]);*/
			parent.postPopulateIndex(zipCodeVal, zipid);
		}
	},
	/*SUBMIT FUNCTION inside IFRAME*/
	submit_button_of_iframe:function(buttonId){
		/*console.log('modal zipcode',button);*/
		var idss = $('#ids').val();
		var retVal=idss.split("~");	
		
		if ($("input:radio:checked").val() == undefined){
			alert('Please select one address.');
			return false;
		}
		/*get the value of CHECKED RADIO*/
		var actualVal=$("input:radio:checked").val().split(","),
			selectedState = $("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val();
		
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[0]).val(actualVal[0]);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[2]).val($.trim(actualVal[2]));
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val(selectedState);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[4]).val($.trim(actualVal[4]));
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[5]).val(actualVal[5]);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[6]).val(actualVal[6]);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[7]).val(actualVal[7] == undefined ? "" : actualVal[7]);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[8]).val(actualVal[8] == undefined ? "" : actualVal[8]);
		/*trigger the BACK button*/
		$('#'+buttonId).parent().find("#back_button_from_iframe").trigger('click');
		parent.getCountyList($('#'+ zip_e).val(), '');
	},
	/*FOCUS OUT function on ZIP*/
	zipcode_focusout: function(e, that){

		var startindex = (e.target.id).indexOf("_");
		var index = (e.target.id).substring(0,startindex);
		
		
		var address1_e='_addressLine1'; var address2_e='_addressLine2'; var city_e= '_primary_city'; var state_e='_primary_state'; var zip_e='_primary_zip';
		var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
		/*check for the underscore in the id of the focus out event of the zipcode. for current scenarios id is "zip_0. hence startindex  ===3 "*/
		if (startindex== 4 || startindex ==7 )
		{
			address1_e=index+address1_e;
			address2_e=index+address2_e;
			city_e=index+city_e;
			state_e=index+state_e;
			zip_e=index+zip_e;
			lat_e=index+lat_e;
			lon_e=index+lon_e;
			rdi_e+=index;
			county_e+=index;
		}

		var model_address1 = address1_e + '_hidden' ;
		var model_address2 = address2_e + '_hidden' ;
		var model_city = city_e + '_hidden' ;
		var model_state = state_e + '_hidden' ;
		var model_zip = zip_e + '_hidden' ;
		//parent.getCountyList($('#'+ zip_e).val(), '');
		
		var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
		if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
			if(($('#'+ address2_e).val())==="Address Line 2"){
				$('#'+ address2_e).val('');
			}	
		viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
				$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
				idsText);
	/*		viewValidAddressListNew($('#home_addressLine1').val(),$('#home_addressLine2').val(),$('#home_primary_city').val(),$('#home_primary_state').val(),$('#home_primary_zip').val(), 
					$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
					idsText);*/
			
			zipIdFocused = $(e.target).attr('id');
			//console.log(zipIdFocused,'1111');
		}
	},
	/*RESET the content in the FORM*/
	input_content_screen_reset: function(that){
    	$('.errorAddress, .errorAddressBtn, .suggestedAddress').remove();
    	$('#modal').find('.modal-header h3').text('Additional Worksite');
    	$('#modal-inputs,#input-footer').show();
    },
    /*VALIDATE the FORM elements*/
    add_Address_function:function(){
    	$("#frmworksites").validate({
            errorClass: "error",
            errorPlacement: function(error, element) {
        		var elementId = element.attr('id');
        		error.appendTo( $("#" + elementId + "_error"));
        		$("#" + elementId + "_error").attr('class','error help-inline');
            },
            rules: {
            	'locations[0].location.address1': { 
                    required: true
                },
                'locations[0].location.city': {
                    required: true
                },
                'locations[0].location.state': {
                    required: true
                },
                'locations[0].location.zip': {
                    required: true,
                    zipcheck : true,
                },
                'locations[0].location.county': {
                    required: true
                }
            }
        });
		
        /*error message for address1*/
        $('input[name="locations[0].location.address1"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter address.</span>"
            }
        });
        /*error message for city*/
        $('input[name="locations[0].location.city"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter city.</span>"
            }
        });
        /*error message for state*/
        $('*[name="locations[0].location.state"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter city.</span>"
            }
        });
        
        /*error message for zip*/
        $('input[name="locations[0].location.zip"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter zip code.</span>",
                zipcheck: "<span><em class='excl'>!</em> Please enter a valid zip code.</span>"
            }
        });
        
        /*error message for county*/
        $('*[name="locations[0].location.county"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter a county.</span>"
            }
        });
        
        /*check the value of ZIP*/
        jQuery.validator.addMethod("zipcheck", function(value, element, param) {
          	elementId = element.id; 
          	var numzip = new Number($("#"+elementId).val());
            var filter = /^[0-9]+$/;
            if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
          		return false;	
          	}return true;
        });
    },
    /*populate STATE DROPDOWN inside MODAL on show*/
    populate_state_dropdown:function(){
    	////console.log($('#jsonStateValue').val())
    	var stateObj = $('#jsonStateValue').val(),
    		stateJson = $.parseJSON(stateObj);
    	
    	var defaultStateValue = $('#defaultStateValue').val();
    	$.each(stateJson, function(index, item){
    		if(item.code === defaultStateValue){
    			$('<option selected="selected" value='+[item.code]+'>'+[item.name]+'</option>').appendTo("select[name='locations[0].location.state']");
    		}else{
    			$('<option value='+[item.code]+'>'+[item.name]+'</option>').appendTo("select[name='locations[0].location.state']");
    		}
    	});
    },
    /*RESIZE the IFRAME HEIGHT*/
    iframeHeight:function(){
    	/*console.log('modal zipcode',$(window.top.document).find("iframe")[0].contentDocument.body.scrollHeight);*/
    	/*setTimeout(function() {
    		$('#modalData').height($(window.top.document).find("iframe")[0].contentDocument.body.scrollHeight);
    	}, 1000);*/
    	$('#modalData').height(290);
    	$('#modalData').width('100%');
    },
    /*populate LOCATION on PARENT PAGE*/
    populate_location_on_parent_page: function(empDetailsJSON, contactlocationid){
		if($.browser.msie && $.browser.version <= 8){
			for(var i=0;i<empDetailsJSON.length;i++){
				var obj = empDetailsJSON[i];
				primarylocationid = obj['id'];
			}
			
			var selected = (obj['id'] == contactlocationid) ? contactlocationid : primarylocationid,
				selectLocationId = (locationval != undefined || locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];
				
			$('#location').prepend('<option value="'+selectLocationId+'" ' +'>'+obj['address1']+", "+obj['city']+", "+obj['state']+'</option>');
			$('#location').val(selected);
		}else{
			for(var i=0;i<empDetailsJSON.length;i++){
				var obj = empDetailsJSON[i];
				var selected = '';
				if(contactlocationid == 0){
					if(obj['primary'] == 'YES'){
						selected = 'selected="selected"';
					}
				}else if(obj['id'] == contactlocationid){
					selected = 'selected="selected"';
				}
				
				var selectLocationId = (locationval != undefined && locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];
                $('#location').prepend('<option value="'+selectLocationId+'" '+ selected +'>'+obj['address1']+", "+obj['city']+", "+obj['state']+'</option>');
			} 
		}
    },
    /*ADDRESS Failure Message*/
    address_failure_message:function(message){        	
    	$('#address-failure-box').remove();
    	var addresFailureMessage = '<div id="address-failure-box"><div class="modal-header clearfix"><h3 class="pull-left">Confirm Address</h3>'+
    		'<button type="button" class="close closeModal">&times;</button></div>'+
			'<div class="modal-body"><div class="suggestedAddress">'+message+'</div></div>'+
			'<div class="modal-footer"><input type="button" class="btn errorAddressBtn" aria-hidden="true" id="back_to_input_modal" value="OK" /></div></div>';
    	

		$('body').append(addresFailureMessage);
		$('#address-failure-box').addClass('modal popup-address');
		application.back_button_to_input_screen(zipIdFocused);
		
    },
    ssp_address_success_modal:function(){
    	$('#editdependent').hide();
    	$('#suggestion-box').show();
    },
    ssp_dependent_modal:function(){
    	$('#editdependent').show();
    	$('#suggestion-box').hide();
    }
};



/*FOCUS OUT event of ZIP CODE*/
$('.zipCode').live('focusout',function(e){
	application.zipcode_focusout(e, $(this));
});

$('.removeModal').live('click',function(){
	/*console.log('zipcode util');*/
	application.remove_modal();
});