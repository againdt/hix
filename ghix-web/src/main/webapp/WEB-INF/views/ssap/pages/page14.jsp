<%@ include file="../include.jsp" %>
<div id="appscr64" style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<strong class="nameOfHouseHold"></strong>&ndash;<spring:message code="ssap.page14.ethnicity" text="Ethnicity" /> &amp; &nbsp;
				<spring:message code="ssap.page14.raceOptional" text="Race (optional)" />
			</h4>
		</div>
		
		<form id="appscr64Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.pageHeader.familyAndHousehold" text="Family and Household" /></h4>
			</div>
			<div class="gutter10">
			<div class="alert alert-info margin20-t">
				<p>
					<spring:message code="ssap.page14.optionalInformation" text="Optional information" />
					<spring:message code="ssap.page14.thisInformation" text="Providing this information won't impact your eligibility for health coverage, your health plan options, or your costs in any way." />
				</p>
			</div>
			
			<div class="control-group">
				<fieldset>
				<legend>
					<spring:message code="ssap.is" text="Is" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page14.hispanicLatino" text="of Hispanic, Latino, or Spanish origin?" />
				</legend>
				<div class="margin5-l">
					<label class="radio" for="checkPersonNameLanguageYes">
						<input value="yes" type="radio" id="checkPersonNameLanguageYes" name="checkPersonNameLanguage" onchange="checkLanguage();" />
						<spring:message code="ssap.yes" text="Yes" />
					</label>
					<label class="radio" for="checkPersonNameLanguageNo">
						<input type="radio" value="no" id="checkPersonNameLanguageNo" name="checkPersonNameLanguage" onchange="checkLanguage();" />
						<spring:message code="ssap.no" text="No" />
					</label>
				</div>
				</fieldset>
			</div><!-- control-group ends -->
			
			<div id="ethnicityDiv" class="hide">
				<div class="control-group">
					<fieldset>
					<legend>
						<span><spring:message code="ssap.page14.ethnicity" text="Ethnicity" /></span>
						<small><spring:message code="ssap.page14.checkAllThatApply" text="(check all that apply.)" /></small>
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</legend>
					<div class="margin5-l">
						<label class="checkbox" for="ethnicity_cuban">
							<input type="checkbox" id="ethnicity_cuban"  name="ethnicity" value="2182-4" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />" data-parsley-errors-container="#ethnicity_cubanErrorDiv"><spring:message code="ssap.page14.cuban" text="Cuban" />
						</label>
						<label for="ethnicity_maxican" class="capitalize-none checkbox">
							<input type="checkbox"  id="ethnicity_maxican"  name="ethnicity" value="2148-5"><spring:message code="ssap.page14.mexicanMexicanAmerican" text="Mexican, Mexican American, or Chicano/a" />
						</label>
						<label class="checkbox" for="ethnicity_puertoRican">
							<input type="checkbox" id="ethnicity_puertoRican"  name="ethnicity" value="2180-8"><spring:message code="ssap.page14.puertoRican" text="Puerto Rican" />
						</label>
						<div class="control-group clearfix">
							<label for="ethnicity_other" class="pull-left margin10-r">
								<input type="checkbox"  id="ethnicity_other"   name="ethnicity" value="0000-0" class="pull-left" onclick="displayOtherEthnicity();">
								<spring:message code="ssap.county.Other" text="Other" />
							</label>
							<input type="text" placeholder="Enter other ethnicity" value="" name="otherEthnicity " id="otherEthnicity" class="other_information_input_field hide" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.otherEthnicityRequired'/>" data-parsley-length="[5, 256]"  data-parsley-length-message="<spring:message code='ssap.error.otherEthnicityLength'/>">
						</div>
						<div id="ethnicity_cubanErrorDiv"></div>
					</div>
					</fieldset>
				</div><!-- control-group ends -->
			</div><!-- ethnicityDiv ends  -->
			
			<div id="raceDiv">
				<div class="control-group">
					<fieldset>
					<legend>
						<span><spring:message code="ssap.page14.race" text="Race" /></span>
						<small><spring:message code="ssap.page14.checkAllThatApply" text="(check all that apply.)" /></small>
					</legend>
					<div class="margin5-l">
						<label for="americanIndianOrAlaskaNative" class="capitalize-none checkbox">
							<input type="checkbox"  id="americanIndianOrAlaskaNative"   name="race" value="1002-5" ><spring:message code="ssap.page14.americanIndianOrAlaska" text="American Indian or Alaska Native" />
						</label>
						<label class="checkbox" for="asianIndian">
							<input type="checkbox" id="asianIndian"  name="race" value="2029-7" ><spring:message code="ssap.page14.asianIndian" text="Asian Indian" />
						</label>
						<label for="BlackOrAfricanAmerican" class="capitalize-none checkbox">
							<input type="checkbox"  id="BlackOrAfricanAmerican"   name="race" value="2054-5" ><spring:message code="ssap.page14.blackOrAfricanAmerican" text="Black or African American" />
						</label>
						<label class="checkbox"  for="chinese">
							<input type="checkbox"  id="chinese"   name="race" value="2034-7" ><spring:message code="ssap.page14.chinese" text="Chinese" />
						</label>
						<label class="checkbox"  for="filipino">
							<input type="checkbox"  id="filipino"   name="race" value="2036-2" ><spring:message code="ssap.page14.filipino" text="Filipino" />
						</label>
						<label for="guamanianOrChamorro" class="capitalize-none checkbox">
							<input type="checkbox"  id="guamanianOrChamorro"   name="race" value="2086-7" ><spring:message code="ssap.page14.guamanianOrChamorro" text="Guamanian or Chamorro" />
						</label>
						<label class="checkbox"  for="japanese">
							<input type="checkbox"  id="japanese"   name="race" value="2039-6" ><spring:message code="ssap.page14.japanese" text="Japanese" />
						</label>
						<label class="checkbox"  for="korean">
							<input type="checkbox"  id="korean"   name="race" value="2040-4" ><spring:message code="ssap.page14.korean" text="Korean" />
						</label>
						<label class="checkbox"  for="nativeHawaiian">
							<input type="checkbox"  id="nativeHawaiian"   name="race" value="2079-2" ><spring:message code="ssap.page14.nativeHawaiian" text="Native Hawaiian" />
						</label>
						<label class="checkbox"  for="otherAsian">
							<input type="checkbox"  id="otherAsian"   name="race" value="2028-9" ><spring:message code="ssap.page14.otherAsian" text="Other Asian" />
						</label>
						<label class="checkbox"  for="otherPacificIslander">
							<input type="checkbox"  id="otherPacificIslander"   name="race" value="2500-7" ><spring:message code="ssap.page14.otherPacificIslander" text="Other Pacific Islander" />
						</label>
						<label class="checkbox"  for="samoan">
							<input type="checkbox"  id="samoan"   name="race" value="2080-0" ><spring:message code="ssap.page14.samoan" text="Samoan" />
						</label>
						<label class="checkbox"  for="vietnamese">
							<input type="checkbox"  id="vietnamese"   name="race" value="2047-9" ><spring:message code="ssap.page14.vietnamese" text="Vietnamese" />
						</label>
						<label for="whiteOrCaucasian" class="capitalize-none checkbox">
							<input type="checkbox"  id="whiteOrCaucasian"   name="race" value="2106-3" ><spring:message code="ssap.page14.whiteOrCaucasian" text="White or Caucasian" />
						</label>
						<div class="control-group clearfix">
							<label for="race_other" class="pull-left margin10-r checkbox">
								<input type="checkbox"  id="race_other"   name="race" value="2131-1" class="pull-left" onclick="displayOtherRace();">
								<spring:message code="ssap.page14.other" text="Other" />
							</label>
							<input type="text" placeholder="<spring:message code='ssap.page14.enterOtherRace' text='Enter other race' />" value="" name="otherRace" id="otherRace" class="other_information_input_field hide" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.otherRaceRequired'/>" data-parsley-length="[5, 256]"  data-parsley-length-message="<spring:message code='ssap.error.otherRaceLength'/>"/>
						</div>
					</div>
					</fieldset>
				</div><!-- control-group ends -->
			</div></div>
		</form>
	</div>
</div>