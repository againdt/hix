package com.getinsured.hix.admin.service.jpa;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.admin.service.UsersNotificationService;
import com.getinsured.hix.admin.util.ManageUserNotificationTemplate;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

@Service("usersNotificationService")
public class UsersNotificationServiceImpl implements UsersNotificationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UsersNotificationServiceImpl.class);

	@Autowired
    private ManageUserNotificationTemplate manageUsersNotificationTemplate; 

	@Override
	public void sendEmailNotification(String notificationType,String recipient,String updatedFields,String subject,String userName) throws NotificationTypeNotFound {
		LOGGER.info("appealRequestNotificationTemplate : START");
		
		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("RECIPIENT", recipient);
		notificationContext.put("UPDATED_FIELDS", updatedFields);
		notificationContext.put("SUBJECT", subject);
		notificationContext.put("USER_NAME", userName);
		Map<String, String> tokens = manageUsersNotificationTemplate.getTokens(notificationContext);
		Map<String, String> emailData = manageUsersNotificationTemplate.getEmailData(notificationContext);
		Location location = null; //TODO: location has to be provided here 
		
		Notice noticeObj = manageUsersNotificationTemplate.generateEmail(manageUsersNotificationTemplate.getClass().getSimpleName(),location, emailData, tokens);
		if(noticeObj == null){
			throw new NotificationTypeNotFound("Failed to find the notification for location:"+manageUsersNotificationTemplate.getClass().getSimpleName());
		}
		//LOGGER.info(noticeObj.getEmailBody());
		manageUsersNotificationTemplate.sendEmail(noticeObj);
		LOGGER.info("appealRequestNotificationTemplate : END");
	}

}
