import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';

// ActionCreators

import { changePage } from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';


/**
 *
 * @param {Object} props
 * @returns {React.Component} - Left Navigation Bar
 * @constructor
 */
const NavBar = props => {

    const {
        adjustedFlow,
        householdMembers,
        messages,
        page
    } = props;

    const currentSection = adjustedFlow[page] && // Do we need it? Clarify with Haripriyaa
        adjustedFlow[page].sectionIndex;

    const currentPage = adjustedFlow[page];

    // Here we prepare data for NavBar creation
    // We transform list of pages what includes tech data and Components themselves
    // to list of sections (with subsections) to iterate from it and build NavBar

    // 1. Iteration through list of pages and creation and combining subsections into arrays

    const adjustedSections = [];

    adjustedFlow.map(page => {

        const index = adjustedSections.findIndex( section => section.sectionIndex === page.sectionIndex);

        if(~index) {

            // Here we add to existing subsections in current section new subsection

            adjustedSections[index] = {
                ...adjustedSections[index],
                subSections: [
                    ...adjustedSections[index].subSections,
                    page
                ]
            };
        } else {

            // Here we create section if it does not exist, assign section name, index and
            // create subsections and add to it first current subsection

            adjustedSections[adjustedSections.length] = {
                sectionName: page.sectionName,
                sectionIndex: page.sectionIndex,
                subSections: [
                    page
                ]
            };
        }

    });

    // 2. Refining subsections into sub-arrays if they are common for all household members

    const dataForRender = adjustedSections.map(section => {

        let subsections = [];

        let loopArr = [];

        // 2.1. Grouping subsections in sub-array if we see a loop for household members

        section.subSections.forEach((subsection, idx) => {
            if(subsection.hhm === -1 ) {
                subsections = [
                    ...subsections,
                    loopArr
                ];
                loopArr = [];
                subsections[idx] = subsection;
            }
            else {
                loopArr = [
                    ...loopArr,
                    subsection
                ];
            }
        });

        // 2.2. Dividing into chunks to support subheader like person name

        const chunkedSubsections = subsections.map(subsection => {

            if(Array.isArray(subsection)){
                return _.chunk(subsection, subsection.length/householdMembers.length);
            } else {
                return subsection;
            }
        });

        return {
            ...section,
            subSections: chunkedSubsections.filter(subsection => subsection) // Getting rid of empty elements in array
        };
    });

    const getClassNameForSubSection = subSection => {

        const {
            pageName,
            hhm
        } = subSection;

        let classValue;

        if(currentPage.pageName === pageName && currentPage.hhm === hhm) {
            classValue = 'current';
        } else {
            classValue = 'sub-links-undone';
        }
        return classValue;
    };

    const getClassNameForSubSubSection = ss => {
        const {
            section,
            pageName,
            hhm
        } = ss;

        let classValue;

        if(currentPage.pageName === pageName && currentPage.section === section && currentPage.hhm === hhm) {
            classValue = 'loop-links-current';
        } else {
            classValue = 'loop-links-undone';
        }

        return classValue;
    };

    const getLinks = () => {

        const {
            householdMembers,
        } = props;

        return dataForRender.map(section => {

            const {
                sectionName,
                sectionIndex,
                subSections = []
            } = section;

            return (
                <li
                    key={ sectionName }
                    className="sidenav__list-header"
                >
                    <a className={`${currentSection >= sectionIndex ? 'usa-current' : 'usa'}`}>
                        { sectionName }
                        <ul
                            className={`usa-unstyled-list ${currentSection === sectionIndex ? 'shown' : 'hidden'} sidenav__list__sublinks`}
                        >
                            {
                                subSections.map((subSection, subSectionIndex) => {

                                    if(subSection.isActive === false) {
                                        return null;
                                    }

                                    const isLoop = Array.isArray(subSection);

                                    const checkIfPersonExtended = pages => {
                                        const {
                                            section,
                                            pageName,
                                            hhm
                                        } = currentPage;

                                        return pages.some(page => page.pageName === pageName && page.section === section && page.hhm === hhm);
                                    };


                                    return (
                                        <li
                                            key={`${subSection.pageName}_${subSectionIndex}`}
                                            className={`sidenav__list__sublinks-link ${!isLoop ? getClassNameForSubSection(subSection) : ''}`}
                                        >
                                            {
                                                !isLoop ?

                                                    <span>
                                                        {
                                                            subSection.page && subSection.page.navBarTextCode && messages ?
                                                                <FormattedMessage id={subSection.page.navBarTextCode}/> :
                                                                subSection.page.navBarText
                                                        }
                                                    </span>

                                                    :

                                                    subSection.map((ss, idx) => {

                                                        return (
                                                            <Fragment key={`${ss[0].pageName}_${idx}`}>
                                                                {
                                                                    ss[0].hhm <= householdMembers.length - 1 ?

                                                                        <ul className='usa-unstyled-list person-links'>
                                                                            <li>{ normalizeNameOfPerson(householdMembers[ss[0].hhm].name) }
                                                                                <ul className='usa-unstyled-list person-links'>
                                                                                    {
                                                                                        checkIfPersonExtended(ss) &&
                                                                                        <Fragment>

                                                                                            {
                                                                                                ss.map((sss, sssIndex )=> {
                                                                                                    return (
                                                                                                        sss.isActive ?
                                                                                                            <li
                                                                                                                key={`${sss.page.navBarText}_${idx}`}
                                                                                                                id={`sublink-header_hhm${ss[0].hhm}_link${sssIndex}`}
                                                                                                                className={getClassNameForSubSubSection(sss)}
                                                                                                            >
                                                                                                                <span>
                                                                                                                    {
                                                                                                                        sss.page && sss.page.navBarTextCode && messages ?
                                                                                                                            <FormattedMessage id={sss.page.navBarTextCode} /> : sss.page.navBarText
                                                                                                                    }
                                                                                                                </span>
                                                                                                            </li> : null
                                                                                                    );
                                                                                                })
                                                                                            }
                                                                                        </Fragment>
                                                                                    }
                                                                                </ul>
                                                                            </li>
                                                                        </ul> :
                                                                        null
                                                                }
                                                            </Fragment>

                                                        );
                                                    })
                                            }
                                        </li>
                                    );
                                })
                            }
                        </ul>
                    </a>
                </li>
            );
        });
    };

    return (
        <Fragment>
            <div className="header">
                <h3 className="usa-heading-alt">Steps</h3>
            </div>
            <nav>
                <ul className="usa-sidenav-list">
                    {
                        getLinks()
                    }
                </ul>
            </nav>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    adjustedFlow: state.adjustedFlow,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    messages: state.messages,
    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changePage
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);