package com.getinsured.eligibility.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.startdates.EligibilityDatesService;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesOverrideDTO;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesRequestDto;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesResponseDto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Controller
@RequestMapping("/eligibility")
public class EligibilityDatesController {

	private static final String ESD_OVERRIDE_URL = "/dates/override";
	private static final String ESD_VALIDATE_URL = "/dates/validate";

	public enum ClientType {INTERFACE, OVERRIDE};

	private static final Logger LOGGER = LoggerFactory.getLogger(EligibilityDatesController.class);

	@Autowired private EligibilityDatesService eligibilityDatesService;
	@Autowired private Gson platformGson;

	@RequestMapping(value = ESD_VALIDATE_URL,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<EligibilityDatesResponseDto> validateEsd(@RequestBody String eligibilityDatesRequestDtoJsonString) {
		if (LOGGER.isInfoEnabled()){
			LOGGER.info(eligibilityDatesRequestDtoJsonString);
		}
		EligibilityDatesRequestDto eligibilityDatesRequestDto = platformGson.fromJson(eligibilityDatesRequestDtoJsonString, new TypeToken<EligibilityDatesRequestDto>() {}.getType());
		EligibilityDatesResponseDto responseDto = eligibilityDatesService.validateEsd(eligibilityDatesRequestDto, null);
		if (LOGGER.isInfoEnabled()){
			LOGGER.info(responseDto.toString());
		}
		return new ResponseEntity<EligibilityDatesResponseDto>(responseDto , HttpStatus.OK);

	}

	@RequestMapping(value = ESD_OVERRIDE_URL,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<EligibilityDatesOverrideDTO> overrideEsd(@Valid @RequestBody EligibilityDatesOverrideDTO eligibilityDatesOverrideDTO) {

		if (LOGGER.isInfoEnabled()){
			LOGGER.info(eligibilityDatesOverrideDTO.toString());
		}

		EligibilityDatesRequestDto eligibilityDatesRequestDto = new EligibilityDatesRequestDto();
		eligibilityDatesRequestDto.setCmrHouseholdId(eligibilityDatesOverrideDTO.getCmrHouseholdId());
		eligibilityDatesRequestDto.setEnAppId(eligibilityDatesOverrideDTO.getEnAppId());
		eligibilityDatesRequestDto.setErAppId(eligibilityDatesOverrideDTO.getErAppId());
		eligibilityDatesRequestDto.setEnrollmentStartDate(eligibilityDatesOverrideDTO.getEnrollmentStartDate());
		eligibilityDatesRequestDto.setEnrollmentEndDate(eligibilityDatesOverrideDTO.getEnrollmentEndDate());
		eligibilityDatesRequestDto.setDentalOnly(eligibilityDatesOverrideDTO.isDentalOnly());

		boolean updated = false;
		EligibilityDatesResponseDto serviceResponse = null;
		if (eligibilityDatesOverrideDTO.getEditMode()){
			serviceResponse = eligibilityDatesService.overrideEsd(eligibilityDatesRequestDto, eligibilityDatesOverrideDTO.getFinancialEffectiveDate());
			updated = true;
		} else {
			serviceResponse = eligibilityDatesService.validateEsd(eligibilityDatesRequestDto, ClientType.OVERRIDE);
			updated = serviceResponse.isUpdated();
		}

		EligibilityDatesOverrideDTO response = new EligibilityDatesOverrideDTO();
		response.setCmrHouseholdId(eligibilityDatesOverrideDTO.getCmrHouseholdId());
		response.setEnAppId(eligibilityDatesOverrideDTO.getEnAppId());
		response.setErAppId(eligibilityDatesOverrideDTO.getErAppId());
		response.setEnrollmentStartDate(eligibilityDatesOverrideDTO.getEnrollmentStartDate());
		response.setEnrollmentEndDate(eligibilityDatesOverrideDTO.getEnrollmentEndDate());
		response.setEditMode(eligibilityDatesOverrideDTO.getEditMode());
		response.setDentalOnly(eligibilityDatesOverrideDTO.isDentalOnly());
		response.setFinancialEffectiveDate(serviceResponse.getFinancialEffectiveDate());
		response.setStatus(serviceResponse.getStatus());
		response.setUpdated(updated);

		if (LOGGER.isInfoEnabled()){
			LOGGER.info(response.toString());
		}

		return new ResponseEntity<EligibilityDatesOverrideDTO>(response, HttpStatus.OK);
	}

}