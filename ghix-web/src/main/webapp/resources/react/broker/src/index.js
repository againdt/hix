import 'react-app-polyfill/ie11';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

// Components
import App from './components/App';

// Redux Store
import appStore from './appStore';

// Styles
//import '../assets/sass/index.sass';

const root = document.getElementById('brokerReactID');

ReactDOM.render(
  <Provider store={appStore}>
    <App />
  </Provider>,
  root
);
