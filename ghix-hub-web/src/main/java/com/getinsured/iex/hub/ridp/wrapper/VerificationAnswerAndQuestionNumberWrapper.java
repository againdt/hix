package com.getinsured.iex.hub.ridp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.VerificationAnswerAndQuestionNumberType;

public class VerificationAnswerAndQuestionNumberWrapper implements JSONAware{
	private VerificationAnswerAndQuestionNumberType answer;

	private String questionNumber;
	private String answerChoice;
	
	public VerificationAnswerAndQuestionNumberWrapper(){
		ObjectFactory factory = new ObjectFactory();
		answer = factory.createVerificationAnswerAndQuestionNumberType();
	}
	
	public void setQuestionNumber(String questionNumber){
		this.questionNumber = questionNumber;
		this.answer.setVerificationQuestionNumber(questionNumber);
	}
	
	public void setAnswerChoice(String choice){
		this.answerChoice = choice;
		this.answer.setVerificatonAnswer(answerChoice);
	}
	
	public VerificationAnswerAndQuestionNumberType getSystemRepresentation(){
		return this.answer;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("questionNumber", this.questionNumber);
		obj.put("answer", this.answerChoice);
		return obj.toJSONString();
	}

}
