<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/consumer-dashboard-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/user-changeinfo-view" prefix="userInfoView"%>


<link rel="stylesheet" media="screen" type="text/css" href="<c:url value="/resources/css/emp-portal.css"/>">
<link rel="stylesheet" media="screen" type="text/css" href="<c:url value="/resources/css/portal-global.css"/>">
<link rel="stylesheet" media="screen" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.css"/>">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript">
	
	jQuery.validator.addMethod("phonecheck",function(value, element, param) {
		phone1 = $("#phone1").val();
		phone2 = $("#phone2").val();
		phone3 = $("#phone3").val();
		
		if ((phone1 == "" || phone2 == "" || phone3 == "") || 
			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) || 
			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)) 
		{
			return false;
		}
		var ph = $("#phone1").val() + "-" + $("#phone2").val() + "-" + $("#phone3").val();
		$("#phone").val(ph);
		$(this).parents().find('dd.phoneModCont').css('display','none');
		$(this).parents().find('dd.phone').css('display','inline');
		return true;
	});
	</script>

<style>
 .dl-horizontal {
 	margin-bottom: 0;
 }
  .dl-horizontal dt {
    font-weight: normal;
  }
  .dl-horizontal dd {
    font-weight: bold;
  }
  #account-settings dt,
  #account-settings dd {margin-bottom: 15px; }
  
  #acc-settings-info dt {
    width: 160px;    
  }
 #acc-settings-info dd {
    margin-left: 180px;
    font-weight: bold;
  }  
  #change-email label, #change-email input,
  #change-password label, #change-password input { margin: 10px 0;}
  #change-questions .form-group{ 
    float: left;
    margin: 5px 0;
  }
  .phone-group input {margin-right: 3px;}
  
  .form-horizontal#phonechangeform {
	clear: none !important;
  }
  .form-horizontal#phonechangeform label {
  	font-size: 15px;
  	width: 160px;
  }
</style>

<div class="gutter10" id="account-settings">
  <h1>Welcome Back,&nbsp;<span id="user-name">${memberName}<span></h1>
  <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
</div>
    
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <util:myStuff ffmClear="${ffmClear}"></util:myStuff>
      <util:quickLinks user="${user}"></util:quickLinks>
    </div><!--/span3-->
   <div class="span9" id="rightpanel">     
        <div class="header">
          <h4 class="span8">Account Settings</h4>
        </div>
        <div class="row-fluid">
          <dl class="dl-horizontal span12" id="acc-settings-info">
          	<dt>Login Email:</dt>
            <dd>${email} 
            </dd>
            <dt>Password:</dt>
            <dd>********* <span> <a id="pwdChange" href="#change-password" data-toggle="modal" class="btn btn-small pull-right">Change</a></dd>
            <dt class="phone">Contact Phone Number:</dt>
            <dd class="phone">${phone} <span><a id="phoneChange" href="#change-phone" data-toggle="modal" class="btn btn-small pull-right">Change</a></span></dd>
          </dl>
          <form class="form-horizontal" id="phonechangeform" name="phonechangeform" action="savephone" method="POST">
              <div class="control-group span12 hide">
                <label class="control-label" for="contactPhoneNumber">Contact Phone Number:</label>
                <label class="aria-hidden" for="phone1">Contact Phone Number 1</label>
                <label class="aria-hidden" for="phone2">Contact Phone Number 2</label>
                <label class="aria-hidden" for="phone3">Contact Phone Number 3</label>
                <div class="controls">
                      <input type="text" name="phone1" id="phone1" value="123" size="3" maxlength="3" class="input-mini" placeholder="xxx" aria-required="true">
                      <input type="text" name="phone2" id="phone2" value="123" size="3" maxlength="3" class="input-mini" placeholder="xxx" aria-required="true">
                      <input type="text" name="phone3" id="phone3" value="1231" size="4" maxlength="4" class="input-mini" placeholder="xxxx" aria-required="true">
                      <span><a id="phoneSave" href="#" class="btn btn-small pull-right">Save</a></span> 
                      <input type="hidden" name="phoneNumber" id="contactPhoneNumber" value="">     
                      <div id="contactPhone1_error"></div>
                      <div id="contactPhone3_error"></div>
                  </div>
              </div>
          </form>
        </div>
        <div class="header">
          <h4 class="span8">Security Questions</h4>
          <a href="#change-questions" data-toggle="modal" class="btn btn-small pull-right">Change Your Security Questions</a>
        </div>
        <div class="row-fluid">
          
          <dl class="dl-horizontal span10">
            <dt>Security Question 1:</dt>
            <dd>${securityQuestions1}</dd>
            <dt>Answer:</dt>
            <dd>*********</dd>
            <dt>Security Question 2:</dt>
            <dd>${securityQuestions2}</dd>
            <dt>Answer:</dt>
            <dd>*********</dd>
            <dt>Security Question 3:</dt>
            <dd>${securityQuestions3}</dd>
            <dt>Answer:</dt>
            <dd>*********</dd>
          </dl>
        </div> 
       </div>  
  </div><!--/row-fluid-->        
</div><!--/main .gutter10-->
<jsp:include page="reportChange.jsp" >
	<jsp:param name="memberName" value="${memberName}" />
</jsp:include>  

<!-- Modal Code Begins Here-->
<div id="change-password" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="password-modal" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="password-modal">Change Your Password</h3>
    </div>
      <userInfoView:changePassword redirectURL="/account/user/accountsettings">
      </userInfoView:changePassword>
</div><!--/.modal change password-->

<div id="change-questions" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="questions-modal" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="questions-modal">Change Your Security Questions</h3>
    </div>
     <userInfoView:securityQuestions redirectURL="/account/user/accountsettings"></userInfoView:securityQuestions>
</div><!--/.modal change password-->

<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js" />"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.clearForm').click(function(){
        $(this).parents("form").find("input").val("");
	});
	$('div[rel=popover]').popover({trigger:'hover'});
	$('div[rel=popover]').popover({trigger:'focus'}); 
});

var d = new Date();
$('#today_date').val((d.getMonth()+1) +'/'+ d.getDate() + '/' + d.getFullYear());

$('input[name=optionsWaive]').click(function(){
  if($('input[value=option5]').is(":checked")) {
    if ($('p#deny-insurance').length < 1){
        $('label[for=optionsWaive5]').after ('<p id="deny-insurance" class="alert-error"><small>Not having health insurance may subject you to tax penalities due to non-compliance with the individual mandate of the Affordable Care Act.</small></p>');
    }
  }

  else {
    if ($('p#deny-insurance').length >= 1){
      $('p#deny-insurance').remove();
    }
  }
});

$("#phoneChange").click(function(){
	$(this).each(function() {
		$(this).parents().find(".phone").addClass("hide");
	});
	$(this).parents().find("#phonechangeform .control-group").removeClass("hide");

});

$("#phoneSave").click(function(){
	
	$(this).parents().find(".phone").each(function() {
		$(this).parents().find(".phone").removeClass("hide");
	});
	$(this).parents().find("#phonechangeform .control-group").addClass("hide");

});


</script>
<script type="text/javascript" src="<c:url value="/resources/js/user/userinfo.js" />"></script>
<script type="text/javascript">
	var validator = $("#phonechangeform")
			.validate({
				    		rules : {
							phone3 : {
								phonecheck:true,
								number : true
							}
						},
						messages : {
							phone3 : {
							    	phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNoFilterLength' javaScriptEscape='true'/></span>",
									number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							}
						},
						onkeyup : false,
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							if ($("#" + elementId + "_error").html() != null) {
								$("#" + elementId + "_error").html('');
							}
							error.appendTo($("#" + elementId + "_error"));
						}
					});
</script>

