<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message code="label.plans"/></a></li>
            <li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message code="label.manageQHPS"/></a></li>
            <li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${planId}" />"><spring:message code="label.linkPlanDetails"/></a></li>
            <li><spring:message code="label.linkPlanHistory"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid">
		<h1><a name="skip"></a><spring:message code="pgheader.qhpRateBenefitReport"/><small></small></h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<!--  beginning of side bar -->

			<br /><br />
			
        </div><!-- end of span3 -->
        
		<div class="span9" id="rightpanel">			
			<display:table name="ratebenefits" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
			           <display:column property="creationTimestamp" titleKey="label.date"  format="{0,date,MMM dd, yyyy}" sortable="true" sortProperty="creationTimestampToSort" />			           
			           <display:column property="createdBy" titleKey="label.createdBy" sortable="false" />
			           <display:column property="ecmDocId" titleKey="label.file" sortable="false" />
			           <display:setProperty name="paging.banner.placement" value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
                       <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.one_item_found" value=''/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
			           <div class="pagination center">
						<ul>
							<li></li>
							<li>{0}</li>
							<li><a href="{3}">Next &gt;</a></li>
						</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
						<div class="pagination center">
							<ul>
								<li><a href="{2}">&lt; Prev</a></li>
								<li>{0}</li>
								<li></li>
							</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.full" value='
						<div class="pagination center">
							<ul>
								<li><a href="{2}">&lt; Prev</a></li>
								<li>{0}</li>
								<li><a href="{3}">Next &gt;</a></li>
							</ul>
						</div>
						'/>
			</display:table>
		</div><!-- end of .span9 -->
	</div>
</div>



<div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel"><spring:message  code='label.viewComment'/></h3>
	</div>
	<div id="commentDet" class="modal-body">
		<p></p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message  code='label.btnClose'/></a>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	});
});

	function getComment(commentId)
	{
		$('#commentDet').html("<p> <spring:message  code='label.loadingComment'/></p>");		
		$.ajax({
			  type: 'GET',
			  url: "../getComment",
			  data:{"commentId":commentId},
			  success: function(data) {
				  $('#modal-body').html("<p> "+ data + "</p>");
	          }	  
			});
		
	}
</script>
