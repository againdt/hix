package com.getinsured.ssap.model.financial;

/**
 * Describes IFSV tax filer category household member {@link IfsvHouseholdMember}.
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
public enum TaxFilerCategory {
  /**
   * Unknown tax filer category, meaning that nothing was set yet.
   */
  UNKNOWN,

  /**
   * Dependent of {@link #PRIMARY} tax household.
   */
  DEPENDENT,

  /**
   * Spouse of {@link #PRIMARY} tax household.
   */
  SPOUSE,

  /**
   * This is primary tax household.
   */
  PRIMARY
}
