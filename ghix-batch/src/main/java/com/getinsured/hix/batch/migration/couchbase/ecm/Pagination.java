package com.getinsured.hix.batch.migration.couchbase.ecm;

public final class Pagination {
	private int totalRecords = 0;
	private final static int pageSize = 500;
	private int currentPageNo = 0;
	private int startRecordNumber = 0;
	private int endRecordNumber = 0;
	private int pageCount = 0;

	public Pagination(final int iParam) {
		if (iParam > 0) {
			totalRecords = iParam;
			pageCount = (totalRecords / pageSize) + (((totalRecords % pageSize) != 0) ? 1 : 0);
		}
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public int getStartRecordNumber() {
		return startRecordNumber;
	}

	public int getEndRecordNumber() {
		return endRecordNumber;
	}

	public boolean isNext() {
		currentPageNo = currentPageNo + 1;
		startRecordNumber = endRecordNumber + 1;
		endRecordNumber = currentPageNo * pageSize;
		boolean next = (currentPageNo <= pageCount);
		return next;
	}

}
