$(document).ready(function(){

//Radio Button: show/hide div 
//Entity Info Page - Education Grant 
  var secret = $('.secret');
  secret.hide();
  $('input:radio[name="educationGrant"]').change(function(){
    if ($(this).is(':checked') && $(this).val() === 'yes') {
      $('#wrap').find('#grantDetails input').attr('data-required', 'true');
      secret.fadeToggle();
    }
    else{
      $('#wrap').find('#grantDetails input').attr('data-required', 'false');
      secret.hide();
      }
	});


//Assisters Page - Assister Certification Number
  $('input:radio[name="assisterCertified"]').change(function(){
    if ($(this).is(':checked') && $(this).val() === 'yes') {
       secret.fadeToggle();
    }
    else{
      secret.hide();
      }
  });

  //Assisters Page - "Save Assister" button hides form and adds "Add" button
  var assisterNewForm = $('#newAssister').html();
  $('.addPlusButton, .roster').hide();
  $('#newAssister').find('.saveAssister').on("click", function(e){
    $('#newAssister, p').hide('slow');
    $('.addPlusButton, .roster').show('slow');
    e.preventDefault();
    $('.add').click(function(){
      $('.addPlusButton').hide('slow');
      $(assisterNewForm).appendTo('.roster');
    });
  });

//Payment Method Page - ETF
  $('input:radio[name="paymentMethod"]').change(function(){
    if ($(this).is(':checked') && $(this).val() === 'option2') {
      $('#wrap').find('#bankAddress input').attr('data-required', 'true');
       secret.fadeToggle();
    }
    else{
      $('#wrap').find('#bankAddress input').attr('data-required', 'false');
      secret.hide();
      }
  });




// Add active class to the open accordion header
  $('.trigger').click(function(){
    $(this).toggleClass('step-active');
  });

});