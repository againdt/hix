<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="label.addPaymentMethod"/></li>
        </ul><!--page-breadcrumb ends-->
		<%-- <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
		<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
    </div>

		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                 <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 			<jsp:param name="pageName" value="financialInfo"/>
 	 	 	 	 </jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span9"><spring:message  code="pgheader.financialInformation"/></h4>
                    <a class="btn btn-small" href="<c:url value="/planmgmt/issuer/account/profile/financial/info/list"/>"><spring:message  code="label.cancel"/></a> 
					<a class="btn btn-primary btn-small" id="save-btn" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                </div>
				<div class="gutter10">
					<p><spring:message  code="label.verifyBankInfo"/></p>
			
				<form class="form-horizontal" id="frmIssuerAcctProfAddFinInfo" name="frmIssuerAcctProfAddFinInfo" action="<c:url value="/planmgmt/issuer/financial/info/save" />" method="POST">
                    <df:csrfToken/>
                    <input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/>
                    <input type="hidden" id="currentPaymentId" name="currentPaymentId" value="<encryptor:enc value="${curent_payment_id}"></encryptor:enc>"/>
                    <input type="hidden" id="currentBankAccountNumber" name="currentBankAccountNumber" value="<encryptor:enc value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}"></encryptor:enc>"/>
					<input type="hidden" id="accountNumChanged" name="accountNumChanged" value="<encryptor:enc value="false"></encryptor:enc>"/>
					                  
                    <div class="control-group">
						<label class="control-label" for="account-name"><spring:message  code="label.paymentMethodName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="paymentMethodName" id="paymentMethodName" class="input-large" value="${paymentMethodsObj.paymentMethodName}"/>
                            <div id="paymentMethodName_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
					
                    <div class="control-group">
						<label class="control-label" for="bankName"><spring:message  code="label.bankName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.bankInfo.bankName" id="bankName" class="input-large" value="${paymentMethodsObj.financialInfo.bankInfo.bankName}"/>
                            <div id="bankName_error" class=""></div>
						</div>
					</div><!-- end of control-group--> 
					
					<div class="control-group">
						<label class="control-label" for="bankABARoutingNumber"><spring:message  code="label.abaRoutingNumber"/><a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover" href="#"><i class="icon-question-sign"></i></a> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<c:choose>
							    <c:when test="${not empty routing_number && isPaymentGateway eq true}">
							        <input type="text"  name="financialInfo.bankInfo.routingNumber" id="bankABARoutingNumber" class="input-large" value="${routing_number}" maxlength="9"/>
							    </c:when>
							    <c:otherwise>
							        <input type="text"  name="financialInfo.bankInfo.routingNumber" id="bankABARoutingNumber" class="input-large" value="${paymentMethodsObj.financialInfo.bankInfo.routingNumber}" maxlength="9"/>
							    </c:otherwise>
							</c:choose>
                            <div id="bankABARoutingNumber_error" class=""></div>
						</div>
					</div><!-- end of control-group-->                  
                    
                    <div class="control-group">
						<label class="control-label" for="bankAccountNumber"><spring:message  code="label.bankAcctNumber"/><a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover" href="#"><i class="icon-question-sign"></i></a> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.bankInfo.accountNumber" id="bankAccountNumber" class="input-large" value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}" maxlength="40"/>
                            <div id="bankAccountNumber_error" class=""></div>
						</div>
					</div><!-- end of control-group-->                    
                    
                    <div class="control-group">
						<label for="nameOnAccount" class="control-label required"><spring:message code="label.nameOnAccount" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.bankInfo.nameOnAccount"
								value="${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}"
								id="nameOnAccount" maxlength="20" class="input-xlarge" size="30" placeholder="">
							<div id="nameOnAccount_error"></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label for="accountType" class="control-label required"><spring:message code="label.bankAcctType"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select id="accountType"
								name="financialInfo.bankInfo.accountType">
								<option value=""><spring:message code="label.selectUpperCase"/></option>
								<option
									<c:if test="${'C' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> SELECTED </c:if>
									value="C"><spring:message code="label.bankAcctTypeChecking"/></option>
								<option
									<c:if test="${'S' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> SELECTED </c:if>
									value="S"><spring:message code="label.bankAcctTypeSaving"/></option>
							</select>
							<div id="accountType_error"></div>
						</div>						
						<!-- end of controls-->
					</div>
					<div class="controls" id="autoPayCheckBox">
							<label class="checkbox"> <input type="checkbox" name="chkboxAutoPay" id="chkboxAutoPay" <c:if test="${paymentMethodsObj.isDefault == 'Y' || curent_payment_id == 0 }">checked="checked" disabled="disabled"</c:if> <c:if test="${paymentMethodsObj.status == 'InActive'}">disabled="disabled"</c:if>> 						
								<spring:message  code="label.setAsDefaultPaymentMethod"/>
							</label>
						
					</div><!-- end of control-group-->
                    <div class="form-actions paddingLR20">
                    	
                   	</div>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>

<div id="dialog-modal" style="display: none;">
  	<p>
	  	<c:if test="${not empty invalid_routing_number && isPaymentGateway eq true}">
	    	${invalid_routing_number} <br/>
		</c:if>
	</p>
</div>

<script type="text/javascript">
	function submitForm(){
		$("#save-btn").attr("disabled","true");
		if($("#paymentMethodName").val() != "" && $("#bankName").val() && $("#bankABARoutingNumber").val() && $("#bankAccountNumber").val() && $("#nameOnAccount").val() && $("#accountType").val()){
			if($("#chkboxAutoPay").is(':checked')){		
				var currPaymentId = '${curent_payment_id}';
				 // ask for confirmation 
				var didConfirm = confirm("This will make current payment method as default and remove default setting of other payment method. Is it ok?");
				if (didConfirm == true) {
					validateRoutingNumber();
				}else{
				        $("#save-btn").removeAttr("disabled");
				}		
			}
			else{		
				validateRoutingNumber();
			}
		}else{
			validateRoutingNumber();
		}
		
	} 
	
	$('.ttclass').tooltip();
	
	jQuery.validator.addMethod("nonNegative", function(value, element, param) {
		if(value < 0){
			return false; 
		}
		return true;
	});

	var validator = $("#frmIssuerAcctProfAddFinInfo").validate({ 
		rules : {
			'paymentMethodName' : {required : true},
			'financialInfo.bankInfo.bankName' : {required : true},			
			'financialInfo.bankInfo.accountNumber' : {required : true, number : true, nonNegative:true},
			'financialInfo.bankInfo.nameOnAccount' : {required : true},
			'financialInfo.bankInfo.routingNumber' : {required : true, number : true, minlength : 9,  maxlength : 9,nonNegative:true},
			'financialInfo.bankInfo.accountType': {required : true}
		},
		messages : {
			'paymentMethodName' : { required : "<span><em class='excl'>!</em><spring:message  code='err.paymentMethodName' javaScriptEscape='true'/></span>"},
			'financialInfo.bankInfo.bankName': { required : "<span><em class='excl'>!</em><spring:message  code='err.bankName' javaScriptEscape='true'/></span>"},				
			'financialInfo.bankInfo.accountNumber' : { required : "<span><em class='excl'>!</em><spring:message  code='err.acctNumber' javaScriptEscape='true'/></span>",
				nonNegative :"<span><em class='excl'>!</em><spring:message  code='err.bankAcctNumberValid' javaScriptEscape='true'/></span>"},
			'financialInfo.bankInfo.nameOnAccount' : { required : "<span> <em class='excl'>!</em><spring:message code='err.nameOnAccount' javaScriptEscape='true'/></span>"},
			'financialInfo.bankInfo.routingNumber' : { required : "<span><em class='excl'>!</em><spring:message  code='err.abaRoutingNumber' javaScriptEscape='true'/></span>",
				nonNegative :"<span><em class='excl'>!</em><spring:message  code='err.bankInvalidRoutingNumber' javaScriptEscape='true'/></span>"},
			'financialInfo.bankInfo.accountType' : { required : "<span> <em class='excl'>!</em><spring:message code='err.bankAcctType' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});

 $('.code').popover({ html: true,
	  trigger: 'hover',
	  placement: 'top',
	  content: function(){return '<img src="'+$(this).data('img') + '" />';}
 });
 
 function validateRoutingNumber(){
	 $("#frmIssuerAcctProfAddFinInfo").submit(); 
 }
 </script>
<script type="text/javascript">
$(document).ready(function()
{
	//alert('${invalid_routing_number}');
	if('${invalid_routing_number}' != null && '${invalid_routing_number}' != '' && '${invalid_routing_number}' != null && '${invalid_routing_number}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		     modal: true,
			 title: "Invalid Information",
			 buttons: {
			   Ok: function() {
			    $( this ).dialog( "close" );
			   }
			   }
		    });
	}
});
</script>
