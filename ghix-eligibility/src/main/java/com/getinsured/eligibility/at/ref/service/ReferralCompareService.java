package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;

/**
 * @author chopra_s
 * 
 */
public interface ReferralCompareService {
	void executeCompare(HouseholdMember primaryHouseholdMember, SingleStreamlinedApplication singleStreamlinedApplication, long enrolledApplicationId);
	
	void executeCompareFromDto(AccountTransferRequestDTO accountTransferRequestDTO);
}
