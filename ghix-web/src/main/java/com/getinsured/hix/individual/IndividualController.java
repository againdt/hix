package com.getinsured.hix.individual;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.service.jpa.ConsumerComposite;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.individual.service.ExternalIndividualService;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.GhixConfiguration;
import com.thoughtworks.xstream.XStream;

@Controller
public class IndividualController {

	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualController.class);

	@Autowired
	UserService userService;

	@Autowired
	private DesignateService designateService;

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private BrokerService brokerService;
	
	@Autowired
	IBrokerRepository brokerRepository;
	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	@Autowired
	private ExternalIndividualService externalIndividualService;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;	
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@Autowired
	private AccountActivationService accountActivationService;
	
	private static final String PAGE_AFTER_SWITCH_BACK = "pageAfterSwitchBack";
	private static final String LAST_VISITED_URL_BEFOR_SWITCH = "lastVisitedURLBeforSwitch";
	private static final String LAST_VISITED_URL = "lastVisitedURL";
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static final String BROKER_ROLE = "BROKER";
	private static final String DESIGNATED_BROKER_PROFILE = "designatedBrokerProfile";
	/**
	 * This method will be invoked when the individual successfully logs in
	 * 
	 * @param individualId
	 *            the individual user id
	 * @param request
	 *            the HTTPRequest object
	 */

	/**
	 * This method will be invoked when the individual successfully logs in
	 * 
	 * @param request
	 *            the HttpServletRequest object
	 * @param model
	 *            the Model object
	 * @return the URL for the landing page i.e. dashboard. If user is invalid,
	 *         flow is redirected to the login page
	 */
	@RequestMapping(value = "/individual/dashboard", method = RequestMethod.GET)
	public String showDashboard(HttpServletRequest request, Model model) {

		String showPopupInFuture = (String) request.getSession().getAttribute("showPopupInFuture");

		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException ex) {
			LOGGER.error("User not logged in");
			return "redirect:/account/user/login";
		}

		boolean isBrokerSwitchToIndvView = userService.getDefaultRole(user).getName()
				.equalsIgnoreCase(RoleService.INDIVIDUAL_ROLE);
		if (!isBrokerSwitchToIndvView) {
			model.addAttribute("isBrokerSwitchToIndvView", "Y");

			String individualId = (String) request.getSession().getAttribute("switchToModuleId");
			AccountUser individualUser = userService.findById(Integer.parseInt(individualId));

			request.getSession().setAttribute("switchToIndvName",
					individualUser.getFirstName() + " " + individualUser.getLastName());

			// Call to render "Your Broker" Menu and To insert user preference
			// in designate_broker
			Character showPopup = showPopupInFuture != null && !"".equals(showPopupInFuture)
					&& !"null".equalsIgnoreCase(showPopupInFuture) ? showPopupInFuture.charAt(0) : null;

			/**
			 * TODO change set designate broker for external individual while
			 * implementing switch user functionality for CA
			 */
			setDesignatedBroker(Integer.parseInt(individualId), request, showPopup);

		} else {
			model.addAttribute("isBrokerSwitchToIndvView", "N");
		}

		request.getSession().removeAttribute("showPopupInFuture");

		return "individual/dashboard";
	}

	/**
	 * This method will always be called from Individual landing page.
	 * 
	 * @param individualId
	 *            the individual user id
	 * @param request
	 *            the HTTPRequest object
	 * 
	 * @param showPopupInFuture
	 *            the flag set not to show switch message again
	 */
	private void setDesignatedBroker(int individualId, HttpServletRequest request, Character showPopupInFuture) {

		DesignateBroker designateBroker = designateService.findBrokerByIndividualId(individualId);

		if (designateBroker != null) {
			Broker broker = brokerRepository.findById(designateBroker.getBrokerId());

			request.getSession().setAttribute("designateBroker", designateBroker);
			request.getSession().setAttribute("designatedBrokerProfile", broker);

			// set switch to employer portal pop-up value in designate_broker
			if (showPopupInFuture != null) {
				designateService.updateUserPreference(showPopupInFuture, designateBroker);
			}
		}
	}

	/**
	 * Retrieves individuals details.
	 * 
	 * @param encHouseHoldId
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate result page
	 * @throws InvalidUserException 
	 * @throws GIException 
	 */
	@RequestMapping(value = "/individual/individualcase/{encryptedId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String getIndividualCaseDetails(@PathVariable("encryptedId") String encryptedId, Model model, HttpServletRequest request) throws InvalidUserException, GIException{

		LOGGER.info("individual contact info page ");
		model.addAttribute("page_title", "Getinsured Health Exchange: Individuals Details Page");
		String decryptedId = null;
		int houseHoldId;
		AccountUser user  = null;
		try{
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			houseHoldId = Integer.parseInt(decryptedId);
			user = userService.getLoggedInUser();
			if(user.getActiveModuleName().equalsIgnoreCase("Broker") && !validateBroker(houseHoldId, request)){
				return "redirect:"+getDashboardPage(request);
			}
			getIndividualDetails(houseHoldId, model,request);
			request.getSession().setAttribute(LAST_VISITED_URL, "/individual/individualcase/"+encryptedId);
		}catch (NumberFormatException nfe){
	    	String errMsg = "Failed to decrypt houseHoldId. Decrypted houseHoldId's value is: "+decryptedId+", Exception is: "+nfe;
	    	LOGGER.error(errMsg);
	    	LOGGER.info("uploadPhoto: END ");
	    	throw new GIException("Error occured in getIndividualCaseDetails. "+errMsg);
		}

		return "individual/individualcasedetails";

	}

	/**
	 * Method to check if Consumer has activated account or not.
	 * 
	 * @param brokerId The broker identifier.
	 * @param householdId The Household identifier.
	 * @return isConsumerActivated The Consumer activated status flag.
	 */
	private boolean isConsumerAccountActivated(Integer brokerId, Integer householdId) {
		boolean isConsumerActivated = true;
		
		AccountActivation accountActivation =  accountActivationService.getAccountActivationByCreatedObjectId(householdId, GhixRole.INDIVIDUAL.getRoleName(), brokerId, GhixRole.BROKER.getRoleName());
		
		if((accountActivation == null || !accountActivation.getStatus().equals(AccountActivation.STATUS.PROCESSED))) {
			isConsumerActivated = false;
		}
		
		return isConsumerActivated;
	}
	
	/**
	 * Retrieves individual details and set it to model object
	 * 
	 * @param id
	 *            Individual Identification Number
	 * @param model
	 *            Model
	 */
private void getIndividualDetails(Integer individualId, Model model,HttpServletRequest request) {
		
		LOGGER.info("getIndividualDetails : STARTS");
        
 		Integer startRecord = 0;
		AccountUser user=null;
		final String IS_INDIVIDUAL_USER_EXISTS = "isIndividualUserExists";
		
		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Broker broker = null;
		ConsumerComposite consumerComposite = new ConsumerComposite();;	
		DesignateBroker designateBroker = null;
		designateBroker = designateService.findBrokerByIndividualId(individualId);
		if(designateBroker!= null){
			broker = brokerRepository.findById(designateBroker.getBrokerId());
		}
		request.getSession().setAttribute(DESIGNATED_BROKER_PROFILE, broker);
		
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
			Map<String, Object> map = new HashMap<>();
			Map<String, Object> searchCriteria = new HashMap<String, Object>();
			searchCriteria.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf(individualId));
			if(broker != null){
				searchCriteria.put(BrokerConstants.MODULE_ID, broker.getId());
				searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.BROKER);
			}else if(user.getActiveModuleName().equalsIgnoreCase("assisterenrollmententity")){
				searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.ENTITY);
				searchCriteria.put(BrokerConstants.MODULE_ID,user.getActiveModuleId());
			}
			searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
			map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);
			List<ConsumerComposite> individualList = new ArrayList<ConsumerComposite>();
			Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);
			if(houseHoldMap!=null && houseHoldMap.size()>0) {
				individualList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
				if(!individualList.isEmpty()){
					consumerComposite = individualList.get(0);
				}
			}	
		} else {
			populateIndividualDetails(consumerComposite, individualId);
		}
		
		model.addAttribute("individualUser", consumerComposite);
		
		if(0 == consumerComposite.getConsumerUserId()) {
			model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, false);
		}
		else {
			model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, true);
		}
		
		if(designateBroker != null)
		{
			model.addAttribute("status", designateBroker != null ? designateBroker.getStatus() : "");
			String showSwitchRolePopup = designateBroker != null ? designateBroker.getShow_switch_role_popup().toString() : "Y";
			model.addAttribute("showSwitchRolePopup", showSwitchRolePopup);
			model.addAttribute("brokerId", designateBroker.getBrokerId());	

			model.addAttribute("isConsumerActivated", isConsumerAccountActivated(designateBroker.getBrokerId(), designateBroker.getIndividualId()));
		}
		/*{
			Household hhIndividual = getHouseHoldById(id);
			model.addAttribute("individualUser", hhIndividual);
		}*/
		
		LOGGER.info("getIndividualDetails : ENDS");
	}

	private void populateIndividualDetails(ConsumerComposite hhIndividual, Integer individualId){
		ExternalIndividual externalIndividual = externalIndividualService.findByExternalIndividualID(individualId);
		
		if(externalIndividual!=null){
			hhIndividual.setId(externalIndividual.getId());
			hhIndividual.setFirstName(externalIndividual.getFirstName());
			hhIndividual.setLastName(externalIndividual.getLastName());
			hhIndividual.setEmailAddress(externalIndividual.getEmail());
			hhIndividual.setPhoneNumber(String.valueOf(externalIndividual.getPhone()));
			hhIndividual.setAddress1(externalIndividual.getAddress1());
			hhIndividual.setAddress2(externalIndividual.getAddress2());
			hhIndividual.setCity(externalIndividual.getCity());
			hhIndividual.setState(externalIndividual.getState());
			hhIndividual.setZip(externalIndividual.getZip());
			hhIndividual.setEligibilityStatus(externalIndividual.getEligibilityStatus());
		}
	}
	
	protected boolean validateBroker(Integer id, HttpServletRequest request)
			throws InvalidUserException {
		
		boolean validBroker=false;
		String switchToModuleName = (String) request.getSession().getAttribute("switchToModuleName");
		//if logged in user is broker/CEC... then check if this broker/CEC is designated to same broker/CEC...or send broker/CEC to defaultLanding page
		AccountUser user = userService.getLoggedInUser();
		
		if("broker".equalsIgnoreCase(switchToModuleName)) {			
			//if user is came here with switching then get broker with active-module-id
			Broker broker;
			if("Y".equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW))){
				broker=brokerService.getBrokerDetail(user.getActiveModuleId());
			}else{
				 broker=brokerService.findBrokerByUserId(user.getId());
			}
			
			DesignateBroker designateBroker=designateService.findBrokerByIndividualId(id);
			if(broker ==null || designateBroker==null){
				validBroker=false;
			}else if(   broker.getId()!=designateBroker.getBrokerId() || "InActive".equalsIgnoreCase(designateBroker.getStatus().toString())  ){
				validBroker=false;
			}else{
				validBroker=true;
			}
			
		}		
		return validBroker;
	}
	
	private String getDashboardPage(HttpServletRequest request) {
		String roleName=(String) request.getSession().getAttribute("switchToModuleName");
		Role activeRole = roleService.findRoleByName(StringUtils.upperCase( roleName));
		if(BROKER_ROLE.equalsIgnoreCase(StringUtils.upperCase( roleName))){
			request.getSession().removeAttribute(LAST_VISITED_URL);
			//This if has been added to keep last_visited functionality of Agent switch to employer.
		}
		return activeRole.getLandingPage();
	}
	
	private Household getHouseHoldById(int id){
		
		LOGGER.info("getHouseHoldById : STARTS");
		Household household = null;
		XStream xstream = null;
		ConsumerResponse consumerResponse = null;
		
		//get the household using rest call
		try{
			LOGGER.debug("Rest call to get householeObj starts");
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, String.valueOf(id), String.class);
			LOGGER.debug("rest call to get householeObj ends");
			xstream = GhixUtils.getXStreamStaxObject();
			if(response != null){
				consumerResponse = (ConsumerResponse) xstream.fromXML(response);
				household = consumerResponse.getHousehold();
			}
		}catch(Exception ex){
			LOGGER.error("Rest call to find householdObj failed", ex);
		}		
		LOGGER.info("getHouseHoldById : ENDS");
		return household;
	}

	/**
	 * Retrieves individuals details.
	 * 
	 * @param encHouseHoldId
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate result page
	 * @throws GIException 
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/individual/viewindividualdetail/{encryptedId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String viewIndividualDetail(@PathVariable("encryptedId") String encryptedId, Model model, HttpServletRequest request,
			@RequestParam(value="desigStatus",required=false) String  desigStatus) throws GIException {

		LOGGER.info("individual contact info page ");
		model.addAttribute("page_title", "Getinsured Health Exchange: Individuals Details Page");
		String decryptedId = null;
		int houseHoldId;
		Household household = null;
		try{
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			houseHoldId = Integer.parseInt(decryptedId);
		}catch (NumberFormatException nfe){
	    	String errMsg = "Failed to decrypt HouseHold id. Decrypted HouseHoldId's value is: "+decryptedId+", Exception is: "+nfe;
	    	LOGGER.error(errMsg);
	    	LOGGER.info("viewIndividualDetail: END ");
	    	throw new GIException("Error occured in iewIndividualDetail. "+errMsg);
	    }
		
		
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			household = new Household();
			
			household.setEmail(request.getParameter("emailAddress")!=null?request.getParameter("emailAddress"):"");
			household.setFirstName(request.getParameter("firstName")!=null?request.getParameter("firstName"):"");
			household.setLastName(request.getParameter("lastName")!=null?request.getParameter("lastName"):"" );
			household.setId(houseHoldId);
			household.setPhoneNumber(request.getParameter("phoneNumber")!=null?request.getParameter("phoneNumber"):"");
			 
		}else{
			household =  getHouseHoldById(houseHoldId);
		}
		
		model.addAttribute("individualUser", household);
		if(desigStatus!=null && !StringUtils.isEmpty(desigStatus))
		{
			model.addAttribute("desigStatus", desigStatus);
		}
		
		String userActiveRoleName = (String) request.getSession().getAttribute("userActiveRoleName");
		request.getSession().setAttribute("userActiveRoleName", userActiveRoleName.toLowerCase());

		return "individual/viewindividualdetail";
	}

	/**
	 * Retrieves individual comments.
	 * 
	 * @param id
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate result page
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/individual/individualcomments/{encryptedId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String getIndividualComments(@PathVariable("encryptedId") String  encryptedId, Model model, HttpServletRequest request) throws InvalidUserException {

		LOGGER.info("individual comments page ");
		int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		model.addAttribute("page_title", "Getinsured Health Exchange: Individuals Comments");
		AccountUser user  =  userService.getLoggedInUser();
		
		if( user.getActiveModuleName().equalsIgnoreCase("Broker") && !validateBroker(id, request)){
			return "redirect:"+getDashboardPage(request);
		}
		getIndividualDetails(id, model,request);
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		model.addAttribute("userActiveRoleName",userService.getDefaultRole(user).getName());
		if (GhixConfiguration.IS_CA_CALL) {
			return "individual/externalindividualcomments";
		}
		request.getSession().setAttribute(LAST_VISITED_URL, "/individual/individualcomments/"+encryptedId);
		return "individual/individualcomments";
	}

	@RequestMapping(value = "/individual/individualincomeinfo/{encryptedId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String getIndividualIncomeInfo(@PathVariable("encryptedId") String  encryptedId, Model model, HttpServletRequest request) throws InvalidUserException {

		LOGGER.info("individual income info page ");
		int id = Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId));
		AccountUser user  =  userService.getLoggedInUser();
		model.addAttribute("page_title", "Getinsured Health Exchange: Individuals Income Information");
		if(user.getActiveModuleName().equalsIgnoreCase("Broker") && !validateBroker(id, request)){
			return "redirect:"+getDashboardPage(request);
		}
		getIndividualDetails(id, model,request);
		if (GhixConfiguration.IS_CA_CALL) {
			return "individual/externalindividualincomeinfo";
		}
		request.getSession().setAttribute(LAST_VISITED_URL, "/individual/individualincomeinfo/"+encryptedId);
		return "individual/individualincomeinfo";
	}

	private String getIndividualContactNumber(int id) {
		// Person person = personService.findByUserId(id);
		// return person != null ? person.getContactNumber() : "";
		return "";
	}

	@RequestMapping(value = "/individual/newcomment", method = { RequestMethod.GET, RequestMethod.POST })
	public String addNewComment(@RequestParam(value = "target_id", required = false) String encryptedId,
			@RequestParam(value = "target_name", required = false) String target_name,
			@RequestParam(value = "individualName", required = false) String individualName, Model model,
			HttpServletRequest request) {
		LOGGER.info("individual add new comment popup");
		
		Integer target_id = encryptedId!=null && !encryptedId.equals("") ?Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId)):null;
		model.addAttribute("page_title", "Getinsured Health Exchange:Individual Add Comment Page");
		model.addAttribute("target_id", target_id);
		model.addAttribute("target_name", target_name);

		return "individual/addnewcomment";
	}
	
	@RequestMapping(value = "/switchToIndividualView/dashboard", method ={RequestMethod.GET, RequestMethod.POST })
	public String switchToIndividualView(Model model, HttpServletRequest request) {
		 if(request.getSession().getAttribute("checkDilog")==null){
	    		String checkDilog= request.getParameter("checkAgentView");
	    		request.getSession().setAttribute("checkDilog", checkDilog);
	    	}
		 String lastVisitedURL = (String) request.getSession().getAttribute(LAST_VISITED_URL_BEFOR_SWITCH);
		 request.getSession().setAttribute(PAGE_AFTER_SWITCH_BACK, lastVisitedURL);
		
		String switchToModuleName = request.getParameter("switchToModuleName");
		String switchToModuleId = request.getParameter("switchToModuleId");
		String switchToResourceName = request.getParameter("switchToResourceName");
		
		 
		HashMap<String,String> paramMap= new HashMap<String,String>(3);
		paramMap.put("switchToModuleName", switchToModuleName);
		paramMap.put("switchToModuleId", switchToModuleId);
		paramMap.put("switchToResourceName", switchToResourceName);
		 
		return "forward:/account/user/switchUserRole?ref="+GhixAESCipherPool.encryptParameterMap(paramMap);
	}
	
	
	@RequestMapping(value = "individual/checkIndividualAccount/{encIndividualId}", method =RequestMethod.GET)
	 @ResponseBody
	 public String checkConsumerAccountActivated(@PathVariable("encIndividualId") String encIndividualId, HttpServletRequest request){
	  String accountActivation = "unActivated";
	  Broker brokerObj = new Broker();
	  try{
	   String individualId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIndividualId);
	   return accountActivation = individualAccountStatus(Integer.parseInt(individualId));
	  }catch(Exception e){
	   LOGGER.error("Exception occured while checkIndividualAccount:", e);
	   throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
	  }
	 }
	 
	private String individualAccountStatus(Integer IndividualId){
		String accountActivation = "unActivated";
		Integer startRecord = 0;
		AccountUser user=null;
	  
		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Household hhIndividual = entityUtils.getHouseHoldById(id);  
		Broker broker = null;
		ConsumerComposite consumerComposite = null; 
		DesignateBroker designateBroker = null;
		
		designateBroker = designateService.findBrokerByIndividualId(IndividualId);
		if(designateBroker!= null){
			broker = brokerRepository.findById(designateBroker.getBrokerId());
		}
		
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> searchCriteria = new HashMap<String, Object>();
		searchCriteria.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf(IndividualId));
		
		if(broker != null){
			searchCriteria.put(BrokerConstants.MODULE_ID, broker.getId());
			searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.BROKER);
		}else if(user.getActiveModuleName().equalsIgnoreCase("assisterenrollmententity")){
			searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.ENTITY);
			searchCriteria.put(BrokerConstants.MODULE_ID,user.getActiveModuleId());
		}
		
		searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
		map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);
		List<ConsumerComposite> individualList = new ArrayList<ConsumerComposite>();
		Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);
		
		if(houseHoldMap!=null && houseHoldMap.size()>0) {
			individualList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
			consumerComposite =  new ConsumerComposite();
			
			if(!individualList.isEmpty()){
				consumerComposite = individualList.get(0);
			}
		} 
	  
		if(0 == consumerComposite.getConsumerUserId()) {
			accountActivation = "unActivated";
		}
		else {
			accountActivation = "Activated";
		}
		return accountActivation;
	 	}
	}
