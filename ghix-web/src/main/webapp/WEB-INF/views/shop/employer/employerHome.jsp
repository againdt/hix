<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<style>
.gray {
color: #666 !important;
background: #DFDFDF !important;
}
</style>

<div class="gutter10">
  <div class="row-fluid">
            <div class="span3" id="sidebar">
              <h3>Notifications</h3>
             
                      <div class="alert fade in">
                    <a href="#" data-dismiss="alert" class="close">&times;</a>
                    <p><strong>Pending: </strong> <br>Message from State HIX administrator: <br>Subject: EIN Verification </p>
                     <p><a href="/hix/inbox/home">View inbox</a> </p> 
                  </div>
                   <div class="alert alert-info">
                            <a href="#" data-dismiss="alert" class="close">&times;</a>
                            <p><strong>New!</strong> <br>E-mail from John Pritkin: <br>Subject: Question about employer contribution</p> 
                    <p><a href="/hix/inbox/home">View inbox</a> </p> 
                  </div>      
                  <h4 class="graydrkbg">Quick LInks</h4>
                    <ul class="nav nav-list">
                  <li><a href="#"><i class="icon-home"></i> Post Announcement</a></li>
                  <li><a href="#"><i class="icon-signal"></i> View Trend Reports</a></li>
                  <li><a href="#"><i class="icon-heart"></i> Customer Service Tickets</a></li>
                </ul>   
      	</div>
                <div class="span6">
                <c:if test="${csrView == true}">            
                <h3>
<strong >Viewing ${employerName} </strong> <a href="/hix/crm/crmemployer/details/${employerId}"class="btn btn-primary showcomments">My Account</a>
	</h3>
	</c:if>
                  <h3>Employee Enrollment Highlight</h3>         
<div id="container" style="width:680px; height: 400px; margin: 0 auto"></div>              
            </div><!--span9-->
                        </div> 
     </div> <!-- row-fluid -->
 <!-- gutter -->

	<script type="text/javascript">
		$(function () {
		    var chart;
		    $(document).ready(function() {
		        chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false
		            },
		            title: {
		                text: 'Employee Coverage for 2013'
		            },
		            colors: [
		                 	'#CF6F1E',
		                 	'#b1c2c5',
		                 	'#D3A900',
		                 	'#89B6CD',
		                 	'#3D96AE'
		                 	],
		                 	credits: {
		                             enabled: false
		                         },
		            tooltip: {
		                formatter: function() {
		                    return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(1)+' %';
		                }
		            },
		            exporting:{ buttons: {
						 printButton:{ enabled:false },
						 exportButton: { enabled:false }
					 	}
		  			},
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: true,
		                        color: '#000000',
		                        connectorColor: '#000000',
		                        formatter: function() {
		                            return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(1)+' %';
		                        }
		                    }
		                }
		            },
		            series: [{
		                type: 'pie',
		                name: 'Employee Coverage: 2013',
		                data: [
		                    ['Enrolled',   77.8],
		                   
		                    {
		                        name: 'Not Enrolled',
		                        y: 15.3,
		                        sliced: true,
		                        selected: true
		                    },
		                    ['Not Eligible',    8.5],
		                    ['Waived',     6.2],
		                    ['Terminated',   0.7]
		                ]
		            }]
		        });
		    });
		    
		});
			
		</script>

<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/exporting.js" />"></script> 
<script>
	$(document).ready(function() {
			$('.info').tooltip();
			$('.plans-section-header').on('click show hide',function(e) {
					$(e.target).find('i').toggleClass('icon-chevron-right icon-chevron-down',200);
				});
});
</script>