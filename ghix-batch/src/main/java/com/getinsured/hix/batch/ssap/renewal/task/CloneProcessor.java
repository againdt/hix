package com.getinsured.hix.batch.ssap.renewal.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.CloneService;
import com.getinsured.hix.batch.ssap.renewal.util.ClonePartitionerParams;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * Clone Processor class is used to return SSAP Application Model to Writer class.
 * 
 * @since July 30, 2019
 */
@Component("ssapCloneProcessor")
public class CloneProcessor implements ItemProcessor<SsapApplication, SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(CloneProcessor.class);

	private CloneService cloneService;
	private ClonePartitionerParams clonePartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Processor ");
		}

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			int partition = executionContext.getInt("partition");
			int startIndex = executionContext.getInt("startIndex");
			int endIndex = executionContext.getInt("endIndex");

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);
			}
		}
	}

	@Override
	public SsapApplication process(SsapApplication processSsapApplication) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Returning SSAP Application is null : {}", (null == processSsapApplication));
		}
		return processSsapApplication;
	}

	public CloneService getCloneService() {
		return cloneService;
	}

	public void setCloneService(CloneService cloneService) {
		this.cloneService = cloneService;
	}

	public ClonePartitionerParams getClonePartitionerParams() {
		return clonePartitionerParams;
	}

	public void setClonePartitionerParams(ClonePartitionerParams clonePartitionerParams) {
		this.clonePartitionerParams = clonePartitionerParams;
	}
}
