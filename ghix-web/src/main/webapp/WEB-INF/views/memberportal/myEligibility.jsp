<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/consumer-dashboard-util" prefix="util"%>

<link rel="stylesheet" media="screen" type="text/css" href="<c:url value="/resources/css/emp-portal.css"/>">
<link rel="stylesheet" media="screen" type="text/css" href="<c:url value="/resources/css/portal-global.css"/>">
<link rel="stylesheet" media="screen" type="text/css" href="<c:url value="/resources/css/bootstrap-responsive.css"/>">
<style>
.bold {
	font-weight: bold;
}
.normal {
	font-weight: normal;
}
.header-secondary {
    background: none repeat scroll 0 0 rgb(208, 233, 255);
    float: left;
    font-size: 16px;
    font-weight: 300;
    line-height: 40px;
    text-indent: 10px;
    margin-bottom: 15px;
    padding: 5px 10px 5px 0;
    width: 100%;
}
.header-secondary h5 {
	margin: 0;
	font-size: 14px;
}
</style>
<div class="gutter10" id="account-settings">
  <h1>Welcome Back,&nbsp;<span id="user-name">${memberName}<span></h1>
  <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
</div>
    
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <util:myStuff ffmClear="${ffmClear}"></util:myStuff>
      <util:quickLinks user="${user}"></util:quickLinks>
    </div><!--/span3-->
	<div class="span9" id="rightpanel">     
        <div class="header">
          <h4 class="span8">My Eligibility</h4>
        </div>
        <p>Congratulations! You are eligible for subsidies on your insurance premium payments and CSR</p>
        <div class="header">
          <h4 class="span8">Household Eligibility</h4>
        </div>
        <div class="gutter10">
        	<div class="header-secondary">
	        	<h5>Program: <span>${program}</h5>
	        	<h5 class="pull-left normal">Eligibility Status: <span class="bold">${eligibilityStatus}</span></h5>
	        </div>	        
		    <div class="gutter10-lr">
		    	<p>Under the Affordable Care Act, an insurance plan that is certified by the Health Insurance Marketplace, provides essential health benefits, follows established limits on cost-sharing (like deductibles, copayments, and out-of-pocket maximum amounts), and meets other requirements.</p>
		    </div>
	    </div>
	    <div class="gutter10-lr">
        	<div class="header-secondary">
        		<h5 class="normal">Premium Tax Credit: <span class="bold">${premiumTaxCreditPerYear}/year (or up to ${premiumTaxCreditPerMonth}/month)</span></h5>
        	</div>
        	<div class="gutter10-lr">
	    		<p>The Affordable Care Act provides a new tax credit to help you afford health coverage purchased through the Marketplace. Advance payments of the tax credit can be used right away to lower your monthly premium costs. You may choose how much advance credit payments to apply to your premiums each month, up to a maximum amount.</p>
	    	</div>	        
	    </div>
	    <div class="gutter10-lr">
        	<div class="header-secondary">
        		<h5 class="normal">Cost Sharing Reduction: <span>${csr}</span></h5>
        	</div>
        	<div class="gutter10-lr">
	    		<p>A discount that lowers the amount you have to pay out-of-pocket for deductibles, coinsurance, and copayments. <a href="https://www.healthcare.gov/will-i-qualify-to-save-on-out-of-pocket-costs/" target="_blank">Learn More</a></p>
	    	</div>	        
	    </div>
	    
	    <c:forEach var="member" items="${members}">
			<div class="header">
	          <h4 class="span8"><c:out value="${member.name}"/></h4>
	        </div>
			 <div class="gutter10">
	        	<div class="header-secondary">
		        	<h5>Program: <span><c:out value="${member.program}"/></h5>
		        	<h5 class="pull-left normal">Eligibility Status: <span class="bold"><c:out value="${member.eligibilityStatus}"/></span></h5>
		        	<h5 class="pull-right normal">Eligibility Period: <span class="bold"><c:out value="${member.eligibilityPeriodStart}"/> - <c:out value="${member.eligibilityPeriodEnd}"/></span></h5>
		        </div>	        
			    <div class="gutter10-lr">
			    	<p>A state-administered health insurance program for low-income families and children, pregnant women, the elderly, people with disabilities, and in some states, other adults. The Federal government provides a portion of the funding for Medicaid and sets guidelines for the program. States also have choices in how they design their program, so Medicaid varies state by state and may have a different name in your state. <a href="http://www.medicaid.gov/" target="_blank">Learn More</a></p>
			    </div>
		    </div>
		</c:forEach>
	    
	    
    </div><!--/right panel-->   
  </div><!--/row-fluid-->        
</div><!--/main .gutter10-->
