
import dataReducer, { productionInitialState, mockData } from '../dataReducer';
import { dataActionTypes } from '../../constants/actionTypes';

const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const setDataAction = {
    type: dataActionTypes.SET_DATA,
    data: { newData: 'newData' }
};

const changeDataAction = {
    type: dataActionTypes.CHANGE_DATA,
    newState: { newState: 'newState' }
};

const clearStateAction = {
    type: dataActionTypes.CLEAR_STATE
};

describe('Test for dataReducer', () => {
    it('This reducer hasn\'t been invoked', () => {
        expect(dataReducer(productionInitialState, anotherAction)).toEqual(productionInitialState);
    });

    it('Should set data', () => {
        expect(dataReducer(productionInitialState, setDataAction)).toEqual({
            'singleStreamlinedApplication': setDataAction.data
        });
    });

    it('Should change data', () => {
        expect(dataReducer(productionInitialState, changeDataAction)).toEqual(changeDataAction.newState);
    });

    it('Should clear state', () => {
        expect(dataReducer({ welcomeText: 'Hi there!' }, clearStateAction)).toEqual(mockData);
    });
});