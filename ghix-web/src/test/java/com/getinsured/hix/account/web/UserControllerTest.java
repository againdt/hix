package com.getinsured.hix.account.web;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.getinsured.hix.BaseTest;

public class UserControllerTest extends BaseTest {
	@Autowired UserController userController; 

	@Test
	public void testLoginPage(){
		Model model  =  new ExtendedModelMap();
		MockHttpServletRequest request = new MockHttpServletRequest();
		
		String viewName = userController.loginPage(model, request);
		Assert.assertEquals(viewName, "account/user/login");	}
	
	@Ignore
	public void testList() {
		Model model  =  new ExtendedModelMap();
		String viewName = "";
		//String viewName = userController.list(model);
		Assert.assertEquals(viewName, "account/user/list");
	}
	
	@Ignore
	public void testEdit() {
		Model model  =  new ExtendedModelMap();
		String viewName = "";
		//String viewName = userController.eedit(model);
		Assert.assertEquals(viewName, "account/user/edit");
	}
	
	@Ignore
	public void testloop(){
		ArrayList<String> values = new ArrayList<String>(); 
		values.add("announcement");
		values.add("announcementRole");
		values.add("role");
		values.add("id");
		String val = "";
		for(int i = 0; i < values.size(); i++ ){
			
			if(i != values.size() - 1){
				val += values.get(i)  + " join ";
				continue;
			}else{
				val +=  " getid " + values.get(i);
			}
		}
		System.out.println(val);
	}
	
	@Ignore
	public void testUnlockPersonalInfo() {
		Model model  =  new ExtendedModelMap();
		MockHttpServletRequest request = new MockHttpServletRequest();
		
		String viewName = userController.unlockPersonalInfo(model, request);
		
		Assert.assertNotNull(viewName);
		Assert.assertEquals("redirect:/account/unlock/password", viewName);
	}
}