import React, {Fragment, useEffect} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {injectIntl} from 'react-intl';
import moment from 'moment';
import * as JS_PDF from 'jspdf';
import AdditionalInformationSummary from '../summary/AdditionalInformationSummary';
import AddressItem from '../summary/AddressItem';
import ApplyingForHealthCoverageSummary from '../summary/ApplyingForHealthCoverageSummary';
import FamilyAndHouseholdSummary_NV from '../summary/FamilyAndHouseholdSummary_NV';
import FamilyAndHouseholdSummary_MN from '../summary/FamilyAndHouseholdSummary_MN';
import IncomeInformationSummary from '../summary/IncomeInformationSummary';
import PageHeader from '../headers/PageHeader';
import SummaryItem from '../summary/SummaryItem';
import {clearMethods, fetchBloodRelationships, fetchTribes, setMethods} from '../../actions/actionCreators';
import helperFunctions from '../../utils/helperFunctions';
import maskSSN from '../../utils/maskSSN';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import phoneNumberTools from '../../utils/phoneNumberTools';
import CONFIG from '../../.env';
import ENDPOINTS from '../../constants/endPointsList';
import FLOW_TYPES from '../../constants/flowTypes';
import {
    COMMUNICATION_METHODS_LIST,
    getOptionsForHardcodedState,
    REASONABLE_EXPLANATIONS_LIST
} from '../../constants/dropdownOptions';
// import Button from '@cmsgov/design-system-core/dist/components/Button/Button';

// Components

// ActionCreators

// Utils

// Constants

const NEW_PAGE_THRESHOLD = 278;
const NEW_PAGE_THRESHOLD_DIFF_MAX = 25;

/**
 *
 * @param {Object} props
 * @returns {React.Component} - Summary page for 'Sign and Review' Section
 *
 */
const Page3_16 = props => {


    const {
        dateOfBirth,
        name,
        householdContact: {
            homeAddress,
            mailingAddress,
            phone: {
                phoneNumber
            },
            otherPhone: {
                phoneNumber : _phoneNumber
            },
            contactPreferences: {
                emailAddress,
                preferredWrittenLanguage,
                preferredSpokenLanguage,
                preferredContactMethod
            }
        }
    } = props.headOfHousehold;

    const {
        data: {
            singleStreamlinedApplication: {
                authorizedRepresentative: {
                    name: authorizedRepresentativeName,
                    phone: authorizedRepresentativePhone
                },
                coverageYear
            }
        },
        flow: {
            flowType
        },
        adjustedFlow,
        intl,
        taxHousehold,
        headOfHousehold,
        householdMembers,
        relations: {
            loadedRelationsWholeList
        },
        stateCode,
        tribes
    } = props;

    const {
        buttonContainer: ButtonContainer,
        PAGES
    } = props;

    const isMN = stateCode === 'MN';
    const isNV = !isMN;

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            loadedRelationsWholeList.length === 0 &&
            props.actions.fetchBloodRelationships(`${window.location.origin}${ENDPOINTS.BLOOD_RELATIONSHIPS_ENDPOINT_WHOLE_LIST}`, true);
        }
    }, []);

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            tribes.length === 0 && props.actions.fetchTribes(`${window.location.origin}${ENDPOINTS.TRIBES_ENDPOINT}`);
        }
    }, []);

    const getBloodRelationshipsFromHousehold = () => {
        const res = householdMembers.filter((item) => item.applicantPersonType === 'PTF' || item.applicantPersonType === 'PC_PTF');
        return res.length > 0 ? res[0].bloodRelationship : [];
    };

    const bloodRelationships = getBloodRelationshipsFromHousehold();

    const isShowUSCitizen = !isMN;

    const authRepTxt = isMN ? 'label.accountHolder' : 'label.authorizedRep';

    const authRepPhoneNumber = ['CELL', 'WORK', 'HOME']
        .reduce((phoneNumber, phoneType) => phoneNumber ? phoneNumber : (ph => ph.phoneNumber ? [ph.phoneNumber, ph.phoneExtension] : null)(authorizedRepresentativePhone.find(ph => ph.phoneType === phoneType)), null);

    const getFullNumberAsString = fullPhone => {
        if(Array.isArray(fullPhone)) {
            const [ phoneNumber, phoneExtension ] = fullPhone;
            return `${phoneNumberTools
                .getAdjustedPhoneNumber(phoneNumber)} ${phoneExtension ? `ext. ${phoneExtension}` : ''}`;
        } else {
            return '';
        }
    };

    const getCoverageType = programTypes => {
        let programName = '';
        programTypes.map(program => {
            if (program.eligible) programName = program.name;
        });
        return programName;
    };

    const addNewPage = (doc, currentLine, currentLineInitialValue, page, textFontSize, pageNumberPosition, bigInterval) => {
        let isNewPage = false;
        if (currentLine % NEW_PAGE_THRESHOLD >= 0 && currentLine % NEW_PAGE_THRESHOLD <= NEW_PAGE_THRESHOLD_DIFF_MAX) {
            page += 1;
            doc.addPage();
            currentLine = currentLineInitialValue;
            doc.setFontSize(textFontSize);
            doc.text(`Page # ${page}`, pageNumberPosition, currentLine);
            isNewPage = true;
            currentLine += bigInterval;
        }

        return {
            currentLine,
            isNewPage,
            page
        };
    };

    /**
     * Function that generates PDF for printing or saving
     *
     * @param {string} action - type of action to do after PDF generation has been finished
     */
    const handleAction = action => {
        const currentLineInitialValue = 20;

        const indent = 20;
        const secondColumn = 80;
        const thirdColumn = 130;
        const forthColumn = 160;
        const pageNumberPosition = 180;

        const pageHeaderFontSize = 16;
        const headerFontSize = 14;
        const textFontSize = 10;

        const bigInterval = 10;
        const mediumInterval = 8;

        const pageHeaderColor = [ 0, 127, 174 ];
        const headerColor = [134, 196, 140];
        const regularColor = [ 0, 0, 0 ];


        let config = {
            'label.email': emailAddress || '',
            'label.phone': phoneNumberTools.getAdjustedPhoneNumber(phoneNumber) || '',
            'label.dob': moment(dateOfBirth).format('MM/DD/YYYY') || '',
            'label.address.home': homeAddress,
            'label.address.mailing': mailingAddress,
            'label.phone.second': phoneNumberTools.getAdjustedPhoneNumber(_phoneNumber) || '',
            'label.language.preferred.spoken': preferredSpokenLanguage || '',
            'label.language.preferred.written': preferredWrittenLanguage || '',
            'label.communication.preferred': COMMUNICATION_METHODS_LIST[preferredContactMethod] || '',
            [authRepTxt]: normalizeNameOfPerson(authorizedRepresentativeName) || '',
            'finalReview.repPhoneNumber': getFullNumberAsString(authRepPhoneNumber)
        };

        let page = 1;
        let doc = new JS_PDF('p', 'mm', 'a4');

        doc.enhancedText = doc.text[Symbol.for('clone-function')]();

        doc.text = function(...args) {
            let [ text, ...rest ] = args;

            const protectedValues = [ null, undefined, false, true ];

            if(protectedValues.some(value => text === value)) {
                text = '';
            }
            if (!Array.isArray(text) && typeof text !== 'string') {
                if (typeof text === 'number') {
                    text = `${text}`;
                } else {
                    text = '';
                }
            }
            doc.enhancedText.apply(this, [ text, ...rest ]);
        };

        let currentLine = currentLineInitialValue;

        // Page Header

        doc.setTextColor(...pageHeaderColor);
        doc.setFontSize(pageHeaderFontSize);
        doc.text('Final Summary', indent, currentLine);

        // Page Number

        doc.setTextColor(...regularColor);
        doc.setFontSize(textFontSize);
        doc.text(`Page # ${page}`, pageNumberPosition, currentLine);

        // Head of household section

        currentLine += bigInterval;
        doc.setFontSize(headerFontSize);
        doc.setTextColor(...headerColor);
        doc.text(normalizeNameOfPerson(headOfHousehold.name), indent, currentLine += bigInterval);
        doc.setTextColor(...regularColor);
        doc.setFontSize(textFontSize);
        currentLine += bigInterval;

        const LONG_LABEL_THRESHOLD = 45;
        const EXTEND_SECOND_COLUMN = 30;
        const ADDL_INDENT = 5;
        const secondColumnPlus = secondColumn + EXTEND_SECOND_COLUMN;

        Object.entries(config).forEach(([ name, value ]) => {
            if (isMN && ((name === 'label.language.preferred.spoken') ||
                        (name === 'label.language.preferred.written') ||
                        (name === 'finalReview.repPhoneNumber'))) {
                // skip for MN
            } else if (name === 'label.address.home' || name === 'label.address.mailing') {
                const {streetAddress1, streetAddress2, city, state, postalCode} = value;
                doc.text(intl.formatMessage({id: name}), indent, currentLine);
                doc.text(streetAddress1, secondColumnPlus, currentLine);
                currentLine += mediumInterval;
                if (streetAddress2 && streetAddress2.length > 0) {
                    doc.text('', indent, currentLine);
                    doc.text(streetAddress2, secondColumnPlus, currentLine);
                    currentLine += mediumInterval;
                }
                doc.text('', indent, currentLine);
                doc.text(`${city} ${state}, ${postalCode}`, secondColumnPlus, currentLine);
                currentLine += mediumInterval;
            } else {
                doc.text(intl.formatMessage({id: name}), indent, currentLine);
                doc.text(value, secondColumnPlus, currentLine);
                currentLine += mediumInterval;
            }
        });

        // <Applying for health coverage> section

        doc.setFontSize(headerFontSize);
        currentLine += bigInterval;
        doc.setTextColor(...headerColor);
        doc.text(intl.formatMessage({id: 'finalReview.applyingForHealthCoverage'}), indent, currentLine);
        doc.setTextColor(...regularColor);
        currentLine += bigInterval;

        // Header for Table

        doc.setFontSize(textFontSize);
        doc.text(intl.formatMessage({id: 'label.name'}), indent, currentLine);
        doc.text(intl.formatMessage({id: 'label.relationship'}), secondColumn, currentLine);
        doc.text(intl.formatMessage({id: 'label.dob'}), thirdColumn, currentLine);
        doc.text(intl.formatMessage({id: 'label.seekingCoverage'}), forthColumn, currentLine);
        currentLine += bigInterval;

        // Rows for Table

        householdMembers.forEach((hhm, i) => {

            const {
                dateOfBirth,
                applyingForCoverageIndicator
            } = hhm;

            const relationToHeadOfHousehold = bloodRelationships
                .find(rel => rel.relatedPersonId === '1' && rel.individualPersonId === String(i + 1));

            const getRelation = () => {

                let res = loadedRelationsWholeList ? loadedRelationsWholeList.find(lr => lr.code === (relationToHeadOfHousehold && relationToHeadOfHousehold.relation)) : null;

                return res ? res.text : '';
            };

            doc.text(normalizeNameOfPerson(hhm.name), indent, currentLine);
            const relationship = getRelation();
            const relationshipLines = doc.splitTextToSize(relationship, 45);
            doc.text(relationshipLines, secondColumn, currentLine);
            doc.text(moment(dateOfBirth).format('MM/DD/YYYY') || '', thirdColumn, currentLine);
            doc.text(applyingForCoverageIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'}), forthColumn, currentLine);
            currentLine += mediumInterval;
        });
        currentLine += bigInterval;

        // Household members summaries

        doc.setFontSize(headerFontSize);
        doc.setTextColor(...headerColor);
        doc.text(intl.formatMessage({id: 'finalReview.householdMembers'}), indent, currentLine);
        doc.setTextColor(...regularColor);
        currentLine += bigInterval;

        const fiveYearsAgo = moment().subtract(5, 'years').format('YYYY');

        let hhmSummaries = [];
        householdMembers.forEach(hhm => {
            const {
                applyingForCoverageIndicator,
                disabilityIndicator,
                name,
                gender,
                medicaidChipDenial,
                medicaidDeniedDate,
                medicaidDeniedDueToImmigration,
                changeInImmigrationSince5Year,
                changeInImmigrationSinceMedicaidDenial,
                blindOrDisabled,
                longTermCare,
                americanIndianAlaskaNative : {
                    memberOfFederallyRecognizedTribeIndicator,
                    tribeName,
                    state
                },
                ethnicityAndRace : {
                    hispanicLatinoSpanishOriginIndicator
                },
                socialSecurityCard: {
                    firstNameOnSSNCard,
                    middleNameOnSSNCard,
                    lastNameOnSSNCard,
                    suffixOnSSNCard,
                    socialSecurityNumber,
                    nameSameOnSSNCardIndicator,
                    reasonableExplanationForNoSSN
                },
                specialCircumstances: {
                    pregnantIndicator,
                    numberBabiesExpectedInPregnancy,
                    everInFosterCareIndicator,
                    ageWhenLeftFosterCare,
                    gettingHealthCareThroughStateMedicaidIndicator,
                    fosterCareState
                },
                citizenshipImmigrationStatus: {
                    citizenshipStatusIndicator,
                    naturalizedCitizenshipIndicator,
                    lawfulPresenceIndicator,
                    livedIntheUSSince1996Indicator,
                    honorablyDischargedOrActiveDutyMilitaryMemberIndicator,
                    citizenshipDocument: {
                        alienNumber,
                        sevisId,
                        nameOnDocument,
                        documentDescription,
                        nameSameOnDocumentIndicator,
                        visaNumber,
                        documentExpirationDate,
                        i94Number,
                        foreignPassportCountryOfIssuance,
                        cardNumber
                    },
                    otherImmigrationDocumentType: {
                        orrcertificationIndicator,
                        cubanHaitianEntrantIndicator,
                        orreligibilityLetterIndicator,
                        stayOfRemovalIndicator,
                        americanSamoanIndicator,
                        withholdingOfRemovalIndicator
                    }
                },
                householdContact: {
                    homeAddress,
                    mailingAddress,
                    mailingAddressSameAsHomeAddressIndicator
                }
            } = hhm;

            const NameOnSSNCard = {
                firstName: firstNameOnSSNCard,
                middleName: middleNameOnSSNCard,
                lastName: lastNameOnSSNCard,
                suffix: suffixOnSSNCard
            };

            const {
                stateFullNameForSummary,
                tribeNameForSummary
            } = (state => state ? { stateFullNameForSummary: state.name, tribeNameForSummary: state.tribes.find(t => t.tribeCode === tribeName).tribeName  }: {})(tribes.find(tr => tr.code === state));
            const [ stateAbbr ] = fosterCareState ? getOptionsForHardcodedState(fosterCareState) : [{}];

            const config =  {
                [`${normalizeNameOfPerson(name)}`]: {isShow: true, value: ''},
                'label.applyingForCoverage': {isShow: true, value: applyingForCoverageIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.gender': {isShow: true, value: gender ? `${gender.slice(0, 1).toUpperCase()}${gender.slice(1)}` : ''},
                'label.ssn': {isShow: true, value: socialSecurityNumber ?
                    maskSSN(socialSecurityNumber) :
                    (reasonableExplanationForNoSSN ? `--NA-- (Reason : ${REASONABLE_EXPLANATIONS_LIST[reasonableExplanationForNoSSN]})` : '--NA--')},
                'label.ssn.is.name': {isShow: !isMN, value: nameSameOnSSNCardIndicator  ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.ssn.name.shown': {
                    isShow: (!isMN && nameSameOnSSNCardIndicator !== null && nameSameOnSSNCardIndicator),
                    value: nameSameOnSSNCardIndicator !== null && !nameSameOnSSNCardIndicator ? normalizeNameOfPerson(NameOnSSNCard) : (nameSameOnSSNCardIndicator === null || nameSameOnSSNCardIndicator ? normalizeNameOfPerson(name) : '')},
                'label.citizen.us': {isShow: !isMN, value: citizenshipStatusIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.citizen.naturalized': {isShow: !isMN, value: naturalizedCitizenshipIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.alien.number': {isShow: !isMN && alienNumber, value: alienNumber},
                'label.naturalization.number': {isShow: !isMN && sevisId, value: sevisId},
                'label.immigration.status': {isShow: !isMN && !citizenshipStatusIndicator, value: lawfulPresenceIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.document.description': {isShow: !isMN && lawfulPresenceIndicator && documentDescription, value: documentDescription},
                'label.visa.number': {isShow: !isMN && lawfulPresenceIndicator && visaNumber, value: visaNumber},
                'label.passport.foreign': {isShow: !isMN && lawfulPresenceIndicator && foreignPassportCountryOfIssuance, value: foreignPassportCountryOfIssuance},
                'label.document.expiration.date': {isShow: !isMN && lawfulPresenceIndicator && documentExpirationDate, value: documentExpirationDate},
                'label.i94.number': {isShow: !isMN && lawfulPresenceIndicator && i94Number, value: i94Number},
                'label.card.number': {isShow: !isMN && lawfulPresenceIndicator && cardNumber, value: cardNumber},
                'ssap.familyHousehold.cuban.orrCertification': {isShow: !isMN && lawfulPresenceIndicator && orrcertificationIndicator, value: orrcertificationIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.cuban.haitian': {isShow: !isMN && lawfulPresenceIndicator && cubanHaitianEntrantIndicator, value: cubanHaitianEntrantIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.refugee.resettlement': {isShow: !isMN && lawfulPresenceIndicator && orreligibilityLetterIndicator, value: orreligibilityLetterIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.staying.issued': {isShow: !isMN && lawfulPresenceIndicator && stayOfRemovalIndicator, value: stayOfRemovalIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.american.samoa': {isShow: !isMN && lawfulPresenceIndicator && americanSamoanIndicator, value: americanSamoanIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.withholding.removal': {isShow: !isMN && lawfulPresenceIndicator && withholdingOfRemovalIndicator, value: withholdingOfRemovalIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.name.shown.document': {isShow: !isMN && lawfulPresenceIndicator && nameSameOnDocumentIndicator !== null && !nameSameOnDocumentIndicator, value: normalizeNameOfPerson(nameOnDocument)},
                'label.residence.primary.since.1996': {isShow: !isMN && lawfulPresenceIndicator && livedIntheUSSince1996Indicator, value: livedIntheUSSince1996Indicator},
                'label.military.discharged': {isShow: !isMN && lawfulPresenceIndicator && honorablyDischargedOrActiveDutyMilitaryMemberIndicator, value: honorablyDischargedOrActiveDutyMilitaryMemberIndicator},
                'label.hispanic.latino.origin': {isShow: !isMN, value: hispanicLatinoSpanishOriginIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.american.indian.alaskan.native': {isShow: !isMN, value: memberOfFederallyRecognizedTribeIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.membership.info': {isShow: !isMN && memberOfFederallyRecognizedTribeIndicator, value: ''},
                'label.state': {isShow: !isMN && memberOfFederallyRecognizedTribeIndicator, value: stateFullNameForSummary},
                'label.tribe.name': {isShow: !isMN && memberOfFederallyRecognizedTribeIndicator, value: tribeNameForSummary},
                'label.medicaid.not.eligible': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW, value: medicaidChipDenial ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.medicaid.denied.chip': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && medicaidChipDenial, value: moment(medicaidDeniedDate).format('MM/DD/YYYY')},
                'label.medicaid.not.eligible.since': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && medicaidChipDenial, value: medicaidDeniedDueToImmigration ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.immigration.status.change.since': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && medicaidChipDenial, value: changeInImmigrationSince5Year ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.immigration.status.change.medicaid': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && medicaidChipDenial, value: changeInImmigrationSinceMedicaidDenial ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.pregnant.is': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && gender.toUpperCase() === 'FEMALE', value: pregnantIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.pregnant.num.babies': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && pregnantIndicator, value: numberBabiesExpectedInPregnancy},
                'label.disability.has': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW, value: disabilityIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.foster.care': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW, value: everInFosterCareIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.foster.care.system.state': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && everInFosterCareIndicator, value: stateAbbr.text},
                'label.health.medicaid': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && everInFosterCareIndicator, value: gettingHealthCareThroughStateMedicaidIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.foster.care.age': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW && everInFosterCareIndicator, value: ageWhenLeftFosterCare},
                'label.disability.physical.mental.health': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW, value: blindOrDisabled ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.activities.help': {isShow: !isMN && flowType === FLOW_TYPES.FINANCIAL_FLOW, value: longTermCare ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                'label.address.home': {isShow: isMN, value: homeAddress},
                'label.address.mailing': {isShow: isMN, value: {mailingAddress, mailingAddressSameAsHomeAddressIndicator}}
            };
            hhmSummaries.push(config);
        });

        hhmSummaries.forEach((summary, idx) => {
            Object.entries(summary).forEach(([ name, {isShow, value} ], entryIdx) => {
                ({
                    currentLine,
                    page
                } = addNewPage(doc, currentLine, currentLineInitialValue, page, textFontSize, pageNumberPosition, bigInterval));

                if(entryIdx === 0) {
                    doc.setTextColor(...headerColor);
                    doc.setFontSize(headerFontSize);
                    doc.text(intl.formatMessage({id: name}), indent, currentLine);
                    doc.text(value, secondColumnPlus, currentLine);
                    doc.setTextColor(...regularColor);
                    currentLine += bigInterval;
                } else {
                    if (isShow && name === 'label.address.home') {
                        const {streetAddress1, streetAddress2, city, state, postalCode} = value;
                        doc.text(intl.formatMessage({id: name}), indent, currentLine);
                        doc.text(streetAddress1, secondColumnPlus, currentLine);
                        currentLine += mediumInterval;
                        if (streetAddress2 && streetAddress2.length > 0) {
                            doc.text('', indent, currentLine);
                            doc.text(streetAddress2, secondColumnPlus, currentLine);
                            currentLine += mediumInterval;
                        }
                        doc.text('', indent, currentLine);
                        doc.text(`${city} ${state}, ${postalCode}`, secondColumnPlus, currentLine);
                        currentLine += mediumInterval;
                    } else if (idx === 0 && isShow && name === 'label.address.mailing') {
                        doc.text(intl.formatMessage({id: name}), indent, currentLine);
                        if (value.mailingAddressSameAsHomeAddressIndicator) {
                            doc.text(intl.formatMessage({id: 'label.address.same.home'}), secondColumnPlus, currentLine);
                            currentLine += mediumInterval;
                        } else {
                            const {streetAddress1, streetAddress2, city, state, postalCode} = value.mailingAddress;
                            doc.text(streetAddress1, secondColumnPlus, currentLine);
                            currentLine += mediumInterval;
                            if (streetAddress2 && streetAddress2.length > 0) {
                                doc.text('', indent, currentLine);
                                doc.text(streetAddress2, secondColumnPlus, currentLine);
                                currentLine += mediumInterval;
                            }
                            doc.text('', indent, currentLine);
                            doc.text(`${city} ${state}, ${postalCode}`, secondColumnPlus, currentLine);
                        }
                        currentLine += mediumInterval;
                    } else {
                        if (isShow && name !== 'label.address.mailing') {
                            doc.setFontSize(textFontSize);
                            let label = intl.formatMessage({id: name}, {fiveYearsAgo});
                            if (label.length > LONG_LABEL_THRESHOLD) {
                                label = doc.splitTextToSize(label, secondColumn);
                                doc.text(label, indent, currentLine);
                                doc.text(value, secondColumnPlus, currentLine);
                                currentLine += (mediumInterval*2);
                            } else {
                                let indentFirstCol = indent;
                                if (name === 'label.state' || name === 'label.tribe.name') {
                                    indentFirstCol = indent + ADDL_INDENT;
                                }
                                doc.text(label, indentFirstCol, currentLine);
                                doc.text(value, secondColumnPlus, currentLine);
                                currentLine += mediumInterval;
                            }
                        }
                    }
                }
            });
            currentLine += bigInterval;
        });

        // More about household section

        if (isMN) {
            doc.setFontSize(headerFontSize);
            doc.setTextColor(...headerColor);
            doc.text(intl.formatMessage({id: 'finalReview.moreAboutHousehold'}), indent, currentLine += bigInterval);
            doc.setTextColor(...regularColor);
            doc.setFontSize(textFontSize);
            doc.text(intl.formatMessage({id: 'finalReview.membersFederally'}), indent, currentLine += bigInterval);
            currentLine += bigInterval;
            householdMembers.forEach((hhm) => {
                ({
                    currentLine,
                    page
                } = addNewPage(doc, currentLine, currentLineInitialValue, page, textFontSize, pageNumberPosition, bigInterval));
                const {
                    name,
                    americanIndianAlaskaNative: {
                        memberOfFederallyRecognizedTribeIndicator
                    }
                } = hhm;

                doc.setFontSize(textFontSize);
                doc.text(normalizeNameOfPerson(name), indent, currentLine);
                doc.text(memberOfFederallyRecognizedTribeIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'}), secondColumnPlus, currentLine);
                currentLine += mediumInterval;
            });

            currentLine += bigInterval;
        }

        // Income Information
        if (flowType === FLOW_TYPES.FINANCIAL_FLOW) {
            doc.setFontSize(headerFontSize);
            doc.setTextColor(...headerColor);
            doc.text(intl.formatMessage({id: 'label.income.info'}), indent, currentLine += bigInterval);
            doc.setTextColor(...regularColor);
            currentLine += bigInterval;
            householdMembers.map((hhm)=> {
                ({
                    currentLine,
                    page
                } = addNewPage(doc, currentLine, currentLineInitialValue, page, textFontSize, pageNumberPosition, bigInterval));
                const {
                    name,
                    annualTaxIncome: {
                        projectedIncome,
                        reportedIncome
                    },
                } = hhm;

                doc.setFontSize(textFontSize);
                doc.setTextColor(...headerColor);
                doc.text(normalizeNameOfPerson(name), indent, currentLine);
                doc.setTextColor(...regularColor);
                currentLine += mediumInterval;

                doc.text(`Yearly income in ${coverageYear}`, indent, currentLine);
                doc.text(helperFunctions.getIncomeValueForRender(projectedIncome, reportedIncome), secondColumnPlus, currentLine);
                currentLine += mediumInterval;
            });
        }

        // Additional Information
        if (flowType === FLOW_TYPES.FINANCIAL_FLOW) {
            doc.setFontSize(headerFontSize);
            doc.setTextColor(...headerColor);
            doc.text(intl.formatMessage({id: 'label.additional.info'}), indent, currentLine += bigInterval);
            doc.setTextColor(...regularColor);
            currentLine += bigInterval;
            ({
                currentLine,
                page
            } = addNewPage(doc, currentLine, currentLineInitialValue, page, textFontSize, pageNumberPosition, bigInterval));

            const {
                reconciledAptc,
                primaryTaxFilerPersonId
            } = taxHousehold;

            let additionalInfoSummaries = [];
            const checkHouseholdMember = (i) => {
                if (adjustedFlow === undefined) {
                    return true;
                } else {
                    const pagesListToCheck = [
                        PAGES.OTHER_HEALTH_COVERAGE_PAGE,
                        PAGES.RECONCILIATION_IN_APTC_PAGE,
                        PAGES.EMPLOYER_COVERAGE_DETAIL_PAGE,
                        PAGES.STATE_EMPLOYEE_HEALTH_BENEFIT_PAGE,
                        PAGES.ADDITIONAL_INFORMATION_PAGE
                    ];
                    return !pagesListToCheck.every(pageName => !adjustedFlow.find(pg => pg.pageName === pageName && pg.hhm === i).isActive);
                }
            };

            householdMembers.filter((hhm, indx) => checkHouseholdMember(indx)).map((member) => {
                ({
                    currentLine,
                    page
                } = addNewPage(doc, currentLine, currentLineInitialValue, page, textFontSize, pageNumberPosition, bigInterval));
                const {
                    name,
                    personId,
                    householdContact: {
                        homeAddress: {
                            state
                        }
                    },
                    healthCoverage: {
                        currentlyHasHealthInsuranceIndicator,
                        employerWillOfferInsuranceIndicator,
                        currentOtherInsurance: {
                            otherStateOrFederalPrograms
                        },
                        otherInsurance,
                        stateHealthBenefit
                    },
                    employerSponsoredCoverage
                } = member;

                const [ stateAbbr ] = state ? getOptionsForHardcodedState(state) : [{}];

                const { insuranceName, policyNumber, limitedBenefit } = otherInsurance;
                const labelStateEmployerBenefit = intl.formatMessage({id:'ssap.additionalInformation.stateEmployerBenefit'}, {stateName: stateAbbr.text});
                let config = {
                    [`${normalizeNameOfPerson(name)}`]: {isShow: true, value: ''},
                    'Do you currently have health coverage?': {isShow: true, value: currentlyHasHealthInsuranceIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                    'Coverage Type': {isShow: currentlyHasHealthInsuranceIndicator, isAddlNameIndent: true, value: getCoverageType(otherStateOrFederalPrograms)},
                    'Insurance Name': {isShow: currentlyHasHealthInsuranceIndicator && insuranceName, isAddlNameIndent: true, value: insuranceName},
                    'Policy Number': {isShow: currentlyHasHealthInsuranceIndicator && policyNumber, isAddlNameIndent: true, value: policyNumber},
                    'Is this a limited benefit coverage?': {isShow: currentlyHasHealthInsuranceIndicator && limitedBenefit, isAddlNameIndent: true, value: limitedBenefit ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                    'Did you reconcile premium tax credits on your tax return for any past years?': {isShow: primaryTaxFilerPersonId === personId, isLongName: true, value: reconciledAptc ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})},
                    'Are you offered health coverage through a job?': {isShow: true, value: employerWillOfferInsuranceIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})}
                };

                employerSponsoredCoverage.map((coverageDetails) => {
                    const { companyName, phone, minimumValuePlan, employerPremium, employerPremiumFrequency } = coverageDetails;
                    config['Employer Name'] = {isShow: employerWillOfferInsuranceIndicator, isAddlNameIndent: true, value: companyName};
                    config['Contact Number'] = {isShow: employerWillOfferInsuranceIndicator, isAddlNameIndent: true, value: phone};
                    config['Health plan meets the minimum value standard?'] = {isShow: employerWillOfferInsuranceIndicator, isAddlNameIndent: true, value: minimumValuePlan ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})};
                    config['Premium amount for the lowest-cost plan'] = {isShow: employerWillOfferInsuranceIndicator && minimumValuePlan && employerPremium > 0 && employerPremiumFrequency !== 'UNSPECIFIED', isAddlNameIndent: true, value: `$${employerPremium / 100} / ${employerPremiumFrequency}`};
                });

                config[labelStateEmployerBenefit] = {isShow: true, isLongName: true, value: stateHealthBenefit ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})};
                
                additionalInfoSummaries.push(config);
            });

            additionalInfoSummaries.forEach((summary) => {
                Object.entries(summary).forEach(([name, {isShow, isAddlNameIndent, isLongName, value}], entryIdx) => {
                    ({
                        currentLine,
                        page
                    } = addNewPage(doc, currentLine, currentLineInitialValue, page, textFontSize, pageNumberPosition, bigInterval));

                    if(entryIdx === 0) {
                        doc.setTextColor(...headerColor);
                        doc.setFontSize(headerFontSize);
                        doc.text(intl.formatMessage({id: name}), indent, currentLine);
                        doc.text(value, secondColumnPlus, currentLine);
                        doc.setTextColor(...regularColor);
                        currentLine += mediumInterval;
                    } else {
                        if (isShow) {
                            const nameIndent = isAddlNameIndent ? indent + ADDL_INDENT : indent;
                            doc.setFontSize(textFontSize);
                            let label = isLongName ? name : intl.formatMessage({id: name});
                            if (isLongName) {
                                label = doc.splitTextToSize(label, secondColumn);
                            }
                            doc.text(label, nameIndent, currentLine);
                            doc.text(value, secondColumnPlus, currentLine);
                            currentLine += (isLongName ? (mediumInterval*2) : mediumInterval);
                        }
                    }
                });
            });
        }

        let tabForPrinting;

        switch(action) {
        case 'download':
            doc.save(`${normalizeNameOfPerson(name)} Summary.pdf`);
            break;
        case 'print':
            tabForPrinting = window.open(doc.output('bloburl'), '_blank');
            setTimeout(() => {
                tabForPrinting.print();
            }, 200);
            break;
        default:
            tabForPrinting = window.open(doc.output('bloburl'), '_blank');
            setTimeout(() => {
                tabForPrinting.print();
            }, 200);
        }
    };

    /**
     * Here we declare and assign handlePrint(), which invokes built-in print dialog
     */
    const handlePrint = () => {
        handleAction('print');
    };


    /**
     * Here we declare and assign handleDownload(), which saves pdf-file on User's machine
     */
    const handleDownload = () => {
        handleAction('download');
    };

    useEffect(() => {
        props.actions.setMethods('Page3_16',
            {
                handlePrint,
                handleDownload
            });

        return () => {
            props.actions.clearMethods('Page3_16');
        };
    }, [ loadedRelationsWholeList ]);

    return (
        <Fragment>
            <div id="rsReview" className="fade-in">
                <div className="subsection">
                    <PageHeader
                        textCode="finalReview.householdAndDemoInformation"
                        headerClassName={'usa-heading-alt summary__header--no-border usa-width-three-fourths'}
                        additionalElement={
                            <Fragment>
                                <ButtonContainer type="downloadButton" onClick= { () => handleDownload() }/>
                                <ButtonContainer type="printButton" onClick= { () => handlePrint() }/>
                            </Fragment>
                        }
                        additionalElementClassName={'header-btn'}
                    />
                    <div className="usa-width-one-whole usa-grid bordered">
                        <div className="usa-width-one-whole">
                            <h4>
                                {normalizeNameOfPerson(name)}
                            </h4>
                        </div>
                        <div className="usa-width-one-whole divider"/>
                        <SummaryItem
                            textCode="label.email"
                            value={emailAddress}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        <SummaryItem
                            textCode="label.phone"
                            value={phoneNumberTools.getAdjustedPhoneNumber(phoneNumber)}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        <SummaryItem
                            textCode="label.dob"
                            value={moment(dateOfBirth).format('MM/DD/YYYY')}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        <AddressItem
                            textCode="label.address.home"
                            {...homeAddress}
                        />
                        <AddressItem
                            textCode="label.address.mailing"
                            {...mailingAddress}
                        />
                        <SummaryItem
                            textCode="label.phone.second"
                            value={phoneNumberTools.getAdjustedPhoneNumber(_phoneNumber)}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />

                        {
                            !isMN &&
                            <>
                                <SummaryItem
                                    textCode="label.language.preferred.spoken"
                                    value={preferredSpokenLanguage}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                                <SummaryItem
                                    textCode="label.language.preferred.written"
                                    value={preferredWrittenLanguage}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                            </>
                        }

                        <SummaryItem
                            textCode="label.communication.preferred"
                            value={COMMUNICATION_METHODS_LIST[preferredContactMethod]}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        <SummaryItem
                            textCode={authRepTxt}
                            value={normalizeNameOfPerson(authorizedRepresentativeName)}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />

                        {
                            !isMN &&
                            <SummaryItem
                                textCode="finalReview.repPhoneNumber"
                                value={getFullNumberAsString(authRepPhoneNumber)}
                                classes2={{
                                    spanClass: 'className1 className001',
                                    divClass1: 'summary__label',
                                    divClass2: 'summary__value'
                                }}
                            />
                        }

                    </div>

                    <PageHeader
                        textCode="finalReview.applyingForHealthCoverage"
                        headerClassName={'usa-heading-alt margin-tb-10 '}
                    />
                    <ApplyingForHealthCoverageSummary bloodRelationships={bloodRelationships}/>

                    {isNV &&
                        <div className="usa-width-one-whole">
                            <PageHeader
                                textCode="finalReview.householdMembers"
                                headerClassName={'usa-heading-alt'}
                            />
                            <FamilyAndHouseholdSummary_NV
                                {...{
                                    buttonContainer: ButtonContainer,
                                    householdMembers,
                                    flowType,
                                    stateCode,
                                    PAGES,
                                    tribes
                                }}
                            />
                        </div>
                    }
                    {isMN &&

                    <FamilyAndHouseholdSummary_MN
                        {...{
                            buttonContainer: ButtonContainer,
                            householdMembers,
                            isShowUSCitizen,
                            flowType,
                            stateCode,
                            PAGES
                        }}
                    />
                    }

                    { flowType === FLOW_TYPES.FINANCIAL_FLOW &&
                        <Fragment>
                            <PageHeader
                                textCode="label.income.info"
                                headerClassName={'usa-heading-alt'}
                            />
                            <div className="usa-width-one-whole">
                                <IncomeInformationSummary
                                    {...{
                                        buttonContainer: ButtonContainer,
                                        householdMembers,
                                        coverageYear,
                                        PAGES
                                    }}
                                />
                            </div>
                            <PageHeader
                                textCode="label.additional.info"
                                headerClassName={'usa-heading-alt'}
                            />
                            <div className="usa-width-one-whole">
                                <AdditionalInformationSummary
                                    {...{
                                        buttonContainer: ButtonContainer,
                                        householdMembers,
                                        taxHousehold,
                                        isShowUSCitizen,
                                        PAGES,
                                        adjustedFlow
                                    }}
                                />
                            </div>
                        </Fragment>
                    }

                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => {

    const sortedHouseholds = state.data.singleStreamlinedApplication.taxHousehold[0].householdMember.sort((a, b) => a.personId - b.personId);
    const {stateCode} = state.headerLinks;

    return {
        data: state.data,
        flow: state.flow,
        adjustedFlow: state.adjustedFlow,
        taxHousehold: state.data.singleStreamlinedApplication.taxHousehold[0],
        householdMembers: sortedHouseholds,
        headOfHousehold: sortedHouseholds && sortedHouseholds.length > 0 ? sortedHouseholds[0] : null,
        messages: state.messages,
        relations: state.relations,
        stateCode,
        tribes: state.tribes
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        clearMethods,
        fetchBloodRelationships,
        fetchTribes,
        setMethods
    }, dispatch)
});

Page3_16.defaultProps = {
    name: 'Page3_16'
};

const intlComponent = injectIntl(Page3_16);

export default connect(mapStateToProps, mapDispatchToProps)(intlComponent);
