package com.getinsured.hix.planmanagement.test;

import static com.getinsured.hix.planmgmt.service.IssuerService.CERTI_FOLDER;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.IssuerQualityRating;
import com.getinsured.hix.planmgmt.repository.IIssuerQualityRatingRepository;
import com.getinsured.hix.planmgmt.service.IssuerQualityRatingService;

public class IssuerQualityRatingServiceTest extends BaseTest {
	@SuppressWarnings("unused")
	@Autowired private IssuerQualityRatingService issuerQualityRatingService;
	@Autowired
	IIssuerQualityRatingRepository issuerQualityRatingRepository;
	//HIX-5669
	private static final Logger LOGGER = LoggerFactory
			.getLogger(IssuerQualityRatingServiceTest.class);

	@Value("#{configProp.uploadPath}")
	private String uploadPath;

	//HIX 5669
	/*@Test
	public void uploadRatingsFile(){
		String ratingsFileName = "qualityRatingsTest.csv";
		String absRatingsFileName = uploadPath + File.separator+ratingsFileName;
		InputStream inputStream = null;
		
		try {	
			    byte[] fileContent = null;
			    System.out.println("Absolute File name is .. "+absRatingsFileName);
				File fileToProcess = new File(absRatingsFileName);
				//Create IssuerRatings object and put in file for saving into DB as BLOB
				IssuerQualityRating issuerRating = new IssuerQualityRating();
				List <IssuerQualityRating> qrList = issuerQualityRatingRepository.findAll();
				issuerRating = qrList.get(0);
                inputStream = new FileInputStream(absRatingsFileName);
				fileContent = IOUtils.toByteArray(inputStream);
				String strOfCSV = new String(fileContent);
				inputStream.close();
				if (fileContent.length > 0 && issuerRating!=null) {
					issuerRating.setQualityRatingDetails(fileContent);
					Date today = new TSDate();
					issuerRating.setEffectiveDate(today);
					issuerQualityRatingService.saveQualityRatingsFile(issuerRating);
					System.out.println("PERSISTED CSV file as BLOB inside table");
					LOGGER.info("Finished Setting quality rating inside rating object");
				}

				//Retrieve and verify
				IssuerQualityRating issuerRating_2 = new IssuerQualityRating();
				List <IssuerQualityRating> qrList_2 = issuerQualityRatingRepository.findAll();
				issuerRating = qrList_2.get(0);
				//Step 1 - Retrieve the BLOB
				byte[] csvFileBytes = issuerRating.getQualityRatingDetails();
				
		        //Step 2 - Convert the BLOB to string that has the CSV lines
				strOfCSV = new String(csvFileBytes);
				System.out.println("CSV Blob retrieved..."+strOfCSV);
				//Step 3 -Convert CSV to XML
				
				Assert.assertEquals(1, 1);
		} catch (Exception e) {
			LOGGER.error("Error while loading file into DB : " + e.getMessage());
			e.printStackTrace();
			fail("No Exception expected here");
		}

	}*/
	
	//HIX 5669
	@Test
	public void getIssuerQualityRatingByIdTest(){

		Map<String, String> ratingFieldsMap = issuerQualityRatingService.getIssuerQualityRatingByIssuerId(5);
		assertNotNull(ratingFieldsMap);
		ratingFieldsMap = issuerQualityRatingService.getIssuerQualityRatingByIssuerId(8);
		assertNotNull(ratingFieldsMap);		
	}
	

}
