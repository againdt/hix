import { environment } from './../../environments/environment';
import { PersonalizedState } from './../models/data.model';
import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { DrugInfo, DosageInfo, ProviderFullData, RxcuiInfo, HouseHold } from "../models/data.model";

@Injectable()
export class FetchService {
    //private readonly baseUrl: string = "/hix"

    private baseUrl: string = "https://wa1dev.ghixqa.com/hix"
    // private baseUrl: string = "https://id1dev.ghixqa.com"

    constructor(
        private http: Http,
    ) {
        // for production environment
        if (environment.production) {
            this.baseUrl = "/hix";
        } else {
            this.baseUrl = "https://wa1dev.ghixqa.com/hix";
            //this.baseUrl = "http://localhost:8080/hix";
        }
    }

    public searchDrug(query: string): Observable<{ drugs: DrugInfo[] }> {
        return this.http.get(`${this.baseUrl}/private/searchDrugByName?term=${query}`).map(response => response.json());
    }

    public searchDrugById(id: string): Observable<{ drugDosages: DosageInfo[] }> {
        return this.http.get(`${this.baseUrl}/private/searchByDrugId?drugId=${id}`).map(rsp => rsp.json());
    }

    public genericRxcui(name: string, strength: string): Observable< any > {
        strength = encodeURI(strength);
        return this.http.get(`${this.baseUrl}/private/genericRxcui?name=${name}&strength=${strength}`);
    }

    public fairPrice(ndc: string): Observable<number> {
        return this.http.get(`${this.baseUrl}/private/getFairPriceOfDrug?ndc=${ndc}`).map(rsp => rsp.json());
    }
    public rxcui(ndc: string): Observable<number> {
        return this.http.get(`${this.baseUrl}/private/getRxcuiByNdc/${ndc}`).map(rsp => rsp.json());
    }
    public doctorSearch(query: string, zipcode: string, searchType: "doctor" | "facility"): Observable<{
        current_start: number,
        next_start: number,
        page_size: number,
        providers: ProviderFullData[],
        status: "SUCCESS" | "FAILURE",
        total_hits: string,
        error: string,
    }> {
        const dis = 25;
        let type = "";
        if (searchType === "facility") {
             type = "hospital";
         }
         if (searchType === "doctor") {
            type = "doctor";
        }
        return this.http.get(`${this.baseUrl}/provider/doctors/search?searchKey=${query}&currentPage=1&userZip=${zipcode}&pageSize=10&searchType=${type}&otherDistance=${dis}`)
            .map(rsp => rsp.json());
    }


    public savePreference(
        shoppingId: string,
        personalizedState: PersonalizedState,
        csrftoken: string,
    ): Observable<Response> {
        const headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
            'csrftoken': csrftoken
        });

        const options = new RequestOptions({ headers: headers });
        const urlSearchParams = new URLSearchParams();

        urlSearchParams.append("providersJson", (() => { return personalizedState.doctor.doctorSearch.length ? JSON.stringify(personalizedState.doctor.doctorSearch) : null })());
        urlSearchParams.append("doctorvisit", personalizedState.doctor.visitFrequency);
        urlSearchParams.append("prescription", personalizedState.drug.monthlyPrescription);
        urlSearchParams.append("prescriptionSearchRequest", (() => { return personalizedState.drug.drugSearch.length ? JSON.stringify(personalizedState.drug.drugSearch) : null })());

        return this.http.post(`${this.baseUrl}/private/savePreferencesForShoppingId/${shoppingId}`, urlSearchParams.toString(), options);
    }


    public getZipcode(shoppingId: string): Observable<any> {
        return this.http.get(`${this.baseUrl}/private/getHouseholdDataByShoppingId/${shoppingId}?random=${Math.floor(Math.random() * 10 ** 7)}`).map(rsp => rsp.json());
    }

    public validateZip(
        csrftoken: string,
        zipcode: string
    ): Observable<Response> {
        const headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
            'csrftoken': csrftoken
        });
        const options = new RequestOptions({ headers: headers });
        const urlSearchParams = new URLSearchParams();

        urlSearchParams.append("zip", zipcode);

        return this.http.post(`${this.baseUrl}/private/validateZipAndPullCounties`, urlSearchParams.toString(), options);
    }

}
