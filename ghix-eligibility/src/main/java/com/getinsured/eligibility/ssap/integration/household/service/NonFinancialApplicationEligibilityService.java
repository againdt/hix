package com.getinsured.eligibility.ssap.integration.household.service;

import java.util.List;

import com.getinsured.eligibility.common.dto.SsapApplicationUpdateDTO;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface NonFinancialApplicationEligibilityService {
    
    SsapApplicationUpdateDTO determineEligibility(SsapApplication ssapApplication) throws Exception;
    boolean isVerificationDone(List<SsapApplicant> ssapApplicants, SingleStreamlinedApplication ssapJson);
    void sendInitialEligibilityNotification(String caseNumber);
    void sendUpdatedEligibilityNotification(String caseNumber);
    void sendAdditionalInfoEligibilityNotification(String caseNumber) throws Exception;
    boolean checkIfEligibilityIsToBeDenied(String caseNumber) throws Exception;
    String updateEligibilityStatus(SsapApplication ssapApplication, SingleStreamlinedApplication ssapJSON);
}
