<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>


<script>
	var employerIndvContribution = 0; 
	var employerDepeContribution = 0;
	var minDentalPremium = 0;
	var maxDentalPremium = 0;
</script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript">
function openFindBrokerModal(){
	$('#findbroker').modal('show');
}
 $(function() {
	 
	 $('#findbroker').click(function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			if (href.indexOf('#') != 0)
				openFindBrokerDialog(href);
		});
	 
     $('#addempdiv :a').attr('disabled', 'disabled'); 
});
 
 $('.modalClose').live('click',function(){
		$('#modal, .modal-backdrop').remove();
		window.location.reload();
	});

$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('#modal').remove();
	}
});

function submitForm(){
	$('#setupOptionsForm').submit();
}
</script>


<div class="gutter10">  	
	<div class="row-fluid margin30-t">
	    <div class="span3" id="sidebar">
	    	<div class="header">
	      		<h4>Coverage Setup</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="#">1 Getting Started</a></li>
	      		<li class="active"><a href="#">2 Health Coverage Setup</a></li>
	      		<c:if test="${isDentalAvailable=='YES' }"><li><a href="#">3 Dental Coverage Setup</a></li></c:if>
	      		<li><a href="#">4 Review and Confirm</a></li>
	      	</ul>

			<div class="header margin30-t">
	      		<h4>Quick Links</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="<c:url value='/broker/search' />" id="findbroker"><i class="icon-search"></i> Find an Agent</a></li>
	      	</ul>
	    </div>
    
	    <div class="span9" id="rightpanel">
	    	<div class="header">
	    		<h4 class="pull-left">Health Coverage Setup</h4>
	    		<a href="#" id="nextbutton" class="btn btn-small  btn-primary pull-right" onclick="submitForm();">Next</a>
	    		<a href="<c:url value="/shop/employer/planselection/gettingstarted" />" class="btn btn-small pull-right healthCoverageSetupActionBackReturn">Back</a>
	    	</div>
			
	    	<div class="gutter10">
		    	<form class="form-horizontal" name="setupOptionsForm" id="setupOptionsForm" method="post" action="<c:url value='/shop/employer/planselection/healthcoveragesetup'/>" >
					<df:csrfToken/>
<!-- NEW SETUP -->					
				  	<div class="gutter10" id="newSetup">
					  	<p>In setting up health insurance for your company, what is the most important objective you are trying to accomplish:</p>
					  	<div class="control-group">
								<label class="checkbox"> 
									<input type="radio" value="1" class="terms" name="setupOptions"> <span>Offer the most comprehensive coverage to my employees.</span> 
								</label>
								
								<label class="checkbox"> 
									<input type="radio" value="2" class="terms" name="setupOptions"> <span>Minimize my employer costs.</span> 
								</label>
								
								<label class="checkbox"> 
									<input type="radio" value="3" class="terms" name="setupOptions"> <span>Not sure</span> 
								</label>	
								<span id="setupOptions_error"></span>
						</div>
					</div>
<!-- NEW SETUP ENDS-->

					<div class="form-actions">
						<a href="#" class="btn btn-primary btn-large margin75-l" onclick="submitForm();">Next</a>
					</div>
				</form>
	    	</div><!-- .gutter10 -->
	    	
		</div><!-- .span9 #rightpanel -->
	</div><!-- .row-fluid -->
</div><!-- .gutter10 -->

<script type="text/javascript">
var validator2 = $('#setupOptionsForm').validate({ 
	rules : {
		setupOptions: {required: true}
	},
	messages : {
		setupOptions: {required:  "<span> <em class='excl'>!</em> Please Select atleast one option</span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('name');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});
</script>