import React from 'react';
import {injectIntl} from 'react-intl';
import SummaryItem from './SummaryItem';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import normalizeAddress from '../../utils/normalizeAddress';
import helperFunctions from '../../utils/helperFunctions';
import SubHeader from '../headers/SubHeader';
import PageHeader from '../headers/PageHeader';

// Components

// Utils


const FamilyAndHouseholdSummary_MN = props => {

    const {
        householdMembers,
        intl,
        buttonContainer: ButtonContainer,
        PAGES
    } = props;

    const moreAboutHousehold = () => householdMembers.map((member, i) => {
        const {
            name,
            americanIndianAlaskaNative : {
                memberOfFederallyRecognizedTribeIndicator
            }
        } = member;

        //const age = helperFunctions.calculateAge(dateOfBirth);
        return (
            <div key={`${member.name.firstName}_${member.name.lastName}_more_${i}`}>
                <SummaryItem
                    category={normalizeNameOfPerson(name)}
                    value= {memberOfFederallyRecognizedTribeIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
            </div>
        );
    });

    const getGender = (gender) => {
        return `${gender && gender.slice(0, 1).toUpperCase()}${gender && gender.slice(1)}`;
    };

    const householdMembersContent = () => householdMembers.map((member, i) => {
        const {
            applyingForCoverageIndicator,
            name,
            gender,
            socialSecurityCard: {
                socialSecurityNumber,
                reasonableExplanationForNoSSN
            },
            householdContact: {
                homeAddress,
                mailingAddress,
                mailingAddressSameAsHomeAddressIndicator
            }
        } = member;

        //const age = helperFunctions.calculateAge(dateOfBirth);
        const maleFemale = getGender(gender) === 'Female' ? intl.formatMessage({id: 'label.female'}) : intl.formatMessage({id: 'label.male'});
        return (
            <div key={`${member.name.firstName}_${member.name.lastName}_hh_${i}`}>
                <div className="usa-grid usa-width-one-whole summary__header">
                    <SubHeader
                        text={
                            <span>
                                {normalizeNameOfPerson(name)}
                                {i === 0 ? '(' + intl.formatMessage({id: 'label.contact.primary'}) + ')' : ''}
                            </span>
                        }
                        headerClassName={'usa-heading-alt'}
                        additionalElement={(
                            <ButtonContainer
                                type="editButton_secondary"
                                args={[i, PAGES.PERSONAL_INFORMATION_PAGE]}
                            />
                        )}
                        additionalElementClassName={'header-btn'}
                    />
                </div>
                <SummaryItem
                    textCode="label.applyingForCoverage"
                    value={applyingForCoverageIndicator ? intl.formatMessage({id: 'label.yes'}) : intl.formatMessage({id: 'label.no'})}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                <SummaryItem
                    textCode="label.gender"
                    value={maleFemale}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                <SummaryItem
                    textCode="label.ssn"
                    value={helperFunctions.getValueForSSN(socialSecurityNumber, reasonableExplanationForNoSSN)}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                <SummaryItem
                    textCode="label.address.home"
                    value={normalizeAddress(homeAddress)}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                {i === 0 && !mailingAddressSameAsHomeAddressIndicator &&
                    < SummaryItem
                        textCode="label.address.mailing"
                        value={normalizeAddress(mailingAddress)}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />
                }
                {i === 0 && mailingAddressSameAsHomeAddressIndicator &&
                < SummaryItem
                    textCode="label.address.mailing"
                    value={intl.formatMessage({id: 'label.address.same.home'})}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                }
            </div>
        );
    });

    return (
        <>
            <PageHeader
                textCode="finalReview.householdMembers"
                headerClassName={'usa-heading-alt'}
            />
            <div className="usa-width-one-whole usa-grid bordered">
                {householdMembersContent()}
            </div>
            
            <PageHeader
                textCode="finalReview.moreAboutHousehold"
                headerClassName={'usa-heading-alt'}
            />
            
            <div className="usa-width-one-whole usa-grid bordered">
                <SubHeader
                    textCode='finalReview.membersFederally'
                    headerClassName={'usa-heading-alt'}
                />
                {moreAboutHousehold()}
            </div>
        </>
    );
};

export default injectIntl(FamilyAndHouseholdSummary_MN);
