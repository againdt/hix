(function(){
	'use strict';
	
	angular.module("resetPasswordApp").directive('dateValidation', dateValidation);
	angular.module("resetPasswordApp").directive('ngFocus', ngFocus);
	angular.module("resetPasswordApp").directive('updateSsnView', updateSsnView);
	angular.module("resetPasswordApp").directive('spinningLoader', spinningLoader);
	
	function dateValidation(){
		var directive = {
			restrict : 'AE',
			require : 'ngModel',
			link : function(scope, element, attrs, ngModel) {		
				
				var dateType = attrs.dateValidation;
				
				function isDateValid(value){
					//remove placeholder
					var date = value.replace(/[A-Z]/g, ''); //var date = value.replace(/[_\/]/g, '');
					
					//remove extra-last-character
					if(date.length === 11){
						date = date.substring(0, date.length - 1);
					}
					
					
					if(date.length === 10){
						if(!moment(date,'MM/DD/YYYY').isValid()){
							return false;
						}
						
						if(dateType === 'birthDate'){
							if(ageCheck('between',date, 0, 104)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'future'){
							if(ageCheck('greater',date, 0)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'under21'){
							if(ageCheck('between',date, 21, 104)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'qe'){
							var inputDate = date.split('/');
							var start = new Date(inputDate[2], inputDate[0]-1, inputDate[1]);

							var today = new Date();
							var end = new Date(today.getFullYear(), today.getMonth(), today.getDate());
							
							var diff = (start.getTime() - end.getTime()) / (24*60*60*1000);
							if(diff <= 0 && diff >= -60){
								return true;
							}else{
								return false;
							};
						}
					}else{
						return true;
					}
					
				}

				function ageCheck(comparator, birth, age, maxAge){
					var birthDate = new Date(birth);
					var today = new Date();
					
					 var years = (today.getFullYear() - birthDate.getFullYear());

				    if (today.getMonth() < birthDate.getMonth() || today.getMonth() == birthDate.getMonth() && today.getDate() < birthDate.getDate()) {
				        years--;
				    }
				    
				    if(comparator === 'older'){
				    	if(years >= parseInt(age)){
					    	return true;
					    }else{
					    	return false;
					    }
					}else if(comparator === 'between'){
						if(years >= parseInt(age) && years <= parseInt(maxAge)){
					    	return true;
					    }else{
					    	return false;
					    }
					}else{
						if(years < parseInt(age)){
					    	return true;
					    }else{
					    	return false;
					    }
					}
					
				};

				ngModel.$parsers.unshift(function(value){
					var dateValidationResult = isDateValid(value);
					
					ngModel.$setValidity(dateType, dateValidationResult);
					
					return value;	
				});
				
				 ngModel.$formatters.unshift(function(value) {
					 if(value === undefined || value === null){
						 return;
					 }
					 var dateValidationResult = isDateValid(value);
						
					 ngModel.$setValidity(dateType, dateValidationResult);
					 
					 return value;
	             });
				
				
			}	
		}
		
		return directive;
	}
	
	
	
	function ngFocus(){
		var directive = {
			restrict: 'A',
			require: 'ngModel',
			link: function(scope, element, attrs, ctrl) {
				ctrl.$focused = false;
				element.bind('focus', function(evt) {
					element.addClass('ng-focused');
					scope.$apply(function() {
						ctrl.$focused = true;
					});
				}).bind('blur', function(evt) {
					element.removeClass('ng-focused');
					scope.$apply(function() {
						ctrl.$focused = false;
					});
				});
			}	
		}
		
		return directive;
	}
	
	
	function updateSsnView(){
		var directive = {
			restrict : 'AE',
			require : 'ngModel',
			link : function(scope, element, attrs, ngModel) {
				
				var validValue = null;
				
				//format text going to user (model to view)
				ngModel.$formatters.push(function(value) {
					if(value !== undefined){
						return '***-**-' + value.slice(5);
					}
					return value;
				});

				//format text from the user (view to model)
				ngModel.$parsers.push(function(value) {
					//don't update model if view is masked
					if(value !== undefined && value.indexOf('*') !== -1){
						return validValue;
					}else{
						validValue = value;
					    return value;
					}
					
				  });
			}	
		}
		
		return directive;
	}
	
	
	spinningLoader.$inject = ['$timeout'];
 	function spinningLoader($timeout){
 		var directive = {
			restrict: "EA", 
			template: '<div id="loaderDiv" ng-show="loadSpinner"><img alt="loading" src="/hix/resources/images/loader_gray.gif" class="ajax-loader"/></div>',
		    link: function (scope, elem, attrs, ctrl) {
	          var starting;
	          scope.$watch(attrs.spinningLoader, function(nV, oV){
	            if (nV === true){
	              starting = new Date();
	              scope.loadSpinner = true;                   
	            } else {
	              var ending = new Date();
	              var timeDiff = ending - starting;
	              if (timeDiff >= 1000){
	                scope.loadSpinner = false;
	              } else {
	                $timeout(function(){
	                  scope.loadSpinner = false;
	                }, (1000 - timeDiff));
	              };           
	            }; 
	         });
	      }	
 		};
 		
 		return directive;
 	}
	
})();

