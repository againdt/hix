/**
 * 
 */

package com.getinsured.hix.batch.enrollment.email.service;

import org.springframework.batch.core.scope.context.ChunkContext;

import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

/**
 * 
 * @author raja
 * @since 07/09/2013
 */
public interface EnrollmentBatchFailureEmailNotificationService {
	
	void sendEnrollmentBatchFailureEMailNotification(EnrollmentBatchEmailDTO enrollmentBatchEmailDTO, ChunkContext chunkContext) throws NotificationTypeNotFound;
}
