package com.getinsured.eligibility.at.resp.si.handler.helper;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component
public class QHPApplicationEligibilityHelper {

	private static final Logger LOGGER = Logger.getLogger(QHPApplicationEligibilityHelper.class);

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	//@Autowired private IEligibilityProgram iEligibilityProgram;


	public void processEligibility(ERPResponse erpResponse) {


		//1. Fetch all incoming applicants from AT Response
		List<TaxHouseholdMember> taxHHList =  erpResponse.getTaxHouseholdMemberList();

		//2. Fetch all existing applicants from SSAP_APPLICANT table
		List<TaxHouseholdMember> ssapHHList =  erpResponse.getSsapApplicantList();

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumberOne(erpResponse.getApplicationID());
		SsapApplication ssapApplication = null;
		if (ssapApplicationList != null && ssapApplicationList.size() > 0){
			ssapApplication = ssapApplicationList.get(0);
		} else {
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + erpResponse.getApplicationID());
		}

		EligibilityStatus currentEligibilityStatus = ssapApplication.getEligibilityStatus();

		if (!erpResponse.isFullAssessment()){
			handleFullDetermination(erpResponse, taxHHList, ssapHHList,
					ssapApplication, currentEligibilityStatus);
		} else {
			handleAssessment(erpResponse, taxHHList, ssapHHList,
					ssapApplication, currentEligibilityStatus);
		}
	}


	private void handleAssessment(ERPResponse erpResponse,
			List<TaxHouseholdMember> taxHHList,
			List<TaxHouseholdMember> ssapHHList,
			SsapApplication ssapApplication,
			EligibilityStatus currentEligibilityStatus) {


		boolean isHHQHPEligible = processQHPForAllApplicants(erpResponse, false, taxHHList, ssapHHList);

		if ((currentEligibilityStatus == EligibilityStatus.CAM || currentEligibilityStatus == EligibilityStatus.DE) && isHHQHPEligible){
			if (currentEligibilityStatus == EligibilityStatus.DE){
				ssapApplication.setEligibilityStatus(EligibilityStatus.CAE);
			}
			ssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.QHP);
			ssapApplicationRepository.save(ssapApplication);
		}

	}


	private boolean processQHPForAllApplicants(ERPResponse erpResponse, Boolean isHHVerified,
			List<TaxHouseholdMember> taxHHList,
			List<TaxHouseholdMember> ssapHHList) {
		boolean isHHQHPEligible = false;

		/* Set Applicant level QHP flag if applicant is not Medicaid, CHIP, APTC, CSR */
		for (TaxHouseholdMember atTaxPerson : taxHHList) {
			TaxHouseholdMember ssapApplicant = MatchHandler.findMatchingMember(atTaxPerson, ssapHHList);

			if (ssapApplicant != null){

				Long ssapApplicantId = ssapApplicant.getPersonInfo().getPrimaryKey();
				SsapApplicant applicant = ssapApplicantRepository.findOne(ssapApplicantId);

				boolean processQHP = processQHP(applicant, erpResponse.isFullAssessment());

				if (processQHP){
					// 1. check individual Incarcerated flag..
					// 2. check individual lawful presence flag..
					boolean isApplicantQHPEligible = false;
					if (!atTaxPerson.getPersonInfo().isIncarcerated() && atTaxPerson.getPersonInfo().isLawfulPresenceAnswered()){
						isApplicantQHPEligible = true;
						isHHQHPEligible = true;
					}

					// 3. check verifications completed
					boolean isApplicantVerified = false;
					if ("VERIFIED".equals(applicant.getIncarcerationStatus()) &&
							("VERIFIED".equals(applicant.getCitizenshipImmigrationStatus()) || "VERIFIED".equals(applicant.getVlpVerificationStatus()))){
						isApplicantVerified = true;
					}

					if (!isApplicantVerified){ /*If anyone not verified then mark HH not verified */
						isHHVerified = false;
					}

					processQHPRecord(applicant, isApplicantQHPEligible, erpResponse.getGiWsPayloadId());

				}


			} else {
				LOGGER.error(EligibilityConstants.MEMBER_MATCHING_LOGIC_FAILED);
				throw new GIRuntimeException(EligibilityConstants.MEMBER_MATCHING_LOGIC_FAILED +
						"AT Person Detail - " + atTaxPerson +
						"ssap HH List - " + ssapHHList);
			}
		}
		return isHHQHPEligible;
	}


	private boolean processQHP(SsapApplicant applicant, boolean isFullAssessment) {
		boolean processQHP = true;
		if (!isFullAssessment){
			if (StringUtils.equalsIgnoreCase("QHP", applicant.getEligibilityStatus())
					|| StringUtils.equalsIgnoreCase("MEDICAID", applicant.getEligibilityStatus())) {
				processQHP = false;
			}
		} else{
			if (StringUtils.equalsIgnoreCase("QHP", applicant.getEligibilityStatus())) {
				processQHP = false;
			}
		}
		return processQHP;
	}


	private void handleFullDetermination(ERPResponse erpResponse,
			List<TaxHouseholdMember> taxHHList,
			List<TaxHouseholdMember> ssapHHList,
			SsapApplication ssapApplication,
			EligibilityStatus currentEligibilityStatus) {

		Boolean hhVerified = true;/* consider all are verified in HH and let processQHPForAllApplicants set correct value for it */
		boolean isHHQHPEligible = processQHPForAllApplicants(erpResponse, hhVerified,
				taxHHList, ssapHHList);

		/* Change HH Level EligibilityStatus and ExchangeEligibilityStatus incase of DE */
		if (currentEligibilityStatus == EligibilityStatus.DE && isHHQHPEligible){
			ssapApplication.setEligibilityStatus(EligibilityStatus.CAE);

			if (hhVerified){
				ssapApplication.setEligibilityStatus(EligibilityStatus.AE);
			}
			ssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.QHP);
			ssapApplicationRepository.save(ssapApplication);
		}

	}


	private void processQHPRecord(SsapApplicant applicant, boolean isQHPEligible, Integer giWsPayloadId) {

		String eliIndicator = isQHPEligible ? EligibilityConstants.TRUE : EligibilityConstants.FALSE;

		/*List<EligibilityProgram> eliModel = iEligibilityProgram.getEligibilities(applicant.getId());

		EligibilityProgram eligibilityProgramDO = null;
		for (EligibilityProgram eligibiltyProgram : eliModel) {
			if (EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE.equals(eligibiltyProgram.getEligibilityType())){
				eligibilityProgramDO = eligibiltyProgram;
				break;
			}
		}

		Date currentDate = new Date();

		if (eligibilityProgramDO == null) {
			eligibilityProgramDO = new EligibilityProgram();
		}

		eligibilityProgramDO.setEligibilityType(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE);
		eligibilityProgramDO.setEligibilityIndicator(eliIndicator);
		eligibilityProgramDO.setEligibilityStartDate(currentDate);
		eligibilityProgramDO.setEligibilityDeterminationDate(currentDate);
		eligibilityProgramDO.setSsapApplicant(applicant);

		List<com.getinsured.eligibility.model.EligibilityProgramGiwspayload> eligibilityProgramGiwspayloadDOList = new ArrayList<>();
		EligibilityProgramGiwspayload eligibilityProgramGiwspayload = new EligibilityProgramGiwspayload();
		eligibilityProgramGiwspayload.setEligibilityProgram(eligibilityProgramDO);
		eligibilityProgramGiwspayload.setGiWsPayloadId(giWsPayloadId);
		eligibilityProgramGiwspayloadDOList.add(eligibilityProgramGiwspayload);

		eligibilityProgramDO.setEligibilityProgramGiwspayload(eligibilityProgramGiwspayloadDOList);*/

		calculateApplicantLevelEligibility(applicant, eliIndicator);

		//iEligibilityProgram.save(eligibilityProgramDO);
	}

	private void calculateApplicantLevelEligibility(SsapApplicant ssapApplicantDO, String eliIndicator) {
		ssapApplicantDO.setEligibilityStatus("TRUE".equals(eliIndicator) ? "QHP" : "NONE");
		ssapApplicantRepository.save(ssapApplicantDO);
	}

}
