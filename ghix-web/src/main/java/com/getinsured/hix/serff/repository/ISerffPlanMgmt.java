package com.getinsured.hix.serff.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.serff.SerffPlanMgmt;

/**
 * Interface is used provide JpaRepository of table SERFF_PLAN_MGMT.
 * @author Bhavin Parmar
 */
@Repository
public interface ISerffPlanMgmt extends JpaRepository<SerffPlanMgmt, Long> {
	@Query("SELECT s FROM SerffPlanMgmt s WHERE s.processIp=:processIP and SERFF_REQ_ID = (SELECT MAX(s2.serffReqId) FROM SerffPlanMgmt s2 WHERE s2.processIp=:processIP)")
    SerffPlanMgmt getRecentPlanRequestByProcessIP(@Param("processIP") String processIP);
}
