<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- Getting Started v1: previously WAIVED  -->
<fmt:formatDate pattern="MM/dd/yyyy" value="${empEnrollment.openEnrollmentEnd}" var="enollEndDate" />
<fmt:formatDate pattern="MM/dd/yyyy" value="${empEnrollment.coverageDateStart}" var="coverageEffDt" />

<p><spring:message  code="label.prevwaived"/> ${employerName} <spring:message  code="label.sponcoverage"/> ${enollEndDate} <spring:message  code="label.byclickingenroll"/></p>
<h4><spring:message  code="label.employersponsoredcov"/> ${coverageEffDt}</h4>
<div class="form-actions margin-top50">
	<a href="verifyemployee" class="btn btn-primary pull-right"><spring:message  code="label.enroll"/></a>
</div>