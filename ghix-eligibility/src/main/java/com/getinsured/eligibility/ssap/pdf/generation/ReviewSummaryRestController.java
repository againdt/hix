package com.getinsured.eligibility.ssap.pdf.generation;




import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import com.getinsured.eligibility.at.resp.service.EligibilityNotificationService;
import com.getinsured.eligibility.ssap.pdf.generation.ReviewSummaryService;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.hix.model.InboxMsgDoc;
import com.getinsured.hix.platform.account.repository.IInboxMsgDocRepository;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.util.LifeChangeEventConstant;


/**
 * Rest Controller for performing account transfer processing operations.
 *
 * @author Gauri Jape
 *
 */
@Controller("ReviewSummaryRestController")
@RequestMapping("/reviewSummary")
public class ReviewSummaryRestController {

	private static final String FAILURE = "FAILURE";
	private static final String REVIEW_SUMMARY_NOTIFICATION= "REVIEW_SUMMARY_NOTIFICATION";
	private static final String SUCCESS = "SUCCESS";
	@Autowired private ContentManagementService ecmService;
	@Autowired private IInboxMsgDocRepository inboxMsgDocRepo;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	

	private static final Logger LOGGER = Logger.getLogger(ReviewSummaryRestController.class);
	@Autowired private EligibilityNotificationService eligibilityNotificationService;
	@Autowired private IntegrationLogService integrationLogService;
	
	@Autowired private ReviewSummaryService reviewSummaryService;

	private String generateReviewSummaryNotification(String caseNumber, Long ssap_pk, Long giWsPayloadId) {
		String result;
		String ecmId =null;
		try {
			ecmId = reviewSummaryService.generateReviewSummaryNotification(caseNumber);
			result = "Post Download - PDF generated for case Number - " + caseNumber + " document id - " + ecmId;

			String serviceName = REVIEW_SUMMARY_NOTIFICATION;
			String status = SUCCESS;
			integrationLogService.save(result, serviceName, status, ssap_pk, giWsPayloadId);

		} catch (Exception e) {
			// Log in DB to re-trigger notifications from batch process..
			String message = "Error occured while generating eligibility notification for caseNumber - " + caseNumber +  " - " + ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(message);
			result = "Post Eligibility Decision Actions - Notification not generated for case Number - " + caseNumber;

			String serviceName = REVIEW_SUMMARY_NOTIFICATION;
			String status = FAILURE;
			integrationLogService.save(message , serviceName, status, ssap_pk, giWsPayloadId);
		}
		return ecmId;
	}
	
	

	@RequestMapping(value = "/pdfgeneraion", method = RequestMethod.GET)
	@ResponseBody
	public String sendPDFNotification(@RequestParam("caseNumber") String caseNumber) throws Exception {
		String result =null;

		try{
		 result = generateReviewSummaryNotification(caseNumber, null, null);
		 
			
		}
		catch(Exception e){
			
			throw new Exception("Error downloading file from reviewSummmary - " + e);
			
		}

		return result;
	}
	
	
	@RequestMapping(value = "/fetchPDF", method = RequestMethod.POST)
	@ResponseBody
	public String fetchPDF(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String ecmId =null;
		InboxMsgDoc msgDoc= new InboxMsgDoc();

		try{
			String	ssapJson = HtmlUtils.htmlUnescape(request.getParameter(LifeChangeEventConstant.SSAP_JSON));
			
			ecmId = generateSummaryPDF(ssapJson, null, null);
			
					
			if (ecmId ==null){
				throw new GIRuntimeException("Cannot generate notification,ECM Id cannot be null  ");
			}
			
			List<InboxMsgDoc> docList=inboxMsgDocRepo.findInboxMsgDocbyDocID(ecmId);
			
			if(docList!=null)
			{
				msgDoc=docList.get(0);
			}
			
			if (msgDoc !=null ){
				if(msgDoc.getDocName()==null || msgDoc.getMessage().getId()==0)
				{
					throw new GIRuntimeException("Name of document or Message ID cannot be null ");
				}
			}else
			{
				throw new GIRuntimeException("InboxMsgDoc cannot be null  ");
			}
			
			String documentName=msgDoc.getDocName();
			HashMap<String,String> refMap = new HashMap<>(3);
			refMap.put("documentId".intern(), ecmId);
			refMap.put("documentName".intern(), documentName);
			refMap.put("viewType".intern(), "attachment".intern());
			refMap.put("msgId", Long.toString(msgDoc.getMessage().getId()));
			
			String pdfInboxURL="/hix/inbox/getAttachment?ref="+GhixAESCipherPool.encryptParameterMap(refMap);
			
			streamData(response, pdfInboxURL);
			
			
				
		}
		catch(GIRuntimeException e){	
			
			throw e;			
		}
		catch(Exception e){
			
			throw new Exception("Error downloading file from reviewSummmary - " + e);
			
		}

		return null;
	}

	private void streamData(HttpServletResponse response, String fileURL) throws IOException {

		response.setContentType("text");
		response.setHeader("Content-Type", "text/html"); 
		FileCopyUtils.copy(fileURL,response.getWriter());
		
	}
	
	private String generateSummaryPDF(String ssapJson, Long ssap_pk, Long giWsPayloadId) {
		String result;
		String ecmId =null;
		try {
			ecmId = reviewSummaryService.generateSummaryPDF(ssapJson);
			result = "Post Download - PDF generated  - " + ecmId;

			String serviceName = REVIEW_SUMMARY_NOTIFICATION;
			String status = SUCCESS;
			integrationLogService.save(result, serviceName, status, ssap_pk, giWsPayloadId);

		} catch (Exception e) {
			// Log in DB to re-trigger notifications from batch process..
			String message = "Error occured while generating eligibility notification "+ ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(message);
			result = "Post Eligibility Decision Actions - Notification not generated " ;

			String serviceName = REVIEW_SUMMARY_NOTIFICATION;
			String status = FAILURE;
			integrationLogService.save(message , serviceName, status, ssap_pk, giWsPayloadId);
		}
		return ecmId;
	}
	
	

}
