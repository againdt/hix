<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>


<!DOCTYPE html>
<html>

<head>
<%       String trackingCode= "UA-XXXXX-X";
if(GhixConstants.GOOGLE_ANALYTICS_CODE!= null)
	   trackingCode = GhixConstants.GOOGLE_ANALYTICS_CODE;
request.setAttribute("trackingCode",trackingCode); %>
	 	<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m);
	})(window, document, 'script', '//www.google-analytics.com/analytics.js',
			'ga');

	ga('create', '${trackingCode}', '<%=GhixConfiguration.EXCHANGE_URL%>');
	ga('send', 'pageview');
</script>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/> 
<!-- IE8 fixes -->
<!--[if lt IE 9]>
<script type="text/javascript"
	src="<c:url value="/resources/js/custom-phix_ie8.js"/>"></script>
<![endif]-->
<!--  typekit scripts -->
<script type="text/javascript" src="//use.typekit.net/xer8gub.js"></script>
	<script type="text/javascript">
	try {
		Typekit.load();
	} catch (e) {}
	</script>
<link rel="icon" href="<c:url value="/resources/images/favicon.ico"/>" type="image/ico" >	
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap.min-phix.css"/>">
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/bootstrap-responsive.min.css"/>">
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/datepicker-phix.css"/>">
	<!-- end -->
<!--  <link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.min-phix.css"/>"> -->
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/jquery.vegas.css"/>">

	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/nanoscroller-phix.css"/>">


	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/custom4.css"/>">
	<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/custom_overide.css"/>">
	
	<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/phone.css"/>">

	
<!-- jquery 1.10.2 -->
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery-1.10.2.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/custom-phix-prelaunch.js"/>"></script>


<style type="text/css">

body {
background:#47A75D;
}
#prelaunch-main{
min-height:480px
	

}

</style>	
</head>
    <body>
   <section id="prelaunch-main">
  <div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<c:if test="${sessionScope.FlowPhoto eq 'found'}">
         			<spring:url value="/affiliate/logo/${sessionScope.ClickTrackAffiliateId}/${sessionScope.ClickTrackAffiliateFlowId}"  var="logoUrl"/>
			    	<a class="pull-left brand">
			    	<img src="${logoUrl}" alt="Flow logo" />
			    	<span>Affiliate</span></a>
			</c:if> 
			<c:if test="${sessionScope.FlowPhoto ne 'found'}">
					<a class="pull-left brand"
					href="<c:url value="/"/>"><img
					src="<c:url value="/resources/images/logo-getinsured.png"/>"
					alt="GetInsured.com"><span>GetInsured.com</span></a>
			</c:if> 


			<a class="btn btn-navbar" data-toggle="collapse"
			data-target=".nav-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>


		<div class="nav-collapse collapse">
			<ul class="nav pull-right" id="topnav">
				<li class="dropdown">
                            <a href="#" data-toggle="dropdown">GET ASSISTANCE&nbsp;<i class="icon-caret-down"></i></a>
                            <ul class="dropdown-menu getassistance-dd">
                            <c:if test="${sessionScope.ClickTrackIVRNumber==null}">
		                 <li><a href="#">(866) 602 8466</a></li>		
		                </c:if>
		                
		                <c:if test="${sessionScope.ClickTrackIVRNumber!=null}">
		                  <li><a href="#">(<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,0,3)}"/>)&nbsp;<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,3,6)}"/>&nbsp;<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,6,10)}"/></a></li>	
		                </c:if>
<!--                           <li><a href="#">(866) 602 8466</a></li> -->
<!--                           <li><a href="#">Chat</a></li> -->
                          <li><a href="http://www.getinsured.com/answers/" target="_blank">ANSWERS</a></li>
                          </ul>
                        </li>
				<li><a href="http://www.getinsured.com/answers/" target="_blank">ANSWERS</a>
				</li>
<%-- 				<li><a href="<c:url value="/account/signup/individual"/>">SIGN UP</a> --%>
<!-- 				</li> -->
<%-- 				<li><a href="<c:url value="/account/user/login"/>">LOG IN</a> --%>
<!-- 				</li> -->
			</ul>
		</div>
	</div>
</div>
  <div class=" panelwrap row-fluid">
  
          <div class="panel span12">
                	
       
                <h4>Thank you!</h4>
                <!-- <p>
Keep an eye on your inbox.  We'll keep you in the loop on everything you need to know to get quality, affordable health coverage.  Then, come back on October 15th to pick the plan that's right for you.
                </p>
                <p>Want more info? Check out our <a href="#">health care learning center.</a></p>
                <p>Here are some other ways to stay in touch:</p> -->
<h4>Want to learn more about the Affordable Care Act? We've got a ton of <a href="http://www.getinsured.com/answers" target="_blank">answers to help you.</a></h4> 
                    <ul class="siteslist">
                    <li><i class="icon-phone"></i>
                    	 <strong><c:if test="${showStateMessage == 'Y'}">
	                <span id="stateMessage">
						 
							<c:if test="${sessionScope.ClickTrackIVRNumber==null}">
								(866) 602 8466                          
	                        </c:if>                                              
	                        <c:if test="${sessionScope.ClickTrackIVRNumber!=null}">
	                             (<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,0,3)}"/>)&nbsp;<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,3,6)}"/>&nbsp;<c:out value="${fn:substring(sessionScope.ClickTrackIVRNumber,6,10)}"/>              
	                        </c:if>
					</span></strong>
				</c:if>
				</li>
                    <li><i class="icon-twitter-sign"></i>&nbsp;<a href="http://twitter.com/GetInsuredcom" target="_blank">@GetInsured</a></li>
                    <li><i class="icon-facebook-sign"></i>&nbsp;<a href="http://www.facebook.com/GetInsured" target="_blank">facebook.com/GetInsured</a></li>
                    <li><i class="icon-envelope"></i>&nbsp;<a href="mailto:learnmore@getinsured.com">learnmore@GetInsured.com</a></li>
                    </ul>
            
           </div>
       </div>
       </section>
       
      <footer>
<div class="container-fluid">
                        
                        <div class="row-fluid">  
                            
                            <div class="span3">
                                <img src="/hix/resources/img/getinsured-green.png" alt="GetInsured.com">
                                <ul class="media-icons">
                                    <li><a href="https://twitter.com/GetInsuredcom" target="_blank">
                                      <span class="icon-stack">
                                        <i class="icon-circle icon-stack-base"></i>
                                        <i class="icon-twitter icon-light"></i>
                                      </span>
                                      </a></li>
                                    <li><a href="https://www.facebook.com/GetInsured" target="_blank">
                                      <span class="icon-stack">
                                        <i class="icon-circle icon-stack-base"></i>
                                        <i class="icon-facebook icon-light"></i>
                                      </span>
                                    </a></li>
                                 <!--    <li><a href="/answers/contact/">
                                      <span class="icon-stack">
                                        <i class="icon-circle icon-stack-base"></i>
                                        <i class="icon-envelope icon-light"></i>
                                      </span></a></li> -->
                                </ul>
                                
                            </div>
                              
                            <div class="span4 offset2">
                            	
                                <ul class="footer-links">
                                     <li><a href="/answers/how-to-use/" target="_blank">How to Use This Site</a></li>
                                     <!-- <li><a href="/answers/" target="_blank">Health Insurance - What You Need to Know</a></li> -->
                                     <li><a href="https://quoting.getinsured.com/exchange/home.html" target="_blank">Partner with Us</a></li> 
                                     <li><a href="/answers/category/blog/" target="_blank">The Latest from our Blog</a></li>
                                  </ul>
                                  
                             </div>
                                <div class="span3">
                                <ul class="footer-links">
                             
                                      <li><a href="/answers/about/" target="_blank">About GetInsured</a></li>
                                         <li><a href="/answers/contact/" target="_blank">Contact Us</a></li>
                                   <!--   <li><a href="/answers/terms-of-service/">Terms of Service</a></li> -->
                                     <li><a href="/answers/privacy-and-legal-notices/" target="_blank">Privacy and Legal Notices</a></li>
                                  <!--    <li><a href="/answers/terms-and-conditions/">Terms & Conditions</a></li> -->
                               </ul>
                               
                            </div>

                            

                           <!--  <div class="span6 blog-link">
                                <h5>From the GetInsured Blog:</h5>
                                <h4><a href="#">3 Things Young People Should Know About Obamacare</a></h4>
                                <p>Okay, you've seen the surveys. My generation is not paying attention to Obamacare. We have no idea what it is or why it matters. And just like we procrasitnated on our homework in school, we're putting off...</p>
                                Read the Full Article</a>
                            </div> -->
                            
                        </div>
                        
                        <div class="row-fluid copyright">
                          <span>� 2013 GetInsured.com </span>
                        </div>
              </div>      
</footer>
         <script type="text/javascript">
        		$(document).ready(function(){
            	  //dropdownmenu

				  $('#dropDoenIcon').on('click', function() {
				  $('.nav-collapse').toggleClass('open');
	  
				  if($('div.overlay').length){
					$('div.overlay').remove();
				  }
				  else{
					$('<div class="overlay"></div>').css('opacity','0.5').height($(document).height()).appendTo('body');
				  }
				  return false;
				});
				});
            </script>  
    </body>
</html>
    