//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.08.09 at 06:16:25 PM PDT 
//


package com.getinsured.ridpv2.cms.dsh.ridp.extension._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.ridpv2.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for the secondary request which consists of the answers provided along with the session ID.
 * 
 * <p>Java class for SecondaryRequestInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SecondaryRequestInformationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://ridp.dsh.cms.gov/extension/1.0}VerificationAnswerSet"/>
 *         &lt;element ref="{http://ridp.dsh.cms.gov/extension/1.0}SessionIdentification"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecondaryRequestInformationType", propOrder = {
    "verificationAnswerSet",
    "sessionIdentification"
})
public class SecondaryRequestInformationType
    extends ComplexObjectType
{

    @XmlElement(name = "VerificationAnswerSet", required = true)
    protected VerificationAnswerSetType verificationAnswerSet;
    @XmlElement(name = "SessionIdentification", required = true)
    protected String sessionIdentification;

    /**
     * Gets the value of the verificationAnswerSet property.
     * 
     * @return
     *     possible object is
     *     {@link VerificationAnswerSetType }
     *     
     */
    public VerificationAnswerSetType getVerificationAnswerSet() {
        return verificationAnswerSet;
    }

    /**
     * Sets the value of the verificationAnswerSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerificationAnswerSetType }
     *     
     */
    public void setVerificationAnswerSet(VerificationAnswerSetType value) {
        this.verificationAnswerSet = value;
    }

    /**
     * Gets the value of the sessionIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionIdentification() {
        return sessionIdentification;
    }

    /**
     * Sets the value of the sessionIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionIdentification(String value) {
        this.sessionIdentification = value;
    }

}
