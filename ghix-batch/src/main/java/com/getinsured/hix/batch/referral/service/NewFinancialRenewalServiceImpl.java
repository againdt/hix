/**
 * 
 */
package com.getinsured.hix.batch.referral.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author Biswakesh
 *
 */
@Service("newFinancialRenewalService")
public class NewFinancialRenewalServiceImpl implements
		NewFinancialRenewalService {
	
	private static final Logger LOGGER = Logger.getLogger(NewFinancialRenewalServiceImpl.class);
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private UserService userService;
	
	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.referral.service.NewFinancialRenewalService#getSsapApplicationIds(java.lang.String, java.lang.String, java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public List<Long> getSsapApplicationIds(RenewalStatus renewalStatus,
			String applicationType, Long coverageDate,
			EligibilityStatus eligibilityStatus, String applicationStatus) {
		List<Long> ssapIds = null;
		
		try {
			ssapIds = ssapApplicationRepository.findSsapApplicationIdsByRenewalStatus(renewalStatus, applicationType, coverageDate, eligibilityStatus, applicationStatus);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING SSAP IDS: ",ex);
		}
		
		return ssapIds;
	}

	/*
	 * (non-Javadoc)
	 * @see com.getinsured.hix.batch.referral.service.NewFinancialRenewalService#processSsapIds(java.util.List)
	 */
	@Override
	public void processSsapIds(List<Long> ssapIds) {
		try {
			final String userName = getUserName();
			
			for (Long id: ssapIds) {
				processSsapId(String.valueOf(id),userName);
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE PROCESSING SSAP IDS: ",ex);
		}
	}
	
	/**
	 * 
	 * @param id
	 * @param userName
	 */
	private void processSsapId(final String id,final String userName) {
		try {
			triggerReferral(id,userName);
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE PROCESSING SSAP ID: "+id,ex);
		}
	}
	
	
	/**
	 * 
	 * @param ssapId
	 * @param userName
	 */
	public void triggerReferral(final String ssapApplicationId,
			final String userName) {
		Boolean isTriggered = false;
		@SuppressWarnings("serial")
		final Map<String, String> request = new HashMap<String, String>() {
			{
				put("ssapApplicationId", ssapApplicationId);
			}
		};
		final ResponseEntity<Boolean> responseEntity = ghixRestTemplate
				.exchange(
						GhixEndPoints.EligibilityEndPoints.TRIGGER_REFERRAL_NOTICE,
						userName, HttpMethod.GET, MediaType.APPLICATION_JSON,
						Boolean.class, request);
		
		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to trigger notification for "+ssapApplicationId);
			throw new GIRuntimeException("Unable to trigger notification for "+ssapApplicationId);
		} else {
			isTriggered = responseEntity.getBody();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private String getUserName() {
		String userName = ReferralConstants.EXADMIN_USERNAME;
		try {
			AccountUser currentUser = getLoggedInUser();
			if (currentUser != null) {
				userName = currentUser.getUserName();
			}
		} catch (Exception e) {
			LOGGER.error("Error fetching currently logged in user details", e);
		}
		return userName;
	}
	
	/**
	 * 
	 * @return
	 */
	private AccountUser getLoggedInUser() {
		AccountUser currentUser = null;
		String errorMessage;
		try {
			currentUser = userService.getLoggedInUser();
		} catch (InvalidUserException invalidUserException) {
			errorMessage = "Error fetching currently logged in user details";
			LOGGER.error(errorMessage, invalidUserException);
		}
		return currentUser;
	}

}
