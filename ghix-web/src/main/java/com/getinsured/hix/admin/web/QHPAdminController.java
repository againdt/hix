package com.getinsured.hix.admin.web;

import static com.getinsured.hix.planmgmt.service.IssuerService.BENEFITS_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.BROCHURE_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.CERTI_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.PLAN_SUPPORT_DOC_FOLDER;
import static com.getinsured.hix.planmgmt.service.IssuerService.RATES_FOLDER;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.account.repository.ITenantRepository;
import com.getinsured.hix.admin.service.AdminService;
import com.getinsured.hix.admin.util.EnrollmentAvailabilityEmailUtil;
import com.getinsured.hix.admin.util.PlanTenantUtil;
import com.getinsured.hix.dto.broker.BrokerContactDTO;
import com.getinsured.hix.dto.planmgmt.FormularyDrugListResponseDTO;
//import com.serff.planmanagementserffapi.serff.model.service.UpdateExchangeWorkflowStatus;
//import com.serff.planmanagementserffapi.serff.model.service.UpdateExchangeWorkflowStatusResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.EnrollmentAvail;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.Plan.PlanStatus;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.mapper.PlanHealthMapper;
import com.getinsured.hix.planmgmt.provider.service.ProviderNetworkService;
import com.getinsured.hix.planmgmt.repository.IFormularyRepository;
import com.getinsured.hix.planmgmt.repository.IPlanDentalRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
//import com.getinsured.hix.planmgmt.service.email.IssuerPlanCertificationEmail;
//import com.getinsured.hix.planmgmt.service.email.IssuerPlanDeCertificationEmail;
//import com.getinsured.hix.planmgmt.service.email.PlanCertificationBulkStatusUpdateEmail;
import com.getinsured.hix.planmgmt.util.BrokerNotificationEmail;
import com.getinsured.hix.planmgmt.util.ExcelGenerator;
import com.getinsured.hix.planmgmt.util.PlanMgmtRestUtil;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.planmgmt.util.ServiceAreaDetails;
import com.getinsured.hix.platform.comment.service.CommentService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.notify.Notification;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class QHPAdminController {
	private static final Logger LOGGER = LoggerFactory.getLogger(QHPAdminController.class);

	@Autowired private PlanMgmtService planMgmtService;
	@Autowired private IssuerService issuerService;
	@Autowired private AdminService adminService;
	@Autowired private UserService userService;
	@Autowired private ProviderNetworkService providerNetworkService;
	@Autowired private CommentService commentService;
	@Autowired private RestTemplate restTemplate;
/*	@Autowired private IssuerPlanCertificationEmail issuerPlanCertificationEmail;
	@Autowired private IssuerPlanDeCertificationEmail issuerPlanDeCertificationEmail;*/
	//@Autowired private PlanCertificationBulkStatusUpdateEmail planCertificationBulkStatusUpdateEmail;
//	@Autowired private IIssuerRepresentativeRepository representativeRepository;
	@Autowired private IPlanHealthRepository iPlanHealthRepository;
	@Autowired private IPlanDentalRepository iPlanDentalRepository;
	@Autowired private IFormularyRepository iFormularyRepository;
	@Autowired private ITenantRepository iTenantRepository;
	@Autowired private ContentManagementService ecmService;
	@Autowired private EnrollmentAvailabilityEmailUtil enrollmentAvailabilityEmailUtil;
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private BrokerNotificationEmail brokerNotificationEmail;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private PlanMgmtRestUtil planMgmtRestUtil;
	@Autowired private Gson platformGson;

	private static final String PLAN_DIS_DATE = "planDisplayDate";
	private static final String NO_USER_LOGGED_IN = "No user logged in";
	private static final String ERROR_UPLOAD_BENEFITS = "Error while saving the uploadng benefits.";
	//private static final int MONTH_DAY = 30;

	private static final String ADMIN_LOGIN_PATH = "redirect:/account/user/login?module=admin";
	private static final String MANAGE_QHP_PATH = "redirect:/admin/planmgmt/manageqhpplans";
	private static final String SUBMIT_QHP_PATH = "redirect:/admin/planmgmt/submitqhp";
	private static final String CERTIFIED = "CERTIFIED";
	
	@Value("#{configProp.uploadPath}") private String uploadPath;
//	private java.lang.String doAHBXPlanSync = "0";

	@RequestMapping(value = "/admin/planmgmt/submitqhp", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String submitQHP(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan planModel, Model model, HttpServletResponse response) {
		try{
		loadPlanToDisplay(model,planModel.getId());
		PlanHealth planHealthModel = new PlanHealth();
		model.addAttribute("qhpmarketList", planModel.getMarketList());
		model.addAttribute("qhplevelList", planModel.getLevelList());
		model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		model.addAttribute("qhpHsaList", planModel.getHsaList());
		model.addAttribute("ehbCoveredList", planHealthModel.getEHBCoveredList());
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.GET_QHP_SUBMIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return "admin/planmgmt/submitqhp";
	}

	/**
	 * Fetches plan for populating its data in the UI
	 * 
	 * @param model
	 *            model attribute in which the data should be set.
	 */
	private Plan loadPlanToDisplay(Model model,Integer planId) {
		Plan plan = null;
		if (planId != null) {
			try{
			plan = planMgmtService.getPlan(planId);
			model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
				String enrollmentAvial = plan.getEnrollmentAvailList().get(Enum.valueOf(EnrollmentAvail.class, plan.getEnrollmentAvail()));
				Calendar cal = TSCalendar.getInstance();
				Date today = cal.getTime();
				model.addAttribute("enrollAvailability", enrollmentAvial);
				
				// Code changes for HIX-HIX-26331
				SimpleDateFormat ft = new SimpleDateFormat ("MMddyyyy");
				if(today.compareTo(plan.getStartDate()) < 0){
					model.addAttribute(PLAN_DIS_DATE, ft.format(plan.getStartDate()));
				}
				else if(today.compareTo(plan.getEndDate()) > 0){
					model.addAttribute(PLAN_DIS_DATE, ft.format(plan.getEndDate()));
				} else {
					model.addAttribute(PLAN_DIS_DATE, ft.format(today));
				}
				model.addAttribute("planHealthacturialValueCertificate", plan.getPlanHealth().getActurialValueCertificate());
				if(plan.getEhbPremiumFraction() != null){
//					Integer ehbPremiumFraction =  (Integer)Math.round(plan.getEhbPremiumFraction() * 100);
					model.addAttribute("ehbPremiumFraction", plan.getEhbPremiumFraction());
				}
				
				if (plan.getProviderNetworkId() != null) {
					Network networkInfo = providerNetworkService.getNetwork(plan.getProviderNetworkId());
					if (networkInfo != null) {
						model.addAttribute(PlanMgmtConstants.NETWORK, networkInfo);
					}
				}
				
				if(plan.getServiceAreaId() != null){
					String serfServiceAreaId = issuerService.getSerfServiceAreaIdById(plan.getServiceAreaId());
					model.addAttribute(PlanMgmtConstants.SERVICE_AREA_ID, serfServiceAreaId);
				}
			}catch(Exception e){
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.PLAN_DISPLAY.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
			}
		}
		return plan;
	}

	@RequestMapping(value = "/admin/planmgmt/submitqhp", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPPersist(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan plan, BindingResult result, Model model, RedirectAttributes redirectAttrs, @RequestParam(value = "planLevelReadOnly", required = false) String planLevelReadOnly, @RequestParam(value = "planEhbCoveredReadOnly", required = false) String planEhbCoveredReadOnly) {
		try{
		plan.setStatus(PlanStatus.INCOMPLETE.toString());
		plan.setEnrollmentAvail(EnrollmentAvail.NOTAVAILABLE.toString());
		plan.setEnrollmentAvailEffDate(plan.getStartDate());
		plan.setInsuranceType(PlanInsuranceType.HEALTH.toString());
		plan.getPlanHealth().setBenefitEffectiveDate(plan.getStartDate());
		plan.getPlanHealth().setRateEffectiveDate(plan.getStartDate());
		//Set default service area id, when upload QHP from UI
		// plan.setServiceAreaId(issuerService.getDefaultServieAreaId("CAALLSTATE"));
		Calendar today = TSCalendar.getInstance();   // This gets the current date and time.
		Integer applicableYear = today.get(Calendar.YEAR);   // This returns the year as an int.
		plan.setServiceAreaId(issuerService.getDefaultServieAreaId("CAALLSTATE", applicableYear));
		
		try {
			Plan updatedPlan = updatePlan(plan, redirectAttrs, planLevelReadOnly, planEhbCoveredReadOnly);
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, Integer.toString(updatedPlan.getId()));
		} catch (InvalidUserException e) {
			LOGGER.error(NO_USER_LOGGED_IN, e);
		}
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QH_PLAN_EDIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "redirect:/admin/planmgmt/submitqhpbenefits";

	}

	private Plan updatePlan(Plan plan, RedirectAttributes redirectAttrs, String planLevelReadOnly, String planEhbCoveredReadOnly) throws InvalidUserException, GIException, IOException, ContentManagementServiceException {
		
		Plan updatedPlan = null;
		Integer userId = null;
		
		//try {
			if (plan.getPlanHealth().getPlanLevel() == null) {
				plan.getPlanHealth().setPlanLevel(planLevelReadOnly);
			}
			if (plan.getPlanHealth().getEhbCovered() == null) {
				plan.getPlanHealth().setEhbCovered(planEhbCoveredReadOnly);
			}
			
			userId = userService.getLoggedInUser().getId();
			plan.setLastUpdatedBy(userId);
			if (plan.getId() > 0) {
				updatedPlan = planMgmtService.updatePlan(plan);
				redirectAttrs.addFlashAttribute(PlanMgmtConstants.IS_EDIT, PlanMgmtConstants.TRUE_STRING);
			} else {
				plan.setCreatedBy(userId);
				updatedPlan = planMgmtService.savePlan(plan);
			}
		
		return updatedPlan;
	}

	@RequestMapping(value = "/admin/planmgmt/submitqhpbenefits", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPBenefits(Model model, HttpServletResponse response) {
		try{
		String planId = (String) model.asMap().get(PlanMgmtConstants.PLAN_ID);
		if (planId == null) {
			return SUBMIT_QHP_PATH;
		}
		String isEdit = (String) model.asMap().get(PlanMgmtConstants.IS_EDIT);
		if (isEdit == null || !isEdit.equals(PlanMgmtConstants.TRUE_STRING)) {
			model.addAttribute(PlanMgmtConstants.IS_EDIT, PlanMgmtConstants.FALSE_STRING);
		}
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		model.addAttribute(PlanMgmtConstants.ISSUER_NAME, plan.getIssuer().getName());
		model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
		model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, plan.getIssuerPlanNumber());
		model.addAttribute("benefitsFileName", planMgmtService.getActualFileName(plan.getPlanHealth().getBenefitFile()));
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_BENEFIT_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return "admin/planmgmt/submitqhpbenefits";
	}

	@RequestMapping(value = "/admin/planmgmt/submitqhpbenefits", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPBenefitsUpload(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan planModel, BindingResult result, @RequestParam(value = PlanMgmtConstants.BENEFIT_FILE, required = false) String benefitsFile, @RequestParam(value = "isEdit", required = false) String isEdit, RedirectAttributes redirectAttrs) {
		if (!adminService.isAdminLoggedIn()) {
			return ADMIN_LOGIN_PATH;
		}
		try {
		if (benefitsFile != null && benefitsFile.length() > 0) {
			Plan plan = planMgmtService.getPlan(planModel.getId());
			LOGGER.debug("Benefits file " + SecurityUtil.sanitizeForLogging(String.valueOf(benefitsFile)));
			
				if (PlanMgmtConstants.TRUE_STRING.equals(isEdit)) {
					String filePath = uploadPath+ File.separator + BENEFITS_FOLDER + File.separator+ benefitsFile;
					boolean isValidPath = planMgmtUtils.isValidPath(filePath);
					if(isValidPath){
						planMgmtService.updatePlanBenefits(plan, filePath);
					}else{
						LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
					}
				} else {
					String filePath = uploadPath+ File.separator + BENEFITS_FOLDER + File.separator+ benefitsFile;
					boolean isValidPath = planMgmtUtils.isValidPath(filePath);
					if(isValidPath){
						planMgmtService.savePlanBenefits(plan, filePath);
					}else{
						LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
					}
				}
			
		}
		} catch (Exception e) {
			LOGGER.error(ERROR_UPLOAD_BENEFITS,e);
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, Integer.toString(planModel.getId()));
			redirectAttrs.addFlashAttribute("fileError", "<label class='error'><span> <em class='excl'>!</em>Error while uploading the file.Please try again.</span></label>");
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.BENEFIT_FILE_UPLOAD.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		redirectAttrs.addFlashAttribute(PlanMgmtConstants.IS_EDIT, isEdit);
		redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, Integer.toString(planModel.getId()));
		return "redirect:/admin/planmgmt/submitqhprates";
	}

	@RequestMapping(value = "/admin/planmgmt/getissuers", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	@ResponseBody
	public String getIssuerNames() {
		try{
		return issuerService.getAllCertifiedIssuerNames();
	}catch(Exception e){
		LOGGER.error("Exception occured while getting IssuerNames: ",e);
		giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_NAMES.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
		return PlanMgmtConstants.GET_ISSUER_NAMES_FAILED;
	}
	}

	@RequestMapping(value = "/admin/planmgmt/submitqhprates", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPRates(Model model, HttpServletResponse response) {
		try {
		String planId = (String) model.asMap().get(PlanMgmtConstants.PLAN_ID);
		if (planId == null) {
			return SUBMIT_QHP_PATH;
		}
		String isEdit = (String) model.asMap().get(PlanMgmtConstants.IS_EDIT);
		if (isEdit == null || !isEdit.equals(PlanMgmtConstants.TRUE_STRING)) {
			model.addAttribute(PlanMgmtConstants.IS_EDIT, PlanMgmtConstants.FALSE_STRING);
		}
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		model.addAttribute(PlanMgmtConstants.ISSUER_NAME, plan.getIssuer().getName());
		model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
		model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, plan.getIssuerPlanNumber());
		model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
		model.addAttribute("ratesFileName", planMgmtService.getActualFileName(plan.getPlanHealth().getRateFile()));
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_SUBMIT_RATE.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return "admin/planmgmt/submitqhprates";
	}

	@RequestMapping(value = "/admin/planmgmt/submitqhprates", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPRatesUpload(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan planModel, BindingResult result, Model model, @RequestParam(value = "isEdit", required = false) String isEdit, @RequestParam(value = PlanMgmtConstants.RATE_FILE, required = false) String ratesFile, RedirectAttributes redirectAttrs) {
		try{
		if (!adminService.isAdminLoggedIn()) {
			return ADMIN_LOGIN_PATH;
		}
		if (ratesFile != null && ratesFile.length() > 0) {
			Plan plan = planMgmtService.getPlan(planModel.getId());
			LOGGER.debug("Rates file " + SecurityUtil.sanitizeForLogging(String.valueOf(ratesFile)));
			
				if (PlanMgmtConstants.TRUE_STRING.equals(isEdit)) {
					String filePath = uploadPath+ File.separator + RATES_FOLDER + File.separator+ ratesFile;
					boolean isValidPath = planMgmtUtils.isValidPath(filePath);
					if(isValidPath){
						planMgmtService.updatePlanRates(plan, filePath);
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.IS_EDIT, PlanMgmtConstants.TRUE_STRING);
					}else{
						LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
					}
				} else {
					String filePath = uploadPath+ File.separator + RATES_FOLDER + File.separator+ ratesFile;
					boolean isValidPath = planMgmtUtils.isValidPath(filePath);
					if(isValidPath){
						redirectAttrs.addFlashAttribute(PlanMgmtConstants.IS_EDIT);
						planMgmtService.savePlanRates(plan, filePath);
					}else{
						LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
					}
				}
			
		}
		} catch (Exception e) {
			LOGGER.error(ERROR_UPLOAD_BENEFITS,e);
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, Integer.toString(planModel.getId()));
			redirectAttrs.addFlashAttribute("fileError", "<label class='error'><span> <em class='excl'>!</em>Error while uploading the file.Please try again.</span></label>");
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHP_RATE_UPLOAD.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, Integer.toString(planModel.getId()));
		return "redirect:/provider/network";
	}

	/*
	 * @Suneel: Secured the method with permission
	 * 
	 */
	@RequestMapping(value = "/admin/planmgmt/manageqhpplans", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String listQHPPlans(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan plan, Model model, HttpServletRequest manageQHPrequest) {

		LOGGER.debug("PERFORMANCE TESTING : Method Execution stated - "+ SecurityUtil.sanitizeForLogging(String.valueOf(new Timestamp(new TSDate().getTime()))));
		
		Integer startRecord = 0;
		int currentPage = 1;
		int showPendingPlanCtr = 1;
		List<String> selectedPlanLevels = new ArrayList<String>();
		String selectedPlanLevel = "";
		String selectedState = "";
		Integer issuerId = 0;
		List<Tenant> tenantList = new ArrayList<Tenant>();
		String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
		List<Issuer> issuers = issuerService.getAllIssuerNamesByInsuranceType(PlanInsuranceType.HEALTH.toString());
		model.addAttribute("issuers", issuers);
		model.addAttribute("statusList", plan.getPlanStatuses());
		model.addAttribute("marketList", plan.getMarketList());
		model.addAttribute("levelList", plan.getLevelList());
		model.addAttribute("verifiedList", plan.getVerifiedList());
		model.addAttribute("enrollmentAvailList", plan.getEnrollmentAvailList());
		try {
			List<Map<String, Integer>> planYearsList = planMgmtService.getPlanYears(PlanMgmtConstants.HEALTH, issuerId);
		if (exchangeType.equals(GhixConstants.PHIX)) {
			tenantList = iTenantRepository.getTenantCodeAndNameMap();
			if (manageQHPrequest.getParameter(PlanMgmtConstants.STATES) != null) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.STATES, manageQHPrequest.getParameter(PlanMgmtConstants.STATES));
				selectedState = manageQHPrequest.getParameter(PlanMgmtConstants.STATES);
			}
			if (manageQHPrequest.getParameter(PlanMgmtConstants.EX_TYPE) != null) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.EX_TYPE, manageQHPrequest.getParameter(PlanMgmtConstants.EX_TYPE));
			}
			if (manageQHPrequest.getParameter(PlanMgmtConstants.TENANT) != null) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.TENANT, manageQHPrequest.getParameter(PlanMgmtConstants.TENANT));
			}
			model.addAttribute(PlanMgmtConstants.EX_TYPE, manageQHPrequest.getParameter(PlanMgmtConstants.EX_TYPE));
			model.addAttribute(PlanMgmtConstants.TENANT, manageQHPrequest.getParameter(PlanMgmtConstants.TENANT));
			model.addAttribute("stateList", new StateHelper().getAllStates());
			model.addAttribute("exTypeList", plan.getExchangeTypeList());
			model.addAttribute("tenantList", tenantList);
		}
		
//		if (manageQHPrequest.getParameter(PlanMgmtConstants.ISSUERNAME) != null) {
//			if(!manageQHPrequest.getParameter(PlanMgmtConstants.ISSUERNAME).isEmpty()){
//				String[] issuerDtls = manageQHPrequest.getParameter(PlanMgmtConstants.ISSUERNAME).split("_");
//				issuerName = issuerDtls[1]; 			
//				manageQHPrequest.setAttribute(PlanMgmtConstants.ISSUERNAME, issuerName);
//			}
//		}
		if (manageQHPrequest.getParameter(PlanMgmtConstants.ISSUERID) != null) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.ISSUERID, manageQHPrequest.getParameter(PlanMgmtConstants.ISSUERID));
		}
		// Plan Number need to be add for sorting
		if (manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER) != null) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.PLAN_NUMBER, manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER));
		}
		if (manageQHPrequest.getParameter(PlanMgmtConstants.MARKET) != null && !manageQHPrequest.getParameter(PlanMgmtConstants.MARKET).equals("")) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.MARKET, manageQHPrequest.getParameter(PlanMgmtConstants.MARKET));
		}
		if (manageQHPrequest.getParameter(PlanMgmtConstants.STATUS) != null && !manageQHPrequest.getParameter(PlanMgmtConstants.STATUS).equals("")) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.STATUS, manageQHPrequest.getParameter(PlanMgmtConstants.STATUS));
			showPendingPlanCtr = 0;
		}
		if (!StringUtils.isEmpty(manageQHPrequest.getParameter(PlanMgmtConstants.ENROLLMENTAVAILABILITY))) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.ENROLLMENTAVAILABILITY, manageQHPrequest.getParameter(PlanMgmtConstants.ENROLLMENTAVAILABILITY));
		}
		if (manageQHPrequest.getParameter(PlanMgmtConstants.VERIFIED) != null && !manageQHPrequest.getParameter(PlanMgmtConstants.VERIFIED).equals("")) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.VERIFIED, manageQHPrequest.getParameter(PlanMgmtConstants.VERIFIED));
		}
		// Parsing all plan levels
		if (manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_LEVEL) != null && !manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_LEVEL).isEmpty()) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.PLAN_LEVEL, manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_LEVEL));
			selectedPlanLevel = manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_LEVEL);
		}
		// Sorting attributes
		if (manageQHPrequest.getParameter(PlanMgmtConstants.SORT_BY) != null) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.SORT_BY, manageQHPrequest.getParameter(PlanMgmtConstants.SORT_BY));
		}
		if (manageQHPrequest.getSession().getAttribute(PlanMgmtConstants.SORT_BY) != null) {
			manageQHPrequest.setAttribute(PlanMgmtConstants.SORT_BY, manageQHPrequest.getSession().getAttribute(PlanMgmtConstants.SORT_BY));
		}
		String sortOrder = (StringUtils.isEmpty(manageQHPrequest.getParameter(PlanMgmtConstants.SORT_ORDER))) ? PlanMgmtConstants.SORT_ORDER_DESC : ((manageQHPrequest.getParameter(PlanMgmtConstants.SORT_ORDER).equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_ASC) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC));
		
		String changeOrder = (StringUtils.isEmpty(manageQHPrequest.getParameter(PlanMgmtConstants.CHANGE_ORDER))) ? PlanMgmtConstants.FALSE_STRING : ((manageQHPrequest.getParameter(PlanMgmtConstants.CHANGE_ORDER).equalsIgnoreCase(PlanMgmtConstants.FALSE_STRING) ? PlanMgmtConstants.FALSE_STRING : PlanMgmtConstants.TRUE_STRING));
		sortOrder = (changeOrder.equals(PlanMgmtConstants.TRUE_STRING)) ? ((sortOrder.equals(PlanMgmtConstants.SORT_ORDER_DESC)) ? PlanMgmtConstants.SORT_ORDER_ASC: PlanMgmtConstants.SORT_ORDER_DESC): sortOrder;
		
		manageQHPrequest.removeAttribute(PlanMgmtConstants.CHANGE_ORDER);
		manageQHPrequest.setAttribute(PlanMgmtConstants.SORT_ORDER, sortOrder);
		manageQHPrequest.setAttribute(PlanMgmtConstants.PAGE_SIZE_VAR, GhixConstants.PAGE_SIZE);
		
		String param = manageQHPrequest.getParameter(PlanMgmtConstants.PAGE_NUMBER);
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1)* GhixConstants.PAGE_SIZE;
			currentPage = Integer.parseInt(param);
		}
		manageQHPrequest.setAttribute("startRecord", startRecord);

		if (manageQHPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR) != null && !manageQHPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).equals("")) {
			Map<String, Integer> selectedYearMap = new HashMap<String, Integer>();
			selectedYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(manageQHPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).toString()));
			model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, selectedYearMap);
			LOGGER.debug("Fetching Plans for Year : " + SecurityUtil.sanitizeForLogging(String.valueOf(selectedYearMap.toString())));
			manageQHPrequest.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, (manageQHPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).toString()));
		}
		else {
			if (planYearsList.size() > PlanMgmtConstants.ZERO) {
				if(manageQHPrequest.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK) != null){
					model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, Integer.parseInt(manageQHPrequest.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString()));
					LOGGER.debug("Fetching Plans for Year : " + SecurityUtil.sanitizeForLogging(String.valueOf(manageQHPrequest.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString())));
					manageQHPrequest.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, manageQHPrequest.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString());
					Map<String, Integer> selectedYearMap = new HashMap<String, Integer>();
					selectedYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(manageQHPrequest.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString()));
					model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, selectedYearMap);
					LOGGER.debug("Updating Plans for Year : " + SecurityUtil.sanitizeForLogging(String.valueOf(selectedYearMap.toString())));
					model.addAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, manageQHPrequest.getSession().getAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK).toString());
					manageQHPrequest.getSession().removeAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK);
				}else{
					model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, planYearsList.get(PlanMgmtConstants.ZERO));
					LOGGER.debug("Fetching Plans for Year : " + SecurityUtil.sanitizeForLogging(String.valueOf(planYearsList.get(PlanMgmtConstants.ZERO).toString())));
					manageQHPrequest.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, (planYearsList.get(PlanMgmtConstants.ZERO).get(PlanMgmtConstants.PLAN_YEARS_KEY)));
				}
			}
			else {
 	 	 		return "admin/planmgmt/manageqhpplans";
 	 	 	}
		}
		
		if(null == manageQHPrequest.getParameter("pageNumber")){
 	 	 	HttpSession session = manageQHPrequest.getSession();
 	 	 	if(session.getAttribute(PlanMgmtConstants.SELECTALL_QHP_PLANS) !=  null){
 	 	 		session.removeAttribute(PlanMgmtConstants.SELECTALL_QHP_PLANS);
 	 	 	}
		}
		
		// retrieving the plans
		List<Plan> planList = planMgmtService.getPlanList(manageQHPrequest, PlanMgmtService.HEALTH);
		
		Map<Plan.PlanStatus, String> bulkStatusList = new HashMap<Plan.PlanStatus, String>();
		bulkStatusList.put(Plan.PlanStatus.CERTIFIED, "Certified");
		bulkStatusList.put(Plan.PlanStatus.INCOMPLETE, "Incomplete");
		model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		model.addAttribute("selectedPlanLevels", selectedPlanLevels);
		model.addAttribute(PlanMgmtConstants.STATES, selectedState);
		model.addAttribute(PlanMgmtConstants.STATUS, manageQHPrequest.getParameter(PlanMgmtConstants.STATUS));
		model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER));		
		model.addAttribute(PlanMgmtConstants.ISSUERID, manageQHPrequest.getParameter(PlanMgmtConstants.ISSUERID));
		model.addAttribute(PlanMgmtConstants.MARKET, manageQHPrequest.getParameter(PlanMgmtConstants.MARKET));
		model.addAttribute(PlanMgmtConstants.SORT_BY, manageQHPrequest.getParameter(PlanMgmtConstants.SORT_BY));
		model.addAttribute("plans", planList);
		model.addAttribute("selectedPlanLevel", selectedPlanLevel);
		model.addAttribute("certifyStatus", PlanMgmtService.STATUS_CERTIFIED);
		model.addAttribute("issuerVerificationList", plan.getIssuerVerificationList());
		model.addAttribute("enrollmentAvailList", plan.getEnrollmentAvailList());
		model.addAttribute("bulkStatusList", bulkStatusList);
		model.addAttribute("showPendingPlanCtr", showPendingPlanCtr);
		model.addAttribute("currentPage", currentPage);
		model.addAttribute(PlanMgmtConstants.SORT_ORDER, sortOrder);
		model.addAttribute(PlanMgmtConstants.SORT_ORDER_EXCEPTFIRSTCOLUMN, sortOrder);
		model.addAttribute(PlanMgmtConstants.PLAN_YEARS_LIST, planYearsList);
		model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
		model.addAttribute("sysDate", new TSDate());
		
		
		setExchangeTypeInModel(model);
		} catch (Exception e) {
			LOGGER.error("Exception is: ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MANAGE_QHP.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("PERFORMANCE TESTING OLD : End Method Execution - "+ SecurityUtil.sanitizeForLogging(String.valueOf(new TSTimestamp())));
		}
		return "admin/planmgmt/manageqhpplans";

	}
	
	private void setExchangeTypeInModel(Model model){
		model.addAttribute("exchangeType", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
	}

	@RequestMapping(value = "/admin/planmgmt/editqhpplanstatus/{id}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String updatePlanStatus(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan planmodel, Model model, HttpServletRequest request, @PathVariable("id") Integer planId) {
		try {
		if (!adminService.isAdminLoggedIn()) {
			return ADMIN_LOGIN_PATH;
		}
		Plan plan = planMgmtService.getPlan(planId);
		if (plan != null) {
			model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
			model.addAttribute("actCerti", planMgmtService.getActualFileName(plan.getPlanHealth().getActurialValueCertificate()));
			model.addAttribute(PlanMgmtConstants.BENEFIT_FILE, planMgmtService.getActualFileName(plan.getPlanHealth().getBenefitFile()));
			model.addAttribute(PlanMgmtConstants.RATE_FILE, planMgmtService.getActualFileName(plan.getPlanHealth().getRateFile()));
			model.addAttribute("qhpcertiStatusList", planmodel.getPlanStatuses());
			model.addAttribute("network", plan.getNetwork());
			return "admin/planmgmt/updatestatusqhpplans";
		} else {
			return MANAGE_QHP_PATH;
		}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.GET_EDIT_QHPSTATUS.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	
	@RequestMapping(value = "/admin/planmgmt/submitqhpvariations", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPVariations(Model model, HttpServletResponse response) {
		String planId = (String) model.asMap().get(PlanMgmtConstants.PLAN_ID);
		try{
		if (planId == null) {
			return SUBMIT_QHP_PATH;
		}
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		model.addAttribute(PlanMgmtConstants.PLAN_ID, planId);
		model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
		model.addAttribute(PlanMgmtConstants.ISSUER_NAME, plan.getIssuer().getName());
		model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, plan.getIssuerPlanNumber());
		Map<String, String> childData = planMgmtService.getReviewScreenDetails(planId);
		if (childData != null && !childData.isEmpty()) {
			model.addAllAttributes(childData);
		}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHP_EDIT_VARIATION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/submitqhpvariations";
	}

	@RequestMapping(value = "/admin/planmgmt/submitcostsharing", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	@ResponseBody
	public String submitQHPReview(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttrs) {
		boolean isValidcertiFile = false;
		boolean isValidbenefitFile = false;
		try {
			if (!adminService.isAdminLoggedIn()) {
				return ADMIN_LOGIN_PATH;
			}
			String costSharing = request.getParameter("costSharing");
			String sectionId = planMgmtService.getSectionId(costSharing);
			String planId = request.getParameter(PlanMgmtConstants.PLAN_ID + sectionId);
			String certiFile = request.getParameter("certiFile" + sectionId);
			String benefitFile = request.getParameter("benefitsFile" + sectionId);
			if (certiFile != null && certiFile.length() > 0) {
				certiFile = uploadPath + File.separator + CERTI_FOLDER + File.separator + certiFile;
				boolean isValidPath = planMgmtUtils.isValidPath(certiFile);
				if(isValidPath){
					isValidcertiFile = true;
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}
			if (benefitFile != null && benefitFile.length() > 0) {
				benefitFile = uploadPath + File.separator + BENEFITS_FOLDER+ File.separator + benefitFile;
				boolean isValidPath = planMgmtUtils.isValidPath(benefitFile);
				if(isValidPath){
					isValidbenefitFile = true;
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}
			if(isValidcertiFile && isValidcertiFile){
				planMgmtService.addCostSharingPlan(planId, costSharing, certiFile, benefitFile);
			}
			redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, planId);
		
		}catch(Exception e){
			LOGGER.error("Exception occured while getting IssuerNames: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.SUBMIT_QHP_REVIEW.toString(), new TSDate(), Exception.class.getName(),e.getMessage().toString(), null);
			return PlanMgmtConstants.SUBMIT_FAILED;
		}
		
		return "redirect:/admin/planmgmt/submitqhpbenefits";
	}

	@RequestMapping(value = "/admin/planmgmt/isExistingPlan", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	@ResponseBody
	public String getMatchingPlanCount(HttpServletRequest request) {
		String hasPlan = PlanMgmtConstants.FALSE_STRING;
		try
		{
		if (!adminService.isAdminLoggedIn()) {
			return "";
		}
		String issuerId = request.getParameter("issuerId");
		String name = request.getParameter("name");
		Long count = new Long(0);
			count = planMgmtService.getMatchingPlanCount(Integer.parseInt(issuerId), name);
		
		if (count > 0) {
			hasPlan = PlanMgmtConstants.TRUE_STRING;
		}
		}catch(Exception e){
			//throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MATCHING_PLAN_COUNT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
			LOGGER.error("Exception occured while getting plan count: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.MATCHING_PLAN_COUNT.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			return PlanMgmtConstants.ERROR;
		}
		return hasPlan;
	}

	@RequestMapping(value = "/admin/planmgmt/submitqhpvariations", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPVariations(@RequestParam(value = PlanMgmtConstants.PLAN_ID, required = false) String planId, RedirectAttributes redirectAttrs) {
		if (!adminService.isAdminLoggedIn()) {
			return ADMIN_LOGIN_PATH;
		}
		redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, planId);
		return "redirect:/admin/planmgmt/submitqhpreview";
	}

	@RequestMapping(value = "/admin/planmgmt/submitqhpreview", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPReview(Model model, HttpServletResponse response) {
		String planId = (String) model.asMap().get(PlanMgmtConstants.PLAN_ID);
		try {
		if (planId == null) {
			return SUBMIT_QHP_PATH;
		}
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
		model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
		model.addAttribute(PlanMgmtConstants.ISSUER_NAME, plan.getIssuer().getName());
		model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, plan.getIssuerPlanNumber());

		model.addAttribute("startDate", plan.getStartDate());
		model.addAttribute("endDate", (plan.getEndDate() == null ? "Not Specified" : plan.getEndDate()));
		model.addAttribute("isSilverPlan", plan.getPlanHealth().getPlanLevel().equalsIgnoreCase(PlanLevel.SILVER.toString()));
		model.addAttribute("actCerti", planMgmtService.getActualFileName(plan.getPlanHealth().getActurialValueCertificate()));
		model.addAttribute(PlanMgmtConstants.BENEFIT_FILE, planMgmtService.getActualFileName(plan.getPlanHealth().getBenefitFile()));
		model.addAttribute(PlanMgmtConstants.RATE_FILE, planMgmtService.getActualFileName(plan.getPlanHealth().getRateFile()));
		Map<String, String> silverPlanData = planMgmtService.getReviewScreenDetails(planId);
		if (plan.getNetwork() != null) {
			model.addAttribute("netName", plan.getNetwork().getName());
			model.addAttribute("netType", plan.getNetwork().getType());
		}
		if (silverPlanData != null && !silverPlanData.isEmpty()) {
			model.addAllAttributes(silverPlanData);
		}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHP_REVIEW_SUBMIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "admin/planmgmt/submitqhpreview";
	}

	@RequestMapping(value = "/admin/planmgmt/editSilverPlan", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPReview(Model model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		try {
		String planId1 = request.getParameter(PlanMgmtConstants.PLAN_ID);
		String secion = request.getParameter("secion");
		redirectAttrs.addFlashAttribute(PlanMgmtConstants.IS_EDIT, PlanMgmtConstants.TRUE_STRING);
		redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, planId1);
		redirectAttrs.addFlashAttribute("section", secion);
		String redirectView = "redirect:/admin/planmgmt/submitqhpvariations";
		if ("0".equals(secion)) {
			redirectView = SUBMIT_QHP_PATH;
		} else if ("6".equals(secion)) {
			redirectView = "redirect:/provider/network";
		}
		return redirectView;
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHP_REVIEW_SUBMIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		
	}

	@RequestMapping(value = "/admin/planmgmt/deletePlan", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	@ResponseBody
	public String deletePlan(Model model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		try {
		if (!adminService.isAdminLoggedIn()) {
			return ADMIN_LOGIN_PATH;
		}
		LOGGER.debug("Delete Plan called");
		String planId1 = request.getParameter(PlanMgmtConstants.PLAN_ID);
		String secionId = request.getParameter("secionId");
		planMgmtService.deletePlan(planId1, secionId);
		if ("0".equals(secionId)) {
			return "/admin/planmgmt/submitqhp";
		} else {
			return PlanMgmtConstants.SUCCESS;
		}
		} catch (Exception e) {
			//throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHP_DELETE.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
			LOGGER.error("Exception occured while getting plan count: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.QHP_DELETE.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			return PlanMgmtConstants.ERROR;
		}
	}

	@RequestMapping(value = "/admin/planmgmt/submitqhpstatus", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	@ResponseBody
	public String updatePlan(Model model, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		try {
		if (!adminService.isAdminLoggedIn()) {
			return ADMIN_LOGIN_PATH;
		}
		LOGGER.info("submitQHPStatus Plan called");
		String planId = request.getParameter(PlanMgmtConstants.PLAN_ID);
		LOGGER.info("Plan to be updated as Pending status");
		String userName = null;
		int userId = 0;
		
			userName = userService.getLoggedInUser().getUserName();
			userId = userService.getLoggedInUser().getId();
		
		planMgmtService.updateStatus(planId, userName, PlanMgmtService.STATUS_LOADED, userId+ "", true, null, null, null);
		
		return PlanMgmtConstants.SUCCESS;
		}catch (Exception e) {
			//throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.UPDTATE_QHPSTATUS.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
			LOGGER.error("Exception occured while undo Decertification: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_CERTIFICATION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			return PlanMgmtConstants.ERROR;
		}
		
	}

	@RequestMapping(value = "/admin/planmgmt/pdfconversion", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public void pdfConversionFile(@RequestParam(value = PlanMgmtConstants.PLAN_ID, required = false) String planId, HttpServletRequest request, HttpServletResponse response){

		if (planId != null) {
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=\""+ "Review-Confirm.pdf" + "\"");
			Document document = new Document();
			try {
				
					PdfWriter.getInstance(document, response.getOutputStream());
				document.open();

				PdfPTable table = new PdfPTable(PlanMgmtConstants.PDF_PAGE_SIZE);
				table.setWidthPercentage(PlanMgmtConstants.PDF_TABLE_SIZE);
				
				table.addCell("Issuer Name");
				table.addCell(plan.getIssuer().getName());

				table.addCell("Plan Name");
				table.addCell(plan.getName());

				table.addCell("Plan Number");
				table.addCell(plan.getIssuerPlanNumber());

				table.addCell("Provider Network");
				table.addCell(plan.getNetwork().getName());

				table.addCell("Plan Start Date");
				table.addCell(plan.getStartDate().toString());

				table.addCell("Plan End Date");
				table.addCell(plan.getEndDate().toString());

				table.addCell("Maximum Enrollment");

				if (plan.getInsuranceType().equals(PlanMgmtService.HEALTH)) {
					table.addCell("Acturial Value certificate");
					table.addCell(planMgmtService.getActualFileName(plan.getPlanHealth().getActurialValueCertificate()));

					table.addCell("Plan Benefit Data");
					table.addCell(planMgmtService.getActualFileName(plan.getPlanHealth().getBenefitFile()));

					table.addCell("Plan Rate by Zip-code");
					table.addCell(planMgmtService.getActualFileName(plan.getPlanHealth().getRateFile()));
				} else if (plan.getInsuranceType().equals(PlanMgmtService.DENTAL)) {
					table.addCell("Acturial Value certificate");
					table.addCell(planMgmtService.getActualFileName(plan.getPlanDental().getActurialValueCertificate()));

					table.addCell("Plan Benefit Data");
					table.addCell(planMgmtService.getActualFileName(plan.getPlanDental().getBenefitFile()));

					table.addCell("Plan Rate by Zip-code");
					table.addCell(planMgmtService.getActualFileName(plan.getPlanDental().getRateFile()));
				}

				table.addCell("Provider Network Type");
				table.addCell(plan.getNetworkType());

				document.add(table);
				document.close();
			}catch(Exception e){
	        	throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.PDF_CONVERSION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
	        }

		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqhphistory/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQHPHistory(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			loadPlanToDisplay(model,planId);
			List<Map<String, Object>> plansData = planMgmtService.loadPlanHistory(planId);
			model.addAttribute(PlanMgmtConstants.DATA, plansData);
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "View QHP History");
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId.toString());
			model.addAttribute(PlanMgmtConstants.PAGE_SIZE_VAR, GhixConstants.PAGE_SIZE);
			setExchangeTypeInModel(model);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_HISTORY.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/viewqhphistory";
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqhpcertification/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQHPCertification(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			loadPlanToDisplay(model,planId);
			List<Map<String, Object>> testData = planMgmtService.loadPlanCertificationHistory(planId, PlanMgmtConstants.ADMIN_ROLE);
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "View QHP Certification");
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId.toString());
			model.addAttribute(PlanMgmtConstants.DATA, testData);
			model.addAttribute(PlanMgmtConstants.PAGE_SIZE_VAR, GhixConstants.PAGE_SIZE);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_CERTIFICATION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/viewqhpcertification";
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhpcertification/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.UPDATE_CERTIFICATION_STATUS)
	public String editQHPCertification(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		String decertificationEffiectiveDate = null;
		Long enrollmentCountForPlanId = 0L;
		String showSaveButton = PlanMgmtConstants.TRUE_STRING;
		String deCertificationEffDate = null;
		try {
		model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Edit QHP Certification");
		decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
		Plan plan = planMgmtService.getPlan(Integer.parseInt(decryptedPlanId));
		
		if (plan != null) {
			if(null != plan.getDeCertificationEffDate()){
				deCertificationEffDate = String.valueOf(plan.getDeCertificationEffDate());
			}

			showSaveButton = planMgmtService.showSaveButtonForUpdate(plan.getStatus(), deCertificationEffDate);
			model.addAttribute("showSaveButton", showSaveButton);
			model.addAttribute("currentStatus", plan.getStatus());
			model.addAttribute("issuerId", plan.getIssuer().getId());
			model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
			model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
			model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, plan.getIssuerPlanNumber());
			model.addAttribute(PlanMgmtConstants.ISSUER_NAME, plan.getIssuer().getName());
			
			model.addAttribute("qhpcertiStatusList", plan.getPlanStatuses(plan.getStatus()));
			model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
			if(null != plan.getEnrollmentEndDate()){
				model.addAttribute("enrollmentEndDate", DateUtil.dateToString(plan.getEnrollmentEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));
			}
			
			Date enrollmentEndDate = planMgmtUtils.getEnrollmentEndDate();
			Date decertificationDate = planMgmtUtils.getDeCertificationDate(enrollmentEndDate);
			setExchangeTypeInModel(model);
			String displayProviderNetwork = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_VIEWQHPDETAILS_DISPLAYPROVIDERNETWORKTAB);
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("displayProviderNetwork_edit: "+ SecurityUtil.sanitizeForLogging(String.valueOf(displayProviderNetwork)));
			}
			String displayAction = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_VIEWQHPDETAILS_DISPLAYACTION);
			String terminateEnrollmentConfiguration = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.TERMINATE_ENROLLMENT_FEATURE);
			if(null != plan.getDeCertificationEffDate()){
				decertificationEffiectiveDate = DateUtil.dateToString(plan.getDeCertificationEffDate(), GhixConstants.REQUIRED_DATE_FORMAT);
			}
			
			/*if(StringUtils.isNotEmpty(terminateEnrollmentConfiguration) && PlanMgmtConstants.ON.equalsIgnoreCase(terminateEnrollmentConfiguration)){
				if(PlanMgmtConstants.ZERO != plan.getId() && PlanMgmtConstants.ZERO != plan.getApplicableYear() && null != decertificationEffiectiveDate){
					enrollmentCountForPlanId = planMgmtRestUtil.getNumberOfEnrollmentForPlanId(plan.getIssuerPlanNumber(), plan.getApplicableYear(), decertificationEffiectiveDate);
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("enrollmentCountForPlanId: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentCountForPlanId)));
					}
				}
			}*/
			
			model.addAttribute(PlanMgmtConstants.PLAN, plan);
			model.addAttribute("issuerStatus", plan.getIssuer().getCertificationStatus());
			model.addAttribute("qhpIssuerVerificationList", plan.getIssuerVerificationList());
			model.addAttribute("certiSuppDoc", planMgmtService.getActualFileName(plan.getSupportFile()));
			model.addAttribute(PlanMgmtConstants.DISPLAYACTION, displayAction);
			model.addAttribute(PlanMgmtConstants.DISPLAYPROVIDERNETWORK, displayProviderNetwork);
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("displayAction_edit: " + SecurityUtil.sanitizeForLogging(String.valueOf(displayAction)));
			}
			model.addAttribute("sysDate", decertificationDate);
			model.addAttribute("enrollmentEndSysDate", DateUtil.dateToString(enrollmentEndDate, GhixConstants.REQUIRED_DATE_FORMAT));
			model.addAttribute("enrollmentCountForPlanId", enrollmentCountForPlanId);
			model.addAttribute("terminateEnrollmentStatus", (null != plan.getTerminateEnrollmentStatus() ? plan.getTerminateEnrollmentStatus() : PlanMgmtConstants.EMPTY_STRING));
			model.addAttribute("showTerminateEnrollment", terminateEnrollmentConfiguration);
		}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_CERTIFICATION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/editqhpcertification";
	}
	
	@RequestMapping(value = "/admin/planmgmt/undoDecertification",method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize(PlanMgmtConstants.UPDATE_CERTIFICATION_STATUS)
	@ResponseBody
	public String undoDecertification(@RequestParam(value = "planId", required = false) String planId){
		LOGGER.info(" undoDecertification started ");
		AccountUser user = null;
		try{
			user = userService.getLoggedInUser();
		if(null != planId){
		Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
		plan.setDeCertificationEffDate(null);
		plan.setEnrollmentEndDate(null);
		planMgmtService.undoDecertification(plan);
		}
		}catch(Exception e){
			LOGGER.error("Exception occured while undo Decertification: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_CERTIFICATION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), user);
			return PlanMgmtConstants.UNDO_DECERTIFICATION_FAILED;
		}
		return PlanMgmtConstants.UNDO_DECERTIFICATION;
	}
	/*
	 * @Suneel: Secured the method with permission
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/admin/planmgmt/updateqhpcertification", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.UPDATE_CERTIFICATION_STATUS)
	public String updateQHPCertification(@ModelAttribute(PlanMgmtConstants.PLAN) Plan planData, Model model, @RequestParam(PlanMgmtConstants.PLAN_ID) String planId, 
			@RequestParam(PlanMgmtConstants.STATUS) String status, @RequestParam("hdnCertSuppDoc") String certSuppDoc, @RequestParam(value = PlanMgmtConstants.COMMENTS, required = false) String comments, 
			@RequestParam(value = PlanMgmtConstants.ISSUER_VERIFICATION, required = false) String issuerVerificationStatus ,@RequestParam(value="deCertificationEffDateUI",required=false) String deCertificationDate, 
			@RequestParam(value="enrollmentEndDateUI",required=false) String enrollmentEndDate, RedirectAttributes redirectAttrs) {
		String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(planId);
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		String sendNotificationToBroker = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.SEND_NOTIFICATION_TO_BROKER);
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
			if(StringUtils.isNotBlank(deCertificationDate)){
				Date deCertiyDate = formatter.parse(deCertificationDate);
				if(deCertiyDate.before(new TSDate())){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
				planData.setDeCertificationEffDate(deCertiyDate);
			}
			if(StringUtils.isNotBlank(enrollmentEndDate)){
				Date enrollEndDate = formatter.parse(enrollmentEndDate);
				if(enrollEndDate.before(new TSDate())){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				}
				planData.setEnrollmentEndDate(enrollEndDate);
			}
			//server side validation
			if(!Plan.PlanStatus.DECERTIFIED.toString().equals(status)){
			 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			 Set<ConstraintViolation<Plan>> violations = validator.validate(planData, Plan.EditCertificationStatus.class);
			 if(violations != null && !violations.isEmpty()){
				//throw exception in case client side validation breached
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			}else{
				//server side validation
				 Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				 Set<ConstraintViolation<Plan>> violations = validator.validate(planData, Plan.DeCertificationStatus.class);
				 if(violations != null && !violations.isEmpty()){
					//throw exception in case client side validation breached
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			}
			Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
			if(Plan.PlanStatus.DECERTIFIED.toString().equalsIgnoreCase(status) && (Plan.EnrollmentAvail.AVAILABLE.toString().equalsIgnoreCase(plan.getEnrollmentAvail()) ||
					Plan.EnrollmentAvail.DEPENDENTSONLY.toString().equalsIgnoreCase(plan.getEnrollmentAvail()))){
				redirectAttrs.addFlashAttribute("decertificationError", PlanMgmtConstants.TRUE_STRING);
				return "redirect:/admin/planmgmt/editqhpcertification/" + encryptedPlanId;
			}
			
			//String notifyToIssuer = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.NOTIFYTOISSUER);
			AccountUser user = null;
			user = userService.getLoggedInUser();
			String oldStatus = plan.getStatus();
			planMgmtService.updatePlanStatus(Integer.parseInt(planId), user, status, certSuppDoc, comments, planData.getDeCertificationEffDate(), planData.getEnrollmentEndDate(), issuerVerificationStatus);
			//boolean isEmailSent = false;
			
			// when plan status change from LOADED to CERTIFIED then send notification to issuer representative except state CA 
			/*if (Plan.PlanStatus.LOADED.toString().equalsIgnoreCase(oldStatus) && Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(status) && !stateCode.equalsIgnoreCase("CA") ) {
				if (exchangeType.equalsIgnoreCase("PHIX") && notifyToIssuer.equalsIgnoreCase("true")) {
					sendNotificationEmail(planMgmtService.getPlan(Integer.parseInt(planId)));
				} else if (exchangeType.equalsIgnoreCase("PHIX") && notifyToIssuer.equalsIgnoreCase("false")) {
					LOGGER.info("Notifier will not send a mail, since notifiy to issuer is false.");
				} else {
					sendNotificationEmailToIssuerRepresentative(planMgmtService.getPlan(Integer.parseInt(planId)), Plan.PlanStatus.CERTIFIED.toString());
					isEmailSent = true;
				}
			}*/
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("sendNotificationToBroker configuration: " + SecurityUtil.sanitizeForLogging(String.valueOf(sendNotificationToBroker)));
			}	
				
			// if send Notification To Broker is ON and state exchange is ID, then send notification to Brokers 
			if(PlanMgmtConstants.ON.equalsIgnoreCase(sendNotificationToBroker) && !stateCode.equalsIgnoreCase(PlanMgmtConstants.STATECODEFORCA)){
				// check if the current date is between plan start date and end date 
				Date currentDate = new TSDate();
				if((currentDate.equals(plan.getEndDate()) || currentDate.equals(plan.getStartDate()) ) || (currentDate.before(plan.getEndDate()) && currentDate.after(plan.getStartDate()) ) ){
					Integer conditionalCaseValForStatus = checkTriggerEmailConditionsforCertificationStatus(oldStatus, status, plan.getEnrollmentAvail());
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("conditionalCaseValForStatus : " + SecurityUtil.sanitizeForLogging(String.valueOf(conditionalCaseValForStatus)));
					}	
					if(conditionalCaseValForStatus > PlanMgmtConstants.ZERO){
						List<BrokerContactDTO> brokersList = new ArrayList<BrokerContactDTO>();
						String brokerResponse = restTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.GET_LISTOF_CERTIFIED_BROKERS, null, String.class);
						Type type = new TypeToken<List<BrokerContactDTO>>() {}.getType();
						
					    if(StringUtils.isNotBlank(brokerResponse)){
					    	brokersList = platformGson.fromJson(brokerResponse, type);
					    }				
					    if(LOGGER.isDebugEnabled()){
							LOGGER.debug("# of brokers recieved through API : " + brokersList.size());
						}
						
						if(brokersList != null && brokersList.size() > PlanMgmtConstants.ZERO){
							if(conditionalCaseValForStatus.equals(PlanMgmtConstants.ONE)){
								sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(planId)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_NONCERTIFIED_OR_CERTIFIED_OR_INCOMPLETE_AND_NOTAVAILABLE);
							}else if(conditionalCaseValForStatus.equals(PlanMgmtConstants.TWO)){
								sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(planId)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_CERTIFIED_AND_AVAILABLE);	
							}
						}
					}
				}
			}
			
			// send notification to issuer rep on plan certification only for ID state exchange
			 if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_ID) && Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(status)){
				planMgmtService.sendNotificationEmailToIssuerRepresentative(planMgmtService.getPlan(Integer.parseInt(planId)), Plan.PlanStatus.CERTIFIED.toString());
			}
	
			// Code changes for HIX-5252
			if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_ID) && Plan.PlanStatus.DECERTIFIED.toString().equalsIgnoreCase(status)){
				planMgmtService.sendNotificationEmailToIssuerRepresentative(planMgmtService.getPlan(Integer.parseInt(planId)), Plan.PlanStatus.DECERTIFIED.toString());	
			}
			// End changes
					
			/*if (status.equals(Plan.PlanStatus.INCOMPLETE)) {
				UpdateExchangeWorkflowStatus updateExchangeWorkflowStatusRequest = new UpdateExchangeWorkflowStatus();
				updateExchangeWorkflowStatusRequest.setIssuerId(Integer.toString(plan.getIssuer().getId()));
				updateExchangeWorkflowStatusRequest.setExchangePlanId(Integer.toString(plan.getId()));
				updateExchangeWorkflowStatusRequest.setExchangeWorkflowStatus(String.valueOf(com.serff.planmanagementexchangeapi.common.model.pm.PlanStatus.RECEIVED));
				String verifyResponse = restTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.PLAN_STATUS_UPDATE_URL, updateExchangeWorkflowStatusRequest, String.class);
				try {
					UpdateExchangeWorkflowStatusResponse updateExchangeWorkflowStatusResponse = mapper.readValue(verifyResponse, UpdateExchangeWorkflowStatusResponse.class);
					LOGGER.debug(SecurityUtil.sanitizeForLogging(String.valueOf(updateExchangeWorkflowStatusResponse.getExchangeWorkflowStatus())));
				} catch (Exception e) {
					LOGGER.error("Error : ex " + e.getMessage(),e);
				}
			}*/
			
			// HIX-90008 : If plan certification status set as CERTIFIED OR INCOMPLETE then call syncPlanWithAHBX() service to sync plan data with AHBX
			if (status.equalsIgnoreCase(PlanStatus.CERTIFIED.toString()) || status.equalsIgnoreCase(PlanStatus.INCOMPLETE.toString()) ) {
				planMgmtService.syncPlanWithAHBX(Integer.parseInt(planId));
			}
			
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.UPDATE_QHP_CERTIFICATION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		
		return "redirect:/admin/planmgmt/viewqhpcertification/" + encryptedPlanId;
	}
	
	/**
	 * @param oldStatus
	 * @param currentPlanStatus
	 * @param currentEnrollmentAvail
	 * @return integer values 1 or 2
	 * 1 for CERTIFIED plan gets DECERTIFIED OR INCOMPLETE
	 * 2 for CERTIFIED plan gets AVAILABLE
	 */
	private Integer checkTriggerEmailConditionsforCertificationStatus(String oldStatus, String currentPlanStatus, String currentEnrollmentAvail){
		Integer conditionalCaseValForStatus = PlanMgmtConstants.ZERO;
		if(Plan.PlanStatus.CERTIFIED.toString().equals(oldStatus) && (Plan.PlanStatus.DECERTIFIED.toString().equals(currentPlanStatus)  || Plan.PlanStatus.INCOMPLETE.toString().equals(currentPlanStatus))){
			conditionalCaseValForStatus = PlanMgmtConstants.ONE;
		}else if(Plan.PlanStatus.CERTIFIED.toString().equals(currentPlanStatus) && Plan.EnrollmentAvail.AVAILABLE.toString().equals(currentEnrollmentAvail)){
			conditionalCaseValForStatus = PlanMgmtConstants.TWO;
		}		
		return conditionalCaseValForStatus;
	}

	/**mail sending functionality to be committed later
	 * 
	 * */
	private void sendNotificationEmailToBrokers(Plan plan, List<BrokerContactDTO> brokers, String planInformationUpdate) {
		HashMap<String, Object> notificationContext = new HashMap<String, Object>();
		Notice noticeObj = null;
		StringBuilder emailId = new StringBuilder();
		StringBuilder tempString = new StringBuilder();
		Issuer issuerObj = issuerService.getIssuerById(plan.getIssuer().getId());
		notificationContext.put("ISSUER", issuerObj);
		notificationContext.put(PlanMgmtConstants.PLANINFORMATION_UPDATE,planInformationUpdate);
		if(null != brokers){
			for(BrokerContactDTO brokerContactDTO : brokers){
				tempString.append(brokerContactDTO.getEmail());
				tempString.append(";");
				if(tempString.length() > PlanMgmtConstants.BCC_ADDRESS_STRING_MAX_LENGTH){
					tempString.append(PlanMgmtConstants.DOLLARSIGN);
					emailId.append(tempString);
					tempString = new StringBuilder();
				}
				
			}
			if(!StringUtils.isEmpty(tempString.toString())){
				emailId.append(tempString);
				tempString = null;
			}
			LOGGER.debug("broker mail ids: " + SecurityUtil.sanitizeForLogging(String.valueOf(emailId)));
			notificationContext.put("PLAN",plan);
			Map<String, String> emailHeaders = null;
			Map<String, String> tokens = null;
			if(null != emailId){
				String []splitBrokerIds = emailId.toString().split(PlanMgmtConstants.DOLLAR);
				for(int i=0; i<splitBrokerIds.length; i++){
					String recepients = splitBrokerIds[i];
					notificationContext.put(PlanMgmtConstants.RECIPIENT, recepients);
					tokens = brokerNotificationEmail.getTokens(notificationContext);
					emailHeaders = brokerNotificationEmail.getEmailData(notificationContext);
					try {
						noticeObj = brokerNotificationEmail.generateEmail(brokerNotificationEmail.getClass().getSimpleName(),null,emailHeaders,tokens);
						Notification notificationObj = brokerNotificationEmail.generateNotification(noticeObj);
						brokerNotificationEmail.sendEmailToMultipleRecipient(notificationObj, noticeObj);
					} catch (Exception e) {
						throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.SEND_NOTIFICATION_EAMIL_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
					}
				}
			}
		}
		
	}
	/*
	 * Method for sending email to notification to Issuer and Issuer Representative when their plans are certified.
	 * Changes for HIX-17561
	 */
	
/*
	private void sendNotificationEmail(Plan plan) throws NotificationTypeNotFound, InvalidUserException, NoticeServiceException {
		Issuer issuerObj = issuerService.getIssuerById(plan.getIssuer().getId());
		AccountUser accountUser = new AccountUser();
		Integer userId = representativeRepository.getUserIdFromIssuer(issuerObj.getId());
		
		Map<String,Object> data = new HashMap<String,Object>();
		if (null != userId && userId != 0) {
			accountUser = userService.findById(userId);
		}
		if (accountUser != null && !StringUtils.isBlank(accountUser.getEmail()) && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(accountUser.getStatus())) {				
			issuerPlanCertificationEmail.setPlanObj(plan);
			issuerPlanCertificationEmail.setIssuerObj(issuerObj);
			issuerPlanCertificationEmail.setUser(accountUser);
			data = issuerPlanCertificationEmail.getSingleData();
			String fileName = PlanMgmtConstants.ISSUER_PLAN_CERTIFICATION + TimeShifterUtil.currentTimeMillis() + PlanMgmtConstants.PDF_EXT;
			noticeService.createNotice(PlanMgmtConstants.PLAN_CERTIFIED_NOTIFICATION,  GhixLanguage.US_EN, data, PlanMgmtConstants.PLAN_ECM_PDF_FILE_PATH, fileName, accountUser, null, GhixNoticeCommunicationMethod.Email);
			LOGGER.info("Message stored in inbox successfully");

		} else {
			LOGGER.info("Issuer email address is blank or null");
		}
}*/

	@RequestMapping(value = "/admin/planmgmt/uploadqhpcertsuppdoc", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.UPDATE_CERTIFICATION_STATUS)
	@ResponseBody
	public String uploadQHPSupportingDocument(Model model, @RequestParam(value = "certSuppDoc", required = false) MultipartFile certSuppDoc) {
		String returnString = null;
		try {
			String modifiedFileName = FilenameUtils.getBaseName(certSuppDoc.getOriginalFilename()) + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis()+ GhixConstants.DOT+ FilenameUtils.getExtension(certSuppDoc.getOriginalFilename());
			String filePath = uploadPath + File.separator+ PLAN_SUPPORT_DOC_FOLDER;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				if (providerNetworkService.uploadFile(filePath, certSuppDoc, modifiedFileName)) {
					returnString = certSuppDoc.getOriginalFilename() + "|"+ modifiedFileName;
					LOGGER.info("File uploaded successfully");
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		} catch (Exception e) {
			returnString = "";
			LOGGER.error("Exception occured while uploading in uploadQHPSupportingDocument: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.UPLOAD_QHP_SUPPORTING_DOCUMENT_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			//return PlanMgmtConstants.ERROR;
		}
		if(null != returnString){
			return returnString;
		}else{
			return PlanMgmtConstants.EMPTY_STRING;
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqhpenrollmentavail/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQHPEnrollmentAvail(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			Integer planId = Integer.parseInt(decryptedPlanId);
			loadPlanToDisplay(model,planId);
			List<Map<String, Object>> logData = planMgmtService.loadPlanEnrollmentHistory(planId, PlanMgmtConstants.ADMIN_ROLE);
			String currStatus = null;
			if (logData != null && !logData.isEmpty()) {
				currStatus = (String) logData.get(0).get(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
			}
			model.addAttribute("currStatus", currStatus);
			model.addAttribute(PlanMgmtConstants.DATA, logData);
			model.addAttribute(PlanMgmtConstants.PAGE_SIZE_VAR, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "View QHP Enrollment Availlability");
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId.toString());
			setExchangeTypeInModel(model);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_ENROLLMENT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/viewqhpenrollavail";
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhpenrollmentavail/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQHPEnrollmentAvail(Model model, HttpServletRequest request, @PathVariable("encPlanId") String encPlanId) {
		
		model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
		setExchangeTypeInModel(model);
		String decryptedPlanId = null;
		Integer planid = null;
		String deCertificationEffDate = null;
		String showSaveButton = PlanMgmtConstants.TRUE_STRING;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planid = Integer.parseInt(decryptedPlanId);
			Plan plan = planMgmtService.getPlan(planid);
			if (plan != null) {
				if(null != plan.getDeCertificationEffDate()){
					deCertificationEffDate = String.valueOf(plan.getDeCertificationEffDate());
				}
				
				showSaveButton = planMgmtService.showSaveButtonForUpdate(plan.getStatus(), deCertificationEffDate);
				model.addAttribute("showSaveButton", showSaveButton);
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
				model.addAttribute("enrollmentAvailList", plan.getEnrollmentAvailList());
				model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Edit QHP Enrollment Availlability");

				SimpleDateFormat ft = new SimpleDateFormat("MMddyyyy");
				model.addAttribute(PLAN_DIS_DATE, ft.format(plan.getStartDate()));
				model.addAttribute(PLAN_DIS_DATE, ft.format(plan.getEndDate()));

				model.addAttribute(PlanMgmtConstants.PLAN, plan);
				model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				if(plan.getFutureErlAvailEffDate()!=null){
					model.addAttribute(PlanMgmtConstants.FUTURE_DATE, DateUtil.dateToString(plan.getFutureErlAvailEffDate(), GhixConstants.REQUIRED_DATE_FORMAT));
				}
				model.addAttribute("sysDate", new TSDate());
				
				return "planmgmt/editqhpenrollavail";
			} else {
				return MANAGE_QHP_PATH;
			}

		}catch(Exception e){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_ENROLLMENT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "admin/planmgmt/editqhpenrollmentavail", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String submitQHPEnrollmentAvail(Model model, @RequestParam(value = PlanMgmtConstants.COMMENTS, required = false) String comments, HttpServletRequest request) {
		Integer userId = null;
		String userName = null;
		try {
			userId = userService.getLoggedInUser().getId();
			userName = userService.getLoggedInUser().getUserName();
			String id = request.getParameter("id");
			String effectiveDate = request.getParameter("effectiveDate");
			String enrollmentAvail = request.getParameter("enrollmentavail");
			if(effectiveDate.isEmpty() || enrollmentAvail.isEmpty() || !enrollmentAvail.matches(PlanMgmtConstants.ENROLLMENTAVAILABILITY_PATTERN)){
				throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			}
			
			Plan oldPlanInfo = planMgmtService.getPlan(Integer.parseInt(id));
			String oldAvailability = oldPlanInfo.getEnrollmentAvail();
			LOGGER.debug("oldAvailability: " + SecurityUtil.sanitizeForLogging(String.valueOf(oldAvailability)));
			Date futureEnrlAailEffDate = oldPlanInfo.getFutureErlAvailEffDate();
			SimpleDateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_MMDDYYYY);
			Date effDate = dateFormat.parse(effectiveDate);
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			String sendNotificationToBroker = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.SEND_NOTIFICATION_TO_BROKER);
			if(null != futureEnrlAailEffDate && effDate.after(futureEnrlAailEffDate)){
				model.addAttribute("futureDateValidationError", PlanMgmtConstants.TRUE_STRING);
				String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(id);
				return "redirect:/admin/planmgmt/editqhpenrollmentavail/" + encryptedPlanId;
			}else{
				Plan plan = planMgmtService.getPlan(Integer.parseInt(id));
				if (planMgmtService.updateEnrollmentAvail(id, userId, userName, effectiveDate, enrollmentAvail, comments)) {
					
					// send enrollment notification to issuer rep only for ID state exchange
					if(plan.getStatus().equalsIgnoreCase(CERTIFIED) && stateCode.equalsIgnoreCase("ID")){
						enrollmentAvailabilityEmailUtil.sendNotificationEmailToIssuerRepresentativeForEnrollment(plan);
					}
					
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("sendNotificationToBroker configuration: " + SecurityUtil.sanitizeForLogging(String.valueOf(sendNotificationToBroker)));
					}
					
					if(PlanMgmtConstants.ON.equalsIgnoreCase(sendNotificationToBroker) && !stateCode.equalsIgnoreCase(PlanMgmtConstants.STATECODEFORCA)){
						// check if the current date is between plan start date and end date 
						Date currentDate = new TSDate();
						if((currentDate.equals(plan.getEndDate()) || currentDate.equals(plan.getStartDate()) ) || (currentDate.before(plan.getEndDate()) && currentDate.after(plan.getStartDate()) ) ){
							Integer conditionalCaseValForAvailability = checkTriggerEmailConditionsforAvailability(oldAvailability ,enrollmentAvail, plan.getStatus());
							if(LOGGER.isDebugEnabled()){
								LOGGER.debug("conditionalCaseValForAvailability : " + SecurityUtil.sanitizeForLogging(String.valueOf(conditionalCaseValForAvailability)));
							}
								if(conditionalCaseValForAvailability > PlanMgmtConstants.ZERO){
									List<BrokerContactDTO> brokersList = new ArrayList<BrokerContactDTO>();
									String brokerResponse = restTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.GET_LISTOF_CERTIFIED_BROKERS, null, String.class);
									Type type = new TypeToken<List<BrokerContactDTO>>() {}.getType();
									
									if(StringUtils.isNotBlank(brokerResponse)){
										brokersList = platformGson.fromJson(brokerResponse, type);
								    }
									if(LOGGER.isDebugEnabled()){
										LOGGER.debug("# of brokers recieved through API : " + brokersList.size());
									}
									
									if(brokersList != null && brokersList.size() > PlanMgmtConstants.ZERO){
										//sendNotificationEmailToBrokers(plan, brokersList,PlanMgmtConstants.PLANINFORMATION_UPDATE_VALUE1);
										if(conditionalCaseValForAvailability.equals(PlanMgmtConstants.ONE)){
											sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(id)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_NONCERTIFIED_OR_CERTIFIED_OR_INCOMPLETE_AND_NOTAVAILABLE);
											
										}else if(conditionalCaseValForAvailability.equals(PlanMgmtConstants.TWO)){
											sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(id)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_CERTIFIED_AND_DEPENDENTSONLY);
											
										}else if(conditionalCaseValForAvailability.equals(PlanMgmtConstants.THREE)){
											sendNotificationEmailToBrokers(planMgmtService.getPlan(Integer.parseInt(id)), brokersList,PlanMgmtConstants.PLANINFORMATION_FOR_CERTIFIED_AND_AVAILABLE);
										}
								}	
							}					
						}
					}
					String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(id);
					return "redirect:/admin/planmgmt/viewqhpenrollmentavail/" + encryptedPlanId;
				} else {
					return MANAGE_QHP_PATH;
				}
			}
		}catch(Exception e){
			LOGGER.error(e.getMessage(), e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, PlanMgmtConstants.MODULE_NAME, null);
		}
	}
	
	/**
	 * @param oldEnrollmentAvail
	 * @param currentEnrollmentAvail
	 * @param currentPlanStatus
	 * @return Integer 1 or 2 or 3
	 * 1 for CERTIFIED and AVAILABLE plan get NOTAVAILABLE
	 * 2 for CERTIFIED and AVAILABLE plan get DEPENDENTSONLY
	 * 3 for CERTIFIED plan get AVAILABLE
	 */
	private Integer checkTriggerEmailConditionsforAvailability(String oldEnrollmentAvail, String currentEnrollmentAvail, String currentPlanStatus){
		Integer conditionalCaseValForStatus = PlanMgmtConstants.ZERO;
		if(Plan.PlanStatus.CERTIFIED.toString().equals(currentPlanStatus) && Plan.EnrollmentAvail.AVAILABLE.toString().equals(oldEnrollmentAvail) 
																		&& Plan.EnrollmentAvail.NOTAVAILABLE.toString().equals(currentEnrollmentAvail)){
			conditionalCaseValForStatus = PlanMgmtConstants.ONE;
			
		}else if(Plan.PlanStatus.CERTIFIED.toString().equals(currentPlanStatus) && Plan.EnrollmentAvail.AVAILABLE.toString().equals(oldEnrollmentAvail) 
																			&& Plan.EnrollmentAvail.DEPENDENTSONLY.toString().equals(currentEnrollmentAvail)){
			conditionalCaseValForStatus = PlanMgmtConstants.TWO;
			
		}else if(Plan.PlanStatus.CERTIFIED.toString().equals(currentPlanStatus) && Plan.EnrollmentAvail.AVAILABLE.toString().equals(currentEnrollmentAvail)){
			conditionalCaseValForStatus = PlanMgmtConstants.THREE;
		}
		return conditionalCaseValForStatus;
	}
		
	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhprates/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQHPRates(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			Date sysDate = new TSDate();
			Plan plan = planMgmtService.getPlan(planId);
			model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
			model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Edit QHP Rates");
			model.addAttribute(PlanMgmtConstants.ISSUER_NAME, plan.getIssuer().getName());
			model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, plan.getIssuerPlanNumber());
			model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(sysDate, GhixConstants.REQUIRED_DATE_FORMAT));
			model.addAttribute("ratesFileName", planMgmtService.getActualFileName(plan.getPlanHealth().getRateFile()));
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_RATE.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/editqhprates";
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhprates", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQHPRates(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan planModel, BindingResult result, Model model, @RequestParam(value = PlanMgmtConstants.RATE_FILE, required = false) String ratesFile, @RequestParam(value = "effectiveDate", required = false) Date effectiveDate, @RequestParam(value = PlanMgmtConstants.COMMENTS, required = false) String comments) {
		try{
		if (ratesFile != null && ratesFile.length() > 0) {
			Plan plan = planMgmtService.getPlan(planModel.getId());
			LOGGER.debug("Rates file " + SecurityUtil.sanitizeForLogging(String.valueOf(ratesFile)));
				planMgmtService.savePlanRatesEditMode(plan, uploadPath+ File.separator + RATES_FOLDER + File.separator + ratesFile, effectiveDate, comments);
		}
		}catch(Exception e){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_RATE_POST.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(planModel.getId()));
		return "redirect:/admin/planmgmt/viewqhprates/" + encryptedPlanId;
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqhprates/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQHPRates(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			loadPlanToDisplay(model,planId);
			List<Map<String, Object>> logData = planMgmtService.loadPlanRateGroupByEffectiveDate(planId, PlanMgmtConstants.ADMIN_ROLE);
			String displayEditButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQHP_PLANRATES_DISPLAYEDITBUTTON);
			model.addAttribute("rates", logData);
			model.addAttribute("recordSize", logData.size());
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId.toString());
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "View QHP Rates");
			model.addAttribute(PlanMgmtConstants.PAGE_SIZE_VAR, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.DISPLAYEDITBUTTON, displayEditButton);
			setExchangeTypeInModel(model);
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_RATE_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/viewqhprates";
	}

	/*
	 * @Suneel: Secured the method with permissio
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhpbenefits/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQHPBenefits(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			if (planId == null) {
				return "redirect:/admin/planmgmt/viewqhpbenefits";
			}
			Plan plan = planMgmtService.getPlan(planId);
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId);
			model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Edit QHP Benefits");
			model.addAttribute(PlanMgmtConstants.ISSUER_NAME, plan.getIssuer().getName());
			model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, plan.getIssuerPlanNumber());
			model.addAttribute("planStatus", plan.getStatus());
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_BENEFIT_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "planmgmt/editqhpbenefits";
	}

	/*
	 * @Suneel: Secured the method with permissio
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhpbenefits", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQHPBenefitsUpload(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan planModel, BindingResult result, Model model, @RequestParam(value = PlanMgmtConstants.BENEFIT_FILE, required = false) String benefitsFile, @RequestParam(value = "brochureFile", required = false) String brochureFile, @RequestParam(value = "benefitStartDate", required = false) String benefitStartDate, @RequestParam(value = PlanMgmtConstants.COMMENTS, required = false) String comments, RedirectAttributes redirectAttrs) {
		String encryptedPlanId = null;
		try{
		// Create planObj only if either benefit file or brochure file upload
		if ((StringUtils.isNotBlank(benefitsFile)) || (StringUtils.isNotBlank(brochureFile))) {
			Plan planObj = planMgmtService.getPlan(planModel.getId());
			encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(planModel.getId()));
			
			if (StringUtils.isNotBlank(benefitsFile)) {
				try {
					String filePath = uploadPath + File.separator + BENEFITS_FOLDER + File.separator+ benefitsFile;
					boolean isValidPath = planMgmtUtils.isValidPath(filePath);
					if(isValidPath){
						planMgmtService.editPlanBenefits(planObj, filePath, benefitStartDate, comments);
					}else{
						LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
					}
				} catch (Exception e) {
					LOGGER.error(ERROR_UPLOAD_BENEFITS,e);
					redirectAttrs.addFlashAttribute("benefitsFileError", "<label class='error'><span> <em class='excl'>!</em>Error while uploading the file. Please try again.</span></label>");
					return "redirect:/admin/planmgmt/editqhpbenefits/"+ encryptedPlanId;
				}
			}
			if (StringUtils.isNotBlank(brochureFile)) {
				try {
					String filePath = uploadPath+ File.separator + BROCHURE_FOLDER + File.separator+ brochureFile;
					boolean isValidPath = planMgmtUtils.isValidPath(filePath);
					if(isValidPath){
						planMgmtService.uploadPlanBrochure(planObj, filePath);
					}else{
						LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
					}
				} catch (Exception e) {
					LOGGER.error("Error while uploadng the brochure.",e);
					redirectAttrs.addFlashAttribute("brochureFileError", "<label class='error'><span> <em class='excl'>!</em>Error while uploading the file. Please try again.</span></label>");
					return "redirect:/admin/planmgmt/editqhpbenefits/"+ encryptedPlanId;
				}
			}
		}
		redirectAttrs.addFlashAttribute(PlanMgmtConstants.PLAN_ID, planModel.getId());
		return "redirect:/admin/planmgmt/viewqhpbenefits/" + encryptedPlanId;
		}catch(Exception e){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_BENEFIT_POST.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permissio
	 * @Suneel: Commented the check for Admin
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqhpbenefits/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQHPBenefits(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			loadPlanToDisplay(model,planId);
			List<Map<String, Object>> plansData = planMgmtService.loadPlanBenefitsHistory(planId, PlanMgmtConstants.ADMIN_ROLE);
			String displayEditButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQHP_PLANBENEFITS_DISPLAYEDITBUTTON);
			model.addAttribute(PlanMgmtConstants.DATA, plansData);
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId.toString());
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "View QHP Benefits");
			model.addAttribute(PlanMgmtConstants.PAGE_SIZE_VAR, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.DISPLAYEDITBUTTON, displayEditButton);
			setExchangeTypeInModel(model);
			model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			return "planmgmt/viewqhpbenefits";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_BENEFIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhpdetail/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQHPDetail(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			PlanHealth planHealthModel = new PlanHealth();
			Plan plan = loadPlanToDisplay(model,planId);
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId);
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Edit QHP Detail");
			model.addAttribute("qhpmarketList", plan.getMarketList());
			model.addAttribute("qhplevelList", plan.getLevelList());
			model.addAttribute("qhpHsaList", plan.getHsaList());
			model.addAttribute("ehbCoveredList", planHealthModel.getEHBCoveredList());
			return "planmgmt/editqhpdetail";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_DETAIL_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhpdetail", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQHPDetailPersist(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan plan, BindingResult result, Model model, RedirectAttributes redirectAttrs, @RequestParam(value = "planLevelReadOnly", required = false) String planLevelReadOnly, @RequestParam(value = "planEhbCoveredReadOnly", required = false) String planEhbCoveredReadOnly) {
		try {
			updatePlan(plan, redirectAttrs, planLevelReadOnly, planEhbCoveredReadOnly);
		model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Edit QHP Detail Post");
		model.addAttribute("qhpmarketList", plan.getMarketList());
		model.addAttribute("qhplevelList", plan.getLevelList());
		String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(plan.getId()));
		return "redirect://admin/planmgmt/viewqhpdetail/" + encryptedPlanId;
		}catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_DETAIL_POST.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqhpdetail/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQHPDetail(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try{
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			Formulary formulary = new Formulary();

			Plan plan = loadPlanToDisplay(model,planId);

			String displayEditButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQHP_PLANDETAILS_DISPLAYEDITBUTTON);
			String displayProviderNetwork = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_VIEWQHPDETAILS_DISPLAYPROVIDERNETWORKTAB);
			String displayAction = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_VIEWQHPDETAILS_DISPLAYACTION);

			if (StringUtils.isNotBlank(plan.getFormularlyId())) {
				formulary = iFormularyRepository.findById(Integer.parseInt(plan.getFormularlyId()));
			}
			
			AccountUser accountUser=userService.getLoggedInUser();
			if(accountUser.getUserPermissions() != null){
				if(accountUser.getUserPermissions().contains(PlanMgmtConstants.PERMISSION_EDIT_PLANS)){
					model.addAttribute(PlanMgmtConstants.HAS_EDIT_PERMISSIONS, true);
				} else {
					model.addAttribute(PlanMgmtConstants.HAS_EDIT_PERMISSIONS, false);
				}
			}
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("displayAction: " + SecurityUtil.sanitizeForLogging(String.valueOf(displayAction)));
				LOGGER.debug("displayProviderNetwork: " + SecurityUtil.sanitizeForLogging(String.valueOf(displayProviderNetwork)));
			}
			
			model.addAttribute(PlanMgmtConstants.DISPLAYPROVIDERNETWORK, displayProviderNetwork);
			model.addAttribute(PlanMgmtConstants.DISPLAYEDITBUTTON, displayEditButton);
			model.addAttribute(PlanMgmtConstants.DISPLAYACTION, displayAction);
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "View QHP Detail");
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId);
			model.addAttribute("formulary", formulary);
			model.addAttribute("levelList", plan.getLevelList());

			// HIX-31861 Code changes
			model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, "");
			model.addAttribute(PlanMgmtConstants.FUTURE_DATE, plan.getFutureErlAvailEffDate());
			model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));

			Date sysDate = new TSDate();
			if (plan.getFutureEnrollmentAvail() == null) {
				model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, "");
			} else if (plan.getFutureErlAvailEffDate() != null && plan.getFutureErlAvailEffDate().compareTo(sysDate) > 0) {
				if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.AVAILABLE.toString())) {
					model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.AVAILABLE);
				} else if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.NOTAVAILABLE.toString())) {
					model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.NOTAVAILABLE);
				} else if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.DEPENDENTSONLY.toString())) {
					model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.DEPENDENTSONLY);
				}
			}
			// HIX-31861 end changes

			setExchangeTypeInModel(model);
			return "planmgmt/viewqhpdetail";
		} catch (Exception e) {
		throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_DETAIL_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
	}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/viewqhpprovidernetwork/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String viewQHPProviderNetwork(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			loadPlanToDisplay(model,planId);
			List<Map<String, Object>> data = planMgmtService.loadProviderHistory(planId);
			String displayEditButton = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.PLAN_MANAGEQHP_PLANDETAILS_DISPLAYEDITBUTTON);

			model.addAttribute(PlanMgmtConstants.DATA, data);
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "View QHP Provider Network");
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planId.toString());
			model.addAttribute(PlanMgmtConstants.PAGE_SIZE_VAR, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.DISPLAYEDITBUTTON, displayEditButton);
			return "planmgmt/viewqhpprovidernetwork";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_PROVIDERNET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/editqhpprovidernetwork/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String editQHPProviderNetwork(Model model, @PathVariable("encPlanId") String encPlanId) {
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			int networkId = 0;
			Plan plan = planMgmtService.getPlan(planId);
			if (plan.getNetwork() != null) {
				networkId = plan.getNetwork().getId();
			}
			List<Network> networkList = providerNetworkService.getProviderNetworkByIssuerId(plan.getIssuer().getId());
			model.addAttribute(PlanMgmtConstants.ISSUER_NAME, plan.getIssuer().getName());
			model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
			model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
			model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, plan.getIssuerPlanNumber());
			model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Edit QHP Provider Network");
			model.addAttribute("issuerId", plan.getIssuer().getId());
			model.addAttribute("currentStatus", plan.getStatus());
			model.addAttribute("networkList", networkList);
			model.addAttribute("selNetwork", networkId);
			return "planmgmt/editqhpprovidernetwork";
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_QHP_PROVIDERNET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/updateqhpprovidernetwork", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public String updateQHPProviderNetwork(Model model, @RequestParam(value = PlanMgmtConstants.PLAN_ID, required = true) String planId, @RequestParam(value = "providerNetwork", required = false) String providerNetworkId) {
		try {
		if (providerNetworkId == null || providerNetworkId.isEmpty()) {
			return MANAGE_QHP_PATH;
		}
		planMgmtService.updateProvider(planId, providerNetworkId);
		String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(planId);
		return "redirect:/admin/planmgmt/viewqhpprovidernetwork/" + encryptedPlanId;
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.UPDATE_QHP_PROVIDERNET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	@RequestMapping(value = "/admin/planmgmt/getComment", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	@ResponseBody
	public String getCommentTextById(HttpServletRequest request) {
		String commentId = request.getParameter("commentId");
		String commentText = null;
		try {
			commentText = commentService.getCommentTextById(Integer.parseInt(commentId));
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching comments: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.GET_COMMENT_TEXT_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			//return PlanMgmtConstants.ERROR; 
		}
		return commentText;
	}

	@RequestMapping(value = "/admin/planmgmt/dowloadbenefits/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public ModelAndView downloadQhpBenefit(HttpServletResponse response, @PathVariable("encPlanId") String encPlanId) {
		FileInputStream fis = null;
		FileOutputStream outFile = null;
		
		try {

			String planId = null;

			try { 
				planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			}
			catch (Exception ex) {
				LOGGER.error("Unable to decript following Plan ID: " + encPlanId);
			}

			Plan plan =	null;
			if (org.apache.commons.lang3.StringUtils.isNumeric(planId)) {
				plan = planMgmtService.getPlan(Integer.valueOf(planId));
			}

			if (null == plan) {
				LOGGER.error("Invalid Plan ID.");
				return null;
			}

			List<String[]> planBenefitsList = null;
			int rows = 0;
			if (Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(plan.getInsuranceType())) {
				PlanHealth planHealth = iPlanHealthRepository.findOne(plan.getPlanHealth().getId());
				List<PlanHealthBenefit> plansData = planMgmtService.getPlanHelthBenefit(planHealth.getId());
				List<PlanHealthCost> planHealthCostData = planMgmtService.getPlanHealthCost(planHealth.getId());
				rows = planHealthCostData.size();
				String constname;
				String name;
				Map<String, String> healthBenefitMap = new HashMap<String, String>();
				for(PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()){
					 constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY)?pcE.name().replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY,""):pcE.name();name = pcE.getValue();
					if(name.contains(PlanMgmtConstants.PLANMGMT_HEALTHBENEFITS)){
						healthBenefitMap.put(constname,DynamicPropertiesUtil.getPropertyValue(name));
					}
				}
				planBenefitsList = PlanHealthMapper.convertObjectListToStringListFromMap(plansData,healthBenefitMap,plan,planHealthCostData);
			}
	
			if (Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(plan.getInsuranceType())) {
				PlanDental planDental = iPlanDentalRepository.findOne(plan.getPlanDental().getId());
				List<PlanDentalBenefit> plansData = planMgmtService.getPlanDentalBenefit(planDental.getId());
				List<PlanDentalCost> planDentalCostData = planMgmtService.getPlanDentalCost(planDental.getId());
				rows = planDentalCostData.size();
				String constname;
				String name;
				Map<String, String> dentalBenefitMap = new HashMap<String, String>();
				
				for(PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()){
					 constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY)?pcE.name().replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY,""):pcE.name();name = pcE.getValue();
					if(name.contains(PlanMgmtConstants.PLANMGMT_DENTALBENEFITS)){
						dentalBenefitMap.put(constname,DynamicPropertiesUtil.getPropertyValue(name));
					}
				}
				planBenefitsList = PlanHealthMapper.convertsQDPBenefitFromMap(plansData,dentalBenefitMap,plan,planDentalCostData);
			}
	
			if (planBenefitsList == null) {
				planBenefitsList = PlanHealthMapper.getFileHeader();
			}
			
			String fileName = plan.getIssuerPlanNumber() +".xls";

			String filePath = uploadPath + File.separator + fileName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				HSSFWorkbook workBook = ExcelGenerator.createBenefitsExcelSheet(planBenefitsList, rows);//createBenefitsExcelSheet(planBenefitsList);
				outFile = new FileOutputStream(filePath);
				workBook.write(outFile);
				outFile.close();
				
				response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
				response.setContentType("application/octet-stream");
				File file = new File(filePath);
				fis = new FileInputStream(file);			 
				FileCopyUtils.copy(fis, response.getOutputStream());
				if(file.exists()){
					file.delete();
				}
				fis.close();
				LOGGER.info("File downloaded successfully!!!");
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_PLAN_BENEFIT.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}finally{
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(outFile);
		}
		return null;
	}

	@RequestMapping(value = "/admin/planmgmt/downloadrates/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public ModelAndView downloadQHPRate(Model model, @PathVariable("encPlanId") String encPlanId, @RequestParam("effective_start_date") String effectiveStartDate, @RequestParam("effective_end_date") String effectiveEndDate, HttpServletResponse response) {
		FileInputStream fis = null;
		FileOutputStream outFile = null;
		
		try {

			String planId = null;

			try {
				planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			}
			catch (Exception ex) {
				LOGGER.error("Unable to decript following Plan ID: " + encPlanId);
			}

			Plan plan =	null;
			if (org.apache.commons.lang3.StringUtils.isNumeric(planId)) {
				plan = planMgmtService.getPlan(Integer.valueOf(planId));
			}

			if (null == plan) {
				LOGGER.error("Invalid Plan ID.");
				return null;
			}
			List<Object[]> rateList = null;
			rateList = planMgmtService.downloadPlanRate(Integer.valueOf(planId), effectiveStartDate, effectiveEndDate);
			String[] effectiveStartDateArr = effectiveStartDate.split(PlanMgmtConstants.SEPARATORAT);
			String[] startDatArr = effectiveStartDateArr[0].split(PlanMgmtConstants.HIGHFEN);
			String[] effectiveEndDateArr = effectiveEndDate.split(PlanMgmtConstants.SEPARATORAT);
			String[] endDatArr = effectiveEndDateArr[0].split(PlanMgmtConstants.HIGHFEN);
			String fileName = plan.getHiosProductId() + PlanMgmtConstants.PLANRATES +".xls";
			
			String filePath = uploadPath + File.separator + fileName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				HSSFWorkbook workBook = ExcelGenerator.createRateExcelSheet(rateList, plan,startDatArr,endDatArr);
				outFile = new FileOutputStream(filePath);
				workBook.write(outFile);
				outFile.close();
				response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
				response.setContentType("application/octet-stream");
				
				File file = new File(filePath);
				fis = new FileInputStream(file);
				FileCopyUtils.copy(fis, response.getOutputStream());
				if(file.exists()){
					file.delete();
				}
				fis.close();
				LOGGER.info("Rates file downloaded successfully");
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
			
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_PLAN_RATE, null,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}finally{
			IOUtils.closeQuietly(fis);
			IOUtils.closeQuietly(outFile);
		}
		return null;
	}

	@RequestMapping(value = "/admin/planmgmt/uploadcertificationdoc", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	@ResponseBody
	public void uploadQualitRatingFile(Model model, @RequestParam(value = "certSuppDoc", required = false) MultipartFile certSuppDoc, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");
		String fileName = uploadFile("certificatonDocument", PlanMgmtConstants.CERTIFICATION_DOC_FOLDER, certSuppDoc);
		response.getWriter().write(fileName);
		response.flushBuffer();
		return;
	}

	@SuppressWarnings("unused")
	private String uploadFile(String fileToUpload, String folderName, MultipartFile file) {
		String returnString = null;
		String documentId = null;
		try {
			String filePath = GhixConstants.ISSUER_DOC_PATH+ GhixConstants.FRONT_SLASH + folderName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT + FilenameUtils.getExtension(file.getOriginalFilename());
				try {
					documentId = ecmService.createContent(filePath, modifiedFileName, file.getBytes()
							, ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY, null);
				} catch (ContentManagementServiceException e) {
					LOGGER.error("Error while reading File from DMS : "+ e.getMessage(),e);
				}
				returnString = fileToUpload + "|" + file.getOriginalFilename()+ "|" + documentId;
				
				String saveFilePath = uploadPath + GhixConstants.FRONT_SLASH + PlanMgmtConstants.CERTIFICATION_DOC_FOLDER;
				boolean isValidSaveFilePath = planMgmtUtils.isValidPath(saveFilePath);
				if(isValidSaveFilePath){
					issuerService.saveFile(saveFilePath, file, file.getOriginalFilename());
				}else{
					LOGGER.error("Invalid SaveFilePath Error");
				}
				LOGGER.info("Upload issuer documents");
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		}catch(Exception e){
			//throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.UPLOAD_FILE, null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
			LOGGER.error("Exception occured while uploading in uploadQualitRatingFile: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.UPLOAD_FILE.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
			returnString =  PlanMgmtConstants.ERROR;
		}
		return returnString;
	}

	@RequestMapping(value = "/admin/issuer/removeTenant", method = { RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	@ResponseBody
	public List<PlanTenantUtil> removeTenants(Model model, HttpServletRequest manageQHPrequest, @RequestParam(value = "selectedPlanIds[]", required = false) String[] selectedPlanIds, @RequestParam(value = "selectedTenants[]", required = false) String[] selectedTenants) {
		LOGGER.info("Remove Tenants Started");
		List<PlanTenantUtil> resultPlans = new ArrayList<PlanTenantUtil>();
		try {
		if (null != selectedPlanIds && null != selectedTenants) {
			Map<Integer, PlanTenantUtil> incomingData = new HashMap<Integer, PlanTenantUtil>();
			Map<Integer, Integer> resultData = new HashMap<Integer, Integer>();
			String[] tenantUtilData = null;
			List<Integer> planIds = new ArrayList<Integer>();
			for (String temp : selectedPlanIds) {
				PlanTenantUtil tempPlanTenantUtil = new PlanTenantUtil();
				if(null != temp) {
					tenantUtilData = temp.split(" ");
				}
				planIds.add(Integer.parseInt(tenantUtilData[0]));
				tempPlanTenantUtil.setPlanId(Integer.parseInt(tenantUtilData[0]));
				// Store the plan number in the util object
				tempPlanTenantUtil.setPlanNumber(tenantUtilData[1]);
				incomingData.put(Integer.parseInt(tenantUtilData[0]), (tempPlanTenantUtil));
				tempPlanTenantUtil = null;
			}
			resultData = planMgmtService.getTenantCount(planIds);
			PlanTenantUtil tempPlanTenantUtil = new PlanTenantUtil();
			if (!resultData.isEmpty()) {
				for (Integer i : planIds) {
					if (resultData.containsKey(i)) {
						tempPlanTenantUtil = incomingData.get(i);
						// If a plan has single tenant or same number of plan as requested to delete then can not delete
						if (resultData.get(i) <= 1 || resultData.get(i) <= selectedTenants.length) {
							tempPlanTenantUtil.setCountOfTenantIds(resultData.get(i));
							resultPlans.add(tempPlanTenantUtil);
						} else {
							planMgmtService.deleteTenants(i, selectedTenants);
						}
					}
				}

			}
		}
		} catch (Exception e) {
			LOGGER.error("Exception occured while removing tenants: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.REMOVE_TENANTS.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
		}
		return resultPlans;
	}

	@RequestMapping(value = "/admin/issuer/addTenants", method = { RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	@ResponseBody
	public List<PlanTenantUtil> addTenants(Model model, HttpServletRequest manageQHPrequest, @RequestParam(value = "selectedPlanIds[]", required = false) String[] selectedPlanIds, @RequestParam(value = "selectedTenants[]", required = false) String[] selectedTenants) {
		LOGGER.info("Add Tenants Started");
		List<PlanTenantUtil> incomingData = new ArrayList<PlanTenantUtil>();
		List<PlanTenantUtil> resultData = new ArrayList<PlanTenantUtil>();
		try {
		if (null != selectedPlanIds && null != selectedTenants) {
			List<Integer> planIds = new ArrayList<Integer>();
			String[] tenantUtilData = null;
			for (String temp : selectedPlanIds) {
				PlanTenantUtil tempPlanTenantUtil = new PlanTenantUtil();
				tenantUtilData = temp.split(" ");
				planIds.add(Integer.parseInt(tenantUtilData[0]));
				tempPlanTenantUtil.setPlanId(Integer.parseInt(tenantUtilData[0]));
				tempPlanTenantUtil.setPlanNumber(tenantUtilData[1]);
				incomingData.add(tempPlanTenantUtil);
				tempPlanTenantUtil = null;
			}
			resultData = planMgmtService.addTenants(incomingData, selectedTenants);
		}
		LOGGER.info("Add Tenants End");
		} catch (Exception e) {
			LOGGER.error("Exception occured while adding tenants: ",e);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.ADD_TENANTS.toString(), new TSDate(), Exception.class.getName(),e.getStackTrace().toString(), null);
		
		}
		return resultData;
	}
	

	@RequestMapping(value = "/admin/planmgmt/downLoadServiceArea/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public void downLoadServiceArea(@PathVariable("encPlanId") String encPlanId, HttpServletResponse response){
		LOGGER.info("Download Service Area Started");
		String decryptedPlanId = null;
		Integer planId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			ServiceAreaDetails serviceAreaDetails = planMgmtService.getServiceAreaDetails(planId,null);
			planMgmtService.downLoadServiceArea(serviceAreaDetails, response);
			LOGGER.info("File downloaded successfully!!!");
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_DETAIL_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}

	}
	
	
	@RequestMapping(value = "/admin/planmgmt/updateQHPstatusinbulk", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_QHP_PLANS_PERMISSION)
	public String loadUpdateStatusInBlock(Model model, HttpServletRequest request, @RequestParam(value = "planIds", required = true) String planIds,
			@RequestParam(value = "certificationStatus", required = false) String certificationStatus,
			@RequestParam(value = "enrollmentavail", required = false) String enrollmentavail, 
			@RequestParam(value = "effectiveDate", required = false) String effectiveDate,
			@RequestParam(value = "planYearSelected", required = true) String planYearSelected, 
			@RequestParam(value = "isSelectAllPlans", required = true) String isSelectAllPlans,
			@RequestParam(value = "updateType", required = true) String updateType,
			RedirectAttributes redirectAttributes) {
		LOGGER.debug("loadUpdateStatusInBlock start: " + SecurityUtil.sanitizeForLogging(String.valueOf(planYearSelected)));
		boolean isPlanDecertified = false;
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT);					
			Date currentDate = dateFormat.parse(dateFormat.format(new TSDate()));
			
			Integer userId = userService.getLoggedInUser().getId();
						
			if (planYearSelected != null && !planYearSelected.equals("")) {
				Map<String, Integer> selectedYearMap = new HashMap<String, Integer>();
				selectedYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(planYearSelected.toString()));
				model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, selectedYearMap);
				LOGGER.debug("Updating Plans for Year : " + SecurityUtil.sanitizeForLogging(String.valueOf(selectedYearMap.toString())));
				model.addAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, planYearSelected);
				request.getSession().setAttribute(PlanMgmtConstants.SELECTEDPLANYEARFORBULK, planYearSelected);
			}
						
			if(isSelectAllPlans.equals(PlanMgmtConstants.TRUE)){ // when select all plan for bulk update				
				
				boolean isUpdated = false;
				String planNumber = "ALL";
				String planLevel = "ALL";
				String planMarket = "ALL";
				String status = "ALL";
				String verified = "ALL";
			
				int issuerId = 0;
				int applicableYear = 2014; 
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.PLAN_NUMBER))) {
					planNumber = request.getParameter(PlanMgmtConstants.PLAN_NUMBER);
				}

//				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.ISSUERNAME))) {					
//					if(!request.getParameter(PlanMgmtConstants.ISSUERNAME).isEmpty()){
//						String[] issuerDtls = request.getParameter(PlanMgmtConstants.ISSUERNAME).split("_");
//						issuerId = Integer.parseInt(issuerDtls[0]); 						
//					}
//				}
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.ISSUERID))) {
					issuerId = Integer.parseInt(request.getParameter(PlanMgmtConstants.ISSUERID));
				}
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL))) {
					planLevel = request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL);
				}
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.PLAN_MARKET))) {
					planMarket = request.getParameter(PlanMgmtConstants.PLAN_MARKET);
				}						
								
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS))) {
					status = request.getParameter(PlanMgmtConstants.STATUS);
				}
				
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.VERIFIED))) {
					verified = request.getParameter(PlanMgmtConstants.VERIFIED);
				}
				
				if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR))) {
					applicableYear = Integer.parseInt(request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR));
				}
				
				if(updateType.equalsIgnoreCase(PlanMgmtConstants.CERTIFICATION_STATUS_UPDATE)){
					if(StringUtils.isBlank(certificationStatus) || !certificationStatus.matches(PlanMgmtConstants.CERTIFICATIONSTATUS_PATTERN) ){
						throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					}					
					List<String[]> updatedPlanList = planMgmtService.bulkUpdateCertificationStatus(certificationStatus, userService.getLoggedInUser().getId(), 
							PlanMgmtConstants.HEALTH, planNumber, planLevel, planMarket, status, verified, applicableYear, issuerId, stateCode);

					/* IF STATE EXCHANGE ID THEN SEND NOTIFICATION EMAIL TO ISSUER REP ON BULK PLAN CERTIFICATION  */
					if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_ID)){
						planMgmtService.sendNotificationEmailToIssuerRepresentative(updatedPlanList);
					}
					
					Map<String, String> requestData = null;
					Object[] temp = null;
					List<Map<String, String>> listOfPlans = new ArrayList<Map<String, String>>();
					if(updatedPlanList != null){	
						for (Object obj : updatedPlanList){
							temp = (Object[])obj;
							
							requestData = new HashMap<String, String>();
							requestData.put(PlanMgmtConstants.AHBX_PLAN_ID, temp[PlanMgmtConstants.TWO].toString());
							requestData.put(PlanMgmtConstants.AHBX_ISSUER_ID, temp[PlanMgmtConstants.ONE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_LEVEL, temp[PlanMgmtConstants.THREE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_MARKET, temp[PlanMgmtConstants.FOUR].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_NAME, temp[PlanMgmtConstants.FIVE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE, temp[PlanMgmtConstants.SIX].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR, (temp[PlanMgmtConstants.SEVEN].toString().equals(temp[PlanMgmtConstants.EIGHT].toString())) ? "N" : "U");
							requestData.put(PlanMgmtConstants.AHBX_PLAN_TYPE, temp[PlanMgmtConstants.NINE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(temp[PlanMgmtConstants.TEN]));
							requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(temp[PlanMgmtConstants.ELEVEN]));
							requestData.put(PlanMgmtConstants.AHBX_PLAN_HIOS_ID, temp[PlanMgmtConstants.TWELVE].toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_STATUS, Plan.PlanStatus.CERTIFIED.toString());
							requestData.put(PlanMgmtConstants.AHBX_PLAN_YEAR, temp[PlanMgmtConstants.THIRTEEN].toString());
							listOfPlans.add(requestData);						
						}
					}	
					
					/* call syncPlanWithAHBXInBulk() service to sync list of plans with AHBX when we do bulk plan certification */
					planMgmtService.syncPlanWithAHBXInBulk(listOfPlans);
					
					isUpdated = true;
				} else if(updateType.equalsIgnoreCase(PlanMgmtConstants.ENROLLMENT_AVBL_UPDATE)){
					if(StringUtils.isBlank(effectiveDate) || StringUtils.isBlank(enrollmentavail) || !enrollmentavail.matches(PlanMgmtConstants.ENROLLMENTAVAILABILITY_PATTERN) ){
						throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					}

					Date enrDate = dateFormat.parse(effectiveDate);
					isUpdated = planMgmtService.bulkUpdateEnrollmentAvbl(enrollmentavail, enrDate, userService.getLoggedInUser().getId(), 
							PlanMgmtConstants.HEALTH, planNumber, planLevel, planMarket, status, verified, applicableYear, issuerId);							
				} else{
					LOGGER.info("Update type not set, update type should be either certification status update or enrollment availability..");
				}
						
				if (isUpdated) {
					LOGGER.info("Bulk Update successfull");
					redirectAttributes.addAttribute(PlanMgmtConstants.SUCCESS_STR, PlanMgmtConstants.TRUE);
				} else {
					LOGGER.info("Bulk Update failed");
					redirectAttributes.addAttribute(PlanMgmtConstants.SUCCESS_STR, PlanMgmtConstants.FALSE);
				}
			}
			else {
				List<Plan> plansToUpdate = new ArrayList<Plan>();
				String[] splittedPlanIds = planIds.split(",");
				
				if(updateType.equalsIgnoreCase(PlanMgmtConstants.CERTIFICATION_STATUS_UPDATE)){					
					List<String> rejectedPlans = new ArrayList<String>();					
					for (int i = 0; i < splittedPlanIds.length; i++) {
						String planId = splittedPlanIds[i];
						Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
						String issuerStatus = issuerService.getIssuerStatus(plan.getIssuer().getId());
						if((Plan.PlanStatus.CERTIFIED.name().equals(certificationStatus)) && !(Issuer.certification_status.CERTIFIED.name().equals(issuerStatus))){
							rejectedPlans.add(plan.getIssuerPlanNumber());
						}else{
							plansToUpdate.add(plan);
						}
					}
					if(!rejectedPlans.isEmpty()){
						redirectAttributes.addAttribute("rejectedPIds",rejectedPlans.toString());
						redirectAttributes.addAttribute("error",PlanMgmtConstants.TRUE);
						setRedirectAttributes(request,redirectAttributes);
						return PlanMgmtConstants.MANAGE_QHP_PATH;
					}
					
					String pendingDecertifyPlans  = planMgmtService.findDeCertifiedPlans(plansToUpdate);
					if(StringUtils.isBlank(pendingDecertifyPlans)){
						boolean isUpdated = false;
						boolean doUpdatePlanCertificationStatus = false;
						Map<String, String> requestData = null;
						String planlevel = null;
						List<Map<String, String>> listOfPlans = new ArrayList<Map<String, String>>();
						
						for(Plan plan: plansToUpdate){
							isUpdated = false;
							doUpdatePlanCertificationStatus = false;
							isPlanDecertified = false;
							
							isPlanDecertified = planMgmtUtils.checkIsPlanDecertified(plan.getStatus(), plan.getDeCertificationEffDate());
							if(!plan.getStatus().equalsIgnoreCase(certificationStatus) ){
								doUpdatePlanCertificationStatus = true;
							}
							
							if(doUpdatePlanCertificationStatus && !(isPlanDecertified)){
								plan.setStatus(certificationStatus);
								plan.setLastUpdatedBy(userId);	
								// when plan status set to CERTIFIED then set issuer verification status as PENDING
								if(certificationStatus.equalsIgnoreCase(PlanMgmtConstants.CERTIFIED)){
									plan.setIssuerVerificationStatus(Plan.IssuerVerificationStatus.PENDING.toString());
									plan.setCertifiedby(userId.toString());
									plan.setCertifiedOn(new TSDate());
								}
								isUpdated = planMgmtService.modifyPlan(plan); 
							}
							if(isUpdated && doUpdatePlanCertificationStatus) {
								// Only for ID state exchange, send notification to issuer representatives when plan certification status set as CERTIFIED 
								if(stateCode.equalsIgnoreCase(PlanMgmtConstants.STATE_CODE_ID) && certificationStatus.equalsIgnoreCase(Plan.PlanStatus.CERTIFIED.toString()) ){
									planMgmtService.sendNotificationEmailToIssuerRepresentative(plan,Plan.PlanStatus.CERTIFIED.toString());
								}
								// prepare plan data for sync with AHBX - START
								if(plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
									planlevel = plan.getPlanHealth().getPlanLevel();
								}else if(plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
									planlevel = plan.getPlanDental().getPlanLevel();
								}
								requestData = new HashMap<String, String>();
								requestData.put(PlanMgmtConstants.AHBX_PLAN_ID, String.valueOf(plan.getId()));
								requestData.put(PlanMgmtConstants.AHBX_ISSUER_ID, plan.getIssuer().getHiosIssuerId());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_LEVEL, planlevel);
								requestData.put(PlanMgmtConstants.AHBX_PLAN_MARKET, plan.getMarket());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_NAME, plan.getName());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE, plan.getNetworkType());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR, (plan.getLastUpdateTimestamp().equals(plan.getCreationTimestamp())) ? "N" : "U");
								requestData.put(PlanMgmtConstants.AHBX_PLAN_TYPE, plan.getInsuranceType());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(plan.getStartDate()));
								requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(plan.getEndDate()));
								requestData.put(PlanMgmtConstants.AHBX_PLAN_HIOS_ID, plan.getIssuerPlanNumber());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_STATUS, plan.getStatus());
								requestData.put(PlanMgmtConstants.AHBX_PLAN_YEAR, Integer.toString(plan.getApplicableYear()));
								listOfPlans.add(requestData);
								// prepare plan data for sync with AHBX - END
							}
						}
						
						/* call syncPlanWithAHBXInBulk() service to sync list of plans with AHBX when we do bulk plan certification */
						planMgmtService.syncPlanWithAHBXInBulk(listOfPlans);
						
					}else if(StringUtils.isNotBlank(pendingDecertifyPlans)){
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.PENDING_DECERTIFICATION, pendingDecertifyPlans);
						setRedirectAttributes(request, redirectAttributes);
						return PlanMgmtConstants.MANAGE_QHP_PATH;
					}
				} else if(updateType.equalsIgnoreCase(PlanMgmtConstants.ENROLLMENT_AVBL_UPDATE)){
					for (int i = 0; i < splittedPlanIds.length; i++) {
						String planId = splittedPlanIds[i];
						Plan plan = planMgmtService.getPlan(Integer.parseInt(planId));
						plansToUpdate.add(plan);							
					}
					
					String enrollmentAvailPlans  = planMgmtService.findEnrollmentAvailablePlans(plansToUpdate);								
					Date enrDate = null;
					if(StringUtils.isBlank(enrollmentAvailPlans)){
						boolean isUpdated = false;						
						boolean isEnrollmentUpdated = false;
						
						for(Plan plan: plansToUpdate){
							isUpdated = false;
							isEnrollmentUpdated = false;
							isPlanDecertified = false;
							
							isPlanDecertified = planMgmtUtils.checkIsPlanDecertified(plan.getStatus(), plan.getDeCertificationEffDate());
							if(!plan.getEnrollmentAvail().equalsIgnoreCase(enrollmentavail) || null == plan.getFutureErlAvailEffDate() || !plan.getFutureErlAvailEffDate().equals(effectiveDate)){
								isEnrollmentUpdated = true;
							}
							if(isEnrollmentUpdated && !(isPlanDecertified)){
								enrDate = dateFormat.parse(effectiveDate);
								if(enrDate.equals(currentDate)){
									plan.setEnrollmentAvail(enrollmentavail);									
									plan.setEnrollmentAvailEffDate(enrDate);
									plan.setFutureEnrollmentAvail(null);
									plan.setFutureErlAvailEffDate(null);
								} else{
									plan.setFutureEnrollmentAvail(enrollmentavail);									
									plan.setFutureErlAvailEffDate(enrDate);	
								}
								
								plan.setLastUpdatedBy(userId);								
								isUpdated = planMgmtService.modifyPlan(plan); 
							}
							
							// send enrollment availability notification to issuer rep only for ID state exchange 
							if(isUpdated && isEnrollmentUpdated && stateCode.equalsIgnoreCase("ID")){
								enrollmentAvailabilityEmailUtil.sendNotificationEmailToIssuerRepresentativeForEnrollment(plan);
							}
						}
					}else if(StringUtils.isNotBlank(enrollmentAvailPlans)){
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.ENROLL_AVAIL, enrollmentAvailPlans);
						setRedirectAttributes(request, redirectAttributes);
						return PlanMgmtConstants.MANAGE_QHP_PATH;
					}
				} else{
					LOGGER.info("Update type not set, update type should be either certification status update or enrollment availability..");
				}
				redirectAttributes.addAttribute(PlanMgmtConstants.SUCCESS_STR, PlanMgmtConstants.TRUE);
			}
			request.getSession().setAttribute(PlanMgmtConstants.SORT_BY, "lastUpdateTimestamp");			
			LOGGER.info("loadUpdateStatusInBlock excuted successfully !!");			
			setRedirectAttributes(request, redirectAttributes);
			
		} catch (Exception e) {
			LOGGER.error("Exception Occured ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE, null,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return PlanMgmtConstants.MANAGE_QHP_PATH;
	}
	
	@RequestMapping(value = "/admin/planmgmt/qhp/updateSelectAllFlag", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'SPL_MANAGE_PLAN')")
	@ResponseBody
	public String setUpdateSelectAllFlag(HttpServletRequest request, Model model, RedirectAttributes redirectAttrs,
								@RequestParam(value = PlanMgmtConstants.SELECTALL_QHP_PLANS, required = false) String isSelectAllPlansOnAllPagesForQHP, HttpServletResponse response) {
		String successString = null;
		try {
			HttpSession session = request.getSession();
			LOGGER.debug("Value of isSelectAllPlansOnAllPagesForQHP flag : " + SecurityUtil.sanitizeForLogging(String.valueOf(isSelectAllPlansOnAllPagesForQHP)));
			if(isSelectAllPlansOnAllPagesForQHP.equalsIgnoreCase("true")){
			session.setAttribute(PlanMgmtConstants.SELECTALL_QHP_PLANS,isSelectAllPlansOnAllPagesForQHP);
			}else{
				session.removeAttribute(PlanMgmtConstants.SELECTALL_QHP_PLANS);
			}
			successString = isSelectAllPlansOnAllPagesForQHP;
		} catch (Exception e) {
			successString = "fail";
			LOGGER.error("Exception Occurred in setUpdateSelectAllFlag: ", e);
		}
		return successString;
	}
	
	private void setRedirectAttributes(HttpServletRequest request, RedirectAttributes redirectAttributes) {
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.VERIFIED))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.VERIFIED, request.getParameter(PlanMgmtConstants.VERIFIED));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.STATUS, request.getParameter(PlanMgmtConstants.STATUS));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.MARKET))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.MARKET, request.getParameter(PlanMgmtConstants.MARKET));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.ISSUERNAME))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.ISSUERNAME, request.getParameter(PlanMgmtConstants.ISSUERNAME));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.ISSUERID))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.ISSUERID, request.getParameter(PlanMgmtConstants.ISSUERID));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.PLAN_NUMBER))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.PLAN_NUMBER, request.getParameter(PlanMgmtConstants.PLAN_NUMBER));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATES))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.STATES, request.getParameter(PlanMgmtConstants.STATES));
		}
		if (StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL))) {
			redirectAttributes.addAttribute(PlanMgmtConstants.SELECTED_PLAN_LEVEL, request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL));
			redirectAttributes.addAttribute(PlanMgmtConstants.PLAN_LEVEL, request.getParameter(PlanMgmtConstants.SELECTED_PLAN_LEVEL));
		}
		HttpSession session = request.getSession();
		if(session.getAttribute(PlanMgmtConstants.SELECTALL_QHP_PLANS) !=  null){
			session.removeAttribute(PlanMgmtConstants.SELECTALL_QHP_PLANS);
		}
	}
	
	
	
	@RequestMapping(value = "/admin/planmgmt/qhp/terminateEnrollment", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'SPL_MANAGE_PLAN')")
	@ResponseBody
	public String terminateEnrollmentAjaxCall(HttpServletRequest request, Model model, RedirectAttributes redirectAttrs,
			@RequestParam(value = "planId", required = false) String encPlanId,  HttpServletResponse response) {
		String successString = null;
		Plan plan = null;
		String terminateEnrollmentResponse = Plan.TerminateEnrollmentStatusResponse.FAILURE.toString();
		String decertificationEffiectiveDate = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
			String decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if(null != decryptedPlanId){
				plan = planMgmtService.getPlan(Integer.parseInt(decryptedPlanId));
			}
			
			if(null != plan){
				String terminateEnrollmentConfiguration = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.TERMINATE_ENROLLMENT_FEATURE);
				if(null != plan.getDeCertificationEffDate()){
					decertificationEffiectiveDate = DateUtil.dateToString(plan.getDeCertificationEffDate(), GhixConstants.REQUIRED_DATE_FORMAT);
				}
				if(StringUtils.isNotBlank(decertificationEffiectiveDate)){
					Date deCertiyDate = formatter.parse(decertificationEffiectiveDate);
					if(deCertiyDate.after(new TSDate())){
						if(StringUtils.isNotEmpty(terminateEnrollmentConfiguration) && PlanMgmtConstants.ON.equalsIgnoreCase(terminateEnrollmentConfiguration)){
							if(null != plan.getIssuerPlanNumber() && PlanMgmtConstants.ZERO != plan.getApplicableYear() && null != decertificationEffiectiveDate){
								terminateEnrollmentResponse = planMgmtRestUtil.terminateEnrollmentByPlanId(plan.getIssuerPlanNumber(), plan.getApplicableYear(), decertificationEffiectiveDate);
								if(LOGGER.isDebugEnabled()){
									LOGGER.debug("terminateEnrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(terminateEnrollmentResponse)));
								}
							}
						}
						if(StringUtils.isNotEmpty(terminateEnrollmentResponse) && Plan.TerminateEnrollmentStatusResponse.SUCCESS.toString().equalsIgnoreCase(terminateEnrollmentResponse)){
							successString = Plan.TerminateEnrollmentStatusResponse.SUCCESS.toString();
						}else{
							successString = Plan.TerminateEnrollmentStatusResponse.FAILURE.toString();
						}
						
						plan.setTerminateEnrollmentStatus(successString);
						boolean updateResponse = planMgmtService.modifyPlan(plan);
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Plan Terminated Successfully " + updateResponse);
						}
					}
				}
			}
		} catch (Exception e) {
			successString = Plan.TerminateEnrollmentStatusResponse.FAILURE.toString();
			LOGGER.error("Exception Occurred in terminateEnrollmentAjaxCall: ", e);
		}
		return successString;
	}
	
	
	@RequestMapping(value = "/admin/planmgmt/downLoadFormularyDrug/{encApplicableYear}/{encIssuerHiosId}/{encFormularyId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ADMIN_PORTAL_MANAGE_PLAN')")
	public void downLoadFormularyDrug(@PathVariable("encApplicableYear") String encApplicableYear, 
			@PathVariable("encIssuerHiosId") String encIssuerHiosId, 
			@PathVariable("encFormularyId") String encFormularyId, HttpServletResponse response){
		LOGGER.info("Download Formulary Drug List Started");
		try {
			if(StringUtils.isEmpty(encApplicableYear) || StringUtils.isEmpty(encFormularyId) || StringUtils.isEmpty(encIssuerHiosId)){
				LOGGER.error("Error While Downloading Formulary Drug List file..!!!");
			}else{
				String decryptedApplicableYear = ghixJasyptEncrytorUtil.decryptStringByJasypt(encApplicableYear);
				String decryptedIssuerHiosId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerHiosId);
				String decryptedFormularyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encFormularyId);
				FormularyDrugListResponseDTO formularyDrugDetails = planMgmtService.getFormularyDrugDetails(decryptedApplicableYear, decryptedIssuerHiosId, decryptedFormularyId);
				planMgmtService.downLoadFormularyDrug(formularyDrugDetails, response);
			}
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_QHP_DETAIL_GET.getCode(), null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}
	
	@RequestMapping(value = "/admin/planmgmt/submitnetworkurl", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN)
	@ResponseBody
	public String editProviderNetworkURL(@RequestParam("id") Integer planId, @RequestParam("networkURL") String networkURL) {
		try {
			Integer userId = userService.getLoggedInUser().getId();
			planMgmtService.updateNetworkURL(planId, networkURL.trim(), userId);
			return PlanMgmtConstants.TRUE;
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_PLAN_DETAILS_EXCEPTION.getCode(), null,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH);			
		}
	}
	
	
	@RequestMapping(value = "/admin/planmgmt/submitformularyurl", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.EDIT_PLAN)
	@ResponseBody
	public String editFormularyURL(@RequestParam("id") Integer planId, @RequestParam("formularyURL") String formularyURL) {
		try {
			Integer userId = userService.getLoggedInUser().getId();
			planMgmtService.updateFomularyURL(planId, formularyURL.trim(), userId);
			return PlanMgmtConstants.TRUE;
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EDIT_PLAN_DETAILS_EXCEPTION.getCode(), null,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH);			
		}
	}
}
