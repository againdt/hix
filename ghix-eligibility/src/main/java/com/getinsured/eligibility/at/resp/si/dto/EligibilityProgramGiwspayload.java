package com.getinsured.eligibility.at.resp.si.dto;



public class EligibilityProgramGiwspayload {

	private Long id;

	private Integer giWsPayloadId;

	private Integer eligibilityProgramId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	/*@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE);
	}*/

	public Integer getEligibilityProgramId() {
		return eligibilityProgramId;
	}

	public void setEligibilityProgramId(Integer eligibilityProgramId) {
		this.eligibilityProgramId = eligibilityProgramId;
	}

}