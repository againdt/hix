
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.getinsured.ssap.model.financial.IfsvErrorMessageDetail;
import com.getinsured.ssap.model.financial.ifsv.exception.IfsvException;
import com.getinsured.ssap.model.financial.nmec.exception.NonEsiMecException;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "totalProcessingTime",
    "memberResponseSet"
})
public class NonEsiMecVerificationResponse implements Serializable
{
	
    @JsonProperty("totalProcessingTime")
    private Long totalProcessingTime;
    @JsonProperty("memberResponseSet")
    private List<MemberResponseSet> memberResponseSet = new ArrayList<MemberResponseSet>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 4357879896297762853L;
    @JsonProperty("ErrorMessageDetail")
    private List<NonEsiMecErrorMessageDetail> errorMessageDetail;
    
    
    public List<NonEsiMecErrorMessageDetail> getErrorMessageDetail() {
		return errorMessageDetail;
	}

	public void setErrorMessageDetail(List<NonEsiMecErrorMessageDetail> errorMessageDetail) {
		this.errorMessageDetail = errorMessageDetail;
	}

	@JsonProperty("Exception")
	private NonEsiMecException exception;
    
    public NonEsiMecException getException() {
		return exception;
	}

	public void setException(NonEsiMecException exception) {
		this.exception = exception;
	}

	@JsonProperty("totalProcessingTime")
    public Long getTotalProcessingTime() {
        return totalProcessingTime;
    }

    @JsonProperty("totalProcessingTime")
    public void setTotalProcessingTime(Long totalProcessingTime) {
        this.totalProcessingTime = totalProcessingTime;
    }

    @JsonProperty("memberResponseSet")
    public List<MemberResponseSet> getMemberResponseSet() {
        return memberResponseSet;
    }

    @JsonProperty("memberResponseSet")
    public void setMemberResponseSet(List<MemberResponseSet> memberResponseSet) {
        this.memberResponseSet = memberResponseSet;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("totalProcessingTime", totalProcessingTime).
        		append("memberResponseSet", memberResponseSet)
        		.append("additionalProperties", additionalProperties)
        		.append("errorMessageDetail", errorMessageDetail)
        		.toString();
    }

}
