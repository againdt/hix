
package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class HealthCoverage {

    @Expose
    private ChpInsurance chpInsurance=new ChpInsurance();
    @Expose
    private Boolean haveBeenUninsuredInLast6MonthsIndicator=false;
    @Expose
    private List<CurrentEmployer> currentEmployer = new ArrayList<>();
    @Expose
    private List<FormerEmployer> formerEmployer = new ArrayList<>();
    @Expose
    private String primaryCarePhysicianName;
    @Expose
    private Boolean otherInsuranceIndicator=false;
    @Expose
    private Boolean currentlyEnrolledInCobraIndicator=false;
    @Expose
    private Boolean currentlyHasHealthInsuranceIndicator=false;
    @Expose
    private Boolean employerWillOfferInsuranceIndicator=null;
    @Expose
    private Boolean currentlyEnrolledInRetireePlanIndicator=false;
    @Expose
    private CurrentOtherInsurance currentOtherInsurance=new CurrentOtherInsurance();

    @Expose
    private OtherInsuranceCoverage otherInsurance = new OtherInsuranceCoverage();

    @Expose
    private Boolean currentlyEnrolledInVeteransProgramIndicator=false;
    @Expose
    private Boolean hasPrimaryCarePhysicianIndicator=false;
    @Expose
    private Date employerWillOfferInsuranceCoverageStartDate;
    @Expose
    private Boolean willBeEnrolledInCobraIndicator=false;
    @Expose
    private Boolean willBeEnrolledInVeteransProgramIndicator=false;
    @Expose
    private MedicaidInsurance medicaidInsurance=new MedicaidInsurance();
    @Expose
    private Boolean isOfferedHealthCoverageThroughJobIndicator=false;
    @Expose
    private Boolean willBeEnrolledInRetireePlanIndicator=false;
    @Expose
    private Date employerWillOfferInsuranceStartDate;

    /**
     * UI wants force user to make initial selection, defaulting to null
     */
    @Expose
    private Boolean stateHealthBenefit = null;

    /**
     * UI wants force user to make initial selection, defaulting to null
     */
    @Expose
    private Boolean eligibleITU = null;

    /**
     * UI wants force user to make initial selection, defaulting to null
     */
    @Expose
    private Boolean receivedITU = null;

    /**
     * UI wants force user to make initial selection, defaulting to null
     */
    @Expose
    private Boolean unpaidBill = null;

    /**
     * UI wants force user to make initial selection, defaulting to null
     */
    @Expose
    private Boolean coveredDependentChild = null;

    /**
     * UI wants force user to make initial selection, defaulting to null
     */
    @Expose
    private Boolean absentParent = null;

    public ChpInsurance getChpInsurance() {
        return chpInsurance;
    }

    public void setChpInsurance(ChpInsurance chpInsurance) {
        this.chpInsurance = chpInsurance;
    }

    public Boolean getHaveBeenUninsuredInLast6MonthsIndicator() {
        return haveBeenUninsuredInLast6MonthsIndicator;
    }

    public void setHaveBeenUninsuredInLast6MonthsIndicator(Boolean haveBeenUninsuredInLast6MonthsIndicator) {
        this.haveBeenUninsuredInLast6MonthsIndicator = haveBeenUninsuredInLast6MonthsIndicator;
    }

    public List<CurrentEmployer> getCurrentEmployer() {
        return currentEmployer;
    }

    public void setCurrentEmployer(List<CurrentEmployer> currentEmployer) {
        this.currentEmployer = currentEmployer;
    }

    public List<FormerEmployer> getFormerEmployer() {
        return formerEmployer;
    }

    public void setFormerEmployer(List<FormerEmployer> formerEmployer) {
        this.formerEmployer = formerEmployer;
    }

    public String getPrimaryCarePhysicianName() {
        return primaryCarePhysicianName;
    }

    public void setPrimaryCarePhysicianName(String primaryCarePhysicianName) {
        this.primaryCarePhysicianName = primaryCarePhysicianName;
    }

    public Boolean getOtherInsuranceIndicator() {
        return otherInsuranceIndicator;
    }

    public void setOtherInsuranceIndicator(Boolean otherInsuranceIndicator) {
        this.otherInsuranceIndicator = otherInsuranceIndicator;
    }

    public Boolean getCurrentlyEnrolledInCobraIndicator() {
        return currentlyEnrolledInCobraIndicator;
    }

    public void setCurrentlyEnrolledInCobraIndicator(Boolean currentlyEnrolledInCobraIndicator) {
        this.currentlyEnrolledInCobraIndicator = currentlyEnrolledInCobraIndicator;
    }

    public Boolean getCurrentlyHasHealthInsuranceIndicator() {
        return currentlyHasHealthInsuranceIndicator;
    }

    public void setCurrentlyHasHealthInsuranceIndicator(Boolean currentlyHasHealthInsuranceIndicator) {
        this.currentlyHasHealthInsuranceIndicator = currentlyHasHealthInsuranceIndicator;
    }

    public Boolean getEmployerWillOfferInsuranceIndicator() {
        return employerWillOfferInsuranceIndicator;
    }

    public void setEmployerWillOfferInsuranceIndicator(Boolean employerWillOfferInsuranceIndicator) {
        this.employerWillOfferInsuranceIndicator = employerWillOfferInsuranceIndicator;
    }

    public Boolean getCurrentlyEnrolledInRetireePlanIndicator() {
        return currentlyEnrolledInRetireePlanIndicator;
    }

    public void setCurrentlyEnrolledInRetireePlanIndicator(Boolean currentlyEnrolledInRetireePlanIndicator) {
        this.currentlyEnrolledInRetireePlanIndicator = currentlyEnrolledInRetireePlanIndicator;
    }

    public CurrentOtherInsurance getCurrentOtherInsurance() {
        return currentOtherInsurance;
    }

    public void setCurrentOtherInsurance(CurrentOtherInsurance currentOtherInsurance) {
        this.currentOtherInsurance = currentOtherInsurance;
    }

    public OtherInsuranceCoverage getOtherInsurance()
    {
        return otherInsurance;
    }

    public void setOtherInsurance(final OtherInsuranceCoverage otherInsurance)
    {
        this.otherInsurance = otherInsurance;
    }

    public Boolean getOfferedHealthCoverageThroughJobIndicator()
    {
        return isOfferedHealthCoverageThroughJobIndicator;
    }

    public void setOfferedHealthCoverageThroughJobIndicator(final Boolean offeredHealthCoverageThroughJobIndicator)
    {
        isOfferedHealthCoverageThroughJobIndicator = offeredHealthCoverageThroughJobIndicator;
    }

    public Boolean getCurrentlyEnrolledInVeteransProgramIndicator() {
        return currentlyEnrolledInVeteransProgramIndicator;
    }

    public void setCurrentlyEnrolledInVeteransProgramIndicator(Boolean currentlyEnrolledInVeteransProgramIndicator) {
        this.currentlyEnrolledInVeteransProgramIndicator = currentlyEnrolledInVeteransProgramIndicator;
    }

    public Boolean getHasPrimaryCarePhysicianIndicator() {
        return hasPrimaryCarePhysicianIndicator;
    }

    public void setHasPrimaryCarePhysicianIndicator(Boolean hasPrimaryCarePhysicianIndicator) {
        this.hasPrimaryCarePhysicianIndicator = hasPrimaryCarePhysicianIndicator;
    }

    public Date getEmployerWillOfferInsuranceCoverageStartDate() {
        return employerWillOfferInsuranceCoverageStartDate;
    }

    public void setEmployerWillOfferInsuranceCoverageStartDate(Date employerWillOfferInsuranceCoverageStartDate) {
        this.employerWillOfferInsuranceCoverageStartDate = employerWillOfferInsuranceCoverageStartDate;
    }

    public Boolean getWillBeEnrolledInCobraIndicator() {
        return willBeEnrolledInCobraIndicator;
    }

    public void setWillBeEnrolledInCobraIndicator(Boolean willBeEnrolledInCobraIndicator) {
        this.willBeEnrolledInCobraIndicator = willBeEnrolledInCobraIndicator;
    }

    public Boolean getWillBeEnrolledInVeteransProgramIndicator() {
        return willBeEnrolledInVeteransProgramIndicator;
    }

    public void setWillBeEnrolledInVeteransProgramIndicator(Boolean willBeEnrolledInVeteransProgramIndicator) {
        this.willBeEnrolledInVeteransProgramIndicator = willBeEnrolledInVeteransProgramIndicator;
    }

    public MedicaidInsurance getMedicaidInsurance() {
        return medicaidInsurance;
    }

    public void setMedicaidInsurance(MedicaidInsurance medicaidInsurance) {
        this.medicaidInsurance = medicaidInsurance;
    }

    public Boolean getIsOfferedHealthCoverageThroughJobIndicator() {
        return isOfferedHealthCoverageThroughJobIndicator;
    }

    public void setIsOfferedHealthCoverageThroughJobIndicator(Boolean isOfferedHealthCoverageThroughJobIndicator) {
        this.isOfferedHealthCoverageThroughJobIndicator = isOfferedHealthCoverageThroughJobIndicator;
    }

    public Boolean getWillBeEnrolledInRetireePlanIndicator() {
        return willBeEnrolledInRetireePlanIndicator;
    }

    public void setWillBeEnrolledInRetireePlanIndicator(Boolean willBeEnrolledInRetireePlanIndicator) {
        this.willBeEnrolledInRetireePlanIndicator = willBeEnrolledInRetireePlanIndicator;
    }

    public Date getEmployerWillOfferInsuranceStartDate() {
        return employerWillOfferInsuranceStartDate;
    }

    public void setEmployerWillOfferInsuranceStartDate(Date employerWillOfferInsuranceStartDate) {
        this.employerWillOfferInsuranceStartDate = employerWillOfferInsuranceStartDate;
    }

    public Boolean getStateHealthBenefit() {
        return stateHealthBenefit;
    }

    public void setStateHealthBenefit(Boolean stateHealthBenefit) {
        this.stateHealthBenefit = stateHealthBenefit;
    }

    public Boolean getUnpaidBill() {
        return unpaidBill;
    }

    public void setUnpaidBill(Boolean unpaidBill) {
        this.unpaidBill = unpaidBill;
    }

    public Boolean getCoveredDependentChild() {
        return coveredDependentChild;
    }

    public void setCoveredDependentChild(Boolean coveredDependentChild) {
        this.coveredDependentChild = coveredDependentChild;
    }

    public Boolean getAbsentParent() {
        return absentParent;
    }

    public void setAbsentParent(Boolean absentParent) {
        this.absentParent = absentParent;
    }

    public Boolean getEligibleITU() {
        return eligibleITU;
    }

    public void setEligibleITU(Boolean eligibleITU) {
        this.eligibleITU = eligibleITU;
    }

    public Boolean getReceivedITU() {
        return receivedITU;
    }

    public void setReceivedITU(Boolean receivedITU) {
        this.receivedITU = receivedITU;
    }
}
