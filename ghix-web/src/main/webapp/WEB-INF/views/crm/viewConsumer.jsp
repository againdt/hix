<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import ="java.util.*" %>




<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<script type="text/javascript">
var ssnReasons = new Object(); // or var map = {};

ssnReasons["RELIGIOUS_EXCEPTION"] = "Religious Exception";
ssnReasons["JUST_APPLIED"] = "Just Applied";
ssnReasons["ILLNESS_EXCEPTION"] = "Illness Exception";
ssnReasons["CITIZEN_EXCEPTION"] = "Citizen Exception";
ssnReasons["OTHER"] = "Other";
ssnReasons[""] = "";

$(document).ready(function(){
	$("#basicInfoMenu").addClass("active navmenu");
	
	if('${household.ssnNotProvidedReason}'){
		$('#ssn_not_provided_reason_text').text('(' +ssnReasons['${household.ssnNotProvidedReason}'] + ')');	
	}
	
	$("#mark_verified").click(function(e) {
		var comment = prompt("<spring:message  code='label.ridpVerificationReason'/>");
		if (comment != null) {
			var ridpOverrideUrl = '<c:url value="/crm/member/markRidpVerified"/>';
			var csrftoken = '${sessionScope.csrftoken}';
			var householdId = '<encryptor:enc value="${householdId}"/>';
			var ridpVerificationFailedMessage = "<spring:message  code='label.ridpVerificationCommentNotProvidedMessage'/>";
			$.ajax({
				url : ridpOverrideUrl,
				type : "POST",
				data : {csrftoken:csrftoken, householdId:householdId, comments:comment},
				success: function(data, textStatus, jqXHR){
					 if(data.ResponseCode == "HS000000") {
						$('#ridp_status').html("<strong><spring:message  code='label.ridpVerified'/></strong>");
					 } else {
						alert(ridpVerificationFailedMessage + data.responseDescription);
					 }
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert(ridpVerificationFailedMessage + textStatus + ", errorThrown: " + errorThrown);
				},
			});
		} else {
			alert("<spring:message  code='label.ridpVerificationCommentNotProvidedMessage'/>");
		}
	});
	
	$("#manual_verification").click(function(e) {
		var pushToManualVerificationUrl = '<c:url value="/crm/member/convertManualRIDP"/>';
		var csrftoken = '${sessionScope.csrftoken}';
		var householdId = '<encryptor:enc value="${householdId}"/>';
		var convertToManualVerificationFailureMessage = "<spring:message  code='label.ridpVerificationCommentNotProvidedMessage'/>";
		var convertToManualVerificationSuccessMessage = "<spring:message  code='label.convertToManualVerificationSuccessMessage'/>";
		$.ajax({
			url : pushToManualVerificationUrl,
			type : "POST",
			data : {csrftoken:csrftoken, householdId:householdId},
			success: function(data, textStatus, jqXHR){
				 if(data.ResponseCode == "HS000000") {
					alert(convertToManualVerificationSuccessMessage);
					$("#manual_verification" ).remove();
				 } else {
					
				 }
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert(convertToManualVerificationFailureMessage + ":" + textStatus + ", errorThrown: " + errorThrown);
			},
		});
	});
});
</script>


<c:set var="encHouseholdId"><encryptor:enc value='${householdId}'/></c:set>
<c:set var="encHouseholdEmail"><encryptor:enc value='${household.email}'/></c:set>
<c:set var="encHouseholdPhone"><encryptor:enc value='${household.phoneNumber}'/></c:set>

<jsp:include page="sendConsumerActivationLink.jsp">
	<jsp:param value="${encHouseholdId}" name="householdId"/>
	<jsp:param value="${encHouseholdEmail}"  name="householdEmail"/>
	<jsp:param value="${encHouseholdPhone}" name="householdPhone"/>
</jsp:include>

<!--start page-breadcrumb -->
<div class="gutter10">
<div class="row-fluid">	
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/member/managemembers"/>">Members</a></li>
			<li><spring:message code="label.basicInformation"/></li>
		</ul> 
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	<h1 class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})</h1>
	</div>
	<!--page-breadcrumb ends-->
	
	<div class="row-fluid">
		<div id="sidebar" class="span3">
		           <jsp:include page="../cap/consumerSummaryNavigation.jsp"></jsp:include>
		</div>
		
		<div id="rightpanel" class="span9">
			<div class="header">
                 <h4 class="pull-left">Basic Information</h4>
                 <c:if test="${permissionMap.memberEdit}">
                 <a class="btn btn-small pull-right headerBtn" id="editMember" title="Edit" href="<c:url value="/crm/member/editmember/${encHouseholdid}"/>">Edit</a>
                 </c:if>
            </div>
            <div class="gutter10">
			<form class="form-horizontal" id="" name="" action="" method="POST">
				<table class="table-border-none table">
					<tbody>
						<tr>
							<td class="txt-right span4"><spring:message  code='label.columnHeaderName'/>:</td>
							<td><strong>${household.firstName} ${household.middleName} ${household.lastName}</strong>
							</td>
						</tr>
						<tr>
						<fmt:formatDate pattern="MM/dd/yyyy" value="${household.birthDate}" var="dob"/>
						<td class="txt-right span4">Date Of Birth:</td>
							<td><strong>${dob}</strong></td>
						</tr>
						<tr >
						<td class="txt-right span4 vertical-align-top" valign="top"><spring:message  code='label.columnHeaderAddress'/>:</td>
							<td>
							<c:if test="${not empty household.prefContactLocation}">
								<strong><p>${household.prefContactLocation.address1} <br />
									<c:if test="${household.prefContactLocation.address2 != null}">${household.prefContactLocation.address2}<br/></c:if>
										${household.prefContactLocation.city}  <c:if test="${household.prefContactLocation.city != null && household.prefContactLocation.state != null}">, </c:if> ${household.prefContactLocation.state}
									</p>
							</strong>
							</c:if>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"><spring:message  code='label.columnHeaderZip'/>:</td>
							<td>
								<c:if test="${not empty household.prefContactLocation}">
									<strong>${household.prefContactLocation.zip}</strong>
								</c:if>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"><spring:message  code='label.emailAddress'/></td>
							<td><strong>${household.email}</strong></td>
						</tr>						
						
<!-- 						<tr> -->
<%-- 						<td class="txt-right span4"><spring:message  code='label.sex'/></td> --%>
<%-- 							<td><strong>${dob}</strong></td> --%>
<!-- 						</tr> -->
<!-- 						<tr> -->
<%-- 						<td class="txt-right span4"><spring:message  code='label.dob'/></td> --%>
<%-- 							<td><strong>${dob}</strong></td> --%>
<!-- 						</tr> -->
						<tr>
						<td class="txt-right span4"><spring:message  code='label.columnHeaderPhone'/>:</td>
							<td x-ms-format-detection="none"><strong>${household.phoneNumber}</strong></td>
						</tr>
						<c:if test="${permissionMap.memberEditRivs}">
							<tr>
								<td class="txt-right span4"><spring:message  code='label.columnHeaderRidpVerified'/>:</td>
								<c:if test="${null != household.ridpVerified && 'Y'.equals(household.ridpVerified)}">
	                	 			<td id="ridp_status"><strong><spring:message  code='label.ridpVerified'/></strong></td>
								</c:if>
								<c:if test="${null == household.ridpVerified || !'Y'.equals(household.ridpVerified)}">
	                	 			<td id="ridp_status">
	                	 				<button type="button" class='btn' id='mark_verified'><spring:message  code='label.markVerified'/></button>&nbsp;
	                	 				<c:if test="${!'MANUAL'.equals(household.ridpSource)}">
	                	 					<button type="button" class='btn' id='manual_verification'><spring:message  code='label.convertToManualVerification'/></button>
	                	 				</c:if>
	                	 			</td>
								</c:if>
							</tr>
						</c:if>
						<c:if test="${permissionMap.memberViewAccessCode}">
							<tr>
									<td class="txt-right span4">Access Code:</td>
								<td><strong>${accessCode}</strong></td>
									
							</tr>
						</c:if>
						<tr>
							<td class="txt-right span4">SSN:</td>
								<td>
									<c:choose>
										<c:when test="${empty household.ssn && empty household.ssnNotProvidedReason}">
											Not Provided
										</c:when>
										<c:when test="${empty household.ssn && not empty household.ssnNotProvidedReason}">
											SSN is not required <span id="ssn_not_provided_reason_text">
										</c:when>
										<c:otherwise>
											${consumerssn}
										</c:otherwise>
									</c:choose>
								</td>
						</tr>
						<c:if test="${permissionMap.memberViewCaseid}">
						<tr>
							<td class="txt-right span4">Case ID:</td>
								<td>
									<c:if test="${not empty household.householdCaseId}">
										${household.householdCaseId}
									</c:if>
								</td>
						</tr>
						</c:if>
						<c:if test="${permissionMap.memberViewMnsureId && not empty household.ffmHouseholdID}">
							<tr>
									<td class="txt-right span4">Primary Contact External ID:</td>
									<td>${household.ffmHouseholdID}</td>
							</tr>
						</c:if>
					</tbody>
				</table>
				<input type="hidden" name="consumerName" id="consumerName" value="${household.firstName} ${household.lastName}" />
			</form>
			</div>
		</div>
		

	</div> 
	<!-- row-fluid -->
</div>
<!-- gutter10 -->
<!--  Latest UI -->
<!-- Modal 
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employer/home/${employerId}" />" novalidate="novalidate">
<form name="dialogForm" id="dialogForm" action="<c:url value="/account/user/switchUserRole/employer/${employerId}" />" novalidate="novalidate">-->
<form name="dialogForm" id="dialogForm" method="POST"  action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Member Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body clearfix gutter0-tb">
    <div class="control-group">	
				<div class="controls">
                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
                Through this portal you will be able to take actions on behalf of the member.<br/>
                Proceed to Member view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Member View</button>
        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value='<encryptor:enc value="individual"/>' /> 
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value='<encryptor:enc value="${householdId}"/>' />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.firstName} ${household.lastName}" />

    <!--a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>