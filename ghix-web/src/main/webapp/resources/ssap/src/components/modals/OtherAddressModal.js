import React, {Fragment, useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import OtherAddressInputs2 from '../pages/pageIncludes/common/address/OtherAddressInputs2';
import FormForModal from '../../components/pages/FormForModal';
import {changeData, fetchCountiesForExactZipcodes, hideModal, removeCounty} from '../../actions/actionCreators';
import validators from '../../utils/validators';
import VALIDATION_TYPES from '../../constants/validationTypes';
import CONFIG from '../../.env';
import ENDPOINTS from '../../constants/endPointsList';


// Components

// ActionCreators

const OtherAddressModal = props => {

    const {
        counties,
        contexts,
        data,
        modal: {
            modalOwner,
            data: [
                otherAddress,
                index,
                pageName,
            ]
        },
    } = props;

    const [ localState, setLocalState ] = useState({
        ...otherAddress,
    });

    const zipCodesForWatch = [localState.postalCode];

    const cleanCountyCode = () => {
        setLocalState(state => ({
            ...state,
            countyCode: null
        }));
        props.actions.removeCounty(0);
    };

    useEffect(() => {

        const {
            postalCode,
            countyCode
        } = localState;
        const isPostalCodeValid = validators
            .find(validator => validator.validatorType === VALIDATION_TYPES.ZIP_CODE)
            .verifier({ value: postalCode });
        if(!isPostalCodeValid && countyCode !== null) {
            cleanCountyCode();
        }
    }, [zipCodesForWatch.toLocaleString()]);

    const valuesForWatch = [localState.postalCode, localState.state];

    useEffect(() => {
        const requestsList = [];

        const {
            postalCode,
            state
        } = localState;

        const isPostalCodeValid = validators
            .find(validator => validator.validatorType === VALIDATION_TYPES.ZIP_CODE)
            .verifier({ value: postalCode });


        if(isPostalCodeValid && state) {
            requestsList.push({
                postalCode: postalCode,
                state: state,
                hhmIndex: 0
            });
        }


        const requestListPostalCodes = [];
        const requestListStateCodes = [];

        requestsList.map(request => {
            requestListPostalCodes.push(request.postalCode);
            requestListStateCodes.push(request.state);
        });

        const countiesPostalCodes = [];
        const countiesStateCodes = [];

        counties[pageName].map(county => {
            const [ , value ] = Object.entries(county)[0];
            countiesPostalCodes.push(value.postalCode);
            countiesStateCodes.push(value.state);
        });

        let isSomePostalCodesUnique = false;

        requestListPostalCodes.forEach(postalCode => {
            if(!countiesPostalCodes.includes(postalCode)) {
                isSomePostalCodesUnique = true;
            }
        });

        let isSomeStateCodesUnique = false;

        requestListStateCodes.forEach(postalCode => {
            if(!countiesStateCodes.includes(postalCode)) {
                isSomeStateCodesUnique = true;
            }
        });

        if(CONFIG.ENV.PRODUCTION && (isSomePostalCodesUnique || isSomeStateCodesUnique)) {
            props.actions.fetchCountiesForExactZipcodes(requestsList, `${window.location.origin}${ENDPOINTS.COUNTY_ENDPOINT}`, pageName);
        }
    }, [valuesForWatch.toLocaleString()]);

    const handleSaveClick = () => {

        const updateData = () => {
            const newState = { ...data };

            if(index === null) {
                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .otherAddresses
                    .push(localState);
            } else {

                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0].householdMember = newState
                        .singleStreamlinedApplication
                        .taxHousehold[0].householdMember
                        .reduce((hhms, hhm, idx) => [ ...hhms, idx === 0 ? hhm : { ...hhm, addressId: 1 } ], []);

                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .otherAddresses[index] = localState;
            }

            props.actions.changeData(newState);
        };

        contexts.FormForModal.onFormSubmitForValidationOnly([updateData, () => props.actions.hideModal(modalOwner)]);
    };

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };

    const onFieldChange = (newData) => {
        setLocalState(localState => ({
            ...localState,
            ...newData
        }));
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal__validate__address">
                <h2>
                    Add address
                </h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div id="iiAddIncomeOrDeductionModal" className="usa-grid">
                <div className="modal__body">
                    <FormForModal>
                        <OtherAddressInputs2
                            {...{
                                ...localState,
                                counties,
                                pageName,
                                currentData: localState,
                                inputActions: {
                                    onFieldChange
                                },
                            }}
                        />
                    </FormForModal>
                </div>
            </div>
            <div className='modal__footer'>
                <div className='usa-grid controls'>
                    <div className='usa-offset-one-third txt-left'>
                        <Button
                            className='modal-cancel-btn usa-button-secondary'
                            onClick={handleHideClick}
                        >
                            Cancel
                        </Button>
                        <Button
                            className='modal-confirm-btn'
                            onClick={handleSaveClick}
                        >
                            Save
                        </Button>
                    </div>
                </div>
            </div>

        </Fragment>
    );
};

const mapStateToProps = state => ({
    contexts: state.contexts,
    counties: state.counties,
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    taxHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        fetchCountiesForExactZipcodes,
        hideModal,
        removeCounty
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(OtherAddressModal);
