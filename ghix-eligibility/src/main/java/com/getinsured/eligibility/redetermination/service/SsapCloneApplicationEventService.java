package com.getinsured.eligibility.redetermination.service;

import java.util.List;

import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

public interface SsapCloneApplicationEventService {

	List<SsapApplicationEvent> cloneSsapApplicationEvent(SsapApplication currentApplication, Long userId, SsapApplicationEventTypeEnum eventType) throws Exception;
}
