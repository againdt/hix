package com.getinsured.eligibility.estimator.mini.util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTimeConstants;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TSLocalTime;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.eligibility.phix.service.PhixService;
import com.getinsured.eligibility.prescreen.calculator.CsrCalculator;
import com.getinsured.eligibility.prescreen.calculator.FplCalculator;
import com.getinsured.eligibility.prescreen.calculator.MandatePenaltyCalculator;
import com.getinsured.eligibility.prescreen.calculator.MedicaidCalculator;
import com.getinsured.eligibility.prescreen.calculator.dto.StatePovertyGuidelines;
import com.getinsured.eligibility.prescreen.repository.IStatePovertyGuidelinesRepository;
import com.getinsured.hix.model.eligibility.CmsPovertyGuidelines;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLead.ExchangeFlowType;
import com.getinsured.hix.model.estimator.mini.EligLead.STAGE;
import com.getinsured.hix.model.estimator.mini.EligLead.STATUS;
import com.getinsured.hix.model.estimator.mini.FamilyMember;
import com.getinsured.hix.model.estimator.mini.MedicaidRequest;
import com.getinsured.hix.model.estimator.mini.MedicaidResponse;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.estimator.mini.MemberRelationships;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorRequest;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.model.estimator.mini.PreEligibilityResults;
import com.getinsured.hix.model.estimator.mini.StateChipMedicaid;
import com.getinsured.hix.model.planmgmt.EligibilityRequest;
import com.getinsured.hix.model.planmgmt.EligibilityResponse;
import com.getinsured.hix.model.prescreen.calculator.FplRequest;
import com.getinsured.hix.model.prescreen.calculator.FplResponse;
import com.getinsured.hix.planmgmt.controller.TeaserPlanRestController;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Utility class for Mini Estimator
 * @author Nikhil Talreja, Sunil Desu
 * @since 09 July, 2013
 */

@Component
public class MiniEstimatorUtil extends MiniEstimatorValidator{
	
	private static final Logger LOGGER = Logger.getLogger(MiniEstimatorUtil.class);
	private static ObjectMapper mapper = new ObjectMapper();	
	
	@Autowired
	public MiniEstimatorUtil(IStatePovertyGuidelinesRepository iStatePovertyGuidelinesRepository) {
		super();
		this.iStatePovertyGuidelinesRepository = iStatePovertyGuidelinesRepository;
		createStaticMaps();
		getLatestAvailCoverageYear();
	}
	
	public MiniEstimatorUtil() {
		super();
	}

	private IStatePovertyGuidelinesRepository iStatePovertyGuidelinesRepository;
	/*HIX-104669: Update the applicable FPL% values (Effective 2017)
	  
	  OLD Set :2,3,4,6.3,8.05,9.66
	           2,4,6.3,8.05,9.5,9.66
	  New Set :2.04,3.06,4.08,6.43,8.21,9.69
	           2.04,4.08,6.43,8.21,9.69,9.69
	           
	  https://www.irs.gov/pub/irs-drop/rp-17-36.pdf         
	  2018 Set :2.01,3.02,4.03,6.34,8.10.9.56
	            2.01,4.03,6.34,8.10,9.56,9.56            
	 
	 */
	
	//Constants for applicable percentage calculations
	protected static final double DEFAULT_APP_PERCENTAGE = 9999.0;

	private static final String _132_MIN_APP_PERCENTAGE = "132_MIN";
	private static final String _132_MAX_APP_PERCENTAGE = "132_MAX";
	private static final String _133_MIN_APP_PERCENTAGE = "133_MIN";
	private static final String _133_MAX_APP_PERCENTAGE = "133_MAX";
	private static final String _150_MIN_APP_PERCENTAGE = "150_MIN";
	private static final String _150_MAX_APP_PERCENTAGE = "150_MAX";
	private static final String _200_MIN_APP_PERCENTAGE = "200_MIN";
	private static final String _200_MAX_APP_PERCENTAGE = "200_MAX";
	private static final String _250_MIN_APP_PERCENTAGE = "250_MIN";
	private static final String _250_MAX_APP_PERCENTAGE = "250_MAX";
	private static final String _300_MIN_APP_PERCENTAGE = "300_MIN";
	private static final String _300_MAX_APP_PERCENTAGE = "300_MAX";

	private static final double FPL_LIMIT_ONE = 133.0;
	private static final double FPL_LIMIT_TWO = 150.0;
	private static final double FPL_LIMIT_THREE = 200.0;
	private static final double FPL_LIMIT_FOUR = 250.0;
	private static final double FPL_LIMIT_FIVE = 300.0;
	private static final double FPL_THRESHOLD = 420.0;
	
	protected static final double NATIVE_AMERICAN_FPL_THRESHOLD = 300.0;
	protected static final double APTC_THRESHOLD = 400.0;
	protected static final int NO_OF_MONTHS = 12;
	protected static final double TWELVE_POINT_ZERO = 12.0;
	protected static final int HUNDRED = 100;
	protected static final double HUNDRED_POINT_ZERO = 100.0;

	protected static final String MEDICARE_URL = "http://www.medicare.gov/";
	protected static final String MEDICARE_PHONE = "1-800-633-4227";
	
	private static final int FIFTEEN = 15;
	protected static final String NOT_APPLICABLE = "N/A";
	protected static final String YES = "Y";
	protected static final String NO = "N";
	
	//Static set for non-applicant types
	private static final Set<String> NON_APPLICANTS = new HashSet<String>();
	
	//Key-value pairs for PM response
	private static final String SECOND_LOWEST_SILVER_PLAN = "SLSP";
	private static final String LOWEST_SILVER_PLAN = "LSP";
	private static final String LOWEST_BRONZE_PLAN = "LBP";
	private static final String GI_LOWEST_SILVER_PLAN = "GILSP";
	private static final String GI_LOWEST_BRONZE_PLAN = "GILBP";
	
	private static final String CHIP = "CHIP";
	private static final String MEDICARE = "Medicare";
	private static final String INELIGIBLE = "Ineligible";
	
	private static final String PLAN_NAME = "PLAN NAME"; 
	private static final String PREMIUM = "MONTHLY PREMIUM";
	
	private static final String PLAN_NAME_KEY = "Plan Name";
	private static final String PREMIUM_KEY = "Monthly Premium";
	
	private static final String OOP_IND = "OOP_MAX_IND";
	private static final String OOP_FLY = "OOP_MAX_FLY";
	private static final String DEDUCTIBLE_IND = "DEDUCTIBLE_IND";
	private static final String DEDUCTIBLE_FLY ="DEDUCTIBLE_FLY";
	private static final String GENERIC = "GENERIC";
	private static final String PRIMARY = "PRIMARY_VISIT"; 
    
	private static final String ATTR = "netwkAttrib";
	private static final String VALUE = "netwkValue";
	
	//Map for pairing of benefit attributes with value string
	private static final Map<String,String> BENEFIT_ATTRIBUTES = new HashMap<String, String>();
	private static final String DOLLAR_VALUE = "$";
	
	private static final String FAILURE = "failure";
	//private static final String API_V2 = "API_V2";
	public static final String NAME_SPLITTER = "~^";
	
	//private static final String EXCHANGE_TYPE_STATE="STATE";
	private static final String EXCHANGE_TYPE_PHIX="PHIX";
	@Autowired private TeaserPlanRestController teaserPlanRestController;
	
	private static final int MAX_NO_OF_CHILDREN_FOR_QUOTING = 3;
	
	private static  int LATEST_AVAILABLE_COVERAGE_YEAR = 0;
	
	@Autowired private FplCalculator fplCalculator;
	@Autowired private CsrCalculator csrCalculator;
	@Autowired private MandatePenaltyCalculator mandatePenaltyCalculator;
	@Autowired private MedicaidCalculator medicaidCalculator;
	
	
	@Autowired private PhixService phixService;
	
	/**
	 * This List contains the year values.
	 * 
	 * <p>
	 * All year values.
	 * </p>
	 * 
	 */
	private static List<CmsPovertyGuidelines> YEAR_VALUES_LIST = new ArrayList<CmsPovertyGuidelines>();
	
	private static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	
	static{
		
		NON_APPLICANTS.add("Medicaid");
		NON_APPLICANTS.add(CHIP);
		NON_APPLICANTS.add(MEDICARE);
		NON_APPLICANTS.add("Ineligible");
		
		BENEFIT_ATTRIBUTES.put("Dollar Amount","$");
		BENEFIT_ATTRIBUTES.put("Percent co-insurance","%");

	}
	
	/**
	 * This map contains the year values.
	 * 
	 * <p>
	 * Key is the coverage year
	 * </p>
	 * 
	 * <p>
	 * Value returned for a matching key is applicable percentages.
	 * </p>
	 * 
	 */
	private static final Map<Integer, Map<String, Float>> APPLICABLE_PERCENT_MAP = new HashMap<Integer, Map<String, Float>>();
	
	/**
	 * constructing static  map for all applicable percentages
	 * @param yearValues
	 */
	private static void createApplicablePercentMap(List<CmsPovertyGuidelines> yearValues) {

		if (yearValues != null && !yearValues.isEmpty()) {
			for (CmsPovertyGuidelines yearValue : yearValues) {
			
				int year = yearValue.getYear().intValue();
				Map<String, Float> applicablePercentageMap = new HashMap<String, Float>();

				applicablePercentageMap.put(_132_MIN_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMin132().toString()));
				applicablePercentageMap.put(_132_MAX_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMax132().toString()));
				applicablePercentageMap.put(_133_MIN_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMin133().toString()));
				applicablePercentageMap.put(_133_MAX_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMax133().toString()));
				applicablePercentageMap.put(_150_MIN_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMin150().toString()));
				applicablePercentageMap.put(_150_MAX_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMax150().toString()));
				applicablePercentageMap.put(_200_MIN_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMin200().toString()));
				applicablePercentageMap.put(_200_MAX_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMax200().toString()));
				applicablePercentageMap.put(_250_MIN_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMin250().toString()));
				applicablePercentageMap.put(_250_MAX_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMax250().toString()));
				applicablePercentageMap.put(_300_MIN_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMin300().toString()));
				applicablePercentageMap.put(_300_MAX_APP_PERCENTAGE, Float.parseFloat(yearValue.getApplicablePercentMax300().toString()));
				
				if (!APPLICABLE_PERCENT_MAP.containsKey(year)) {
					APPLICABLE_PERCENT_MAP.put(year, applicablePercentageMap);
				}

			}
		}
	}
	
	
	private void createStaticMaps() {
		List<CmsPovertyGuidelines> yearvalues = getYearValues();
		createApplicablePercentMap(yearvalues);
	}
	
	/**
	 * This method all yearValues from DB.
	 * 
	 * @return List<CmsPovertyGuidelines>
	 */
	private List<CmsPovertyGuidelines> getYearValues() {
		if (YEAR_VALUES_LIST.isEmpty())
			YEAR_VALUES_LIST.addAll(iStatePovertyGuidelinesRepository.findAll());
		return YEAR_VALUES_LIST;
	}
	
	private void getLatestAvailCoverageYear() {
		LATEST_AVAILABLE_COVERAGE_YEAR = iStatePovertyGuidelinesRepository.getLatestCoverageYear().intValue();
	}
	
	/**
	 * Utility method to get age of a member
	 *
	 * @author Nikhil Talreja
	 * @since May 20, 2013
	 * @param String - The DOB in MM/dd/yyyy format
	 * @return int - age of the member
	 * @throws ParseException 
	 */
	public int getAgeFromDob(String dob, MiniEstimatorRequest request) throws ParseException{

		if(StringUtils.isBlank(dob)){
			return 0;
		}

		//Implementation change as per Joda Time
		Calendar cal = computeEffectiveDate(request);
		LocalDate now = new LocalDate(cal.getTimeInMillis());
		
		Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
		LocalDate birthdate = new LocalDate(dobDate.getTime());
		
		Years years = Years.yearsBetween(birthdate, now);
		int age =  years.getYears();
		LOGGER.debug("Age as per effective date " + age);
		
		return age;
		

	

	
	}
	
	
	/**
	 * This method computes the Applicable Percentage value based on the FPL
	 * value for the household.
	 * 
	 * @author Sunil Desu
	 * @since April 18 2013
	 * 
	 * @param fpl
	 *            value for the household
	 */
	protected double calculateApplicablePercentage(double fpl, int year) {
		
		year = year - 1;
		
		if(!APPLICABLE_PERCENT_MAP.containsKey(year)) {
			year = LATEST_AVAILABLE_COVERAGE_YEAR;
		}
		
		double applicablePercentage = 0.0;
		
		if (fpl < FPL_LIMIT_ONE) {
			applicablePercentage = APPLICABLE_PERCENT_MAP.get(year).get(_132_MIN_APP_PERCENTAGE);
		} else if (fpl >= FPL_LIMIT_ONE && fpl < FPL_LIMIT_TWO) {
			applicablePercentage = computePercentage(FPL_LIMIT_ONE, FPL_LIMIT_TWO, fpl, APPLICABLE_PERCENT_MAP.get(year).get(_133_MIN_APP_PERCENTAGE),APPLICABLE_PERCENT_MAP.get(year).get(_133_MAX_APP_PERCENTAGE));
		} else if (fpl >= FPL_LIMIT_TWO && fpl < FPL_LIMIT_THREE) {
			applicablePercentage = computePercentage(FPL_LIMIT_TWO, FPL_LIMIT_THREE, fpl, APPLICABLE_PERCENT_MAP.get(year).get(_150_MIN_APP_PERCENTAGE),APPLICABLE_PERCENT_MAP.get(year).get(_150_MAX_APP_PERCENTAGE));
		} else if (fpl >= FPL_LIMIT_THREE && fpl < FPL_LIMIT_FOUR) {
			applicablePercentage = computePercentage(FPL_LIMIT_THREE, FPL_LIMIT_FOUR, fpl, APPLICABLE_PERCENT_MAP.get(year).get(_200_MIN_APP_PERCENTAGE),APPLICABLE_PERCENT_MAP.get(year).get(_200_MAX_APP_PERCENTAGE));
		} else if (fpl >= FPL_LIMIT_FOUR && fpl < FPL_LIMIT_FIVE) {
			applicablePercentage = computePercentage(FPL_LIMIT_FOUR, FPL_LIMIT_FIVE, fpl, APPLICABLE_PERCENT_MAP.get(year).get(_250_MIN_APP_PERCENTAGE),APPLICABLE_PERCENT_MAP.get(year).get(_250_MAX_APP_PERCENTAGE));
		} else if (fpl >= FPL_LIMIT_FIVE && fpl < FPL_THRESHOLD) {
			applicablePercentage = APPLICABLE_PERCENT_MAP.get(year).get(_300_MAX_APP_PERCENTAGE);
		}
		else if (fpl > FPL_THRESHOLD){
			applicablePercentage = DEFAULT_APP_PERCENTAGE;
		}
		
		return applicablePercentage;
	}
	
	/**
	 * This method computes the Applicable Percentage value based on the FPL
	 * value for the household.
	 * 
	 * @author Sunil Desu
	 * @since April 18 2013
	 * 
	 * @param fpl
	 *            value for the household
	 */
	private double computePercentage(double lowerLimit, double upperLimit,
			double actualFpl, double lowerLimitPercentage,
			double upperLimitPercentage) {
		double applPercent = 0.0;
		applPercent = lowerLimitPercentage
				+ ((upperLimitPercentage - lowerLimitPercentage)
						* (actualFpl - lowerLimit) / (upperLimit - lowerLimit));
		/*
		 * Setting the Applicable Percentage to two digits precision as per IRS Guideline Ref# HIX-104669
		The Applicable Percentage Table and the Section 36B Required Contribution Percentage calculated under this revenue procedure are rounded to hundredths of a percentage point. 
		https://www.irs.gov/pub/irs-drop/rp-14-37.pdf
		
		*/
		BigDecimal applicablePercent = new BigDecimal(applPercent);
		applicablePercent = applicablePercent.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		
		return applicablePercent.doubleValue();
	}
	
	
	/**
	 * Calculates FPL for a household
	 * 
	 * @author - Nikhil Talreja, Sunil Desu
	 * @param response  
	 * @throws GIException 
	 * @since 11 July, 2013
	 */
	
	public FplResponse calculateFpl(String state, MiniEstimatorRequest request, boolean isMedicaidChip, int familySize) throws GIException{
		
		FplRequest fplRequest = new FplRequest();
		fplRequest.setState(state);
		fplRequest.setFamilySize(request.getFamilySize());
		fplRequest.setFamilyIncome(request.getHhIncome());
		fplRequest.setCoverageYear(request.getCoverageYear());
		
		
		
		LOGGER.debug("Calculataing FPL for state " + state);
		
		FplResponse fplResponse = fplCalculator.calculateFpl(fplRequest, isMedicaidChip, familySize);
		
		LOGGER.debug("FPL for household is " + fplResponse.getFplPercentage());
		if(fplResponse.getErrMsg()!=null && fplResponse.getErrMsg().length()>0){
			throw new GIException(fplResponse.getErrMsg());
		}
		return fplResponse;
	}
	
	/**
	 * Calculates CSR for a household
	 * 
	 * @author - Nikhil Talreja
	 * @param response 
	 * @since 11 July, 2013
	 */
	public String computeCSR(double fpl){
		return csrCalculator.computeCSR(fpl);
	}
	
	public String computeCSR(double fpl,boolean isNativeAmericanHousehold){
		return csrCalculator.computeCSR(fpl,isNativeAmericanHousehold);
	}
	/**
	 * Calculates Mandate Penalty for a household
	 * 
	 * @author - Nikhil Talreja
	 * @param response 
	 * @throws ParseException 
	 * @since 11 July, 2013
	 */
	public double calculateMandatePenalty(MiniEstimatorRequest request) throws ParseException{
		return mandatePenaltyCalculator.calculateMandatePenalty(request);
	}
	
	
	/**
	 * Calculates Medicaid/ Chip eligibility for a household
	 * JIRA: HIX-12653
	 * 
	 * @author - Nikhil Talreja
	 * @param response 
	 * @since 11 July, 2013
	 */
	public void calculateMedicaidChipEligibility(String stateCode, double fpl, MiniEstimatorRequest request,MiniEstimatorResponse response){
		
		try{
			MedicaidRequest medicaidRequest = new MedicaidRequest();
			medicaidRequest.setStateCode(stateCode);
			medicaidRequest.setFplPercentage(fpl);
			
			Calendar effDate = computeEffectiveDate(request);
			// Create an instance of SimpleDateFormat used for formatting 
			// the string representation of date (month/day/year)
			SimpleDateFormat df = new SimpleDateFormat(SHORT_DATE_FORMAT);
			String effectiveDate = df.format(effDate.getTime());			
			medicaidRequest.setEffectiveStartDate(effectiveDate);
			
			List<FamilyMember> familyMembers = new ArrayList<FamilyMember>();
			boolean isHouseholdParent=false;
			int childCount=0;
			for(Member member : request.getMembers()){
				
				FamilyMember familyMember = new FamilyMember();
				familyMember.setDateOfBirth(member.getDateOfBirth());
				if (getAgeFromDob(member.getDateOfBirth(), request) <19)
				{
					isHouseholdParent=true;
					childCount++;
				}
				familyMember.setSeekingCoverage(member.getIsSeekingCoverage().equals("Y")?true:false);
				familyMember.setNativeAmerican(member.getIsNativeAmerican().equals("Y")?true:false);
				familyMember.setPregnant(member.getIsPregnant().equals("Y")?true:false);
				familyMembers.add(familyMember);
				
			}
			
			medicaidRequest.setMembers(familyMembers);
			medicaidRequest.setHouseholdChildCount(childCount);
			medicaidRequest.setHouseholdParent(isHouseholdParent);
			
			MedicaidResponse medicaidResponse = medicaidCalculator.calculateMedicaidAndCHIPEligibility(medicaidRequest);
			
			boolean hasMedicareMember = false;
			
			if(medicaidResponse != null && medicaidResponse.getErrCode() == 0){
				
				List<Member> members = new ArrayList<Member>();
				
				for(FamilyMember member : medicaidResponse.getMembers()){
					
					Member record = new Member();
					record.setMemberEligibility(member.getMemberEligibility());
					record.setMemberNumber(member.getSequenceNumber());
					record.setDateOfBirth(request.getMembers().get(record.getMemberNumber() - 1 ).getDateOfBirth());
					record.setIsTobaccoUser(request.getMembers().get(record.getMemberNumber() - 1).getIsTobaccoUser());
					record.setIsSeekingCoverage(request.getMembers().get(record.getMemberNumber() - 1).getIsSeekingCoverage());
					record.setRelationshipToPrimary(request.getMembers().get(record.getMemberNumber() - 1).getRelationshipToPrimary());
					record.setIsNativeAmerican(request.getMembers().get(record.getMemberNumber() - 1).getIsNativeAmerican());
					members.add(record);
					
					if(StringUtils.equalsIgnoreCase(MEDICARE, record.getMemberEligibility())){
						hasMedicareMember = true;
					}
				}
				
				response.setMembers(members);
				
				updateMedicaidResponse(medicaidResponse, response, stateCode, hasMedicareMember);
				
			}
			else{
				LOGGER.error("Unable to determine Medicaid/CHIP eligibility for household");
				throw new RuntimeException("Unable to determine Medicaid/CHIP eligibility for household");
			}
		}catch(Exception e){
			LOGGER.error("Unable to determine Medicaid/CHIP eligibility for household");
			throw new RuntimeException("Unable to determine Medicaid/CHIP eligibility for household "+e.fillInStackTrace(), e);
		}
	}
	
	/**
	 * Updates estimator response form medicaid response
	 * JIRA: HIX-12653
	 * 
	 * @author - Nikhil Talreja
	 *  
	 * @since 02 September, 2013
	 */
	public void updateMedicaidResponse(MedicaidResponse medicaidResponse, MiniEstimatorResponse response, String stateCode, boolean hasMedicareMember){
		
		//Medicaid Expansion flag
		response.setMedicaidExpansion(medicaidResponse.getMedicaidExpansion());
		
		//Medicaid Eligible
		if(medicaidResponse.isHouseholdMedicaidEligible()){
			response.setMedicaidHousehold(YES);
		}
		
		//CHIP household
		if(medicaidResponse.isHouseholdCHIPEligible()){
			response.setChipHousehold(YES);
		}
		
		//Medicare household
		if(medicaidResponse.isHouseholdMedicareEligible()){
			response.setMedicareHousehold(YES);
		}
		
		//NativeAmerican household
		if(medicaidResponse.isNativeAmericanHousehold()){
			response.setNativeAmericanHousehold(YES);
		}
		//SBE Details
		if(StringUtils.isNotBlank(medicaidResponse.getSbeWebsite())){
			response.setHixURL(medicaidResponse.getSbeWebsite());
			response.setHixName(new StateHelper().getStateName(stateCode)+" Health Exchange");
		}
		if(StringUtils.isNotBlank(medicaidResponse.getSbePhone())){
			response.setHixPhoneNumber(medicaidResponse.getSbePhone());
		}
		
		//Medicare URL and Phone
		if(hasMedicareMember){
			response.setMedicareURL(MEDICARE_URL);
			response.setMedicarePhoneNumber(MEDICARE_PHONE);
		}
		
	}
	
	/**
	 * Updates member eligibility response strings for certain cases
	 * JIRA: HIX-12653
	 * 
	 * @author - Nikhil Talreja
	 * @param response 
	 * @since 11 July, 2013
	 */
	public void updateMemberEligibility(double fpl, MiniEstimatorRequest request,MiniEstimatorResponse response){
		
		for(Member member : response.getMembers()){
			
			//If APTC and CSR Eligible, change eligibility from APTC to APTC/CSR
			if(StringUtils.equalsIgnoreCase(member.getMemberEligibility(),"APTC")
					&& StringUtils.equalsIgnoreCase(response.getCsrEligible(),YES)){
				if (member.getIsNativeAmerican().equalsIgnoreCase(YES) && fpl >= HUNDRED_POINT_ZERO && fpl< NATIVE_AMERICAN_FPL_THRESHOLD  )
				{
					member.setMemberEligibility("APTC/IndianEligibility2");
				}
				else
				{
				member.setMemberEligibility("APTC/CSR");
				}
			}else if (StringUtils.equalsIgnoreCase(member.getMemberEligibility(),"APTC")
					&& StringUtils.equalsIgnoreCase(response.getCsrEligible(),NO)){
				if (member.getIsNativeAmerican().equalsIgnoreCase(YES) && fpl >= HUNDRED_POINT_ZERO && fpl< NATIVE_AMERICAN_FPL_THRESHOLD  )
				{
					member.setMemberEligibility("APTC/IndianEligibility2");
				}else if (member.getIsNativeAmerican().equalsIgnoreCase(YES) && fpl>= NATIVE_AMERICAN_FPL_THRESHOLD  )
				{
					member.setMemberEligibility("APTC/IndianEligibility3");
				}
			}
			//FPL > 420%
			else if(fpl<HUNDRED_POINT_ZERO || fpl > APTC_THRESHOLD && !StringUtils.equalsIgnoreCase(member.getMemberEligibility(),MEDICARE)){
				if( fpl > APTC_THRESHOLD && response.getNativeAmericanHousehold().equalsIgnoreCase(YES)?true:false== true)
				{
					member.setMemberEligibility("IndianEligibility3");
				}
				else if(!member.getMemberEligibility().equalsIgnoreCase(CHIP)&&!member.getMemberEligibility().equalsIgnoreCase("Medicaid") &&!StringUtils.equalsIgnoreCase(member.getMemberEligibility(),MEDICARE)){
					member.setMemberEligibility(INELIGIBLE);
				}
			}
		}
	}
	
	/**
	 * Computes effective date for coverage start
	 * 
	 * Following logic is implemented as part of HIX-24255
	 * 
	 * 1. If current date is on or before 15th of a month, effective date is 1st of next month
	 * 2. If current date is after 15th of a month, effective date is 1st of next to next month
	 * 
	 * Exmaples:
	 * 1. Current Date - 12th July 2014 , Effective Date - 1st August 2014
	 * 2. Current Date - 19th July 2014, Effective Date - 1st September 2014
	 * 3. Current Date - 28th November 2013, Effective Date - 1st January 2014
	 * 4. Current Date - 25th December 2013, Effective Date - 1st February 2014
	 * 
	 * @author - Sunil Desu, Nikhil Talreja
	 * @throws ParseException 
	 */
	private Calendar computeEffectiveDate(MiniEstimatorRequest request) throws ParseException {
		
		LOGGER.debug("Computing effective date");
		
		/*
		 * HIX-55453 Modify computeEffectiveDate base on 15 day rule
		 * @author - Nikhil Talreja
		 */
		LocalDate effectiveDate = TSLocalTime.getLocalDateInstance();
		LocalDate currentDate = TSLocalTime.getLocalDateInstance();

		Calendar effDate = TSCalendar.getInstance();
		if(request.getEffectiveStartDate()!=null){
			SimpleDateFormat df = new SimpleDateFormat(SHORT_DATE_FORMAT);
			Date coverageStartDate = df.parse(request.getEffectiveStartDate());
			effDate.setTime(coverageStartDate);
			return effDate;
		}
		
		// Current Date is on or before 15 of month
		if (currentDate.getDayOfMonth() <= FIFTEEN) {
			effectiveDate = effectiveDate.plusMonths(1);
		}
		// Current Date is after 15 of month
		else {
			effectiveDate = effectiveDate.plusMonths(2);
		}
		
		effectiveDate = effectiveDate.withDayOfMonth(1);
		
		//Minimum coverage start date is 1st Jan of the coverage year
		LocalDate minCovStart = new LocalDate(request.getCoverageYear(), DateTimeConstants.JANUARY,1);
		if(effectiveDate.isBefore(minCovStart)){
			effectiveDate = minCovStart;
		}
		
		effDate.setTime(effectiveDate.toDate());
		return effDate;		
	}
	
	/**
	 * Gets sample plan details from Plan Management
	 * JIRA: HIX-12787
	 * 
	 * @author - Nikhil Talreja
	 * @throws ParseException 
	 * @since 17 July, 2013
	 */
	public void getPlanDetails(MiniEstimatorRequest request,MiniEstimatorResponse response,String state, double applicablePercentage,String flowType,String exchangeType, double fpl) throws ParseException{
		
		try{
			LOGGER.info("Fetching plan details");
			
			EligibilityRequest eligibilityRequest = createPlanManagementRequest(request,response,state, fpl);
			eligibilityRequest.setExchangeFlowType(flowType);
			eligibilityRequest.setSendOnlySilverPlan(true);
			//No call will be made to PM API if no. of applicants is 0
			if(CollectionUtils.isEmpty(eligibilityRequest.getMembers())){
				LOGGER.info("No applicants eligbile for APTC/CSR");
				return;
			}
			
			//String restUrl = GhixEndPoints.PlanMgmtEndPoints.TEASER_ELIGIBILITY_PLANS_API_URL;
			LOGGER.debug("Request to plan management " +  eligibilityRequest);
			EligibilityResponse eligibilityResponse = teaserPlanRestController.getEligibilityPremiumHelper(eligibilityRequest);
			LOGGER.debug("Response from plan management " + eligibilityResponse);
			
			//Check if PM response has error
			if(eligibilityResponse != null && eligibilityResponse.getErrCode() == GhixConstants.ELIGIBILITY_RESPONSE_ERRORCODE){
				throw new RuntimeException("Error received in Plan Management Response " + eligibilityResponse.getErrMsg());
			}
			
			populatePlanDetails(request,response,eligibilityRequest,eligibilityResponse,applicablePercentage,exchangeType);
			
		}
		catch(Exception e){
			LOGGER.error("Exception occured while invoking Plan Management API",e);
			throw new RuntimeException("Error Invoking Plan Management API "+e.fillInStackTrace(), e);
		}
		
	}

	/**
	 * Gets request object to call plan management API
	 * JIRA: HIX-12787
	 * 
	 * @author - Nikhil Talreja
	 * @param response 
	 * @throws ParseException 
	 */
	private EligibilityRequest createPlanManagementRequest(MiniEstimatorRequest request, MiniEstimatorResponse response, String state, double fpl) throws ParseException {

		EligibilityRequest eligibilityRequest = new EligibilityRequest();
		
		eligibilityRequest.setZipCode(request.getZipCode());
		eligibilityRequest.setCounty(request.getCountyCode());
		eligibilityRequest.setState(state);
		eligibilityRequest.setEffectiveDate(computeEffectiveDate(request));
		
		if(StringUtils.equalsIgnoreCase(response.getCsrLevelSilver(), "CS4") || StringUtils.equalsIgnoreCase(response.getCsrLevelSilver(), "CS5")
				|| StringUtils.equalsIgnoreCase(response.getCsrLevelSilver(), "CS6")){
			eligibilityRequest.setCostSharingVariation(response.getCsrLevelSilver());
		}
		else{
			/*
			 * Cost Sharing variance should not be sent as null to PM
			 */
			eligibilityRequest.setCostSharingVariation("CS1");
		}
		
		com.getinsured.hix.model.planmgmt.Member pmMember;
		Calendar dateOfBirth = TSCalendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat(SHORT_DATE_FORMAT);
		
		List<com.getinsured.hix.model.planmgmt.Member> members = new ArrayList<com.getinsured.hix.model.planmgmt.Member>();
		
		//Adding Data for members
		for(Member member : getEligibileMembersForQuoting(response, fpl, request)){
			
			LOGGER.debug("Member eligibility " + member.getMemberEligibility());
			
			//Only applicants to be considered
			//if(!NON_APPLICANTS.contains(member.getMemberEligibility()) && 
					//StringUtils.equalsIgnoreCase(member.getIsSeekingCoverage(), YES)){
				
				pmMember = new com.getinsured.hix.model.planmgmt.Member();
				pmMember.setMemberType(member.getRelationshipToPrimary().name());
				dateOfBirth.setTime(dateFormat.parse(member.getDateOfBirth()));
				pmMember.setDateOfBirth(dateOfBirth);
				pmMember.setAge(getAgeFromDob(member.getDateOfBirth(), request));
				
				//Tobacco usage
				if(StringUtils.equalsIgnoreCase(member.getIsTobaccoUser(), YES)){
					pmMember.setTobaccoUser(true);
				}
				
				members.add(pmMember);
			//}
			
		}
		
		eligibilityRequest.setMembers(members);
		
		return eligibilityRequest;
	}
	
	/**
	 * Populates estimator response with plan details from Plan Management response
	 * JIRA: HIX-12787
	 * 
	 * @author - Nikhil Talreja 
	 */
	private void populatePlanDetails(MiniEstimatorRequest request, MiniEstimatorResponse response, EligibilityRequest eligibilityRequest,
			EligibilityResponse eligibilityResponse, double applicablePercentage,String exchangeType) {
		
		Map<String,HashMap<String, String>> plan = null;
		
		int noOfApplicants = eligibilityRequest.getMembers().size();
		double aptc = 0.0;
		
		//No. of Silver and Bronze plans
		response.setNoOfSilverPlansAvailable(eligibilityResponse.getNumberSilverAvailable());
		response.setNoOfBronzePlansAvailable(eligibilityResponse.getNumberBronzeAvailable());
		
		if(eligibilityResponse.isAllPlansAvailableSilver() && eligibilityResponse.isAllPlansAvailableBronze()){
			response.setAllPlansAvailable(YES);
		}
		else{
			response.setAllPlansAvailable(NO);
		}
		
		//Check if Silver plans are available
		if(eligibilityResponse.isAllPlansAvailableSilver()){
			
			//Calculate APTC using non tobacco premium
			LOGGER.debug("Calculating APTC for household");
			aptc = calculateAPTC(eligibilityResponse.getBenchmarkPremium(),request.getHhIncome(),applicablePercentage);
			if(response.getAptcEligible().equalsIgnoreCase(NO)){
				aptc = 0.0;
			}
			response.setAptcAmount(DOLLAR_VALUE+aptc);
			

			/*
			 * Second lowest silver plan
			 * If only one silver plan is available, SLSP will be same as LSP
			 */
			if(eligibilityResponse.getNumberSilverAvailable() == 1){
				plan = eligibilityResponse.getPlansInformation().get(LOWEST_SILVER_PLAN);
			}
			else{
				plan = eligibilityResponse.getPlansInformation().get(SECOND_LOWEST_SILVER_PLAN);
			}
			if(!exchangeType.equalsIgnoreCase(EXCHANGE_TYPE_PHIX))
			{
			response.setSlspName(plan.get(PLAN_NAME).get(PLAN_NAME_KEY));
			response.setSlspMonthlyPremiumBeforeAPTC(DOLLAR_VALUE+plan.get(PREMIUM).get(PREMIUM_KEY));
			if(NumberUtils.isNumber(plan.get(PREMIUM).get(PREMIUM_KEY))){
				response.setSlspMonthlyPremiumBeforeAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY))));
				if(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc >0){
					
				response.setSlspMonthlyPremiumAfterAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc ));
				}
				else
				{
					response.setSlspMonthlyPremiumAfterAPTC(DOLLAR_VALUE +0);
				}
			}
			addBenefitsToPlan(response, plan, SECOND_LOWEST_SILVER_PLAN,noOfApplicants);
			
			//Lowest Silver plan
			plan = eligibilityResponse.getPlansInformation().get(LOWEST_SILVER_PLAN);
			response.setLspName(plan.get(PLAN_NAME).get(PLAN_NAME_KEY));
			response.setLspMonthlyPremiumBeforeAPTC(DOLLAR_VALUE+plan.get(PREMIUM).get(PREMIUM_KEY));
			if(NumberUtils.isNumber(plan.get(PREMIUM).get(PREMIUM_KEY))){
				response.setLspMonthlyPremiumBeforeAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY))));
				if(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc >0){
				response.setLspMonthlyPremiumAfterAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc));
				}
				else
				{
					response.setLspMonthlyPremiumAfterAPTC(DOLLAR_VALUE +0);
				}
			}
			addBenefitsToPlan(response, plan, LOWEST_SILVER_PLAN,noOfApplicants);
			
			}
		

		}
	}
	
	/**
	 * Checks for availablity of bronze plans from PM response and populated Estimator response
	 * 
	 * @author Nikhil Talreja
	 * @since 23 September, 2013
	 */
	
	@SuppressWarnings("unused")
	private void checkForBronzePlans(MiniEstimatorResponse response, EligibilityResponse eligibilityResponse, int noOfApplicants, double aptc,String exchangeType){
		
		Map<String,HashMap<String, String>> plan = null;
		
		/*
		 * Check if Lowest Bronze plan is available
		 * If yes, GILBP will be same as LBP
		 */
		if(eligibilityResponse.isAllPlansAvailableBronze()){
			
			//LBP
			plan  = eligibilityResponse.getPlansInformation().get(LOWEST_BRONZE_PLAN);
			if(plan != null){
				if(!exchangeType.equalsIgnoreCase(EXCHANGE_TYPE_PHIX))
				{
				response.setLbpName(plan.get(PLAN_NAME).get(PLAN_NAME_KEY));
				response.setLbpMonthlyPremiumBeforeAPTC(DOLLAR_VALUE+plan.get(PREMIUM).get(PREMIUM_KEY));
				if(NumberUtils.isNumber(plan.get(PREMIUM).get(PREMIUM_KEY))){
					response.setLbpMonthlyPremiumBeforeAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY))));
					if(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc >0){
					response.setLbpMonthlyPremiumAfterAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc ));
					}
					else
					{
						response.setLbpMonthlyPremiumAfterAPTC(DOLLAR_VALUE +0);
					}
						
				}
				addBenefitsToPlan(response, plan, LOWEST_BRONZE_PLAN,noOfApplicants);
				}
				//GILBP
				response.setGiAvailableLbpName(plan.get(PLAN_NAME).get(PLAN_NAME_KEY));
				response.setGiAvailableLbpMonthlyPremiumBeforeAPTC(DOLLAR_VALUE+plan.get(PREMIUM).get(PREMIUM_KEY));
				if(NumberUtils.isNumber(plan.get(PREMIUM).get(PREMIUM_KEY))){
					response.setGiAvailableLbpMonthlyPremiumBeforeAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY))));
					if(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc >0){
					response.setGiAvailableLbpMonthlyPremiumAfterAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc ));
					}
					else
					{
						response.setGiAvailableLbpMonthlyPremiumAfterAPTC(DOLLAR_VALUE +0);
					}
				}
				addBenefitsToGIPlan(response, plan, GI_LOWEST_BRONZE_PLAN,noOfApplicants);	
			}
	
		}
		else{
			plan  = eligibilityResponse.getPlansInformation().get(GI_LOWEST_BRONZE_PLAN);
			
			if(plan != null){
				response.setGiAvailableLbpName(plan.get(PLAN_NAME).get(PLAN_NAME_KEY));
				response.setGiAvailableLbpMonthlyPremiumBeforeAPTC(DOLLAR_VALUE+plan.get(PREMIUM).get(PREMIUM_KEY));
				if(NumberUtils.isNumber(plan.get(PREMIUM).get(PREMIUM_KEY))){
					response.setGiAvailableLbpMonthlyPremiumBeforeAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY))));
					if(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc >0){
						
					response.setGiAvailableLbpMonthlyPremiumAfterAPTC(DOLLAR_VALUE + Math.round(Double.parseDouble(plan.get(PREMIUM).get(PREMIUM_KEY)) - aptc ));
					}
					else
					{
						response.setGiAvailableLbpMonthlyPremiumAfterAPTC(DOLLAR_VALUE +0);
					}
				}
				addBenefitsToGIPlan(response, plan, GI_LOWEST_BRONZE_PLAN,noOfApplicants);	
			}
		}
		
	}
	
	/**
	 * Populates estimator response with plan benefits from Plan Management response
	 * for plans other than GI plans
	 * JIRA: HIX-12787
	 * 
	 * @author - Nikhil Talreja 
	 */
	public void addBenefitsToPlan(MiniEstimatorResponse response,
			Map<String,HashMap<String, String>> benefits, String planType,int noOfApplicants){
		
		//Second Lowest Silver Plan
		if(StringUtils.equalsIgnoreCase(planType, SECOND_LOWEST_SILVER_PLAN)){
			if(noOfApplicants == 1){
				response.setSlspDeductible(setBenefit(benefits,DEDUCTIBLE_IND));
				response.setSlspMaxOutOfPocket(setBenefit(benefits,OOP_IND));
			}
			else{
				response.setSlspDeductible(setBenefit(benefits,DEDUCTIBLE_FLY));
				response.setSlspMaxOutOfPocket(setBenefit(benefits,OOP_FLY));
			}
			response.setSlspDoctorVisits(setBenefit(benefits,PRIMARY));
			response.setSlspGenericDrugs(setBenefit(benefits,(GENERIC)));
		}
		
		//Lowest Silver Plan
		else if(StringUtils.equalsIgnoreCase(planType, LOWEST_SILVER_PLAN)){
			if(noOfApplicants == 1){
				response.setLspDeductible(setBenefit(benefits,DEDUCTIBLE_IND));
				response.setLspMaxOutOfPocket(setBenefit(benefits,OOP_IND));
			}
			else{
				response.setLspDeductible(setBenefit(benefits,DEDUCTIBLE_FLY));
				response.setLspMaxOutOfPocket(setBenefit(benefits,OOP_FLY));
			}
			response.setLspDoctorVisits(setBenefit(benefits,PRIMARY));
			response.setLspGenericDrugs(setBenefit(benefits,(GENERIC)));
		}
		//Lowest Bronze Plan
		else if(StringUtils.equalsIgnoreCase(planType, LOWEST_BRONZE_PLAN)){
			if(noOfApplicants == 1){
				response.setLbpDeductible(setBenefit(benefits,DEDUCTIBLE_IND));
				response.setLbpMaxOutOfPocket(setBenefit(benefits,OOP_IND));
			}
			else{
				response.setLbpDeductible(setBenefit(benefits,DEDUCTIBLE_FLY));
				response.setLbpMaxOutOfPocket(setBenefit(benefits,OOP_FLY));
			}
			response.setLbpDoctorVisits(setBenefit(benefits,PRIMARY));
			response.setLbpGenericDrugs(setBenefit(benefits,(GENERIC)));
		}
	}
	
	/**
	 * Populates estimator response with plan benefits from Plan Management response
	 * for GI plans
	 * JIRA: HIX-12787
	 * 
	 * @author - Nikhil Talreja
	 */
	public void addBenefitsToGIPlan(MiniEstimatorResponse response,
			Map<String,HashMap<String, String>> benefits, String planType,int noOfApplicants){
		
		//GI Second Lowest Silver Plan
		if(StringUtils.equalsIgnoreCase(planType, GI_LOWEST_SILVER_PLAN)){
			if(noOfApplicants == 1){
				response.setGiAvailableLspDeductible(setBenefit(benefits,DEDUCTIBLE_IND));
				response.setGiAvailableLspMaxOutOfPocket(setBenefit(benefits,OOP_IND));
			}
			else{
				response.setGiAvailableLspDeductible(setBenefit(benefits,DEDUCTIBLE_FLY));
				response.setGiAvailableLspMaxOutOfPocket(setBenefit(benefits,OOP_FLY));
			}
			response.setGiAvailableLspDoctorVisits(setBenefit(benefits,PRIMARY));
			response.setGiAvailableLspGenericDrugs(setBenefit(benefits,(GENERIC)));
		}
				
		//GI Lowest Bronze Plan
		else if(StringUtils.equalsIgnoreCase(planType, GI_LOWEST_BRONZE_PLAN)){
			if(noOfApplicants == 1){
				response.setGiAvailableLbpDeductible(setBenefit(benefits,DEDUCTIBLE_IND));
				response.setGiAvailableLbpMaxOutOfPocket(setBenefit(benefits,OOP_IND));
			}
			else{
				response.setGiAvailableLbpDeductible(setBenefit(benefits,DEDUCTIBLE_FLY));
				response.setGiAvailableLbpMaxOutOfPocket(setBenefit(benefits,OOP_FLY));
			}
			response.setGiAvailableLbpDoctorVisits(setBenefit(benefits,PRIMARY));
			response.setGiAvailableLbpGenericDrugs(setBenefit(benefits,(GENERIC)));
		}
	}
	
	/**
	 * Formats a benefit based on value and attribute type
	 * JIRA: HIX-12787
	 * 
	 * E.g - $100, 120% etc.
	 * 
	 * @author - Nikhil Talreja 
	 * @param benefits 
	 * @return String - Formatted benefit
	 * @since - 19 July, 2013
	 */
	private String setBenefit(Map<String, HashMap<String, String>> benefits, String benefit){
		
		
		if(benefit == null){
			return "Benefit not covered by this plan";
		}
		
		Map<String, String> benefitType = new HashMap<String, String>();
		benefitType = benefits.get(benefit);
		if(benefitType == null){
			return "Benefit not covered by this plan";
		}
		String value = benefitType.get(VALUE);
		
		if(value == null){
			return "Benefit not covered by this plan";
		}
		
		String attr = BENEFIT_ATTRIBUTES.get(benefits.get(benefit).get(ATTR));
		
		//If attribute is other value
		if(attr == null){
			return value + " " + benefits.get(benefit).get(ATTR);
		}
		//Attribute is %
		else if (StringUtils.equalsIgnoreCase(attr, "%")){
			return value + attr;
		}
		//Attribute is $
		else{
			return attr + value;
		}
	}
	
	/**
	 * Calculate APTC for household
	 * 
	 * @author - Nikhil Talreja
	 * @since - 19 July, 2013
	 */
	public double calculateAPTC(double benchmarkPlanPremium, double householdIncome, double applicablePercentage){
		
		double maxAptc = (benchmarkPlanPremium * TWELVE_POINT_ZERO) - (householdIncome * applicablePercentage / HUNDRED);
		
		if(maxAptc < 0.0){
			maxAptc = 0.0;
		}
		else{
			maxAptc = Math.round(maxAptc / NO_OF_MONTHS);
		}
		
		LOGGER.debug("Max APTC for household " + maxAptc);
		return maxAptc;
	}
	
	
	/**
	 * Resets a response to default values in case of exceptions
	 * 
	 * @author - Nikhil Talreja
	 * @since - 13 August, 2013
	 */
	public void resetResponse(MiniEstimatorResponse response) {
		
		response.setAllPlansAvailable(NOT_APPLICABLE);
		response.setSubsidyEligible(NOT_APPLICABLE);
		response.setMandatePenaltyAmount(NOT_APPLICABLE);
		response.setExpectedMonthlyPremium(NOT_APPLICABLE);
		response.setAptcEligible(NOT_APPLICABLE);
		response.setAptcAmount(NOT_APPLICABLE);
		response.setCsrEligible(NOT_APPLICABLE);
		response.setCsrLevelSilver(NOT_APPLICABLE);
		
		//Re-setting medicaid/CHIP data
		response.setMedicaidHousehold(NOT_APPLICABLE);
		response.setChipHousehold(NOT_APPLICABLE);
		response.setMedicaidExpansion(NOT_APPLICABLE);
		
		//Re-setting plan Data
		response.setNoOfSilverPlansAvailable(0);
		response.setNoOfBronzePlansAvailable(0);
		
		response.setSlspDeductible(NOT_APPLICABLE);
		response.setSlspMaxOutOfPocket(NOT_APPLICABLE);
		response.setSlspDoctorVisits(NOT_APPLICABLE);
		response.setSlspGenericDrugs(NOT_APPLICABLE);
		response.setSlspName(NOT_APPLICABLE);
		response.setSlspMonthlyPremiumBeforeAPTC(NOT_APPLICABLE);
		response.setSlspMonthlyPremiumAfterAPTC(NOT_APPLICABLE);
		
		response.setLspDeductible(NOT_APPLICABLE);
		response.setLspMaxOutOfPocket(NOT_APPLICABLE);
		response.setLspDoctorVisits(NOT_APPLICABLE);
		response.setLspGenericDrugs(NOT_APPLICABLE);
		response.setLspName(NOT_APPLICABLE);
		response.setLspMonthlyPremiumBeforeAPTC(NOT_APPLICABLE);
		response.setLspMonthlyPremiumAfterAPTC(NOT_APPLICABLE);
		
		response.setLbpDeductible(NOT_APPLICABLE);
		response.setLbpMaxOutOfPocket(NOT_APPLICABLE);
		response.setLbpDoctorVisits(NOT_APPLICABLE);
		response.setLbpGenericDrugs(NOT_APPLICABLE);
		response.setLbpName(NOT_APPLICABLE);
		response.setLbpMonthlyPremiumBeforeAPTC(NOT_APPLICABLE);
		response.setLbpMonthlyPremiumAfterAPTC(NOT_APPLICABLE);
		
		response.setGiAvailableLspDeductible(NOT_APPLICABLE);
		response.setGiAvailableLspMaxOutOfPocket(NOT_APPLICABLE);
		response.setGiAvailableLspDoctorVisits(NOT_APPLICABLE);
		response.setGiAvailableLspGenericDrugs(NOT_APPLICABLE);
		response.setGiAvailableLspName(NOT_APPLICABLE);
		response.setGiAvailableLspMonthlyPremiumBeforeAPTC(NOT_APPLICABLE);
		response.setGiAvailableLspMonthlyPremiumAfterAPTC(NOT_APPLICABLE);

		response.setGiAvailableLbpDeductible(NOT_APPLICABLE);
		response.setGiAvailableLbpMaxOutOfPocket(NOT_APPLICABLE);
		response.setGiAvailableLbpDoctorVisits(NOT_APPLICABLE);
		response.setGiAvailableLbpGenericDrugs(NOT_APPLICABLE);
		response.setGiAvailableLbpName(NOT_APPLICABLE);
		response.setGiAvailableLbpMonthlyPremiumBeforeAPTC(NOT_APPLICABLE);
		response.setGiAvailableLbpMonthlyPremiumAfterAPTC(NOT_APPLICABLE);
		
		//Resetting SBE details
		response.setHixName(NOT_APPLICABLE);
		response.setHixPhoneNumber(NOT_APPLICABLE);
		response.setHixURL(NOT_APPLICABLE);
		response.setMedicarePhoneNumber(NOT_APPLICABLE);
		response.setMedicareURL(NOT_APPLICABLE);
	}
	
	/**
	 * Creates new record for ELIG_LEAD table
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14 August, 2013
	 */
	public EligLead createNewEligLeadRecord(MiniEstimatorRequest request) {
		
		if(request == null){
			return null;
		}
		
		//Get leadId from request. Fetch from DB. If record exists, update it. Else, create it and populate it in response.
		EligLead record = checkEligLeadRecord(request.getLeadId());
		PreEligibilityResults preEligResult = null;
		List<PreEligibilityResults> preEligibilityResults = null;
		if(record == null){
			record = new EligLead();
			//Create a new record for pre eligibility results for the given year
			preEligibilityResults = new ArrayList<PreEligibilityResults>();
			preEligResult = new PreEligibilityResults();
			preEligResult.setCoverageYear(request.getCoverageYear());
		}
		else{
			//Check if there is a current year record for pre elig result
			preEligibilityResults = record.getPreEligibilityResults();
			if(preEligibilityResults != null && !preEligibilityResults.isEmpty()){
				for(PreEligibilityResults result : preEligibilityResults){
					if(result.getCoverageYear() == request.getCoverageYear()){
						preEligResult = result;
						break;
					}
				}
				//There is no pre elig result for the request year
				if (preEligResult == null){
					preEligResult = new PreEligibilityResults();
					preEligResult.setCoverageYear(request.getCoverageYear());
				}
			}
			//There are no pre elig results for this lead
			else{
				preEligibilityResults = new ArrayList<PreEligibilityResults>();
				preEligResult = new PreEligibilityResults();
				preEligResult.setCoverageYear(request.getCoverageYear());
			}
		}
		
		//Adding basic validations to prevent SQL exceptions
		StringBuilder errors = new StringBuilder();
		if(StringUtils.length(request.getZipCode()) <= ZIP_COUNTY_LENGTH ){
			record.setZipCode(request.getZipCode());
			preEligResult.setZipCode(request.getZipCode());
		}
		else{
			errors.append("Zip code length should be 5;");
		}
		if(StringUtils.length(request.getCountyCode()) <= ZIP_COUNTY_LENGTH ){
			record.setCountyCode(request.getCountyCode());
			preEligResult.setCountyCode(request.getCountyCode());
		}
		else{
			errors.append("County code length should be 5;");
		}
		record.setFamilySize(request.getFamilySize());
		record.setHouseholdIncome(request.getHhIncome());
		record.setDocVisitFrequency(request.getDocVisitFrequency());
		record.setNoOfPrescriptions(request.getNoOfPrescriptions());
		record.setBenefits(request.getBenefits());
		record.setNoOfApplicants(findNoOfApplicants(request.getMembers()));
		preEligResult.setFamilySize(request.getFamilySize());
		preEligResult.setHouseholdIncome(request.getHhIncome());
		preEligResult.setNoOfApplicants(findNoOfApplicants(request.getMembers()));
		record.setEmailAddress(request.getEmail());
		if(StringUtils.length(request.getPhone()) <= PHONE_NUMBER_LENGTH ){
			record.setPhoneNumber(request.getPhone());
		}
		else{
			errors.append("Phone number length should be 10;");
		}
		
		
		if(!request.getMembers().isEmpty()){
			try {
				record.setMemberData(mapper.writeValueAsString(request.getMembers()));
				preEligResult.setMemberData(mapper.writeValueAsString(request.getMembers()));
			} catch (Exception e) {
				LOGGER.error("Error fetching member data",e);
			}
		}
		
		record.setAffiliateId(request.getAffiliateId());
		record.setFlowId(request.getFlowId());
		record.setExtAffHouseholdId(request.getAffiliateHouseholdId());
		record.setClickId(request.getClickId());
		record.setStatus(STATUS.REDIRECTED);
		record.setStage(STAGE.PRE_ELIGIBILITY);
		record.setErrorMessage(errors.toString());
		preEligResult.setStatus(STATUS.REDIRECTED);
		preEligResult.setStage(STAGE.PRE_ELIGIBILITY);
		preEligResult.setErrorMessage(errors.toString());
		
		//HIX-64221 Support for 2016 Issuer Plan Preview
		try{
			if(StringUtils.isNotBlank(request.getEffectiveStartDate())) {
				SimpleDateFormat df = new SimpleDateFormat(SHORT_DATE_FORMAT);
				Date coverageStartDate = df.parse(request.getEffectiveStartDate());
				record.setCoverageStartDate(coverageStartDate);
				preEligResult.setCoverageStartDate(coverageStartDate);
			}
			else{
				record.setCoverageStartDate(computeEffectiveDate(request).getTime());
				preEligResult.setCoverageStartDate(computeEffectiveDate(request).getTime());
			}
			
			preEligResult.setCoverageYear(request.getCoverageYear());
		}
		catch(ParseException e){
			LOGGER.error("Unable to compute effective date",e);
		}
		
		preEligResult.setLeadId(record);
		preEligResult.setCreatedBy(record.getCreatedBy());
		preEligibilityResults.add(preEligResult);
		record.setPreEligibilityResults(preEligibilityResults);
		
		record = updateEligLeadRecord(record);
		
		return record;
	}
	
	/**
	 * Updates fields for ELIG_LEAD record table
	 * This method is called in the finally block of the estimator
	 * 
	 * @author - Nikhil Talreja
	 * @param coverageYear 
	 * @since - 14 August, 2013
	 */
	
	public void updateFiledsForEligLeadRecord(EligLead record, int coverageYear, MiniEstimatorResponse response) {
		
		LOGGER.info("Updating fields for " +record.getId() + " inside ELIG_LEAD table");
		
		record.setAptc(response.getAptcAmount());
		record.setPremium(response.getSlspMonthlyPremiumBeforeAPTC());
		record.setCsr(response.getCsrLevelSilver());
		
		record.setOverallApiStatus(response.getStatus());
		record.setGiExecutionDuration(response.getExecDuration());
		
		PreEligibilityResults result = getPreEligResultForCoverageYear(record, coverageYear);
		
		if(result != null){
			result.setAptc(response.getAptcAmount());
			result.setPremium(response.getSlspMonthlyPremiumBeforeAPTC());
			result.setCsr(response.getCsrLevelSilver());
			
			result.setOverallApiStatus(response.getStatus());
			if(response!=null ){
				try {
					record.setApiOutput(mapper.writeValueAsString(response));
					result.setApiOutput(mapper.writeValueAsString(response));
				} catch (Exception e) {
					LOGGER.error("Error fetching API data",e);
				}
			}
			setPreEligResultForCoverageYear(record,result,coverageYear);
			updateEligLeadRecord(record);
		}
	}
	
	/**
	 * Checks if a record exists in ELIG_LEAD table
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14 August, 2013
	 */
	public EligLead checkEligLeadRecord(long id){
		LOGGER.info("Fetching record " + id +" from ELIG_LEAD table");
		return phixService.getEligLeadRecord(id);
	}
	
	/**
	 * Updates a record in ELIG_LEAD table
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14 August, 2013
	 */
	public EligLead updateEligLeadRecord(EligLead record){
		LOGGER.info("Saving record in ELIG_LEAD table");
		return phixService.saveEligLeadRecord(record);
	}
	
	/**
	 * Fetches a record from STATE_MEDICAID_CHIP table based on state code
	 * 
	 * @author - Nikhil Talreja
	 * @since - 10 September, 2013
	 */
	public StateChipMedicaid getStateChipMedicaidRecordForState(String stateCode){
		LOGGER.info("Fetching record from STATE_MEDICAID_CHIP table for state " + stateCode);
		return phixService.getStateChipMedicaidRecordByState(stateCode);
	}
	
	/**
	 * Finds the no. of applicants in a request.
	 * An applicant is a member for whom date of birth is input
	 * 
	 * @author - Nikhil Talreja
	 * @since - 19 August, 2013
	 */
	public int findNoOfApplicants(List<Member> members){
		
		if(CollectionUtils.isEmpty(members)){
			return 0;
		}
		
		int noOfApplicants = 0;
		
		for(Member member : members){
			if(StringUtils.isNotBlank(member.getDateOfBirth())){
				noOfApplicants++;
			}
		}
		
		return noOfApplicants;
	}
	
	/**
	 * Handles exception occured while calling estimator API
	 * 
	 * @author - Nikhil Talreja
	 * @param coverageYear 
	 * @since - 23 September, 2013
	 */
	public void handleEstimatorException(MiniEstimatorResponse response, EligLead record, int coverageYear, Exception e, int errorMsgLength){
		
		PreEligibilityResults result = getPreEligResultForCoverageYear(record, coverageYear);
		
		response.getErrors().clear();
		response.getErrors().put(TECHNICAL_ISSUES_ERROR_CODE,TECHNICAL_ISSUES_ERROR_MESSAGE);
		response.setStatus(FAILURE);
		resetResponse(response);
		response.endResponse();
		if(record!=null){
			
			if(StringUtils.length(e.getMessage()) <= errorMsgLength){
				record.setErrorMessage(e.getMessage());
			}
			else if(StringUtils.length(""+e.fillInStackTrace()) <= errorMsgLength){
				record.setErrorMessage(""+e.fillInStackTrace());
			}
			else{
				record.setErrorMessage(TECHNICAL_ISSUES_ERROR_MESSAGE);
			}
			
		}
		if(result!=null){
			
			if(StringUtils.length(e.getMessage()) <= errorMsgLength){
				result.setErrorMessage(e.getMessage());
			}
			else if(StringUtils.length(""+e.fillInStackTrace()) <= errorMsgLength){
				result.setErrorMessage(""+e.fillInStackTrace());
			}
			else{
				result.setErrorMessage(TECHNICAL_ISSUES_ERROR_MESSAGE);
			}
			setPreEligResultForCoverageYear(record,result,coverageYear);
		}
	}
	
	/**
	 * This method checks if any member is eligible for a type of subsidy and returns the count
	 * 
	 * @author - Nikhil Talreja
	 * @since 03 October, 2013
	 * 
	 */
	public int findNoOfMembersForEligibilityType(List<Member> members,String type) {
		
		if(CollectionUtils.isEmpty(members)){
			return 0;
		}
		
		int count = 0;
		for(Member member : members){
			if(StringUtils.equalsIgnoreCase(member.getMemberEligibility(), type)){
				count++;
			}
		}
		return count;
	}
	
	/**
	 * @author Sunil Desu
	 * @since September 27 2013
	 * @see HIX-19317
	 * 
	 * Gets the list of members to considered for quoting
	 * 
	 */
	private List<Member> getEligibileMembersForQuoting(MiniEstimatorResponse response, double fpl, MiniEstimatorRequest request) throws ParseException {
		List<Member> eligibileMembersForQuoting = new ArrayList<Member>();	
		List<Member> eligibileChildrenForQuoting = new ArrayList<Member>();
		for(Member member : response.getMembers()){			
			LOGGER.debug("Member eligibility " + member.getMemberEligibility());	
			
			//Only applicants who are seeking coverage and not eligible for public programs
			if(!NON_APPLICANTS.contains(member.getMemberEligibility()) && 
					StringUtils.equalsIgnoreCase(member.getIsSeekingCoverage(), YES)){	
				if(member.getRelationshipToPrimary() == MemberRelationships.CHILD){
					 eligibileChildrenForQuoting.add(member);
				}
				else{
					 eligibileMembersForQuoting.add(member);
				}
			}
			/*
			 * Condition 1 : Member is ineligible for subsidy
			 * Condition 2 : Member is seeking coverage
			 * Condition 3 : FPL between 0 and 100 OR FPL > 400
			 */
			//else if((member.getMemberEligibility().equalsIgnoreCase(INELIGIBLE) && fpl > APTC_THRESHOLD && StringUtils.equalsIgnoreCase(member.getIsSeekingCoverage(), YES))||
			//		 (member.getMemberEligibility().equalsIgnoreCase(INELIGIBLE) && fpl < HUNDRED_POINT_ZERO && StringUtils.equalsIgnoreCase(member.getIsSeekingCoverage(), YES))){
			else if(member.getMemberEligibility().equalsIgnoreCase(INELIGIBLE) && StringUtils.equalsIgnoreCase(member.getIsSeekingCoverage(), YES) &&
					(fpl > APTC_THRESHOLD || fpl < HUNDRED_POINT_ZERO) ){
				if(member.getRelationshipToPrimary() == MemberRelationships.CHILD){
					 eligibileChildrenForQuoting.add(member);
				}
				else{
					 eligibileMembersForQuoting.add(member);
				}
			}
		}
		
		/*
		 * HIX-24912 - Cap 3 childrens in Pre Eligibility
		 * 
		 * Find eldest 3 children (if no. of children is more than 3)
		 * and add only those 3 children to eligible members for quoting list
		 */
		if(eligibileChildrenForQuoting.size() <= MAX_NO_OF_CHILDREN_FOR_QUOTING){
			eligibileMembersForQuoting.addAll(eligibileChildrenForQuoting);
		}
		else{
			LOGGER.debug("Only the eldest "+MAX_NO_OF_CHILDREN_FOR_QUOTING+" children in the household will be considered for quoting");
			eligibileMembersForQuoting.addAll(getEldestChildren(eligibileChildrenForQuoting, request));
		}
		
		return eligibileMembersForQuoting;
	}
	
	/**
	 * Returns a list of children sorted in ascending order of age
	 * 
	 * @author Nikhil Talreja
	 * @throws ParseException 
	 * @since 19 December, 2013
	 */
	public List<Member> getEldestChildren(List<Member> children, MiniEstimatorRequest request) throws ParseException{
		
		for(int  i=0; i < children.size() - 1 ;i++){
			for(int  j=0; j < children.size() - i - 1;j++){
				if(getAgeFromDob(children.get(j).getDateOfBirth(), request) < 
						getAgeFromDob(children.get(j+1).getDateOfBirth(), request)){
					Member temp = children.get(j);
					children.set(j, children.get(j+1));
					children.set(j+1, temp);
				}
			}
		}
		LOGGER.debug("Eldest children " + children.subList(0, MAX_NO_OF_CHILDREN_FOR_QUOTING));
		
		return children.subList(0, MAX_NO_OF_CHILDREN_FOR_QUOTING);
	}
	
	
	/** HIX-21393
	 *  Step 8 : Update GI_EXCHANGE_FLOW_TYPE in ELIG_LEAD based on following rules:
	 *  Off Exchange = 
	 *		1. aptcEligible=N & medicaidHousehold=N & medicareHousehold=N & chipHousehold=N 
	 *		2. aptcEligible=Y & aptcAmount=$0 
	 *   On Exchange = aptcEligible=Y & aptcAmount > $0 or n/a 
	 * @author - Nikhil Talreja
	 * @since 26 November, 2013
	 */
	protected String determineFlowType(MiniEstimatorResponse response,EligLead record){
		
		LOGGER.debug("Determining exchange flow type : ON/OFF/PUBLIC");
		double aptc = determineAptcAmount(response.getAptcAmount());
		
		if(StringUtils.equalsIgnoreCase(response.getAptcEligible(),NO)
				&& determineAllPublicHousehold(response)){
			record.setExchangeFlowType(ExchangeFlowType.OFF);
		}
		else if(StringUtils.equalsIgnoreCase(response.getAptcEligible(),YES) && new Double(aptc).equals(0)){
			record.setExchangeFlowType(ExchangeFlowType.OFF);
		}
		else if(StringUtils.equalsIgnoreCase(response.getAptcEligible(),YES) && 
				(aptc > 0 || StringUtils.equalsIgnoreCase(response.getAptcAmount(), NOT_APPLICABLE))){
			record.setExchangeFlowType(ExchangeFlowType.ON);
		}
		else if(StringUtils.equalsIgnoreCase(response.getMedicaidHousehold(), YES)
				|| StringUtils.equalsIgnoreCase(response.getChipHousehold(), YES)
				|| StringUtils.equalsIgnoreCase(response.getMedicareHousehold(), YES)
				|| findNoOfMembersForEligibilityType(response.getMembers(),CHIP) + 
					findNoOfMembersForEligibilityType(response.getMembers(),MEDICARE) == org.apache.commons.collections.CollectionUtils.size(response.getMembers())){
			record.setExchangeFlowType(ExchangeFlowType.PUBLIC);
		}
		LOGGER.debug("Exchange flow type : " + record.getExchangeFlowType());
		return record.getExchangeFlowType().toString();
	}
	/** HIX-31611
	 *  In order for Plan Management to identify whether there are plans in On/Off Exchange separately, 
	 *  send to Plan Management On / Off Exchange flag based on aptcEligible value 
	 *  
	 *  1. If (aptcEligible="null" or if aptcEligible="N/A" or aptcEligible="$0") & medicaidHousehold=N & medicareHousehold=N then Off Exchange 
	 *  2. If aptcEligible > $0, then On Exchange
	 * @author - Nikhil Talreja
	 * @since 26 November, 2013
	 */
	protected String determineFlowType(MiniEstimatorResponse response){
		
		String exchangeType = null;
		
		LOGGER.debug("Determining exchange type for Plan available info : ON/OFF");
		double aptc = determineAptcAmount(response.getAptcAmount());
		
		if(aptc <= 0 
				&& StringUtils.equalsIgnoreCase(response.getMedicaidHousehold(),NO)
				&& StringUtils.equalsIgnoreCase(response.getMedicareHousehold(),NO)){
			exchangeType = "OFF";
		}
		
		if(aptc > 0){
			exchangeType = "ON";
		}
		
		LOGGER.debug("Exchange type : " + exchangeType);
		
		return exchangeType;
	}
	
	/**
	 * Finds numeric APTC amount from APTC Amount string
	 * E.g. $360.5 returns 360.5
	 * JIRA: HIX-24323
	 * 
	 * @author - Nikhil Talreja
	 * @since 26 November, 2013
	 */
	protected double determineAptcAmount(String aptcAmount) {
		
		try{
			return Double.parseDouble(aptcAmount.replaceAll("\\$", ""));
		}
		catch(Exception e){
			//In case of APTC = N/A or blank
			LOGGER.error("Error occured in determineAptcAmount returning '-1' :: "+e);
			return -1;
		}
		
		
	}
	
	/**
	 * Finds if entire household is eligible for public programs
	 * JIRA: HIX-24323
	 * 
	 * @author - Nikhil Talreja
	 * @since 26 November, 2013
	 */
	protected boolean determineAllPublicHousehold(MiniEstimatorResponse response){
		
		if(StringUtils.equalsIgnoreCase(response.getMedicaidHousehold(), NO)
				&& StringUtils.equalsIgnoreCase(response.getChipHousehold(), NO)
				&& StringUtils.equalsIgnoreCase(response.getMedicareHousehold(), NO)
				&& findNoOfMembersForEligibilityType(response.getMembers(),CHIP) + 
					findNoOfMembersForEligibilityType(response.getMembers(),MEDICARE) != org.apache.commons.collections.CollectionUtils.size(response.getMembers())){
			return true;
		}
		
		return false;
	}
	
	/**
	 * Gets the pre eligibility results for a lead for a particular year
	 * 
	 * @author Nikhil Talreja
	 * 
	 * @param lead
	 * @param newResult
	 * @param coverageYear
	 * @return
	 */
	protected PreEligibilityResults getPreEligResultForCoverageYear(EligLead lead, int coverageYear){
		
		if(lead != null){
			List<PreEligibilityResults> results = lead.getPreEligibilityResults();
			if(results != null && !results.isEmpty()){
				for(PreEligibilityResults result: results){
					if(result.getCoverageYear() == coverageYear){
						return result;
					}
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Sets the pre eligibility results for a lead for a particular year
	 * 
	 * @author Nikhil Talreja
	 * 
	 * @param lead
	 * @param newResult
	 * @param coverageYear
	 * @return
	 */
	protected EligLead setPreEligResultForCoverageYear(EligLead lead, PreEligibilityResults newResult, int coverageYear){
		
		if(lead != null){
			List<PreEligibilityResults> results = lead.getPreEligibilityResults();
			if(results != null && !results.isEmpty()){
				for(int i=0; i<results.size(); i++){
					PreEligibilityResults result = results.get(i);
					if(result.getCoverageYear() == coverageYear){
						results.set(i,newResult);
						return lead;
					}
				}
			}
		}
		
		return lead;
	}
	
public int checkForPregnantMembers(MiniEstimatorRequest request) {
	int familySize = 0;
	if(request!=null && request.getMembers()!=null){
		familySize = request.getFamilySize();
			for (Member member : request.getMembers()) {
				if(member.getIsPregnant().equalsIgnoreCase("Y")) {
					// Increment the Family size by 1 for each pregnant member
					familySize++;
				}
			}
		}
		
		return familySize;
	}	
	
}
