<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />

<!-- Content -->
      <div class="row-fluid">
        
    <!-- #sidebar -->      
        <div class="span3">
          <div id="sidebar">
            <h4>Steps</h4>
            <ul class="nav nav-list bs-docs-sidenav">
              <li class="step-finished"><a href="entitieInfo.html">Entity Information</a></li>
              <li class="step-finished"><a href="populationServed.jsp">Populations Served</a></li>
              <ul>
             	 <li class="step-active"><a href="/hix/entity/enrollmententity/viewsite?isPrimary=true">Location and Hours</a></li>
             	 <li id ="primarySiteId" class="step-not-started"><a href="#">Primary Site</a></li>
             	 <li id="subSiteId" class="step-not-started"><a href="/hix/entity/enrollmententity/homesite?isPrimary=false">Sub Sites</a></li>
              </ul>
              <li class="step-not-started"><a href="entityContact.html">Entity Contacts</a></li>
              <li class="step-not-started"><a href="">Assisters</a></li>
              <li class="step-not-started"><a href="">Payment Information</a></li>
            </ul>
          </div><!-- #sidebar .affix --> 
        </div><!-- .span3 --> 
    <!-- #sidebar ENDS -->
        
    <!-- #section -->
        <div class="span9" id="rightpanel">
          <div id="section">
          	<h4 class="header-container" id="addSubSiteStepHeader" style="display:none">Step 2:Add SubSites</h4>
            <h4 class="header-container" id="addPrimarySiteStepHeader" style="display:none">Step 2:Location and Hours</h4>
   
             <div class="control-group">
               <a class="btn btn-primary"  id="addSubSite" style="display:none" href="<c:url value="/entity/enrollmententity/addSubsite"/>">Add Sub-Site</a>           
             </div>
            <div class="control-group">             
                <a class="btn btn-primary" id="subSiteDone" style="display:none" onClick="window.location.href='/hix/entity/enrollmententity/entitycontactinfo'">Done</a>
                <a class="btn btn-primary" id="subSiteBack" style="display:none" onClick="window.location.href='/hix/entity/enrollmententity/site?isPrimary=true&&isEdit=true'">Back</a>
             </div>
            
            <h5  class="header-container" id="subSiteHeader" style="display:none">SubSite</h5>
            	<div id="subSiteTable" class="control-group" style="display:none">
          			<div id="section">		 			
				         <table class="table table-striped table-bordered" id="siteList">
				         	<tr>				                     
				                      <th>Site Name</th>	
				                      <th>Site Location</th>
				                      <th>Edit Site</th>			                      
				                    </tr>				                  
				                  <tbody>
				                  <c:set var="string1" value="${siteList}"/>				                 
				                  <c:forEach items="${siteList}" var="site">
				                    <tr>
				                        			                    	
				                  		<td><c:out value="${site.siteLocationName}" /></td>
				                  		<td><c:out value="${site.physicalLocation.address1} ${site.physicalLocation.address2} ${site.physicalLocation.zip}"/></td>
				                 		 
				                 		<td>
				                 		<div class="dropdown">
				                 		<a class="dropdown-toggle" data-toggle="dropdown" href="#" ><i class="icon-cog"></i><i class="caret"></i></a>
										<ul class="dropdown-menu"><li><a id ="editId" href="<c:url value="/entity/enrollmententity/site?siteId=${site.id}&&isEdit=true&&isPrimary=false"/>"class="offset1">
											<i class="icon-pencil"></i>Edit</a></li></ul>
										</div>									
										</td>
																
				                  </tr>
				                  
				                  </c:forEach>
				                  
				         </table>
				         
				       
				     </div>
				 </div>
			  
              <form class="form-horizontal" style="display:none" name="frmaddSite" id="frmaddSite"  method="POST" action="homesite">
           		<div class="control-group">
	                <label class="control-label" id="siteNamelbl" name="siteNamelbl" for="siteName" style="display:none"></label>
	                <div class="controls">
	                  <input type="text" id="siteLocationName" name ="siteLocationName" placeholder="Site Name" value="${site.siteLocationName}">
	               	  
	                </div><!-- .controls -->
	            </div><!-- .control-group -->
          
              <div class="control-group">
                <label class="control-label" for="primaryEmailAddress">Primary Email Address</label>
                <div class="controls">
                  <input type="text" id="primaryEmailAddress" name="primaryEmailAddress" placeholder="Primary Email Address" value="${site.primaryEmailAddress}">
                </div><!-- .controls -->
              </div><!-- .control-group -->	
              
              <div class="control-group">
                <label class="control-label" for="primaryPhone">Primary Phone Number</label>
                <div class="controls">
                  <input type="text" name="primaryPhone1" id="primaryPhone1" value="${primaryPhone1}" size="3" maxlength="3" class="input-mini" placeholder="408"/>
                  <input type="text" name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}" size="3" maxlength="3" class="input-mini" placeholder="833"/>
                  <input type="text" name="primaryPhone3" id="primaryPhone3" value="${primaryPhone3}" size="4" maxlength="4" class="input-mini" placeholder="1861"/>  
                  <input type="hidden" name="primaryPhoneNumber" id="primaryPhoneNumber" value="${site.primaryPhoneNumber}" /> 
                  <div id="primaryPhone3_error"></div>   
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">
                <label class="control-label" for="secondaryPhone">Secondary Phone Number</label>
                <div class="controls">
                   <input type="text" name="secondaryPhone1" id="secondaryPhone1" value="${secondaryPhone1}" size="3" maxlength="3" class="input-mini" placeholder="408"/>
                  <input type="text" name="secondaryPhone2" id="secondaryPhone2" value="${secondaryPhone2}" size="3" maxlength="3" class="input-mini" placeholder="453"/>
                  <input type="text" name="secondaryPhone3" id="secondaryPhone3" value="${secondaryPhone3}" size="4" maxlength="4" class="input-mini" placeholder="9978"/>
                  <input type="hidden" name="secondaryPhoneNumber" id="secondaryPhoneNumber" value="${site.primaryPhoneNumber}" /> 
				   <div id="secondaryPhone3_error"></div>               
                </div><!-- .controls -->
              </div><!-- .control-group -->
               <div class="control-group">
          		  <div id="from-to-day">
            		  <h5>Hours of Operation</h5>
            				<c:forEach var="day" items="${daysList}">
					                <div class="control-group" id="from-to">
					                		
							                <label id="dayId" class="control-label" for="${day.lookupValueLabel}"> ${day.lookupValueLabel}</label>
							                <div class="controls">				          
												<select size="1" id="${day.lookupValueLabel}From" name="${day.lookupValueLabel}From" class="input-medium">
													<option value="">Select</option>
													<c:forEach var="fromhour" items="${fromTimelist}">
														<option id="DayStart:${day.lookupValueLabel}" class="hourclass" value="<c:out value="${fromhour.lookupValueCode}" />"<c:if test="${fromhour.lookupValueCode == siteLocationHoursMap[day.lookupValueLabel].fromTime}"> SELECTED </c:if>>
														${fromhour.lookupValueLabel}
														</option>
															
													</c:forEach>
												</select>		
												
											<select size="1" id="${day.lookupValueLabel}To" name="${day.lookupValueLabel}To" class="input-medium">
											<option value="">Select</option>
											<c:forEach var="tohour" items="${toTimelist}">
												<option id="DayEnd:${day.lookupValueLabel}" class="hourclass" value="<c:out value="${tohour.lookupValueCode}" />"<c:if test="${tohour.lookupValueCode == siteLocationHoursMap[day.lookupValueLabel].toTime}"> SELECTED </c:if>>
														${tohour.lookupValueLabel}
												</option>
											</c:forEach>
										</select>	
												
									</div>										
								 </div>
							</c:forEach>			
							
							</div>
							</div>
     						<input type="hidden" id="siteLocationHoursHidden" name="siteLocationHoursHidden" value=""/>
     																															          
                <div class="control-group">
          		  <div id="section">
            		  <h5>Physical Address</h5>
					      <div class="control-group">
							  <label class="control-label" for="address1">Street Address</label>
								<div class="controls">
									<input type="text" id="address1" name="physicalLocation.address1" placeholder="Street Address" value="${site.physicalLocation.address1}">
									<input type="hidden" id="address1_physical_hidden" name="address1_physical_hidden" value="${site.physicalLocation.address1}">
									<div id="address1_error"></div> 
								</div>
							</div>
							<div class="control-group">
	            	   		<label class="control-label" for="physicalLocation.address2">Suite</label>
		                		<div class="controls">
		                  			<input type="text" id="physicalLocation.address2" name ="physicalLocation.address2" placeholder="Suite" value="${site.physicalLocation.address2}">
		                  			<input type="hidden" id="address2_physical_hidden" name="address2_physical_hidden" value="${site.physicalLocation.address2}">
		            	   		</div>
		            	   	</div>
		            	 <div class="control-group">
							<label for="physicalLocation.state" class="control-label">State</label>
							<div class="controls">
								<select size="1" id="physicalLocation.state" name="physicalLocation.state" class="input-medium">
								<option value="">Select</option>
								<c:forEach var="state" items="${statelist}">
									<option id="${state.code}" <c:if test="${state.code == site.physicalLocation.state}"> selected="selected" </c:if> value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<div id="physicalLocation.state_error"></div> 		
							<input type="hidden" id="state_mailing_hidden" name="state_mailing_hidden" value="${site.physicalLocation.state}">								
						</div><!-- end of control-group -->        	   		
            	 	</div>
		            	   	<div class="control-group">	
	            	   		<label class="control-label" for="physicalLocation.zip">Zip Code</label>
		                		<div class="controls">
		                  			<input type="text" id="physicalLocation.zip" name="physicalLocation.zip" placeholder="Zip Code" value="${site.physicalLocation.zip}">
		                  			<input type="hidden" id="zip_physical_hidden" name="zip_physical_hidden" value="${site.physicalLocation.zip}">
		            	   			<div id="physicalLocation.zip_error"></div> 
		            	   		</div>
		            	   	</div>
            	</div> 	                	

            	 <div class="control-group">
          		  <div id="section">
            		  <h5>Mailing Address</h5>
            		  	<div class="control-group">
		            		  <label class="control-label" for="mailingLocation.address1">Street Address</label>
		                		<div class="controls">
		                  			<input type="text" id="mailingLocation.address1" name="mailingLocation.address1" placeholder="Street Address" value="${site.mailingLocation.address1}">
		                  			<input type="hidden" id="address1_mailing_hidden" name="address1_mailing_hidden" value="${site.mailingLocation.address1}">
		            	   			
		            	   		</div>
	            	   		</div>
	            	   		<div class="control-group">
		            	   		<label class="control-label" for="mailingLocation.address2">Suite</label>
			                		<div class="controls">
			                  			<input type="text" id="mailingLocation.address2" name="mailingLocation.address2" placeholder="Suite" value="${site.mailingLocation.address2}">
			                  			<input type="hidden" id="address2_mailing_hidden" name="address2_mailing_hidden" value="${site.mailingLocation.address2}">
			            	   		</div>
			            	 </div> 
			            	  <label for="mailingLocation.state" class="control-label">State</label>
							<div class="controls">
								<select size="1" id="mailingLocation.state" name="mailingLocation.state"  class="input-medium">
								<option value="">Select</option>
								<c:forEach var="state" items="${statelist}">
									<option id="${state.code}" <c:if test="${state.code == site.mailingLocation.state}"> selected="selected" </c:if> value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<input type="hidden" id="state_mailing_hidden" name="state_mailing_hidden" value="${site.mailingLocation.state}">								
						</div><!-- end of control-group -->
						   
			            	 <div class="control-group"> 		
		            	   			<label class="control-label" for="mailingLocation.zip">Zip Code</label>
			                		<div class="controls">
			                  			<input type="text" id="mailingLocation.zip" name="mailingLocation.zip" placeholder="Zip Code" value ="${site.mailingLocation.zip}">
			          					<input type="hidden" id="zip_mailing_hidden" name="zip_mailing_hidden" value="${site.mailingLocation.zip}">
			            	   		</div>
			            	  </div>
			          </div>	            	   		    	   		
            	 	</div>
            	<div>
            	<h5 data-toggle="collapse" data-target="#languagesSupported" class="trigger">Languages Supported</h5>
            	<div class="control-group">
          			<div id="section">
		                <p>Check all that apply.</p>
				               <table  id="languageTable" class="table table-striped">
									 <tr>				                     
				                      <th>Spoken Languages Supported</th>	
				                      <th>Written Languages Supported</th>				                      
				                    </tr>				                  
				                  <tbody>
				                  <c:forEach var="language" items="${listOflanguages}">
				                    <tr>				                      
				                      <td id="spoken"><input type="checkbox" name='spoken' id="spokenLanguagesId"  value="${language.lookupValueLabel}" ${fn:contains(siteLanguages.spokenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
				                      <c:out value="${language.lookupValueLabel}" /></td>
				                      <td><input type="checkbox" id="writtenLanguagesId" name='written' value="${language.lookupValueLabel}" ${fn:contains(siteLanguages.writtenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
				                      <c:out value="${language.lookupValueLabel}" /></td>
				                    </tr>
				                  </c:forEach>  
				                  
				              </tbody>
				              
				              </table>
				              <input type="hidden" id="spokenLanguages" name="spokenLanguages" value=""/>
				               <input type="hidden" id="writtenLanguages" name="writtenLanguages" value=""/>
				       </div>
				 </div>    
            </div> 	         				
		      </div>                                      
                       
            	   
            	   <input type="hidden" id="addSubsiteForm" name="addSubsiteForm" value=""/>
            	      <div class="well well-small clear"> 
            	    <c:choose>
							    <c:when test="${isPrimarySite == 'true'}">							            
							    	 <input type="button" name="back" id="primarySiteBack" value="Back" class="btn" onClick="window.location.href='/hix/entity/enrollmententity/populationServed'"/>
							    	 <input type="submit" name="save" id="save" class="btn btn-primary offset8" value="Next" /> 					    		   		
							    	  <input type="hidden" name="isPrimary" value="${isPrimarySite}"/>
							    </c:when>
							    <c:otherwise>
							    	<input type="button" name="back" id="subSiteBack" class="btn" value="<Back"  onClick="window.location.href='/hix/entity/enrollmententity/site?isPrimary=true'"/>							  
							    	<input type="submit" name="save" id="save" class="btn btn-primary offset8" value="Save &#38; Continue" /> 
							    	 <input type="hidden" name="isPrimary" value="false"/>
							    </c:otherwise>
							</c:choose>    	   		
            	   </div>      
	        
	         <input type="hidden" name="siteId" id ="siteId" value="${site.id}"/>
	         <input type="hidden" name="siteLanguageId" value="${siteLanguages.id}"/>
          </form> 
          
          </div><!-- #section --> 
        </div><!-- .span9 -->
    <!-- #section ENDS -->
        
      </div><!-- .row-fluid -->
 


  
<script type="text/javascript">
function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
        }
	}
}
$(document).ready(function() {  
	ie8Trim();
	var isPrimary='${isPrimarySite}';
	if(isPrimary=="true") {	
			$("#subSiteHeader").hide();
			
			$("#addSubSiteStepHeader").hide();
			$("#addSubSite").hide();
			$("#subSiteHeader").hide();
			$("#subSiteDone").hide();
		
			$("#subSiteTable").hide();
			$("#frmaddSite").show();
			$("#siteNamelbl").show();
			$("#siteNamelbl").text("Primary Site Name");
			$("#nextPrimary").show();
		
			$("#addPrimarySiteStepHeader").show();
			
			$("#primarySiteId").addClass("step-active");
			$("#subSiteId").attr("href", "#");
		}
	  else
		{	$("#addSubSiteStepHeader").show();
			$("#subSiteDone").show();
			
			$("#primarySiteId").addClass("step-not-started");
			$("#subSiteId").addClass("step-active");
			$("#subSiteHeader").show();
			$("#addSubSite").show();
			$("#subSiteBack").show();
			$("#primarySiteId").attr("href", "#");			
			$("#siteNamelbl").show();
			$("#siteNamelbl").text("Sub Site Name");
			var siteAdd='${siteAdd}';
			if(siteAdd =='true')
				{
				
				$("#frmaddSite").show();
				}
			var siteNumber='${siteNumber}';
			
				if(siteNumber>0)
					{
						$("#subSiteHeader").show();
						$("#subSiteTable").show();
					}
				else
					{
						$("#subSiteHeader").hide();
						$("#subSiteTable").hide();
					}
				var isEdit='${isEdit}';
				
				if(isEdit=="true")
					{
						$("#frmaddSite").show();
						$("#subSiteHeader").hide();
						$("#subSiteTable").hide();
						$("#subSiteDone").hide();
						$("#subSiteBack").hide();
						$("#addSubSite").hide();
						$("#addSubSiteStepHeader").hide();
					}
		}		
});
$(function() {
	$("#addSubSite").click(function(e){
		ie8Trim();
		$("#frmaddSite").show();
	});

// 	$('#frmaddSite').submit(function (event)
// 			{
// 			    var action = '';
// 				var isPrimary='${isPrimarySite}';
// 			    // compute action here...
// 			    if(isPrimary=='true')
// 			    {
// 			    	$(this).attr('action','homesite?isPrimary=true');
// 			    }
// 			    else
// 			    	{
// 			    	 $(this).attr('post','homesite?isPrimary=false');
// 			    	}
			    	
// 			});
//$(function() {
//	$("#test").click(function(){
	//var spoken='${siteLocationHoursMap["Monday"].fromTime}';
	//var language='${language.lookupvalueLabel}';
	//alert(spoken);
	
//	alert(language);
	
	//});
	$("#save").click(function(){
		ie8Trim();
		
		var siteLocationHoursHidden = '';
		var days='${daysListNames}';
		days=days.replace("[","");
		days=days.replace("]","");
		days=days.replace(" ","");
		var dayarray=days.split(',');

		for (var i = 0; i < dayarray.length; i++) {
			
			var fromId = dayarray[i] + 'From';
			fromId=fromId.replace(" ","");
			var fromVal = $("#" + fromId).val();
			
			var toId = dayarray[i] + 'To';
			toId=toId.replace(" ","");
			var toVal = $("#" + toId).val();
			//alert(fromVal + " : " + toVal);
			if(fromVal!="")
			siteLocationHoursHidden = siteLocationHoursHidden + dayarray[i] + ',' + fromVal + ',' + toVal + '~';
		}
		
		$("#siteLocationHoursHidden").val(siteLocationHoursHidden);
		var spokenVal ="";
		var writtenVal="";
		$.each($("input[name='spoken']:checked"), function() {
			var spokenValcurrent = $(this).attr('value');
			spokenVal=spokenValcurrent+","+spokenVal;
			
		});
		$.each($("input[name='written']:checked"), function() {
			var  writtenValcurrent = $(this).attr('value');
			writtenVal=writtenValcurrent+","+writtenVal;
			
		});
		 $("#spokenLanguages").val(spokenVal);
		 $("#writtenLanguages").val(writtenVal);
	});
});

var validator = $("#frmaddSite").validate({ 
	onkeyup: false,
	onclick: false,
	rules : {
		siteLocationName :{alphaNumeric :true},
		primaryPhone3 : {primaryphonecheck : true},
		secondaryPhone3 : {secondaryphonecheck : true},			
		"physicalLocation.zip" : {required : true, number: true,ZipCodecheck:true },
		"physicalLocation.state":{required : true},
		"physicalLocation.address1":{required : true},
		
	},
	messages : {
		siteLocationName :{alphaNumeric :"<span> <em class='excl'>!Please Enter Alpha Numeric value</span>"},
		primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},
	    secondaryPhone3: { secondaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNo' javaScriptEscape='true'/></span>"},
	    "physicalLocation.zip" :{ required :"<span> <em class='excl'>!</em>Please enter valid Zip Code.</span>"}, 
	    "physicalLocation.state":{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
	    "physicalLocation.address1" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress1' javaScriptEscape='true'/></span>"},
	   
	}
	,
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

jQuery.validator.addMethod("alphaNumeric", function(value, element, param) {
	ie8Trim();
	 return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
	 
});

jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {
	ie8Trim();
	primphone1 = $("#primaryPhone1").val().trim(); 
	primphone2 = $("#primaryPhone2").val().trim(); 
	primphone3 = $("#primaryPhone3").val().trim(); 
 
	if( (primphone1 == "" || primphone2 == "" || primphone3 == "")  || (isNaN(primphone1)) || (primphone1.length < 3 ) || (isNaN(primphone2)) || (primphone2.length < 3 ) || (isNaN(primphone3)) || (primphone3.length < 4 )  )
	{
		return false;
	}
	else
	{
		$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
		return true;
	}	
} );

jQuery.validator.addMethod("secondaryphonecheck", function(value, element, param) {
	ie8Trim();
	secondaryphone1 = $("#secondaryPhone1").val().trim(); 
	secondaryphone2 = $("#secondaryPhone2").val().trim(); 
	secondaryphone3 = $("#secondaryPhone3").val().trim(); 
 
	if( (secondaryphone1.length < 3 ) || (secondaryphone2.length < 3 )  || (secondaryphone3.length < 4 )  )
	{
		return false;
	}
	else
	{
		$("#secondaryPhoneNumber").val(secondaryphone1 + secondaryphone2 + secondaryphone3);		
		return true;
	}	
} );
jQuery.validator.addMethod("ZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#zip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000") )){ 
		return false; 
	}
	return true;
});
</script>	


	