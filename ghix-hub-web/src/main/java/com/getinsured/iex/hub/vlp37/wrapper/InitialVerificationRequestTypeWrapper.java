package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationRequestSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;
import com.getinsured.iex.hub.vlp37.wrapper.util.InitialVerificationRequestTypeWrapperUtil;


public class InitialVerificationRequestTypeWrapper implements JSONAware{
	
	private static ObjectFactory factory = null;
	
	private InitialVerificationRequestSetType request = null;
	
	
	private DHSIDTypeWrapper dhsid;
	private String firstName;
	private String middleName;
	private String lastName;
	private String dateOfBirth;
	private String aka;
	private boolean fiveYearBarApplicabilityIndicator;
	private boolean requestSponsorDataIndicator;
	private boolean requestGrantDateIndicator;
	private String requesterCommentsForHub;
	
	private boolean suspectedCounterfeitAlteredDocumentIndicator;
	private boolean requestCubanHaitianEntrantIndicator;
	private byte[] documentBinaryAttachment;
	private String casePOCFullName;
	private String casePOCPhoneNumber;
	private String casePOCPhoneNumberExtension;
		
	private static final String DHSID_KEY = "DHSID";
	private static final String FIRSTNAME_KEY = "FirstName";
	private static final String MIDDLENAME_KEY = "MiddleName";
	private static final String LASTNAME_KEY = "LastName";
	private static final String DATEOFBIRTH_KEY = "DateOfBirth";
	private static final String AKA_KEY = "AKA";
	private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
	private static final String REQUESTSPONSORDATAINDICATOR_KEY = "RequestSponsorDataIndicator";
	private static final String REQUESTGRANTDATEINDICATOR_KEY = "RequestGrantDateIndicator";
	private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	private static final String  SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY = "SuspectedCounterfeitAlteredDocumentIndicator";
    private static final String  REQUESTCUBANHAITIANENTRANTINDICATOR_KEY = "RequestCubanHaitianEntrantIndicator";
    private static final String  DOCUMENTBINARYATTACHMENT_KEY = "DocumentBinaryAttachment";
    private static final String  CASEPOCFULLNAME_KEY = "CasePOCFullName";
    private static final String  CASEPOCPHONENUMBER_KEY = "CasePOCPhoneNumber";
    private static final String  CASEPOCPHONENUMBEREXTENSION_KEY = "CasePOCPhoneNumberExtension";
    
	
	
	public InitialVerificationRequestTypeWrapper(){
		factory = new ObjectFactory();
		request = factory.createInitialVerificationRequestSetType();
	}
	
	public void setRequest(InitialVerificationRequestSetType request) {
		this.request = request;
	}
	
	/**
	 * This method will also do the request specific validations
	 */
	public InitialVerificationRequestSetType getRequest() throws HubServiceException {
		
		if(this.request.getFirstName() == null){
			throw new HubServiceException("First Name is required for InitialVerificationRequest");
		}
		
		if(this.request.getLastName() == null){
			throw new HubServiceException("Last Name is required for InitialVerificationRequest");
		}
		
		if(this.request.getDateOfBirth() == null){
			throw new HubServiceException("Date of Birth is required for InitialVerificationRequest");
		}
		
		return this.request;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.request.setFirstName(firstName);
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.request.setMiddleName(middleName);
		this.middleName = middleName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.request.setLastName(lastName);
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) throws HubServiceException {
		
		try{
			this.request.setDateOfBirth(VLPServiceUtil.stringToXMLDate(dateOfBirth, VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}
		catch (Exception e){
			throw new HubServiceException(e);
		}
		this.dateOfBirth = dateOfBirth;
	}

	public String getAKA() {
		return aka;
	}

	public void setAKA(String aka) {
		this.request.setAKA(aka);
		this.aka = aka;
	}

	public boolean isFiveYearBarApplicabilityIndicator() {
		return fiveYearBarApplicabilityIndicator;
	}

	public void setFiveYearBarApplicabilityIndicator(
			boolean fiveYearBarApplicabilityIndicator) {
		this.fiveYearBarApplicabilityIndicator = fiveYearBarApplicabilityIndicator;
		this.request.setFiveYearBarApplicabilityIndicator(fiveYearBarApplicabilityIndicator);
	}

	public DHSIDTypeWrapper getDhsid() {
		return dhsid;
	}

	public void setDhsid(DHSIDTypeWrapper dhsid) {
		this.dhsid = dhsid;
		this.request.setDHSID(dhsid.getDhsId());
	}

	public boolean isRequestSponsorDataIndicator() {
		return requestSponsorDataIndicator;
	}

	public void setRequestSponsorDataIndicator(boolean requestSponsorDataIndicator) {
		this.requestSponsorDataIndicator = requestSponsorDataIndicator;
		this.request.setRequestSponsorDataIndicator(requestSponsorDataIndicator);
	}

	public boolean isRequestGrantDateIndicator() {
		return requestGrantDateIndicator;
	}

	public void setRequestGrantDateIndicator(boolean requestGrantDateIndicator) {
		this.requestGrantDateIndicator = requestGrantDateIndicator;
		this.request.setRequestGrantDateIndicator(requestGrantDateIndicator);
	}

	public String getRequesterCommentsForHub() {
		return requesterCommentsForHub;
	}

	public void setRequesterCommentsForHub(String requesterCommentsForHub) {
		this.requesterCommentsForHub = requesterCommentsForHub;
		this.request.setRequesterCommentsForHub(requesterCommentsForHub);
	}


	public boolean getSuspectedCounterfeitAlteredDocumentIndicator() {
		return suspectedCounterfeitAlteredDocumentIndicator;
	}

	public void setSuspectedCounterfeitAlteredDocumentIndicator(boolean suspectedCounterfeitAlteredDocumentIndicator) {
		this.suspectedCounterfeitAlteredDocumentIndicator = suspectedCounterfeitAlteredDocumentIndicator;
	}

	public boolean getRequestCubanHaitianEntrantIndicator() {
		return requestCubanHaitianEntrantIndicator;
	}

	public void setRequestCubanHaitianEntrantIndicator(boolean requestCubanHaitianEntrantIndicator) {
		this.requestCubanHaitianEntrantIndicator = requestCubanHaitianEntrantIndicator;
	}

	public byte[] getDocumentBinaryAttachment() {
		return documentBinaryAttachment;
	}

	public void setDocumentBinaryAttachment(byte[] documentBinaryAttachment) {
		this.documentBinaryAttachment = documentBinaryAttachment;
	}

	public String getCasePOCFullName() {
		return casePOCFullName;
	}

	public void setCasePOCFullName(String casePOCFullName) {
		this.request.setCasePOCFullName(casePOCFullName);
		this.casePOCFullName = casePOCFullName;
	}

	public String getCasePOCPhoneNumber() {
		return casePOCPhoneNumber;
	}

	public void setCasePOCPhoneNumber(String casePOCPhoneNumber) {
		this.request.setCasePOCPhoneNumber(casePOCPhoneNumber);
		this.casePOCPhoneNumber = casePOCPhoneNumber;
	}

	public String getCasePOCPhoneNumberExtension() {
		return casePOCPhoneNumberExtension;
	}

	public void setCasePOCPhoneNumberExtension(String casePOCPhoneNumberExtension) {
		this.setCasePOCPhoneNumberExtension(casePOCPhoneNumberExtension);
		this.casePOCPhoneNumberExtension = casePOCPhoneNumberExtension;
	}

	/**
	 * Converts the JSON String to Agency3InitVerifRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for Agency3InitVerifRequestTypeWrapper
	 * @return Agency3InitVerifRequestTypeWrapper Object 
	 */
	public static InitialVerificationRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I327DocumentID3Wrapper from null or empty input");
		}
		
		InitialVerificationRequestTypeWrapper wrapper = new InitialVerificationRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		param = obj.get(DHSID_KEY);
		if(param != null){
			JSONObject dhsId = (JSONObject)param;
			wrapper.setDhsid(DHSIDTypeWrapper.fromJsonObject(dhsId));
		}
		
		InitialVerificationRequestTypeWrapperUtil.addNameToWrapper(obj, wrapper);
		
		param = obj.get(AKA_KEY);
		if(param != null){
			wrapper.setAKA((String)param);
		}
		
		param = obj.get(FIRSTNAME_KEY);
		if(param != null){
			wrapper.setFirstName((String)param);
		}
		
		param = obj.get(MIDDLENAME_KEY);
		if(param != null){
			wrapper.setMiddleName((String)param);
		}
		
		param = obj.get(LASTNAME_KEY);
		if(param != null){
			wrapper.setLastName((String)param);
		}
		
		param = obj.get(DATEOFBIRTH_KEY);
		if(param != null){
			wrapper.setDateOfBirth((String)param);
		}
		
		param = obj.get(SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY);
		if(param != null){
			wrapper.setSuspectedCounterfeitAlteredDocumentIndicator((Boolean)param);
		}
		
		param = obj.get(REQUESTCUBANHAITIANENTRANTINDICATOR_KEY);
		if(param != null){
			wrapper.setRequestCubanHaitianEntrantIndicator((Boolean)param);
		}
		
		param = obj.get(DOCUMENTBINARYATTACHMENT_KEY);
		if(param != null){
		//	wrapper.setDocumentBinaryAttachment((String)param);
		}
		
		param = obj.get(CASEPOCFULLNAME_KEY);
		if(param != null){
			wrapper.setCasePOCFullName((String)param);
		}
		
		param = obj.get(CASEPOCPHONENUMBER_KEY);
		if(param != null){
			wrapper.setCasePOCPhoneNumber((String)param);
		}
		
		param = obj.get(CASEPOCPHONENUMBEREXTENSION_KEY);
		if(param != null){
			wrapper.setCasePOCPhoneNumberExtension((String)param);
		}
		
		InitialVerificationRequestTypeWrapperUtil.addIndicatorsToWrapper(obj, wrapper);
		
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.dhsid != null){
			obj.put(DHSID_KEY, this.dhsid.toJSONObject());
		}
		obj.put(FIRSTNAME_KEY, this.firstName);
		obj.put(MIDDLENAME_KEY, this.middleName);
		obj.put(LASTNAME_KEY, this.lastName);
		obj.put(DATEOFBIRTH_KEY, this.dateOfBirth);
		obj.put(AKA_KEY, this.aka);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTSPONSORDATAINDICATOR_KEY, this.requestSponsorDataIndicator);
		obj.put(REQUESTGRANTDATEINDICATOR_KEY, this.requestGrantDateIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		
		obj.put(SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY, this.suspectedCounterfeitAlteredDocumentIndicator);
		obj.put(REQUESTCUBANHAITIANENTRANTINDICATOR_KEY, this.requestCubanHaitianEntrantIndicator);
		obj.put(DOCUMENTBINARYATTACHMENT_KEY, this.documentBinaryAttachment);
		obj.put(CASEPOCFULLNAME_KEY, this.casePOCFullName);
		obj.put(CASEPOCPHONENUMBER_KEY, this.casePOCPhoneNumber);
		obj.put(CASEPOCPHONENUMBEREXTENSION_KEY, this.casePOCPhoneNumberExtension);
		
		return obj.toJSONString();

	}
	
	public InitialVerificationRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		
		InitialVerificationRequestTypeWrapper wrapper = new InitialVerificationRequestTypeWrapper();
		if(jsonObj.get(DHSID_KEY) != null){
			wrapper.setDhsid(DHSIDTypeWrapper.fromJsonObject((JSONObject) jsonObj.get(DHSID_KEY)));
		}
		wrapper.setFirstName((String)jsonObj.get(FIRSTNAME_KEY));
		wrapper.setMiddleName((String)jsonObj.get(MIDDLENAME_KEY));
		wrapper.setLastName((String)jsonObj.get(LASTNAME_KEY));
		wrapper.setDateOfBirth((String)jsonObj.get(DATEOFBIRTH_KEY));
		wrapper.setAKA((String)jsonObj.get(AKA_KEY));
		wrapper.setFiveYearBarApplicabilityIndicator(Boolean.parseBoolean((String)jsonObj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY)));
		wrapper.setRequestSponsorDataIndicator(Boolean.parseBoolean((String)jsonObj.get(REQUESTSPONSORDATAINDICATOR_KEY)));
		wrapper.setRequestGrantDateIndicator(Boolean.parseBoolean((String)jsonObj.get(REQUESTGRANTDATEINDICATOR_KEY)));
		wrapper.setRequesterCommentsForHub((String)jsonObj.get(REQUESTERCOMMENTSFORHUB_KEY));
		
		wrapper.setSuspectedCounterfeitAlteredDocumentIndicator((Boolean)jsonObj.get(SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY));
		wrapper.setRequestCubanHaitianEntrantIndicator((Boolean)jsonObj.get(REQUESTCUBANHAITIANENTRANTINDICATOR_KEY));
		
		//wrapper.setDocumentBinaryAttachment((String)jsonObj.get(DOCUMENTBINARYATTACHMENT_KEY));
		wrapper.setCasePOCFullName((String)jsonObj.get(CASEPOCFULLNAME_KEY));
		wrapper.setCasePOCPhoneNumber((String)jsonObj.get(CASEPOCPHONENUMBER_KEY));
		wrapper.setCasePOCPhoneNumberExtension((String)jsonObj.get(CASEPOCPHONENUMBEREXTENSION_KEY));
		
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		if(this.dhsid != null){
			obj.put(DHSID_KEY, this.dhsid.toJSONObject());
		}
		obj.put(FIRSTNAME_KEY, this.firstName);
		obj.put(MIDDLENAME_KEY, this.middleName);
		obj.put(LASTNAME_KEY, this.lastName);
		obj.put(DATEOFBIRTH_KEY, this.dateOfBirth);
		obj.put(AKA_KEY, this.aka);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTSPONSORDATAINDICATOR_KEY, this.requestSponsorDataIndicator);
		obj.put(REQUESTGRANTDATEINDICATOR_KEY, this.requestGrantDateIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		obj.put(SUSPECTEDCOUNTERFEITALTEREDDOCUMENTINDICATOR_KEY, this.suspectedCounterfeitAlteredDocumentIndicator);
		obj.put(REQUESTCUBANHAITIANENTRANTINDICATOR_KEY, this.requestCubanHaitianEntrantIndicator);
		obj.put(DOCUMENTBINARYATTACHMENT_KEY, this.documentBinaryAttachment);
		obj.put(CASEPOCFULLNAME_KEY, this.casePOCFullName);
		obj.put(CASEPOCPHONENUMBER_KEY, this.casePOCPhoneNumber);
		obj.put(CASEPOCPHONENUMBEREXTENSION_KEY, this.casePOCPhoneNumberExtension);
		return obj;
	}
}
