
	<script type="text/javascript">
jQuery.validator.addMethod("userNameCheck", function(value, element, param) {
	return $("#userNameCheck").val() == 0 ? false : true;
});

function chkUserName(){
	$.get('signup/chkUserName',
		{userName: $("#userName").val()},
		function(response) {
			if(response == "EXIST"){
				$("#userNameCheck").val(0);
			} else {
				$("#userNameCheck").val(1);
			}
		}
	);
}

var validator = $("#frmbrokerreg").validate({ 
	rules : {
		firstName : { required : true},
		lastName : { required : true},
		email : { required : true},
		userName : { required: true,
			userNameCheck : true}},
	messages : {
		firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>"},
		lastName : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>"},
		email :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>"},
		userName :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validateUserName' javaScriptEscape='true'/></span>",
			userNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateUserNameCheck' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo($("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class',
			'error help-inline');
	}
});
	</script>
<div class="gutter10">
	<div class="row-fluid">
		<h1><a name="skip" class="skip">Latest Notifications</a></h1>
	</div>

	<!-- start of secondary navbar - TO DO: Move to topnav.jsp -->
<!-- 	<ul class="nav nav-tabs"> -->
<!-- 		<li class="active"><a href="#">Home</a></li> -->
<!-- 		<li><a href="#">Members</a></li> -->
<!-- 		<li><a href="#">Health Plans</a></li>    -->
<!-- 		<li><a href="#">Payments</a></li>    -->
<!-- 		<li><a href="#">Reports</a></li> -->
<!-- 		<li><a href="#">Outreach</a></li> -->
<!-- 		<li><a href="#">Get Assistance</a></li> -->
<!-- 	</ul> -->
	<!-- end of secondary navbar -->

	<div class="row-fluid">
		<div class="span3" id="sidebar">
		<div class="header">
			<h4>Agent Home</h4>
		</div>
			<!--  start of side bar -->
			<ul class="nav nav-list">
				<li class="active"><a href="../broker/notifications">Notifications</a></li>
				<li><a href="../broker/viewprofile">Profile</a></li>
				<li><a href="../broker/certificationstatus">Certification Status</a></li>
				<li><a href="../broker/complaintsfeedback">Complaints and Feedback</a></li>
			</ul><!-- end of side bar -->

		</div><!-- end of span3 -->
		<div class="span9" id="rightpanel">

			<form class="form-vertical" id="frmbrokerreg" name="frmbrokerreg" action="signup" method="POST">
				<div class="gutter10">
					<p>
						<span class="badge">New</span>
						<span>Member Name has designated you as Agent to aid in enrollment.</span><span>12 May 13</span>
					</p>

					<p>
						<span class="badge">New</span>
						<span>Consumer complaint (dated 5/1/13, #123) against you has been dismissed by the GHIX Administrator.</span> <span>12 May 13</span>
					</p>

					<p>
						<span class="badge">New</span>
						<span>Application sent to State Health Benefit Exchange Administration to enroll Member Name has been received.</span> <span>12 May 13</span>
					</p>

					<p>
						<span class="badge">New</span>
						<span>Profile information updated. Added Languages. Areas Serviced.</span> <span>12 May 13</span>
					</p>
				</div><!-- end of gutter10 -->

				<div class="form-actions">
					<input type="submit" name="submit" id="submit" value="All Notifications" class="btn btn-primary" title="All Notifications"/> 
				</div><!-- end of form-actions -->

			</form>
		</div><!-- end of span9 -->
	</div><!--  end of .row-fluid -->
</div>
