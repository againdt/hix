package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for case number
 * 
 * @author Nikhil Talreja
 * @since 06-Feb-2014
 *
 */
public class CaseNumberValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CaseNumberValidator.class);
	
	/** 
	 * Validations for case number
	 * Alphanumeric - first 13 characters are digits, last two characters upper case letters
	 * 
	 * Required : Yes
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String caseNumber = null;
		
		//Value is not mandatory
		if(value == null){
			throw new InputDataValidationException(VLPServiceConstants.CASE_NUMBER_REQUIRED_ERROR_MESSAGE);
		}
		else{
			caseNumber = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(caseNumber.length() < VLPServiceConstants.CASE_NUMBER_MIN_LEN
				|| caseNumber.length() > VLPServiceConstants.CASE_NUMBER_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.CASE_NUMBER_ERROR_MESSAGE);
		}

		if (!caseNumber.matches(VLPServiceConstants.CASE_NUM_REGEX)){
			throw new InputDataValidationException(VLPServiceConstants.CASE_NUMBER_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
