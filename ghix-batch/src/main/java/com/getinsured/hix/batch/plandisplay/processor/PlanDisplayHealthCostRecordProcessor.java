package com.getinsured.hix.batch.plandisplay.processor;

import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.getinsured.hix.batch.plandisplay.reader.PlanDisplayHealthCostMapper;

public class PlanDisplayHealthCostRecordProcessor implements ItemProcessor<PlanDisplayHealthCostMapper, SolrInputDocument>{
	private static Logger logger = LoggerFactory.getLogger(PlanDisplayHealthCostRecordProcessor.class);

	@Override
	public SolrInputDocument process(PlanDisplayHealthCostMapper fromReader) throws Exception {
		logger.info("In process ---> "+fromReader.getName());
		return prepareSolrDocument(fromReader);
	}
	
	private SolrInputDocument prepareSolrDocument(PlanDisplayHealthCostMapper providerData){
		
		SolrInputDocument solrInputDocument = providerData.getSolrInputDocument();
		logger.info("Processing Data completed : "+providerData.getName());
		return solrInputDocument;
	}
}
