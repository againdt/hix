<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!-- Pre link dialog START -->
<div id="preLinkAppDlg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-body">
    
		<div class="controls">
	    	<h4 class="margin0 pull-left"> Fetching application data </h4>
		</div>	
  </div>
  <div class="modal-footer clearfix">
  </div>
  
  <div id="ajaxloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
    </div>
</div>
<!-- Pre link dialog ENDS  -->

<!-- LCE dialog START -->
<div id="lceInfoDlg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" 
	aria-hidden="true"	data-keyboard="false" data-backdrop="static">
	<div class="modal-body">
		<p>The application type is 'LCE'.
		Please ensure that the applicant is available collect the information needed.
		</p>
	</div>
	<div class="modal-footer clearfix">
		<button class="btn pull-left" aria-hidden="true" data-dismiss="modal" id="cancelLCEBtn"><spring:message code="button.cancel" /></button>
		<button class="btn btn-primary"	  id="btnLCEOK"> Proceed </button>
	</div>
</div>
<!-- LCE dialog ENDS -->

<!-- Linking in progress dialog START -->
<div id="linkingProgressDlg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" 
	aria-hidden="true"	data-keyboard="false" data-backdrop="static">
	<div class="modal-body">
		<p>Linking for this application is already in progress.
		Press 'OK' to continue linking.
		</p>
	</div>
	<div class="modal-footer clearfix">
		<button class="btn btn-primary"	 data-dismiss="modal" id="btnlinkingInProgressOK"> Ok </button>
	</div>
</div>
<!-- Linking in progress dialog ENDS -->

<!-- SwitchToRole dialog START -->
<form name="switchToIndvDlgform" id="switchToIndvDlgform" method="POST" method="POST"  action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
    <div id="switchToIndvDlg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="myModalLabel">View Individual Account</h3>
        </div>
        <div class="modal-body">
        
            <p>Clicking "Individual View" will take you to the Individual Portal.
                Through this portal you will be able to enroll on behalf of the individual.</p>
            <p>Proceed to Individual View?</p>
        </div>
  <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  /> 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Individual View</button>
        
         <input type="hidden" name="switchToNonDefaultURL" id="switchToNonDefaultURL" />
        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value='<encryptor:enc value="individual"/>' /> 
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value="test" />
        <input type="hidden" name="csrReferralFlow" id="csrReferralFlow" value="csrReferralFlow" />
        
  </div>
  </div>
  </form> 
<!-- SwitchToRole dialog ENDS -->

<!-- Model to dis-enroll APP STARTS -->

<!-- Model to dis-enroll APP ENDS -->

<script>
function checkPreLinkData(ssapAppId, cmrId) {

	$("#ajaxloader").show();
	$("#preLinkAppDlg").modal();
	
	var id = ssapAppId;
	if(cmrId == undefined || cmrId <= 0) {
		id = $("#encssapApplicationId_"+ssapAppId).val().toString().trim();
		cmrId = 0;
	} else {
		id =$("#encssapApplicationId").val().toString().trim();
	}
		
	var validateUrl = '<c:url value="/crm/application/checkprelinkdata" />'+ '/' + id + "/" + cmrId;
	
	$.ajax({
		url : validateUrl,
		type : "GET",
		
		success : function(response) {
			$("#ajaxloader").hide();
			$("#preLinkAppDlg").modal('hide');
			
			var jsonResp = JSON.parse(response);
			initiateLinking(id, jsonResp, cmrId);
		},
		error : function(error) {
			$("#ajaxloader").hide();
			$("#preLinkAppDlg").modal('hide');
			alert("Unable to get pre linking data. Please try again..");
			
			var preLinkData = {"REF_LCE_CMR_ID":"","CMR_ENROLLED_FLG":"N","REF_LCE_STATUS_FLG":"N"};
			//$("#cmrJSON").val(preLinkData);
			//alert(preLinkData.CMR_ENROLLED_FLG);
			
		//	initiateLinking(ssapAppId, preLinkData, cmrId);
		}
	});
	
	function initiateLinking(ssapAppId, preLinkData, cmrId) {
		var isLCE = preLinkData.REF_LCE_STATUS_FLG;
		
		if(isLCE == "Y") { // show lce info modal			
			$("#lceInfoDlg").modal(); // user can cancel
			$("#lceInfoDlg").find("#btnLCEOK").unbind("click",proccedAfterLCEModal);
			$("#lceInfoDlg").find("#btnLCEOK").bind("click",{ssapAppId:ssapAppId,preLinkData:preLinkData, formCmrId : cmrId},proccedAfterLCEModal);
		} else{
			displayLinkModal(ssapAppId, preLinkData, cmrId);
		}	
	}
	
	function proccedAfterLCEModal(e) {
		var ssapId=e.data.ssapAppId;
		var preLinkData=e.data.preLinkData;
		
		displayLinkModal(ssapId, preLinkData, e.data.formCmrId);
	}
	
	function displayLinkModal(ssapAppId, preLinkData, formCmrId) {
		
		$("#linkingMsgDiv").hide();
		var cmrId = preLinkData.REF_LCE_CMR_ID;
		var isCmrEnrolled = preLinkData.CMR_ENROLLED_FLG;
		
		if((cmrId == null || cmrId == "") && (formCmrId == 0)) { // show search Page
			window.location = "/hix/crm/application/linkDetails/" + ssapAppId;
		} else {
			// show individual dashboard
			if((cmrId == null || cmrId == "" ) && formCmrId != "" )  //show modal to disenroll n cancel app
			{// show link wizard
			   linkUrl = "/referral/csrflow/" + ssapAppId + "/" + formCmrId;
			   $("#switchToModuleId").val(formCmrId);
		    }else
                 {
		    	$("#linkingProgressDlg").modal();
		    	$("#btnlinkingInProgressOK").unbind("click",OnLinkingProgressOk).bind("click",{ssapAppId:ssapAppId,cmrId:cmrId},OnLinkingProgressOk);
		    	return;
                
			  }	   
			
			  $("#switchToNonDefaultURL").val(linkUrl);
			  $("#switchToIndvDlg").modal();
			
		}
		
	}

}	
var OnLinkingProgressOk=function(e)
{
	$("#linkingProgressDlg").modal("hide");
	var ssapAppId=e.data.ssapAppId;
	var cmrId=e.data.cmrId;
	var linkUrl = "/referral/csrflow/" + ssapAppId + "/" + cmrId;
    $("#switchToModuleId").val(cmrId);
    $("#switchToNonDefaultURL").val(linkUrl);
	$("#switchToIndvDlg").modal();
};
</script>