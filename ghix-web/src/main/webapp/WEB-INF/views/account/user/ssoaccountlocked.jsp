<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/user-changeinfo-view" prefix="userInfoView"%>
<%@page
	import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page
	import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<div class="gutter10">
	<div class="row-fluid">
		<div class="span10 offset1" id="rightpanel">
			<!-- Alert Message: Start -->
			<%--c:if test="${isAccLocked}">
				<div class="box-loose txt-center">
					Your account has been locked due to several unsuccessful login
					attempts. Please click the below link to unlock your account.<br>
					<br> <a href="<c:url value="/account/unlock/email"/>"
						class="margin30-t">Your account locked ?</a>
				</div>
			</c:if>
			<c:if test="${isAccInactiveDormant}">
				<div class="box-loose txt-center">
					Your account has been disabled due to inactivity. Please click the
					below link to enable your account.<br> <br> <a
						href="<c:url value="/account/unlock/email"/>" class="margin30-t">Your
						account disabled ?</a>
				</div>
			</c:if>
			<c:if test="${isAccInactive}" --%>
				<div class="box-loose txt-center">
					Your account has been locked. Please contact <%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE)%> between
					8 am and 5pm to unlock your account. When you try again please make sure you logout from the current session and login again.<br> <br>
					<a href="<c:url value="/account/user/logout"/>"
					class="btn btn-primary">Logout</a>
				</div>
				
			<%--/c:if --%>
			<!-- Alert Message: End -->
			<!-- /form -->

		</div>
	</div>
</div>