package com.getinsured.eligibility.redetermination.service.impl;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationEventService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;

@Service
public class SsapCloneApplicationEventServiceImpl implements
	SsapCloneApplicationEventService {
		
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;

	@Override
	public List<SsapApplicationEvent> cloneSsapApplicationEvent(SsapApplication currentApplication, Long userId, SsapApplicationEventTypeEnum eventType) throws Exception {
		int renewalYear = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
		
		List<SsapApplicationEvent> ssapApplicationEvents = new ArrayList<SsapApplicationEvent>();
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(userId.intValue());
		ssapApplicationEvent.setCreatedTimeStamp(new Timestamp(new TSDate().getTime()));
		ssapApplicationEvent.setSsapApplication(currentApplication);
		ssapApplicationEvent.setEventType(eventType);
		ssapApplicationEvent.setKeepOnly(LifeChangeEventConstant.NO);
		ssapApplicationEvent.setReportEndDate(new Timestamp(new TSDate().getTime()));
		ssapApplicationEvent.setReportStartDate(new Timestamp(new TSDate().getTime()));
		ssapApplicationEvent.setKeepOnly("N"); 
		ssapApplicationEvent.setEventDate(new Timestamp(new TSDate().getTime()));
		ssapApplicationEvent.setChangePlan("Y");
		/*ssapApplicationEvent.setCoverageStartDate(getCoverageStartDateForRenewalYear(renewalYear));
		ssapApplicationEvent.setCoverageEndDate(getCoverageEndDate(renewalYear));*/
		
		ssapApplicationEvents.add(ssapApplicationEvent);
		
		
		currentApplication.setSsapApplicationEvents(ssapApplicationEvents);
		
		return ssapApplicationEvents;
	}
	
	private Timestamp getCoverageStartDateForRenewalYear(int renewalYear) {
		Calendar coverageCal = TSCalendar.getInstance();
	    coverageCal.set(Calendar.DAY_OF_MONTH, 1);
	    coverageCal.set(Calendar.YEAR, renewalYear);
		coverageCal.set(Calendar.MONTH, 0);
		
		return new Timestamp(coverageCal.getTime().getTime());
	}
	
	private Timestamp getCoverageEndDate(int renewalYear) {
	
		Calendar calendar = TSCalendar.getInstance();  
        calendar.set(Calendar.DAY_OF_MONTH, 31);  
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.YEAR, renewalYear);

	    return new Timestamp(calendar.getTime().getTime());
	}
}
