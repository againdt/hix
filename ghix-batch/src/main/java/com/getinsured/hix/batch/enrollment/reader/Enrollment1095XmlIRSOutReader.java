package com.getinsured.hix.batch.enrollment.reader;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.skip.Enrollment1095XmlParams;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentIds;


public class Enrollment1095XmlIRSOutReader implements ItemReader<Integer>{
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095XmlIRSOutReader.class);
	List<Integer> enrollment1095Ids;		
	int partition;
	int loopCount;
//	int startIndex;
//	int endIndex;
	
	private EnrollmentIds enrollmentIdList;
	private Enrollment1095XmlParams enrollment1095XmlParams;
	
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			partition =ec.getInt("partition");
//			startIndex=ec.getInt("startIndex");
//			endIndex=ec.getInt("endIndex");
			enrollment1095Ids =enrollment1095XmlParams.getModifiedPartitionFromId(partition);
					
		/*	if(enrollmentIdList!=null && enrollmentIdList.getEnrollmentIdList()!=null && !enrollmentIdList.getEnrollmentIdList().isEmpty()) {
				if(startIndex<enrollmentIdList.getEnrollmentIdList().size() && endIndex<=enrollmentIdList.getEnrollmentIdList().size()){
					enrollment1095Ids= new ArrayList<Integer>(enrollmentIdList.getEnrollmentIdList().subList(startIndex, endIndex));
				}
			}*/
		}
	}
	
	@Override
	public Integer read() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {
		
		if(enrollment1095Ids!=null && !enrollment1095Ids.isEmpty()){
			if(loopCount<enrollment1095Ids.size()){
				loopCount++;
				return enrollment1095Ids.get(loopCount-1);
				
			}else{
				return null;
			}
			
		}
			return null;
	}

	public EnrollmentIds getEnrollmentIdList() {
		return enrollmentIdList;
	}

	public void setEnrollmentIdList(EnrollmentIds enrollmentIdList) {
		this.enrollmentIdList = enrollmentIdList;
	}

	public Enrollment1095XmlParams getEnrollment1095XmlParams() {
		return enrollment1095XmlParams;
	}

	public void setEnrollment1095XmlParams(Enrollment1095XmlParams enrollment1095XmlParams) {
		this.enrollment1095XmlParams = enrollment1095XmlParams;
	}

}
