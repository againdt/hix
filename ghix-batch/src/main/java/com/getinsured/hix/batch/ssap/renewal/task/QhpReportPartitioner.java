package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.QhpReportService;
import com.getinsured.hix.batch.ssap.renewal.util.QhpReportPartitionerParams;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;

/**
 * QHP Report Partitioner class is used to get read Enrollment IDs from database and do Partitioner.
 * 
 * @since June 14, 2019
 */
@Component("renewalQHPReportPartitioner")
@Scope("step")
public class QhpReportPartitioner implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(QhpReportPartitioner.class);

	private Integer coverageYear;
	private String qhpReportCommitInterval;
	private QhpReportService qhpReportService;
	private BatchJobExecutionService batchJobExecutionService;
	private QhpReportPartitionerParams qhpReportPartitionerParams;

	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {

		Map<String, ExecutionContext> partitionMap = null;
		StringBuffer errorMessage = new StringBuffer();

		try {

			if (!hasRunningBatchSizeOne()) {
				errorMessage.append(RenewalUtils.EMSG_RUNNING_BATCH);
				return partitionMap;
			}

			if (!validateQHPReportsParams(errorMessage)) {
				return partitionMap;
			}

			List<Integer> partitionerEnrollmentIdList = qhpReportService.getEnrollmentIdListByCoverageYearForQHPReports(coverageYear);

			if (CollectionUtils.isNotEmpty(partitionerEnrollmentIdList)) {

				LOGGER.info("Number of Enrollment-ID to generate QHP Report: {}", partitionerEnrollmentIdList.size());
				partitionMap = new HashMap<String, ExecutionContext>();
				qhpReportPartitionerParams.clearEnrollmentIdList();
				qhpReportPartitionerParams.addAllToEnrollmentIdList(partitionerEnrollmentIdList);

				int maxEnrollmentsCommitInterval = 1;
				int size = partitionerEnrollmentIdList.size();

				if (this.qhpReportCommitInterval != null && !this.qhpReportCommitInterval.isEmpty()) {
					maxEnrollmentsCommitInterval = Integer.valueOf(this.qhpReportCommitInterval.trim());
				}

				int numberOfEnrollmentIdToCommit = size / maxEnrollmentsCommitInterval;
				if (size % maxEnrollmentsCommitInterval != 0) {
					numberOfEnrollmentIdToCommit++;
				}

				int firstIndex = 0;
				int lastIndex = 0;

				for (int i = 0; i < numberOfEnrollmentIdToCommit; i++) {
					firstIndex = i * maxEnrollmentsCommitInterval;
					lastIndex = (i + 1) * maxEnrollmentsCommitInterval;

					if (lastIndex > size) {
						lastIndex = size;
					}
					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition", i);
					partitionMap.put("partition - " + i, value);
				}
			}
			else {
				errorMessage.append("Enrollment data is not found to generate QHP Reports.");
			}
		}
		catch (Exception ex) {
			errorMessage.append("QhpReportPartitioner failed to execute : ").append(ex.getMessage());
			LOGGER.error(errorMessage.toString(), ex);
		}
		finally {

			if (0 < errorMessage.length()) {
				qhpReportService.saveAndThrowsErrorLog(errorMessage.toString());
			}
		}
		return partitionMap;
	}

	private boolean validateQHPReportsParams(StringBuffer errorMessage) {

		boolean hasValidParams = true;

		if (null == coverageYear || 2000 > coverageYear || 2099 < coverageYear) {
			errorMessage.append("Invalid Coverage Year: ");
			errorMessage.append(coverageYear);
			hasValidParams = false;
		}

		if (!hasValidParams) {
			LOGGER.error(errorMessage.toString());
		}
		return hasValidParams;
	}

	/**
	 * Method is used to get Running Batch List.
	 */
	private boolean hasRunningBatchSizeOne() {

		boolean hasRunningBatchSizeOne = false;

		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob(QhpReportService.QHP_REPORT_JOB_NAME);
		if (batchExecutionList != null && batchExecutionList.size() == 1) {
			hasRunningBatchSizeOne = true;
		}
		return hasRunningBatchSizeOne;
	}

	public Integer getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getQhpReportCommitInterval() {
		return qhpReportCommitInterval;
	}

	public void setQhpReportCommitInterval(String qhpReportCommitInterval) {
		this.qhpReportCommitInterval = qhpReportCommitInterval;
	}

	public QhpReportService getQhpReportService() {
		return qhpReportService;
	}

	public void setQhpReportService(QhpReportService qhpReportService) {
		this.qhpReportService = qhpReportService;
	}

	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	public QhpReportPartitionerParams getQhpReportPartitionerParams() {
		return qhpReportPartitionerParams;
	}

	public void setQhpReportPartitionerParams(QhpReportPartitionerParams qhpReportPartitionerParams) {
		this.qhpReportPartitionerParams = qhpReportPartitionerParams;
	}
}
