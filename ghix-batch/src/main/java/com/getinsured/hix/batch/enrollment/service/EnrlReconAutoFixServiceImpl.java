package com.getinsured.hix.batch.enrollment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrolleeUpdateDto;
import com.getinsured.hix.dto.enrollment.EnrollmentStatusUpdateDto;
import com.getinsured.hix.dto.enrollment.ReconEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.ReconMemberDTO;
import com.getinsured.hix.enrollment.repository.IEnrlReconDiscrepancyRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSnapshotRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSummaryRepository;
import com.getinsured.hix.enrollment.service.EnrollmentServiceImpl;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.google.gson.Gson;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy;
import com.getinsured.hix.model.enrollment.EnrlReconSnapshot;
import com.getinsured.hix.model.enrollment.EnrlReconSummary;
import com.getinsured.hix.model.enrollment.EnrlReconDiscrepancy.DiscrepancyStatus;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;

@Service("enrlReconAutoFixService")
public class EnrlReconAutoFixServiceImpl implements EnrlReconAutoFixService {
	@Autowired private IEnrlReconDiscrepancyRepository enrlReconDiscrepancyRepository;
	@Autowired private IEnrlReconSnapshotRepository enrlReconSnapshotRepository;
	@Autowired private Gson platformGson;
	@Autowired private EnrollmentServiceImpl enrollmentService;
	@Autowired private IEnrlReconSummaryRepository enrlReconSummaryRepository;
	@Autowired private UserService userService;
	
	private static  Logger LOGGER = LoggerFactory.getLogger(EnrlReconAutoFixServiceImpl.class);
	@Override
	public void processAutoFix(Integer fileId, Job job, JobLauncher jobLauncher){
		if(fileId!=null){
			EnrlReconSummary summary= enrlReconSummaryRepository.getByFileId(fileId);
			String autoFix = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_AUTOFIX);
			if(autoFix!=null && autoFix.trim().equalsIgnoreCase("true")){
				List<String> discrepancyCodeList= new ArrayList<>();
				discrepancyCodeList.add(EnrollmentConstants.RECON_DISCREPANCY_8200_AA);
				List<DiscrepancyStatus> discrepancyStatusList= new ArrayList<>();
				discrepancyStatusList.add(DiscrepancyStatus.OPEN);
				List<EnrlReconDiscrepancy> autoFixDiscrepancies= enrlReconDiscrepancyRepository.getDiscrepancyByDiscrepancyLkpAndFileId(fileId, discrepancyCodeList, discrepancyStatusList);
				if(autoFixDiscrepancies!=null && autoFixDiscrepancies.size()>0){
					for(EnrlReconDiscrepancy disc: autoFixDiscrepancies){
						try{
							Enrollment enrollment=enrollmentService.findById(disc.getHixEnrollmentId().intValue());
							if(enrollment==null){
								throw new Exception("No Enrollment found in DB with ID :: "+disc.getHixEnrollmentId());
							}else if(enrollment.getEnrollmentStatusLkp()!=null && enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL)){
								throw new Exception("Enrollment "+disc.getHixEnrollmentId()+"  is in CANCEL status");
							}
							//String issuerData=enrlReconSnapshotRepository.findIssuerDataByFileIdAndEnrollmentId(fileId, disc.getHixEnrollmentId());
							EnrlReconSnapshot reconData= enrlReconSnapshotRepository.findSnapshotDataByFileIdAndEnrollmentId(fileId, disc.getHixEnrollmentId());
							
							if(reconData!=null && EnrollmentUtils.isNotNullAndEmpty(reconData.getIssuerData()) && EnrollmentUtils.isNotNullAndEmpty(reconData.getHixData())){
								ReconEnrollmentDTO issuerDTO= platformGson.fromJson(reconData.getIssuerData(), ReconEnrollmentDTO.class);
								ReconEnrollmentDTO hixDTO= platformGson.fromJson(reconData.getHixData(), ReconEnrollmentDTO.class);
								if(hixDTO!=null){
									EnrollmentStatusUpdateDto updateDto= new EnrollmentStatusUpdateDto();
									updateDto.setEnrollmentId(disc.getHixEnrollmentId().intValue());
									updateDto.setIssuerAssignPolicyNo(issuerDTO.getIssuerAssignedPolicyId());
									List<EnrolleeUpdateDto> enrolleeUpdateDtoList= new ArrayList<>();
									for(ReconMemberDTO member: hixDTO.getMembers()){
										EnrolleeUpdateDto enrolleeDto= new EnrolleeUpdateDto();
										enrolleeDto.setExchgIndivIdentifier(member.getExchangeAssignedMemberId());
										for(ReconMemberDTO issuerMember: issuerDTO.getMembers()){
											if(member.getExchangeAssignedMemberId().equals(issuerMember.getExchangeAssignedMemberId())){
												enrolleeDto.setIssuerIndivIdentifier(issuerMember.getIssuerAssignedMemberId());
												break;
											}
										}
										if(EnrollmentUtils.isNotNullAndEmpty(issuerDTO.getPaidThroughDate())){
											enrolleeDto.setLastPremiumPaidDate(DateUtil.changeFormat(issuerDTO.getPaidThroughDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, GhixConstants.REQUIRED_DATE_FORMAT));
										}
										
										/*
										 Following code is commented for HIX-102027
										
										if(EnrollmentUtils.isNotNullAndEmpty(member.getMemberBeginDate())){
											enrolleeDto.setEffectiveStartDate(DateUtil.changeFormat(member.getMemberBeginDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, GhixConstants.REQUIRED_DATE_FORMAT));
										}
										if(EnrollmentUtils.isNotNullAndEmpty(member.getMemberEndDate())){
											enrolleeDto.setEffectiveEndDate(DateUtil.changeFormat(member.getMemberEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD, GhixConstants.REQUIRED_DATE_FORMAT));
										}*/
										enrolleeUpdateDtoList.add(enrolleeDto);
									}
									updateDto.setUpdatedByUser(getAccountUser());
									updateDto.setEnrolleeUpdateDtoList(enrolleeUpdateDtoList);
									EnrollmentResponse updateResponse= new EnrollmentResponse();
									enrollmentService.updateEnrollmentStatus(updateDto, updateResponse, EnrollmentEvent.TRANSACTION_IDENTIFIER.RECON_AUTOFIX_JOB);
								    if(updateResponse.getStatus()!=null && updateResponse.getStatus().equalsIgnoreCase(EnrollmentConstants.FAILURE)){
								    	throw new Exception(updateResponse.getErrMsg()+ "  " +updateResponse.getErrMsg());
								    }
								    
								    disc.setStatus(EnrlReconDiscrepancy.DiscrepancyStatus.AUTOFIXED);
								    disc.setDateResolved(new Date());
								    disc.setAutoFixError(EnrollmentConstants.N);
								}
							}
							
						}catch(Exception e){
							disc.setAutoFixError(EnrollmentConstants.Y);
							disc.setRemarks(EnrollmentUtils.shortStackTrace(e, 1000));
						}
						
						enrlReconDiscrepancyRepository.save(disc);
						markHistoricalDiscrepancies(discrepancyCodeList, disc);
					}
				}
			}else{
				LOGGER.error("autoFix is set to false");
			}
			if(summary!=null){
				summary.setStatus(EnrlReconSummary.SummaryStatus.AUTOFIXED.toString());
				enrlReconSummaryRepository.save(summary);
			}
			String jobChaining = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENRL_RECON_BATCH_JOB_CHAINING);

			if(null != jobChaining && "false".equalsIgnoreCase(jobChaining)){
				LOGGER.info(" NOT Triggering discrepancy report generation job for batch File_ID(S) : " + fileId);
				LOGGER.info(" ENRL_RECON_BATCH_JOB_CHAINING : "+ jobChaining);

			}else{
				triggerBatchJobWithParams( fileId.longValue(),  job,  jobLauncher);
			}
			
		}
	}
	
	/**
	 * Marks historical discrepancies as AUTOFIXED for a particular enrollment
	 * @param discrepancyCodeList
	 * @param currentDisc
	 */
	private void markHistoricalDiscrepancies(List<String> discrepancyCodeList, EnrlReconDiscrepancy currentDisc) {
		if(currentDisc.getStatus() == EnrlReconDiscrepancy.DiscrepancyStatus.AUTOFIXED){
			List<DiscrepancyStatus> discrepancyStatusList= new ArrayList<>();
			discrepancyStatusList.add(DiscrepancyStatus.OPEN);
			discrepancyStatusList.add(DiscrepancyStatus.HOLD);
			List<EnrlReconDiscrepancy> autoFixDiscrepancies = enrlReconDiscrepancyRepository.getDiscrepancyByDiscrepancyLkpAndEnrollmentId(discrepancyCodeList, discrepancyStatusList,currentDisc.getHixEnrollmentId());
			for(EnrlReconDiscrepancy disc : autoFixDiscrepancies){
			    disc.setStatus(EnrlReconDiscrepancy.DiscrepancyStatus.AUTOFIXED);
			    disc.setDateResolved(new Date());
			    disc.setAutoFixError(EnrollmentConstants.N);
			}
			try{
				enrlReconDiscrepancyRepository.save(autoFixDiscrepancies);
			}catch(Exception e){
				LOGGER.error("Error marking historical discrepancies as AUTOFIXED",  e);
			}
		}
	}
	
	private AccountUser getAccountUser() {
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			if (null == user) {
				user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
			}
		} catch (InvalidUserException e) {
			LOGGER.debug("Error fetching logged in user", e);
		}
		return user;
	}
	
	private void triggerBatchJobWithParams(Long fileId,  Job job,JobLauncher jobLauncher) {
		LOGGER.info("Triggering Discrepancy Report Job");
		try {
			JobParameters jobParameters = new JobParametersBuilder()
											.addString("fileId", fileId+"")
											.addLong("EXECUTION_DATE", new Date().getTime())
											.toJobParameters();
			
			JobExecution jobExecution = jobLauncher.run(job,jobParameters);
			
		} catch (Exception e) {
			LOGGER.error("Error in triggering discrepancy report job batch job from autofix job", e);
		}
	}
}
