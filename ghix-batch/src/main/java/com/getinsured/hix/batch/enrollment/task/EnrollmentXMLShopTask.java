package com.getinsured.hix.batch.enrollment.task;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author ajinkya_m
 * 
 */
public class EnrollmentXMLShopTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentXMLShopTask.class);
	private EnrollmentBatchService enrollmentBatchService;
	private EnrollmentEmailUtils enrollmentEmailUtils;
	private static final String ENROLLMENT_TYPE_SHOP="24";
	private String startDateStr;
	private String endDateStr;
	private String carrierResendFlag;
	private String hiosIssuerIdStr;
	private String hiosIssuerId;
	
	public String getCarrierResendFlag() {
		return carrierResendFlag;
	}

	public void setCarrierResendFlag(String carrierResendFlag) {
		if(carrierResendFlag != null && ("".equalsIgnoreCase(carrierResendFlag) || "null".equalsIgnoreCase(carrierResendFlag))){
			this.carrierResendFlag = null;
		}else{
			this.carrierResendFlag = carrierResendFlag;
		}
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws GIException {
		Date lastRunDate=null;
		
		boolean datesProvided=false;
		EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
		enrollmentBatchEmailDTO.setFileLocation(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.XMLEXTRACTPATH));
		enrollmentBatchEmailDTO.setErrorCode(201);
		enrollmentBatchEmailDTO.setAppServer(GhixBatchConstants.appserver);
		try {
			LOGGER.info(" EnrollmentXMLShopTask.execute : START ");
			if((startDateStr==null &&endDateStr!=null)||(startDateStr!=null &&endDateStr==null)){
				LOGGER.error("Please provide both Start Date and End Date");
				enrollmentBatchEmailDTO.setErrorMessage("Please provide both Start Date and End Date");
				throw new GIException("Please provide both Start Date and End Date");
			}else if(startDateStr!=null && endDateStr!=null){
						if((startDateStr.equalsIgnoreCase("null") && endDateStr.equalsIgnoreCase("null")) || (startDateStr.equalsIgnoreCase("") && endDateStr.equalsIgnoreCase(""))){
							startDateStr = null;
							endDateStr = null;
						}else if (isNotNullAndEmpty(startDateStr) && isNotNullAndEmpty(endDateStr)){
					if(DateUtil.isValidDate(startDateStr, EnrollmentConstants.BATCH_DATE_FORMAT) && DateUtil.isValidDate(endDateStr, EnrollmentConstants.BATCH_DATE_FORMAT)){
						if(!DateUtil.isValidDateInterval(DateUtil.StringToDate(startDateStr, EnrollmentConstants.BATCH_DATE_FORMAT),DateUtil.StringToDate(endDateStr,EnrollmentConstants.BATCH_DATE_FORMAT))){
							
							LOGGER.error(" EnrollmentXMLShopTask.execute : Start date is greater than End date.");
							enrollmentBatchEmailDTO.setErrorMessage("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
							throw new GIException("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
							
						}
						datesProvided=true;
					}else{
						LOGGER.error("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
						enrollmentBatchEmailDTO.setErrorMessage("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
						throw new GIException("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
					}
				}
			}
			//validate carrierResendFlag
			if(carrierResendFlag != null){
				if(carrierResendFlag.equalsIgnoreCase(GhixBatchConstants.RESEND_FLAG) || carrierResendFlag.equalsIgnoreCase(GhixBatchConstants.HOLD_FLAG)){
					datesProvided=true;
					carrierResendFlag=carrierResendFlag.toUpperCase();
				}else{
					LOGGER.error("Please provide valid value for Carrier_send_flag else remove the Carrier_send_flag parameter");
					enrollmentBatchEmailDTO.setErrorMessage("Please provide valid value for Carrier_send_flag else remove the Carrier_send_flag parameter");
					throw new GIException("Please provide valid value for Carrier_send_flag else remove the Carrier_send_flag parameter");
				}
			}
			
			// Apply Validation on HIOSIssuer Id 
			if(isNotNullAndEmpty(hiosIssuerIdStr)){
				
			 hiosIssuerId = hiosIssuerIdStr;
			}
			
			//enrollmentBatchService.serveEnrollmentRecordJob(ENROLLMENT_TYPE_SHOP,startDateStr,endDateStr, carrierResendFlag, hiosIssuerId);
			LOGGER.info(" EnrollmentXMLShopTask.execute : END ");
			
			if(datesProvided){
				lastRunDate=enrollmentBatchService.getJOBLastRunDate(chunkContext.getStepContext().getJobName());
			chunkContext.getStepContext().getStepExecution().getJobExecution().setStartTime(lastRunDate);
			}
		} catch (Exception e) {
			LOGGER.info("EnrollmentXMLShopTask.execute : FAIL "+e.getCause());
			enrollmentBatchEmailDTO.setErrorMessage(e.getMessage());
			enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
			throw new GIException("EnrollmentXMLShopTask.execute : FAIL "+ e.getCause(),e);
		}
		return RepeatStatus.FINISHED;
	}

	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(
			EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}

	public EnrollmentEmailUtils getEnrollmentEmailUtils() {
		return enrollmentEmailUtils;
	}

	public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
		this.enrollmentEmailUtils = enrollmentEmailUtils;
	}

	public String getHiosIssuerIdStr() {
		return hiosIssuerIdStr;
	}

	public void setHiosIssuerIdStr(String hiosIssuerIdStr) {
		this.hiosIssuerIdStr = hiosIssuerIdStr;
	}

}
