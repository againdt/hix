//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.08 at 02:48:07 PM IST 
//


package com.getinsured.ahbx.delegation.delegatecode;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for responseValidInvalid.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="responseValidInvalid">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="VALID"/>
 *     &lt;enumeration value="INVALID"/>
 *     &lt;enumeration value="FAILURE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "responseValidInvalid", namespace = "http://ahbx.accenture.com/AHBXValidation")
@XmlEnum
public enum ResponseValidInvalid {

    VALID,
    INVALID,
    FAILURE;

    public String value() {
        return name();
    }

    public static ResponseValidInvalid fromValue(String v) {
        return valueOf(v);
    }

}
