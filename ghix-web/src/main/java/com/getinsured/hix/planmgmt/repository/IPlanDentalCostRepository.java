package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.getinsured.hix.model.PlanDentalCost;

public interface IPlanDentalCostRepository extends JpaRepository<PlanDentalCost, Integer>{
	@Query("SELECT pdc FROM PlanDentalCost pdc WHERE pdc.planDental.id = :planId")
	List<PlanDentalCost> getPlanDentalCostByPlanId(@Param("planId") Integer planId);

}
