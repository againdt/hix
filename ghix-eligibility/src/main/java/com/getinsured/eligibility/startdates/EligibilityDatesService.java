package com.getinsured.eligibility.startdates;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.controller.EligibilityDatesController.ClientType;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.exception.InvalidIncomingValueException;
import com.getinsured.eligibility.exception.InvalidStateException;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesRequestDto;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesResponseDto;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesResponseDto.Status;
import com.getinsured.eligibility.startdates.dto.EligibilityDatesResponseDto.SubStatus;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

@Service
public class EligibilityDatesService {

	private static final String INCOMING_FINANCIAL_EFFECTIVE_DATE_SHOULD_BE_FIRST_OF_A_MONTH = "Incoming FinancialEffectiveDate should be first of a month.";
	private static final String INCOMING_FINANCIAL_EFFECTIVE_DATE_SHOULD_FALL_WITHIN_ENROLLMENT_DATES_WINDOW = "Incoming FinancialEffectiveDate should fall within enrollment dates window.";
	private static final String INCOMING_FINANCIAL_EFFECTIVE_DATE_CANNOT_BE_BLANK = "Incoming FinancialEffectiveDate cannot be blank.";
	private static final String NOT_A_VALID_CASE_TO_OVERRIDE_ELIGIBILITY_START_DATES = "Not a valid case to override eligibility start dates.";
	private static final String NO_PROGRAM_ELIGIBILITY_RECORD_FOR_ER_APP_ID = "No program eligibility record for ErAppId.";
	private static final String ER_APP_ID_IS_NOT_IN_ER_STATE = "erAppId is not in ER state";
	private static final String EN_APP_ID_IS_NOT_IN_EN_STATE = "enAppId is not in EN state";
	private static final String EN_APP_ID_AND_ER_APP_ID_SHOULD_BELONG_TO_SAME_HOUSEHOLD = "enAppId and erAppId should belong to same household.";
	private static final String EN_APP_ID_AND_ER_APP_ID_SHOULD_BELONG_TO_SAME_COVERAGE_YEAR = "enAppId and erAppId should belong to same coverage year.";
	private static final String NO_APPLICATION_FOUND_FOR_EN_APP_ID_AND_ER_APP_ID = "No application found for enAppId and erAppId.";
	//private static final String COVERAGE_START_DATE_CANNOT_BE_NULL = "Coverage Start Date cannot be null.";
	private static final String APTC_ELIGIBILITY_TYPE = "APTCEligibilityType";
	private static final String EXCHANGE_ELIGIBILITY_TYPE = "ExchangeEligibilityType";

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private IEligibilityProgram iEligibilityProgram;

	public EligibilityDatesResponseDto overrideEsd(EligibilityDatesRequestDto eligibilityDatesRequestDto, Date overriddenDate) {

		boolean isNF2Fconversion = preProcess(eligibilityDatesRequestDto, ClientType.OVERRIDE);

		EligibilityProgram targetEligibilityProgram = readExchangeEligibility(eligibilityDatesRequestDto);
		validateOverrideRequest(eligibilityDatesRequestDto, overriddenDate, targetEligibilityProgram, isNF2Fconversion);

		updateProgramEligibility(targetEligibilityProgram, Status.OVERRIDDEN, overriddenDate);

		return validateEsd(eligibilityDatesRequestDto, ClientType.OVERRIDE);
	}

	public EligibilityDatesResponseDto validateEsd(EligibilityDatesRequestDto eligibilityDatesRequestDto, ClientType clientType){

		boolean isValidSceanrio = preProcess(eligibilityDatesRequestDto, clientType);

		SsapApplication erApp = ssapApplicationRepository.findOne(eligibilityDatesRequestDto.getErAppId());

		EligibilityProgram targetEligibilityProgram = readExchangeEligibility(eligibilityDatesRequestDto);
		String eligibilityStartDateOverrideFlag = targetEligibilityProgram.getEligStartDateOverride();
		Date financialEffectiveDate = targetEligibilityProgram.getEligibilityStartDate();

		if (!isValidSceanrio){
			return EligibilityDatesResponseDto.builder()
					.status(Status.NOTREQUIRED).subStatus(SubStatus.NOTREQUIRED)
					.financialEffectiveDate(financialEffectiveDate)
					.build();
		}

		boolean isUpdated = false;
		Status status = Status.VALID;
		SubStatus subStatus = SubStatus.VALID;

		if (targetEligibilityProgram.getEligibilityStartDate().after(eligibilityDatesRequestDto.getEnrollmentEndDate())){
			status = Status.REQUIRED;
			subStatus = SubStatus.GREATER_THAN_ENROLLMENT_END_DATE;
		} else {
			if (StringUtils.equals(eligibilityStartDateOverrideFlag, Status.OVERRIDDEN.name())){
				if (clientType != ClientType.OVERRIDE &&
						targetEligibilityProgram.getEligibilityStartDate().before(eligibilityDatesRequestDto.getEnrollmentStartDate())){
					financialEffectiveDate = eligibilityDatesRequestDto.getEnrollmentStartDate();
				}
				status = Status.OVERRIDDEN; subStatus = SubStatus.VALID;
			} else {
				Date thresholdPlus60Days = new LocalDate(erApp.getCreationTimestamp()).plusDays(60).toDate();
				Date thresholdMinus60Days = new LocalDate(erApp.getCreationTimestamp()).minusDays(60).toDate();
				if (targetEligibilityProgram.getEligibilityStartDate().after(thresholdPlus60Days)){
					status = Status.REQUIRED; subStatus = SubStatus.OLDER_60_DAYS;
				} else if (targetEligibilityProgram.getEligibilityStartDate().before(thresholdMinus60Days)){
					status = Status.REQUIRED; subStatus = SubStatus.OLDER_60_DAYS;
				} else {
					if (clientType != ClientType.OVERRIDE &&
							targetEligibilityProgram.getEligibilityStartDate().before(eligibilityDatesRequestDto.getEnrollmentStartDate())){
						financialEffectiveDate = eligibilityDatesRequestDto.getEnrollmentStartDate(); // make ESD same as Enrollment Start Date
					}
				}
			}
		}

		if (!StringUtils.equals(eligibilityStartDateOverrideFlag, status.name())){
			isUpdated = true;
			updateProgramEligibility(targetEligibilityProgram, status, null);
		}

		return EligibilityDatesResponseDto.builder()
				.status(status).subStatus(subStatus)
				.isNF2Fcase(true)
				.financialEffectiveDate(financialEffectiveDate).isUpdated(isUpdated).build();

	}

	private void updateProgramEligibility(EligibilityProgram targetEligibilityProgram, Status status, Date financialEffectiveDate) {
		EligibilityProgram epDO = iEligibilityProgram.findOne(targetEligibilityProgram.getId());
		if (financialEffectiveDate != null){
			epDO.setEligibilityStartDate(financialEffectiveDate);
		}

		epDO.setEligStartDateOverride(status.name());
		iEligibilityProgram.save(epDO);
	}

	private boolean preProcess(EligibilityDatesRequestDto eligibilityDatesRequestDto, ClientType clientType){
		SsapApplication enApp = ssapApplicationRepository.findOne(eligibilityDatesRequestDto.getEnAppId());
		SsapApplication erApp = ssapApplicationRepository.findOne(eligibilityDatesRequestDto.getErAppId());
		validate(eligibilityDatesRequestDto, enApp, erApp, clientType);
        boolean isValid = false;
		if (!eligibilityDatesRequestDto.isDentalOnly() && isNF2Fconversion(enApp, erApp) && !isCSREligible(erApp)){
			isValid = true;
		}

		return isValid;
	}

	private void validate(EligibilityDatesRequestDto eligibilityDatesRequestDto, SsapApplication enApp, SsapApplication erApp, ClientType clientType) {
		/*if (clientType != ClientType.OVERRIDE){
			if (eligibilityDatesRequestDto.getCoverageStartDate() == null){
				throw new InvalidIncomingValueException(COVERAGE_START_DATE_CANNOT_BE_NULL);
			}
		}*/ /* Not needed as enrollment will always match financialEffectiveDate = CoverageStartDate if financialEffectiveDate is not passed through IND19 */

		if (enApp == null || erApp == null){
			throw new InvalidIncomingValueException(NO_APPLICATION_FOUND_FOR_EN_APP_ID_AND_ER_APP_ID);
		}

		if (enApp.getCmrHouseoldId().compareTo(erApp.getCmrHouseoldId()) != 0){
			throw new InvalidStateException(EN_APP_ID_AND_ER_APP_ID_SHOULD_BELONG_TO_SAME_HOUSEHOLD);
		}

		if (enApp.getCoverageYear() != erApp.getCoverageYear()){
			throw new InvalidStateException(EN_APP_ID_AND_ER_APP_ID_SHOULD_BELONG_TO_SAME_COVERAGE_YEAR);
		}

		if (!StringUtils.equals(enApp.getApplicationStatus(), ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode()) && !StringUtils.equals(enApp.getApplicationStatus(), ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode())){
			throw new InvalidStateException(EN_APP_ID_IS_NOT_IN_EN_STATE);
		}

		if (!StringUtils.equals(erApp.getApplicationStatus(), ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode()) && !StringUtils.equals(enApp.getApplicationStatus(), ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode())){
			throw new InvalidStateException(ER_APP_ID_IS_NOT_IN_ER_STATE);
		}
	}

	private boolean isNF2Fconversion(SsapApplication enApp, SsapApplication erApp) {
		return StringUtils.equals(enApp.getFinancialAssistanceFlag(), ReferralConstants.N) &&
				StringUtils.equals(erApp.getFinancialAssistanceFlag(), ReferralConstants.Y);
	}

	private boolean isCSREligible(SsapApplication erApp) {
		return StringUtils.isNotBlank(erApp.getCsrLevel());
	}

	private EligibilityProgram readExchangeEligibility(EligibilityDatesRequestDto eligibilityDatesRequestDto) {
		List<EligibilityProgram> eligibilityPrograms = iEligibilityProgram.getPrimaryApplicantEligibilities(eligibilityDatesRequestDto.getErAppId());
		return eligibilityPrograms.stream()
								.filter(e -> EXCHANGE_ELIGIBILITY_TYPE.equals(e.getEligibilityType()))
								.findFirst()
								.orElseThrow(() -> new InvalidStateException(NO_PROGRAM_ELIGIBILITY_RECORD_FOR_ER_APP_ID));
	}

	private void validateOverrideRequest(EligibilityDatesRequestDto eligibilityDatesRequestDto, Date overriddenDate,
			EligibilityProgram targetEligibilityProgram, boolean isNF2Fconversion) {

		if (!isNF2Fconversion){
			throw new InvalidStateException(NOT_A_VALID_CASE_TO_OVERRIDE_ELIGIBILITY_START_DATES);
		}

		if (overriddenDate == null){
			throw new InvalidStateException(INCOMING_FINANCIAL_EFFECTIVE_DATE_CANNOT_BE_BLANK);
		}

		if (overriddenDate.after(eligibilityDatesRequestDto.getEnrollmentEndDate())){
			throw new InvalidStateException(INCOMING_FINANCIAL_EFFECTIVE_DATE_SHOULD_FALL_WITHIN_ENROLLMENT_DATES_WINDOW);
		}

		if (!isFirstOfMonth(overriddenDate)){
			throw new InvalidStateException(INCOMING_FINANCIAL_EFFECTIVE_DATE_SHOULD_BE_FIRST_OF_A_MONTH);
		}
	}

	private boolean isFirstOfMonth(Date financialEffectiveDate) {
		DateTime date = new DateTime(financialEffectiveDate.getTime());
		return date.getDayOfMonth() == 1 ? true : false;
	}
}
