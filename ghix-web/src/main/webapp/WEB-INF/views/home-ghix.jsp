<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="stateCode" value="${StateConfiguration.stateCode}" />
<c:if test="${stateCode == 'CA'}">
	<jsp:include page="./home/ca/home-ghix.jsp" />
</c:if>
<c:if test="${stateCode == 'CT'}">
	<jsp:include page="./home/ct/home-ghix.jsp" />
</c:if>
<c:if test="${stateCode == 'ID'}">
	<jsp:include page="./home/id/home-ghix.jsp" />
</c:if>
<c:if test="${stateCode == 'MN'}">
	<jsp:include page="./home/mn/home-ghix.jsp" />
</c:if>
<c:if test="${stateCode == 'MS'}">
	<jsp:include page="./home/ms/home-ghix.jsp" />
</c:if>
<c:if test="${stateCode == 'NM'}">
	<jsp:include page="./home/nm/home-ghix.jsp" />
</c:if>
<c:if test="${stateCode == 'NV'}">
	<jsp:include page="./home/nv/home-ghix.jsp" />
</c:if>
<c:if test="${stateCode == 'WA'}">
	<jsp:include page="./home/wa/home-ghix.jsp" />
</c:if>
