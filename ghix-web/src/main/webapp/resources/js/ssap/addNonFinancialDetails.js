var nonFinancialHouseHoldDone = 1 ;
var HouseHoldRepeat=1;
 
var HouseHoldgender ;
var HouseHoldHavingSSN ;
var  HouseHoldssn1 ;
var HouseHoldssn2 ;
var  HouseHoldssn3 ;
var  HouseHoldSSN ;
var  notAvailableSSNReason ;
var UScitizen ;
var naturalizedCitizenshipIndicator  ;
var livedIntheUSSince1996Indicator ;


var certificateType ;	
var certificateAlignNumber ;
var certificateNumber ;

var documentSameNameIndicator ;
var documentFirstName ;
var documentLastName ;
var documentMiddleName ;
var documentSuffix ;

var atLeastOneChildUnder19 ;
var takeCareOfSelf ;
var takeCareOfAnotherChild ;
var takeCareOfAnotherChildFirstName ; 
var takeCareOfAnotherChildMiddleName ;
var takeCareOfAnotherChildLastName ;
var takeCareOfAnotherChildSuffix ;
var takeCareOfAnotherChildDob ;
var relationShipWithAnotherChild ;
var doesLiveWithAdoptivePerent ;


function resetNonFinancialGlobalData()
{
	$("#askAboutSSNOnPage61").show();
	/*$("#sameSSNPage61Info").show();*/
	$("#mandateExemptionP61").show();
	$("#sameNameInfoPage62P2").show();
	//$(".parentOrCaretakerBasicDetailsContainer").show(); --Commented by Yogesh to avoid auto display of parentcaretaker details for second applicant
	 HouseHoldgender ='';
	 HouseHoldHavingSSN ='';
	 HouseHoldssn1=  '';
	 HouseHoldssn2= '';
	 HouseHoldssn3=  '';
	 HouseHoldSSN = '';
	 notAvailableSSNReason = '';
	 UScitizen='';
	 naturalizedCitizenshipIndicator = '';
	 livedIntheUSSince1996Indicator='';
	 

	 certificateType = '';
	 certificateAlignNumber = "";
	 certificateNumber = "";
	 
	 documentSameNameIndicator = '';
	 documentFirstName = '';
	 documentLastName = '';
	 documentMiddleName = '';
	 documentSuffix = '';
	 
	 atLeastOneChildUnder19 = '';
	 takeCareOfSelf = '';
	 takeCareOfAnotherChild = '';
	 takeCareOfAnotherChildFirstName = '';
	 takeCareOfAnotherChildMiddleName = '';
	 takeCareOfAnotherChildLastName = '';
	 takeCareOfAnotherChildSuffix = '';
	 takeCareOfAnotherChildDob = '';
	 relationShipWithAnotherChild = '';
	 doesLiveWithAdoptivePerent = '';
	 cleanAppscr67();
	 cleanAppscr61();
	 cleanAppscr63();
	 
}




function addNonFinancialHouseHold()
{
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}
	
	var noOfHouseHold = parseInt($('#numberOfHouseHold').text());
	if(currentPage == 'appscr64' && nonFinancialHouseHoldDone == noOfHouseHold)
	{	
	next();	
	return;
	}
	
	if(currentPage == 'appscr64' && editNonFinacial == true)
	{	
	next();	
	editNonFinacial == false;
	return;
	}
	
	if(currentPage == 'appscr64' && (editFlag == true || flagToUpdateFromResult== true)	)
	{
		
		
		appscr67EditControllerUpdateValues(editMemberNumber);
		updateNonFinancialDetailsToTable();
		//showHouseHoldContactSummary();
		
		if(flagToUpdateFromResult == true)
		{
		flagToUpdateFromResult=false;
		if(checkStillUpdateRequired())
			{
			
			return false;
			}
		else
			{
			goToPageById('appscrBloodRel');
			$('#finalEditDiv').show();
			return false;
			}
		
		}
		else
		goToPageById('appscr67');
		editFlag = false;		
		return false;
	}
	else if(editFlag == true || flagToUpdateFromResult== true)
	 {
		  $('#back_button_div').show();
		  var houseHoldname = $('#appscr57FirstNameTD'+editMemberNumber).text()+" "+$('#appscr57MiddleNameTD'+editMemberNumber).text()+" "+$('#appscr57LastNameTD'+editMemberNumber).text();
		  
		  $('.nameOfHouseHold').text(houseHoldname);
	 }
	
	
	$('#goBack').show();
	$('#prevBtn').attr('onclick','prev();');
	var noOfHouseHold = parseInt($('#numberOfHouseHold').text());
	
	
	
	 if(currentPage=='appscr64' && nonFinancialHouseHoldDone == noOfHouseHold)
	 {
		 updatePageOtherAddress();
		 populateDataToRelations();
		 setAddressDetailsToSpan();
	 }
	 
	if(currentPage=='appscr64')
		{
		
		if($('#appscrHouseHoldgender57TD'+nonFinancialHouseHoldDone).html() == undefined)
			addNonFinancialDetailsToTable();
		else
			updateNonFinancialDetailsToTable();
		resetNonFinancialGlobalData();
		resetAddNonFinancialDetails();
		}
	else
		if(currentPage=='appscr62Part1'){	
			
			checkUSCitizenForNextPage();
			
		}
	

	
	
	if(currentPage=='appscr64' && nonFinancialHouseHoldDone < noOfHouseHold)
			{
				
				nonFinancialHouseHoldDone++;
				$('#goBack').hide();
				while(getInfantData(nonFinancialHouseHoldDone)=='yes')
				{	
					setNonFinancialPageProperty(nonFinancialHouseHoldDone);
					if($('#appscrHouseHoldgender57TD'+nonFinancialHouseHoldDone).html() == undefined)
						addNonFinancialDetailsToTable();
					else
						updateNonFinancialDetailsToTable();
					resetNonFinancialGlobalData();
					if(nonFinancialHouseHoldDone == noOfHouseHold)
						break;
						nonFinancialHouseHoldDone++;
				}
				if(currentPage=='appscr64' && nonFinancialHouseHoldDone == noOfHouseHold && getInfantData(nonFinancialHouseHoldDone)=='yes')
				 {
					 updatePageOtherAddress();
					 populateDataToRelations();
					 setAddressDetailsToSpan();
					 setNonFinancialPageProperty(1);
					 next();	 
				 }
				else
					if(nonFinancialHouseHoldDone <= noOfHouseHold)
					{
					$('#DontaskAboutSSNOnPage61').hide();	
					goToPageById('appscr61');
					cleanAppscr67();
					resetNonFinancialGlobalData();
					resetAddNonFinancialDetails();
					setNonFinancialPageProperty(nonFinancialHouseHoldDone);
					}
					
				else
					{
					setNonFinancialPageProperty(1);
				
					next();	
					}
				
				if(haveData == true)
				{
					haveDataFlag = true;
					if(useJSON == true){
						appscr67EditController(nonFinancialHouseHoldDone);
					}else {
						if(nonFinancialHouseHoldDone<=houseHoldNumberValue)
							appscr67EditController(nonFinancialHouseHoldDone);		
					}					
					haveDataFlag = false;
				}
			}
	else
		if(nonFinancialHouseHoldDone == noOfHouseHold)
			{
			if(currentPage == "appscr64")
				fillValuesToFields("appscr64");
			next();
			$('#goBack').show();
		
			}
		else

			next();
	
	 
	
}


function addNonFinancialDetailsToTable()
{
	
	 
	 HouseHoldgender = $('input[name=appscr61_gender]:radio:checked').val();
	 HouseHoldHavingSSN =$('input[name=socialSecurityCardHolderIndicator]:radio:checked').val();
	 /*HouseHoldssn1=  $('#ssn1').val();
	 HouseHoldssn2=  $('#ssn2').val();
	 HouseHoldssn3=  $('#ssn3').val();
	 HouseHoldSSN = HouseHoldssn1 +'-'+HouseHoldssn2+'-'+HouseHoldssn3;*/
	 
	 HouseHoldSSN = $('#ssn').val();
	 notAvailableSSNReason = $('#reasonableExplanationForNoSSN').val();
	
	 UScitizen=$('input[name=UScitizen]:radio:checked').val(); 
	 
	 var UScitizenManualVerification= $('input[name=UScitizenManualVerification]:radio:checked').val();
	 
	 naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val();
	 livedIntheUSSince1996Indicator= $('input[name=livedIntheUSSince1996Indicator]:radio:checked').val();
	
	 var naturalCitizen_eligibleImmigrationStatus = $('#naturalCitizen_eligibleImmigrationStatus').prop('checked');
	 
	 certificateType = $('input[name=naturalizedCitizen]:radio:checked').val();	
	 certificateAlignNumber = "";
	 certificateNumber = "";
	 
	 documentSameNameIndicator = $('input[name=UScitizen62]:radio:checked').val();
	 documentFirstName = $('#documentFirstName').val();
	 documentLastName = $('#documentLastName').val();
	 documentMiddleName = $('#documentMiddleName').val();
	 documentSuffix = $('#documentSuffix').val();		
	 
	 atLeastOneChildUnder19 = $('input[name=appscr63AtLeastOneChildUnder19Indicator]:radio:checked').val();
	 //takeCareOfSelf = $('#takeCareOf_applicantOrNonapplicant').prop('checked');
	 takeCareOfAnotherChild = $('#takeCareOf_anotherChild').prop('checked');
	 takeCareOfAnotherChildFirstName = $('#parent-or-caretaker-firstName').val();
	 takeCareOfAnotherChildMiddleName = $('#parent-or-caretaker-middleName').val();
	 takeCareOfAnotherChildLastName = $('#parent-or-caretaker-lastName').val();
	 takeCareOfAnotherChildSuffix = $('#parent-or-caretaker-suffix').val();
	 takeCareOfAnotherChildDob = $('#parent-or-caretaker-dob').val();
	 relationShipWithAnotherChild = $('#applicant-relationship').val();
	 doesLiveWithAdoptivePerent = $('input[name=birthOrAdoptiveParents]:radio:checked').val();	
	 
	 var appendRowData='';
	 
	 if(getInfantData(nonFinancialHouseHoldDone)=='yes')
	 {
	 
		 	appendRowData+="<td class='sex' id='appscrHouseHoldgender57TD"+nonFinancialHouseHoldDone+"'>"+"male"+"</td>";
	 		appendRowData+="<td class='i_ssn_verified' id='appscr57HouseHoldHavingSSNTD"+nonFinancialHouseHoldDone+"'>"+"no"+"</td>";
			appendRowData+="<td class='i_ssn' id='appscr57HouseHoldSSNTD"+nonFinancialHouseHoldDone+"'>"+"--"+"</td>";			
			appendRowData+="<td class='sameNameInSSNCardIndicator' id='appscr57SameNameInSSNCardIndicatorTD"+nonFinancialHouseHoldDone+"'>no</td>";
			appendRowData+="<td class='firstNameOnSSNCard' id='appscr57FirstNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='middleNameOnSSNCard' id='appscr57MiddleNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='lastNameOnSSNCard' id='appscr57LastNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='suffixOnSSNCard' id='appscr57SuffixOnSSNCardTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
			appendRowData+="<td class='notAvailableSSNReason' id='appscr57notAvailableSSNReasonTD"+nonFinancialHouseHoldDone+"'>"+"no"+"</td>";
			
			appendRowData+="<td class='i_citizenship' id='appscr57UScitizenTD"+nonFinancialHouseHoldDone+"'>"+UScitizen+"</td>";			
			appendRowData+="<td class='i_citizenshipManualVerified' id='appscr57UScitizenManualVerificationTD"+nonFinancialHouseHoldDone+"'>"+UScitizenManualVerification+"</td>";			
					
			appendRowData+="<td class='i_naturalized' id='appscr57naturalizedCitizenshipIndicatorTD"+nonFinancialHouseHoldDone+"'>"+"no"+"</td>";						
			appendRowData+="<td class='i_certificateType' id='appscr57certificateTypeIndicatorTD"+nonFinancialHouseHoldDone+"'>"+certificateType+"</td>";
			appendRowData+="<td class='i_certificateAlignNumber' id='appscr57naturalizedCitizenshipAlignNumberTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='i_certificateNumber' id='appscr57certificateNumberTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='i_migrant' id='appscr57MigrantStatusIndicatorTD"+nonFinancialHouseHoldDone+"'>"+naturalCitizen_eligibleImmigrationStatus+"</td>";
			
			appendRowData+="<td class='documentSameNameIndicator' id='appscr57documentSameNameIndicatorTD"+nonFinancialHouseHoldDone+"'>yes</td>";
			appendRowData+="<td class='documentFirstName' id='appscr57documentFirstNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='documentMiddleName' id='appscr57documentMiddleNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='documentLastName' id='appscr57documentLastNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='documentSuffix' id='appscr57documentSuffixTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
			
			appendRowData+="<td class='citizenshipAsVerifiedIndicator' id='appscr57livedIntheUSSince1996IndicatorTD"+nonFinancialHouseHoldDone+"'>"+"false"+"</td>";
			
			appendRowData+="<td class='atLeastOneChildUnder19' id='appscr57AtLeastOneChildUnder19TD"+nonFinancialHouseHoldDone+"'>no</td>";
			appendRowData+="<td class='takeCareOfSelf' id='appscr57TakeCareOfSelfTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='takeCareOfAnotherChild' id='appscr57TakeCareOfAnotherChildTD"+nonFinancialHouseHoldDone+"'>false</td>";
			
			appendRowData+="<td class='takeCareOfAnotherChildFirstName' id='appscr57TakeCareOfAnotherChildFirstNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='takeCareOfAnotherChildMiddleName' id='appscr57TakeCareOfAnotherChildMiddleNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='takeCareOfAnotherChildLastName' id='appscr57TakeCareOfAnotherChildLastNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='takeCareOfAnotherChildSuffix' id='appscr57TakeCareOfAnotherChildSuffixTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
			appendRowData+="<td class='takeCareOfAnotherChildDob' id='appscr57TakeCareOfAnotherChildDobTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='relationShipWithAnotherChild' id='appscr57RelationShipWithAnotherChildTD"+nonFinancialHouseHoldDone+"'></td>";
			
			appendRowData+="<td class='doesLiveWithAdoptivePerent' id='appscr57DoesLiveWithAdoptivePerentTD"+nonFinancialHouseHoldDone+"'>no</td>";
			
			
			appendRowData+="<td class='ethnicityIndicator' id='appscr57EthnicityIndicatorTD"+nonFinancialHouseHoldDone+"'>yes</td>";
			
			appendRowData+="<td class='ethnicityCuban' id='appscr57EthnicityCubanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='ethnicityMexicanMexican' id='appscr57EthnicityMexicanMexicanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='ethnicityPuertoRican' id='appscr57EthnicityPuertoRicanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='ethnicityOther' id='appscr57EthnicityOtherTD"+nonFinancialHouseHoldDone+"'>true</td>";
			appendRowData+="<td class='ethnicityText' id='appscr57EthnicityTextTD"+nonFinancialHouseHoldDone+"'></td>";
			
			appendRowData+="<td class='raceAmericanIndian' id='appscr57RaceAmericanIndianTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceAsianIndian' id='appscr57RaceAsianIndianTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceBlackOrAfricanAmerican' id='appscr57RaceBlackOrAfricanAmericanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceChinese' id='appscr57RaceChineseTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceFilipino' id='appscr57RaceFilipinoTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceGuamanianOrChamorro' id='appscr57RaceGuamanianOrChamorroTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceJapanese' id='appscr57RaceJapaneseTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceKorean' id='appscr57RaceKoreanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceNativeHawaiian' id='appscr57RaceNativeHawaiianTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceOtherAsian' id='appscr57RaceOtherAsianTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceOtherPacificIslander' id='appscr57RaceOtherPacificIslanderTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceSamoan' id='appscr57RaceSamoanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceVietnamese' id='appscr57RaceVietnameseTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceWhiteOrCaucasian' id='appscr57RaceWhiteOrCaucasianTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='raceOther' id='appscr57RaceOtherTD"+nonFinancialHouseHoldDone+"'>true</td>";
			appendRowData+="<td class='raceOtherText' id='appscr57RaceOtherTextTD"+nonFinancialHouseHoldDone+"'></td>";
		 
	 }
 else{
	 	appendRowData+="<td class='sex' id='appscrHouseHoldgender57TD"+nonFinancialHouseHoldDone+"'>"+HouseHoldgender+"</td>";
		appendRowData+="<td class='i_ssn_verified' id='appscr57HouseHoldHavingSSNTD"+nonFinancialHouseHoldDone+"'>"+HouseHoldHavingSSN+"</td>";
		appendRowData+="<td class='i_ssn' id='appscr57HouseHoldSSNTD"+nonFinancialHouseHoldDone+"'>"+HouseHoldSSN+"</td>";
		if($('input[name=fnlnsSame]:radio:checked').val()=="yes")
		{
			appendRowData+="<td class='sameNameInSSNCardIndicator' id='appscr57SameNameInSSNCardIndicatorTD"+nonFinancialHouseHoldDone+"'>yes</td>";
			appendRowData+="<td class='firstNameOnSSNCard' id='appscr57FirstNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='middleNameOnSSNCard' id='appscr57MiddleNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='lastNameOnSSNCard' id='appscr57LastNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='suffixOnSSNCard' id='appscr57SuffixOnSSNCardTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
		}
		else
		{
			appendRowData+="<td class='sameNameInSSNCardIndicator' id='appscr57SameNameInSSNCardIndicatorTD"+nonFinancialHouseHoldDone+"'>no</td>";
			appendRowData+="<td class='firstNameOnSSNCard' id='appscr57FirstNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'>"+$('#firstNameOnSSNCard').val()+"</td>";
			appendRowData+="<td class='middleNameOnSSNCard' id='appscr57MiddleNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'>"+$('#middleNameOnSSNCard').val()+"</td>";
			appendRowData+="<td class='lastNameOnSSNCard' id='appscr57LastNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'>"+$('#lastNameOnSSNCard').val()+"</td>";
			appendRowData+="<td class='suffixOnSSNCard' id='appscr57SuffixOnSSNCardTD"+nonFinancialHouseHoldDone+"'>"+$('#suffixOnSSNCard').val()+"</td>";
		}		
		appendRowData+="<td class='notAvailableSSNReason' id='appscr57notAvailableSSNReasonTD"+nonFinancialHouseHoldDone+"'>"+notAvailableSSNReason+"</td>";
		
		appendRowData+="<td class='i_citizenship' id='appscr57UScitizenTD"+nonFinancialHouseHoldDone+"'>"+UScitizen+"</td>";		
		appendRowData+="<td class='i_citizenshipManualVerified' id='appscr57UScitizenManualVerificationTD"+nonFinancialHouseHoldDone+"'>"+UScitizenManualVerification+"</td>";		
		appendRowData+="<td class='i_naturalized' id='appscr57naturalizedCitizenshipIndicatorTD"+nonFinancialHouseHoldDone+"'>"+naturalizedCitizenshipIndicator+"</td>";
				
		if(certificateType == "CitizenshipCertificate")
		{
			certificateAlignNumber = $('#citizenshipAlignNumber').val();
			certificateNumber = $('#appscr62p1citizenshipCertificateNumber').val();
			appendRowData+="<td class='i_certificateType' id='appscr57certificateTypeIndicatorTD"+nonFinancialHouseHoldDone+"'>"+certificateType+"</td>";
			appendRowData+="<td class='i_certificateAlignNumber' id='appscr57naturalizedCitizenshipAlignNumberTD"+nonFinancialHouseHoldDone+"'>"+certificateAlignNumber+"</td>";
			appendRowData+="<td class='i_certificateNumber' id='appscr57certificateNumberTD"+nonFinancialHouseHoldDone+"'>"+certificateNumber+"</td>";
		}
		else
		{
			certificateAlignNumber = $('#naturalizationAlignNumber').val();
			certificateNumber = $('#naturalizationCertificateNumber').val();
						
			appendRowData+="<td class='i_certificateType' id='appscr57certificateTypeIndicatorTD"+nonFinancialHouseHoldDone+"'>"+certificateType+"</td>";
			appendRowData+="<td class='i_certificateAlignNumber' id='appscr57naturalizedCitizenshipAlignNumberTD"+nonFinancialHouseHoldDone+"'>"+certificateAlignNumber+"</td>";
			appendRowData+="<td class='i_certificateNumber' id='appscr57certificateNumberTD"+nonFinancialHouseHoldDone+"'>"+certificateNumber+"</td>";
		}
		appendRowData+="<td class='i_migrant' id='appscr57ImmigrationStatusTD"+nonFinancialHouseHoldDone+"'>"+naturalCitizen_eligibleImmigrationStatus+"</td>";
		
		appendRowData+="<td class='documentSameNameIndicator' id='appscr57documentSameNameIndicatorTD"+nonFinancialHouseHoldDone+"'>"+documentSameNameIndicator+"</td>";
		if(documentSameNameIndicator == "no")
		{
			appendRowData+="<td class='documentFirstName' id='appscr57documentFirstNameTD"+nonFinancialHouseHoldDone+"'>"+documentFirstName+"</td>";
			appendRowData+="<td class='documentMiddleName' id='appscr57documentMiddleNameTD"+nonFinancialHouseHoldDone+"'>"+documentMiddleName+"</td>";
			appendRowData+="<td class='documentLastName' id='appscr57documentLastNameTD"+nonFinancialHouseHoldDone+"'>"+documentLastName+"</td>";
			appendRowData+="<td class='documentSuffix' id='appscr57documentSuffixTD"+nonFinancialHouseHoldDone+"'>"+documentSuffix+"</td>";
		}
		else
		{
			appendRowData+="<td class='documentFirstName' id='appscr57documentFirstNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='documentMiddleName' id='appscr57documentMiddleNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='documentLastName' id='appscr57documentLastNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='documentSuffix' id='appscr57documentSuffixTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
		}
		appendRowData+="<td class='citizenshipAsVerifiedIndicator' id='appscr57livedIntheUSSince1996IndicatorTD"+nonFinancialHouseHoldDone+"'>"+livedIntheUSSince1996Indicator+"</td>";
		
		
		appendRowData+="<td class='atLeastOneChildUnder19' id='appscr57AtLeastOneChildUnder19TD"+nonFinancialHouseHoldDone+"'>"+atLeastOneChildUnder19+"</td>";
		appendRowData+="<td class='takeCareOfSelf' id='appscr57TakeCareOfSelfTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfSelf+"</td>";
		appendRowData+="<td class='takeCareOfAnotherChild' id='appscr57TakeCareOfAnotherChildTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChild+"</td>";
		if(takeCareOfAnotherChild == true || takeCareOfAnotherChild == "true")
		{
			appendRowData+="<td class='takeCareOfAnotherChildFirstName' id='appscr57TakeCareOfAnotherChildFirstNameTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildFirstName+"</td>";
			appendRowData+="<td class='takeCareOfAnotherChildMiddleName' id='appscr57TakeCareOfAnotherChildMiddleNameTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildMiddleName+"</td>";
			appendRowData+="<td class='takeCareOfAnotherChildLastName' id='appscr57TakeCareOfAnotherChildLastNameTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildLastName+"</td>";
			appendRowData+="<td class='takeCareOfAnotherChildSuffix' id='appscr57TakeCareOfAnotherChildSuffixTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildSuffix+"</td>";
			appendRowData+="<td class='takeCareOfAnotherChildDob' id='appscr57TakeCareOfAnotherChildDobTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildDob+"</td>";
			appendRowData+="<td class='relationShipWithAnotherChild' id='appscr57RelationShipWithAnotherChildTD"+nonFinancialHouseHoldDone+"'>"+relationShipWithAnotherChild+"</td>";
		}
		else
		{
			appendRowData+="<td class='takeCareOfAnotherChildFirstName' id='appscr57TakeCareOfAnotherChildFirstNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='takeCareOfAnotherChildMiddleName' id='appscr57TakeCareOfAnotherChildMiddleNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='takeCareOfAnotherChildLastName' id='appscr57TakeCareOfAnotherChildLastNameTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='takeCareOfAnotherChildSuffix' id='appscr57TakeCareOfAnotherChildSuffixTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
			appendRowData+="<td class='takeCareOfAnotherChildDob' id='appscr57TakeCareOfAnotherChildDobTD"+nonFinancialHouseHoldDone+"'></td>";
			appendRowData+="<td class='relationShipWithAnotherChild' id='appscr57RelationShipWithAnotherChildTD"+nonFinancialHouseHoldDone+"'></td>";
		}
		
		appendRowData+="<td class='doesLiveWithAdoptivePerent' id='appscr57DoesLiveWithAdoptivePerentTD"+nonFinancialHouseHoldDone+"'>"+doesLiveWithAdoptivePerent+"</td>";
		
		var ethnicityIndicator = $('input[name=checkPersonNameLanguage]:radio:checked').val();
		if(ethnicityIndicator == "yes" || ethnicityIndicator == "Yes")
		{
			appendRowData+="<td class='ethnicityIndicator' id='appscr57EthnicityIndicatorTD"+nonFinancialHouseHoldDone+"'>yes</td>";
			
			appendRowData+="<td class='ethnicityCuban' id='appscr57EthnicityCubanTD"+nonFinancialHouseHoldDone+"'>"+$('#ethnicity_cuban').prop("checked")+"</td>";
			appendRowData+="<td class='ethnicityMexicanMexican' id='appscr57EthnicityMexicanMexicanTD"+nonFinancialHouseHoldDone+"'>"+$('#ethnicity_maxican').prop("checked")+"</td>";
			appendRowData+="<td class='ethnicityPuertoRican' id='appscr57EthnicityPuertoRicanTD"+nonFinancialHouseHoldDone+"'>"+$('#ethnicity_puertoRican').prop("checked")+"</td>";
			/*appendRowData+="<td class='ethnicityOther' id='appscr57EthnicityOtherTD"+nonFinancialHouseHoldDone+"'>"+$('#ethnicity_other').prop("")+"</td>";
			appendRowData+="<td class='ethnicityText' id='appscr57EthnicityTextTD"+nonFinancialHouseHoldDone+"'>"+$('#other_ethnicity ').val()+"</td>";*/
		}
		else
		{
			appendRowData+="<td class='ethnicityIndicator' id='appscr57EthnicityIndicatorTD"+nonFinancialHouseHoldDone+"'>no</td>";
			
			appendRowData+="<td class='ethnicityCuban' id='appscr57EthnicityCubanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='ethnicityMexicanMexican' id='appscr57EthnicityMexicanMexicanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='ethnicityPuertoRican' id='appscr57EthnicityPuertoRicanTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='ethnicityOther' id='appscr57EthnicityOtherTD"+nonFinancialHouseHoldDone+"'>false</td>";
			appendRowData+="<td class='ethnicityText' id='appscr57EthnicityTextTD"+nonFinancialHouseHoldDone+"'></td>";
		}
		appendRowData+="<td class='raceAmericanIndian' id='appscr57RaceAmericanIndianTD"+nonFinancialHouseHoldDone+"'>"+$('#americanIndianOrAlaskaNative').prop("checked")+"</td>";
		appendRowData+="<td class='raceAsianIndian' id='appscr57RaceAsianIndianTD"+nonFinancialHouseHoldDone+"'>"+$('#asianIndian').prop("checked")+"</td>";
		appendRowData+="<td class='raceBlackOrAfricanAmerican' id='appscr57RaceBlackOrAfricanAmericanTD"+nonFinancialHouseHoldDone+"'>"+$('#BlackOrAfricanAmerican').prop("checked")+"</td>";
		appendRowData+="<td class='raceChinese' id='appscr57RaceChineseTD"+nonFinancialHouseHoldDone+"'>"+$('#chinese').prop("checked")+"</td>";
		appendRowData+="<td class='raceFilipino' id='appscr57RaceFilipinoTD"+nonFinancialHouseHoldDone+"'>"+$('#filipino').prop("checked")+"</td>";
		appendRowData+="<td class='raceGuamanianOrChamorro' id='appscr57RaceGuamanianOrChamorroTD"+nonFinancialHouseHoldDone+"'>"+$('#guamanianOrChamorro').prop("checked")+"</td>";
		appendRowData+="<td class='raceJapanese' id='appscr57RaceJapaneseTD"+nonFinancialHouseHoldDone+"'>"+$('#japanese').prop("checked")+"</td>";
		appendRowData+="<td class='raceKorean' id='appscr57RaceKoreanTD"+nonFinancialHouseHoldDone+"'>"+$('#korean').prop("checked")+"</td>";
		appendRowData+="<td class='raceNativeHawaiian' id='appscr57RaceNativeHawaiianTD"+nonFinancialHouseHoldDone+"'>"+$('#nativeHawaiian').prop("checked")+"</td>";
		appendRowData+="<td class='raceOtherAsian' id='appscr57RaceOtherAsianTD"+nonFinancialHouseHoldDone+"'>"+$('#otherAsian').prop("checked")+"</td>";
		appendRowData+="<td class='raceOtherPacificIslander' id='appscr57RaceOtherPacificIslanderTD"+nonFinancialHouseHoldDone+"'>"+$('#otherPacificIslander').prop("checked")+"</td>";
		appendRowData+="<td class='raceSamoan' id='appscr57RaceSamoanTD"+nonFinancialHouseHoldDone+"'>"+$('#samoan').prop("checked")+"</td>";
		appendRowData+="<td class='raceVietnamese' id='appscr57RaceVietnameseTD"+nonFinancialHouseHoldDone+"'>"+$('#vietnamese').prop("checked")+"</td>";
		appendRowData+="<td class='raceWhiteOrCaucasian' id='appscr57RaceWhiteOrCaucasianTD"+nonFinancialHouseHoldDone+"'>"+$('#whiteOrCaucasian').prop("checked")+"</td>";
		/*appendRowData+="<td class='raceOther' id='appscr57RaceOtherTD"+nonFinancialHouseHoldDone+"'>"+$('#raceOther').prop("")+"</td>";
		appendRowData+="<td class='raceOtherText' id='appscr57RaceOtherTextTD"+nonFinancialHouseHoldDone+"'>"+$('#other_race').val()+"</td>";*/
		
	 }
		//$('#appscr57HouseHoldPersonalInformation').find('#'+$('#houseHoldTRNo').val()).append(appendRowData);
	 $('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+getCurrentApplicantID()).append(appendRowData);
		
}

function resetAddNonFinancialDetails()
{
	/// PageId 61
	$('#appscr61Form input:text').val('');
	$('#appscr61Form select').val('0');
	
	/*$("#appscr61GenderMaleID").prop("checked", true);
	$("#socialSecurityCardHolderIndicatorYes").prop("checked", true);
	$("#fnlnsSameIndicatorNo").prop("checked", true);
	$("#fnlnsExemptionIndicatorYes").prop("checked", true);	*/
	
	/*$('#sameSSNPage61Info').show();*/
	
	/// PageId 62Part1
	$('#appscr62Part1Form input:text').val('');
	$('#appscr62Part1Form select').val('0');
	//$("#UScitizenIndicatorYes").prop("checked", true);
	//$("#naturalizedCitizenshipIndicatorYes").prop("checked", true);
	$("#naturalizedCitizenNaturalizedIndicator").prop("checked", false);
	//document.getElementById('naturalizedDonotHave').checked = false;
	//document.getElementById('citizenDonotHave').checked = false;
	//$("#naturalCitizen_eligibleImmigrationStatus").prop("checked", true);
	//$("#docType_other").prop("checked", true);
	showOrHideNaturalCitizenImmigrationStatus();
	/// PageId 62Part2
	$('#appscr62Part2Form input:text').val('');
	$('#appscr62Part2Form select').val('0');
	$("#docType_otherDocs").prop("checked", true);
	showOrHideAt62Part2();
	$('input[name=UScitizen62]').prop("checked", false);
	displayOrHideSameNameInfoPage62();
//	/$("#62Part_livesInUS").prop("checked", true);
	$("#62Part_livedIntheUSSince1996Indicator").prop("checked", true);
	
	$("#otherDocStatusOnPage62P1").show();
	isNaturalizedCitizen();
	/// PageId 63
	$('#appscr63Form input:text').val('');
	$('#appscr63Form select').val('0');
	$("#liveOneChildAge19").prop("checked", true);
	//document.getElementById('takeCareOf_applicantOrNonapplicant').checked = false;
	//document.getElementById('takeCareOf_anotherChild').checked = true;
	//$('#appscr63BirthOrAdoptiveParentsNo').prop("checked", true);
	
	/// PageId 64
	//$("#checkPersonNameLanguageYes").prop("checked", true);
	$('#appscr64Form input:checkbox').attr('checked',false);
	$('#appscr64Form input:text').val('');
	/*document.getElementById('ethnicity_other').checked = false;
	document.getElementById('raceOther').checked = false;*/
	
	
	
	$('#ethnicityDiv').hide();
	$("#documentTypeDiv").show();
	
	$("#naturalizedCitizenDiv").hide();
	$("#documentTypeDiv").hide();
	$("#eligibleImmigrationStatusDiv").hide();
	
	cleanAppscr61();
	fillAppscr61(nonFinancialHouseHoldDone);
}


function updatePageOtherAddress()
{
	
	var houseHoldName='';
	$('#noOfOtherApplicant').html('');
	
	$('#HouseHoldAddressDiv').html('');
	
	var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	for(var i=1;i<=noOfHouseHoldmember;i++){
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var nameWithSpace=""+trimSpaces(householdMemberObj.name.firstName) +" "+trimSpaces(householdMemberObj.name.middleName)+" "+trimSpaces(householdMemberObj.name.lastName);
		var pureNameOfUser = trimSpaces(householdMemberObj.name.firstName) + "_" + trimSpaces(householdMemberObj.name.middleName) + "_" + trimSpaces(householdMemberObj.name.lastName);
		
		if(i>1){
			
			var chDATA="<label  class='capitalize-none' ><input type='checkbox'   id='householdContactAddress"+i+"' onclick=\"checkUncheck('"+i+"')\""+" name='householdContactAddress' value='"+pureNameOfUser+"' data-parsley-errors-container='#householdContactAddressErrorDiv' data-parsley-required data-parsley-required-message='"+ jQuery.i18n.prop("ssap.required.field") +"'><span class='camelCaseName'>"+nameWithSpace+"</span></label>";
			
			
			namesOfHouseHold.push(pureNameOfUser);				
			
			
			$('#noOfOtherApplicant').append(chDATA);
			
			$('#HouseHoldAddressDiv').append( getAddressData(pureNameOfUser, i, houseHoldName));
			
			/*HIX-39915*/
			if(exchangeName == "NMHIX"){
				$(".stateName").text("New Mexico");
			}else{
				$(".stateName").text("Idaho");
			}

			
			$('input[name=isTemporarilyLivingOutsideIndicator'+i+']').change(function(){
						
				if($(this).val()=="yes")
				{	
					$(this).closest('.formContainerSecound').find('div[id^=inNMDiv]').show();
					//$('#inNMDiv'+pureNameOfUser).show();
				}
				else
				{	$(this).closest('.formContainerSecound').find('div[id^=inNMDiv]').hide();
					//$('#inNMDiv'+pureNameOfUser).hide();
				}

			});
			
		}
	}
	
	var otherDATA='<label  class="capitalize-none">'
		+'<input type="checkbox" id="HouseHoldotherAddressNoneOfTheAbove" onchange="otherAddressNoneOfTheAbove()" name="householdContactAddress" value="others"'
		+' data-parsley-required data-parsley-required-message="' + jQuery.i18n.prop("ssap.required.field") + '" data-parsley-errors-container="#householdContactAddressErrorDiv"'
		+' />'
		+'None of the Above</div>'
		+'</label><div id="householdContactAddressErrorDiv"></div>';
	$('#noOfOtherApplicant').append(otherDATA);
	
	//removeRequiredValidation();
}


//KEY FOR STATES

function getAddressData(applicantActualName, i, houseHoldName)
{
	houseHoldName = getNameRemovedBySpace(houseHoldName);
	var nameOfSpacedName = getNameRemovedBySpace(applicantActualName);
	
	var data ='<div id="other_Address_'+i+'" class="formContainerSecound hide">'
	+'<div class="control-group"><p>'
	+'Where does <strong><span class = "camelCaseName">'+nameOfSpacedName+'</span></strong> live? </span>'
	+'</p></div>';
	
	if(i<2){
		data+='<div class="hide">';
	}else{
		data+='<div>';
	}
	
	data+='</div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.address1")+''
		+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<input type="text" placeholder="Address 1" value="" name="applicantOrNon-applicantAddress1" id="applicant_or_non-applican_address_1'+i+'" class="input-medium" data-parsley-pattern="^[A-Za-z0-9\\\'\\.\\-\\s\\,]{5,75}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.addressValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.addressRequired")+'"/>'
		+'</div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.address2")+'</label>'
		+'<div class="controls">'
		+'<input type="text" placeholder="Address 2" value="" name="applicantOrapplicantAddress2" id="applicant_or_non-applican_address_2'+i+'" class="input-medium" data-parsley-pattern="^[A-Za-z0-9\\\'\\.\\-\\s\\,]{5,75}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.addressValid")+'"/>'
		+'</div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.city")+''
		+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<input type="text" placeholder="City" value="" name="city" id="city'+i+'" class="input-medium" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.cityRequired")+'"  data-parsley-pattern="^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.cityValid")+'" data-parsley-length="[1, 100]" data-parsley-length-message="'+jQuery.i18n.prop("ssap.error.cityValid")+'" />'
		+'</div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.zip")+''
		+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<input type="text" placeholder="Zip" value="" name="zip" id="zip'+i+'" class="input-medium" onblur="getCountyListByZipCode('+i+')" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.zipRequired")+'" data-parsley-pattern="^[0-9]{5}(\\-[0-9]{4})?$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.zipValid")+'"/>'
		+'</div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.State")+''
		+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<select name="applicantOrNon-applicantState" id="applicant_or_non-applican_state'+i+'" class="input-medium" onChange="getCountyListByZipCode('+i+')" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.stateRequired")+'">'
		+'<option value="">'+jQuery.i18n.prop("ssap.State")+'</option>'
		+'<option value="AL">'+jQuery.i18n.prop("ssap.state.Albama")+'</option>'
		+'<option value="AK">'+jQuery.i18n.prop("ssap.state.Alaska")+'</option>'
		+'<option value="AS">'+jQuery.i18n.prop("ssap.state.AmericanSamoa")+'</option>'
		+'<option value="AZ">'+jQuery.i18n.prop("ssap.state.Arizona")+'</option>'
		+'<option value="AR">'+jQuery.i18n.prop("ssap.state.Arkansas")+'</option>'
		+'<option value="CA">'+jQuery.i18n.prop("ssap.state.California")+'</option>'
		+'<option value="CO">'+jQuery.i18n.prop("ssap.state.Colorado")+'</option>'
		+'<option value="CT">'+jQuery.i18n.prop("ssap.state.Connecticut")+'</option>'
		+'<option value="DE">'+jQuery.i18n.prop("ssap.state.Delaware")+'</option>'
		+'<option value="DC">'+jQuery.i18n.prop("ssap.state.DistrictOfColumbia")+'</option>'
		+'<option value="FL">'+jQuery.i18n.prop("ssap.state.Florida")+'</option>'
		+'<option value="GA">'+jQuery.i18n.prop("ssap.state.Georgia")+'</option>'
		+'<option value="GU">'+jQuery.i18n.prop("ssap.state.Guam")+'</option>'
		+'<option value="HI">'+jQuery.i18n.prop("ssap.state.Hawaii")+'</option>'
		+'<option value="ID">'+jQuery.i18n.prop("ssap.state.Idaho")+'</option>'
		+'<option value="IL">'+jQuery.i18n.prop("ssap.state.Illinois")+'</option>'
		+'<option value="IN">'+jQuery.i18n.prop("ssap.state.Indiana")+'</option>'
		+'<option value="IA">'+jQuery.i18n.prop("ssap.state.Iowa")+'</option>'
		+'<option value="KS">'+jQuery.i18n.prop("ssap.state.Kansas")+'</option>'
		+'<option value="KY">'+jQuery.i18n.prop("ssap.state.Kentucky")+'</option>'
		+'<option value="LA">'+jQuery.i18n.prop("ssap.state.Louisiana")+'</option>'
		+'<option value="ME">'+jQuery.i18n.prop("ssap.state.Maine")+'</option>'
		+'<option value="MD">'+jQuery.i18n.prop("ssap.state.Maryland")+'</option>'
		+'<option value="MA">'+jQuery.i18n.prop("ssap.state.Massachusetts")+'</option>'
		+'<option value="MI">'+jQuery.i18n.prop("ssap.state.Michigan")+'</option>'
		+'<option value="MN">'+jQuery.i18n.prop("ssap.state.Minnesota")+'</option>'
		+'<option value="MS">'+jQuery.i18n.prop("ssap.state.Mississippi")+'</option>'
		+'<option value="MO">'+jQuery.i18n.prop("ssap.state.Missouri")+'</option>'
		+'<option value="MT">'+jQuery.i18n.prop("ssap.state.Montana")+'</option>'
		+'<option value="NE">'+jQuery.i18n.prop("ssap.state.Nebraska")+'</option>'
		+'<option value="NV">'+jQuery.i18n.prop("ssap.state.Nevada")+'</option>'
		+'<option value="NH">'+jQuery.i18n.prop("ssap.state.NewHampshire")+'</option>'
		+'<option value="NJ">'+jQuery.i18n.prop("ssap.state.NewJersey")+'</option>'
		+'<option value="NM">'+jQuery.i18n.prop("ssap.state.NewMexico")+'</option>'
		+'<option value="NY">'+jQuery.i18n.prop("ssap.state.NewYork")+'</option>'
		+'<option value="NC">'+jQuery.i18n.prop("ssap.state.NorthCarolina")+'</option>'
		+'<option value="ND">'+jQuery.i18n.prop("ssap.state.NorthDakota")+'</option>'
		+'<option value="MP">'+jQuery.i18n.prop("ssap.state.NorthernMarianasIslands")+'</option>'
		+'<option value="OH">'+jQuery.i18n.prop("ssap.state.Ohio")+'</option>'
		+'<option value="OK">'+jQuery.i18n.prop("ssap.state.Oklahoma")+'</option>'
		+'<option value="OR">'+jQuery.i18n.prop("ssap.state.Oregon")+'</option>'
		+'<option value="PA">'+jQuery.i18n.prop("ssap.state.Pennsylvania")+'</option>'
		+'<option value="PR">'+jQuery.i18n.prop("ssap.state.PuertoRico")+'</option>'
		+'<option value="RI">'+jQuery.i18n.prop("ssap.state.RhodeIsland")+'</option>'
		+'<option value="SC">'+jQuery.i18n.prop("ssap.state.SouthCarolina")+'</option>'
		+'<option value="SD">'+jQuery.i18n.prop("ssap.state.SouthDakota")+'</option>'
		+'<option value="TN">'+jQuery.i18n.prop("ssap.state.Tennessee")+'</option>'
		+'<option value="TX">'+jQuery.i18n.prop("ssap.state.Texas")+'</option>'
		+'<option value="UT">'+jQuery.i18n.prop("ssap.state.Utah")+'</option>'
		+'<option value="VT">'+jQuery.i18n.prop("ssap.state.Vermont")+'</option>'
		+'<option value="VA">'+jQuery.i18n.prop("ssap.state.Virginia")+'</option>'
		+'<option value="VI">'+jQuery.i18n.prop("ssap.state.VirginIslands")+'</option>'
		+'<option value="WA">'+jQuery.i18n.prop("ssap.state.Washington")+'</option>'
		+'<option value="WV">'+jQuery.i18n.prop("ssap.state.WestVirginia")+'</option>'
		+'<option value="WI">'+jQuery.i18n.prop("ssap.state.Wisconsin")+'</option>'
		+'<option value="WY">'+jQuery.i18n.prop("ssap.state.Wyoming")+'</option>'
		+'</select>'
		+'</div></div>'
		+'<div class="control-group">'
		+'<label class="control-label" for="home_primary_county">'+jQuery.i18n.prop("ssap.county")+''		
		+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
		+'<span aria-label="Required!"></span></label>'
		+ '<div class="controls">'		
		+'<select data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.county")+'" id="applicant_or_non-applican_county'+i+'" name="home_primary_county" class="input-large" data-rel="countyPopulate">'
		+'<option value=""><spring:message code="ssap.County" text="Select County" /></option></select>'
		+'</div></div>'
		+'<p>Is&nbsp;<strong>'+nameOfSpacedName+'</strong>&nbsp;living outside <span class="stateName"></span> temporarily?<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />'
		+'<div class="margin5-l controls"><label>'
		+'<input type="radio" value="yes" id="TemporarilyLivingOutsideIndicatorYes'+i+'" name="isTemporarilyLivingOutsideIndicator'+i+'" required parsley-error-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>Yes'
		+'</label><label>'
		+'<input type="radio" id="TemporarilyLivingOutsideIndicatorNo" value="no" checked  name="isTemporarilyLivingOutsideIndicator'+i+'" />No'
		+'</label></div>'
		+'<div class="hide" id="inNMDiv'+i+'">'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.city")+''
		+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<input type="text" placeholder="City" value="" name="applicant2City" id="applicant2city'+i+'" class="input-medium" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.cityRequired")+'"  data-parsley-pattern="^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.cityValid")+'" data-parsley-length="[1, 100]" data-parsley-length-message="'+jQuery.i18n.prop("ssap.error.cityValid")+'"/>'
		+'</div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.zip")+''
		+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<input type="text" placeholder="Zip" value="" name="applicant2Zip" id="applicant-2-zip'+i+'" class="input-medium" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.zipRequired")+'" data-parsley-pattern="^[0-9]{5}(\\-[0-9]{4})?$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.zipValid")+'"/>'
		+'</div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.State")+''
		+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<select name="applicantOrNon-applicantState" id="applicant_or_non-applican_stateTemp'+i+'" class="input-medium" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.stateRequired")+'">'
		+'<option value="">'+jQuery.i18n.prop("ssap.State")+'</option>'
		+'<option value="AL">'+jQuery.i18n.prop("ssap.state.Albama")+'</option>'
		+'<option value="AK">'+jQuery.i18n.prop("ssap.state.Alaska")+'</option>'
		+'<option value="AS">'+jQuery.i18n.prop("ssap.state.AmericanSamoa")+'</option>'
		+'<option value="AZ">'+jQuery.i18n.prop("ssap.state.Arizona")+'</option>'
		+'<option value="AR">'+jQuery.i18n.prop("ssap.state.Arkansas")+'</option>'
		+'<option value="CA">'+jQuery.i18n.prop("ssap.state.California")+'</option>'
		+'<option value="CO">'+jQuery.i18n.prop("ssap.state.Colorado")+'</option>'
		+'<option value="CT">'+jQuery.i18n.prop("ssap.state.Connecticut")+'</option>'
		+'<option value="DE">'+jQuery.i18n.prop("ssap.state.Delaware")+'</option>'
		+'<option value="DC">'+jQuery.i18n.prop("ssap.state.DistrictOfColumbia")+'</option>'
		+'<option value="FL">'+jQuery.i18n.prop("ssap.state.Florida")+'</option>'
		+'<option value="GA">'+jQuery.i18n.prop("ssap.state.Georgia")+'</option>'
		+'<option value="GU">'+jQuery.i18n.prop("ssap.state.Guam")+'</option>'
		+'<option value="HI">'+jQuery.i18n.prop("ssap.state.Hawaii")+'</option>'
		+'<option value="ID">'+jQuery.i18n.prop("ssap.state.Idaho")+'</option>'
		+'<option value="IL">'+jQuery.i18n.prop("ssap.state.Illinois")+'</option>'
		+'<option value="IN">'+jQuery.i18n.prop("ssap.state.Indiana")+'</option>'
		+'<option value="IA">'+jQuery.i18n.prop("ssap.state.Iowa")+'</option>'
		+'<option value="KS">'+jQuery.i18n.prop("ssap.state.Kansas")+'</option>'
		+'<option value="KY">'+jQuery.i18n.prop("ssap.state.Kentucky")+'</option>'
		+'<option value="LA">'+jQuery.i18n.prop("ssap.state.Louisiana")+'</option>'
		+'<option value="ME">'+jQuery.i18n.prop("ssap.state.Maine")+'</option>'
		+'<option value="MD">'+jQuery.i18n.prop("ssap.state.Maryland")+'</option>'
		+'<option value="MA">'+jQuery.i18n.prop("ssap.state.Massachusetts")+'</option>'
		+'<option value="MI">'+jQuery.i18n.prop("ssap.state.Michigan")+'</option>'
		+'<option value="MN">'+jQuery.i18n.prop("ssap.state.Minnesota")+'</option>'
		+'<option value="MS">'+jQuery.i18n.prop("ssap.state.Mississippi")+'</option>'
		+'<option value="MO">'+jQuery.i18n.prop("ssap.state.Missouri")+'</option>'
		+'<option value="MT">'+jQuery.i18n.prop("ssap.state.Montana")+'</option>'
		+'<option value="NE">'+jQuery.i18n.prop("ssap.state.Nebraska")+'</option>'
		+'<option value="NV">'+jQuery.i18n.prop("ssap.state.Nevada")+'</option>'
		+'<option value="NH">'+jQuery.i18n.prop("ssap.state.NewHampshire")+'</option>'
		+'<option value="NJ">'+jQuery.i18n.prop("ssap.state.NewJersey")+'</option>'
		+'<option value="NM">'+jQuery.i18n.prop("ssap.state.NewMexico")+'</option>'
		+'<option value="NY">'+jQuery.i18n.prop("ssap.state.NewYork")+'</option>'
		+'<option value="NC">'+jQuery.i18n.prop("ssap.state.NorthCarolina")+'</option>'
		+'<option value="ND">'+jQuery.i18n.prop("ssap.state.NorthDakota")+'</option>'
		+'<option value="MP">'+jQuery.i18n.prop("ssap.state.NorthernMarianasIslands")+'</option>'
		+'<option value="OH">'+jQuery.i18n.prop("ssap.state.Ohio")+'</option>'
		+'<option value="OK">'+jQuery.i18n.prop("ssap.state.Oklahoma")+'</option>'
		+'<option value="OR">'+jQuery.i18n.prop("ssap.state.Oregon")+'</option>'
		+'<option value="PA">'+jQuery.i18n.prop("ssap.state.Pennsylvania")+'</option>'
		+'<option value="PR">'+jQuery.i18n.prop("ssap.state.PuertoRico")+'</option>'
		+'<option value="RI">'+jQuery.i18n.prop("ssap.state.RhodeIsland")+'</option>'
		+'<option value="SC">'+jQuery.i18n.prop("ssap.state.SouthCarolina")+'</option>'
		+'<option value="SD">'+jQuery.i18n.prop("ssap.state.SouthDakota")+'</option>'
		+'<option value="TN">'+jQuery.i18n.prop("ssap.state.Tennessee")+'</option>'
		+'<option value="TX">'+jQuery.i18n.prop("ssap.state.Texas")+'</option>'
		+'<option value="UT">'+jQuery.i18n.prop("ssap.state.Utah")+'</option>'
		+'<option value="VT">'+jQuery.i18n.prop("ssap.state.Vermont")+'</option>'
		+'<option value="VA">'+jQuery.i18n.prop("ssap.state.Virginia")+'</option>'
		+'<option value="VI">'+jQuery.i18n.prop("ssap.state.VirginIslands")+'</option>'
		+'<option value="WA">'+jQuery.i18n.prop("ssap.state.Washington")+'</option>'
		+'<option value="WV">'+jQuery.i18n.prop("ssap.state.WestVirginia")+'</option>'
		+'<option value="WI">'+jQuery.i18n.prop("ssap.state.Wisconsin")+'</option>'
		+'<option value="WY">'+jQuery.i18n.prop("ssap.state.Wyoming")+'</option>'
		+'</select>'
		+'</div></div>'
		+'</div>';
		
	/*my code above*/

		


return data;
}


function checkUncheck(addressID) { 
	

	
	if ($('#householdContactAddress'+addressID).is(':checked')) {
	
    
            $("#other_Address_"+addressID).show();
            $('#HouseHoldotherAddressNoneOfTheAbove').attr("checked",false);
	}
     else {
   
            $("#other_Address_"+addressID).hide();
        }
    

}


function setAddressDetailsNonfinancial()
{	
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}
	var home_addressLine1=$('#home_addressLine1').val();
	var home_addressLine2=$('#home_addressLine2').val();
	var home_primary_city=$('#home_primary_city').val();
	var home_primary_zip=$('#home_primary_zip').val();
	var home_primary_state=$('#home_primary_state').val();
	var home_primary_county=$('#home_primary_county').val();
	var liveAtOtherAddressIndicator=false;
	var mailing_addressLine1=$('#mailing_addressLine1').val();
	var mailing_addressLine2=$('#mailing_addressLine2').val();
	var mailing_primary_city=$('#mailing_primary_city').val();
	var mailing_primary_zip=$('#mailing_primary_zip').val();
	var mailing_primary_state=$('#mailing_primary_state').val();
	var mailing_primary_county=$('#mailing_primary_county').val();
	
	var houseHoldPhone1=$('#first_phoneNo').val();
	var houseHoldPhoneExt1=$('#first_ext').val();
	var houseHoldPhoneType1=$('#first_phoneType').val();
	var houseHoldPhone2=$('#second_phoneNo').val();
	var houseHoldPhoneExt2=$('#second_ext').val();
	var houseHoldPhoneType2=$('#second_phoneType').val();
	var houseHoldSpokenLanguag=$('#preffegred_spoken_language').val();
	var houseHoldWrittenLanguag=$('#preffered_written_language').val();
	
	var isTemporarilyLivingOutsideIndicator=true;  
	 var otherAddressOfHouseHold="";

	
	 
	
		$('#appscr57HouseHoldPersonalInformation tr').each(function( i ) {
			
			if($('#appscr57HouseHoldhome_addressLine1TD'+(i+1)).html() != undefined)
			{
			UpdateAddressDetailsNonfinancial(i);
		
			return;
			}
			
			var appendRowData='';
		if(i==0)
			{
			
			appendRowData+="<td id='appscr57HouseHoldhome_addressLine1TD"+(i+1)+"' class='residensy_addressLine1'>"+home_addressLine1+"</td>";
			appendRowData+="<td id='appscr57HouseHoldhome_addressLine2TD"+(i+1)+"' class='residensy_addressLine2'>"+home_addressLine2+"</td>";
			appendRowData+="<td id='appscr57HouseHoldhome_primary_cityTD"+(i+1)+"' class='city'>"+home_primary_city+"</td>";
			appendRowData+="<td id='appscr57HouseHoldhome_primary_zipTD"+(i+1)+"' class='zip'>"+home_primary_zip+"</td>";
			appendRowData+="<td class='homeState'  id='appscr57HouseHoldhome_primary_stateTD"+(i+1)+"'>"+home_primary_state+"</td>";
			appendRowData+="<td class='homeCounty'  id='appscr57HouseHoldhome_primary_countyTD"+(i+1)+"'>"+home_primary_county+"</td>";
			
		
			appendRowData+="<td id='appscr57HouseHoldmailing_addressLine1TD"+(i+1)+"' class='mailing_addressLine1'>"+mailing_addressLine1+"</td>";
			appendRowData+="<td id='appscr57HouseHoldmailing_addressLine2TD"+(i+1)+"' class='mailing_addressLine2'>"+mailing_addressLine2+"</td>";
			appendRowData+="<td id='appscr57HouseHoldmailing_primary_cityTD"+(i+1)+"' class='mailingCity'>"+mailing_primary_city+"</td>";
			appendRowData+="<td id='appscr57HouseHoldmailing_primary_zipTD"+(i+1)+"' class='mailingZip'>"+mailing_primary_zip+"</td>";
			appendRowData+="<td class='mailingState' id='appscr57mailing_primary_stateTD"+(i+1)+"'>"+mailing_primary_state+"</td>";
			appendRowData+="<td class='mailingCounty'  id='appscr57HouseHoldmailing_primary_countyTD"+(i+1)+"'>"+mailing_primary_county+"</td>";			
					
			appendRowData+="<td class='houseHoldPhoneFirst'  id='appscr57houseHoldPhoneFirstTD"+(i+1)+"'>"+houseHoldPhone1+"</td>";
			appendRowData+="<td class='houseHoldPhoneExtFirst'  id='appscr57houseHoldPhoneExtFirstTD"+(i+1)+"'>"+houseHoldPhoneExt1+"</td>";
			appendRowData+="<td class='houseHoldPhoneTypeFirst'  id='appscr57houseHoldPhoneTypeFirstTD"+(i+1)+"'>"+houseHoldPhoneType1+"</td>";
			appendRowData+="<td class='houseHoldPhoneSecond'  id='appscr57houseHoldPhoneSecondTD"+(i+1)+"'>"+houseHoldPhone2+"</td>";
			appendRowData+="<td class='houseHoldPhoneExtSecond'  id='appscr57houseHoldPhoneExtSecondTD"+(i+1)+"'>"+houseHoldPhoneExt2+"</td>";
			appendRowData+="<td class='houseHoldPhoneTypeSecond'  id='appscr57houseHoldPhoneTypeSecondTD"+(i+1)+"'>"+houseHoldPhoneType2+"</td>";
			appendRowData+="<td class='houseHoldSpokenLanguage'  id='appscr57houseHoldSpokenLanguageTD"+(i+1)+"'>"+houseHoldSpokenLanguag+"</td>";
			appendRowData+="<td class='houseHoldWrittenLanguage'  id='appscr57houseHoldWrittenLanguageTD"+(i+1)+"'>"+houseHoldWrittenLanguag+"</td>";
			
			appendRowData+="<td class='liveAtOtherAddressIndicatorClass' id='appscr57liveAtOtherAddressIndicatorTD"+(i+1)+"'>"+liveAtOtherAddressIndicator+"</td>";
			appendRowData+="<td class='isTemporarilyLivingOutsideIndicatorClass' id='appscr57isTemporarilyLivingOutsideIndicatorTD"+(i+1)+"'>"+isTemporarilyLivingOutsideIndicator+"</td>";
			appendRowData+="<td id='appscr57otherAddressState"+(i+1)+"' class='otherAddressState'>"+home_primary_state+"</td>";
			
			
			}
		else
			{
		
			 var actualName=getNameByTRIndex(i);
	 
			  home_addressLine1=$('#applicant_or_non-applican_address_1'+actualName).val();
			  home_addressLine2=$('#applicant_or_non-applican_address_2'+actualName).val();
			  home_primary_city=$('#city'+actualName).val();
			  home_primary_zip=$('#zip'+actualName).val();
			  otherAddressOfHouseHold=$('#applicant_or_non-applican_state'+actualName).val();
			  
			  if ($('#householdContactAddress'+actualName).is(':checked')) {
				  liveAtOtherAddressIndicator=true;
				  home_addressLine1 = $('#applicant_or_non-applican_address_1'+actualName).val();
				  home_addressLine2 = $('#applicant_or_non-applican_address_2'+actualName).val();
				  home_primary_city = $('#city'+actualName).val();
				  home_primary_zip = $('#zip'+actualName).val();
				  home_primary_state= $('#applicant_or_non-applican_state'+actualName).val();
				  otherAddressOfHouseHold=home_primary_state=$('#applicant_or_non-applican_state'+actualName).val();
			  }
			  else
				  {
				  liveAtOtherAddressIndicator=false;
				  
				  home_addressLine1=$('#home_addressLine1').val();
				  home_addressLine2=$('#home_addressLine2').val();
				  home_primary_city=$('#home_primary_city').val();
				  home_primary_zip=$('#home_primary_zip').val();
				  home_primary_state=$('#home_primary_state').val();
				  otherAddressOfHouseHold=home_primary_state=$('#home_primary_state').val();
				  }
			
			  
			mailing_primary_state=home_primary_state;
			
			isTemporarilyLivingOutsideIndicator = $('input[name=isTemporarilyLivingOutsideIndicator'+actualName+']:radio:checked').val();
		
			appendRowData+="<td id='appscr57HouseHoldhome_addressLine1TD"+(i+1)+"' class='residensy_addressLine1'>"+home_addressLine1+"</td>";
			appendRowData+="<td id='appscr57HouseHoldhome_addressLine2TD"+(i+1)+"' class='residensy_addressLine2'>"+home_addressLine2+"</td>";
			appendRowData+="<td id='appscr57HouseHoldhome_primary_cityTD"+(i+1)+"' class='city'>"+home_primary_city+"</td>";
			appendRowData+="<td id='appscr57HouseHoldhome_primary_zipTD"+(i+1)+"' class='zip'>"+home_primary_zip+"</td>";
			appendRowData+="<td class='homeState' id='appscr57HouseHoldhome_primary_stateTD"+(i+1)+"'>"+home_primary_state+"</td>";			
			appendRowData+="<td class='homeCounty'  id='appscr57HouseHoldhome_primary_countyTD"+(i+1)+"'>"+mailing_primary_county+"</td>"; 
			
		
			appendRowData+="<td id='appscr57HouseHoldmailing_addressLine1TD"+(i+1)+"' class='mailing_addressLine1'>"+mailing_addressLine1+"</td>";
			appendRowData+="<td id='appscr57HouseHoldmailing_addressLine2TD"+(i+1)+"' class='mailing_addressLine2'>"+mailing_addressLine2+"</td>";
			appendRowData+="<td id='appscr57HouseHoldmailing_primary_cityTD"+(i+1)+"' class='mailingCity'>"+mailing_primary_city+"</td>";
			appendRowData+="<td id='appscr57HouseHoldmailing_primary_zipTD"+(i+1)+"' class='mailingZip'>"+mailing_primary_zip+"</td>";
			appendRowData+="<td class='mailingState' id='appscr57mailing_primary_stateTD"+(i+1)+"'>"+mailing_primary_state+"</td>";
			appendRowData+="<td class='mailingCounty'  id='appscr57HouseHoldmailing_primary_countyTD"+(i+1)+"'>"+mailing_primary_county+"</td>";			
					
			appendRowData+="<td class='houseHoldPhoneFirst'  id='appscr57houseHoldPhoneFirstTD"+(i+1)+"'>"+houseHoldPhone1+"</td>";
			appendRowData+="<td class='houseHoldPhoneExtFirst'  id='appscr57houseHoldPhoneExtFirstTD"+(i+1)+"'>"+houseHoldPhoneExt1+"</td>";
			appendRowData+="<td class='houseHoldPhoneTypeFirst'  id='appscr57houseHoldPhoneTypeFirstTD"+(i+1)+"'>"+houseHoldPhoneType1+"</td>";
			appendRowData+="<td class='houseHoldPhoneSecond'  id='appscr57houseHoldPhoneSecondTD"+(i+1)+"'>"+houseHoldPhone2+"</td>";
			appendRowData+="<td class='houseHoldPhoneExtSecond'  id='appscr57houseHoldPhoneExtSecondTD"+(i+1)+"'>"+houseHoldPhoneExt2+"</td>";
			appendRowData+="<td class='houseHoldPhoneTypeSecond'  id='appscr57houseHoldPhoneTypeSecondTD"+(i+1)+"'>"+houseHoldPhoneType2+"</td>";
			appendRowData+="<td class='houseHoldSpokenLanguage'  id='appscr57houseHoldSpokenLanguageTD"+(i+1)+"'>"+houseHoldSpokenLanguag+"</td>";
			appendRowData+="<td class='houseHoldWrittenLanguage'  id='appscr57houseHoldWrittenLanguageTD"+(i+1)+"'>"+houseHoldWrittenLanguag+"</td>";
			
			appendRowData+="<td class='liveAtOtherAddressIndicatorClass' id='appscr57liveAtOtherAddressIndicatorTD"+(i+1)+"'>"+liveAtOtherAddressIndicator+"</td>";
			appendRowData+="<td class='isTemporarilyLivingOutsideIndicatorClass' id='appscr57isTemporarilyLivingOutsideIndicatorTD"+(i+1)+"'>"+isTemporarilyLivingOutsideIndicator+"</td>";
			appendRowData+="<td id='appscr57otherAddressState"+(i+1)+"' class='otherAddressState'>"+otherAddressOfHouseHold+"</td>";
			}
			
		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+(i+1)).append(appendRowData);
			
		});
		addSpecialCircumstances();

		
}




function getNameByTRIndex(index)
{
	
	
	return getNameByPersonId(index);
	
	/*var houseHoldName='';
	
$('#appscr57HouseHoldPersonalInformation tr').each(function( i ) {
		
	var pureNameOfUser='';
		
		
	 if(i==index)
		 {
			 $("td", this).each(function( j ) {
				if(j<3)
					pureNameOfUser+= $(this).text();
				 if(j<2)
					 pureNameOfUser+='_';
				  });
		
		
			houseHoldName=pureNameOfUser;
		 }

		
		
	});
	
	return houseHoldName;*/
}

function getNameRemovedBySpace(userNameId)
{

	var removedScoreId = userNameId.replace(/_/g," ");
	
	return removedScoreId;
}

function setAddressDetailsToSpan()
{
	$('#houseHoldAddressDiv').html('');
	
	var home_addressLine1=$('#home_addressLine1').val();
	var home_addressLine2=$('#home_addressLine2').val();
	var home_primary_city=$('#home_primary_city').val();
	var home_primary_state=$('#home_primary_state').val();

	var address=home_addressLine1+" "+home_addressLine2+" "+home_primary_city+" "+home_primary_state;
	$('#houseHoldAddressDiv').append(address);
}


function setHouseHoldContact()
{
	
	$('#houseHold_Contact_span1').html('');
	$('#houseHold_Contact_span1').html('');
	$('#houseHold_Contact_span1').html('');
	
	
	if($('#middleName').val() == "Middle Name")
		$('#middleName').val('');
	
	if($('#lastName').val() == "Last Name")
		$('#lastName').val('');
	
	var name= $('#firstName').val() + " " + $('#middleName').val() + " " + $('#lastName').val();
	
	$('#houseHold_Contact_span1').html(name);
	$('#houseHold_Contact_span2').html(name);
	$('#houseHold_Contact_span3').html(name);
	
	/*validation of appscr54*/
	if(validation(currentPage)==false){
		return false;
	}
	
	next();
	
}


function apppscr60SetGenderInSSAP(){
	
	setHisOrHerAfterContinue();
}



function checkUSCitizenForNextPage()
{
	if($('input[name=UScitizen]:radio:checked').val()=='yes'){
		
		next();
	}
}

function cleanAppscr61() {	
	$('#UScitizenIndicatorYes').prop("checked", false);
	$('#UScitizenIndicatorNo').prop("checked", false);
	$("#naturalizedCitizenDiv").hide();
	$("#naturalizedCitizenDiv").hide();
	$("#documentTypeDiv").hide();
	$("#eligibleImmigrationStatusDiv").hide();
	//fillAppscr61(nonFinancialHouseHoldDone);
	
}

function  fillAppscr61(i) {
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

	var editHouseHoldgender 				= householdMemberObj.gender; //$('#appscrHouseHoldgender57TD'+i).text();
	var editHouseHoldHavingSSN  			= householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator; //$('#appscr57HouseHoldHavingSSNTD'+i).text();
	var editHouseHoldSSN 					= householdMemberObj.socialSecurityCard.socialSecurityNumber; //$('#appscr57HouseHoldSSNTD'+i).text();
	var editsameNameInSSNCardIndicator		= householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator; //$('#appscr57SameNameInSSNCardIndicatorTD'+i).text();
	var editfirstNameOnSSNCard				= householdMemberObj.socialSecurityCard.firstNameOnSSNCard; //$('#appscr57FirstNameOnSSNCardTD'+i).text();
	var editmiddleNameOnSSNCard				= householdMemberObj.socialSecurityCard.middleNameOnSSNCard; //$('#appscr57MiddleNameOnSSNCardTD'+i).text();
	var editlastNameOnSSNCard				= householdMemberObj.socialSecurityCard.lastNameOnSSNCard; //$('#appscr57LastNameOnSSNCardTD'+i).text();
	var editsuffixOnSSNCard					= householdMemberObj.socialSecurityCard.suffixOnSSNCard; //$('#appscr57SuffixOnSSNCardTD'+i).text(); //WE DON'T HAVE SUFFIX IN JSON
	var editnotAvailableSSNReason			= householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN; //$('#appscr57notAvailableSSNReasonTD'+i).text();

	
	/*var ssn1 = editHouseHoldSSN.split("-")[0];
	var ssn2 = editHouseHoldSSN.split("-")[1];
	var ssn3 = editHouseHoldSSN.split("-")[2];*/

	var houseHoldname = householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName; //$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
	
	/*This causes an infinity loop*/
	/*if(editHouseHoldSSN == "" || editHouseHoldSSN == null){
		resetAddNonFinancialDetails();
	}*/

	$('.nameOfHouseHold').text(houseHoldname);

	if(editHouseHoldgender != "" && editHouseHoldgender != undefined){
		if(editHouseHoldgender.toLowerCase() == 'male')
		{
			$('#appscr61GenderMaleID').prop('checked',true);
			apppscr60SetGenderInSSAP();

		}
		else if(editHouseHoldgender.toLowerCase() == 'female')
		{
			$('#appscr61GenderFemaleID').prop('checked',true);
			apppscr60SetGenderInSSAP();

		}
	}
	if(editHouseHoldHavingSSN == 'yes' || editHouseHoldHavingSSN == 'true' || editHouseHoldHavingSSN == true )
	{
		$('#socialSecurityCardHolderIndicatorYes').prop('checked',true);
		askForSSNNoPage61();
	}
	else if(editHouseHoldHavingSSN == 'no' || editHouseHoldHavingSSN == 'false' || editHouseHoldHavingSSN === false )
	{
		$('#socialSecurityCardHolderIndicatorNo').prop('checked',true);

		validation('appscr61');
		askForSSNNoPage61();
	}

	/*$('#ssn1').val(ssn1);
	$('#ssn2').val(ssn2);
	$('#ssn3').val(ssn3);*/
	
	if(editHouseHoldSSN){
		$("#ssn").attr('data-ssn',editHouseHoldSSN).val("***-**-" + editHouseHoldSSN.substring(5));
	}

	if(editsameNameInSSNCardIndicator == "yes" || editsameNameInSSNCardIndicator == 'true' || editsameNameInSSNCardIndicator == true )
		$('#fnlnsSameIndicatorYes').prop("checked", true);
	else if(editsameNameInSSNCardIndicator == "no" || editsameNameInSSNCardIndicator == 'false' || editsameNameInSSNCardIndicator === false )
	{

		$('#fnlnsSameIndicatorNo').prop("checked", true);
		$('#firstNameOnSSNCard').val(editfirstNameOnSSNCard);
		$('#middleNameOnSSNCard').val(editmiddleNameOnSSNCard);
		$('#lastNameOnSSNCard').val(editlastNameOnSSNCard);
		$('#suffixOnSSNCard').val(editsuffixOnSSNCard);
	}

	hasSameSSNNumberP61();
	if(editnotAvailableSSNReason == 0)
		$('#reasonableExplanationForNoSSN').val('');
	else
		$('#reasonableExplanationForNoSSN').val(editnotAvailableSSNReason);
}

function cleanAppscr63() {
	$('input').prop("checked", false);
	$(".parentCareTaker, .parentOrCaretakerBasicDetailsContainer").hide();
}

function cleanAppscr67() {
	$('#appscr61GenderMaleID').prop("checked", false);
	$('#appscr61GenderFemaleID').prop("checked", false);
	$('#socialSecurityCardHolderIndicatorYes').prop("checked", false);
	$('#socialSecurityCardHolderIndicatorNo').prop("checked", false);
	$("#DontaskAboutSSNOnPage61").hide();
	$("#askAboutSSNOnPage61").hide();
	/*$("#ssn1").val("");
	$("#ssn2").val("");
	$("#ssn3").val("");*/
	$("#ssn").val("");
	$('#fnlnsSameIndicatorYes').prop("checked", false);
	$('#fnlnsSameIndicatorNo').prop("checked", false);
	$("#sameSSNPage61Info").hide();
}

function  fillAppscr64(i) {
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];	
	var editethnicityIndicator =  householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator;
	var houseHoldname = getNameByPersonId(i);
	$('.nameOfHouseHold').html(""+houseHoldname);
	if(editethnicityIndicator == true){
		$('#checkPersonNameLanguageYes').prop("checked", true);
		
		for(var cnt=0; cnt<householdMemberObj.ethnicityAndRace.ethnicity.length; cnt++){
			$('input:checkbox[name="ethnicity"]').each(function(){
				if($(this).val() == householdMemberObj.ethnicityAndRace.ethnicity[cnt].code){
					$(this).prop('checked',true);
					
					//for other ethnicity
					if($(this).val() == '0000-0'){
					
						$('#otherEthnicity').val(householdMemberObj.ethnicityAndRace.ethnicity[cnt].label);
					}
				}
			});
		}
		
		
		
	}else if(editethnicityIndicator == false){
		$('#checkPersonNameLanguageNo').prop("checked", true);
	}
	
	displayOtherEthnicity();
	checkLanguage();
	
	
	for(var cnt=0; cnt<householdMemberObj.ethnicityAndRace.race.length; cnt++){
		$('input:checkbox[name="race"]').each(function(){
			if($(this).val() == householdMemberObj.ethnicityAndRace.race[cnt].code){
				
				$(this).prop('checked',true);
				//for other race
				if($(this).val() == '2131-1'){
					$('#otherRace').val(householdMemberObj.ethnicityAndRace.race[cnt].label);
				}
			}
		});
	}

	displayOtherRace();
	
}

function getCountyListByZipCode(applicantindex,countyCode) {
	var zipCode = $('#zip'+applicantindex).val();
	var stateCode = $('#applicant_or_non-applican_state'+ applicantindex +" option:selected").val();
	
	if(zipCode == "" || stateCode==""){
		$('#applicant_or_non-applican_county'+applicantindex).html('');
		return false;
	}
	$.ajax({
		type : "POST",
		url : theUrlForAddCountiesForStates,
		data : {
			zipCode : zipCode,
			stateCode: stateCode			
		},
		dataType : 'json',
		success : function(response) {

		populateOtherAddressCounties(response,applicantindex,countyCode); 
		},
		error : function(e) {
			alert("Failed to Populate County Details");
		}
	});
}

function populateOtherAddressCounties(response, applicantindex, countyCode) {
	// console.log(response, eIndex,county,"populate counties")
	$('#applicant_or_non-applican_county'+applicantindex).html('');
	countyCode = typeof countyCode !== 'undefined' ? countyCode : "";
	var optionsstring = '<option value="">County</option>';
	var i = 0;
	$.each(response, function(key, value) {
		var selected = (countyCode == value) ? 'selected' : '';
		var options = '<option value="' + value + '" '+selected+'>' + key + '</option>';
		optionsstring = optionsstring + options;
		i++;
	});

	$('#applicant_or_non-applican_county'+applicantindex).html(optionsstring);	
}

