//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.30 at 02:39:50 PM IST 
//


package com.getinsured.ahbx.broker.bookofbusiness;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnrollmentTypeValue.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EnrollmentTypeValue">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="I"/>
 *     &lt;enumeration value="A"/>
 *     &lt;enumeration value="S"/>
 *     &lt;enumeration value="N"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EnrollmentTypeValue", namespace = "http://ahbx.accenture.com/AHBXValidation")
@XmlEnum
public enum EnrollmentTypeValue {


    /**
     * It refers to Initial Enrollment
     * 					
     * 
     */
    I,

    /**
     * It refers to Renewal
     * 					
     * 
     */
    A,

    /**
     * It refers to Special Enrollment
     * 					
     * 
     */
    S,

    /**
     * It refers to Shop Enrollment
     * 					
     * 
     */
    N;

    public String value() {
        return name();
    }

    public static EnrollmentTypeValue fromValue(String v) {
        return valueOf(v);
    }

}
