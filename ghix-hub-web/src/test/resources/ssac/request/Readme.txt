Request JSON Strings for SSAC requests

1. SSNVerified - Request to verify SSN
2. CitizenshipVerified - Request to verify Citizenship
3. DeathVerification - Request to verify Death Confirmation code for a citizen
4. CitizenshipUnverfied - Request that returns false for citizenship verification
5. IncarcerationVerified - Request to verify Incarceration details
6. IncarcerationUnverified - Request that returns false for Incarceration verification