<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />

<c:if test="${CA_STATE_CODE}">
	<link href="<c:url value="/resources/css/broker-search.css" />" media="screen" rel="stylesheet" type="text/css" />
</c:if>
<c:if test="${!CA_STATE_CODE}">
	<link href="<c:url value="/resources/css/broker-search-id.css" />" media="screen" rel="stylesheet" type="text/css" />
</c:if>
<style>
	body {
	  background-image: none;
	  background: #fff;
	  padding-top: 0;
	}
</style>
<c:choose>
	<c:when test="${CA_STATE_CODE}">
		<c:set value="col-xs-12 col-sm-12 col-md-4 col-lg-4" var="cssClass"></c:set>
	</c:when>
	<c:otherwise>
		<c:set value="col-xs-12 col-sm-6 col-md-6 col-lg-6" var="cssClass"></c:set>
	</c:otherwise>
</c:choose>


<div class="container-fluid" id="locate_assistance">
	<div class="row-fluid">
		<div class="titlebar">
			<c:if test="${stateCode == 'NV'}">
				<a href="https://www.nevadahealthlink.com/"  target="_blank" class="masthead_logo" title="Link open in new window or tab"><img width="150" class="brand" src="<gi:cdnurl value="/resources/img/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)%>"  alt="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>"/></a>
			</c:if>
			<h1><a name="skip"></a><spring:message code="label.locate.assistance.modal.1"/></h1>
			<c:if test="${CA_STATE_CODE}">
				<h3><a name="skip"></a><spring:message code="label.locate.assistance.modal.13"/></h3>
			</c:if>
		</div>

		<div class="row-fluid" style="margin-top:4px;">
			<ul class="thumbnails">
				<li class="${cssClass}">
				 	<div class="thumbnail txt-center">
				   		<img src="<gi:cdnurl value="/resources/images/Eli-brokers-L.jpg" />" alt="" />
				   		<c:if test="${CA_STATE_CODE}">
				   			<h3 class="center"><spring:message code="label.locate.assistance.modal.6" htmlEscape="false"/></h3>
				   		</c:if>
				     	<p class="txt-left gutter10-rbl thumbnail-para"><spring:message code="label.locate.assistance.modal.7"/></p>
				     	<a class="btn btn-primary margin10-b modal-button" id="aid-findAgent" href="/hix/broker/search/individual" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Get Help Events', 'Click', 'Find an Agent', 1]);" title="<spring:message code="label.locate.assistance.modal.6" htmlEscape="false"/> " aria-role="button"><spring:message code="label.locate.assistance.modal.8"/></a>
				   </div>
				</li>

				<li class="${cssClass}">
				 	<div class="thumbnail txt-center middle">
				   		<img src="<gi:cdnurl value="/resources/images/Eli-employer-L.jpg" />" alt="" />
				   		<c:if test="${CA_STATE_CODE}">
				   			<h3 class="center"><spring:message code="label.locate.assistance.modal.3" htmlEscape="false"/></h3>
				   		</c:if>
				     	<p class="txt-left gutter10-rbl thumbnail-para"><spring:message code="label.locate.assistance.modal.4"/> </p>
				     	<a class="btn btn-primary margin10-b modal-button" id="aid-findCertifiedCounselor" href="/hix/entity/locateassister/searchentities" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Get Help Events', 'Click', 'Find Certified Enrollment Counselor', 1]);" title="<spring:message code="label.locate.assistance.modal.3" htmlEscape="false"/>" aria-role="button"><spring:message code="label.locate.assistance.modal.5"/></a>
				    </div>
				</li>
				<c:if test="${CA_STATE_CODE}">
				 	<li class="${cssClass}">
					  	<div class="thumbnail txt-center">
					   	 	<img class="custom-img-sz" src="/hix/resources/images/Eli-county-L.jpg" alt="" />
					      	<h3 class="center"><spring:message code="label.locate.assistance.modal.9"/></h3>
					      	<p class="txt-left gutter10-rbl thumbnail-para"><spring:message code="label.locate.assistance.modal.10"/></p>
					      	<a class="btn btn-primary margin10-b modal-button" id="aid-findCountyOffice" href="http://www.dhcs.ca.gov/services/medi-cal/Pages/CountyOffices.aspx" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Get Help Events', 'Click', 'Find County Office', 1]);" title="<spring:message code="label.locate.assistance.modal.11" htmlEscape="false"/>" aria-role="button"><spring:message code="label.locate.assistance.modal.12"/></a>
					  	</div>
			 		</li>
				</c:if>
			</ul>
		</div> <!-- /.row-fluid -->
	</div> <!-- /.row -->
</div> <!-- /.container-fluid -->

<script type="text/javascript">
	window.addEventListener('load', function(){
		$('.modal-button').bind('touchstart click', function(e) {
	    	this.click();
		});
	});
</script>
<script type="text/javascript">
var find_help = new Array();
	find_help['pd.label.commontext.externalWarnBox.leavingAlert1'] = "<spring:message code='pd.label.commontext.externalWarnBox.leavingAlert1' javaScriptEscape='true'/>";
	find_help['pd.label.commontext.externalWarnBox.leavingAlert2'] = "<spring:message code='pd.label.commontext.externalWarnBox.leavingAlert2' javaScriptEscape='true'/>";
	find_help['pd.label.commontext.externalWarnBox.access'] = "<spring:message code='pd.label.commontext.externalWarnBox.access' javaScriptEscape='true'/>";
	find_help['pd.label.commontext.externalWarnBox.no'] = "<spring:message code='pd.label.commontext.externalWarnBox.no' javaScriptEscape='true'/>";
	find_help['pd.label.commontext.externalWarnBox1'] = "<spring:message code='pd.label.commontext.externalWarnBox1' javaScriptEscape='true'/>";
	find_help['pd.label.commontext.externalWarnBox.closeModal'] = "<spring:message code='pd.label.commontext.externalWarnBox.closeModal' javaScriptEscape='true'/>";
	find_help['pd.label.commontext.externalWarnBox.yes'] = "<spring:message code='pd.label.commontext.externalWarnBox.yes'/>";
	find_help['pd.label.commontext.externalWarnBox.thankYou'] = "<spring:message code='pd.label.commontext.externalWarnBox.thankYou'/>";
</script>

<!-- <script type="text/javascript">
if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,194,new Date().getTime(),"","","","",""]);
</script> -->
