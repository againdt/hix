package com.getinsured.iex.hub.qac.prefixmappers;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class QACNamespaceMapper extends NamespacePrefixMapper {

private static final Map<String, String> PREFIXES = new HashMap<String, String>();
	
	static{
		PREFIXES.put("http://vqac.ee.sim.dsh.cms.hhs.gov/extension/1.0","ext");
		PREFIXES.put("http://hix.cms.gov/0.1/hix-core","hix-core");
		PREFIXES.put("http://niem.gov/niem/appinfo/2.0","i");
		PREFIXES.put("http://niem.gov/niem/appinfo/2.1","i2");
		PREFIXES.put("http://niem.gov/niem/niem-core/2.0","nc");
		PREFIXES.put("http://niem.gov/niem/proxy/xsd/2.0","neim-xsd");
		PREFIXES.put("http://niem.gov/niem/structures/2.0","s");
		PREFIXES.put("http://hix.cms.gov/0.1/hix-ee","hix-ee");
		PREFIXES.put("http://vqac.ee.sim.dsh.cms.hhs.gov/exchange/1.0","exch");
		PREFIXES.put("http://www.w3.org/2001/XMLSchema-instance","xsi");
		PREFIXES.put("http://hix.cms.gov/0.1/hix-types","hix-types");
		PREFIXES.put("http://hix.cms.gov/0.1/hix-pm","hix-pm");
		
	}
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		
		String prefix = PREFIXES.get(namespaceUri);
		
		if(prefix == null){
			prefix = suggestion;
		}
		return prefix;
	}
}


		

