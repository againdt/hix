import { createStore, applyMiddleware, compose } from 'redux';
import appReducer from './reducers/appReducer';
import thunk from 'redux-thunk';
import _ from 'lodash';

// Using stateSanitizer() because of issue with serialization of huge "contexts" property value

const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        stateSanitizer: state => state.contexts ?
            {
                ...state,
                contexts: _.mapValues(state.contexts,
                    component =>
                        `<<"This" points on: ${component.props ? component.props.name : component.defaultProps.name}>>`)
            }
            : state
    }) : compose;

const enhancer = composeEnhancers(applyMiddleware(thunk));

const appStore = createStore(appReducer, enhancer);

export default appStore;