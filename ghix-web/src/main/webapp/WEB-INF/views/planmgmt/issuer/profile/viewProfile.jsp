<!-- 
 @author raja
 newIssuerLanding.jsp is the requirement of the new Issuer Portal
 @UserStory : HIX-1810
 @Jira-id : HIX-2450
 -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>


<div class="gutter10">
	<div class="row-fluid">
        	<ul class="page-breadcrumb">
                <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
                <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
                <li><spring:message  code="pgheader.issuerProfile"/></li> 
            </ul><!--page-breadcrumb ends-->
            
           <%--  <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
           <h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
		</div>
	
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.welcome"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 		<jsp:param name="pageName" value="issuerDetails"/>
 	 	 	 	</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4><spring:message  code="pgheader.issuerProfile"/></h4>
                </div>
				<div class="gutter10">
					<form class="form-horizontal" id="frmIssuerLandingInfo" name="frmIssuerLandingInfo" action="" method="POST">
					<div class="profile">
						<table class="table table-border-none">

							<tbody>
								<!-- <tr>
									<td class="txt-right"><spring:message  code="label.licenseNumber"/></td>
									<td><strong>${issuerObj.licenseNumber}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.licenseStatus"/></td>
									<td><strong>${issuerObj.licenseStatus}</strong></td>
								</tr> -->
								<tr>
									<td class="txt-right span4"><spring:message  code="label.name"/></td>
									<td><strong>${issuerObj.name}</strong></td>
								</tr>
								<tr>
	                                <td class="txt-right"><spring:message  code="label.NAICCompanyNumber"/>
                                    	<a class="ttclass" rel="tooltip" href="#" data-original-title="The code assigned by the NAIC to all U.S. domiciled companies which file a Financial Annual Statement with the NAIC. "><i class="icon-question-sign"></i></a>
                                    </td>
	                                <td><strong>${issuerObj.naicCompanyCode}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.NAICGroupCode"/>
                                    	<a class="ttclass" rel="tooltip" href="#" data-original-title="The code assigned by the NAIC to identify those companies that are a part of a given holding company structure. A zero indicates that the company is not part of an insurance holding company. "><i class="icon-question-sign"></i></a>
                                    </td>
	                                <td><strong>${issuerObj.naicGroupCode}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.FederalEmployerId"/>
                                    	<a class="ttclass" rel="tooltip" href="#" data-original-title="The identification code assigned to you by the US federal government."><i class="icon-question-sign"></i></a>
                                    </td>
	                                <td><strong>${issuerObj.federalEin}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.HIOSIssuerId"/>
                                    	<a class="ttclass" rel="tooltip" href="#" data-original-title="The identification number issued to you by the Health Insurance Oversight System (HIOS)."><i class="icon-question-sign"></i></a>
                                    </td>
	                                <td><strong>${issuerObj.hiosIssuerId}</strong></td>
	                            </tr>
								
							</tbody>
						</table><br />
                        
                        <div class="graydrkaction margin0">
                            <h4><spring:message  code="pgheader.issuerAddress"/></h4>
                        </div>
                        
                        
                        <table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.addressLine1"/></td>
                                <td><strong>${issuerObj.addressLine1}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.addressLine2"/></td>
                                <td><strong>${issuerObj.addressLine2}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.city"/></td>
                                <td><strong>${issuerObj.city}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.state"/></td>
                                <td><strong>${issuerObj.state}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.zipCode"/></td>
                                <td><strong>${issuerObj.zip}</strong></td>
                            </tr>
                            <!--<tr>
                                <td class="txt-right span4">&nbsp;</td>
                                <td><a href='<c:url value="/planmgmt/issuer/account/profile/financial/info/list" />' class="btn btn-primary"><spring:message  code="label.next"/></a></td>
                            </tr>-->
						</table>
					</div>			
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
	</div>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<script type="text/javascript">
	$('.ttclass').tooltip();  
</script>
