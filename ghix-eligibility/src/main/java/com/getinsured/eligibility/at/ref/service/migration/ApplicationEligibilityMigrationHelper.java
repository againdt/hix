package com.getinsured.eligibility.at.ref.service.migration;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.handler.helper.ApplicationExchangeEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.EligibilityStatusHelper;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component
public class ApplicationEligibilityMigrationHelper {

	private static final Logger LOGGER = Logger.getLogger(ApplicationEligibilityMigrationHelper.class);

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private ApplicationExchangeEligibilityHelper applicationExchangeEligibilityHelper;
	
	@Autowired private AptcHistoryRepository aptcHistoryRepository;
	@Autowired
	@Qualifier("enrollmentUpdateAdapter")
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;


	public void processEligibility(	Map<Long, List<EligibilityProgram>> eligibilityProgramMap,
			boolean fullAssessment, String applicationID, Map<String, Object> hhLevelEliDetails) {

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(applicationID);
		if (!ssapApplicationList.isEmpty()){
			SsapApplication ssapApplication = ssapApplicationList.get(0);

			setApplicationEligibilityStatus(eligibilityProgramMap, ssapApplication, fullAssessment);
			setApplicationExchangeEligibilityStatus(eligibilityProgramMap, ssapApplication);
			setHHLevelEliDetails(ssapApplication, hhLevelEliDetails);
			ssapApplicationRepository.save(ssapApplication);
		} else {
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + applicationID);
		}

	}

	private void setApplicationExchangeEligibilityStatus(Map<Long, List<EligibilityProgram>> eligibilityProgramMap, SsapApplication ssapApplication) {
		ExchangeEligibilityStatus exchangeEligibilityStatus = applicationExchangeEligibilityHelper.processEligibility(eligibilityProgramMap);
		ssapApplication.setExchangeEligibilityStatus(exchangeEligibilityStatus);
	}

	private void setApplicationEligibilityStatus(Map<Long, List<com.getinsured.eligibility.model.EligibilityProgram>> eligibilityProgramMap, SsapApplication ssapApplication, boolean isFullAssessment) {

		// Populate applicationEligibility Set for all applicants..
		Set<String> applicationEligibility = EligibilityStatusHelper.prepareApplicationEligibilitySet(eligibilityProgramMap);

		// Determine Application Eligibility Status & Exchange Eligibility Status
		boolean isAssessment = isFullAssessment;

		if (isAssessment){
			EligibilityStatusHelper.assessmentStatus(applicationEligibility, ssapApplication);
		} else {
			EligibilityStatusHelper.fullDeterminationStatus(applicationEligibility, ssapApplication);
		}

	}

	private void setHHLevelEliDetails(SsapApplication ssapApplication, Map<String, Object> hhLevelEliDetails) {

		BigDecimal maxAPTCAmount = null;
		String csrLevel = null;

		if (!hhLevelEliDetails.isEmpty()){
			Boolean isApplicantionAPTCElig = (Boolean) hhLevelEliDetails.get("APTCElig");
			if(isApplicantionAPTCElig!=null && isApplicantionAPTCElig.equals(Boolean.TRUE) && hhLevelEliDetails.containsKey(EligibilityConstants.MAX_APTC_AMT)) {
				maxAPTCAmount = (BigDecimal) hhLevelEliDetails.get(EligibilityConstants.MAX_APTC_AMT);
			}
			
			if (hhLevelEliDetails.containsKey(EligibilityConstants.CSR_LEVEL)){
				csrLevel = (String) hhLevelEliDetails.get(EligibilityConstants.CSR_LEVEL);
			}
		}

		ssapApplication.setMaximumAPTC(maxAPTCAmount);
		ssapApplication.setCsrLevel(csrLevel);
		
		/**
		 * HIX-49705 - Multiple CSR level. If case comes out to be APTC with some CSR Level, reset the CSR Value.
		 */
		if (ExchangeEligibilityStatus.APTC.equals(ssapApplication.getExchangeEligibilityStatus()) 
				|| ExchangeEligibilityStatus.QHP.equals(ssapApplication.getExchangeEligibilityStatus())){
			ssapApplication.setCsrLevel(null);
		}

		compareAndModifyElectedAPTCAmount(ssapApplication, maxAPTCAmount);

	}

	private void compareAndModifyElectedAPTCAmount(SsapApplication ssapApplication, BigDecimal maxAPTCAmount) {

		BigDecimal electedAPTCAmount = ssapApplication.getElectedAPTC();

		if (maxAPTCAmount != null && electedAPTCAmount != null && electedAPTCAmount.compareTo(maxAPTCAmount) > 0) {
			LOGGER.info("Elected APTC Amount is greater than Maximum APTC Amount. Set Elected APTC Amount to Maximum APTC Amount.");
			ssapApplication.setElectedAPTC(maxAPTCAmount);
		}
	}
	
	public void insertHHAptc(com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram eligibilityProgram,String applicationID) {

		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(applicationID);
		BigDecimal cmrHouseholdId = null; 
		SsapApplication ssapApplication = null;
		if (!ssapApplicationList.isEmpty()){
			ssapApplication = ssapApplicationList.get(0);
			cmrHouseholdId = ssapApplication.getCmrHouseoldId();
		} else {
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES + applicationID);
		}
		AptcHistory aptcHistory = new AptcHistory();
		aptcHistory.setAptcAmount(eligibilityProgram.getMaxAPTCAmount());
		aptcHistory.setSsapApplicationId(ssapApplication.getId());
		aptcHistory.setCmrHouseholdId(cmrHouseholdId);
		aptcHistory.setCoverageYear(ssapApplication.getCoverageYear());
		aptcHistory.setEligStartDate(eligibilityProgram.getEligibilityStartDate());
		aptcHistory.setEligEndDate(eligibilityProgram.getEligibilityEndDate());
		aptcHistoryRepository.save(aptcHistory);
	}

}
