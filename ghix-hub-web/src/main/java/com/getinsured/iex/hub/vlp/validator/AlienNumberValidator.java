package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for alien number
 * 
 * @author Nikhil Talreja
 * @since 31-Jan-2014
 *
 */
public class AlienNumberValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AlienNumberValidator.class);
	
	/** 
	 * Validations for alien number
	 * 1. Should be a number
	 * 2. Should be exactly 9 digits
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String alienNumber = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			alienNumber = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(alienNumber.length() < VLPServiceConstants.ALIEN_NUMBER_MIN_LEN
				|| alienNumber.length() > VLPServiceConstants.ALIEN_NUMBER_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.ALIEN_NUMBER_LEN_ERROR_MESSAGE);
		}
		
		try{
			Long.parseLong(alienNumber);
		}
		catch(NumberFormatException e){
			throw new InputDataValidationException(VLPServiceConstants.NON_NUMERIC_ERROR_MESSAGE, e);
		}
	
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
