<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<div class="gutter10">
  <div class="row-fluid">
      <div class="row-fluid">
          <h1>Waive Employer Coverage</h1>
      </div>
      <div class="row-fluid">
        <div class="span3" id="sidebar">
          <h4 class="graydrkbg">You Should Know</h4>
          <div class="gutter10">
            <p>By waiving employer coverage you are forfeiting any employer premium contrinutions. 
              Waiving your employer provided coverage means you will be unable to enroll in employer 
              coverage until next open enrollment unless you experience a qualified event. You should 
              only waive coverage if you have coverage elsewhere. </p>
          </div>
        </div>
        <!-- /.sidebar-->
        <div class="span9" id="rightpanel">
        <fmt:formatDate pattern="MM/dd/yyyy" value="${enrollmentEndDt}" var="enrollEndDate" />
        <fmt:formatDate pattern="MM/dd/yyyy" value="${coverageDate}" var="coverageEffectiveDate" />
          <div class="row-fluid">
            <h4 class="graydrkbg gutter10">Employer Coverage Waive Confirmation</h4>
            <div class="gutter10">
              <p>You have waived your employer provided coverage, you may still enroll in coverage by the end of open enrollment on ${enrollEndDate}.
                After open enrollment is completed you will be unable to enroll in coverage unless you experience a qualified levent.</p>
              <h4>Employer Sponsored Coverage Waived Effective ${coverageEffectiveDate}</h4>
              <form class="form-horizontal gutter10">
              <df:csrfToken/>
                <div class="form-actions margin-top50">
                <div class="controls"><a href="<c:url value="/shop/employee/planselection/verifyemployee" />" class="btn"> Enroll </a> <a href="<c:url value="/account/user/logout" />" class="btn btn-primary offset1"> Exit </a>
                </div>
                </div>
              </form>
            </div>
            <!--gutter10--> 
          </div>
          <!--row-fluid--> 
        </div>
        <!--rightpanel--> 
      </div>
      <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<!--gutter10-->
<script type="text/javascript">
$(document).ready(function(){
$('.info').tooltip();
});

</script>
