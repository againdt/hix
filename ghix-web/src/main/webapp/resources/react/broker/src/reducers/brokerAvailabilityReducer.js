import { brokerAvailabilityActionTypes } from '../constants/actionTypes';

const initialState = {
  available: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case brokerAvailabilityActionTypes.SET_BROKER_AVAILABILITY:
      return { ...state, ...action.status };
    default:
      return state;
  }
};
