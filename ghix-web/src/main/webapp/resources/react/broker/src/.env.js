const PRODUCTION = 'production';
const DEVELOPMENT = 'development';

export default {
    ENV: {
        PRODUCTION: process.env.NODE_ENV === PRODUCTION,
        DEVELOPMENT: process.env.NODE_ENV === DEVELOPMENT
    }
};