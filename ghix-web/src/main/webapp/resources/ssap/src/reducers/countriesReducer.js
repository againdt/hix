import {countriesActionTypes} from '../constants/actionTypes';

export const initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
    case countriesActionTypes.SET_COUNTRIES:
        return action.countries;
    case countriesActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};