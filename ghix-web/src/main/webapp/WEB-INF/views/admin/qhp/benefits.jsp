<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">  		
<div class="row-fluid">
       <div class="span3" id="sidebar">
       	<div class="header">
        	<h4 class="margin0"><a name="skip"></a><spring:message  code='label.addnewQHPapplication'/></h4>
        </div>
        	<!--  beginning of side bar -->
            <ol class="nav nav-list">
                    <li class="done"><a href="#"><spring:message  code='label.provideplandetails'/></a></li>
                    <li class="active"><a href="#"><spring:message  code='label.uploadplanbenefits'/></a></li>
                    <li class="link"><a href="#"><spring:message  code='label.uploadplanrates'/></a></li>
                    <li class="link"><a href="#"><spring:message  code='label.selectoraddprovidernetworks'/></a></li>
                    <!--<li><a href="#"><spring:message  code='label.addcostsharingvariations'/></a></li>-->
                    <li class="link"><a href="#"><spring:message  code='label.reviewandconfirm'/></a></li>
                </ol>
        	<!-- end of side bar -->
       	
       </div><!-- end of span3 -->
       <div class="span9" id="rightpanel">
       	<div class="header">
	        <h4><spring:message  code='label.step2'/> <spring:message code='label.uploadplanbenefits'/></h4>
	    </div>
	        
       		<form class="form-horizontal" id="frmUploadBenefits" name="frmUploadBenefits" action="<c:url value="/admin/planmgmt/submitqhpbenefits" />" method="POST">
                       <df:csrfToken/>
                       <div class="control-group margin0">
                            <label for="planName" class="required control-label"><spring:message  code='label.issuerName'/></label>
                            <div class="controls paddingT5">
                                <strong>${issuerName}</strong>
                            </div> <!-- end of controls-->
                        </div>
                        
                        <div class="control-group margin0">
                            <label for="planName" class="required control-label"><spring:message  code='label.planName'/></label>
                            <div class="controls paddingT5">
                                <strong>${planName}</strong>
                            </div> <!-- end of controls-->
                        </div>
                        
                        <div class="control-group margin0">
                            <label for="planName" class="required control-label"><spring:message  code='label.planNumber'/></label>
                            <div class="controls paddingT5">
                                <strong>${planNumber}</strong>
                            </div> <!-- end of controls-->
                        </div>
					<input type="hidden" id="benefitFile" name="benefitFile"/>
					<input type="hidden" id="id" name="id" value="${planId}"/>
					<input type="hidden" id="isEdit" name="isEdit" value="${isEdit}"/>
			</form>  
                    
               <form class="form-horizontal form-bottom" id="frmBenefits" name="frmBenefits" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
                 	<df:csrfToken/>
                 	<div class="box-gutter10"> 
                    <div class="control-group">
                       <label for="planBenefits" class="control-label"><spring:message  code='label.titlePlanBenefits'/></label>
                       <div class="controls">
								<span><input type="file" id="benefits" name="benefits"  class="input-file">
								&nbsp; <input type="button" value="Upload" title="Upload" class="btn" onclick="validCsv('#benefits','#frmBenefits','#benefitFile_error')"/>&nbsp; <a href=<c:url value="/resources/sampleTemplates/benefits.csv" />><spring:message code='label.downloadTemplate'/></a>
								<input type="hidden" name="fileType" value="benefits"/>
                         </span>
								<div id="benefitFile_error" class="">${fileError}</div>
					<div id="benefitsFileName" class="help">${benefitsFileName}</div>
							
                       </div>  <!-- end of controls-->
                    </div>  <!-- end of control-group -->
   
                   
                       <div class="form-actions">
                           <button type="button" name="submitBtn" id="submitBtn" class="btn btn-primary" title="<spring:message code='label.saveAndContinue'/>" onclick="formSubmit()" ><spring:message code='label.saveAndContinue'/></button> 
                       </div><!-- end of form-actions -->
              		</div>      
               </form>
                                 
       </div><!-- end of span9 -->

		</div>
    </div>
<script type="text/javascript">
function validCsv(fileElementId, formElementId, errorElementId)
{
	var ext = $(fileElementId).val().split('.').pop().toLowerCase();
	var allow = new Array('csv');
	if(jQuery.inArray(ext, allow) == -1) {
	    $(errorElementId).html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileTypeError'/></span></label>");
	   return false;
	}else{
		$(formElementId).submit();
	  } 
}

function  formSubmit()
{
	$("#benefitFile_error").html("");	
	$("#frmUploadBenefits").submit();
}

$(document).ready(function(){
	//getIssuerNames();
	var accreditationUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitAccreditationRequest,  // pre-submit callback 
	        success:       submitAccreditationResponse  // post-submit callback 
	    };
    $('#frmBenefits').ajaxForm(accreditationUpload);
    isFileReq();
});

function isFileReq()
{
	if("false" == '${isEdit}')
	{
	$("#benefitFile").rules("add", "required");
	}	
}
function submitAccreditationResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{	
		$("#benefitsFileName").text(val[1]);
		$("#benefitFile").val(val[2]);
	}
	else
	{
		$("#benefitFile_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message code='label.fileSizeError'/></span></label>");
	}
		
}

function submitAccreditationRequest(formData, jqForm, options) {
	var fileName = $('#benefits').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}


var validator = $("#frmUploadBenefits").validate({ 
	 ignore: "",
	rules : {benefitFile : false
	}, 
	messages : {
		benefitFile : { required : "<span><em class='excl'>!</em><spring:message code='error.benefitFileRequired'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});
jQuery("#frmBenefits").validate({});
</script>
