package com.getinsured.hix.batch.notification.ssap;

import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class AgedOutDisenrollmentNoticeTask extends StepExecutionListenerSupport implements Tasklet {

	private static final Logger lOGGER = Logger.getLogger(AgedOutDisenrollmentNoticeTask.class);
	private AgedOutDisenrollmentNoticeService agedOutDisenrollmentNoticeService;
	private String caseNumbers;  
	@Override
	public void beforeStep(StepExecution stepExecution) {
		  JobParameters jobParameters = stepExecution.getJobParameters();
		  caseNumbers = jobParameters.getString("case_numbers");
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
	
		lOGGER.debug(this.getClass().getName()+" started at " + new Timestamp(System.currentTimeMillis()));
		try {
			List<String> dataList =  agedOutDisenrollmentNoticeService.processAgeOutDependents(caseNumbers);
			lOGGER.debug(dataList);
		} catch (Exception e) {
			lOGGER.error(e);
		}
	
		lOGGER.debug(this.getClass().getName()+" finishing at " + new Timestamp(System.currentTimeMillis()));
		return RepeatStatus.FINISHED;
	}

	public AgedOutDisenrollmentNoticeService getAgedOutDisenrollmentNoticeService() {
		return agedOutDisenrollmentNoticeService;
	}

	public void setAgedOutDisenrollmentNoticeService(
			AgedOutDisenrollmentNoticeService agedOutDisenrollmentNoticeService) {
		this.agedOutDisenrollmentNoticeService = agedOutDisenrollmentNoticeService;
	}	
	
}
