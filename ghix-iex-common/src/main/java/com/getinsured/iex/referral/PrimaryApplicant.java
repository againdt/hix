package com.getinsured.iex.referral;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class PrimaryApplicant {
	
	private static final String DATE_FORMAT = "MM/dd/yyyy";

	private String firstName;
	private String lastName;
	private Date dateOfBirth;
	private String zipCode;
	private String formattedDateofBirth;
	private String ssn;
	
	public PrimaryApplicant() {
		   
	 }
	
	public PrimaryApplicant(String firstName, String lastName, 
			Date dateOfBirth, String ssn, String zipCode) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.zipCode = zipCode;
		this.formattedDateofBirth = formatDateofBirth(dateOfBirth);
		this.ssn = ssn;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	
    public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public String getFormattedDateofBirth() {
		return formattedDateofBirth;
	}

	public void setFormattedDateofBirth(String formattedDateofBirth) {
		this.formattedDateofBirth = formattedDateofBirth;
	}
	
	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String formatDateofBirth(Date birthDate) {
		if(birthDate!=null){
		return new SimpleDateFormat(DATE_FORMAT).format(birthDate);
		}else{
			return null;
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(StringUtils.upperCase(firstName))
				.append(StringUtils.upperCase(lastName))
				.append(zipCode)
				.append(ssn)
				.append(formattedDateofBirth)
				.toHashCode();
	}

	@Override
	public boolean equals(final Object obj) {
		if(obj instanceof PrimaryApplicant){
			final PrimaryApplicant other = (PrimaryApplicant) obj;
			if((StringUtils.isNotBlank(ssn) && StringUtils.isNotBlank(other.ssn)) && 
					(StringUtils.isNotBlank(formattedDateofBirth) && StringUtils.isNotBlank(other.formattedDateofBirth))){
				return new EqualsBuilder()
						.append(StringUtils.upperCase(firstName), StringUtils.upperCase(other.firstName))
						.append(StringUtils.upperCase(lastName), StringUtils.upperCase(other.lastName))
						.append(ssn, other.ssn)
						.append(formattedDateofBirth, other.formattedDateofBirth)
						.isEquals();
			}
			else{
				return new EqualsBuilder()
						.append(StringUtils.upperCase(firstName), StringUtils.upperCase(other.firstName))
						.append(StringUtils.upperCase(lastName), StringUtils.upperCase(other.lastName))
						.append(zipCode, other.zipCode)
						.append(formattedDateofBirth, other.formattedDateofBirth)
						.isEquals();
			}
		} else{
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PrimaryApplicant [firstName=").append(firstName).append(", lastName=").append(lastName)
				.append(", dateOfBirth=").append(formattedDateofBirth).append(", zip=").append(zipCode).append(", ssn=").append(ssn).append("]");
		return builder.toString();
	}

	


}
