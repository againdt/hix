package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for card/receipt number
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class CardNumberValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CardNumberValidator.class);
	
	/** 
	 * Validations for card/receipt number
	 * Alphanumeric - first 3 characters alpha and the remaining 10 numeric characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String cardNumber = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			cardNumber = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(cardNumber.length() < VLPServiceConstants.CARD_NUMBER_MIN_LEN
				|| cardNumber.length() > VLPServiceConstants.CARD_NUMBER_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.CARD_NUMBER_ERROR_MESSAGE);
		}

		if (!cardNumber.matches(VLPServiceConstants.CARD_NUM_REGEX)){
			throw new InputDataValidationException(VLPServiceConstants.CARD_NUMBER_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
