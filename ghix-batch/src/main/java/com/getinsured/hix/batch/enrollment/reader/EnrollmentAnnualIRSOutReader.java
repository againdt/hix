package com.getinsured.hix.batch.enrollment.reader;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.skip.Enrollment1095StagingParams;


public class EnrollmentAnnualIRSOutReader implements ItemReader<Integer>{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnualIRSOutReader.class);
	List<Integer> enrollmentIds;		
	int partition;
	int loopCount;
	int startIndex;
	int endIndex;
	
//	private EnrollmentIds enrollmentIdList;
	private Enrollment1095StagingParams enrollment1095StagingParams;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			partition =ec.getInt("partition");
			startIndex=ec.getInt("startIndex");
			endIndex=ec.getInt("endIndex");
			if(enrollment1095StagingParams!=null && enrollment1095StagingParams.getEnrollmentIdList()!=null && !enrollment1095StagingParams.getEnrollmentIdList().isEmpty()) {
				if(startIndex<enrollment1095StagingParams.getEnrollmentIdList().size() && endIndex<=enrollment1095StagingParams.getEnrollmentIdList().size()){
					enrollmentIds= new ArrayList<Integer>(enrollment1095StagingParams.getEnrollmentIdList().subList(startIndex, endIndex));
				}
			}
		}
	}
	
	@Override
	public Integer read() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {
		
		if(enrollmentIds!=null && !enrollmentIds.isEmpty()){
			if(loopCount<enrollmentIds.size()){
				loopCount++;
				return enrollmentIds.get(loopCount-1);
				
			}else{
				return null;
			}
			
		}
			return null;
	}

	public Enrollment1095StagingParams getEnrollment1095StagingParams() {
		return enrollment1095StagingParams;
	}

	public void setEnrollment1095StagingParams(Enrollment1095StagingParams enrollment1095StagingParams) {
		this.enrollment1095StagingParams = enrollment1095StagingParams;
	}

}
