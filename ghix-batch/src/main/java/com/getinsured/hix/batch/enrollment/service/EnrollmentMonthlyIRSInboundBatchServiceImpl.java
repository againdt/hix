/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.getinsured.hix.enrollment.repository.IEnrollmentIRSMonthlyExecutionRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentIRSMonthlyInRepository;
import com.getinsured.hix.enrollment.repository.IPLROutboundRepository;
import com.getinsured.hix.enrollment.service.EnrollmentMonthlyIRSNotificationService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.enrollment.util.ZipHelper;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.EnrollmentIRSMonthlyExecution;
import com.getinsured.hix.model.enrollment.EnrollmentIRSMonthlyIn;
import com.getinsured.hix.model.enrollment.EnrollmentIRSMonthlyInDtl;
import com.getinsured.hix.model.enrollment.EnrollmentIRSMonthlyInDtl.FIELD_NAME;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.JiraUtil;
import com.getinsured.hix.platform.util.exception.GIException;

import gov.monthly.cms.dsh.bdshfferesp.extension._1.BatchRequestType;
import gov.monthly.cms.dsh.bdshfferesp.extension._1.RequestDocumentType;
import gov.monthly.cms.hix._0_1.hix_core.ResponseMetadataType;
import us.gov.treasury.irs.common.IRSHouseholdErrorDtlType;
//http://bdshfferesp.dsh.cms.gov/extension/1.0
import us.gov.treasury.irs.ext.aca.hhsisr._4.BatchCategoryCodeType;
import us.gov.treasury.irs.msg.hhsirsepderrorresponsedata.HealthExchangeErrorType;

/**
 * Inbound batch service for Enrollment Monthly IRS reports
 * @author negi_s
 *
 */
@Service("enrollmentMonthlyIRSInboundBatchService")
public class EnrollmentMonthlyIRSInboundBatchServiceImpl implements EnrollmentMonthlyIRSInboundBatchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyIRSInboundBatchServiceImpl.class);
	private static final String MONTHLY_NACK_REPSONSE = "EOMNAK";
	private static final String MONTHLY_ACK_REPSONSE = "EOMACK";
	private static final String MONTHLY_OUT_REPSONSE = "EOMOUT";

	private static final String MONTHLY_NACK_FOLDER = "IRS_NACK";
	private static final String MONTHLY_ACK_FOLDER = "IRS_ACK";
	private static final String MONTHLY_OUT_FOLDER = "IRS_OUT";
	private static final String OUT = ".OUT";

	private static final String ACK = "ack";
	private static final String NACK = "nack";
	private static final String MANIFEST = "manifest";


	private static final String DO_NOTHING = "DO_NOTHING";
	private static final String MARK_ACK ="MARK_ACK";
	private static final String MARK_SUCCESS = "MARK_SUCCESS";
	private static final String RESUB_XML = "RESUB_XML";
	private static final String REGEN_XML = "REGEN_XML";
	private static final String REGEN_ZIP = "REGEN_ZIP";
	private static final String SUCCESS = "SUCCESS";
	private static final String ACKFLAG = "ACK";
	
	private static final String JIRA_MESSAGE_BATCH_TRIGGER = "Unable to trigger monthlyIRSOutJob job from inbound code";



	@Autowired private IEnrollmentIRSMonthlyInRepository enrollmentInRepository;
	@Autowired private IPLROutboundRepository enrollmentOutboundRepository;
	@Autowired private IEnrollmentIRSMonthlyExecutionRepository enrollmentIrsExecutionRepository;
	@Autowired private LookupService lookupService;
	@Autowired private EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService;
	@Autowired private BatchJobExecutionService batchJobExecutionService;
	@Autowired private EnrollmentMonthlyIRSNotificationService enrollmentMonthlyIRSNotificationService;
	@Autowired private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;

	@Override
	public void processInboundResponse(Long jobExecutionId, JobLauncher jobLauncher, Job job) throws GIException {
		String irsMonthlyResponsePath = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.IRS.toString(),
								EnrollmentConstants.TRANSFER_DIRECTION_IN).toString();
		//String irs1095ResponsePath = "D:\\vino\\D2C\\TestIRS";

		File[] allWipDirForDelete = getWipDirList(irsMonthlyResponsePath,OUT);

		if(allWipDirForDelete != null && allWipDirForDelete.length > 0){
			for (File wipDir : allWipDirForDelete) {
				deleteWipFolder(wipDir);
			}
		}

		File[] allInFileForMail = EnrollmentUtils.getFilesInAFolderByName(irsMonthlyResponsePath, OUT);

		if(allInFileForMail != null && allInFileForMail.length > 0){
			sendEnrollmentMonthlyIRSInEmail(allInFileForMail);
		}

		String wipFolder = "WIP_"+new Date().getTime();
		String irsMonthlyResponseWipPath = irsMonthlyResponsePath+ File.separator + wipFolder;

		//Filter Files
		File[] irsAckResponseFiles = EnrollmentUtils.getFilesInAFolderByName(irsMonthlyResponsePath, MONTHLY_ACK_REPSONSE);
		File[] irsNackResponseFiles = EnrollmentUtils.getFilesInAFolderByName(irsMonthlyResponsePath, MONTHLY_NACK_REPSONSE );
		File[] irsOutResponseFiles = EnrollmentUtils.getFilesInAFolderByName(irsMonthlyResponsePath, MONTHLY_OUT_REPSONSE);

		try{
			//Move file to WIP folder
			if(irsAckResponseFiles != null && irsAckResponseFiles.length > 0){
				EnrollmentUtils.moveZipFiles(irsMonthlyResponseWipPath + File.separator + MONTHLY_ACK_FOLDER, irsAckResponseFiles);
			}

			if(irsNackResponseFiles != null && irsNackResponseFiles.length > 0){
				EnrollmentUtils.moveZipFiles(irsMonthlyResponseWipPath + File.separator + MONTHLY_NACK_FOLDER, irsNackResponseFiles);
			}

			if(irsOutResponseFiles != null && irsOutResponseFiles.length > 0){
				EnrollmentUtils.moveZipFiles(irsMonthlyResponseWipPath + File.separator + MONTHLY_OUT_FOLDER, irsOutResponseFiles);
			}

		}catch(Exception e){
			throw new GIException("Error in moving response files" , e);
		}

		try{
			//Process Files
			processInboundAckResponse(irsMonthlyResponseWipPath + File.separator + MONTHLY_ACK_FOLDER, jobExecutionId);
			processInboundNackResponse(irsMonthlyResponseWipPath + File.separator + MONTHLY_NACK_FOLDER, jobExecutionId);
			processInboundOutResponse(irsMonthlyResponseWipPath + File.separator + MONTHLY_OUT_FOLDER, jobExecutionId);

		}catch(Exception e){
			throw new GIException("Error in moving response files" , e);
		}finally{
			deleteWipFolder(new File(irsMonthlyResponseWipPath));
		}
		List<EnrollmentIRSMonthlyIn> processList = reSubmittFileToIrs(jobExecutionId, jobLauncher, job);

		if(processList != null && processList.size() > 0){
			sendEnrollmentMonthlyInProcessEmail(processList);
		}
	}

	private void deleteWipFolder(File irsMonthlyResponseWipPath) {
		try{
			//FileUtils.deleteQuietly(new File(irs1095ResponseWipPath));
			if (irsMonthlyResponseWipPath.isDirectory()) {
				File[] files = irsMonthlyResponseWipPath.listFiles();
				if (files != null && files.length > 0) {
					for (File aFile : files) {
						deleteWipFolder(aFile);
					}
				}
				irsMonthlyResponseWipPath.delete();
			}else {
				irsMonthlyResponseWipPath.delete();
			}
		}catch(Exception ex){
			LOGGER.error("Error Occurred while deleting WIP folder", ex);
		}
	}

	private List<EnrollmentIRSMonthlyIn> reSubmittFileToIrs(Long jobExecutionId, JobLauncher jobLauncher, Job job) throws GIException{

		List<EnrollmentIRSMonthlyIn> monthlyInAllList = enrollmentInRepository.getbyJobExecutionId(String.valueOf(jobExecutionId));
		List<EnrollmentIRSMonthlyIn> monthlyInReSubXMLList   = new ArrayList<>();
		List<EnrollmentIRSMonthlyIn> monthlyInReGenXMLList   = new ArrayList<>();
		List<EnrollmentIRSMonthlyIn> monthlyInReGenZipList   = new ArrayList<>();
		if(monthlyInAllList != null && monthlyInAllList.size() > 0 ){
			for (EnrollmentIRSMonthlyIn enrollmentMonthlyIn : monthlyInAllList) {
				String giAction = enrollmentMonthlyIn.getErrorCodeLkp().getLookupValueLabel();
				switch (giAction) {
				case DO_NOTHING:
					doNothing(enrollmentMonthlyIn);
					break;
				case MARK_ACK:
					updateEnrollmentIRSMonthlyExecution(enrollmentMonthlyIn, ACKFLAG);
					break;
				case MARK_SUCCESS:
					updateEnrollmentIRSMonthlyExecution(enrollmentMonthlyIn, SUCCESS);
					break;
				case RESUB_XML:
					addToResubXML(monthlyInReSubXMLList,enrollmentMonthlyIn);
					break;
				case REGEN_XML:
					monthlyInReGenXMLList.add(enrollmentMonthlyIn);
					updateEnrollmentIRSMonthlyExecution(enrollmentMonthlyIn, REGEN_XML);
					break;
				case REGEN_ZIP:
					monthlyInReGenZipList.add(enrollmentMonthlyIn);
					updateEnrollmentIRSMonthlyExecution(enrollmentMonthlyIn, REGEN_ZIP);
					break;
				}
			}
		}
		resubXML(monthlyInReSubXMLList);
		//REGEN CASE
		Map<String, String> batchRegenXMLTriggerMap = new HashMap<String, String>();
		Map<String, String> batchRegenZipTriggerMap = new HashMap<String, String>();
		for(EnrollmentIRSMonthlyIn enrollmentMonthlyIn : monthlyInReGenXMLList){
			batchRegenXMLTriggerMap.put(enrollmentMonthlyIn.getBatchId(), REGEN_XML);
		}
		for(EnrollmentIRSMonthlyIn enrollmentMonthlyIn : monthlyInReGenZipList){
			batchRegenZipTriggerMap.put(enrollmentMonthlyIn.getBatchId(), REGEN_ZIP);
		}
		if(!batchRegenZipTriggerMap.isEmpty()){
			triggerBatchJobWithParams(batchRegenZipTriggerMap, jobLauncher, job);
		}
		if(!batchRegenXMLTriggerMap.isEmpty()){
			triggerBatchJobWithParams(batchRegenXMLTriggerMap, jobLauncher, job );
		}
		// FOR Process Mail
		List<EnrollmentIRSMonthlyIn> irsMonthlyInList = new ArrayList<>();
		irsMonthlyInList.addAll(monthlyInAllList);

		return irsMonthlyInList;
	}

	/**
	 * Trigger batch job manually from code
	 * @param batchActionMap
	 */
	private void triggerBatchJobWithParams(Map<String, String> batchActionMap, JobLauncher jobLauncher, Job job) {
		Calendar cal = Calendar.getInstance();
		for (Map.Entry<String,String> entry : batchActionMap.entrySet()){
			String batchId = entry.getKey();
			String action = entry.getValue();
			//Query outbound table to get month and year
			String year = null;
			String month =  null;
			List<Object[]> objList = enrollmentOutboundRepository.getYearAndMonthByBatchIdAndType(batchId, "IRS");
			if(null != objList && !objList.isEmpty()){
				year = (String)objList.get(0)[0];
				month = (String)objList.get(0)[1];
			}
			if(NumberUtils.isNumber(month) && NumberUtils.isNumber(year) && Integer.parseInt(month) == (cal.get(Calendar.MONTH)+1) && Integer.parseInt(year) == cal.get(Calendar.YEAR)){
				LOGGER.info("Triggering " + action + " batch Job for batch ID : " + batchId);
				//Trigger batch Job
				try {
					JobParameters jobParameters = new JobParametersBuilder().addString("Year", year)
							.addString("Month", month).addString("BatchId", batchId)
							.addString("Action", action).addLong("EXECUTION_DATE", new Date().getTime())
							.toJobParameters();
					JobExecution jobExecution = jobLauncher.run(job,jobParameters);
					LOGGER.info("Wait till job completes");
					while(jobExecution.isRunning()){
					}
					LOGGER.info("Job complete with exit status : " + jobExecution.getExitStatus());
				} catch (Exception e) {
					LOGGER.error("Error in triggering batch job from inbound code", e);
					logBug(JIRA_MESSAGE_BATCH_TRIGGER, e.getMessage());
				}
			}else{
				String errorDescription = "Month and Year parameteres are null or empty. monthlyIRSOutJob batch job will not be triggered.";
				LOGGER.error(errorDescription);
				logBug(JIRA_MESSAGE_BATCH_TRIGGER, errorDescription);
			}
		}
	}
	
	private void logBug(String message, String description) {
		String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		if(environment!=null && environment.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_ENVIRONMENT_PRODUCTION) && isJiraEnabled){
			 String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
	         JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
	        		         Arrays.asList(fixVersion), 
	        		         description, 
	        		         message,
	        		         null);
		}
		 enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.IRS,
				 message, description);
	}

	private void updateEnrollmentIRSMonthlyExecution(EnrollmentIRSMonthlyIn enrollmentMonthlyIn, String update) {
		String batchId = enrollmentMonthlyIn.getBatchId();
		String documentId = enrollmentMonthlyIn.getDocumentSeqId();
		String householdCaseIds = null;
		try{
			if(batchId != null && documentId != null && !REGEN_ZIP.equalsIgnoreCase(update)){
				if(documentId.contains("|")){
					String documentIds [] = documentId.split("\\|");
					StringBuilder householdIdBuilder = new StringBuilder();
					for (String docId : documentIds) {
						String householdIds = enrollmentOutboundRepository.getHouseholdCaseIds(batchId, docId);
						if(null != householdIds){
							householdIdBuilder.append(householdIds).append(",");
						}
					}
					if(householdIdBuilder.length() > 0){
						householdCaseIds = householdIdBuilder.deleteCharAt(householdIdBuilder.length()-1).toString();
					}
				}else{
					householdCaseIds = enrollmentOutboundRepository.getHouseholdCaseIds(batchId, documentId);
				}

			}else if(batchId != null && REGEN_ZIP.equalsIgnoreCase(update)){
				List<String> householdCaseIdList = enrollmentOutboundRepository.getHouseholdIdsByBatchId(batchId);
				StringBuilder householdIdBuilder = new StringBuilder();
				for(String enrollmentIdClob : householdCaseIdList){
					householdIdBuilder.append(enrollmentIdClob).append(",");
				}
				householdCaseIds = householdIdBuilder.deleteCharAt(householdIdBuilder.length()-1).toString();
			}else{
				throw new GIException("Error spliting BatchId = "+ batchId +" or Document Seq Id =  "+documentId );
			}
		}catch(Exception exception){
			LOGGER.error("Error spliting BatchId = "+ batchId +" or Document Seq Id =  "+documentId , exception);
		}


		if(householdCaseIds != null){
			String[] householdArray = householdCaseIds.split(",");

			if(householdArray != null && householdArray.length > 0){
				List<Integer> householdList = new ArrayList<>();
				for (String string : householdArray) {
					householdList.add(Integer.valueOf(string.trim()));
				}
				List<EnrollmentIRSMonthlyExecution> enrollmentExecutionList = new ArrayList<EnrollmentIRSMonthlyExecution>();
				int size = householdList.size();
				if (size > 1000) {
					int numberOfPartitions = size / 1000;
					if (size % 1000 != 0) {
						numberOfPartitions++;
					}
					int firstIndex = 0;
					int lastIndex = 0;
					for (int i = 0; i < numberOfPartitions; i++) {
						firstIndex = i * 1000;
						lastIndex = (i + 1) * 1000;
						if (lastIndex > size) {
							lastIndex = size;
						}
						enrollmentExecutionList.addAll(enrollmentIrsExecutionRepository.getEnrollmentExecutionsByHouseholdAndBatchId(householdList.subList(firstIndex, lastIndex), batchId));
					}
				} else {
					enrollmentExecutionList = enrollmentIrsExecutionRepository.getEnrollmentExecutionsByHouseholdAndBatchId(householdList, batchId);
				}

				for (EnrollmentIRSMonthlyExecution enrollmentIRSMonthlyExecution : enrollmentExecutionList) {
					enrollmentIRSMonthlyExecution.setIrsInboundBatchCategoryCode(enrollmentMonthlyIn.getBatchCategoryCode());
					//					enrollmentIRSMonthlyExecution.setEnrollmentIn1095Id(enrollmentMonthlyIn.getId());
					enrollmentIRSMonthlyExecution.setIrsInboundAction(update);
					enrollmentIrsExecutionRepository.save(enrollmentIRSMonthlyExecution);
				}
				enrollmentMonthlyIn.setIsProcessedFlag(EnrollmentConstants.Y);
				enrollmentInRepository.saveAndFlush(enrollmentMonthlyIn);
			}
		}
	}

	private void addToResubXML(List<EnrollmentIRSMonthlyIn> monthlyInReSubXMLList, EnrollmentIRSMonthlyIn enrollmentMonthlyIn){
		updateEnrollmentIRSMonthlyExecution(enrollmentMonthlyIn,RESUB_XML);
		monthlyInReSubXMLList.add(enrollmentMonthlyIn);
	}

	private void resubXML(List<EnrollmentIRSMonthlyIn> monthlyInReSubXMLList) {

		/* 	
		   	1. Pull XML file from Archive
			2. Package REGEN XMLs into one of many packages based on their Original Batch ID and Batch Category Code. For example REGEN XMLs belonging to a Batch Category Codes IRS_EOY_REQ should be seperated from IRS_EOY_SUBMIT_CORRECTED_RECORDS_REQ and IRS_EOY_RESUBMIT_CORRECTED_RECORDS_REQ ones in seperate package. Similarly REGEN XMLs belonging to seperate Original Batch ID should be packaged into seperate packages.
			3. Generate new Batch ID for each package. 
			4. Generate new Manifest for each package. Only include REGEN XMLs and do not include any new transactions, or mix with corrections or void transactions.
			5. Reference Original Batch ID
			6. Resubmit & Alert Email
		 */

		Map<String,List<String>> batchIdMap = new HashMap<>();
		Map<String,List<EnrollmentIRSMonthlyIn>> enrInIdMap = new HashMap<>();
		if(monthlyInReSubXMLList != null && monthlyInReSubXMLList.size() > 0 ){
			for (EnrollmentIRSMonthlyIn enrollmentMonthlyIn : monthlyInReSubXMLList) {
				String batchId = enrollmentMonthlyIn.getBatchId();
				String previousBatchId = enrollmentMonthlyIn.getPreviousBatchId();
				if(null == previousBatchId){
					Pageable pageable = new PageRequest(0, 1);
					List<String> batchIdList = enrollmentIrsExecutionRepository.getOriginalBatchId(batchId, pageable);
					if(batchIdList != null && batchIdList.size() > 0) {
						previousBatchId = batchIdList.get(0);
					}
				}
				String key = null;

				if(previousBatchId != null){
					key = batchId+"|"+previousBatchId+"|"+enrollmentMonthlyIn.getBatchCategoryCode();
				}else{
					key = batchId+"|"+enrollmentMonthlyIn.getBatchCategoryCode();
				}

				if(batchIdMap.containsKey(key)){
					List<String> tempList = batchIdMap.get(key);
					tempList.add(enrollmentMonthlyIn.getDocumentSeqId());

					List<EnrollmentIRSMonthlyIn> tempIdList = enrInIdMap.get(key);
					tempIdList.add(enrollmentMonthlyIn);

				}else{
					List<String> tempList = new ArrayList<>();
					tempList.add(enrollmentMonthlyIn.getDocumentSeqId());

					batchIdMap.put(key, tempList);

					List<EnrollmentIRSMonthlyIn> tempInList = new ArrayList<>();
					tempInList.add((enrollmentMonthlyIn));
					enrInIdMap.put(key, tempInList);
				}
			}

			for (Map.Entry<String,List<String>> entry : batchIdMap.entrySet()){
				String batchIdAndCode = entry.getKey();
				List<String> documentList = entry.getValue();

				// Split Documnet Seq Id as it may contains "|"
				List<String> newDocumentList = new ArrayList<>();
				for (String docId : documentList) {
					if(docId.contains("|")){
						newDocumentList.addAll(Arrays.asList(docId.split("\\|")));
					}else{
						newDocumentList.add(docId);
					}
				}

				resendXML(batchIdAndCode, newDocumentList, enrInIdMap.get(batchIdAndCode));
			}
		}
	}

	private void doNothing(EnrollmentIRSMonthlyIn enrollmentMonthlyIn) {
		updateEnrollmentIRSMonthlyExecution(enrollmentMonthlyIn, DO_NOTHING );
		sendDoNothingEmail(enrollmentMonthlyIn);
	}

	private void resendXML(String batchIdAndCode, List<String> documentList, List<EnrollmentIRSMonthlyIn> irsMonthlyInList) {
		// copy documents to temp folder
		// create manifest file
		// set to hub
//		String irsFolderPath= DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_IRS_FOLDER_PATH);
		String irsFolderPath= EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.IRS.toString(), EnrollmentConstants.TRANSFER_DIRECTION_OUT).toString();
		String irsAnnualReportArchiveFolderPath = irsFolderPath + File.separator + EnrollmentConstants.ARCHIVE_FOLDER;
		//String irs_annual_report_archive_folderpath = "D:\\vino\\D2C\\TestIRSArchive";

		String batchId_bcc_array [] = batchIdAndCode.split("\\|");
		String batchId = null;
		String originalBatchId = null;
		String batchCategoryCode = null;

		if(batchId_bcc_array != null){
			if(batchId_bcc_array.length == 3){
				batchId = batchId_bcc_array[0];
				originalBatchId = batchId_bcc_array[1];
				batchCategoryCode = batchId_bcc_array[2];
			}else{
				batchId = batchId_bcc_array[0];
				originalBatchId = null;
				batchCategoryCode = batchId_bcc_array[1];
			}
		}
		//Get year and month
		Integer year = 0;
		Integer month = 0;
		List<Object[]> objList = enrollmentOutboundRepository.getYearAndMonthByBatchIdAndType(batchId, "IRS");
		if(null != objList && !objList.isEmpty()){
			year = Integer.valueOf((String)objList.get(0)[0]);
			month = Integer.valueOf((String)objList.get(0)[1]);
		}

		String folderName = batchId.replace(":", "-");

		String directoryTobeZipped = irsAnnualReportArchiveFolderPath + File.separator + folderName;

//		String irsMonthlyResponsePath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_MONTHLY_RESPONSE_PATH);
		String irsMonthlyResponsePath = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.IRS.toString(), EnrollmentConstants.TRANSFER_DIRECTION_IN).toString();
		//String irs1095ResponsePath = "D:\\vino\\D2C\\TestIRS";
		String wipFolder = "WIP_"+new Date().getTime();
		String irsMonthlyResponseWipPath = irsMonthlyResponsePath+ File.separator + wipFolder;

		moveXMLToResend(directoryTobeZipped, irsMonthlyResponseWipPath, documentList);
		String processError = null;

		try {
			enrollmentMonthlyIRSBatchService.generateManifestXML(BatchCategoryCodeType.IRS_EOM_IND_RESUBMIT_MISSING_FILE_REQ.toString(), 
					irsMonthlyResponseWipPath, 
					originalBatchId, 
					batchId, 
					year, 
					month
					);
		} catch (NumberFormatException | GIException e) {
			processError = EnrollmentUtils.shortenedStackTrace(e, 3);
			LOGGER.error("generateManifestXML()::"+e.getMessage(),e);
		}

		if(null == processError){
			for (EnrollmentIRSMonthlyIn enrollmentIn : irsMonthlyInList) {
				enrollmentIn.setIsProcessedFlag(EnrollmentConstants.Y);
				enrollmentInRepository.saveAndFlush(enrollmentIn);
			}
		}else{
			for (EnrollmentIRSMonthlyIn enrollmentIn : irsMonthlyInList) {
				enrollmentIn.setIsProcessedFlag(EnrollmentConstants.N);
				enrollmentIn.setProcessError(processError);
				enrollmentInRepository.saveAndFlush(enrollmentIn);
			}
		}
	}

	private void moveXMLToResend(String directoryTobeZipped, String irsMonthlyResponseWipPath, List<String> documentList) {
		for (String documentId : documentList) {
			File[] irsContentFiles = EnrollmentUtils.getFilesInAFolderByName(directoryTobeZipped, documentId );
			EnrollmentUtils.copyZipFiles(irsMonthlyResponseWipPath , irsContentFiles);
		}
	}

	private void processInboundAckResponse (String ackResponsePath, Long jobExecutionId) {
		File ackfilePath = new File(ackResponsePath);
		ZipHelper zippy = new ZipHelper();
		if(ackfilePath.exists()){
			renameOutFilesToZip(ackfilePath, ackResponsePath);
			String[] ZipFileNames = ackfilePath.list();
			for(String responseFileName : ZipFileNames){
				File SourceZipFile = new File(ackResponsePath + File.separator + responseFileName);

				cleanDir(ackResponsePath);

				zippy.extract(SourceZipFile, ackfilePath);
				try{
					processAckFile(ackResponsePath, responseFileName, jobExecutionId);
				}catch (Exception e) {
					LOGGER.error("Exception while processing File : "+ responseFileName +" Exception :"+ e.getMessage());
				}

				moveToArchive(SourceZipFile);
			}
		}
	}

	private void processInboundNackResponse(String nackResponsePath, Long jobExecutionId) {
		File nackfilePath = new File(nackResponsePath);
		ZipHelper zippy = new ZipHelper();
		if(nackfilePath.exists()){
			renameOutFilesToZip(nackfilePath, nackResponsePath);
			String[] ZipFileNames = nackfilePath.list();
			for(String responseFileName : ZipFileNames){
				File SourceZipFile = new File(nackResponsePath + File.separator + responseFileName);

				cleanDir(nackResponsePath);

				zippy.extract(SourceZipFile, nackfilePath);
				try{
					processNackFile(nackResponsePath,responseFileName, jobExecutionId);
				}catch (Exception e) {
					LOGGER.error("Exception while processing File : "+ responseFileName +" Exception :", e);
				}
				moveToArchive(SourceZipFile);
			}
		}
	}

	private void processInboundOutResponse(String outResponsePath, Long jobExecutionId) {
		File outfilePath = new File(outResponsePath);
		ZipHelper zippy = new ZipHelper();
		if(outfilePath.exists()){
			renameOutFilesToZip(outfilePath, outResponsePath);
			String[] ZipFileNames = outfilePath.list();

			for(String responseFileName : ZipFileNames){
				File SourceZipFile = new File(outResponsePath + File.separator + responseFileName);

				cleanDir(outResponsePath);

				zippy.extract(SourceZipFile, outfilePath);
				try{
					processManifestFile(outResponsePath,responseFileName, jobExecutionId);
				}catch (Exception e) {
					LOGGER.error("Exception while processing File : "+ responseFileName +" Exception :"+ e.getMessage());
				}
				moveToArchive(SourceZipFile);
			}
		}
	}

	private void cleanDir(String outResponsePath) {
		// clean directory before extracting file
		File[] nonZipFileList = EnrollmentUtils.getFilesInAFolderNotByName(new File(outResponsePath).getAbsolutePath(), ".zip" );
		for (File file : nonZipFileList) {
			file.delete();
		}

	}

	@SuppressWarnings("unchecked")
	private void processManifestFile(String outResponsePath,String responseFileName, Long jobExecutionId) {
		//Process Manifest file
		File[] manifestResponseFiles = EnrollmentUtils.getFilesInAFolderByName(new File(outResponsePath).getAbsolutePath(), MANIFEST );
		BatchRequestType manifestBatchRequestType = null;

		if(manifestResponseFiles != null && manifestResponseFiles.length == 1){
			try{
				JAXBContext jaxbContext = JAXBContext.newInstance(gov.monthly.cms.dsh.bdshfferesp.exchange._1.ObjectFactory.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				JAXBElement<BatchRequestType> batchRequestTypeList = null;
				batchRequestTypeList = (JAXBElement<BatchRequestType>) jaxbUnmarshaller.unmarshal(manifestResponseFiles[0]);
				manifestBatchRequestType = batchRequestTypeList.getValue();
			}catch (JAXBException e) {
				LOGGER.error("JAXBException while Processing Manifest File");
			}

		}

		File[] nonManifestResponseFiles = getNonManifestResponseFile(new File(outResponsePath).getAbsolutePath());
		Map<String,HealthExchangeErrorType> attachmentMap = new HashMap<>();

		if(nonManifestResponseFiles != null && nonManifestResponseFiles.length == 1){
			for (File file : nonManifestResponseFiles) {
				try{
					JAXBContext jaxbContext = JAXBContext.newInstance(us.gov.treasury.irs.msg.hhsirsepderrorresponsedata.ObjectFactory.class);
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					JAXBElement<HealthExchangeErrorType> healthExchangeErrorType = null;
					healthExchangeErrorType = (JAXBElement<HealthExchangeErrorType>) jaxbUnmarshaller.unmarshal(file);
					attachmentMap.put(file.getName(), healthExchangeErrorType.getValue());
				}catch (JAXBException e) {
					LOGGER.error("JAXBException while Processing Manifest File");
				}
			}
		}

		updateEnrollmentIRSMonthlyInForManifest(manifestBatchRequestType, attachmentMap, responseFileName, jobExecutionId);
	}

	private File[] getNonManifestResponseFile(String absolutePath) {
		File[] files = null;
		try{
			File folder= new File(absolutePath);
			files = folder.listFiles( new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					if(pathname.isFile() && (pathname.getName().contains(MANIFEST) || pathname.getName().contains(".zip")) ){
						return false;
					}
					return true;
				}
			});
		}catch(Exception e){
			LOGGER.error("Error getting files in location", e);
		}
		return files;
	}

	@SuppressWarnings("unchecked")
	private void processAckFile(String ackResponsePath, String responseFileName, Long jobExecutionId) {
		File[] irsNackResponseFiles = EnrollmentUtils.getFilesInAFolderByName(ackResponsePath, ACK );
		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(gov.monthly.cms.dsh.bdshfferesp.exchange._1.ObjectFactory.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			JAXBElement<BatchRequestType> batchRequestTypeList = null;
			BatchRequestType batchRequestType = null;

			for (File file : irsNackResponseFiles) {
				batchRequestTypeList = (JAXBElement<BatchRequestType>) jaxbUnmarshaller.unmarshal(file);
				batchRequestType = batchRequestTypeList.getValue();
				updateEnrollmentMonthlyInForAckAndNack(batchRequestType, ACK, responseFileName, jobExecutionId);
			}
		}catch (JAXBException e) {
			e.printStackTrace();
		}
		LOGGER.info("Process ACK File");
	}

	@SuppressWarnings("unchecked")
	private void processNackFile(String nackFilePath, String responseFileName, Long jobExecutionId) {
		File[] irsNackResponseFiles = EnrollmentUtils.getFilesInAFolderByName(nackFilePath, NACK );
		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(gov.monthly.cms.dsh.bdshfferesp.exchange._1.ObjectFactory.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			JAXBElement<BatchRequestType> batchRequestTypeList = null;
			BatchRequestType batchRequestType = null;

			for (File file : irsNackResponseFiles) {
				batchRequestTypeList = (JAXBElement<BatchRequestType>) jaxbUnmarshaller.unmarshal(file);
				batchRequestType = batchRequestTypeList.getValue();
				updateEnrollmentMonthlyInForAckAndNack(batchRequestType, NACK, responseFileName, jobExecutionId);
			}
		}catch (JAXBException e) {
			LOGGER.info("Error processing NACK File JAXBException " , e);
		}
		LOGGER.info("Process NACK File");
	}

	private void updateEnrollmentMonthlyInForAckAndNack(BatchRequestType batchRequestType, String fileName, String responseFileName, Long jobExecutionId) {
		List<EnrollmentIRSMonthlyIn> enrollmentInList = new ArrayList<>();
		enrollmentInList.add(updateEnrollmentMonthlyIn(batchRequestType, fileName, responseFileName, jobExecutionId));

		for (EnrollmentIRSMonthlyIn enrIn : enrollmentInList) {
			enrollmentInRepository.save(enrIn);
		}
	}

	private void updateEnrollmentIRSMonthlyInForManifest(BatchRequestType manifestBatchRequestType, Map<String, HealthExchangeErrorType> attachmentMap, String responseFileName, Long jobExecutionId) {

		EnrollmentIRSMonthlyIn enrollmentIn = updateEnrollmentMonthlyIn(manifestBatchRequestType, MANIFEST, responseFileName, jobExecutionId);
		List<EnrollmentIRSMonthlyInDtl> enrollmentMonthlyInDtlList = new ArrayList<>();

		if(attachmentMap != null && !attachmentMap.isEmpty()){
			for(Entry<String, HealthExchangeErrorType> entry : attachmentMap.entrySet()){
				HealthExchangeErrorType healthExchangeErrorType = entry.getValue();

				if(null != healthExchangeErrorType.getIndividualExchangeError()){
					JAXBContext jaxbContext = null;
					Marshaller jaxbMarshaller = null;
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = null;
					try {
						jaxbContext = JAXBContext.newInstance(IRSHouseholdErrorDtlType.class);
						jaxbMarshaller = jaxbContext.createMarshaller();
						jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
						dBuilder = dbFactory.newDocumentBuilder();
					} catch (Exception e) {
						LOGGER.error("Error creating marshaller or Document Builder :: " + e.getMessage(), e);
					}
					for(IRSHouseholdErrorDtlType householdErrorDtlType :healthExchangeErrorType.getIndividualExchangeError().getIRSHouseholdErrorDtl()){
						LOGGER.info("IRSGroupIdentificationNum : " + householdErrorDtlType.getIRSGroupIdentificationNum());
						StringWriter sw = new StringWriter();
						if(null != jaxbMarshaller && null != dBuilder){
							try {
								jaxbMarshaller.marshal(householdErrorDtlType, sw);
								Document doc = dBuilder.parse(new ByteArrayInputStream(sw.toString().trim().getBytes()));
								doc.getDocumentElement().normalize();
								XPath xPath =  XPathFactory.newInstance().newXPath();
								String expression = ".//EPDErrorDetail";
								NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
								for (int i = 0; i < nodeList.getLength(); i++) {
									EnrollmentIRSMonthlyInDtl enrollmentIRSMonthlyInDtl = new EnrollmentIRSMonthlyInDtl();
									Node nNode = nodeList.item(i);
									if (nNode.getNodeType() == Node.ELEMENT_NODE) {
										Element eElement = (Element) nNode;
										enrollmentIRSMonthlyInDtl.setEnrollmentMonthlyIn(enrollmentIn);
										enrollmentIRSMonthlyInDtl.setHouseholdCaseId(Integer.valueOf(householdErrorDtlType.getIRSGroupIdentificationNum()));
										enrollmentIRSMonthlyInDtl.setErrorCode(eElement.getElementsByTagName("ErrorMessageCd").item(0).getTextContent());
										enrollmentIRSMonthlyInDtl.setXpathContent(eElement.getElementsByTagName("XpathContent").item(0).getTextContent());
										if("500".equalsIgnoreCase(enrollmentIRSMonthlyInDtl.getErrorCode())) {
											if(null != enrollmentIRSMonthlyInDtl.getXpathContent() && enrollmentIRSMonthlyInDtl.getXpathContent().contains("SSN")) {
												enrollmentIRSMonthlyInDtl.setErrorFieldName(FIELD_NAME.SSN.toString());
											}else if(null != enrollmentIRSMonthlyInDtl.getXpathContent() && enrollmentIRSMonthlyInDtl.getXpathContent().contains("EIN")) {
												enrollmentIRSMonthlyInDtl.setErrorFieldName(FIELD_NAME.EIN.toString());
											}
										}
										enrollmentIRSMonthlyInDtl.setErrorMessage(eElement.getElementsByTagName("ErrorMessageTxt").item(0).getTextContent());
									}
									enrollmentMonthlyInDtlList.add(enrollmentIRSMonthlyInDtl);
								}
							} catch (Exception e){
								LOGGER.error("Error processing .OUT file for household  :: "+ householdErrorDtlType.getIRSGroupIdentificationNum(), e);
							}
						}
					}
				}
			}
		}
		if(!enrollmentMonthlyInDtlList.isEmpty()){
			enrollmentIn.setEnrollmentIrsMonthlyInDtlList(enrollmentMonthlyInDtlList);
			enrollmentInRepository.save(enrollmentIn);
		}
	}

	private EnrollmentIRSMonthlyIn updateEnrollmentMonthlyIn(BatchRequestType batchRequestType, String fileName,String responseFileName, Long jobExecutionId) {
		EnrollmentIRSMonthlyIn enrollmentMonthlyIn = new EnrollmentIRSMonthlyIn();

		fillEnrollmentMonthlyIn(batchRequestType,enrollmentMonthlyIn);

		// .OUT file name
		if(null != responseFileName && responseFileName.length()>4 && responseFileName.endsWith(".zip")){
			enrollmentMonthlyIn.setInPackageFileName(responseFileName.substring(0, responseFileName.length()-4));
		}else{
			enrollmentMonthlyIn.setInPackageFileName(responseFileName);
		} 
		enrollmentMonthlyIn.setResponseFileName(fileName); // ack or nac or eom XML file after unzip
		enrollmentMonthlyIn.setJobExecutionId(String.valueOf(jobExecutionId));
		enrollmentMonthlyIn.setErrorCodeLkp(
				lookupService.getlookupValueByTypeANDLookupValueCode(EnrollmentConstants.IRS_MONTHLY_CATEGORY_CODE_LOOKUP_TYPE,batchRequestType.getBatchMetadata().getBatchCategoryCode().value()) );

		List<ResponseMetadataType> responseDataList = null;
		StringBuilder errorCodes = new StringBuilder();
		StringBuilder errorMsg = new StringBuilder();
		if(batchRequestType.getServiceSpecificData() != null){
			responseDataList = batchRequestType.getServiceSpecificData().getResponseMetadata();
			if(null != responseDataList && !responseDataList.isEmpty()){
				for(ResponseMetadataType responseMetadataType : responseDataList){
					errorCodes.append(responseMetadataType.getResponseCode().getValue()).append(",");
					errorMsg.append(responseMetadataType.getResponseDescriptionText().getValue()).append(",");
				}
			}
		}else if(!batchRequestType.getAttachment().isEmpty()){
			for(RequestDocumentType requestDocumentType : batchRequestType.getAttachment()){
				ResponseMetadataType responseMetadataType = requestDocumentType.getResponseMetadata();
				if(null != responseMetadataType){
					errorCodes.append(responseMetadataType.getResponseCode().getValue()).append(",");
					errorMsg.append(responseMetadataType.getResponseDescriptionText().getValue()).append(",");
				}
			}
		}
		enrollmentMonthlyIn.setErrorCode((errorCodes.length() > 1) ? errorCodes.deleteCharAt(errorCodes.length() -1 ).toString() :  null);
		enrollmentMonthlyIn.setErrorMessage((errorMsg.length() > 1) ? errorMsg.deleteCharAt(errorMsg.length() -1 ).toString() :  null);
		return enrollmentMonthlyIn;
	}

	private void fillEnrollmentMonthlyIn(BatchRequestType batchRequestType, EnrollmentIRSMonthlyIn enrollmentMonthlyIn ){
		String documentSeqId = null;
		if(batchRequestType.getBatchMetadata() != null){
			String batchId = batchRequestType.getBatchMetadata().getBatchID().getValue();
			if(StringUtils.isNotEmpty(batchId)){
				if(batchId.contains("|")){
					enrollmentMonthlyIn.setBatchId(batchId.split("\\|")[0]);
					documentSeqId = batchId.substring(batchId.indexOf("|")+EnrollmentConstants.ONE, batchId.length());
					enrollmentMonthlyIn.setDocumentSeqId(documentSeqId);
				}else{
					enrollmentMonthlyIn.setBatchId(batchId);
				}
			}
		}
		// Add MissingDocumentSequenceID in DocumentSeqId coloum
		if(batchRequestType.getServiceSpecificData() != null){
			List<BigInteger> missingDocumentSequenceIDList = batchRequestType.getServiceSpecificData().getMissingDocumentSequenceID();
			if(missingDocumentSequenceIDList != null && missingDocumentSequenceIDList.size() > 0){
				StringBuilder missingDocIds = new StringBuilder();
				DecimalFormat df = new DecimalFormat(String.valueOf("00000"));
				for (BigInteger bigInteger : missingDocumentSequenceIDList) {
					String temp = df.format(bigInteger);
					missingDocIds.append(temp).append("|");
				}
				if(missingDocIds.length() > 5){
					documentSeqId = missingDocIds.deleteCharAt(missingDocIds.length()-1).toString();
					enrollmentMonthlyIn.setDocumentSeqId(documentSeqId);
				}
			}
		}

		if(documentSeqId == null){
			List<RequestDocumentType> requestDocList = batchRequestType.getAttachment();
			if(requestDocList != null && requestDocList.size() > 0){
				StringBuilder missingDocIds = new StringBuilder();
				for (RequestDocumentType requestDocumentType : requestDocList) {
					String temp = requestDocumentType.getDocumentSequenceID().getValue();
					missingDocIds.append(temp).append("|");
				}
				if(missingDocIds.length() > 5){
					documentSeqId = missingDocIds.deleteCharAt(missingDocIds.length()-1).toString();
					enrollmentMonthlyIn.setDocumentSeqId(documentSeqId);
				}
			}
		}

		if(batchRequestType.getServiceSpecificData() != null && batchRequestType.getServiceSpecificData().getOriginalBatchID() != null){
			enrollmentMonthlyIn.setPreviousBatchId(batchRequestType.getServiceSpecificData().getOriginalBatchID().toString());
		}
		if(batchRequestType.getBatchMetadata() != null && batchRequestType.getBatchMetadata().getBatchCategoryCode() != null){
			enrollmentMonthlyIn.setBatchCategoryCode(batchRequestType.getBatchMetadata().getBatchCategoryCode().toString());
		}

		if(batchRequestType.getServiceSpecificData() != null && batchRequestType.getServiceSpecificData().getEFTFileName() != null){
			enrollmentMonthlyIn.setEftFileName(batchRequestType.getServiceSpecificData().getEFTFileName().getValue());
		}
	}

	/**
	 * Send Email Notification
	 * @author negi_s
	 * @param allInFileForMail 
	 */
	private void sendEnrollmentMonthlyIRSInEmail(File[] allInFileForMail) {

		Map<String, String> emailDataMap=new HashMap<>();  
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.GENERATED_DATE, DateUtil.dateToString(new Date(), EnrollmentConstants.FINANCE_DATE_FORMAT));
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_IRS_MONTHLY_INBOUND_PROCESS_JOB);
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.IN_FILE_NAME_ARRAY, getFormattedFileName(Arrays.asList(allInFileForMail).toString()));
		try {
			enrollmentMonthlyIRSNotificationService.sendEnrollmentMonthlyIRSInEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending Enrollment Inbound Process email notification :: ", e);
		}
	}

	/**
	 * Send Email Notification
	 * @author negi_s
	 * @param allInFileForMail 
	 */
	private void sendDoNothingEmail(EnrollmentIRSMonthlyIn enrollmentMonthlyIn) {
		Map<String, String> emailDataMap=new HashMap<>();  
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.GENERATED_DATE, DateUtil.dateToString(new Date(), EnrollmentConstants.FINANCE_DATE_FORMAT));
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_IRS_MONTHLY_INBOUND_PROCESS_JOB);
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.IN_FILE_NAME, enrollmentMonthlyIn.getInPackageFileName());
		try {
			enrollmentMonthlyIRSNotificationService.sendDoNothingEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending Enrollment Inbound Process email notification :: ", e);
		}
	}

	private String getFormattedFileName(String fileNameArray) {
		return fileNameArray.replace("[", "").replace("]", "").replace(",", "<br>");
	}
	 
	private void sendEnrollmentMonthlyInProcessEmail(List<EnrollmentIRSMonthlyIn> processList) {
		Map<String, String> emailDataMap=new HashMap<>();  
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.GENERATED_DATE, DateUtil.dateToString(new Date(), EnrollmentConstants.FINANCE_DATE_FORMAT));
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.JOB_NAME, EnrollmentConstants.ENROLLMENT_IRS_MONTHLY_INBOUND_PROCESS_JOB);
		StringBuffer processFile = new StringBuffer();
		for (EnrollmentIRSMonthlyIn enrollmentIn : processList) {
			processFile.append("<tr>");

			processFile.append("<td style=\"border:1px solid #000;\">");
			processFile.append(enrollmentIn.getInPackageFileName());
			processFile.append("</td>");

			processFile.append("<td style=\"border:1px solid #000;\">");
			processFile.append(enrollmentIn.getIsProcessedFlag());
			processFile.append("</td>");

			processFile.append("<td style=\"border:1px solid #000;\">");
			processFile.append(enrollmentIn.getProcessError());
			processFile.append("</td>");

			processFile.append("</tr>");
		}
		emailDataMap.put(EnrollmentConstants.Enrollment_Monthly_IRS_IN_BatchEmail.IN_FILE_PROCESS_DATA, processFile.toString());

		try {
			enrollmentMonthlyIRSNotificationService.sendEnrollmentMonthlyInProcessEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending Enrollment Inbound Process email notification :: ", e);
		}

	}

	private void moveToArchive(File sourceZipFile) {
		String irsMonthlyResponseArchivePath = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.IRS.toString(),
				EnrollmentConstants.TRANSFER_DIRECTION_IN).append(File.separator).append(EnrollmentConstants.ARCHIVE_FOLDER).toString();
		//String irs1095ResponseArchivePath = "D:\\vino\\D2C\\TestIRSArchive";

		try{
			EnrollmentUtils.createDirectory(irsMonthlyResponseArchivePath);
			if (sourceZipFile!=null && new File(irsMonthlyResponseArchivePath).exists()) {
				//move all files from source to target folder
				sourceZipFile.renameTo(new File(irsMonthlyResponseArchivePath+File.separator+sourceZipFile.getName()));
			}
		}catch(Exception e){
			LOGGER.error("Error in moveFiles()"+e.toString(),e);
		}
	}

	/**
	 * Rename .OUT files to .zip
	 * @param file
	 * @param sourceFolder
	 */
	private void renameOutFilesToZip(File file, String sourceFolder) {
		String[] renameZipFileNames = file.list();
		for(String name : renameZipFileNames){
			File sourceFile = new File(sourceFolder + File.separator + name);
			if(sourceFile.isFile() && sourceFile.getName().endsWith(".OUT")){
				File oldzipFile =sourceFile;          
				// File (or directory) with new name
				String newZipFileName =  oldzipFile + ".zip";			
				File newZipFile = new File(newZipFileName);   // Rename file (or directory)
				oldzipFile.renameTo(newZipFile);
			}
		}		
	}

	private static File[] getWipDirList(String outZipWipPath, String manifest) {
		File[] files = null;
		try{
			File folder= new File(outZipWipPath);
			final String finalResponseType=manifest;
			files = folder.listFiles( new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					if ((pathname.isFile() && pathname.getName().contains(finalResponseType)) || (pathname.isDirectory()
							&& pathname.getName().contains(EnrollmentConstants.ARCHIVE_FOLDER))) {
						return false;
					}
					return true;
				}
			});
		}catch(Exception e){
			LOGGER.error("Error getting files in location");
		}
		return files;
	}
	
	@Override
	public List<BatchJobExecution> getRunningBatchList(String jobName) {
		return batchJobExecutionService.findRunningJob(jobName);
	}


}
