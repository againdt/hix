package com.getinsured.eligibility.plan.service;

import java.util.Date;
import java.util.List;

public class PlanAvailabilityAdapterRequest {

	private long ssapApplicationId;
	private long enrolledSsapApplicationId;
	private boolean healthEnrolled;
	private boolean dentalEnrolled;
	private List<String> healthEnrollees;
	private List<String> dentalEnrollees;
	private long dentalEnrollmentId;
	private long healthEnrollmentId;
	private Date coverageStartDate;
	private boolean healthProcess;
	private boolean dentalProcess;

	public boolean isHealthProcess() {
		return healthProcess;
	}

	public void setHealthProcess(boolean healthProcess) {
		this.healthProcess = healthProcess;
	}

	public boolean isDentalProcess() {
		return dentalProcess;
	}

	public void setDentalProcess(boolean dentalProcess) {
		this.dentalProcess = dentalProcess;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public long getEnrolledSsapApplicationId() {
		return enrolledSsapApplicationId;
	}

	public void setEnrolledSsapApplicationId(long enrolledSsapApplicationId) {
		this.enrolledSsapApplicationId = enrolledSsapApplicationId;
	}

	public List<String> getHealthEnrollees() {
		return healthEnrollees;
	}

	public void setHealthEnrollees(List<String> healthEnrollees) {
		this.healthEnrollees = healthEnrollees;
	}

	public List<String> getDentalEnrollees() {
		return dentalEnrollees;
	}

	public void setDentalEnrollees(List<String> dentalEnrollees) {
		this.dentalEnrollees = dentalEnrollees;
	}

	public long getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}

	public void setDentalEnrollmentId(long dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}

	public long getHealthEnrollmentId() {
		return healthEnrollmentId;
	}

	public void setHealthEnrollmentId(long healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}

	public Date getCoverageStartDate() {
	    return coverageStartDate;
    }

	public void setCoverageStartDate(Date coverageStartDate) {
	    this.coverageStartDate = coverageStartDate;
    }

	public boolean isHealthEnrolled() {
	    return healthEnrolled;
    }

	public void setHealthEnrolled(boolean healthEnrolled) {
	    this.healthEnrolled = healthEnrolled;
    }

	public boolean isDentalEnrolled() {
	    return dentalEnrolled;
    }

	public void setDentalEnrolled(boolean dentalEnrolled) {
	    this.dentalEnrolled = dentalEnrolled;
    }

}
