<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@page import="com.getinsured.hix.model.Broker"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>

<style>
	#menu {
		display:none!important;
	}
</style>

<div class="gutter10">
	<div class="row-fluid">
		<h1>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
	</div>
	
		<form class="form-vertical" id="frmviewbrokerprofile" name="frmviewbrokerprofile" action="viewprofile" method="POST">
		<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
			<div class="row-fluid">
				<div class="span">			
					<table class="table table-border-none span7" role="presentation">
						<tbody>				
							<tr>
								<td>
								<spring:url value="/broker/photo/${encBrokerId}"  var="photoUrl"/>
									<img src="${photoUrl}" alt="Broker thumbnail photo" class="profilephoto thumbnail" width="100px" height="100px" />
								</td>
								<td>
									<h4>${broker.user.firstName}&nbsp;${broker.user.lastName}</h4>
									<p>${broker.location.address1}
									<c:if test="${broker.location.address2!=null && broker.location.address2!=\"\"}"> 
										<br>${broker.location.address2} 
									</c:if>
									<br>
									${broker.location.city}, ${broker.location.state}  ${broker.location.zip}  
									<br/>
									</p>
								</td>
							</tr>
							<tr>
									<td class="txt-right" style="width:30%" ><spring:message code="label.brkPhone"/></td>
									<td style="width:70%"><strong>(${fn:substring(broker.contactNumber,0,3)}) ${fn:substring(broker.contactNumber,4,12)}</strong></td>
							</tr>
							
							<tr>
									<td class="txt-right" style="width:30%" ><spring:message  code="label.brkPublicEmailAdress"/></td>
									<td style="width:70%"><strong>${broker.yourPublicEmail}</strong></td>
							</tr>
							
							<tr>
								<td class="txt-right"><spring:message  code="label.brkClientServed"/></td>
								<td><strong>${broker.clientsServed}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.brkLanguageSpoken"/></td>
								<td><strong>${broker.languagesSpoken}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.brkProductExpertises"/></td>
								<td><strong>${broker.productExpertise}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.brkEducation"/></td>
								<td><strong>${broker.education}</strong></td>
							</tr>
							<c:if test="${broker.aboutMe != '' && broker.aboutMe != null}">
								<tr>
									<td class="txt-right">About me</td>
									<td><strong>${broker.aboutMe}</strong></td>
								</tr>
							</c:if>
						</tbody>
					</table>
					<div class="span5 gutter10">
						<iframe width="270" height="270" title="Google map frame" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="thumbnail" src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}&amp;gl=us&amp;t=m&amp;ll=${broker.location.lat},${broker.location.lon}&amp;z=13&amp;iwloc=near&amp;output=embed"><span class="aria-hidden">your user agent does not support frames or it is currently configured not to display frames.</span></iframe>
					</div>
					</div>
				</div>
		</form>
</div>
	
