package com.getinsured.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.ConsumerDocument;

public interface IConsumerDocumentRepository extends JpaRepository<ConsumerDocument, Long> {

}
