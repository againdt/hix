(function (collections, cart, model) {
	collections.Carts = Backbone.Collection.extend({
		model : model,
		url : 'cartItems?'+(Math.random()*10),
		
		/**
		 * @param resp the response returned by the server
		 * @returns (Array) plans
		 */
		parse : function (resp) {
			var carts = resp;
			
			return carts;
		}
	});
	
	_.extend(collections.Carts.prototype, cart);
})(App.collections, App.backbone.Cart, App.models.Cart);