package com.getinsured.eligibility.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.eligibility.model.EligibilityProgramGiwspayload;

public interface IEligibilityProgramGiwspayload extends JpaRepository<EligibilityProgramGiwspayload, Long> {


}
