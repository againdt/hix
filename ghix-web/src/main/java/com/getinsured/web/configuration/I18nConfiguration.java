package com.getinsured.web.configuration;


import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.getinsured.hix.platform.content.GiReloadableResourceBundleMessageSource;

/**
 * Configuration for i18n.
 *
 * @author Yevgen Golubenko
 * @since 3/1/19
 */
@Configuration
public class I18nConfiguration {
  private static final Logger log = LoggerFactory.getLogger(I18nConfiguration.class);

  @Autowired
  private StateConfiguration stateConfiguration;

  @Value("#{configProp['ghixContentServiceURL']}")
  private String contentServiceUrl;

  private String exchangeCode;

  @PostConstruct
  public void postConstruct() {
    if(stateConfiguration != null && stateConfiguration.getStateCode() != null) {
      log.info("Loaded state configuration: {}, content service url: {}",
          stateConfiguration.getStateCode(), contentServiceUrl);
      exchangeCode = stateConfiguration.getStateCode();
    }
    else {
      log.error("Unable to load state configuration: {}", stateConfiguration);
    }

    if(exchangeCode == null) {
      exchangeCode = "";
    }
  }

  /**
   * Configures {@link GiReloadableResourceBundleMessageSource} as {@code jsonMessageSource}.
   * @return configured {@link GiReloadableResourceBundleMessageSource} object.
   */
  @Bean
  public GiReloadableResourceBundleMessageSource jsonMessageSource() {
    final GiReloadableResourceBundleMessageSource giReloadableResourceBundleMessageSource =
        new GiReloadableResourceBundleMessageSource();

    final String lowerExchangeCode = exchangeCode.toLowerCase();

    String[] resolvedBaseNames =  Arrays.stream(JSON_PROPERTY_FILES)
        .map(prop -> resolveJson(prop, lowerExchangeCode))
        .toArray(String[]::new);

    giReloadableResourceBundleMessageSource.setGiContentServiceUrl(contentServiceUrl);
    giReloadableResourceBundleMessageSource.setBasenames(resolvedBaseNames);

    return giReloadableResourceBundleMessageSource;
  }

  /**
   * Configures global {@link MessageSource}.
   * <p>
   *   {@code MessageSource} configuration will be taking into account exchange name
   *   and will load appropriate properties from the directory for that exchange.
   * </p>
   * @return {@link MessageSource} that is configured with all custom property file locations.
   */
  @Bean
  public MessageSource messageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setDefaultEncoding("UTF-8");
    messageSource.setParentMessageSource(jsonMessageSource());

    log.warn("Configuring message properties for exchange: {}", exchangeCode);

    String[] resolvedBaseNames;

    // initialize state code to be lowercase exchangeCode, so that we don't call .toLowerCase() inside a stream.
    final String stateCode = exchangeCode.toLowerCase();
    switch(exchangeCode)
    {
      case "CA":
        resolvedBaseNames = Arrays.stream(CA_PROPERTIES)
          .map(prop -> resolvePath(prop, stateCode))
          .toArray(String[]::new);
        log.info("Loaded {} i18n properties for: {} exchange", resolvedBaseNames.length, exchangeCode);
        break;
      case "CT":
        resolvedBaseNames = Arrays.stream(CT_PROPERTIES)
            .map(prop -> resolvePath(prop, stateCode))
            .toArray(String[]::new);
        log.info("Loaded {} i18n properties for: {} exchange", resolvedBaseNames.length, exchangeCode);
        break;
      case "ID":
        resolvedBaseNames = Arrays.stream(ID_PROPERTIES)
            .map(prop -> resolvePath(prop, stateCode))
            .toArray(String[]::new);
        log.info("Loaded {} i18n properties for: {} exchange", resolvedBaseNames.length, exchangeCode);
        break;
      case "MN":
        resolvedBaseNames = Arrays.stream(MN_PROPERTIES)
            .map(prop -> resolvePath(prop, stateCode))
            .toArray(String[]::new);
        log.info("Loaded {} i18n properties for: {} exchange", resolvedBaseNames.length, exchangeCode);
        break;
      case "NV":
        resolvedBaseNames = Arrays.stream(NV_PROPERTIES)
            .map(prop -> resolvePath(prop, stateCode))
            .toArray(String[]::new);
        log.info("Loaded {} i18n properties for: {} exchange", resolvedBaseNames.length, exchangeCode);
        break;
      case "WA":
        resolvedBaseNames = Arrays.stream(WA_PROPERTIES)
            .map(prop -> resolvePath(prop, stateCode))
            .toArray(String[]::new);
        log.info("Loaded {} i18n properties for: {} exchange", resolvedBaseNames.length, exchangeCode);
        break;
      default:
        throw new BeanCreationException("Unable to create MessageSource bean, exchange code cannot be identified: " + exchangeCode);
    }

    String[] resolvedCommonBaseNames = Arrays.stream(COMMON_PROPERTY_FILES)
        .map(this::resolveCommon)
        .toArray(String[]::new);

    String[] combinedBaseNames = Arrays.copyOf(resolvedBaseNames, resolvedBaseNames.length + resolvedCommonBaseNames.length);
    System.arraycopy(resolvedCommonBaseNames, 0, combinedBaseNames, resolvedBaseNames.length, resolvedCommonBaseNames.length);

    messageSource.setBasenames(combinedBaseNames);
    messageSource.setFallbackToSystemLocale(true);
    messageSource.setUseCodeAsDefaultMessage(true);

    if(stateConfiguration.isDevBucketName()) {
      messageSource.setCacheSeconds(10);
    }

    return messageSource;
  }

  /**
   * Setup message source for validation annotations.
   * @return {@link LocalValidatorFactoryBean}
   */
  @Bean(name = "validator")
  @Primary
  public LocalValidatorFactoryBean localValidatorFactoryBean() {
    LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
    bean.setValidationMessageSource(messageSource());
    bean.afterPropertiesSet();
    return bean;
  }

  /**
   * Resolves path based on current exchange.
   *
   * @param basename base name of the properties file.
   * @param exchangeCode exchange code.
   * @return resolved path.
   */
  private String resolvePath(String basename, String exchangeCode) {
    return String.format("i18n/%s/%s", exchangeCode, basename);
  }

  /**
   * Resolves properties name from common directory.
   * @param basename base name of property file.
   * @return path to a property file.
   */
  private String resolveCommon(String basename) {
    return String.format("i18n/common/%s", basename);
  }

  /**
   * Resolves path of JSON properties.
   * @param basename base name of the json properties files.
   * @param exchangeCode exchange code.
   * @return resolved path.
   */
  private String resolveJson(String basename, String exchangeCode) {
    return String.format("i18n/%s/json/%s", exchangeCode, basename);
  }

  /**
   * List of property files to be loaded for CA, don't use / in the beginning,
   * don't put any state codes in front.
   *
   * <ol>
   *  <li>
   *    Path is resolved like this:<br/>
   *    {@code $iex_root/src/main/resources/i18n/$exchange_code/$PROPERTY_FILE}
   *  </li>
   *  <li>
   *    Dont use postfix _en, _es, as it will be resolved automatically.
   *  </li>
   *  <li>
   *    List is sorted Alphabetically, please keep it that way.
   *  </li>
   * </ol>
   */
  private static final String[] CA_PROPERTIES = new String[] {
      "1095A_admin_menu",
      "a_1095_menu",
      "Account_exceptions",
      "Account_format",
      "Admin_exceptions",
      "Admin_format",
      "admin_menu",
      "Affiliate_format",
      "agency_manager_menu",
      "Application_exceptions",
      "Application_format",
      "approvedadminstaffl1_menu",
      "approvedadminstaffl2_menu",
      "assister_admin_menu",
      "assisterenrollmententityadmin_menu",
      "assisterenrollmententity_menu",
      "Assister_format",
      "assister_menu",
      "Autopay_format",
      "batch_admin_menu",
      "broker_admin_menu",
      "broker_aud",
      "Broker_exceptions",
      "Broker_format",
      "broker_menu",
      "Browser_detection",
      "calheersadministrator_menu",
      "Consumer_format",
      "CRM_exceptions",
      "CRM_format",
      "csr_menu",
      "Employee_format",
      "employee_menu",
      "Employer_format",
      "employer_menu",
      "Enrollment_exceptions",
      "Enrollment_format",
      "enrollment_entity_menu",
      "Entity_exceptions",
      "Entity_format",
      "Financial_exceptions",
      "Financial_info",
      "Home_page",
      "iex_resources",
      "individual_menu",
      "Individual_Portal",
      "IssuerAdmin_format",
      "issuer_admin_menu",
      "issuer_enrollment_representative_menu",
      "Issuer_exceptions",
      "Issuer_format",
      "issuer_menu",
      "issuer_representative_menu",
      "JobSchedule_exceptions",
      "JobSchedule_info",
      "l0_readonly_menu",
      "l1_csr_menu",
      "l2_csr_menu",
      "lce",
      "Member_portal",
      "Noticetype_exceptions",
      "Noticetype_format",
      "operations_menu",
      "Page_title",
      "Plan_display",
      "Plan_exceptions",
      "Plan_format",
      "Provider",
      "Provider_exceptions",
      "Provider",
      "ridp",
      "security_questions",
      "Shop_complaint_format",
      "Shop_exceptions",
      "Shop_format",
      "ssap",
      "supervisor_menu",
      "Third_party_csr_menu",
      "Ticket"
  };

  /**
   * List of property files to be loaded for CT, don't use / in the beginning,
   * don't put any state codes in front.
   *
   * <ol>
   *  <li>
   *    Path is resolved like this:<br/>
   *    {@code $iex_root/src/main/resources/i18n/$exchange_code/$PROPERTY_FILE}
   *  </li>
   *  <li>
   *    Dont use postfix _en, _es, as it will be resolved automatically.
   *  </li>
   *  <li>
   *    List is sorted Alphabetically, please keep it that way.
   *  </li>
   * </ol>
   */
  private static final String[] CT_PROPERTIES = new String[] {
      "1095A_admin_menu",
      "a_1095_menu",
      "Account_exceptions",
      "Account_format",
      "Admin_exceptions",
      "Admin_format",
      "admin_menu",
      "Affiliate_format",
      "Application_exceptions",
      "Application_format",
      "assister_admin_menu",
      "assisterenrollmententityadmin_menu",
      "assisterenrollmententity_menu",
      "Assister_format",
      "assister_menu",
      "Autopay_format",
      "batch_admin_menu",
      "broker_admin_menu",
      "broker_aud",
      "Broker_exceptions",
      "Broker_format",
      "broker_menu",
      "Browser_detection",
      "calheersadministrator_menu",
      "Consumer_format",
      "CRM_exceptions",
      "CRM_format",
      "csr_menu",
      "Employee_format",
      "employee_menu",
      "Employer_format",
      "employer_menu",
      "Enrollment_exceptions",
      "Enrollment_format",
      "enrollment_entity_menu",
      "Entity_exceptions",
      "Entity_format",
      "Financial_exceptions",
      "Financial_info",
      "Home_page",
      "iex_resources",
      "individual_menu",
      "Individual_Portal",
      "IssuerAdmin_format",
      "issuer_admin_menu",
      "issuer_enrollment_representative_menu",
      "Issuer_exceptions",
      "Issuer_format",
      "issuer_menu",
      "issuer_representative_menu",
      "JobSchedule_exceptions",
      "JobSchedule_info",
      "l1_csr_menu",
      "l2_csr_menu",
      "lce",
      "Member_portal",
      "Noticetype_exceptions",
      "Noticetype_format",
      "operations_menu",
      "Page_title",
      "Plan_display",
      "Plan_exceptions",
      "Plan_format",
      "Provider",
      "Provider_exceptions",
      "Provider",
      "ridp",
      "security_questions",
      "Shop_complaint_format",
      "Shop_exceptions",
      "Shop_format",
      "ssap",
      "supervisor_menu",
      "Third_party_csr_menu",
      "Ticket"
  };

  /**
   * List of property files to be loaded for ID, don't use / in the beginning,
   * don't put any state codes in front.
   *
   * <ol>
   *  <li>
   *    Path is resolved like this:<br/>
   *    {@code $iex_root/src/main/resources/i18n/$exchange_code/$PROPERTY_FILE}
   *  </li>
   *  <li>
   *    Dont use postfix _en, _es, as it will be resolved automatically.
   *  </li>
   *  <li>
   *    List is sorted Alphabetically, please keep it that way.
   *  </li>
   * </ol>
   */
  private static final String[] ID_PROPERTIES = new String[] {
      "1095A_admin_menu",
      "a_1095_menu",
      "Account_exceptions",
      "Account_format",
      "Admin_exceptions",
      "Admin_format",
      "admin_menu",
      "Affiliate_format",
      "agency_manager_menu",
      "Application_exceptions",
      "Application_format",
      "approvedadminstaffl1_menu",
      "approvedadminstaffl2_menu",
      "assister_admin_menu",
      "assisterenrollmententityadmin_menu",
      "assisterenrollmententity_menu",
      "Assister_format",
      "assister_menu",
      "Autopay_format",
      "batch_admin_menu",
      "broker_admin_menu",
      "broker_aud",
      "Broker_exceptions",
      "Broker_format",
      "broker_menu",
      "Browser_detection",
      "calheersadministrator_menu",
      "Consumer_format",
      "CRM_exceptions",
      "CRM_format",
      "csr_menu",
      "Employee_format",
      "employee_menu",
      "Employer_format",
      "employer_menu",
      "Enrollment_exceptions",
      "Enrollment_format",
      "enrollment_entity_menu",
      "Entity_exceptions",
      "Entity_format",
      "Financial_exceptions",
      "Financial_info",
      "Home_page",
      "iex_resources",
      "individual_menu",
      "Individual_Portal",
      "IssuerAdmin_format",
      "issuer_admin_menu",
      "issuer_enrollment_representative_menu",
      "Issuer_exceptions",
      "Issuer_format",
      "issuer_menu",
      "issuer_representative_menu",
      "JobSchedule_exceptions",
      "JobSchedule_info",
      "l1_csr_menu",
      "l2_csr_menu",
      "lce",
      "Member_portal",
      "Noticetype_exceptions",
      "Noticetype_format",
      "operations_menu",
      "Page_title",
      "Plan_display",
      "Plan_exceptions",
      "Plan_format",
      "Provider_exceptions",
      "Provider",
      "ridp",
      "security_questions",
      "Shop_complaint_format",
      "Shop_exceptions",
      "Shop_format",
      "ssap",
      "supervisor_menu",
      "Third_party_csr_menu",
      "l0_readonly_menu",
      "Ticket"
  };

  /**
   * List of property files to be loaded for MN, don't use / in the beginning,
   * don't put any state codes in front.
   *
   * <ol>
   *  <li>
   *    Path is resolved like this:<br/>
   *    {@code $iex_root/src/main/resources/i18n/$exchange_code/$PROPERTY_FILE}
   *  </li>
   *  <li>
   *    Dont use postfix _en, _es, as it will be resolved automatically.
   *  </li>
   *  <li>
   *    List is sorted Alphabetically, please keep it that way.
   *  </li>
   * </ol>
   */
  private static final String[] MN_PROPERTIES = new String[] {
      "1095A_admin_menu",
      "a_1095_menu",
      "Account_exceptions",
      "Account_format",
      "Admin_exceptions",
      "Admin_format",
      "admin_menu",
      "Affiliate_format",
      "Application_exceptions",
      "Application_format",
      "assister_admin_menu",
      "assisterenrollmententityadmin_menu",
      "assisterenrollmententity_menu",
      "Assister_format",
      "assister_menu",
      "Autopay_format",
      "batch_admin_menu",
      "broker_admin_menu",
      "broker_aud",
      "Broker_exceptions",
      "Broker_format",
      "broker_menu",
      "Browser_detection",
      "calheersadministrator_menu",
      "Consumer_format",
      "CRM_exceptions",
      "CRM_format",
      "csr_menu",
      "Employee_format",
      "employee_menu",
      "Employer_format",
      "employer_menu",
      "Enrollment_exceptions",
      "Enrollment_format",
      "enrollment_entity_menu",
      "Entity_exceptions",
      "Entity_format",
      "Financial_exceptions",
      "Financial_info",
      "Home_page",
      "iex_resources",
      "individual_menu",
      "Individual_Portal",
      "IssuerAdmin_format",
      "issuer_admin_menu",
      "issuer_enrollment_representative_menu",
      "Issuer_exceptions",
      "Issuer_format",
      "issuer_menu",
      "issuer_representative_menu",
      "JobSchedule_exceptions",
      "JobSchedule_info",
      "l0_readonly_menu",
      "l1_csr_menu",
      "l2_csr_menu",
      "lce",
      "Member_portal",
      "Noticetype_exceptions",
      "Noticetype_format",
      "operations_menu",
      "Page_title",
      "Plan_display",
      "Plan_exceptions",
      "Plan_format",
      "Provider",
      "Provider_exceptions",
      "ridp",
      "security_questions",
      "Shop_complaint_format",
      "Shop_exceptions",
      "Shop_format",
      "ssap",
      "supervisor_menu",
      "Third_party_csr_menu",
      "Ticket"
  };

  /**
   * List of property files to be loaded for NV, don't use / in the beginning,
   * don't put any state codes in front.
   *
   * <ol>
   *  <li>
   *    Path is resolved like this:<br/>
   *    {@code $iex_root/src/main/resources/i18n/$exchange_code/$PROPERTY_FILE}
   *  </li>
   *  <li>
   *    Dont use postfix _en, _es, as it will be resolved automatically.
   *  </li>
   *  <li>
   *    List is sorted Alphabetically, please keep it that way.
   *  </li>
   * </ol>
   */
  private static final String[] NV_PROPERTIES = new String[] {
      "1095A_admin_menu",
      "a_1095_menu",
      "Account_exceptions",
      "Account_format",
      "Admin_exceptions",
      "Admin_format",
      "admin_menu",
      "Affiliate_format",
      "Application_exceptions",
      "Application_format",
      "assister_admin_menu",
      "assisterenrollmententityadmin_menu",
      "assisterenrollmententity_menu",
      "Assister_format",
      "assister_menu",
      "Autopay_format",
      "batch_admin_menu",
      "broker_admin_menu",
      "broker_aud",
      "Broker_exceptions",
      "Broker_format",
      "broker_menu",
      "Browser_detection",
      "calheersadministrator_menu",
      "Consumer_format",
      "CRM_exceptions",
      "CRM_format",
      "csr_menu",
      "Employee_format",
      "employee_menu",
      "Employer_format",
      "employer_menu",
      "Enrollment_exceptions",
      "Enrollment_format",
      "enrollment_entity_menu",
      "Entity_exceptions",
      "Entity_format",
      "Financial_exceptions",
      "Financial_info",
      "Home_page",
      "iex_resources",
      "individual_menu",
      "Individual_Portal",
      "IssuerAdmin_format",
      "issuer_admin_menu",
      "issuer_enrollment_representative_menu",
      "Issuer_exceptions",
      "Issuer_format",
      "issuer_menu",
      "issuer_representative_menu",
      "JobSchedule_exceptions",
      "JobSchedule_info",
      "l0_readonly_menu",
      "l1_csr_menu",
      "l2_csr_menu",
      "lce",
      "Member_portal",
      "Noticetype_exceptions",
      "Noticetype_format",
      "operations_menu",
      "Page_title",
      "Plan_display",
      "Plan_exceptions",
      "Plan_format",
      "Provider",
      "Provider_exceptions",
      "Provider",
      "ridp",
      "security_questions",
      "Shop_complaint_format",
      "Shop_exceptions",
      "Shop_format",
      "ssap",
      "supervisor_menu",
      "Third_party_csr_menu",
      "Ticket"
  };

  /**
   * List of property files to be loaded for WA, don't use / in the beginning,
   * don't put any state codes in front.
   *
   * <ol>
   *  <li>
   *    Path is resolved like this:<br/>
   *    {@code $iex_root/src/main/resources/i18n/$exchange_code/$PROPERTY_FILE}
   *  </li>
   *  <li>
   *    Dont use postfix _en, _es, as it will be resolved automatically.
   *  </li>
   *  <li>
   *    List is sorted Alphabetically, please keep it that way.
   *  </li>
   * </ol>
   */
  private static final String[] WA_PROPERTIES = new String[] {
      "1095A_admin_menu",
      "a_1095_menu",
      "Account_exceptions",
      "Account_format",
      "Admin_exceptions",
      "Admin_format",
      "admin_menu",
      "Affiliate_format",
      "Application_exceptions",
      "Application_format",
      "assister_admin_menu",
      "assisterenrollmententityadmin_menu",
      "assisterenrollmententity_menu",
      "Assister_format",
      "assister_menu",
      "Autopay_format",
      "batch_admin_menu",
      "broker_admin_menu",
      "broker_aud",
      "Broker_exceptions",
      "Broker_format",
      "broker_menu",
      "Browser_detection",
      "calheersadministrator_menu",
      "Consumer_format",
      "CRM_exceptions",
      "CRM_format",
      "csr_menu",
      "Employee_format",
      "employee_menu",
      "Employer_format",
      "employer_menu",
      "Enrollment_exceptions",
      "Enrollment_format",
      "enrollment_entity_menu",
      "Entity_exceptions",
      "Entity_format",
      "Financial_exceptions",
      "Financial_info",
      "Home_page",
      "iex_resources",
      "individual_menu",
      "Individual_Portal",
      "IssuerAdmin_format",
      "issuer_admin_menu",
      "issuer_enrollment_representative_menu",
      "Issuer_exceptions",
      "Issuer_format",
      "issuer_menu",
      "issuer_representative_menu",
      "JobSchedule_exceptions",
      "JobSchedule_info",
      "l1_csr_menu",
      "l2_csr_menu",
      "lce",
      "Member_portal",
      "Noticetype_exceptions",
      "Noticetype_format",
      "operations_menu",
      "Page_title",
      "Plan_display",
      "Plan_exceptions",
      "Plan_format",
      "Provider_exceptions",
      "Provider",
      "ridp",
      "security_questions",
      "Shop_complaint_format",
      "Shop_exceptions",
      "Shop_format",
      "ssap",
      "supervisor_menu",
      "Third_party_csr_menu",
      "Ticket"
  };


  /**
   * Common property files should be put in resources/i18n/common directory.
   *
   * Dont use postfix _en, _es, as it will be resolved automatically.
   */
  private static final String[] COMMON_PROPERTY_FILES = new String[] {
      /*
       * If there are i18n common properties that are the same across the state exchanges
       * put them into ./i18n/common directory and reference base names here.
       */
      "messages"
  };

  /**
   * JSON property files in i18n/state_code/json/ directory.
   */
  private static final String[] JSON_PROPERTY_FILES = new String[] {
      "agencyportal",
      "enroll"
  };

  /*
  //  for f in $(ls) ; do name=$(echo "$f" | cut -f 1 -d '.'); echo "\"$name\","; done

    <beans:value>classpath:/i18n/Account_exceptions</beans:value>
    <beans:value>classpath:/i18n/Plan_display</beans:value>
    <beans:value>classpath:/i18n/Home_page</beans:value>

     MessageSourceJson: json/agencyportal, json/enroll
  */
}
