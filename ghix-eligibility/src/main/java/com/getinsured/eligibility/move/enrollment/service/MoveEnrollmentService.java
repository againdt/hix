package com.getinsured.eligibility.move.enrollment.service;



public interface MoveEnrollmentService {

	String process(MoveEnrollmentRequest request);
}
