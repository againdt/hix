//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ProperNameTextType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.AugmentationType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType;


/**
 * A data type that supplements an nc:Person with tribal information.
 * 
 * <p>Java class for TribalAugmentationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TribalAugmentationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}AugmentationType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}PersonRecognizedTribeIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}PersonAmericanIndianOrAlaskaNativeIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}PersonTribeName" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}LocationStateUSPostalServiceCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TribalAugmentationType", propOrder = {
    "personRecognizedTribeIndicator",
    "personAmericanIndianOrAlaskaNativeIndicator",
    "personTribeName",
    "locationStateUSPostalServiceCode"
})
public class TribalAugmentationType
    extends AugmentationType
{

    @XmlElement(name = "PersonRecognizedTribeIndicator")
    protected Boolean personRecognizedTribeIndicator;
    @XmlElement(name = "PersonAmericanIndianOrAlaskaNativeIndicator")
    protected Boolean personAmericanIndianOrAlaskaNativeIndicator;
    @XmlElement(name = "PersonTribeName")
    protected ProperNameTextType personTribeName;
    @XmlElement(name = "LocationStateUSPostalServiceCode", namespace = "http://niem.gov/niem/niem-core/2.0")
    protected USStateCodeType locationStateUSPostalServiceCode;

    /**
     * Gets the value of the personRecognizedTribeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPersonRecognizedTribeIndicator() {
        return personRecognizedTribeIndicator;
    }

    /**
     * Sets the value of the personRecognizedTribeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonRecognizedTribeIndicator(Boolean value) {
        this.personRecognizedTribeIndicator = value;
    }

    /**
     * Gets the value of the personAmericanIndianOrAlaskaNativeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPersonAmericanIndianOrAlaskaNativeIndicator() {
        return personAmericanIndianOrAlaskaNativeIndicator;
    }

    /**
     * Sets the value of the personAmericanIndianOrAlaskaNativeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonAmericanIndianOrAlaskaNativeIndicator(Boolean value) {
        this.personAmericanIndianOrAlaskaNativeIndicator = value;
    }

    /**
     * Gets the value of the personTribeName property.
     * 
     * @return
     *     possible object is
     *     {@link ProperNameTextType }
     *     
     */
    public ProperNameTextType getPersonTribeName() {
        return personTribeName;
    }

    /**
     * Sets the value of the personTribeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProperNameTextType }
     *     
     */
    public void setPersonTribeName(ProperNameTextType value) {
        this.personTribeName = value;
    }

    /**
     * Gets the value of the locationStateUSPostalServiceCode property.
     * 
     * @return
     *     possible object is
     *     {@link USStateCodeType }
     *     
     */
    public USStateCodeType getLocationStateUSPostalServiceCode() {
        return locationStateUSPostalServiceCode;
    }

    /**
     * Sets the value of the locationStateUSPostalServiceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link USStateCodeType }
     *     
     */
    public void setLocationStateUSPostalServiceCode(USStateCodeType value) {
        this.locationStateUSPostalServiceCode = value;
    }

}
