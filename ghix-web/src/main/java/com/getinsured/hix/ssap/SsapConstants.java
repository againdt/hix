package com.getinsured.hix.ssap;

import org.springframework.stereotype.Component;

@Component
public final class SsapConstants {

	private SsapConstants() {
	}
	
	public static final String SSAP_TKM_PREVIEW = "hasPermission(#model, 'SSAP_TKM_PREVIEW')";
	public static final String SSAP_EDIT= "hasPermission(#model, 'SSAP_EDIT')";
	public static final String SSAP_VIEW= "hasPermission(#model, 'SSAP_VIEW')";
	public static final String SSAP_COUNTY_VIEW= "hasPermission(#model, 'SSAP_COUNTY_VIEW')";
	public static final String SSAP_TRIBE_VIEW= "hasPermission(#model, 'SSAP_TRIBE_VIEW')";
	public static final String SSAP_RIDP_VIEW= "hasPermission(#model, 'SSAP_RIDP_VIEW')";
	public static final String SSAP_SESSION_EDIT= "hasPermission(#model, 'SSAP_SESSION_EDIT')";
	public static final String SSAP_VERIFICATIONS_EDIT= "hasPermission(#model,'individual', 'SSAP_VERIFICATIONS_EDIT')";
	public static final String SSAP_APPLICANT_DOC_CATEGORY_VIEW= "hasPermission(#model, 'SSAP_APPLICANT_DOC_CATEGORY_VIEW')";
	public static final String SSAP_ACCEPT_VERIFICATION_EDIT= "hasPermission(#model, 'SSAP_ACCEPT_VERIFICATION_EDIT')";
	public static final String SSAP_UPLOAD_DOCUMENT_EDIT= "hasPermission(#model, 'SSAP_UPLOAD_DOCUMENT_EDIT')";
	
	public static final String CSR_OVER_RIDE= "csroverride";
	
	public static final String PRIMARY_APPLICANT_FIRST_NAME = "firstName";
	public static final String PRIMARY_APPLICANT_LAST_NAME = "lastName";
	
	public static final String APPLICATION_SOURCE = "applicationSource";
	public static final String TRACK_APPLICATION_SOURCE = "trackApplicationSource";
	public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
}
