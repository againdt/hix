package com.getinsured.eligibility.at.ref.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TSCalendar;

/**
 * @author chopra_s
 * 
 */
@Component("referralOEService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralOEServiceImpl implements ReferralOEService {
	
	//private String previousCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;

	@Override
	public boolean isQE() {
		String oeEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
		final String currentDate = ReferralUtil.formatDate(ReferralUtil.currentDate(), ReferralConstants.DEFDATEPATTERN);
		return (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeEndDate), ReferralUtil.convertStringToDate(currentDate)) > 0);
	}
	
	
	
	/**
	 *  If current date is less than OE start date (iex.current_oe_start_date) &&
	 *  current coverage year (iex.current_coverage_year) is equal to AT coverage year 
	 *  then renewal is true
	 * 
	 */
	@Override
	public boolean isRenewal(long coverageYear) {
		final String currentDate = ReferralUtil.formatDate(ReferralUtil.currentDate(), ReferralConstants.DEFDATEPATTERN);
		String oeStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
		String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		boolean returnVal = false;
		if (currentCoverageYear.equals(String.valueOf(coverageYear)) && (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeStartDate), ReferralUtil.convertStringToDate(currentDate)) < 0)) {
			returnVal = true;
		}
		return returnVal;
	}

	@Override
	public boolean isQE(long coverageYear) {
		final String currentDate = ReferralUtil.formatDate(ReferralUtil.currentDate(), ReferralConstants.DEFDATEPATTERN);
		String oeEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
		String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		String previousOEEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE);
		if (currentCoverageYear.equals(String.valueOf(coverageYear))) {
			return (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeEndDate), ReferralUtil.convertStringToDate(currentDate)) > 0);
		} else {
			return (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(previousOEEndDate), ReferralUtil.convertStringToDate(currentDate)) > 0);
		}
	}

	@Override
	public boolean isOpenEnrollment(long coverageYear) {
		final String currentDate = ReferralUtil.formatDate(ReferralUtil.currentDate(), ReferralConstants.DEFDATEPATTERN);
		String oeStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
		String oeEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
		String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		String previousOEStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_START_DATE);
		String previousOEEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE);
		if (currentCoverageYear.equals(String.valueOf(coverageYear))) {
			return ((ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeStartDate), ReferralUtil.convertStringToDate(currentDate))) >= 0 && (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeEndDate), ReferralUtil.convertStringToDate(currentDate)) <= 0));
		} else {
			return ((ReferralUtil.compareDate(ReferralUtil.convertStringToDate(previousOEStartDate), ReferralUtil.convertStringToDate(currentDate))) >= 0 && (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(previousOEEndDate), ReferralUtil.convertStringToDate(currentDate)) <= 0));
		}
	}



	@Override
	public boolean isQEPDuringOE(long coverageYear) {
		
		String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		String qEPDuringOEStartDateValue = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_QEP_DURING_OE_START_DATE);
		String qEPDuringOEEndDateValue = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_QEP_DURING_OE_END_DATE);
		final String currentDate = ReferralUtil.formatDate(ReferralUtil.currentDate(), ReferralConstants.DEFDATEPATTERN);
		
		if (currentCoverageYear.equals(String.valueOf(coverageYear)) && StringUtils.isNotBlank(qEPDuringOEStartDateValue) && StringUtils.isNotBlank(qEPDuringOEEndDateValue)) {
			return ((ReferralUtil.compareDate(ReferralUtil.convertStringToDate(qEPDuringOEStartDateValue), ReferralUtil.convertStringToDate(currentDate))) >= 0 && (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(qEPDuringOEEndDateValue), ReferralUtil.convertStringToDate(currentDate)) <= 0));
		} else {
			return false;
		}
	}
	
	@Override
	public boolean isDateInsideOEWindow(Date date) {
		boolean status = false;
		Date oeStart = null;
		Date oeEnd = null;
		try {
			oeStart = new SimpleDateFormat("MM/dd/yyyy").parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE));
			oeEnd = new SimpleDateFormat("MM/dd/yyyy").parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE));
			Calendar calendar = TSCalendar.getInstance();
			calendar.setTime(oeEnd);
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.add(Calendar.SECOND, 59);
			oeEnd = calendar.getTime();

			if ((date.equals(oeStart) || date.equals(oeEnd)) || (date.after(oeStart) && date.before(oeEnd))) {
				status = true;
			}

		} catch (ParseException e) {
			throw new GIRuntimeException("Exception occured while checking OE dates :");
		}
		return status;
	}
}
