package com.getinsured.hix.indportal.dto.dashboard;

public class DashboardAnnouncementDTO {

	private String announcementStartDate;
	private String announcementEndDate;
	private String announcementContent;

	public String getAnnouncementStartDate() {
		return announcementStartDate;
	}

	public void setAnnouncementStartDate(String announcementStartDate) {
		this.announcementStartDate = announcementStartDate;
	}

	public String getAnnouncementEndDate() {
		return announcementEndDate;
	}

	public void setAnnouncementEndDate(String announcementEndDate) {
		this.announcementEndDate = announcementEndDate;
	}

	public String getAnnouncementContent() {
		return announcementContent;
	}

	public void setAnnouncementContent(String announcementContent) {
		this.announcementContent = announcementContent;
	}

}
