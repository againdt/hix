package com.getinsured.hix.indportal.dto.dm.application;

import java.util.Map;

public class LawfulPresence {
	 private Boolean lawfulPresenceStatusIndicator;
	 private Boolean citizenshipIndicator;
	 private Boolean naturalizedCitizenIndicator;
	 private Map<String,LawfulPresenceDocument> lawfulPresenceDocumentation;
	 private Boolean noAlienNumberIndicator;


	 // Getter Methods 

	 public Map<String, LawfulPresenceDocument> getLawfulPresenceDocumentation() {
		return lawfulPresenceDocumentation;
	}

	public void setLawfulPresenceDocumentation(Map<String, LawfulPresenceDocument> lawfulPresenceDocumentation) {
		this.lawfulPresenceDocumentation = lawfulPresenceDocumentation;
	}

	public Boolean getCitizenshipIndicator() {
	  return citizenshipIndicator;
	 }

	 public Boolean getNaturalizedCitizenIndicator() {
	  return naturalizedCitizenIndicator;
	 }

	 public Boolean getNoAlienNumberIndicator() {
	  return noAlienNumberIndicator;
	 }

	 // Setter Methods 

	 public void setCitizenshipIndicator(Boolean citizenshipIndicator) {
	  this.citizenshipIndicator = citizenshipIndicator;
	 }

	 public void setNaturalizedCitizenIndicator(Boolean naturalizedCitizenIndicator) {
	  this.naturalizedCitizenIndicator = naturalizedCitizenIndicator;
	 }

	 public void setNoAlienNumberIndicator(Boolean noAlienNumberIndicator) {
	  this.noAlienNumberIndicator = noAlienNumberIndicator;
	 }

	public Boolean getLawfulPresenceStatusIndicator() {
		return lawfulPresenceStatusIndicator;
	}

	public void setLawfulPresenceStatusIndicator(Boolean lawfulPresenceStatusIndicator) {
		this.lawfulPresenceStatusIndicator = lawfulPresenceStatusIndicator;
	}
	}
