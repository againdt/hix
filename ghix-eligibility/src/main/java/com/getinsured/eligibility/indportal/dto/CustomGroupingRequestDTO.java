package com.getinsured.eligibility.indportal.dto;

import java.math.BigDecimal;
import java.util.List;

import com.getinsured.hix.indportal.dto.Group;

public class CustomGroupingRequestDTO {
	 private String coverageStartDate;
	 private BigDecimal applicationMaxAptc;
	 private BigDecimal maximumStateSubsidy;
	 private BigDecimal remainingAPTC;
	 private BigDecimal remainingStateSubsidy;
	 private BigDecimal finalRemainingAPTC;
	 private BigDecimal finalRemainingStateSubsidy;
	 private List<Group> groupList;
	 	 
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public BigDecimal getApplicationMaxAptc() {
		return applicationMaxAptc;
	}
	public void setApplicationMaxAptc(BigDecimal applicationMaxAptc) {
		this.applicationMaxAptc = applicationMaxAptc;
	}
	public BigDecimal getMaximumStateSubsidy() {
		return maximumStateSubsidy;
	}
	public void setMaximumStateSubsidy(BigDecimal maximumStateSubsidy) {
		this.maximumStateSubsidy = maximumStateSubsidy;
	}
	public BigDecimal getRemainingAPTC() {
		return remainingAPTC;
	}
	public void setRemainingAPTC(BigDecimal remainingAPTC) {
		this.remainingAPTC = remainingAPTC;
	}
	public BigDecimal getRemainingStateSubsidy() {
		return remainingStateSubsidy;
	}
	public void setRemainingStateSubsidy(BigDecimal remainingStateSubsidy) {
		this.remainingStateSubsidy = remainingStateSubsidy;
	}
	public BigDecimal getFinalRemainingAPTC() {
		return finalRemainingAPTC;
	}
	public void setFinalRemainingAPTC(BigDecimal finalRemainingAPTC) {
		this.finalRemainingAPTC = finalRemainingAPTC;
	}
	public BigDecimal getFinalRemainingStateSubsidy() {
		return finalRemainingStateSubsidy;
	}
	public void setFinalRemainingStateSubsidy(BigDecimal finalRemainingStateSubsidy) {
		this.finalRemainingStateSubsidy = finalRemainingStateSubsidy;
	}
	public List<Group> getGroupList() {
		return groupList;
	}
	public void setGroupList(List<Group> groupList) {
		this.groupList = groupList;
	}
		
		 
}
