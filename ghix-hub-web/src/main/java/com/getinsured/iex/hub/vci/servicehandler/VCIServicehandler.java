package com.getinsured.iex.hub.vci.servicehandler;

import java.util.Iterator;

import javax.xml.bind.JAXBElement;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ssap.iex.HouseholdMember;
import com.getinsured.iex.hub.platform.ssap.iex.SSAP;
import com.getinsured.iex.hub.platform.ssap.iex.SSAPTaxHouseHold;
import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.exchange._1.ObjectFactory;
import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentIncomeResponsePayloadType;
import com.getinsured.iex.hub.vci.wrapper.CurrentIncomeRequestPayloadTypeWrapper;
import com.getinsured.iex.hub.vci.wrapper.CurrentIncomeResponsePayloadTypeWrapper;
import com.getinsured.iex.hub.vci.wrapper.PersonTypeWrapper;
import com.getinsured.iex.hub.vci.prefixmappers.VCINamespaceMapper;

/**
 * Service handler for VCI
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 *
 */
public class VCIServicehandler extends AbstractServiceHandler{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VCIServicehandler.class);
	private static Marshaller marshaller;
	
	@Override
	public Object getRequestpayLoad() throws HubServiceException {
		
		try{
			return getPayLoadFromJSONInput();
		}catch(HubServiceException he){
			LOGGER.error("Failed to generate the payload",he);
			throw new HubServiceException("Failed to generate the payload:",he);
		}
	}
	
	private SSAP validateInputJSON() throws HubMappingException {
		try{
			return new SSAP(getJsonInput());
		}catch(ParseException ex){
			listJSONKeys();
			throw new HubMappingException("Failed to process input data ["+ex.getMessage()+"]", ex);
		}
	}
	
	public Object getPayLoadFromJSONInput() throws HubServiceException{
		
		SSAP ssap = null;
		try{
			ssap = validateInputJSON();
		}
		catch(HubMappingException he){
			LOGGER.error("Failed to parse SSAP JSON",he);
			throw new HubServiceException("Failed to parse SSAP JSON",he);
		}
		
		SSAPTaxHouseHold taxHousehold = null;
		
		Iterator<SSAPTaxHouseHold> householdCursor = ssap.houseHoldIterator();
		
		CurrentIncomeRequestPayloadTypeWrapper payLoad = new CurrentIncomeRequestPayloadTypeWrapper();
		
		if(householdCursor == null){
			throw new HubServiceException("No household information provided");
		}
		
		while(householdCursor.hasNext()){
			
			taxHousehold = householdCursor.next();
			Iterator<HouseholdMember> memberCursor = taxHousehold.iterator();
			
			createHouseholdForPayload(memberCursor, payLoad);	
			
			if(payLoad.getPerson().isEmpty()){
				throw new HubServiceException("Mimimum one applicant expected for VCI Service");
			}
			
		}
		
		ObjectFactory factory = new ObjectFactory(); 
		
		return factory.createCurrentIncomeRequest(payLoad.getRequest());
		
	}
	
	private void createHouseholdForPayload(Iterator<HouseholdMember> memberCursor, CurrentIncomeRequestPayloadTypeWrapper payLoad) throws HubServiceException{
		
		HouseholdMember houseHoldMember = null;
		
		if(memberCursor == null){
			throw new HubServiceException("No member information provided");
		}
		
		try{
			while(memberCursor.hasNext()){
				houseHoldMember = memberCursor.next();
				PersonTypeWrapper person = new PersonTypeWrapper();
				this.extractSSAPData(houseHoldMember.getSsnInfo(), person);
				this.extractSSAPData(houseHoldMember, person);
				payLoad.getPerson().add(person);
			}
		}
		catch(HubMappingException he){
			LOGGER.error("Failed to create household information",he);
			throw new HubServiceException("Failed to create household information",he);
		}	
		
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public String handleResponse(Object responseObject) 
			throws HubServiceException {
		String message = "";
		try {
			JAXBElement<CurrentIncomeResponsePayloadType> test = (JAXBElement<CurrentIncomeResponsePayloadType>)responseObject;
			CurrentIncomeResponsePayloadType response = test.getValue();
			CurrentIncomeResponsePayloadTypeWrapper responseWrapper = new CurrentIncomeResponsePayloadTypeWrapper(response);
			if(responseWrapper != null && responseWrapper.getResponseMetadata() != null){
				this.setResponseCode(responseWrapper.getResponseMetadata().getResponseCode());
			}
			message = responseWrapper.toJSONString();
			this.requestStatus = RequestStatus.SUCCESS;
		}
		catch(ClassCastException ce) {
			message = "Help! I do know not any thing about "+responseObject.getClass().getName()+" Expecting:"+CurrentIncomeResponsePayloadType.class.getName();
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			throw new HubServiceException(message, ce);
		}
		
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		return "H08T";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(marshaller == null) {
			marshaller= this.getMarshaller("com.getinsured.iex.hub.vci.gov.cms.dsh.vci.exchange._1",new VCINamespaceMapper());
		}
		return marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(marshaller == null) {
			marshaller= this.getMarshaller("com.getinsured.iex.hub.vci.gov.cms.dsh.vci.exchange._1",new VCINamespaceMapper());
		}
		return (Unmarshaller)marshaller;
	}

}
	