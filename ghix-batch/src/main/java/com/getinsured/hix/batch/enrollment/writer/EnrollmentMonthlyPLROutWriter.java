package com.getinsured.hix.batch.enrollment.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyPLRBatchService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentPLROut;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;


public class EnrollmentMonthlyPLROutWriter implements ItemWriter<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPLROutWriter.class);
	int applicableMonth;
	int applicableYear;
	int partition;
	private EnrollmentPLROut enrollmentPLROut;
	private EnrollmentMonthlyPLRBatchService enrollmentMonthlyPLRBatchService;
	private JobService jobService;
	long jobExecutionId = -1;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		ExecutionContext ec = stepExecution.getExecutionContext();
		jobExecutionId=stepExecution.getJobExecution().getId();
		if(ec != null){
			applicableMonth=ec.getInt("month", 0);
			applicableYear=ec.getInt("year",0);
			partition =ec.getInt("partition");
		}
	}

	@Override
	public void write(List<? extends String> items) throws Exception {
		List<String> householdIdList= null;
		try{
			
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
			if(items!=null && items.size()>0){
				householdIdList= new ArrayList<String>(items);
				enrollmentMonthlyPLRBatchService.processPLRHousehold(householdIdList, applicableMonth, applicableYear, partition);
				
			}
			
		}catch(Exception e){
			LOGGER.error("Exception occurred in IndivEnrollmentIRSOutWriter: ", e);
			
		}
		}

	public EnrollmentPLROut getEnrollmentPLROut() {
		return enrollmentPLROut;
	}

	public void setEnrollmentPLROut(EnrollmentPLROut enrollmentPLROut) {
		this.enrollmentPLROut = enrollmentPLROut;
	}

	public EnrollmentMonthlyPLRBatchService getEnrollmentMonthlyPLRBatchService() {
		return enrollmentMonthlyPLRBatchService;
	}

	public void setEnrollmentMonthlyPLRBatchService(
			EnrollmentMonthlyPLRBatchService enrollmentMonthlyPLRBatchService) {
		this.enrollmentMonthlyPLRBatchService = enrollmentMonthlyPLRBatchService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	
}
