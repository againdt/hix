<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="paddingR20">
<div id="header" class="gray"></div> 
<div class="row-fluid">
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
</div>           
<div id="commentbox" style="display: none">
        <form class="form-vertical">
        <input type="hidden" name="employeeName" id="employeeName" value="" />
                <div class="profile">
                        <div id="loading" class="txt-center" style="display:none">
                                <label class="green" class="gutter10"><spring:message code="label.agent.employee.addNewComment.SavingYourComment"/></label>
                                <img  src="<%=request.getContextPath() %>/resources/img/loader_greendots.gif" width="15%" alt="loader dots" />
                        </div>          

                        <div class="control-group">     
                                <div class="controls">
                                        <textarea class="span" name="comment_text" id="comment_text" rows="7" cols="40" style="resize: none;" placeholder="Enter comments here&hellip;"
                                                        maxlength="4000" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();"></textarea>
                                        <div id="comment_text_error"></div>
                                        <div>
                                                <button class="btn" data-dismiss="modal" aria-hidden="true" onClick="window.parent.closeIFrame();"><spring:message code="label.agent.employee.addNewComment.Cancel"/></button>
                                                <input type="button" name="submit_comment" id="submit_comment" value="<spring:message code="label.agent.employee.addNewComment.save-button"/>" class="btn btn-primary" target="_parent" />
                                                <span id="chars_left" class="pull-right"><spring:message code="label.agent.employee.addNewComment.CharactersLeft"/>&#58; <b>4000</b>&nbsp;</span>
                                        </div>
                                </div>
                        </div>
                </div>
        </form> 
</div>
</div>
<script type="text/javascript">
        
        $(document).ready(function() {
        	        	
        		$("#emplComment").removeClass("link");
        		$("#emplNewcomment").addClass("active");
        		$("#emplViewEmployerAccount").removeClass("link");
        		$("#emplViewEmployeeAccount").removeClass("link");		
        		$("#emplEnrollment").removeClass("link");
        		$("#emplSummary").removeClass("link");
        		
                $('#commentbox').show('slow', 'linear');
                
                var employeeName = getParameterByName('employeeName');
                $('#header').html("<h4>"+employeeName+"</h4>");
                
                $('#submit_comment').click(function(){
                        
                        var target_id = ${target_id};
                        var target_name = "${target_name}";
                        //var commented_by =  document.getElementById("commenter_name").value;
                        var comment_text =  document.getElementById("comment_text").value;
                        
                        //Validations
                        var valMessage="";
                        /* if($.trim(commented_by) == ''){
                                valMessage = "Please enter your name";
                                document.getElementById("commenter_name").value = '';
                        } */
                        if($.trim(comment_text) == ''){
                                valMessage += "Please enter comment";
                                document.getElementById("comment_text").value = '';
                        }
                        if(target_name == ''){
                                valMessage += "\nTarget-name missing";
                        }
                        if(target_id == ''){
                                valMessage += "\nTarget-id missing";
                        }
                        if(valMessage != ''){
                        	$('<div id="alert" class="modal" style="margin-top: 0px;margin-left: auto;width: 300px;" ><div class="modal-body">' + valMessage + ' <br/><br/><button class="btn offset2" data-dismiss="modal" aria-hidden="true">OK</button><br/></div></div>').modal({backdrop:false});
                                return false;
                        }
                        
                        if ($('#loading').is(':hidden')) {
                                $('#loading').show('slow', 'linear');
                        }
                        
                        var  strPath =  "<%=  request.getContextPath()  %>";
                        var  pathURL=strPath+"/platform/web/comment/controller/savecomment";
                        
                        //$.get(pathURL, function(data) {
                        //POST request for ajax call
                        $.post(pathURL, {target_id :target_id, target_name : target_name, comment_text : document.getElementById("comment_text").value  }, 
                                        function(data) {        
                                
                                if(data == 'success'){
                                        //alert('Thank you for your comments !');
                                }
                                else{           
                                        alert('Sorry your comment could not be added');
                                }
                                $('#commentbox').hide('slow', 'linear');
                                $('#loading').hide('slow', 'linear');
                                //$('#submit_comment').removeAttr("disabled"); 
                                
                                //Re-setting fields
                                //document.getElementById("commenter_name").value = '';
                                document.getElementById("comment_text").value = '';
                                $('#chars_left').html('Characters left <b>4000</b>' );
                                $('#commentbox').show('slow', 'linear');                        
                                window.parent.closeCommentBox();                                
                        });             
                });             
        });
        
        //Update remaining characters for text area
        function updateCharCount(){             
                var currentLen = $.trim(document.getElementById("comment_text").value).length;
                var maxLen = 4000;
                var charLeft = maxLen - currentLen;
                
                $('#chars_left').html('Characters left <b>' + charLeft + '</b>' );
        }
        
        function getParameterByName( name ) //courtesy Artem
        {
          name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
          var regexS = "[\\?&]"+name+"=([^&#]*)";
          var regex = new RegExp( regexS );
          var results = regex.exec( window.location.href );
          if( results == null )
            return "";
          else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }       
        
</script>