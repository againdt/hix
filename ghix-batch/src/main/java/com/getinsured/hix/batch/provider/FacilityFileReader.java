package com.getinsured.hix.batch.provider;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.CollectionUtils;
import org.xml.sax.SAXException;

import com.getinsured.hix.batch.provider.service.ProviderService;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.ProviderUpload;
import com.getinsured.hix.platform.util.GhixConstants;

public class FacilityFileReader implements ItemReader<ProviderRecord>,
		ItemStream {


	private ProviderFieldsMetadata metadataMap = null;
	private ArrayList<String> columnList;
	private ArrayList<FieldMetadata> derivedFields;
	private HashSet<String> readers = new HashSet<String>();
	private File enclarityDbDir;
	private boolean setupCompleted = false;
	private String enclarityDataDirLocation;
	private String enclarityFacilitiesDataFile;
	private BufferedReader reader = null;
	
	/*********************************Reader Specific variables***********************************************/
	/*********** Please make sure if this class is used as spring bean, scope is defined as "prototype" *****/
	/*********************************************************************************************************/
	
	private String columnDefLine;
	private int lineNo = 0;
	private int readerScope = ProviderFieldsMetadata.FACILITY;
	private static final Logger LOGGER = LoggerFactory.getLogger(FacilityFileReader.class);
	private String currentFileName = null;
	private static final String CSV_SEPARATOR = ",";
	private static final String CSV_HEADER = "Line No,Column,FieldName,Message";
	private static final String FACILITY_ERROR_CSV_FILE = "/logs/facility_error.csv";
	private List<ProviderCsvLine> csvContent;
	private int validRecordCount = 0;
	private int invalidRecordCount = 0;
	private int invalidFieldRecordCount = 0;

	private ProviderService batchProviderService;

	public FacilityFileReader() {
	}

	public ProviderService getBatchProviderService() {
		return batchProviderService;
	}

	public void setBatchProviderService(ProviderService batchProviderService) {
		this.batchProviderService = batchProviderService;
	}

	private void printjobEnv() {

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Provider Data dir:" + enclarityDataDirLocation);
			LOGGER.info("Facility Data File Prefix:" + enclarityFacilitiesDataFile);
		}
	}

	public int getReaderScope() {
		return this.readerScope;
	}

	private synchronized void setupReader() throws InvalidOperationException,
			IOException, ParserConfigurationException, SAXException {
		if (setupCompleted) {
			return;
		}
		enclarityDataDirLocation = GhixConstants.ENCLARITY_DATA_DIR;
		enclarityFacilitiesDataFile = GhixConstants.ENCLARITY_FACILITY_FILE_PREFIX;
		// Only if debug enabled
		// if(logger.isDebugEnabled()){
		this.printjobEnv();
		// }
		if (enclarityDataDirLocation == null) {
			throw new InvalidOperationException(
					"Failed to setup the reader, Enclarity data directory not available");
		}
		enclarityDbDir = new File(enclarityDataDirLocation);
		if (!enclarityDbDir.exists() || !enclarityDbDir.isDirectory()) {
			throw new InvalidOperationException("Failed to setup the reader, ["
					+ enclarityDataDirLocation
					+ "] doesn't exists or not a directory");
		}
		metadataMap = ProviderFieldsMetadata.getMetadataMap();
		this.derivedFields = metadataMap.getDerivedFields();
		csvContent = new ArrayList<>();
		setupCompleted = true;
	}

	public void open(ExecutionContext executionContext)
			throws ItemStreamException {
		currentFileName = null;
		Exception e = null;

		try {
			this.setupReader();
		}
		catch (InvalidOperationException ie) {
			e = new UnexpectedInputException(ie.getMessage(), ie);
		}
		catch (IOException ioe) {
			e = new UnexpectedInputException(ioe.getMessage(), ioe);
		}
		catch (Exception ex) {
			e = new InvalidOperationException("Faild to setup the reader [Failed with message " + ex.getMessage() + "]", ex);
		}

		if (e != null) {
			LOGGER.error("Exception Found",e);
			throw new ItemStreamException(e);
		}

		try {

			List<File> filesInFolder = Files.walk(Paths.get(enclarityDataDirLocation)).filter(Files::isRegularFile).map(Path::toFile).collect(Collectors.toList());
			File currentFile = null;
			boolean loadFileFromECM = false;

			if (CollectionUtils.isEmpty(filesInFolder)) {
				loadFileFromECM = true;
				LOGGER.warn("There is no manual data available file in Enclarity data directory.");
			}
			else {

				int fileCounter = 0;
				for (File file : filesInFolder) {
	
					if (file.isFile() && file.getName().toLowerCase().startsWith(enclarityFacilitiesDataFile.toLowerCase())) {
						fileCounter ++;
						currentFileName = file.getName();
						currentFile = file;
					}
				}

				if (1 < fileCounter) {
					throw new ItemStreamException("Failed to setup the reader, Expected only one file starting with " + enclarityFacilitiesDataFile + ". Actually found " + fileCounter);
				}
				else if (0 == fileCounter) {
					loadFileFromECM = true;
				}
			}

			InputStream currentFileInputStream = null;

			if (loadFileFromECM && null != batchProviderService) {
				String sourceFileEcmId = batchProviderService.getDocumentIdByWaitingStatusFromTrackingTable(ProviderUpload.PROVIDER_TYPE_INDICATOR.F.name());

				if (StringUtils.isBlank(sourceFileEcmId)) {
					throw new ItemStreamException("Failed to setup the reader, Expected Document ECM ID is from tracking table.");
				}
				byte[] fileByteData = batchProviderService.getContentDataById(sourceFileEcmId);
				Content docMetaData = batchProviderService.getContentById(sourceFileEcmId);

				if (null != fileByteData && null != docMetaData) {
					currentFileInputStream = new ByteArrayInputStream(fileByteData);
					currentFileName = docMetaData.getTitle();
				}
				else {
					throw new ItemStreamException("Failed to setup the reader, not able to download Facility File from ECM.");
				}
			}
			else if (null != currentFile) {
				LOGGER.info("Current File is uploaded manually in Enclarity Directory.");
				currentFileInputStream = new FileInputStream(currentFile);
			}

			if (readers.contains(currentFileName)) {
				throw new ItemStreamException("There is another job processing the file:" + currentFileName + " Please try after some time");
			}

			if (null == currentFileInputStream) {
				throw new ItemStreamException("Failed to setup the reader, There is no data available file either in Enclarity data directory or in ECM.");
			}

			if (loadFileFromECM && null != batchProviderService) {
				// Update IN_PROGRESS status of Tracking record from file name
				batchProviderService.updateTrackingStatusFromFileName(currentFileName, ProviderUpload.STATUS.IN_PROGRESS);
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Loading data from file:" + currentFileName);
			}
			reader = new BufferedReader(new InputStreamReader(currentFileInputStream));
			columnDefLine = reader.readLine();
			lineNo++;

			if (columnDefLine == null) {
				throw new InvalidOperationException("No data available, Invalid file");
			}

			String[] columns = columnDefLine.split(",");
			columnList = new ArrayList<String>();

			for (String column : columns) {
				columnList.add(column);

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Found column:" + column);
				}
			}
			readers.add(currentFileName);
		}
		catch (Exception ex) {
			LOGGER.error("Exception caught while opening the file:"+this.currentFileName+" Failed with :"+ex.getMessage(),ex);

			try {
				if (reader != null) {
					reader.close();
					reader = null;
				}
			}
			catch (IOException ioe) {
				LOGGER.warn("Exception caught while closing the file:" + this.currentFileName + " Failed with :" + ex.getMessage(),ioe);
			}
			throw new ItemStreamException(ex);
		}
	}

	private synchronized ProviderRecord readProviderRecordLine()
			throws IOException {
		ProviderRecord pr = null;
		String str = reader.readLine();
		if (str != null) {
			lineNo++;
			pr = new ProviderRecord();
			pr.setColumnDefinitionLine(columnDefLine);
			pr.setLineNo(lineNo);
			pr.setRecordData(str);
		}
		if (pr == null) {
			reader.close();
			reader = null;
			readers.remove(currentFileName);
		}
		return pr;
	}

	@Override
	public ProviderRecord read() throws UnexpectedInputException,
			ParseException, NonTransientResourceException,
			InvalidOperationException, IOException, ProviderValidationException {
		ProviderRecord pr = readProviderRecordLine();
		if (pr != null) {
			ProviderDataField field = null;
			int tokenCounter = 0;
			String str = pr.getRecordData();
			String[] recordFields = new String[columnList.size()];
			boolean startFound = false;
			boolean countExceed = false;
			int wordStart = 0;
			int wordEnd = 0;
			int colNum = 0;

			if (str.contains(",\\N")) {
				str = str.replace(",\\N", ",\"\"");

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Unexpected value:\\N found at line " + pr.getLineNo() + ", defaulting to agreed empty string \"\"");
				}
			}

			for (int current = 0; current < str.length(); current++) {
				// marking the starting of the word
				if (str.charAt(current) == '\"') {
					if (!startFound) {
						if (colNum == recordFields.length) {
							countExceed = true;
							break;
						}
						wordStart = current;
						startFound = true;
					}
					else {
						// found the matching end quote
						wordEnd = current;
						startFound = false;
						str.substring(wordStart, wordEnd + 1);
						recordFields[colNum] = str.substring(wordStart, wordEnd + 1);
						colNum++;
					}
				}
			}

			if (countExceed) {
				invalidRecordCount++;
				throw new ProviderValidationException("Invalid provider data, field list contains more fields than expected count [" + columnList.size() + "]");
			}
			else if (colNum < recordFields.length) {
				invalidRecordCount++;
				throw new ProviderValidationException("Invalid provider data, field list contains less fields than expected count [" + columnList.size() + "]");
			}

			for (String fieldStr : recordFields) {
				tokenCounter++;
				field = handleDataField(pr.getLineNo(), fieldStr, tokenCounter);
				if (field != null) {
					pr.addDataField(field);
				}
			}
			if(pr.getInvalidFields()!=null && pr.getInvalidFields().size() > 0) {
				addCsvFileLine(pr.getInvalidFields());
				invalidFieldRecordCount++;
			}else {
				validRecordCount++;
			}
			
			pr = finalizeProviderRecord(pr);
		}
		return pr;
	}
	
	private void addCsvFileLine(List<ProviderDataField> invalidFields) {
		
		for(ProviderDataField field : invalidFields) {
			ProviderCsvLine line =  new ProviderCsvLine();
			line.setLineNo(field.getLineNo());
			line.setColumn(field.getColumn());
			line.setFieldName(field.getName());
			line.setMessage(field.getValidationException()!=null ? field.getValidationException().getMessage() : "");
			csvContent.add(line);
		}
	}

	private ProviderDataField handleDataField(int lineNo, String sval,
			int tokenCount) throws InvalidOperationException {
		String fName = columnList.get(tokenCount - 1); // List index starts from  0

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(" Processing: Field:" + fName + " Value:" + sval);
		}
		ProviderDataField field = new ProviderDataField();
		if (metadataMap == null) {
			throw new InvalidOperationException(
					"Metadata not available, setup didn't complete, please check logs");
		}
		fName = fName.replace("\"", "");
		FieldMetadata md = metadataMap.get(fName);

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Total field metadata available:" + metadataMap);
		}
		if (md == null) {
			throw new InvalidOperationException(
					"Unknown input field ["
							+ fName
							+ "="
							+ sval
							+ "] on line #"
							+ lineNo
							+ " encountered did you forget to update the metadata file?");
		}
		field.setRecordInfo(lineNo, tokenCount);
		field.setFieldMetaData(md);
		field.setValue(sval, this.readerScope, md.getValidationRequired());
		if (field.isMultiValue()) {
			String name = field.getMultiValueFieldName();
			ArrayList<String> multiVals = field.getProcessedValue();

			if (LOGGER.isDebugEnabled()) {
				if (multiVals == null) {
					LOGGER.debug(field.getName() + " is a multi field without any fields, expected multi fields with name:" + name);
				} else {
					LOGGER.debug(field.getName() + " is a multi field with:" + multiVals.size() + " fields, each with name:" + name);
				}
			}
		}
		return field;
	}

	/*
	 * Check if this provider record already exists, if so merge the 2 or create
	 * new
	 */
	private ProviderRecord finalizeProviderRecord(ProviderRecord pr)
			throws InvalidOperationException, ProviderValidationException {
		if (pr == null) {
			return null;
		}
		if (pr.getErrorCount() > 0) {
			throw new ProviderValidationException(pr.toString());
		}
		String name = null;
		ProviderDataField pd = null;
		for (FieldMetadata md : derivedFields) {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Processing derived field:" + md.getName());
			}
			md.getProcessor(0).getValidationContext()
					.addContextInfo("record_fields", pr.getValidFields());
			pd = new ProviderDataField();
			pd.setName(name);
			pd.setFieldMetaData(md);
			// value will be derived, pass a random string to satisfy the
			// initial validation
			pd.setValue("\"ToBeDerived\"", readerScope, true);
			pr.addDataField(pd);
		}
		// Create a provider class field on the fly and send it to SOLR
		ProviderDataField pdf = new ProviderDataField();
		pdf.setName("provider_class");
		pdf.setFieldMetaData(this.getDefaultMetaData("provider_class"));
		pdf.setValue("facility", readerScope, false);
		pr.addDataField(pdf);
		return pr;
	}

	/*
	 * default metadata for any field we create on the fly and want to send to
	 * SOLR
	 */
	private FieldMetadata getDefaultMetaData(String name) {
		FieldMetadata defMd = new FieldMetadata();
		defMd.setHasMultipleValues(false);
		defMd.setName(name);
		defMd.setSolrAware(true);
		defMd.setType("string");
		return defMd;
	}

	public static void main(String[] args) throws UnexpectedInputException,
			ParseException, NonTransientResourceException, Exception {
		String[] config = { "standalone.xml", "spring-batch.xml" };
		ApplicationContext context = new ClassPathXmlApplicationContext(config);

		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		// Job facilityDataJob = (Job) context.getBean("facilityDirectoryJob");
		Job individualDatajob = (Job) context.getBean("facilityDirectoryJob");

		try {
			/*
			 * Added this line of code to set new parameters to job instance
			 */
			JobParameters jobParameters = new JobParametersBuilder().addLong(
					"time", System.currentTimeMillis()).toJobParameters();

			// JobExecution fac_execution = jobLauncher.run(facilityDataJob,
			// jobParameters);
			// System.out.println("Exit Status [Facility] : " +
			// fac_execution.getStatus());
			JobExecution idv_execution = jobLauncher.run(individualDatajob,
					jobParameters);
			System.out.println("Exit Status [Individual] : "
					+ idv_execution.getStatus());

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Done");
	}

	@Override
	public void update(ExecutionContext executionContext)
			throws ItemStreamException {
	}

	@Override
	public void close() throws ItemStreamException {
		//Write to CSV file before closing up resources
		writeToCsvFile(csvContent);

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Cleaning up reader for "+currentFileName);
		}
		if(reader != null){
			try{
				reader.close();
				reader = null;
				readers.remove(currentFileName);
				lineNo = 0;
			}catch(IOException ie){
				//Ignored
			}
		}
		
	}
	
	
	private void writeToCsvFile(List<ProviderCsvLine> csvContent) {
		try
        {
				File outputFile = new File(GhixConstants.ENCLARITY_DATA_DIR + GhixConstants.FRONT_SLASH + FACILITY_ERROR_CSV_FILE);
				outputFile.getParentFile().mkdir();
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
				bw.write(CSV_HEADER);
				bw.newLine();
				for (ProviderCsvLine csvLine : csvContent)
				{
					StringBuffer oneLine = new StringBuffer();
					oneLine.append(csvLine.getLineNo());
					oneLine.append(CSV_SEPARATOR);
					oneLine.append(csvLine.getColumn());
					oneLine.append(CSV_SEPARATOR);
					oneLine.append(csvLine.getFieldName());
					oneLine.append(CSV_SEPARATOR);
					oneLine.append("\""+csvLine.getMessage()+"\"");
					bw.write(oneLine.toString());
					bw.newLine();
				}
				//Collect field name error counts
				Map<String, Long> summaryMap = csvContent.stream()
				        .collect(Collectors.groupingBy(ProviderCsvLine::getFieldName, Collectors.counting()));
				//Write summary to CSV
				bw.newLine();
				bw.write("Summary");
				bw.newLine();
				bw.write("Total Records Processed" + CSV_SEPARATOR + (validRecordCount + invalidRecordCount + invalidFieldRecordCount)); 
				bw.newLine();
				bw.write("Records Successfully Loaded" + CSV_SEPARATOR + validRecordCount); 
				bw.newLine();
				bw.write("Records Failed Due to Invalid Field Value" + CSV_SEPARATOR + invalidFieldRecordCount); 
				bw.newLine();
				bw.write("Records Failed Due to Field Count Mismatch" + CSV_SEPARATOR + invalidRecordCount); 
				bw.newLine();
				bw.newLine();
				bw.write("Field Name" + CSV_SEPARATOR + "Error Count");
				bw.newLine();
				for(String fieldName : summaryMap.keySet()) {
					StringBuffer summary = new StringBuffer();
					summary.append(fieldName);
					summary.append(CSV_SEPARATOR);
					summary.append(summaryMap.get(fieldName));
					bw.write(summary.toString());
					bw.newLine();
				}
				bw.flush();
				bw.close();

				if (null != batchProviderService) {
					String logs = new String(Files.readAllBytes(Paths.get(GhixConstants.ENCLARITY_DATA_DIR + GhixConstants.FRONT_SLASH + FACILITY_ERROR_CSV_FILE)));
					// Update Tracking (Provider Upload) table with Provider Logs.
					batchProviderService.updateLogsInTrackingTableUsingFileName(logs, currentFileName, validRecordCount);
				}
        }
        catch (Exception e) {
        	LOGGER.error("Exception while writing to csv file: ", e.getMessage());
        }
	}
}
