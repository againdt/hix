package com.getinsured.eligibility.at.ref.service.migration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.eligibility.qlevalidation.QLEAutomationChangesHandlerDetermination;
import com.getinsured.eligibility.qlevalidation.QLEAutomationChangesHandlerDetermination.AllChangesHandlerType;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;

@Service("qleAutomationChangesHandlerMigrationService")
public class QleAutomationChangesHandlerMigrationService extends LceAllChangesHandlerBaseMigrationService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(QleAutomationChangesHandlerMigrationService.class);

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("QleAutomationChangesHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		
		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId());
		
		final AllChangesHandlerType handlerToInvoke 
			= QLEAutomationChangesHandlerDetermination.handlerToInvoke(lceProcessRequestDTO.getEnrolledApplicationAttributes(), ssapApplicationEvent);
		
		LOGGER.info("Handler to invoke - " + handlerToInvoke);
		
		
		if (AllChangesHandlerType.SINGLE.equals(handlerToInvoke)) {
			LOGGER.info("Invoking case " + AllChangesHandlerType.SINGLE + " handler for " + currentApplication.getId());
			handleSingle(currentApplication, lceProcessRequestDTO);
		} 
		else {
		}
	}
	
	@SuppressWarnings("unused")
	private void handleCSAutomation(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		updateAllowEnrollment(currentApplication, Y);
	}

	@SuppressWarnings("unused")
	private void handlePureRemove(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Handle " + AllChangesHandlerType.PURE_REMOVE + " case for " + currentApplication.getId());
		handleAutomation(currentApplication, lceProcessRequestDTO);
	}

	// TODO: use this for QLE
	private void handleAutomation(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		processMixed(currentApplication, true, lceProcessRequestDTO);
    }

	private void handleSingle(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Handle " + AllChangesHandlerType.SINGLE + " case for " + currentApplication.getId());
		updateAllowEnrollment(currentApplication, Y);
	}

	@SuppressWarnings("unused")
	private void handleMixed(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Handle " + AllChangesHandlerType.MIXED + " case for " + currentApplication.getId());
		processMixed(currentApplication, true, lceProcessRequestDTO);
	}

	@SuppressWarnings("unused")
	private void handleDenial(SsapApplication currentApplication) {
		LOGGER.info("Handle " + AllChangesHandlerType.DENIAL + " case for " + currentApplication.getId());
	}

	@SuppressWarnings("unused")
	private void handleManual(SsapApplication currentApplication) {
		updateAllowEnrollment(currentApplication, Y);
	}
}
