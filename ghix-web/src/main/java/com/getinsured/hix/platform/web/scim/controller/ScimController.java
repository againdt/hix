/**
 * 
 */
package com.getinsured.hix.platform.web.scim.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author Biswakesh.Praharaj
 * 
 */
@RestController
@RequestMapping("/scim")
public class ScimController {
	
	private static final String DESCRIPTION = "description";

	private static final String CODE = "code";

	private static final Logger LOGGER = Logger.getLogger(ScimController.class);

	private static final String FALSE = "false";

	private static final String TRUE = "true";

	private static final String RIDP_FLAG_MSG = "RIDP FLAG VALUE IS INCORRECT. PLEASE PASS TRUE OR FALSE. ";

	private static final String SSN_MSG = "SSN FORMAT IS INVALID. PLEASE ENTER SSN IN XXX-XX-XXXX FORMAT. ";

	private static final String SSN_PATTERN = "^\\d{3}[- ]\\d{2}[- ]\\d{4}$";

	private static final String DATE_PATTERN = "MM-dd-yyyy";

	private static final String DATE_MSG = "DATE IS INVALID. PLEASE ENTER VALID DATE IN MM-DD-YYYY FORMAT. ";

	private static final String RIDP_FLAG = "RIDP FLAG";

	private static final String PREFERRED_LANGUAGE = "PREFERRED LANGUAGE";

	private static final String SSN = "SSN";

	private static final String DOB = "DOB";

	private static final String USERNAME = "USERNAME";

	private static final String REST_USER_UPDATE_URI = "/update";
	

	@Autowired
	private UserService userService;

	@Autowired
	private SCIMUserManager scimUserManager;

	@RequestMapping(value = ScimController.REST_USER_UPDATE_URI, method = RequestMethod.POST, consumes = "*/*")
	public @ResponseBody
	String updateUser(
			@RequestParam(value = "username") String username,
			@RequestParam(value = "dob") String dob,
			@RequestParam(value = "ssn") String ssn,
			@RequestParam(value = "preferredLanguage") String preferredLanguage,
			@RequestParam(value = "ridpFlag") String ridpFlag)
			throws GIException {
		AccountUser accountUser = null;
		String externalAppId = null;
		String response = null;
		GIException ex = null;
		String validationErrs = null;
		try {
			validationErrs = validateRequest(username, dob, ssn, preferredLanguage, ridpFlag);
			if(null != validationErrs && validationErrs.length() > 0) {
				response = validationErrs;
			} else {
				accountUser = userService.findByEmail(username);
				if (null != accountUser) {
					externalAppId = accountUser.getExtnAppUserId();
					if (null != externalAppId && externalAppId.length() > 0) {
						
						response = scimUserManager.updateUserAdditionalAttribs(
								externalAppId, username, dob, ssn,
								preferredLanguage, ridpFlag);
						if (LOGGER.isDebugEnabled()) {
							LOGGER.debug("SCIM USR ATTRIBS UPDATED VIA REST");
						}
					}
				}
			}
			
		} catch (GIException e) {
			ex = e;
		} catch (Exception e) {
			ex = new GIException(e.getMessage(), e);
		}

		if (null != ex) {
			throw ex;
		}

		return response;
	}

	/**
	 * 
	 * @param username
	 * @param dob
	 * @param ssn
	 * @param preferredLanguage
	 * @param ridpFlag
	 * @return
	 * @throws GIException
	 */
	private String validateRequest(String username, String dob, String ssn,
			String preferredLanguage, String ridpFlag) throws GIException {
		GIException ex = null;
		StringBuffer messageBuff = null;
		String message = null;
		JSONObject core = null;
		JSONArray jsonArray = null;
		JSONObject errors = null;
		String validationsErr = null;
		try {
			messageBuff = new StringBuffer();
			message = isAttributeEmpty(USERNAME, username);
			if (null != message && message.length() > 0) {
				messageBuff.append(message);
			}

			message = isAttributeEmpty(DOB, dob);
			if (null != message && message.length() > 0) {
				messageBuff.append(message);
			} else {
				message = validateDob(dob);
				if (null != message && message.length() > 0) {
					messageBuff.append(message);
				}
			}
			
			message = isAttributeEmpty(SSN, ssn);
			if (null != message && message.length() > 0) {
				messageBuff.append(message);
			} else {
				message = validateSsn(ssn);
				if (null != message && message.length() > 0) {
					messageBuff.append(message);
				}
			}
			
			message = isAttributeEmpty(PREFERRED_LANGUAGE, preferredLanguage);
			if (null != message && message.length() > 0) {
				messageBuff.append(message);
			}
			
			message = isAttributeEmpty(RIDP_FLAG, ridpFlag);
			if (null != message && message.length() > 0) {
				messageBuff.append(message);
			} else {
				message = validateRidp(ridpFlag);
				if (null != message && message.length() > 0) {
					messageBuff.append(message);
				}
			}
			
			if(null != messageBuff && messageBuff.length() > 0) {
				core = new JSONObject();
				core.put(CODE, HttpServletResponse.SC_BAD_REQUEST);
				core.put(DESCRIPTION, messageBuff.toString());
				
				jsonArray = new JSONArray();
				jsonArray.add(core);
				
				errors = new JSONObject();
				errors.put("Errors", jsonArray);
				validationsErr = errors.toJSONString();
			}
			
		} catch (Exception e) {
			LOGGER.error("ERR: WHILE VALIDATING SCIM ADDNL ATTRIBS UPDATE REQ: ",e);
			ex = new GIException(e.getMessage(), e);
		}
		if(null != ex) {
			throw ex;
		}
		return validationsErr;
	}

	/**
	 * 
	 * @param attributeName
	 * @param attributeValue
	 * @return
	 */
	private String isAttributeEmpty(String attributeName, String attributeValue) {
		StringBuffer message = null;
		try {
			message = new StringBuffer();
			if (null == attributeValue || attributeValue.length() <= 0) {
				message.append(attributeName).append(" CANNOT BE EMPTY. ");
			}

		} catch (Exception e) {
			LOGGER.error("ERR: WHILE CHECKING ATTRS: ", e);
		}
		return message.toString();
	}

	/**
	 * 
	 * @param dob
	 * @return
	 */
	private String validateDob(String dob) {
		StringBuffer message = null;
		DateFormat formatter = null;
		Date date = null;
		try {
			message = new StringBuffer();
			formatter = new SimpleDateFormat(DATE_PATTERN);
			formatter.setLenient(false);
			date = formatter.parse(dob);
		} catch (ParseException e) {
			message.append(DATE_MSG);
			LOGGER.error("ERR: WHILE PARSING DATE: ", e);
		} catch (Exception e) {
			message.append(DATE_MSG);
			LOGGER.error("ERR: WHILE PARSING DATE: ", e);
		}
		return message.toString();
	}

	/**
	 * 
	 * @param ssn
	 * @throws Exception
	 */
	public String validateSsn(String ssn) throws Exception {
		StringBuffer message = null;
		// Initialize reg expression for SSN.
		String expression = SSN_PATTERN;
		CharSequence inputStr = null;
		Pattern pattern = null;
		Matcher matcher = null;

		try {
			message = new StringBuffer();
			
			inputStr = ssn;
			pattern = Pattern.compile(expression);
			matcher = pattern.matcher(inputStr);
			if (!matcher.matches()) {
				message.append(SSN_MSG);
			}
		} catch (Exception e) {
			message.append(SSN_MSG);
			LOGGER.error("ERR: WHILE PARSING SSN: ", e);
		}
		return message.toString();
	}
	
	/**
	 * 
	 * @param dob
	 * @return
	 */
	private String validateRidp(String ridpFlag) {
		StringBuffer message = null;
		
		try {
			message = new StringBuffer();
			
			if(!ridpFlag.equalsIgnoreCase(TRUE) && !ridpFlag.equalsIgnoreCase(FALSE)) {
				message.append(RIDP_FLAG_MSG);
			}
		} catch (Exception e) {
			message.append(RIDP_FLAG_MSG);
			LOGGER.error("ERR: WHILE PARSING DATE: ", e);
		}
		return message.toString();
	}
}
