package com.getinsured.ssap.exception;

/**
 * Interface for checked SSAP exceptions.
 * @author Yevgen Golubenko
 * @since 1/3/19
 */
public interface SsapException
{
  ExceptionType getType();
}
