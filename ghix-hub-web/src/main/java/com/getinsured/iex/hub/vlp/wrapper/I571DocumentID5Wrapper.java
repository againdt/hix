package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.I571DocumentID5Type;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for I571DocumentID5Type
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class I571DocumentID5Wrapper implements JSONAware{
	
private static final Logger LOGGER = LoggerFactory.getLogger(I571DocumentID5Wrapper.class);
	
	private ObjectFactory factory;
	private I571DocumentID5Type i571DocumentID5Type;
	
	private String alienNumber;
	private String expiryDate;

	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	
	public I571DocumentID5Wrapper(){
		this.factory = new ObjectFactory();
		this.i571DocumentID5Type = factory.createI571DocumentID5Type();
	}
	
	public void setAlienNumber(String alienNumber) throws HubServiceException{
		
		if(alienNumber == null){
			throw new HubServiceException("No Alien number provided");
		}
		
		this.alienNumber = alienNumber;
		this.i571DocumentID5Type.setAlienNumber(this.alienNumber);
	}
	
	public String getAlienNumber() {
		return alienNumber;
	}
	
	public void setExpiryDate(String expiryDate) {
		
		try{
			this.i571DocumentID5Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I571 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}
	
	public I571DocumentID5Type geti571DocumentID5Type(){
		return this.i571DocumentID5Type;
	}
	
	/**
	 * Converts the JSON String to I571DocumentID5Wrapper object
	 * 
	 * @param jsonString - String representation for I571DocumentID5Wrapper
	 * @return I571DocumentID5Wrapper Object
	 */
	public static I571DocumentID5Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I571DocumentID5Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		I571DocumentID5Wrapper wrapper = new I571DocumentID5Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(ALIEN_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.alienNumber = (String)tmpObj;
			wrapper.i571DocumentID5Type.setAlienNumber(wrapper.alienNumber);
		}
		
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.i571DocumentID5Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating I571DocumentID5Wrapper from JSON, Invalid expiration date",e);
			}
		}

		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static I571DocumentID5Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		I571DocumentID5Wrapper wrapper = new I571DocumentID5Wrapper();
		wrapper.setAlienNumber((String) jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
}
