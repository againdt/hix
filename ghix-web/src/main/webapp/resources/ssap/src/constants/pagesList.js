import React from 'react';

// Components

import Page1_00 from '../components/pages/Page1_00';
import Page1_01 from '../components/pages/Page1_01';
import Page1_02 from '../components/pages/Page1_02';
import Page1_03 from '../components/pages/Page1_03';
import Page1_04 from '../components/pages/Page1_04';
import Page1_05 from '../components/pages/Page1_05';
import Page2_07 from '../components/pages/Page2_07';
import Page2_08 from '../components/pages/Page2_08';
import Page2_09 from '../components/pages/Page2_09';
import Page2_10 from '../components/pages/Page2_10';
import Page2_11_mod2 from '../components/pages/Page2_11_mod2';
import Page2_12 from '../components/pages/Page2_12';
import Page2_13 from '../components/pages/Page2_13';
import Page2_14 from '../components/pages/Page2_14';
import Page3_15 from '../components/pages/Page3_15';
import Page3_16 from '../components/pages/Page3_16';
import Page3_17 from '../components/pages/Page3_17';
import Page2_21 from '../components/pages/Page2_21';
import Page2_22 from '../components/pages/Page2_22';
import Page2_23 from '../components/pages/Page2_23';
import Page2_24 from '../components/pages/Page2_24';
import Page2_25 from '../components/pages/Page2_25';
import Page2_26 from '../components/pages/Page2_26';
import Page2_27 from '../components/pages/Page2_27';
import Page1_03_E1 from '../components/pages/Page1_03_E1';
import Page3_31_E1 from '../components/pages/Page3_31_E1';
import Page3_32_E1 from '../components/pages/Page3_32_E1';
import Page3_33_E1 from '../components/pages/Page3_33_E1';
import Page3_34_E1 from '../components/pages/Page3_34_E1';
import Page3_35_E1 from '../components/pages/Page3_35_E1';
import Page3_36_E1 from '../components/pages/Page3_36_E1';
import Page3_37_E1 from '../components/pages/Page3_37_E1';
import Page4_61 from '../components/pages/Page4_61';
import Page4_62 from '../components/pages/Page4_62';
import Page4_63 from '../components/pages/Page4_63';
import Page4_64 from '../components/pages/Page4_64';
import Page4_65 from '../components/pages/Page4_65';
import Page4_66 from '../components/pages/Page4_66';
import Page1_07_E2 from '../components/pages/Page1_07_E2';
import Page2_07_E2_mod1 from '../components/pages/Page2_07_E2_mod1';
import Page2_28_E3 from '../components/pages/Page2_28_E3';


// Constants

import buttonsTypes from '../constants/buttonsTypes';


/*******************************

 Pages names

 *******************************/


const ABOUT_YOUR_HOUSEHOLD_PAGE = 'ABOUT_YOUR_HOUSEHOLD_PAGE';
const ACTIVE_MILITARY_VETERAN_PAGE = 'ACTIVE_MILITARY_VETERAN_PAGE';
const ADDITIONAL_INFORMATION_SUMMARY_PAGE = 'ADDITIONAL_INFORMATION_SUMMARY_PAGE';
const ADDITIONAL_INFORMATION_PAGE = 'ADDITIONAL_INFORMATION_PAGE';
const APPLICATION_ASSISTANCE_PAGE = 'APPLICATION_ASSISTANCE_PAGE';
const APPLICANTS_PAGE = 'APPLICANTS_PAGE';
const AMERICAN_INDIAN_ALASKA_NATIVE_PAGE = 'AMERICAN_INDIAN_ALASKA_NATIVE_PAGE';
const BEFORE_WE_BEGIN_PAGE = 'BEFORE_WE_BEGIN_PAGE';
const CITIZENSHIP_IMMIGRATION_STATUS_PAGE = 'CITIZENSHIP_IMMIGRATION_STATUS_PAGE';
const DEDUCTION_SOURCES_PAGE = 'DEDUCTION_SOURCES_PAGE';
const DISABILITY_INFORMATION_PAGE =  'DISABILITY_INFORMATION_PAGE';
const EMPLOYER_COVERAGE_DETAIL_PAGE = 'EMPLOYER_COVERAGE_DETAIL_PAGE';
const ETHNICITY_AND_RACE_PAGE = 'ETHNICITY_AND_RACE_PAGE';
const EXPECTED_INCOME_PAGE = 'EXPECTED_INCOME_PAGE';
const FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE = 'FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE';
const FINAL_APPLICATION_REVIEW_PAGE = 'FINAL_APPLICATION_REVIEW_PAGE';
const FOSTER_CARE_INFORMATION_PAGE = 'FOSTER_CARE_INFORMATION_PAGE';
const FULL_TIME_STUDENT_PAGE = 'FULL_TIME_STUDENT_PAGE';
const GET_READY_FOR_HOUSEHOLD_AND_FAMILY_PAGE = 'GET_READY_FOR_HOUSEHOLD_AND_FAMILY_PAGE';
const GET_READY_FOR_INCOME_PAGE = 'GET_READY_FOR_INCOME_PAGE';
const GET_READY_PAGE = 'GET_READY_PAGE';
const HELP_PAYING_FOR_COVERAGE_PAGE = 'HELP_PAYING_F0R_COVERAGE_PAGE';
const HOUSEHOLD_SUMMARY_PAGE = 'HOUSEHOLD_SUMMARY_PAGE';
const INCOME_SOURCES_PAGE = 'INCOME_SOURCES_PAGE';
const INCOME_INFORMATION_SUMMARY_PAGE = 'INCOME_INFORMATION_SUMMARY_PAGE';
const MARITAL_STATUS = 'MARITAL_STATUS';
const MEDICAID_CHIP_DENIAL_INFORMATION_PAGE = 'MEDICAID_CHIP_DENIAL_INFORMATION_PAGE';
const MORE_ABOUT_HOUSEHOLD_SUMMARY_PAGE = 'MORE_ABOUT_HOUSEHOLD_SUMMARY_PAGE';
const OTHER_ADDRESSES_PAGE = 'OTHER_ADDRESSES_PAGE';
const OTHER_HEALTH_COVERAGE_PAGE = 'OTHER_HEALTH_COVERAGE_PAGE';
const PARENT_CARETAKER_INFORMATION_PAGE = 'PARENT_CARETAKER_INFORMATION_PAGE';
const PERSONAL_INCOME_SUMMARY_PAGE = 'PERSONAL_INCOME_SUMMARY_PAGE';
const PERSONAL_INFORMATION_PAGE = 'PERSONAL_INFORMATION_PAGE';
const PREGNANCY_INFORMATION_PAGE = 'PREGNANCY_INFORMATION_PAGE';
const PRIMARY_CONTACT_INFORMATION_PAGE = 'PRIMARY_CONTACT_INFORMATION_PAGE';
const PRIMARY_TAX_FILER_PAGE = 'PRIMARY_TAX_FILER_PAGE';
const RECONCILIATION_IN_APTC_PAGE = 'RECONCILIATION_IN_APTC_PAGE';
const RELATIONSHIPS_PAGE = 'RELATIONSHIPS_PAGE';
const REVIEW_AND_SIGN_PAGE = 'REVIEW_AND_SIGN_PAGE';
const SIGN_AND_SUBMIT_PAGE = 'SIGN_AND_SUBMIT_PAGE';
const STATE_EMPLOYEE_HEALTH_BENEFIT_PAGE = 'STATE_EMPLOYEE_HEALTH_BENEFIT_PAGE';
const TAX_HOUSEHOLD_COMPOSITION_PAGE = 'TAX_HOUSEHOLD_COMPOSITION_PAGE';
const TRIBAL_SOURCE_INCOME_PAGE = 'TRIBAL_SOURCE_INCOME_PAGE';


export const PAGES = {
    ABOUT_YOUR_HOUSEHOLD_PAGE,
    ACTIVE_MILITARY_VETERAN_PAGE,
    ADDITIONAL_INFORMATION_SUMMARY_PAGE,
    ADDITIONAL_INFORMATION_PAGE,
    AMERICAN_INDIAN_ALASKA_NATIVE_PAGE,
    APPLICANTS_PAGE,
    APPLICATION_ASSISTANCE_PAGE,
    BEFORE_WE_BEGIN_PAGE,
    CITIZENSHIP_IMMIGRATION_STATUS_PAGE,
    DISABILITY_INFORMATION_PAGE,
    DEDUCTION_SOURCES_PAGE,
    EMPLOYER_COVERAGE_DETAIL_PAGE,
    ETHNICITY_AND_RACE_PAGE,
    EXPECTED_INCOME_PAGE,
    FOSTER_CARE_INFORMATION_PAGE,
    FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE,
    FINAL_APPLICATION_REVIEW_PAGE,
    FULL_TIME_STUDENT_PAGE,
    GET_READY_FOR_HOUSEHOLD_AND_FAMILY_PAGE,
    GET_READY_FOR_INCOME_PAGE,
    GET_READY_PAGE,
    INCOME_SOURCES_PAGE,
    INCOME_INFORMATION_SUMMARY_PAGE,
    HELP_PAYING_FOR_COVERAGE_PAGE,
    HOUSEHOLD_SUMMARY_PAGE,
    MARITAL_STATUS,
    MEDICAID_CHIP_DENIAL_INFORMATION_PAGE,
    MORE_ABOUT_HOUSEHOLD_SUMMARY_PAGE,
    OTHER_ADDRESSES_PAGE,
    OTHER_HEALTH_COVERAGE_PAGE,
    PARENT_CARETAKER_INFORMATION_PAGE,
    PREGNANCY_INFORMATION_PAGE,
    PERSONAL_INCOME_SUMMARY_PAGE,
    PERSONAL_INFORMATION_PAGE,
    PRIMARY_CONTACT_INFORMATION_PAGE,
    PRIMARY_TAX_FILER_PAGE,
    RECONCILIATION_IN_APTC_PAGE,
    RELATIONSHIPS_PAGE,
    REVIEW_AND_SIGN_PAGE,
    SIGN_AND_SUBMIT_PAGE,
    STATE_EMPLOYEE_HEALTH_BENEFIT_PAGE,
    TAX_HOUSEHOLD_COMPOSITION_PAGE,
    TRIBAL_SOURCE_INCOME_PAGE,
};




/**************************************************

 # Typical Buttons Schemas. Just to reduce code

 *************************************************/

const INITIAL_BUTTON_SCHEME = {
    leftButton: buttonsTypes.INITIAL_BTN,
    middleButton: buttonsTypes.INITIAL_BTN,
    rightButton: buttonsTypes.COMBINED_BTN
};

const DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME = {
    leftButton: buttonsTypes.DEFAULT_BTN,
    middleButton: buttonsTypes.INITIAL_BTN,
    rightButton: buttonsTypes.COMBINED_BTN
};

const DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME = {
    leftButton: buttonsTypes.DEFAULT_BTN,
    middleButton: buttonsTypes.INITIAL_BTN,
    rightButton: buttonsTypes.DEFAULT_BTN
};

const NON_FINANCIAL_FLOW_NON_SWITCHABLE = [
    {
        sectionName: 'start',
        text: 'Start Your Application',
        startPage: 0,
        arrIndex: null,
        subSections: [
            {
                pageName: BEFORE_WE_BEGIN_PAGE,
                navBarText: 'Before We Begin',
                component: <Page1_00/>,
                buttons: INITIAL_BUTTON_SCHEME
            },
            {
                pageName: GET_READY_PAGE,
                navBarText: 'Get Ready',
                component: <Page1_01/>,
                buttons: DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME
            },
            {
                pageName: PRIMARY_CONTACT_INFORMATION_PAGE,
                navBarText: 'Primary Contact Information',
                component: <Page1_02/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
            },
            {
                pageName: APPLICATION_ASSISTANCE_PAGE,
                navBarText: 'Help applying for coverage',
                component: <Page1_03/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
            },
            {
                pageName: APPLICANTS_PAGE,
                navBarText: 'Who needs coverage',
                component: <Page1_04/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
            },
            {
                pageName: ABOUT_YOUR_HOUSEHOLD_PAGE,
                navBarText: 'About Your Household',
                component: <Page1_05/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                    deleteButton: buttonsTypes.DEFAULT_BTN,
                    addPersonButton: buttonsTypes.DEFAULT_BTN,
                },
                extraButtons: {
                    addPersonButton: buttonsTypes.DEFAULT_BTN,
                }
            },
            {
                pageName: RELATIONSHIPS_PAGE,
                navBarText: 'Household Relationship',
                component: <Page2_13/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: OTHER_ADDRESSES_PAGE,
                navBarText: 'Household Addresses',
                component: <Page2_11_mod2/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: HOUSEHOLD_SUMMARY_PAGE,
                navBarText: 'Summary',
                component: <Page1_07_E2/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN,
                }
            }
        ]
    },
    {
        sectionName: 'family',
        text: 'Family and Household',
        startPage: 11,
        arrIndex: 1,
        subSections: [
            {
                pageName: GET_READY_FOR_HOUSEHOLD_AND_FAMILY_PAGE,
                navBarText: 'Get Ready',
                component:  <Page2_07/>,
                buttons: DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME
            },
            [
                {
                    pageName: PERSONAL_INFORMATION_PAGE,
                    navBarText: 'Personal Information',
                    component: <Page2_08/>,
                    buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                },
                {
                    pageName: CITIZENSHIP_IMMIGRATION_STATUS_PAGE,
                    navBarText: 'Citizenship/Immigration Status',
                    component: <Page2_09/>,
                    buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                },
                {
                    pageName: ETHNICITY_AND_RACE_PAGE,
                    navBarText: 'Ethnicity and Race',
                    component: <Page2_10/>,
                    buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                }
            ],
            {
                pageName: ACTIVE_MILITARY_VETERAN_PAGE,
                navBarText: 'Military Service',
                component: <Page2_25/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: AMERICAN_INDIAN_ALASKA_NATIVE_PAGE,
                navBarText: 'American Indian/Alaska Native',
                component: <Page2_12/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE,
                navBarText: 'Summary',
                component: <Page2_14/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN,
                }
            }
        ]
    },
    {
        sectionName: 'review',
        text: 'Review and Sign',
        startPage: 91,
        arrIndex: null,
        subSections: [
            {
                pageName: REVIEW_AND_SIGN_PAGE,
                navBarText: 'Review and Sign',
                component: <Page3_15/>,
                buttons: DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME
            },
            {
                pageName: FINAL_APPLICATION_REVIEW_PAGE,
                navBarText: 'Final Review',
                navBarTextCode: 'finalReview.title',
                component: <Page3_16/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN,
                    downloadButton: buttonsTypes.INITIAL_BTN,
                    printButton: buttonsTypes.INITIAL_BTN
                }
            },
            {
                pageName: SIGN_AND_SUBMIT_PAGE,
                navBarText: 'Sign and Submit',
                component: <Page3_17/>,
                buttons: {
                    leftButton: buttonsTypes.DEFAULT_BTN,
                    middleButton: buttonsTypes.INITIAL_BTN,
                    rightButton: buttonsTypes.SWITCHED_TO_SUBMIT_APPLICATION_BTN,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN
                }
            }
        ]
    }
];

const NON_FINANCIAL_FLOW_SWITCHABLE = [
    {
        sectionName: 'start',
        text: 'Start Your Application',
        startPage: 0,
        arrIndex: null,
        subSections: [
            {
                pageName: BEFORE_WE_BEGIN_PAGE,
                navBarText: 'Before We Begin',
                component: <Page1_00/>,
                buttons: INITIAL_BUTTON_SCHEME
            },
            {
                pageName: GET_READY_PAGE,
                navBarText: 'Get Ready',
                component: <Page1_01/>,
                buttons: DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME
            },
            {
                pageName: PRIMARY_CONTACT_INFORMATION_PAGE,
                navBarText: 'Primary Contact Information',
                component: <Page1_02/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
            },
            {
                pageName: APPLICATION_ASSISTANCE_PAGE,
                navBarText: 'Help Applying for Coverage',
                component: <Page1_03/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
            },
            {
                pageName: HELP_PAYING_FOR_COVERAGE_PAGE,
                navBarText: 'Help Paying for Coverage',
                component: <Page1_03_E1/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
            },
            {
                pageName: APPLICANTS_PAGE,
                navBarText: 'Who Needs Coverage',
                component: <Page1_04/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
            },
            {
                pageName: ABOUT_YOUR_HOUSEHOLD_PAGE,
                navBarText: 'About Your Household',
                component: <Page1_05/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                    deleteButton: buttonsTypes.DEFAULT_BTN,
                    addPersonButton: buttonsTypes.DEFAULT_BTN,
                },
                extraButtons: {
                    addPersonButton: buttonsTypes.DEFAULT_BTN,
                }
            },
            {
                pageName: RELATIONSHIPS_PAGE,
                navBarText: 'Household Relationships',
                component: <Page2_13/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: OTHER_ADDRESSES_PAGE,
                navBarText: 'Household Addresses',
                component: <Page2_11_mod2/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: HOUSEHOLD_SUMMARY_PAGE,
                navBarText: 'Summary',
                component: <Page1_07_E2/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN,
                }
            }
        ]
    },
    {
        sectionName: 'family',
        text: 'Family and Household',
        startPage: 11,
        arrIndex: 1,
        subSections: [
            {
                pageName: GET_READY_FOR_HOUSEHOLD_AND_FAMILY_PAGE,
                navBarText: 'Get Ready',
                component:  <Page2_07/>,
                buttons: DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME
            },
            [
                {
                    pageName: PERSONAL_INFORMATION_PAGE,
                    navBarText: 'Personal Information',
                    component: <Page2_08/>,
                    buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                },
                {
                    pageName: CITIZENSHIP_IMMIGRATION_STATUS_PAGE,
                    navBarText: 'Citizenship/Immigration Status',
                    component: <Page2_09/>,
                    buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                },
                {
                    pageName: ETHNICITY_AND_RACE_PAGE,
                    navBarText: 'Ethnicity and Race',
                    component: <Page2_10/>,
                    buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                }
            ],
            {
                pageName: ACTIVE_MILITARY_VETERAN_PAGE,
                navBarText: 'Military Service',
                component: <Page2_25/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: AMERICAN_INDIAN_ALASKA_NATIVE_PAGE,
                navBarText: 'American Indian/Alaska Native',
                component: <Page2_12/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE,
                navBarText: 'Summary',
                component: <Page2_14/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN,
                }
            }
        ],
    },
    {
        sectionName: 'review',
        text: 'Review and Sign',
        startPage: 91,
        arrIndex: null,
        subSections: [
            {
                pageName: REVIEW_AND_SIGN_PAGE,
                navBarText: 'Review and Sign',
                component: <Page3_15/>,
                buttons: DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME
            },
            {
                pageName: FINAL_APPLICATION_REVIEW_PAGE,
                navBarText: 'Final Review',
                navBarTextCode: 'finalReview.title',
                component: <Page3_16/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN,
                    downloadButton: buttonsTypes.INITIAL_BTN,
                    printButton: buttonsTypes.INITIAL_BTN
                }
            },
            {
                pageName: SIGN_AND_SUBMIT_PAGE,
                navBarText: 'Sign and Submit',
                component: <Page3_17/>,
                buttons: {
                    leftButton: buttonsTypes.DEFAULT_BTN,
                    middleButton: buttonsTypes.INITIAL_BTN,
                    rightButton: buttonsTypes.SWITCHED_TO_SUBMIT_APPLICATION_BTN,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN
                }
            }
        ]
    }
];

const [ START, , REVIEW ] = NON_FINANCIAL_FLOW_SWITCHABLE;

const FAMILY_FOR_FF = {
    sectionName: 'family',
    text: 'Family and Household',
    startPage: 11,
    arrIndex: 1,
    subSections: [
        {
            pageName: GET_READY_FOR_HOUSEHOLD_AND_FAMILY_PAGE,
            navBarText: 'Get Ready',
            component: <Page2_07/>,
            buttons: DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME
        },
        [
            {
                pageName: PERSONAL_INFORMATION_PAGE,
                navBarText: 'Personal Information',
                component: <Page2_08/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: CITIZENSHIP_IMMIGRATION_STATUS_PAGE,
                navBarText: 'Citizenship/Immigration Status',
                component: <Page2_09/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: ETHNICITY_AND_RACE_PAGE,
                navBarText: 'Ethnicity and Race',
                component: <Page2_10/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
            {
                pageName: MARITAL_STATUS,
                navBarText: 'Marital Status',
                component: <Page2_28_E3/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                    updateInformationButton: buttonsTypes.SPOUSE_BTN,
                    fixRelationshipButton: buttonsTypes.SPOUSE_BTN
                }
            },
            {
                pageName: PARENT_CARETAKER_INFORMATION_PAGE,
                navBarText: 'Parent / Caretaker Information',
                component: <Page2_26/>,
                buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
            },
        ],
        {
            pageName: ACTIVE_MILITARY_VETERAN_PAGE,
            navBarText: 'Military Service',
            component: <Page2_25/>,
            buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
        },
        {
            pageName: TAX_HOUSEHOLD_COMPOSITION_PAGE,
            navBarText: 'Household information',
            component: <Page2_07_E2_mod1/>,
            buttons: {
                ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                updateInformationButton: buttonsTypes.DEFAULT_BTN
            }
        },
        {
            pageName: AMERICAN_INDIAN_ALASKA_NATIVE_PAGE,
            navBarText: 'American Indian/Alaska Native',
            component: <Page2_12/>,
            buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
        },
        {
            pageName: MEDICAID_CHIP_DENIAL_INFORMATION_PAGE,
            navBarText: 'Medicaid/ CHIP Denial Information',
            component: <Page2_21/>,
            buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
        },
        {
            pageName: PREGNANCY_INFORMATION_PAGE,
            navBarText: 'Pregnancy Information',
            component: <Page2_22/>,
            buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
        },
        {
            pageName: DISABILITY_INFORMATION_PAGE,
            navBarText: 'Disability Information',
            component: <Page2_23/>,
            buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
        },
        {
            pageName: FOSTER_CARE_INFORMATION_PAGE,
            navBarText: 'Foster Care Information',
            component: <Page2_24/>,
            buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
        },
        {
            pageName: FULL_TIME_STUDENT_PAGE,
            navBarText: 'Full Time Student',
            component: <Page2_27/>,
            buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
        },
        {
            pageName: FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE,
            navBarText: 'Summary',
            component: <Page2_14/>,
            buttons: {
                ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                editButton_secondary: buttonsTypes.DEFAULT_BTN,
            }
        }
    ]
};

const FINANCIAL_FLOW = [
    START,
    FAMILY_FOR_FF,
    {
        sectionName: 'income',
        text: 'Income information',
        startPage: 31,
        arrIndex: null,
        subSections: [
            {
                pageName: GET_READY_FOR_INCOME_PAGE,
                navBarText: 'Get Ready',
                component: <Page3_31_E1/>,
                buttons: DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME
            },
            [
                {
                    pageName: INCOME_SOURCES_PAGE,
                    navBarText: 'Income Sources',
                    component: <Page3_32_E1/>,
                    buttons: {
                        ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                        addIncomeSourceButton: buttonsTypes.DEFAULT_BTN,
                        editButton_secondary: buttonsTypes.DEFAULT_BTN,
                        removeButton: buttonsTypes.DEFAULT_BTN,
                    }
                },
                {
                    pageName: TRIBAL_SOURCE_INCOME_PAGE,
                    navBarText: 'Tribal Income',
                    component: <Page3_33_E1/>,
                    buttons: {
                        ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
                    }
                },
                {
                    pageName: DEDUCTION_SOURCES_PAGE,
                    navBarText: 'Deduction Sources',
                    component: <Page3_34_E1/>,
                    buttons: {
                        ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                        addDeductionSourceButton: buttonsTypes.DEFAULT_BTN,
                        editButton_secondary: buttonsTypes.DEFAULT_BTN,
                        removeButton: buttonsTypes.DEFAULT_BTN,
                    }
                },
                {
                    pageName: EXPECTED_INCOME_PAGE,
                    navBarText: 'Expected Income',
                    component: <Page3_35_E1/>,
                    buttons: {
                        ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                        addDeductionSourceButton: buttonsTypes.DEFAULT_BTN,
                        editButton_secondary: buttonsTypes.DEFAULT_BTN,
                        removeButton: buttonsTypes.DEFAULT_BTN,
                    }
                },
                {
                    pageName: PERSONAL_INCOME_SUMMARY_PAGE,
                    navBarText: 'Summary',
                    component: <Page3_36_E1/>,
                    buttons: {
                        ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                        editButton_secondary: buttonsTypes.DEFAULT_BTN
                    }
                }
            ],
            {
                pageName: INCOME_INFORMATION_SUMMARY_PAGE,
                navBarText: 'Income Summary',
                component: <Page3_37_E1/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITH_CONTINUE_ONE_SCHEME,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN,
                }
            }
        ]
    },
    {
        sectionName: 'additional',
        text: 'Additional information',
        startPage: 61,
        arrIndex: null,
        subSections: [
            [
                {
                    pageName: OTHER_HEALTH_COVERAGE_PAGE,
                    navBarText: 'Other Health Coverage',
                    component: <Page4_61/>,
                    buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
                },
                {
                    pageName: RECONCILIATION_IN_APTC_PAGE,
                    navBarText: 'Reconciliation of APTC',
                    component: <Page4_62/>,
                    buttons: DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
                },
                {
                    pageName: EMPLOYER_COVERAGE_DETAIL_PAGE,
                    navBarText: 'Employer Coverage Detail',
                    component: <Page4_63/>,
                    buttons: {
                        ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                        addEmployerCoverageDetailButton: buttonsTypes.DEFAULT_BTN
                    }

                },
                {
                    pageName: STATE_EMPLOYEE_HEALTH_BENEFIT_PAGE,
                    navBarText: 'State Employee Health Benefit',
                    component: <Page4_65/>,
                    buttons: {
                        ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
                    }

                },
                {
                    pageName: ADDITIONAL_INFORMATION_PAGE,
                    navBarText: 'Additional Information',
                    component: <Page4_66/>,
                    buttons: {
                        ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME
                    }

                }
            ],
            {
                pageName: ADDITIONAL_INFORMATION_SUMMARY_PAGE,
                navBarText: 'Summary',
                component: <Page4_64/>,
                buttons: {
                    ...DEFAULT_BUTTONS_WITHOUT_MIDDLE_ONE_SCHEME,
                    editButton_secondary: buttonsTypes.DEFAULT_BTN
                }

            }
        ]
    },
    REVIEW
];

export const preparePagesForCurrentFlow = () => {

    const cachedFlows = new Map();

    return function(flowType, hhmsQty) {

        const key = `${flowType}${hhmsQty}`;

        if(cachedFlows.has(key)) {
            return cachedFlows.get(key);
        } else {
            let output = [];

            FLOWS[flowType].map((section, sectionIndex) => {

                let pageIndex = 0;

                section.subSections.forEach(subsection => {
                    if (Array.isArray(subsection)) {
                        Array.from({length: hhmsQty}).forEach((ent, idx) => {
                            subsection.forEach(sss => {
                                output = [
                                    ...output,
                                    {
                                        hhm: idx,
                                        isActive: true,
                                        pageName: sss.pageName,
                                        reasons: [],
                                        page: sss,
                                        pageIndex,
                                        sectionIndex,
                                        sectionName: section.text
                                    }
                                ];
                                pageIndex++;
                            });
                        });
                    } else {
                        output = [
                            ...output,
                            {
                                hhm: -1,
                                isActive: true,
                                pageName: subsection.pageName,
                                reasons: [],
                                page: subsection,
                                pageIndex,
                                sectionIndex,
                                sectionName: section.text,
                            }
                        ];
                        pageIndex++;
                    }

                });
            });
            cachedFlows.set(key, output);
            return output;
        }
    };
};

export const getCachedFlows = preparePagesForCurrentFlow();

export const FLOWS = {
    FINANCIAL_FLOW,
    NON_FINANCIAL_FLOW_NON_SWITCHABLE,
    NON_FINANCIAL_FLOW_SWITCHABLE
};
