package com.getinsured.hix.indportal.dto;

import java.util.List;

public class CustomGroupingResponse extends AppGroupResponse{

	private List<DisenrollDialogData> disenrollDialogDataList;
	
	
	public List<DisenrollDialogData> getDisenrollDialogDataList() {
		return disenrollDialogDataList;
	}

	public void setDisenrollDialogDataList(List<DisenrollDialogData> disenrollDialogDataList) {
		this.disenrollDialogDataList = disenrollDialogDataList;
	}

			
}
