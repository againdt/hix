package com.getinsured.hix.account.dto;

import java.io.Serializable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;

/**
 * The persistent class for the users database table.
 *
 */
public class SignUpUser implements Serializable {
	private static final long serialVersionUID = 1L;

	public interface SignUpUserData extends Default{
		
	}
	
	private String encRoleName;
	
	private Role signUpRole;
	
	private boolean isActivationFlow=false;
	
	private String activationId;
	
	@NotEmpty(message="{label.validateFirstName}",groups=SignUpUser.SignUpUserData.class)
	private String firstName;

	@NotEmpty(message="{label.validateLastName}",groups=SignUpUser.SignUpUserData.class)
	private String lastName;

	@NotEmpty(message="{label.validatePassword}",groups=SignUpUser.SignUpUserData.class)
	private String password;
	
	@NotEmpty(message="{label.validatePassword}",groups=SignUpUser.SignUpUserData.class)
	private String confirmPassword;

	@NotEmpty(message="{label.validateEmail}",groups=SignUpUser.SignUpUserData.class)
	@Email(message="{label.validatePleaseEnterValidEmail}",groups=SignUpUser.SignUpUserData.class)
	@Size(max=50,groups=SignUpUser.SignUpUserData.class)
	private String email;
	
	@NotEmpty(message="{label.validateEmail}",groups=SignUpUser.SignUpUserData.class)
	@Email(message="{label.validatePleaseEnterValidEmail}",groups=SignUpUser.SignUpUserData.class)
	@Size(max=50,groups=SignUpUser.SignUpUserData.class)
	private String confirmEmail;
	
	
	@Size(min=9,max=11,groups=SignUpUser.SignUpUserData.class)
	@Pattern(regexp="[0-9]*",message="{label.validateSSN}",groups=SignUpUser.SignUpUserData.class)
	private String ssn;

	@NotEmpty(message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	@Size(min=3,max=3,message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	@Pattern(regexp="[0-9]*",message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	private String phone1;
	
	@NotEmpty(message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	@Size(min=3,max=3,message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	@Pattern(regexp="[0-9]*",message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	private String phone2;
	
	@NotEmpty(message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	@Size(min=4,max=4,message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	@Pattern(regexp="[0-9]*",message="{label.validatePhoneNo}",groups=SignUpUser.SignUpUserData.class)
	private String phone3;
	
	private String securityQuestion1;

	
	private String securityAnswer1;

	private String securityQuestion2;

	private String securityAnswer2;

	private String securityQuestion3;

	private String securityAnswer3;

	
	private String[] securityQuestions  = new String[3];
	private String[] securityAnswers  = new String[3];
	
	private String terms;

	public SignUpUser() {
	}





	/**
	 * @return the encRoleName
	 */
	public String getEncRoleName() {
		return encRoleName;
	}





	/**
	 * @param encRoleName the encRoleName to set
	 */
	public void setEncRoleName(String encRoleName) {
		this.encRoleName = encRoleName;
	}



	/**
	 * @return the signUpRole
	 */
	public Role getSignUpRole() {
		return signUpRole;
	}





	/**
	 * @param signUpRole the signUpRole to set
	 */
	public void setSignUpRole(Role signUpRole) {
		this.signUpRole = signUpRole;
	}



	/**
	 * @return the isActivationFlow
	 */
	public boolean isActivationFlow() {
		return isActivationFlow;
	}


	/**
	 * @return the isActivationFlow
	 */
	public boolean getIsActivationFlow() {
		return isActivationFlow;
	}


	/**
	 * @param isActivationFlow the isActivationFlow to set
	 */
	public void setActivationFlow(boolean isActivationFlow) {
		this.isActivationFlow = isActivationFlow;
	}





	/**
	 * @return the activationId
	 */
	public String getActivationId() {
		return activationId;
	}





	/**
	 * @param activationId the activationId to set
	 */
	public void setActivationId(String activationId) {
		this.activationId = activationId;
	}





	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}





	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}





	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}





	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}





	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}





	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}





	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}





	/**
	 * @param confirmPassword the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}





	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}





	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}





	/**
	 * @return the confirmEmail
	 */
	public String getConfirmEmail() {
		return confirmEmail;
	}





	/**
	 * @param confirmEmail the confirmEmail to set
	 */
	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}





	/**
	 * @return the phone1
	 */
	public String getPhone1() {
		return phone1;
	}





	/**
	 * @param phone1 the phone1 to set
	 */
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}





	/**
	 * @return the phone2
	 */
	public String getPhone2() {
		return phone2;
	}





	/**
	 * @param phone2 the phone2 to set
	 */
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}





	/**
	 * @return the phone3
	 */
	public String getPhone3() {
		return phone3;
	}





	/**
	 * @param phone3 the phone3 to set
	 */
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}





	/**
	 * @return the securityQuestion1
	 */
	public String getSecurityQuestion1() {
		return securityQuestion1;
	}





	/**
	 * @param securityQuestion1 the securityQuestion1 to set
	 */
	public void setSecurityQuestion1(String securityQuestion1) {
		this.securityQuestion1 = securityQuestion1;
		this.securityQuestions[0]=securityQuestion1;
	}





	/**
	 * @return the securityAnswer1
	 */
	public String getSecurityAnswer1() {
		return securityAnswer1;
	}





	/**
	 * @param securityAnswer1 the securityAnswer1 to set
	 */
	public void setSecurityAnswer1(String securityAnswer1) {
		this.securityAnswer1 = securityAnswer1;
		this.securityAnswers[0]=securityAnswer1;
	}





	/**
	 * @return the securityQuestion2
	 */
	public String getSecurityQuestion2() {
		return securityQuestion2;
	}





	/**
	 * @param securityQuestion2 the securityQuestion2 to set
	 */
	public void setSecurityQuestion2(String securityQuestion2) {
		this.securityQuestion2 = securityQuestion2;
		this.securityQuestions[1]=securityQuestion2;
	}





	/**
	 * @return the securityAnswer2
	 */
	public String getSecurityAnswer2() {
		return securityAnswer2;
	}





	/**
	 * @param securityAnswer2 the securityAnswer2 to set
	 */
	public void setSecurityAnswer2(String securityAnswer2) {
		this.securityAnswer2 = securityAnswer2;
		this.securityAnswers[1]=securityAnswer2;
	}





	/**
	 * @return the securityQuestion3
	 */
	public String getSecurityQuestion3() {
		return securityQuestion3;
	}





	/**
	 * @param securityQuestion3 the securityQuestion3 to set
	 */
	public void setSecurityQuestion3(String securityQuestion3) {
		this.securityQuestion3 = securityQuestion3;
		this.securityQuestions[2]=securityQuestion3;
	}





	/**
	 * @return the securityAnswer3
	 */
	public String getSecurityAnswer3() {
		return securityAnswer3;
	}





	/**
	 * @param securityAnswer3 the securityAnswer3 to set
	 */
	public void setSecurityAnswer3(String securityAnswer3) {
		this.securityAnswer3 = securityAnswer3;
		this.securityAnswers[2]=securityAnswer3;
	}




	/**
	 * @return the securityQuestion
	 */
	public String[] getSecurityQuestions() {
		return (String[])securityQuestions.clone();
	}





	/**
	 * @param securityQuestion the securityQuestion to set
	 */
	public void setSecurityQuestions(String[] securityQuestions) {
		this.securityQuestions = securityQuestions;
	}





	/**
	 * @return the securityAnswer
	 */
	public String[] getSecurityAnswers() {
		return (String[])securityAnswers.clone();
	}





	/**
	 * @param securityAnswer the securityAnswer to set
	 */
	public void setSecurityAnswers(String[] securityAnswers) {
		this.securityAnswers = securityAnswers;
	}


	



	/**
	 * @return the terms
	 */
	public String getTerms() {
		return terms;
	}





	/**
	 * @param terms the terms to set
	 */
	public void setTerms(String terms) {
		this.terms = terms;
	}





	public AccountUser getAccountUser(){
		AccountUser accountUser = new AccountUser();
		
		accountUser.setFirstName(firstName);
		accountUser.setLastName(lastName);
		accountUser.setPassword(password);
		
		String phone = phone1+phone2+phone3;
		/*
		 * user below condition of the phone field in JSP is Single entry field
		if (phone != null) {
			phone = phone.replaceAll("-", "");
		}*/
		
		accountUser.setPhone(phone);
		accountUser.setEmail(email.toLowerCase());
		
		
		int noOfSecQtns = 3; //default no of questions max 3
		
		for(int idx=1;idx<=noOfSecQtns;idx++){
			  String currQstn = securityQuestions[idx-1];
			  String currAns = securityAnswers[idx-1];
			  if(StringUtils.isNotBlank(currQstn) && StringUtils.isNotBlank(currAns)){
				  accountUser.setSecurityQuestion(idx, currQstn);
				  accountUser.setSecurityAnswer(idx, currAns);
			  }
			 
		  }
		
		return accountUser;
		
		
	}





	public String getSsn() {
		return ssn;
	}





	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

}