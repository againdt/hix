package com.getinsured.web.rest;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.RolePermission;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.account.service.InboxMsgService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.UIConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.ssap.SsapConstants;
import com.getinsured.timeshift.sql.TSDate;
import com.getinsured.web.model.Asset;
import com.getinsured.web.model.AssetType;
import com.getinsured.web.model.Link;
import com.getinsured.web.model.LinkType;
import com.getinsured.web.model.Property;
import com.getinsured.web.model.PropertyType;
import com.getinsured.web.model.UserJson;
import com.getinsured.web.model.WebEnvironment;

/**
 * REST controller responsible for providing details about current environment.
 * Usually environment would be different based on the current state.
 *
 * TODO: Once structure and all things more or less finalized with respect to new React UI,
 *        rewrite all of this in Kotlin.
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
@RestController
public class EnvironmentController {
  private static final Logger log = LoggerFactory.getLogger(EnvironmentController.class);

  private final UserService userService;
  private final InboxMsgService inboxMsgService;

  @Autowired
  public EnvironmentController(final UserService userService, final InboxMsgService inboxMsgService) {
    this.userService = userService;
    this.inboxMsgService = inboxMsgService;
  }

  @RequestMapping(value = "/ui/user", method = RequestMethod.GET)
  public ResponseEntity<AccountUser> getAccountUser() {
    AccountUser user = null;

    try {
      user = userService.getLoggedInUser();

      if (user != null) {
        return ResponseEntity.ok(user);
      }
    } catch (InvalidUserException e) {
      log.error("Unable to get currently logged in user", e);
    }

    return null;
  }

  /**
   * This API will be called by the UI to extend user session.
   * <p>
   *   On the UI, we will be prompting user if they are still there
   *   when session is about to expire. If they acknowledge their presence,
   *   call to this API should update last accessed time for the session and
   *   thus extend it. If user doesn't acknowledge, we will log them out
   *   of the system.
   * </p>
   * @param request HTTP Servlet Request, auto-injected by the Spring Framework.
   * @return Response {@code Map} that contains information about current session.
   */
  @RequestMapping(value = "/ui/ping", method = RequestMethod.GET)
  public ResponseEntity<Map<String, Object>> ping(final HttpServletRequest request) {
    final HttpSession session = request.getSession(false);
    final Map<String, Object> ret = new HashMap<>();
    final long now = new Date().getTime();
    ret.put("creationTime", session.getCreationTime());
    ret.put("inactiveIntervalMin", session.getMaxInactiveInterval() / 60);
    ret.put("lastAccessedTime", session.getLastAccessedTime());
    ret.put("now", now);
    ret.put("nowTimeShifted", new TSDate(now).getTime());

    return ResponseEntity.ok(ret);
  }

  @RequestMapping(value = "/ui/environment", method = RequestMethod.GET)
  public ResponseEntity<WebEnvironment> getEnvironment(HttpServletRequest request) {
    WebEnvironment environment = new WebEnvironment();
    HttpSession session = request.getSession(false);

    String serverPath = getFullServerUrl(request);

    try {
      AccountUser user = userService.getLoggedInUser();

      if (user != null) {
        environment.setAnonymous(false);
        UserJson userJson = new UserJson(user);
        environment.setUser(userJson);
        addUnreadMessageCount(environment, user);
      }
    } catch (InvalidUserException e) {
      log.info("Unable to get currently logged in user, user is anonymous. {}", e.getMessage());
    }

    try {
      TenantDTO tenantDTO = TenantContextHolder.getTenant();

      if (tenantDTO != null) {
        environment.setTenantId(tenantDTO.getId());
      }
    } catch (Exception e) {
      log.info("Unable to get current tenant. {}", e.getMessage());
    }

    environment.setStateCode(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
    environment.setExchangeName(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

    final String oepStartDateConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
    final String oepEndDateConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
    final String coverageYearConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
    final String renewalCoverageYearConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_RENEWAL_COVERAGE_YEAR);
    final String previousOepStartDateConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_START_DATE);
    final String previousOepEndDateConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE);
    final String oepEnrollStartDateConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_START_DATE);
    final String oepEnrollEndDateConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_OE_ENROLL_END_DATE);

    if(!oepEndDateConfig.isEmpty()) {
      try {
        environment.setOepStartDate(DateUtils.parseDate(oepStartDateConfig, GhixConstants.REQUIRED_DATE_FORMAT));
      } catch (ParseException e) {
        log.error("Unable to parse configured OEP start date: {}, error: {}", oepStartDateConfig, e.getMessage());
      }
    }

    if(!oepEndDateConfig.isEmpty()) {
      try {
        environment.setOepEndDate(DateUtils.parseDate(oepEndDateConfig, GhixConstants.REQUIRED_DATE_FORMAT));
      } catch (ParseException e) {
        log.error("Unable to parse configured OEP end date: {}, error: {}", oepEndDateConfig, e.getMessage());
      }
    }

    if(!coverageYearConfig.isEmpty()) {
      try {
        environment.setCoverageYear(Integer.parseInt(coverageYearConfig));
      }
      catch(NumberFormatException nfe) {
        log.error("Unable to parse exchange coverage year: {}, error: {}", coverageYearConfig, nfe.getMessage());
      }
    }

    if(!renewalCoverageYearConfig.isEmpty()) {
      try {
        environment.setRenewalCoverageYear(Integer.parseInt(renewalCoverageYearConfig));
      } catch(NumberFormatException e) {
        log.error("Unable to parse renewal coverage year configuration date: {}, error: {}", renewalCoverageYearConfig, e.getMessage());
      }
    }

    if(!previousOepStartDateConfig.isEmpty()) {
      try {
        environment.setPreviousOepStartDate(DateUtils.parseDate(previousOepStartDateConfig, GhixConstants.REQUIRED_DATE_FORMAT));
      }
      catch(ParseException e) {
        log.error("Unable to parse previous OEP start date: {}, error: {}", previousOepStartDateConfig, e.getMessage());
      }
    }

    if(!previousOepEndDateConfig.isEmpty()) {
      try {
        environment.setPreviousOepEndDate(DateUtils.parseDate(previousOepEndDateConfig, GhixConstants.REQUIRED_DATE_FORMAT));
      }
      catch(ParseException e) {
        log.error("Unable to parse previous OEP end date: {}, error: {}", previousOepEndDateConfig, e.getMessage());
      }
    }

    if(!oepEnrollStartDateConfig.isEmpty()) {
      try {
        environment.setOepEnrollStartDate(DateUtils.parseDate(oepEnrollStartDateConfig, GhixConstants.REQUIRED_DATE_FORMAT));
      } catch (ParseException e) {
        log.error("Unable to parse configured OEP enrollment start date: {}, error: {}", oepEnrollStartDateConfig, e.getMessage());
      }
    }

    if(!oepEnrollEndDateConfig.isEmpty()) {
      try {
        environment.setOepEnrollEndDate(DateUtils.parseDate(oepEnrollEndDateConfig, GhixConstants.REQUIRED_DATE_FORMAT));
      } catch (ParseException e) {
        log.error("Unable to parse configured OEP enrollment end date: {}, error: {}", oepEnrollEndDateConfig, e.getMessage());
      }
    }

    if (session != null && session.getAttribute("lang") != null && !session.getAttribute("lang").equals("")) {
      environment.setLanguage(String.valueOf(session.getAttribute("lang")));
    } else {
      Locale locale = request.getLocale();

      if (locale.getLanguage().equals("en") || locale.getLanguage().equals("es")) {
        environment.setLanguage(locale.getLanguage());
      }
    }

    Map<AssetType, String> assets = new HashMap<>();

    // Styles
    Asset favIcon = new Asset();
    favIcon.setType(AssetType.FAV_ICON);
    favIcon.setValue(String.format("%s/resources/images/%s", serverPath, DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFAVICON)));
    assets.put(favIcon.getType(), favIcon.getValue());

    Asset logo = new Asset();
    logo.setType(AssetType.LOGO);
    logo.setValue(String.format("%s/resources/img/%s", serverPath, DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFILE)));
    assets.put(logo.getType(), logo.getValue());

    Asset logoMobile = new Asset();
    logoMobile.setType(AssetType.LOGO_MOBILE);

    // Unlike BRANDFILE that is taken from Application Configuration Admin screen, there is no
    // value for mobile logo, which was not used in non-react header/footer. For this, we will follow
    // naming conventions: logo_mobile_<state code>.png, e.g. logo_mobile_nv.png, logo_mobile_id.png, etc.
    String stateCode = environment.getStateCode();
    if (stateCode == null) {
      stateCode = "";
    } else {
      stateCode = stateCode.toLowerCase();
    }

    logoMobile.setValue(String.format("%s/resources/img/logo_mobile_%s.png", serverPath, stateCode));
    assets.put(logoMobile.getType(), logoMobile.getValue());

    environment.setAssets(assets);

    Map<LinkType, String> links = new HashMap<>();

    Link homeLink = new Link();
    homeLink.setType(LinkType.HOME);
    homeLink.setUrl(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));

    links.put(homeLink.getType(), homeLink.getUrl());

    Link helpLink = new Link();
    helpLink.setType(LinkType.HELP);

    Link assistanceLink = new Link();
    assistanceLink.setType(LinkType.ASSISTANCE);

    Link contactUsLink = new Link();
    contactUsLink.setType(LinkType.CONTACT_US);

    Link manageAssiterLink = new Link();
    manageAssiterLink.setType(LinkType.MANAGE_ASSISTER);

    switch (environment.getStateCode()) {
      case "ID":
        helpLink.setUrl("https://www.yourhealthidaho.org/enrollment-resources/browse-frequently-asked-questions");
        assistanceLink.setUrl("https://www.yourhealthidaho.org/find-help/");
        contactUsLink.setUrl("#");
        break;
      case "NV":
        helpLink.setUrl("https://help.nevadahealthlink.com/hc/en-us");
        assistanceLink.setUrl("#");
        contactUsLink.setUrl("https://help.nevadahealthlink.com/hc/en-us/articles/360029638331-Contact-Us");
        break;
      case "MN":
        helpLink.setUrl("https://www.mnsure.org/help/customer-service/index.jsp");
        manageAssiterLink.setUrl(GhixConstants.MNSURE_ENV);
        assistanceLink.setUrl("#");
        contactUsLink.setUrl("#");
        break;
      default:
        // TODO: Check PHIX flow for FAQ
        helpLink.setUrl("/hix/faq");
        assistanceLink.setUrl("#");
        contactUsLink.setUrl("#");
        break;
    }

    links.put(helpLink.getType(), helpLink.getUrl());
    links.put(manageAssiterLink.getType(), manageAssiterLink.getUrl());
    links.put(assistanceLink.getType(), assistanceLink.getUrl());
    links.put(contactUsLink.getType(), contactUsLink.getUrl());

    Link loginLink = new Link();
    loginLink.setType(LinkType.LOGIN);
    loginLink.setUrl(String.format("%s/account/user/login", serverPath));
    links.put(loginLink.getType(), loginLink.getUrl());

    Link logoutLink = new Link();
    logoutLink.setType(LinkType.LOGOUT);
    logoutLink.setUrl(String.format("%s/account/user/logout", serverPath));
    links.put(logoutLink.getType(), logoutLink.getUrl());

    Link dashboardLink = new Link();
    dashboardLink.setType(LinkType.DASHBOARD);
    dashboardLink.setUrl(String.format("%s/account/user/getDashBoard", serverPath));
    links.put(dashboardLink.getType(), dashboardLink.getUrl());

    Link settingsLink = new Link();
    settingsLink.setType(LinkType.SETTINGS);
    settingsLink.setUrl(String.format("%s/account/user/accountsettings", serverPath));
    links.put(settingsLink.getType(), settingsLink.getUrl());

    Link unreadMessagesLink = new Link();
    unreadMessagesLink.setType(LinkType.UNREAD_MESSAGES);
    //unreadMessagesLink.setUrl(GhixConstants.MY_INBOX_URL);
    unreadMessagesLink.setUrl(String.format("%s/inbox/home?languageCode=%s", serverPath, environment.getLanguage()));
    links.put(unreadMessagesLink.getType(), unreadMessagesLink.getUrl());

    Link phoneLink = new Link();
    phoneLink.setType(LinkType.PHONE);
    phoneLink.setUrl(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
    links.put(phoneLink.getType(), phoneLink.getUrl());

    Link chatLink = new Link();
    chatLink.setType(LinkType.CHAT);
    // languageCode stuff currently in masthead for CA, but should not hurt for all.
    if (GhixConstants.CHAT_URL != null) {
      chatLink.setUrl(String.format("%s/%s?languageCode=%s", serverPath, GhixConstants.CHAT_URL, environment.getLanguage()));
    }

    links.put(chatLink.getType(), chatLink.getUrl());

    Link privacyPolicy = new Link();
    privacyPolicy.setType(LinkType.PRIVACY_POLICY);
    privacyPolicy.setUrl(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL));
    links.put(privacyPolicy.getType(), privacyPolicy.getUrl());

    Link calHeersLink = new Link();
    calHeersLink.setType(LinkType.CALHEERS_URL);
    calHeersLink.setUrl(GhixConstants.CALHEERS_ENV);
    links.put(calHeersLink.getType(), calHeersLink.getUrl());

    environment.setDevEnvironment(GhixConstants.GHIX_ENVIRONMENT.equals("DEV"));
    if (GhixConstants.GHIX_ENVIRONMENT.equals("DEV")) {
      // TODO: Encrypt next two properties and change links
      // /account/signup/<broker|assister encrypted string>
      String brokerRoleName = "BROKER";
      String assisterRoleName = "ASSISTER";

      Link signupBrokerLink = new Link();
      signupBrokerLink.setType(LinkType.AGENT_BROKER_SIGNUP);
      signupBrokerLink.setUrl(String.format("%s/account/signup/?name=%s", serverPath, brokerRoleName));
      links.put(signupBrokerLink.getType(), signupBrokerLink.getUrl());

      Link signupAssisterLink = new Link();
      signupAssisterLink.setType(LinkType.ENTITY_SIGNUP);
      signupAssisterLink.setUrl(String.format("%s/account/signup/?name=%s", serverPath, assisterRoleName));

      links.put(signupAssisterLink.getType(), signupAssisterLink.getUrl());
    }

    environment.setLinks(links);

    Map<PropertyType, String> props = new HashMap<>();

    Property gaCode = new Property();
    gaCode.setType(PropertyType.GOOGLE_ANALYTICS_CODE);
    gaCode.setValue(GhixConstants.GOOGLE_ANALYTICS_CODE);
    props.put(gaCode.getType(), gaCode.getValue());

    Property gaContainerId = new Property();
    gaContainerId.setType(PropertyType.GOOGLE_ANALYTICS_CONTAINER_ID);
    gaContainerId.setValue(GhixPlatformConstants.GTM_CONTAINER_ID);
    props.put(gaContainerId.getType(), gaContainerId.getValue());

    environment.setProperties(props);

    addImpersonationDetails(environment, request);

    return ResponseEntity.ok(environment);
  }

  /**
   * Sets number of unread messages.
   * @param environment {@link WebEnvironment} object.
   * @param user {@link AccountUser} object.
   */
  private void addUnreadMessageCount(final WebEnvironment environment, final AccountUser user) {
    Long unreadMessages = 0L;

    if(user.getActiveModuleId() != 0 && user.getActiveModuleName() != null && !user.getActiveModuleName().equals("")) {
      unreadMessages = inboxMsgService.countUnreadMessagesByModule(user.getActiveModuleId(), user.getActiveModuleName());
    }
    else{
      unreadMessages = inboxMsgService.countUnreadMessagesByOwner(user.getId());
    }

    environment.setUnreadMessageCount(unreadMessages);
  }

  /**
   * Checks to see if we are impersonating another user and adding details of it.
   * <p>
   * Admins, CSR's and other power-users can impersonate another users to
   * act on behalf ot them, if that's the case, we need to provide details to the
   * callee that this is taking place, so that they can execute appropriate logic.
   * </p>
   *
   * @param environment current {@link WebEnvironment} object.
   * @param request     {@link HttpServletRequest} that will contain a {@link HttpSession} in which
   *                    details are stored.
   */
  private void addImpersonationDetails(final WebEnvironment environment, final HttpServletRequest request) {
    final HttpSession session = request.getSession(false);

    if ("Y".equals(session.getAttribute("isUserSwitchToOtherView")) ||
        "Y".equals(session.getAttribute("isBrokerSwitchToEmplView")) ||
        "Y".equals(session.getAttribute("isBrokerSwitchToIndvView"))) {

      Map<String, Object> details = new HashMap<>();
      details.put("name", session.getAttribute("switchToResourceName"));

      final String userSessionRole = String.valueOf(session.getAttribute("userActiveRoleName")).toUpperCase();
      details.put("role", userSessionRole);

      if(session.getAttribute("switchToModuleId") != null) {
        try {
          details.put("householdId", Long.valueOf((String) session.getAttribute("switchToModuleId")));
        }
        catch(Exception e) {
          log.error("Unable to parse household id from switchToModuleId: {}, error: {}", session.getAttribute("switchToModuleId"), e.getMessage());
          details.put("householdId", session.getAttribute("switchToModuleId"));
        }
      }

      String serverPath = getFullServerUrl(request);
      if (details.get("role") != null) {
        details.put("switchBackLink", String.format("%s/account/user/switchNonDefRole/%s", serverPath, details.get("role")));
      } else {
        details.put("switchBackLink", String.format("%s%s", serverPath, session.getAttribute("userLandingPage")));
      }

      boolean edit = false;

      if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
        AccountUser user = null;

        Object userDetails = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (userDetails instanceof AccountUser) {
          user = (AccountUser) userDetails;
        } else if (userDetails instanceof UserDetails) {
          UserDetails ud = (UserDetails) userDetails;
          user = userService.findByUserName(ud.getUsername());
        }

        if(user != null) {
          final String currentUserRole = user.getCurrentUserRole();
          Optional<UserRole> roleOptional = user.getUserRole().stream().filter(userRole -> userRole.getRole().getName().equals(currentUserRole)).findFirst();

          if(roleOptional.isPresent()) {
            UserRole userRole = roleOptional.get();
            Role currRole=userRole.getRole();
            Set<RolePermission> userRolePermissions= currRole.getRolePermissions();
            for(RolePermission rolePermission : userRolePermissions) {
              switch(rolePermission.getPermission().getName()) {
                case SsapConstants.SSAP_EDIT:
                case "IND_PORTAL_EDIT_APP":
                case "IND_PORTAL_RESUME_APP":
                //case "CRM_VIEW_MEMBER":
                  edit = true;
                  break;
                default:
                  break;
              }

              log.debug("[{}] {} => {} [{}]",
                  rolePermission.getPermission().getId(),
                  rolePermission.getPermission().getName(),
                  rolePermission.getPermission().getDescription(),
                  rolePermission.getPermission().getIsDefault()
              );
            }
          }
        }
      }

      details.put("updateAllowed", edit);

      environment.setImpersonationDetails(details);
      environment.setImpersonation(true);
    }
    else if(session.getAttribute(SsapConstants.CSR_OVER_RIDE) != null &&
        session.getAttribute(SsapConstants.CSR_OVER_RIDE) instanceof Boolean)
    {
      boolean csrOverride = (boolean) session.getAttribute(SsapConstants.CSR_OVER_RIDE);

      Object userDetails = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
      AccountUser user = null;

      if (userDetails instanceof AccountUser) {
        user = (AccountUser) userDetails;
      } else if (userDetails instanceof UserDetails) {
        UserDetails ud = (UserDetails) userDetails;
        user = userService.findByUserName(ud.getUsername());
      }

      if(user != null)
      {
        final Map<String, Object> details = new HashMap<>();
        final String currentUserRole = user.getCurrentUserRole();

        details.put("role", currentUserRole);
        environment.setImpersonationDetails(details);
      }

      environment.setImpersonation(true);
    }
  }

  /**
   * Gets server's domain from request URL.
   *
   * @param request {@link HttpServletRequest} object from which request URL will be taken.
   * @return server base url (e.g. https://server.domain.com)
   */
  private String getFullServerUrl(HttpServletRequest request) {

    if (request.getParameter("server") == null || request.getParameter("server").equals("0")) {
      return "/hix";
    }

    final HttpRequest httpRequest = new ServletServerHttpRequest(request);
    final UriComponents uriComponents = UriComponentsBuilder.fromHttpRequest(httpRequest).build();

    String scheme = uriComponents.getScheme();     // http / https
    String serverName = request.getServerName();   // hostname.com
    int serverPort = request.getServerPort();      // 80
    String contextPath = request.getContextPath(); // /app

    // Reconstruct original requesting URL
    StringBuilder url = new StringBuilder();
    url.append(scheme).append("://");
    url.append(serverName);

    if (serverPort != 80 && serverPort != 443) {
      url.append(":").append(serverPort);
    }
    url.append(contextPath);
    return url.toString();
  }

}
