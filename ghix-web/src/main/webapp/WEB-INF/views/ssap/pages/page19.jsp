<%@ include file="../include.jsp" %>

<div id="appscr69" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<%-- <spring:message code="ssap.page19.incomeReportedByIRS" text="Income Reported by IRS" /> --%>
				<spring:message code="ssap.page19.ExpectedIncomeIn" text="Expected Income in" />&nbsp;<span class='coverageYear'></span>
				 
			</h4>
		</div>
		<form id="appscr69Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.pageHeader.income" text="Income" /></h4>
			</div>
			<div class="gutter10">
			<!-- <div class="control-group">
				<p>
					<spring:message code="ssap.page19.accordingToFederalIncome" text="According to federal income tax return data" />, 
					<strong class="nameOfHouseHold"></strong>'s
					<spring:message code="ssap.page19.incomeWas" text="income was" />&nbsp;
					<span class="irsIncome2013" id="irsIncomeLabel" onmouseover="">[<spring:message code="ssap.page19.amountFromIRS" text="amount from IRS" />]</span>
					<spring:message code="ssap.page19.in" text="in" />
					<span class="" onmouseover="">2013</span>.
				</p>
				<div id="demo2_tip" class="hide">
	               <b><spring:message code="ssap.page19.irsIncome" text="IRS Income" /></b><hr/>
	               
	               
	               <table>
	               <tr>
	               <td> <input type='checkbox' id='noIrsData' onChange='noIrsData();' />
	               </td>
	               <td> <b><spring:message code="ssap.page19.noIRSData" text="No IRS Data" /> ? </b>
	               </td>
	               </tr>
	               </table>
	               <div class="Edit_button_style_Ui">
							<a class='noMargin' href="javascript:irsIncomePrevYear();"><spring:message code="ssap.page19.okay" text="Okay" /></a>
					</div>
	               <input type='hidden' id='irsIncome' value='0' />
	            </div>
			</div> --><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page19.whatDoYouExpect" text="What do you expect" />
					<strong class="nameOfHouseHold"></strong>'s
					<spring:message code="ssap.page19.yearlyIncomeWillBe" text="yearly income will be in" />
					<span class='coverageYear'></span>?&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p>
				<div class="margin5-l">
					<label for="expeditedIncome69">
						$&nbsp;
						<input id="expeditedIncome69" name="expeditedIncome69" type="text" placeholder="Dollar Amount" class="input-medium currency" required onBlur="irsIncome2014(this);" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>"/>
					</label>
					<span class="errorMessage"></span>
				</div>
			</div><!-- control-group ends -->
			
			<%-- <div class="control-group hide">
				<p>
					 Is
					 <strong class="nameOfHouseHold"></strong>&apos;s&nbsp;
					 <spring:message code="ssap.page19.incomeBeforeTaxesFor" text="income (before taxes) for this month greater than" /> $<span id="beforeTaxesIncome"></span> ?
				</p>
				<div class="margin5-l">
					<label>
						<input type="radio" value="yes" id="appscr69radios2Yes" name="appscr69radios2"/>
						<spring:message code="ssap.yes" text="Yes" />
					</label>
					<label>
						<input type="radio" value="no" checked="checked" id="appscr69radios2No"  name="appscr69radios2"/>
						<spring:message code="ssap.no" text="No" />
					</label>
				</div>
			</div><!-- control-group ends -->
			
			<div class="hide">
				<input type="checkbox" id="appscr69CBdontKnow" /> 
				<span><spring:message code="ssap.iDontKnow" text="I don't know" /></span>                    
			</div> --%>
			</div>
		</form>
	</div> 
</div>