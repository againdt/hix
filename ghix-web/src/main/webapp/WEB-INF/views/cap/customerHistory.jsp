<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script src="<c:url value="/resources/js/angular.js"/>"></script>
<script type="text/javascript">
	var CapString={};
	CapString.Today="Today";
	CapString.LastWeek="Last Week";
	CapString.LastMonth="Last Month";
	CapString.LastYear="Last Year";
</script>
<div ng-app="customerHistoryApp">
 	<div ng-controller="mainController">
		<div class="gutter10"  id="consumerHistory">
		    <div class="row-fluid">
		        <h1 id="skip">Consumer History</h1>
		    </div>
		    <div class="row-fluid">
		        <div id="sidebar" class="span3">
					<div class="header">
						<h4><spring:message  code="label.aboutConsumer"/></h4>
						</div>
						<ul class="nav nav-list">
						    <c:set var="encHouseHoldId" ><encryptor:enc value="${householdId}" isurl="true"/> </c:set>
							<li><a href="/hix/crm/member/viewmember/${encHouseHoldId}"><spring:message code="label.basicInformation"/></a></li>
							<li><a href="<c:url value="/crm/member/enrollment/" />${encHouseHoldId}">Enrollments</a></li>
							<li><a href="/hix/crm/member/comments/${encHouseHoldId}"><spring:message code="label.brkComments"/></a></li>	
							<li class="active">History</li>
							<li><a href="/hix/crm/member/appealhistory/${encHouseHoldId}"><spring:message code="label.brkAppealHistory"/></a></li>
							<li><a href="/hix/crm/member/tickethistory/${encHouseHoldId}"><spring:message code="label.brkTicketHistory"/></a></li>	
							<li><a href="<c:url value="/crm/consumer/securityquestions/" />${encHouseHoldId}"><spring:message code="label.brkSecurityQuestions"/></a></li>	
						</ul>
						<br>
						<div class="header"><i class="icon-cog icon-white"></i> Actions</div>
			            <ul class="nav nav-list">
                <c:if test="${checkDilog == null}">
                    <li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Member Account</a></li>
                </c:if>
                <c:if test="${checkDilog != null}">
                    <!-- TODO : From where to pull the household.name in ahref ??? -->
                    <jsp:useBean id="paramMap" class="java.util.HashMap" scope="request"/>
                    <c:set target="${paramMap}" property="switchToModuleName" value="individual"/>
                    <c:set var="idAsString">${householdId}</c:set>
                    <c:set target="${paramMap}" property="switchToModuleId" value="${idAsString}"/>
                    <c:set target="${paramMap}" property="switchToResourceName" value="${household.firstName} ${household.lastName}"/>    
                    <surl:secureUrl paramMap="${paramMap}" var="encryptedParamMap" />
                    <li class="navmenu"><a href="/hix/account/user/switchUserRole?ref=${encryptedParamMap}"      role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Member Account</a></li>
                </c:if>
                <li class="navmenu"><a href="<c:url value="/ticketmgmt/ticket/createpage?prefillUser=true&roleName=INDIVIDUAL&moduleId=${householdId}&requestorId=${household.user.id}&requestorName=${household.firstName} ${household.lastName}&moduleName=HOUSEHOLD"/>"  id="addNewTicket" name="addNewTicket" >Create Ticket</a></li>
                <c:if test="${household.user == null}">
                	 <li class="navmenu"><a href="#confirmActivationDlg" role="button"  data-toggle="modal" ><i class="icon-envelope-alt"></i>Send Account Activation Email </a></li>
                </c:if>
            </ul>  
				</div>
				<div class="span9" id="rightpanel">
			    	<div class="row-fluid">
			            <div class="header margin20-b">
			                <h4>Consumer History</h4>
			            </div>
	            		<div class="gutter10">
			            
			            	<div ng-include src="'<c:url value="/resources/html/cap/partials/customerHistory.html "/>'"> </div>
			            	
			            </div>
			        </div>
			   	</div>
			</div>
		</div>
		
      	
      	
 	</div>
 </div>

<!-- gutter10 -->
<!--  Latest UI -->
<!-- Modal 
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employer/home/${employerId}" />" novalidate="novalidate">
<form name="dialogForm" id="dialogForm" action="<c:url value="/account/user/switchUserRole/employer/${employerId}" />" novalidate="novalidate">-->
<form name="dialogForm" id="dialogForm" method="POST"  action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Member Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body clearfix gutter10-lr">
    <div class="control-group">	
				<div class="controls">
                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
                Through this portal you will be able to take actions on behalf of the member.<br/>
                Proceed to Member view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Member View</button>
        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="individual"/>" />
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value='<encryptor:enc value="${householdId}"/>' />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.firstName} ${household.lastName}" />
    <!--a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
  		<input type="hidden" name="householdId" id="householdId" value='<encryptor:enc value="${householdId}"/>'  />

<script src="<c:url value="/resources/js/angular.js"/>"></script>
<script src="<c:url value="/resources/js/cap/customerHistory.js"/>"></script> 
<script src="<c:url value="/resources/js/parsley.2.0.js" />"></script>