
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Agency3InitVerifResponseSetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Agency3InitVerifResponseSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}Agency3InitVerifIndividualResponse" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Agency3InitVerifResponseSetType", propOrder = {
    "agency3InitVerifIndividualResponse"
})
public class Agency3InitVerifResponseSetType {

    @XmlElement(name = "Agency3InitVerifIndividualResponse", required = true)
    protected List<Agency3InitVerifIndividualResponseType> agency3InitVerifIndividualResponse;

    /**
     * Gets the value of the agency3InitVerifIndividualResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agency3InitVerifIndividualResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgency3InitVerifIndividualResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Agency3InitVerifIndividualResponseType }
     * 
     * 
     */
    public List<Agency3InitVerifIndividualResponseType> getAgency3InitVerifIndividualResponse() {
        if (agency3InitVerifIndividualResponse == null) {
            agency3InitVerifIndividualResponse = new ArrayList<Agency3InitVerifIndividualResponseType>();
        }
        return this.agency3InitVerifIndividualResponse;
    }

}
