(function() {
'use strict';

angular.module('agencyInformationCtrlModule', ['translateApp'])
.controller('AgencyInformationCtrl', AgencyInformationCtrl);

AgencyInformationCtrl.$inject = ['$http', '$stateParams', '$scope', '$state', 'dataStorageService', '$translate', 'StatusService'];

    function AgencyInformationCtrl($http, $stateParams, $scope, $state, dataStorageService, $translate, StatusService) {
        var vm = this;

        vm.init = init;
        vm.saveAgencyInformation = saveAgencyInformation;
        vm.editAgencyInformation = editAgencyInformation;
        vm.cancelAgencyInformation = cancelAgencyInformation;
        vm.chkInputLength = chkInputLength;
        vm.resetDuplicateFederalTaxIdFlag = resetDuplicateFederalTaxIdFlag;
        vm.currentUserRole = StatusService.CurrentUserRole;
        vm.showAgencyManagerInfo = false;

        function init(){
            vm.editMode = false;

            vm.duplicateFederalTaxIdFlag = false;

            vm.flowFlag = {};

            updateFlowFlag();

            if(!vm.agencyId && vm.flowFlag.adminFlow){
                dataStorageService.setItem('encryptedAgencyId', $stateParams.agencyId);
                vm.agencyId = dataStorageService.getItem('encryptedAgencyId');
            }

            if(!vm.flowFlag.adminFlow){
                $scope.$parent.vm.sideNavListOne[1].showOKicon = false;
                $scope.$parent.vm.sideNavListOne[2].showOKicon = false;
                $scope.$parent.vm.sideNavListOne[3].showOKicon = false;
                $scope.$parent.vm.sideNavListOne[4].showOKicon = false;
                $scope.$parent.vm.sideNavListOne[5].showOKicon = false;

                $scope.$parent.vm.sideNavListOne[1].disableLink = true;
                $scope.$parent.vm.sideNavListOne[2].disableLink = true;
                $scope.$parent.vm.sideNavListOne[3].disableLink = true;
                $scope.$parent.vm.sideNavListOne[4].disableLink = true;
                $scope.$parent.vm.sideNavListOne[5].disableLink = true;
                vm.agencyId =  $("#agencyId").val() || dataStorageService.getItem('agencyId');
            }

            if (vm.agencyId) {
                var promise = $http.get('/hix/agency/information/view/' + vm.agencyId);

                promise.success(function(response){
                    vm.loader = false;
                    vm.agencyInformationData = response;
    
                    vm.agencyInformationData.federalTaxId = chkInputLength(vm.agencyInformationData.federalTaxId, 9);
                    dataStorageService.setItem('agencyName',  vm.agencyInformationData.agencyName);
    
                    if(response.agencyManagersInfoDto !== null && typeof(response.agencyManagersInfoDto) !== 'undefined'){
                        vm.agencyManagersInfo = response.agencyManagersInfoDto;
                        vm.showAgencyManagerInfo = true;
                    }
                    updateFlowFlag();
                })
                promise.error(function(response){
                    vm.loader = false;
                })
            } else {
                vm.loader = false;
                vm.agencyInformationData = {
                    agencyName: '',
                    businessLegalName: '',
                    federalTaxId: '',
                    licenseNumber: ''
                }
                updateFlowFlag();
                vm.flowFlag.adminFlow = true;
            }
        }


        function saveAgencyInformation(){
            vm.loader = true;
            vm.duplicateFederalTaxIdFlag = false;
            var data = {
                id: vm.agencyId,
                agencyName: vm.agencyInformationData.agencyName,
                businessLegalName: vm.agencyInformationData.businessLegalName,
                federalTaxId: vm.agencyInformationData.federalTaxId,
                licenseNumber:vm.agencyInformationData.licenseNumber
            };

            var promise = $http.post('/hix/agency/information', data);

            promise.success(function(response){
                vm.loader = false;
                chkInputLength(vm.agencyInformationData.federalTaxId, 9);
                if(response.statusCode === '200' && vm.flowFlag.agencyResgisterFlow){
                    $state.go ("accsetup.locationHour");
                    dataStorageService.setItem('agencyId', response.id);
                }else if(response.statusCode === '200'){
                    vm.editMode = false;
                    dataStorageService.setItem('agencyId', response.id);
                }
                else if (response.statusCode === '500'){
                    vm.duplicateFederalTaxIdFlag = true;
                }

            })


            promise.error(function(response){
                vm.loader = false;
                vm.duplicateFederalTaxIdFlag = false;
            })
        }


        function updateFlowFlag(){

            //for admin flow
            var isAdminRole = $("#isAdminRole").val();

            //for agency flow
            var certificationStatus = $('#agencyCertificationStatus').val();


            if(isAdminRole === 'true'){
                vm.flowFlag.adminFlow = true;
            }else{

                if(certificationStatus === '' || certificationStatus === 'INCOMPLETE'){
                    vm.flowFlag.agencyResgisterFlow = true;
                    vm.editMode = true;
                }else if(certificationStatus === 'CERTIFIED'){
                    vm.flowFlag.agencyViewEditFlow = true;
                }else{
                    vm.flowFlag.agencyViewOnlyFlow = true;
                }
            }
        }


        function editAgencyInformation(){
            vm.editMode = !vm.editMode;
            vm.agencyInformationDataCopy = angular.copy(vm.agencyInformationData);
        }

        function cancelAgencyInformation(){
            vm.editMode = !vm.editMode;
            vm.duplicateFederalTaxIdFlag = false;
            vm.agencyInformationData = angular.copy(vm.agencyInformationDataCopy);
        }

        function chkInputLength(inputVal, inputLength) {
            var newValue = '000000000' + inputVal;
            vm.agencyInformationData.federalTaxId = newValue.substr(newValue.length-inputLength);
            return vm.agencyInformationData.federalTaxId;
        }

        function resetDuplicateFederalTaxIdFlag() {
            vm.duplicateFederalTaxIdFlag = false;
        }

    }

})();
