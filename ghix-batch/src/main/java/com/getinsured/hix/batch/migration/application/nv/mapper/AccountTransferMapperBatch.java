package com.getinsured.hix.batch.migration.application.nv.mapper;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.indportal.dto.dm.application.ApplicationDTO;
import com.getinsured.hix.indportal.dto.dm.application.ApplicationJSONDTO;
import com.getinsured.hix.indportal.dto.dm.application.AttestationsMember;
import com.getinsured.hix.indportal.dto.dm.application.Member;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.TransferActivityCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.TransferActivityCodeType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.TransferActivityType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.TransferHeaderType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.InformationExchangeSystemType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicationAssisterAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.PhysicalHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.EmploymentStatusCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FamilyRelationshipHIPAACodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ReferenceType;


@Component(value="accountTransferMapperBatch")
public class AccountTransferMapperBatch {
	
	@Autowired	private VerificationMetadataWrapperBatch verificationMetadataWrapper;
	@Autowired private InsuranceApplicationMapperBatch insuranceApplicationMapper;
	@Autowired private PersonTypeMapperBatch personTypeMapper;
	
	private static Logger lOGGER = Logger.getLogger(AccountTransferMapperBatch.class);
	private static final int TRANFER_ID_LENGTH = 17;//because states schematron expects only 20 characters.
	private int coverageSeekingCount = 0;
	
	public static final Map<String,String> relationships;
	public static final Map<String,String> reverseRelationships;
	
	public static final Map<String,EmploymentStatusCodeSimpleType> employeeStatus;
	static {
		relationships = new HashMap<String, String>();
		relationships.put("SPOUSE","01");
		relationships.put("PARENT","03");
		relationships.put("SON_DAUGHTER","19");
		relationships.put("STEPSON_STEPDAUGHTER","17");
		relationships.put("GRANDCHILD","05");
		relationships.put("SIBLING","14");
		relationships.put("DOMESTIC_PARTNER","53");
		relationships.put("CHILD_OF_DOMESTIC_PARTNER","53-19");
		relationships.put("UNRELATED","G8");
		relationships.put("OTHER_RELATIVE","G9");
		relationships.put("STEP_PARENT","16");
		relationships.put("AUNT_UNCLE","06");
		relationships.put("NEPHEW_NIECE","07");
		relationships.put("GRANDPARENT","04");
		relationships.put("FIRST_COUSIN","08");
		relationships.put("PARENTS_DOMESTIC_PARTNER","03-53");
		relationships.put("BROTHER_IN_LAW_SISTER_IN_LAW","12");
		relationships.put("DAUGHTER_IN_LAW_SON_IN_LAW","11");
		relationships.put("MOTHER_IN_LAW_FATHER_IN_LAW","13");
		relationships.put("SELF","18");
		//relationships.put("PARENTS_DOMESTIC_PARTNER","18");
		/*relationships.put("FOSTER_CHILD","10");
		relationships.put("ADOPTED_CHILD","09");
		relationships.put("COURT_APPOINTED_GUARDIAN","31");
		relationships.put("FORMER_SPOUSE","25");
		relationships.put("GUARDIAN","26");
		relationships.put("ANNUITANT","60");
		relationships.put("TRUSTEE","D2");
		relationships.put("WARD","15");
		relationships.put("OTHER","G8");
		relationships.put("SPONSORED_DEPENDENT","23");
		relationships.put("DEPENDENT_OF_A_MINOR_DEPENDENT","24");
		relationships.put("COLLATERAL_DEPENDENT","38");*/
		
		reverseRelationships = new HashMap<String, String>();
		reverseRelationships.put("SPOUSE","01");
		reverseRelationships.put("PARENT","19");
		reverseRelationships.put("SON_DAUGHTER","03");
		reverseRelationships.put("STEPSON_STEPDAUGHTER","16");
		reverseRelationships.put("GRANDCHILD","04");
		reverseRelationships.put("SIBLING","14");
		reverseRelationships.put("DOMESTIC_PARTNER","53");
		reverseRelationships.put("CHILD_OF_DOMESTIC_PARTNER","03-53");
		reverseRelationships.put("UNRELATED","G8");
		reverseRelationships.put("OTHER_RELATIVE","G9");
		reverseRelationships.put("PARENTS_DOMESTIC_PARTNER","53-19");
		reverseRelationships.put("BROTHER_IN_LAW_SISTER_IN_LAW","12");
		reverseRelationships.put("DAUGHTER_IN_LAW_SON_IN_LAW","13");
		reverseRelationships.put("MOTHER_IN_LAW_FATHER_IN_LAW","11");
		reverseRelationships.put("STEP_PARENT","17");
		reverseRelationships.put("AUNT_UNCLE","07");
		reverseRelationships.put("GRANDPARENT","05");
		reverseRelationships.put("FIRST_COUSIN","08");
		reverseRelationships.put("SELF","18");
		reverseRelationships.put("NEPHEW_NIECE","06");

		/*reverseRelationships.put("FOSTER_CHILD","10");
		reverseRelationships.put("ADOPTED_CHILD","09");
		reverseRelationships.put("COURT_APPOINTED_GUARDIAN","31");
		reverseRelationships.put("FORMER_SPOUSE","25");
		reverseRelationships.put("GUARDIAN","26");
		reverseRelationships.put("ANNUITANT","60");
		reverseRelationships.put("TRUSTEE","D2");
		reverseRelationships.put("WARD","15");
		reverseRelationships.put("OTHER","G8");
		reverseRelationships.put("SPONSORED_DEPENDENT","23");
		reverseRelationships.put("DEPENDENT_OF_A_MINOR_DEPENDENT","24");
		reverseRelationships.put("COLLATERAL_DEPENDENT","38");*/
		
		
		employeeStatus = new HashMap<String,EmploymentStatusCodeSimpleType>();
		employeeStatus.put("WORKING", EmploymentStatusCodeSimpleType.CURRENTLY_EMPLOYED);
		employeeStatus.put("NO_LONGER_WORKING", EmploymentStatusCodeSimpleType.NO_LONGER_EMPLOYED);
		employeeStatus.put("RETIRED", EmploymentStatusCodeSimpleType.RETIRED);
		
	}
	/**
	 * Transforms appDataJson to AccountTransfer JAXB object.
	 * 
	 * @param ssap JSON and applicants verifications
	 * @return AccountTransferRequestPayloadType
	 * @throws ParseException
	 * @throws DatatypeConfigurationException
	 */
	@SuppressWarnings({ "rawtypes" })
	public AccountTransferRequestPayloadType populateAccountTransferRequest(ApplicationJSONDTO applicationDTO)
			throws GIException {
		AccountTransferRequestPayloadType returnObj = AccountTransferUtil.mainFactory.createAccountTransferRequestPayloadType();
	    returnObj.setAtVersionText("2.4");
	    // Get the verification metadata for applicants in the household
	    long ssapId=2;
		Map<Long, Map<String, VerificationMetadataType>> verificationMetadatas = verificationMetadataWrapper.createVerificationMetadata(ssapId);
		Map<String,InsuranceApplicantType> applicantsByMemberId = new HashMap<String,InsuranceApplicantType>();
         // Add all verification metadata
        /*for (Map<String, VerificationMetadataType> personVerificationMetadatas : verificationMetadatas.values()) {
            for (VerificationMetadataType verificationMetadata : personVerificationMetadatas.values()) {
                returnObj.getVerificationMetadata().add(verificationMetadata);
            }
        }*/
		// number of applicants seeking coverage
		setCoverageSeekingCount(applicationDTO);
		// transfer header
		returnObj.setTransferHeader(createTransferHeader());
		// Sender
		returnObj.getSender().add(createSender());
		returnObj.getSender().get(0).setId("SBM1"); 
		// Receiver
		returnObj.getReceiver().add(createReceiver());
		// InsuranceApplication
		/*if (AccountTransferUtil.checkBoolean(singleStreamlinedApplication.get("getHelpIndicator"))){
			AssisterType assisterType = new AssisterMapper().createAssister(singleStreamlinedApplication);  
			if(assisterType != null){
				returnObj.setAssister(assisterType);
			}
		}*/
		ApplicationDTO application=applicationDTO.getApplication().values().iterator().next();
		returnObj.setInsuranceApplication(insuranceApplicationMapper.mapInsuranceApplication(application, verificationMetadatas,applicantsByMemberId));
		
		
		if(returnObj.getAssister() != null){
			ReferenceType assisterReferenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			assisterReferenceType.setRef(returnObj.getAssister());
			// link assister
			InsuranceApplicationAssisterAssociationType insuranceApplicationAssisterAssociationType = AccountTransferUtil.insuranceApplicationObjFactory.createInsuranceApplicationAssisterAssociationType();
			insuranceApplicationAssisterAssociationType.setInsuranceApplicationAssisterReference(assisterReferenceType);
			returnObj.getInsuranceApplication().setInsuranceApplicationAssisterAssociation(insuranceApplicationAssisterAssociationType);
		}
		// Person element
		String primaryTaxFilerPersonId = AccountTransferUtil.getPersonTrackingNumber(application.getResult().getAttestations().getApplication().getContactMemberIdentifier(), application.getResult().getComputed().getMembers());
		for (Entry<String,AttestationsMember> memberEntry : application.getResult().getAttestations().getMembers().entrySet()) {
			// add person
			returnObj.getPerson().add(personTypeMapper.createPerson(memberEntry,application.getResult().getAttestations().getApplication(), verificationMetadatas,returnObj, application.getResult().getComputed().getMembers(),returnObj.getInsuranceApplication().getSSFPrimaryContact()));
			// Reference linking person to applicant
			//ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			
			// Reference linking person to SSFsigner
			//returnObj.getInsuranceApplication().getSSFSigner().setRoleOfPersonReference(referenceType);
			//returnObj.getInsuranceApplication().getSSFPrimaryContact().setRoleOfPersonReference(referenceType);
		}
		/*//TODO: authorizedRepresentativeIndicator is null if agent is not designated so we need to change below condition.
		if(singleStreamlinedApplication.get("authorizedRepresentativeIndicator") != null){
			if((java.lang.Boolean)singleStreamlinedApplication.get("authorizedRepresentativeIndicator")){
				returnObj.setAuthorizedRepresentative(new AuthorizedRepresentativeMapper().createAuthorizedRep((JSONObject)singleStreamlinedApplication.get("authorizedRepresentative")));
		    }
		}*/
		;
		
		// TaxReturn		
		//if(primaryTaxFilerPersonId != null && !primaryTaxFilerPersonId.toString().equals("0") ){
			returnObj.getTaxReturn().add(new TaxHouseHoldMapper().createTaxReturns(application.getResult().getAttestations(), application.getResult().getComputed(),primaryTaxFilerPersonId,application.getResult().getCoverageYear() , returnObj, verificationMetadatas));
		//}
		
		returnObj = createReferences(returnObj, application,applicantsByMemberId,primaryTaxFilerPersonId);
		lOGGER.debug(" created JAXB object from the given JSON : "+returnObj );
		return returnObj;
	}
	
	private AccountTransferRequestPayloadType createReferences(
			AccountTransferRequestPayloadType returnObj, ApplicationDTO application, Map<String, InsuranceApplicantType> applicantsByMemberId, String primaryTaxFilerPersonId) {
		PhysicalHouseholdType primaryPhysicalHouseHold = AccountTransferUtil.insuranceApplicationObjFactory.createPhysicalHouseholdType();
		for(Entry<String, InsuranceApplicantType> applicants : applicantsByMemberId.entrySet())
		{
			PersonType person = AccountTransferUtil.getPersonById(returnObj.getPerson(), AccountTransferUtil.getPersonTrackingNumber(applicants.getKey(), application.getResult().getComputed().getMembers()));
			ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			referenceType.setRef(person);
			applicants.getValue().setRoleOfPersonReference(referenceType);
			ReferenceType refTypePhyHousehold = AccountTransferUtil.niemStructFactory.createReferenceType();
			refTypePhyHousehold.setRef(person);
			Boolean livesWith =application.getResult().getAttestations().getMembers().get(applicants.getKey()).getFamily().getLiveWithParentOrSiblingIndicator();
			if(livesWith != null  && livesWith)
			{
				primaryPhysicalHouseHold.getHouseholdMemberReference().add(referenceType);
			}else {
				PhysicalHouseholdType separatePhysicalHouseHold = AccountTransferUtil.insuranceApplicationObjFactory.createPhysicalHouseholdType();
				separatePhysicalHouseHold.getHouseholdMemberReference().add(refTypePhyHousehold);
				returnObj.getPhysicalHousehold().add(separatePhysicalHouseHold);
			}
		}

		// createPersonAssociation under for each person under any person.
		if(application.getResult().getAttestations().getHousehold() != null) {
		createPersonRelationships(application.getResult().getAttestations().getHousehold().getFamilyRelationships(), returnObj.getPerson(),application.getResult().getComputed().getMembers());
		}
		// SSF referneces
		ReferenceType primaryReferenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
		PersonType primaryPerson = AccountTransferUtil.getPersonById(returnObj.getPerson(), primaryTaxFilerPersonId);
		primaryReferenceType.setRef(primaryPerson);
		returnObj.getInsuranceApplication().getSSFSigner().setRoleOfPersonReference(primaryReferenceType);

		returnObj.getInsuranceApplication().getSSFPrimaryContact().setRoleOfPersonReference(primaryReferenceType);

		for(InsuranceApplicantType  insuranceApplicant: returnObj.getInsuranceApplication().getInsuranceApplicant()){
			ReferenceType reciverReferenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			reciverReferenceType.setRef(returnObj.getReceiver().get(0));
			insuranceApplicant.getReferralActivity().setReferralActivityReceiverReference(reciverReferenceType);
			ReferenceType senderReferenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			senderReferenceType.setRef(returnObj.getSender().get(0));
			insuranceApplicant.getReferralActivity().setReferralActivitySenderReference(senderReferenceType);
		}
		
		
		return returnObj;
	}
	

	
	/**
	 * 
	 * @param map 
	 * @param arrayList
	 * @param list
	 * @return
	 */
	private void createPersonRelationships(ArrayList<Object> relationships, List<PersonType> persons, Map<String, Member> computedMembers){
		if(relationships !=null && !relationships.isEmpty())
		{
			for(Object relList : relationships) {
				
				String fromPersonId = (String)((ArrayList<Object>) relList).get(0);
				String toPersonId = (String)((ArrayList<Object>) relList).get(2);
				String relation = (String)((ArrayList<Object>) relList).get(1);
				PersonType fromPerson = AccountTransferUtil.getPersonById(persons, AccountTransferUtil.getPersonTrackingNumber(fromPersonId, computedMembers));
				PersonType toPerson = AccountTransferUtil.getPersonById(persons, AccountTransferUtil.getPersonTrackingNumber(toPersonId, computedMembers));
				if(!fromPersonId.equals(toPersonId))
				{
					createRelationship(AccountTransferMapperBatch.reverseRelationships.get(relation), fromPerson, toPerson);
					createRelationship(AccountTransferMapperBatch.relationships.get(relation), toPerson, fromPerson);
				}	
			}
		}
	}

	public void createRelationship(String relationshipCode, PersonType fromPerson, PersonType toPerson) {
		ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
		referenceType.setRef(toPerson);
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAssociationType personAssociationType = AccountTransferUtil.hixCoreFactory.createPersonAssociationType();
		personAssociationType.getPersonReference().add(referenceType);
		FamilyRelationshipHIPAACodeType familyRelationshipHIPAACodeType = AccountTransferUtil.hixTypeFactory.createFamilyRelationshipHIPAACodeType();
		familyRelationshipHIPAACodeType.setValue(relationshipCode);
		personAssociationType.setFamilyRelationshipCode(familyRelationshipHIPAACodeType);
		
		DateType associationBeginDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		Date associationBeginDate = AccountTransferUtil.basicFactory.createDate();
		associationBeginDate.setValue(AccountTransferUtil.getSystemDate());
		associationBeginDateType.setDate(associationBeginDate);
		personAssociationType.setAssociationBeginDate(associationBeginDateType);
		fromPerson.getPersonAugmentation().getPersonAssociation().add(personAssociationType);
	}
	

	/**
	 * creates the TransferHeader object
	 * 
	 * @return
	 */
	private TransferHeaderType createTransferHeader() throws GIException {
		String stateCode = "NV";
		TransferHeaderType transferHeaderType = AccountTransferUtil.mainFactory.createTransferHeaderType();
		TransferActivityType transferActivityType = AccountTransferUtil.mainFactory.createTransferActivityType();
		IdentificationType identificationType = AccountTransferUtil.neimCore2fctry.createIdentificationType();
		identificationType.setIdentificationID(AccountTransferUtil.createString(getTransferID()));
		transferActivityType.setActivityIdentification(identificationType);

		// time
		DateType activityDate = AccountTransferUtil.neimCore2fctry.createDateType();
		// activity Date
		DateTime dateTime = AccountTransferUtil.basicFactory.createDateTime();
		dateTime.setValue(AccountTransferUtil.getSystemDateTime());
		transferActivityType.setActivityDate(activityDate);
		transferActivityType.getActivityDate().setDateTime(dateTime);
		
		// no of Referrals
		QuantityType noOfReferrals = AccountTransferUtil.neimCore2fctry.createQuantityType();

		lOGGER.debug("coverageSeekingCount :" + coverageSeekingCount);
		noOfReferrals.setValue(new BigDecimal(this.coverageSeekingCount));

		transferActivityType.setTransferActivityReferralQuantity(noOfReferrals);
		// state
		transferActivityType.setRecipientTransferActivityStateCode(AccountTransferUtil.createUSStateCodeType(stateCode));
		// recipient
		TransferActivityCodeType transferActivityCodeType = AccountTransferUtil.mainFactory.createTransferActivityCodeType();
		transferActivityCodeType.setValue(TransferActivityCodeSimpleType.MEDICAID_CHIP); // because Exchange is sender
		transferActivityType.setRecipientTransferActivityCode(transferActivityCodeType);
		transferHeaderType.setTransferActivity(transferActivityType);

		return transferHeaderType;
	}
	
	private InformationExchangeSystemType createSender() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		InformationExchangeSystemType informationExchangeSystemType = AccountTransferUtil.hixCoreFactory.createInformationExchangeSystemType();
		InformationExchangeSystemCategoryCodeType informationExchangeSystemCategoryCodeType = AccountTransferUtil.hixTypeFactory.createInformationExchangeSystemCategoryCodeType();
		informationExchangeSystemCategoryCodeType.setValue(InformationExchangeSystemCategoryCodeSimpleType.EXCHANGE);
		informationExchangeSystemType.setInformationExchangeSystemCategoryCode(informationExchangeSystemCategoryCodeType);
		informationExchangeSystemType.setInformationExchangeSystemStateCode(AccountTransferUtil.createUSStateCodeType(stateCode));
		return informationExchangeSystemType;
	}


	private int setCoverageSeekingCount(ApplicationJSONDTO jsonObject) {
		coverageSeekingCount = 0;
		Entry<String,ApplicationDTO> data = jsonObject.getApplication().entrySet().iterator().next();
		ApplicationDTO application = data.getValue();
		Collection<AttestationsMember> members = application.getResult().getAttestations().getMembers().values();
		for(AttestationsMember member : members)
		{
			//--- COnunting all members rather than nly seekinmembers --
			//if(Boolean.TRUE.equals(member.getRequestingCoverageIndicator()))
			//{
				coverageSeekingCount++;
			//}
		}
		return coverageSeekingCount;
	}
	
	private InformationExchangeSystemType createReceiver(){
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		InformationExchangeSystemType recieverInformationExchangeSystemType = AccountTransferUtil.hixCoreFactory.createInformationExchangeSystemType();
		InformationExchangeSystemCategoryCodeType informationExchangeSystemCategoryCodeType = AccountTransferUtil.hixTypeFactory.createInformationExchangeSystemCategoryCodeType();
		recieverInformationExchangeSystemType.setId("reciever1");
		informationExchangeSystemCategoryCodeType.setValue(InformationExchangeSystemCategoryCodeSimpleType.MEDICAID_AGENCY);
		recieverInformationExchangeSystemType.setInformationExchangeSystemCategoryCode(informationExchangeSystemCategoryCodeType);
		recieverInformationExchangeSystemType.setInformationExchangeSystemStateCode(AccountTransferUtil.createUSStateCodeType(stateCode));
		return recieverInformationExchangeSystemType;
	}

    public static String getTransferID() {
		return java.util.UUID.randomUUID().toString().substring(0, TRANFER_ID_LENGTH);
	}
}
