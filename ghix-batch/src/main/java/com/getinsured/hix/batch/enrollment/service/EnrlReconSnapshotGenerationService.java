package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;

import com.getinsured.hix.model.enrollment.EnrlReconSummary;

public interface EnrlReconSnapshotGenerationService {
	
	void serveEnrlReconSnapshotGenerationJob(Integer fileId, long jobExecutionId) throws Exception;
	void populateEnrlReconSnapshot(List<Long> issuerPolicyIds, Integer fileId, List<Long> failedPolicyIds, EnrlReconSummary summary,  long jobExecutionId);
	void updateSummary(Integer fileId, Job job, JobLauncher jobLauncher, long jobExecutionId) throws Exception;
}
