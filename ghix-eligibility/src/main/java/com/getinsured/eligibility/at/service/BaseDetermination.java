package com.getinsured.eligibility.at.service;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * @author chopra_s
 * 
 */
public interface BaseDetermination {
	AccountTransferRequestDTO determineProcessor(String identificationId, long coverageYear, AccountTransferRequestPayloadType request, Integer giwsPayloadId, SsapApplicant ssapApplicant, String caseNumber);

	String PROCESS_INITIAL_REFERRAL = "/processOEReferral";
	
	String PROCESS_LCE_REFERRAL = "/processLceReferral";
	
	String PROCESS_QE_REFERRAL = "/processQEReferral";
	
	String PROCESS_RENEWAL_REFERRAL = "/processRenewalReferral";
}
