package com.getinsured.hub.vci;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hub.platform.GhixBaseTest;
import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIHUBResponse;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.repository.base.GIWSPayloadRepository;
import com.getinsured.iex.hub.platform.repository.base.HubResponseRepository;

@TransactionConfiguration(transactionManager="defaultTransactionManager")
public class VCIServiceTest extends GhixBaseTest {

	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired
	private HubResponseRepository hubResponseRepository;
	@Autowired
	private WebServiceTemplate vciServiceTemplate;
	
	@Test
	public void testInvalidSSNForVCI(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] invalidSSN = {"22222479","abc","22222479133","","23@#$@$","Barrera"};
		
		for(int i=0; i<invalidSSN.length; i++){
			try{
				requestPayload = null;
				String ifsvInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"personId\":1,\"name\":{\"firstName\":\"Arthur\",\"middleName\":\"A\",\"lastName\":\"Barrera\"},\"dateOfBirth\":\"06/06/1966\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"222224850\"}},{\"personId\":2,\"name\":{\"firstName\":\"Mira\",\"middleName\":\"M\",\"lastName\":\"Paudel\"},\"dateOfBirth\":\"06/06/1966\",\"socialSecurityCard\":{\"socialSecurityNumber\":\""+invalidSSN[i]+"\"}}]}]}";
				HubServiceBridge vciBridge = HubServiceBridge.getHubServiceBridge("VCIRequest");
				handler = vciBridge.getServiceHandler();
				handler.setJsonInput(ifsvInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNull(requestPayload);
		}
	}
	
	@Test
	public void testValidSSNForVCI(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] validSSN = {"222224850","222-22-4850","000000000","000-00-0000"};
		
		for(int i=0; i<validSSN.length; i++){
			try{
				requestPayload = null;
				String ifsvInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"personId\":1,\"name\":{\"firstName\":\"Arthur\",\"middleName\":\"A\",\"lastName\":\"Barrera\"},\"dateOfBirth\":\"06/06/1966\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"222224850\"}},{\"personId\":2,\"name\":{\"firstName\":\"Mira\",\"middleName\":\"M\",\"lastName\":\"Paudel\"},\"dateOfBirth\":\"06/06/1966\",\"socialSecurityCard\":{\"socialSecurityNumber\":\""+validSSN[i]+"\"}}]}]}";
				HubServiceBridge vciBridge = HubServiceBridge.getHubServiceBridge("VCIRequest");
				handler = vciBridge.getServiceHandler();
				handler.setJsonInput(ifsvInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNotNull(requestPayload);
		}
	}
	
	
	@Test
	public void testInvalidDOBForVCI(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] invalidDOB = {"5 May, 2013","02/30/1988","asda","","02/21/2016","Barrera","Jul 30, 1950 00:00:00 AM","Jul 30, 1950 00:00:00 PM","Jul 30, 1950 00:00:00 BM","Jul 30, 1950 0:0:0","Jul 30, 2015 0:0:0","Jul 30, 2015 000012:0000011:000000055", "JUL 30, 1950 00:00:00","AUGUST 30, 2015 00:00:00"};
		
		for(int i=0; i<invalidDOB.length; i++){
			try{
				requestPayload = null;
				String ifsvInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"personId\":1,\"name\":{\"firstName\":\"Arthur\",\"middleName\":\"A\",\"lastName\":\"Barrera\"},\"dateOfBirth\":\""+invalidDOB[i]+"\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"222224850\"}}]}]}";
				HubServiceBridge vciBridge = HubServiceBridge.getHubServiceBridge("VCIRequest");
				handler = vciBridge.getServiceHandler();
				handler.setJsonInput(ifsvInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNull(requestPayload);
		}
	}
	
	@Test
	public void testValidDOBForVCI(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] validDOB = {"02/21/1988","05/09/2014","06/06/1966"};
		
		for(int i=0; i<validDOB.length; i++){
			try{
				requestPayload = null;
				String ifsvInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"personId\":1,\"name\":{\"firstName\":\"Arthur\",\"middleName\":\"A\",\"lastName\":\"Barrera\"},\"dateOfBirth\":\""+validDOB[i]+"\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"222224850\"}}]}]}";
				HubServiceBridge vciBridge = HubServiceBridge.getHubServiceBridge("VCIRequest");
				handler = vciBridge.getServiceHandler();
				handler.setJsonInput(ifsvInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNotNull(requestPayload);
		}
	}
	
	@Test
	@Transactional(readOnly = true)
	public void testVCIService() throws HubMappingException, BridgeException, HubServiceException {

		ServiceHandler handler = null;
		GIWSPayload requestRecord = null;

		String ifsvInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"personId\":1,\"name\":{\"firstName\":\"Arthur\",\"middleName\":\"A\",\"lastName\":\"Barrera\"},\"dateOfBirth\":\"06/06/1966\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"222224850\"}},{\"personId\":2,\"name\":{\"firstName\":\"Mira\",\"middleName\":\"M\",\"lastName\":\"Paudel\"},\"dateOfBirth\":\"06/06/1966\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"222224879\"}}]}]}";
		HubServiceBridge vciBridge = HubServiceBridge
				.getHubServiceBridge("VCIRequest");
		handler = vciBridge.getServiceHandler();
		handler.setJsonInput(ifsvInputJSON);

		requestRecord = new GIWSPayload();
		requestRecord.setEndpointFunction("VCI");
		requestRecord.setEndpointOperationName("VCI");
		requestRecord.setAccessIp("0.0.0.0");
		requestRecord.setStatus("PENDING");
		requestRecord.setSsapApplicationId(null);

		// Persist initial request
		requestRecord = this.giWSPayloadRepository.saveAndFlush(requestRecord);
		Assert.assertNotSame(0L, requestRecord.getId());

		// Creating the payload
		Object requestPayload = handler.getRequestpayLoad();
		Assert.assertNotNull(requestPayload);

		// Call HUB service
		long start = System.currentTimeMillis();
		Object webserviceResponse = vciServiceTemplate
				.marshalSendAndReceive(requestPayload);
		long timeTaken = System.currentTimeMillis() - start;
		Assert.assertNotNull(webserviceResponse);

		// Save final request
		requestRecord.setEndpointUrl(vciServiceTemplate.getDefaultUri());
		requestRecord.setRequestPayload(handler.getJsonInput());
		requestRecord.setStatus(RequestStatus.SUCCESS.getStatusCode());
		requestRecord = this.giWSPayloadRepository.saveAndFlush(requestRecord);

		// Process and save hub response
		String responseStr = handler.handleResponse(webserviceResponse);
		Assert.assertNotNull(responseStr);
		Assert.assertNotSame(responseStr, "");
		GIHUBResponse hubResponse = new GIHUBResponse();
		hubResponse.setEndPointURL(vciServiceTemplate.getDefaultUri());
		hubResponse.setGiWspayloadId(requestRecord.getId());
		hubResponse.setClientNodeIp("0.0.0.0");
		hubResponse.setSsapApplicationId(requestRecord.getSsapApplicantId());
		hubResponse.setStatus(RequestStatus.SUCCESS.getStatusCode());
		hubResponse.setElapsedTime(timeTaken);
		hubResponse.setHubResponsePayload(responseStr);
		hubResponse.setHubResponseCode(handler.getResponseCode());
		hubResponse.setHubCaseNumber(""
				+ handler.getResponseContext().get("HUB_CASE_NUMBER"));
		hubResponse = hubResponseRepository.save(hubResponse);
		Assert.assertNotSame(0L, hubResponse.getId());
		Assert.assertNotNull(hubResponse.getHubResponsePayload());

	}

}
