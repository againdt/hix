import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';


// Components

import FormForModal from '../pages/FormForModal';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { hideModal, changeData } from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import INPUT_TYPES from '../../constants/inputTypes';



const PreEligibilityModal = props => {

    const {
        data,
        householdMembers,
        modal: {
            modalOwner,
            data: [
                hhms, saveData, submitApplication, body = undefined
            ]
        },
        PHONE
    } = props;

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };

    const handleSubmit = () => {
        if(!body) {
            submitApplication();
        } else {
            submitApplication(body);
        }
    };

    const getHhmNameByApplicantGuid = applicantGuid => {
        return normalizeNameOfPerson(householdMembers.find(hhm => hhm.applicantGuid === applicantGuid).name);
    };

    const onFieldChange = (...args) => {

        const [ , applicantGuid, , [ , value ] ] = args;

        let newState = { ...data };

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember = newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember.map(hhm =>
                    hhm.applicantGuid === applicantGuid ?
                        {
                            ...hhm,
                            seeksQhp: value
                        } : hhm
                );

        saveData(newState);
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal__pre_eligibility">
                Information about your Eligibility
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>

            <div id="rsPreEligibilityModal" className="usa-grid">
                <div className="modal__body">
                    <p>
                        The applicants listed below may be eligible for Medicaid/CHIP. We are referring them to the Department of Welfare and Social Services (Nevada&#39;s Medicaid/CHIP Agency) for additional review and final determination. In the meantime, these applicants may be eligible to enroll in a plan on the Exchange at full price. If someone has Medicaid/CHIP coverage, he or she may not be eligible for Marketplace subsidies (APTC/CSR).
                    </p>
                    <p>
                        Please select applicants you would like to enroll in a full-priced exchange plan while awaiting Medicaid eligibility determination:
                    </p>
                    <FormForModal>
                        {
                            hhms.map(hhm => {

                                const {
                                    applicantGuid
                                } = hhm;

                                const value = (hhm => hhm ? hhm.seeksQhp : null)(householdMembers.find(hhm => hhm.applicantGuid === applicantGuid));

                                return (
                                    <UniversalInput
                                        key={applicantGuid}
                                        label={{
                                            text: getHhmNameByApplicantGuid(applicantGuid)
                                        }}
                                        inputData={{
                                            isInputRequired: false,
                                            type: INPUT_TYPES.CHECKBOX,
                                            name: 'isGoingQHP',
                                            classes: [''],
                                            checked: value,
                                            value: value,
                                            currentData: { },
                                            fields: [],
                                        }}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                        indexInArray={applicantGuid}
                                    />
                                );
                            })
                        }
                    </FormForModal>
                    <p>
                        If you need additional information about your Medicaid/CHIP assessment, then please reach out to 800-992-0900. If you need information about enrolling in a Nevada Health Link Exchange plan, then please call { PHONE }
                    </p>
                </div>
            </div>
            <div className="modal__footer">
                <div className='usa-grid controls'>
                    <div className='usa-offset-one-third txt-left'>
                        <Button
                            className='modal-cancel-btn usa-button-secondary'
                            onClick={handleHideClick}
                        >
                            Cancel
                        </Button>
                        <Button
                            className='modal-confirm-btn'
                            onClick={handleSubmit}
                        >
                            Save
                        </Button>
                    </div>
                </div>
            </div>

        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    PHONE: state
        .headerLinks
        .links
        .PHONE,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal,
        changeData
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PreEligibilityModal);