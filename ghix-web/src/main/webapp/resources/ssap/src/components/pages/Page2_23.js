import React, { Fragment, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import InstructionalText from '../notifications/InstructionalText';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import {changeData} from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import INPUT_TYPES from '../../constants/inputTypes';


/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Preganant Details ' Page
 *
 */
const Page2_23 = props => {

    const {
        data,
        householdMembers
    } = props;

    const {
        isFirstVisit
    } = props;

    // Local State of Functional Component

    const [localState, setLocalState] = useState({
        noneOfTheAboveblindOrDisabled: isFirstVisit ? false : householdMembers.every(member => !member.blindOrDisabled),
        noneOfTheAbovelongTermCare: isFirstVisit ? false : householdMembers.every(member => !member.longTermCare)
    });

    const onFieldChange_member = (...args) => {
        const [ newData, indexInArray, , [ name ] ] = args;

        setLocalState(state => {
            return {
                ...state,
                [`noneOfTheAbove${name}`]: false
            };
        });

        const {
            blindOrDisabled,
            longTermCare
        } = newData;

        const newState = {...data };

        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] =
            {
                ...newData,
                disabilityIndicator: !!(blindOrDisabled || longTermCare)
            };
        props.actions.changeData(newState);
    };


    // Updating status of 'None of Above' checkbox and if it is checked:
    // a) uncheck all household members
    // b) clear all <americanIndianAlaskaNative> objects in household members
    // c) save changes to Redux Store

    const onFieldChange_none_of_above = (...args) => {

        const [ , , , [ name, value ] ] = args;

        setLocalState(state => {
            return {
                ...state,
                [name]: value
            };
        });

        if(value === true) {
            const newState = {...data };

            const updatedHouseholdMembers =
                householdMembers
                    .map(member => {

                        // First of all we update property

                        member[name.split('noneOfTheAbove')[1]] = false;

                        // Here we use updated property, like another one

                        const {
                            blindOrDisabled,
                            longTermCare
                        } = member;

                        return {
                            ...member,
                            disabilityIndicator: !!(blindOrDisabled || longTermCare)
                        };
                    });

            newState.singleStreamlinedApplication.taxHousehold[0].householdMember = updatedHouseholdMembers;
            props.actions.changeData(newState);
        }
    };

    // Iterating through all household members and providing for rendering data
    // UI is bound to Local State if household member is checked or not

    const getHouseholdMembers = (householdMembers, indicator) => {

        const combinedMembers = [];

        householdMembers.forEach((member, i) => {

            const {
                name,
                applyingForCoverageIndicator
            } = member;

            const memberName = `${normalizeNameOfPerson(name)}`;

            combinedMembers.push(
                <Fragment key={`${name.firstName}_${i}`}>
                    {applyingForCoverageIndicator &&
                    <Fragment>
                        <ul className="usa-unstyled-list">
                            <li>
                                <UniversalInput
                                    label={{
                                        text: `${memberName}`
                                    }}
                                    inputData={{
                                        isInputRequired: false,
                                        type: INPUT_TYPES.CHECKBOX,
                                        name: `${indicator}`,
                                        classes:[''],
                                        checked: member[indicator],
                                        value: member[indicator],
                                        currentData: cloneObject(member),
                                        fields: [],
                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange_member
                                    }}
                                    indexInArray={i}
                                    errorClasses={['margin-l-0']}
                                />
                            </li>
                        </ul>
                    </Fragment>
                    }

                </Fragment>
            );

        });

        combinedMembers.push(
            <UniversalInput
                key={`None_of_above_${combinedMembers.length + 1}`}
                label={{
                    text: 'None of the Above',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: `noneOfTheAbove${indicator}`,
                    checked: localState[`noneOfTheAbove${indicator}`],
                    value: localState[`noneOfTheAbove${indicator}`],
                    currentData: Object.assign({}, localState),
                    fields: [],
                }}
                inputActions={{
                    onFieldChange: onFieldChange_none_of_above
                }}
            />
        );
        return combinedMembers;
    };

    return (
        <Fragment>
            <Form>
                <div id="fhMoreInfo" className="fade-in">
                    <div className="subsection">

                        <InstructionalText
                            Component={
                                <p data-instructions='disability_information' className="required-false">
                                    Do any of these people have a physical disability or mental health condition that limits their ability to work, attend school, or take care of their daily needs?
                                </p>
                            }
                        />


                        {
                            getHouseholdMembers(householdMembers, 'blindOrDisabled')
                        }

                        <p className="required-false">
                              Do any of these people need help with activities of daily living (like bathing, dressing, and using the bathroom), or live in a nursing home, or other medical facility?
                        </p>

                        {
                            getHouseholdMembers(householdMembers, 'longTermCare')
                        }
                    </div>
                </div>
            </Form>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_23.defaultProps = {
    name: 'Page2_23'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_23);
