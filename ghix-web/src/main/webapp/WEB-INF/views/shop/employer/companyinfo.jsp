<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="gutter10">
	<div class="page-header">
		<h1><spring:message  code="label.setupcompany"/> <small class=""><spring:message  code="label.companyinfoh2"/></small></h1>
	</div>

	<div class="row-fluid">
		<!-- add id for skip side bar -->
		<div class="span3" id="sidebar">
			<util:nav pageName="Enter Company Info" navStepNo="1"></util:nav>
			
		</div>
		<div class="span9" id="rightpanel">

			<form:form class="form-horizontal" id="frmcompanyinfo" name="frmcompanyinfo" action="createcompanyinfo" method="POST" commandName="employer" >
				<df:csrfToken/>
				    <input type="hidden" name="id" id="id" value="${employer.id}"/>
					<input type="hidden" name="createdBy" id="creatdeBy" value="${userOBJ.id}"/>
					<input type="hidden" name="updatedBy" id="updatedBy" value="${userOBJ.id}"/>
					
					<div class="header margin10-b">
					<h4><spring:message  code="label.companyinfoh2"/></h4>
					</div>
					<div class="profile">
						<div class="control-group">
							<label for="name" class="control-label required"><spring:message  code="label.businessname"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					        <div class="controls">
								<%-- <form:input path="name" id="name" value="${employer.name}" class="input-large" size="30"/> --%>
								<input type="text" name="name" id="name" value="${employer.name}" class="input-large" maxlength="50" size="50">
								<div id="name_error"></div>
								<%-- <form:errors path="name" cssClass="error" /> --%>
							</div>	
						</div>
					    <div class="control-group">
			               <label for="federalEIN" class="control-label required"><spring:message  code="label.federalein"/>&#40;EIN&#41;</label>
			               <div class="controls">
								<form:input path="federalEIN" id="federalEIN" value="${employer.federalEIN}" class="input-large" maxlength="9" size="30"/>
								&nbsp;&nbsp;<small><a href="#einModal" role="button" class="" data-toggle="modal"><spring:message  code="label.eininfo"/></a></small>
								<div id="federalEIN_error"></div>
						   </div>
		                </div>
		                <div class="control-group">
							<label for="orgType" class="control-label required"><spring:message  code="label.typeofcompany"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					        <div class="controls">
								<form:select id="orgType" path="orgType">
									<option value=""><spring:message  code="label.select"/></option>
									<option <c:if test="${employer.orgType == 'CORPORATION'}"> SELECTED </c:if> value="CORPORATION">Corporation</option>
									<option <c:if test="${employer.orgType == 'NON_PROFIT'}"> SELECTED </c:if> value="NON_PROFIT">Non-Profit</option>
									<option <c:if test="${employer.orgType == 'SOLE_PROPRIETORSHIP'}"> SELECTED </c:if> value="SOLE_PROPRIETORSHIP">Sole Proprietorship</option>
									<option <c:if test="${employer.orgType == 'PARTNERSHIP'}"> SELECTED </c:if>  value="PARTNERSHIP">Partnership</option>
								</form:select>
								<div id="orgType_error"></div>
							</div>	
						</div>
		                				          					
				    </div>
				    
				  
					<h4><spring:message  code="label.emplEmpNos"/></h4>
					<div class="profile">
						<div class="control-group">
							<label class="control-label required" for="fullTimeEmp"><spring:message code='label.emplFulltime' javaScriptEscape='true'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					        <div class="controls">
								<form:input id="fullTimeEmp" class="input-mini" size="3" maxlength="3" value="${employer.fullTimeEmp}" path="fullTimeEmp"/>
								&nbsp;&nbsp;<small><a href="#fteModal" role="button" class="" data-toggle="modal">How do I calculate Full-Time Equivalent Employees?</a></small>
								<!--&nbsp;&nbsp;<small>30+ hours/week</small>-->
								<div id="fullTimeEmp_error"></div>
							</div>	
						</div>
						<div class="control-group">
							<label class="control-label required" for="averageSalary"><spring:message code='label.averageSalary' javaScriptEscape='true'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /><a href="#" tabindex="-1" rel="tooltip" data-placement="top" data-original-title="<spring:message  code="label.averageSalaryInfo"/> " class="info"> <i class="icon-question-sign"></i></a></label>
					        <div class="controls">
								<form:input id="averageSalary" class="input-large" size="30" maxlength="20" value="${employer.averageSalary}" path="averageSalary"/>
								
								<!--&nbsp;&nbsp;<small>30+ hours/week</small>-->
								<div id="averageSalary_error"></div>
							</div>	
						</div>						
						<!--<div class="control-group">
							<label class="control-label required" for="emplParttime"><spring:message code='label.emplParttime' javaScriptEscape='true'/></label>
					        <div class="controls">
								<input id="partTimeEmp" class="input-mini" type="text" size="3" maxlength="3" value="" name="partTimeEmp"/>
								<div id="partTimeEmp_error"></div>
							</div>	
						</div>-->
						
						<div class="control-group" id="seasonal_div" style="display:none;">
							<label class="control-label required" for="seasonalEmp"><spring:message code='label.emplSeasonal' javaScriptEscape='true'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					        <div class="controls">
								<input id="seasonalEmp" class="input-mini" type="text" size="3" maxlength="3" value="" name="seasonalEmp"/>
								<div id="seasonalEmp_error"></div>
							</div>	
						</div>
					</div>
	
				<div class="form-actions">
					<input type="submit" name="submitbutton" id="submitbutton" class="btn btn-primary pull-right" value="<spring:message  code='label.next'/>" title="<spring:message  code='label.next'/>"/>
				</div>
			</form:form>
     
    <!-- Modal -->
			<div id="einModal" class="modal hide fade" tabindex="-1" data-backdrop="static" modal="true"
				role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<p id="myModalLabel1">&nbsp;</p>
				</div>
				<div class="modal-body">
					<h3><spring:message  code='label.eininfotitle'/></h3>
					<p><spring:message  code='label.eininfodesc1'/> ${exchangeName} <spring:message  code='label.eininfodesc2'/></p>
					<a
						href="http://www.irs.gov/Businesses/Small-Businesses-&-Self-Employed/Apply-for-an-Employer-Identification-Number-(EIN)-Online"><spring:message  code='label.getnewein'/></a>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal"><spring:message  code='label.close'/></button>
				</div>
			</div>
			
	<!-- Full-Time Equivalent Employees Modal -->
			<div id="fteModal" class="modal hide fade" tabindex="-1" data-backdrop="static" modal="true" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3><spring:message code='label.emplFulltime' javaScriptEscape='true'/></h3>
				</div>
				<div class="modal-body">
					<p><spring:message  code="label.howcalculateFTEmodal1"/> ${maxShopEmployee} <spring:message  code="label.howcalculateFTEmodal2"/></p>
					<p><spring:message  code="label.howcalculateFTEmodal3"/></p>
					<p><spring:message  code="label.howcalculateFTEmodal4"/> <a href="http://www.irs.gov/uac/Small-Business-Health-Care-Tax-Credit-Questions-and-Answers:-Determining-FTEs-and-Average-Annual-Wages"><spring:message  code="label.howcalculateFTEmodal5"/></a> <spring:message  code="label.howcalculateFTEmodal6"/></p>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal"><spring:message  code='label.close'/></button>
				</div>
			</div>
			
			
			
			<div id="empcount" class="modal hide fade" data-backdrop="static" modal="true" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<p id="myModalLabel2">&nbsp;</p>
				</div>
				<div class="modal-body">
					<p><spring:message  code='label.eininfoshortdesc1'/> ${stateName} <spring:message  code='label.eininfoshortdesc2'/> ${maxShopEmployee} <spring:message  code='label.eininfoshortdesc2_2'/></p>
					<a href="#" class="nmhide mshide"><spring:message  code='label.moreinfo'/></a>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal"><spring:message  code='label.ok'/></button>
				</div>
			</div>
			
		</div>
	</div>
</div>

<script type="text/javascript">

function checkEmployeeCount(){
	var fulltime = ( isNaN(parseInt($('#fullTimeEmp').val())) ? 0 : $('#fullTimeEmp').val() );
	var totalEmployees = parseInt(fulltime);
	$('#totalEmployees').val(totalEmployees);
	var maxShopEmp = '${maxShopEmployee}';
	if(totalEmployees > parseInt(maxShopEmp)){
		$('#empcount').modal('show');
	}
}

$('#fullTimeEmp').blur(checkEmployeeCount);

jQuery.validator.addMethod("fullTimeEmpcheck", function(value, element, param) {
	var fulltime = ( isNaN(parseInt($('#fullTimeEmp').val())) ? 0 : $('#fullTimeEmp').val() );
	var totalEmployees = parseInt(fulltime);
	if(totalEmployees <= 0){
  		return false;	
  	}return true;
});
jQuery.validator.addMethod("currencyCheck", function(value, element, param) {
	//var averageSalary = $('#averageSalary').val();
	var pattern = /^(\\$)?([0-9\,])+(\.\d{0,5})?$/;
	return pattern.test(value);
});

jQuery.validator.addMethod("greaterThanZero", function(value, element, param) {
	//var averageSalary = $('#averageSalary').val();
	var numVal = parseFloat(value.replace(/[^0-9.-]+/g, ''));
	
	if(isNaN(numVal)){
		return false;
	}
	if(numVal <=0){
		return false;}
	else{ 
		return true;}	
});

jQuery.validator.addMethod("companynamecheck", function(value, element, param) {
	var companyName = $('#name').val();
	comapnyname = jQuery.trim(companyName);
	if(companyName.length >= 2 && companyName.length <= 50){
		return true;	
	}
	return false;	
});

var existing_ein = '${employer.federalEIN}';

jQuery.validator.addMethod("eincheck", function(value, element, param) {
	var einvalue = $('#federalEIN').val();
	einvalue = jQuery.trim(einvalue);
	existing_ein = jQuery.trim(existing_ein);
	if(existing_ein!=''){
		if(einvalue.length == 0){
			return false;	
		}
	}
	return true;	
});


 var validator = $('#frmcompanyinfo').validate({ 
	rules : {
		name : { required : true, companynamecheck : true},
		fullTimeEmp : {required : true, number: true, fullTimeEmpcheck: true},
		federalEIN  : {eincheck : true, number: true, minlength: 9},
		averageSalary : { required : true, currencyCheck: true,greaterThanZero:true},
		orgType : { required : true }
	},
	messages : {
		name : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCompanyNameSize' javaScriptEscape='true'/></span>",
			companynamecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateCompanyNameSize' javaScriptEscape='true'/></span>" },
		fullTimeEmp : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatefulltimeemployee' javaScriptEscape='true'/> </span>",
						number: "<span> <em class='excl'>!</em><spring:message code='label.validatefulltimeempnumber' javaScriptEscape='true'/></span>" ,
						fullTimeEmpcheck: "<span> <em class='excl'>!</em><spring:message code='label.validatefulltimeempnumber' javaScriptEscape='true'/></span>" },
		federalEIN  : { eincheck : "<span> <em class='excl'>!</em><spring:message code='label.validateeinnumber' javaScriptEscape='true'/></span>",
						number: "<span> <em class='excl'>!</em><spring:message code='label.validateeinnumber' javaScriptEscape='true'/></span>", 
						minlength: 	"<span> <em class='excl'>!</em><spring:message code='label.validateeinlength' javaScriptEscape='true'/></span>"
		},
		averageSalary : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateavgsalary' javaScriptEscape='true'/> </span>",
						currencyCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateavgsalaryformat' javaScriptEscape='true'/> </span>",
						greaterThanZero : "<span> <em class='excl'>!</em><spring:message code='label.validateavgsalaryformat' javaScriptEscape='true'/> </span>"
		},
		orgType : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateorgtype' javaScriptEscape='true'/> </span>" }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
}); 
$('.info').tooltip();
</script>