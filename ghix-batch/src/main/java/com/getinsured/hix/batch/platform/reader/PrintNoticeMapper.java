package com.getinsured.hix.batch.platform.reader;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.getinsured.hix.model.NoticeDTO;

public class PrintNoticeMapper implements RowMapper<NoticeDTO>{
	
	@Override
	public NoticeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			
		NoticeDTO noticeDTO = new NoticeDTO(rs.getInt("ID"), rs.getString("ECM_ID"), rs.getDate("CREATION_TIMESTAMP"));
		return noticeDTO;
	}

}
