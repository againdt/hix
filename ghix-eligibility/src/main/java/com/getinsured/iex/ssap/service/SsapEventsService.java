package com.getinsured.iex.ssap.service;

import com.getinsured.iex.dto.qep.QepEventRequest;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

public interface SsapEventsService {
	void createSsapEvents(QepEventRequest qepEventRequest) throws Exception;
	SsapApplicationEvent findSsapApplicationEventByApplicationId(long ssapApplicationId);
	void triggerQleValidation(String caseNumber, int userId);
}
