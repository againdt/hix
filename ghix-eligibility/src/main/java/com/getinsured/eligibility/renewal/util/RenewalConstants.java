package com.getinsured.eligibility.renewal.util;

public interface RenewalConstants {

	String HEALTH = "HEALTH";
	String DENTAL = "DENTAL";
	String QHP = "QHP";
	String MEDICAID = "MEDICAID";
	String AT = "AT";
	String Y = "Y";
	String N = "N";
	String RENEWALS = "RENEWALS";
	String NOT_IN_AT = "NOT_IN_AT";
	String NEW_MEMBER = "NEW_MEMBER";
	String DENTAL_IN_ELIGIBLE = "HDIE";
	String DENTAL_AGE_CHECK_FAILED = "DENTAL_AGE_CHECK_FAILED";
	String HEALTH_AGE_CHECK_FAILED = "HEALTH_AGE_CHECK_FAILED";
	String HEALTH_IN_ELIGIBLE = "HIE";
	String GROUP_INCONSISTENT = "GIC";
	String GROUP_IN_ELIGIBLE = "GIE";
	String HEALTH_INCONSISTENT = "HIC";
	String HEALTH_NOT_AVAILABLE = "HNA";
	String MULTIPLE_GROUP = "MULTIPLE_GROUP";
	String MEMBER_NOT_ELIGIBLE = "MIE";
	String ENROLLMENT_NOT_AVAILABLE = "ENA";
	String DENTAL_NOT_CONSIDERED = "DNC";
	String USER_SHOPPED_PLANS = "USP";
	String TRUE = "true";
	String USE_MIGRATION_ENROLLMENTS = "enrollment.api.useMigratedEnrollments";
	String CLOSE_APPLICATION = "CLOSE_APPLICATION";
	String SSAP_TO_CONSIDER_BATCH = "SSAP_TO_CONSIDER_BATCH";
	String REDETERMINE_ELIGIBILITY_BATCH = "REDETERMINE_ELIGIBILITY_BATCH";
	String FAILURE = "FAILURE";
	String SUCCESS = "SUCCESS";
	String KEY_ENROLLMENTS = "enrollments";
	String NEW_APTC_MEMBER = "NEW_APTC_MEMBER";
	String MEDICAID_ELIGIBLE_MEMBER = "MIME";
	String MEDICAID_ELIGIBLE_GROUP = "GIME";
	String MEDICAID_ELIGIBLE_HOUSEHOLD = "HIME";
	
	
	// Lookup data
	enum RenewalLookup {

		TYPE("RENEWALS"),
		CUSTOM_GROUPING_TO_MARK_NOT_TO_CONSIDER("CGMNTC");

		private final String code;

		private RenewalLookup(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}
}
