<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>

		<div class="row-fluid">
			<div class="span3" id="sidebar">
			
					<!--  beginning of side bar -->
					<ul class="nav nav-list">
						<li class="nav-header"><spring:message code="label.issuerCertification"/></li>
						<li><a href="#"><spring:message code="label.authorizeRepresentatives"/></a></li>
						<li><a href="#"><spring:message code="label.submitIssuerApplication"/></a></li>
						<li><a href="#"><spring:message code="label.reviewAndConfirm"/></a></li>
					</ul>
					<!-- end of side bar -->
				
			</div>
			<!-- end of span3 -->
			<div class="span9" id="sidebar">
				<div class="gutter10">
					<div class="page-header">
						<h1><a name="skip" class="skip"><spring:message code="pgheader.verifiInfo"/></a></h1>
						<p><spring:message code="note.verifiInfo"/></p>
					</div>
					<!-- end of page-header -->
				
				  <form class="form-vertical" id="frmIssuerVerifyAccInfo" name="frmIssuerVerifyAccInfo" action="<c:url value="/planmgmt/issuercertificationverifiinfo" />" method="POST">
					<div class="profile">
					

						<table class="table">
							<thead>
								<tr>
									<td class="txt-right span4"><spring:message  code="label.name"/></td>
									<td><strong>${issuerObj.name}</strong></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="txt-right"><spring:message  code="label.licenseNumber"/></td>
									<td><strong>${issuerObj.licenseNumber}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.licenseStatus"/></td>
									<td><strong>${issuerObj.licenseStatus}</strong></td>
								</tr>
								<tr>
	                                <td class="txt-right"><spring:message  code="label.NAICCompanyNumber"/></td>
	                                <td><strong>${issuer.naicCompanyCode}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.NAICGroupCode"/></td>
	                                <td><strong>${issuer.naicGroupCode}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right"><spring:message  code="label.HIOSIssuerId"/></td>
	                                <td><strong>${issuer.hiosIssuerId}</strong></td>
	                            </tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.busiAddress"/></td>
									<td><strong>${issuer.addressLine1} ${issuer.addressLine2}<br />${issuerObj.city}, ${issuerObj.state} ${issuerObj.zip}</strong>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.busiPhoneNumber"/></td>
									<td><strong>${issuerObj.phone}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.busiEmail"/></td>
									<td><strong>${issuerObj.email}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.certifictionStatus"/></td>
									<td><strong>${issuerObj.certificationStatus}</strong></td>
								</tr>
								
							</tbody>
						</table>
					</div>			
					<c:if test="${issuerObj.certificationStatus == issuerStatusRegistered}">
						<div class="form-actions"> 
							<input type="button" name="submitUpdate" id="submitUpdate" value="<spring:message code='lable.updateAccountInfo'/>" class="btn btn-primary" disabled="disabled"/>
							<input type="submit" name="submit" id="submit" value="<spring:message code='label.btnConfirmContinue'/>" class="btn btn-primary" title="<spring:message code='label.btnConfirmContinue'/>"/>
						</div><!-- end of .form-actions -->
					</c:if>
				</form>

			</div>
		
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
