package com.getinsured.hix.batch.plandisplay.reader;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.jdbc.core.RowMapper;

public class PlanDisplayHealthCostMapper implements RowMapper<PlanDisplayHealthCostMapper>{

	private int id;
	
	private int planHealthId;

	private String name;
	
	private int planId;

	private Double inNetWorkInd;

	private Double inNetworkFly;

	private Double inNetworkTier2Ind;

	private Double inNetworkTier2Fly;

	private Double outNetworkInd;

	private Double outNetworkFly;

	private Double combinedInOutNetworkInd;

	private Double combinedInOutNetworkFly;

	private Double combDefCoinsNetworkTier1;

	private Double combDefCoinsNetworkTier2;
	
	private String limitExcepDisplay;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPlanHealthId() {
		return planHealthId;
	}

	public void setPlanHealthId(int planHealthId) {
		this.planHealthId = planHealthId;
	}
	
	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public String getLimitExcepDisplay() {
		return limitExcepDisplay;
	}

	public void setLimitExcepDisplay(String limitExcepDisplay) {
		this.limitExcepDisplay = limitExcepDisplay;
	}

	@Override
	public PlanDisplayHealthCostMapper mapRow(ResultSet result, int arg1) throws SQLException {
		
		PlanDisplayHealthCostMapper obj = new PlanDisplayHealthCostMapper();
		
		obj.name = result.getString("NAME");
		obj.id = result.getInt("ID");
		obj.planHealthId = result.getInt("plan_health_id");
		obj.planId = result.getInt("plan_Id");
		obj.inNetWorkInd = result.getDouble("IN_NETWORK_IND");
		obj.inNetworkFly = result.getDouble("IN_NETWORK_FLY");
		obj.inNetworkTier2Ind = result.getDouble("IN_NETWORK_TIER2_IND");
		obj.inNetworkTier2Fly = result.getDouble("IN_NETWORK_TIER2_FLY");
		obj.outNetworkInd = result.getDouble("OUT_NETWORK_IND");
		obj.outNetworkFly =  result.getDouble("OUT_NETWORK_FLY");
		obj.combinedInOutNetworkInd = result.getDouble("COMBINED_IN_OUT_NETWORK_IND");
		obj.combinedInOutNetworkFly = result.getDouble("COMBINED_IN_OUT_NETWORK_FLY");
		obj.combDefCoinsNetworkTier1 = result.getDouble("COMB_DEF_COINS_NETWORK_TIER1");
		obj.combDefCoinsNetworkTier2 = result.getDouble("COMB_DEF_COINS_NETWORK_TIER2");
		obj.limitExcepDisplay = result.getString("LIMIT_EXCEP_DISPLAY");
		return obj;
	}
	
	public SolrInputDocument getSolrInputDocument(){
		
		SolrInputDocument solrDoc = new SolrInputDocument();
		
		solrDoc.addField("name", this.name);
		solrDoc.addField("id", this.id);
		solrDoc.addField("planHealthId",this.planHealthId);
		solrDoc.addField("planId",this.planId);
		solrDoc.addField("inNetWorkInd", this.inNetWorkInd);
		solrDoc.addField("inNetworkFly", this.inNetworkFly);
		solrDoc.addField("inNetworkTier2Ind", this.inNetworkTier2Ind);			
		solrDoc.addField("inNetworkTier2Fly", this.inNetworkTier2Fly);   
		solrDoc.addField("outNetworkInd", this.outNetworkInd);					 		
		solrDoc.addField("outNetworkFly", this.outNetworkFly);						
		solrDoc.addField("combinedInOutNetworkInd", this.combinedInOutNetworkInd);					
		solrDoc.addField("combinedInOutNetworkFly", this.combinedInOutNetworkFly);			
		solrDoc.addField("combDefCoinsNetworkTier1", this.combDefCoinsNetworkTier1);     
		solrDoc.addField("combDefCoinsNetworkTier2", this.combDefCoinsNetworkTier2);
		solrDoc.addField("limitExcepDisplay", this.limitExcepDisplay);
		return solrDoc;
	}
	
}
