---
- name: EC2 facts
  ec2_instance_facts:
    region: "{{ aws_region }}"
    aws_access_key: "{{ aws_access_key }}"
    aws_secret_key: "{{ aws_secret_key }}"
    filters:
      "tag:type": "{{ web_host_tag }}"
  register: ec2

- name: Print all info of EC2 variable
  debug:
    var: ec2

- name: Configure new API Gateway
  local_action: shell {{ aws_cmd }} apigateway create-rest-api --name hubapi --description 'GetInsured HUB Rest API' --endpoint-configuration types=REGIONAL
  register: create_api_results

- name: Print Create Api Results
  debug:
    var: create_api_results.stdout

- name: Print API id
  debug:
    msg: "New api id is {{ (create_api_results.stdout | from_json).id }}"

- name: Save new API id as variable
  set_fact:
    api_id: "{{ (create_api_results.stdout | from_json).id }}"

# TODO: Get single resource based on above api_id
- name: Get information of created api (get-resources)
  local_action: shell {{ aws_cmd }} apigateway get-resources --rest-api-id {{ api_id }} --region {{ aws_region }}
  register: resource_info

- name: Print resource info
  debug:
    var: resource_info.stdout

- name: Parse resource JSON
  set_fact:
    res_info: "{{ resource_info.stdout | from_json }}"

- name: Print Parent Id
  debug:
    msg: "Parent id is {{ res_info['items'][0].id }}"

- name: Save parent id as variable
  set_fact:
    parent_id: "{{ res_info['items'][0].id }}"

- name: Create Proxy Resource
  local_action: shell {{ aws_cmd }} apigateway create-resource --rest-api-id {{ api_id }} --region {{ aws_region }} --parent-id {{ parent_id }} --path-part {proxy+}
  register: proxy_resource

- name: Print proxy resource
  debug:
    msg: "Proxy resource: {{ proxy_resource.stdout | from_json }}"

- name: Save proxy resource id as variable
  set_fact:
    resource_id: "{{ (proxy_resource.stdout | from_json).id }}"

- name: Create Proxy Method
  local_action: shell {{ aws_cmd }} apigateway put-method --rest-api-id {{ api_id }} --region {{ aws_region }} --resource-id {{ resource_id }} --http-method ANY --authorization-type NONE --request-parameters '{"method.request.path.proxy":true}' --api-key-required
  register: proxy_method

- name: Print Proxy Method info
  debug:
    var: proxy_method.stdout

- name: Create Integration for proxy method
  local_action: shell {{ aws_cmd }} apigateway put-integration --rest-api-id {{ api_id }} --resource-id {{ resource_id }} --http-method ANY --integration-http-method ANY --type HTTP_PROXY --connection-type INTERNET --uri 'http://{{ ec2.instances[0].public_dns_name }}/{proxy}' --request-parameters '{"integration.request.path.proxy":"method.request.path.proxy"}'
  register: integration_results

- name: Print Integration Results
  debug:
    var: integration_results.stdout

- name: Create new API Deployment
  local_action: shell {{ aws_cmd }} apigateway create-deployment --stage-name {{ api_stage_name }} --rest-api-id {{ api_id }}
  register: api_deployment_results

- name: Print API Deployment Results
  debug:
    var: api_deployment_results.stdout

- name: Create API key
  local_action: shell {{ aws_cmd }} apigateway create-api-key --name 'hub-api-key' --description 'API Key for access of {{ api_stage_name }} stage' --enabled --stage-keys restApiId='{{ api_id }}',stageName='{{ api_stage_name}}'
  register: api_key_results

- name: Save Api key information
  set_fact:
    api_key_info: "{{ api_key_results.stdout | from_json }}"

- name: Print API key
  debug:
    var: api_key_results.stdout
    
- name: Test API
  uri:
    headers:
    x-api-key: '{{ api_key_info.value }}'
    url: "https://{{ api_id }}.execute-api.{{ aws_region }}.amazonaws.com/dev/actuator/health"
    return_content: yes
  register: api_test_results

- name: Print API Test Results
  debug:
    var: api_test_results
