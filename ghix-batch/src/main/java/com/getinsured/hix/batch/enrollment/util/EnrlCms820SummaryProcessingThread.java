package com.getinsured.hix.batch.enrollment.util;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;

import com.getinsured.hix.batch.enrollment.service.EnrlCms820Service;

public class EnrlCms820SummaryProcessingThread implements Callable<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlCms820SummaryProcessingThread.class);

	private Integer summaryId;
	private JobService jobService = null;
	private StepExecution stepExecution = null;
	
	public EnrlCms820SummaryProcessingThread(Integer summaryId, JobService jobService, StepExecution stepExecution,
			EnrlCms820Service enrlCms820Service) {
		super();
		this.summaryId = summaryId;
		this.jobService = jobService;
		this.stepExecution = stepExecution;
		this.enrlCms820Service = enrlCms820Service;
	}

	private EnrlCms820Service enrlCms820Service;

	@Override
	public String call() throws Exception {
		try {
			return enrlCms820Service.processLoadedData(summaryId, jobService, stepExecution);
		} catch (Exception e) {
			LOGGER.error("Exception occurred in EnrlCms820SummaryProcessingThread for summary Id: " + summaryId , e);
		}
		return null;
	}

}
