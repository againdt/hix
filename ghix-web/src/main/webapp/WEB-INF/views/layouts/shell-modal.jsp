<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.getinsured.hix.util.GhixConfiguration"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="author" content="" />
<title><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></title>

 <c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
 <link href="<gi:cdnurl value="/resources/css/bootstrap-responsive.min.css" />" rel="stylesheet"  type="text/css" />
<%--  <link href="<gi:cdnurl value="/resources/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" /> --%>
 <link href="<c:url value="/resources/css/bs3-responsive-classes.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<gi:cdnurl value="/resources/css/formvalidation.css" />" rel="stylesheet"  type="text/css" />
 <link href="<c:url value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />
 <link href="<gi:cdnurl value="/resources/css/ghixcustom.css" />" rel="stylesheet"  type="text/css" />
 <link href="/hix/resources/css/iframe.css" rel="stylesheet"  type="text/css" />
 
<!-- <link href="<gi:cdnurl value="/resources/css/docs.css" />" rel="stylesheet"  type="text/css" />-->
 <link href="<gi:cdnurl value="/resources/css/custom-theme/jquery-ui-1.10.4.custom.min.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<gi:cdnurl value="/resources/css/jquery.ui.datepicker.css" />" rel="stylesheet"  type="text/css" />		
 <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
 <link href="<gi:cdnurl value="/resources/css/accessibility.css" />" media="screen" rel="stylesheet" type="text/css" />

 
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery-1.7.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery-ui-1.8.16.custom.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/bootstrap.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/ghixcustom.js" />"></script>

 <!--[if lt IE 9]>
	<script type="text/javascript" src="<gi:cdnurl value="/resources/js/html5.js" />"></script>
 <![endif]-->
</head>
<body class="shellmodal-body">	
		<input type="hidden" name="exchangeName" id="exchangeName" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>">	
		<div id="iframeMain">
			<tiles:insertAttribute name="main" />
		</div>		
</body>
</html>	