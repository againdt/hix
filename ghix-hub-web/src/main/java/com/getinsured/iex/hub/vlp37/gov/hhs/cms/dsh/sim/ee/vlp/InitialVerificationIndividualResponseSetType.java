//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.12.26 at 10:33:01 AM IST 
//


package com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpsda.ArrayOfSponsorshipDataType;




/**
 * <p>Java class for InitialVerificationIndividualResponseSetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InitialVerificationIndividualResponseSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}CaseNumber"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitLastName" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitFirstName" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitMiddleName" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitBirthDate" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitEntryDate" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}AdmittedToDate" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}AdmittedToText" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitCountryBirthCd" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitCountryCitCd" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitCoaCode" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitProvOfLaw" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NonCitEadsExpireDate" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}EligStatementCd"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}EligStatementTxt"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}IAVTypeCode" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}IAVTypeTxt" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}WebServSftwrVer"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}GrantDate" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}GrantDateReasonCd" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}SponsorDataFoundIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://vlpsda.ee.sim.dsh.cms.hhs.gov}ArrayOfSponsorshipData" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}SponsorshipReasonCd" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}AgencyAction"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}FiveYearBarApplyCode"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}QualifiedNonCitizenCode"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}FiveYearBarMetCode"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}USCitizenCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InitialVerificationIndividualResponseSetType", propOrder = {
    "caseNumber",
    "nonCitLastName",
    "nonCitFirstName",
    "nonCitMiddleName",
    "nonCitBirthDate",
    "nonCitEntryDate",
    "admittedToDate",
    "admittedToText",
    "nonCitCountryBirthCd",
    "nonCitCountryCitCd",
    "nonCitCoaCode",
    "nonCitProvOfLaw",
    "nonCitEadsExpireDate",
    "eligStatementCd",
    "eligStatementTxt",
    "iavTypeCode",
    "iavTypeTxt",
    "webServSftwrVer",
    "grantDate",
    "grantDateReasonCd",
    "sponsorDataFoundIndicator",
    "arrayOfSponsorshipData",
    "sponsorshipReasonCd",
    "agencyAction",
    "fiveYearBarApplyCode",
    "qualifiedNonCitizenCode",
    "fiveYearBarMetCode",
    "usCitizenCode"
})
public class InitialVerificationIndividualResponseSetType {

    @XmlElement(name = "CaseNumber", required = true)
    protected String caseNumber;
    @XmlElement(name = "NonCitLastName")
    protected String nonCitLastName;
    @XmlElement(name = "NonCitFirstName")
    protected String nonCitFirstName;
    @XmlElement(name = "NonCitMiddleName")
    protected String nonCitMiddleName;
    @XmlElement(name = "NonCitBirthDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nonCitBirthDate;
    @XmlElement(name = "NonCitEntryDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nonCitEntryDate;
    @XmlElement(name = "AdmittedToDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar admittedToDate;
    @XmlElement(name = "AdmittedToText")
    protected String admittedToText;
    @XmlElement(name = "NonCitCountryBirthCd")
    protected String nonCitCountryBirthCd;
    @XmlElement(name = "NonCitCountryCitCd")
    protected String nonCitCountryCitCd;
    @XmlElement(name = "NonCitCoaCode")
    protected String nonCitCoaCode;
    @XmlElement(name = "NonCitProvOfLaw")
    protected String nonCitProvOfLaw;
    @XmlElement(name = "NonCitEadsExpireDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nonCitEadsExpireDate;
    @XmlElement(name = "EligStatementCd", required = true)
    protected BigInteger eligStatementCd;
    @XmlElement(name = "EligStatementTxt", required = true)
    protected String eligStatementTxt;
    @XmlElement(name = "IAVTypeCode")
    protected String iavTypeCode;
    @XmlElement(name = "IAVTypeTxt")
    protected String iavTypeTxt;
    @XmlElement(name = "WebServSftwrVer", required = true)
    protected String webServSftwrVer;
    @XmlElement(name = "GrantDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar grantDate;
    @XmlElement(name = "GrantDateReasonCd")
    protected String grantDateReasonCd;
    @XmlElement(name = "SponsorDataFoundIndicator")
    protected Boolean sponsorDataFoundIndicator;
    @XmlElement(name = "ArrayOfSponsorshipData", namespace = "http://vlpsda.ee.sim.dsh.cms.hhs.gov")
    protected ArrayOfSponsorshipDataType arrayOfSponsorshipData;
    @XmlElement(name = "SponsorshipReasonCd")
    protected String sponsorshipReasonCd;
    @XmlElement(name = "AgencyAction", required = true)
    protected String agencyAction;
    @XmlElement(name = "FiveYearBarApplyCode", required = true)
    protected String fiveYearBarApplyCode;
    @XmlElement(name = "QualifiedNonCitizenCode", required = true)
    protected String qualifiedNonCitizenCode;
    @XmlElement(name = "FiveYearBarMetCode", required = true)
    protected String fiveYearBarMetCode;
    @XmlElement(name = "USCitizenCode", required = true)
    protected String usCitizenCode;

    /**
     * Gets the value of the caseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseNumber() {
        return caseNumber;
    }

    /**
     * Sets the value of the caseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseNumber(String value) {
        this.caseNumber = value;
    }

    /**
     * Gets the value of the nonCitLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitLastName() {
        return nonCitLastName;
    }

    /**
     * Sets the value of the nonCitLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitLastName(String value) {
        this.nonCitLastName = value;
    }

    /**
     * Gets the value of the nonCitFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitFirstName() {
        return nonCitFirstName;
    }

    /**
     * Sets the value of the nonCitFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitFirstName(String value) {
        this.nonCitFirstName = value;
    }

    /**
     * Gets the value of the nonCitMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitMiddleName() {
        return nonCitMiddleName;
    }

    /**
     * Sets the value of the nonCitMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitMiddleName(String value) {
        this.nonCitMiddleName = value;
    }

    /**
     * Gets the value of the nonCitBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNonCitBirthDate() {
        return nonCitBirthDate;
    }

    /**
     * Sets the value of the nonCitBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNonCitBirthDate(XMLGregorianCalendar value) {
        this.nonCitBirthDate = value;
    }

    /**
     * Gets the value of the nonCitEntryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNonCitEntryDate() {
        return nonCitEntryDate;
    }

    /**
     * Sets the value of the nonCitEntryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNonCitEntryDate(XMLGregorianCalendar value) {
        this.nonCitEntryDate = value;
    }

    /**
     * Gets the value of the admittedToDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAdmittedToDate() {
        return admittedToDate;
    }

    /**
     * Sets the value of the admittedToDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAdmittedToDate(XMLGregorianCalendar value) {
        this.admittedToDate = value;
    }

    /**
     * Gets the value of the admittedToText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdmittedToText() {
        return admittedToText;
    }

    /**
     * Sets the value of the admittedToText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdmittedToText(String value) {
        this.admittedToText = value;
    }

    /**
     * Gets the value of the nonCitCountryBirthCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitCountryBirthCd() {
        return nonCitCountryBirthCd;
    }

    /**
     * Sets the value of the nonCitCountryBirthCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitCountryBirthCd(String value) {
        this.nonCitCountryBirthCd = value;
    }

    /**
     * Gets the value of the nonCitCountryCitCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitCountryCitCd() {
        return nonCitCountryCitCd;
    }

    /**
     * Sets the value of the nonCitCountryCitCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitCountryCitCd(String value) {
        this.nonCitCountryCitCd = value;
    }

    /**
     * Gets the value of the nonCitCoaCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitCoaCode() {
        return nonCitCoaCode;
    }

    /**
     * Sets the value of the nonCitCoaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitCoaCode(String value) {
        this.nonCitCoaCode = value;
    }

    /**
     * Gets the value of the nonCitProvOfLaw property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonCitProvOfLaw() {
        return nonCitProvOfLaw;
    }

    /**
     * Sets the value of the nonCitProvOfLaw property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonCitProvOfLaw(String value) {
        this.nonCitProvOfLaw = value;
    }

    /**
     * Gets the value of the nonCitEadsExpireDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNonCitEadsExpireDate() {
        return nonCitEadsExpireDate;
    }

    /**
     * Sets the value of the nonCitEadsExpireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNonCitEadsExpireDate(XMLGregorianCalendar value) {
        this.nonCitEadsExpireDate = value;
    }

    /**
     * Gets the value of the eligStatementCd property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEligStatementCd() {
        return eligStatementCd;
    }

    /**
     * Sets the value of the eligStatementCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEligStatementCd(BigInteger value) {
        this.eligStatementCd = value;
    }

    /**
     * Gets the value of the eligStatementTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEligStatementTxt() {
        return eligStatementTxt;
    }

    /**
     * Sets the value of the eligStatementTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEligStatementTxt(String value) {
        this.eligStatementTxt = value;
    }

    /**
     * Gets the value of the iavTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIAVTypeCode() {
        return iavTypeCode;
    }

    /**
     * Sets the value of the iavTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIAVTypeCode(String value) {
        this.iavTypeCode = value;
    }

    /**
     * Gets the value of the iavTypeTxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIAVTypeTxt() {
        return iavTypeTxt;
    }

    /**
     * Sets the value of the iavTypeTxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIAVTypeTxt(String value) {
        this.iavTypeTxt = value;
    }

    /**
     * Gets the value of the webServSftwrVer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebServSftwrVer() {
        return webServSftwrVer;
    }

    /**
     * Sets the value of the webServSftwrVer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebServSftwrVer(String value) {
        this.webServSftwrVer = value;
    }

    /**
     * Gets the value of the grantDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGrantDate() {
        return grantDate;
    }

    /**
     * Sets the value of the grantDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGrantDate(XMLGregorianCalendar value) {
        this.grantDate = value;
    }

    /**
     * Gets the value of the grantDateReasonCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrantDateReasonCd() {
        return grantDateReasonCd;
    }

    /**
     * Sets the value of the grantDateReasonCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrantDateReasonCd(String value) {
        this.grantDateReasonCd = value;
    }

    /**
     * Gets the value of the sponsorDataFoundIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSponsorDataFoundIndicator() {
        return sponsorDataFoundIndicator;
    }

    /**
     * Sets the value of the sponsorDataFoundIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSponsorDataFoundIndicator(Boolean value) {
        this.sponsorDataFoundIndicator = value;
    }

    /**
     * Gets the value of the arrayOfSponsorshipData property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSponsorshipDataType }
     *     
     */
    public ArrayOfSponsorshipDataType getArrayOfSponsorshipData() {
        return arrayOfSponsorshipData;
    }

    /**
     * Sets the value of the arrayOfSponsorshipData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSponsorshipDataType }
     *     
     */
    public void setArrayOfSponsorshipData(ArrayOfSponsorshipDataType value) {
        this.arrayOfSponsorshipData = value;
    }

    /**
     * Gets the value of the sponsorshipReasonCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSponsorshipReasonCd() {
        return sponsorshipReasonCd;
    }

    /**
     * Sets the value of the sponsorshipReasonCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSponsorshipReasonCd(String value) {
        this.sponsorshipReasonCd = value;
    }

    /**
     * Gets the value of the agencyAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencyAction() {
        return agencyAction;
    }

    /**
     * Sets the value of the agencyAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencyAction(String value) {
        this.agencyAction = value;
    }

    /**
     * Gets the value of the fiveYearBarApplyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiveYearBarApplyCode() {
        return fiveYearBarApplyCode;
    }

    /**
     * Sets the value of the fiveYearBarApplyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiveYearBarApplyCode(String value) {
        this.fiveYearBarApplyCode = value;
    }

    /**
     * Gets the value of the qualifiedNonCitizenCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualifiedNonCitizenCode() {
        return qualifiedNonCitizenCode;
    }

    /**
     * Sets the value of the qualifiedNonCitizenCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualifiedNonCitizenCode(String value) {
        this.qualifiedNonCitizenCode = value;
    }

    /**
     * Gets the value of the fiveYearBarMetCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiveYearBarMetCode() {
        return fiveYearBarMetCode;
    }

    /**
     * Sets the value of the fiveYearBarMetCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiveYearBarMetCode(String value) {
        this.fiveYearBarMetCode = value;
    }

    /**
     * Gets the value of the usCitizenCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSCitizenCode() {
        return usCitizenCode;
    }

    /**
     * Sets the value of the usCitizenCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSCitizenCode(String value) {
        this.usCitizenCode = value;
    }

}
