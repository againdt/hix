<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>


<div class="gutter10-lr">
	<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
	<div class="l-page-breadcrumb" style="margin-top:10px;">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a href="<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>"><spring:message code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
			</ul>
			<div style="font-size: 14px; color: red">
				<c:if test="${errorMsg != ''}">
					<p>
						<c:out value="${errorMsg}"></c:out>
					<p />
				</c:if>
				<br>
			</div>
			
		</div><!--  end of row-fluid -->
	</div><!--  end l-page-breadcrumb -->
	<div class="row-fluid">
		<h1><a name="skip"></a>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
	</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header"></div>
			<ul class="nav nav-list">						
				<li><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
				<li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
				<li><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>
				
				<li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>	
				<li class="active"><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>	
				<%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li>	 --%>
			</ul>
			<br>
			<div class="header">
				<h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
			</div>
			<ul class="nav nav-list">
			<c:if test="${checkDilog == null}">  
				<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
			</c:if>
			<c:if test="${checkDilog != null}">  
				<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${encSwitchToResourceFullName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
			</c:if>
            </ul>   
		</div><!-- end of span3 -->
       <div class="span9">	   	   
			<div class="margin5-b header">
				<h4 class="pull-left"><spring:message code="label.brkTicketHistory"></spring:message></h4>
			</div>
			<c:choose>
				<c:when test="${fn:length(ticketHistoryList) > 0}">
					<display:table id="ticketHistory" name="ticketHistoryList"  pagesize="${pageSize}" requestURI="" sort="list" class="table table-condensed table-border-none table-striped">
						 <display:column titleKey="label.bkr.TicketId" sortable="true" headerClass="graydrkbg">  
      				 	 <c:set var="id" value="${ticketHistory.id}" />  
       					 <a href="../ticketdetail/${id}?brokerId=${broker.id}">${ticketHistory.number} </a>   
   						 </display:column>  
						<display:column property="subject" titleKey="label.bkr.Subject" sortable="true" headerClass="graydrkbg" />
						<display:column property="status" titleKey="label.brkStatus" sortable="true" headerClass="graydrkbg" />
						<display:column property="created" titleKey="label.bkr.CreatedDate" format="{0,date,MMM dd, yyyy}" sortable="true" headerClass="graydrkbg" />
						<display:column property="updated" titleKey="label.bkr.CloseDate" format="{0,date,MMM dd, yyyy}" sortable="true" headerClass="graydrkbg" />
					    <display:setProperty name="paging.banner.placement" value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
			           <display:setProperty name="paging.banner.last" value=''/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
			           <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.one_item_found" value=''/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
			           <div class="pagination center">
						<ul>
							<li>{0}</li>
							<li><a href="{3}">Next</a></li>
						</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
						<div class="pagination center">
							<ul>
								<li><a href="{2}">Prev</a></li>
								<li>{0}</li>
							</ul>
						</div>
					</span>'/>
					<display:setProperty name="paging.banner.full" value='
						<div class="pagination center">
							<ul>
								<li><a href="{2}">Prev</a></li>
								<li>{0}</li>
								<li><a href="{3}">Next</a></li>
							</ul>
						</div>
						'/>
					</display:table>
				</c:when>
				<c:otherwise>
					<div class="alert alert-info">
						<spring:message code='label.norecords' />
					</div>
				</c:otherwise>
			</c:choose>
		</div>
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->


<script type="text/javascript">
function saveComment(ticketId)
{
	var validateUrl = "<c:url value='/admin/ticketmgmt/savecomment'/>";
	var commentData = $("#comment_text").val();
	$.ajax({
		url: validateUrl,
		data: {ticketId : ticketId,
			comment : commentData},
		success: function(response)
		{
			if(response)
			{
				$('#markCompleteDialog').modal('hide');
			}
		}
		});
}
</script>
<%-- Modal for switch user role --%>
  <c:set scope="request" var="switchToModuleId" value="${broker.id}"></c:set>
  <c:set scope="request" var="switchToResourceName" value="${encSwitchToResourceFullName}"></c:set>
  <c:set scope="request" var="currSwitchToResourceFullName" value="${unEncSwitchToResourceFullName}"></c:set>
 
  <jsp:include page="admintobrokerswitch.jsp"></jsp:include>
<%--Modal ends --%>