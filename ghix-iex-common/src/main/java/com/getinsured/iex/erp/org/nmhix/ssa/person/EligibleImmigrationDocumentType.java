package com.getinsured.iex.erp.org.nmhix.ssa.person;

/**
 * @author chopra_s
 * 
 */
public class EligibleImmigrationDocumentType {
	private boolean I551Indicator = false;
	private boolean TemporaryI551StampIndicator = false;
	private boolean MachineReadableVisaIndicator = false;
	private boolean I766Indicator = false;
	private boolean I94Indicator = false;
	private boolean I94InPassportIndicator = false;
	private boolean I797Indicator = false;
	private boolean UnexpiredForeignPassportIndicator = false;
	private boolean I327Indicator = false;
	private boolean I571Indicator = false;
	private boolean I20Indicator = false;
	private boolean DS2019Indicator = false;
	private boolean OtherDocumentTypeIndicator = false;

	public boolean isI551Indicator() {
		return I551Indicator;
	}

	public void setI551Indicator(boolean i551Indicator) {
		I551Indicator = i551Indicator;
	}

	public boolean isTemporaryI551StampIndicator() {
		return TemporaryI551StampIndicator;
	}

	public void setTemporaryI551StampIndicator(
			boolean temporaryI551StampIndicator) {
		TemporaryI551StampIndicator = temporaryI551StampIndicator;
	}

	public boolean isMachineReadableVisaIndicator() {
		return MachineReadableVisaIndicator;
	}

	public void setMachineReadableVisaIndicator(
			boolean machineReadableVisaIndicator) {
		MachineReadableVisaIndicator = machineReadableVisaIndicator;
	}

	public boolean isI766Indicator() {
		return I766Indicator;
	}

	public void setI766Indicator(boolean i766Indicator) {
		I766Indicator = i766Indicator;
	}

	public boolean isI94Indicator() {
		return I94Indicator;
	}

	public void setI94Indicator(boolean i94Indicator) {
		I94Indicator = i94Indicator;
	}

	public boolean isI94InPassportIndicator() {
		return I94InPassportIndicator;
	}

	public void setI94InPassportIndicator(boolean i94InPassportIndicator) {
		I94InPassportIndicator = i94InPassportIndicator;
	}

	public boolean isI797Indicator() {
		return I797Indicator;
	}

	public void setI797Indicator(boolean i797Indicator) {
		I797Indicator = i797Indicator;
	}

	public boolean isUnexpiredForeignPassportIndicator() {
		return UnexpiredForeignPassportIndicator;
	}

	public void setUnexpiredForeignPassportIndicator(
			boolean unexpiredForeignPassportIndicator) {
		UnexpiredForeignPassportIndicator = unexpiredForeignPassportIndicator;
	}

	public boolean isI327Indicator() {
		return I327Indicator;
	}

	public void setI327Indicator(boolean i327Indicator) {
		I327Indicator = i327Indicator;
	}

	public boolean isI571Indicator() {
		return I571Indicator;
	}

	public void setI571Indicator(boolean i571Indicator) {
		I571Indicator = i571Indicator;
	}

	public boolean isI20Indicator() {
		return I20Indicator;
	}

	public void setI20Indicator(boolean i20Indicator) {
		I20Indicator = i20Indicator;
	}

	public boolean isDS2019Indicator() {
		return DS2019Indicator;
	}

	public void setDS2019Indicator(boolean dS2019Indicator) {
		DS2019Indicator = dS2019Indicator;
	}

	public boolean isOtherDocumentTypeIndicator() {
		return OtherDocumentTypeIndicator;
	}

	public void setOtherDocumentTypeIndicator(boolean otherDocumentTypeIndicator) {
		OtherDocumentTypeIndicator = otherDocumentTypeIndicator;
	}

}
