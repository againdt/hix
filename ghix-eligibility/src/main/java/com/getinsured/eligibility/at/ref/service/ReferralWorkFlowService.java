package com.getinsured.eligibility.at.ref.service;

import java.util.Map;

/**
 * @author chopra_s
 *
 */
public interface ReferralWorkFlowService {
	Map<String, String> processRidpAndLinking(final long referralActivationId, int userId,int cmrId);
}
