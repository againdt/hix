package com.getinsured.consumer.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import com.getinsured.timeshift.util.TSDate;

import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.consumer.repository.IConsumerCallLogsRepository;
import com.getinsured.consumer.repository.IHouseholdEnrollmentRepository;
import com.getinsured.consumer.repository.IHouseholdRepository;
import com.getinsured.consumer.repository.ILocationRepository;
import com.getinsured.consumer.repository.IMemberRepository;
import com.getinsured.consumer.repository.IPrescriptionRepository;
import com.getinsured.consumer.repository.ISsapApplicantRepository;
import com.getinsured.consumer.repository.ISsapApplicationRepository;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.hix.dto.consumer.ConsumerRequestDTO;
import com.getinsured.hix.dto.consumer.HouseholdRequestDTO;
import com.getinsured.hix.dto.consumer.MemberDTO;
import com.getinsured.hix.dto.consumer.MemberPortalRequestDTO;
import com.getinsured.hix.dto.consumer.MemberPortalResponseDTO;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.ConsumerCallLog;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.FFMHouseholdResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.consumer.HouseholdEnrollment;
import com.getinsured.hix.model.consumer.ManageApplicantsDto;
import com.getinsured.hix.model.consumer.Member;
import com.getinsured.hix.model.consumer.Prescription;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

@Service("consumerService")
@SuppressWarnings("rawtypes")
public class ConsumerServiceImpl implements ConsumerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerServiceImpl.class);
	private static final String NAME = "name";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String HOUSEHOLD_LIST = "householdList";
	private static final String PHONE_NUMBER = "phoneNumber";
	private static final String ZIPCODE = "zipCode";
	private static final String RECORD_COUNT = "recordCount";
	private static final String RELATIONSHIP = "RELATIONSHIP";
	private static final int THREE = 3;
	private static final int SIX = 6;
	private static final int TEN = 10;
	private static final String SORT_BY = "sortBy";
	private static final String SORT_ORDER = "sortOrder";
	private static final String MODULE_NAME = "moduleName";
	private static final String MODULE_ID = "moduleId";
	private static final String STATUS = "status";
	private static final String REQUEST_SENT_FROM = "requestSentFrom";
	private static final String QUERY_STRING_SEPERATOR = ", '";
	private static final String DATE_FORMAT_MM_DD_YYYY = "mm/dd/yyyy";
	private static final String REQUEST_SENT_TO = "requestSentTo";
	private static final String INACTIVE_DATE_FROM = "inactiveDateFrom";
	private static final String INACTIVE_DATE_TO = "inactiveDateTo";
	private static final String ELIGIBILITY_STATUS = "eligibilityStatus";	
	private static final String APPLICATION_STATUS = "applicationStatus";
	private static final String TOTAL_RECORDS = "totalRecords";
	private static final String CONSUMERS = "consumers";
	private static final String BROKER = "BROKER";
	private static final String ASSISTER = "ASSISTER";
	private static final String ENTITY = "ENTITY";	
	private static final String SEARCH_CRITERIA = "searchCriteria";
	private static final String INDIVIDUAL_FIRST_OR_LAST_NAME = "indFirstOrLastName";
	private static final String CEC_FIRST_OR_LAST_NAME = "cecFirstOrLastName";
	private static final String INDIVIDUAL_ID = "individualId";
	
	@Autowired
	private IMemberRepository memberRepository;
	@Autowired
	private IHouseholdRepository householdRepository;
	@Autowired
	private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired
	private ILocationRepository locationRepository;
	@Autowired 
	private LookupService lookupService;
	@Autowired
	private IHouseholdEnrollmentRepository householdEnrollmentRepository;
	@Autowired
	private IConsumerCallLogsRepository callLogsRepository;
	
	@PersistenceUnit private EntityManagerFactory emf;
	
	@Autowired
	private IPrescriptionRepository prescriptionRepository;

	@Autowired ISsapApplicationRepository iSsapApplicationRepository;
	@Autowired ISsapApplicantRepository iSsapApplicantRepository;
	@Autowired ILocationRepository iLocationRepository;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	/**
	 * @author Sneha
	 * @param id
	 * @return Member below method finds Member by Id
	 */

	@Override
    public Member findMemberById(int id) {
		return memberRepository.findOne(id);
	}

	/**
	 * @author Sneha
	 * @param Member
	 * @return Member below method saves Member
	 */
    @Override
    @Transactional
	public Member saveMember(Member member) {
		return memberRepository.save(member);
	}

	/**
	 * @author Suhasini
	 * @param Household
	 * @return List<Member> below method saves Member
	 */
	public List<Member> findByHousehold(Household household) {
		return memberRepository.findByHousehold(household);
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public Map<String, Object> searchApplicants(Map<String, Object> searchCriteria) {
		Map<String, Object> mapApplicantData =  new HashMap<>();
		Map<String, Object> mapQueryParam =  new HashMap<>();
		Long recordCount = (long) 0.0;
		List<Object[]> resultList = null;
		List<ManageApplicantsDto> applicantList = new ArrayList<ManageApplicantsDto>();
		
		String selectClause = 
				" SELECT DISTINCT APPLICATIONS.CMR_HOUSEOLD_ID , APPLICANT1.FIRST_NAME, APPLICANT1.LAST_NAME, "
				+ " APPLICANT1.SSN , APPLICANT1.PERSON_ID , APPLICANT1.SSAP_APPLICATION_ID, " 
				+ " APPLICANT1.EXTERNAL_APPLICANT_ID, APPLICATIONS.EXTERNAL_APPLICATION_ID, APPLICANT1.PHONE_NUMBER , "
				+ " APPLICATIONS.SOURCE , (APPLICANT2.FIRST_NAME||' '||APPLICANT2.LAST_NAME) , APPLICATIONS.CASE_NUMBER , "
				+ " APPLICATIONS.APPLICATION_STATUS , APPLICATIONS.COVERAGE_YEAR , APPLICANT1.APPLICANT_GUID , APPLICATIONS.FINANCIAL_ASSISTANCE_FLAG,"
				+ " lower(APPLICANT1.FIRST_NAME||' '|| APPLICANT1.LAST_NAME ) AS APPLICANT_FULL_NAME,"
				+ " APPLICATIONS.APPLICATION_DATA";  

		String selectCountClause = "SELECT count(distinct APPLICANT1.ID) ";
		/*FROM SSAP_APPLICANTS appl, 
		SSAP_APPLICATIONS sap INNER JOIN SSAP_APPLICANTS appl2 on appl2.ssap_application_id =sap.id AND appl2.person_id = 1 
		(select distinct (p.firstName || ' ' || p.lastName) FROM SsapApplicant p where p.ssapApplication =appl.ssapApplication.id AND p.personId = 1
		*/
		String fromClause = " FROM SSAP_APPLICANTS APPLICANT1  "
				+ " INNER JOIN SSAP_APPLICATIONS APPLICATIONS ON (APPLICANT1.SSAP_APPLICATION_ID = APPLICATIONS.ID)  "
				+ " INNER JOIN SSAP_APPLICANTS APPLICANT2 ON (APPLICANT1.SSAP_APPLICATION_ID = APPLICANT2.SSAP_APPLICATION_ID "
				+ " AND APPLICANT2.PERSON_ID = 1)";

		
		// join with entollment table if search params are present.
		if(searchCriteria.get("enrollmentId") != null || searchCriteria.get("enrollmentStatus") != null){
			fromClause += " INNER JOIN ENROLLMENT EN ON EN.SSAP_APPLICATION_ID = APPLICATIONS.ID " ;
		}
		
		if(searchCriteria.get("externalCaseId") != null){
			fromClause += " INNER JOIN CMR_HOUSEHOLD CMR ON CMR.ID = APPLICATIONS.CMR_HOUSEOLD_ID " ;
		}
		
		
		String whereClause = applyFilters(searchCriteria, mapQueryParam);

		StringBuilder query = new StringBuilder(selectClause);
		query.append(fromClause);
		query.append(whereClause);
		
		String sortOrderClause = formSortByClause(searchCriteria);
		String paginatedQuery = formPaginationQuery(searchCriteria, query, sortOrderClause);
		
		EntityManager em = null;
		try {					
			
			//Execute the query, get the records as per search criteria.
			em = emf.createEntityManager();
			Query nativeQuery = em.createNativeQuery(paginatedQuery.toString());
			setNamedQueryParams(nativeQuery, mapQueryParam);
			// Set the paging criteria		
			resultList = nativeQuery.getResultList();
			
			if(!resultList.isEmpty()) {
				
				applicantList=formApplicantDetails(resultList);
				query.replace(query.indexOf(selectClause), selectClause.length(), selectCountClause);

				Query nativeCountQuery = em.createNativeQuery(query.toString());
				setNamedQueryParams(nativeCountQuery, mapQueryParam);
				recordCount =  ((BigInteger)nativeCountQuery.getSingleResult()).longValue();
			}
			
		} catch (Exception e) {
			LOGGER.error("Unable to search applicants", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
			
		mapApplicantData.put("applicantList", applicantList);
		mapApplicantData.put("recordCount", recordCount);
		
		String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
		mapApplicantData.put("exchangeType", exchangeType);
		
		return mapApplicantData;
	}

	private String formPaginationQuery(Map<String, Object> searchCriteria, StringBuilder query, String sortOrderClause) {
		Integer pageSize = (Integer) searchCriteria.get("pageSize");
		Integer startIndex = (Integer) searchCriteria.get("startRecord");
				
		return  query.toString() + " " + sortOrderClause + " OFFSET " + startIndex.intValue() + " LIMIT " + pageSize.intValue();
	}
	
	private String formSortByClause(Map<String, Object> searchCriteria) {		
		String columnName = searchCriteria.get("sortBy").toString();				
		String sortOrder = searchCriteria.get("sortOrder").toString();
		
		sortOrder = (("DESC").compareToIgnoreCase(sortOrder) == 0) ? " DESC " : " ASC ";
		String sortBy = StringUtils.EMPTY;
		if(!StringUtils.isEmpty(columnName)) {
			if(("firstName").equalsIgnoreCase(columnName)) {
				sortBy = " APPLICANT_FULL_NAME ";
			}
		}
		String sortOrderClause = " ORDER BY " + sortBy + " " + sortOrder;
		return sortOrderClause;
	}
	private List<ManageApplicantsDto> formApplicantDetails(List<Object[]> rsList) {
		List<ManageApplicantsDto> applicantList = new ArrayList<ManageApplicantsDto>();
		for (Object result : rsList) {  
			ManageApplicantsDto eachApplicant = new ManageApplicantsDto();
			Object[] r = (Object[]) result;
			eachApplicant.setHouseholdId((BigDecimal)r[0]);
			eachApplicant.setFirstName((String)r[1]);
			eachApplicant.setLastName((String)r[2]);
			eachApplicant.setSsn((String)r[3]);
			if(r[4] != null){
				eachApplicant.setPersonId(((BigDecimal)r[4]).longValue());	
			}
			else{
				eachApplicant.setPersonId(0);
			}
			if(r[5] != null){
				eachApplicant.setSsapApplicationId(((BigDecimal)r[5]).longValue());
			}
			else{
				eachApplicant.setSsapApplicationId(0);
			}
			eachApplicant.setExternalApplicantId((String)r[6]);
			eachApplicant.setExternalApplicationId((String)r[7]);
			eachApplicant.setPhoneNumber((String)r[8]);
			eachApplicant.setSource((String)r[9]);
			eachApplicant.setPrimaryContactName((String)r[10]);
			eachApplicant.setCaseNumber((String)r[11]);
			eachApplicant.setApplicationStatus((String)r[12]);
			eachApplicant.setApplicationYear(String.valueOf(r[13]));
			eachApplicant.setApplicantGuidCode((String)r[14]);
			eachApplicant.setFinancialAssistanceFlag((String)r[15]);
			String applicationStatus =  this.getAppStatusDescription((String)r[12]);
		    eachApplicant.setAppStatusDescription(applicationStatus);
			
			SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson((String)r[17]);
			String authRepName = "";
			if(null != applicationData && null!=applicationData.getAuthorizedRepresentativeIndicator() && (applicationData.getAuthorizedRepresentativeIndicator() == true)){
				authRepName = applicationData.getAuthorizedRepresentative().getName().getFirstName() + " " + applicationData.getAuthorizedRepresentative().getName().getLastName();
			}
			eachApplicant.setAuthRepName(authRepName);
			
			applicantList.add(eachApplicant);
		}
		return applicantList;
	}
	
	public String getAppStatusDescription(String val) {
		for(ApplicationStatus status : ApplicationStatus.values()) {
			if(status.toString().equals(val)){
				return status.name();
			}
		}
		return "";
	}

	private String applyFilters(Map<String, Object> searchCriteria, Map<String, Object> mapwhereClauseParam) {
		StringBuilder whereClause = new StringBuilder();
		String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
		// filter by firstName
		String firstName = (String) searchCriteria.get("firstName");
		if (StringUtils.isNotEmpty(firstName)) {
			
			whereClause.append(" AND (lower(APPLICANT1.FIRST_NAME) like  :").append("firstName").append(") ");
			mapwhereClauseParam.put("firstName", firstName.toLowerCase() + "%");
		}
		
		// filter by firstName
		String lastName = (String) searchCriteria.get("lastName");
		if (StringUtils.isNotEmpty(lastName)) {
			whereClause.append(" AND  (lower(APPLICANT1.LAST_NAME) like :").append("lastName").append(") ");
			mapwhereClauseParam.put("lastName", lastName.toLowerCase() + "%");
		}
		
		// filter by contactNumber by
		String contactNumber = (String) searchCriteria.get("contactNumber");
		if (StringUtils.isNotEmpty(contactNumber)) {
			try {
				if (contactNumber.length() == TEN) {
					contactNumber = String.format("(%s)%s-%s",
							contactNumber.substring(0, THREE),
							contactNumber.substring(THREE, SIX),
							contactNumber.substring(SIX, TEN));
				}
				
				whereClause.append(" AND (APPLICANT1.PHONE_NUMBER in ('" + (String) searchCriteria.get("contactNumber") + "' ,'" + contactNumber +"' ))" );
				
			} catch (IllegalFormatException ex) {
				LOGGER.error("ConsumerServiceImpl searchHousehold phoneNumberFormat error:", ex);
			}
		}
		
		// filter by SSN (last 4 chars)
		String strSSN = (String) searchCriteria.get("ssn");
		if (StringUtils.isNotEmpty(strSSN)) {
			whereClause.append(" AND (APPLICANT1.SSN like :").append("strSSN").append(") "); 
			mapwhereClauseParam.put("strSSN",  "%" + strSSN);
		}
		
		// filter by ssapAppId
		String ssapAppId = (String) searchCriteria.get("appId");
		if (StringUtils.isNotEmpty(ssapAppId)) {
			whereClause.append(" AND (lower(APPLICATIONS.CASE_NUMBER) like :").append("ssapAppId").append(") ");
			mapwhereClauseParam.put("ssapAppId", ssapAppId.toLowerCase() + "%");
		}
		
		// filter by externalApplicantId
		String extAppId = (String) searchCriteria.get("extAppId");
		if (StringUtils.isNotEmpty(extAppId)) {
			if(!"both".equalsIgnoreCase(exchangeType)){
				whereClause.append(" AND (APPLICANT1.EXTERNAL_APPLICANT_ID like :").append("extAppId").append(") ");
				mapwhereClauseParam.put("extAppId", extAppId + "%");
			}
			else{
				whereClause.append(" AND (APPLICATIONS.EXTERNAL_APPLICATION_ID like :").append("extAppId").append(") ");
				mapwhereClauseParam.put("extAppId", extAppId + "%");
			}
		}
		
		//filter by externalApplicantId field
		String externalApplicantId = (String) searchCriteria.get("externalApplicantId");
		if (StringUtils.isNotEmpty(externalApplicantId)) {
			whereClause.append(" AND (lower(APPLICANT1.EXTERNAL_APPLICANT_ID) like :").append("externalApplicantId").append(") ");
			mapwhereClauseParam.put("externalApplicantId", externalApplicantId.toLowerCase() + "%");
		}
		
		// filter by application source
		String appSource = (String) searchCriteria.get("appSource");
		if (StringUtils.isNotEmpty(appSource)) {
			whereClause.append("AND APPLICATIONS.source like :").append("appSource").append(" ");
			mapwhereClauseParam.put("appSource", appSource);
		}
				// filter by access code.
		String accessCode = (String) searchCriteria.get("accessCode");
		if (StringUtils.isNotEmpty(accessCode)) {
			whereClause.append(" AND APPLICANT1.APPLICANT_GUID= :").append("accessCode").append(" ");
			mapwhereClauseParam.put("accessCode", accessCode);
		}
				
				// filter by effective year
			String effectiveYear = (String) searchCriteria.get("effectiveYear");
			if (StringUtils.isNotEmpty(effectiveYear) && !("All".equalsIgnoreCase(effectiveYear))) {
				whereClause.append(" AND APPLICATIONS.coverage_year = :" ).append("effectiveYear").append(" ");
				mapwhereClauseParam.put("effectiveYear", Integer.parseInt(effectiveYear));
			}
			
			String dateOfBirth = (String) searchCriteria.get("dateOfBirth");
			if (StringUtils.isNotEmpty(dateOfBirth)) {
				whereClause.append(" AND APPLICANT1.BIRTH_DATE = to_date(:").append("dateOfBirth").append(", 'MM/dd/yyyy')").append(" ") ;
				mapwhereClauseParam.put("dateOfBirth", dateOfBirth);
			}
			
			String enrollmentId = (String)searchCriteria.get("enrollmentId");
			if(StringUtils.isNotEmpty(enrollmentId)){
				whereClause.append(" AND EN.id=:").append("enrollmentId").append(" ");
				mapwhereClauseParam.put("enrollmentId", Integer.parseInt(enrollmentId));
			}
			
			String enrollmentStatus = (String)searchCriteria.get("enrollmentStatus");
			if(StringUtils.isNotEmpty(enrollmentStatus)){
				whereClause.append(" AND EN.ENROLLMENT_STATUS_LKP= :").append("enrollmentStatus").append(" ");
				mapwhereClauseParam.put("enrollmentStatus", Integer.parseInt(enrollmentStatus));
			}
			
			// filter by cmr household id
			String cmrHouseholdId = (String) searchCriteria.get("cmrHouseholdId");
			if (StringUtils.isNotEmpty(cmrHouseholdId)) {
				whereClause.append(" AND APPLICATIONS.CMR_HOUSEOLD_ID = :").append("cmrHouseholdId").append(" ");
				mapwhereClauseParam.put("cmrHouseholdId", Integer.parseInt(cmrHouseholdId));
			}
			// Replace first AND with WHERE
			
			String externalCaseId = (String) searchCriteria.get("externalCaseId");
			if (StringUtils.isNotEmpty(externalCaseId)) {
				whereClause.append(" AND CMR.EXTERNAL_HOUSEHOLD_CASE_ID = :").append("externalCaseId").append(" ");
				mapwhereClauseParam.put("externalCaseId", externalCaseId);
			}
			
			String whereClauseString = whereClause.toString().replaceFirst("AND", "WHERE");
			
			return whereClauseString;
	}
	
	/*public static String getDobDate(String dateOfBirth) throws ParseException {
		SimpleDateFormat fromUser = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat myFormat = new SimpleDateFormat("dd-MMM-yy");
		String reformattedDt=null;
		reformattedDt = myFormat.format(fromUser.parse(dateOfBirth));
		return reformattedDt;
	}*/

	private void setNamedQueryParams(Query query, Map<String, Object> mapQueryParam) {
		for(String param : mapQueryParam.keySet()){
			query.setParameter(param, mapQueryParam.get(param));
		}
	}
	
	private void filterByPaging(Query hqlQuery, Integer startRecord, Integer pageSize) {
		if (pageSize != -1) {
			hqlQuery.setFirstResult((startRecord !=null) ? startRecord : 0);
			hqlQuery.setMaxResults(pageSize);
		}		
	}
	/**
	 * @author Sneha
	 * @param searchCreteria
	 * @return Map below method search Member by searchCreteria which are not linked to any SSAP_APPLICATION
	 */
	@Override
    @SuppressWarnings("unchecked")
	public Map<String, Object> searchHousehold(Map<String, Object> searchCriteria) {
		Map<String, Object> mapConsumerData =  new HashMap<>();
		Map<String, Object> mapQueryParam =  new HashMap<>();
		Long recordCount = (long)0.0;
		List<Object> resultList = null;
		
		String selectClause = "SELECT h.id, h.firstName, h.lastName, l.address1, l.city, l.zip, h.phoneNumber ";
		String selectCountClause = "SELECT count(*) ";
		String fromClause = "FROM Household h LEFT OUTER JOIN h.location as l ";
		String whereClause = "WHERE h.id NOT IN (select sap.cmrHouseoldId FROM SsapApplication sap WHERE sap.cmrHouseoldId IS NOT NULL) ";
		
		StringBuilder query = new StringBuilder(selectClause);
		query.append(fromClause);
		query.append(whereClause);
		
		applyHouseholdFilters(query, searchCriteria, mapQueryParam);
		
		EntityManager em = null;
		try {					
			//Execute the query, get the records as per search criteria.
			em = emf.createEntityManager();
			Query hqlQuery = em.createQuery(query.toString());
					
			// Set query parameters
			setNamedQueryParams(hqlQuery, mapQueryParam);
			// Set the paging criteria		
			filterByPaging(hqlQuery, (Integer) searchCriteria.get("startRecord"), (Integer) searchCriteria.get("pageSize"));
			resultList = hqlQuery.getResultList();
			
			if(!resultList.isEmpty()) {
				query.replace(query.indexOf(selectClause), selectClause.length(), selectCountClause);
				Query hqlCountQuery = em.createQuery(query.toString());
				setNamedQueryParams(hqlCountQuery, mapQueryParam);		
				recordCount = (Long) hqlCountQuery.getSingleResult();
			}
			
		} catch (Exception e) {
			LOGGER.error("Unable to search applicants", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
			
		mapConsumerData.put("householdList", resultList);
		mapConsumerData.put("recordCount", recordCount);
		return mapConsumerData;
	}

	/**
	 * Add the filters for the household to filter as per the search criteria provided.
	 * @param query
	 * @param searchCriteria
	 * @param mapQueryParam
	 */
	private void applyHouseholdFilters(StringBuilder query, Map<String, Object> searchCriteria, Map<String, Object> mapQueryParam) {
		// filter by firstName
		String firstName = (String) searchCriteria.get("firstName");
		if (StringUtils.isNotEmpty(firstName)) {
			query.append("AND LOWER(h.firstName) like :").append("firstName").append(" ");
			mapQueryParam.put("firstName", firstName.toLowerCase() + "%");
		}
		
		// filter by firstName
		String lastName = (String) searchCriteria.get("lastName");
		if (StringUtils.isNotEmpty(lastName)) {
			query.append("AND LOWER(h.lastName) like :").append("lastName").append(" ");
			mapQueryParam.put("lastName", lastName.toLowerCase() + "%");
		}
		
		// filter by contactNumber by
		String contactNumber = (String) searchCriteria.get("contactNumber");
		List<String> phoneList = new ArrayList<String>(2);
		phoneList.add((String) searchCriteria.get("contactNumber"));
		if (StringUtils.isNotEmpty(contactNumber)) {
			try {
				if (contactNumber.length() == TEN) {
					contactNumber = String.format("(%s)%s-%s",
							contactNumber.substring(0, THREE),
							contactNumber.substring(THREE, SIX),
							contactNumber.substring(SIX, TEN));
				}
				phoneList.add(contactNumber);
				query.append("AND h.phoneNumber IN :").append("contactList").append(" ");
				mapQueryParam.put("contactList", phoneList);
			} catch (IllegalFormatException ex) {
				LOGGER.error("ConsumerServiceImpl searchHousehold phoneNumberFormat error:", ex);
			}
		}
		
		// filter by zip code (to be fetched from locations table)
		String zipCode = (String) searchCriteria.get(ZIPCODE);
		if (StringUtils.isNotEmpty(zipCode)) {
			query.append("AND h.location.zip = :").append(ZIPCODE).append(" ");
			mapQueryParam.put(ZIPCODE, zipCode);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> searchHouseholdold(Map<String, Object> searchCriteria) {
		List<String> joinTables = new ArrayList<String>();
		joinTables.add("location");
		QueryBuilder<Household> searchQuery = delegateFactory.getObject();
		searchQuery.buildSelectQuery(Household.class,joinTables);

		List<Map<String, Object>> householdList = null;
		Map<String, Object> householdListAndRecordCount = new HashMap<String, Object>();

		/**
		 * Boundary condition if searchCriteria is null or empty
		 */
		if (searchCriteria == null || searchCriteria.isEmpty()) {
			if(LOGGER.isInfoEnabled())
		    {
				LOGGER.info("searchHousehold() -- SearchCriteria is null or empty");
		    }
			householdListAndRecordCount.put(HOUSEHOLD_LIST, householdList);
			householdListAndRecordCount.put(RECORD_COUNT,
					searchQuery.getRecordCount());
			return householdListAndRecordCount;
		} else {
			String firstName = (String) searchCriteria.get(FIRST_NAME);
			if (StringUtils.isNotEmpty(firstName)) {
				if(StringUtils.contains(firstName, " ")) {
					firstName = StringUtils.substringBefore(firstName, " ");
				}
				searchQuery.applyWhere(FIRST_NAME, firstName.toUpperCase(),
						DataType.STRING, ComparisonType.STARTWITH_EQUALS_CASE_IGNORE);
			}

			String lastName = (String) searchCriteria.get(LAST_NAME);
			if (StringUtils.isNotEmpty(lastName)) {
				if(StringUtils.contains(lastName, " ")) {
					lastName = StringUtils.substringAfterLast(lastName, " ");
				}
				searchQuery.applyWhere(LAST_NAME, lastName.toUpperCase(),
						DataType.STRING, ComparisonType.STARTWITH_EQUALS_CASE_IGNORE);
			}

			String contactNumber = (String) searchCriteria.get("contactNumber");
			List<String> phoneList = new ArrayList<String>(2);
			phoneList.add((String) searchCriteria.get("contactNumber"));
			if (StringUtils.isNotEmpty(contactNumber)) {
				try {
					if (contactNumber.length() == TEN) {
						contactNumber = String.format("%s-%s-%s",
								contactNumber.substring(0, THREE),
								contactNumber.substring(THREE, SIX),
								contactNumber.substring(SIX, TEN));
					}
					phoneList.add(contactNumber);
					searchQuery.applyWhere(PHONE_NUMBER, phoneList,	DataType.LIST, ComparisonType.EQUALS);
				} catch (IllegalFormatException ex) {
					LOGGER.error("ConsumerServiceImpl searchHousehold phoneNumberFormat error:", ex);
				}
			}

			String zipCode = (String) searchCriteria.get(ZIPCODE);

			if (StringUtils.isNotEmpty(zipCode)) {
				try {
					searchQuery.applyWhere("location.zip",
							zipCode, DataType.STRING,
							ComparisonType.EQUALS);
				} catch (NumberFormatException ex) {
					LOGGER.error("ConsumerServiceImpl searchHousehold zipformat error:", ex);
				}
			}

			

			searchQuery.applySort(searchCriteria.get(SORT_BY).toString(),
					SortOrder.valueOf(searchCriteria.get("sortOrder")
							.toString()));

			List<String> columns = new ArrayList<String>();
			columns.add("id");
			columns.add(FIRST_NAME);
			columns.add(LAST_NAME);
			columns.add("location.address1");
			columns.add("location.city");
			columns.add("location.zip");
			columns.add(PHONE_NUMBER);
			

			searchQuery.applySelectColumns(columns);
			try {
				searchQuery.setFetchDistinct(true);

				householdList = searchQuery.getData(
						(Integer) searchCriteria.get("startRecord"),
						(Integer) searchCriteria.get("pageSize"));

				householdListAndRecordCount.put(HOUSEHOLD_LIST, householdList);
				householdListAndRecordCount.put(RECORD_COUNT,
						searchQuery.getRecordCount());


			} catch (Exception e) {
				LOGGER.error("ConsumerServiceImpl searchHousehold error:", e);
				householdListAndRecordCount.put(HOUSEHOLD_LIST, householdList);
				householdListAndRecordCount.put(RECORD_COUNT,0);
			}

		}
		return householdListAndRecordCount;

	}

	/**
	 * 
	 * @param searchCriteria
	 * @return
	 */
	@Override
    @SuppressWarnings("unchecked")
	public Map<String, Object> searchEligLead(
			Map<String, Object> searchCriteria) {
		QueryBuilder<EligLead> searchQuery = delegateFactory.getObject();
		searchQuery.buildSelectQuery(EligLead.class);

		List<Map<String, Object>> eligLeadList = null;
		Map<String, Object> eligLeadListAndRecordCount = new HashMap<String, Object>();

		/**
		 * Boundary condition if searchCriteria is null or empty
		 */
		if (searchCriteria == null || searchCriteria.isEmpty()) {
			if(LOGGER.isInfoEnabled())
		    {
				LOGGER.info("searchEligLead() -- SearchCriteria is null or empty");
		    }
			eligLeadListAndRecordCount.put("eligLeadList", eligLeadList);
			eligLeadListAndRecordCount.put(RECORD_COUNT,
					searchQuery.getRecordCount());
			return eligLeadListAndRecordCount;
		} else {
			

			String contactNumber = (String) searchCriteria.get("contactNumber");
			if (StringUtils.isNotEmpty(contactNumber)) {
				try {
// HIX-28291					
//					contactNumber = String.format("%s-%s-%s",
//							contactNumber.substring(0, THREE),
//							contactNumber.substring(THREE, SIX),
//							contactNumber.substring(SIX, TEN));
					searchQuery.applyWhere(PHONE_NUMBER, contactNumber,
							DataType.STRING, ComparisonType.EQUALS);
				} catch (IllegalFormatException ex) {
					LOGGER.error(" searchEligLead : Error while searching by phone number " + ex);
				}
			}

			String zipCode = (String) searchCriteria.get(ZIPCODE);

			if (StringUtils.isNotEmpty(zipCode)) {
				try {
					searchQuery.applyWhere(ZIPCODE,
							zipCode, DataType.STRING,
							ComparisonType.EQUALS);
				} catch (NumberFormatException ex) {
					LOGGER.error("  searchEligLead : Error while searching by zip code" + ex);
				}
			}

			String affiliateHouseholdID = (String) searchCriteria.get("affiliateHouseholdID");

			if (StringUtils.isNotEmpty(affiliateHouseholdID)) {
				
					searchQuery.applyWhere("extAffHouseholdId",
							affiliateHouseholdID, DataType.STRING,
							ComparisonType.EQUALS);
				
			}
			
			// Add sub query to filter out leads which are converted into consumers
			filterConsumers(searchQuery);
            
			searchQuery.applySort(searchCriteria.get(SORT_BY).toString(),	SortOrder.valueOf(searchCriteria.get("sortOrder").toString()));

			List<String> columns = new ArrayList<String>();
			columns.add("id");
			columns.add(NAME);
			// columns.add("lastName");
			columns.add(ZIPCODE);
			columns.add("extAffHouseholdId");
			columns.add(PHONE_NUMBER);
			columns.add("stage");
			columns.add("appStatus");
			columns.add("appStatusUpdateTimeStamp");
			columns.add("lastUpdatedBy");
			columns.add("creationTimestamp");

			searchQuery.applySelectColumns(columns);
			try {
				searchQuery.setFetchDistinct(true);

				eligLeadList = searchQuery.getData(
						(Integer) searchCriteria.get("startRecord"),
						(Integer) searchCriteria.get("pageSize"));

			} catch (Exception e) {
				LOGGER.error("Exception in searchEligLead : ", e);
			}

			eligLeadListAndRecordCount.put(HOUSEHOLD_LIST, eligLeadList);
			eligLeadListAndRecordCount.put(RECORD_COUNT,
					searchQuery.getRecordCount());
		}
		return eligLeadListAndRecordCount;

	}
	
	


	@SuppressWarnings("unchecked")
	private void filterConsumers(QueryBuilder<EligLead> searchQuery){
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("filterConsumers() --To filter out leads which are converted into consumers : START ");
	    }		
		QueryBuilder<Household> searchHouseholdQuery = delegateFactory.getObject();
		searchHouseholdQuery.buildSubQuery(Household.class);
		searchHouseholdQuery.updateSubqueryPredicate(searchQuery.getBuilder().equal(searchQuery.getPath("id"),searchHouseholdQuery.getPath("eligLead.id")));
		

		searchQuery.updateCriteriaPredicate(searchQuery.getBuilder().not(searchQuery.getBuilder().exists(searchHouseholdQuery.getSubQuery())));
		
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("filterConsumers() --To filter out leads which are converted into consumers : END");
	    }
	}

	
	/**
	 * @author Suhasini
	 * @param id
	 * @return Household below method finds Member by Id
	 */

	@Override
    public Household findHouseholdById(int id) {
		return householdRepository.findOne(id);
	}

	/**
	 * @author Suhasini
	 * @param Household
	 * @return Household below method saves Household
	 */
    @Override
    @Transactional
	public Household saveHousehold(Household household) {
		return householdRepository.save(household);
	}

	/**
	 * @author Suhasini
	 * @param userId
	 * @return Household below method finds Member by Id
	 */

	@Override
    public Household findHouseholdByUserId(int userId) {
		return householdRepository.findByUserId(userId);
	}
	
	/**
	 * @author Suhasini
	 * @param userId
	 * @return Household below method finds Member by Id
	 */

	@Override
    public Household findHouseholdRecordByUserId(int userId) {
		return householdRepository.findByUserId(userId);
	}

	@Override
    public List<Member> findMembersByHousehold(Household household) {
		return memberRepository.findByHousehold(household);
	}
	
	@Override
	public Household findHouseholdByEligLeadId(long eligLeadId) {
		return householdRepository.findByEligLeadId(eligLeadId);
	}

	/**
	 * Save response returned from FFM
	 */
	/*@Override
    @Transactional
	public Household saveResponseFromFFM(FFMHouseholdResponse ffmResponse) throws ConsumerServiceException {
		
		LOGGER.info("saveResponseFromFFM inovked on ConsumerService");
		Household hhResponse =null;
		
		if (ffmResponse == null || ffmResponse.getGiHouseholdId() == null) {
			LOGGER.error("GI household Id is null, there is no way to map it with user id");
			throw new ConsumerServiceException(
					"No Unique identifier to associate with User table present!!");
		}
		
		hhResponse = findHouseholdByGIHouseholdId(ffmResponse.getGiHouseholdId());
		
		if (hhResponse != null){
		    hhResponse = saveHouseholdInfo(hhResponse, ffmResponse);
		
		    if (ffmResponse.getMembers() != null) {
                saveMemberInfo(ffmResponse.getMembers(), hhResponse);
		    }
	    }
								
		return hhResponse;
	}*/

	/**
	 * return household record for given giHouseholdId
	 */
	@Override
    public Household findHouseholdByGIHouseholdId(String giHouseholdId) {
		Household household = null;
		if (giHouseholdId != null) {
			household = householdRepository.findByGIHouseholdId(giHouseholdId);
		}
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("findHouseholdByGIHouseholdId returned empty");
	    }
		return household;
	}
/*
	private List<Map<String, Object>> searchByEligLeadName(List<Map<String, Object>> eligLeadList, String firstName, String lastName) {
		List<Map<String, Object>> eligLeadFilteredList = new ArrayList<Map<String, Object>>();
		for (Iterator<Map<String, Object>> iterator = eligLeadList.iterator(); iterator.hasNext();) {
			Map<String, Object> currentEligLeadData = iterator.next();
			String eligLeadName = (String)currentEligLeadData.get(NAME);
			String firstNameLead = null;
			String lastNameLead = null;
			if(StringUtils.isNotEmpty(eligLeadName)) {
				firstNameLead = StringUtils.substringBefore(eligLeadName, " ");
				lastNameLead = StringUtils.substringAfterLast(eligLeadName, " ");
				if(StringUtils.isNotEmpty(firstNameLead) && StringUtils.isNotEmpty(lastNameLead)) {
					if((StringUtils.isNotEmpty(firstName) && StringUtils.startsWithIgnoreCase(firstNameLead, firstName)) ||
							(StringUtils.isNotEmpty(lastName) && StringUtils.containsIgnoreCase(lastNameLead, lastName))) {
						eligLeadFilteredList.add(currentEligLeadData);
					}
				}
			}
			
		}
		
		return eligLeadFilteredList;
	}
*/	
	@Transactional
	private Household saveHouseholdInfo(Household household, FFMHouseholdResponse ffmResponse) throws ConsumerServiceException{
		Household savedHousehold = null;
		if(ffmResponse!=null){
			household.setAptc(ffmResponse.getAptc());
			household.setFfmHouseholdID(ffmResponse.getFfmHouseholdId());
			household.setCsr(ffmResponse.getCsr());
			household.setCountyCode(ffmResponse.getCountyCode());
			household.setPhoneNumber(ffmResponse.getPhoneNumber());
			household.setZipCode(ffmResponse.getZipCode());
			household.setState(ffmResponse.getState());
			household.setEmail(ffmResponse.getEmail());
					
			if(ffmResponse.getMembers() !=null){
				 household.setFamilySize((ffmResponse.getMembers()).size());
			}
				
			//save household
			savedHousehold = householdRepository.save(household);
			if(savedHousehold==null){
					LOGGER.error("Error saving to Household table " + ffmResponse);
					throw new ConsumerServiceException("Error persisting to Household");
			}
		}
		return savedHousehold;
	}
	
	/*@Transactional
	private void saveMemberInfo(List<FFMMemberResponse> responseMembers, Household household) throws ConsumerServiceException {
		
		Member member;
		if (responseMembers != null){
			for(FFMMemberResponse responseMember : responseMembers){
				member = new Member();
				member.setFirstName(responseMember.getFirstName());
				member.setLastName(responseMember.getLastName());
				member.setBirthDate(responseMember.getBirthDate());
				member.setGender(responseMember.getGender());
				member.setPhoneNumber(responseMember.getPhoneNumber());
				member.setEmail(responseMember.getEmail());
				member.setEligibilityEndDate(responseMember.getEligibilityEndDate());
				member.setEligibilityStartDate(responseMember.getEligibilityStartDate());
				member.setEligibilityStatus(responseMember.getEligibilityStatus());
				member.setIsHouseHoldContact(responseMember.getIsHouseHoldContact());
				member.setAptcEligibilityIndicator(responseMember.getAptcEligibilityIndicator());
				member.setAptcEligibilityEndDate(responseMember.getAptcEligibilityEndDate());
				member.setAptcEligibilityStartDate(responseMember.getAptcEligibilityStartDate());
				member.setCsrEligibilityIndicator(responseMember.getCsrEligibilityIndicator());
				member.setCsrEligibilityStartDate(responseMember.getCsrEligibilityStartDate());
				member.setCsrEligibilityEndDate(responseMember.getCsrEligibilityEndDate());
				member.setCsrLevel(responseMember.getCsrLevel());
				member.setFfeApplicantId(responseMember.getFfeApplicantId());
				member.setMaritalStatus(responseMember.getMaritalStatus());
				member.setMonthlyAptcAmount(responseMember.getMonthlyAptcAmount());
				member.setHousehold(household);
				member.setSmoker(responseMember.getSmoker());
				member.setContactLocation(responseMember.getHomeAddress());
				member.setMailingLocation(responseMember.getMailingAddress());
																
				computeEligibilityProgramInfo(member, responseMember);
				
				member = memberRepository.save(member);
				if(member==null){
					LOGGER.error("ERROR saving to Member table " + responseMember);
					throw new ConsumerServiceException("Error persisting to Member table");
				}
			}
		}
	}
	*/
	/**
	 * @author Suhasini
	 * @param Member
	 * @param FFMMemberResponse
	 * @return void
	 */
	
	/*private void computeEligibilityProgramInfo(Member member, FFMMemberResponse responseMember){
		
		MemberBooleanFlag chipIndicator     = responseMember.getChipEligibilityIndicator();
		MemberBooleanFlag medicaidIndicator = responseMember.getMedicaidEligibilityIndicator();
		MemberBooleanFlag qhpIndicator      = responseMember.getQhpEligibilityIndicator();
		
		EligibilityProgramValues eligibilityProgramValue = null;
		Date eligibilityStartDate = null;
		Date eligibilityEndDate = null;
		
		
		if (qhpIndicator != null && qhpIndicator == MemberBooleanFlag.YES){
			eligibilityProgramValue = EligibilityProgramValues.QHP;
			eligibilityStartDate = responseMember.getQhpEligibilityStartDate();
			eligibilityEndDate = responseMember.getQhpEligibilityEndDate();
		}
		
		
		if (medicaidIndicator != null && medicaidIndicator == MemberBooleanFlag.YES){
			eligibilityProgramValue = EligibilityProgramValues.MEDICAID;
			eligibilityStartDate = responseMember.getMedicaidEligibilityStartDate();
			eligibilityEndDate = responseMember.getMedicaidEligibilityEndDate();
		}
		
		if (chipIndicator != null && chipIndicator == MemberBooleanFlag.YES){
			eligibilityProgramValue = EligibilityProgramValues.CHIP;
			eligibilityStartDate = responseMember.getChipEligibilityStartDate();
			eligibilityEndDate = responseMember.getChipEligibilityEndDate();
		}
		
		//when the all the eligibility values are null from ffm for a given member, we are setting it to blank
		if (eligibilityProgramValue == null){
			member.setEligibilityProgram(EligibilityProgramValues.BLANK);
		}
		
		member.setEligibilityProgram(eligibilityProgramValue);
		member.setEligibilityStartDate(eligibilityStartDate);
		member.setEligibilityEndDate(eligibilityEndDate);
			
	}*/
	
	@Transactional
	@Override
	public MemberPortalResponseDTO saveHouseholdInformation(MemberPortalRequestDTO memberPortalRequestDTO) throws ConsumerServiceException{
		MemberPortalResponseDTO memberPortalResponseDTO = new MemberPortalResponseDTO();
		Household household;
		Map<String, String> memberIdMap = new HashMap<String, String>(); 
				
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerService.saveHouseholdInformation - Enter");
	    }		
		if (memberPortalRequestDTO == null || memberPortalRequestDTO.getHouseholdId() == null) {
			LOGGER.error("ConsumerService.saveHouseholdInformation - household Id is null");
			throw new ConsumerServiceException("ConsumerService.saveHouseholdInformation - household Id is null");
		}
		
		household = findHouseholdById(memberPortalRequestDTO.getHouseholdId());
		
		if (household == null){
		    throw new ConsumerServiceException("Error persisting to Household");
		}
		
		if (memberPortalRequestDTO.getMemberDTOList() != null){
            memberIdMap = saveMemberInformation(memberPortalRequestDTO.getMemberDTOList(), household);	
		    memberPortalResponseDTO.setMemberIdMap(memberIdMap);
		}
						
		return memberPortalResponseDTO;
	}
	
	@Transactional
	private Map<String, String> saveMemberInformation(List<MemberDTO> memberDTOList, Household household) throws ConsumerServiceException {
		
		Member member;
		Map<String, String> memberIdMap = new HashMap<String, String>();
		String relationshipCode = null;
		LookupValue lookupValue = null;
		
		if (memberDTOList != null){
			for(MemberDTO memberDTO : memberDTOList){
				member = new Member();
				member.setFirstName(memberDTO.getFirstName());
				member.setMiddleName(memberDTO.getMiddleName());
				member.setLastName(memberDTO.getLastName());
				member.setBirthDate(memberDTO.getBirthDate());
				member.setGender(memberDTO.getGender());
				member.setPhoneNumber(memberDTO.getPhoneNumber());
				member.setIsHouseHoldContact(memberDTO.getIsHouseHoldContact());
				member.setHousehold(household);
				member.setContactLocation(memberDTO.getContactLocation());
				member.setPreferredTimeToContact(memberDTO.getPreferredTimeToContact());
				
				relationshipCode = memberDTO.getRelationshipCode();
				if (!StringUtils.isBlank(relationshipCode)){
					lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(RELATIONSHIP, relationshipCode);
                    if(lookupValue != null) {
                    	member.setRelationship(lookupValue.getLookupValueLabel());
                    }
				}
					
				member = memberRepository.save(member);
				if(member==null){
					LOGGER.error("saveMemberInformation - ERROR saving to Member table ");
					throw new ConsumerServiceException("saveMemberInformation - Error persisting to Member table");
				}
				
				if (memberDTO.getMemberId() != null){
				    memberIdMap.put(memberDTO.getMemberId(), String.valueOf(member.getId()));		
				}
			}
		}
		return memberIdMap;
	}
	
	/**
	 * @author Suhasini
	 * @param id
	 * @return find a prescription by Id
	 */
	/*
	@Transactional
	@Override
	public void updateSmokerInfoForMembers(FFMHouseholdResponse ffmResponse) throws ConsumerServiceException {
		
		LOGGER.debug("ConsumerService.updateSmokerInfoForMembers invoked.");
		List<FFMMemberResponse> members = null;
		List<Long> smokerYesList = new ArrayList<Long>();
		List<Long> smokerNoList  = new ArrayList<Long>();
		
		if(ffmResponse == null) {
			return;
		}
		
		members = ffmResponse.getMembers();
		if (members != null) {
               for(FFMMemberResponse member : members){
            	   Long ffeApplicantId = member.getFfeApplicantId();
            	   MemberBooleanFlag smoker = member.getSmoker();
            	   if (smoker != null && smoker == MemberBooleanFlag.YES) {
            		   smokerYesList.add(ffeApplicantId);
            	   } else{
            		   smokerNoList.add(ffeApplicantId);
            	   }
               }
		}
		if(smokerYesList.size() > 0) {
			memberRepository.updateSmokerInfo(MemberBooleanFlag.YES, smokerYesList);
		}
		
		if(smokerNoList.size() > 0) {
		    memberRepository.updateSmokerInfo(MemberBooleanFlag.NO, smokerNoList);
		}
		    
		return;
	}
	*/
	
	/**
	 * @author Suhasini
	 * @param id
	 * @return find a prescription by Id
	 */
	@Override
 	 public Prescription findPrescriptionById(long id) {
		return prescriptionRepository.findOne(id);
	 }

 	/**
 	 * @author Suhasini
 	 * @param Household
 	 * @return List<Member> below method saves Member
 	 */
 	@Override
 	public ConsumerResponse findPrescriptionsByHousehold(Household household) {
 		List<Prescription> prescriptions = prescriptionRepository.findByHousehold(household);
 		
 		ConsumerResponse consumerResponse = new ConsumerResponse();
 		consumerResponse.setPrescriptions(prescriptions);
 		return consumerResponse;
 	}
 	
 	/**
 	 * @author Suhasini
 	 * @param HouseholdId
 	 * @return ConsumerResponse
 	 */
 	@Override
 	public ConsumerResponse findPrescriptionsByHouseholdId(int householdId) {
 		List<Prescription> prescriptions = prescriptionRepository.findByHouseholdId(householdId);
 		
 		ConsumerResponse consumerResponse = new ConsumerResponse();
 		consumerResponse.setPrescriptions(prescriptions);
 		return consumerResponse;
 		
 	}
 	
 	/**
 	 * @author Suhasini
 	 * @param HouseholdId
 	 * @return Prescription
 	 */
 	@Override
 	public Prescription findPrescriptionByName(Prescription prescription) {
 		return prescriptionRepository.findByPrescriptionAndHousehold(prescription.getPrescriptionName(), prescription.getHousehold().getId());
 		
 	}
 	
 	/**
	 * @author Suhasini
	 * @param  Prescription
	 * @return Prescription below method saves Prescription
	 */
    @Transactional
    @Override
	public Prescription savePrescription(Prescription prescription) throws ConsumerServiceException {
		Prescription prescriptionObj = prescriptionRepository.save(prescription);
		
		if(prescriptionObj == null){
			LOGGER.error("ConsumerServiceImpl.savePrescription - ERROR saving a prescription ");
			throw new ConsumerServiceException("ConsumerServiceImpl.savePrescription - Error persisting a prescription.");
		}
		return prescriptionObj;
	}
    
    /**
	 * @author Suhasini
	 * @param  ConsumerResponse
	 * @return ConsumerResponse below method saves Prescription
	 */
	@Transactional
	@Override
	public ConsumerResponse savePrescriptions(ConsumerResponse consumerResponse) throws ConsumerServiceException {
		
		LOGGER.debug("ConsumerServiceImpl.savePrescriptions");
		if(consumerResponse == null) {
			return new ConsumerResponse();
		}
		
		List<Prescription> prescriptions = consumerResponse.getPrescriptions();
		List<Prescription> prescriptionList = new ArrayList<Prescription>();
		
		if (prescriptions == null) {
			return new ConsumerResponse();
		}
		
		for(Prescription prescription : prescriptions){
			Prescription prescriptionObj = prescriptionRepository.save(prescription);
			
			if(prescriptionObj == null){
				LOGGER.error("ConsumerServiceImpl.savePrescriptions - ERROR saving a prescription ");
				throw new ConsumerServiceException("ConsumerServiceImpl.savePrescriptions - Error persisting a prescription.");
			}
			prescriptionList.add(prescriptionObj);
		}
		ConsumerResponse consumerResponseObj = new ConsumerResponse();
		consumerResponseObj.setPrescriptions(prescriptionList);
		
		return consumerResponseObj;
	}
	
	/**
	 * This method deletes a prescription from the cmr_prescriptions table given a prescription id
	 * @author Suhasini
	 * @param id
	 * @return void
	 */
	@Transactional
	@Override
	public void deletePrescriptionById(long id) {
		prescriptionRepository.delete(id);
		return;
	}
	
	/**
	 * This method deletes a prescription from the cmr_prescriptions table given a prescription id
	 * @author Suhasini
	 * @param id
	 * @return void
	 */
	@Transactional
	@Override
	public void deletePrescription(Prescription prescription) {
		prescriptionRepository.delete(prescription);
		return;
	}
	
	/**
	 * @author Kunal Dav
	 * @param Location
	 * @return Location below method saves Location
	 */
    @Override
    @Transactional
	public Location saveLocation(Location location) {
		return locationRepository.save(location);
	}
    
    /**
	 * @author Kunal Dav
	 * @param HouseholdEnrollment
	 * @return HouseholdEnrollment below method saves HouseholdEnrollment
	 */
    @Override
    @Transactional
	public HouseholdEnrollment saveHouseholdEnrollment(HouseholdEnrollment householdEnrollment) {
		return householdEnrollmentRepository.save(householdEnrollment);
	}

	/**
	 * @author Kunal Dav
	 * @param householdId
	 * @return HouseholdEnrollment below method finds HouseholdEnrollment by HouseholdId
	 */

	@Override
    public List<HouseholdEnrollment> findHouseholdEnrollmentByHouseholdId(Integer householdId) {
		return householdEnrollmentRepository.findHouseholdEnrollmentByHouseholdId(householdId);
	}

    /**
     * @author Jyothi
     * @param eligLeadId
     * @param zip
     * @return Household
     */
    @Override
    @Transactional
    public Household saveStateInHousehold(ConsumerRequestDTO consumerRequestDTO) {
    	
    	Household household = findHouseholdByEligLeadId(consumerRequestDTO.getEligLeadId());
    	if(null != household) {
			household.setState(consumerRequestDTO.getStateCode());
			household.setZipCode(consumerRequestDTO.getZipCode());
			saveHousehold(household);
    	}
    	return household;
    }	
    
    /**
	 * @author Kunal Dav
	 * @param CallLogs
	 * @return CallLogs below method saves CallLogs
	 */
    @Override
    @Transactional
	public ConsumerCallLog saveCallLogs(ConsumerCallLog callLogs) {
		return callLogsRepository.save(callLogs);
	}
    
    /**
	 * @author Suhasini Goli	
	 * @param  HouseholdRequestDTO
	 * @return List<Household>
	 */
    @Override
    @SuppressWarnings("unchecked")
	@Transactional
	public List<Household> getHouseholdList (HouseholdRequestDTO hhRequestDTO) {
    	List<Household> hhList = new ArrayList<Household>();
    	List hhIdList;
    	
    	if (hhRequestDTO == null || hhRequestDTO.getHouseholdIdList() == null || hhRequestDTO.getHouseholdIdList().size() == 0) {
    	   	return hhList;
    	}
    	
    	hhIdList = hhRequestDTO.getHouseholdIdList();
    	hhList = householdRepository.findHouseholds(hhIdList);
    	
    	return hhList;
	}
    
    /*
     * (non-Javadoc)
     * @see com.getinsured.consumer.service.ConsumerService#getConsumerList(java.util.Map)
     * Gives the list of consumers
     * @Params: Map<String, Object> searchCriteria defined by the user
     * 
     */
    @Override
    public Map<String,Object> getConsumerList(Map<String, Object> searchCriteria) {
    	
    	if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.getConsumerList - STARTS");
	    }
    	EntityManager em = emf.createEntityManager();
    	List<IndividualComposite> consumers = new ArrayList<IndividualComposite>();
    	StringBuilder sb = null;
    	boolean isEntityFlag = false;
    	Map<String,Object> output = new HashMap<String,Object>();    	
    	 
    	try{        	
        	@SuppressWarnings("unchecked")
			Map <String,Object> params =(Map<String,Object>)searchCriteria.get(SEARCH_CRITERIA);
        	if(params!=null && params.size()>0){
            	String moduleName = params.get(MODULE_NAME).toString();
            	
            	if(moduleName !=null){
            		isEntityFlag = (moduleName.equalsIgnoreCase(ENTITY))?true:false;
            		
            		sb = buildQueryForConsumerList(params, moduleName);
                	Query query = em.createNativeQuery(sb.toString());
                	query = setSearchParameter(query, params);
                	
                	query.setMaxResults(10); 
                	query.setFirstResult(Integer.parseInt(params.get("startPage").toString()));     	
                	                	
                	List rsList = query.getResultList();
                	Iterator rsIterator = rsList.iterator();
                	
                	while(rsIterator.hasNext()){
                		Object[] objArray = (Object[]) rsIterator.next();
                		IndividualComposite consumerComposite = new IndividualComposite();
                		consumerComposite.setFirstName(objArray[0]==null?"":objArray[0].toString());
                		consumerComposite.setLastName(objArray[1]==null?"":objArray[1].toString());
                		consumerComposite.setApplicationStatus(objArray[2]==null?"":objArray[2].toString());                		
                		consumerComposite.setRequestSent(objArray[3]==null?"":objArray[3].toString());
                		consumerComposite.setInActiveSince(objArray[4]==null?"":objArray[4].toString());
                		consumerComposite.setConsumerId(Long.parseLong(objArray[5]==null?"0":objArray[5].toString()));
                		consumerComposite.setStatus(objArray[6]==null?"":objArray[6].toString());
                		
                		if(isEntityFlag){
                			consumerComposite.setCecId(Long.parseLong(objArray[7]==null?"0":objArray[7].toString()));
                			consumerComposite.setCecFirstName(objArray[8]==null?"":objArray[8].toString());
                			consumerComposite.setCecLastName(objArray[9]==null?"":objArray[9].toString());
                			consumerComposite.setPhoneNumber(objArray[10]==null?"":objArray[10].toString());
                			consumerComposite.setEmailAddress(objArray[11]==null?"":objArray[11].toString());                		                    		
                    		consumerComposite.setEligibilityStatus(objArray[12]==null?"":objArray[12].toString());
                    		consumerComposite.setAddress1(objArray[13]==null?"":objArray[13].toString());
                    		consumerComposite.setAddress2(objArray[14]==null?"":objArray[14].toString());
                    		consumerComposite.setCity(objArray[15]==null?"":objArray[15].toString());
                    		consumerComposite.setState(objArray[16]==null?"":objArray[16].toString());
                    		consumerComposite.setZip(objArray[17]==null?"":objArray[17].toString());
                    		consumerComposite.setConsumerUserId(Long.parseLong(objArray[18]==null?"0":objArray[18].toString()));                    		
                			consumers.add(consumerComposite);
                			continue;
                		}
                		consumerComposite.setPhoneNumber(objArray[7]==null?"":objArray[7].toString());
            			consumerComposite.setEmailAddress(objArray[8]==null?"":objArray[8].toString());
                		consumerComposite.setEligibilityStatus(objArray[9]==null?"":objArray[9].toString());
                		String eligStatus = consumerComposite.getEligibilityStatus();
                		
                		if(eligStatus!=null && eligStatus.trim().length()>0){
                			consumerComposite.setEligibilityStatus(EligibilityStatus.valueOf(eligStatus).getDescription());
                		}		

                		consumerComposite.setFamilySize(Integer.parseInt(objArray[10]!=null?objArray[10].toString():"0"));
                		consumerComposite.setHouseHoldIncome(Double.parseDouble(objArray[11]!=null?objArray[11].toString():"0"));
                		
                		consumerComposite.setAddress1(objArray[12]==null?"":objArray[12].toString());
                		consumerComposite.setAddress2(objArray[13]==null?"":objArray[13].toString());
                		consumerComposite.setCity(objArray[14]==null?"":objArray[14].toString());
                		consumerComposite.setState(objArray[15]==null?"":objArray[15].toString());
                		consumerComposite.setZip(objArray[16]==null?"":objArray[16].toString());
                		consumerComposite.setConsumerUserId(Long.parseLong(objArray[17]==null?"0":objArray[17].toString()));
                		consumers.add(consumerComposite);
                	}
            	}
        	}

        	int totalRecords = getConsumerRecordCount(sb, params);
        	
        	output.put(TOTAL_RECORDS, new Integer(totalRecords));
        	output.put(CONSUMERS, consumers);
        	 
    	}catch(GIException gi){
    		LOGGER.error("ConsumerServiceImpl.getConsumerList - ERROR in retrieving consumer list. Error message is: "+gi.getMessage());
    	}
    	catch(Exception e){
    		LOGGER.error("ConsumerServiceImpl.getConsumerList - ERROR in retrieving a consumer list. Error message is: "+e.getMessage());
    	}finally{
    		if(em != null && em.isOpen()){
    			em.close();
    		}
    	}
    	if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.getConsumerList - ENDS");
	    }
    	return output;
    }
    
    private StringBuilder buildQueryForConsumerList(Map <String,Object> paramsMap, String moduleName) throws GIException{
    	
    	if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.buildQueryForConsumerList - STARTS");
	    }
    	
    	String firstName = (String) paramsMap.get(FIRST_NAME);
    	String lastName = (String) paramsMap.get(LAST_NAME);
    	String status = (String) paramsMap.get(STATUS);
    	String requestSentFrom = (String) paramsMap.get(REQUEST_SENT_FROM);
    	String requestSentTo = (String) paramsMap.get(REQUEST_SENT_TO);
    	String inactiveDateFrom = (String) paramsMap.get(INACTIVE_DATE_FROM);
    	String inactiveDateTo = (String) paramsMap.get(INACTIVE_DATE_TO);
    	String eligibilityStatus = (String) paramsMap.get(ELIGIBILITY_STATUS);
    	String applicationStatus = (String) paramsMap.get(APPLICATION_STATUS);
    	String indFirstOrLastName = (String) paramsMap.get(INDIVIDUAL_FIRST_OR_LAST_NAME);
    	String cecFirstOrLastName = (String) paramsMap.get(CEC_FIRST_OR_LAST_NAME);  
    	String individualId = (String) paramsMap.get(INDIVIDUAL_ID);  
    	StringBuilder sb = new StringBuilder();
    	boolean whereFlag = false;
    	
    	sb.append("select a.first_name, a.last_name, application_status, a.requestsent, a.inactivesince, a.id, a.status, ");
    	sb.append("a.phone_number,a.email_address,eligibility_status, family_size, a.household_income, ");
    	sb.append("address1, address2, city, state, zip, a.USER_ID ");
    	sb.append("from ");
    	
    	switch(moduleName){
    	case BROKER:
    		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
    			sb.append("(select T1.ID, T1.FIRST_NAME, T1.LAST_NAME,  date_trunc('day',T3.CREATION_TIMESTAMP) AS REQUESTSENT, ");
        		sb.append(" date_trunc('day',T3.LAST_UPDATE_TIMESTAMP) AS INACTIVESINCE, T3.STATUS, T1.PHONE_NUMBER,T1.EMAIL_ADDRESS,T1.household_income,T1.USER_ID");
    		}else{
    		sb.append("(select T1.ID, T1.FIRST_NAME, T1.LAST_NAME,  trunc(T3.CREATION_TIMESTAMP) AS REQUESTSENT, ");
    		sb.append("trunc(T3.LAST_UPDATE_TIMESTAMP) AS INACTIVESINCE, T3.STATUS, T1.PHONE_NUMBER,T1.EMAIL_ADDRESS,T1.household_income,T1.USER_ID");
    		}
    		sb.append(",loc.ADDRESS1, loc.ADDRESS2, loc.CITY, loc.STATE, loc.ZIP ");
    		sb.append("from cmr_household T1 INNER JOIN DESIGNATE_BROKER T3 ON T1.id = T3.individualid LEFT OUTER JOIN locations loc ON T1.contact_location_id = loc.id  ");
    		sb.append("where  T3.BROKERID = :moduleId  ");
    		     		
        	
    		if(StringUtils.isNotEmpty(individualId)) {
    			sb.append("and T1.id = :individualId ");
    		}
    		
    		if (StringUtils.isNotEmpty(firstName)) {
    			if(StringUtils.contains(firstName, " ")) {
    				firstName = StringUtils.substringBefore(firstName, " ");
    			}
    			sb.append("and upper(T1.first_name) like upper(:firstName) ");
    		}

    		if (StringUtils.isNotEmpty(lastName)) {
    			if(StringUtils.contains(lastName, " ")) {
    				lastName = StringUtils.substringBefore(lastName, " ");
    			}
    			sb.append("and upper(T1.last_name) like upper(:lastName) ");
    		}
    		break;
    	case ASSISTER:
    		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
    			sb.append("(select T1.ID, T1.FIRST_NAME, T1.LAST_NAME,  date_trunc('day',T3.CREATION_TIMESTAMP) AS REQUESTSENT, ");
        		sb.append("date_trunc('day',T3.LAST_UPDATE_TIMESTAMP) AS INACTIVESINCE, T3.STATUS, T1.PHONE_NUMBER,T1.EMAIL_ADDRESS,T1.household_income, T1.USER_ID");
    		}else{
    		sb.append("(select T1.ID, T1.FIRST_NAME, T1.LAST_NAME,  trunc(T3.CREATION_TIMESTAMP) AS REQUESTSENT, ");
    		sb.append("trunc(T3.LAST_UPDATE_TIMESTAMP) AS INACTIVESINCE, T3.STATUS, T1.PHONE_NUMBER,T1.EMAIL_ADDRESS,T1.household_income, T1.USER_ID");
    		}
    		sb.append(",loc.ADDRESS1, loc.ADDRESS2, loc.CITY, loc.STATE, loc.ZIP ");
    		sb.append("from cmr_household T1  INNER JOIN EE_DESIGNATE_ASSISTERS T3 ON T1.id = T3.individual_id  LEFT OUTER JOIN  locations loc ON T1.contact_location_id = loc.id  ");
    		sb.append("where  T3.ASSISTER_ID = :moduleId ");
    		
    		if(StringUtils.isNotEmpty(individualId)) {
    			sb.append("and T1.id = :individualId ");
    		}
    		
    		if (StringUtils.isNotEmpty(firstName)) {
    			if(StringUtils.contains(firstName, " ")) {
    				firstName = StringUtils.substringBefore(firstName, " ");
    			}
    			sb.append("and upper(T1.first_name) like upper(:firstName) ");
    		}

    		if (StringUtils.isNotEmpty(lastName)) {
    			if(StringUtils.contains(lastName, " ")) {
    				lastName = StringUtils.substringBefore(lastName, " ");
    			}
    			sb.append("and upper(T1.last_name) like upper(:lastName) ");
    		}
    		
    		break;
    	case ENTITY:
    		sb.delete(0, sb.length());
        	sb.append("select a.first_name, a.last_name, application_status, a.requestsent, ");
        	sb.append("a.inactivesince, a.id, a.status, a.cec_id, a.cec_first_name, a.cec_last_name,a.phone_number,a.email_address,eligibility_status,"); 
        	sb.append("address1, address2, city, state, zip, a.USER_ID ");
        	sb.append("from ");
        	
        	if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
        		sb.append("(select T1.ID, T1.FIRST_NAME, T1.LAST_NAME, date_trunc('day',T3.CREATION_TIMESTAMP) AS REQUESTSENT");
        		sb.append(", date_trunc('day',T3.LAST_UPDATE_TIMESTAMP) AS INACTIVESINCE, T3.STATUS, T1.USER_ID, ");
        	}else{
    		sb.append("(select T1.ID, T1.FIRST_NAME, T1.LAST_NAME,trunc(T3.CREATION_TIMESTAMP) AS REQUESTSENT");
    		sb.append(", trunc(T3.LAST_UPDATE_TIMESTAMP) AS INACTIVESINCE, T3.STATUS, T1.USER_ID, ");
        	}
    		sb.append("CEC.id as cec_id, cec.first_name as cec_first_name, cec.last_name as cec_last_name,T1.PHONE_NUMBER,T1.EMAIL_ADDRESS");
    		sb.append(",loc.ADDRESS1, loc.ADDRESS2, loc.CITY, loc.STATE, loc.ZIP ");
    		sb.append("from cmr_household T1  INNER JOIN EE_DESIGNATE_ASSISTERS T3 ON T1.id = T3.individual_id INNER JOIN ee_assisters CEC ON T3.assister_id = CEC.id LEFT OUTER JOIN locations loc ON T1.contact_location_id = loc.id  ");
    		sb.append("where  T3.ENTITY_ID = :moduleId ");
    		
    		if(StringUtils.isNotEmpty(individualId)) {
    			sb.append("and T1.id = :individualId ");
    		}
    		
    		
    		if (StringUtils.isNotEmpty(indFirstOrLastName)) {
    			indFirstOrLastName = indFirstOrLastName.trim().replace(" ", "%");
    			sb.append("and upper(T1.first_name) || upper(T1.last_name) like upper(:indFirstOrLastName) ");
    		}
    		
    		if (StringUtils.isNotEmpty(cecFirstOrLastName)) {
    			cecFirstOrLastName = cecFirstOrLastName.trim().replace(" ", "%");
    			sb.append("and upper(cec.first_name) || upper(cec.last_name) like upper(:cecFirstOrLastName) ");
    		}
    		
    		break;
    		default:
    			throw new GIException("Invalid Module Name: " + moduleName);
    	}
		
		if (StringUtils.isNotEmpty(status)) {
			if(StringUtils.contains(status, " ")) {
				status = StringUtils.substringBefore(status, " ");
			}
			sb.append("and T3.status in (:status) ");
		}
		
		if(StringUtils.isNotBlank(requestSentFrom) || StringUtils.isNotBlank(requestSentTo) || StringUtils.isNotBlank(inactiveDateFrom) || StringUtils.isNotBlank(inactiveDateTo)){
			appendDateToQuery(requestSentFrom, requestSentTo, inactiveDateFrom, inactiveDateTo, sb, paramsMap.get(MODULE_NAME).toString());
		}

		sb.append(")A ");
		
    	sb.append("left outer join ");
    	sb.append("(select * from (select id, application_status, eligibility_status, cmr_houseold_id,(SELECT COUNT(*) FROM SSAP_APPLICANTS S_APPLICANTS WHERE S_APPLICANTS.SSAP_APPLICATION_ID = ssap_applications.ID and S_APPLICANTS.ON_APPLICATION='Y') AS family_size ");
    	sb.append("from ssap_applications where ssap_applications.application_status in ('OP','SG','SU','ER','EN') and ssap_applications.ID = (SELECT MAX(ID) FROM ssap_applications SSAP_MAX where SSAP_MAX.CMR_HOUSEOLD_ID=ssap_applications.CMR_HOUSEOLD_ID and SSAP_MAX.application_status in ('OP','SG','SU','ER','EN')  ) ) T4) B ");
    	sb.append("on A.id = b.cmr_houseold_id ");
    	
		if (StringUtils.isNotEmpty(eligibilityStatus)) {
			if(StringUtils.contains(eligibilityStatus, " ")) {
				eligibilityStatus = StringUtils.substringBefore(eligibilityStatus, " ");
			}
			whereFlag = true;
			sb.append("WHERE eligibility_status = :eligibilityStatus ");
		}
		
		if (StringUtils.isNotEmpty(applicationStatus)) {
			if(StringUtils.contains(applicationStatus, " ")) {
				applicationStatus = StringUtils.substringBefore(applicationStatus, " ");
			}
			if(whereFlag){
				sb.append("AND application_status = :applicationStatus ");
			}else{
				sb.append("WHERE application_status = :applicationStatus ");
			}
		}

    	String orderBy = (String) paramsMap.get(SORT_BY);
		if (StringUtils.isNotEmpty(orderBy)) {
			sb.append("order by ");
			sb.append(orderBy);
			sb.append(" ");

			String sortOrder = (String) paramsMap.get(SORT_ORDER);
			if (StringUtils.isNotEmpty(sortOrder)) {
				sb.append(sortOrder);
			}
		}
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.buildQueryForConsumerList - ENDS");
	    }
    	return sb;
    }
    
    private int getConsumerRecordCount(StringBuilder queryString, Map<String, Object> searchCriteria){
    	
    	if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.getConsumerRecordCount - STARTS");
	    }
    	int recordCount = 0;
    	EntityManager em =null;
    	String countStart = "SELECT COUNT(*) FROM (";
    	String countEnd = " ) SUBQ1 ";
    	try{
    		  em = emf.createEntityManager();
			   queryString.insert(0, countStart);
		    	int length = queryString.length();
		    	queryString.insert(length, countEnd);
		    	
		    	Query query = em.createNativeQuery(queryString.toString());
		    	query = setSearchParameter(query, searchCriteria);
		    	
		    	Object result = query.getSingleResult();
		    	if(result != null){
		        	Number count = (Number) result;
		        	recordCount = count.intValue();
		    	}
		    	if(LOGGER.isInfoEnabled())
			    {
					LOGGER.info("Record count value = "+SecurityUtil.sanitizeForLogging(""+recordCount));
			    }
    	}catch(Exception e){
			LOGGER.error("Exception occurred while loading assisters", e);
		}finally{
			if(em != null && em.isOpen()){
				em.close();
			}
		}
    	if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.getConsumerRecordCount - ENDS");
	    }
    	
    	return recordCount;
    	
    }
    
    private Query setSearchParameter(Query query, Map<String, Object> searchCriteria){
    	
    	if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.setSearchParameter - STARTS");  
	    }    	    	
			
    	StringBuilder searchParams = new StringBuilder();
    	String firstName = (String) searchCriteria.get(FIRST_NAME);
    	String lastName = (String) searchCriteria.get(LAST_NAME);
    	String status = (String) searchCriteria.get(STATUS);
    	String requestSentFrom = (String) searchCriteria.get(REQUEST_SENT_FROM);
    	String requestSentTo = (String) searchCriteria.get(REQUEST_SENT_TO);
    	String inactiveDateFrom = (String) searchCriteria.get(INACTIVE_DATE_FROM);
    	String inactiveDateTo = (String) searchCriteria.get(INACTIVE_DATE_TO);
    	String eligibilityStatus = (String) searchCriteria.get(ELIGIBILITY_STATUS);
    	String applicationStatus = (String) searchCriteria.get(APPLICATION_STATUS);
    	String indFirstOrLastName = (String) searchCriteria.get(INDIVIDUAL_FIRST_OR_LAST_NAME);
    	String cecFirstOrLastName = (String) searchCriteria.get(CEC_FIRST_OR_LAST_NAME);
    	String individualId = (String) searchCriteria.get(INDIVIDUAL_ID);  
    	
    	searchParams.append("Values for the query parameters are: ");
    	searchParams.append("firstName="+firstName+", lastName="+lastName+", status="+status+", requestSentFrom="+requestSentFrom);
    	searchParams.append(", requestSentTo="+requestSentTo+", inactiveDateFrom="+inactiveDateFrom+", inactiveDateTo="+inactiveDateTo);
    	searchParams.append(", eligibilityStatus="+eligibilityStatus);
    	
    	
    	if (searchCriteria.get(MODULE_ID) instanceof String) {
    		query.setParameter(MODULE_ID, Integer.parseInt((String)searchCriteria.get(MODULE_ID)));
    	}else{
    	query.setParameter(MODULE_ID, searchCriteria.get(MODULE_ID));
    	}
    	
    	
    	if (StringUtils.isNotEmpty(firstName)) {
    		query.setParameter(FIRST_NAME, "%" + searchCriteria.get(FIRST_NAME) + "%");
    	}
    	if (StringUtils.isNotEmpty(lastName)) {
    		query.setParameter(LAST_NAME, "%" + searchCriteria.get(LAST_NAME) + "%");
    	}

    	if (StringUtils.isNotEmpty(status)) {
    		String[] listOfStatus = status.split(",");       
    		query.setParameter(STATUS,  Arrays.asList(listOfStatus));
    	}
    	
    	if(StringUtils.isNotEmpty(requestSentFrom)){
    		query.setParameter(REQUEST_SENT_FROM,  searchCriteria.get(REQUEST_SENT_FROM));
    	}
    	
    	if(StringUtils.isNotEmpty(requestSentTo)){
    		query.setParameter(REQUEST_SENT_TO,  searchCriteria.get(REQUEST_SENT_TO));
    	}
    	
    	if(StringUtils.isNotEmpty(inactiveDateFrom)){
    		query.setParameter(INACTIVE_DATE_FROM,  searchCriteria.get(INACTIVE_DATE_FROM));
    	}
    	
    	if(StringUtils.isNotEmpty(inactiveDateTo)){
    		query.setParameter(INACTIVE_DATE_TO,  searchCriteria.get(INACTIVE_DATE_TO));
    	}
    	
    	if(StringUtils.isNotEmpty(eligibilityStatus)){
    		query.setParameter(ELIGIBILITY_STATUS,  searchCriteria.get(ELIGIBILITY_STATUS));
    	}
    	if(StringUtils.isNotEmpty(applicationStatus)){
    		query.setParameter(APPLICATION_STATUS,  searchCriteria.get(APPLICATION_STATUS));
    	}
    	
		if (StringUtils.isNotEmpty(indFirstOrLastName)) {
			if(StringUtils.contains(indFirstOrLastName, " ")) {
				indFirstOrLastName = StringUtils.replace(indFirstOrLastName, " ", "%");
			}
			query.setParameter(INDIVIDUAL_FIRST_OR_LAST_NAME,  "%"+indFirstOrLastName+"%");
		}
		
		if (StringUtils.isNotEmpty(cecFirstOrLastName)) {
			if(StringUtils.contains(cecFirstOrLastName, " ")) {
				cecFirstOrLastName = StringUtils.replace(cecFirstOrLastName, " ", "%");
			}
			query.setParameter(CEC_FIRST_OR_LAST_NAME,  "%" + cecFirstOrLastName + "%");
		}
		
		if(StringUtils.isNotEmpty(individualId)) {
				query.setParameter(INDIVIDUAL_ID,Integer.parseInt(individualId));
			}
		
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.setSearchParameter - ENDS");
	    }
    	return query;
    }
    
	/**
	 * Appends date criteria to builds query.
	 *
	 * @param requestSentFrom
	 *            Designate Request sent date
	 * @param requestSentTo
	 *            Designate request sent date
	 * @param inactiveDateFrom
	 *            Decline designation date
	 * @param inactiveDateTo
	 *            Decline designation date
	 * @param buildquery
	 *            query to which date criteria should be appended
	 */
	private void appendDateToQuery(String requestSentFrom, String requestSentTo, String inactiveDateFrom,
			String inactiveDateTo, StringBuilder buildquery, String moduleName) {
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.appendDateToQuery : START");
	    }
		
		String creationTime = (moduleName!=null)?"T3.CREATION_TIMESTAMP":null;
		
		if(creationTime!=null){
			// Search For pending employer requests
			if (requestSentFrom != null && requestSentTo != null) {
				buildquery.append(" AND "+creationTime+" BETWEEN TO_DATE(:" + REQUEST_SENT_FROM + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY+"') AND TO_DATE(:"
		 	 	 	 		      + REQUEST_SENT_TO + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY+"') +1");
			} 
			else if (requestSentFrom != null && requestSentTo == null) {
				buildquery.append(" AND "+creationTime+" >= TO_DATE(:" + REQUEST_SENT_FROM + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY + "')");
			} 
			else if (requestSentFrom == null && requestSentTo != null) {
				buildquery.append(" AND "+creationTime+" <= TO_DATE(:" + REQUEST_SENT_TO + QUERY_STRING_SEPERATOR+DATE_FORMAT_MM_DD_YYYY+"')+1");
			}
	
			appendSearchInactiveDateParams(inactiveDateFrom, inactiveDateTo,
					buildquery, moduleName);
		}
		
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.appendDateToQuery : END");
	    }
	}

	private void appendSearchInactiveDateParams(String inactiveDateFrom,
			String inactiveDateTo, StringBuilder buildquery, String moduleName) {
		
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.appendSearchInactiveDateParams : STARTS");
	    }
		
		String updatedTime = (moduleName!=null)?"T3.LAST_UPDATE_TIMESTAMP":null;
		// Search For Inactive employer requests
		if (inactiveDateFrom != null && inactiveDateTo != null) {
			buildquery.append(" AND "+updatedTime+" BETWEEN TO_DATE(:" + INACTIVE_DATE_FROM + QUERY_STRING_SEPERATOR+DATE_FORMAT_MM_DD_YYYY+"') AND TO_DATE(:"
								+ INACTIVE_DATE_TO + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY + "')+1");
		} 
		else if (inactiveDateFrom != null && inactiveDateTo == null) {
			buildquery.append(" AND "+updatedTime+" >= TO_DATE(:" + INACTIVE_DATE_FROM + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY + "')");
		} 
		else if (inactiveDateFrom == null && inactiveDateTo != null) {
			buildquery.append(" AND "+updatedTime+" <= TO_DATE(:" + INACTIVE_DATE_TO + QUERY_STRING_SEPERATOR + DATE_FORMAT_MM_DD_YYYY + "')+1");
		}
		
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("ConsumerServiceImpl.appendSearchInactiveDateParams : ENDS");
	    }
	}
	
	@Override
	public Map<String, Object> getlinkappdata(Map<String, Object> searchCriteria) {
		Map<String, Object> mapLinkApplicantData = new HashMap<>();
		Map<String, Object> mapApplication = new HashMap<>();
		
		String ssapAppId = (String) searchCriteria.get("ssapAppId");
		Long lssapAppId = (StringUtils.isEmpty(ssapAppId)) ? (long)(0.0) : Long.parseLong(ssapAppId);
		
		//Get the application details and the details of primary applicant
		if(iSsapApplicationRepository.exists(lssapAppId)) {
			SsapApplication application = iSsapApplicationRepository.findOne(Long.parseLong(ssapAppId));
			SsapApplicant applicant = (application == null) ? null : iSsapApplicantRepository.findByPrimaryApplicant(application.getId());
			
			if(applicant != null) {
				// Primary contact details
				mapApplication.put("name", applicant.getFirstName() + " " + applicant.getLastName());
				mapApplication.put("firstName", applicant.getFirstName());
				mapApplication.put("lastName", applicant.getLastName());
				mapApplication.put("phoneNumber", applicant.getPhoneNumber());
				mapApplication.put("emailAddress", applicant.getEmailAddress());
				mapApplication.put("dob", applicant.getBirthDate());
				
				String zip = StringUtils.EMPTY;
				  if(applicant.getMailiingLocationId()!=null)
				     {
				      Location objLocation=iLocationRepository.findOne(applicant.getMailiingLocationId().intValue());
				      zip = objLocation.getZip();
				     }
				 mapApplication.put("zip", zip);
				
			    Date date=TSDate.getNoOffsetTSDate(application.getCreationTimestamp().getTime());
			    SimpleDateFormat df2 = new SimpleDateFormat("MM-dd-yyyy hh:mm aa");
			    String dateString = df2.format(date);
				// Application details
				mapApplication.put("referralId", application.getExternalApplicationId());
				mapApplication.put("status", application.getApplicationStatus());
				mapApplication.put("id", application.getId());
				mapApplication.put("created", dateString);
			}
		}
		
		// Fetch the household list  based on the application details
		Integer startRecord = (Integer) searchCriteria.get("startRecord");
		Integer pageSize = (Integer) searchCriteria.get("pageSize");
		Map<String, Object> houseHoldList = getHouseHoldListForApplication(mapApplication, startRecord, pageSize);
		
		mapLinkApplicantData.put("application", mapApplication);
		mapLinkApplicantData.putAll(houseHoldList);
		
		return mapLinkApplicantData;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getHouseHoldListForApplication(Map<String, Object> mapApplication, 
																Integer startRecord, Integer pageSize) {
		Map<String, Object> houseHoldList = new HashMap<>();
		Long recordCount = (long) 0.0;
		List<Object> resultList = null;
		String filterQuery = addApplicantFiltersForLink(mapApplication);
		
		if(!mapApplication.isEmpty() || !StringUtils.isEmpty(filterQuery)) {
			String selectClause = "SELECT  h.firstName, h.lastName, h.phoneNumber, h.email, h.id,user ";
			String selectCountClause = "SELECT count(*) ";
			String fromClause = "FROM Household h LEFT OUTER JOIN h.user as user ";
			String whereClause = "WHERE ";
					
			StringBuilder query = new StringBuilder(selectClause);
			query.append(fromClause);
			query.append(whereClause);
			query.append(filterQuery);
			
			EntityManager em = null;
			try {					
				//Execute the query, get the records as per search criteria.
				em = emf.createEntityManager();
				Query hqlQuery = em.createQuery(query.toString());
						
				// Set the paging criteria		
				filterByPaging(hqlQuery, startRecord, pageSize);
				resultList = hqlQuery.getResultList();
				
				if(!resultList.isEmpty()){
					query.replace(query.indexOf(selectClause), selectClause.length(), selectCountClause);
					Query hqlCountQuery = em.createQuery(query.toString());
					recordCount =  (Long) hqlCountQuery.getSingleResult();
				}
			} catch (Exception e) {
				LOGGER.error("Unable to search households", e);
			} finally {
				if (em != null && em.isOpen()) {
					em.clear();
					em.close();
				}
			}
		}
			
		houseHoldList.put("houseHoldList", resultList);
		houseHoldList.put("recordCount", recordCount);
		
		return houseHoldList;
	}

	private String addApplicantFiltersForLink(Map<String, Object> mapApplication) {
		StringBuilder query = new StringBuilder(StringUtils.EMPTY);
		
		// first name
		String firstName = (String) mapApplication.get("firstName");
		if(!StringUtils.isEmpty(firstName)) {
			query.append("LOWER(h.firstName) = '").append(StringUtils.lowerCase(firstName)).append("' ");
		}
			
		// last name
		String lastName = (String) mapApplication.get("lastName");
		if(!StringUtils.isEmpty(lastName)) {
			if(query.length() > 0) {
				query.append(" AND ");
			}
			query.append("LOWER(h.lastName) = '").append(StringUtils.lowerCase(lastName)).append("' ");
		}

		return query.toString();
	}
	
	
	
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean checkIfEmailExists(String Email) {
		List<Object> resultList = null;
		boolean ifEmailExists=false;
		
		if(!StringUtils.isEmpty(Email))
		{	
			String querytoCheckEmails="SELECT users.email FROM users WHERE users.email = '"+Email+"' UNION  SELECT cmr_household.email_address FROM cmr_household WHERE cmr_household.email_address= '"+Email+"'";
				 		
			StringBuilder query = new StringBuilder(querytoCheckEmails);
			
			EntityManager em = null;
			try {					
				//Execute the query, get the records as per search criteria.
				em = emf.createEntityManager();
				Query hqlQuery = em.createNativeQuery(query.toString());
				 resultList = hqlQuery.getResultList();
				 
				 if(resultList.size()>0)
				 {
					 ifEmailExists=true; 
				 }
				
				
			} catch (Exception e) {
				LOGGER.error("Unable to check email", e);
			} finally {
				if (em != null && em.isOpen()) {
					em.clear();
					em.close();
				}
			}
		
		}		
		
		return ifEmailExists;
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public Map<String, Object> searchHouseholdUsingNativeQuery(Map<String, Object> searchCriteria){
		Map<String, Object> householdListAndRecordCount = new HashMap<String, Object>();
		
		StringBuilder basehHouseholdQuery = new StringBuilder(
				"SELECT CMR.id householdid, CMR.first_name HHfirst_name, CMR.last_name last_name, LOC.address1, "
				+ "LOC.city, CMR.zipcode zipcode, CMR.phone_number phone_number, " +
				"SSAP.application_status, SSAP.ID ssapId, USR.email, USR.CREATION_TIMESTAMP as userCreationTime, USR.ID userId,"
				+ "CMR.CREATION_TIMESTAMP, CMR.EMAIL_ADDRESS, SSAP.CASE_NUMBER, SSAP.FINANCIAL_ASSISTANCE_FLAG "+
				"FROM CMR_HOUSEHOLD CMR LEFT OUTER JOIN USERS USR ON CMR.USER_ID=USR.ID "+ 
				"LEFT OUTER JOIN LOCATIONS LOC ON CMR.CONTACT_LOCATION_ID=LOC.ID "+
				"LEFT OUTER JOIN SSAP_APPLICATIONS SSAP ON SSAP.CMR_HOUSEOLD_ID = CMR.ID  "); 
		
		StringBuilder searchHouseholdQuery = new StringBuilder("");
		Map<String, Object> mapConsumerData =  new HashMap<>();	
		Map<String, Object> mapQueryParam = new HashMap<>();
		
		/**
		 * Boundary condition if searchCriteria is null or empty
		 */ 
		if (searchCriteria == null || searchCriteria.isEmpty()) {
			if(LOGGER.isInfoEnabled())
		    {
				LOGGER.info("searchHousehold() -- SearchCriteria is null or empty");
		    }
			
			return null;
		} else {
			applyGeneralNativeQueryFilters(searchHouseholdQuery, searchCriteria, mapQueryParam);
		}
		StringBuilder buildquery = new StringBuilder();
		buildquery.append(basehHouseholdQuery.toString());
		if(searchHouseholdQuery!=null && searchHouseholdQuery.length()>0)
		{
			buildquery.append(" WHERE ");
			buildquery.append(searchHouseholdQuery.toString());
		}
		
		String sortBy = (String) searchCriteria.get("sortBy");
		String sortOrder = (String) searchCriteria.get("sortOrder");
		String sortOrderClause = null;
		if(null != sortBy && null != sortOrder) {
			if(sortBy.equalsIgnoreCase("firstName")) {
				sortBy = "hhfirst_name";
			}else if(sortBy.equalsIgnoreCase("updated") && StringUtils.isEmpty((String) searchCriteria.get("consumerStatus"))) {
				sortBy = "last_update_timestamp";
			} else {
				sortBy = "GI_APP_STATUS_UPDATE_TMSTP";
			}
			sortOrderClause = " order by "+ sortBy + " " +sortOrder+" , householdid "+sortOrder;
		} else {
			sortOrderClause = " order by hhfirst_name asc , householdid asc ";			
		}
		buildquery.append(sortOrderClause);
		
		
		Integer pageSize = 100;
		Integer startIndex = (Integer) searchCriteria.get("startRecord") + 1;
		Integer endIndex = startIndex + pageSize-1;
		Integer uIPageSize = ((Integer) searchCriteria.get("pageSize") != null) ? (Integer) searchCriteria.get("pageSize") : 10;
						
						

		StringBuilder rowNumQuery = new StringBuilder();
		rowNumQuery.append(" select * from ( select A.* , COUNT(1) OVER () RESULT_COUNT , ROW_NUMBER() OVER ( "+sortOrderClause+" ) row_num from ( ");
		rowNumQuery.append(buildquery);
		rowNumQuery.append(" ) A ) B where row_num between "+startIndex.intValue()+ " and "+endIndex.intValue() );
		EntityManager entityManager = null;
		try {
		entityManager = emf.createEntityManager();
		Query query = entityManager.createNativeQuery(rowNumQuery.toString());
		
		setNamedQueryParams(query, mapQueryParam);
		
		List<Object[]> resultList = query.getResultList();	
		mapConsumerData.put("householdList", mapResultToConsumerList(resultList,uIPageSize));
		
		StringBuilder countQuery = new StringBuilder();
		countQuery.append(" select count(distinct householdid) from ( ");
		countQuery.append(buildquery);
		countQuery.append(" ) A ");
		
	//	entityManager = emf.createEntityManager();
		
		query = entityManager.createNativeQuery(countQuery.toString());
		
		setNamedQueryParams(query, mapQueryParam);
		
		Integer householdRecordCount = ((Number)query.getSingleResult()).intValue();
		
		mapConsumerData.put("recordCount", householdRecordCount);
		
		}	catch (Exception exception) {
			LOGGER.error("Exception while searching for Members : "+ exception);
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
			}
		}

		return mapConsumerData;
	}
	
	private List<Object[]> mapResultToConsumerList(List<Object[]> resultList,Integer pageSize) {
		int count=0;
		Map<String,Object[]>consumerMap = new java.util.LinkedHashMap<String,Object[]>();
		List<Object[]> consumerList = new ArrayList<Object[]>();
		for (Object row : resultList) {
			Object[] rowArray = (Object[]) row;
			Object[] consumerDto = new Object[rowArray.length];
			
			String householdId = String.valueOf((BigDecimal)rowArray[0]);
			
			consumerDto[0]=householdId;
			consumerDto[1]=rowArray[1];
			consumerDto[2]=rowArray[2];
			consumerDto[3]=rowArray[3];
			consumerDto[4]=rowArray[4];
			consumerDto[5]=rowArray[5];
			consumerDto[6]=rowArray[6];
			consumerDto[7]=rowArray[7];
			
			String ssapId = String.valueOf((BigDecimal)rowArray[8]);
			consumerDto[8]=ssapId;
			consumerDto[9]=rowArray[9];
			consumerDto[10]=rowArray[10];
			consumerDto[11]=rowArray[11];
			consumerDto[12]=rowArray[12];
			consumerDto[13]=rowArray[13];
			consumerDto[14]=rowArray[14];
			consumerDto[15]=rowArray[15];
			consumerDto[16]=rowArray[16];
			consumerDto[17]=rowArray[17];
			Object[] existingConsumer = consumerMap.get(householdId);
			
			//HIX-88097 replace the duplicate household with the max sssapId
			
			if ((null != existingConsumer)
					&& (Integer.parseInt((String) existingConsumer[8]) < Integer.parseInt((String)consumerDto[8]))
					&& (Integer.parseInt((String) existingConsumer[0]) == Integer
							.parseInt((String)consumerDto[0]))) {
				consumerMap.remove((String) existingConsumer[0]);
				consumerMap.put((String)consumerDto[0], consumerDto);
			}

			// add the new household in the map
			else {

				if ((null == existingConsumer) || (null != existingConsumer && Integer
						.parseInt((String) existingConsumer[0]) != Integer.parseInt((String)consumerDto[0]))) {
					consumerMap.put((String)consumerDto[0], consumerDto);
					count++;
				}

			}
			
			//if map count is same as page size then break
			if(count==pageSize){
				break;
			}
			
		}
		//convert the map to list
		if(consumerMap.size()>0){
			consumerList = new ArrayList<Object[]>(consumerMap.values());
		}
		
		return consumerList;
	}

	private void applyGeneralNativeQueryFilters(StringBuilder searchHouseholdQuery, Map<String, Object> searchCriteria,
			Map<String, Object> mapQueryParam) {
		boolean isCriteriaPresent = false;
		
		String firstName = (String) searchCriteria.get(FIRST_NAME);
		if (StringUtils.isNotEmpty(firstName)) {
			firstName=firstName.replace("'", "''");
			if(StringUtils.contains(firstName, " ")) {
				firstName = StringUtils.substringBefore(firstName, " ");
			}
			searchHouseholdQuery.append(" lower(CMR.first_name) like :firstName ");
			mapQueryParam.put("firstName", firstName.toLowerCase() + "%");
			isCriteriaPresent = true;
		}

		String lastName = (String) searchCriteria.get(LAST_NAME);
		if (StringUtils.isNotEmpty(lastName)) {
			lastName=lastName.replace("'", "''");
			if(StringUtils.contains(lastName, " ")) {
				lastName = StringUtils.substringAfterLast(lastName, " ");
			}
			if(isCriteriaPresent) {
				searchHouseholdQuery.append(" and ");
			} else {
				isCriteriaPresent = true;		
			}
			searchHouseholdQuery.append(" lower(CMR.last_name) like :lastName ");
			mapQueryParam.put("lastName", lastName.toLowerCase() + "%");
		}
		
		String email = (String) searchCriteria.get("householdEmail");
		if (StringUtils.isNotEmpty(email)) {
			if(StringUtils.contains(email, " ")) {
				email = StringUtils.substringAfterLast(email, " ");
			}
			if(isCriteriaPresent) {
				searchHouseholdQuery.append(" and ");
			} else {
				isCriteriaPresent = true;		
			}
			searchHouseholdQuery.append(" LOWER(CMR.EMAIL_ADDRESS) = :email " );
			mapQueryParam.put("email", email.toLowerCase());
		}

		String contactNumber = (String) searchCriteria.get("contactNumber");
		String contactNumberFormatted = (String) searchCriteria.get("contactNumber");
		if (StringUtils.isNotEmpty(contactNumber)) {
			try {
				if (contactNumber.length() == TEN) {
					contactNumberFormatted = String.format("%s-%s-%s",
							contactNumber.substring(0, THREE),
							contactNumber.substring(THREE, SIX),
							contactNumber.substring(SIX, TEN));
				}
				if(isCriteriaPresent) {
					searchHouseholdQuery.append(" and ");
				} else {
					isCriteriaPresent = true;		
				}
				searchHouseholdQuery.append("  CMR.phone_number = :contactNumber ");
				mapQueryParam.put("contactNumber",contactNumber);
				
			} catch (IllegalFormatException ex) {
				LOGGER.error("ConsumerServiceImpl searchHousehold phoneNumberFormat error:", ex);
			}
		}

		String zipCode = (String) searchCriteria.get(ZIPCODE);

		if (StringUtils.isNotEmpty(zipCode)) {
			try {
				if(isCriteriaPresent) {
					searchHouseholdQuery.append(" and ");
				} else {
					isCriteriaPresent = true;		
				}
				searchHouseholdQuery.append(" CMR.zipcode= :zipCode" );
				mapQueryParam.put("zipCode", zipCode);
			} catch (NumberFormatException ex) {
				LOGGER.error("ConsumerServiceImpl searchHousehold zipformat error:", ex);
			}
		}
		
		String dateOfBirth = (String) searchCriteria.get("dateOfBirth");
		if (StringUtils.isNotEmpty(dateOfBirth)) {
			if(isCriteriaPresent) {
				searchHouseholdQuery.append(" and ");
			} else {
				isCriteriaPresent = true;		
			}
			searchHouseholdQuery.append(" to_char(CMR.BIRTH_DATE,'MM/dd/yyyy') =").append(":dateOfBirth").append(" ");
			mapQueryParam.put("dateOfBirth", dateOfBirth);
		}
		
	
}

	@Override
	@SuppressWarnings("unchecked")
	public boolean checkExactHouseholdExists(Household household) {

		boolean ifExactHouseholdExists = false;
		
		String birthDate = (new SimpleDateFormat("MM/dd/yyyy")).format(household.getBirthDate());

		String queryToCheckExactHousehold="SELECT FIRST_NAME, LAST_NAME, ZIPCODE, BIRTH_DATE FROM CMR_HOUSEHOLD "
				+ "WHERE FIRST_NAME='"+ household.getFirstName() +"' AND LAST_NAME='"+ household.getLastName() +"' "
						+ "AND ZIPCODE='"+ household.getZipCode() +"' AND BIRTH_DATE=TO_DATE('"+ birthDate +"','MM/dd/yyyy')";
			 		
		StringBuilder query = new StringBuilder(queryToCheckExactHousehold);
		
		EntityManager em = null;
		try {					
			em = emf.createEntityManager();
			Query hqlQuery = em.createNativeQuery(query.toString());
			List<Object> resultList = hqlQuery.getResultList();
			 
			 if(resultList.size() > 0) {
				 ifExactHouseholdExists=true; 
			 }
			
		} catch (Exception e) {
			LOGGER.error("Unable to check Exact Household Exists", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}		
		
		return ifExactHouseholdExists;
	}
	
	
	@Override
	public List<String> getPrimaryApplicantAccessCodes(int id) {
		BigDecimal cmrId = new BigDecimal(id);
		List<String> accessCodeList = new ArrayList<String>();
		accessCodeList= iSsapApplicantRepository.findApplicantAccessCodes(cmrId);
		return accessCodeList;
	}	
	
	/**
	 * return household record for given giHouseholdId
	 */
	@Override
    public Household findHouseholdByCaseId(String caseId) {
		Household household = null;
		if (StringUtils.isNotEmpty(caseId)) {
			household = householdRepository.findByCaseId(caseId);
		}
		if(LOGGER.isInfoEnabled())
	    {
			LOGGER.info("findHouseholdByCaseId returned empty");
	    }
		return household;
	}
}
