<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/ng-template" id="qepnoteligible">
  <div class="row-fluid">
    <div id="rightpanel">
      <div class="header">
        <h4><spring:message code="lce.qepeligible.qualifyingEvents"/></h4>
      </div><!-- header -->

      <section class="gutter20">
        <h3>Your results</h3>
        <div class="margin20 gutter10 alert alert-info">
          <p><strong><spring:message code="lce.qepnoteligible.explanTitle"/></strong></p>
          <ul>
            <li><spring:message code="lce.qepnoteligible.content1"/></li>
            <li><spring:message code="lce.qepnoteligible.content2"/></li>
			<li><spring:message code="lce.qepnoteligible.content3"/></li>
          </ul>
        </div>
      </section>
		
	      <%--<div class="margin20 gutter10">--%>
          <%--<p><spring:message code="lce.qepnoteligible.medicaidChipTitle"/>You can find out if you qualify for Medicaid and CHIP 2 ways:</p>--%>
          <%--<ul>--%>
            <%--<li><spring:message code="lce.qepnoteligible.medicaidChipPt1"/></li>--%>
            <%--<li><spring:message code="lce.qepnoteligible.medicaidChipPt2"/></li>--%>
          <%--</ul>--%>
          <%--<p><spring:message code="lce.qepnoteligible.medicaidChipContent"/></p>--%>
        <%--</div>--%>




	 <dl class="dl-horizontal pull-right">
        <a href="#" class="btn btn-primary">
          <spring:message code="lce.qepeligible.returnBtn"/>
        </a>
      </dl>

    </div><!-- rightside -->
  </div> <!-- row-fluid -->
</script>
