package com.getinsured.hix.admin.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ProviderUpload;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.provider.service.ManageFacilitiesDTO;
import com.getinsured.hix.provider.service.ManageProvidersDTO;
import com.getinsured.hix.provider.service.ProviderLoadStatusDTO;
import com.getinsured.hix.provider.service.ProviderService;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;

@Controller
public class AdminProviderController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminProviderController.class);

	@Autowired private UserService userService;
	@Autowired private ProviderService providerService;
	@Autowired private ContentManagementService ecmService;

	@RequestMapping(value="/admin/planmgmt/qdpaddprovider",method = RequestMethod.GET)
	public String addQDPProvider(@ModelAttribute("Plan") Plan planmodel, Model model, HttpServletResponse response) throws Exception {
	
		return "admin/planmgmt/qdpaddprovider";	
	}

	/**
	 * Method is used to manage/upload Providers & Facilities data screen.
	 */
	@RequestMapping(value = "admin/provider/manageProviderDataFile", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	public String manageProviderDataFile(Model model) {

		LOGGER.debug("manageProviderDataFile() is Begin");

		try {
			setProviderDataFileAttributes(model);
		}
		catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
		finally {
			LOGGER.debug("manageProviderDataFile() is End");
		}
		return "admin/provider/manageProviderDataFile";
	}

	private void setProviderDataFileAttributes(Model model) {
		model.addAttribute("providerTypeIndicatorList", ProviderUpload.getProviderTypeIndicatorOptionsList());
	}

	/**
	 * Method is used to get Providers & Facilities Data load status data from database.
	 */
	@RequestMapping(value = "admin/provider/getProviderLoadStatusDataList", method = RequestMethod.GET, produces= "application/json")
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	@ResponseBody
	public Page<ProviderLoadStatusDTO> getProviderLoadStatusDataList(@RequestParam("page") String pageStr,
			Model model, final HttpServletRequest request) throws GIException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getProviderLoadStatusDataList(): pageStr: " + pageStr);
		}

		int page = 0;
		if (StringUtils.isNumeric(pageStr)) {
			page = Integer.parseInt(pageStr);
		}

		Page<ProviderLoadStatusDTO> resultPage = providerService.getProviderLoadStatusDataList(page, GhixConstants.PAGE_SIZE);

		if (page > resultPage.getTotalPages()) {
			throw new GIException("Page requested is invalid");
		}
		return resultPage;
	}

	/**
	 * Method is used to manage Provider data screen.
	 */
	@RequestMapping(value = "admin/provider/uploadProviderDataFile", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	public String uploadProviderDataFile(Model model, @RequestParam("fileToUpload") MultipartFile fileToUpload,
			@RequestParam("selectYear") String selectYear, @RequestParam("selectType") String selectType) {

		LOGGER.debug("uploadProviderDataFile() is Begin");

		try {

			AccountUser currentAccountUser = userService.getLoggedInUser();
			StringBuffer errorMessage = new StringBuffer();
			boolean status = providerService.uploadProviderFileAndSaveTrackingDataInDB(fileToUpload, selectYear, selectType, currentAccountUser.getId(), errorMessage);

			if (status) {
				model.addAttribute("uploadProviderFileMessage", "Provider File uploaded successfully.");
			}
			else if (StringUtils.isNotBlank(errorMessage)) {
				model.addAttribute("uploadFailedProviderFileMessage", errorMessage.toString());
			}
			else {
				model.addAttribute("uploadFailedProviderFileMessage", "Failed to upload Provider File, please check logs.");
			}
			setProviderDataFileAttributes(model);
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while uploading Provider Data File: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("uploadProviderDataFile() is End");
		}
		return "admin/provider/uploadProviderDataFile";
	}

	/**
	 * Method is used to download Provider Logs CSV File from database.
	 */
	@RequestMapping(value = "/admin/provider/downloadProviderLogs", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	public void downloadProviderLogs(@RequestParam("selectedLogs") String selectedLogs, HttpServletResponse response) throws GIException {

		LOGGER.debug("downloadProviderLogs() start");

		try {

			if (StringUtils.isNotBlank(selectedLogs)) {

				String fileName = "provider_logs.csv";
				byte[] fileByteData = selectedLogs.getBytes();

				if (null != fileByteData) {
					response.setContentType("application/octet-stream");
					response.setContentLength(fileByteData.length);
					response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
					FileCopyUtils.copy(fileByteData, response.getOutputStream());
				}
			}
			else {
				LOGGER.error("Logs is empty.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while downloading Provider Logs: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("downloadProviderLogs() end");
		}
	}

	/**
	 * Method is used to download Provider File from database.
	 */
	@RequestMapping(value = "/admin/provider/downloadProviderFile", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	public void downloadProviderFile(@RequestParam("selectedDocumentId") String selectedDocumentId,
			HttpServletResponse response) throws GIException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("downloadProviderFile start with selectedDocumentId: " + SecurityUtil.sanitizeForLogging(selectedDocumentId));
		}

		try {

			if (StringUtils.isNotBlank(selectedDocumentId)) {

				byte[] fileByteData = ecmService.getContentDataById(selectedDocumentId);
				Content docMetaData = ecmService.getContentById(selectedDocumentId);

				if (null != fileByteData && null != docMetaData) {

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("docMetaData getTitle : " + docMetaData.getTitle());
					}
					response.setContentType("application/octet-stream");
					response.setContentLength(fileByteData.length);
					response.setHeader("Content-Disposition", "attachment; filename=" + docMetaData.getTitle());
					FileCopyUtils.copy(fileByteData, response.getOutputStream());
				}
				else if(LOGGER.isWarnEnabled()) {
					LOGGER.warn("File is null for Document ID: " + SecurityUtil.sanitizeForLogging(selectedDocumentId));
				}
			}
			else {
				LOGGER.error("Document ID is empty.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while downloading Provider File for Document ID: " + SecurityUtil.sanitizeForLogging(selectedDocumentId) + ": " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("downloadProviderFile end");
		}
	}

	/**
	 * Method is used to manage Providers data screen.
	 */
	@RequestMapping(value = "admin/provider/manageProviders", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	public String manageProviders() {

		LOGGER.debug("Load Manage Providers screen");
		return "admin/provider/manageProviders";
	}

	/**
	 * Method is used to get Providers Data from SOLR.
	 */
	@RequestMapping(value = "admin/provider/getProviderDataList", method = RequestMethod.GET, produces= "application/json")
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	@ResponseBody
	public Page<ManageProvidersDTO> getProviderDataList(@RequestParam("page") String pageStr,
			@RequestParam("nameFilter") String nameFilter, @RequestParam("specialityFilter") String specialityFilter,
			@RequestParam("networkIdFilter") String networkIdFilter, Model model, final HttpServletRequest request)
			throws GIException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getProviderDataList(): pageStr: " + pageStr);
		}

		int page = 0;
		if (StringUtils.isNumeric(pageStr)) {
			page = Integer.parseInt(pageStr);
		}

		Page<ManageProvidersDTO> resultPage = providerService.getProviderDataList(nameFilter, specialityFilter, networkIdFilter, page, GhixConstants.PAGE_SIZE);
		if (null != resultPage && page > resultPage.getTotalPages()) {
			throw new GIException("Page requested is invalid");
		}
		return resultPage;
	}

	/**
	 * Method is used to manage Facilities data screen.
	 */
	@RequestMapping(value = "admin/provider/manageFacilities", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	public String manageFacilities() {

		LOGGER.debug("Load Manage Facilities screen");
		return "admin/provider/manageFacilities";
	}

	/**
	 * Method is used to get Facilities Data from SOLR.
	 */
	@RequestMapping(value = "admin/provider/getFacilityDataList", method = RequestMethod.GET, produces= "application/json")
	@PreAuthorize(PlanMgmtConstants.MANAGE_PROVIDER_DATA_PERMISSION)
	@ResponseBody
	public Page<ManageFacilitiesDTO> getFacilityDataList(@RequestParam("page") String pageStr,
			@RequestParam("facilityFilter") String facilityFilter, @RequestParam("specialityFilter") String specialityFilter,
			@RequestParam("networkIdFilter") String networkIdFilter, Model model, final HttpServletRequest request)
			throws GIException {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getFacilityDataList(): pageStr: " + pageStr);
		}

		int page = 0;
		if (StringUtils.isNumeric(pageStr)) {
			page = Integer.parseInt(pageStr);
		}

		Page<ManageFacilitiesDTO> resultPage = providerService.getFacilityDataList(facilityFilter, specialityFilter, networkIdFilter, page, GhixConstants.PAGE_SIZE);
		if (null != resultPage && page > resultPage.getTotalPages()) {
			throw new GIException("Page requested is invalid");
		}
		return resultPage;
	}
}
