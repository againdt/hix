package com.getinsured.hix.batch.externalnotices.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.externalnotices.service.ExternalNoticesService;
import com.getinsured.hix.model.ExternalNotice;
import com.getinsured.hix.model.ExternalNotice.ExternalNoticesStatus;


@Component("externalNoticeProcessor")
@Scope("step")
public class ExternalNoticeProcessor implements ItemProcessor<ExternalNotice, ExternalNotice>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalNoticeProcessor.class);
	private ExternalNoticesService externalNoticesService;

	@Override
	public ExternalNotice process(ExternalNotice externalNotices) throws Exception {
		if (externalNotices != null) {
			try {
				ExternalNoticesStatus status = externalNotices.getStatusType();
				if ((status.equals(ExternalNoticesStatus.valueOf("INIT"))) || (status.equals(ExternalNoticesStatus.valueOf("RESEND"))) || (status.equals(ExternalNoticesStatus.valueOf("FAILED")))) {
					externalNoticesService.processBatchInitRecords(externalNotices);
				} else if (status.equals(ExternalNoticesStatus.valueOf("QUEUED"))) {
					externalNoticesService.processBatchQueuedRecords(externalNotices);
				}
			} catch (Exception e) {
				LOGGER.error("Failed to update the external notices ", e);
			}
		}
		return externalNotices;
	}

	public ExternalNoticesService getExternalNoticesService() {
		return externalNoticesService;
	}

	public void setExternalNoticesService(ExternalNoticesService externalNoticesService) {
		this.externalNoticesService = externalNoticesService;
	}

}
