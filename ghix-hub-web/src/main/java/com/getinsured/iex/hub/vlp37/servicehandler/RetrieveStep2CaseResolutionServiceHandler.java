package com.getinsured.iex.hub.vlp37.servicehandler;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r.RetrieveStep2CaseResolutionRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r.RetrieveStep2CaseResolutionResponseType;
import com.getinsured.iex.hub.vlp37.prefixmappers.VLPNamespacePrefixMapper;
import com.getinsured.iex.hub.vlp37.service.VLP_PostProcessor;
import com.getinsured.iex.hub.vlp37.wrapper.RetrieveStep2CaseResolutionRequestTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.RetrieveStep2CaseResolutionResponseSetTypeWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.RetrieveStep2CaseResolutionResponseTypeWrapper;



public class RetrieveStep2CaseResolutionServiceHandler extends AbstractServiceHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RetrieveStep2CaseResolutionServiceHandler.class);
	private Jaxb2Marshaller marshaller;
	private String caseNumber = null;
	private RetrieveStep2CaseResolutionRequestType reqObj = null;
	
	public Object getRequestpayLoad() throws HubServiceException{
		
			try{
				JSONParser parser = new JSONParser();
				JSONObject payloadObj = (JSONObject) parser.parse(getJsonInput());
				
				
				processJSONObject(payloadObj);
				
				RetrieveStep2CaseResolutionRequestTypeWrapper obj = (RetrieveStep2CaseResolutionRequestTypeWrapper)getNamespaceObjects().get(RetrieveStep2CaseResolutionRequestTypeWrapper.class.getName());
				if(obj == null){
					throw new HubServiceException("required VLP Request parameters not available");
				}
				
				reqObj = obj.getRequest();
				this.caseNumber = reqObj.getCaseNumber();
				if(caseNumber == null) {
					throw new RuntimeException("Mandatory attribute caseNumber nit available");
				}
				setJsonInput(obj.toJSONString());
				return reqObj;
			}catch(Exception pe){
				throw new HubServiceException("Failed to generate the payload:",pe);
			}
			
	}
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	 
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	this.handleInputParameter(key, value);
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		String message = "";
		try{
			
			JAXBElement<RetrieveStep2CaseResolutionResponseType> resp= (JAXBElement<RetrieveStep2CaseResolutionResponseType>)responseObject;
			RetrieveStep2CaseResolutionResponseType payload = resp.getValue();
			RetrieveStep2CaseResolutionResponseTypeWrapper response = new RetrieveStep2CaseResolutionResponseTypeWrapper(payload);
			RetrieveStep2CaseResolutionResponseSetTypeWrapper individualResponse = null; 
			
			if(response != null && response.getRetrieveStep2CaseResolutionResponseSet() != null){	
				message = response.toJSONString();
				/*
				 * To set Response code for VLP, we will use the first response in the set of individual responses
				 * Since VLP service will always be called separately for each applicant, we will receive at most
				 * once response 
				 */
				individualResponse = response.getRetrieveStep2CaseResolutionResponseSet();
				if(response != null && response.getResponseMetadata() != null){
					this.setResponseCode(response.getResponseMetadata().getResponseCode());
				}
				VLP_PostProcessor responseHandler = (VLP_PostProcessor) this.getContext().get(ServiceHandler.RESPONSE_HANDLER);
				if(responseHandler != null) {
					responseHandler.submit(this.reqObj, payload, this.getContext());
				}
				/*
				 * HIX-34256 - Case needs to be closed as soon as we get step 1 response
				 */
				this.requestStatus = RequestStatus.PENDING_CLOSE;
			}
			
			getResponseData(individualResponse);
			
			return message;
		}catch(ClassCastException ce){
			message = "Help! I do not any thing about "+responseObject.getClass().getName()+" Expecting:"+ RetrieveStep2CaseResolutionResponseTypeWrapper.class.getName();
			this.requestStatus = RequestStatus.HUB_ERROR_RESPONSE;
			throw new HubServiceException(message, ce);
		}
	}
	
	/**
	 * Method to get Case Number from HUB response
	
	 */
	private void getResponseData(RetrieveStep2CaseResolutionResponseSetTypeWrapper individualResponse){
		
		//Case number
		if(individualResponse != null ){
			this.getResponseContext().put("HUB_CASE_NUMBER", individualResponse.getCaseNumber());
		}
		
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return "H100";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		//com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

}
