package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;

import org.springframework.batch.core.UnexpectedJobExecutionException;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.SsapApplication;


/**
 * 
 */
public interface ToConsiderService {

	List<Long> getSsapApplicationIdsForRenewal(Long renewalYear,Long batchSize);
	
	void saveAndThrowsErrorLog(String errorMessage, String errorCode) throws UnexpectedJobExecutionException;
	
	void processErrorApplications(Long renewalYear, int serverCount, int serverName) throws Exception;

	List<SsapApplication> getSsapApplicationListByIds(List<Long> applicationIdList);
	
	Integer logToGIMonitor(String errorMessage, String errorCode);

	List<Long> getSsapApplicationIdsByServerName(Long renewalYear, int serverCount, int serverName, Long batchSize);

	void executeToConsiderLogicForRenewal(SsapApplication currentApplication, Long renewalYear, Integer serverName)
			throws GIException, Exception;
}
