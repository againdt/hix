package com.getinsured.hix.util;

import java.text.NumberFormat;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurrencyFormat {

	private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyFormat.class);
	static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);
    
	static{
		currencyFormat.setMaximumFractionDigits(0);
        currencyFormat.setMinimumFractionDigits(0);
	}
	
	private CurrencyFormat() {
        
    }
    
    public static String getCurrancyFormat(double number) {
             return currencyFormat.format(number);
    }

}
