<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<c:if test="${(ghixEnvironment!='PROD')}"> 
 <img src="${cookieDownloadURL}" class="hide">
 </c:if>

<h3>Updating your account...</h3>
<p>with the latest information from Healthcare.gov</p>
<div style="width:100%; text-align:center">
  <img src="<c:url value="/resources/img/loader.gif"/>">
</div>
<form id="postform" name="postform" method="post" action="${ffmEndpointURL}">
  <df:csrfToken/>
  <input type="hidden" name="SAMLResponse" value="${samlResponse}"/>
  <input type="hidden" name="ghixEnvironment" id="ghixEnvironment" value="${ghixEnvironment}" />
</form>
<script type="text/javascript">

$( document ).ready(function() {
	
	if($("#ghixEnvironment").val() != 'PROD') 
	{
	var t=setTimeout(function(){submitForm();},6000)
	}
	else {
		document.postform.submit();
	}
	});
	
function submitForm(){
	document.postform.submit();
}

</script>