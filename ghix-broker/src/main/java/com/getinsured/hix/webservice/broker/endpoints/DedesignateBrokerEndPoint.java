package com.getinsured.hix.webservice.broker.endpoints;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.service.ExternalEmployerService;
import com.getinsured.hix.broker.service.ExternalIndividualService;
import com.getinsured.hix.broker.util.AHBXRestCallInvoker;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.entity.service.AssisterService;
import com.getinsured.hix.entity.service.DesignateAssisterService;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.EmployersBOBResponse;
import com.getinsured.hix.model.ExternalEmployer;
import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.hix.model.IndividualBOBresponse;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.broker.dedesignatebroker.DeDesignateBrokerRequest;
import com.getinsured.hix.webservice.broker.dedesignatebroker.DeDesignateBrokerResponse;
import com.getinsured.hix.webservice.broker.dedesignatebroker.DesignationIndicator;
import com.getinsured.hix.webservice.broker.dedesignatebroker.RecordType;

/**
 * This is the EndPoint Class which will be used by third party to designate
 * broker and assister.
 */
@Endpoint
public class DedesignateBrokerEndPoint {

	private static final Logger LOGGER = Logger.getLogger(DedesignateBrokerEndPoint.class);

	@Autowired
	private DesignateService designateService;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private AssisterService assisterService;
	@Autowired
	private DesignateAssisterService designateAssisterService;
	@Autowired
	private AHBXRestCallInvoker ahbxRestCallInvoker;
	@Autowired
	private ExternalEmployerService externalEmployerService;
	@Autowired 
	private ExternalIndividualService externalIndividualService;

	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/broker/dedesignatebroker";
	
	public static final String RESPONSE_SUCCESS = "SUCCESS";

	/**
	 * This method will designate broker/assister from given employer/individual
	 * 
	 * @param request
	 *            contains broker id/assister id and employer id/individual id
	 * @return response code and description
	 */
	public @ResponsePayload
	@PayloadRoot(localPart = "deDesignateBrokerRequest", namespace = TARGET_NAMESPACE)
	DeDesignateBrokerResponse deDesignateBroker(@RequestPayload DeDesignateBrokerRequest request) {
		LOGGER.info("IND48 : Agent/Assister De-Designation EndPoint Starts");
		DeDesignateBrokerResponse response = new DeDesignateBrokerResponse();
		DesignateBroker designateBroker = null;
		DesignateAssister designateAssister = null;
		IndividualBOBresponse individualBOBresponse = null;
		EmployersBOBResponse employersBOBResponse = null;
		try {
			String employer = request.getEmployerId();
			String individual = request.getIndividualId();
			String recordId = request.getRecordId();
			RecordType record = request.getRecordType();
			String recordType = record.value();
			DesignationIndicator designationIndicator = request.getDesignationIndicator();
			String indicator = designationIndicator.value();

			//LOGGER.info("IND48 : Agent/Assister De-Designation Request Object : " + request);
			
			if (!BrokerUtils.isEmpty(indicator)) {
				if (!BrokerUtils.isEmpty(recordType)) {
					if(!BrokerUtils.isEmpty(recordId)){
						if( recordId.length() <= BrokerConstants.MAXIDLENGTH ){
							if(BrokerUtils.isNumeric(recordId) && !BrokerUtils.isZero(Integer.valueOf(recordId))){
								if(isEmpty(employer) && isEmpty(individual)){
									setFailureResponse(BrokerConstants.RESPONSECODEBOTHEMPLOYERINDIVIDUALIDMISSING,
											BrokerConstants.RESPONSEDESCBOTHEMPLOYERINDIVIDUALIDMISSING, response);
								} else {
									if(!isEmpty(employer) && !isEmpty(individual)){
										setFailureResponse(BrokerConstants.RESPONSECODEBOTHAGENTANDASSISTERIDPRESENT,
												BrokerConstants.RESPONSEDESCBOTHEMPLOYERINDIVIDUALIDPRESENT, response);
									} else {
										if (BrokerConstants.BROKERRECORDTYPE.equalsIgnoreCase(recordType)) {
											if (!isEmpty(individual) && (!BrokerUtils.isNumeric(individual) || BrokerUtils.isZero(Integer.valueOf(individual)))) {
												setFailureResponse(BrokerConstants.RESPONSECODEINDIVIDUALIDINCORRECT, BrokerConstants.RESPONSEDESCINVALIDINDIVIDUALID, response);
											} else if (!isEmpty(employer) && (!BrokerUtils.isNumeric(employer) || BrokerUtils.isZero(Integer.valueOf(employer)))) {
												setFailureResponse(BrokerConstants.RESPONSECODEEMPLOYERIDINCORRECT, BrokerConstants.RESPONSEDESCINVALIDEMPLOYERID, response);
											} else if (!checkBroker(Integer.valueOf(recordId))) {
												setFailureResponse(BrokerConstants.RESPONSECODEAGENTIDDOESNOTEXIST, BrokerConstants.RESPONSEDESCAGENTIDDOESNOTEXIST, response);
											} else {
													if (!isEmpty(employer)) {
														if(BrokerConstants.DESIGNATION.equalsIgnoreCase(indicator)){
															if (checkDesignateAgentForEmployer(Integer.valueOf(recordId),
																	Integer.valueOf(employer))) {
																setFailureResponse(BrokerConstants.RESPONSECODEAGENTALREADYDESIGNATEDBYEMPLOYER, BrokerConstants.RESPONSEDESCAGENTALREADYDESIGNATEDBYEMPLOYER, response);
															} else {
																	try{
																		//Trigger for IND50
																		LOGGER.info("External Employer BOB Detail AHBX service call starts");
																		employersBOBResponse = ahbxRestCallInvoker.retrieveAndSaveExternalEmployerBOBDetail(Long.valueOf(employer));
																		LOGGER.info("External Employer BOB Detail AHBX service call completed");
																	} catch(Exception exception){
																		setFailureResponse(BrokerConstants.RESPONSECODEIND50FAILED, BrokerConstants.RESPONSEDESCIND50FAILED, response);
																		return response;
																	}
																	
																	if(employersBOBResponse!=null && !BrokerUtils.isEmpty(employersBOBResponse.getStatus()) && RESPONSE_SUCCESS.equals(employersBOBResponse.getStatus())){
																		//LOGGER.debug("Agent Designation Service Starts for Employer : "+ employer);
																		try{
																			designateBroker = designateService.designateEmployerByBroker(
																					Integer.valueOf(recordId), Integer.valueOf(employer));
																			if (designateBroker != null) {
																				response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
																				response.setResponseDescription(BrokerConstants.RESPONSEDESCSUCCESS);
																			} else {
																				setFailureResponse(BrokerConstants.RESPONSECODENOAGENTDESIGNATIONRECORDFOREMPLOYER, BrokerConstants.RESPONSEDESCNOAGENTDESIGNATIONRECORDFOREMPLOYER, response);
																			}
																		} catch(Exception exception){
																			setFailureResponse(BrokerConstants.RESPONSECODEAGENTEMPLOYERDESIGNATIONFAILED, BrokerConstants.RESPONSEDESCAGENTEMPLOYERDESIGNATIONFAILED, response);
																			return response;
																		}
																		
																		//LOGGER.debug("Agent Designation Service Ends for Employer : "+ designateBroker);
																	} else {
																		setFailureResponse(BrokerConstants.RESPONSECODEIND50FAILED, BrokerConstants.RESPONSEDESCIND50FAILED, response);
																	}
															}
														} else {
															if(!checkEmployer(Long.valueOf(employer))){
																setFailureResponse(BrokerConstants.RESPONSECODEEMPLOYERIDDOESNOTEXIST, BrokerConstants.RESPONSEDESCEMPLOYERIDDOESNOTEXIST, response);
															} else if (checkExistingDesignateBrokerForEmployer(Integer.valueOf(recordId),
																		Integer.valueOf(employer))) {
																setFailureResponse(BrokerConstants.RESPONSECODEAGENTALREADYDEDESIGNATEDBYEMPLOYER, BrokerConstants.RESPONSEDESCAGENTALREADYDEDESIGNATEDBYEMPLOYER, response);
															} else if (!checkDesignateAgentForEmployer(Integer.valueOf(recordId),
																Integer.valueOf(employer))) {
																setFailureResponse(BrokerConstants.RESPONSECODENOAGENTDESIGNATIONRECORDFOREMPLOYER, BrokerConstants.RESPONSEDESCNOAGENTDESIGNATIONRECORDFOREMPLOYER, response);
															} else {
																try{
																	designateBroker = designateService.deDesignateEmployerForBroker(
																			Integer.valueOf(recordId), Integer.valueOf(employer));
																	if (designateBroker != null) {
																		response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
																		response.setResponseDescription(BrokerConstants.RESPONSEDESCSUCCESS);
																	} else {
																		setFailureResponse(BrokerConstants.RESPONSECODENOAGENTDESIGNATIONRECORDFOREMPLOYER, BrokerConstants.RESPONSEDESCNOAGENTDESIGNATIONRECORDFOREMPLOYER, response);
																	}
																} catch(Exception exception){
																	setFailureResponse(BrokerConstants.RESPONSECODEAGENTEMPLOYERDEDESIGNATIONFAILED, BrokerConstants.RESPONSEDESCAGENTEMPLOYERDEDESIGNATIONFAILED, response);
																	return response;
																}
															}
														}
													} else {
														if(BrokerConstants.DESIGNATION.equalsIgnoreCase(indicator)){
															if (checkDesignateAgentForIndividual(Integer.valueOf(recordId),
																	Integer.valueOf(individual))) {
																setFailureResponse(BrokerConstants.RESPONSECODEAGENTALREADYDESIGNATEDBYINDIVIDUAL, BrokerConstants.RESPONSEDESCAGENTALREADYDESIGNATEDBYINDIVIDUAL, response);
															} else {
																	try{
																		//Trigger for IND52
																		LOGGER.info("External Individual BOB Detail AHBX service call starts");
																		individualBOBresponse = ahbxRestCallInvoker.retrieveAndSaveExternalIndividualDetail(Long.valueOf(individual));
																		LOGGER.info("External Individual BOB Detail AHBX service call completed");
																	} catch(Exception exception){
																		setFailureResponse(BrokerConstants.RESPONSECODEIND52FAILED, BrokerConstants.RESPONSEDESCIND52FAILED, response);
																		return response;
																	}
																	
																	if(individualBOBresponse!=null && !BrokerUtils.isEmpty(individualBOBresponse.getStatus()) && RESPONSE_SUCCESS.equals(individualBOBresponse.getStatus())){
																		//LOGGER.debug("Agent Designation Service Starts for Individual : "+ individual);
																		try{
																			designateBroker = designateService.designateIndividualForBroker(
																					Integer.valueOf(recordId), Integer.valueOf(individual));
																			if (designateBroker != null) {
																				response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
																				response.setResponseDescription(BrokerConstants.RESPONSEDESCSUCCESS);
																			} else {
																				setFailureResponse(BrokerConstants.RESPONSECODENOAGENTDESIGNATIONRECORDFORINDIVIDUAL, BrokerConstants.RESPONSEDESCNOAGENTDESIGNATIONRECORDFORINDIVIDUAL, response);
																			}
																		} catch(Exception exception){
																			setFailureResponse(BrokerConstants.RESPONSECODEAGENTINDIVIDUALDESIGNATIONFAILED, BrokerConstants.RESPONSEDESCAGENTINDIVIDUALDESIGNATIONFAILED, response);
																			return response;
																		}
																		
																		//LOGGER.debug("Agent Designation Service Ends for Individual : "+ designateBroker);
																	} else {
																		setFailureResponse(BrokerConstants.RESPONSECODEIND52FAILED, BrokerConstants.RESPONSEDESCIND52FAILED, response);
																	}
															}
														} else {
															if(!checkIndividual(Long.valueOf(individual))){
																setFailureResponse(BrokerConstants.RESPONSECODEINDIVIDUALIDDOESNOTEXIST, BrokerConstants.RESPONSEDESCINDIVIDUALIDDOESNOTEXIST, response);
															} else if (checkExistingDesignateBrokerForIndividual(Integer.valueOf(recordId),
																		Integer.valueOf(individual))) {
																	setFailureResponse(BrokerConstants.RESPONSECODEAGENTALREADYDEDESIGNATEDBYINDIVIDUAL, BrokerConstants.RESPONSEDESCAGENTALREADYDEDESIGNATEDBYINDIVIDUAL, response);
															} else if (!checkDesignateAgentForIndividual(Integer.valueOf(recordId),
																	Integer.valueOf(individual))) {
																setFailureResponse(BrokerConstants.RESPONSECODENOAGENTDESIGNATIONRECORDFORINDIVIDUAL, BrokerConstants.RESPONSEDESCNOAGENTDESIGNATIONRECORDFORINDIVIDUAL, response);
															} else {
																try{
																	designateBroker = designateService.deDesignateIndividualForBroker(
																			Integer.valueOf(recordId), Integer.valueOf(individual));
	
																	if (designateBroker != null) {
																		response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
																		response.setResponseDescription(BrokerConstants.RESPONSEDESCSUCCESS);
																	} else {
																		setFailureResponse(BrokerConstants.RESPONSECODENOAGENTDESIGNATIONRECORDFORINDIVIDUAL, BrokerConstants.RESPONSEDESCNOAGENTDESIGNATIONRECORDFORINDIVIDUAL, response);
																	}
																} catch(Exception exception){
																	setFailureResponse(BrokerConstants.RESPONSECODEAGENTINDIVIDUALDEDESIGNATIONFAILED, BrokerConstants.RESPONSEDESCAGENTINDIVIDUALDEDESIGNATIONFAILED, response);
																	return response;
																}
															}
														}
													}
											}
										}else if (BrokerConstants.ASSISTERRECORDTYPE.equalsIgnoreCase(recordType)) {
											if(isEmpty(employer)){
												if (!isEmpty(individual)) {
													if(BrokerUtils.isNumeric(individual) && !BrokerUtils.isZero(Integer.valueOf(individual))){
														if (!checkAssister(Integer.valueOf(recordId))) {
															setFailureResponse(BrokerConstants.RESPONSECODEASSISTERIDDOESNOTEXIST, BrokerConstants.RESPONSEDESCASSISTERIDDOESNOTEXIST, response);
														} else {
															if(BrokerConstants.DESIGNATION.equalsIgnoreCase(indicator)){
																if (checkDesignateAssister(Integer.valueOf(recordId),
																		Integer.valueOf(individual))) {
																	setFailureResponse(BrokerConstants.RESPONSECODEASSISTERALREADYDESIGNATEDBYINDIVIDUAL, BrokerConstants.RESPONSEDESCASSISTERALREADYDESIGNATEDBYINDIVIDUAL, response);
																} else {
																		try{
																			//Trigger for IND52
																			LOGGER.info("External Individual BOB Detail AHBX service call starts");
																			individualBOBresponse = ahbxRestCallInvoker.retrieveAndSaveExternalIndividualDetail(Long.valueOf(individual));
																			LOGGER.info("External Individual BOB Detail AHBX service call completed");

																		}catch(Exception exception){
																			setFailureResponse(BrokerConstants.RESPONSECODEIND52FAILED, BrokerConstants.RESPONSEDESCIND52FAILED, response);
																			return response;
																		}
																		
																		if(individualBOBresponse!=null && !BrokerUtils.isEmpty(individualBOBresponse.getStatus()) && RESPONSE_SUCCESS.equals(individualBOBresponse.getStatus())){
																			//LOGGER.debug("Assister Designation Service Starts for Individual : "+ individual);
																			try {
																				designateAssister = designateAssisterService.designateAssisterForIndividual(
																						Integer.valueOf(recordId), Integer.valueOf(individual));
																				if (designateAssister != null) {
																					response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
																					response.setResponseDescription(BrokerConstants.RESPONSEDESCSUCCESS);
																				} else {
																					setFailureResponse(BrokerConstants.RESPONSECODENOASSISTERDESIGNATIONRECORD, BrokerConstants.RESPONSEDESCNOASSISTERDESIGNATIONRECORD, response);
																				}
																			} catch(Exception exception){
																				setFailureResponse(BrokerConstants.RESPONSECODEASSISTERINDIVIDUALDESIGNATIONFAILED, BrokerConstants.RESPONSEDESCASSISTERINDIVIDUALDESIGNATIONFAILED, response);
																				return response;
																			}
																			
																			//LOGGER.debug("Assister Designation Service Starts for Individual : "+ designateAssister);
																		} else {
																			setFailureResponse(BrokerConstants.RESPONSECODEIND52FAILED, BrokerConstants.RESPONSEDESCIND52FAILED, response);
																		}
																}
															} else {
																if(!checkIndividual(Long.valueOf(individual))){
																	setFailureResponse(BrokerConstants.RESPONSECODEINDIVIDUALIDDOESNOTEXIST, BrokerConstants.RESPONSEDESCINDIVIDUALIDDOESNOTEXIST, response);
																} else if (checkExistingDesignateAssister(Integer.valueOf(recordId),
																		Integer.valueOf(individual))) {
																	setFailureResponse(BrokerConstants.RESPONSECODEASSISTERALREADYDEDESIGNATEDBYINDIVIDUAL, BrokerConstants.RESPONSEDESCASSISTERALREADYDEDESIGNATEDBYINDIVIDUAL, response);
																} else if (!checkDesignateAssister(Integer.valueOf(recordId), Integer.valueOf(individual))) {
																	setFailureResponse(BrokerConstants.RESPONSECODENOASSISTERDESIGNATIONRECORD, BrokerConstants.RESPONSEDESCNOASSISTERDESIGNATIONRECORD, response);
																}  else {
																	try{
																		designateAssister = designateAssisterService.deDesignateAssisterForIndividual(
																				Integer.valueOf(recordId), Integer.valueOf(individual));
																		if (designateAssister != null) {
																			response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
																			response.setResponseDescription(BrokerConstants.RESPONSEDESCSUCCESS);
																		} else {
																			setFailureResponse(BrokerConstants.RESPONSECODENOASSISTERDESIGNATIONRECORD, BrokerConstants.RESPONSEDESCNOASSISTERDESIGNATIONRECORD, response);
																		}
																	} catch(Exception exception){
																		setFailureResponse(BrokerConstants.RESPONSECODEASSISTERINDIVIDUALDEDESIGNATIONFAILED, BrokerConstants.RESPONSEDESCASSISTERINDIVIDUALDEDESIGNATIONFAILED, response);
																		return response;
																	}
																}
															}
														}
													} else {
														setFailureResponse(BrokerConstants.RESPONSECODEINDIVIDUALIDINCORRECT, BrokerConstants.RESPONSEDESCINVALIDINDIVIDUALID, response);
													}
												} else {
													setFailureResponse(BrokerConstants.RESPONSECODEINDIVIDUALIDINCORRECT, BrokerConstants.RESPONSEDESCINVALIDINDIVIDUALID, response);
												}
											} else {
												setFailureResponse(BrokerConstants.RESPONSECODEEMPLOYERIDCANNOTBEPRESENTFORASSISTER, BrokerConstants.RESPONSEDESCEMPLOYERIDCANNOTBEPRESENTFORASSISTER, response);
											}
										} else {
											setFailureResponse(BrokerConstants.RESPONSECODEINVALIDRECORDTYPE, BrokerConstants.RESPONSEDESCINVALIDRECORDTYPE, response);
										}
									}
								}
							} else {
								setFailureResponse(BrokerConstants.RESPONSECODERECORDIDINCORRECT, BrokerConstants.RESPONSEDESCINVALIDRECORDID, response);
							}
						}
						else {
							setFailureResponse(BrokerConstants.RESPONSECODEEXCEEDSMAXLENGTH, BrokerConstants.RESPONSEDESCEXCEEDSMAXLENGTH, response);
						}
					} else {
						setFailureResponse(BrokerConstants.RESPONSECODERECORDIDMISSING, BrokerConstants.RESPONSEDESCRECORDIDMISSING, response);
					}
				} else {
					setFailureResponse(BrokerConstants.RESPONSECODERECORDTYPEMISSING, BrokerConstants.RESPONSEDESCRECORDTYPEMISSING, response);
				}
			} else {
				setFailureResponse(BrokerConstants.RESPONSECODEINVALIDDESIGNATIONINDICATOR, BrokerConstants.RESPONSEDESCINVALIDDESIGNATIONINDICATOR, response);
			}
			
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while dedesignating Agent/Assister : ", exception);

			setFailureResponse(BrokerConstants.RESPONSECODEUNKNOWNERROR, BrokerConstants.RESPONSEDESCUNKNOWNERROR, response);

			LOGGER.info("IND48 : Response Code : " + response.getResponseCode() + " : Response Description : "
					+ response.getResponseDescription());
			LOGGER.info("IND48 : Agent/Assister De-Designation EndPoint Ends");

			return response;
		}

		LOGGER.info("IND48 : Response Code : " + response.getResponseCode() + " : Response Description : "
				+ response.getResponseDescription());
		LOGGER.info("IND48 : Agent/Assister De-Designation EndPoint Ends");

		return response;
	}
	
	/**
	 * This method now returns true but in future this will be used for
	 * validating employer
	 * 
	 * @param employerId
	 * @return
	 * @throws GIException
	 */
	private boolean checkEmployer(long employerId) throws GIException {
		
		ExternalEmployer emp = externalEmployerService.findByExternalEmployerId(employerId);
		if(emp!=null){ 
			 return true; 
		}
		 
		return false;
	}
	
	/**
	 * This method now returns true but in future this will be used for
	 * validating Individual
	 * 
	 * @param individualId
	 * @return
	 * @throws GIException
	 */
	private boolean checkIndividual(long individualId) throws GIException {
		ExternalIndividual individual = externalIndividualService.findByExternalIndividualID(individualId);
		if(individual!=null){ 
			 return true; 
		}
		return false;
	}


	/**
	 * This will validate broker in DB
	 * 
	 * @param brokerid
	 * @return
	 */
	private boolean checkBroker(int brokerid) {
		Broker broker = brokerService.findById(brokerid);

		if (broker != null) {
			return true;
		}
		return false;
	}


	/**
	 * This will validate assister in DB
	 * 
	 * @param assisterId
	 * @return
	 */
	private boolean checkAssister(int assisterId) {
		Assister assister = assisterService.findById(assisterId);

		if (assister != null) {
			return true;
		}
		return false;
	}

	/**
	 * This will validate designate assister record in DB for Individual
	 * 
	 * @param assisterId
	 * @param individualId
	 * @return boolean
	 */
	private boolean checkDesignateAssister(int assisterId, int individualId) {
		DesignateAssister designateAssister = designateAssisterService.findByIndividualAndAssisterId(assisterId,
				individualId);

		if(designateAssister != null && ( DesignateAssister.Status.Pending.equals(designateAssister.getStatus()) || DesignateAssister.Status.Active.equals(designateAssister.getStatus()) )){
			return true;
		}
		return false;
	}

	/**
	 * This will validate designate Assister record in DB for Individual
	 * 
	 * @param assisterId
	 * @param individualId
	 * @return boolean
	 */
	private boolean checkExistingDesignateAssister(int assisterId, int individualId) {
		DesignateAssister designateAssister = designateAssisterService.findByIndividualAndAssisterIdAndStatus(
				assisterId, individualId, DesignateAssister.Status.InActive);
		if (designateAssister != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * This will validate designate agent record in DB for Employer
	 * 
	 * @param agentId
	 * @param employerId
	 * @return boolean
	 */
	private boolean checkExistingDesignateBrokerForEmployer(int agentId, int employerId) {
		DesignateBroker designateBroker = designateService.findByEmployerAndAgentIdAndStatus(agentId, employerId,
				DesignateBroker.Status.InActive);
		if (designateBroker != null) {
			return true;
		}
		return false;
	}

	/**
	 * This will validate Existing designate agent record in DB for Individual
	 * 
	 * @param agentId
	 * @param individualId
	 * @return boolean
	 */
	private boolean checkExistingDesignateBrokerForIndividual(int agentId, int individualId) {
		DesignateBroker designateBroker = designateService.findByIndividualAndAgentIdAndStatus(agentId, individualId,
				DesignateBroker.Status.InActive);
		if (designateBroker != null) {
			return true;
		}
		return false;
	}

	/**
	 * This will validate designate agent record in DB for Individual
	 * 
	 * @param agentId
	 * @param individualId
	 * @return boolean
	 */
	private boolean checkDesignateAgentForIndividual(int agentId, int individualId) {
		DesignateBroker designateBroker = designateService.findDeDesignateRecordForBrokerAndIndividualId(agentId,
				individualId);

		if(designateBroker!=null && ( DesignateBroker.Status.Pending.equals(designateBroker.getStatus()) || DesignateBroker.Status.Active.equals(designateBroker.getStatus()) )){
			return true;
		}
		return false;
	}

	/**
	 * This will validate designate agent record in DB for Employer
	 * 
	 * @param agentId
	 * @param employerId
	 * @return boolean
	 */
	private boolean checkDesignateAgentForEmployer(int agentId, int employerId) {
		DesignateBroker designateBroker = designateService.findDeDesignateRecordForBrokerAndEmployerId(agentId,
				employerId);
		
		if(designateBroker!=null && ( DesignateBroker.Status.Pending.equals(designateBroker.getStatus()) || DesignateBroker.Status.Active.equals(designateBroker.getStatus()) )){
			return true;
		}
		
		return false;
	}

	/*private DeDesignateBrokerResponse setFailureResponse(int responseCode, String responseDesc, String faultDesc, DeDesignateBrokerResponse response) {

		Fault fault = new Fault();
		Faultstring faultString = new Faultstring();
		fault.setFaultcode(String.valueOf(responseCode));

		faultString.setValue(faultDesc);
		fault.setFaultstring(faultString);

		response.setResponseCode(responseCode);
		response.setResponseDescription(responseDesc);
		response.setFault(fault);

		LOGGER.info("Fault Code : " + fault.getFaultcode() + " : Fault String : " + fault.getFaultstring().getValue());

		Detail detail = new Detail();
		List<String> list = new ArrayList<String>();
		list.add(faultDesc);
		detail.setValidationError(list);
		fault.setDetail(detail);
		return response;
	}*/
	
	private DeDesignateBrokerResponse setFailureResponse(int responseCode, String responseDesc, DeDesignateBrokerResponse response) {

		response.setResponseCode(responseCode);
		response.setResponseDescription(responseDesc);
		return response;
	}

	private boolean isEmpty(String value) {

		return (value != null && !"".equals(value)) ? false : true;
	}
}
