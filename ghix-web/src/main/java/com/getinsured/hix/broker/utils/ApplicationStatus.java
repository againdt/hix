package com.getinsured.hix.broker.utils;

public enum ApplicationStatus {
	
	OP("Open"),
	SG("Signed"), SU("Submitted"), ER("Eligibility Received"),
	EN("Enrolled (Or Active)"), CC("Cancelled"), CL("Closed"),
	PN("Partially Enrolled"), QU("Queued"), DQ("De-Queued"), UC("Unclaimed");
	

	private String	description;

	private ApplicationStatus(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

}
