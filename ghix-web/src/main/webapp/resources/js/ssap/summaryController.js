var houseHoldSummaryHtml = "";
var incomeSummaryHtml = "";

function showHouseHoldContactSummary()
{
	$('#houseHoldContactSummaryDiv').html('');
	createHouseHoldContactSummaryHtml();
	$('#houseHoldContactSummaryDiv').append(houseHoldSummaryHtml);
	
	if(ssapApplicationType == "NON_FINANCIAL"){
		$(".non_financial_hide").css("display","none");
	}else{
		$(".non_financial_hide").css("display","");
	}
	
	incomeNumber = 0;
	deductionNumber = 0;
}

function parseDateofBirth(dateofBirth) {
	if(dateofBirth == null || dateofBirth == "") {
		return "";
	}
	
	return dob = dateofBirth.substring(0, 12);
	
}

function getRelationshipWithprimary(personId) {
	var relationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	for(var i=0; i<relationArr.length; i++) {
		var object = relationArr[i];
		//get relationship if relatedPersonId is PC
		if(object.individualPersonId == personId  && object.relatedPersonId==1) {
			return getRelationshipText(object.individualPersonId);
		}
	}
}
/*
function getrelationshiptext(relationshipId) {
	if(relationshipId == "18") {
		return "Self";
	}
	if(relationshipId == "01") {
		return "Spouse";
	}
	else {
		return "other";
	}
}*/

function getSSN(householdMemberObj) {
	var ssn = householdMemberObj.socialSecurityCard.socialSecurityNumber;
	if(ssn == null || ssn == "--") {
		return "";
	}
	else {
		return "***-**-" + ssn.substring(7);
	}
	
}

function getNameOnSsnCard(householdMemberObj) {
	var ssn =  getSSN(householdMemberObj);
	if(ssn == "") {
		return "";
	}
	
	if(householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator) {
		return getFormattedname(householdMemberObj.name.firstName) + " " 
		+ getFormattedname(householdMemberObj.name.middleName) + " " 
		+ getFormattedname(householdMemberObj.name.lastName);
	}
	else {
		return getFormattedname(householdMemberObj.socialSecurityCard.firstNameOnSSNCard) + " " 
		+ getFormattedname(householdMemberObj.socialSecurityCard.middleNameonSSNCard) + " " 
		+ getFormattedname(householdMemberObj.socialSecurityCard.lastNameOnSSNCard);
	}
}

function getFormattedname(name) {
	if(name == null) {
		return "";
	}
	else {
		return name;
	}
	
}

function getTaxDependendantsInfo() {
	var dependentName = "";
	var dependentInfo = "";
	for(var j=0; j<webJson.singleStreamlinedApplication.taxFilerDependants.length; j++){
		var dependentId = webJson.singleStreamlinedApplication.taxFilerDependants[j];
		
		var dependentHouseholdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependentId-1];
		
		
		dependentName = getFormattedname(dependentHouseholdMemberObj.name.firstName)+" "+getFormattedname(dependentHouseholdMemberObj.name.middleName)+" "+getFormattedname(dependentHouseholdMemberObj.name.lastName) ;
		dependentInfo+=	" | " + dependentName;
	}
	return dependentInfo;
}


function getFormattedHomeAddress(householdMemberObj) {
	var homeAddress = householdMemberObj.householdContact.homeAddress.streetAddress1;
	if(householdMemberObj.householdContact.homeAddress.streetAddress2 !=null && householdMemberObj.householdContact.homeAddress.streetAddress2!="")
		homeAddress += ", " + householdMemberObj.householdContact.homeAddress.streetAddress2;
	if(householdMemberObj.householdContact.homeAddress.city !=null && householdMemberObj.householdContact.homeAddress.city!="")
		homeAddress += ", " + householdMemberObj.householdContact.homeAddress.city;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		homeAddress += ", " + householdMemberObj.householdContact.homeAddress.state;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		homeAddress += ", " + householdMemberObj.householdContact.homeAddress.postalCode;
	
	return homeAddress;
	
}

function getFormattedMailingAddress(householdMemberObj) {
	var mailingAddress = householdMemberObj.householdContact.homeAddress.streetAddress1;
	if(householdMemberObj.householdContact.homeAddress.streetAddress2 !=null && householdMemberObj.householdContact.homeAddress.streetAddress2!="")
		mailingAddress += ", " + householdMemberObj.householdContact.homeAddress.streetAddress2;
	if(householdMemberObj.householdContact.homeAddress.city !=null && householdMemberObj.householdContact.homeAddress.city!="")
		mailingAddress += ", " + householdMemberObj.householdContact.homeAddress.city;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		mailingAddress += ", " + householdMemberObj.householdContact.homeAddress.state;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		mailingAddress += ", " + householdMemberObj.householdContact.homeAddress.postalCode;
    return mailingAddress;
}
function getMembersApplyingCoverage() {

	noOfHouseHold = getNoOfHouseHolds();
	var coverageInfo = "<p>-------------------------------------------------------------------------------------------------------------------------</p>";
	coverageInfo += "<p><h2><u>Applying for coverage</u></h2>.</p>";
	
	for ( var i = 1; i <= noOfHouseHold; i++) {
		var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
		coverageInfo+="<p><h3>" + nameOfCurrentMember + "</h3></p>";
		coverageInfo+="<p>Date of birth: " + parseDateofBirth(householdMemberObj.dateOfBirth); + "</p>";
		if(i == 1){
			coverageInfo+="<p>Reltionship to primary: Self</p>";
		}else{
			coverageInfo+="<p>Reltionship to primary: " + getRelationshipWithPrimary(i); + "</p>";			
		}
		if($('#applicationSource').val() == 'RF' || $('#applicationSource').val() == 'CN'){
			coverageInfo+="<p>Applying for coverage: " + (householdMemberObj.applyingForCoverageIndicator? 'Yes' : 'No') + "</p>";
		}else{
			coverageInfo+="<p>Applying for coverage: " + ((householdMemberObj.applyingForCoverageIndicator && householdMemberObj.under26Indicator)? 'Yes' : 'No') + "</p>";	
		}
		coverageInfo += "<p>  </p>";
	}
	//coverageInfo = "";
	//coverageInfo = "<div><table border=\"1\"><tbody><tr><td>Name</td><td>Relationship</td><td>Date of Birth</td><td>Applying for Health Coverage</td><td>&nbsp;</td></tr><tr><td>Mama  John</td><td>&nbsp;&nbsp;</td><td>Jan 01, 1987 12:00:00 PM</td><td>true</td><td>Edit</td></tr><tr><td>Papa  John</td><td>&nbsp;&nbsp;</td><td>Jan 10, 1988 12:00:00 PM</td><td>true</td><td>Edit</td></tr></tbody></table></div>";
	return coverageInfo;
}

function getSpouseInformation(householdMemberObj) {
	var spouseInformation = "";
	householdMemberSpouseObj = null;
	
	if(householdMemberObj.marriedIndicator){
		var spouseId = householdMemberObj.taxFiler.spouseHouseholdMemberId;
		if(!isNaN(spouseId)){
			householdMemberSpouseObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[spouseId-1];
		}
		if(householdMemberSpouseObj != null) {
			spouseInformation = getFormattedname(householdMemberSpouseObj.name.firstName) 
			+ " "+getFormattedname(householdMemberSpouseObj.name.middleName)
			+ " "+getFormattedname(householdMemberSpouseObj.name.lastName) ;
		}
		
	}
	return spouseInformation;
}
function getHouseholdMemberDetails() {
	noOfHouseHold = getNoOfHouseHolds();
	var memberInfo = "";
	var spouseId = "";
	
	for ( var i = 1; i <= noOfHouseHold; i++) {
		var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName) 
			+" "+getFormattedname(householdMemberObj.name.middleName) 
			+" "+getFormattedname(householdMemberObj.name.lastName);
		memberInfo+="<p><h3>" + nameOfCurrentMember + "'s information</h3></p>";
		memberInfo+="<p><b>Gender:</b> " + householdMemberObj.gender + "</p>";
		var ssn = getSSN(householdMemberObj);
		memberInfo+="<p><b>SSN:</b> " + ssn + "</p>";
		
		if(ssn == "") {
			memberInfo+="<p><b>Name on SSN card:</b> </p>";
		}
		else {
			var nameOnSsnCard = getNameOnSsnCard(householdMemberObj);
			memberInfo+="<p><b>Name on SSN card:</b> "  + nameOnSsnCard + "</p>";
			
		}
		memberInfo+="<p><b>U.S. citizen or U.S. national:</b> " + householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator + "</p>";
		
		var homeAddress = getFormattedHomeAddress(householdMemberObj);
		memberInfo += '<p><b>Home address</b>: ' + homeAddress + '</p>';
		
		var mailingAddress = getFormattedMailingAddress(householdMemberObj);
		memberInfo += '<p><b>Mailing address</b>: ' + mailingAddress + '</p>';
		
		var taxFilingInfo="No";
		if(householdMemberObj.planToFileFTRIndicator){
			
			taxFilingInfo = "Yes";
			
			if(householdMemberObj.planToFileFTRJontlyIndicator){
				taxFilingInfo +=", Jointly with "+ getSpouseInformation(householdMemberObj) ;
			}
		}
		
		memberInfo += '<p><b>Will file 2014 tax return</b>: ' + taxFilingInfo + '</p>';
		
		if(householdMemberObj.marriedIndicator) {
			memberInfo += '<p><b>Spouse information</b>: ' + getSpouseInformation(householdMemberObj) + '</p>';
			spouseId = householdMemberObj.taxFiler.spouseHouseholdMemberId;
		}
		
		if(householdMemberObj.planToFileFTRIndicator){
			memberInfo += '<p><b>Tax dependent Claim for 2014</b>: ' + webJson.singleStreamlinedApplication.taxFilerDependants.length + '</p>';
			memberInfo += "<p><b>Tax dependents: "  + getTaxDependendantsInfo() + "</b></p>";
		}
		
		else {
			memberInfo += '<p><b>Tax dependent Claim for 2014</b>: None</p>';
		}
	}
	return memberInfo;
}

function generatePdf() {
	
	var caseNumber = webJson.singleStreamlinedApplication.applicationGuid;
	 window.open(theUrlForReviewSummaryPDF+"?caseNumber="+caseNumber);
	 return;
}
	
	/*
	
	var margins = {
		      top: 40,
		      bottom: 40,
		      left: 20,
		     // width: 522
		    };
	var specialElementHandlers = {
			// element with id of "bypass" - jQuery style selector
			'#bypassme': function(element, renderer){
				// true = "handled elsewhere, bypass text extraction"
				return true
			}
		};
	var doc = new jsPDF();
		
	var source = "";
	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	source += "<h2>Household contact information</h2>";
	
	source += '<p><b>Name</b>: ' + getFormattedname(householdMemberObj.name.firstName) +' '+getFormattedname(householdMemberObj.name.middleName)+' '+getFormattedname(householdMemberObj.name.lastName) + '</p>';
	source += '<p><b>Date of birth</b>: ' + parseDateofBirth(householdMemberObj.dateOfBirth) + '</p>';
	source += '<p><b>Email</b>: ' + householdMemberObj.householdContact.contactPreferences.emailAddress + '</p>';
	
	var homeAdress = getFormattedHomeAddress(householdMemberObj);
	source += '<p><b>Home address</b>: ' + homeAdress + '</p>';
	
	var mailingAddress = getFormattedMailingAddress(householdMemberObj);
	source += '<p><b>Mailing address</b>: ' + mailingAddress + '</p>';
	
	
	var phoneNumber = 'Not provided';
	var otherPhoneNumber = 'Not provided';
	
	if(householdMemberObj.householdContact.phone.phoneNumber != ''){
		phoneNumber = householdMemberObj.householdContact.phone.phoneNumber;
	}
	if(householdMemberObj.householdContact.otherPhone.phoneNumber != '' ){
		householdMemberObj.householdContact.otherPhone.phoneNumber;
	}
	
	source += '<p><b>Phone number</b>: ' + phoneNumber + '</p>';
	source += '<p><b>Second phone number</b>: ' + otherPhoneNumber + '</p>';
	
	source += '<p><b>Prefferred spoken language</b>: ' + householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage + '</p>';
	source += '<p><b>Prefferred written language</b>: ' + householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage + '</p>';
	
	source += '<p><b>Preferred method to read notices</b>: ' + householdMemberObj.householdContact.contactPreferences.preferredContactMethod + '</p>';
	
	
	
	source += getMembersApplyingCoverage();
	source += "<p>-------------------------------------------------------------------------------------------------------------------------</p>";
	source += "<h2>Household members</h2";
	
	source += getHouseholdMemberDetails();
	
	doc.text(20, 20, $('#exchamgeName').val() + " Application ID: " + webJson.singleStreamlinedApplication.applicationGuid);
	doc.line(20, 25, 200, 25); // horizontal line
	doc.setFontSize(12);
	
	
	doc.fromHTML(source
			,margins.left // x coord
	    	, margins.top // y coord
	    	
	    	, {
	    		'width': margins.width // max width of content on PDF
	    		,'elementHandlers': specialElementHandlers
	    	},
	    	
	    	function (dispose) {
	      	  // dispose: object with X, Y of the last line add to the PDF 
	      	  //          this allow the insertion of new lines after html
	    		if (navigator.userAgent.match(/msie|trident/i) ) {
	    	    	doc.output('save', 'downloadApplication.pdf');
	    	    }
	    	    else {
	    	    	doc.output('dataurlnewwindow', 'downloadApplication.pdf');
	    	    }
	          },
		
	    	margins
	);
	
*/

function createHouseHoldContactSummaryHtml(){
	var noOfHouseHold = getNoOfHouseHolds();
	var primaryContactHomeAddress = "";
	var appendRowData='';
	
	noOfHouseHoldmember = getNoOfHouseHolds();
		
	try{
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
		//$('#appscr57HouseHoldPersonalInformation tr').each(function( i ) {
			
			var nameOfCurrentMember;

			//if(i == 0){
				householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
				nameOfCurrentMember=getNameByPersonId(i);
			//}else{
				//nameOfCurrentMember= getNameByTRIndex(i);
			//}

			//nameOfCurrentMember=getNameRemovedBySpace(nameOfCurrentMember);
			//var ssnNo= $(this).find("#appscr57HouseHoldSSNTD"+(i+1)).html();
			var ssnNo = "";
			var nameOnSsnCard = "";
			var homeAddress = "";
			var mailingAddress = "";
			var taxFilingInfo = "";
			var householdMemberSpouseObj=null;
			var spouseInformation = "";
			
			if( householdMemberObj.socialSecurityCard.socialSecurityNumber !="" && householdMemberObj.socialSecurityCard.socialSecurityNumber !=null && householdMemberObj.socialSecurityCard.socialSecurityNumber != "--"){
				ssnNo = "***-**-"+householdMemberObj.socialSecurityCard.socialSecurityNumber.substring(5);   
	 		}
			if(householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator){
				nameOnSsnCard = nameOfCurrentMember;
			}else{
				nameOnSsnCard = getFormattedname(householdMemberObj.socialSecurityCard.firstNameOnSSNCard)+" "+getFormattedname(householdMemberObj.socialSecurityCard.middleNameOnSSNCard)+" "+getFormattedname(householdMemberObj.socialSecurityCard.lastNameOnSSNCard);
			}
			
			if(householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator!=null && !householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator){
				var ReasonableExplanationForNoSSNCode = replaceNullWithEmpty(householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN);
				if(ReasonableExplanationForNoSSNCode == "" ){
					ReasonableExplanationForNoSSNText = "";
				}else {
					ReasonableExplanationForNoSSNText = $("#reasonableExplanationForNoSSN option[value="+ReasonableExplanationForNoSSNCode+"]").text();
				}
				ssnNo = "--NA-- (Reason : "+ReasonableExplanationForNoSSNText+")";				
				nameOnSsnCard = "--NA--";
			}			
			if(householdMemberObj.householdContact.homeAddressIndicator == true || householdMemberObj.householdContact.homeAddressIndicator == 'true'){				
				primaryContactHomeAddress = householdMemberObj.householdContact.homeAddress.streetAddress1+", "+replaceNullWithEmpty(householdMemberObj.householdContact.homeAddress.streetAddress2)+" "+householdMemberObj.householdContact.homeAddress.city+" "+householdMemberObj.householdContact.homeAddress.state+" "+householdMemberObj.householdContact.homeAddress.postalCode+" ";				
			}else if(i>1 && householdMemberObj.livesAtOtherAddressIndicator == true){
					primaryContactHomeAddress = householdMemberObj.otherAddress.address.streetAddress1+", "+replaceNullWithEmpty(householdMemberObj.otherAddress.address.streetAddress2)+" "+householdMemberObj.otherAddress.address.city+" "+householdMemberObj.otherAddress.address.state+" "+householdMemberObj.otherAddress.address.postalCode+" ";
			}
			
			if(householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator == true || householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator == 'true'){
				mailingAddress = "Same as home address";
			}else{
				mailingAddress = householdMemberObj.householdContact.mailingAddress.streetAddress1+", "+replaceNullWithEmpty(householdMemberObj.householdContact.mailingAddress.streetAddress2)+" "+householdMemberObj.householdContact.mailingAddress.city+" "+householdMemberObj.householdContact.mailingAddress.state+" "+householdMemberObj.householdContact.mailingAddress.postalCode+" ";
						
			}
			
			if(householdMemberObj.householdContactIndicator){
				
			}
			if(householdMemberObj.marriedIndicator ){
				var spouseId = householdMemberObj.taxFiler.spouseHouseholdMemberId;
				if(!isNaN(spouseId) && spouseId !== null){
					householdMemberSpouseObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[spouseId-1];
					spouseInformation = getFormattedname(householdMemberSpouseObj.name.firstName)+" "+getFormattedname(householdMemberSpouseObj.name.middleName)+" "+getFormattedname(householdMemberSpouseObj.name.lastName) ;
				}
				
			}
			
			if(householdMemberObj.planToFileFTRIndicator){
				
				taxFilingInfo = "Yes";
				
				if(householdMemberObj.planToFileFTRJontlyIndicator && householdMemberSpouseObj!=null){
					taxFilingInfo +=", Jointly with "+ getFormattedname(householdMemberSpouseObj.name.firstName)+" "+getFormattedname(householdMemberSpouseObj.name.middleName)+" "+getFormattedname(householdMemberSpouseObj.name.lastName) ;
				}
			}
			
			var citizenshipStatusIndicator = "No";
			if(householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator!=null && householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator){
				citizenshipStatusIndicator = "Yes";
			}
			
			//No age check for PC, spouse and ward, so under26Indicator should be always true for them
			var applyingForCoverage = '';
			
			if($('#applicationSource').val() == 'RF' || $('#applicationSource').val() == 'CN'){
				applyingForCoverage = ((householdMemberObj.applyingForCoverageIndicator == true)? 'Yes' : 'No');
			}else{
				applyingForCoverage = ((householdMemberObj.applyingForCoverageIndicator && householdMemberObj.under26Indicator)? 'Yes' : 'No');
			}

			appendRowData+="<div class='hixApplicantInfo'><h5 class='camelCaseName'>"+nameOfCurrentMember+'&nbsp;';

			if(i==1){
				appendRowData+=jQuery.i18n.prop('ssap.primaryContact')+'&nbsp;';
			}

			var isInfant = $('#appscr57IsInfantTD'+(i)).text();

			if(isInfant == "yes")
			{
				appendRowData+= '</h5><p>'+jQuery.i18n.prop("ssap.socialSecurityNumber")+'&nbsp;<strong>'+ssnNo+'</strong></p>'
				+'<p>'+jQuery.i18n.prop("ssap.applyingForCoverage")+'&nbsp;<strong>'+applyingForCoverage+"</strong></p>"
				+'';
			}
			else
			{
				if(applicationStatus=='OP'){
					appendRowData+='<span class="pull-right"><input type="button" value="Edit" class="btn btn-primary" onclick="appscr67Edit('+(i)+')"/></span>';
				}
				appendRowData+='</h5>'

				+'<table class="summary-table">'
				+'<tr><td>'+jQuery.i18n.prop("ssap.applyingForCoverage")+'</td> <td>'+applyingForCoverage+'</td></tr>'
				+'<tr><td> Gender</td> <td class='+'"camelCaseName"'+'>'+householdMemberObj.gender+'</td></tr>'
				+'<tr><td>'+jQuery.i18n.prop("ssap.socialSecurityNumber")+'</td> <td>'+ssnNo+'</td></tr>'
				/*+'<tr><td> Social Security No:</td> <td>'+ssnNo+'</td></tr>'*/
				+'<tr><td> Name on SSN Card</td> <td>'+nameOnSsnCard+'</td></tr>'
				+'<tr><td> US Citizen or US National</td> <td>'+citizenshipStatusIndicator+'</td></tr>'
				+'<tr><td> Home Address</td> <td>'+primaryContactHomeAddress+'</td></tr>';
				if(i==1){
					appendRowData+='<tr><td> Mailing Address</td> <td>'+mailingAddress+'</td></tr>';
				}				
				appendRowData+='<tr class="non_financial_hide"><td> Will File 2014 Income Tax Return</td> <td>'+taxFilingInfo+'</td></tr>'
				+'<tr class="non_financial_hide"><td> Spouse Information</td> <td>'+spouseInformation+'</td></tr>'
				+'<tr class="non_financial_hide"><td> Tax Dependent Claim for 2014</td> <td>'+webJson.singleStreamlinedApplication.taxFilerDependants.length+'</td></tr>'
				
				+'</table>';
				appendRowData+= '<table class="summary-table margin20-b non_financial_hide"><tr><td>Tax Dependent:</td> <td>Claimed on tax return for</td></tr>';
				for(var j=0; j<webJson.singleStreamlinedApplication.taxFilerDependants.length; j++){
					var dependentHouseholdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[j];
					var dependentName = getFormattedname(dependentHouseholdMemberObj.name.firstName)+" "+getFormattedname(dependentHouseholdMemberObj.name.middleName)+" "+getFormattedname(dependentHouseholdMemberObj.name.lastName) ;
					appendRowData+=	'<tr><td>'+dependentName+'</td><td>2014</td></tr>';
				}
				appendRowData+= '</table>';
				/*appendRowData+= '<table><tr><td>Lives with and has responsibility for primary care of</td></tr></table>';
				
				appendRowData+= '<table><tr><td>Child Name</td><td>Date of Birth</td><td>Relationship</td></tr>';
				for(var j=0; j<webJson.singleStreamlinedApplication.taxFilerDependants.length; j++){
					var dependentHouseholdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[j];
					var dependentName = dependentHouseholdMemberObj.name.firstName+" "+dependentHouseholdMemberObj.name.middleName+" "+dependentHouseholdMemberObj.name.lastName ;
					appendRowData+=	'<tr><td>'+dependentName+'</td><td>2014</td></tr>';
				}*/
			}


			appendRowData += '</div></div>'; 
	// if(i==noOfHouseHold-1)
//		appendRowData+=	'<hr class="hide" />';
	// else
//		appendRowData+=	'<hr />';
			if(i==noOfHouseHold)
				appendRowData+='<div class="control-group hide"><input type="button" value="Edit Household Composition" class="edit_household_button" /></div>';

			
			}
		
		appendRowData+='<h4>More About This Household</h4><div class="hixApplicantInfo">';
		
		appendRowData+='<h5 class="non_financial_hide">Has a disability</h5>';
		appendRowData+='<table class="summary-table margin40-b non_financial_hide">';

		
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			var nameOfCurrentMember;

			//if(i == 0){
				var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
				nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
				var disabilityIndicator = householdMemberObj.disabilityIndicator?'Yes':'No';
				if(disabilityIndicator == 'Yes'){
					disabilityIndicator += ' [<b>Tribe</b> : '+householdMemberObj.americanIndianAlaskaNative.tribeFullName+' / <b>State</b> : '+householdMemberObj.americanIndianAlaskaNative.stateFullName+']';
				}
				appendRowData+='<tr><td>'+nameOfCurrentMember+'</td><td>'+disabilityIndicator+'</td></tr>';
		}

		appendRowData+='</table>';
		
		appendRowData+='<h5 class="non_financial_hide">Need Help with Activities of daily living</h5>';
		appendRowData+='<table class="summary-table margin40-b non_financial_hide">';
		
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			
	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	
		var nameOfCurrentMember;
		nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
		var disabilityIndicator = householdMemberObj.healthCoverage.medicaidInsurance.requestHelpPayingMedicalBillsLast3MonthsIndicator?'Yes':'No';
		appendRowData+='<tr><td>'+nameOfCurrentMember+'</td><td>'+disabilityIndicator+'</td></tr>';

	
}
		appendRowData+='</table>';
		
		
		appendRowData+='<h5>Household members who are American Indian or Alaska Native</h5>';
		appendRowData+='<table class="summary-table margin40-b camelCaseName">';
		
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			
			var nameOfCurrentMember;

			//if(i == 0){
				var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
				nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
				var disabilityIndicator = householdMemberObj.specialCircumstances.americanIndianAlaskaNativeIndicator?'Yes':'No';
				appendRowData+='<tr><td>'+nameOfCurrentMember+'</td><td>'+disabilityIndicator+'</td></tr>';
}
		appendRowData+='</table>';
		var appendPregnantData ='<h5 class="non_financial_hide">Is Pregnant</h5>';
		appendPregnantData+='<table class="summary-table margin20-b non_financial_hide">';
		var isFemaleApplicant = false;
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			
	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	if(householdMemberObj.gender=='female'){
		isFemaleApplicant = true;
		var nameOfCurrentMember;
		nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
		var disabilityIndicator = householdMemberObj.specialCircumstances.pregnantIndicator?'Yes':'No';
		appendPregnantData+='<tr><td>'+nameOfCurrentMember+'</td><td>'+disabilityIndicator+'</td></tr>';

	}
}
		
		appendPregnantData+='</table>';
		if(isFemaleApplicant == true){
			appendRowData+=appendPregnantData;
		}
		

		appendRowData+='</div>';	
		

	}catch(e){
		e.message();
	}
	
	houseHoldSummaryHtml = appendRowData;
}







function showFinancialDetailsSummary(){

	$('#FinancialDetailsSummaryDiv form').html('');
	showFinancialDetailsSummaryJSON();
	$('#FinancialDetailsSummaryDiv form').append(incomeSummaryHtml);
	
}

function showFinancialDetailsSummaryJSON(){

	var idNumber = 0;
	var noOfHouseHold = parseInt(webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length);
	var appendRowData='';
	
	for(var cnt=0; cnt<noOfHouseHold; cnt++){

		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt];		
		var nameOfCurrentMember= getNameOfCurrentMember(cnt);
		nameOfCurrentMember=getNameRemovedBySpace(nameOfCurrentMember);
		appendRowData+='<div class="hixApplicantInfo camelCaseName"><h5>'+nameOfCurrentMember;
		if(cnt==0){
			appendRowData+='&nbsp;<small>'+jQuery.i18n.prop("ssap.primaryContact")+'</small>';
		}
		appendRowData+='<span class="pull-right"><input type="button" value="Edit" class="btn btn-primary" onclick="appscr74EditController('+(cnt+1)+')"/></span></h5>';

		for(key in householdMemberObj.detailedIncome) {
			var amount = 0;
			var incomeFrequency = "";
			  //if(householdMemberObj.detailedIncome(key)) {

				  switch(key)
				  {
				  case 'jobIncome':
					 data = householdMemberObj.detailedIncome[key];
					 amount = data[0].incomeAmountBeforeTaxes;
					 frequency = data[0].incomeFrequency;
					 incomeType = "Job";
				    break;
				  case "selfEmploymentIncome":
					  data = householdMemberObj.detailedIncome[key];
						 amount = data.monthlyNetIncome;
						 frequency = "Monthly";
						 incomeType = "Self Employment";
				    break;
				  case "socialSecurityBenefit":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.benefitAmount;
						frequency = data.incomeFrequency;
						incomeType = "Social Security Benefit";
				    break;
				  case "unemploymentBenefit":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.benefitAmount;
						frequency = data.incomeFrequency;
						incomeType = "Unemployment Benefit";
				    break;
				  case "retirementOrPension":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.taxableAmount;
						frequency = data.incomeFrequency;
						incomeType = "Retirement Or Pension";
				    break;
				  case "capitalGains":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.annualNetCapitalGains;
						frequency = "Yearly";
						incomeType = "Capital Gains";
				    break;
				  case "investmentIncome":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.incomeAmount;
						frequency = data.incomeFrequency;
						incomeType = "Investment Income";
				    break;
				  case "rentalRoyaltyIncome":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.netIncomeAmount;
						frequency = data.incomeFrequency;
						incomeType = "Rental Royalty Income";
				    break;
				  case "farmFishingIncome":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.netIncomeAmount;
						frequency = data.incomeFrequency;
						incomeType = "Farm Fishing Income";
				    break;
				  case "alimonyReceived":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.amountReceived;
						frequency = data.incomeFrequency;
						incomeType = "Alimony Received";
				    break;
				  case "otherIncome":
				    	data = householdMemberObj.detailedIncome[key];
				    	for(var i=0;i<data.length;i++){
					    	if(data[i].incomeAmount != "" || data[i].incomeAmount != null){
								amount = data[i].incomeAmount;
								frequency = data[i].incomeFrequency;
								incomeType = data[i].otherIncomeTypeDescription;
								if(Number(amount) > 0 ){
									appendRowData+='<table class="summary-table margin10-b"><tr><td>' +jQuery.i18n.prop("ssap.incomeType1")+'</td><td>'
									+incomeType+'</td></tr><tr><td>'+jQuery.i18n.prop("ssap.income")+'</td>'
									+'<td>$'+Moneyformat(amount);

									appendRowData+="/"+frequency+"</td></tr></table></div>" ;
								}
					    	}
				    	}
				    break;

				  default:
				    //code to be executed if n is different from case 1 and 2
				  }

				  if(key != 'otherIncome'){
				  if(Number(amount) > 0 ){
						appendRowData+='<table class="summary-table margin10-b"><tr><td>' +jQuery.i18n.prop("ssap.incomeType1")+'</td><td>'
						+incomeType+'</td></tr><tr><td>'+jQuery.i18n.prop("ssap.income")+'</td>'
						+'<td>$'+Moneyformat(amount);

						appendRowData+="/"+frequency+"</td></tr></table>" ;
					}
				  }
		}
				if(householdMemberObj.detailedIncome !== undefined 
					&& householdMemberObj.detailedIncome.deductions !== undefined){
						for(key in householdMemberObj.detailedIncome.deductions) {

							householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount
							amount = 0;
							switch(key)
							{
							case 'alimonyDeductionAmount':
							   amount = householdMemberObj.detailedIncome.deductions[key];
							   frequency = householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency;
							   incomeType = "Alimony";
							  break;
							case 'studentLoanDeductionAmount':
								   amount = householdMemberObj.detailedIncome.deductions[key];
								   frequency = householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency;
								   incomeType = "Student Loan Deduction";
								  break;
	  
							case 'otherDeductionAmount':
								   amount = householdMemberObj.detailedIncome.deductions[key];
								   frequency = householdMemberObj.detailedIncome.deductions.otherDeductionFrequency;
								   incomeType = householdMemberObj.detailedIncome.deductions.deductionType[2];
								  break;
							}
	  
	  
							if(Number(amount) > 0 ){
	  
								  appendRowData+='<table class="summary-table margin10-b"><tr><td>' +jQuery.i18n.prop("ssap.deductionsType")+'</td><td>'
								  +incomeType+'</td></tr><tr><td>Deduction</td>'
								  +'<td>$'+Moneyformat(amount);
	  
								  appendRowData+="/"+frequency+"</td></tr></table></div>" ;
								}
	  
						}
				}
				  
				 
			}

		incomeSummaryHtml = appendRowData;
		totalIncomeShowMethod();
	}

function caluculateFrequencywiseIncome(salary, frequency) {
	var w = new String(frequency);	
	var f = trimSpaces(w.toLowerCase());
	var salaryInfloat = parseFloat(new String(salary));
	var salaryReturned = 0.0;
	if (f == "hourly") {
		salaryReturned = salaryInfloat * 24 * 30;
	} else if (f == "daily") {
		salaryReturned = salaryInfloat * 30;
	} else if (f == "quarterly") {
		salaryReturned = salaryInfloat / 3;
	} else if (f = "monthly") {
		salaryReturned = salaryInfloat;
	} else if (f == "weekly") {
		salaryReturned = salaryInfloat * 4;
	} else if (f == "everytwoweeks") {
		salaryReturned = salaryInfloat * 2;
	} else if (f == "twiceamonth") {
		salaryReturned = salaryInfloat * 2;
	} else if (f == "yearly" || f == "onetimeonly") {
		salaryReturned = salaryInfloat / 12;
	}

	return salaryReturned;

}




function totalIncomeShowMethod()
{

	for(var i=0; i<noOfHouseHold; i++)
	{
			totalIncomeToArray(i+1);
			totalDeductionInArray(i+1);


		var totalIncomess = totalIncomes[i] - totalIncomeDeductions[i];

		$('#totalIncome72_'+i).text('  $  '+totalIncomess.toFixed(2)+"/"+jQuery.i18n.prop('ssap.monthly'));
		$('#deductionLabel_show_'+i).text('  $  '+totalIncomeDeductions[i]+"/"+jQuery.i18n.prop('ssap.monthly'));
	}



}

function getNameOfCurrentMember(i){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i];
	return getFormattedname(householdMemberObj.name.firstName) + " " + getFormattedname(householdMemberObj.name.middleName) + " " + getFormattedname(householdMemberObj.name.lastName);
}


//Keys
function addReviewSummary()
{

	if(useJSON == true){
		addReviewSummaryJson();
		return;
	}

}


function addReviewSummaryJson()
{
	
	generateReviewSummaryReport();
	if(currentPage == 'appscr67' && !webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator) { // suneel 05092014
		return;
	}

	else if (applicationStatus == "CC" || applicationStatus == "CL" || applicationStatus == "SG" || applicationStatus == "SU" || applicationStatus == "ER" || applicationStatus == "EN") {
		if( $("#csroverride").val()=="Y"){
			next();
		} else {
			return; // this is for preview application after esign and submit
		}
	}
	else {
		next();
	}

}

function generateReviewSummaryReport(){
	var strOuter = "";
	noOfHouseHold = getNoOfHouseHolds();
	document.getElementById('appscr84addReviewSummary').innerHTML = '';

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];

	var houseHoldName = getFormattedname(householdMemberObj.name.firstName) +" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
	//Show adress of primary contact only

	var address = householdMemberObj.householdContact.homeAddress.streetAddress1;
	if(householdMemberObj.householdContact.homeAddress.streetAddress2 !=null && householdMemberObj.householdContact.homeAddress.streetAddress2!="")
			address += "</br> " + householdMemberObj.householdContact.homeAddress.streetAddress2;
	if(householdMemberObj.householdContact.homeAddress.city !=null && householdMemberObj.householdContact.homeAddress.city!="")
			address += ", " + householdMemberObj.householdContact.homeAddress.city;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		address += ", " + householdMemberObj.householdContact.homeAddress.state;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.postalCode!="")
		address += ", " + householdMemberObj.householdContact.homeAddress.postalCode;

	var primaryApplicantAddress =  address;

	
	var mailingAddress = householdMemberObj.householdContact.mailingAddress.streetAddress1;
	if(householdMemberObj.householdContact.mailingAddress.streetAddress2 !=null && householdMemberObj.householdContact.mailingAddress.streetAddress2!="")
		mailingAddress += "</br> " + householdMemberObj.householdContact.mailingAddress.streetAddress2;
	if(householdMemberObj.householdContact.mailingAddress.city !=null && householdMemberObj.householdContact.mailingAddress.city!="")
		mailingAddress += ", " + householdMemberObj.householdContact.mailingAddress.city;
	if(householdMemberObj.householdContact.mailingAddress.state !=null && householdMemberObj.householdContact.mailingAddress.state!="")
		mailingAddress += ", " + householdMemberObj.householdContact.mailingAddress.state;
	if(householdMemberObj.householdContact.mailingAddress.postalCode !=null && householdMemberObj.householdContact.mailingAddress.postalCode!="")
		mailingAddress += ", " + householdMemberObj.householdContact.mailingAddress.postalCode;
	
	
	if(householdMemberObj.householdContact.otherPhone.phoneNumber == null) {
		householdMemberObj.householdContact.otherPhone.phoneNumber = '';
	}
	
	if(householdMemberObj.householdContact.phone.phoneNumber == null) {
		householdMemberObj.householdContact.phone.phoneNumber = '';
	}

	strOuter ='<div class="header"><h4 class="pull-left noBorder">Final Application Review and Confirmation</h4>'
	+'<a href="javascript:void(0)" class="btn btn-small pull-right" onclick="window.print()">'+jQuery.i18n.prop("ssap.print")+'</a>'
	+'<a href="javascript:generatePdf();" class="btn btn-small btn-primary pull-right">'+jQuery.i18n.prop("ssap.download")+'</a>'
	+'</div>'
	+'<div class="gutter10"><div class="hixApplicantInfo"><h5 camelCaseName>'+houseHoldName+'</h5><table class="summary-table">'
	+'<tbody>'
	+'<tr><td>Email</td><td>'+getFormattedname(householdMemberObj.householdContact.contactPreferences.emailAddress)+'</td></tr>';
	
	var ext = (householdMemberObj.householdContact.otherPhone.phoneExtension=="" || householdMemberObj.householdContact.otherPhone.phoneExtension==null)? "" : " Ext.: "+householdMemberObj.householdContact.otherPhone.phoneExtension; 
	if(householdMemberObj.householdContact.phone.phoneNumber != '' && householdMemberObj.householdContact.otherPhone.phoneNumber != '' ){
		strOuter+='<tr><td>Phone</td><td>'+formatPhoneNumbr(householdMemberObj.householdContact.phone.phoneNumber)+' / '+formatPhoneNumbr(householdMemberObj.householdContact.otherPhone.phoneNumber)+ ext +'</td></tr>';
	}
	if(householdMemberObj.householdContact.phone.phoneNumber != '' && householdMemberObj.householdContact.otherPhone.phoneNumber == '' ){
		strOuter+='<tr><td>Phone</td><td>'+formatPhoneNumbr(householdMemberObj.householdContact.phone.phoneNumber)+'</td></tr>';
	}
	if(householdMemberObj.householdContact.phone.phoneNumber == '' && householdMemberObj.householdContact.otherPhone.phoneNumber != '' ){
		strOuter+='<tr><td>Phone</td><td>'+formatPhoneNumbr(householdMemberObj.householdContact.otherPhone.phoneNumber)+ext+'</td></tr>';
	}
	if(householdMemberObj.householdContact.phone.phoneNumber == '' && householdMemberObj.householdContact.otherPhone.phoneNumber == '' ){
		//strOuter+='<tr><td class="span4 txt-right">Phone</td><td><strong>'+jQuery.i18n.prop("ssap.notAvailable")+'</td></tr>';
	}
	strOuter+='<tr><td>Date of Birth</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td></tr>';
	strOuter+='<tr><td>Home Address</td><td>'+address+'</td></tr>';
	strOuter+='<tr><td>Mailing Address</td><td>'+(mailingAddress == null? '':mailingAddress)+'</td></tr>';
	strOuter+='<tr><td>Preferred Phone Number</td><td>'+(householdMemberObj.householdContact.phone.phoneNumber == '' ? '': formatPhoneNumbr(householdMemberObj.householdContact.phone.phoneNumber) +' (Cell)' )+'</td></tr>';
	strOuter+='<tr><td>Second Phone Number</td><td>'+(householdMemberObj.householdContact.otherPhone.phoneNumber == '' ? '': formatPhoneNumbr(householdMemberObj.householdContact.otherPhone.phoneNumber) + ' (Home)')+ext+'</td></tr>';
	strOuter+='<tr><td>Preferred Spoken Language</td><td>'+(householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage == null? '':householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage)+'</td></tr>';
	strOuter+='<tr><td>Preferred Written Language</td><td>'+(householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage == null? '':householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage)+'</td></tr>';
	strOuter+='<tr><td>Preferred Method of Communication</td><td>'+(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == null? '' : householdMemberObj.householdContact.contactPreferences.preferredContactMethod)+'</td></tr>';
	
	
	strOuter+='</tbody></table>';
	
	strOuter+='<h5 class="margin40-t margin20-b">Applying for health coverage</h5>';
	strOuter+='<table class="table table-condensed">';
	strOuter+='<tr><td>Name</td><td>Relationship</td><td>Date of Birth</td><td>Seeking Coverage</td><td>&nbsp;</td></tr>';
	for ( var i = 1; i <= noOfHouseHold; i++) {
		
			var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+ getFormattedname(householdMemberObj.name.middleName) +" "+getFormattedname(householdMemberObj.name.lastName);
			
			//relationship
			if(i>1){
				var relationText = getRelationshipText(householdMemberObj.personId);
				if($('#applicationSource').val() == 'RF' || $('#applicationSource').val() == 'CN'){
					strOuter+='<tr><td>'+nameOfCurrentMember+'</td><td>' + relationText + '</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td><td>'+ ((householdMemberObj.applyingForCoverageIndicator == true)? 'Yes' : 'No') + '</td><td>&nbsp;</td></tr>';
				}else{
					strOuter+='<tr><td>'+nameOfCurrentMember+'</td><td>' + relationText + '</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td><td>'+ ((householdMemberObj.applyingForCoverageIndicator && householdMemberObj.under26Indicator)? 'Yes' : 'No') + '</td><td>&nbsp;</td></tr>';
				}
			}else{
				
				if($('#applicationSource').val() == 'RF' || $('#applicationSource').val() == 'CN'){
					strOuter+='<tr><td>'+nameOfCurrentMember+'</td><td>Self</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td><td>'+ ((householdMemberObj.applyingForCoverageIndicator)? 'Yes' : 'No') + '</td><td>&nbsp;</td></tr>';
				}else{
					strOuter+='<tr><td>'+nameOfCurrentMember+'</td><td>Self</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td><td>'+ ((householdMemberObj.applyingForCoverageIndicator && householdMemberObj.under26Indicator)? 'Yes' : 'No') + '</td><td>&nbsp;</td></tr>';
				}
			}
	}
	strOuter+='</table></div></div>';
	strOuter+='<div class="header"><h4>Household Members</h4></div><div class="gutter10">';
	
	//for review application after logout
	if(houseHoldSummaryHtml == ''){
		createHouseHoldContactSummaryHtml();
	}
	strOuter+=houseHoldSummaryHtml;
	strOuter+='</div><div class="header non_financial_hide"><h4>Income</h4></div><div class="gutter10 non_financial_hide">';
	//for review application after logout
	 if(incomeSummaryHtml == '' && webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator == true){
		showFinancialDetailsSummaryJSON();
	}
	strOuter+=incomeSummaryHtml;
	strOuter+='</div>';	
	
	document.getElementById('appscr84addReviewSummary').innerHTML = strOuter;

	if(ssapApplicationType == "NON_FINANCIAL"){
		$(".non_financial_hide").css("display","none");
	}else{
		$(".non_financial_hide").css("display","");
	}

}


function getRelationshipText(relatedPersonId){
	var bloodRelationshipArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	var relationShipCode = '';
	var reversedRelation = '';
	//relationship
	for(var i=0;i<bloodRelationshipArr.length;i++){
		if(bloodRelationshipArr[i].relatedPersonId == 1  && bloodRelationshipArr[i].individualPersonId == relatedPersonId ){
			
			//one to many relationship
			if(bloodRelationshipArr[i].relation == '03' || bloodRelationshipArr[i].relation == '15'){	//multiple relationship
				
				//get reversed relation
				for(var j=0; j<bloodRelationshipArr.length; j++){			
					if(bloodRelationshipArr[j].relatedPersonId == relatedPersonId && bloodRelationshipArr[j].individualPersonId == 1){
						reversedRelation = bloodRelationshipArr[j].relation;
					}
				}
				
				//get relation code based on its reversed relation
				if(reversedRelation == '09'){
					relationShipCode = '03a';
				}else if(reversedRelation == '10'){
					relationShipCode = '03f';
				}else if(reversedRelation == '19'){
					relationShipCode = '03';
				}else if(reversedRelation == '26'){
					relationShipCode = '15';
				}else if(reversedRelation == '31'){
					relationShipCode = '15c';
				}else if(reversedRelation == '15'){
					relationShipCode = '03w';
				}else if(reversedRelation == '03'){
					relationShipCode = '15p';
				}else{
					relationShipCode = bloodRelationshipArr[i].relation;
				}
			
			}else{
				relationShipCode = bloodRelationshipArr[i].relation;
			}
			
			return  $("#relationshipOptions_all option[value="+relationShipCode+"]").text();
			
		}
	}
}



function CapitaliseFirstLetter(elementId) {
    var txt = $("#" + elementId).val().toLowerCase();
    $("#" + elementId).val(txt.replace(/^(.)|\s(.)/g, function($1) {
    return $1.toUpperCase(); }));
    }

function replaceNullWithEmpty(elementId){
	if(elementId!=null){
		return elementId.replace("null", ""); 
	}else{
		return "";
	}
}
function formatPhoneNumbr(strPhoneNo){
	if(strPhoneNo =="" || strPhoneNo == null || strPhoneNo == undefined){
		return "";
	}else{
		return strPhoneNo.replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3");
	}
	
}
