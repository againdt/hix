/**
 * 
 */
package com.getinsured.eligibility.at.ref.dozzer.converter;

import org.dozer.DozerConverter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.iex.ssap.enums.IncomeFrequencyEnum;



/**
 * @author chopra_s
 * 
 * 
 */
@Component("dozzerIncomeFrequencyConverter")
@Scope("singleton")
public class IncomeFrequencyConverter
		extends
		DozerConverter<com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType, String> {

	public IncomeFrequencyConverter() {
		super(
				com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType.class,
				String.class);
	}

	@Override
	public com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType convertFrom(
			String arg0,
			com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType arg1) {
		return null;
	}

	@Override
	public String convertTo(
			com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType source,
			String dest) {
		if (source != null) {
			String s = source.getValue();
			for (IncomeFrequencyEnum c : IncomeFrequencyEnum.values()) {
				if (c.value().equals(s)) {
					return c.value();
				}
			}
		}
		return null;
	}
}
