package com.getinsured.hix.cap.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.cap.consumer.dto.ConsumerDto;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.ServerSideValidationUtil;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component
public class CapRequestValidatorUtil extends ServerSideValidationUtil {
	
	private static final String MM_DD_YYYY= "MM/dd/yyyy";
	
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	public boolean validateEncryptedId(String encId) {
		boolean isValidReq = true;
		
		if(!StringUtils.isNumeric(encId)) {
			try {
				ghixJasyptEncrytorUtil.decryptStringByJasypt(encId);
			} catch(Exception e) {
				isValidReq = false;
			}
		}
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	private void throwServerSideValidationException(boolean isValidReq) {
		if(!isValidReq){
			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}
	
	public boolean validateAddConsumerSubmit(ConsumerDto consumer) {
		boolean isValidReq = true;
		
		String firstName = consumer.getFirstName();
		String lastName = consumer.getLastName();
		String dob = consumer.getDob();
		String zip = consumer.getZipcode();
		String phone = consumer.getPhoneNumber();
		String email = consumer.getEmailAddress();
		
		isValidReq = isNotBlank(firstName) && Utils.isPureAscii(firstName) 
						&& isNotBlank(lastName) && Utils.isPureAscii(lastName)
							&& isValidDate(dob, MM_DD_YYYY)
								&& isValidZipCode(zip)
									&& isValidPhoneNumber(phone)
										&& isValidEmail(email, true);
		
	
		return isValidReq;
	}
	
	public boolean validateSearchMember(HttpServletRequest request) {
		boolean isValidReq = true;
		
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String phone = request.getParameter("contactNumber");
		String email = request.getParameter("householdEmail");
		String zip = request.getParameter("zipCode");
		String pageNumber = request.getParameter("pageNumber");
				
		isValidReq = isNotBlank(firstName, true)
				&& isNotBlank(lastName, true)
					&& isValidPhoneNumber(phone, true)
						&& isValidEmail(email, true)
							&& isValidZipCode(zip, true)
								&& isNumeric(pageNumber, true);
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	public boolean validateViewMember(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateMarkRidpVerified(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateConvertManualRIDP(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateEditMember(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateEditMemberSubmit(Household household) {
		boolean isValidReq = true;
		
		String firstName = household.getFirstName();
		String lastName = household.getLastName();
		String phone = household.getPhoneNumber();
		String email = household.getEmail();
		
		isValidReq = isNotBlank(firstName)
						&& isNotBlank(lastName)
							&& isValidPhoneNumber(phone)
								&& isValidEmail(email, true);
				
		return isValidReq;
		
	}
	
	public boolean validateViewEnrollment(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateGetComments(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateGetHistory(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateGetConsumerAppealHistory(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateGetConsumerTicketHistory(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateGetSecurityQuestions(String encHouseholdId) {
		return validateEncryptedId(encHouseholdId);
	}
	
	public boolean validateSendActivationLink(String encHouseholdId, String email) {
		return (validateEncryptedId(encHouseholdId) && isValidEmail(email));
	}
	
	public boolean validateSearchApplicants(HttpServletRequest request) {
		boolean isValidReq = true;
		
		String effectiveYear = request.getParameter("effectiveYear");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String phone = request.getParameter("contactNumber");
		String ssn = request.getParameter("ssn");
		String appId = request.getParameter("appId");
		String extAppId = request.getParameter("extAppId");
		String appSource = request.getParameter("appSource");
		String accessCode = request.getParameter("accessCode");
		String pageNumber = request.getParameter("pageNumber");
		String cmrHouseholdId = request.getParameter("cmrHouseholdId");
		String externalApplicantId = request.getParameter("externalApplicantId");
		String externalCaseId = request.getParameter("externalCaseId");
		
		isValidReq = (StringUtils.isBlank(effectiveYear) || (isNotBlank(effectiveYear) && (("All".equalsIgnoreCase(effectiveYear)) || ((isNumeric(effectiveYear)) && (effectiveYear.length() == 4)))))
						&& (StringUtils.isBlank(appSource) || (isNotBlank(appSource) && (("All".equalsIgnoreCase(appSource)) || ((StringUtils.isAlpha(appSource)) && (appSource.length() == 2)))))
							&& isNotBlank(firstName, true)
								&& isNotBlank(lastName, true)
									&& isValidPhoneNumber(phone, true)
										&& isNumeric(ssn, true)
											&& isNotBlank(appId, true)
												&& isNotBlank(extAppId, true)
													&& isNotBlank(accessCode, true)
						&& isNumeric(pageNumber, true)
						&& isNumeric(cmrHouseholdId, true)
						&& isNotBlank(externalApplicantId, true)
						&& isNotBlank(externalCaseId, true);
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	public boolean validateShowCreateTicketPage(HttpServletRequest request) {
		boolean isValidReq = true;
		
		String prefillUser = request.getParameter("prefillUser");
		String roleName = request.getParameter("roleName");
		String moduleId = request.getParameter("moduleId");
		String moduleName = request.getParameter("moduleName");
		String requestorId = request.getParameter("requestorId");
		String requestorName = request.getParameter("requestorName");
		
		isValidReq = isTrueOrFalse(prefillUser, true)
						&& (StringUtils.isBlank(roleName) || (isNotBlank(roleName) && ("INDIVIDUAL".equalsIgnoreCase(roleName))))
							&& isNumeric(moduleId, true)
								&& (StringUtils.isBlank(moduleName) || (isNotBlank(moduleName) && ("HOUSEHOLD".equalsIgnoreCase(moduleName))))
									&& isNumeric(requestorId, true)
										&& isNotBlank(requestorName, true);
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	public boolean validateSaveTicket(HttpServletRequest request) {
		boolean isValidReq = true;
		
		String id = request.getParameter("id");
		String prevPage = request.getParameter("prevPage");
		String subject = request.getParameter("subject");
		String category = request.getParameter("tkmWorkflows.category");
		String type = request.getParameter("tkmWorkflows.type");
		String userRoleId = request.getParameter("userRole.id");
		String company = request.getParameter("company");
		String moduleId = request.getParameter("moduleId");
		String moduleName = request.getParameter("moduleName");
		String roleId = request.getParameter("role.id");
		String priority = request.getParameter("priority");
		String description = request.getParameter("description");
		
		isValidReq = isNumeric(id, true)
						&& isNotBlank(prevPage, true)
							&& isNotBlank(subject)
								&& isNotBlank(category)
									&& isNotBlank(type, true)
										&& isNumeric(userRoleId)
											&& isNotBlank(company, true)
												&& isNumeric(moduleId, true)
													&& isNotBlank(moduleName, true)
														&& isNumeric(roleId)
															&& StringUtils.isAlpha(priority)
																&& isNotBlank(description);
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	public boolean validateTicketList(HttpServletRequest request) {
		boolean isValidReq = true;
			
		String ticketSubject = request.getParameter("ticketSubject");
		String ticketComments = request.getParameter("ticketComments");
		String requestedByName = request.getParameter("requestedByName");
		String requestedBy = request.getParameter("requestedBy");
		String ticketQueues = (request.getParameter("ticketQueues") != null) ? (request.getParameter("ticketQueues").replaceAll(",", StringUtils.EMPTY).replaceAll(" ", StringUtils.EMPTY)) : StringUtils.EMPTY;
		String ticketPriority = (request.getParameter("ticketPriority") != null) ? (request.getParameter("ticketPriority").replaceAll(",", StringUtils.EMPTY)) : StringUtils.EMPTY;
		String assignee = request.getParameter("assignee");
		String assigneeId = request.getParameter("assigneeId");
		String ticketNumber = (request.getParameter("ticketNumber") != null) ? (request.getParameter("ticketNumber").replaceAll("TIC-", StringUtils.EMPTY)) : StringUtils.EMPTY;
		String submittedBefore = request.getParameter("submittedBefore");
		String submittedAfter = request.getParameter("submittedAfter");
		String ticketStatus = (request.getParameter("ticketStatus") != null ) ? (request.getParameter("ticketStatus").replaceAll(",", StringUtils.EMPTY)) : StringUtils.EMPTY;
		String taskStatus = request.getParameter("taskStatus");
		String assignedToMe = request.getParameter("assignedToMe");
		String unClaimedTicket = request.getParameter("unClaimedTicket");
		String roleId = request.getParameter("roleId");
		String requester = request.getParameter("requester");
		String userRole = request.getParameter("userRole");
		String archiveFlagValue = request.getParameter("archiveFlagValue");
		String pageNumber = request.getParameter("pageNumber");
		
		isValidReq = isNotBlank(ticketSubject, true)
						&& isNotBlank(ticketComments, true)
							&& isNotBlank(requestedByName, true)
								&& isNumeric(requestedBy, true)
									&& StringUtils.isAlpha(ticketQueues)
										&& StringUtils.isAlpha(ticketPriority)
											&& isNotBlank(assignee, true)
												&& isNumeric(assigneeId, true)
													&& isNumeric(ticketNumber, true)
														&& isValidDate(submittedBefore, MM_DD_YYYY, true)
															&& isValidDate(submittedAfter, MM_DD_YYYY, true)
																&& StringUtils.isAlpha(ticketStatus)
																	&& isNotBlank(taskStatus, true)
																		&& isTrueOrFalse(assignedToMe, true)
																			&& isTrueOrFalse(unClaimedTicket, true)
																				&& isNumeric(roleId, true)
																					&& isNotBlank(requester, true)
																						&& isNumeric(userRole, true)
																							&& isTrueOrFalse(archiveFlagValue, true)
																								&& isNumeric(pageNumber, true);
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	public boolean validateGetTicketDetailById(String encTicketId) {
		return validateEncryptedId(encTicketId);
	}
	
	public boolean validateShowEditTicketPage(String encTicketId) {
		return validateEncryptedId(encTicketId);
	}
	
	public boolean validateEditTicketSubmit(HttpServletRequest request) {
		boolean isValidReq = true;
		
		String id = request.getParameter("id");
		String prevPage = request.getParameter("prevPage");
		String subject = request.getParameter("subject");
		String userRoleId = request.getParameter("userRole.id");
		String company = request.getParameter("company");
		String moduleId = request.getParameter("moduleId");
		String moduleName = request.getParameter("moduleName");
		String roleId = request.getParameter("role.id");
		String priority = request.getParameter("priority");
		String description = request.getParameter("description");
		
		isValidReq = isNumeric(id, true)
						&& isNotBlank(prevPage, true)
							&& isNotBlank(subject)
								&& isNumeric(userRoleId)
									&& isNotBlank(company, true)
										&& isNumeric(moduleId, true)
											&& isNotBlank(moduleName, true)
												&& isNumeric(roleId)
													&& StringUtils.isAlpha(priority)
														&& isNotBlank(description);
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	public boolean validateCompleteUserTask(Map map, String comments, String commentLength) {
		boolean isValidReq = true;
		
		String activitiTaskId = String.valueOf(map.get("ActivitiTaskId"));
		String processInstanceId = String.valueOf(map.get("ProcessInstanceId"));
		String taskId = String.valueOf(map.get("TaskId"));
		String ticketId = String.valueOf(map.get("TicketId"));
		String validateComments=String.valueOf(map.get("ValidateComments"));
		
		isValidReq = validateEncryptedId(activitiTaskId)
						&& isNumeric(processInstanceId, true)
							&& isNumeric(taskId, true)
								&& isNotBlank(ticketId, true)
									&& (validateComments.equalsIgnoreCase("N") || isNotBlank(comments))
										&& isNumeric(commentLength);
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	public boolean validateGetTicketHistory(String encTicketId, HttpServletRequest request) {
		String pageNumber = request.getParameter("pageNumber");
		
		return validateEncryptedId(encTicketId) && isNumeric(pageNumber, true);
	}
	
	public boolean validateShowComment(String encTicketId) {
		return validateEncryptedId(encTicketId);
	}
	
	public boolean validateLoadTicketDocuments(String encTicketId) {
		return validateEncryptedId(encTicketId);
	}
	
	public boolean validateSaveAppealFiles(String encTicketId, String fileName) {
		return validateEncryptedId(encTicketId) && isNotBlank(fileName);
	}
	
	public boolean validateGetTicketTaskList(String encTicketId) {
		return validateEncryptedId(encTicketId);
	}
	
	public boolean validateGetReclassfiedTargetProperties(String ticketId, String category, String type) {
		boolean isValidReq = true;
		
		isValidReq = validateEncryptedId(ticketId)
						&& isNotBlank(category)
									&& isNotBlank(type);
		
		throwServerSideValidationException(isValidReq);
		
		return isValidReq;
	}
	
	public String removeSpecialCharacters(String inputString) {
		if (inputString == null || inputString.trim().isEmpty()) {
			return null;
		}
		
		int totalChars = inputString.length();
		if(inputString.startsWith("=") || inputString.startsWith("@") || inputString.startsWith("+") || inputString.startsWith("-")) {
			return inputString.substring(1,totalChars);
		} else {
			return inputString;
		}

	}

}
