package com.getinsured.hix.batch.enrollment.xmlEnrollments.partition;
 
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;

import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
 
public class EnrollmentPartitioner implements Partitioner {
 
    private String inboundDir;
    
    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {
 
    	inboundDir = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.INBOUND834XMLPATH);
        Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
        File dir = new File(inboundDir);
        
        //Move all files from inbound834XMLPath to WIP folder
        
        String wipFolderPath = inboundDir + File.separator + EnrollmentConstants.WIP_FOLDER_NAME;
        File wipFolder = new File(wipFolderPath);
        boolean wipPathValid = true;
        

        if (dir != null && !(wipFolder.exists())) {
        	if(!wipFolder.mkdirs()){
        		wipPathValid = false;
        	}
        }
        
        if(wipPathValid){
	        //Include existing files in WIP folder, if any
	        if (wipFolder.isDirectory()) {
	            File[] files = wipFolder.listFiles();
	            for (File file : files) {
	                if(file.isFile() && file.getName().toLowerCase().endsWith(EnrollmentConstants.FILE_TYPE_XML)){
	            	   ExecutionContext context = new ExecutionContext();
	            	   context.put("fileResource", file.toURI().toString());
	               	   context.put("inboundDir", inboundDir);
	            	   partitionMap.put(file.getName(), context);
	               }
	            }
	        }
	        
	      //Move files from inboundDir to WIP folder
	        if (dir.isDirectory()) {
	            File[] files = dir.listFiles();
	            for (File file : files) {
	                if(file.isFile() && file.getName().toLowerCase().endsWith(EnrollmentConstants.FILE_TYPE_XML)){
	            	   ExecutionContext context = new ExecutionContext();
	            	   File wipFile = new File(wipFolderPath + File.separator+ file.getName());
	            	   file.renameTo(wipFile);
	            	   context.put("fileResource", wipFile.toURI().toString());
	               	   context.put("inboundDir", inboundDir);
	            	   partitionMap.put(wipFile.getName(), context);
	               }
	            }
	        }
        }
        
        return partitionMap;
    }
 
    public String getInboundDir() {
        return inboundDir;
    }
 
    public void setInboundDir(String inboundDir) {
        this.inboundDir = inboundDir;
    }
 
}

