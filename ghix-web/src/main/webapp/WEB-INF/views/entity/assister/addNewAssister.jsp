<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<META http-equiv="Cache-Control" content="max-age=0" />
<META http-equiv="Cache-Control" content="no-cache,no-store,must-revalidate" />
<META HTTP-EQUIV="Expires" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
<META http-equiv="Pragma" content="no-cache" />

<link href="<c:url value="/resources/css/chosen.css" />" rel="stylesheet" type="text/css" media="screen,print">
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>

 <script type="text/javascript">
  $(document).ready(function(){
	  var config = {
		      '.chosen-select'           : {},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
  });

	$(document).ready(function() {
		// disable autocomplete
		if (document.getElementsByTagName) {
			var inputElements = null;
			var elemIds = ["address1", "address2", "city", "state", "zip"];
			
			for(i=0; elemIds[i]; i++) {
				inputElements = document.getElementById(elemIds[i]);
				
				for (j=0; inputElements[j]; j++) {							
					if (inputElements[j].className && (inputElements[j].className.indexOf("disableAutoComplete") != -1)) {
						inputElements[j].setAttribute("autocomplete","off");
					}
				}
			}
		}
	});

</script>

				<input id="tokid" type="hidden" value="${sessionScope.csrftoken}" />	
				<div class="gutter10">
					<div class="l-page-breadcrumb hide">
						<!--start page-breadcrumb -->
						<div class="row-fluid">
							<ul class="page-breadcrumb">
								<li><a href="#">&lt; <spring:message code="label.assister.back"/></a></li>
								<li><a href="<c:url value="/entity/entityadmin/displayassisters" />"><spring:message code="label.assister.assisters"/></a></li>
								<li><a href="<c:url value="/entity/entityadmin/displayassisters" />"><spring:message code="label.assister.manage"/></a></li>
								<li><spring:message code="label.assister.addNewAssister"/></li>
							</ul>
							<!--page-breadcrumb ends-->
						</div><!-- end of .row-fluid -->
					</div><!--l-page-breadcrumb ends-->
					
					<div class="row-fluid">
						<div id="sidebar" class="span3">
							<div class="header"><spring:message code="label.assister.addNewAssister"/></div><br>
							<ul class="nav nav-list">
								<li class="active"><a href="#">1 
								<spring:message code="label.assister.newAssisterForm"/></a></li>
							</ul>
						</div><!-- end of span3 -->
						<div class="span9">
							<form class="form-horizontal gutter10 entityAddressValidation" id="frmAssister" enctype="multipart/form-data" name="frmAssister" action="addnewassister" method="POST" autocomplete="off">
							<df:csrfToken/>
					<input type="hidden" id="assisterId" name="assisterId" value="<encryptor:enc value="${assister.id}"/>" />
					<input type="hidden" name="assisterLanguageId" name="assisterLanguageId" value="<encryptor:enc value="${assisterLanguages.id}"/>"/>
						<div class="header">
							<h4><spring:message code="label.assister.newAssisterForm"/></h4>
						</div>
						<div class="control-group">
						</div>
						<div class="control-group">
							<label for="firstName" class="required control-label" id="firstName"><spring:message code="label.assister.firstName"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="firstName" id="firstName" value="${assister.firstName}" class="input-xlarge" size="30" maxlength="50"/>
								<div id="firstName_error"></div>
								</div>
							</div>
						<div class="control-group">
							<label for="lastName" class="required control-label" id="lastName"><spring:message code="label.assister.lastName"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="lastName" id="lastName" value="${assister.lastName}" class="input-xlarge" size="30" maxlength="50"/>						
					    		<div id="lastName_error"></div>
					    		</div>
					     </div>
						 
						 
						 <div class="control-group">
			                <label class="control-label" for="emailAddress"><spring:message code="label.assister.emailAddress"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
			                <div class="controls">
			                  <input type="text" name="emailAddress" id="emailAddress" value="${assister.emailAddress}" class="input-xlarge" size="30" placeholder="<spring:message code="label.assister.asscompany@email.com"/>"/>
			                	<div id="emailAddress_error"></div>
			                </div>
		             	 </div>
						 
						<div class="control-group phone-group">
							<label for="primaryPhoneNumber" class="required control-label"><spring:message code="label.assister.phoneNumber"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<label for="primaryPhone1" class="hide">Primary phone 1</label>
							<label for="primaryPhone2" class="hide">Primary phone 2</label>
							<label for="primaryPhone3" class="hide">Primary phone 3</label>
							<div class="controls">
								<input type="text" name="primaryPhone1" id="primaryPhone1" value="${primaryPhone1}" maxlength="3" placeholder="xxx" class="area-code input-mini"  />
								<input type="text" name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}" maxlength="3" placeholder="xxx" class="input-mini" />
								<input type="text" name="primaryPhone3" id="primaryPhone3" value="${primaryPhone3}" maxlength="4" placeholder="xxxx" class="input-small" />
								<input type="hidden" name="primaryPhoneNumber" id="primaryPhoneNumber" value="" />      
								<div id="primaryPhone3_error"></div>
								<div id="primaryPhone1_error"></div>
							</div>
						</div>
						<div class="control-group phone-group">
							<label for="secondaryPhoneNumber" class="control-label"><spring:message code="label.assister.secondaryPhoneNumber"/></label>
							<label for="secondaryPhone1" class="hide">Secondary phone 1</label>
							<label for="secondaryPhone2" class="hide">Secondary phone 2</label>
							<label for="secondaryPhone3" class="hide">Secondary phone 3</label>
							<div class="controls">
								<input type="text" name="secondaryPhone1" id="secondaryPhone1" value="${secondaryPhone1}" maxlength="3" placeholder="xxx" class="area-code input-mini"  />
								<input type="text" name="secondaryPhone2" id="secondaryPhone2" value="${secondaryPhone2}" maxlength="3" placeholder="xxx" class="input-mini" />
								<input type="text" name="secondaryPhone3" id="secondaryPhone3" value="${secondaryPhone3}" maxlength="4" placeholder="xxxx" class="input-small" />
								<input type="hidden" name="secondaryPhoneNumber" id="secondaryPhoneNumber" value="" />
								<div id="secondaryPhone3_error"></div>
							</div>
						</div>
						<%-- <div class="control-group">
			                <label class="control-label" for="businessLegalName"><spring:message code="label.assister.businessLegalName"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
			                <div class="controls">
			                  <input type="text" name="businessLegalName" id="businessLegalName" value="${assister.businessLegalName}" class="input-xlarge" size="30" placeholder=""/>			                	
			                </div>
		             	 </div> --%>
						 <div class="control-group">
						 <fieldset>
								<legend class="control-label" for="communicationPreference"><spring:message code="label.assister.preferredMethodofCommunication"/></legend>
								<div class="controls">
									
										<label class="radio inline" for="email"> 
											<c:choose>
												<c:when test="${assister.communicationPreference == 'email'}">
													<input type="radio" name="communicationPreference" id="email" value="email" checked />
												</c:when>
												<c:otherwise>
													<input type="radio" name="communicationPreference" id="email" value="email" />
												</c:otherwise>
											</c:choose> 
											<spring:message code="label.assister.emailAddress"/>
										</label> <br>
									
										<label class="radio inline" for="phone"> 
											<c:choose>
												<c:when test="${assister.communicationPreference == 'phone'}">
													<input type="radio" name="communicationPreference" id="phone" value="phone" checked />
												</c:when>
												<c:otherwise>
													<input type="radio" name="communicationPreference" id="phone" value="phone" checked="checked"/>
												</c:otherwise>
											</c:choose> 
											<spring:message code="label.entity.phone"/>
										</label> <br>
									
										<label class="radio inline" for="mail"> 
											<c:choose>
												<c:when test="${assister.communicationPreference == 'mail'}">
													<input type="radio" name="communicationPreference" id="mail" value="mail" checked />
												</c:when>
												<c:otherwise>
													<input type="radio" name="communicationPreference" id="mail" value="mail" />
												</c:otherwise>
											</c:choose> 
											<spring:message code="label.assister.Mail"/>
										</label>
									
									
									<div id="communicationPreference_error"></div>
								</div>
								</fieldset>
						</div>
						
						<c:choose>
						 						<c:when test="${showPostalMailOption == 'true'}">
						 								<div class="control-group" >
						 						</c:when>
						 						<c:otherwise>
						 								<div class="control-group" style="visibility: hidden;display: none;">
						 						</c:otherwise>
						 	</c:choose>
							
						 		<fieldset>
						 				<legend for="postalMail" class="control-label"> <spring:message code="label.assister.receiveNotices"/> </legend>  
						 			
						 				<div class="controls">
						 					<c:choose>
						 						<c:when test="${assister.postalMail!=null && assister.postalMail == 'Y'}">
						 								<input type="checkbox" name="postalMail" value="Y" checked="checked" id="postalMail" />
						 						</c:when>
						 						<c:otherwise>
						 								<input type="checkbox" name="postalMail" value="Y"  id="postalMail" />
						 						</c:otherwise>
						 					</c:choose>
						 						
						 						<div id="postalMail_error"></div>
						 						<div> <spring:message code="label.assister.alwaysReceiveEmail"/></div>
						 				</div>
						 		</fieldset>
							</div>
						
							 <div class="control-group">
							 <fieldset>
								<legend for="isAssisterCertified" class="required control-label"><spring:message code="label.assister.isCertifiedAssister"/></legend>
								<div class="controls">
								<label for="selectNo">
									<c:if test="${assister.certificationNumber!=null && assister.certificationNumber != ''}">
										<input type="radio" name="isAssisterCertified" value="No" id="selectNo" onclick="$('#certification').hide()"><spring:message code="label.assister.No"/><br>
										</label>
										<label for="selectYes">
										<input type="radio"	name="isAssisterCertified" value="Yes" id="selectYes" onclick="$('#certification').show()" checked><spring:message code="label.assister.Yes"/>
										</label>
									</c:if>
									
									<c:if test="${assister.certificationNumber==null || assister.certificationNumber == ''}">
									<label for="selectNo">
										<input type="radio" name="isAssisterCertified" value="No" id="selectNo" onclick="$('#certification').hide()" checked><spring:message code="label.assister.No"/><br>
										</label>
										<label for="selectYes">
										<input type="radio"	name="isAssisterCertified" value="Yes" id="selectYes" onclick="$('#certification').show()"><spring:message code="label.assister.Yes"/>
										</label>
									</c:if>
								</div>
								</fieldset>
						</div>
						
						<div id="certification" style="display: none" class="control-group">
							<label for="assisterCertification" class="required control-label"><spring:message code="label.assister.AssisterCertification"/>#</label>
							<div class="controls">
								<c:choose>
									<c:when test="${assister.certificationNumber != null && assister.certificationNumber != ''}">
										<input type="text" name="certificationNumber" id="certificationNumber" value="<fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/>" readonly="readonly" class="input-xlarge" size="30" />
									</c:when>
									<c:otherwise>
										<input type="text" name="certificationNumber" id="certificationNumber" maxlength="10" value="<fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/>" class="input-xlarge" size="30" />
									</c:otherwise>
								</c:choose> 
					 			<div id="certificationNumber_error"></div>
							</div>
						</div>																					
					
						<div class="control-group">
							<label for="primarySite" class="control-label"><spring:message code="label.assister.priAssisterSite"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<select id="primarySite" name="primarySite" path="sitelist" class="ie8">
									<option value=""><spring:message code="label.assister.select"/></option>
									<c:forEach var="primary" items="${primarySitelist}">
										<option id="${primary}" 
											<c:if test="${primary.id == assister.primaryAssisterSite.id}"> selected="selected"</c:if> value="<c:out value="${primary.id}" />"> ${primary.siteLocationName}
										</option>
									</c:forEach>
								</select> 
<%-- 								<input type="hidden" id="primaryAssisterSite_id" name="primaryAssisterSite_id" value="${priSite}">
 --%>								<div id="primarySite_error"></div>
							</div>
						</div>
		
						<div class="control-group">
							<label for="secondarySite" class="control-label"><spring:message code="label.assister.secAssisterSite"/></label>
							<div class="controls">
								<select id="secondarySite" name="secondarySite" path="sitelist" class="ie8">
									<option value=""><spring:message code="label.assister.select"/></option>
									<c:forEach var="secondary" items="${secondarySitelist}">
										<option id="${secondary.id}"
											<c:if test="${secondary.id == assister.secondaryAssisterSite.id}">selected="selected"</c:if>
												value="<c:out value="${secondary.id}" />">
												${secondary.siteLocationName}
										</option>	
																	
									</c:forEach>									
								</select> 
<%-- 								<input type="hidden" id="secondaryAssisterSite_id" name="secondaryAssisterSite_id" value="${secSite}">
 --%>							</div>
						</div>
		
						<div class="header">
							<h4 class="pull-left"><spring:message code="label.assister.mailingAddress"/></h4>
						</div>
						
						<div class="control-group">
						</div>
						<div class="control-group">
							<label for="address1" class="control-label required"><spring:message code="label.assister.streetAddress"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">							
								<input type="text" placeholder="<spring:message code="label.address1placeholder"/>" name="mailingLocation.address1" id="address1" value="${assister.mailingLocation.address1}" class="input-xlarge" size="30" maxlength="25" autocomplete="off"/>
								<input type="hidden" id="address1_hidden" name="address1_hidden" value="${assister.mailingLocation.address1}">
								<div id="address1_error"></div>
							</div>	
						</div>	
						
						<div class="control-group">
							<label for="address2" class="control-label"><spring:message code="label.assister.suite"/></label>
							<div class="controls">
								<input type="text" placeholder="<spring:message code="label.address2placeholder"/>" name="mailingLocation.address2" id="address2" value="${assister.mailingLocation.address2}" class="input-xlarge" size="30" maxlength="25" autocomplete="off"/>
								<input type="hidden" id="address2_hidden" name="address2_hidden" value="${assister.mailingLocation.address2}">
							</div>
						</div>
						 
						 <div class="control-group">
								<label for="city" class="control-label"><spring:message code="label.assister.city"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
								<div class="controls">
									<input type="text" placeholder="<spring:message code="label.cityplaceholder"/>" name="mailingLocation.city" id="city" value="${assister.mailingLocation.city}" class="input-xlarge" size="30" maxlength="15" autocomplete="off"/>
									<input type="hidden" id="city_hidden" name="city_hidden" value="${assister.mailingLocation.city}">
								<div id="city_error"></div>
								</div>	
						</div>	
						 
						 	
						<div class="control-group">
							<label for="state" class="control-label"><spring:message code="label.assister.state"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<select size="1" id="state" name="mailingLocation.state" path="statelist" class="input-medium" autocomplete="off">
									<option value=""><spring:message code="label.assister.select"/></option>
									<c:forEach var="state" items="${statelist}">
										<option id="${state.code}" <c:if test="${state.code == assister.mailingLocation.state}"> selected="selected"</c:if>  value="<c:out value="${state.code}" />">
											<c:out value="${state.name}" />
										</option>
									</c:forEach>
								</select>
								<input type="hidden" id="state_hidden" name="state_hidden" value="${assister.mailingLocation.state}">
								<div id="state_error"></div>
							</div>	
						</div>
						
						<div class="control-group">
							<label for="zip_mailing" class="control-label"><spring:message code="label.assister.zipCode"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<input type="text" placeholder=""  name="mailingLocation.zip" id="zip" value="${assister.mailingLocation.zip}" class="input-mini zipCode" maxlength="5" autocomplete="off"/>
								<input type="hidden" id="zip_hidden" name="zip_hidden" value="${assister.mailingLocation.zip}">	
								<div id="zip_error"></div>
							</div>
						</div>
							
					
						<div class="header">
							<h4 class="pull-left"><spring:message code="label.assister.profileInformation"/></h4>
						</div>
						<div class="control-group">
							<label for="spoken" class="control-label"><spring:message code="label.assister.spokenLanguagesSupported"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<input id="${otherSpokenLanguage}" class="hide">
							
							<label for="${language.lookupValueLabel}" class="hide">${language.lookupValueLabel}</label>
							<div class="controls">
								<c:forEach var="language" items="${languageSpokenList}">
				                      <input type="checkbox" name="spoken" id="spoken"  value="${language.lookupValueLabel}" ${fn:contains(assisterLanguages.spokenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
				                      <c:out value="${language.lookupValueLabel}" /><br>
			                  	</c:forEach> 
			                  	
			                   	<input type="checkbox" name="otherLang" id="otherLang"  value="others"> <spring:message code="label.assister.otherLang"/>&nbsp;&nbsp; 
			                    <label for="${otherSpokenLanguage}" class="hide">${otherSpokenLanguage}</label>
		                      	<select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="otherSpokenLanguage" name="otherSpokenLanguage"  class="chosen-select" multiple style="width:350px;" tabindex="4" ></select>
								<input type="hidden" id="spokenLanguages" name="spokenLanguages" value=""/>
		                      	
		                      	<div id="spoken_error"></div> 
		                      	<div id="otherSpokenLanguage_error"></div>
		                      	<div id="otherLang_error"></div>
							</div>	
						</div>	
						
						<div class="control-group">
							<label for="languagesSupported" class="control-label"><spring:message code="label.assister.writtenLanguagesSupported"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<input class="hide" id="languagesSupported">
							
							<label for="${language.lookupValueLabel}" class="hide">${language.lookupValueLabel}</label>
							<div class="controls">
									<c:forEach var="language" items="${languageWrittenList}">
					                      <input type="checkbox" id="written" name="written" value="${language.lookupValueLabel}" ${fn:contains(assisterLanguages.writtenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
			                     		  <c:out value="${language.lookupValueLabel}" /><br>
				                  	</c:forEach> 
				                   
				                    <input type="checkbox" name="otherWrittenLang" id="otherWrittenLang"  value="others"><spring:message code="label.assister.otherLang"/>&nbsp;&nbsp; 
				                    <label for="${otherWrittenLanguage}" class="hide"></label>
			                      	<select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="otherWrittenLanguage" name="otherWrittenLanguage"  class="chosen-select" multiple style="width:350px;" tabindex="4" ></select>
									<input type="hidden" id="writtenLanguages" name="writtenLanguages" value=""/>
									
									<div id="written_error"></div> 
									<div id="otherWrittenLanguage_error"></div>	
									<div id="otherWrittenLang_error"></div>
							</div>	
						</div>	
		              
						<div class="control-group">
							<label for="education" class="control-label"><spring:message code="label.assister.education"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<select size="1" id="education" name="education" path="educationlist" class="input-large">
									<option value=""><spring:message code="label.assister.select"/></option>
									<c:forEach var="educationVar" items="${educationlist}">
										<option id="${educationVar}" <c:if test="${educationVar == assister.education}"> selected="selected"</c:if>  value="<c:out value="${educationVar}" />">
											<c:out value="${educationVar}" />
										</option>
									</c:forEach>
								</select>
								<input type="hidden" id="education_hidden" name="education_hidden" value="${assister.education}">
								<div id="education_error"></div>
							</div>	
						</div>	
						
						<div class="control-group">
								<label class="control-label" for="fileInput"><spring:message code="label.assister.uploadPhoto"/></label>
								<div class="controls paddingT5">
									<input type="file" class="input-file" id="fileInput" name="fileInput">&nbsp;
									<input type="hidden" id="fileInput_Size" name="fileInput_Size" value="1">
									<div>
										<spring:message code="label.uploadDocumentCaption"/>
									</div>
					                <div id="fileInput_error"></div>
								</div>
						</div>
						 <input type="hidden" name="languagesSpokenCheck" id="languagesSpokenCheck" value="0" />
					     <input type="hidden" name="languagesWrittenCheck" id="languagesWrittenCheck" value="0" />
						<input type="hidden" name="mailingLocation.lat" id="lat" value="${assister.mailingLocation.lat != null ? assister.mailingLocation.lat : 0.0}" />
						<input type="hidden" name="mailingLocation.lon" id="lon" value="${assister.mailingLocation.lon != null ? assister.mailingLocation.lon : 0.0}" />
						<input type="hidden" name="mailingLocation.rdi" id="rdi" value="${assister.mailingLocation.rdi != null ? assister.mailingLocation.rdi : ''}" />
						<input type="hidden" name="mailingLocation.county" id="county" value="${assister.mailingLocation.county}" />	
						<div class="form-actions">
							<input type="button" name="SaveAssister" id="SaveAssister" onClick="javascript:validateForm();" value="<spring:message code="label.assister.SaveAssister"/>" class="btn btn-primary" />							
						</div>
				</form>
						</div>
					</div>
				</div>
<c:url value="/assister/checkEmail" var="theUrltocheckEmail">  
	 
</c:url>
<script type="text/javascript">	
function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
        };
	}
}	
$(document).ready(function() {
	var isCertified ='${isCertified}';
	if(isCertified=="true") {
		$("#certification").show();
	}  else {	
		$("#certification").hide();
	}
});

$(function() {
	$("#SaveAssister").click(function(){
		ie8Trim();
		var spokenVal ="";
		var writtenVal="";
		$.each($("input[name='spoken']:checked"), function() {
			var spokenValcurrent = $(this).attr('value');
			if(spokenVal == ""){ 
				spokenVal=spokenValcurrent;
			} else {
				spokenVal=spokenVal+","+spokenValcurrent;
			}
		});
		$.each($("input[name='written']:checked"), function() {
			var  writtenValcurrent = $(this).attr('value');
			if(writtenVal == ""){ 
				writtenVal=writtenValcurrent;
			} else {
				writtenVal=writtenVal+","+writtenValcurrent;
			}
		});
		otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
		if(otherSpokenLanguage != "" &&  otherSpokenLanguage !=null){ 
			if(spokenVal != ""){ 
				spokenVal = spokenVal+","+otherSpokenLanguage;
			}
			else{ 
				spokenVal =otherSpokenLanguage;
			}
		}
		
		otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
		if(otherWrittenLanguage != "" &&  otherWrittenLanguage !=null){ 
			if(writtenVal != ""){ 
				writtenVal = writtenVal+","+otherWrittenLanguage;
			}
			else{ 
				writtenVal =otherWrittenLanguage;
			}
		}
		
		 $("#spokenLanguages").val(spokenVal);
		 $("#writtenLanguages").val(writtenVal);
	});
});

var theUrltocheckEmail = '<c:out value="${theUrltocheckEmail}"/>';
function validateForm() {
	if($("#frmAssister").validate().form() )
		checkExistingEmail();
	else return false;
}

function isInvalidCSRFToken(xhr) {
    var rv = false;
    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
    	alert($('Session is invalid').text());
           rv = true;
    }
    return rv;
}

function checkExistingEmail() {
	var validateUrl = theUrltocheckEmail;
	$("#SaveAssister").attr("disabled", true);
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			newEmail : $("#emailAddress").val(),
			oldEmail : '${assister.emailAddress}',
			csrftoken : $('#tokid').val() ,
		},
		success : function(response) {
			if(isInvalidCSRFToken(response)){
				$("#SaveAssister").attr("disabled", false);
				return; 
			}                                  
                
			
			if (response) {
				error = "<label class='error' generated='true'><span> <em class='excl'>!</em><spring:message code='label.validateEmailCheckID' javaScriptEscape='true'/></span></label>";
				$('#emailAddress_error').html(error);
				$("#SaveAssister").attr("disabled", false);
				return false;
			} else {
				$("#frmAssister").submit();
			}
		},

	}); 
}

jQuery.validator.addMethod("OtherSpokenLanguageCheck", function(value, element, param) {
	ie8Trim();
	otherSpokenLanguage = $("#otherSpokenLanguage").val().trim(); 
	if(otherSpokenLanguage != ""){ 
		var languages='${languageNames}';
		languages=languages.replace("[","");
		languages=languages.replace("]","");
		languages=languages.replace(" ","");
		var languagesArray=languages.split(',');
		
		var found = false;
		for (i = 0; i < languagesArray.length && !found; i++) {
		  if (languagesArray[i].trim() === otherSpokenLanguage) {
			  found = true;
		  }
		}
		if(found){
			return false;
		}
		else
			{
				return true;
			}
	}
	else
	{
		if($("#otherLang").attr("checked")=='checked' && otherSpokenLanguage =="")
		{					
				return false;
		}
		else
			{
				return true;
			}
	}	
});
jQuery.validator.addMethod("OtherWrittenLanguageCheck", function(value, element, param) {
	ie8Trim();
	otherWrittenLanguage = $("#otherWrittenLanguage").val().trim(); 
	if(otherWrittenLanguage != ""){ 
		var languages='${languageWrittenNames}';
		languages=languages.replace("[","");
		languages=languages.replace("]","");
		languages=languages.replace(" ","");
		var languagesArray=languages.split(',');
		var found = false;
		for (i = 0; i < languagesArray.length && !found; i++) {
		languagesTocompare=languagesArray[i].trim();
		 if (languagesTocompare.toLowerCase().match(otherWrittenLanguage.toLowerCase())) {	
			  found = true;
		  }
		}  
		if(found){
			return false;
		}
		else
			{
				return true;
			}
	
	}
	else
	{	
		if($("#otherWrittenLang").attr("checked")=='checked' && otherWrittenLanguage =="")
		{
			
			return false;
		}
		else
			{
				return true;
			}
	}
});

jQuery.validator.addMethod("SpokenLanguageCheck", function(value, element, param) {
	ie8Trim();
	var fields = $("input[name='spoken']").serializeArray(); 
	if (fields.length == 0) 
    { 
        otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
    	if(otherSpokenLanguage == null || otherSpokenLanguage == ""){
    		return false;
    	}
    } 
	return true;
});

jQuery.validator.addMethod("otherSpokenLanguageCheckboxCheck", function(value, element, param) {
	ie8Trim();
	
	otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
	if (otherSpokenLanguage != null ) 
    { 
	    if((otherSpokenLanguage != null || otherSpokenLanguage != "") && !($("#otherLang").attr("checked")=='checked' )){
	    		return false;
	    	}
    } 
	return true;
});

jQuery.validator.addMethod("otherSpokenLanguageSelectCheck", function(value, element, param) {
	ie8Trim();
	
	temp = $("#otherLang").attr("checked")=='checked';
	if (temp != false ) 
    { 
	    if((otherSpokenLanguage == null || otherSpokenLanguage == "") && ($("#otherLang").attr("checked")=='checked' )){
	    		return false;
	    	}
    } 
	return true;
});
jQuery.validator.addMethod("WrittenLanguageCheck", function(value, element, param) {
	ie8Trim();
	var fields = $("input[name='written']").serializeArray(); 
    if (fields.length == 0) 
    { 
    
       otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
       if(otherWrittenLanguage  == null || otherWrittenLanguage == ""){ 
    		return false;
    	}
    } 
	return true;
});
	

jQuery.validator.addMethod("otherWrittenLanguageCheckboxCheck", function(value, element, param) {
	ie8Trim();
	otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
	if (otherWrittenLanguage != null) 
    { 
    	if((otherWrittenLanguage != null || otherWrittenLanguage != "") && !($("#otherWrittenLang").attr("checked")=='checked' )){
    			return false;
    	}
    } 
	return true;
});

jQuery.validator.addMethod("otherWrittenLanguageSelectCheck", function(value, element, param) {
	ie8Trim();
	
	temp = $("#otherWrittenLang").attr("checked")=='checked';
	if (temp != false ) 
    { 
	    if((otherWrittenLanguage == null || otherWrittenLanguage == "") && ($("#otherWrittenLang").attr("checked")=='checked' )){
	    		return false;
	    	}
    } 
	return true;
});

jQuery.validator.addMethod("PhotoUploadCheck", function(value, element, param) {
	ie8Trim();
      var file = $('input[type="file"]').val();
      var exts = ['jpg','jpeg','gif','png','bmp'];
      if ( file ) {
        var get_ext = file.split('.');
        get_ext = get_ext.reverse();

        if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
          return true;
        } else {
          return false;
        }
     }
     return true;
});


var validator = $("#frmAssister").validate({ 
	onkeyup: false,
	onclick: false,
	rules : {
		firstName : {required : true},
		lastName : {required : true},
		businessLegalName : {required : true, alphaNumeric :false},
		
		emailAddress : { required : true,email: true},
		
		primaryPhone3 : {primaryphonecheck : true},
	
		secondaryPhone3 : {secondaryphonecheck : true},
		communicationPreference : { required : true},
		"mailingLocation.address1" : { required: true},
		"mailingLocation.city" : { required: true},
		"mailingLocation.state" : { required: true},
		"mailingLocation.zip" : {required: true, MailingZipCodecheck: true, digits: true},
		primarySite : {required: true},
		certificationNumber : { CertificationNoCheck: true},
		spoken : {SpokenLanguageCheck: true},
		written : {WrittenLanguageCheck: true},
		otherSpokenLanguage : {OtherSpokenLanguageCheck: true, languagesSpokenCheck: true},
		otherWrittenLanguage : {OtherWrittenLanguageCheck: true, languagesWrittenCheck : true },
		fileInput : {PhotoUploadCheck: true, sizeCheck: true},
	 	education : { required: true},
	 	primaryPhone1 :{numberStartsWithZeroCheck : true},
	 	secondaryPhone1 :{numberStartsWithZeroCheck : false},
	 	otherLang : {otherSpokenLanguageCheckboxCheck : true, otherSpokenLanguageSelectCheck : true},
	 	otherWrittenLang : {otherWrittenLanguageCheckboxCheck : true, otherWrittenLanguageSelectCheck : true}
	},
	messages : {
		firstName: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidateFirstName' javaScriptEscape='true'/></span>"},
         lastName: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidateLastName' javaScriptEscape='true'/></span>"},
         businessLegalName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateBusinessLegalName' javaScriptEscape='true'/></span>",
                      alphaNumeric:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterAlphaNumericName' javaScriptEscape='true'/></span>"},
		emailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.entityValidatePleaseEnterValidEmail' javaScriptEscape='true'/></span>",
	    	email : "<span> <em class='excl'>!</em><spring:message code='label.entityValidatePleaseEnterValidEmail' javaScriptEscape='true'/></span>"},
	    	primaryPhone1 : {numberStartsWithZeroCheck :  "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>" },
	    	primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},
	     	secondaryPhone3: { secondaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNo' javaScriptEscape='true'/></span>"},
	    
			 communicationPreference: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCommunicationPreference' javaScriptEscape='true'/></span>"},
		"mailingLocation.address1" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterMailingAddress' javaScriptEscape='true'/></span>"},
		"mailingLocation.city" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterCity' javaScriptEscape='true'/></span>"},
		"mailingLocation.state" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseSelectState' javaScriptEscape='true'/></span>"},
		"mailingLocation.zip" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterZipCode' javaScriptEscape='true'/></span>",
		MailingZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidZipCode' javaScriptEscape='true'/></span>"},
		primarySite : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryAssisterSite' javaScriptEscape='true'/></span>"},
		certificationNumber : {CertificationNoCheck : "<span> <em class='excl'>!</em><spring:message code='label.errorEnterDigitNo' javaScriptEscape='true'/></span>"},
 		spoken : {SpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSpokenLanguage' javaScriptEscape='true'/></span>"},
 		written : {WrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateWrittenLanguage' javaScriptEscape='true'/></span>"},
 		otherSpokenLanguage : {OtherSpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherSpokenLanguage' javaScriptEscape='true'/></span>",
 			 languagesSpokenCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterLanguageSpokenFromDropDown' javaScriptEscape='true'/></span>"},
 		otherWrittenLanguage : {OtherWrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherWrittenLanguage' javaScriptEscape='true'/></span>",
 			languagesWrittenCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterLanguageWrittenFromDropDown' javaScriptEscape='true'/></span>"},
 		education : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterEducation' javaScriptEscape='true'/></span>"},
 		fileInput : {PhotoUploadCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePhoto' javaScriptEscape='true'/></span>",
 					sizeCheck : "<span> <em class='excl'>!</em><spring:message  code='label.brkvalidatePleaseSelectFileWithSizeLessThan5MB' javaScriptEscape='true'/></span>"},
 		otherLang : {otherSpokenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckbox'/></span>",
			otherSpokenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforother'/></span>"},
		otherWrittenLang : {otherWrittenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckboxforwritten'/></span>",
			otherWrittenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforotherwritten'/></span>"}
	}
	,
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

jQuery.validator.addMethod("alphaNumeric", function(value, element, param) {
	ie8Trim();
	 return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
	 
});

jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {
	ie8Trim();
	primphone1 = $("#primaryPhone1").val().trim(); 
	primphone2 = $("#primaryPhone2").val().trim(); 
	primphone3 = $("#primaryPhone3").val().trim(); 
 
	if( (primphone1 == "" || primphone2 == "" || primphone3 == "")  || (isNaN(primphone1)) || (primphone1.length < 3 ) || (isNaN(primphone2)) || (primphone2.length < 3 ) || (isNaN(primphone3)) || (primphone3.length < 4 )  )
	{
		return false;
	}
	else
	{
		$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
		return true;
	}	
} );


jQuery.validator.addMethod("secondaryphonecheck", function(value, element, param) {
	ie8Trim();
	secondaryphone1 = $("#secondaryPhone1").val().trim(); 
	secondaryphone2 = $("#secondaryPhone2").val().trim(); 
	secondaryphone3 = $("#secondaryPhone3").val().trim(); 

	if(secondaryphone1 == "" && secondaryphone2 == "" && secondaryphone3 == ""){
		return true;
	} else if(isNaN(secondaryphone1) || secondaryphone1.length < 3  || isNaN(secondaryphone2) || secondaryphone2.length < 3  || isNaN(secondaryphone3) || secondaryphone3.length < 4){
		return false;
	} else {
		$("#secondaryPhoneNumber").val(secondaryphone1 + secondaryphone2 + secondaryphone3);
		return true;
	}
} );


// function shiftbox(element,nextelement){
// 	ie8Trim();
// 	maxlength = parseInt(element.getAttribute('maxlength'));
// 	if(element.value.length == maxlength){
// 		nextelement = document.getElementById(nextelement);
// 		nextelement.focus();
// 	}
//}

jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#zip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000") )){ 
		return false; 
	}
	return true;
});

jQuery.validator.addMethod("CertificationNoCheck", function(value, element, param) {
	ie8Trim();
	var isChecked = jQuery("input[name=isAssisterCertified]:checked").val();
	
	if(isChecked == "Yes" ){
		certificationNo = $("#certificationNumber").val().trim();
		if((certificationNo == "")  || (isNaN(certificationNo)) || (value.length < 10 ) || (value == "0000000000"))
		{ 
			return false; 
		}
		return true;
	}
});

jQuery.validator.addMethod("numberStartsWithZeroCheck", function(value, element, param) {
	ie8Trim();
	if((value.length == 0)) {
		return true;
	}

	var firstChar = value.charAt(0);
	if(firstChar == 0) {
			return false;
	} else{
	    return true;
	}
	
});

$('#address1').focusin(function() {
	
			if(($('#address2').val())==="Address Line 2"){
				$('#address2').val('');
			}
		
		
	
});

function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}

 $(document).ready(function() {
	 $("#locationAndHours").removeClass("link");
		
   	     
    $( ".otherLanguages").autocomplete({
        source: function (request, response) {
            $.getJSON("${pageContext. request. contextPath}/getOtherLanaguageList", {
                term: extractLast(request.term)
            }, response);
        },
        search: function () {
            // custom minLength
            var term = extractLast(this.value);
            if (term.length < 1) {
                return false;
            }
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
 });
 
 $('#otherWrittenLanguage').focusout(function() {
     county=$("#otherWrittenLanguage").val().trim();
     var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
         $('#otherWrittenLanguage').val(index);
	 }
	 else{
		  }
     
  });

 $('#otherSpokenLanguage').focusout(function() {
     county=$("#otherSpokenLanguage").val().trim();
     var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
         $('#otherSpokenLanguage').val(index);
	 }
	 else{
		  }
  });
 
 $('#otherWrittenLanguage').focusout(function() {
     county=$("#otherWrittenLanguage").val().trim();
     var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
         $('#otherWrittenLanguage').val(index);
	 }
	 else{
		  }
     
  });

 $('#otherSpokenLanguage').focusout(function() {
     county=$("#otherSpokenLanguage").val().trim();
     var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
         $('#otherSpokenLanguage').val(index);
	 }
	 else{
		  }
  });
 
 
 jQuery.validator.addMethod("languagesSpokenCheck", function(value, element, param) {
		if(value == '') {
			return true;
		}
		checkLanguageSpokenForSite();
		return $("#languagesSpokenCheck").val() == 1 ? false : true;
		});
		
	function checkLanguageSpokenForSite(){
		
		$.get('registration/checkLanguagesSpokenForAsister',
		{otherSpokenLanguage: $("#otherSpokenLanguage").val()},
		            function(response){
		                    if(response == true){
		                            $("#languagesSpokenCheck").val(0);
									
		                    }else{
		                            $("#languagesSpokenCheck").val(1);
									
		                    }
		            }
		        );
		}
	
	jQuery.validator.addMethod("languagesWrittenCheck", function(value, element, param) {
		if(value == '') {
			return true;
		}
		checkLanguageWrittenForSite();
		return $("#languagesWrittenCheck").val() == 1 ? false : true;
		});
		
	function checkLanguageWrittenForSite(){
		
		$.get('registration/checkLanguagesWrittenForAssister',
		{otherWrittenLanguage: $("#otherWrittenLanguage").val()},
		            function(response){
		                    if(response == true){
		                            $("#languagesWrittenCheck").val(0);
									
		                    }else{
		                            $("#languagesWrittenCheck").val(1);
									
		                    }
		            }
		        );
		}
	
	$(function(){
		 $('#fileInput').change(function(){
				var rv = -1; // Return value assumes failure.
				 if (navigator.appName == 'Microsoft Internet Explorer')
				 {
				    var ua = navigator.userAgent;
				    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
				    if (re.exec(ua) != null)
				       rv = parseFloat( RegExp.$1 );
				 }
				 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
				 var f=this.files[0];
				if((f.size > 5242880)||(f.fileSize > 5242880)){
					$("#fileInput_Size").val(0);
				}
				else{
					$("#fileInput_Size").val(1);	
				} 
				}
				else{
					$("#fileInput_Size").val(1);	
				} 

			});
		});

	jQuery.validator.addMethod("sizeCheck", function(value, element, param) { 
		  ie8Trim();
		  if($("#fileInput_Size").val()==1){
			  return true;
		  }
		  else{
			  document.getElementById('fileInput').value=null;
			  $("#fileInput_Size").val(1);
			  return false;
		  }
	});
	
</script>			
<script type="text/javascript">
 //load the jquery chosen plugin 
	function loadLanguages() {
		$("#otherSpokenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherSpokenLanguage}';
		for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkLanguages(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	     
	     $('#otherSpokenLanguage').trigger("liszt:updated");
	   }
	function checkLanguages(county){
	     var counties='${otherSpokenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguages();
	});
	

</script>
<script type="text/javascript">
 //load the jquery chosen plugin 
	function loadLanguagesForWritten() {
		$("#otherWrittenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherWrittenLanguage}';
	    for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkLanguage(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	    $('#otherWrittenLanguage').trigger("liszt:updated");
	   
	   }
	function checkLanguage(county){
	     var counties='${otherWrittenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguagesForWritten();
	});
</script>