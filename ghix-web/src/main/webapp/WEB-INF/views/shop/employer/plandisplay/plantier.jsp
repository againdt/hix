<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld"
  prefix="util"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/shopstyles.css"/>" />

<style>
.scroll-left-arrow-wrapper,.scroll-right-arrow-wrapper {
  width: 25px;
  background: #f3f3f3;
  padding: 70px 4px;
  cursor: pointer;
}

.scroll-left-arrow-wrapper {
  float: left;
  margin-left: -11px;
  border-right: solid 1px #e2e2e2;
}

.scroll-right-arrow-wrapper {
  float: left;
  margin-right: -11px;
  border-left: solid 1px #e2e2e2;
}

.view-plans {
  width: 93.5%;
  margin: 0 32px;
}

.plans-top-wrapper {
  position: fixed;
  z-index: 2;
  text-align: center;
}

.plans-section-content .plan {
  float: left;
  width: 212px; /*22.64%;*/
  position: relative;
}

.plans-container {
	border: 0px;
}

#planSelect div.plan:nth-child(4n) {
  border-right: 0px !important;
}

div.plans-top-wrapper.span6 {
  width: 48.317948718%;
}

.scroll-left-arrow-wrapper {
  margin-left: 0px;
}

.plans-wrap {
  width: 90.5%;
}

.plans-section-content .plan {
  float: left;
  width: 164px;
  position: relative;
}

.table th,.table td,.view-plans .table td {
  border: 0px;
  vertical-align: bottom;
}

.dedictible .table td {
  border-bottom: 1px solid #E2E2E2;
}

#planSummary .table td {
  text-align: center;
}

.plan .table tr td {
  text-align: center !important;
}

input[type="radio"] {
  background: #0F9;
}

.highcharts-legend,.highcharts-axi {
  display: none !important;
}

.plans-section-header {
  margin-right: -1px;
}

/* .plan { */
/*   border-right: 1px solid #E2E2E2; */
/* }  */
.plan:nth-child(5) {
  border-right: 0px;
} 
</style>
<script type="text/javascript">
function capitalizeString(val){
	val.toLowerCase();
    return val.charAt(0).toUpperCase()+val.substr(1).toLowerCase();
}
</script>


<div class="gutter10">
<div class="row-fluid">
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <div class="accordion graysidebar" id="accordion2">
        <util:nav pageName="${page_name}"></util:nav>
      </div>
      <util:selection pageName="${page_name}"></util:selection>
    </div>
    <!--sidebar-->

    <div class="span9" id="rightpanel">
      <h4 class="graydrkbg gutter10" id="skip">
       <a href="<c:url value="/shop/employer/plandisplay/contribution" />" class="btn"> <spring:message code = "label.back"/></a>
            <span class="offset1"><spring:message code = "label.selectPlanLevel"/></span> 
            <a href="#" class="btn btn-primary getSelectedTier pull-right"> <spring:message  code="label.next"/> </a>
            </h4>
      <!--titlebar-->
      
      <div class="gutter10">
      	<p><spring:message code = "label.planSelectionInfo"/> </p>
        <!-- <p>All the plans offered by this Exchange meet the State's basic requirements and fall into
        one of four levels shown below. You'll select a level and then your employees will be able to select any plan in that level</p>-->
        <div class="plans-container">
          <div class="plans-section  row-fluid">
            <div class="plans-section-header"></div>
            <div class="plans-section-content row-fluid in collapse" id="row1">
              <input type="hidden" name="tierType" id="tierType" value="${tierType}" />
              <input type="hidden" name="maxEmployerCost" id="maxEmployerCost" value="" />
              <input type="hidden" name="maxEmployeeCost" id="maxEmployeeCost" value="" />
              
              <div id="summary"></div>
            </div>
            <!--plans-section-content-->
          </div>
          <!--plans-section row-fluid-->
        <!--ACTURIAL SECTION  -->
        <div class="plans-section  row-fluid"> 
        <!--plans-section plans-header-->
              <div class="plans-section-header">
              <h4 data-toggle="collapse" data-target="#row2" class="collapsetoggle">
                <i class="icon icon-chevron-right"></i> <spring:message code = "label.actuarialValue"/> 
                <a tabindex="-1" href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message code = "label.actuarialvalueDesc"/>" class="info"><i class="icon icon-question-sign"></i></a>
              </h4>
            </div>
            
              <div class="plans-section-content row-fluid in collapse" id="row2">
                <div id="acturialValue">
                  <div class="">
                 </div>
               </div>
              <!--plans-section-content--> 
            </div>
            <!--plans-section row-fluid--> 
            </div>
          <!--MONTHLY EMPLOYER COST SECTION  -->
          <div class="plans-section  row-fluid">
            <!--plans-section plans-header-->
            <div class="plans-section-header">
              <h4 data-toggle="collapse" data-target="#row3"
                class="collapsetoggle">
                <i class="icon icon-chevron-right"></i> <spring:message code = "label.monthlyCostToYou"/> <small><spring:message code = "label.basedOnContriProposed"/> ${emplrContributionEmp}% <spring:message code = "label.and"/> ${emplrContributionDep}%)</small> <a href="#" rel="tooltip" data-placement="top"
                  data-original-title="<spring:message code = "label.costCaldesc1"/>" class="info">
                  <i class="icon icon-question-sign"></i></a>
              </h4>
            </div>
            <div class="plans-section-content row-fluid in collapse" id="row3">
              <div id="employerCost">
              </div>
            </div>
            <!--plans-section-content-->
          </div>
          <!--plans-section row-fluid-->
          <!--MONTHLY EMPLOYEE COST SECTION  -->
          <div class="plans-section  row-fluid">
            <!--plans-section plans-header-->
            <div class="plans-section-header">
              <h4 data-toggle="collapse" data-target="#row4"
                class="collapsetoggle">
                <i class="icon icon-chevron-right"></i> <spring:message code = "label.costToEmp"/> <small><spring:message code = "label.basedOnProposed"/> ${emplrContributionEmp}% <spring:message code = "label.contriEmpPart"/>) </small>
                  <a href="#" rel="tooltip" data-placement="top"
                  data-original-title="<spring:message code = "label.costCaldesc2"/>" 
                  class="info"><i class="icon icon-question-sign"></i></a>
              </h4>
            </div>
            <div class="plans-section-content row-fluid in collapse" id="row4">
              <div id="employeeCost"></div>
            </div>
            <!--plans-section-content-->
          </div>
          <!--plans-section row-fluid-->
      <!--INSURANCE COMPANIES SECTION  -->
      <!-- <div class="plans-section  row-fluid"> plans-section plans-header
              <div class="plans-section-header">
              <h3 data-toggle="collapse" data-target="#row5" class="collapsetoggle"> <i class="icon icon-chevron-right"></i> Insurance Companies </h3>
              </div>
              <div class="plans-section-content row-fluid collapse" id="row5">
                <div id="drugs">
                  <div class="plan">
                    <table class="table ">
                      <tbody>
                        <tr>
                          <td>Insurance Companies</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="plan">
                    <table class="table ">
                      <tbody>
                        <tr>
                          <td>Insurance Companies</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="plan">
                    <table class="table ">
                      <tbody>
                        <tr>
                          <td>Insurance Companies</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="plan">
                    <table class="table ">
                      <tbody>
                        <tr>
                          <td>Insurance Companies</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              plans-section-content 
            </div> -->
          <!--plans-section row-fluid-->
        <!-- </div> -->
      </div>
      <!--gutter10-->
    </div>
    <!--rightpanel-->
  </div>
  <!--row-fluid-->
</div>
</div>
<!--main-->
</div>
<div id="missinginfo" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="childhomeaddress" aria-hidden="true">
                  <div class="modal-header"></div>
                  <div class="modal-body">
                    <h3 class="green"><spring:message code = "label.processingMsg"/></h3>
          <img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots">
                  </div>
                  <div class="modal-footer"></div>
</div>

<div id="planAvailability" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="childhomeaddress" aria-hidden="true">
                  <div class="modal-header">
                  <h3><spring:message code = "label.planAvailable"/></h3>
                  </div>
                  <div class="modal-body">
	                   <p><spring:message code = "label.planExistInfo1"/> <span id="tierName"></span> <spring:message code = "label.planExistInfo2"/><br>
	                   		
	                   		<strong><span id="notAvailableEmployees"></span></strong></p>
                  </div>
                  <div class="modal-footer">
                  	<a href='#' class="btn btn-primary btn-small" onclick="$('#planAvailability').modal('hide');" >OK</a>
                  </div>
</div>
                <!-- /modal -->
<!-- Please limit view to plans -->
<script type="text/html" id="resultItemTemplateSummary">
<div class="plan plan-<@=tierName@> ">
  <table class="table">
    <tbody>
      <tr>
        <td><h3><@=tierName @></h3> (<a href="<c:url value="/shop/employer/plandisplay/tierdetails/<@=tierName@>"/>"><@=planIds.length @> <span class="aria-hidden"><@=tierName @></span> <spring:message code = "label.plans"/> 
				<span class="aria-hidden"><spring:message code = "label.premiumRange"/> from <@=formatCurrency(minIndvPremium/empIds.length) @> to <@=formatCurrency(maxIndvPremium/empIds.length) @></span>
		  </a>)<br>
          <spring:message code = "label.premiumRange"/><br>
          <@=formatCurrency(minIndvPremium/empIds.length) @> - <@=formatCurrency(maxIndvPremium/empIds.length) @><br>
          <br>
          <a href='#' class="btn btn-primary selectTier" id="tier_<@=tierName@>" name="selectTier"><spring:message code = "label.select"/> <span class="aria-hidden"><@=tierName @> Plan</span></a>
      <a href='#' class="btn btn-primary active removeTier" style="display:none;" id="removetier_<@=tierName@>"><spring:message code = "label.selected"/> <span class="aria-hidden"><@=tierName @> Plan</span></a></td>
      </tr>
    </tbody>
  </table>
</div>
</script>
<script type="text/html" id="acturialSummary">
<div class="plan">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>
								<div id="pie_<@=tierName@>" style="min-width: 140px; height: 170px; margin: 0 auto"></div>
								<div class="tierName" aria-hidden="true">
									<@ if(tierName == 'BRONZE')	{ @>
											<spring:message code = "label.leastComprehenplans"/>
									<@ } else if (tierName == 'PLATINUM') { @>
											<spring:message code = "label.mostComprehenPlans"/>
									<@ } else { @>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<@ } @>
								</div>
						  </td>

                        </tr>
                      </tbody>
                    </table>
                  </div>
</script>
<script type="text/html" id="employerCostSummary">
<div class="plan">
  <table class="table">
    <tbody>
      <tr>
        <td>
          <div id="Employer_<@=tierName@>" style="min-width: 140px; height: 170px; margin: 0 auto"></div>
    </td>
      </tr>
    </tbody>
  </table>
</div>
          
</script>
<script type="text/html" id="employeeCostSummary">
<div class="plan">
  <table class="table">
    <tbody>
      <tr>
        <td>
      <div id="Employee_<@=tierName@>" style="min-width: 140px; height: 170px; margin: 0 auto"></div>
      </tr>
    </tbody>
  </table>
</div>
</script>
<script>
  $(document).ready(function() {
	  $('.info').tooltip();
      $('.plans-section-header').on('click show hide',function(e) {
          $(e.target).find('i').toggleClass('icon-chevron-right icon-chevron-down',200);
        });
});
</script>

<script src="<c:url value="/resources/js/underscore-min.js" />">  </script>
<script src="<c:url value="/resources/js/backbone-min.js" />">  </script>
<script src="<c:url value="/resources/js/json2.js" />"> </script>
<script src="<c:url value="/resources/js/plandisplay/showtiers.js" />"> </script>
<script src="<c:url value="/resources/js/plandisplay/backbone-tier.js" />">  </script>

<!--Models/Collections-->
<script src="<c:url value="/resources/js/plandisplay/models/tier.js" />">  </script>
<script src="<c:url value="/resources/js/plandisplay/collections/TierCollection.js" />"> </script>
<!--Views-->
<script src="<c:url value="/resources/js/plandisplay/views/TierView.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/exporting.js" />"></script> 

<script type="text/javascript">

function drawpie(pieId,tierName,valx,valy){
       chart = new Highcharts.Chart({
                chart: {
                    renderTo: pieId,
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
		            marginTop: 0,
		            marginLeft: 0,
		            marginRight: 0,
		            marginBottom: 0           
                },
                exporting:{ 
                	buttons: {
	            		printButton:{ enabled:false },
	             		exportButton: { enabled:false }
            		}
            	},
                title: {
                	useHTML: true,
                    text: "<span class='aria-hidden'>Actuarial Value for " + tierName + " is " + valy + "%</span>" 
                },
                tooltip: {
                    formatter: function() {
                        return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(1)+' %';
                    }
                },

			      colors: [
					  '#ffffff', 
					  '#aecb4a' 
				  ],
  				credits: {
		            enabled: false
		        },
                plotOptions: {
            pointWidth: 100,
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                
                            connectorColor: '#000000',
                            formatter: function() {
                              // return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(1)+' %';
                            }
                        }
                    }
                },
                 series: [{
                type: 'pie',
         shadow: false,
         borderWidth: 0,
                data: [
                    ['.',   valx],
                    {
                        name: tierName,
                        y: valy,
                        sliced: true,
                        selected: true
                    }
                    
                ]
                }]
            });
       
       //for accessibility ie10
       if(tierName == "PLATINUM"){
    	   $("#pie_PLATINUM").append("<span class='offScreen' tabindex='0'>Actuarial Value for " + tierName + " is " + valy + "%</span>")
       }
  
}
function drawbar(barId, tierName, valArray, color1, color2,maxVal){
	//for accessibility
	if(barId.search("Employee")){
		var costType="Monthly cost to you";
	}else if(barId.search("Employer")){
		var costType="Monthly cost to each employee"; 
	}
	
  chart = new Highcharts.Chart({
    chart:   { renderTo: barId, type: 'column', spacingBottom: 0, marginBottom: 20, marginTop: 10 },
    credits: { enabled: false },
    legend:  { enabled: false },
    exporting:{ buttons: {
                 printButton:{ enabled:false },
                 exportButton: { enabled:false }
               }
          },
    title: { text: '' },
    xAxis: { categories: [ 'Min','Max' ], title: {enabled: true, text: tierName + ' plan ' + costType +' Minimun is ' + formatCurrency(valArray[0]) + ' Maximun is ' + formatCurrency(valArray[1])} },
    colors: [color1, color2 ],
    yAxis: { min: 0, max: maxVal , title: { text: '' }, gridLineWidth: 0, gridLineColor: '#ffffff', labels: { enabled: false } },
    plotOptions: { 
      column: { pointPadding: 0.5, borderWidth: 0, pointWidth: 56, colorByPoint: true, color: '#009999' }
    },
    tooltip: {
        formatter: function() {
            return this.x  + ' <b>'+ tierName + ":</b>" + formatCurrency(this.y);
        },
	    positioner: function () {
	    	return { x: 5, y: 0 };
	    }
    },
    series: [{name: tierName, shadow: false, color: '#009999', data: valArray, dataLabels: {
    enabled: true,
    formatter: function() {
       return formatCurrency(this.y);
    }
 }    }]
  });
  
  $("g[zIndex='6'],g[zIndex='7']").attr("aria-hidden","true");
}

function formatCurrency(num) {
	num = parseFloat(num).toFixed(0);
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num);
}
</script> 