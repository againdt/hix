package com.getinsured.hix.admin.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;



/**
 * This class creates the plan management report for Data-wise statistics
 * HIX-26726
 * 
 * @author Nikhil Talreja
 * @since 08 January, 2013
 *
 */
@Component
public class PlanManagementRepots {
	
	private static final Logger LOGGER = Logger.getLogger(PlanManagementRepots.class);
	
	private Map<String,HashMap<String,BigDecimal>> values = new HashMap<String, HashMap<String,BigDecimal>>();
	
	/**
	 * This method generates reports for a particular date filter
	 * 
	 * @author Nikhil Talreja
	 * @since 08 January, 2013
	 *
	 */
//	@SuppressWarnings("unchecked")
//	public List<PlanManagmentReportsRecord> getDatewiseStats(String dateFilter){
//		
//		//Queries
//		String createdQuery = "select trunc(creation_timestamp), count(*) as created_total from plan where creation_timestamp > systimestamp -"+dateFilter+" group by trunc(creation_timestamp) order by trunc(creation_timestamp)";	
//		String startDateQuery = "select trunc(start_Date), count(*) as start_date_total from plan where start_Date > sysdate -"+dateFilter+" group by trunc(start_date) order by trunc(start_date)";
//		String endDateQuery = "select trunc(end_date), count(*) as end_date_total from plan where end_date > sysdate -"+dateFilter+" group by trunc(end_date) order by trunc(end_date)";
//		
//		String updatedQuery = "select unique date1, count(*) as total from(select trunc(last_update_timestamp) date1 FROM PLAN where last_update_timestamp > systimestamp - "+dateFilter+" ) GROUP BY DATE1 order by date1";
//		String certifiedQuery = "select unique date1, count(*) as total from(select trunc(certified_on) date1 from plan where certified_on > sysdate -"+dateFilter+") group by date1 order by date1";
//		String enrollmentQuery = "select unique date1, count(*) as total from(select trunc(enrollment_avail_effdate) date1 from plan where enrollment_avail_effdate > sysdate - "+dateFilter+") group by date1 order by date1";
//		
//		EntityManager entityManager = entityManagerFactory.createEntityManager();
//		
//		values.clear();
//		
//		//Creation TimeStamp
//		Query query = entityManager.createNativeQuery(createdQuery);
//		List<Object[]> results =  query.getResultList();
//		addValuesToMap(results, "creationCount");
//		
//		//Start Date
//		query = entityManager.createNativeQuery(startDateQuery);
//		results =  query.getResultList();
//		addValuesToMap(results, "startDateCount");
//		
//		//End Date
//		query = entityManager.createNativeQuery(endDateQuery);
//		results =  query.getResultList();
//		addValuesToMap(results, "endDateCount");
//		
//		//Updated Date
//		query = entityManager.createNativeQuery(updatedQuery);
//		results =  query.getResultList();
//		addValuesToMap(results, "updationCount");
//		
//		//Certified Date
//		query = entityManager.createNativeQuery(certifiedQuery);
//		results =  query.getResultList();
//		addValuesToMap(results, "certifiedDateCount");
//		
//		//Enrollment effective Date
//		query = entityManager.createNativeQuery(enrollmentQuery);
//		results =  query.getResultList();
//		addValuesToMap(results, "enrollmentEffectiveDateCount");
//		
//		LOGGER.debug(values.toString());
//		
//		return convertMapToList(values);
//	}
	
	
	/**
	 * Adds the values to the map for a particular date type
	 * 
	 * @author Nikhil Talreja
	 * @since 08 January, 2013
	 *
	 */
	private void addValuesToMap(List<Object[]> results, String statTye){
		
		for(int i=0; i<results.size(); i++){
			Object[] record = results.get(i);
			String date = record[0].toString();
			if(values.get(date) == null){
				HashMap<String,BigDecimal> value = new HashMap<String,BigDecimal>();
				values.put(date, value);
			}
			values.get(date).put(statTye, (BigDecimal)record[1]);
		}
	}
	
	/**
	 * Converts the map of values to List of records
	 * 
	 * @author Nikhil Talreja
	 * @since 08 January, 2013
	 *
	 */
	private List<PlanManagmentReportsRecord> convertMapToList(Map<String,HashMap<String,BigDecimal>> records){
		
		List<PlanManagmentReportsRecord> reportRecords = new ArrayList<PlanManagmentReportsRecord>();
		for(Map.Entry<String, HashMap<String,BigDecimal>> entry : records.entrySet()){
			PlanManagmentReportsRecord reportRecord = new PlanManagmentReportsRecord(entry.getKey().replaceAll(" 00:00:00.0",""),
					entry.getValue().get("creationCount"),entry.getValue().get("startDateCount"),entry.getValue().get("endDateCount"),entry.getValue().get("updationCount"),
					entry.getValue().get("certifiedDateCount"),entry.getValue().get("enrollmentEffectiveDateCount"));
			reportRecords.add(reportRecord);
		}
		
		LOGGER.debug(reportRecords.toString());
		return reportRecords;
		
	}
}
