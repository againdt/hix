package com.getinsured.eligibility.indportal.enrollment.api;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.indportal.customGrouping.CustomGroupingController.EligibilityType;
import com.getinsured.eligibility.indportal.dto.HouseholdData;
import com.getinsured.eligibility.indportal.indCreation.HouseholdFactory;
import com.getinsured.eligibility.indportal.indCreation.IHousehold;
import com.getinsured.eligibility.lce.task.LifeChangeEventTask;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentRequest;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.ssap.integration.notices.QEPNonFinancialNotificationService;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentAptcUpdateDto;
import com.getinsured.hix.dto.enrollment.EnrollmentDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorMemberData;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorRequest;
import com.getinsured.hix.dto.plandisplay.APTCCalculatorResponse;
import com.getinsured.hix.dto.plandisplay.APTCPlanRequestDTO;
import com.getinsured.hix.dto.plandisplay.APTCPlanResponseDTO;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.indportal.dto.ReverseSepQepDenial;
import com.getinsured.hix.indportal.dto.dashboard.DashboardApplicantDTO;
import com.getinsured.hix.indportal.dto.dashboard.DashboardApplicantDTO.RelationEnum;
import com.getinsured.hix.indportal.dto.dashboard.SepApplcantEventDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.ObjectFactory;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.Ind57Mapping;
import com.getinsured.iex.indportal.dto.IndDisenroll;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * @author Krishna Gajulapalli, Sunil Desu
 *
 */
@Component
public class IndPortalEnrollmentUtility {
	
	@Autowired 
	private MoveEnrollmentService moveEnrollmentService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IndPortalEnrollmentUtility.class);

	//HIX-67344
	private static final String DENTAL_PLAN = "dental";
	private static final String HEALTH_PLAN = "health";
	private static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";

	@PersistenceUnit(unitName="entityManagerFactory")
	private EntityManagerFactory emf;
	//HIX-67344
		
	private	final ObjectFactory objectFactory = new ObjectFactory();
	public static List<String> CHILD_RELATIONSHIP_CODES = Arrays.asList("09", "17", "19");
	public static List<String> SPOUSE_RELATIONSHIP_CODES = Arrays.asList("01");
	public static List<String> SELF_RELATIONSHIP_CODES = Arrays.asList("18");
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
			
	/*@Autowired
	private SsapApplicationAccessor ssapApplicationAccessor;*/
	
	@Autowired
	private ResourceCreator resourceCreator;
	
	/*@Autowired
	private EligibilityService eligibilityService;*/
	
	@Autowired
	private ReferralLceNotificationService referralLceNotificationService; 
	
	@Autowired
	private LifeChangeEventTask lifeChangeEventTask;
	
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Autowired
	private QEPNonFinancialNotificationService qepNonFinancialNotificationService;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired private IEligibilityProgram eligibilityProgramRepository;
	
	@Autowired private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired 
	private Gson platformGson;
	
	@Autowired private HouseholdFactory householdFactory;
	@Autowired private SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
		
	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(
			SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	public ResourceCreator getResourceCreator() {
		return resourceCreator;
	}
	
	public String invokeIndDisenrollment(IndDisenroll indDisenroll) throws GIException{
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		EnrollmentDisEnrollmentDTO disEnrollDTO = new EnrollmentDisEnrollmentDTO();		
		disEnrollDTO.setSsapApplicationid(indDisenroll.getApplicationId());
		disEnrollDTO.setTerminationDate(indDisenroll.getTerminationDate());
		disEnrollDTO.setTerminationReasonCode(indDisenroll.getReasonCode());		
		if(indDisenroll.getReasonCode().equalsIgnoreCase("3")){
			disEnrollDTO.setDeathDate(indDisenroll.getDeathDate());
		}
		enrollmentRequest.setEnrollmentDisEnrollmentDTO(disEnrollDTO);
		LOGGER.debug("Invoking  ind disenrollement Service");
		String responseMsg=EligibilityConstants.FAILURE;
		try {
			XStream xStream = GhixUtils.getXStreamStaxObject();
			String get_resp=  (ghixRestTemplate.exchange(EnrollmentEndPoints.DISENROLL_BY_APPLICATION_ID_URL, indDisenroll.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,xStream.toXML(enrollmentRequest))).getBody();
			enrollmentResponse = (EnrollmentResponse) xStream.fromXML(get_resp);
			if(null != get_resp && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && enrollmentResponse.getErrCode()==EligibilityConstants.TWO_HUNDREAD){
				responseMsg=EligibilityConstants.SUCCESS;	
			}else{
				throw new GIRuntimeException("Unable to disenroll. Error Details: " + enrollmentResponse.getErrCode() + ":" + enrollmentResponse.getErrMsg());
			}
		}catch(Exception e) {
			LOGGER.error("Exception in invokeIndDisenrollment", e);
			throw new GIRuntimeException("Exception occurred while invoking ",e);
		}	
		return responseMsg;
	}
	
	public String invokeAdminUpdateEnrollment(Ind57Mapping ind57Mapping) throws GIException{
		String responseValue = EligibilityConstants.FAILURE;
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentResponse enrollmentResponse= null;
		LOGGER.debug("Invoking AdminUpate Enrollment");
		try {		
			// make db call using repository
			List<SsapApplication> ssapApplications = this.getSsapApplicationRepository().findByCaseNumber(ind57Mapping.getCaseNumber());
			SsapApplication ssapForInd57 = ssapApplications.get(0);
			if(ssapForInd57 != null) {
				AdminUpdateIndividualRequest adminUpdateIndRequest = objectFactory.createAdminUpdateIndividualRequest();
				enrollmentRequest.setAdminUpdateIndividualRequest(adminUpdateIndRequest);
				boolean requestFormed = populateRequest(adminUpdateIndRequest, ssapForInd57, ind57Mapping);
				if(!requestFormed)
				{
					return EligibilityConstants.FAILURE;
				}
				Boolean mailingAddressChanged = ind57Mapping.getMailingAddressChanged();
				if(null!=mailingAddressChanged && Boolean.TRUE.equals(mailingAddressChanged.booleanValue()) && null!=ind57Mapping.getUpdatedMailingAddressLocationID()){
					adminUpdateIndRequest.getHousehold().setIsMailingAddressChanged(true);
					adminUpdateIndRequest.getHousehold().setUpdatedMailingAddressLocationID(ind57Mapping.getUpdatedMailingAddressLocationID());
				}
				try {

					Gson gson = new Gson();
					String get_resp=  (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.ADMIN_UPDATE_ENROLLMENT_URL,ind57Mapping.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,gson.toJson(enrollmentRequest))).getBody();
					enrollmentResponse = gson.fromJson(get_resp, EnrollmentResponse.class);
					if(null != get_resp && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && enrollmentResponse.getErrCode()==EligibilityConstants.TWO_HUNDREAD){
						responseValue = EligibilityConstants.SUCCESS;		
					}else{
						throw new GIRuntimeException("Unable to update the admin enrollment details. Error Details: " + enrollmentResponse.getErrCode() + ":" + enrollmentResponse.getErrMsg());
					}
				}catch(Exception e) {
					LOGGER.error("Exception occurred while invoking Admin Update Enrollment"+e);
					responseValue = EligibilityConstants.FAILURE;
				}

			}else {
				LOGGER.error("No application found for case number " );
				throw new GIException("No application found for case number ");
			}
				
		 }catch(Exception e) {		
			 LOGGER.error("Exception in invokeAdminUpdateEnrollment", e);
			 throw new GIException("Exception occurred while invoing admin update enrollment ", e);
		 }  
	    return responseValue;
	 }	
	
	@SuppressWarnings("unchecked")
	private boolean populateRequest(AdminUpdateIndividualRequest adminUpdateIndividualRequest, SsapApplication ssapForInd57, Ind57Mapping ind57Mapping) throws GIException {
		LOGGER.debug("Constructing request for Ind57 invocation");
		String ssapJSON = ssapForInd57.getApplicationData();
		LOGGER.debug("json object for IND57"+ssapJSON);
		boolean requestFormed = true;
		try {
			// Household
			AdminUpdateIndividualRequest.Household houseHold = objectFactory.createAdminUpdateIndividualRequestHousehold();
			AdminUpdateIndividualRequest.Household.Members members = objectFactory.createAdminUpdateIndividualRequestHouseholdMembers();
					
			IHousehold ind57Household  = householdFactory.getObject("IND57Household");
			
			HouseholdData householdData = new HouseholdData();
			ind57Household.setHouseholdData(ssapForInd57, null, householdData);
			
			if(CollectionUtils.isEmpty(ind57Mapping.getMemberGuids())){
				AdminUpdateIndividualRequest.Household.ResponsiblePerson responsiblePerson = 
						(AdminUpdateIndividualRequest.Household.ResponsiblePerson) ind57Household.createResponsiblePerson(householdData);
				houseHold.setResponsiblePerson(responsiblePerson);
				
				AdminUpdateIndividualRequest.Household.HouseHoldContact houseHoldContact = 
						(AdminUpdateIndividualRequest.Household.HouseHoldContact) ind57Household.createHouseHoldContact(householdData);
				houseHold.setHouseHoldContact(houseHoldContact);
			}
			
			Members memberList = (Members) ind57Household.createMembers(members, ssapForInd57, null, householdData, ind57Mapping.getUserName(), ind57Mapping.getMemberGuids());
			
			houseHold.setMembers(memberList);				
			houseHold.setMaintenanceReasonCode("25");
			houseHold.setHouseholdCaseId(ssapForInd57.getCmrHouseoldId().longValue());			
			houseHold.setSsapApplicationid(ssapForInd57.getId());
			adminUpdateIndividualRequest.setHousehold(houseHold);	
			LOGGER.debug("successfully constructed request  for Ind57 admin update");
		} catch (Exception e) {
			LOGGER.error("Exception during IND57 request creation, mapping", e);
			//throw new GIException("Exception during IND57 request creation, mapping", e);
			requestFormed =false;
		
	}
	return requestFormed;
	}
	
	public String changeDateFormatString(String inputDateStr) throws GIException {
		final String OLD_FORMAT = "MMM dd, yyyy hh:ww:ss";
		final String NEW_FORMAT = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = null;
		try {
			d = sdf.parse(inputDateStr);
		} catch (Exception e) {

			LOGGER.error(" exception during parsing date ", e);
			GIException gie = new GIException(e);
			gie.setErrorMsg("exception during parsing date" + inputDateStr
					+ "expected format is MMM dd, yyyy hh:ww:ss");
			throw gie;
		}

		sdf.applyPattern(NEW_FORMAT);
		return sdf.format(d);
	}	
		
		
	@Transactional
	public String invokeIndDisenrollmentForSep(IndDisenroll indDisenroll) throws GIException{
		
		String finalStatus = EligibilityConstants.FAILURE;
		String moveEnrollmentStatus = moveEnrollmentToNewApplication(indDisenroll.getApplicationId(), indDisenroll.getCurrentEnrolledApplicationId(), indDisenroll.getUserName());
		if (!moveEnrollmentStatus.equals(GhixConstants.RESPONSE_SUCCESS)) {
			throw new GIRuntimeException("Error moving enrollment to new application");
		}
		//Closing old application from which enrollment is moved
		closeApplication(indDisenroll.getCurrentEnrolledApplicationCaseNumber());
		String disenrollStatus = disenrollCurrentSepApplication(indDisenroll);
		if(null!=disenrollStatus && EligibilityConstants.SUCCESS.equalsIgnoreCase(disenrollStatus)){ 
			//Closing application to which enrollment is moved
			closeApplication(indDisenroll.getCaseNumber());
			finalStatus = EligibilityConstants.SUCCESS;
		}
		return finalStatus;
	}
	
	
	private String moveEnrollmentToNewApplication(Long currentApplicationId, Long enrolledApplicationId, String userName) {
		LOGGER.info("Move Enrollment for Enrolled Application - " + enrolledApplicationId + " to Application id - " + currentApplicationId);
		MoveEnrollmentRequest request = new MoveEnrollmentRequest();
		request.setEnrolledSsapApplicationId(enrolledApplicationId);
		request.setNewSsapApplicationId(currentApplicationId);
		request.setUserName(userName);
		return moveEnrollmentService.process(request);
	}	
	
	private void closeApplication(String caseNumber){
			ssapApplicationRepository.updateApplicationStatus("CL", caseNumber, new Timestamp(new TSDate().getTime()));	
	}
	
	private String disenrollCurrentSepApplication(IndDisenroll indDisenroll){

		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
		EnrollmentDisEnrollmentDTO disEnrollDTO = new EnrollmentDisEnrollmentDTO();
		
		disEnrollDTO.setSsapApplicationid(indDisenroll.getApplicationId());
		disEnrollDTO.setTerminationDate(indDisenroll.getTerminationDate());
		disEnrollDTO.setTerminationReasonCode(indDisenroll.getReasonCode());		
		
		enrollmentRequest.setEnrollmentDisEnrollmentDTO(disEnrollDTO);
		
		LOGGER.debug("Invoking  ind disenrollement Service");
		
		String responseMsg = EligibilityConstants.FAILURE;
		
		try {
			XStream xStream = GhixUtils.getXStreamStaxObject();
			String get_resp=  (ghixRestTemplate.exchange(EnrollmentEndPoints.DISENROLL_BY_APPLICATION_ID_URL, indDisenroll.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,xStream.toXML(enrollmentRequest))).getBody();
			enrollmentResponse = (EnrollmentResponse) xStream.fromXML(get_resp);
			if(null != get_resp && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS) && enrollmentResponse.getErrCode()==EligibilityConstants.TWO_HUNDREAD){
				responseMsg=EligibilityConstants.SUCCESS;	
			}else{
				throw new GIRuntimeException("Unable to disenroll. Error Details: " + enrollmentResponse.getErrCode() + ":" + enrollmentResponse.getErrMsg());
			}
		}catch(Exception e) {
			LOGGER.error("Exception in invokeIndDisenrollment", e);
			throw new GIRuntimeException("Exception occurred while invoking ",e);
		}	
	
		return responseMsg;
	}
	
	/**
	 * Added to this class as part of HIX-67344
	 * 
	 * Computes the effective date as per 15-day rule
	 * 
	 * @author - Nikhil Talreja
	 */
	private Calendar computeEffectiveDate() {
		
		LOGGER.debug("Computing effective date");
		//Coverage date is 1st of next month
		Calendar effectiveDate = TSCalendar.getInstance();
		effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
		effectiveDate.set(Calendar.MINUTE,0);
		effectiveDate.set(Calendar.SECOND,0);
		effectiveDate.set(Calendar.MILLISECOND, 0);
		effectiveDate.set(Calendar.DAY_OF_MONTH,1);
		effectiveDate.add(Calendar.MONTH, 1);
		return effectiveDate;
	}
	
	/**
	 * Story HIX-48237
	 * As an Exchange, allow consumer (House hold) to 
	 * change the Elected APTC Amount
	 * 
	 * Sub-task HIX-57890 
	 * Create api to accept enrollment id, elected aptc 
	 * from plan summary page	
	 * 
	 * Creates List of DTOs to call enrollment API
	 * to update elected APTC
	 * 
	 * Moved to ghix-eligibility as part of HIX-67344
	 * 
	 * @author Nikhil Talreja
	 * @return Success/Failure
	 */
	private List<EnrollmentAptcUpdateDto>  createUpdateAPTCDTO(AptcUpdate aptcUpdate,
			Date aptcEffectiveDate, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap) {
		
		List<EnrollmentAptcUpdateDto> enrollmentAptcUpdateDtoList =	new ArrayList<EnrollmentAptcUpdateDto>();
		EnrollmentShopDTO dentalEnrollment = enrollmentsByTypeMap.get(DENTAL_PLAN);
		EnrollmentShopDTO healthEnrollment = enrollmentsByTypeMap.get(HEALTH_PLAN);
		
		EnrollmentAptcUpdateDto healthPlan = null;
		EnrollmentAptcUpdateDto dentalPlan = null;
		Float aptcAmt = aptcUpdate.getAptcAmt();
		boolean convFlag = false;
		boolean dentalAptcReqd = false;
		
		
		//set the isConvertedToNonFinancialFlag = true if IsNonFinancialConversion(Financial App->Non-Financial App) comes true from reffral 
		if(null!= aptcUpdate.getIsNonFinancialConversion() && aptcUpdate.getIsNonFinancialConversion()) {
			convFlag = true;
			aptcAmt = null;
		}
		
		if(StringUtils.isNotBlank(aptcUpdate.getDentalEnrollmentId())
				&& dentalEnrollment != null
				&& !StringUtils.equalsIgnoreCase(dentalEnrollment.getEnrollmentStatus(),"cancelled")
				&& !StringUtils.equalsIgnoreCase(dentalEnrollment.getEnrollmentStatus(),"terminated")) {
			dentalAptcReqd = true;
		}
		
		//Added below condition for Story HIX-69287
		if (null == aptcUpdate.getHealthEnrollmentId()) {
			//Dental Aptc is zero => sent from web /terminateHealthPlan
			 dentalPlan = createEnrollAptcUpdDTO(
					 aptcUpdate.getDentalEnrollmentId(), aptcUpdate.getTerminationDate(), aptcAmt, convFlag);
		} else if(aptcUpdate.getIsNonFinancialConversion() != null && aptcUpdate.getIsNonFinancialConversion()) {
			 healthPlan = createEnrollAptcUpdDTO(
					 aptcUpdate.getHealthEnrollmentId(), aptcEffectiveDate, aptcAmt, convFlag);
			 if(dentalAptcReqd) {
				 dentalPlan = createEnrollAptcUpdDTO(
						 aptcUpdate.getDentalEnrollmentId(), aptcEffectiveDate, aptcAmt, convFlag);
			 }
		} else {
			//Call Plan display API to determine Health\Dental Aptc amount
			APTCCalculatorResponse aptcCalculatorResponse = callCalculateAptcAPI(enrollmentsByTypeMap, aptcAmt, aptcUpdate.getUserName());
			if(aptcCalculatorResponse != null && aptcCalculatorResponse.getPlans() != null && !aptcCalculatorResponse.getPlans().isEmpty()){
				for(APTCPlanResponseDTO planResponseDTO : aptcCalculatorResponse.getPlans()) {
					if(planResponseDTO != null){
						if(InsuranceType.HEALTH.toString().equalsIgnoreCase(planResponseDTO.getInsuranceType())) {					
							healthPlan = createEnrollAptcUpdDTO(
									 aptcUpdate.getHealthEnrollmentId(), aptcEffectiveDate, planResponseDTO.getAppliedAptc(), convFlag);
						} else {
							if(dentalAptcReqd) {
								 dentalPlan = createEnrollAptcUpdDTO(
										 aptcUpdate.getDentalEnrollmentId(), aptcEffectiveDate, planResponseDTO.getAppliedAptc(), convFlag);
							}
						}								
					}
				}
			}
		}
		
		Float electedHealthAptc = null;
		Float electedDentalAptc = null;	
		if(healthEnrollment != null) {
			electedHealthAptc = healthEnrollment.getAptcAmt();
		}
		if(dentalEnrollment != null) {
			electedDentalAptc = dentalEnrollment.getAptcAmt();
		}
		
		if(healthPlan != null && !isEqual(healthPlan.getAptcAmt(), electedHealthAptc)) {
			enrollmentAptcUpdateDtoList.add(healthPlan);
		}
		
		if(((dentalPlan != null && !isEqual(dentalPlan.getAptcAmt(), electedDentalAptc)) 
				&& (dentalEnrollment.getCoverageEndDate() !=null) && isAptcStartDateBeforeEnrollmentEndDate(dentalPlan.getAptcEffectiveDate(), dentalEnrollment.getCoverageEndDate()))){
			enrollmentAptcUpdateDtoList.add(dentalPlan);
		}				
		
		return enrollmentAptcUpdateDtoList;	
		
	}
	
	private APTCCalculatorResponse callCalculateAptcAPI(Map<String, EnrollmentShopDTO> enrollments, float currentAptc, String userName) {
		APTCCalculatorResponse aptcCalculatorResponse = null;
		try{		
			APTCCalculatorRequest aptcCalculatorRequest = new APTCCalculatorRequest();			
			aptcCalculatorRequest.setCurrentAptc(currentAptc);
			
			List<APTCPlanRequestDTO> planRequestList = new ArrayList<>();
			EnrollmentShopDTO enrollmentForHealth = enrollments.get(HEALTH_PLAN);
			EnrollmentShopDTO enrollmentForDental = enrollments.get(DENTAL_PLAN);
			
			if(enrollmentForHealth!= null){	
				aptcCalculatorRequest.setIsChildPresent(false);
				aptcCalculatorRequest.setMemberCount(enrollmentForHealth.getEnrolleeShopDTOList().size());
				APTCPlanRequestDTO healthPlanRequestDTO =  createPlanRequestDTO(
						enrollmentForHealth.getPlanType().toString(), enrollmentForHealth.getMonthlyPremium(),
						enrollmentForHealth.getPlanLevel(), enrollmentForHealth.getEssentialHealthBenefitPrmPercent());
				planRequestList.add(healthPlanRequestDTO);
			}			
			if(enrollmentForDental != null){
				List<APTCCalculatorMemberData> members = new ArrayList<APTCCalculatorMemberData>();
				boolean hasChild = false;
				for(EnrolleeShopDTO enrolleeShopDTO : enrollmentForDental.getEnrolleeShopDTOList()){
					if(enrolleeShopDTO != null){
						int age = getAgeFromCoverageDate(enrolleeShopDTO.getQuotingDate(), enrolleeShopDTO.getBirthDate());
						if(age < 19){
							hasChild = true;
							//break;
						}
						APTCCalculatorMemberData member =  new APTCCalculatorMemberData();
						member.setId(Long.parseLong(enrolleeShopDTO.getExchgIndivIdentifier()));
						member.setAge(age);
						member.setPremium(enrolleeShopDTO.getTotalIndvResponsibilityAmt());
						members.add(member);
					}
				}
				aptcCalculatorRequest.setMemberData(members);
				aptcCalculatorRequest.setIsChildPresent(hasChild);
				aptcCalculatorRequest.setMemberCount(enrollmentForDental.getEnrolleeShopDTOList().size());
				APTCPlanRequestDTO dentalPlanRequestDTO =  createPlanRequestDTO(
						enrollmentForDental.getPlanType().toString(), enrollmentForDental.getMonthlyPremium(),
						enrollmentForDental.getPlanLevel(), enrollmentForDental.getDntlEssentialHealthBenefitPrmDollarVal());
				planRequestList.add(dentalPlanRequestDTO);
			}			
			
			aptcCalculatorRequest.setPlans(planRequestList); 
			
			String response = ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.CALCULATE_SLIDER_APTC,
					userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,platformGson.toJson(aptcCalculatorRequest)).getBody();
	
			if (null != response){
				aptcCalculatorResponse = platformGson.fromJson(response, APTCCalculatorResponse.class);
			}
			
		}catch (Exception e) {
			LOGGER.error("Exception occured while calling PlanDisplay Slider Api : ", e);
		}		
		return aptcCalculatorResponse;
	}
	
	private APTCPlanRequestDTO createPlanRequestDTO(String insuranceType, Float premium, String level, Float ehbPercentage) {
		APTCPlanRequestDTO planRequestDTO = new APTCPlanRequestDTO();
		planRequestDTO.setInsuranceType(insuranceType);
		planRequestDTO.setPremium(premium);
		planRequestDTO.setLevel(level);
		planRequestDTO.setEhbPercentage(ehbPercentage);
		return planRequestDTO;
	}
	/**
	 * Create EnrollmentAptcUpdateDto for given enrollment
	 * @param enrollmentId
	 * @param aptcEffectiveDate
	 * @param aptcAmt
	 * @param convFlag
	 * @return
	 */
	private EnrollmentAptcUpdateDto createEnrollAptcUpdDTO(String enrollmentId, Date aptcEffectiveDate, Float aptcAmt, boolean convFlag) {
		
		EnrollmentAptcUpdateDto plan = new EnrollmentAptcUpdateDto(); 
		
		plan.setEnrollmentId(Integer.valueOf(enrollmentId));
		plan.setAptcEffectiveDate(aptcEffectiveDate);
		plan.setConvertedToNonFinancialFlag(convFlag);
		plan.setAptcAmt(aptcAmt);
		 
		return plan;
	}
	
	/**
	 * Added to this class as part of HIX-67344
	 * 
	 * Computes the effective date as per 15-day rule
	 * 
	 * @author - Nikhil Talreja
	 */
	private Object getIdFromCaseNumber(String caseNumber){
		EntityManager em = emf.createEntityManager();
		Object id = null;
		try{
		Query query = em.createNativeQuery("select id from ssap_applications where case_number=:caseNumber");
		query.setParameter("caseNumber", caseNumber);
			@SuppressWarnings("unchecked")
			List<Object[]> resultSet = query.getResultList();
			id = resultSet.get(0);
		}catch(Exception ex){
			LOGGER.debug("Exception while retrieving id from case number ", ex);
		}finally{
		em.close();
		}
		return id;
	}
	
	/**
	 * HIX-59205 Use of JSON API to get enrollment details
	 * 
	 * @author Nikhil Talreja
	 * 
	 * @param applicationId - SSAP ID
	 * @param caseNumber - Case number SSAP 
	 * 
	 */
	public Map<String, EnrollmentShopDTO> getEnrollmentDetailsJson(String applicationId, String caseNumber, String userName) throws GIException{
		
		EnrolleeResponse enrolleeResponse = null;
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>(0);
		StringBuilder errMsg = new StringBuilder("Exception occured while fetching Enrollment Plan Details URL: " 
						+ GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON +" with details:");
		
		try {	
			boolean isRespInvalid = true;
			if(null  != applicationId){
				
				EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
				enrolleeRequest.setSsapApplicationId(Long.parseLong(applicationId));
								
				errMsg.append(" [" + enrolleeRequest + "]");
				LOGGER.debug("Calling enrollment api " + GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON +" with details: [" + enrolleeRequest + "]");
				ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON, 
						userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrolleeRequest)); 
				
				if (null != response && null != response.getBody() && null != response.getBody()){ 
					enrolleeResponse = platformGson.fromJson(response.getBody(), EnrolleeResponse.class);
					if(enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						enrollmentsByTypeMap =  extractEnrollments(enrolleeResponse.getEnrollmentShopDTOList());
						isRespInvalid = false;
					}
					else{
						errMsg.append(",Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
					}
				}
			}
			
			if(isRespInvalid) {
				throw new GIException(errMsg.toString());
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : " + e.getMessage(), e);
			throw new GIException("Unable to get Enrollment Plan Details", e);
		}
		return enrollmentsByTypeMap;
	}
	
	private Map<String, EnrollmentShopDTO> extractEnrollments(List<EnrollmentShopDTO> enrollmentShopDTOs) throws GIException{
		
		List<EnrollmentShopDTO> enrollmentForHealth = new ArrayList<EnrollmentShopDTO>();
		List<EnrollmentShopDTO> enrollmentForDental = new ArrayList<EnrollmentShopDTO>();
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>(0);
		
		for (EnrollmentShopDTO enrollmentShopDto : enrollmentShopDTOs) {
			if(enrollmentShopDto.getPlanType().equalsIgnoreCase(HEALTH_PLAN)){
				enrollmentForHealth.add(enrollmentShopDto);
			}else if(enrollmentShopDto.getPlanType().equalsIgnoreCase(DENTAL_PLAN)){
				enrollmentForDental.add(enrollmentShopDto);
			}
		}
			
		if(enrollmentForHealth.size()>1){
			Collections.sort(enrollmentForHealth, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
		}
		if(enrollmentForHealth!= null && !enrollmentForHealth.isEmpty()){
			enrollmentsByTypeMap.put(HEALTH_PLAN ,enrollmentForHealth.get(0));
		}
		if(enrollmentForDental.size()>1){
			Collections.sort(enrollmentForDental, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
		}
		if(enrollmentForDental != null && !enrollmentForDental.isEmpty()){
			enrollmentsByTypeMap.put(DENTAL_PLAN, enrollmentForDental.get(0));
		}
		
		return enrollmentsByTypeMap;
	}
	
	/**
	 * Story HIX-48237
	 * As an Exchange, allow consumer (House hold) to 
	 * change the Elected APTC Amount
	 * 
	 * Sub-task HIX-57890 
	 * Create api to accept enrollment id, elected aptc 
	 * from plan summary page	
	 * 
	 * Makes the API call to change the elected APTC
	 * for the enrollment
	 * 
	 * Moved to ghix-eligibility as part of HIX-67344
	 * 
	 * @author Nikhil Talreja
	 * @return Success/Failure
	 */
	public String changeElectedAPTC(AptcUpdate aptcUpdate) throws GIException{
	
		try{
			
			Date aptcEffectiveDate = null;
			SimpleDateFormat df = new SimpleDateFormat(SHORT_DATE_FORMAT);
			
			if(aptcUpdate.getAptcEffectiveDate() != null) {
				aptcEffectiveDate = aptcUpdate.getAptcEffectiveDate();
			} else {
				aptcEffectiveDate = computeEffectiveDate().getTime();
			}

			/*
			 * Fetch existing APTC for health and dental plans
			 * Call enrollment API only if current and new APTC values are different
			 */
			Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = 
					getEnrollmentDetailsJson(getIdFromCaseNumber(aptcUpdate.getCaseNumber()).toString(), aptcUpdate.getCaseNumber(), aptcUpdate.getUserName());
			EnrollmentShopDTO healthEnrollment = enrollmentsByTypeMap.get(HEALTH_PLAN);

			/**
			 * Compare the health enrollment start date and aptc effective date
			 * and pick the one that is later
			 * 
			 */
			if(null!=healthEnrollment && null != aptcUpdate.getHealthEnrollmentId()){
				String healthEnrollmentStartDate = df.format(healthEnrollment.getCoverageStartDate());
				String healthEnrollmentEndDate = df.format(healthEnrollment.getCoverageEndDate());
				if(StringUtils.isNotEmpty(healthEnrollmentEndDate) && isAptcStartDateAfterEnrollmentEndDate(aptcEffectiveDate, healthEnrollmentEndDate)){
					return EligibilityConstants.FAILURE;
				}
				if(StringUtils.isNotEmpty(healthEnrollmentStartDate) && isAptcStartDateBeforeEnrollmentStartDate(aptcEffectiveDate, healthEnrollmentStartDate)){
					aptcEffectiveDate = df.parse(healthEnrollmentStartDate);
				}
				
			}	
			
			List<EnrollmentAptcUpdateDto> enrollmentAptcUpdateDtoList =	createUpdateAPTCDTO(aptcUpdate, aptcEffectiveDate, enrollmentsByTypeMap);
			
			//Call enrollment API to change elected APTC
			return updateEnrollmentAptc(enrollmentAptcUpdateDtoList, aptcUpdate.getUserName());				
		}
		catch(Exception e){
			LOGGER.error("Error occured while calling enrollment API  to change elected APTC: ",e);
			return EligibilityConstants.FAILURE;
		}
		
	}
	
	

	public void notifyOnSepQepDenied(ReverseSepQepDenial reverseSepQepDenial) {
		try {
			
			String caseNumber = reverseSepQepDenial.getCaseNumber();
			
			SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumberId(caseNumber);
			if (null == ssapApplication) {
				LOGGER.error("No SSAP Application is present for the caseNumber: ");
			}
			
			boolean isFinancial = "Y".equalsIgnoreCase(ssapApplication.getFinancialAssistanceFlag());
			String applicationType = ssapApplication.getApplicationType();
			
			if (ssapApplication.getValidationStatus() == ApplicationValidationStatus.PENDING){
				referralLceNotificationService.generateLceDocumentRequiredNotice(caseNumber);
			} else {
			if (isFinancial) {
				if (StringUtils.equalsIgnoreCase(applicationType, "SEP")) {
					referralLceNotificationService.generateSEPEventNotice(caseNumber);
				}else if (StringUtils.equalsIgnoreCase(applicationType, "QEP")) {
					referralLceNotificationService.generateQEPEventNotice(caseNumber);
				}
			}else if (!isFinancial && null!=ssapApplication) {
				if (StringUtils.equalsIgnoreCase(applicationType, "SEP")) {
					// TODO : the below notice will be triggered in case of MN for now we can change if needed
					// Replace EE027 by EE020 in case of MN
					String stateCode = DynamicPropertiesUtil
							.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					if (stateCode.equalsIgnoreCase("MN")) {
						// trigger SEP LCE04 notice (EE020)
						referralLceNotificationService.generateSEPEventNotice(ssapApplication.getCaseNumber());
					} else {
						List <SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplication.getId());
						Household household = cmrHouseholdRepository.findOne(reverseSepQepDenial.getCmrHouseholdId().intValue()); 
						AccountUser accountUser = household.getUser();
						DateFormat dateFormat = new SimpleDateFormat(SHORT_DATE_FORMAT);
						lifeChangeEventTask.sendNotification(null, accountUser, LifeChangeEventConstant.LCE_SEP_NOTIFICATION_FILE_NAME, 
								LifeChangeEventConstant.LCE_SEP_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_SEP_ECM_RELATIVE_PATH, 
								dateFormat.parse(reverseSepQepDenial.getEnrollmentEndDate()),ssapApplicants, household);	
					}
					
				}else if (StringUtils.equalsIgnoreCase(applicationType, "QEP")) {
					qepNonFinancialNotificationService.generateQEPGrantNoticeForNonFinancial(caseNumber);
				}
			}
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while calling enrollment API  to change elected APTC: ",e);
		}
	}
	
	private boolean isEqual(Float f1, Float f2){
		boolean isEqual = false;
		
		if(f1 == null && f2 == null){
			isEqual = true;
		}else if(f1!=null && f2!=null && f1.floatValue()==f2.floatValue()){
			isEqual=true;
		}
		return isEqual;
	}
	
	public boolean isAptcStartDateBeforeEnrollmentStartDate(Date aptcEffectiveDate, String enrollmentEffectiveStartDate) throws ParseException{
		boolean isAptcStartDateBeforeEnrollmentStartDate = false;
		
		SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
		LocalDate aptcStartDate = new DateTime(aptcEffectiveDate.getTime()).toLocalDate();
		LocalDate enrollmentStartDate = new DateTime(mmddyyyy.parseObject(enrollmentEffectiveStartDate)).toLocalDate();
		if(aptcStartDate.isBefore(enrollmentStartDate)){
			isAptcStartDateBeforeEnrollmentStartDate = true;
		}
		
		return isAptcStartDateBeforeEnrollmentStartDate;
	}

	public boolean isAptcStartDateAfterEnrollmentEndDate(Date aptcEffectiveDate, String enrollmentEffectiveEndDate) throws ParseException{
		boolean isAptcStartDateAfterEnrollmentEndDate = false;
		
		SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
		LocalDate aptcStartDate = new DateTime(aptcEffectiveDate.getTime()).toLocalDate();
		LocalDate enrollmentEndDate = new DateTime(mmddyyyy.parseObject(enrollmentEffectiveEndDate)).toLocalDate();
		if(aptcStartDate.isAfter(enrollmentEndDate)){
			isAptcStartDateAfterEnrollmentEndDate = true;
		}
		
		return isAptcStartDateAfterEnrollmentEndDate;
	}
	
	public boolean isAptcStartDateBeforeEnrollmentEndDate(Date aptcEffectiveDate, Date enrollmentEffectiveEndDate){
		boolean isAptcStartDateBeforeEnrollmentEndDate = false;		
		
		LocalDate aptcStartDate = new DateTime(aptcEffectiveDate.getTime()).toLocalDate();
		LocalDate enrollmentEndDate = new DateTime(enrollmentEffectiveEndDate).toLocalDate();
		if(aptcStartDate.isBefore(enrollmentEndDate)){
			isAptcStartDateBeforeEnrollmentEndDate = true;
		}		
		
		return isAptcStartDateBeforeEnrollmentEndDate;
	}
	
	public static int getAgeFromCoverageDate(Date coverageStartDate, Date dateOfBirth) {
		Calendar today = TSCalendar.getInstance();
		today.setTime(coverageStartDate);
		Calendar dob = TSCalendar.getInstance();
		dob.setTime(dateOfBirth);
		Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if(dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) 
			|| (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE))){
			age--;
		}
		
		return age;
	}

	public List<DashboardApplicantDTO> getApplicantDetails(Long ssapApplicantionId) throws GIException {
		SsapApplication ssapApplication = getSsapApplicationById(ssapApplicantionId);
		List<DashboardApplicantDTO> applicants = new ArrayList<DashboardApplicantDTO>();
		if(ssapApplication != null){	
			SingleStreamlinedApplication singleStreamlinedApplication =  ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
			TaxHousehold taxHousehold = singleStreamlinedApplication.getTaxHousehold().get(0);
			List<HouseholdMember> householdMembers = taxHousehold.getHouseholdMember();
			if(ssapApplicantionId != null){
				List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplicantionId);
				if(ssapApplicants != null && !ssapApplicants.isEmpty()){
					for(SsapApplicant ssapApplicant : ssapApplicants){
						DashboardApplicantDTO applicantDTO = new DashboardApplicantDTO();
						applicantDTO.setGuid(ssapApplicant.getApplicantGuid());
						applicantDTO.setFirstName(ssapApplicant.getFirstName());
						applicantDTO.setLastName(ssapApplicant.getLastName());
						applicantDTO.setMiddleName(ssapApplicant.getMiddleName() != null ? ssapApplicant.getMiddleName() : StringUtils.EMPTY);
						applicantDTO.setMemberId(Long.toString(ssapApplicant.getId()));
						applicantDTO.setNameSuffix(ssapApplicant.getNameSuffix()!= null ? ssapApplicant.getNameSuffix() : StringUtils.EMPTY);
						
						if(StringUtils.isNotBlank(ssapApplicant.getRelationship())){
							applicantDTO.setRelation(getRelationShip(ssapApplicant.getRelationship()).toString());
						}
						
						applicantDTO.setCsrLevel(ssapApplicant.getCsrLevel());					
						
						if(StringUtils.isNotBlank(ssapApplicant.getApplyingForCoverage()) && ssapApplicant.getApplyingForCoverage().equalsIgnoreCase("Y")){
							applicantDTO.setSeekingCoverage(true);
						}else{
							applicantDTO.setSeekingCoverage(false);
						}
							
						if(StringUtils.isNotBlank(ssapApplicant.getNativeAmericanFlag()) && ssapApplicant.getNativeAmericanFlag().equalsIgnoreCase("Y")){
							applicantDTO.setNativeAmerican(true);
						}else{
							applicantDTO.setNativeAmerican(false);
						}
						
						List<EligibilityProgram>  applicantEligibilities = getProgramEligibility(ssapApplicant);
						
						if(applicantEligibilities != null && applicantEligibilities.size() > 0) {
							for(EligibilityProgram eligibilityProgram : applicantEligibilities){
								boolean isEligibilityTrue = "TRUE".equalsIgnoreCase(eligibilityProgram.getEligibilityIndicator());
								if (EligibilityType.EXCHANGE_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setExchangeEligible(isEligibilityTrue);
								}
								if (EligibilityType.APTC_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setAptcEligible(isEligibilityTrue);
								}
								if (EligibilityType.STATE_SUBSIDY_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setStateSubsidyEligible(isEligibilityTrue);
								}
								if (EligibilityType.CSR_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setCsrEligible(isEligibilityTrue);
								}
								if (EligibilityType.CHIP_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setChipEligible(isEligibilityTrue);
								}
								if (EligibilityType.ASSESSED_CHIP_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setAssessedChipEligibile(isEligibilityTrue);
								}
								if (EligibilityType.MEDICAID_MAGI_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setMedicaidMAGIEligibile(isEligibilityTrue);
								}
								if (EligibilityType.MEDICAID_NON_MAGI_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setMedicaidNonMAGIEligibile(isEligibilityTrue);
								}
								if (EligibilityType.ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setAssessedMedicaidMAGIEligibile(isEligibilityTrue);
								}
								if (EligibilityType.ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE.toString().equalsIgnoreCase(eligibilityProgram.getEligibilityType())){
									applicantDTO.setAssessedMedicaidNonMAGIEligibile(isEligibilityTrue);
								}
							}
						}
						
						List<SsapApplicantEvent>  ssapApplicantEvents = ssapApplicantEventRepository.getApplicantEventByApplicantId(ssapApplicant.getId());
						if(ssapApplicantEvents != null && !ssapApplicantEvents.isEmpty()){
							List<SepApplcantEventDTO> sepApplcantEvents = new ArrayList<SepApplcantEventDTO>();
							for(SsapApplicantEvent ssapApplicantEvent : ssapApplicantEvents){
								if(ssapApplicantEvent != null){
									SepEvents sepEvents = ssapApplicantEvent.getSepEvents();
									if(sepEvents != null){
										SepApplcantEventDTO sepApplcantEventDTO = new SepApplcantEventDTO();
										sepApplcantEventDTO.setEventName(sepEvents.getName());
										sepApplcantEventDTO.setChangeType(sepEvents.getChangeType());
										sepApplcantEvents.add(sepApplcantEventDTO);
									}
								}
							}
							applicantDTO.setSepApplicantEvents(sepApplcantEvents);
						}
						HouseholdMember householdMember = getMemberByApplicant(ssapApplicant.getApplicantGuid(), householdMembers);
						if(householdMember != null) {
							applicantDTO.setSeeksQHP(householdMember.isSeeksQhp());
						}
						
						applicants.add(applicantDTO);
					}
				}		
			}
		}		
		return applicants;
	}
	
	private HouseholdMember getMemberByApplicant(String applicantGuid, List<HouseholdMember> householdMembers) {
		if(householdMembers != null && householdMembers.size()>0) {
			for(HouseholdMember householdMember: householdMembers){
				if(householdMember.getApplicantGuid().equals(applicantGuid)) {
					return householdMember;
				}
			}
		}
		return null;
	}

	private List<EligibilityProgram> getProgramEligibility(SsapApplicant ssapApplicant) {
		return eligibilityProgramRepository.getApplicantEligibilities(ssapApplicant.getId());
	}
	
	private SsapApplication getSsapApplicationById(Long applicationId) throws GIException{
		SsapApplication ssapApplication = null;
		try{
			List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(applicationId);
			if (ssapApplications != null && ssapApplications.size() > 0) {
				ssapApplication = ssapApplications.get(0);
			}		
		}catch (Exception e) {
			LOGGER.error("Unable to find application for ssapApplicationId " + applicationId);
		}		
		return ssapApplication;		
	}
	
	private RelationEnum getRelationShip(String relationId) {
		 RelationEnum relationShipCode = null;			
		if (SELF_RELATIONSHIP_CODES.contains(relationId)){
			relationShipCode = RelationEnum.SELF;
		} else if (SPOUSE_RELATIONSHIP_CODES.contains(relationId)){
			relationShipCode = RelationEnum.SPOUSE;
		} else if (CHILD_RELATIONSHIP_CODES.contains(relationId)){
			relationShipCode = RelationEnum.CHILD;
		} else {
			relationShipCode = RelationEnum.DEPENDENT;
		}
		return relationShipCode;
	 }
		
	
	public String updateAPTC(AptcUpdate aptcUpdate) throws GIException{			
		try {	
			Date aptcEffectiveDate = null;
			if(aptcUpdate.getAptcEffectiveDate() != null) {
				aptcEffectiveDate = aptcUpdate.getAptcEffectiveDate();
			} else {
				aptcEffectiveDate = computeEffectiveDate().getTime();
			}			
			
			List<EnrollmentDataDTO> enrollmentDataDTOList = getEnrollmentDateByEnrlId(aptcUpdate);
			EnrollmentDataDTO enrollmentDataDTO = enrollmentDataDTOList.get(0);
						
			List<EnrollmentAptcUpdateDto> enrollmentAptcUpdateDtoList =	new ArrayList<EnrollmentAptcUpdateDto>();	
			EnrollmentAptcUpdateDto  enrollmentAptcUpdateDto  = createEnrollAptcUpdDTO(enrollmentDataDTO.getEnrollmentId().toString(), aptcEffectiveDate, aptcUpdate.getAptcAmt(), false);
			enrollmentAptcUpdateDtoList.add(enrollmentAptcUpdateDto);
			return updateEnrollmentAptc(enrollmentAptcUpdateDtoList, aptcUpdate.getUserName());			
			
		} catch (Exception e) {
			LOGGER.error("Error occured while calling enrollment API to change elected APTC in case of custom grouping: ",e);
			return EligibilityConstants.FAILURE;
		}	
	}	
	
	private List<EnrollmentDataDTO> getEnrollmentDateByEnrlId(AptcUpdate aptcUpdate) throws GIException{
		try {	
					
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			List<Integer> idList = new ArrayList<Integer>();
			String enrollmentId = null;
			if(StringUtils.isNotBlank(aptcUpdate.getHealthEnrollmentId())){
				enrollmentId = aptcUpdate.getHealthEnrollmentId();
			}else if(StringUtils.isNotBlank(aptcUpdate.getDentalEnrollmentId())){
				enrollmentId = aptcUpdate.getDentalEnrollmentId();
			}	
			if(enrollmentId != null){
				idList.add(Integer.valueOf(enrollmentId));
				 enrollmentRequest.setIdList(idList);
				 enrollmentRequest.setPremiumDataRequired(true);
				 enrollmentRequest.setSepDataRequired(false);
				 String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_LIST_OF_HOUSEHOLDCASEIDS, 
						 aptcUpdate.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
	
				 if (null != response ){
					 EnrollmentResponse enrollmentResponse = platformGson.fromJson(response, EnrollmentResponse.class);					
					 if(enrollmentResponse != null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						 List<EnrollmentDataDTO> enrollmentDataDTOList = new ArrayList<EnrollmentDataDTO>(1);	
						 for (Map.Entry<String, List<EnrollmentDataDTO>> entry : enrollmentResponse.getEnrollmentDataDtoListMap().entrySet()) {
							 enrollmentDataDTOList = entry.getValue();
						 }
						 return enrollmentDataDTOList;					
					 }	else{
						throw new GIException("Error Details: " + enrollmentResponse.getErrCode() + ":" + enrollmentResponse.getErrMsg());
					 }
				 }else{
					 throw new GIException("Exception occured while fetching Enrollment data by enrollment ID : "+ enrollmentId);
				 }
			}else{
				throw new GIException("Exception occured while fetching Enrollment data by enrollment ID : "+ enrollmentId);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : " + e.getMessage(), e);
			throw new GIException("Exception occured while fetching Enrollment data by enrollment Id", e);
		}	
	}
	
	private String updateEnrollmentAptc(List<EnrollmentAptcUpdateDto> enrollmentAptcUpdateDtoList, String userName){
		//Call enrollment API to change elected APTC
		if(enrollmentAptcUpdateDtoList != null && !enrollmentAptcUpdateDtoList.isEmpty()){
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setEnrollmentAptcUpdateDtoList(enrollmentAptcUpdateDtoList);
			
			String jsonResponse = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_APTC, userName, HttpMethod.POST, 
					MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest))).getBody();
			
			EnrollmentResponse enrollmentResponse = platformGson.fromJson(jsonResponse, EnrollmentResponse.class);
			if (null != enrollmentResponse && StringUtils.equalsIgnoreCase(enrollmentResponse.getStatus(),GhixConstants.RESPONSE_SUCCESS)) {
				return EligibilityConstants.SUCCESS;
			}		
		}
		return EligibilityConstants.FAILURE;
	}
					
}
