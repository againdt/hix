package com.getinsured.eligibility.at.ref.service;

import java.util.Date;

/**
 * @author chopra_s
 *
 */
public interface ReferralOEService {
	
	boolean isOpenEnrollment(long coverageYear);
	
	boolean isQE();
	
	boolean isQE(long coverageYear);
	
	boolean isQEPDuringOE(long coverageYear);
	
	boolean isRenewal(long coverageYear);
	
	boolean isDateInsideOEWindow(Date date);

}
