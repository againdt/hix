package com.getinsured.eligibility.ssap.integration.household.service;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

@Component(value = "ssacRequestServiceActivator")
public class SSACRequestServiceActivator {

	private static final String SSAC_REQUEST_STATUS = "SSAC_REQUEST_STATUS";
    private static final Logger LOGGER = LoggerFactory.getLogger(SSACRequestServiceActivator.class);
    @Value("#{configProp['fdh.cms_partner'] != null ? configProp['fdh.cms_partner'] : 'NOT_PROVIDED'}") 
   	private String cms_partner;

	@ServiceActivator
	public Message<String> receive(Message<String> message) throws UnknownHostException, ParseException {
		String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.FILTERED_SSAP_JSON_HEADER).toString();
		Message<String> ssacRequest = removeNonSSNApplicants(ssapJSON, message);
		return ssacRequest;
	}

	private Message<String> removeNonSSNApplicants(String ssapJSON, Message<String> message) throws ParseException, UnknownHostException {
		String ssacRequestObject = processJsonForSSACRequest(ssapJSON, message.getHeaders().get(SSAPIntegrationConstants.CLIENT_IP_HEADER) + "");
		if(ssacRequestObject.equals(StringUtils.EMPTY)) {
			return MessageBuilder.withPayload(ssacRequestObject).copyHeaders(message.getHeaders()).setHeader(SSAC_REQUEST_STATUS, "ERROR").build();
		}
		return MessageBuilder.withPayload(ssacRequestObject).copyHeaders(message.getHeaders()).setHeader(SSAC_REQUEST_STATUS, "SUCCESS").build();
	}


	@SuppressWarnings("unchecked")
	public String processJsonForSSACRequest(String ssapJSON, String ipAddress)
			throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject ssacRequestObject = new JSONObject();
		List<JSONObject> membersToRemove = new ArrayList<JSONObject>();

		JSONObject ssapJSONObject = (JSONObject) parser.parse(ssapJSON);
		JSONArray taxHouseholds = (JSONArray) ((JSONObject) ssapJSONObject
				.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION))
				.get(SSAPIntegrationConstants.TAX_HOUSEHOLD);
		JSONArray updatedtaxHouseHolds = new JSONArray();
		String applicationId = ((JSONObject) ssapJSONObject
				.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION))
				.get(SSAPIntegrationConstants.SSAP_APPLICATION_ID).toString();
		ssacRequestObject.put("applicationId", Long.parseLong(applicationId));
		ssacRequestObject.put(SSAPIntegrationConstants.CLIENT_IP, GhixPlatformConstants.LOCAL_NODE_IP);
		ssacRequestObject.put("cms_partner",this.cms_partner);
		for(Object taxHousehold : taxHouseholds) {
			JSONObject household = (JSONObject) taxHousehold;
			JSONArray householdMembers = (JSONArray) household.get(SSAPIntegrationConstants.HOUSEHOLD_MEMBER);
			for(Object householdMember : householdMembers) {
				JSONObject member = (JSONObject) householdMember;
				member = SSACfilter.filterMemberAttributesForSSAC(member);
				JSONObject socialSecurityCard = (JSONObject)member.get("socialSecurityCard");
				if (null == socialSecurityCard || null == socialSecurityCard.get("socialSecurityNumber") || StringUtils.isBlank(socialSecurityCard.get("socialSecurityNumber").toString())) {
					membersToRemove.add(member);
				} else {
					JSONObject nameObject = (JSONObject)member.get("name");
					if(null != socialSecurityCard.get("nameSameOnSSNCardIndicator") && !Boolean.valueOf(socialSecurityCard.get("nameSameOnSSNCardIndicator").toString())) {
						if(null != socialSecurityCard.get("firstNameOnSSNCard") && StringUtils.isNotBlank(socialSecurityCard.get("firstNameOnSSNCard").toString())) {
							nameObject.put("firstName", socialSecurityCard.get("firstNameOnSSNCard"));
						}

						if(null != socialSecurityCard.get("middleNameOnSSNCard") && StringUtils.isNotBlank(socialSecurityCard.get("middleNameOnSSNCard").toString())) {
							nameObject.put("middleName", socialSecurityCard.get("middleNameOnSSNCard"));
						} else {
							nameObject.remove("middleName");
						}

						if(null != socialSecurityCard.get("lastNameOnSSNCard") && StringUtils.isNotBlank(socialSecurityCard.get("lastNameOnSSNCard").toString())) {
							nameObject.put("lastName", socialSecurityCard.get("lastNameOnSSNCard"));
						}
					} else if(null != nameObject && nameObject.get("middleName") != null && StringUtils.isBlank(nameObject.get("middleName").toString())) {
						nameObject.remove("middleName");
					}
				}

				// Hardcode incarceration to true because we do not take user's word for doing the incarceration
				((JSONObject)member.get("incarcerationStatus")).put("incarcerationAsAttestedIndicator", true);

				boolean citizenshipAsAttested = (boolean)((JSONObject)member.get("citizenshipImmigrationStatus")).get("citizenshipAsAttestedIndicator");
				boolean citizenshipStatus = (boolean)((JSONObject)member.get("citizenshipImmigrationStatus")).get("citizenshipStatusIndicator");

				if(citizenshipStatus && !citizenshipAsAttested) {
					((JSONObject)member.get("citizenshipImmigrationStatus")).put("citizenshipAsAttestedIndicator", true);
				}

				if(citizenshipAsAttested && !citizenshipStatus) {
					((JSONObject)member.get("citizenshipImmigrationStatus")).put("citizenshipStatusIndicator", true);
				}
			}
			householdMembers.removeAll(membersToRemove);

			if(householdMembers.isEmpty()) {
			    return StringUtils.EMPTY;
			}

			household.put(SSAPIntegrationConstants.HOUSEHOLD_MEMBER, householdMembers);
			updatedtaxHouseHolds.add(household);
		}
		JSONObject payload = new JSONObject();
		payload.put(SSAPIntegrationConstants.TAX_HOUSEHOLD, updatedtaxHouseHolds);
		ssacRequestObject.put("payload", payload);
		return ssacRequestObject.toJSONString();
	}
}
