(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .directive('monthSelect',monthSelect);

    function monthSelect(){

            return {
                restrict: 'E',
                replace: true,
                scope: '=',
                template: '<select id="monthSelect"><option ng-repeat="month in months" value="{{month.monthNum}}">{{month.monthName}}</option></select>',
                // template: '<select ng-options="month.value for month in months"></select>',
                controller: ["$scope", "$element", "$attrs", function (scope, element, attrs) {
                }]
            }
        }
})();
