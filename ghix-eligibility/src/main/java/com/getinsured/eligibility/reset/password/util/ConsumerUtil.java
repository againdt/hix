package com.getinsured.eligibility.reset.password.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.exception.DataNotFoundException;
import com.getinsured.eligibility.exception.InvalidStateException;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.reset.password.dto.Question;
import com.getinsured.eligibility.reset.password.dto.Question.Code;
import com.getinsured.eligibility.reset.password.dto.Question.DataType;
import com.getinsured.eligibility.reset.password.dto.Questions;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.CAPConfiguration.CAPConfigurationEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.repository.IModuleUserRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component
public class ConsumerUtil {

	
	private static final Logger LOGGER = Logger
			.getLogger(ConsumerUtil.class);
	private static final int NO_OF_RANDOM_QS = 2;
	private static final int THIRTY_TWO = 32;
	public static final String RESET_CONTROLLER_URL = "account/user/reset/";
	private static final String APPEVENT_ACCOUNT = "APPEVENT_ACCOUNT";
    private static final String APP_RESET_EVENT_TYPE = "ACCOUNT_PASSWORD_RESET_LINK";
    private static final String APP_LOCK_EVENT_TYPE = "ACCOUNT_LOCKED";
    private static final String SENT_BY_KEY = "Sent By";
    private static final String LOCKED_BY_KEY = "Locked By";
    private static final String SENT_BY_LABEL = "Sent by password reset bot";
    private static final String LOCKED_BY_LABEL = "Locked by password reset bot ";
    private static final String CHANNEL_TYPE = "Channel Type";
    private static final String EXADMIN_USERNAME = "exadmin@ghix.com";
	
	@Autowired
	private UserService userService;
	
	@Autowired 
	private IModuleUserRepository iModuleUserRepository;
	
	@Autowired 
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired 
	private ILocationRepository iLocationRepository;
	
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Autowired 
	private AppEventService appEventService;
    
	@Autowired 
	private LookupService lookupService;
	
	
	public AccountUser findAccount(String phone, String userName){
		AccountUser accountUser = null;
		if(phone!=null && userName==null){
			List<AccountUser> accountUsers = userService.findByPhone(phone);
 			if(accountUsers != null && accountUsers.size() > 0){
 				if(accountUsers.size() == 1){
 					accountUser = accountUsers.get(0);
 	 			}else{
 	 				throw new InvalidStateException("Application not found for this account");
 	 			}
 				
 			}
		}else if(phone==null && userName!=null){
			accountUser = userService.findByEmail(userName);
		}else if(phone!=null && userName!=null){
			accountUser = userService.findByPhoneAndUserName(phone, userName);
		}else{
			throw new InvalidStateException("Application not found for this account");
		}
		
		return accountUser;
	}
	
	public AccountUser accountWithApplication(AccountUser accountUser){
		AccountUser verifiedAccount = null;
		if(accountUser != null){
			Household household = getHouseholdByUser(accountUser);
			if(household != null){
				List<SsapApplication> ssapApplicationsList = ssapApplicationRepository.findByCmrHouseoldId(new BigDecimal(household.getId()));
				if(ssapApplicationsList.size() > 0){
 						verifiedAccount = accountUser;
 					}
				}
			}
		return verifiedAccount;
	}
	
	public boolean verifyIfAccountLocked(AccountUser acountUser){
		Boolean isAccountLocked = Boolean.FALSE;
		if(acountUser.getConfirmed() == 0 || acountUser.getSecQueRetryCount() >= getResetPasswordRetryLimit()){
			isAccountLocked = Boolean.TRUE;
		}
		return isAccountLocked;
	}
	
	private int getResetPasswordRetryLimit() {
		String passwordResetRetryLimit = DynamicPropertiesUtil.getPropertyValue(CAPConfigurationEnum.CAP_CSR_PASSWORD_RESET_RETRIES_LIMIT.getValue());
		int noOfRetries = 0;
		if(StringUtils.isNumeric(passwordResetRetryLimit)){
			noOfRetries = Integer.parseInt(passwordResetRetryLimit);
		}
		else{
			LOGGER.info("******** passwordResetRetryLimit IS EITHER NOT CONFIGURED OR NOT NUMERIC ***********");
		}
		return noOfRetries;
	}
	
	private Household getHouseholdByUser(AccountUser accountUser){
		Household household = null;
		List<ModuleUser> moduleUsers = iModuleUserRepository.findUserModules(accountUser.getId(),"individual");
		if(moduleUsers != null && moduleUsers.size() > 0){
			ModuleUser moduleUser = moduleUsers.get(0);
			if(moduleUser != null){
				household = cmrHouseholdRepository.findOne(moduleUser.getModuleId());
			}
		}
		return household;
	}
	
	public List<Question> getResetPasswordQuestions(int accountId){
		List<Question> questionsList = null;
		AccountUser accountUser = userService.findById(accountId);
		if(accountUser != null){
			Household household = getHouseholdByUser(accountUser);
			if(household != null){
				questionsList = this.createResetPasswordQuestions(household);
			}
		}
		return questionsList;
	}
		
		
	
	private List<Question> createResetPasswordQuestions(Household household){
		List<Question> questionList = null;
		boolean validateFlag = false;
		List<Question> randomQuestionsList = new ArrayList<>();
		questionList = createFixedQuestionAnswers(household, validateFlag);
		
		this.addApplicantLevelQuestions(household, randomQuestionsList, validateFlag);
		this.addRandomQuestions(questionList, randomQuestionsList);
		return questionList;
	}
	
	private List<Question> createFixedQuestionAnswers(Household household, boolean validateFlag){
		List<Question> fixedQuestionList = new ArrayList<>();
		if(validateFlag){
			fixedQuestionList.add(new Question(Code.FIRSTNAME, DataType.STRING, household.getFirstName()));
			fixedQuestionList.add(new Question(Code.LASTNAME, DataType.STRING, household.getLastName()));
			if(household.getBirthDate() != null && !StringUtils.isEmpty(household.getBirthDate().toString())){
				fixedQuestionList.add(new Question(Code.DOB, DataType.DOB, this.getFormattedDOB(household.getBirthDate())));
			}
		}else{
			fixedQuestionList.add(new Question(Code.FIRSTNAME, DataType.STRING, null));
			fixedQuestionList.add(new Question(Code.LASTNAME, DataType.STRING, null));
			fixedQuestionList.add(new Question(Code.DOB, DataType.DOB, null));
		}
		
		
		return fixedQuestionList;
	}
	
	private void addApplicantLevelQuestions(Household household, List<Question> randomQuestionsList, boolean validateFlag) {
		List<SsapApplication> ssapApplicationsList = ssapApplicationRepository.findLatestSsapApplicationByHouseholdId(new BigDecimal(household.getId()));
		SsapApplicant primaryApplicant = null;
		List<SsapApplicant> ssapApplicants = null;
		SsapApplication ssapApplication = null;
		if(ssapApplicationsList != null && ssapApplicationsList.size() > 0 && !ssapApplicationsList.isEmpty()){
			ssapApplication = ssapApplicationsList.get(0);
			if (null != ssapApplication){
				ssapApplicants = ssapApplication.getSsapApplicants();
				if(!ssapApplicants.isEmpty()){
					for (SsapApplicant ssapApplicant : ssapApplicants) {
						if(ssapApplicant.getPersonId() == 1){
							primaryApplicant = ssapApplicant;
							break;
						}
					}
				}
			}
		}else{
			throw new InvalidStateException("Application not found for this account");
		}
		
		if(primaryApplicant != null){
			Location location = this.getLocation(primaryApplicant);
			if(validateFlag){
				randomQuestionsList.add(new Question(Code.HOUSEHOLDSIZE, DataType.INT, String.valueOf(ssapApplicants.size())));
				if(!StringUtils.isEmpty(primaryApplicant.getGender())){
					randomQuestionsList.add(new Question(Code.GENDER, DataType.STRING, primaryApplicant.getGender()));
				}
				if(!StringUtils.isEmpty(primaryApplicant.getSsn())){
					randomQuestionsList.add(new Question(Code.SSN, DataType.STRING, primaryApplicant.getSsn().substring(primaryApplicant.getSsn().length()-4,primaryApplicant.getSsn().length())));
				}
				if(location!= null && !StringUtils.isEmpty(location.getZip())){
					randomQuestionsList.add(new Question(Code.ZIPCODE, DataType.STRING, location.getZip()));
				}
				if(location!= null && !StringUtils.isEmpty(location.getCounty())){
					randomQuestionsList.add(new Question(Code.COUNTY, DataType.STRING, location.getCounty()));
				}
				/*
				 * Override dob in case application is found.
				 * */
				if(primaryApplicant.getBirthDate() != null && !StringUtils.isEmpty(primaryApplicant.getBirthDate().toString())){
					if(randomQuestionsList.size() > 3 && randomQuestionsList.get(2).getCode()==Code.DOB){
						randomQuestionsList.get(2).setAnswer(this.getFormattedDOB(primaryApplicant.getBirthDate()));
					}
				}
				
			}else{
					randomQuestionsList.add(new Question(Code.HOUSEHOLDSIZE, DataType.INT, null));
					randomQuestionsList.add(new Question(Code.GENDER, DataType.STRING, null));
					randomQuestionsList.add(new Question(Code.SSN, DataType.STRING, null));
					randomQuestionsList.add(new Question(Code.ZIPCODE, DataType.STRING, null));
					randomQuestionsList.add(new Question(Code.COUNTY, DataType.STRING, null));
				}
			}
		}
	
	private String getFormattedDOB(Date dob){
		String DATE_FORMAT_NOW = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String stringDate = sdf.format(dob );
		return stringDate;
	}
	
	private Location getLocation(SsapApplicant ssapApplicant) {
		Location location = null;
		if (ssapApplicant.getOtherLocationId() != null) {
			location = iLocationRepository.findOne(ssapApplicant
					.getOtherLocationId().intValue());
		}
		return location;
	}
	
	private void addRandomQuestions(List<Question> questionList,List<Question> randomQuestionsList){
		if(!randomQuestionsList.isEmpty()){
			Random random = new Random();
			try{
				if(randomQuestionsList.size() >= NO_OF_RANDOM_QS){
					for (int idx = 0; idx < NO_OF_RANDOM_QS; idx++) {
						int randomInt = -1;
						do{
							randomInt = getRandomInteger(0, randomQuestionsList.size(), random);
						}
						while(questionList.contains(randomQuestionsList.get(randomInt)));
						questionList.add(randomQuestionsList.get(randomInt));
					}
				}
			} catch (Exception ex) {
				LOGGER.error("ERR: WHILE GENERATING RANDOM QST LST: ", ex);
			}
		}
		
	}
	
	private Integer getRandomInteger(int aStart, int aEnd, Random aRandom) {
		if (aStart > aEnd) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		}
		long range = (long) aEnd - (long) aStart;
		long fraction = (long) (range * aRandom.nextDouble());
		int randomNumber = (int) (fraction + aStart);
		return randomNumber;
	}
	
	
	public boolean validateResetPasswordQuestions(int accountId, Questions questions){
		List<Question> dbQuestionsList = null;
		List<Question> questionsList = null;
		Map<Question.Code, Question> dbQuestionsMap = new HashMap<Question.Code, Question>();
		boolean isValid = true;
		boolean validateFlag = true;
		AccountUser accountUser = userService.findById(accountId);
		if(accountUser != null){
			Household household = getHouseholdByUser(accountUser);
			if(household != null){
				dbQuestionsList = this.createFixedQuestionAnswers(household, validateFlag);
				this.addApplicantLevelQuestions(household, dbQuestionsList, validateFlag);
				for(Question question: dbQuestionsList){
					dbQuestionsMap.put(question.getCode(), question);
				}
			}else{
				isValid = false;
			}
		}else{
			isValid = false;
		}
		if(questions != null){
			questionsList = questions.getQuestionList();
			if(questionsList!=null && !questionsList.isEmpty()){
				if(dbQuestionsMap != null && !dbQuestionsMap.isEmpty()){
					for(Question question: questionsList){
						if(!StringUtils.isEmpty(question.getAnswer()) && question.getCode() == Question.Code.GENDER){
							if(!dbQuestionsMap.get(question.getCode()).getAnswer().toLowerCase().startsWith(question.getAnswer().toLowerCase())){
								isValid = false;
								break;
							}
						}else{
							if(StringUtils.isEmpty(question.getAnswer()) || !question.getAnswer().trim().equalsIgnoreCase(dbQuestionsMap.get(question.getCode()).getAnswer())){
								isValid = false;
								break;
							}
						}
					}
				}
				
			}
		}else{
			isValid = false;
		}
		
		return isValid;
	}
	
	public String generateResetLink(Integer accountId, String channelType){
		String resetUrl = null;
		AccountUser user = userService.findById(accountId);
		if(user != null){
			String randomToken = GhixEncryptorUtil.generateUUIDHexString(THIRTY_TWO);
			user = userService.savePasswordRecoveryDetails(user.getEmail(), randomToken);
			resetUrl = new StringBuilder(GhixEndPoints.GHIXWEB_SERVICE_URL).append(RESET_CONTROLLER_URL).append(user.getPasswordRecoveryToken()).toString();
			//Auditing reset link application event
			logAccountHistory(APPEVENT_ACCOUNT, APP_RESET_EVENT_TYPE, user, channelType);
		}else{
			throw new DataNotFoundException("Account not found");
		}
		return resetUrl;
	}
	
	private void logAccountHistory(String eventName, String eventType,AccountUser user, String channelType){
	       
		try {

			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			
			Household household = getHouseholdByUser(user);
			
			AccountUser exadminUser = userService.findByEmail(EXADMIN_USERNAME);
			
			EventInfoDto eventInfoDto = new EventInfoDto();
			
			Map<String, String> mapEventParam = new HashMap<>();
			mapEventParam.put(CHANNEL_TYPE, channelType);
			
			if(eventType.equalsIgnoreCase(APP_RESET_EVENT_TYPE)){
				mapEventParam.put(SENT_BY_KEY, SENT_BY_LABEL);
			}else{
				mapEventParam.put(LOCKED_BY_KEY, LOCKED_BY_LABEL);
			}
			
			if(null != household){
				eventInfoDto.setModuleId(household.getId());	
			}
			
			eventInfoDto.setModuleName("INDIVIDUAL");
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam, exadminUser);
			
		} catch(Exception e){
			LOGGER.error("Exception occured while log Account event"+e.getMessage(), e);
		}
	}
	
	public boolean lockAccount(Integer accountId, String channelType){
		//Send confirmed as 0 to lock the account
		userService.changeUserStatus(0, accountId);
		AccountUser user = userService.findById(accountId);
		if(user !=null && user.getConfirmed()==0){
			//Auditing lock account application event
			logAccountHistory(APPEVENT_ACCOUNT, APP_LOCK_EVENT_TYPE, user, channelType);
			return true;
		}
		return false;
	}
	
}
