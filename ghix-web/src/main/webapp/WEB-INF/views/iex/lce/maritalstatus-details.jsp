<script type="text/ng-template" id="maritalstatus-details.jsp">
  <div class="gutter10" ng-controller="maritalStatusDetail" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">

    <h3>{{eventForSpouseOrDependent}} <spring:message code="label.lce.householdinformation"/></h3>

    <form class="form-horizontal" id="maritalStatusDetails" name="maritalStatusDetails" novalidate>

    	<!-- First Name -->
      <div class="control-group">
        <label class="control-label" for="firstName">
          <spring:message code="label.lce.firstname"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
        </label>
        <div class="controls">
          <input type="text" class="span6" name="firstName" id="firstName" ng-model="householdMember.name.firstName" ng-pattern='/^[a-zA-Z]{1,45}$/' required placeholder='<spring:message code="label.lce.firstname"/>'>  <!-- ng-model="" -->
        </div>
        <div class="controls">
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.firstName.$error.required">
            <spring:message code="label.lce.pleaseenterafirstname"/>
          </div>
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.firstName.$error.pattern">
            <spring:message code="label.lce.entervalidfirstname"/>
          </div>
        </div>
      </div>

      <!-- Middle Name -->
      <div class="control-group">
        <label class="control-label" for="middleName">
          <span class="margin10-r">
            <spring:message code="label.lce.middlename"/>
          </span>
        </label>
        <div class="controls">
          <input type="text" id="middleName" name="middleName" class="span6" ng-pattern='/^[a-zA-Z]{1,45}$/' ng-model="householdMember.name.middleName" placeholder='<spring:message code="label.lce.middlename"/>'>
        </div>
        <div class="controls">
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.middleName.$error.pattern">
            <spring:message code="label.lce.entervalidmiddlename"/>
          </div>
        </div>
      </div>

      <!-- Last Name -->
      <div class="control-group">
        <label class="control-label" for="lastName">
          <spring:message code="label.lce.lastname"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
        </label>
        <div class="controls">
          <input type="text" id="lastName" class="span6" ng-maxlength="45" ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" required name="lastName" ng-model="householdMember.name.lastName" placeholder='<spring:message code="label.lce.lastname"/>'>
        </div>
        <div class="controls">
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.lastName.$error.pattern">
            <spring:message code="label.lce.entervalidlastname"/>
          </div>
			<div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.lastName.$error.maxlength">
            <spring:message code="label.lce.entervalidlastname"/>
          </div>
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.lastName.$error.required">
            <spring:message code="label.lce.pleaseenteralastname"/>
          </div>
        </div>

      <!-- Suffix -->
      <div class="control-group margin20-t">
        <label class="control-label" for="suffixOnSSNCard">
          <spring:message code="label.lce.suffix"/>
        </label>
        <div class="controls">
          <select class="span6" id="suffixOnSSNCard" name="suffixOnSSNCard" ng-model="householdMember.name.suffix" ng-options="s for s in suffix" >
            <option value=""><spring:message code="label.lce.suffix"/></option>
          </select>
        </div>
      </div>

      <!-- Date of Birth -->
      <div class="control-group margin20-t">
        <label class="control-label" for="dateOfBirth">
          <spring:message code="label.lce.dob"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
        </label>
        <div class="controls">
           <!-- Dummy Control for BIRTH event -->	
		  <div  class="input-append"   ng-if="(eventInfo.eventSubCategory ==='BIRTH')"   >
			<label for="dummyBirthDate" class="aria-hidden">dummy birthdate</label>
            <input id="dummyBirthDate" type="text"   class="span12" ng-model="dummyBirthDate" disabled />
            <span class="add-on">
              <i class="icon-calendar"></i>
            </span>
          </div>
               <!--Control for events other than birth-->
          <div id="date" class="input-append" name="dateOfBirthDiv" ng-model="householdMember.dateOfBirth" ng-if="!(eventInfo.eventSubCategory ==='BIRTH')" datetimez required check-after="0" check-before="1" dobrange="yes" >
            <input type="text" id="dateOfBirth" name="dateOfBirth" date-formatter date="householdMember.dateOfBirth" class="span12"  placeholder="MM/DD/YYYY"></input>
            <span class="add-on">
              <i class="icon-calendar"></i>
            </span>
          </div>
        </div>
        <div class="controls">
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.dateOfBirthDiv.$error.pattern">
            <spring:message code="label.lce.entervalidedob"/>
          </div>
           <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.dateOfBirthDiv.$error.required">
            <spring:message code="label.lce.pleaseenteradob"/>
          </div>
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.dateOfBirthDiv.$error.checkAfter">
            <spring:message code="lce.alertfuturedate"/>
          </div>
			<div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.dateOfBirthDiv.$error.checkBefore">
            	<spring:message code="label.lce.birthdatevalidaiton"/>
          </div>
		</div>
      </div>

      <!-- Email  -->
      <%--div class="control-group margin20-t">
        <label class="control-label" for="emailAddess">
          <spring:message code="ssap.page26.emailAddress"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
        </label>
        <div class="controls">
          <input type="text" class="span6" required  ng-pattern='/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/' name="emailAddess" id="emailAddess" ng-model="householdMember.householdContact.contactPreferences.emailAddress" placeholder='<spring:message code="ssap.page26.emailAddress"/>'>
        </div>
        <div class="controls">
          <span for="emailAddess" class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.emailAddess.$error.required ">
            <spring:message code="label.lce.pleaseenteremail"/>
          </span>
          <span for="emailAddess" class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.emailAddess.$error.pattern">
            <spring:message code="label.lce.entervalidemail"/>
          </span>
        </div>
      </div--%>

  		<!-- Applying for Health Care -->
  		<div class="control-group margin20-t">
        <label class="control-label" for="areyouseekingcoverage">
          <input type="hidden" id="areyouseekingcoverage">
          <spring:message code="label.lce.is"/> <strong>{{householdMember.name.firstName| capitalize:true}} {{householdMember.name.middleName| capitalize:true}} {{householdMember.name.lastName| capitalize:true}}</strong>
          <spring:message code="label.lce.areyouseekingcoverage"/>
        </label>
        <div class="controls">
          <label class="radio inline" for="applyingforhealthcaret">
            <input type="radio" id="applyingforhealthcaret" name="applying" ng-value="true" ng-model="householdMember.applyingForCoverageIndicator">
            <spring:message code="label.lce.yes"/>
          </label>
          <label class="radio inline" for="applyingforhealthcaref">
            <input type="radio" id="applyingforhealthcaref"name="applying"  ng-value="false" ng-model="householdMember.applyingForCoverageIndicator">
            <spring:message code="label.lce.no"/>
          </label>
        </div>
      </div>

      <!-- Sex -->
      <div class="control-group margin20-t">
        <label class="control-label" for="sexgender">
          <input type="hidden" id="sexgender">
          <spring:message code="label.lce.sex"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
        </label>
        <div class="controls">
  	      <label class="radio inline" for="female">
  	        <input type="radio" id="female" name="gender" value="female" ng-model="householdMember.gender" required/>
  	        <spring:message code="label.lce.female"/>
  	      </label>
  	      <label class="radio inline" for="male">
  	      	<input type="radio" id="male" name="gender" value="male" ng-model="householdMember.gender" required />
						<spring:message code="label.lce.male"/>
  	      </label>
        </div>
        <div class="controls">
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.gender.$error.required">
            <spring:message code="label.lce.pleaseselectagender"/>
          </div>
        </div>
      </div>

      <!-- PREGNANT? -->
      <%--div class="control-group margin20-t" ng-if="householdMember.gender=='female'">
        <label class="control-label" for="hidIsPregnant">
          <spring:message code="label.lce.is"/>
          <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong><br/>
          <spring:message code="label.lce.pregnant"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
		  <input type="hidden" id="hidIsPregnant" />
        </label>
        <div class="controls">
          <div class="margin10-t">
            <label class="radio inline" for="pregnanty">
            	<input type="radio" id="pregnanty" name="pregnant" ng-value="true" ng-model="householdMember.specialCircumstances.pregnantIndicator" ng-required="householdMember.gender=='female'"> <spring:message code="label.lce.yes"/>
            </label>
            <label class="radio inline" for="pregnantn">
            	<input type="radio" id="pregnantn" name="pregnant" ng-value="false" ng-model="householdMember.specialCircumstances.pregnantIndicator" ng-required="householdMember.gender=='female'"> <spring:message code="label.lce.no"/>
            </label>
          </div>
        </div>
        <div class="controls">
          <div class="validation-error-text-container" ng-if="submitted && maritalStatusDetails.pregnant.$error.required">
            <spring:message code="label.lce.pleaseselectif"/>
            <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
            <spring:message code="label.lce.ispregnant"/>
          </div>
        </div>
      </div--%>
    </form>


    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a role="button" tabindex="0" class="btn btn-secondary" ng-click="removeHouseholdIfRequired();back();"><spring:message code="label.lce.back"/></a>
      <a role="button" tabindex="0" ng-if="maritalStatusDetails.$valid" class="btn btn-primary" ng-click="next();"><spring:message code="label.lce.next"/></a>
      <a role="button" tabindex="0" ng-if="!maritalStatusDetails.$valid" class="btn btn-primary" ng-click="$parent.submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>

<script>
$(document).ready(function(){
	$('#dateOfBirth').inputmask({"mask": "99/99/9999"});
});
</script>