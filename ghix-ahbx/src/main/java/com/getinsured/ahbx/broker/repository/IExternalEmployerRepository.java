package com.getinsured.ahbx.broker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ExternalEmployer;

/**
 * Spring-JPA repository that encapsulates methods that interact with the ExternalEmployer table
 */
public interface IExternalEmployerRepository extends JpaRepository<ExternalEmployer, Long> {

	/**
	 * Fetches the external employer record against given employer case id(same as external employer id)
	 * 
	 * @param extemployerid 	the external employer id
	 * @return the external employer record against the employer case id(same as external employer id)
	 */
	@Query("FROM ExternalEmployer as an where an.id = :extemployerid")
	ExternalEmployer getExternalEmployer(@Param("extemployerid") long extemployerid);
}