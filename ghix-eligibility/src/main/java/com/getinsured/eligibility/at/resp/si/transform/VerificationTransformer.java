package com.getinsured.eligibility.at.resp.si.transform;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.eligibility.at.resp.si.dto.Verification;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateRangeType;

public final class VerificationTransformer {

	private static final String DIFFERENT_TYPE_OF_OBJECT_FOUND = "Different type of object found in extractVerifications. Type should be VerificationMetadataType. Got - ";
	private static final String UNKNOWN = "UNKNOWN";

	private static final Logger LOGGER = Logger.getLogger(VerificationTransformer.class);

	private VerificationTransformer(){}

	public static List<Verification> extractVerifications(List<Object> objectList) {

		List<Verification> verificationList = new ArrayList<>();
		if (objectList != null){
			for (Object object : objectList) {

				if (object instanceof VerificationMetadataType){
					VerificationMetadataType verificationMetadataType = (VerificationMetadataType) object;
					Verification verification = extractVerification(verificationMetadataType);

					if (verification != null){
						verificationList.add(verification);
					}
				} else {
					LOGGER.error(DIFFERENT_TYPE_OF_OBJECT_FOUND + object.getClass());
					throw new GIRuntimeException(DIFFERENT_TYPE_OF_OBJECT_FOUND + object.getClass());
				}

			}
		}
		return verificationList;
	}

	private static Verification extractVerification(VerificationMetadataType verifMetadata) {

		Verification verification = new Verification();
		// type
		String type = null;
		if (!verifMetadata.getVerificationCategoryCode().isEmpty()) {
			VerificationCategoryCodeType verifCategoryCode = verifMetadata.getVerificationCategoryCode().get(0);
			type = verifCategoryCode.getValue().toString();
		}

		if (StringUtils.isEmpty(type)){
			return null;
		}
		verification.setType(type);

		// status, startDate and endDate
		VerificationStatusType verifStatusType = verifMetadata.getVerificationStatus();
		Date startDate = null, endDate = null;
		if (verifStatusType != null) {
			DateRangeType dateRange = verifStatusType.getStatusValidDateRange();

			if (dateRange != null){
				startDate = TransformerHelper.extractDate(dateRange.getStartDate());
				endDate = TransformerHelper.extractDate(dateRange.getEndDate());
			}

			String status = verifStatusType.getVerificationStatusCode() != null ? verifStatusType.getVerificationStatusCode().getValue() : UNKNOWN;
			verification.setStatus(status);
		}
		verification.setStartDate(startDate);
		verification.setEndDate(endDate);

		// set status based on type..
		String status = getStatus(type, verifMetadata);
		verification.setStatus(status);

		// source
		String source = null;
		if ( null != verifMetadata.getVerificationRequestingSystem() && null != verifMetadata.getVerificationRequestingSystem().getInformationExchangeSystemCategoryCode()
				&& null != verifMetadata.getVerificationRequestingSystem().getInformationExchangeSystemCategoryCode().getValue()){
			source = verifMetadata.getVerificationRequestingSystem().getInformationExchangeSystemCategoryCode().getValue().toString();
		}
		verification.setSource(source);

		return verification;
	}

	private static String getStatus(String type, VerificationMetadataType verifMetadata) {

		String status = "NOT_VERIFIED";

		switch (type) {
			case "SSN":
				status = verifMetadata.getVerificationIndicator() != null ? verifMetadata.getVerificationIndicator().isValue() == true ? "VERIFIED": "NOT_VERIFIED" : "NOT_VERIFIED";
				break;
			case "CITIZENSHIP":
				status = verifMetadata.getVerificationIndicator() != null ? verifMetadata.getVerificationIndicator().isValue() == true ? "VERIFIED": "NOT_VERIFIED" : "NOT_VERIFIED";
				break;
			case "ELIGIBLE_IMMIGRATION_STATUS":
				status = verifMetadata.getVerificationIndicator() != null ? verifMetadata.getVerificationIndicator().isValue() == true ? "VERIFIED": "NOT_VERIFIED" : "NOT_VERIFIED";
				break;
			case "ANNUAL_INCOME":
				break;
			case "CURRENT_INCOME":
				status = verifMetadata.getVerificationIndicator() != null ? verifMetadata.getVerificationIndicator().isValue() == true ? "VERIFIED": "NOT_VERIFIED" : "NOT_VERIFIED";
				break;
			case "INCARCERATION_STATUS":
				break;
			case "ESI_MEC":
				break;
			case "NON_ESI_MEC":
				break;
			default:
				LOGGER.warn(EligibilityConstants.INVALID_VERIFICATION_TYPE_FOUND + type);
				break;
		}

		return status;

	}
}
