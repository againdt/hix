package com.getinsured.eligibility.at.resp.si.dto;

import java.util.List;

/**
 * @author chopra_s
 * 
 */
public class ApplicationExtension {

	private int coverageYear;
	private Boolean verificationIndicator;
	private List<ApplicantDetails> extendedApplicant;

	public int getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(int coverageYear) {
		this.coverageYear = coverageYear;
	}

	public Boolean getVerificationIndicator() {
		return verificationIndicator;
	}

	public void setVerificationIndicator(Boolean verificationIndicator) {
		this.verificationIndicator = verificationIndicator;
	}

	public List<ApplicantDetails> getExtendedApplicant() {
		return extendedApplicant;
	}

	public void setExtendedApplicant(List<ApplicantDetails> extendedApplicant) {
		this.extendedApplicant = extendedApplicant;
	}
}
