<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="datasource.jsp"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>

<%@ page isELIgnored="false"%>

<div class="gutter10">	
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li>Tickets</li>
        </ul><!--page-breadcrumb ends-->
        <h1>SERFF Records</h1>
    </div>	
    
    <div class="row-fluid">
		<div class="span8 dataRecords">
            <form method="POST" action="#">
            <df:csrfToken/>
                <fieldset id="searchRecords">
                    <legend>Search Records</legend>
                    <div class="control-group">
                        <label class="required control-label">Search Name</label>
                        <div class="controls">
                            <select size="1" id="field_name" name="field_name" class="width210">
                                <option value="issuer_id">Issuer Id (only admin portal)</option>
                                <option value="issuer_name">Issuer Name(only admin portal)</option>
                                <option value="plan_id">Plan Id</option>
                                <option value="plan_name">Plan Name</option>
                                <option value="serff_track_num">SERFF Tracking number</option>
                                <option value="state_track_num">State Tracking number</option>
                                <option value="operation">Operation</option>
                            </select>
                        </div> <!-- end of controls-->
                    </div><!--control-group ends-->
                    <div class="control-group">
                        <label class="control-label">Value</label>
                        <div class="controls">
                        	<input type="text" id="field_value" name="field_value" class="width112" size="10" maxlength="100">
                        </div> <!-- end of controls-->
                    </div><!--control-group ends-->
                    <div class="control-group">
                        <label class="control-label">From</label>
                        <div class="controls">
                            <input type="text" class="width85">
                        </div> <!-- end of controls-->
                    </div><!--control-group ends-->
                    <div class="control-group">
                        <label class="control-label">To</label>
                        <div class="controls">
                            <input type="text" class="width85">
                        </div> <!-- end of controls-->
                    </div><!--control-group ends-->
                    
                    <div class="control-group margin0">
                        <div class="controls">
                           <input type="submit" title="Filter requests basis on field value" value="Search" onclick="setWhereClause('B4');" class="btn btn-primary span12">
                        </div> <!-- end of controls-->
                    </div>
                </fieldset>
            </form>
        </div><!--searchRecords ends-->
        <div class="or-Text">OR</div>
        <div class="span3 dataRecords">
        	<form method="POST" action="#">
                <fieldset>
                <df:csrfToken/>
                    <legend>Show Records</legend>
                    <div class="control-group" >
	                    <input type="submit" title="View requests made in last 1 hours" value="Last 1 Hours" onclick="setWhereClause('B3');" class="btn btn-primary span12">
                        <input type="submit" title="View requests made in last 12 hours" value="Last 12 Hours" onclick="setWhereClause('B2');" class="btn btn-primary span12">
                        <input type="submit" title="View requests made in last 24 hours" value="Last 24 Hours" onclick="setWhereClause('B1');" class="btn btn-primary span12">
                    </div><!--control-group ends-->
                </fieldset>
            </form>
        </div><!--show records ends-->
        
        <input id="whereClause" name="whereClause" type="hidden">
		<%
			String whereClause = request.getParameter("whereClause");
			if (whereClause == null) {
				whereClause = "start_time <= sysdate";
			}
		%>
        
		<sql:query dataSource="${jspDataSource}" var="result1">
			select serff_req_id,issuer_id,issuer_name,hios_product_id,plan_id, plan_name,serff_track_num,state_track_num,operation,process_ip, 
			to_char(start_time,'DD-MON-YY HH24:MM:SS') as start1,
			to_char(end_time,'DD-MON-YY HH24:MM:SS') as end1,
			'<a href="#" id="hide'||serff_req_id||'" class="hide">HIDE</a><a href="#" id="show'||serff_req_id||'">SHOW</a>' as list1,
			'<a href="#" id="hide'||serff_req_id||'" class="hide">HIDE</a><a href="#" id="show'||serff_req_id||'">SHOW</a>' as request_xml,
			'<a href="#" id="hide'||serff_req_id||'" class="hide">HIDE</a><a href="#" id="show'||serff_req_id||'">SHOW</a>' as response_xml,
			decode (request_status,'F','Failed','S','Success','Unknown') request_status, 
			remarks, attachments_list,
			decode (request_state, 'B','Begin','E','Ended','P','In-Progress','W','Waiting') request_state,
			request_state_desc
			from serff_plan_mgmt 
			where <%=whereClause%>
			order by serff_req_id desc
		</sql:query>
        
        <h1 class="clearfix">Results <!-- <small>(Total: 135)</small> --></h1>
        <div id="resultTable">
			<display:table name="${result1.rows}" requestURI="/serffAdmin" id="table1" class="table table-striped" pagesize="5">
				<%-- <display:column property="serff_req_id" title="ID" sortable="false" />
				<display:column property="issuer_id" title="Issuer ID" sortable="false" /> --%>
				<display:column property="issuer_name" title="Issuer Name" sortable="false" />
				<display:column property="hios_product_id" title="HIOS" sortable="false" />
				<display:column property="plan_id" title="Plan ID" sortable="false" />
				<display:column property="plan_name" title="Plan Name" sortable="false" />
				<display:column property="serff_track_num" title="SERFF ID" sortable="false" />
				<display:column property="state_track_num" title="State ID" sortable="false" />
				<%-- <display:column property="operation" title="Operation" sortable="false" />
				<display:column property="process_ip" title="Process IP" sortable="false" />
				<display:column property="start1" title="Start" sortable="false" />
				<display:column property="end1" title="End" sortable="false" /> --%>
				<display:column property="list1" title="Attachments" sortable="false" />
				<display:column property="request_xml" title="Request" sortable="false" />
				<display:column property="response_xml" title="Response" sortable="false" />
				<%-- <display:column property="request_status" title="Status" sortable="false" />
				<display:column property="remarks" title="Remarks" sortable="false" />
				<display:column property="attachments_list" title="attachments_list" sortable="false" />
				<display:column property="request_state" title="State" sortable="false" />
				<display:column property="request_state_desc" title="State Desc" sortable="false"/> --%>
			</display:table>
			
        	<%-- <table class="table table-striped">
                <thead>
                    <tr class="graydrkbg">
                    	<th>Issuer Name</th>
                        <th>HIOS</th>
                        <th>Plan ID</th>
                        <th>Plan Name</th>
                        <th>SERFF ID</th>
                        <th>State ID</th>
                        <th>Attachments</th>
                        <th>Request</th>
                        <th>Response</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                    	<td>Company</td>
                        <td>48</td>
                        <td>123456</td>
                        <td>PPO saver</td>
                        <td>113112</td>
                        <td>798789</td>
                        <td><a href="#" id="hide1">HIDE</a><a href="#" id="show1">SHOW</a></td>
                        <td><a href="#">SHOW</a></td>
                        <td><a href="#">SHOW</a></td>
                    </tr>
                </tbody>
            </table> --%>
        </div><!--resultTable ends-->
        <%-- <div class="center"> 
            <div class="pagination"> 
                <ul> 
                    <li class="active"><a href="#">1</a></li>
                    <li class=""><a href="#">2</a></li>
                    <li class=""><a href="#">3</a></li>
                    <li class=""><a href="#">4</a></li>
                    <li class=""><a href="#">5</a></li>
	            </ul>
            </div><!--pagination ends-->
        </div><!--center ends--> --%>
	</div><!-- row-fluid ends -->
</div> <!-- gutter10 ends -->


<script type="text/javascript">
	$(function() {
		
		/* $('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx+'/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true
			});
		}); */
		
		$('#show1').click(function(){
			if($('.attachment').length === 0){
				$(this).hide();
				$('#hide1').show().parent().addClass('attach-active');
				$(this).parents('tr').after('<tr class="attachments" id="attachment1"><td colspan="9"><table class="table table-striped"><thead><tr class="graydrkbg"><th>Id</th><th>ECM ID</th><th>File Name</th><th>Size</th><th>Attachments</th></tr></thead><tbody><tr><td>23456</td><td>34567890</td><td>example.xml</td><td>3456kb</td><td><a href="#" class="fileOpen fileLink">OPEN</a><a href="#" class="fileDownload fileLink">DOWNLOAD</a></td></tr></tbody></table></td></tr>');
			}
			
		});
		
		$('#hide1').click(function(){
			$(this).hide().parent().removeClass('attach-active');
			$('#show1').show();
			$('#attachment1').remove();
		});
	});
	
	function validateDate(e) {
		
		var today=new Date();
		
		var dobParts = document.getElementById('date').value.split('/');
		
		if(dobParts == null || dobParts.length == 0){
			alert('Please enter a valid date in MM/DD/YYYY format');
			return false;
		}
		
		var dob_mm = dobParts[0];  
		var dob_dd = dobParts[1];  
		var dob_yy = dobParts[2]; 
		
	    var birthDate=new Date();
	    birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	    
	    if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) {
	    	alert('Please enter a valid date in MM/DD/YYYY format');
	    	return false; 
	    }
	    
	    if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) {
	    	alert('Please enter a valid date in MM/DD/YYYY format');
	    	return false; 
	    }
	    
	    if(today.getTime() < birthDate.getTime()){
	    	alert('Please enter a valid date in MM/DD/YYYY format');
	    	return false; 
	    }
	    setWhereClause('B6');
		
	}
	
	function setWhereClause(type) {
		
		var whereClause = '';
		var filterStatement = '';
		
		switch (type) {
			case 'B1' : 
				whereClause = 'start_time between (sysdate-1) and sysdate';
				filterStatement = ' in last 24 hours';
				break;
			case 'B2' : 
				whereClause = 'start_time between (sysdate-1/2) and sysdate';
				filterStatement = ' in last 12 hours';
				break;
			case 'B3' : 
				whereClause = 'start_time between (sysdate-1/24) and sysdate';
				filterStatement = ' in last 1 hour';
				break;
			case 'B4' : 
				var fieldName = document.getElementById('field_name').value;
				var fieldValue = document.getElementById('field_value').value;
				whereClause = "lower("+fieldName + ") like lower('%" + fieldValue + "%')";
				filterStatement = ' with ' + fieldName + " = " + fieldValue;
				break;
			case 'B5' :
				var statuses = document.getElementById('status');
				var status = statuses.options[statuses.selectedIndex].value;
				whereClause = "request_status = '" + status + "'";
				
				if(status == 'S') {
					filterStatement = ' which were successful';
				}
				
				if(status == 'F') {
					filterStatement = ' which have failed';
				}
				break;
			case 'B6' :
				var date = document.getElementById('date').value;
				whereClause = "start_time between to_timestamp('"+date+" 00:00:00','mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+date+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
				filterStatement = 'made on ' +  date;
				break;
		}
		
		document.getElementById('whereClause').value = whereClause;
		document.getElementById('filterStatement').value = filterStatement;
	}
</script>