{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "insurance": {
              "insurancePolicyExpirationDate": "12/31/2014",
              "insurancePolicyEffectiveDate": "01/25/2014"
            },
            "householdContact": {
              "homeAddress": {
                "state": "AZ"
              }
            },
            "organization": [
              {
                "organizationCode": ""
              }
            ],
            "dateOfBirth": "01/25/1967",
            "socialSecurityCard": {
              "socialSecurityNumber": "222222311"
            },
            "name": {
              "middleName": "",
              "lastName": "Smith",
              "firstName": "John",
              "personNameSuffixText": ""
            },
            "gender": "M"
          },
          {
            "insurance": {
              "insurancePolicyExpirationDate": "12/31/2014",
              "insurancePolicyEffectiveDate": "01/25/2014"
            },
            "householdContact": {
              "homeAddress": {
                "state": "MD"
              }
            },
            "organization": [
              {
                "organizationCode": ""
              }
            ],
            "dateOfBirth": "01/25/1967",
            "socialSecurityCard": {
              "socialSecurityNumber": "222222312"
            },
            "name": {
              "middleName": "",
              "lastName": "Fox",
              "firstName": "John",
              "personNameSuffixText": ""
            },
            "gender": "M"
          }
        ]
      }
    ]
  }
}