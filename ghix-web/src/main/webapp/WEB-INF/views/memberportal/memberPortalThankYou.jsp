	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<div id="concierge-thankYou" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="concierge-thankYou" aria-hidden="true">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 id="myModalLabel">GetInsured Concierge</h3>
                </div>
                
                <div class="modal-body">
                	<form class="form-horizontal" id="frmConciergeHelpOptions" name="getinsuredconcierge" action="/hix/memberportal/conciergeDetail" method="POST">
                	<label>Thanks for contacting us!</label>
                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                <label>We'll get back to you as soon as possible (typically within 2 business</label>	
	                
                     <label>days), and do our best to help.</label>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     
                      <label>If you need immediate help, please call us at 
                      <c:if test="${sessionScope.ClickTrackIVRNumber==null}">
                               (866) 602 8466  
                                       
                              </c:if>
                           <c:if test="${sessionScope.ClickTrackIVRNumber!=null }">
                        
                                   (<c:out
                                         value="${fn:substring(sessionScope.ClickTrackIVRNumber,0,3)}" />)&nbsp;<c:out
                                         value="${fn:substring(sessionScope.ClickTrackIVRNumber,3,6)}" />&nbsp;<c:out
                                         value="${fn:substring(sessionScope.ClickTrackIVRNumber,6,10)}" />
                           </c:if>
 
                      or start</label>
					  <label>an online chat with a Getinsured Concierge representative.</label>
						<div class="modal-footer">
						
                		<button class="btn pull-right" data-dismiss="modal" aria-hidden="true">Close</button>						
              			
              	    </div>
					</form>
				</div><!-- /modal-body -->

				
              	           
	</div>
	
	<script>
		function fun() {
			var subject = $("#subject").val();
			
			$("#frmConciergeHelpOptions").submit();
		}
	</script>
