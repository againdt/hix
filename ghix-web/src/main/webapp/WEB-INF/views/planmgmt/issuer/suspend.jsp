<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/preserveImgAspectRatio.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>
	<div class="row-fluid">
		<div class="gutter10">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
    	   	<!-- start of side bar -->
    	   	<!-- <div class="header">
            </div> -->
            <!-- end of side bar -->
		</div>
		<!-- end of span3 -->
		<div class="span9" id="rightpanel">
			<div class="gutter10">
				<h3><spring:message code='label.suspendedMsg'/></h3> 
			</div>	
		</div>
	</div><!--  end of span9 -->
