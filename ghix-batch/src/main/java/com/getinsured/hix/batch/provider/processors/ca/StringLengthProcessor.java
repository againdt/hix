package com.getinsured.hix.batch.provider.processors.ca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;


public class StringLengthProcessor implements ProviderDataFieldProcessor{

	private ValidationContext context;
	private int index;
	private Logger logger = LoggerFactory.getLogger(StringLengthProcessor.class);
	public StringLengthProcessor() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object process(Object obj) throws ProviderValidationException {
		String s;
		int len;
		int sLen;
		String maxObj = null;
		String minObj = null;
		try{
			s = (String)obj;
			sLen = s.length();
		}catch (ClassCastException ce){
			throw new ProviderValidationException("Invalid input for length validation", ce);
		}
		String length = (String)context.getNamedConstraintField("length");
		//First check if exact length is expected
		if(length != null){
			len = Integer.valueOf(length);
			if(sLen != len){
				throw new ProviderValidationException("Value length validation failed, Expected:"+len+" Found:"+s.length());
			}
		}else{
		// Now check if range validation is expected
			maxObj = context.getNamedConstraintField("max");
			minObj = context.getNamedConstraintField("min");
			try{
				if(maxObj != null){
					int max = Integer.valueOf(maxObj);
					if(sLen > max){
						throw new ProviderValidationException ("validation failed for maximum value [ max:"+max+"] found: "+sLen);
					}
				}
				if(minObj != null){
					int min = Integer.valueOf(minObj);
					if(sLen < min){
						throw new ProviderValidationException ("validation failed for minimum value [min:"+min+"] found: "+sLen);
					}
				}
			}catch(NumberFormatException ne){
				throw new ProviderValidationException("Illegal validation context", ne);
			}
		}
		return s;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
			this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
