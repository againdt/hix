package com.getinsured.hix.tkm.service;

import java.util.List;

import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.tkm.tkmException.TKMException;

public interface TkmWorkflowsService {
	
	List<TkmWorkflows> getTicketTypeList() throws TKMException;
	
	List<String> getDistinctTicketType() throws TKMException;
	
	TkmWorkflows getTicketWorkflowsByCriteria (String type, String ticketCategory);
	
	TkmWorkflows getTicketWorkflowsByCriteria (String ticketType);
	
	List<TkmWorkflows> getActiveTicketTypeList() throws TKMException;
}
