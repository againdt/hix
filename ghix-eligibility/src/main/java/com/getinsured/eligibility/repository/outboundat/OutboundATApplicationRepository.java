package com.getinsured.eligibility.repository.outboundat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.ssap.model.OutboundATApplication;

@Repository
public interface OutboundATApplicationRepository extends JpaRepository<OutboundATApplication, Long> {

	List<OutboundATApplication> findBySsapApplicationId(long ssapApplicationId);

}
