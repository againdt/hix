package com.getinsured.hix.agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.agency.Agency;

@Repository
public interface IAgencyRepository extends RevisionRepository<Agency, Integer, Integer>, JpaRepository<Agency, Integer> {

	Agency findById(Long agencyId);
	
	@Query("select a from Agency a,Broker b where a.id = b.agencyId and b.id = :brokerId")
	Agency findByBrokerId(@Param("brokerId")int brokerId); 
	
	
	@Query("select count(*) from Agency a where a.federalTaxId = :federalTaxId")
	int findByFederalTaxId(@Param("federalTaxId")String federalTaxId);
	
	@Query("select a from Agency a,AgencyAssistant aa where a.id = aa.agency.id and aa.id = :agencyAssistantId")
	Agency findByAgencyAssistantId(@Param("agencyAssistantId")long agencyAssistantId);


}