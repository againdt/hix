package com.getinsured.hix.batch.platform.task;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.platform.security.service.UserService;

public class DisableInactiveAccountsTask implements Tasklet {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DisableInactiveAccountsTask.class);
	//private static final String DATASOURCE ="java:/jdbc/ghixBatchDS";
	private UserService userService;
	private String taskParameter = null;
	/*
	 * HIX-20720 : Disable Inactive Accounts after 180 days
	 * @author :James Liu 04-28-2014
	 * 
	 */
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws Exception {
		//Connection con = null;
		LOGGER.info("Batch job DisableInactiveAccounts started on :-"+new Timestamp(System.currentTimeMillis()));
		
		LOGGER.info("Disable Inactive Accounts Task started ");
		try {
			List<String> expiredUsers = userService.disableInactiveAccounts();
		
			LOGGER.info("There are this many inactive accounts found::"+ expiredUsers.size()+" ; details follows");
			for(String currRec:expiredUsers){
				LOGGER.info("Inactive account has been disabled: " + currRec);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while executing  batch job", e);
		}
	      
		LOGGER.info("Batch job DisableInactiveAccountsTask completed on :-" +new Timestamp(System.currentTimeMillis()));
		return RepeatStatus.FINISHED;
	}

	public String getTaskParameter() {
		return taskParameter;
	}

	public void setTaskParameter(String taskParameter) {
		this.taskParameter = taskParameter;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
}
