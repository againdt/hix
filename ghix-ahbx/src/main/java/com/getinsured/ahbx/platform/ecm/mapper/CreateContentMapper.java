package com.getinsured.ahbx.platform.ecm.mapper;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;

import com.getinsured.ahbx.platform.ecm.CheckinAttachedFile;
import com.getinsured.ahbx.platform.ecm.CheckinAttachedFileResponse;
import com.getinsured.ahbx.platform.ecm.CheckinFile;
import com.getinsured.ahbx.platform.ecm.CheckinFileResponse;
import com.getinsured.ahbx.platform.ecm.ObjectFactory;
import com.getinsured.hix.model.PlatformRequest;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * CreateContentMapper class to transform
 * 		1. GHIX-Request to AHBX Request
 * 		2. AHBX Response to GHIX-Response
 *
 * @author Ekram Ali Kazi
 *
 */
public final class CreateContentMapper {

	private static final Logger LOGGER = Logger.getLogger(CreateContentMapper.class);

	private CreateContentMapper(){}

	/**
	 * Map GHIX-Request to AHBX Request
	 *
	 * @param ecmRequest
	 * @return AHBX Request object - JAXBElement<CheckinFile>
	 */
	public static JAXBElement<CheckinAttachedFile> map(PlatformRequest.ECMRequest ecmRequest) {
		// form dataMapAsXml for HashMap
		XStream xstream1 = GhixUtils.getXStreamStaxObject();
		String dataMapAsXml = xstream1.toXML(ecmRequest.getMetadata());

		ObjectFactory of = new ObjectFactory();
		CheckinAttachedFile checkinAttachedFile = of.createCheckinAttachedFile();
		checkinAttachedFile.setArg0(ecmRequest.getBytes());
		checkinAttachedFile.setDataAsXML(dataMapAsXml);

		LOGGER.info("CheckinAttachedFile request - " + checkinAttachedFile);

		return of.createCheckinAttachedFile(checkinAttachedFile);
	}

	/**
	 * Map AHBX Response to GHIX-Response
	 *
	 * @param ahbxResponse
	 * @return String - ECM Doc Id
	 */
	public static String map(JAXBElement<CheckinAttachedFileResponse> ahbxResponse) {
		return ahbxResponse.getValue().getReturn();
	}
	
	/**
	 * Map GHIX-Request to AHBX Request
	 *
	 * @param ecmRequest
	 * @return AHBX Request object - JAXBElement<CheckinFile>
	 */
	public static JAXBElement<CheckinFile> mapV1(PlatformRequest.ECMRequest ecmRequest) {
		// form dataMapAsXml for HashMap
		XStream xstream1 = GhixUtils.getXStreamStaxObject();
		String dataMapAsXml = xstream1.toXML(ecmRequest.getMetadata());

		ObjectFactory of = new ObjectFactory();
		CheckinFile checkinFile = of.createCheckinFile();
		checkinFile.setBytes(ecmRequest.getBytes());
		checkinFile.setDataAsXML(dataMapAsXml);

		LOGGER.info("CheckinFile request - " + checkinFile);

		return of.createCheckinFile(checkinFile);
	}

	/**
	 * Map AHBX Response to GHIX-Response
	 *
	 * @param ahbxResponse
	 * @return String - ECM Doc Id
	 */
	public static String mapV1(JAXBElement<CheckinFileResponse> ahbxResponse) {
		return ahbxResponse.getValue().getReturn();
	}	

}
