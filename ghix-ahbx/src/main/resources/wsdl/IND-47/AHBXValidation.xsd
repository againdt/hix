<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema" targetNamespace="http://ahbx.accenture.com/AHBXValidation"
	xmlns:tns="http://ahbx.accenture.com/AHBXValidation"
	elementFormDefault="qualified">

	<simpleType name="StringFixed10">
		<annotation>
			<documentation>
				This field is used where we required alpha numeric
				values which should be length = 10.
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="10" fixed="true" />
		</restriction>
	</simpleType>
	
	<simpleType name="LongLength10">
		<annotation>
			<documentation>
				This field is used where we required numeric values
				which have at max 10
				digits.
			</documentation>
		</annotation>
		<restriction base="long">
		    <minInclusive value="1"></minInclusive>
			<maxInclusive value="9999999999"></maxInclusive>
		</restriction>
	</simpleType>
	
	<simpleType name="AccountNumber">
		<annotation>
			<documentation>
				This type is used wherever Account Number
				element is
				required with the
				max length restriction as 20.
			</documentation>
		</annotation>
		<restriction base="integer">
			<maxInclusive value="99999999999999999999"></maxInclusive>
		</restriction>
	</simpleType>
	<simpleType name="UserType">
		<annotation>
			<documentation>
				This field is used to denote the different user types
				available in our
				application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="Broker"></enumeration>
			<enumeration value="Carrier"></enumeration>
			<enumeration value="Navigator individual">
			</enumeration>
			<enumeration value="Navigator with enrollment entity"></enumeration>
		</restriction>
	</simpleType>
	
	<simpleType name="BrokerNavUserType">
	<annotation>
			<documentation>
				This field is used to denote the different user types
				available in our
				application.
			</documentation>
		</annotation>
		<restriction base="string">
			<enumeration value="Broker"></enumeration>
			<enumeration value="Navigator individual">
			</enumeration>
			<enumeration value="Navigator with enrollment entity"></enumeration>
		</restriction>
	</simpleType>
	
	<simpleType name="PlanTierIDType">
		<annotation>
			<documentation>
				This field is used to denote the different plan Tier
				Types available in our application.
			</documentation>
		</annotation>
		<restriction base="string">
		<enumeration value="CataStrophic" />
		<enumeration value="Bronze" />
		<enumeration value="Silver" />
		<enumeration value="Silver+" />
		<enumeration value="Gold" />
		<enumeration value="Gold+" />
		<enumeration value="Platinum" />
		</restriction>
		</simpleType>

	<simpleType name="StartEndDate">
		<restriction base="string">
			<pattern value="(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((20)\d\d)">
			</pattern>
		</restriction>
	</simpleType>

	<simpleType name="StringLength50">
		<annotation>
			<documentation>
				This type is used wherever Name/Alphanumeric
				element
				required with the
				max length restriction as 50.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="50"></maxLength>
		</restriction>
	</simpleType>

	<simpleType name="Date">
		<annotation>
			<documentation>
				This type is used where ever date required.
				Padding
				zero in the left side is not mandatory for date month. (mm/dd/yyyy)
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/[\d]{4}"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="EINType">
		<annotation>
			<documentation>
				This specify the EIN format of US. (length=9)
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="[\d]{9}"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="AddressLineType">
		<annotation>
			<documentation>
				This type used to specify the limit of the each
				address line should
				follow.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="25"></maxLength>
		</restriction>
	</simpleType>

	<simpleType name="CityType">
		<annotation>
			<documentation>
				This type used to specify the limit of the each
				City
				field should
				follow.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="15"></maxLength>
		</restriction>
	</simpleType>

	<simpleType name="StateType">
		<annotation>
			<documentation>
				This type used to specify the format of the
				State field
				should follow. (2 letter)
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="2"></maxLength>
			<pattern value="[a-zA-Z]*"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="ZipCodeType">
		<annotation>
			<documentation>
				This type is used wherever zipcode
				element is required
				with the pattern as aaaaa-bbbb
				(where first 5 elements are mandatory).
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="[\d]{5}(-[\d]{4})?"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="PhoneNumberType">
		<annotation>
			<documentation>
				This type is used to specify the format of the
				phone
				number (10 digits).
			</documentation>
		</annotation>
		<restriction base="string">
			<pattern value="[\d]{10}"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="StringFixed6">
		<annotation>
			<documentation>
				This field is used where ever we required alphanumeric
				values and length of data fixed to 6 letters.
			</documentation>
		</annotation>
		<restriction base="string">
			<length value="6" fixed="true"></length>
		</restriction>
	</simpleType>

	<simpleType name="YNFlag">
		<annotation>
			<documentation>
				This Flag field is used to specify the Y/N values
				in
				both cases.
			</documentation>
		</annotation>
		<restriction base="string">
			<maxLength value="1"></maxLength>
			<pattern value="[yYnN]"></pattern>
		</restriction>
	</simpleType>

	<simpleType name="ResponseCodeType">
		<annotation>
			<documentation>
				This type used to specify the response code format
				which is yet to be
				confirmed. (Temp: it will be Integer without
				any
				restriction)
			</documentation>
		</annotation>
		<restriction base="int"></restriction>
	</simpleType>

	<simpleType name="ResponseDescriptionType">
		<annotation>
			<documentation>
				This type used to specify the response description
				format which is yet to be
				confirmed. (Temp: it will be Alphanumeric
				without any restriction)
			</documentation>
		</annotation>
	<restriction base="string"></restriction>
	</simpleType>
	
	<simpleType name="AccountType">
		<restriction base="string">
			<enumeration value="Savings"></enumeration>
			<enumeration value="savings"></enumeration>
			<enumeration value="Checking"></enumeration>
			<enumeration value="checking"></enumeration>
		</restriction>
	</simpleType>
	
	<simpleType name="StringFixed4">
			<annotation>
				<documentation>
					This field is used where ever we required alphanumeric
					values and length of data fixed to 6 letters.
				</documentation>
			</annotation>
			<restriction base="string">
				<length value="4" fixed="true"></length>
			</restriction>
		</simpleType>
	
		<simpleType name="responseValidInvalid">
			<annotation>
				<documentation>
					This type used to specify the response values as VALID or INVALID
				</documentation>
			</annotation>
			<restriction base="string">
				<enumeration value="VALID" />
				<enumeration value="INVALID" />
			</restriction>
	</simpleType>

</schema>