package com.getinsured.hix.batch.externalnotices.exceptions;

public class InvalidRecordException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidRecordException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidRecordException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidRecordException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidRecordException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
