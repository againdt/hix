package com.getinsured.eligibility.externalnotices.service;

import com.getinsured.hix.dto.externalnotices.ExternalNoticesResponseDTO;
import com.getinsured.hix.model.ExternalNotice;

public interface ExternalNoticesService {
	
	public ExternalNoticesResponseDTO getSsapApplicationDetails(Long ssapApplicationId);
	
	public void processBatchInitRecords(ExternalNotice externalNotices);
	
	public void processBatchQueuedRecords(ExternalNotice externalNotices);

}
