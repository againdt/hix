package com.getinsured.iex.hub.debug;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.springframework.ws.client.WebServiceClientException;

import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public class GIPayloadInterceptor implements ClientInterceptor {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GIPayloadInterceptor.class);

	private Document parseXmlFile(String in) {
		try {
			DocumentBuilderFactory dbf = PlatformServiceUtil.getDocumentBuilderFactoryInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(in));
			return db.parse(is);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	
	private String format(String unformattedXml) {
		try {
			final Document document = parseXmlFile(unformattedXml);

			TransformerFactory tf = TransformerFactory.newInstance();
		    Transformer transformer = tf.newTransformer();
		    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		    ByteArrayOutputStream bout = new ByteArrayOutputStream(1024);
		    transformer.transform(new DOMSource(document), 
		         new StreamResult(new OutputStreamWriter(bout, "UTF-8")));

			return bout.toString("UTF-8");
		} catch (IOException | TransformerException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private void dumpMessage(WebServiceMessage message) {
		try {
			ByteArrayOutputStream sw = new ByteArrayOutputStream();
			message.writeTo(sw);
			LOGGER.debug("\n"+this.format(sw.toString()));
		} catch (IOException e) {
			LOGGER.error(e.getMessage(),e);
		}
	}

	@Override
	public boolean handleRequest(MessageContext messageContext){
		WebServiceMessage x = messageContext.getRequest();
		LOGGER.debug("******************* Request **************************");
		this.dumpMessage(x);
		LOGGER.debug("******************* End Request **************************");
		return true;
	}

	@Override
	public boolean handleResponse(MessageContext messageContext){
		WebServiceMessage x = messageContext.getResponse();
		LOGGER.debug("******************* Response **************************");
		this.dumpMessage(x);
		LOGGER.debug("******************* End Response **************************");
		return true;
	}

	@Override
	public boolean handleFault(MessageContext messageContext){
		WebServiceMessage x = messageContext.getResponse();
		LOGGER.debug("******************* Fault Response **************************");
		this.dumpMessage(x);
		LOGGER.debug("******************* End Fault Response **************************");
		return true;
	}

	@Override
	public void afterCompletion(MessageContext arg0, Exception arg1) throws WebServiceClientException {
		// TODO Auto-generated method stub
		
	}

}
