package com.getinsured.iex.hub.nmec.wrapper;

import java.text.ParseException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.IndividualRequestType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.InsurancePolicyType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ObjectFactory;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.OrganizationCodeSimpleType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.OrganizationCodeType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.OrganizationType;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public class IndividualRequestTypeWrapper implements JSONAware {

	private IndividualRequestType individualRequestType = null;
	private ObjectFactory objectFactory = null;

	private ApplicantTypeWrapper applicantWrapper = null;
	private USStateCodeTypeWrapper locationStateUSPostalServiceCode = null;
	private OrganizationType organizationType = null;
	private InsurancePolicyType insurancePolicyType = null;

	private String insurancePolicyEffectiveDate = null;
	private String insurancePolicyExpirationDate = null;
	
	private String organizationCode = null;
	private static final Logger logger = LoggerFactory.getLogger(IndividualRequestTypeWrapper.class);
	
	public IndividualRequestTypeWrapper() {
		objectFactory = new ObjectFactory();
		this.individualRequestType = objectFactory
				.createIndividualRequestType();
		this.insurancePolicyType = objectFactory.createInsurancePolicyType();
		this.organizationType = objectFactory.createOrganizationType();
		applicantWrapper = new ApplicantTypeWrapper();
		locationStateUSPostalServiceCode = new USStateCodeTypeWrapper();
		
		individualRequestType
				.setLocationStateUSPostalServiceCode(locationStateUSPostalServiceCode
						.getSystemRepresentation());
		individualRequestType.setApplicant(applicantWrapper
				.getSystemRepresentation());
		
	}

	public IndividualRequestType getSystemRepresentation() {
		return individualRequestType;
	}

	public void setInsurancePolicyEffectiveDate(
			String insurancePolicyEffectiveDate) {
		
		if(insurancePolicyEffectiveDate == null || insurancePolicyEffectiveDate.isEmpty()){
			return;
		}
		
		try {
			XMLGregorianCalendar gc = PlatformServiceUtil.stringToXMLDate(
					insurancePolicyEffectiveDate, "MM/dd/yyyy");
			gc.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
			insurancePolicyType.setInsurancePolicyEffectiveDate(gc);
			this.insurancePolicyEffectiveDate = insurancePolicyEffectiveDate;
			this.individualRequestType.setInsurancePolicy(insurancePolicyType);
		} catch (ParseException e) {
			logger.info(e.getStackTrace().toString());
		} catch (DatatypeConfigurationException e) {
			logger.info(e.getStackTrace().toString());
		}
	}

	public String getInsurancePolicyEffectiveDate() {
		return insurancePolicyEffectiveDate;
	}

	public void setApplicantTypeWrapper(
			ApplicantTypeWrapper applicantTypeWrapper) {
		this.applicantWrapper = applicantTypeWrapper;
		individualRequestType.setApplicant(applicantWrapper
				.getSystemRepresentation());
	}

	public void setUSStateCodeTypeWrapper(
			USStateCodeTypeWrapper uSStateCodeTypeWrapper) {
		this.locationStateUSPostalServiceCode = uSStateCodeTypeWrapper;
		individualRequestType
				.setLocationStateUSPostalServiceCode(locationStateUSPostalServiceCode
						.getSystemRepresentation());
	}

	public void setInsurancePolicyExpirationDate(
			String insurancePolicyExpirationDate) {
			
		if(insurancePolicyExpirationDate == null || insurancePolicyExpirationDate.isEmpty()){
			return;
		}
		try {
			XMLGregorianCalendar gc = PlatformServiceUtil.stringToXMLDate(
					insurancePolicyExpirationDate, "MM/dd/yyyy");
			gc.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
			insurancePolicyType.setInsurancePolicyExpirationDate(gc);
			this.insurancePolicyExpirationDate = insurancePolicyExpirationDate;
			this.individualRequestType.setInsurancePolicy(insurancePolicyType);
		} catch (ParseException e) {
			logger.info(e.getStackTrace().toString());
		} catch (DatatypeConfigurationException e) {
			logger.info(e.getStackTrace().toString());
		}
	}

	public String getInsurancePolicyExpirationDate() {
		return insurancePolicyExpirationDate;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCodeSimpleType) {
		if(organizationCodeSimpleType == null || organizationCodeSimpleType.trim().length() == 0){
			return;
		}
		OrganizationCodeType organizationCodeType = objectFactory.createOrganizationCodeType();
		organizationCodeType.setValue(OrganizationCodeSimpleType
				.fromValue(organizationCodeSimpleType));
		organizationType.getOrganizationCode().add(organizationCodeType);
		individualRequestType.setOrganization(organizationType);
		this.organizationCode = organizationCodeSimpleType;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ApplicantType", applicantWrapper);
		obj.put("LocationStateUSPostalServiceCode",
				locationStateUSPostalServiceCode);
		obj.put("InsurancePolicyEffectiveDate", insurancePolicyEffectiveDate);
		obj.put("InsurancePolicyExpirationDate", insurancePolicyExpirationDate);
		JSONArray organizationCodes = new JSONArray();

		for (OrganizationCodeType data : this.organizationType.getOrganizationCode()) {
			organizationCodes.add(data.getValue().value());
		}
		obj.put("OrganizationCode", organizationCodes);
		return obj.toJSONString();
	}

}
