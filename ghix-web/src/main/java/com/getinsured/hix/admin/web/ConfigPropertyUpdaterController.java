package com.getinsured.hix.admin.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.config.ConfigValidationException;
import com.getinsured.hix.config.InvalidOperationException;
import com.getinsured.hix.config.model.ConfigPageData;
import com.getinsured.hix.model.GIAppProperties;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.repository.PropertiesRepository;
import com.getinsured.hix.platform.repository.TenantRepository;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;



@Controller
public class ConfigPropertyUpdaterController {
	private static final Logger log = LoggerFactory.getLogger(ConfigPropertyUpdaterController.class);

	private static final String RESULT_SIZE = "resultSize";
	private static final String GI_APP_PROPERTIES = "giAppProperties";
	private static final String UI_CONFIGURATION = "UI Configuration";
	private static final String GLOBAL_CONFIGURATION = "Global Configuration";
	private static final String SHOP_CONFIGURATION = "Shop Configuration";
	private static final String HEADER = "Header";
	private static final String ENROLLMENT_CONFIGURATION = "Enrollment Configuration";
	private static final String PLANSELECTION_CONFIGURATION = "Plan Selection Configuration";
	private static final String PLANMANAGEMENT_CONFIGURATION = "Plan Management Configuration";
	private static final String FINANCIAL_CONFIGURATION = "Financial Configuration";
	private static final String BROKERMANAGEMENT_CONFIGURATION = "Broker Management Configuration";
	private static final String SERFF_CONFIGURATION = "Serff Configuration";
	private static final String SECURITY_CONFIGURATION = "Security Configuration";
	private static final String HUB_CONFIGURATION = "HUB Configuration";
	private static final String IEX_CONFIGURATION = "IEX Configuration";
	private static final String ENROLLMENT_1095_CONFIGURATION = "1095 Validation Configuration";
	private static final String RENEWAL_CONFIGURATION = "Renewal Configuration";

	@Autowired PropertiesRepository propertiesRepo;
	@Autowired private UserService userService;

	/**
	 * Method that returns search results as users type on properties modification page.
	 *
	 * @param section section for which properties needs to be searched.
	 * @param term search term.
	 * @return list of properties in given section that contain given term.
	 */
	@RequestMapping(value = "/admin/property/search", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE } )
	@PreAuthorize("hasPermission(#model, 'EDIT_APP_CONFIG')")
	public @ResponseBody
	List<GIAppProperties> searchProperties(@RequestParam("section") String section, @RequestParam("term") String term)
	{
		List<GIAppProperties> props = null;

		if(section != null && term != null)
		{
			if(log.isInfoEnabled())
			{
				log.info("Searching for property term: '{}' in section: {}", term, section);
			}

			props = propertiesRepo.searchByPropertyKey(section, term);
		}
		else
		{
			if(log.isErrorEnabled())
			{
				log.error("Unable to search for property key: '{}' in section: {}", term, section);
			}
		}

		return props;
	}


	@RequestMapping(value = "/admin/property", method = RequestMethod.GET)
	@GiAudit(transactionName = "Read Application Configuration", eventType = EventTypeEnum.SECURITY_CONFIG, eventName = EventNameEnum.READ)
	@PreAuthorize("hasPermission(#model, 'EDIT_APP_CONFIG')")
	public String getProperty(Model model, HttpServletRequest request) {
		
		String action=request.getParameter("ACTION");
		
		List<GIAppProperties> properties = propertiesRepo.getPropertyKeyList(request.getParameter("ACTION"));
		
		model.addAttribute(GI_APP_PROPERTIES,properties) ; 
		model.addAttribute(RESULT_SIZE,properties.size()) ;  
		GiAuditParameterUtil.add(new GiAuditParameter("Action", action));
		
		if(action!=null)
		{
			model.addAttribute("ACTION",action);
			if(StringUtils.equals(action, "global"))
			{
				model.addAttribute(HEADER,GLOBAL_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "shop")){
				model.addAttribute(HEADER,SHOP_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "enrollment")){
				model.addAttribute(HEADER,ENROLLMENT_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "planSelection")){
				model.addAttribute(HEADER,PLANSELECTION_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "planManagement")){
				model.addAttribute(HEADER,PLANMANAGEMENT_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "finance")){
				model.addAttribute(HEADER,FINANCIAL_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "brokerManagement")){
				model.addAttribute(HEADER,BROKERMANAGEMENT_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "serff")){
				model.addAttribute(HEADER,SERFF_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "security")){
				model.addAttribute(HEADER,SECURITY_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "hub")){
				model.addAttribute(HEADER,HUB_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "iex")){
				model.addAttribute(HEADER,IEX_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "enrollment1095")){
				model.addAttribute(HEADER,ENROLLMENT_1095_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "renewal")){
				model.addAttribute(HEADER,RENEWAL_CONFIGURATION);
			}
			else
			{
				model.addAttribute(HEADER,UI_CONFIGURATION);
			}
		}
		return "admin/property";
	}
	
	
	
	@RequestMapping(value = "/admin/property", method = RequestMethod.POST)
	@GiAudit(transactionName = "Edit properties from Application Configuration", eventType = EventTypeEnum.SECURITY_CONFIG, eventName = EventNameEnum.UPDATE)
	@PreAuthorize("hasPermission(#model, 'EDIT_APP_CONFIG')")
	public String saveProperty(@ModelAttribute("GIAppProperties") GIAppProperties giAppProperties,BindingResult result, Model model, HttpServletRequest request) {
		String action=request.getParameter("ACTION");
		String errorMsg = null;
		
		String keyStr = request.getParameter("actualpropertyKey")!=null?request.getParameter("actualpropertyKey"):"";
		String valueStr = request.getParameter("actualpropertyValue")!=null?request.getParameter("actualpropertyValue"):"";
		try {
			if(keyStr != null && !keyStr.isEmpty()){
				ConfigPageData.validateConfigProperty(keyStr, valueStr);
			}
		GIAppProperties objGIAppProperties = new GIAppProperties();
		objGIAppProperties.setId(Integer.parseInt(request.getParameter("actualId")!=null?request.getParameter("actualId"):""));
		objGIAppProperties.setDescription(request.getParameter("description")!=null?request.getParameter("description"):"");
		objGIAppProperties.setPropertyKey(keyStr);
		objGIAppProperties.setPropertyValue(valueStr);
		try {
			objGIAppProperties.setUpdatedBy(userService.getLoggedInUser().getId());
		} catch (InvalidUserException e) {
			e.printStackTrace();
		}
		GiAuditParameterUtil.add(new GiAuditParameter("Action", action),new GiAuditParameter("id", request.getParameter("actualId")),new GiAuditParameter("Description", request.getParameter("description")),
				new GiAuditParameter("Property Key", request.getParameter("actualpropertyKey")),new GiAuditParameter("Property Value", request.getParameter("actualpropertyValue")));
		if(objGIAppProperties!=null)
		{
			propertiesRepo.saveAndFlush(objGIAppProperties);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Saved properties into DB"));
		}
		} catch (InvalidOperationException | ConfigValidationException e) {
			errorMsg = "Value not set, failed to validate the property ["+keyStr+"] "+e.getMessage();
		}
		
		List<GIAppProperties> properties = propertiesRepo.getPropertyKeyList(action);
		
		if(action!=null)
		{
			model.addAttribute("ACTION",action);
			if(StringUtils.equals(action, "global"))
			{
				model.addAttribute(HEADER,GLOBAL_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "shop")){
				model.addAttribute(HEADER,SHOP_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "enrollment")){
				model.addAttribute(HEADER,ENROLLMENT_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "planSelection")){
				model.addAttribute(HEADER,PLANSELECTION_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "planManagement")){
				model.addAttribute(HEADER,PLANMANAGEMENT_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "finance")){
				model.addAttribute(HEADER,FINANCIAL_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "brokerManagement")){
				model.addAttribute(HEADER,BROKERMANAGEMENT_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "serff")){
				model.addAttribute(HEADER,SERFF_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "security")){
				model.addAttribute(HEADER,SECURITY_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "hub")){
				model.addAttribute(HEADER,HUB_CONFIGURATION);
			}else if(StringUtils.equals(action, "iex")){
				model.addAttribute(HEADER,IEX_CONFIGURATION);
			}
			else if(StringUtils.equals(action, "enrollment1095")){
				model.addAttribute(HEADER,ENROLLMENT_1095_CONFIGURATION);
			}
			else
			{
				model.addAttribute(HEADER,UI_CONFIGURATION);
			}
		}
		model.addAttribute(GI_APP_PROPERTIES,properties);
		model.addAttribute(RESULT_SIZE,properties.size());

		if(errorMsg == null){
			model.addAttribute("successMsg", "Value successfully updated");
		}else{
			model.addAttribute("sessionErrorInfo", errorMsg);
		}
        return "admin/property";
	}
	
	
	@RequestMapping(value = "/admin/provision", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'USER_PROVISIONING')")
		public String getProvisioningRules(HttpServletRequest request) {
		return "admin/provision";
	}
	
}
