package com.getinsured.hix.batch.enrollment.external.nv.dto;

import java.math.BigDecimal;

public class ApplicableCostSharingReduction {
	 private BigDecimal csrAmount;
	 private String csrAmountEffectiveStartDate;
	 private String csrAmountEffectiveEndDate;


	 // Getter Methods 

	 public BigDecimal getCsrAmount() {
	  return csrAmount;
	 }

	 public String getCsrAmountEffectiveStartDate() {
	  return csrAmountEffectiveStartDate;
	 }

	 public String getCsrAmountEffectiveEndDate() {
	  return csrAmountEffectiveEndDate;
	 }

	 // Setter Methods 

	 public void setCsrAmount(BigDecimal csrAmount) {
	  this.csrAmount = csrAmount;
	 }

	 public void setCsrAmountEffectiveStartDate(String csrAmountEffectiveStartDate) {
	  this.csrAmountEffectiveStartDate = csrAmountEffectiveStartDate;
	 }

	 public void setCsrAmountEffectiveEndDate(String csrAmountEffectiveEndDate) {
	  this.csrAmountEffectiveEndDate = csrAmountEffectiveEndDate;
	 }
	}
