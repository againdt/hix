<!-- Household info popup -->
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script id="resendActivationEmailToInd" type="text/ng-template" >
<div id="resendActivationModal" class="modal" style="display:none">
<form class="form-horizontal" id="resendActivationForm" name="resendActivationForm"  novalidate="novalidate" >
	<div class="modal-header">
		  <p class="pull-left"><spring:message code="label.agent.individual.activationLink"/>!</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	 <div class="modal-body">
	    <div class="control-group">
	        <label class="control-label" for="firstName"><spring:message code="label.employeeDetails.PrimaryApplicant"/> </label>
	        <div class="controls">
	            {{modalContactData.firstName}}  {{modalContactData.lastName}}
	           </div>
	    </div>
		<div class="control-group">
	        <label class="control-label" for="email"><spring:message  code="label.employeeDetails.EmailAddress"/> <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
	        <div class="controls">
	            <input ng-model="indActLinkModle.email"  type="text" name="email" id="email"  required ng-pattern="/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/" />
					<span style="color:red;" ng-show="$parent.showInvalidEmailError">
							Email Address is already being used by an existing account
					</span>
					<span style="color:red;" ng-show="resendActivationForm.email.$error.required">Email is required.</span>
					<p style="color:red;" ng-show="resendActivationForm.email.$error.pattern"><spring:message code='label.validatePublicEmail'/></p>
	        
			 </div>
	    </div>
	    <div class="control-group">
	        <label class="control-label" for="phoneNumber1">Phone Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" /></label>
	        <div class="controls">
	        	<div style="padding:0px;margin:0px;float:left;">
					<input ng-model="indActLinkModle.phone1" type="text" id="phoneNumber1"  name="phoneNumber1" maxlength="3" ng-minlength="3" ng-maxlength="3" class="span2"  placeholder="" required   value=""/>
					<input ng-model="indActLinkModle.phone2" type="text" id="phoneNumber2"  name="phoneNumber2" maxlength="3" ng-minlength="3" ng-maxlength="3" class="span2"  placeholder="" required  value=""/>
					<input ng-model="indActLinkModle.phone3" type="text" id="phoneNumber3"  name="phoneNumber3" maxlength="4" ng-minlength="4" ng-maxlength="4" class="span2" placeholder="" required  value=""/>
				</div>
				<p style="color:red;" 
							ng-show="(resendActivationForm.phoneNumber1.$error.required || resendActivationForm.phoneNumber1.$error.minlength || resendActivationForm.phoneNumber1.$error.maxlength) || 
									 (resendActivationForm.phoneNumber2.$error.required || resendActivationForm.phoneNumber2.$error.minlength || resendActivationForm.phoneNumber2.$error.maxlength)||
									 (resendActivationForm.phoneNumber3.$error.required || resendActivationForm.phoneNumber3.$error.minlength || resendActivationForm.phoneNumber3.$error.maxlength)" >
							<spring:message code='label.broker.validatePhoneNo'/>
				</p>
		

	        </div>
	        <div class="clearfix"></div> <!-- Required clearfix div for HIX-57616 (Critical)  -->
	    </div>
	</div>
	<div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-primary" ng-if="!resendActivationForm.$valid">Send</button>
		    <button type="button" class="btn btn-primary" ng-click="sendIndividualActivationLink()" ng-if="resendActivationForm.$valid">Send</button>
	    
     </div>
</form>	
</div>
</script>

<script id="accountAlreadyCreated" type="text/ng-template" >
<div id="resendFailureModal" class="modal" style="display:none">
	<div class="modal-header">
	 	<p class="pull-left">Failure!</p>
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	<div class="modal-body">
		<p>This action is only allowed for individual consumers who have not completed the sign up process </p>
		<p>{{modalContactData.firstName}}  {{modalContactData.lastName}} has already completed the sign up process </p>
	</div>
     <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label.agent.details.okbtn"/></button>
     </div>
</div>
</script>
<script id="resendActEmailSuccess" type="text/ng-template" >
<div id="resendSuccessModal" class="modal" style="display:none">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	 <div class="modal-body">
     <p><spring:message code="label.agent.individual.activationLink.message.success.positive"/> {{modalContactData.firstName}}  {{modalContactData.lastName}}</p>
     </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="label.agent.details.okbtn"/></button>
      </div>
</div>
</script>
<script>
/* window.ParsleyConfig = {
		 successClass: "has-success",
		 errorClass: "has-error",
		 classHandler: function (el) {
		   return el.$element.closest(".control-group");
		 },
		 errorsContainer: function (el) {
		   return el.$element.closest(".control-group");
		 },
		 errorsWrapper: "<div class='error'></div>",
		 errorTemplate: "<div></div>"
		};
$("#frmaddconsumer").parsley();



$('#phoneNumber1, #phoneNumber2, #phoneNumber3').on('change keyup', function() {
	  var sanitized = $(this).val().replace(/[^0-9]/g, '');
	  $(this).val(sanitized);
	});
 */
</script>