package com.getinsured.hix.indportal.dto.dm.application;

public class MailingAddress {
	 private String streetName1;
	 private String streetName2;
	 private String cityName;
	 private String stateCode;
	 private String zipCode;
	 private String countyFipsCode;
	 private String countyName;


	 // Getter Methods 

	 public String getStreetName1() {
	  return streetName1;
	 }

	 public String getCityName() {
	  return cityName;
	 }

	 public String getStateCode() {
	  return stateCode;
	 }

	 public String getZipCode() {
	  return zipCode;
	 }

	 public String getCountyFipsCode() {
	  return countyFipsCode;
	 }

	 public String getCountyName() {
	  return countyName;
	 }

	 // Setter Methods 

	 public void setStreetName1(String streetName1) {
	  this.streetName1 = streetName1;
	 }

	 public void setCityName(String cityName) {
	  this.cityName = cityName;
	 }

	 public void setStateCode(String stateCode) {
	  this.stateCode = stateCode;
	 }

	 public void setZipCode(String zipCode) {
	  this.zipCode = zipCode;
	 }

	 public void setCountyFipsCode(String countyFipsCode) {
	  this.countyFipsCode = countyFipsCode;
	 }

	 public void setCountyName(String countyName) {
	  this.countyName = countyName;
	 }

	public String getStreetName2() {
		return streetName2;
	}

	public void setStreetName2(String streetName2) {
		this.streetName2 = streetName2;
	}
	 
	}
