package com.getinsured.hix.planmgmt.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.dto.planmgmt.ahbx.AHBXSyncRequest;
import com.getinsured.hix.dto.planmgmt.ahbx.AHBXSyncRequest.RequestMethod;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.repository.IPlanRepository;
import com.getinsured.hix.platform.util.GhixConstants;


public class PlanMgmtServiceTest extends BaseTest {
	private static final Logger LOGGER = Logger.getLogger(PlanMgmtServiceTest.class);
	
	@Autowired PlanMgmtService planMgmtService;
	@Autowired IPlanRepository iPlanRepository;
	@Autowired private RestTemplate restTemplate;

	@Value("#{configProp.plan_management.ahbx.sync.plan}")
	private static String PLAN_MANAGEMENT_AHBX_SYNC_PLAN_REST_URL;
		
	Plan iPlan=new Plan();;
	private int issuerId=5;
	private String name="test plan";
	
	private JSONParser parser = new JSONParser();
	
	@Test
	public void testgetMatchingPlanCount()
	{
		
		Long matchcount = iPlanRepository.getMatchingPlanCount(issuerId,name);
		System.out.println("Issuer ID is " +issuerId);
		System.out.println("Issuer Name is " +name);
		System.out.println("Matching count is "+matchcount);			
	}
	@SuppressWarnings("unused")
	@Test
	public void testAddedOrUpdateds()
	{
		String pattern = "MM/dd/yyyy";
	    SimpleDateFormat format = new SimpleDateFormat(pattern);
		try{
		Date startdate=format.parse("02/28/2013");
		Date enddate=format.parse("12/31/2014");
		
		List<Plan> planlist=iPlanRepository.getAddedOrUpdatedPlans(startdate, enddate);
		//assertTrue("No record found",planlist.size()>0);
		System.out.println("Start Date is "+ startdate);
		System.out.println("End Date is " + enddate);
		
		}
		catch (ParseException e) {
		      e.printStackTrace();
		    }
		   
	}
	
	/**
	 * Method to test ReST service for 'syncPlanWithAHBX'.
	 *
	 * @return void
	 */
	@Test
	public void testSyncPlanWithAHBX() {
		LOGGER.info("Beginning to process testSyncPlanWithAHBX().");
		/*try {
			AHBXSyncRequest requester = new AHBXSyncRequest(restTemplate);
			
			requester.setUrl(PLAN_MANAGEMENT_AHBX_SYNC_PLAN_REST_URL);
			requester.setRequestMethod(RequestMethod.POST);
			
			String responseJSON = requester.sendRequest();
	
			Validate.notNull(responseJSON, "Response is missing for request.");
			
			JSONObject rawObject = (JSONObject) parser.parse(responseJSON);
			
			Validate.notNull(rawObject, "Invalid instance for parsed responseJSON encountered.");
			
			String status = (String) rawObject.get("status");
		
			if(null == status || status.equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				LOGGER.info("Response of testGetBenchmarkRate() is '" + GhixConstants.RESPONSE_FAILURE + "'. Cause: errorcode= " + rawObject.get("errCode") + " message=" + rawObject.get("errMsg"));
				Assert.fail("Response of testGetBenchmarkRate() is '" + GhixConstants.RESPONSE_FAILURE + "'. Cause: errorcode= " + rawObject.get("errCode") + " message=" + rawObject.get("errMsg"));
				
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Response:" + responseJSON);
				}	
			}
			else {
				Map<String, Object> responseDataMap = (Map<String, Object>) rawObject.get("responseData");
				
				if(null == responseDataMap || responseDataMap.isEmpty()) {
					LOGGER.warn("Response data is missing for current ReST call.");
					Assert.fail("Response data is missing for current ReST call.");
				}
				else {
					Boolean responseData = (Boolean) responseDataMap.get("responseData");
	
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Response Data:" + responseData);
					}	
					
					 if(null == responseData) {
						LOGGER.warn("Invalid response found for current ReST call.");
						Assert.fail("Invalid response found for current ReST call.");
					 }
					 else {
						 if(LOGGER.isDebugEnabled()) {
							 LOGGER.debug("Response Data value:" + responseData.booleanValue());
						 }	
						 
						 if(responseData.booleanValue()) {
							 Assert.assertTrue(true);
						 }
						 else {
							 Assert.fail("Received negative acknowledgement in ReST response.");
						 }
					 }
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred while processing testSyncPlanWithAHBX()." + ex.getMessage());
			ex.printStackTrace();
			Assert.fail("Exception occurred while processing testSyncPlanWithAHBX()." + ex.getMessage());
		}
		finally {
			LOGGER.info("Processing of testSyncPlanWithAHBX() is completed.");
		}*/
	}
}
