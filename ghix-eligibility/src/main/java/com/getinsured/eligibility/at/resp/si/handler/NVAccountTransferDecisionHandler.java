package com.getinsured.eligibility.at.resp.si.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.transform.ERPTransformer;
import com.getinsured.eligibility.at.resp.si.transform.FetchData;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;

/**
 * EligibilityDecisionHandler to compare eligibilities.
 *
 * Take decision based on current and enrolled applications; if enrolled application found
 *
 * Take decision based current applications; if enrolled application not found
 *
 * EligibilityDecisionHandler updates Application status - {ER, EN and CL}
 *
 * @author Ekram
 *
 */
@Component("nvAccountTransferDecisionHandler")
@DependsOn("dynamicPropertiesUtil")
public class NVAccountTransferDecisionHandler{

	private static final Logger LOGGER = Logger.getLogger(NVAccountTransferDecisionHandler.class);

	@Autowired private ERPTransformer eRPTransformer;
	@Autowired private FetchData fetchData;
	
	private String STATE_CODE;

	@PostConstruct
	public void createStateContext() {
		STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	public String checkATProcessType(AccountTransferRequestDTO accountTransferRequestDTO){
		LOGGER.info("------- Inside checkATProcessType -------");
		Map<String, Object> resultMap = new HashMap<>();
		ERPResponse erpResponse = new ERPResponse();	/* safety for parsing json later.. */
		try {
			erpResponse = eRPTransformer.transform(accountTransferRequestDTO);
			erpResponse = fetchData.fetchData(erpResponse);
			AccountTransferRequestPayloadType request = accountTransferRequestDTO.getAccountTransferRequestPayloadType();
			
			if("NV".equalsIgnoreCase(STATE_CODE)){
				List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes = request.getInsuranceApplication().getApplicationIdentification();
				for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
					if(tIdentificationType.getIdentificationCategoryText() == null || (!"HouseholdCaseId".equalsIgnoreCase(tIdentificationType.getIdentificationCategoryText().getValue()) )){
						resultMap.put(EligibilityConstants.AT_PROCESS, EligibilityConstants.NV_INBOUND);
						break;
					}else{
						resultMap.put(EligibilityConstants.AT_PROCESS, EligibilityConstants.ELIGIBILITY);
					}
				}
			}else{
				resultMap.put(EligibilityConstants.AT_PROCESS, EligibilityConstants.ELIGIBILITY);
			}
		} catch (Exception e) {
			LOGGER.info("------- Error while executing checkATProcessType -------");
			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_AT_DECISION_REQUEST_FOR_SSAP_APPLICATION).
					append(accountTransferRequestDTO.getCaseNumber()).append(EligibilityConstants.REASON).append(e);

			LOGGER.error(errorReason.toString());
			resultMap.put(EligibilityConstants.AT_PROCESS_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());
			resultMap.put(EligibilityConstants.AT_PROCESS, EligibilityConstants.ERROR);
		}finally {
			resultMap.put(EligibilityConstants.ACCOUNT_TRANSFER_REQUEST_DTO, accountTransferRequestDTO);
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);
	}
	
	
	public String processERP(Message<ERPResponse> message){
		LOGGER.info("-------Inside NVAccountTransferDecisionHandler--- processERP -------");
		Map<String, Object> resultMap = new HashMap<>();
		try {
			ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}catch (Exception e) {
			LOGGER.info("------- Error while executing  NVAccountTransferDecisionHandler--- processERP -------");
			LOGGER.error(e.getStackTrace());
		}
		return EligibilityUtils.marshal(resultMap);
	}
	
	
	@SuppressWarnings("unchecked")
	public AccountTransferRequestDTO sendAccountTransferDto(String resultMapStr){
		LOGGER.info("-------Inside NVAccountTransferDecisionHandler --- sendAccountTransferDto -------");
		Map<String, Object> resultMap = new HashMap<>();
		AccountTransferRequestDTO accountTransferRequestDTO = new AccountTransferRequestDTO();
		try {
			resultMap = (Map<String, Object>) EligibilityUtils.unmarshal(resultMapStr);
			accountTransferRequestDTO =  (AccountTransferRequestDTO) resultMap.get(EligibilityConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
		}catch (Exception e) {
			LOGGER.info("------- Error while executing  NVAccountTransferDecisionHandler --- sendAccountTransferDto -------");
			LOGGER.error(e.getStackTrace());
		}
		return accountTransferRequestDTO;
	}
}
