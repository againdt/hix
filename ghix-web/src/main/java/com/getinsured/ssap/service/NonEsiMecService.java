package com.getinsured.ssap.service;

import com.getinsured.ssap.model.financial.nmec.NonEsiMecVerificationResponse;

/**
 * Non ESI MEC Verfication service
 *
 * @author Suresh Kancherla
 * @since 8/9/19
 */
public interface NonEsiMecService {

  /**
   * Invokes Non ESI MEC for given {@link com.getinsured.iex.ssap.model.SsapApplication} id.
   * @param ssapApplicationId SSAP id.
   */
	NonEsiMecVerificationResponse invoke(final long ssapApplicationId);
}
