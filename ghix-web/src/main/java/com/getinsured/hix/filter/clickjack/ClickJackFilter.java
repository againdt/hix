package com.getinsured.hix.filter.clickjack;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to prevent other website to access GI in iFrame.
 */

public class ClickJackFilter implements Filter {

  private static final Logger log = LoggerFactory.getLogger(ClickJackFilter.class);

  private String mode = "SAMEORIGIN";

  /**
   * This method will allow the page to be framed if it belongs to the same
   * origin as the page being framed
   *
   * @param request
   * @param response
   */
  public void doFilter(ServletRequest request, ServletResponse response,
                       FilterChain chain) throws IOException, ServletException {

    final HttpServletResponse httpServletResponse = (HttpServletResponse) response;
    final HttpServletRequest httpServletRequest = (HttpServletRequest) request;

    httpServletResponse.addHeader("X-FRAME-OPTIONS", mode);

    final String path = httpServletRequest.getServletPath();

    if (path != null && path.startsWith("/fonts/")) {
      final String context = httpServletRequest.getContextPath() != null ? httpServletRequest.getContextPath() : "";
      final String newPath = String.format("%s/resources%s", context, path);
      log.info("[warn] <attention: UI Team> Rewriting invalid font reference: {} => {}", path, newPath);
      httpServletResponse.sendRedirect(newPath);
    } else {
      chain.doFilter(request, response);
    }
  }

  public void destroy() {
  }

  /**
   * This method will do initialization for X-Frame option
   *
   * @param filterConfig
   */
  public void init(FilterConfig filterConfig) {
    String configMode = filterConfig.getInitParameter("mode");
    if (configMode != null) {
      mode = configMode;
    }
  }

}
