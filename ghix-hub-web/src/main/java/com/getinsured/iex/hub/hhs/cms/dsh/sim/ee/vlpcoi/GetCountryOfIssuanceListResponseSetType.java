//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.09 at 08:11:43 AM IST 
//


package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCountryOfIssuanceListResponseSetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCountryOfIssuanceListResponseSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlpcoi.ee.sim.dsh.cms.hhs.gov}NumberOfCountryCodes"/>
 *         &lt;element ref="{http://vlpcoi.ee.sim.dsh.cms.hhs.gov}CountryCodeArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCountryOfIssuanceListResponseSetType", propOrder = {
    "numberOfCountryCodes",
    "countryCodeArray"
})
public class GetCountryOfIssuanceListResponseSetType {

    @XmlElement(name = "NumberOfCountryCodes")
    protected int numberOfCountryCodes;
    @XmlElement(name = "CountryCodeArray", required = true)
    protected CountryCodeArrayType countryCodeArray;

    /**
     * Gets the value of the numberOfCountryCodes property.
     * 
     */
    public int getNumberOfCountryCodes() {
        return numberOfCountryCodes;
    }

    /**
     * Sets the value of the numberOfCountryCodes property.
     * 
     */
    public void setNumberOfCountryCodes(int value) {
        this.numberOfCountryCodes = value;
    }

    /**
     * Gets the value of the countryCodeArray property.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeArrayType }
     *     
     */
    public CountryCodeArrayType getCountryCodeArray() {
        return countryCodeArray;
    }

    /**
     * Sets the value of the countryCodeArray property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeArrayType }
     *     
     */
    public void setCountryCodeArray(CountryCodeArrayType value) {
        this.countryCodeArray = value;
    }

}
