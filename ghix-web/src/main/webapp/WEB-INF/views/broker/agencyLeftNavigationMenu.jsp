<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
String agencyId =  (String) request.getSession().getAttribute("landingAgencyId");
%>
<c:set var="agencyId" value="<%=agencyId%>"/>
<%--<input type="hidden" id="agencyId" value="${agencyId}"/>--%>

<!-- #sidebar  -->     

 <div class="span3" id="sidebar">
	<div class="header">
        <h4><spring:message code="label.brkSteps"/></h4>
        
    </div>         			
    <ul class="nav nav-list">
     	<li><a href="<c:url value="./../agency#/information/view/${agencyId}" />"><i class="icon-ok"></i> Agency Information</a></li>
        <li><a href="./../agency#/locationHour"><i class="icon-ok"></i> Location and Hours</a></li>
        <c:choose>
			<c:when test="${showOKicon == true}">
					<li><a href="/hix/broker/certificationapplication"><i class="icon-ok"></i>Agency Manager Information<!-- <spring:message code="label.brkBrokerInformation"/> --></a></li>
			</c:when>
			<c:otherwise>
				<li <c:if test="${activePage == 'agentInfo' }">class="active"</c:if> ><a href="#">3. &nbsp; Agency Manager Information<!-- <spring:message code="label.brkBrokerInformation"/> --></a></li>
			</c:otherwise>
		</c:choose>
		<li <c:if test="${activePage == 'agentProfile' }">class="active"</c:if> <c:if test="${activePage == 'agentInfo' }">class="disabled"</c:if>><a href="#">4. &nbsp; <spring:message code="label.profile"/></a></li>
		<!-- <c:choose>
			<c:when test="${sessionScope.designatedBrokerProfile.clientsServed != null || sessionScope.designatedBrokerProfile.productExpertise != null || sessionScope.designatedBrokerProfile.education != null || sessionScope.designatedBrokerProfile.aboutMe != null }">
				<li <c:if test="${activePage == 'agentProfile' }">class="active"</c:if> ><a href="#"><i class="icon-ok"></i><spring:message code="label.profile"/></a></li>
			</c:when>
			<c:otherwise>
			     <li <c:if test="${activePage == 'agentProfile' }">class="active"</c:if> <c:if test="${activePage == 'agentInfo' }">class="disabled"</c:if>><a href="#">4. &nbsp; <spring:message code="label.profile"/></a></li>
			</c:otherwise>
		</c:choose> -->
        <li class="disabled"><a href="./../agency#/documentupload">5. Document Upload</a></li>
        <li class="disabled"><a href="./../agency#/certificationstatus">6. Certification Status</a></li>       
    </ul>
</div>

<script>
	$('document').ready(function() {
		$('.disabled').click(function(e) {
			e.preventDefault();
		});
	});

</script>