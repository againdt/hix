module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "jest": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "globals": {
        "__SNAPSHOT__": true
    },
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "react-hooks"
    ],
    "rules": {
        "no-console": "off",
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error",
        "react-hooks/rules-of-hooks": "error",
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "react/prop-types": 0
    },
    settings: {
        react: {
            version: require('./package.json').devDependencies.react,
        },
    },
    "overrides": [
        {
            "files": ["bin/*.js", "lib/*.js", "./node_modules/**/*.*"],
// "excludedFiles": "*.test.js",
// "rules": {
// "quotes": [ 2, "single" ]
// },
            rules: {}
        }
    ]
};

