package com.getinsured.hix.batch.bulkusers.listeners;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;

public class BulkUsersJobExecutionListener implements JobExecutionListener{
	
	private static final Logger logger = LoggerFactory.getLogger(BulkUsersJobExecutionListener.class);

	@Override
	public void beforeJob(JobExecution jobExecution) {
		logger.info("Job is started -->[ jobProcessId : "+CommonUtil.getJobProcessId(jobExecution)+" , Start Time : "+CommonUtil.dateToString(new Date())+" ]");
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		logger.info("Job is ended -->[ jobProcessId : "+CommonUtil.getJobProcessId(jobExecution)+" , End Time : "+CommonUtil.dateToString(new Date())+" ]");
	}

}
