package com.getinsured.eligibility.at.ref.si;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.mes.service.AtSpanProcessingService;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;

/**
 * @author chopra_s
 *
 */
@Component("mesProcessor")
@Scope("singleton")
public class MESProcessor {
	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(MESProcessor.class);
	
	@Autowired
	AtSpanProcessingService atSpanProcessingService;
	
	@Autowired
	private ExceptionUtil exceptionUtil;

	public String processSpans(Message<String> message) throws Exception {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("MESProcessor processSpans starts - ");
		}
		
		long ssapApplicationId = 0;
		ReferralResponse referralResponse = new ReferralResponse();
		final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
		try {
				referralResponse.getData().putAll(input.getData());
				if(null != referralResponse.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID)) {
					ssapApplicationId = (long) referralResponse.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);	
				}				
				// Perform MES steps only if mes config is set to true and it is not called from mes batch processing.
				// otherwise return the referral response as received from previous step.
				String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
				if(mutipleEligibiltySpanEnabled.equalsIgnoreCase(Boolean.TRUE.toString()) 
						&& ReferralProcessingConstants.REQUESTER_AT.equalsIgnoreCase((String)input.getData().get(ReferralProcessingConstants.PROCESSOR_REQUESTER))){
					String spanStatus = atSpanProcessingService.processCurrentSpan(referralResponse.getData());
					if(StringUtils.isNotEmpty(spanStatus)){
						referralResponse.setHeadermessage(spanStatus);// set PROCESSED_REAL_TIME,QUEUED,CLOSED
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("MESProcessor span processed, received span status as - {} for application id {}",spanStatus,referralResponse.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
						}
					}
					else{
						referralResponse.setHeadermessage(ReferralResponse.REFERRAL_ERROR);
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("MESProcessor span processed, received NULL span status for application id {}",referralResponse.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
						}
					}
				}
				else{
					referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("MES CONFIG IS FALSE. NO ACTION REQUIRED IN MES STEP");
					}
				}
			
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_IN_MES_PROCESSING).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_MES_PROCESSING);
			if(0 != ssapApplicationId) {
				exceptionUtil.persistGiMonitorId(ssapApplicationId, e);	
			}			
		} 
		final String response = EligibilityUtils.marshal(referralResponse);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("MESProcessor processSpans ends - Response is - {}",response);
		}

		return response;

	}
}
