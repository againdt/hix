package com.getinsured.eligibility.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.iex.ssap.model.SsapApplicant;

public interface EligibilityProgramRepository extends JpaRepository<EligibilityProgram, Long> {

    List<EligibilityProgram> findBySsapApplicant(SsapApplicant ssapApplicant);
    
    @Query("Select count(eligibilityProgram.id) " +
			" FROM EligibilityProgram as eligibilityProgram "+
			" where eligibilityProgram.ssapApplicant.ssapApplication.id = :applicationId "+
			" and eligibilityType in(:eligibilityTypes)"+
			" and eligibilityIndicator = 'TRUE'"
			)
    long getChipAndMedicaidEligibilityByApplicant(@Param("applicationId") Long applicationId, @Param("eligibilityTypes") List<String> eligibilityTypes);
    
    @Query("Select ep " +
            " FROM EligibilityProgram as ep "+
            " inner join fetch ep.ssapApplicant as ssapApplicant"+
            " inner join fetch ssapApplicant.ssapApplication as ssapApplication"+
            " where ssapApplication.id = :ssapApplicationId ")
    List<EligibilityProgram> getEligibilitiesForApplication(@Param("ssapApplicationId") Long ssapApplicationId);

    
}
