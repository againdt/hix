package com.getinsured.eligibility.indportal.dto;

import java.math.BigDecimal;

public class IndividualDTO extends HouseholdMemberDTO {
	private String csr;
    private Float aptc;
    private BigDecimal stateSubsidy;
    private String serc;
    private Float ehbAmount;
    private Float aptcForKeep;
    private Long householdCaseId;
    private String enrollmentType;    
    private String coverageStartDate;
    private String financialEffectiveDate;
    
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public Float getAptc() {
		return aptc;
	}
	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}
	public String getSerc() {
		return serc;
	}
	public void setSerc(String serc) {
		this.serc = serc;
	}
	public Float getEhbAmount() {
		return ehbAmount;
	}
	public void setEhbAmount(Float ehbAmount) {
		this.ehbAmount = ehbAmount;
	}
	public Float getAptcForKeep() {
		return aptcForKeep;
	}
	public void setAptcForKeep(Float aptcForKeep) {
		this.aptcForKeep = aptcForKeep;
	}
	public Long getHouseholdCaseId() {
		return householdCaseId;
	}
	public void setHouseholdCaseId(Long householdCaseId) {
		this.householdCaseId = householdCaseId;
	}
	public String getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getFinancialEffectiveDate() {
		return financialEffectiveDate;
	}
	public void setFinancialEffectiveDate(String financialEffectiveDate) {
		this.financialEffectiveDate = financialEffectiveDate;
	}

	public BigDecimal getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(BigDecimal stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}
}
