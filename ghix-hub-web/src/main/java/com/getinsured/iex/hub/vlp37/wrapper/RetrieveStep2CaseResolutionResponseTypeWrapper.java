package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;


import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r.RetrieveStep2CaseResolutionResponseType;

public class RetrieveStep2CaseResolutionResponseTypeWrapper implements JSONAware{

	private RetrieveStep2CaseResolutionResponseType retrieveStep2CaseResolutionResponseType;
	
	private RetrieveStep2ResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;
	private RetrieveStep2CaseResolutionResponseSetTypeWrapper retrieveStep2ResponseTypeResponseSet;
	
	public RetrieveStep2CaseResolutionResponseTypeWrapper(RetrieveStep2CaseResolutionResponseType retrieveStep2CaseResolutionResponseType){
		
		if(retrieveStep2CaseResolutionResponseType == null){
			return;
		}
		
		RetrieveStep2ResponseMetadataWrapper responseMetadataWrapper = new RetrieveStep2ResponseMetadataWrapper(retrieveStep2CaseResolutionResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
//		this.lawfulPresenceVerifiedCode = intialVerificationResponseType.getLawfulPresenceVerifiedCode();
		
		RetrieveStep2CaseResolutionResponseSetTypeWrapper retrieveStep2CaseResolutionResponseSetTypeWrapper = 
				new RetrieveStep2CaseResolutionResponseSetTypeWrapper(retrieveStep2CaseResolutionResponseType.getRetrieveStep2CaseResolutionResponseSet());
		
		this.retrieveStep2ResponseTypeResponseSet = retrieveStep2CaseResolutionResponseSetTypeWrapper;
	}

	public RetrieveStep2CaseResolutionResponseType getRetrieveStep2CaseResolutionResponseType() {
		return retrieveStep2CaseResolutionResponseType;
	}

	public void setRetrieveStep2CaseResolutionResponseType(
			RetrieveStep2CaseResolutionResponseType retrieveStep2CaseResolutionResponseType) {
		this.retrieveStep2CaseResolutionResponseType = retrieveStep2CaseResolutionResponseType;
	}

	public RetrieveStep2ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(RetrieveStep2ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	public RetrieveStep2CaseResolutionResponseSetTypeWrapper getRetrieveStep2CaseResolutionResponseSet() {
		return retrieveStep2ResponseTypeResponseSet;
	}

	public void setRetrieveStep2CaseResolutionResponseSet(
			RetrieveStep2CaseResolutionResponseSetTypeWrapper retrieveStep2CaseResolutionResponseSetTypeWrapper) {
		this.retrieveStep2ResponseTypeResponseSet = retrieveStep2CaseResolutionResponseSetTypeWrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.responseMetadata != null){
			obj.put("ResponseMetadata",this.responseMetadata);
		}
		
		if(this.lawfulPresenceVerifiedCode != null){
			obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		}
		
		if(this.retrieveStep2ResponseTypeResponseSet != null){
			obj.put("RetriveStep2ResponseTypeResponseSet",this.retrieveStep2ResponseTypeResponseSet);
		}

		return obj.toJSONString();
	}

}
