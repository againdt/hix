package com.getinsured.hix.admin.web;

import java.util.Comparator;

import com.getinsured.hix.model.Issuer;

public class ComparatorClass implements Comparator<Issuer>
{
    public int compare(Issuer i1, Issuer i2)
    {
    	if(i1.getName()!=null && i2.getName()!=null){
    		return i1.getName().compareTo(i2.getName());
    	}else{
    		return -1;
    	}
       
   }
}