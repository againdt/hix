package com.getinsured.ahbx.util;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.getinsured.hix.model.GHIXResponse;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * AHBX Util class.
 * 
 * @author Ekram Ali Kazi
 * 
 */
public class AHBXUtils {

	/**
	 * Marshal and form XML response.
	 * 
	 * @param response
	 * @return XML String
	 */
	public static String marshal(GHIXResponse response) {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		return xstream.toXML(response);
	}

	/**
	 * Converts java util date to java sql date
	 * 
	 * @param date
	 * @return java.sql.Date
	 */
	public static java.sql.Date convertUtilToSQLDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}

	/**
	 * Formats the given date into format defined in
	 * GhixAhbxConstants.DATE_FORMAT
	 * 
	 * @param date
	 * @return java.util.Date formatted date
	 */
	public static Date formatDate(String date) throws Exception {
		Date formattedDate;
		SimpleDateFormat format = new SimpleDateFormat(
				GhixAhbxConstants.DATE_FORMAT);
		try {
			formattedDate = format.parse(date);
		} catch (ParseException parseException) {
			throw new Exception(parseException);
		}
		return formattedDate;
	}

	/**
	 * Formats the given date into format defined in
	 * GhixAhbxConstants.PLAIN_DATE_FORMAT
	 * 
	 * @param date
	 * @return java.util.Date formatted date
	 */
	public static Date formatPlainDate(String date) throws Exception {
		Date formattedDate;
		SimpleDateFormat format = new SimpleDateFormat(
				GhixAhbxConstants.PLAIN_DATE_FORMAT);
		try {
			formattedDate = format.parse(date);
		} catch (ParseException parseException) {
			throw new Exception(parseException);
		}
		return formattedDate;
	}
	
	/**
	 * Checks for null or blank String objects
	 * 
	 * @param value
	 * @return boolean
	 */
	public static boolean isEmpty(String value) {
		return (value != null && !"".equals(value)) ? false : true;
	}
	
	public static String searchXML(String node, String xmlString) {
		String nodeValue = null;
		try {
			XPathFactory xPathFactory = XPathFactory.newInstance();
			XPath xpath = xPathFactory.newXPath();
			nodeValue = xpath
					.evaluate(node, convertStringToDocument(xmlString));
		} catch (Exception exception) {
		}

		return nodeValue;
	}

	private static Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(
					xmlStr)));
			return doc;
		} catch (Exception exception) {
		}
		return null;
	}
}
