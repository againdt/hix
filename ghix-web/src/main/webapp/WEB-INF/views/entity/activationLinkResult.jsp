<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
				<c:choose>
				<c:when test="${param.activationLinkResult == 'Success'}">
					<div style="font-size: 14px; color: green">
						<p>
							<spring:message code='label.assister.sendactivationlink'/><p />							
								<br>
					</div>
				</c:when>
			
					<c:when test="${param.activationLinkResult == 'Failure'}">
						<div style="font-size: 14px; color: red">							
						<p>
							<spring:message code='label.assister.activationlinkfailed'/><p/>							
								<br>
						</div>			
						</c:when>
				</c:choose>	