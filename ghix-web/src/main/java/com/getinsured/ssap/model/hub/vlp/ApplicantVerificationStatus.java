package com.getinsured.ssap.model.hub.vlp;

import java.util.Arrays;

/**
 * Holds status codes for applicant verification results.
 *
 * @author Yevgen Golubenko
 * @since 3/30/19
 */
public enum ApplicantVerificationStatus {
  /**
   * Unknown status, we are not able to parse given string value into enum
   * or database holds a value that is not matching any of the current supported
   * status codes.
   */
  UNKNOWN("UNKNOWN"),

  /**
   * Verification was successful.
   */
  VERIFIED("VERIFIED"),

  /**
   * Verification failed.
   */
  NOT_VERIFIED("NOT_VERIFIED"),

  /**
   * Verification is pending. This could mean either of these:
   * <ul>
   *   <li>Communication Error/Service not available</li>
   *   <li>Exception on the HUB side</li>
   *   <li>Verification just started and in progress of receiving response</li>
   * </ul>
   */
  PENDING("PENDING"),

  /**
   * Verification call returned status that could mean any the of following:
   * <ul>
   *   <li>Verification returned status code that requires
   *    more information from the user</li>
   *   <li>Verification returned status code that
   *    requires modification of the original information submitted by the user</li>
   * </ul>
   */
  MORE_INFO_NEEDED("MORE_INFO_NEEDED");

  private String value;

  ApplicantVerificationStatus(final String value) {
    this.value = value;
  }

  /**
   * Returns string representation of this enum.
   * @return string representation of this enum.
   */
  public String getValue() {
    return this.value;
  }

  /**
   * Returns {@link ApplicantVerificationStatus} enum from given string value.
   * <p>
   *   We can use this if value contains spaces, e.g. NOT VERIFIED instead of NOT_VERIFIED.
   * </p>
   * @param value String value.
   * @return {@link ApplicantVerificationStatus} or in case of invalid string value {@link ApplicantVerificationStatus#UNKNOWN}.
   */
  public static ApplicantVerificationStatus fromValue(final String value) {

    if("NOT VERIFIED".equals(value) || "NOT_VERIFIED".equals(value)) {
      return ApplicantVerificationStatus.NOT_VERIFIED;
    } else if("VERIFIED".equals(value)) {
      return ApplicantVerificationStatus.VERIFIED;
    } else if("PENDING".equals(value)) {
      return ApplicantVerificationStatus.PENDING;
    } else if("MORE_INFO_NEEDED".equals(value)) {
      return ApplicantVerificationStatus.MORE_INFO_NEEDED;
    } else if("UNKNOWN".equals(value) || value == null) {
      return ApplicantVerificationStatus.UNKNOWN;
    }

    return Arrays.stream(ApplicantVerificationStatus.values()).filter(rc -> rc.getValue().equals(value)).findFirst().orElse(ApplicantVerificationStatus.UNKNOWN);
  }
}
