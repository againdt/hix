package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpsav.ResubmitResponseType;



public class ResubmitResponseTypeWrapper implements JSONAware{

	private ResubmitResponseType resubmitResponseType;
	
	private ResubmitResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;
	private ResubmitResponseSetTypeWrapper resubmitResponseTypeResponseSet;
	
	public ResubmitResponseTypeWrapper(ResubmitResponseType resubmitResponseType){
		
		if(resubmitResponseType == null){
			return;
		}
		
		ResubmitResponseMetadataWrapper responseMetadataWrapper = new ResubmitResponseMetadataWrapper(resubmitResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
//		this.lawfulPresenceVerifiedCode = intialVerificationResponseType.getLawfulPresenceVerifiedCode();
		
		ResubmitResponseSetTypeWrapper resubmitResponseSetTypeWrapper = 
				new ResubmitResponseSetTypeWrapper(resubmitResponseType.getResubmitResponseSet());
		
		this.resubmitResponseTypeResponseSet = resubmitResponseSetTypeWrapper;
	}

	public ResubmitResponseType getReVerificationResponseType() {
		return resubmitResponseType;
	}

	public void setReVerificationResponseType(
			ResubmitResponseType resubmitResponseType) {
		this.resubmitResponseType = resubmitResponseType;
	}

	public ResubmitResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ResubmitResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	public ResubmitResponseSetTypeWrapper getReSubmitResponseSet() {
		return resubmitResponseTypeResponseSet;
	}

	public void setReSubmitResponseSet(
			ResubmitResponseSetTypeWrapper resubmitResponseTypeResponseSet) {
		this.resubmitResponseTypeResponseSet = resubmitResponseTypeResponseSet;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.responseMetadata != null){
			obj.put("ResponseMetadata",this.responseMetadata);
		}
		
		if(this.lawfulPresenceVerifiedCode != null){
			obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		}
		
		if(this.resubmitResponseTypeResponseSet != null){
			obj.put("IntialVerificationResponseTypeResponseSet",this.resubmitResponseTypeResponseSet);
		}

		return obj.toJSONString();
	}

}
