package com.getinsured.hix.batch.enrollment.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component("enrollmentMonthlyPremiumsProcessor")
public class EnrollmentMonthlyPremiumsProcessor  implements ItemProcessor<Integer, Integer>{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPremiumsProcessor.class);

	@Override
	public Integer process(Integer enrollmentId) throws Exception {
		LOGGER.info("EnrollmentMonthlyPremiumsProcessor :: " + enrollmentId);
		return enrollmentId;
	}
}
