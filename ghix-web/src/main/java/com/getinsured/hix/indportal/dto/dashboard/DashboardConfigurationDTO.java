package com.getinsured.hix.indportal.dto.dashboard;

import java.util.List;

public class DashboardConfigurationDTO {

	private String oepStartDate;
	private String oepEndDate;
	private Integer oeApproachingdays;
	private String sepStartDate;
	private String sepEndDate;
	private String tobbacoEnabled;
	private String hardshipEnabled;
	private String serverDate;
	private String ssapFlowVersion;
	private List<DashboardAnnouncementDTO> announcements;
	private String oeEnrollEndDate;

	public String getOepStartDate() {
		return oepStartDate;
	}

	public void setOepStartDate(String oepStartDate) {
		this.oepStartDate = oepStartDate;
	}

	public String getOepEndDate() {
		return oepEndDate;
	}

	public void setOepEndDate(String oepEndDate) {
		this.oepEndDate = oepEndDate;
	}

	public Integer getOeApproachingdays() {
		return oeApproachingdays;
	}

	public void setOeApproachingdays(Integer oeApproachingdays) {
		this.oeApproachingdays = oeApproachingdays;
	}

	public String getSepStartDate() {
		return sepStartDate;
	}

	public void setSepStartDate(String sepStartDate) {
		this.sepStartDate = sepStartDate;
	}

	public String getSepEndDate() {
		return sepEndDate;
	}

	public void setSepEndDate(String sepEndDate) {
		this.sepEndDate = sepEndDate;
	}

	public String getTobbacoEnabled() {
		return tobbacoEnabled;
	}

	public void setTobbacoEnabled(String tobbacoEnabled) {
		this.tobbacoEnabled = tobbacoEnabled;
	}

	public String getHardshipEnabled() {
		return hardshipEnabled;
	}

	public void setHardshipEnabled(String hardshipEnabled) {
		this.hardshipEnabled = hardshipEnabled;
	}

	public String getServerDate() {
		return serverDate;
	}

	public void setServerDate(String serverDate) {
		this.serverDate = serverDate;
	}

	public String getSsapFlowVersion() {
		return ssapFlowVersion;
	}

	public void setSsapFlowVersion(String ssapFlowVersion) {
		this.ssapFlowVersion = ssapFlowVersion;
	}

	public List<DashboardAnnouncementDTO> getAnnouncements() {
		return announcements;
	}

	public void setAnnouncements(List<DashboardAnnouncementDTO> announcements) {
		this.announcements = announcements;
	}

	public String getOeEnrollEndDate() {
		return oeEnrollEndDate;
	}

	public void setOeEnrollEndDate(String oeEnrollEndDate) {
		this.oeEnrollEndDate = oeEnrollEndDate;
	}

}
