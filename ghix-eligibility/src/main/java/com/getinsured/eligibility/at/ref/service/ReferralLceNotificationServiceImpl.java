package com.getinsured.eligibility.at.ref.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.resp.service.SsapApplicantService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.notification.dto.ReferralLCENotificationDTO;
import com.getinsured.eligibility.plan.service.ApplicantPlanDto;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapter;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterRequest;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterResponse;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.EmailNotificationService;
import com.getinsured.hix.platform.notification.dto.NotificationDto;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.ApplicantEventValidationStatus;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

@Component("referralLceNotificationService")
@Scope("singleton")
@DependsOn("dynamicPropertiesUtil")
public class ReferralLceNotificationServiceImpl implements ReferralLceNotificationService {
	
	private static final Logger LOGGER = Logger.getLogger(ReferralLceNotificationServiceImpl.class);

	private static final String CA_STATE_CODE = "CA";
	private static final String ID_STATE_CODE = "ID";
	
	@Autowired
	private NoticeService noticeService;
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;

	@Autowired private PreferencesService preferencesService;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private ILocationRepository iLocationRepository;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;

	@Autowired
	@Qualifier("planAvailabilityAdapter")
	private PlanAvailabilityAdapter planAvailabilityAdapter;

	@Autowired
	private SsapApplicantService ssapApplicantService;

	@Autowired
	private EmailNotificationService emailNotificationService;

	private static final String YES = "Yes";
	private static final String NO = "No";
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;
	
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	@Autowired private Gson platformGson;

	/**
	 * New notice for Loss or gain of AI / AN status
	 */
	@Override
	public String generateAIANLCENotice(String caseNumber) throws Exception {
		return generate(caseNumber, "EE066LCEAIANStatusChangeNotification");
	}
	
	/**
	 * New notice for CSR level change for IND
	 */
	@Override
	public String generateLossOfCSRIndNotice(String caseNumber) throws Exception {
		return generate(caseNumber, "EE065CSRLossForAMember");
	}
	
	@Override
	public String generateDemographicChangeNotice(String caseNumber) throws Exception {
		/* LCE01DemographicChange */
		return generate(caseNumber, "DemographicChange");
	}
	
	@Override
	public String generateResponseATChangeNotice(String caseNumber) throws Exception {
		return generate(caseNumber, "ResponseATChange");		
	}

	@Override
	public String generateSEPEventNotice(String caseNumber) throws Exception {

		return generate(caseNumber, "SEPEvent");
	}
	
	@Override
	public String generateSEPEventNoticeQEPConfirmLifeEvent(String caseNumber,boolean triggerDocumentRequiredNoticeFlag) throws Exception {

		return generateSEPEventNoticeForQEP(caseNumber, "SEPEvent",triggerDocumentRequiredNoticeFlag);
	}

	@Override
	public String generateFinancialLCENotice(String caseNumber) throws Exception {

		return generate(caseNumber, "FinancialLCE");
	}

	@Override
	public String generateEligibilityLossNotice(String caseNumber) throws Exception {

		return generate(caseNumber, "HouseHoldEligibilityLoss");
	}

	/*
	 * @Override public String generateNoChangeNotice(String caseNumber) throws Exception { return generate(caseNumber, "NoChange"); }
	 */

	@Override
	public String generateAPTCChangeNotice(String caseNumber) throws Exception {
		return generate(caseNumber, "APTCAmountChangedNotice");
	}

	@Override
	public String generateSEPDenialNotice(String caseNumber) throws Exception {
		return generate(caseNumber, "SpecialEnrollmentPeriodDenied");
	}

	@Override
	public String generateQEPEventNotice(String caseNumber) throws Exception {

		return generate(caseNumber, "QualifyingEvent");
	}

	@Override
	public String generateChangeActionNotice(String caseNumber, LCEProcessRequestDTO lceProcessRequestDTO, boolean triggerEE20) throws Exception {

		if (lceProcessRequestDTO == null) {
			lceProcessRequestDTO = populatelceProcessRequestDTO(caseNumber);
		}

		PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(lceProcessRequestDTO);
		if (checkForFailure(planAvailabilityAdapterResponse)) {
			if (triggerEE20) {
				return generateSEPEventNotice(caseNumber);
			} else {
				return generateSEPEventKeepOnlyNotice(caseNumber);
			}
		} else {
			return generateAndSendNotice(caseNumber, planAvailabilityAdapterResponse, lceProcessRequestDTO);
		}
	}

	@Override
	public String generateDenialNotice(String caseNumber) throws Exception {
		return generateDenial(caseNumber, "DenialNotificationEmailNotice");
	}

	@Override
	public String generateCSPlanChangeNotice(String caseNumber, boolean isHealthDisEnrolled, boolean isDentalDisEnrolled, Date terminationDate) throws Exception {
		return generateCSPlanAndRTChange(caseNumber, "EE018FCSPlanChange", isHealthDisEnrolled, isDentalDisEnrolled, terminationDate);
	}

	@Override
	public String generateRatingAreaChangeNotice(String caseNumber, boolean isHealthDisEnrolled, boolean isDentalDisEnrolled, Date terminationDate) throws Exception {
		return generateCSPlanAndRTChange(caseNumber, "EE018FRTRatingAreaChange", isHealthDisEnrolled, isDentalDisEnrolled, terminationDate);
	}

	private LCEProcessRequestDTO populatelceProcessRequestDTO(String caseNumber) {

		LCEProcessRequestDTO lCEProcessRequestDTO = new LCEProcessRequestDTO();

		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);
		lCEProcessRequestDTO.setCurrentApplicationId(ssapApplication.getId());

		CompareMainDTO compareMainDTO = populateCompareMainDTO(ssapApplication);
		lCEProcessRequestDTO.setEnrolledApplicationId(compareMainDTO.getEnrolledApplication().getId());

		ssapEnrolleeHandler.parseListofEnrollees(compareMainDTO);

		lCEProcessRequestDTO.setEnrolledApplicationAttributes(compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes());
		return lCEProcessRequestDTO;
	}

	private CompareMainDTO populateCompareMainDTO(SsapApplication ssapApplication) {
		CompareMainDTO compareMainDTO = new CompareMainDTO();
		String externalApplicantId = null;

		compareMainDTO.setEnrolledApplication(new CompareApplicationDTO());
		List<SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (ssapApplicant.getPersonId() == ReferralConstants.ONE) {
				externalApplicantId = ssapApplicant.getExternalApplicantId();
				break;
			}
		}
		List<SsapApplication> ssapApplications = ssapApplicantService.findSsapApplicationForExternalApplicantId(externalApplicantId);
		for (SsapApplication ssapApplicationTemp : ssapApplications) {
			if (ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplicationTemp.getApplicationStatus())) {
				compareMainDTO.getEnrolledApplication().setId(ssapApplicationTemp.getId());

				for (SsapApplicant ssapApplicant : ssapApplicationTemp.getSsapApplicants()) {
					CompareApplicantDTO compareApplicantDTO = new CompareApplicantDTO();
					compareApplicantDTO.setApplicantGuid(ssapApplicant.getApplicantGuid());
					compareMainDTO.getEnrolledApplication().getApplicants().add(compareApplicantDTO);
				}
				break;
			}
		}
		return compareMainDTO;
	}

	private String generateAndSendNotice(String caseNumber, PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse, LCEProcessRequestDTO lceProcessRequestDTO) throws Exception {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);

		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);

		int moduleId = extractHouseholdId(ssapApplication);
		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, moduleId);

		if (planAvailabilityAdapterResponse.getHealthEnrollmentId() > 0) {
			Map<String, ApplicantPlanDto> healthPlanDisplayMap = planAvailabilityAdapterResponse.getHealthPlanDisplayMap();
			if (healthPlanDisplayMap != null && healthPlanDisplayMap.size() > 0) {
				referralLCENotificationDTO.setCurrentPlanAvailable(YES);
				ApplicantPlanDto applicantPlanDto = healthPlanDisplayMap.values().iterator().next();
				Float newPremium = applicantPlanDto.getPlanLevelPremiumAfterCredit();
				Float oldPremium = lceProcessRequestDTO.getEnrolledApplicationAttributes().getHealthNetPremiumAmt();
				if (newPremium != null && newPremium.equals(oldPremium)) {
					referralLCENotificationDTO.setCurrentPlanPremiumChange(NO);
				} else {
					referralLCENotificationDTO.setCurrentPlanPremiumChange(YES);
					referralLCENotificationDTO.setNewPremium(df.format(newPremium));
				}
			}
		} else {
			referralLCENotificationDTO.setCurrentPlanAvailable(NO);
		}
		return generateNotice("ChangeActionRequiredNotice", ssapApplication, moduleId, referralLCENotificationDTO);
	}

	private boolean checkForFailure(PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse) {
		boolean returnValue = false;
		if (planAvailabilityAdapterResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
			returnValue = true;
		} else if (planAvailabilityAdapterResponse.getHealthEnrollmentId() == 0 || planAvailabilityAdapterResponse.getHealthPlanDisplayMap() == null) {
			returnValue = true;
		}
		return returnValue;
	}

	@Override
	public String generateSEPEventKeepOnlyNotice(String caseNumber) throws Exception {
		return generate(caseNumber, "SEPKeepOnlyEvent");
	}

	@Override
	public String generateNonFinConversionNoticeWithAptcCsrIneligibile(String caseNumber) throws Exception {
		return generate(caseNumber, "EE058NonFinancialConversionWithAPTCAndCSR");
	}

	@Override
	public String generateNonFinConversionNoticeWithAptcOnlyIneligibile(String caseNumber) throws Exception {

		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);
		Map<String, Object> enrollmentIdMap = ssapEnrolleeHandler.fetchEnrollmentID(ssapApplication.getId());

		int moduleId = extractHouseholdId(ssapApplication);
		ReferralLCENotificationDTO referralLCENotificationDTO = populateReferralLCENotificationDTO(enrollmentIdMap, ssapApplication, moduleId);
		return generateNotice("EE056NonFinancialConversionWithAPTCOnly", ssapApplication, moduleId, referralLCENotificationDTO);

	}

	@Override
	public String generateNonFinToFinConversionNotice(String caseNumber) throws Exception {
		return generate(caseNumber, "EE032ConversionNonFinToFin");
	}

	@Override
	public String generateNonFinToFinConversionNoticeManual(String caseNumber) throws Exception {
		return generate(caseNumber, "EE033NonFinToFinManual");
	}

	@Override
	public String generateHHLostEligibilityNoticewithNonQhpIndicators(String caseNumber, long enrolledApplicationId) throws Exception {
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);
		int moduleId = extractHouseholdId(ssapApplication);
		Date endDate = ssapEnrolleeHandler.calculateDisenrollmentEndDate(enrolledApplicationId);
		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, moduleId);
		referralLCENotificationDTO.setEnrollmentEndDate(endDate);
		if (referralLCENotificationDTO.getEnrollmentEndDate() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
			referralLCENotificationDTO.setSpanishEnrollmentDate(formatter.format(referralLCENotificationDTO.getEnrollmentEndDate()));
		}
		return generateNotice("EE019LceDisenrollNotification", ssapApplication, moduleId, referralLCENotificationDTO);
	}

	@Override
	public String generateAddRemoveAutomationNotice(String caseNumber) throws Exception {
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);
		int moduleId = extractHouseholdId(ssapApplication);
		List<SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();
		ApplicantStatusEnum applicantStatus;
		String dependentName = StringUtils.EMPTY;
		List<String> addedDependentList = new ArrayList<String>();
		List<String> removedDependentList = new ArrayList<String>();
		
		Map<String,String> gUidMemberCoverageStartDateMap = new HashMap<String, String>();
		Map<String,String> gUidMemberCoverageEndDateMap = new HashMap<String, String>();
		
		List<SsapApplicant> addedApplicants = new ArrayList<SsapApplicant>();
		Map<String, String> memberNames = new HashMap<String, String>();
		List<Map<String, String>> memberNameList = new ArrayList<Map<String, String>>();
		if(checkStateCode(ID_STATE_CODE) && ssapApplicants != null && ssapApplicants.size()>0) {
			String enrollmentResponseStr = getEnrollmentsByMember(ssapApplicants, ssapApplication.getCoverageYear()+"");
			if(StringUtils.isNotBlank(enrollmentResponseStr)) {
				EnrollmentResponse enrollmentResponse = platformGson.fromJson(enrollmentResponseStr, EnrollmentResponse.class);
				if(enrollmentResponse != null) {
					Map<String, List<EnrollmentMemberDataDTO>> enrolleeData = enrollmentResponse.getEnrollmentMemberDataDTOMap();
					if(enrolleeData != null) {
						for (SsapApplicant ssapApplicant : ssapApplicants) {
							List<EnrollmentMemberDataDTO> enrollmentDTOList = enrolleeData.get(ssapApplicant.getApplicantGuid());
							if(enrollmentDTOList != null && !enrollmentDTOList.isEmpty()) {
								for(EnrollmentMemberDataDTO enrollmentDTO : enrollmentDTOList) {
									if("Health".equalsIgnoreCase(enrollmentDTO.getInsuranceType())) {
										gUidMemberCoverageStartDateMap.put(ssapApplicant.getApplicantGuid(), DateUtil.dateToString(enrollmentDTO.getMemberCoverageStartDate(), "MM/dd/yyyy"));
										gUidMemberCoverageEndDateMap.put(ssapApplicant.getApplicantGuid(), DateUtil.dateToString(enrollmentDTO.getMemberCoverageEndDate(), "MM/dd/yyyy"));
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		for (SsapApplicant ssapApplicant : ssapApplicants) {
			applicantStatus = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(applicantStatus)) {
				if(!checkStateCode(CA_STATE_CODE)){
					dependentName = ReferralUtil.fullName(ssapApplicant.getFirstName(), ssapApplicant.getMiddleName(), ssapApplicant.getLastName());
					String effectiveDate = gUidMemberCoverageStartDateMap.get(ssapApplicant.getApplicantGuid());
					if(effectiveDate!=null) {
						addedDependentList.add(dependentName + " : " + effectiveDate);
					}
					else {
						addedDependentList.add(dependentName);
					}
				}else{
					memberNames.put("memberFirstName", ssapApplicant.getFirstName());
					memberNames.put("memberLastName", ssapApplicant.getLastName());
					memberNames.put("memberSuffix", ssapApplicant.getNameSuffix());
					memberNameList.add(memberNames);
					addedApplicants.add(ssapApplicant);
				}
			} else if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(applicantStatus) && !checkStateCode(CA_STATE_CODE)) {
				dependentName = ReferralUtil.fullName(ssapApplicant.getFirstName(), ssapApplicant.getMiddleName(), ssapApplicant.getLastName());
				String termDate = gUidMemberCoverageEndDateMap.get(ssapApplicant.getApplicantGuid());
				if(termDate!=null) {
					removedDependentList.add(dependentName + " : " + termDate );
				} else { 
					removedDependentList.add(dependentName);
				}
			}

		}

		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, moduleId);
		referralLCENotificationDTO.setAddedDependents(addedDependentList);
		if(!checkStateCode(CA_STATE_CODE)){
			referralLCENotificationDTO.setRemovedDependents(removedDependentList);
		}else if(checkStateCode(CA_STATE_CODE)){
			//get Plan name and coverage start date for newly eligible / newly ineligible applicants.
			Map<String, List<EnrollmentMemberDataDTO>> enrolleeData = null;
			String enrollmentResponseStr = getEnrollmentsByMember(addedApplicants, ssapApplication.getCoverageYear()+"");
			if(StringUtils.isNotBlank(enrollmentResponseStr)) {
				EnrollmentResponse enrollmentResponse = platformGson.fromJson(enrollmentResponseStr, EnrollmentResponse.class);
				if(enrollmentResponse != null) {
					enrolleeData = enrollmentResponse.getEnrollmentMemberDataDTOMap();
					if(enrolleeData != null && addedApplicants != null && addedApplicants.size()>0) {
						SsapApplicant addedSsapApplicant = addedApplicants.get(0);
						List<EnrollmentMemberDataDTO> enrollmentDTOList = enrolleeData.get(addedSsapApplicant.getApplicantGuid());
						if(enrollmentDTOList != null && !enrollmentDTOList.isEmpty()) {
							for(EnrollmentMemberDataDTO enrollmentDTO : enrollmentDTOList) {
								if("Health".equalsIgnoreCase(enrollmentDTO.getInsuranceType())) {
									referralLCENotificationDTO.setCoverageStartDate(DateUtil.dateToString(enrollmentDTO.getMemberCoverageStartDate(), "MM/dd/yyyy"));
									referralLCENotificationDTO.setPlanName(enrollmentDTO.getPlanName());
									referralLCENotificationDTO.setMemberNames(memberNameList);
									break;
								}
							}
						}
					}
				}
			}
		}
		
		return generateNotice("EE052AutomationAddRemove", ssapApplication, moduleId, referralLCENotificationDTO);
	}

	private String generateCSPlanAndRTChange(String caseNumber, String noticeTemplateName, boolean isHealthDisEnrolled, boolean isDentalDisEnrolled, Date terminationDate) throws NoticeServiceException {
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);

		int moduleId = extractHouseholdId(ssapApplication);
		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, moduleId);

		referralLCENotificationDTO.setIsHealthDisEnrolled(isHealthDisEnrolled ? "Y" : "N");
		referralLCENotificationDTO.setIsDentalDisEnrolled(isDentalDisEnrolled ? "Y" : "N");
		referralLCENotificationDTO.setTerminationDate(terminationDate);
		return generateNotice(noticeTemplateName, ssapApplication, moduleId, referralLCENotificationDTO);
	}

	private String generate(String caseNumber, String noticeTemplateName) throws NoticeServiceException {
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);

		int moduleId = extractHouseholdId(ssapApplication);
		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, moduleId);

		return generateNotice(noticeTemplateName, ssapApplication, moduleId, referralLCENotificationDTO);
	}
	
	private String generateSEPEventNoticeForQEP(String caseNumber, String noticeTemplateName,boolean triggerDocumentRequiredNoticeFlag) throws NoticeServiceException {
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);

		int moduleId = extractHouseholdId(ssapApplication);
		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, moduleId);
		referralLCENotificationDTO.setTriggerDocumentRequiredNoticeFlag(triggerDocumentRequiredNoticeFlag);
		return generateNotice(noticeTemplateName, ssapApplication, moduleId, referralLCENotificationDTO);
	}

	private ReferralLCENotificationDTO populateReferralLCENotificationDTO(Map<String, Object> enrollmentIdMap, SsapApplication ssapApplication, int moduleId) {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		List<String> healthEnrolleeList = new ArrayList<String>();
		List<String> dentalEnrolleeList = new ArrayList<String>();

		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, moduleId);
		if (null != enrollmentIdMap.get(ReferralConstants.HEALTH_ENROLLMENT_DTO)) {
			EnrollmentShopDTO healthEnrollmentShopDTO = (EnrollmentShopDTO) enrollmentIdMap.get(ReferralConstants.HEALTH_ENROLLMENT_DTO);

			Float newPremium = healthEnrollmentShopDTO.getMonthlyPremium();
			referralLCENotificationDTO.setNewHealthPremiumBeforeCredit(df.format(newPremium));
			List<EnrolleeShopDTO> enrolleeShopDTOList = healthEnrollmentShopDTO.getEnrolleeShopDTOList();
			for (EnrolleeShopDTO enrolleeShopDTO : enrolleeShopDTOList) {
				healthEnrolleeList.add(ReferralUtil.fullName(enrolleeShopDTO.getFirstName(), null, enrolleeShopDTO.getLastName()));
			}
			referralLCENotificationDTO.setHealthEnrollees(healthEnrolleeList);
		}
		if (null != enrollmentIdMap.get(ReferralConstants.DENTAL_ENROLLMENT_DTO)) {
			EnrollmentShopDTO dentalEnrollmentShopDTO = (EnrollmentShopDTO) enrollmentIdMap.get(ReferralConstants.DENTAL_ENROLLMENT_DTO);
			Float newPremium = dentalEnrollmentShopDTO.getMonthlyPremium();
			referralLCENotificationDTO.setNewDentalPremiumBeforeCredit(df.format(newPremium));
			List<EnrolleeShopDTO> enrolleeShopDTOList = dentalEnrollmentShopDTO.getEnrolleeShopDTOList();
			for (EnrolleeShopDTO enrolleeShopDTO : enrolleeShopDTOList) {
				dentalEnrolleeList.add(ReferralUtil.fullName(enrolleeShopDTO.getFirstName(), null, enrolleeShopDTO.getLastName()));
			}
			referralLCENotificationDTO.setDentalEnrollees(dentalEnrolleeList);
		}
		return referralLCENotificationDTO;
	}

	private String generateDenial(String caseNumber, String noticeTemplateName) throws NoticeServiceException, NotificationTypeNotFound {
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);

		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, 0);
		return generateNotice(noticeTemplateName, ssapApplication, referralLCENotificationDTO);
	}

	private String generateNotice(String noticeTemplateName, SsapApplication ssapApplication, int moduleId, ReferralLCENotificationDTO referralLCENotificationDTO) throws NoticeServiceException {
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = getName(ssapApplication);
		
		PreferencesDTO preferencesDTO = preferencesService.getPreferences(moduleId, false);
		Location location = getLocation(preferencesDTO);
		//Location location = getLocation(ssapApplication);
		
		// send EE061LCEDocumentRequiredNotice if status is PENDING
		if (ssapApplication.getValidationStatus() == ApplicationValidationStatus.PENDING){
			/**
			 *  this check as added as while QEP confirm life event for gated events
			 *  DocumentRequried notice is triggered twice as while confirming life event for 
			 *  MN two notices are triggered EE031QEPEventNotification & LCE04SEPEventNotification 
			 *  so to avoid 2 notices being sent this flag is added 
			 */
			if(referralLCENotificationDTO.isTriggerDocumentRequiredNoticeFlag()) {
				noticeTemplateName = "EE061LCEDocumentRequiredNotice";	
			}			
		}

		String relativePath = "cmr/" + moduleId + "/";
		String ecmFileName = noticeTemplateName + "_" + moduleId + (new TSDate().getTime()) + ".pdf";

		//String emailId = getEmailId(ssapApplication);
		String emailId = getEmailId(preferencesDTO);

		List<String> validEmails = emailId != null ? Arrays.asList(emailId) : null;
		
		GhixNoticeCommunicationMethod noticeCommunicationPref = getCommunicationMethod(preferencesDTO);
		
		Notice notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(referralLCENotificationDTO, ssapApplication, noticeTemplateName), relativePath, ecmFileName, moduleName, moduleId, validEmails,
		        DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), fullName, location, noticeCommunicationPref);

		return notice.getEcmId();
	}

	private GhixNoticeCommunicationMethod getCommunicationMethod(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null || preferencesDTO.getPrefCommunication() == null){
			throw new GIRuntimeException("preferencesDTO or preferencesDTO.getPrefCommunication() is null");
		}
		
		return preferencesDTO.getPrefCommunication();
	}

	private String getEmailId(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null){
			throw new GIRuntimeException("preferencesDTO is null");
		}
		
		return !StringUtils.isEmpty(preferencesDTO.getEmailAddress()) ? preferencesDTO.getEmailAddress() : null;
	}

	private Location getLocation(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null || preferencesDTO.getLocationDto() == null){
			throw new GIRuntimeException("preferencesDTO or preferencesDTO.getLocationDto() is null");
		}
		
		LocationDTO locationDto = preferencesDTO.getLocationDto();
		
		if (StringUtils.isEmpty(locationDto.getAddressLine1()) || StringUtils.isEmpty(locationDto.getCity()) || 
				StringUtils.isEmpty(locationDto.getState()) || StringUtils.isEmpty(locationDto.getZipcode())){
			throw new GIRuntimeException("preferencesDTO.getLocationDto() address fields missing");
		}
		
		Location location = new Location();
		location.setAddress1(locationDto.getAddressLine1());
		location.setAddress2(locationDto.getAddressLine2());
		location.setCity(locationDto.getCity());
		location.setState(locationDto.getState());
		location.setZip(locationDto.getZipcode());
		
		return location;
	}

	private String generateNotice(String noticeTemplateName, SsapApplication ssapApplication, ReferralLCENotificationDTO referralLCENotificationDTO) throws NoticeServiceException, NotificationTypeNotFound {

		String relativePath = "ssap/" + ssapApplication.getId() + "/";
		String ecmFileName = noticeTemplateName + "_" + ssapApplication.getId() + (new TSDate().getTime()) + ".pdf";

		Map<String, String> emailData = new HashMap<String, String>();
		emailData.put("To", getEmailId(ssapApplication));

		Map<String, String> tokens = setTokens(referralLCENotificationDTO, ssapApplication, noticeTemplateName);

		NotificationDto notificationDto = populateNotificationDto(getLocation(ssapApplication), referralLCENotificationDTO.getPrimaryApplicantName(), relativePath, ecmFileName);

		Notice notice = emailNotificationService.sendEmail(emailNotificationService.generateEmail(emailData, tokens, noticeTemplateName, notificationDto));
		return notice.getEcmId();
	}

	protected int extractHouseholdId(SsapApplication ssapApplication) {

		if (ssapApplication.getCmrHouseoldId() != null && ssapApplication.getCmrHouseoldId().intValue() != 0) {
			return ssapApplication.getCmrHouseoldId().intValue();
		} else {
			throw new GIRuntimeException("Cmr Household Id ");
		}

	}

	protected Location getLocation(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				if (ssapApplicant.getMailiingLocationId() != null) {
					return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
				} else if (ssapApplicant.getOtherLocationId() != null) {
					return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
				}
			}
		}

		return null;
	}

	protected ReferralLCENotificationDTO getReferralLCENotificationDTO(SsapApplication ssapApplication, int moduleId) {
		ReferralLCENotificationDTO referralLCENotificationDTO = new ReferralLCENotificationDTO();

		referralLCENotificationDTO.setUserName(getUserName(moduleId));

		referralLCENotificationDTO.setCaseNumber(ssapApplication.getCaseNumber());

		referralLCENotificationDTO.setPrimaryApplicantName(getName(ssapApplication));
		
		referralLCENotificationDTO.setApplicationValidationStatus(ssapApplication.getValidationStatus() != null ? ssapApplication.getValidationStatus().name() : StringUtils.EMPTY);

		referralLCENotificationDTO.setSsapApplicants(getUpdatedSSAPApplicants(ssapApplication.getSsapApplicants()));
		
		if(moduleId!=0){
			String houseHoldCaseId = cmrHouseholdRepository.findHHCaseIdByHouseholdId(moduleId);
			if(houseHoldCaseId!=null && checkStateCode(CA_STATE_CODE))
			{
				referralLCENotificationDTO.setMeta_external_household_case_id(houseHoldCaseId);	
			}
		}

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(ssapApplication.getId()); 
		// Added by pravin_r to avoid null poointer expection.
		// SSAP application event entry will not be there for some scenarios.

		if (ssapApplicationEvent != null) {
			referralLCENotificationDTO.setEnrollmentEndDate(ssapApplicationEvent.getEnrollmentEndDate());
			referralLCENotificationDTO.setKeepOnly(StringUtils.isEmpty(ssapApplicationEvent.getKeepOnly()) ? ReferralConstants.N : ssapApplicationEvent.getKeepOnly());

			if (referralLCENotificationDTO.getEnrollmentEndDate() != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
				referralLCENotificationDTO.setSpanishEnrollmentDate(formatter.format(referralLCENotificationDTO.getEnrollmentEndDate()));
			}
			
			referralLCENotificationDTO.setChangePlan(StringUtils.isEmpty(ssapApplicationEvent.getChangePlan()) ? ReferralConstants.N : ssapApplicationEvent.getChangePlan());
		
			if (ReferralUtil.listSize(ssapApplicationEvent.getSsapApplicantEvents()) != ReferralConstants.NONE){
				for (SsapApplicantEvent applicantEvent : ssapApplicationEvent.getSsapApplicantEvents()) {
					if (applicantEvent.getValidationStatus() == ApplicantEventValidationStatus.PENDING || applicantEvent.getValidationStatus() == ApplicantEventValidationStatus.REJECTED ){
						referralLCENotificationDTO.addGatedEventLabel(applicantEvent.getSepEvents().getLabel());
					}
				}
			}
		}
		
		if (ssapApplication.getValidationStatus() == ApplicationValidationStatus.PENDING && referralLCENotificationDTO.getGatedEventLabels().size() == 0){
			LOGGER.error("ERROR*** - Something went wrong. Application validation is PENDING with no PENDING applicant event! - Case number : " + ssapApplication.getCaseNumber());
		}

		return referralLCENotificationDTO;
	}

	protected String getEmailId(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getEmailAddress();
			}
		}

		return null;
	}

	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName();
			}
		}

		return null;
	}

	private List<String> getUpdatedSSAPApplicants(List<SsapApplicant> ssapApplicants) {
		List<String> ssapApplicantNames = new ArrayList<String>();
		ApplicantStatusEnum temp;
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			temp = ApplicantStatusEnum.fromValue(ssapApplicant.getStatus());
			if (null == temp || temp == ApplicantStatusEnum.NO_CHANGE) {
				continue;
			}
			if (ReferralConstants.DEMOGRAPHIC_APPLICANT_STATUS.contains(temp)) {
				ssapApplicantNames.add(ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName());
			}
		}
		return ssapApplicantNames;
	}

	protected SsapApplication getSSAPApplicationData(String caseNumber) {

		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);
		if (ssapApplications != null && ssapApplications.size() > 0) {
			return ssapApplications.get(0);
		}
		throw new GIRuntimeException("SSAP Id is null.Unable to fetch household id. Email cannot be triggered for" + caseNumber);
	}

	protected String getUserName(int householdId) {

		return cmrHouseholdRepository.findUserNameByCmrId(householdId);
	}

	@Transactional
	private Map<String, Object> getReplaceableObjectData(ReferralLCENotificationDTO referralLCENotificationDTO, SsapApplication ssapApplication, String noticeTemplateName) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();

		tokens.put("referralLCENotificationDTO", referralLCENotificationDTO);
		if (noticeTemplateName.equalsIgnoreCase("SEPEvent") && referralLCENotificationDTO.getEnrollmentEndDate() != null) {
			tokens.put("updateSubject", "Your Special Enrollment Period Expires on " + ReferralUtil.formatDate(referralLCENotificationDTO.getEnrollmentEndDate(), ReferralConstants.DEFDATEPATTERN));
        }
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		tokens.put("spanishDate", formatter.format(new TSDate()));
		tokens.put("todaysDate", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
		tokens.put("sepDenialResonEng", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ONLINE1_DENIAL_REASON_ENGLISH));
		tokens.put("sepDenialResonSpanish", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ONLINE1_DENIAL_REASON_SPANISH));
		tokens.put("Numberofdays", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SEP_NUMBER_OF_DAYS_TO_SUBMIT_AN_APPEAL));
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put("exgName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		tokens.put("exgCityName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		tokens.put("exgStateName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		tokens.put("zip", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		tokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put("exchangeFax", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		tokens.put("exchangeAddressEmail", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put("ssapApplicationId", ssapApplication.getId());
		tokens.put("medicaidName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.MEDICAID_NAME));
		List<SsapApplicationEvent> events = ssapApplicationEventRepository.findBySsapApplication(ssapApplication);
		boolean set = false;
		if(!CollectionUtils.isEmpty(events)) {
			List<SsapApplicantEvent> aaspAppEvent = ssapApplicantEventRepository.findBySsapAplicationEvent(events.get(0));
			if(!CollectionUtils.isEmpty(aaspAppEvent)) {
				tokens.put("event_date", DateUtil.dateToString(aaspAppEvent.get(0).getEventDate(), "MMM dd, YYYY"));
				try {
					SepEvents sepEvent = aaspAppEvent.get(0).getSepEvents();
					tokens.put("event", sepEvent.getName());	
				} catch (Exception e) {
					e.printStackTrace();
					tokens.put("event", "");
				}
				set = true;
			}
		}
		if(!set) {
			tokens.put("event", "");
			tokens.put("event_date", "");
		}
		return tokens;
	}

	private PlanAvailabilityAdapterResponse executePlanAvailability(LCEProcessRequestDTO lceProcessRequestDTO) {
		PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest = new PlanAvailabilityAdapterRequest();
		planAvailabilityAdapterRequest.setSsapApplicationId(lceProcessRequestDTO.getCurrentApplicationId());
		planAvailabilityAdapterRequest.setHealthEnrollees(lceProcessRequestDTO.getEnrolledApplicationAttributes().getHealthEnrollees());
		planAvailabilityAdapterRequest.setDentalEnrollees(lceProcessRequestDTO.getEnrolledApplicationAttributes().getDentalEnrollees());
		planAvailabilityAdapterRequest.setHealthEnrollmentId(lceProcessRequestDTO.getEnrolledApplicationAttributes().getHealthEnrollmentId());
		planAvailabilityAdapterRequest.setDentalEnrollmentId(lceProcessRequestDTO.getEnrolledApplicationAttributes().getDentalEnrollmentId());
		planAvailabilityAdapterRequest.setSsapApplicationId(lceProcessRequestDTO.getCurrentApplicationId());

		PlanAvailabilityAdapterResponse response = null;
		try {
			response = planAvailabilityAdapter.execute(planAvailabilityAdapterRequest);
		} catch (Exception e) {
		}
		if (response == null) {
			response = new PlanAvailabilityAdapterResponse();
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		} else {
			response.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}

		return response;
	}

	private NotificationDto populateNotificationDto(final Location location, final String userFullName, final String relativePath, final String ecmFileName) {
		NotificationDto notificationDto = new NotificationDto();

		notificationDto.setLocation(location);
		notificationDto.setUserFullName(userFullName);
		notificationDto.setEcmFileName(ecmFileName);
		notificationDto.setEcmRelativePath(relativePath);
		notificationDto.setPrintable(true);

		return notificationDto;
	}

	private Map<String, String> setTokens(ReferralLCENotificationDTO referralLCENotificationDTO, SsapApplication ssapApplication, String noticeTemplateName) throws NoticeServiceException {
		Map<String, String> tokens = new HashMap<String, String>();

		// tokens.put("referralLCENotificationDTO", referralLCENotificationDTO);
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));

		tokens.put("caseNumber", referralLCENotificationDTO.getCaseNumber());
		tokens.put("primaryApplicantName", referralLCENotificationDTO.getPrimaryApplicantName());

		tokens.put("spanishDate", formatter.format(new TSDate()));
		tokens.put("todaysDate", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
		tokens.put("sepDenialResonEng", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ONLINE1_DENIAL_REASON_ENGLISH));
		tokens.put("sepDenialResonSpanish", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ONLINE1_DENIAL_REASON_SPANISH));
		tokens.put("Numberofdays", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SEP_NUMBER_OF_DAYS_TO_SUBMIT_AN_APPEAL));

		return tokens;
	}

	@Override
	public String generateAutomationNoticeEE060(String caseNumber) throws NoticeServiceException {
		return generate(caseNumber, "EE060AutomationNotice");
	}
	
	@Override
	public String generateLceDocumentRequiredNotice(String caseNumber) throws Exception {
		return generate(caseNumber, "EE061LCEDocumentRequiredNotice");
	}
	
	public static boolean checkStateCode(String code){
 	 	String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
 	 	return stateCode.equalsIgnoreCase(code);
 	}
	
	private String getEnrollmentsByMember(List<SsapApplicant> ssapApplicants, String coverageYear) {
		List<String> applicantGuidList = new ArrayList<String>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidList.add(ssapApplicant.getApplicantGuid());
		}
		
		List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
		enrollmentStatusList.add(EnrollmentStatus.PENDING);
		enrollmentStatusList.add(EnrollmentStatus.CONFIRM);
		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setMemberIdList(applicantGuidList);
		enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
		
		List<String> coverageYearList = new ArrayList<String>();
		coverageYearList.add(coverageYear);
		enrollmentRequest.setCoverageYearList(coverageYearList);
		String enrollmentResponseStr = ghixRestTemplate.exchange(EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_MEMBERID_LIST, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
	
		return enrollmentResponseStr;
	}
	
}
