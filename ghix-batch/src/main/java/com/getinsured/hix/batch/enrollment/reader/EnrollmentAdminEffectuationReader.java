package com.getinsured.hix.batch.enrollment.reader;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

public class EnrollmentAdminEffectuationReader  implements ItemReader<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAdminEffectuationReader.class);
	String fileName;
	int counter = 0;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader EnrollmentAdminEffectuationReader");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			fileName =ec.getString("fileResource");

		}
	}
	@Override
	public String read() throws Exception, UnexpectedInputException,ParseException, NonTransientResourceException {
		if(counter == 0 ){
			counter++;
		return fileName;
		}else{
			return null;
		}
		
	}

}
