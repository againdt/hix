import { adjustedFlowActionTypes } from '../constants/actionTypes';

export const initialState = [];

export default (state = initialState, action) => {
    switch (action.type) {
    case adjustedFlowActionTypes.ADJUST_FLOW:
        return action.value;
    case adjustedFlowActionTypes.CLEAR_STATE:
        return initialState;
    default: return state;
    }
};