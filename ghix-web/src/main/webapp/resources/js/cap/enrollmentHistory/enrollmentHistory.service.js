
(function() {
	"use strict";
	angular.module("enrollmentHistoryApp").factory("enrollmentHistoryService", enrollmentHistoryService);
	enrollmentHistoryService.$inject = ['$http', '$q'];
	
	function enrollmentHistoryService($http, $q){
		var apiGetRequest = function(url) {
			return $http({
				method: "GET",
				url: url
			});
		};
		
		var apiPostRequest = function(url, data, headers,transformResponse) {
			return $http({
				method: "POST",
				url: url,
				data: data,
				headers:headers == undefined? undefined : headers,
				transformResponse:transformResponse == undefined? undefined : transformResponse
			});	
		};
		
		return{
			relationshipOrderBy: relationshipOrderBy,
			getEnrollmentEditToolErrorMessage: getEnrollmentEditToolErrorMessage,
			getUIDateFormat : getUIDateFormat,
			apiGetResponse: function(url) {
				return apiGetRequest(url);
			},
			apiPostResponse: function(url, data, headers, transformResponse) {
				return apiPostRequest(url, data, headers, transformResponse);
			}
		};
		
		function relationshipOrderBy(enrollee){
			var order = 4;
			switch(enrollee.relationshipToSubscriber) {
				case 'Self':
					order = 1;
					break;
				case 'Spouse':
					order = 2;
					break;
				case 'Child':
					order = 3;
					break;	
				default:
					order = 4;
			}	
			return order;
		}
		
		function getEnrollmentEditToolErrorMessage(errorCode, errMessage){
			var errorMessage = '';
			switch(errorCode) {
				case 200:
					errorMessage = 'Your change has been successfully updated.';
					break;
				case 201:
					errorMessage = 'Request is null or empty. Please contact your Technical Support Team.';
					break;
				case 202:
					errorMessage = 'Action is null or empty. Please contact your Technical Support Team.';
					break;
				case 203:
					errorMessage = 'Enrollment ID is null or empty. Please contact your Technical Support Team.';
					break;
				case 204:
					errorMessage = 'No enrollment found for given ID. Please contact your Technical Support Team.';
					break;
				case 205:
					errorMessage = 'No member found for given Exchange Individual Identifier. Please contact your Technical Support Team.';
					break;
				case 206:
					errorMessage = 'A change has been made to this enrollment outside of the requested dates. If you would like to override the changes, please contact your Technical Support Team. (Effective dates exist outside coverage)';
					break;
				case 207:
					errorMessage = 'A member’s enrollment dates fall outside of the dates entered. Please verify dates and enrollments for each member.';
					break;
				case 208:
					errorMessage = 'Member level dates are not within the dates of the overall enrollment. Please update the household enrollment before adjusting the individual member.';
					break;	
				case 210:
					errorMessage = 'The date entered will cause member\'s Start date to be before their birth date. Please review the dates and try again.';
					break;
				case 211:
					errorMessage = 'The date entered will cause member\'s End Date to be beyond the date of death. Please review the dates and try again.';
					break;
				case 212:
					errorMessage = 'The date entered will cause the Enrollment End Date to be before the Start Date. Please review and correct the dates.';
					break;
				case 213:
					errorMessage = 'Date filed is blank. Please enter a valid date.';
					break;	
				case 214:
					errorMessage = 'The enrollment you are trying to edit is Cancelled and cannot be edited. Please contact Technical Support.';
					break;
				case 215:
					errorMessage = 'The date(s) entered are not within the correct Coverage Year.';
					break;
				case 216:
					errorMessage = 'Failure to apply changes due to another overlapping enrollment. Please contact Technical Support.';
					break;
				case 217:
					errorMessage = 'The provided Member Benefit End Date is smaller than Member Benefit Start Date.';
					break;
				case 218:
					errorMessage = 'The Subscriber\'s Coverage Start/End dates do not match the Enrollment\'s Start/End dates. Please review the dates and try again.';
					break;
				case 219:
					errorMessage = 'Technical Error Occurred. To edit this Enrollment, please reinstate through the application.';
					break;
				case 220:
					errorMessage = 'Coverage year mismatch, Please verify enrollment and member date';
					break;
				case 227:
					errorMessage = errMessage;
					break;
				case 299:
					errorMessage = 'Failure to apply changes. Please contact Technical Support.';
					break;
				case 1000:
					errorMessage = 'The provided Enrollment Start Date and Subscriber\'s Benefit Start Dates do not match. Please review the dates and try again.';
					break;
				case 1001:
					errorMessage = 'The provided Enrollment End Date and Subscriber\'s Benefit End Dates do not match. Please review the dates and try again.';
					break;
				case 1002:
					errorMessage = 'The provided Enrollment Coverage dates do not match with Subscriber\'s Benefit dates. Please review the dates and try again.';
					break;
				case 1003:
					errorMessage = 'The provided Enrollment End Date is Less than Enrollment Start Date.';
					break;
				case 1004:
					errorMessage = 'This enrollment cannot be reinstated because a subsequent application has been submitted. If you need to reinstate this enrollment, please cancel any enrollments that have been created under the new application and cancel the new application first before attempting to reinstate this enrollment. ';
					break;
				default:
					errorMessage = 'We apologize for the technical issue. Please try again after 10 minutes.';
			}
			
			return errorMessage;
		}
		

		function getUIDateFormat(dbDate) {
			if (dbDate) {
				var formattedDate = new Date(dbDate);

				var dd = formattedDate.getDate();
				var mm = formattedDate.getMonth() + 1;
				var yyyy = formattedDate.getFullYear();

				dd = dd < 10 ? '0' + dd : dd;
				mm = mm < 10 ? '0' + mm : mm;

				return mm + "/" + dd + "/" + yyyy;
			}
			return null;
		}
		
	}
	
})();