package com.getinsured.eligibility.at.ref.service.migration;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.getinsured.eligibility.active.enrollment.service.ActiveEnrollmentService;
import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.eligibility.at.ref.service.ReferralCancelService;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.resp.si.dto.SsapApplicationCmrCreatedDto;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

public abstract class BaseDeterminationClassMigration {
	private static final Logger LOGGER = Logger.getLogger(BaseDeterminationClassMigration.class);
	
	@Autowired
	@Qualifier("activeEnrollmentService")
	private ActiveEnrollmentService activeEnrollmentService;

	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Autowired
	@Qualifier("referralOEService")
	private ReferralOEService referralOEService;

	@Autowired
	private ReferralCancelService referralCancelService;
	 
	@Autowired
	@Qualifier("qleValidationMigrationService")
	private QLEValidationService qleValidationService;
	
	@Autowired private IUserRepository iUserRepository;

	ApplicationsWithSameIdDTO populateDataInDto(SsapApplication ssapApplication) {
		ApplicationsWithSameIdDTO fieldData = new ApplicationsWithSameIdDTO();

		fieldData.setApplicationId(ssapApplication.getId());
		fieldData.setStatus(ssapApplication.getApplicationStatus());
		fieldData.setCmrId(ReferralUtil.convertToInt(ssapApplication.getCmrHouseoldId()));
		fieldData.setCoverageYear(ssapApplication.getCoverageYear());
		fieldData.setFinancialAssistance(ssapApplication.getFinancialAssistanceFlag());

		return fieldData;
	}

	boolean isEnrollmentActive(SsapApplication enApp) {
		return activeEnrollmentService.isEnrollmentActive(enApp.getId(), ReferralConstants.EXADMIN_USERNAME);
	}

	long retrieveComparedApplicationId(List<SsapApplication> listOfApps, Set<BigDecimal> cmrIds) {
		if (ReferralUtil.listSize(listOfApps) == ReferralConstants.ONE) {
			LOGGER.info("Found one Enrolled Application Only - " + listOfApps.get(0).getId());
			return listOfApps.get(0).getId();
		} else {
			if (ReferralUtil.collectionSize(cmrIds) == ReferralConstants.ONE) {
				LOGGER.info("Found Multiple Enrolled Applications, But One Consumer Only Only - " + listOfApps.get(0).getId());
				return listOfApps.get(0).getId();
			} else {
				Set<SsapApplicationCmrCreatedDto> data = new TreeSet<>();
				for (SsapApplication ssapApplication : listOfApps) {
					data.add(new SsapApplicationCmrCreatedDto(ssapApplication.getId(), cmrHouseholdRepository.findCreatedDateOfCmr(ssapApplication.getCmrHouseoldId().intValue()), ssapApplication.getCreationTimestamp(), ssapApplication
					        .getCmrHouseoldId()));
				}
				LOGGER.info("Found Multiple Enrolled Applications and Multiple Consumers - " + data);
				return ((SsapApplicationCmrCreatedDto) data.iterator().next()).getSsapApplicationId();
			}
		}
	}

	long retrieveLastComparedToApplication(List<SsapApplication> sameYearApplications) {
		if(sameYearApplications  != null && !sameYearApplications.isEmpty()) {
			return sameYearApplications.get(0).getId();
		}
		return ReferralConstants.NONE;
	}

	long retrieveComparedToApplication(List<SsapApplication> applications, long coverageYear) {
		SsapApplication returnApplication = null;
		for (SsapApplication ssapApplication : applications) {
			if (ReferralConstants.Y.equals(ssapApplication.getFinancialAssistanceFlag()) && ssapApplication.getCoverageYear() == coverageYear) {
				if (returnApplication == null) {
					returnApplication = ssapApplication;
				}
				if (ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())) {
					return ssapApplication.getId();
				}
			}
		}
		return (returnApplication == null ? ReferralConstants.NONE : returnApplication.getId());
	}

	void closeApplications(List<SsapApplication> otherApps) {
		for (SsapApplication ssapApplication : otherApps) {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("ID - CLOSE " + ssapApplication.getApplicationStatus() + " APP...");
			}
			closeApplication(ssapApplication);
		}
	}

	boolean closeApplication(SsapApplication erApp) {
		/* close app */
		//ssapApplicationService.closeApplication(erApp.getId());
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		qleValidationService.discardApp(erApp.getCaseNumber(),  ApplicationStatus.CLOSED, updateUser.getId());
		/* If application status is other than UC then dont ignore reseting activation object (as there is none) */
		if (!ApplicationStatus.UNCLAIMED.getApplicationStatusCode().equals(erApp.getApplicationStatus())){
			return true;
		}
		
		int id = (int) erApp.getId();
		/* call Referral API to Cancel Activation and from result determine whether UC was LCE */
		return referralCancelService.cancelActivation(id);
	}

	boolean isQEPeriod(AccountTransferRequestDTO accountTransferRequestDTO) {
		String currentDate = getCurrentDate(accountTransferRequestDTO);
		String oeEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
		String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		String previousOEEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE);
		if (currentCoverageYear.equals(String.valueOf(accountTransferRequestDTO.getCoverageYear()))) {
			return (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeEndDate), ReferralUtil.convertStringToDate(currentDate)) > 0);
		} else {
			return (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(previousOEEndDate), ReferralUtil.convertStringToDate(currentDate)) > 0);
		}
	}

	boolean isOEPeriod(AccountTransferRequestDTO accountTransferRequestDTO) {
		String currentDate = getCurrentDate(accountTransferRequestDTO);
		String oeStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
		String oeEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
		String currentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
		String previousOEStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_START_DATE);
		String previousOEEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE);
		if (currentCoverageYear.equals(String.valueOf(accountTransferRequestDTO.getCoverageYear()))) {
			return ((ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeStartDate), ReferralUtil.convertStringToDate(currentDate))) >= 0 && (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(oeEndDate), ReferralUtil.convertStringToDate(currentDate)) <= 0));
		} else {
			return ((ReferralUtil.compareDate(ReferralUtil.convertStringToDate(previousOEStartDate), ReferralUtil.convertStringToDate(currentDate))) >= 0 && (ReferralUtil.compareDate(ReferralUtil.convertStringToDate(previousOEEndDate), ReferralUtil.convertStringToDate(currentDate)) <= 0));
		}
	}

	private String getCurrentDate(AccountTransferRequestDTO accountTransferRequestDTO){
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime activityDate = accountTransferRequestDTO.getAccountTransferRequestPayloadType().getTransferHeader().getTransferActivity().getActivityDate().getDateTime();
		return ReferralUtil.formatDate(activityDate.getValue().toGregorianCalendar().getTime(), ReferralConstants.DEFDATEPATTERN);
	}
}
