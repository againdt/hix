/**
 * Created by sellathurai_s on 3/20/17.
 */

angular
    .module('reconciliationWorkbenchApp')
		.controller(
				'discrepanyDetailCtrl',
				function($scope, $timeout, $stateParams, $state,
						discrepancyDetailService, searchReconciliation,
						dataStorageService) {

        $scope.currntOpenDiscrepancy = {};
        $scope.comments = [];
        $scope.visualSummary = {};

        var openDiscrepancies = [];

					// $scope.getColor = function(grid, row, col,
					// rowRenderIndex, colRenderIndex) {
					// // if
					// (row.entity.discrepancyFields.indexOf(col.colDef.field)
					// !== -1 ) {
        //     //     return 'fieldDiscrepancy';
        //     // }
        //     return 'fieldRegular';
        // };

					$scope.getColor = function(hasDiscrepancy) {
						return hasDiscrepancy ? 'fieldDiscrepancy'
								: 'fieldRegular';
            //     return 'fieldDiscrepancy';
        };
        
        $scope.backToSearch = function() {
        	$state.go('search');
        } 
        
        $scope.initialize = function() {
            var enrollmentData = $scope.getEnrollmentData();

            if (enrollmentData) {
                var requestJSON = {
								"hixEnrollmentId" : enrollmentData.enrollmentId
                };

							discrepancyDetailService
									.fetchVisualSummary(requestJSON)
									.then(
											function(summary) {
                        $scope.visualSummary = summary.discrepancyEnrollmentDTO;
                        $scope.visualSummary.enrollmentId = enrollmentData.enrollmentId;
                        $scope.visualSummary.membersList = summary.discrepancyEnrollmentDTO.discrepancyMemberDTOList;
                        $scope.gridOptionsForMembers.data = $scope.visualSummary.membersList;
                        $scope.visualSummary.premiumList =  summary.discrepancyEnrollmentDTO.discrepancyPremiumDTOList;
                        $scope.gridOptionsForMonths.data = $scope.visualSummary.premiumList;
                        $scope.encEnrollmentID = $scope.visualSummary.encEnrollmentID;
                        //$scope.setMinRowsToShow();
											}, function(result) {
                    });
            }
        };

        $scope.gridOptionsForMembers = {
						enableColumnResizing : true,
						enableSorting : false,
						enablePaging : false,
						enableColumnMenus : false,
						// headerRowHeight: 10,
						// minRowsToShow : 5,
						enableHorizontalScrollbar: 0,
						enableVerticalScrollbar: 0,
						
						columnDefs : [
                {
									field : 'relationshipWithSubscriber.hixValue',
									displayName : 'TYPE',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.relationshipWithSubscriber.hixValue}} <br> Issuer Value:{{row.entity.relationshipWithSubscriber.issuerValue}}'>{{ (row.entity.relationshipWithSubscriber.hixValue !== null || row.entity.relationshipWithSubscriber.hixValue !== '') ? row.entity.relationshipWithSubscriber.hixValue : row.entity.relationshipWithSubscriber.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.relationshipWithSubscriber
												&& row.entity.relationshipWithSubscriber.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },

                {
									field : 'memberFullName.hixValue',
									displayName : 'NAME',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.memberFullName.hixValue}} <br> Issuer Value:{{row.entity.memberFullName.issuerValue}}'>{{ (row.entity.memberFullName.hixValue !== null || row.entity.memberFullName.hixValue !== '') ? row.entity.memberFullName.hixValue : row.entity.memberFullName.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.memberFullName
												&& row.entity.memberFullName.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
                {
									field : 'memberDateOfBirth.hixValue',
									displayName : 'BIRTH DATE',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.memberDateOfBirth.hixValue}} <br> Issuer Value:{{row.entity.memberDateOfBirth.issuerValue}}'>{{ (row.entity.memberDateOfBirth.hixValue !== null || row.entity.memberDateOfBirth.hixValue !== '') ? row.entity.memberDateOfBirth.hixValue : row.entity.memberDateOfBirth.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.memberDateOfBirth
												&& row.entity.memberDateOfBirth.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    },
									cellFilter : 'date:\'dd-MM-yyyy\''
                },
                {
									field : 'gender.hixValue',
									displayName : 'GENDER',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.gender.hixValue}} <br> Issuer Value:{{row.entity.gender.issuerValue}}'>{{ (row.entity.gender.hixValue !== null || row.entity.gender.hixValue !== '') ? row.entity.gender.hixValue : row.entity.gender.issuerValue}}</div> ",
									cellClass : function(grid, row, col) {
										return row.entity.gender
												&& row.entity.gender.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
                {
									field : 'SSN.hixValue',
									displayName : 'SSN',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.SSN.hixValue}} <br> Issuer Value:{{row.entity.SSN.issuerValue}}'>{{ (row.entity.SSN.hixValue !== null  ||  row.entity.SSN.hixValue !== '') ? row.entity.SSN.hixValue : row.entity.SSN.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.SSN
												&& row.entity.SSN.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
                {
									field : 'memberEffectiveStartDate.hixValue',
									displayName : 'START DATE',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.memberEffectiveStartDate.hixValue}} <br> Issuer Value:{{row.entity.memberEffectiveStartDate.issuerValue}}'>{{ (row.entity.memberEffectiveStartDate.hixValue !== null || row.entity.memberEffectiveStartDate.hixValue !== '')? row.entity.memberEffectiveStartDate.hixValue : row.entity.memberEffectiveStartDate.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.memberEffectiveStartDate
												&& row.entity.memberEffectiveStartDate.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    },
									cellFilter : 'date:\'dd-MM-yyyy\''
                },
                {
									field : 'memberEffectiveEndDate.hixValue',
									displayName : 'END DATE',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.memberEffectiveEndDate.hixValue}} <br> Issuer Value:{{row.entity.memberEffectiveEndDate.issuerValue}}'>{{ (row.entity.memberEffectiveEndDate.hixValue !== null || row.entity.memberEffectiveEndDate.hixValue !== '')? row.entity.memberEffectiveEndDate.hixValue : row.entity.memberEffectiveEndDate.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.memberEffectiveEndDate
												&& row.entity.memberEffectiveEndDate.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    },
									cellFilter : 'date:\'dd-MM-yyyy\''
                },
                {
									field : 'tobaccoUsage.hixValue',
									displayName : 'TOBACCO',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.tobaccoUsage.hixValue}} <br> Issuer Value:{{row.entity.tobaccoUsage.issuerValue}}'>{{ (row.entity.tobaccoUsage.hixValue !== null || row.entity.tobaccoUsage.hixValue !== '') ? row.entity.tobaccoUsage.hixValue : row.entity.tobaccoUsage.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.tobaccoUsage
												&& row.entity.tobaccoUsage.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
                {
									field : 'exchgIndivIdentifier.hixValue',
									displayName : 'MEMBER ID',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.exchgIndivIdentifier.hixValue}} <br> Issuer Value:{{row.entity.exchgIndivIdentifier.issuerValue}}'>{{ ( row.entity.exchgIndivIdentifier.hixValue !== null || row.entity.exchgIndivIdentifier.hixValue !== '' ) ? row.entity.exchgIndivIdentifier.hixValue : row.entity.exchgIndivIdentifier.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.exchgIndivIdentifier
												&& row.entity.exchgIndivIdentifier.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
								} ]
        };
		
		$scope.getTableHeight = function() {
			var rowHeight = 30; // your row height
			var headerHeight = 30; // your header height
			return {
				height: ($scope.gridOptionsForMembers.data.length * $scope.gridOptionsForMembers.rowHeight + headerHeight) + "px"
			};
		};
		
        $scope.gridOptionsForMonths = {
						enableColumnResizing : true,
						enableSorting : false,
						enablePaging : false,
						enableColumnMenus : false,
					    minRowsToShow : 12,
						enableHorizontalScrollbar: 0,
						enableVerticalScrollbar: 0,
						columnDefs : [
								{
									field : 'monthName',
									displayName : 'MONTH'
								},
								{
									field : 'grossPremiumAmount.hixValue',
									displayName : 'GROSS PREMIUM',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.grossPremiumAmount.hixValue}} <br> Issuer Value:{{row.entity.grossPremiumAmount.issuerValue}}'>{{ (row.entity.grossPremiumAmount.hixValue !== null || row.entity.grossPremiumAmount.hixValue !== '') ? row.entity.grossPremiumAmount.hixValue : row.entity.grossPremiumAmount.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.grossPremiumAmount
												&& row.entity.grossPremiumAmount.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
								{
									field : 'aptcAmount.hixValue',
									displayName : 'APTC',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.aptcAmount.hixValue}} <br> Issuer Value:{{row.entity.aptcAmount.issuerValue}}'>{{ (row.entity.aptcAmount.hixValue !== null || row.entity.aptcAmount.hixValue !== '' ) ? row.entity.aptcAmount.hixValue : row.entity.aptcAmount.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.aptcAmount
												&& row.entity.aptcAmount.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
								{
									field : 'netPremiumAmount.hixValue',
									displayName : 'NET PREMIUM',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.netPremiumAmount.hixValue}} <br> Issuer Value:{{row.entity.netPremiumAmount.issuerValue}}'>{{ (row.entity.netPremiumAmount.hixValue !== null || row.entity.netPremiumAmount.hixValue !== '' ) ? row.entity.netPremiumAmount.hixValue : row.entity.netPremiumAmount.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.netPremiumAmount
												&& row.entity.netPremiumAmount.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
								{
									field : 'csrAmount.hixValue',
									displayName : 'CSR AMOUNT',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.csrAmount.hixValue}} <br> Issuer Value:{{row.entity.csrAmount.issuerValue}}'>{{ (row.entity.csrAmount.hixValue !== null  || row.entity.csrAmount.hixValue !== '')  ? row.entity.csrAmount.hixValue : row.entity.csrAmount.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.csrAmount
												&& row.entity.csrAmount.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
								{
									field : 'isFinancial.hixValue',
									displayName : 'RATING AREA',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.ratingArea.hixValue}} <br> Issuer Value:{{row.entity.ratingArea.issuerValue}}'>{{ (row.entity.ratingArea.hixValue !== null || row.entity.ratingArea.hixValue !== '') ? row.entity.ratingArea.hixValue : row.entity.ratingArea.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.ratingArea
												&& row.entity.ratingArea.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
                    }
                },
								{
									field : 'isFinancial.hixValue',
									displayName : 'FINANCIAL TYPE',
									cellTemplate : "<div class='ui-grid-cell-contents' data-toggle='tooltip' data-placement='top'  bs-tooltip data-original-title='Hix Value:{{row.entity.isFinancial.hixValue}} <br> Issuer Value:{{row.entity.isFinancial.issuerValue}}'>{{ (row.entity.isFinancial.hixValue !== null || row.entity.isFinancial.hixValue !== '') ? row.entity.isFinancial.hixValue : row.entity.isFinancial.issuerValue}}</div> ",

									cellClass : function(grid, row, col) {
										return row.entity.isFinancial
												&& row.entity.isFinancial.hasDiscrepancy ? 'fieldDiscrepancy'
												: 'fieldRegular'
									}
								} ]
        };

        var paginationOptionsForOpenDiscrepancies = {
						currentPage : 1,
						pageSize : 10,
						sortByField : null,
						sort : null,
						sortOrder : null
        };

        $scope.gridOptionsForOpenDiscrepancies = {
						paginationPageSizes : [
								paginationOptionsForOpenDiscrepancies.pageSize,
								paginationOptionsForOpenDiscrepancies.pageSize * 2,
								paginationOptionsForOpenDiscrepancies.pageSize * 3 ],
						paginationPageSize : paginationOptionsForOpenDiscrepancies.pageSize,
						minRowsToShow : paginationOptionsForOpenDiscrepancies.pageSize * 1,
						useExternalPagination : true,
						useExternalSorting : true,
						enableSorting : true,
						enableColumnMenus : false,
            // minRowsToShow: 5,
						columnDefs : [
								{
									field : 'reportedDate',
									displayName : 'REPORTED DATE'
								},
								{
									field : 'discrepancyCategory',
									displayName : 'CATEGORY'
								},
								{
									field : 'discrepancyLabel',
									displayName : 'NAME'
								},
								{
									field : 'memberId',
									displayName : 'MEMBER ID'
								},
								{
									field : 'hixValue',
									displayName : 'HIX VALUE'
								},
								{
									field : 'issuerValue',
									displayName : 'ISSUER VALUE'
								},
								{
									field : 'discrepancyStatus',
									displayName : 'STATUS',
									width : 100
								},
								{
									field : 'action',
									displayName : 'ACTION',
									width : '*',
									enableColumnMenu : false,
									cellTemplate : '<div class="ui-grid-vcenter"><span class="ui-grid-btn" style="margin-right: 5px;" ng-click="grid.appScope.showAddCommentsModal(row.entity)"><b>Add Comment</b></span><span class="ui-grid-btn" ng-click="grid.appScope.showSnoozeModal(row.entity)"><b>Hold</b></span></div>'
								} ],
						onRegisterApi : function(gridApi) {
                $scope.gridApi = gridApi;
							gridApi.core.on
									.sortChanged(
											$scope,
											function(grid, sortColumns) {
                    if (sortColumns.length == 0) {
                        paginationOptionsForOpenDiscrepancies.sort = null;
                        paginationOptionsForOpenDiscrepancies.sortOrder = null;
                    } else {
                        paginationOptionsForOpenDiscrepancies.sortOrder = sortColumns[0].sort.direction;
                        paginationOptionsForOpenDiscrepancies.sortByField = sortColumns[0].field;
                        paginationOptionsForOpenDiscrepancies.currentPage = 1;
                    }
												$scope
														.getOpenDiscrepancies(paginationOptionsForOpenDiscrepancies);
												if ($scope.gridApi.pagination
														.getPage() > 1) {
													$scope.gridApi.pagination
															.seek(1);
            }
                });

							gridApi.pagination.on
									.paginationChanged(
											$scope,
											function(newPage, pageSize) {
                    paginationOptionsForOpenDiscrepancies.currentPage = newPage;
                    paginationOptionsForOpenDiscrepancies.pageSize = pageSize;
                    $scope.gridOptionsForOpenDiscrepancies.virtualizationThreshold =  $scope.gridOptionsForOpenDiscrepancies.paginationPageSize * 2;
												$scope
														.getOpenDiscrepancies(paginationOptionsForOpenDiscrepancies);
                });
            }
        };

        var paginationOptionsForComments = {
						currentPage : 1,
						pageSize : 10,
						sortByField : null,
						sort : null,
						sortOrder : null
        };

        $scope.gridOptionsForComments = {
						paginationPageSizes : [
								paginationOptionsForComments.pageSize,
                                paginationOptionsForComments.pageSize * 2,
								paginationOptionsForComments.pageSize * 3 ],
						paginationPageSize : paginationOptionsForComments.pageSize,
						minRowsToShow : paginationOptionsForComments.pageSize * 1,
						useExternalPagination : true,
						useExternalSorting : true,
						enableSorting : true,
						enableColumnMenus : false,
            // headerRowHeight: 10,
            // minRowsToShow: 5,
            // rowHeight:22,
						columnDefs : [ {
							field : 'commentAddedOn',
							displayName : 'Comment Date'.toUpperCase(),
							width : 155
						}, {
							field : 'commentedByUser',
							displayName : 'Comment By'.toUpperCase(),
							width : 150
						}, {
							field : 'discrepancyLabel',
							displayName : 'Discrepancy Name'.toUpperCase(),
							width : 165
						}, {
							field : 'commentText',
							displayName : 'Comments'.toUpperCase()
						} ],
						onRegisterApi : function(gridApi) {
                $scope.gridApi = gridApi;
							gridApi.core.on
									.sortChanged(
											$scope,
											function(grid, sortColumns) {
                    if (sortColumns.length == 0) {
                        paginationOptionsForComments.sort = null;
                        paginationOptionsForComments.sortOrder = null;
                    } else {
                        paginationOptionsForComments.sortOrder = sortColumns[0].sort.direction;
                        paginationOptionsForComments.sortByField = sortColumns[0].field;
                        paginationOptionsForComments.currentPage = 1;
                    }
												$scope
														.getCommentsForDiscrepancies(paginationOptionsForComments);
												if ($scope.gridApi.pagination
														.getPage() > 1) {
													$scope.gridApi.pagination
															.seek(1);
                    }
                });

							gridApi.pagination.on
									.paginationChanged(
											$scope,
											function(newPage, pageSize) {
                    paginationOptionsForComments.currentPage = newPage;
                    paginationOptionsForComments.pageSize = pageSize;
                    $scope.gridOptionsForComments.virtualizationThreshold =  $scope.gridOptionsForComments.paginationPageSize * 2;
												$scope
														.getCommentsForDiscrepancies(paginationOptionsForComments);

                });
            }
        };

					$scope.getOpenDiscrepancies = function(paginationOptions) {
            var enrollmentData = $scope.getEnrollmentData();
            if (!enrollmentData) {
            } else {
                var requestJSON = {
								"hixEnrollmentId" : enrollmentData.enrollmentId,
								"discrepancyStatusList" : [ "OPEN" ]
                };

                if (paginationOptions)
								$scope.getPaginationOptions(requestJSON,
										paginationOptions);

							discrepancyDetailService
									.fetchOpenDiscrepancies(requestJSON)
									.then(
											function(discrepancies) {
                        openDiscrepancies = discrepancies;
												$scope.gridOptionsForOpenDiscrepancies.enablePaginationControls = openDiscrepancies.totalRecordCount > 5;
                                                if (openDiscrepancies.discrepancyDetailDTOList) {
													$timeout(
															function() {
                                $scope.gridOptionsForOpenDiscrepancies.totalItems = openDiscrepancies.totalRecordCount;
																$scope.gridOptionsForOpenDiscrepancies.enablePaginationControls = openDiscrepancies.totalRecordCount > 5;
																$scope.gridOptionsForOpenDiscrepancies.data = openDiscrepancies.discrepancyDetailDTOList ? openDiscrepancies.discrepancyDetailDTOList : [];
                            }, 0);
												} else
                            $scope.gridOptionsForOpenDiscrepancies.data = [];
											}, function(result) {
                    });
            }
        };

					$scope.getPaginationOptions = function(requestJSON,
							paginationOptions) {
						if (paginationOptions) {
                if (paginationOptions.pageSize)
                    requestJSON.pageSize = paginationOptions.pageSize;

                if (paginationOptions.currentPage)
                    requestJSON.pageNumber = paginationOptions.currentPage;

                if (paginationOptions.sortByField)
                    requestJSON.sortByField = paginationOptions.sortByField;

                if (paginationOptions.sortOrder)
                    requestJSON.sortOrder = paginationOptions.sortOrder;
            }
        };

					$scope.getCommentsForDiscrepancies = function(
							paginationOptions) {
            var enrollmentData = $scope.getEnrollmentData();
            var requestJSON = {
							"hixEnrollmentId" : enrollmentData.enrollmentId
            };

						if (paginationOptions)
							$scope.getPaginationOptions(requestJSON,
									paginationOptions);

						discrepancyDetailService
								.fetchCommentsForDiscrepancies(requestJSON)
								.then(
                function(comments) {
                    $scope.comments = comments;
											$timeout(
													function() {
														$scope.gridOptionsForComments.totalItems = $scope.comments.discrepancyCommentDTOList ? $scope.comments.totalRecordCount
																: 0;
                        $scope.gridOptionsForComments.enablePaginationControls = $scope.gridOptionsForComments.totalItems > 5;
														$scope.gridOptionsForComments.data = $scope.comments.discrepancyCommentDTOList ? $scope.comments.discrepancyCommentDTOList
																: [];
                    }, 0);
										}, function(result) {
                });
        };

					$scope.getMembersCount = function() {
            return memberResults.length;
        };

					$scope.getPremiumCount = function() {
            return monthResults.length;
        };

					$scope.getOpenDiscrepanciesCount = function() {
						return $scope.gridOptionsForOpenDiscrepancies
								&& $scope.gridOptionsForOpenDiscrepancies.totalItems ? $scope.gridOptionsForOpenDiscrepancies.totalItems
								: 0;
        };

					$scope.getCommentsCount = function() {
						return $scope.gridOptionsForComments
								&& $scope.gridOptionsForComments.totalItems ? $scope.gridOptionsForComments.totalItems
								: 0;
        };

					

					$scope.getEnrollmentData = function() {
						var enrollmentData = searchReconciliation
								.getEnrollment();

						if (!enrollmentData.enrollmentId) {
							enrollmentData = dataStorageService
									.getItem("enrollementData");
            }

            return enrollmentData;
        }

					$scope.setMinRowsToShow = function() {
            var maxRowToShow = 12;
						// if data length is smaller, we shrink. otherwise we
						// can do pagination.
						$scope.gridOptionsForMembers.minRowsToShow = Math.min(
								$scope.gridOptionsForMembers.data.length,
								maxRowToShow);
						$scope.gridOptionsForMembers.virtualizationThreshold = $scope.gridOptionsForMembers.minRowsToShow;

						$scope.gridOptionsForMonths.minRowsToShow = Math.min(
								$scope.gridOptionsForMonths.data.length,
								maxRowToShow);
						$scope.gridOptionsForMonths.virtualizationThreshold = $scope.gridOptionsForMonths.minRowsToShow;
        };

        $scope.initialize();

					$scope
							.getOpenDiscrepancies(paginationOptionsForOpenDiscrepancies);
					$scope
							.getCommentsForDiscrepancies(paginationOptionsForComments);

					$scope.showAddCommentsModal = function(openDiscrepancy) {
            $scope.currntOpenDiscrepancy = openDiscrepancy;
            $scope.currntOpenDiscrepancy.action =  "ADD_COMMENT";
            $scope.currntOpenDiscrepancy.header = "Add Comment ";

            $('#discrepancyCommentModal').modal({
							keyboard : false,
							backdrop : 'static'
            });
        };

					$scope.showSnoozeModal = function(openDiscrepancy) {
            $scope.currntOpenDiscrepancy = openDiscrepancy;
						$scope.currntOpenDiscrepancy.header = "Reason for Hold";
            $scope.currntOpenDiscrepancy.comment = "";
						$scope.currntOpenDiscrepancy.action = "HOLD";

            $('#discrepancyCommentModal').modal({
							keyboard : false,
							backdrop : 'static'
            });
        };

					$scope.emptyComments = function() {
            $scope.currntOpenDiscrepancy.comment = "";
        };

					$scope.addCommentsForDiscrpancy = function() {
            var requestJSON = {};
						if ($scope.currntOpenDiscrepancy.action === "ADD_COMMENT") {
                requestJSON = constructRequestJSON("ADD_COMMENT");
            }

						if ($scope.currntOpenDiscrepancy.action === "HOLD") {
							requestJSON = constructRequestJSON("HOLD");
            }

						if ($scope.currntOpenDiscrepancy.action === "SNOOZE_ALL") {
                requestJSON = constructRequestJSON("SNOOZE_ALL");
            }

						discrepancyDetailService
								.doDiscrepancyAction(requestJSON)
								.then(
                function(comments) {
                    $scope.initialize();
											$scope
													.getOpenDiscrepancies(paginationOptionsForOpenDiscrepancies);
											$scope
													.getCommentsForDiscrepancies(paginationOptionsForComments);
										}, function(result) {
                });
        };

        function constructRequestJSON(action) {
						var enrollmentData = searchReconciliation
								.getEnrollment();
						var requestJSON = {
							"discrepancyAction" : action,
							"discrepancyId" : $scope.currntOpenDiscrepancy.discrepancyId,
							"enrollmentId" : enrollmentData.enrollmentId,
							"commentText" : $scope.currntOpenDiscrepancy.comment
                };

            return requestJSON;
        }

    });

(function() {
	angular.module('reconciliationWorkbenchApp').directive('myUiGridResize',
			myUiGridResizeDirective);
	angular.module('reconciliationWorkbenchApp').directive('bsTooltip',
			bsTooltip);
	
	function bsTooltip(){
		var directive = {
			restrict : 'AE',
			link : function(scope, elem, attrs) {
				elem.tooltip();
			}
		};
		
		return directive;
	}

    /* @ngInject */
    function myUiGridResizeDirective(gridUtil, uiGridConstants) {
        return {
			restrict : 'A',
			require : 'uiGrid',
			link : function($scope, $elm, $attrs, uiGridCtrl) {
				$scope
						.$watch(
								$attrs.uiGrid + '.minRowsToShow',
								function(val) {
                    var grid = uiGridCtrl.grid;

									// Initialize scrollbars (TODO: move to
									// controller??)
                    uiGridCtrl.scrollbars = [];

                    // Figure out the new height
									var contentHeight = grid.options.minRowsToShow
											* grid.options.rowHeight;
									var headerHeight = grid.options.hideHeader ? 0
											: grid.options.headerRowHeight;
									var footerHeight = grid.options.showFooter ? grid.options.footerRowHeight
											: 0;
									var columnFooterHeight = grid.options.showColumnFooter ? grid.options.columnFooterHeight
											: 0;
									var scrollbarHeight = grid.options.enableScrollbars ? gridUtil
											.getScrollbarWidth()
											: 0;
									var pagerHeight = grid.options.enablePagination ? gridUtil
											.elementHeight($elm.children(
													".ui-grid-pager-panel")
													.height(''))
											: 0;

                    var maxNumberOfFilters = 0;
									// Calculates the maximum number of filters
									// in the columns
									angular
											.forEach(
													grid.options.columnDefs,
													function(col) {
														if (col
																.hasOwnProperty('filter')) {
                            if (maxNumberOfFilters < 1) {
                                maxNumberOfFilters = 1;
                            }
														} else if (col
																.hasOwnProperty('filters')) {
                            if (maxNumberOfFilters < col.filters.length) {
                                maxNumberOfFilters = col.filters.length;
                            }
                        }
                    });
									var filterHeight = maxNumberOfFilters
											* headerHeight;

									var newHeight = headerHeight
											+ contentHeight + footerHeight
											+ columnFooterHeight
											+ scrollbarHeight + filterHeight
											+ pagerHeight;

                    $elm.css('height', newHeight + 'px');
                    
//                    $scope.grid.grid = {
//                    	    'min-height': newHeight + "px"
//                    	};

                    grid.gridHeight = $scope.gridHeight = gridUtil
											.elementHeight($elm);
									

                    // Run initial canvas refresh
                    grid.refreshCanvas();
                });
            }
        };
    }
})();
