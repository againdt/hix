<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>


<script type="text/javascript" src="<gi:cdnurl value="/resources/js/preserveImgAspectRatio.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>
	<div class="row-fluid">
		<form class="form-vertical" id="frmApplication" name="frmApplication" action="<c:url value="/planmgmt/issuer/enrollmentrep/enrollments" />"  method="POST">
		<df:csrfToken/>

		<div class="gutter10">
			<div class="row-fluid">
				<h1><spring:message code='label.enrollments' /></h1>
				<div class="pull-right">
					<label class="inline-block margin10-r" for="applicableYear" style="vertical-align: super;"><spring:message code='label.enrollmentYear'/></label>
                	<select id="applicableYear" name="applicableYear" onChange="changeYear()" class="input-medium">
 	                   	<c:forEach var="year" items="${yearList}">
						<option <c:if test="${enrollmentSubscriberRequest.coverageYear == year}">selected</c:if> value="${year}" >${year}</option>
						</c:forEach>
                     </select>
         		</div>
        	</div>
        </div>

		<div id="searchFilterParentDiv" class="row-fluid ">
			<div id="rightpanel" class="gray">
				<div class="header">
					<h4 class="margin0"><spring:message code='label.refineresult' /></h4>
				</div>
				<div id="searchFilterDiv" class="toggle hide row-fluid" style="display: block;">
                    <div class="row-fluid">
                    	<div class="control-group span3">
                    		<label for="planname" class="control-label"><spring:message code='label.subscriberName'/></label>
                            <div class="controls">
                               <input type="text" name="name" id="name" value="${enrollmentSubscriberRequest.subscriberName}"  placeholder="<spring:message code='label.subscriberName'/>" class="span12" size="30" />
                            </div>
                        </div>  
                        <div class="control-group span3">
                        	<label for="planname" class="control-label"><spring:message code='label.policyId'/></label>
                            <div class="controls">
                               <input type="text" name="policyId" id="policyId" value="${enrollmentSubscriberRequest.enrollmentID}" placeholder="<spring:message code='label.exchangeAssignedPolicyID'/>" class="span12" size="30" />
                            </div>
                        </div> 
                        <div class="control-group span3">
                    		<label for="planname" class="control-label"><spring:message code='label.planNumber'/></label>
                            <div class="controls">
                            	<input type="text" name="plannumber" id="plannumber" value="${enrollmentSubscriberRequest.CMSPlanID}" placeholder="<spring:message code='label.planNumber'/>"  class="span12" size="30" />
                             </div>
                    	</div>   
                    	<div class="control-group span3">
                    		<label for="planname" class="control-label"><spring:message code='label.planType'/></label>
                            <div class="controls">
                            	<select name="insuranceType" id="insuranceType">
                            		<option value="ALL">All</option>
                            		<c:forEach var="insuranceType" items="${insuranceTypeList}">
                            			<c:if test="${'HLT' == fn:toUpperCase(insuranceType.lookupValueCode) || 'DEN' == fn:toUpperCase(insuranceType.lookupValueCode)}">
											<option <c:if test="${enrollmentSubscriberRequest.insuranceTypeLookupCode == insuranceType.lookupValueCode}">selected</c:if> value="${insuranceType.lookupValueCode}" >${insuranceType.lookupValueLabel}</option>
										</c:if>	
									</c:forEach>
                            	</select>   
                             </div>
                    	</div>   
                    </div>
                    <div class="row-fluid">      
                    	<div class="control-group span3">
                    		<label for="planname" class="control-label"><spring:message  code='label.status'/></label>
                            <div class="controls">  
                            	<select name="status" id="status">
                            		<option value="ALL">All</option>
                            		<c:forEach var="enrollmentStatus" items="${enrollmentStatusList}">
                            			<c:if test="${'ABORTED' != fn:toUpperCase(enrollmentStatus.lookupValueCode)}">
											<option <c:if test="${enrollmentSubscriberRequest.enrollmentStatus== enrollmentStatus.lookupValueCode}">selected</c:if> value="${enrollmentStatus.lookupValueCode}" >${enrollmentStatus.lookupValueLabel}</option>
										</c:if>	
									</c:forEach>
                            	</select>                                 
                            </div>
                    	</div>
                    	<div class="control-group span3">
                    		<label for="planname" class="control-label"><spring:message code='label.subscriberId'/> </label>
                            <div class="controls">
                                 <input type="text" name="subscriberid" id="subscriberid" value="${enrollmentSubscriberRequest.exchgSubscriberIdentifier}" placeholder="<spring:message code='label.subscriberId'/>" class="span12" size="30" />
                            </div>
                    	</div>
                    	<div class="control-group span3">
                    		<label for="planname" class="control-label"><spring:message code='label.last4SSNNumber'/></label>
                            <div class="controls">
                                <input type="text" name="ssn" maxlength="4" id="ssn" value="${enrollmentSubscriberRequest.SSNLast4Digit}" placeholder="<spring:message code='label.last4SSNNumber'/>" />
                            </div>
                    	</div>
                    </div>	
                    <div class="row-fluid">
                    	<div class="control-group span3">
                    		<label for="planname" class="control-label"><spring:message code='label.dobOfSubscriber'/></label>
                            <div class="controls">
                            	<div class="input-append date date-picker" id="date" data-date="" data-date-format="mm/dd/yyyy"> 	                	
	                				<input type="text" name="subscriberdob" id="subscriberdob" value="${enrollmentSubscriberRequest.subscriberDateOfBirth}" placeholder="<spring:message code='label.dobOfSubscriber'/>" size="10" />
	                			    <span class="add-on"><i class="icon-calendar"></i></span>
							   </div>
                            </div>
                    	</div> 
                    	<div class="control-group span5">
                    		<label for="planname" class="control-label">&nbsp;</label>
                    		 <div class="controls">
                    		 <input type="submit" name="submitbtn" onclick="submitFrm()" id="submitbtn" value="<spring:message code='label.go'/>" class="btn btn-primary"  title="<spring:message code='label.go'/>" />
                    		 
                    		 <c:if test="${fn:length(applicationlist) > 0}">	
                    		 	&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="exportbtn" id="exportbtn" onclick="exportData()" class="btn btn-primary" value="<spring:message code='label.export'/>"  title="<spring:message code='label.export'/>" >
                    		 </c:if>	 
                    		</div>  
                    	</div>
                     </div>                       
                    	
                   </div> 
       		 </div><!--end of search form  -->
       		  <input type="hidden" name="totalrecords" id="totalrecords" value="${totalrecords }"> 
        </div>
        </form>
	</div>
	<div class="row-fluid">
        <div>
       		<c:choose>
                    <c:when test="${fn:length(applicationlist) > 0}">
                        <table class="table table-striped">
                            <thead>
                                <tr class="header">                                     	
                                	<th scope="col"><spring:message code='label.subscriber'/></th>
                                    <th scope="col"><spring:message code='label.DOB'/></th>
                                    <th scope="col">SSN</th>						    
                                    <th scope="col"><spring:message code='label.policyId'/></th>
                                     <th scope="col"><spring:message code='label.planType'/></th>
                                    <th scope="col"><spring:message code='label.planNumber'/></th>
                                    <th scope="col"><spring:message code='label.enrollmentStatus'/></th>
                                    <th scope="col"><spring:message code='label.effectiveStartDate'/></th>
                                    <th scope="col"><spring:message code='label.subscriberId'/></th>						     					     
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${applicationlist}" var="application">
                            	<tr>
                                	<td>${application.subscriberFullName}</td>
                                    <td><fmt:formatDate value="${application.birthDate}" pattern="MM/dd/yyyy"/></td>
                                    <td>${application.lastFourDigitSSN}</td>
                                    <c:set var="encPolicyId" ><encryptor:enc value="${application.enrollmentID}" isurl="true"/> </c:set>
                                    <td><a href="<c:url value="/planmgmt/issuer/enrollmentrep/enrollmentdetails/${encPolicyId}" />">${application.enrollmentID}</a></td>
                                    <td>${application.insuranceTypeLookupLabel}</td>
                                    <td>${application.CMSPlanID}</td>
                                    <td>${application.enrollmentStatusLookupLabel}</td>
                                    <td><fmt:formatDate value="${application.enrollmentBenefitStartDate}" pattern="MM/dd/yyyy"/></td>
                                    <td>${application.exchgSubscriberIdentifier}</td>					
                                 </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <div class="center"><dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"  hideTotal="true"/></div>
                        
                    </c:when>
                    <c:otherwise>
                        <div class="alert alert-info"><spring:message code='label.recordsNotFound'/></div>
                    </c:otherwise>
                </c:choose>
        </div>
    </div><!-- end of .row-fluid -->
<script type="text/javascript">
function exportData(){
	$("#frmApplication").attr("action", "${pageContext.request.contextPath}" + "/planmgmt/issuer/enrollmentrep/exportreport");
	$("#frmApplication").attr("method", "POST");
	$("#frmApplication").attr("target", "_blank");
	$("#frmApplication").submit();
}

function changeYear(){
	$("#frmApplication").attr("action", "${pageContext.request.contextPath}" + "/planmgmt/issuer/enrollmentrep/enrollments");
	$("#frmApplication").attr("method", "POST");
	$("#frmApplication").attr("target", "");
	$("#frmApplication").submit();
}
function submitFrm(){
	$("#frmApplication").attr("action", "${pageContext.request.contextPath}" + "/planmgmt/issuer/enrollmentrep/enrollments");
	$("#frmApplication").attr("method", "POST");
	$("#frmApplication").attr("target", "");
	$("#frmApplication").submit();
}

$(document).ready(function(){
	$('.date-picker').datepicker({
		//startDate: '+'+ '${systemDate}' + 'd',
		autoclose: true,
	    format: 'mm/dd/yyyy'
	});	
		
});
</script>