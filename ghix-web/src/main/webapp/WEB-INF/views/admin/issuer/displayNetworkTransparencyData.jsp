<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>

<%@ page isELIgnored="false"%>
<style>
	h1 img {
	    height: auto;
	    width: auto;
	}
</style>

<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/manageissuer"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
        	<li><spring:message  code="pgheader.networkTransparencyStatus"/></li>
        </ul><!--page-breadcrumb ends-->		
		<h1 id="skip"><img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img"/>${issuerObj.name}</h1>
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
               <h4><spring:message  code="pgheader.issuerAbout"/></h4>
            </div>
			<jsp:include page="issuerDetailsLeftNav.jsp">
        			<jsp:param name="pageName" value="netTrans"/>
			</jsp:include>
		</div>
		<!-- end of span3 -->
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message  code="pgheader.networkTransparencyStatus"/></h4>
				<a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/loadNetworkTransparency/${encIssuerId}"/>"><spring:message  code="label.upload"/> </a>
			</div>
			<div class="header" style="margin-left:2000px;"></div>

			<form class="form-horizontal" id="frmIssuerHistory" name="frmIssuerHistory" action="#" method="POST">
				<df:csrfToken/>

				<display:table name="${serffDocList}" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" > 
					<display:column property="docName" titleKey="label.fileName" sortable="true" />
					<%-- HIX-89724: Display Plan Year for Network Breath list view --%>
					<display:column property="serffTrackNum" titleKey="label.planYear" sortable="true" />
					<display:column property="createdOn" titleKey="label.date"  format="{0,date,MMM dd, yyyy}" sortable="true" defaultorder="descending"/>
					<display:column  titleKey="label.download" href="${pageContext.request.contextPath}/admin/crosswalk/DownloadExcel" paramId="ecmId" paramProperty="ecmDocId"><spring:message  code="label.download"/></display:column>
					<display:setProperty name="paging.banner.some_items_found" value=''/>
					<display:setProperty name="paging.banner.all_items_found" value=''/>
					<display:setProperty name="paging.banner.group_size" value='50'/>
					<display:setProperty name="paging.banner.onepage" value=''/>
					<display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
					<display:setProperty name="paging.banner.page.selected" value='<li class="active"><a href="#">{0}</a></li>'/>
					<display:setProperty name="paging.banner.first" value='<span class="pagelinks"><div class="pagination center"><ul><li></li><li>{0}</li><li><a href="{3}">Next &gt;</a></li></ul></div></span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks"><div class="pagination center"><ul><li><a href="{2}">&lt; Prev</a></li><li>{0}</li><li></li></ul></div></span>'/>
					<display:setProperty name="paging.banner.full" value='<span class="pagelinks"><div class="pagination center"><ul><li><a href="{2}">&lt; Prev</a></li><li>{0}</li><li><a href="{3}">Next &gt;</a></li></ul></div></span>'/>
				</display:table>
			</form>
		</div>
	</div><!--  end of span9 -->
</div><!-- end row-fluid -->
