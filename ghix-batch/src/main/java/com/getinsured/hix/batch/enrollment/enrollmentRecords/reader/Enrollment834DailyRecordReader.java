package com.getinsured.hix.batch.enrollment.enrollmentRecords.reader;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.enrollmentRecords.IssuerFileList;
import com.getinsured.hix.batch.enrollment.skip.Enrollment834Out;
import com.getinsured.hix.batch.enrollment.xmlEnrollments.exceptions.InvalidRecordException;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.service.ValidationFolderPathService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRace;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentEvent;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

public class Enrollment834DailyRecordReader  implements ItemReader<Enrollment>{
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment834DailyRecordReader.class);
	Date startDate = null;
	Date endDate = 	null;
	boolean bIssuerValidated = false;
	String jobName;
	String enrollmentType;
	private ValidationFolderPathService validationFolderPathService = null;
	private UserService userService = null; 
	private ExecutionContext executionContext;
	private EnrollmentBatchService enrollmentBatchService;
	private IssuerFileList issuerFileList;
	private Enrollment834Out enrollment834Out;
	private List<Integer> enrollmentIds;
	private IEnrollmentRepository enrollmentRepository;
	int startIndex;
	int endIndex;
	int loopCount;
	int issuerSubList;
	private JobService jobService;
	long jobExecutionId = -1;
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	//private static final String ENROLLMENT_TYPE_INDIVIDUAL = "FI";
	
	public ValidationFolderPathService getValidationFolderPathService() {
		return validationFolderPathService;
	}

	public void setValidationFolderPathService(
			ValidationFolderPathService validationFolderPathService) {
		this.validationFolderPathService = validationFolderPathService;
	}

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) throws Exception{
		try{
			LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
			jobExecutionId = stepExecution.getJobExecution().getId();
			ExecutionContext ec = stepExecution.getExecutionContext();
			if(ec != null){
				executionContext = stepExecution.getJobExecution().getExecutionContext();
				enrollmentType = ec.getString("enrollmentType");
				startDate = (Date) ec.get("startDate");
				endDate = 	(Date) ec.get("endDate");
				startIndex=ec.getInt("startIndex");
				endIndex=ec.getInt("endIndex");
				issuerSubList= ec.getInt("issuerSubList");
				jobName=(String)stepExecution.getJobExecution().getJobInstance().getJobName();
				
				enrollmentIds=enrollment834Out.getIssuerEnrollmentIdMap().get(""+ec.getInt("issuerIds")).subList(startIndex, endIndex);
			}
		}catch(Exception e){
			if(executionContext != null){
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
			throw e;
		}
		
	}
	
	
	
		private void setEnrollmentsFor834Daily(Enrollment enrollment, Date startDate, Date endDate) throws InvalidRecordException {
		if(enrollment != null){
			List<Enrollee> enrolleesList=null;
			try{
			List<Enrollee> allEnrolleeList = enrollment.getEnrollees();
			try{
				if(isNotNullAndEmpty(enrollmentType) && enrollmentType.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_TYPE_SHOP)){
					if(enrollmentBatchService.isEnrollmentFromPendingToCancel(enrollment)){
						throw new InvalidRecordException("Enrollment is Cancelled from Pending for SHOP type enrollment id= " + enrollment.getId());					}
				}
				enrolleesList = filterEnrolleesForEDIDaily(allEnrolleeList, startDate,endDate);
			}catch(Exception e){
				throw new InvalidRecordException(e);
			}
			enrollment.setEnrollees(enrolleesList);
			}catch(InvalidRecordException e){
				throw e;
			}
			catch(Exception e){
				enrollment.setReaderStatus(EnrollmentConstants.ENROLLMENT_EVENT_STATUS_FAILED);
				enrollment.setReaderErrorMsg(e.getMessage());
			}
			if(enrollment.getReaderStatus()==null &&(enrolleesList == null || enrolleesList.isEmpty())){
				LOGGER.info(Thread.currentThread().getName() + " :  Skipping enrollment as Enrollee list is empty for enrollment id: " + enrollment.getId());
				throw new InvalidRecordException("Enrollment need not be extracted as it does not have any enrollee events in specified period " + enrollment.getId());
			}
		}
	}


	private List<Enrollee> filterEnrolleesForEDIDaily(List<Enrollee> allEnrollees, Date startDate,Date endDate) throws GIException{
		List<Enrollee> updatedEnrolleeList = null;
		try{
			if(allEnrollees!=null ){
				AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
				updatedEnrolleeList = new ArrayList<Enrollee>();
				for(Enrollee enrollee:allEnrollees){
					LOGGER.debug(Thread.currentThread().getName() +  " : Reader : filterEnrolleesForEDIDaily :: Started filtering for enrollee id="+enrollee.getId());
					if(enrollee.getPersonTypeLkp()!=null &&(enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.LOOKUP_PERSON_TYPE_ENROLLEE) || enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER) )){
							List<EnrollmentEvent> allEvents= enrollee.getEnrollmentEvents();
							List<EnrollmentEvent> createdEvents= new ArrayList<EnrollmentEvent>();
							Date eventCreationDate=null;
							for(EnrollmentEvent event:allEvents){
								// This event.getSendToCarrierFlag() can be null, in case of null we need to send this to carrier.
								// Don't send only when this flag is false. This is false in case of renewal issuer is same and terminate prior enrollment.
								if(event.getSendToCarrierFlag() != null && !Boolean.parseBoolean(event.getSendToCarrierFlag())){
									LOGGER.debug(Thread.currentThread().getName() + " : Reader: Ignoring event : " + event.getId() + " as event.getSendToCarrierFlag() is false for Enrollee: " + enrollee.getId());
									continue;
								}
								eventCreationDate=event.getCreatedOn();
								if((!(eventCreationDate.before(startDate))&&!(eventCreationDate.after(endDate)))  || (event.getExtractionStatus() != null && event.getExtractionStatus().equalsIgnoreCase(EnrollmentConstants.RESEND_FLAG) && !event.getCreatedOn().after(endDate))){
									if(event.getCreatedBy() == null || ((isNotNullAndEmpty(user) && isNotNullAndEmpty(event.getCreatedBy())) && (event.getCreatedBy().getId() != user.getId()))){
										createdEvents.add(event);
									}else{
								LOGGER.debug(Thread.currentThread().getName() + " : Reader: Ignoring event : " + event.getId() + " as " + (event.getCreatedBy() == null ? null : event.getCreatedBy().getId())  +" is not same as " +  user.getId() + " for Enrollee: " + enrollee.getId());
							}
								}
							}
							//if((createdEvents != null && !createdEvents.isEmpty())||
							//		(enrollee.getPersonTypeLkp() != null && enrollee.getPersonTypeLkp().getLookupValueCode().equals(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER))){
							if((createdEvents!=null && !createdEvents.isEmpty())){
								enrollee.setEnrollmentEvents(createdEvents);
								updatedEnrolleeList.add(enrollee);
								List<EnrolleeRace> enrolleeRaceList =  enrollee.getEnrolleeRace();
								
							}else{
						LOGGER.debug(Thread.currentThread().getName() + " : Reader: Ignoring enrollee : " + enrollee.getId() + " as there are no events in the provided date range");
					}
					}else{
						LOGGER.debug( (Thread.currentThread().getName() + " : Reader: Ignoring enrollee : " + enrollee.getId() + " as personTypeLkp is " + (enrollee.getPersonTypeLkp() == null? null : enrollee.getPersonTypeLkp().getLookupValueCode())));
					}
				}
			}else{
				LOGGER.debug(Thread.currentThread().getName() + " : Reader: Enrollee list passed to filterEnrolleesForEDIDaily is null ");
			}
		}catch(Exception e){
			LOGGER.error("Exception in filterEnrolleesForEDIDaily()"+e.getMessage());
			throw new GIException("Exception in filterEnrolleesForEDIDaily()"+e.getMessage(),e);
		}
		return updatedEnrolleeList;
	}
	
	

	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(
			EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}

	public IssuerFileList getIssuerFileList() {
		return issuerFileList;
	}

	public void setIssuerFileList(IssuerFileList issuerFileList) {
		this.issuerFileList = issuerFileList;
	}

	public Enrollment834Out getEnrollment834Out() {
		return enrollment834Out;
	}

	public void setEnrollment834Out(Enrollment834Out enrollment834Out) {
		this.enrollment834Out = enrollment834Out;
	}

	@Override
	public Enrollment read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		try{
			
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				issuerFileList.deleteAllFiles();
				issuerFileList.clearFiles();
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
		if(executionContext != null){
				Object jobExecutionStatus = executionContext.get("jobExecutionStatus");
				String jobStatus = null;
				
				if(jobExecutionStatus != null){
					jobStatus = (String)jobExecutionStatus;
				}
				
				if(jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0){
					LOGGER.info(Thread.currentThread().getName() +  " : Terminating thread " + Thread.currentThread().getName() + " due to failure of other thread execution");
					throw new GIException("Terminating thread " + Thread.currentThread().getName() + "due to failure of other thread execution");
				}
			}
		
		if(enrollmentIds!=null && !enrollmentIds.isEmpty()){
			if(loopCount<enrollmentIds.size()){
				loopCount++;
				Enrollment enrollment= enrollmentRepository.findById( enrollmentIds.get(loopCount-1));
				if(enrollment != null && enrollment.getIssuerId() != null){
					setEnrollmentsFor834Daily(enrollment, startDate,endDate);
				}else{
					if(enrollment == null){
						LOGGER.info(Thread.currentThread().getName() + "Reader: setEnrollmentsFor834Daily was not called as Enrollment is null");
					}else{
						LOGGER.info(Thread.currentThread().getName() + "Reader: setEnrollmentsFor834Daily was not called as issuer is null for enrollment : " + enrollment.getId());
					}
				}
				return enrollment;
			}else{
				return null;
			}
			
		}
		}catch(InvalidRecordException e){
			throw e;
		}	
		catch(Exception e){
			if(executionContext != null){
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
			throw e;
		}
			return null;
	}
	public IEnrollmentRepository getEnrollmentRepository() {
		return enrollmentRepository;
	}

	public void setEnrollmentRepository(IEnrollmentRepository enrollmentRepository) {
		this.enrollmentRepository = enrollmentRepository;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	
}
