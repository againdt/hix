import normalizeStringToBooleanValue from './normalizeStringToBooleanValue';

export const navigationBarEnhancer = (...args) => {

    const [ e, btnRef, btn2Ref, navRef, ulRef, ul2Ref ] = args;

    const ids = [
        'get-assistance_01',
        'get-assistance_02',
        'my-account'
    ];

    const whichIsExtended = (buttons => {
        return (btn => {
            return btn ? btn.id : null;
        })(buttons.find(btn => btn && normalizeStringToBooleanValue(btn.getAttribute('aria-expanded'))));
    })([ btnRef.current, btn2Ref.current ]);

    if(typeof e.target.className === 'string' &&  e.target.className.indexOf('collapse') !== -1) {
        return null;
    }

    function checkParent(node) {
        if(node.id === 'root') {
            return false;
        }
        if(node.hasAttribute('role')) {
            return true;
        }
        if(node.parentNode) {
            return checkParent(node.parentNode);
        } else {
            return false;
        }
    }

    if(navRef.current.className.indexOf('is-visible') !== -1 && !checkParent(e.target)) {
        navRef.current.className = 'usa-nav main__nav';
        document.querySelector('body').className = '';
    }

    if(ids.every(id => id !== e.target.id && id !== e.target.parentNode.id)) {
        if(whichIsExtended === 'get-assistance_01' || whichIsExtended === 'get-assistance_02') {
            btnRef.current.setAttribute('aria-expanded', 'false');
            ulRef.current.setAttribute('aria-hidden', 'true');
        }
        if(whichIsExtended === 'my-account') {
            btn2Ref.current.setAttribute('aria-expanded', 'false');
            ul2Ref.current.setAttribute('aria-hidden', 'true');
        }
    }
};