package com.getinsured.eligibility.at.ref.service;

import static com.getinsured.eligibility.at.ref.service.LceProcessHandlerService.ERROR_LCE_NOTIFICATION;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;

/**
 * @author chopra_s
 * 
 */
@Component("lceDemoHandlerService")
@Scope("singleton")
public class LceDemoHandlerService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceDemoHandlerService.class);

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceDemoHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		final SsapApplication enrolledApplication = loadApplication(enrolledApplicationId);
		createDemoEvent(currentApplication, enrolledApplication);
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);//HIX-108047
		currentApplication.setEffectiveDate(enrolledApplication.getEffectiveDate());//Set effective date from previous enrolled application as we won't receive callback from enrollment in case of demo changes.
		updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		final String status = moveEnrollmentToNewApplication(currentApplication, enrolledApplication);
		if (!status.equals(GhixConstants.RESPONSE_SUCCESS)) {
			throw new GIRuntimeException(ERROR_MOVE_ENROLLMENT + currentApplication.getId());
		}
		
		//HIX-109703
		String previousApplicationStatus = enrolledApplication.getApplicationStatus();
		String previousApplicationDentalStatus = enrolledApplication.getApplicationDentalStatus();
		
		super.closeSsapApplication(enrolledApplication);
		currentApplication.setApplicationStatus(previousApplicationStatus);
		currentApplication.setApplicationDentalStatus(previousApplicationDentalStatus);
		super.updateSsapApplication(currentApplication);
		//updateCurrentAppToEN(currentApplication);
		
		updateAllowEnrollment(currentApplication, Y);
		copyEHBAmount(currentApplication, enrolledApplication);
		
		triggerDemographicEmail(currentApplication);
		super.closePreviousERApplication(currentApplication,lceProcessRequestDTO);
		LOGGER.info("LceDemoHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	private void createDemoEvent(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Create Demographic SEP event - " + currentApplication.getId());
		ssapApplicationEventService.createDemoOnlyApplicationEvent(currentApplication.getId(), enrolledApplication.getId());
	}

	private void triggerDemographicEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger Demographic Email - Notice LCE1");
		try {
			referralLceNotificationService.generateDemographicChangeNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE1").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}
}
