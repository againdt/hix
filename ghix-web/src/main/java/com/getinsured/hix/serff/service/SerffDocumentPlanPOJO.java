package com.getinsured.hix.serff.service;

import java.util.Date;

/**
 * Class is used to retrieve data from SERFF_DOCUMENT & SERFF_PLAN_MGMT tables for Crosswalk and Network Rating.
 * @author Bhavin Parmar
 */
public class SerffDocumentPlanPOJO {

	private String docName;
	private String serffTrackNum;
	private String ecmDocId;
	private Date createdOn;

	public SerffDocumentPlanPOJO(String docName, String serffTrackNum, String ecmDocId, Date createdOn) {
		this.docName = docName;
		this.serffTrackNum = serffTrackNum;
		this.ecmDocId = ecmDocId;
		this.createdOn = createdOn;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getSerffTrackNum() {
		return serffTrackNum;
	}

	public void setSerffTrackNum(String serffTrackNum) {
		this.serffTrackNum = serffTrackNum;
	}

	public String getEcmDocId() {
		return ecmDocId;
	}

	public void setEcmDocId(String ecmDocId) {
		this.ecmDocId = ecmDocId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
}
