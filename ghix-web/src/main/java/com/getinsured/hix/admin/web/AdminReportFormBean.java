package com.getinsured.hix.admin.web;


public class AdminReportFormBean {
	private String issuerName=null;
	private String planName=null;
	private String planLevel=null;
	private String market=null;
	private String enrollcount=null;
	private String timePeriod=null;
	private String enrollmentCount=null;
	private String planNumber=null;
	private String costSharing=null;
	
	public String getPlanNumber() {
		return planNumber;
	}
	public void setPlanNumber(String planNumber) {
		this.planNumber = planNumber;
	}
	public String getEnrollmentCount() {
		return enrollmentCount;
	}
	public void setEnrollmentCount(String enrollmentCount) {
		this.enrollmentCount = enrollmentCount;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPlanLevel() {
		return planLevel;
	}
	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getEnrollcount() {
		return enrollcount;
	}
	public void setEnrollcount(String enrollcount) {
		this.enrollcount = enrollcount;
	}
	public String getTimePeriod() {
		return timePeriod;
	}
	public void setTimePeriod(String timePeriod) {
		this.timePeriod = timePeriod;
	}

	public String getCostSharing() {
		return costSharing;
	}
	public void setCostSharing(String costSharing) {
		this.costSharing = costSharing;
	}
}
