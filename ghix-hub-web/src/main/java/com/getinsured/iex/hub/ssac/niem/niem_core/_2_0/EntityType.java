
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for a person, organization, or thing capable of bearing legal rights and responsibilities.
 * 
 * <p>Java class for EntityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}EntityRepresentation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityType", propOrder = {
    "entityRepresentation"
})
public class EntityType
    extends ComplexObjectType
{

    @XmlElementRef(name = "EntityRepresentation", namespace = "http://niem.gov/niem/niem-core/2.0", type = JAXBElement.class)
    protected JAXBElement<?> entityRepresentation;

    /**
     * Gets the value of the entityRepresentation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link PersonType }{@code >}
     *     
     */
    public JAXBElement<?> getEntityRepresentation() {
        return entityRepresentation;
    }

    /**
     * Sets the value of the entityRepresentation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link PersonType }{@code >}
     *     
     */
    public void setEntityRepresentation(JAXBElement<?> value) {
        this.entityRepresentation = value;
    }

}
