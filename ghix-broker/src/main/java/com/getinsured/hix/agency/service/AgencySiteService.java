package com.getinsured.hix.agency.service;

import java.util.List;

import com.getinsured.hix.dto.agency.AgencySiteDto;
import com.getinsured.hix.model.agency.AgencySite;

public interface AgencySiteService {
	public AgencySite saveOrUpdate(AgencySite agencySite);
	public List<AgencySite> getSiteByAgencyId(Long agencyId);
	public AgencySite findSiteById(Long siteId);
}
