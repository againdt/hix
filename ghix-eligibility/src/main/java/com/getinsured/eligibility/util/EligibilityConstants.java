package com.getinsured.eligibility.util;


import java.util.Arrays;
import java.util.List;

/**
 * All Eligibility related error, warning, info and constants are encapsulated here.
 * This class will help us in resolving SONAR related issues.
 *
 * @author Ekram
 *
 */
public final class EligibilityConstants {


	private EligibilityConstants() { }

	public static final String INVALID_VERIFICATION_TYPE_FOUND = "Invalid verification type found - ";

	public static final String Y = "Y";
	public static final String N = "N";

	public static final String RECORD_FOUND_IN_GI_WS_PAYLOAD_TABLE_FOR_ID = "Record found in GI_WS_PAYLOAD table for ID - ";

	public static final String AND_ENDPOINT_FUNCTION_NAME = " and endpointFunctionName - ";

	public static final String NO_RECORD_FOUND_IN_GI_WS_PAYLOAD_TABLE_FOR_ID = "No record found in GI_WS_PAYLOAD table for ID - ";

	public static final String ID = "id";

	public static final String OK = "ok";

	public static final String UNMATCHED_MEMBER_LIST = "unmatchedMemberList";

	public static final String MISMATCH_MEMBER = "mismatch member(s)";

	public static final String NEW_MEMBER = "new member(s)";

	public static final String MISSING_MEMBERS = "missing member(s)";

	public static final String HOUSEHOLD_MATCHED = "Household matched!";

	public static final String AT_RESPONSE_PERSON = "AT_RESPONSE_PERSON";

	public static final String UNMATCHED_PERSON = "UNMATCHED_PERSON";

	public static final String MEMBER_MATCH_RESULT = "MEMBER_MATCH_RESULT";

	public static final String AT_TRANSFORMER_RESULT = "AT_TRANSFORMER_RESULT";

	public static final String HOUSEHOLD_NOT_MATCHED = "Household not matched!";

	public static final String HS000000 = "HS000000";
	
	public static final String HE001111 = "HE001111";

	// this is for business validations
	public static final String BUSINESS_VALIDATION_ERROR_CODE = "HE001111";

	// added for schema validation response code
	public static final String SCHEMATRON_VALIDATION_ERROR_CODE = "HE002222";


	public static final String WELCOME_MESSAGE = "Welcome to GHIX Eligibility Service module....";

	public static final String SSAP_APPLICATION_ID = "SSAP_APPLICATION_ID";

	public static final String REASON = " - Reason - ";

	public static final String ERROR = "ERROR";

	public static final String ERROR_REASON = "ERROR_REASON";

	public static final String ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID = "Error processing ERP request for SSAP_APPLICATION_ID - ";
	
	public static final String ERROR_PROCESSING_AT_DECISION_REQUEST_FOR_SSAP_APPLICATION = "Error processing AT decision request for SSAP_APPLICATION - ";

	public static final String MANUAL = "MANUAL";

	public static final String AUTO = "AUTO";
	
	public static final String AT_PROCESS = "AT_PROCESS";
	
	public static final String AT_PROCESS_RESULT = "AT_PROCESS_RESULT";
	
	public static final String ELIGIBILITY = "ELIGIBILITY";
	
	public static final String NV_INBOUND = "NV_INBOUND";
	
	public static final String INITIATED_AT = "INITIATED_AT";
	
	public static final String ACCOUNT_TRANSFER_REQUEST_DTO = "ACCOUNT_TRANSFER_REQUEST_DTO";

	public static final String ELIGIBILTY_DECISION_RESULT = "ELIGIBILTY_DECISION_RESULT";

	public static final String ERP_RESPONSE = "ERP_RESPONSE";

	public static final String UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES = "Unable to find SSAP Application in GI tables ";

	public static final String COVERAGE_START_DATE_NULL = "Coverage Start Date cannot be null ";

	public static final String UNABLE_TO_FIND_SSAP_APPLICANTS_IN_GI_TABLES = "Unable to find SSAP Applicants in GI tables ";

	public static final String ELIGIBILTY_MATCH_RESULT = "ELIGIBILTY_MATCH_RESULT";

	public static final String MEMBER_MATCHING_LOGIC_FAILED = "Member matching logic failed!!! Control should never reach here....";

	public static final String TRUE = "TRUE";

	public static final String FALSE = "FALSE";

	public static final String CSR_LEVEL = "CSR_LEVEL";

	public static final String MAX_APTC_AMT = "MAX_APTC_AMT";

	public static final String MAX_STATE_SUBSIDY_AMT = "MAX_STATE_SUBSIDY_AMT";

	public static final String endpointFunctionName = "ERP-TRANSFERACCOUNT";


	public static final String INVALID_ELIGIBILITY_TYPE_FOUND = "Invalid Eligibility Type found - ";

	public static final String CHIP_ELIGIBILITY_TYPE = "CHIPEligibilityType";
	public static final String CSR_ELIGIBILITY_TYPE = "CSREligibilityType";
	public static final String EXCHANGE_ELIGIBILITY_TYPE = "ExchangeEligibilityType";
	public static final String APTC_ELIGIBILITY_TYPE = "APTCEligibilityType";
	public static final String STATE_SUBSIDY_ELIGIBILITY_TYPE = "StateSubsidyEligibilityType";
	public static final String MEDICAID_NON_MAGI_ELIGIBILITY_TYPE = "MedicaidNonMAGIEligibilityType";
	public static final String MEDICAID_MAGI_ELIGIBILITY_TYPE = "MedicaidMAGIEligibilityType";
	public static final String REFUGEE_MEDICAL_ASSISTANCE_ELIGIBILITY_TYPE = "RefugeeMedicalAssistanceEligibilityType";
	public static final String EMERGENCY_MEDICAID_ELIGIBILITY_TYPE = "EmergencyMedicaidEligibilityType";
	public static final String MEDICAID_ELIGIBILITY_TYPE = "MedicaidEligibilityType";
	public static final String ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE = "AssessedMedicaidMAGIEligibilityType";
	public static final String ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE = "AssessedMedicaidNonMAGIEligibilityType";
	public static final String ASSESSED_CHIP_ELIGIBILITY_TYPE = "AssessedCHIPEligibilityType";

	public static final String 	NO_PENDING_NOTICES="no pending notices";
	public static final String 	NO_DATE_SELECTED ="no dates selected";
	
	public static final int FIFTEEN = 15;	
	public static final int NINETEEN = 19;
	public static final int TWENTY_THREE = 23;
	public static final int FIFTY_NINE = 59;
	public static final int TWENTY_ONE = 21;
	public static final int ZERO_ZERO = 00;
	public static final int SIXTEEN = 16;
	public static final int TWO_THOUSAND_FIFTEEN = 2015;
	public static final int TWO_HUNDREAD = 200;
	public static final String 	FAILURE = "failure";
	public static final String 	SUCCESS = "success";
	
	public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	
	public static final String SUCCESS_WITH_WARNING_CODE = "HS000001";

	// Verification Statuses
	public static final String PENDING = "PENDING";
	public static final String MORE_INFO_NEEDED = "MORE_INFO_NEEDED";
	public static final String NOT_REQUIRED = "NOT_REQUIRED";
	public static final String NOT_VERIFIED = "NOT_VERIFIED";
	public static final String NOT_VERIFIED_VARIANT = "NOT VERIFIED";
	public static final String VERIFIED = "VERIFIED";
	public static final List<String> NOT_VERIFIED_STATUS_LIST = Arrays.asList(PENDING, MORE_INFO_NEEDED, NOT_VERIFIED, NOT_VERIFIED_VARIANT);
}