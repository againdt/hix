﻿var vega = function (ngAppId) {
    var vm = this;
    
    vm.registerEvent("BEFORE_CONTROLLER_CALLED");
    vm.registerEvent("AFTER_CONTROLLER_CALLED");
    vm._ngAppId = ngAppId;
    vm._ngController = [];
    vm._rawData = null;
    require.config({
        baseUrl: baseURL + "components",
        path: baseURL
    });
    $(function () {
        vm._loadUIData();

    });
        
};
vega.prototype = new baseComponent();
vega.prototype.constructor = baseComponent;
vega.prototype.componentMap = {
    string: "textbox",
    date:"date"
};
vega.prototype.url = '';
vega.prototype.comp = {};
vega.prototype._ngAppId = null;
vega.prototype._ngController = [];
vega.prototype._ngControllerCbks = {};
vega.prototype._rawData = null;
vega.prototype.ngApp = null;
vega.prototype._loadUIData = function () {
    var vm = this;
    vm.init();
    vm.ngApp = angular.module(vm._ngAppId, ["kendo.directives", "ngRoute"]);
    vm.ngApp.config(['$provide', function ($provide) {
        $provide.decorator('$controller', function($delegate) {
            return function(constructor, locals, later, indent) {
                if (typeof constructor === 'string') { //&& !locals.$scope.controllerName
                    locals.$scope.controllerName =  constructor;
                }
                return $delegate(constructor, locals, later, indent);
            };
        });
    }]);
    if (vm._rawData == null) {
        $.ajax({
            url: this.url + '/getapi',
            type: "post",
            dataType: "json",
            success: function (e) {
                vm._loadComponents(e);
            }
        });
    } else {
        vm._loadComponents(vm._rawData);
    }
};
vega.prototype._loadComponents = function (data) {
    var vm = this;
    vm._rawData = data;
    var compsList = [];
    compsList.push(vm._rawData.type);
    for (var b = 0; b < vm._rawData.pageData.length; b++) {
        var thisPageData = vm._rawData.pageData[b];
        compsList.push(thisPageData.type)
        if (thisPageData.type == "form") {
            if (thisPageData.schema != undefined) {
                thisPageData.fields = vm._addTypeFromSchema(thisPageData.schema, thisPageData.fields);
            };
        };        
        if (thisPageData.pageData != undefined) {
            compsList = compsList.concat(vm._getCompsList(thisPageData.pageData));
        };
        if (thisPageData.fields != undefined) {
            compsList = compsList.concat(vm._getCompsList(thisPageData.fields));
        };
    };
    var uniqueComps = [];
    $.each(compsList, function (i, el) {
        if ($.inArray(el, uniqueComps) === -1) uniqueComps.push(el);
    });
    require(uniqueComps, function () {
        for (var d = 0; d < arguments.length; d++) {
            var loadedComp = arguments[d];
            if (loadedComp != undefined) {
                vm.registerComponent(loadedComp);
            }
        };
        vm._injectControllers();
    });
};
vega.prototype._injectControllers = function () {
    var vm = this;
    for (var f = 0; f < 1; f++) {
        var ctrl = vm._ngController[f];
        $("#" + vm._ngAppId).append('<div class="gutter10" id="' + ctrl.controllerName + '" ng-controller="' + ctrl.controllerName + '">');
    };
    vm._processUIData();
};
vega.prototype._processUIData = function (data) {
    var vm = this;
    var data = vm._rawData;
    for (var c = 0; c < vm._ngController.length; c++) {
        var nController = vm._ngController[c];
        var mdls = [];
        mdls = mdls.concat(nController.modules);
        var cbk = nController.callBack;
        vm._ngControllerCbks[nController.controllerName] = cbk;
        mdls = mdls.concat(function () {
            vm._ctrlCallBack(cbk, arguments);
        });
        vm.ngApp.controller(nController.controllerName, mdls);
    };
    vm._renderPageLayout();

};
vega.prototype._renderPageLayout = function () {
    var vm = this;
    var layout = vm._getRawHTML(vm._rawData);
    var fistCtrl = vm._ngController[0];
    $("#" + fistCtrl.controllerName).append(layout);
    angular.bootstrap(document.getElementById(vm._ngAppId), [vm._ngAppId]);
};
vega.prototype.registerComponent = function (comp) {
    var vm = this;    
    comp.context = vm;
    vm.comp[comp.type] = comp;
    comp.preInit();
};
vega.prototype._getCompsList = function(obj)
{
    var vm = this;
    var compsList = [];
    if ($.isArray(obj)) {
        for (var c = 0; c < obj.length; c++) {
            compsList = compsList.concat(vm._getCompsList(obj[c]));
        }
    } else {
        if (obj.type == undefined) { return "" };
        if (obj.type == "form") {
            compsList = compsList.concat("form");
            if (obj.schema != undefined) {
                obj.fields = vm._addTypeFromSchema(obj.schema, obj.fields);
                //compsList = compsList.concat(vm._getCompListFromSchema(obj.schema));
            }
            compsList = compsList.concat(vm._getCompsList(obj.fields));
        } else {
            if (compsList.indexOf(obj.type) < 0) {
                compsList = compsList.concat(obj.type);
            }
        }
    };
    return compsList;
};
vega.prototype._addTypeFromSchema = function (schema, fields) {
    var vm = this;
    for (var f = 0; f < fields.length; f++) {
        var thisField = fields[f];
        if ($.isArray(thisField)) {
            thisField = vm._addTypeFromSchema(schema, thisField)
        } else {
            if (thisField.type == undefined) {
                var thisSchema = schema[thisField.name];
                if (thisSchema != undefined) {
                    var schemaMap = vm.componentMap[thisSchema.type];
                    if (schemaMap != undefined) {
                        thisField.type = schemaMap;
                    }
                };
            };
        };
        
    };
    return fields;
};
vega.prototype._getCompListFromSchema = function (schema) {
    var retArr = [];
    var vm = this;
    var addToArr = function (str) {
        if(retArr.indexOf(str)<0)
        {
            retArr.push(str);
        }
    };
    for (prop in schema) {
        var schemaObj = schema[prop];
        if (schemaObj.type != undefined) {
            var mapObj = vm.componentMap[schemaObj.type];
            if (mapObj != undefined) {
                addToArr(mapObj);
            };
        };
    };
    return retArr;
};
vega.prototype._ctrlCallBack = function (cbk, args) {
    var ctrlName = args[0].controllerName;
    cbk = this._ngControllerCbks[ctrlName];
    var vm = this;
    if (!vm.check(vm.events.BEFORE_CONTROLLER_CALLED, args[0])) { return; }
    vm.injectData(args[0]);
    cbk.apply(null, args);
    vm.dispach(vm.events.AFTER_CONTROLLER_CALLED, args);
};
vega.prototype.ngController = function(controllerName,modules,callBack)
{
    var vm = this;
    vm._ngController.push({
        controllerName: controllerName,
        modules: modules,
        callBack: callBack
    });        
};
vega.prototype.injectData = function($scope)
{
    var vm = this;
    var formDataValues = vm._rawData.data;
    if ($.isArray(formDataValues)) {
        for (var d = 0; d < formDataValues.length; d++) {
            if ($.isArray(formDataValues[d].value)) {
                $scope[String(formDataValues[d].name) + "List"] = formDataValues[d].value;
                for (var i = 0; i < formDataValues[d].value.length; i++) {
                    if (formDataValues[d].value[i].selected) {
                        $scope[String(formDataValues[d].name)] = formDataValues[d].value[i];
                    };
                };
            } else {
                $scope[String(formDataValues[d].name)] = formDataValues[d].value;
            };
        };
    };
};
vega.prototype.renderComponents = function(target,scope,compile)
{
    var vm = this;
    var $target = $("#" + target);
    var data = vm._rawData;
    var $scope = scope;
    var $compile = compile;
    var str = "";
    str = vm._getRawHTML(data);
    var frmStr = $compile(str)($scope);
    $target.append(frmStr);
};
vega.prototype._getRawHTML = function(components)
{
    var vm=this;
    var str = "";
    if($.isArray(components))
    {
        for (var e = 0; e < components.length; e++) {
            str += vm._getRawHTML(components[e]);
        };
    } else {
        if (components.type == undefined) {
            str += "";
        } else {
            str += vm.comp[components.type].getHTML(components);
        }
    }
    return str;
};
vega.prototype._componentJSLoaded = function (module, data, $target, $scope, $compile) {
    var vm = this;
    var rawHtml = module.render($target, data);
    var frmStr = $compile(rawHtml)($scope);
    $target.html(frmStr);
};
vega.prototype.getRootScope = function()
{
    var vm = this;
    return angular.element("#" + vm._ngAppId).scope();
};

var utils = {};
utils.compareObject = function (obj1, obj2) {
    var aObj = {};
    var bObj = {};
    $.extend(aObj, obj1);
    $.extend(bObj, obj2);
    aObj = utils.convertObjectTotring(aObj);
    bObj = utils.convertObjectTotring(bObj);
    if (aObj == bObj) {
        return true;
    } else {
        return false;
    }

};
utils.convertObjectToString = function (obj) {
    var retObj = {};
    $.extend(retObj, obj);
    for (key in retObj) {
        if (typeof (retObj[key]) == "function") {
            retObj[key] = String(retObj[key]);
        };
    };
    return JSON.stringify(retObj);
};
var log = function (str) {
    var logStr = new Date().toLocaleTimeString();
    logStr += ":- " + str;
    var logObj = null;
    if (typeof (str) == "object") {
        logObj = str;
    };
    try {
        /* ##no_release start## */
        console.log(logStr);
        if (logObj != null) {
            console.log(logObj);
        }
        /* ##no_release end## */
    } catch (e) { };
};
if (typeof Array.prototype.vegaGetDataObj !== 'function') {
    Array.prototype.vegaGetDataObj = function (propName, propVal) {
        var retObj = null;
        this.forEach(function (obj) {
            if (obj[propName] == propVal) {
                retObj = obj;
            };
        });
        return retObj;
    };
};
