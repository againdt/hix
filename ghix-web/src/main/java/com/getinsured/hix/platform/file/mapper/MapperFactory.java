package com.getinsured.hix.platform.file.mapper;

import com.getinsured.hix.planmgmt.mapper.HashMapMapper;
import com.getinsured.hix.planmgmt.provider.mapper.FacilityAddressMapper;
import com.getinsured.hix.planmgmt.provider.mapper.FacilityMapper;
import com.getinsured.hix.planmgmt.provider.mapper.PhysicianAddressMapper;
import com.getinsured.hix.planmgmt.provider.mapper.PhysicianMapper;
import com.getinsured.hix.planmgmt.provider.mapper.SpecialtyMapper;

/**
 * Factory class for returning Mapper class object for corresponding Model class name specified.
 */
public class MapperFactory 
{
	public final static String LIST = "List";
	public final static String HASHMAP = "HashMap";
	public final static String Employee = "Employee";
	
	public final static String Facility = "Facility";
	public final static String FacilityAddress = "FacilityAddress";
	
	public final static String Physician = "Physician";
	public final static String PhysicianAddress = "PhysicianAddress";
	
	public final static String Specialty = "Specialty";
	
	public final static String PLANHEALTH = "PlanHealth";
	public final static String PLANHEALTHSERVICEREGION = "PlanHealthServiceRegion";
	
	/**
	 * Returns the Mapper based on {@code mapperName}
	 * @param mapperName
	 * 			Simple Name of the class for which the Mapper to be instantiated.
	 */
	@SuppressWarnings("rawtypes")
	public static Mapper getMapper(String mapperName)
	{
		Mapper mapper = null;
		if (HASHMAP.equals(mapperName))
		{
			mapper = new HashMapMapper();
		}else if(Physician.endsWith(mapperName)){
			mapper = new PhysicianMapper();
		}else if(PhysicianAddress.endsWith(mapperName)){
			mapper = new PhysicianAddressMapper();
		}else if(Facility.endsWith(mapperName)){
			mapper = new FacilityMapper();
		}else if(FacilityAddress.endsWith(mapperName)){
			mapper = new FacilityAddressMapper();
		}else if(Specialty.endsWith(mapperName)){
			mapper = new SpecialtyMapper();
		}else if(LIST.endsWith(mapperName)){
			mapper = new DefaultMapper();
		}
		return mapper;
	}
}
