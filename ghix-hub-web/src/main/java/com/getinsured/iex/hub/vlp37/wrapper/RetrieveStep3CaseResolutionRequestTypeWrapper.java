package com.getinsured.iex.hub.vlp37.wrapper;

import javax.xml.bind.annotation.XmlElement;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.ObjectFactory;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.RetrieveStep3CaseResolutionRequestType;


public class RetrieveStep3CaseResolutionRequestTypeWrapper implements JSONAware{
	
	private static ObjectFactory factory = null;
	
	private RetrieveStep3CaseResolutionRequestType request = null;
	
	
	@XmlElement(name = "CaseNumber", required = true)
    private String caseNumber;
		
	private static final String CASE_NUMBER = "CaseNumber";
	
	
	public RetrieveStep3CaseResolutionRequestTypeWrapper(){
		factory = new ObjectFactory();
		request = factory.createRetrieveStep3CaseResolutionRequestType();
	}
	
	public void setRequest(RetrieveStep3CaseResolutionRequestType request) {
		this.request = request;
	}
	
	/**
	 * This method will also do the request specific validations
	 */
	public RetrieveStep3CaseResolutionRequestType getRequest() throws HubServiceException {
		return this.request;
	}
	
	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.request.setCaseNumber(caseNumber);
		this.caseNumber = caseNumber;
	}


	/**
	 * Converts the JSON String to Agency3InitVerifRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for Agency3InitVerifRequestTypeWrapper
	 * @return Agency3InitVerifRequestTypeWrapper Object 
	 */
	public static RetrieveStep3CaseResolutionRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create RetrieveStep3CaseResolutionRequestTypeWrapper from null or empty input");
		}
		
		RetrieveStep3CaseResolutionRequestTypeWrapper wrapper = new RetrieveStep3CaseResolutionRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		param = obj.get(CASE_NUMBER);
		
		if(param != null){
			wrapper.setCaseNumber((String)param);
		}
		
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put(CASE_NUMBER, this.caseNumber);	
		
		return obj.toJSONString();

	}
	
	public RetrieveStep3CaseResolutionRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		
		RetrieveStep3CaseResolutionRequestTypeWrapper wrapper = new RetrieveStep3CaseResolutionRequestTypeWrapper();
		wrapper.setCaseNumber((String)jsonObj.get(CASE_NUMBER));
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(CASE_NUMBER, this.caseNumber);
		return obj;
	}
}
