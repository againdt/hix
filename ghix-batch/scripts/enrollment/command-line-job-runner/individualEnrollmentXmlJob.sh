#!/bin/bash
# run this script as root
cd /home/jboss/default/standalone/deployments/ghix-batch/WEB-INF/classes
java -cp ".:../lib/*" org.springframework.batch.core.launch.support.CommandLineJobRunner file:./META-INF/spring/batch/jobs/enrollment/commandlinejobrunner/individualEnrollmentXmlJobCL.xml enrollmentXMLIndividualJob processOutboundTranslatorPath=/home/tibcouser/tibco/translator/3.2/Scripts/processEDI_Translator_out.sh processOutboundValidatorPath=/home/tibcouser/tibco/translator/3.3/Scripts/processEDI_Validator_out.sh
 
