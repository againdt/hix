<!-- MOVE TO CSS -->
<style type="text/css">
  .bootstrap-datetimepicker-widget {
    position: relative;
    z-index: 400000;
  }
  
  .address-modal .modal-body{
	max-height: 300px;
  }
</style>
<script type="text/ng-template" id="changeinaddress.jsp">
	<div ng-controller="changeInAddress" class="gutter20">
		<div id="primaryhomeaddress">
			<h3>Enter Event Details</h3>
			<form class="form-horizontal">
				<div class="control-group">
					<span class="control-label margin5-t">
						<spring:message code="label.lce.selectcriteria"/>
					</span>
					<div class="controls">
						<label class="radio-inline" for="newOptionst">
						<input type="radio" id="newOptionst"  name="name" ng-value="true" value="true" required ng-model="newOptions" ng-click="eventInfo.eventSubCategory='CHANGE_IN_ADDRESS_WITHIN_STATE';resetValues();" ng-checked="eventInfo.eventSubCategory=='CHANGE_IN_ADDRESS_WITHIN_STATE'" ng-change="eventInfoArrayTemp[0].changeInAddressDate=''" ng-disabled="disableHomeAddress"> Change of home address
						</label>
						<label class="radio-inline" for="newOptionsf">
						<input type="radio" id="newOptionsf" name="name" ng-value="false" value="false" required ng-model="newOptions" ng-click="resetValues();getDummyPrimaryApplicant();eventInfo.eventSubCategory='MAILING_ADDRESS_CHANGE';" ng-checked="eventInfo.eventSubCategory=='MAILING_ADDRESS_CHANGE'" ng-change="eventInfoArrayTemp[0].changeInAddressDate=''"> Change of mailing address
						</label>
					</div>
				</div>
			</form>
			<!-- Error -->
			<div class="margin10-l">
				<span class="validation-error-text-container" ng-if="submitted && changeinaddressform.samename.$error.required">
					<spring:message code="label.lce.pleaseselectif"/>
					<strong class="">{{householdMember.name.firstName}}</strong> has a different primary contact home address.
					<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
				</span>
			</div>
		</div>
		<div ng-if="eventInfo.eventSubCategory=='CHANGE_IN_ADDRESS_WITHIN_STATE'">
			<div class="margin30-t">
				<h4>
					<spring:message code="label.lce.famHouseholdSum"/>
				</h4>
			</div>
			<table id="" class="table table-striped"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold" role="presentation">
				<tbody>
					<tr>
						<td>
							<spring:message code="label.lce.firstname"/>
						</td>
						<td>
							<spring:message code="label.lce.lastname"/>
						</td>
						<td>
							<spring:message code="label.lce.relationship"/>
						</td>
						<td>
							<spring:message code="label.lce.seekingcoverage"/>
						</td>
						<td>
							Take Action
						</td>
					</tr>
					<tr ng-repeat="(fIndex, householdMember)  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
						<td>{{householdMember.name.firstName}}</td>
						<td>{{householdMember.name.lastName}}</td>
						<td>
							<div>
								{{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
							</div>
						</td>
						<td>
							<span ng-if="householdMember.applyingForCoverageIndicator">
								<spring:message code="label.lce.yes"/>
							</span>
							<span ng-if="!householdMember.applyingForCoverageIndicator">
								<spring:message code="label.lce.no"/>
							</span>
						</td>
						<td>
							<a class="btn btn-primary btn-small" ng-click='toggleModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember));submitted=false;$parent.submitted=false;'>
								<spring:message code="label.lce.update"/>
							</a>
						</td>
					</tr>
				</tbody>
			</table>
			<div id="wrapperToHomeAddress">
				<modal-dialog show='$parent.modalShown' ng-model="modalHouseholdMember">
					<div class="margin50-lr" >
						<h4>
							<spring:message code="label.lce.pleaseupdate"/>
							<strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{ modalHouseholdMember.name.lastName}}</strong>
							<spring:message code="label.lce.changeinaddress.address"/>
						</h4>
						<form class="form-horizontal" name="addressChangeEventDateForm" >
							<div class="control-group">
								<label class="control-label" for="changeinaddressdate">
									<spring:message code="label.lce.eventdate"/>
									<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
								</label>
								<div class="controls">
									<div class="input-append" name="homeAddressChangeEDate" ng-model="eventInfoArrayTemp[originalIndex].changeInAddressDate" id="changeInAddressCal" datetimez check-after="0">
										<label for="changeinaddressdate"></label>
										<input type="text" id="changeinaddressdate" name="changeinaddressdate" class="span12" ng-required="$parent.modalShown" ng-model="eventInfoArrayTemp[originalIndex].changeInAddressDate" placeholder="MM/DD/YYYY" ></input>
										<span class="add-on">
										<i class="icon-calendar"></i>
										</span>
									</div>
								</div>
								<div class="controls margin5-t">
									<span class="validation-error-text-container" ng-if="(submitted|| $parent.submitted ||  $parent.$parent.submitted) && addressChangeEventDateForm.changeinaddressdate.$error.required">
										<spring:message code="label.lce.plsSelectDate"/>
									</span>
									<span class="validation-error-text-container" ng-if="(submitted|| $parent.submitted ||  $parent.$parent.submitted) && addressChangeEventDateForm.homeAddressChangeEDate.$error.checkAfter">
										<spring:message code="label.lce.futureNotAllowed"/>
									</span>
								</div>
							</div>
						</form>
						<!-- Address -->
						<div class="margin30-t" ng-if="modalHouseholdMember.personId === 1 || modalHouseholdMember.personId === '1' ">
							<form class="form-horizontal" name="primaryApplicantAddressForm">
								<div class="differentLocation addressBlock">
									<div class="control-group">
										<label class="control-label" for="home_addressLine1">
											<spring:message code="label.lce.address1"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
										</label>
										<div class="controls">
											<input type="text" class="span8" id="home_addressLine1" name="home_addressLine1" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$/" required ng-model="modalHouseholdMember.householdContact.homeAddress.streetAddress1">
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_addressLine1.$error.required">
												<spring:message code="label.lce.pleaseenteraddress1"/>
											</span>
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_addressLine1.$error.pattern">
												<spring:message code="label.lce.entervalidaddress1"/>
											</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="home_addressLine2">
											<spring:message code="label.lce.address2"/>
										</label>
										<div class="controls">
											<input type="text" class="span8" id="home_addressLine2" name="home_addressLine2" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{1,75}$/" ng-model="modalHouseholdMember.householdContact.homeAddress.streetAddress2"  >
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_addressLine2.$error.pattern">
												<spring:message code="label.lce.entervalidaddress2"/>
											</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="home_primary_city">
											<spring:message code="label.lce.city"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
										</label>
										<div class="controls">
											<input type="text" class="span8" id="home_primary_city" name="home_primary_city" required ng-pattern="/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/" ng-model="modalHouseholdMember.householdContact.homeAddress.city">
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_primary_city.$error.required">
												<spring:message code="label.lce.pleaseentercity"/>
											</span>
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_primary_city.$error.pattern">
												<spring:message code="label.lce.entervalidcity"/>
											</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="home_primary_state">
											<spring:message code="label.lce.state"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
										</label>
										<div class="controls">
											<select class="" id="home_primary_state" name="home_primary_state"  ng-model="modalHouseholdMember.householdContact.homeAddress.state"  required ng-change="modalHouseholdMember.householdContact.homeAddress.postalCode=null">
											<!--	<option value="">
													<spring:message code="label.lce.selectstate"/>
												</option>-->
												<option ng-repeat="state in stateArray" value="{{state.stateCode}}"  ng-selected="{{state.stateCode == modalHouseholdMember.householdContact.homeAddress.state}}" label="{{state.stateName}}"  >{{state.stateName}}</option>
											</select>
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_primary_state.$error.required">
												<spring:message code="label.lce.pleaseselectstate"/>
											</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="home_primary_zip">
											<spring:message code="label.lce.zip"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true" onChange="trim('ape')">
										</label>
										<div class="controls">
											<input type="text" class="span8" id="home_primary_zip" name="home_primary_zip" required maxlength="5" ng-minlength="5" ng-model="modalHouseholdMember.householdContact.homeAddress.postalCode"  ng-change="modalHouseholdMember.householdContact.homeAddress.primaryAddressCountyFipsCode='';modalHouseholdMember.householdContact.homeAddress.county='';loadCounties(modalHouseholdMember.householdContact.homeAddress.state,modalHouseholdMember.householdContact.homeAddress.postalCode,0);" >
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_primary_zip.$error.required">
												<spring:message code="label.lce.pleaseenterzip"/>
											</span>
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_primary_zip.$error.minlength">
												<spring:message code="label.lce.entervalidzip"/>
											</span>
										</div>
									</div>
									<!-- County -->
									<div class="control-group">
										<label class="control-label" for="home_primary_county">
											<spring:message code="label.lce.county"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
										</label>
										<div class="controls">
											<select id="home_primary_county" name="home_primary_county" required select-label-model="modalHouseholdMember.householdContact.homeAddress.county" ng-model="modalHouseholdMember.householdContact.homeAddress.primaryAddressCountyFipsCode"  >
												<option value="">Select County</option>
												<option ng-repeat="(key,value) in countiesother" value="{{value}}" ng-selected="{{value == modalHouseholdMember.householdContact.homeAddress.primaryAddressCountyFipsCode}}" label="{{key}}">{{key}}</option>
											</select>
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.home_primary_county.$error.required">
												<spring:message code="label.lce.pleaseselectcounty"/>
											</span>
										</div>
									</div>
								</div>
								<div id="primaryhomeaddress" class="margin30-t" ng-if="householdArray.length > 1">
									<h4>Do you want to update address of other members to same as above? <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></h4>
									<div class="margin10-l">
										<label class="radio-inline" for="sameprimaryt">
											<input type="radio" id="sameprimaryt" name="updateOtrMem" ng-value="true" value="true" required ng-model="$parent.updateAllHouseholdAddress" />
											<spring:message code="label.lce.yes"/>
										</label>
										<label class="radio-inline" for="sameprimaryf">
											<input type="radio" id="sameprimaryf" name="updateOtrMem" ng-value="false" value="false" required ng-model="$parent.updateAllHouseholdAddress" />
											<spring:message code="label.lce.no"/>
										</label>
									</div>
									<!-- Error -->
									<div class="margin10-l">
										<span class="validation-error-text-container" ng-if="submitted && primaryApplicantAddressForm.updateOtrMem.$error.required">
											<spring:message code="label.lce.pleaseselectif"/>
											<strong class="">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{ modalHouseholdMember.name.lastName}}</strong> has a different primary contact home address.
										</span>
									</div>
								</div>
							</form>
							<dl class="dl-horizontal pull-right margin30-t">
								
								
								<div ng-if="((!primaryApplicantAddressForm.$valid) || (!addressChangeEventDateForm.$valid))" >
									<a class="btn btn-secondary" ng-click="submitted=false;$parent.submitted=false;closeModal()">Cancel</a>
									<a class="btn btn-primary" ng-click="submitted=true;$parent.submitted=true;$parent.$parent.submitted=true;">
										<spring:message code="label.lce.ok"/>
									</a>
								</div>

								<div ng-if="(primaryApplicantAddressForm.$valid && addressChangeEventDateForm.$valid)" address-validation-directive="modalHouseholdMember.householdContact.homeAddress" address-validation-callback="submitted=false;$parent.submitted=false;$parent.$parent.submitted=false; modelOK(modalHouseholdMember,originalIndex,eventInfoArrayTemp[originalIndex].changeInAddressDate);updateHomeAddressOfSecondaryHouseHolds($parent.updateAllHouseholdAddress);" address-validation-same-address-dto="false">
									<a class="btn btn-secondary" ng-click="submitted=false;$parent.submitted=false;closeModal()">Cancel</a>
									<a class="btn btn-primary address-validation-click">
										<spring:message code="label.lce.ok"/>
									</a>
								</div>

							</dl>
						</div>
						<div class="margin30-t" ng-if="!(modalHouseholdMember.personId === 1 || modalHouseholdMember.personId === '1')">
							<h4>
								<spring:message code="label.lce.wheredoes"/>
								{{modalHouseholdMember.name.firstName}} {{ modalHouseholdMember.name.lastName}}
								<spring:message code="label.lce.live"/>
							</h4>
							<form class="form-horizontal"  name="secondaryApplicantAddressForm">
								<div class="margin10-l">
									<label class="radio-inline" for="addressYes">
										<input type="radio" id="addressYes" name="address" ng-value="false" value="false" required ng-model="modalHouseholdMember.livesAtOtherAddressIndicator"  ng-click="modalHouseholdMember.householdContact.homeAddressIndicator=true;modalHouseholdMember.livesAtOtherAddressIndicator=false;changeAddressOfMember(modalHouseholdMember);">
										<spring:message code="label.lce.sameAsPrimaryContact"/>
									</label>
								</div>
								<div class="margin10-l">
									<label class="radio-inline" for="addressNo">
										<input type="radio" id="addressNo" name="address" ng-value="true" value="true" required ng-model="modalHouseholdMember.livesAtOtherAddressIndicator"  ng-click="modalHouseholdMember.householdContact.homeAddressIndicator=false;modalHouseholdMember.livesAtOtherAddressIndicator=true;changeAddressOfMember(modalHouseholdMember);">
										<spring:message code="label.lce.differentlocation"/>
									</label>
								</div>
								<div class="margin20-l margin10-t">
									<span class="validation-error-text-container" ng-if="submitted && secondaryApplicantAddressForm.address.$error.required">
										<spring:message code="label.lce.thisvalueisrequired"/>
									</span>
								</div>
								<!-- DIFFERENT ADDRESS -->
								<div class="differentLocation addressBlock" ng-if="modalHouseholdMember.livesAtOtherAddressIndicator==true">
									<div class="control-group">
										<label class="control-label" for="home_addressLine1">
											<spring:message code="label.lce.address1"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
										</label>
										<div class="controls">
											<input type="text" class="span8" id="home_addressLine1" name="home_addressLine1" ng-required="modalHouseholdMember.livesAtOtherAddressIndicator"  ng-model="modalHouseholdMember.otherAddress.address.streetAddress1">
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && secondaryApplicantAddressForm.home_addressLine1.$error.required">
												<spring:message code="label.lce.pleaseenteraddress1"/>
											</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="home_addressLine2">
											<spring:message code="label.lce.address2"/>
										</label>
										<div class="controls">
											<input type="text" class="span8" id="home_addressLine2" ng-model="modalHouseholdMember.otherAddress.address.streetAddress2"  >
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="home_primary_city">
											<spring:message code="label.lce.city"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
										</label>
										<div class="controls">
											<input type="text" class="span8" id="home_primary_city" name="home_primary_city"  ng-required="modalHouseholdMember.livesAtOtherAddressIndicator" ng-model="modalHouseholdMember.otherAddress.address.city">
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && secondaryApplicantAddressForm.home_primary_city.$error.required">
												<spring:message code="label.lce.pleaseentercity"/>
											</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="home_primary_state">
											<spring:message code="label.lce.state"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
										</label>
										<div class="controls">
											<select name="inputPrimaryMailingState" class="" id="home_primary_state" name="home_primary_state" ng-required="modalHouseholdMember.livesAtOtherAddressIndicator" ng-model="modalHouseholdMember.otherAddress.address.state" ng-change="modalHouseholdMember.otherAddress.address.postalCode=null"  >
												<option value="">
													<spring:message code="label.lce.selectstate"/>
												</option>
												<option ng-repeat="state in stateArray" value="{{state.stateCode}}" ng-selected="{{state.stateCode == modalHouseholdMember.otherAddress.address.state}}"  label="{{state.stateName}}"  >{{state.stateName}}</option>
											</select>
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && secondaryApplicantAddressForm.home_primary_state.$error.required">
												<spring:message code="label.lce.pleaseselectstate"/>
											</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="home_primary_zip">
											<spring:message code="label.lce.zip"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true" onChange="trim('ape')">
										</label>
										<div class="controls">
											<input type="text" class="span8" id="home_primary_zip" name="home_primary_zip" ng-required="modalHouseholdMember.livesAtOtherAddressIndicator" ng-model="modalHouseholdMember.otherAddress.address.postalCode" ng-change="modalHouseholdMember.otherAddress.address.county='';loadCounties(modalHouseholdMember.otherAddress.address.state,modalHouseholdMember.otherAddress.address.postalCode,0);" >
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && secondaryApplicantAddressForm.home_primary_zip.$error.required">
												<spring:message code="label.lce.entervalidzip"/>
											</span>
										</div>
									</div>
									<!-- County -->
									<div class="control-group">
										<label class="control-label" for="home_primary_county">
											<spring:message code="label.lce.county"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
										</label>
										<div class="controls">
											<select id="home_primary_county" name="home_primary_county" ng-required="modalHouseholdMember.livesAtOtherAddressIndicator" ng-model="modalHouseholdMember.otherAddress.address.county"  >
												<option value="">
													<spring:message code="label.lce.selectcounty"/>
												</option>
												<option ng-repeat="(key,value) in countiesother" value="{{value}}" ng-selected="{{value == modalHouseholdMember.otherAddress.address.county}}" label="{{key}}">{{key}}</option>
											</select>
										</div>
										<!-- Error -->
										<div class="controls margin5-t">
											<span class="validation-error-text-container" ng-if="submitted && secondaryApplicantAddressForm.home_primary_county.$error.required">
												<spring:message code="label.lce.pleaseselectcounty"/>
											</span>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label" for="hidFirstLastName">
											<spring:message code="label.lce.is"/>
											<strong>{{modalHouseholdMember.name.firstName| capitalize:true}} {{modalHouseholdMember.name.middleName| capitalize:true}} {{modalHouseholdMember.name.lastName| capitalize:true}}</strong>
											<spring:message code="lce.livingoutsideofidaho"/>
											<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
										</label>
										<input type="hidden" id="hidFirstLastName" /> <!-- Accessibility use -->
										<div class="controls">
											<label class="radio-inline" for="tempresidenceyes">
												<input type="radio" id="tempresidenceyes" name="temporarilyLivingOutsideIndicator" ng-required="modalHouseholdMember.livesAtOtherAddressIndicator" ng-value="true" ng-model="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyIndicator"> 
												<spring:message code="label.lce.yes"/>
											</label>
											<label class="radio-inline" for="tempresidenceno">
												<input type="radio" id="tempresidenceno" name="temporarilyLivingOutsideIndicator" ng-required="modalHouseholdMember.livesAtOtherAddressIndicator" ng-change="changeTempAddress(modalHouseholdMember)"  ng-value="false" ng-model="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyIndicator"> 
												<spring:message code="label.lce.no"/>
											</label>
										</div>
										<div class="controls">
											<div class="validation-error-text-container" for="home_primary_county" ng-if="submitted && secondaryApplicantAddressForm.temporarilyLivingOutsideIndicator.$error.required">
												<spring:message code="label.lce.temporarilyLivingOutside"/>
											</div>
										</div>
									</div>
									<!-- OUTSIDE CITY-->
									<div ng-if="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyIndicator== true" class="addressBlock form-horizontal">
										<div class="control-group">
											<label class="control-label" for="home_temp_city">
												<spring:message code="label.lce.city"/>
												<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
											</label>
											<div class="controls">
												<input id="home_temp_city" name="home_temp_city" type="text" ng-minlength="5" ng-maxlength="100" ng-required="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyIndicator && modalHouseholdMember.livesAtOtherAddressIndicator" ng-pattern="/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/" class="" ng-model="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyAddress.city" placeholder='<spring:message code="label.lce.city"/>'>
											</div>
											<div class="controls">
												<div class="validation-error-text-container margin30-l" ng-if="submitted && secondaryApplicantAddressForm.home_temp_city.$error.required">
													<spring:message code="label.lce.pleaseentercity"/>
												</div>
												<div class="validation-error-text-container margin30-l" ng-if="submitted && secondaryApplicantAddressForm.home_temp_city.$error.pattern">
													<spring:message code="label.lce.entervalidcity"/>
												</div>
												<div class="validation-error-text-container margin30-l" ng-if="submitted && (secondaryApplicantAddressForm.home_temp_city.$error.minlength || secondaryApplicantAddressForm.home_temp_city.$error.maxlength)">
													<spring:message code="label.lce.entervalidcity"/>
												</div>
											</div>
										</div>
									</div>
									<!-- OUTSIDE STATE-->
									<div ng-if="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyIndicator== true" class="addressBlock form-horizontal">
										<div class="control-group">
											<label class="control-label" for="home_temp_state">
												<spring:message code="label.lce.state"/>
												<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
											</label>
											<div class="controls">
												<select id="home_temp_state" name="home_temp_state" ng-required="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyIndicator && modalHouseholdMember.livesAtOtherAddressIndicator"  ng-model="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyAddress.state" ng-options="state.stateCode as state.stateName for state in stateArray"></select>
											</div>
											<div class="controls">
												<div class="validation-error-text-container margin30-l" for="home_temp_state" ng-if="submitted && secondaryApplicantAddressForm.home_temp_state.$error.required">
													<spring:message code="label.lce.pleaseselectstate"/>
												</div>
											</div>
										</div>
									</div>
									<!-- OUTSIDE ZIP-->
									<div ng-if="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyIndicator== true" class="addressBlock form-horizontal">
										<div class="control-group">
											<label class="control-label" for="home_temp_zip">
												<spring:message code="label.lce.zip"/>
												<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
											</label>
											<div class="controls">
												<input id="home_temp_zip" name="home_temp_zip" ng-required="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyIndicator && modalHouseholdMember.livesAtOtherAddressIndicator" ng-pattern="/^[0-9]{5}(\-[0-9]{4})?$/" type="text" ng-model="modalHouseholdMember.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode" placeholder='<spring:message code="label.lce.zip"/>'>
											</div>
											<div class="controls">
												<div class="validation-error-text-container margin30-l" for="home_temp_zip" ng-if="submitted && secondaryApplicantAddressForm.home_temp_zip.$error.required">
													<spring:message code="label.lce.pleaseenterzip"/>
												</div>
												<div class="validation-error-text-container margin30-l" for="home_temp_zip" ng-if="submitted && secondaryApplicantAddressForm.home_temp_zip.$error.pattern">
													<spring:message code="label.lce.entervalidzip"/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
							<dl class="dl-horizontal pull-right">

								<div ng-if="(!secondaryApplicantAddressForm.$valid)||(!addressChangeEventDateForm.$valid)">
									<a class="btn btn-secondary" ng-click="submitted=false;$parent.submitted=false;closeModal()">
										<spring:message code="lce.cancelbutton"/>
									</a>
									<a class="btn btn-primary" ng-click="submitted=true;$parent.submitted=true;$parent.$parent.submitted=true;">
										<spring:message code="label.lce.ok"/>
									</a>
								</div>

								<div ng-if="secondaryApplicantAddressForm.$valid && addressChangeEventDateForm.$valid" address-validation-directive="modalHouseholdMember.otherAddress.address" address-validation-callback="submitted=false;$parent.submitted=false;$parent.$parent.submitted=false;modelOK(modalHouseholdMember,originalIndex,eventInfoArrayTemp[originalIndex].changeInAddressDate);" address-validation-same-address-dto="false">
									<a class="btn btn-secondary" ng-click="submitted=false;$parent.submitted=false;closeModal()">
										<spring:message code="lce.cancelbutton"/>
									</a>
									<a ng-if="modalHouseholdMember.livesAtOtherAddressIndicator" class="btn btn-primary address-validation-click">
										<spring:message code="label.lce.ok"/>
									</a>
									<a ng-if="!modalHouseholdMember.livesAtOtherAddressIndicator"  class="btn btn-primary"  ng-click="submitted=false;$parent.submitted=false;$parent.$parent.submitted=false;modelOK(modalHouseholdMember,originalIndex,eventInfoArrayTemp[originalIndex].changeInAddressDate);" >
										<spring:message code="label.lce.ok"/>
									</a>
								</div>
							</dl>
						</div>
					</div>
				</modal-dialog>
			</div>
			<div class="pull-right">
				<a class="btn btn-secondary" onclick="history.go(-1);return false;" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
					<spring:message code='label.lce.back'/>
				</a>
				<a class="btn btn-primary"  ng-click="next();">
					<spring:message code="label.lce.next"/>
				</a>
			</div>
		</div>
		<div ng-if="eventInfo.eventSubCategory=='MAILING_ADDRESS_CHANGE'" class="margin30-t">
			<h4>
				<spring:message code="label.lce.plsUpdateMailAdd"/>
			</h4>
			<form class="form-horizontal" name="mailingAddressChangeForm">
				<div class="control-group">
					<label class="control-label" for="mailingAddressChange">
						<spring:message code="label.lce.eventdate"/>
						<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
					</label>
					<div class="controls">
						<div name="mailingChangeDate" class="input-append" ng-model="eventInfoArrayTemp[0].changeInAddressDate" datetimez ng-required="!newOptions" check-after="0" >
							<input type="text" id="mailingAddressChange" class="span12" name="mailingAddressChange" ng-model="eventInfoArrayTemp[0].changeInAddressDate" placeholder="MM/DD/YYYY"></input>
							<span class="add-on">
							<i class="icon-calendar"></i>
							</span>
						</div>
						<div class="margin10-t">
							<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.mailingChangeDate.$error.required">
								<spring:message code="label.lce.plsSelectDate"/>
							</span>
							<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.mailingChangeDate.$error.checkAfter">
								<spring:message code="label.lce.futureNotAllowed"/>
							</span>
						</div>
					</div>
				</div>
				<!-- Address -->
				<div class="margin30-t">
					<div class="differentLocation addressBlock">
						<div class="control-group">
							<label class="control-label" for="home_addressLine1">
								<spring:message code="label.lce.address1"/>
								<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
							</label>
							<div class="controls">
								<input type="text" class="span8" id="home_addressLine1" name="home_addressLine1" ng-required="!newOptions" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$/" ng-model="dummyPrimaryApplicant.householdContact.mailingAddress.streetAddress1">
							</div>
							<!-- Error -->
							<div class="controls margin5-t">
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_addressLine1.$error.required">
									<spring:message code="label.lce.pleaseenteraddress1"/>
								</span>
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_addressLine1.$error.pattern">
									<spring:message code="label.lce.entervalidaddress1"/>
								</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="home_addressLine2">
								<spring:message code="label.lce.address2"/>
							</label>
							<div class="controls">
								<input type="text" class="span8" id="home_addressLine2" name="home_addressLine2" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{1,75}$/" ng-model="dummyPrimaryApplicant.householdContact.mailingAddress.streetAddress2"  >
							</div>
							<!-- Error -->
							<div class="controls margin5-t">
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_addressLine2.$error.pattern">
									<spring:message code="label.lce.entervalidaddress2"/>
								</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="home_primary_city">
								<spring:message code="label.lce.city"/>
								<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
							</label>
							<div class="controls">
								<input type="text" class="span8" id="home_primary_city" name="home_primary_city" ng-required="!newOptions" ng-pattern="/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/" ng-model="dummyPrimaryApplicant.householdContact.mailingAddress.city">
							</div>
							<!-- Error -->
							<div class="controls margin5-t">
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_primary_city.$error.required">
									<spring:message code="label.lce.pleaseentercity"/>
								</span>
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_primary_city.$error.pattern">
									<spring:message code="label.lce.entervalidcity"/>
								</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="home_primary_state">
								<spring:message code="label.lce.state"/>
								<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
							</label>
							<div class="controls">
								<select name="inputPrimaryMailingState" class="" id="home_primary_state" name="home_primary_state" ng-required="!newOptions"  ng-model="dummyPrimaryApplicant.householdContact.mailingAddress.state" ng-change="dummyPrimaryApplicant.householdContact.mailingAddress.postalCode=null;" >
									<!--<option value="">
										<spring:message code="label.lce.selectstate"/>
									</option>-->
									<option ng-repeat="state in stateArray" value="{{state.stateCode}}" ng-selected="{{state.stateCode == dummyPrimaryApplicant.householdContact.mailingAddress.state}}" label="{{state.stateName}}">{{state.stateName}}</option>
								</select>
							</div>
							<!-- Error -->
							<div class="controls margin5-t">
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_primary_state.$error.required">
									<spring:message code="label.lce.pleaseselectstate"/>
								</span>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="home_primary_zip">
								<spring:message code="label.lce.zip"/>
								<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true" onChange="trim('ape')">
							</label>
							<div class="controls">
								<input type="text" class="span8" id="home_primary_zip" name="home_primary_zip" ng-required="!newOptions" maxlength="5" ng-minlength="5" ng-model="dummyPrimaryApplicant.householdContact.mailingAddress.postalCode" ng-change="dummyPrimaryApplicant.householdContact.mailingAddress.county='';dummyPriCountyFips='';loadCounties(dummyPrimaryApplicant.householdContact.mailingAddress.state,dummyPrimaryApplicant.householdContact.mailingAddress.postalCode,0);" >
							</div>
							<!-- Error -->
							<div class="controls margin5-t">
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_primary_zip.$error.required">
									<spring:message code="label.lce.pleaseenterzip"/>
								</span>
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_primary_zip.$error.minlength">
									<spring:message code="label.lce.entervalidzip"/>
								</span>
							</div>
						</div>
						<!-- County -->
						<div class="control-group">
							<label class="control-label" for="home_primary_county">
								<spring:message code="label.lce.county"/>
								<img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
							</label>
							<div class="controls">
								<select id="home_primary_county" name="home_primary_county"  ng-required="!newOptions" select-label-model="dummyPrimaryApplicant.householdContact.mailingAddress.county" ng-model="dummyPriCountyFips"  >
									<option value="">Select County</option>
									<option ng-repeat="(key,value) in countiesother" value="{{value}}" ng-selected="{{key == dummyPrimaryApplicant.householdContact.mailingAddress.county}}"  label="{{key}}" >{{key}}</option>
								</select>
							</div>
							<!-- Error -->
							<div class="controls margin5-t">
								<span class="validation-error-text-container" ng-if="submitted && mailingAddressChangeForm.home_primary_county.$error.required">
									<spring:message code="label.lce.pleaseselectcounty"/>
								</span>
							</div>
						</div>
					</div>
				</div>
			</form>
			<dl class="dl-horizontal pull-right" address-validation-directive="dummyPrimaryApplicant.householdContact.mailingAddress" address-validation-callback="handleAddressSelect(dummyPrimaryApplicant,eventInfoArrayTemp[0].changeInAddressDate)" address-validation-same-address-dto="false">
				<a ng-if="!mailingAddressUpdate" href="javascript:void(0)" class="btn btn-secondary" onclick="history.go(-1);return false;" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
					<spring:message code='label.lce.back'/>
				</a>
				<a ng-if="mailingAddressUpdate" href="javascript:void(0)" class="btn btn-secondary" onclick='window.location="<c:url value="/indportal#/mypreferences"/>"' ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
					<spring:message code='label.lce.back'/>
				</a>
				<a class="btn btn-primary" ng-if="!mailingAddressChangeForm.$valid" ng-click="$parent.$parent.submitted=true">
					<spring:message code="label.lce.next"/>
				</a>
				<a class="btn btn-primary address-validation-click" ng-if="mailingAddressChangeForm.$valid">
					<spring:message code="label.lce.next"/>
				</a>
			</dl>
		</div>
		<modal-Tiny show='showWarningOnChange' ng-model="modalHouseholdMember">
			<div class="gutter20">
				<div>
					<spring:message code="label.lce.changeEvent"/>
					<strong>{{previousEventName}}</strong> 
					<spring:message code="label.lce.to"/>
					<strong>{{currentEventName}}</strong>. 
					<spring:message code="label.lce.yourChangesFor"/>
					<strong>{{previousEventName}}</strong> 
					<spring:message code="label.lce.revertToOriginal"/>
				</div>
				<div class="pull-right">
					<button class="btn btn-secondary" ng-click="closeModal()">
						<spring:message code="label.lce.ok"/>
					</button>
				</div>
			</div>
		</modal-Tiny>
	
	<div id="addressModal" class="modal hide" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;�</button>
				<h3><spring:message code="label.lce.checkYourAddress"/></h3>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lce.cancelbutton"/></button>
				<button class="btn btn-primary" id="modalOk" onclick="addressSelected();"><spring:message code="label.lce.ok"/></button>
			</div>
  </div>
  <div id="addressErrorModal" class="modal hide" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;�</button>
				<h3><spring:message code="label.lce.confrimAddress"/></h3>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.lce.close"/></button>
			</div>
		</div>
	</div>
	
	


</script>