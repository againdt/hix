package com.getinsured.eligibility.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.Location;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
/**
 * @author chopra_s
 *
 */
@Repository ("cmrHouseholdRepository")
@Transactional
public interface CmrHouseholdRepository extends JpaRepository<Household, Integer>{

	/**
	 * Fetches the Household Id for a given User
	 * @param userId 
	 * @return Household associated with that user
	 */
	@Query("select hh.id FROM Household hh where hh.user.id = :userId")
	Integer findHouseholdByUserId(@Param("userId") Integer userId); 

	@Query("select hh FROM Household hh where hh.user.id = :userId")
	Household findHouseholdObjectByUserId(@Param("userId") Integer userId);
	
	/**
	 * Fetches the User Id for a given Cmr
	 * @param userId 
	 * @return User associated with that cmr
	 */
	@Query("select hh.user.id FROM Household hh where hh.id = :cmrId")
	Integer findUserByCmrId(@Param("cmrId") Integer cmrId); 
	
	@Query("select hh.user.userName FROM Household hh where hh.id = :cmrId")
	String findUserNameByCmrId(@Param("cmrId") Integer cmrId); 
	
	@Query("select hh.created FROM Household hh where hh.id = :cmrId")
	Timestamp findCreatedDateOfCmr(@Param("cmrId") Integer cmrId);

	@Query("select hh FROM Household hh where hh.user.id is not null and  hh.ridpVerified = :ridpVerified and hh.ridpDate > :startDate and hh.ridpDate < :endDate" )
    List<Household> findByRidpVerifiedAndRidpDateBetween(@Param("ridpVerified") String ridpVerified, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query("select hh.id FROM Household hh where hh.firstName = :firstName and hh.lastName = :lastName and hh.birthDate = :birthDate and zipCode =:zip")
	List<Integer> findByFirstNameLastNameAndBirthDate(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("birthDate") Date birthDate,  @Param("zip") String zip);

	@Query("select hh.id FROM Household hh where hh.firstName = :firstName and hh.lastName = :lastName and hh.birthDate = :birthDate and hh.ssn =:ssn")
	List<Integer> findMatchingCmrByProfile(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("birthDate") Date birthDate,  @Param("ssn") String ssn);

	@Query("select count(hh.id) FROM Household hh where hh.firstName = :firstName and hh.lastName = :lastName and hh.birthDate = :birthDate and hh.id = :id")
	long findMatchingCmrByProfile(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("birthDate") Date birthDate, @Param("id") int id);
	
	@Query("SELECT hh FROM Household hh WHERE "
			+ "("
			+ "(:phoneNumber IS NOT NULL AND :emailAddress IS NULL  AND hh.phoneNumber = :phoneNumber)"
			+ "OR (:phoneNumber IS NULL AND :emailAddress IS NOT NULL AND hh.email = LOWER(:emailAddress)) "
			+ "OR (:phoneNumber IS NOT NULL AND :emailAddress IS NOT NULL AND hh.phoneNumber = :phoneNumber AND hh.email = LOWER(:emailAddress)) )")
	List<Household> findHouseHoldbyPhoneOrEmail(@Param("phoneNumber") String phoneNumber, @Param("emailAddress") String emailAddress);
	
	@Query("select hh.id FROM Household hh where hh.ssn =:ssn")
	List<Integer> findMatchingCmrBySSN(  @Param("ssn") String ssn);

	@Query("select hh FROM Household hh where hh.ssn =:ssn")
	List<Household> findMatchingCmrObjectBySSN(  @Param("ssn") String ssn);
	
	@Query("select hh.id FROM Household hh where  hh.birthDate = :birthDate and hh.ssn =:ssn")
	 List<Integer> findMatchingCmrBySsnDoB(  @Param("birthDate") Date birthDate,  @Param("ssn") String ssn);

	@Query("select hh.id FROM Household hh where hh.householdCaseId =:householdCaseId")
	List<Integer> findMatchingCmrByHHCaseId(  @Param("householdCaseId") String householdCaseId);

	@Query("select hh.householdCaseId FROM Household hh where hh.id = :id")
	String findHHCaseIdByHouseholdId(@Param("id") Integer id);

	@Modifying
	@Transactional
	@Query("update Household set locationId = :location where id = :id")
	void updateHouseholdLocation(@Param("location") Location location, @Param("id") Integer id);

}