/*
 * Backbone Model binder view
 */

$(function() {
	
	///Model to form binder view
	UpdateView = Backbone.View.extend({
		
		_modelBinder: undefined,
		
		initialize : function() {
			this._modelBinder = new Backbone.ModelBinder();
			_.bindAll(this, 'render');
			this.render();
		},
		
		render: function(){
			this._modelBinder.bind(this.model, this.el, this.options.bindings);
	    }
		
	});
	
});