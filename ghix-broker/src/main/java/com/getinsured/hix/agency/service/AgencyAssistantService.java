package com.getinsured.hix.agency.service;


import com.getinsured.hix.dto.agency.AgencyAssistantApprovalDto;
import com.getinsured.hix.dto.agency.AgencyAssistantInfoDto;
import com.getinsured.hix.dto.agency.AgencyAssistantListDto;
import com.getinsured.hix.dto.agency.AgencyAssistantSearchDto;
import com.getinsured.hix.dto.agency.AgencyAssistantStatusDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant;

public interface AgencyAssistantService {
	
	public AgencyAssistant saveOrUpdate(AgencyAssistant agency);
	
	public AgencyAssistant findById(Long assistantId);
	
	public AgencyAssistant findByAgencyId(Long agencyId);
	
	public void createModuleUser(AgencyAssistant assistant);

	public AgencyAssistantListDto findAgencyAssistantsForAdmin(AgencyAssistantSearchDto agencyAssistantSearchDto);

	public AgencyAssistantListDto findAgencyAssistantsForAgency(AgencyAssistantSearchDto agencyAssistantSearchDto);

	public AgencyAssistantApprovalDto loadAgencyAssistantApprovalHistory(Long agencyAssistantId);

	AgencyAssistant saveAssistantInformation(AgencyAssistant assistant, AgencyAssistantInfoDto agencyAssistantInfoDto);
	
	AgencyAssistant updateActivityStatus(AgencyAssistantStatusDto agencyAssistantStatusDto);
	
    AgencyAssistant updateApprovalStatus(AgencyAssistantApprovalDto agencyAssistantApprovalDto);
	
	public AgencyAssistantStatusDto loadAgencyAssistantStatusHistory(Long agencyAssistantId);

	AgencyAssistant saveUserForAgencyAssistant(long assistantId, AccountUser user);

	AgencyAssistant findByPersonalEmailAddress(String personalEmailAddress);
	
	AgencyAssistant findByUserId(int userId);
	
	AgencyAssistantInfoDto getAgencySitesForAssistant(AgencyAssistant assistant);

}
