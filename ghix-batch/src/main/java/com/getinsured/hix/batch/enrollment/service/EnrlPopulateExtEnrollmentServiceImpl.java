/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import static org.apache.commons.lang3.StringUtils.isNoneBlank;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.batch.enrollment.util.EnrlPopulateExtEnrollmentJobThread;
import com.getinsured.hix.dto.enrollment.ExternalEnrollmentCsvDTO;
import com.getinsured.hix.enrollment.email.EnrlPopulateExtEnrollmentNotification;
import com.getinsured.hix.enrollment.repository.IExternalEnrollmentRepository;
import com.getinsured.hix.enrollment.repository.IExternalEnrollmentSummaryRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.enrollment.ExternalEnrollment;
import com.getinsured.hix.model.enrollment.ExternalEnrollmentSummary;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.timeshift.util.TSDate;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

/**
 * @author negi_s
 *
 */
@Service("enrlPopulateExtEnrollmentService")
@Transactional
public class EnrlPopulateExtEnrollmentServiceImpl implements EnrlPopulateExtEnrollmentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlPopulateExtEnrollmentServiceImpl.class);

	private static String[] MN_COLUMNS = new String[] { "applicantExternalId", "healthExchangeAssignedPolicyId",
			"healthPlanId", "dentalExchangeAssignedPolicyId", "dentalPlanId", "tobaccoIndicator" };

	private static final char SEPARATOR = ',';

	@Autowired
	private IExternalEnrollmentRepository externalEnrollmentRepository;

	@Autowired
	private IExternalEnrollmentSummaryRepository summaryRepository;
	
	@Autowired 
	private EnrlPopulateExtEnrollmentNotification enrlPopulateExtEnrollmentNotification;
	
	@Override
	public void processExternalCSV(JobService jobService, StepExecution stepExecution, String replace)
			throws IOException, GIException, InterruptedException {

		if ("Y".equalsIgnoreCase(replace)) {
			purgeOldData();
		}

		Long jobId = stepExecution.getJobExecution().getJobId();
		// Moving the Previous files to failure folder
		moveAllWipFiletoFailureFolder(jobId);
		String wipFolderPath = moveAllInFileToWipFolder();

		long fileCount = 0;
		boolean processExtCSVFilesInParallel = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
				EnrollmentConfiguration.EnrollmentConfigurationEnum.PROCESS_EXT_ENROLLMENT_FILE_IN_PARALLEL));
		ExecutorService executor = null;
		if (null != wipFolderPath) {
			fileCount = Files.list(Paths.get(wipFolderPath)).count();
			if (fileCount > 1 && processExtCSVFilesInParallel) {
				executor = Executors.newFixedThreadPool(EnrollmentConstants.TEN);
			} else {
				executor = Executors.newFixedThreadPool(EnrollmentConstants.ONE);
			}

			File[] csvFiles = EnrollmentUtils.getFilesInAFolder(wipFolderPath, EnrollmentConstants.FILE_TYPE_CSV);

			File archivePath = createPath(jobId, EnrollmentConstants.ARCHIVE_FOLDER);

			List<Callable<String>> taskList = new ArrayList<>();

			for (int index = 0; index < csvFiles.length; index++) {
				taskList.add(new EnrlPopulateExtEnrollmentJobThread(csvFiles[index].getPath(), jobId, jobService,
						stepExecution, archivePath, this));
			}
			List<String> processResultList = executor.invokeAll(taskList).stream().map(future -> {
				try {
					return future.get();

				} catch (Exception ex) {

					LOGGER.error("Exception occurred in processReconciliationInFile: ", ex);
				}
				return null;
			}).filter(p -> p != null).collect(Collectors.toCollection(ArrayList<String>::new));

			executor.shutdown();
		} else {
			LOGGER.error("wipFolderPath is Null");
		}
	}

	private void purgeOldData() throws GIException {
		try {
			externalEnrollmentRepository.deleteAll();
			summaryRepository.updateStatusToPurged();
		} catch (Exception e) {
			LOGGER.error("Unable to delete previous data", e);
			throw new GIException("Unable to delete previous data", e);
		}
	}

	@Override
	public String populateExtEnrollmentData(String fileName, Long batchExecutionId, JobService jobService,
			StepExecution stepExecution, File archivePath) throws Exception {

		Integer fileId = createSummary(fileName, batchExecutionId, false);
		Integer counter = 0;

		String batchJobStatus = null;

		if (jobService != null) {
			batchJobStatus = jobService.getJobExecution(stepExecution.getJobExecutionId()).getStatus().name();
		}

		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {

			updateSummery(fileId, EnrollmentConstants.BATCH_JOB_STATUS_STOPPED);
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}

		List<ExternalEnrollmentCsvDTO> csvLineList = new ArrayList<ExternalEnrollmentCsvDTO>();
		List<String> errorList = new ArrayList<>();

		try {

			if (fileId != 0) {

				counter = Files.lines(Paths.get(fileName)).map(line -> line.trim()).filter(line -> line.length() > 0)
						.map(line -> convertToBeanAndSave(line, fileId, csvLineList, errorList))
						.reduce(0, Integer::sum);

				counter = counter + loadAndSaveExternalEnrollment(csvLineList, fileId, errorList);

				createErrorFile(errorList, batchExecutionId, fileName);

				updateSummery(fileId, "COMPLETED");
			}

		} catch (Exception e) {
			updateSummery(fileId, "FAILURE");
			LOGGER.error("EnrlPopulateExtEnrollmentService SERVICE : Exception while processing + " + fileName, e);
			throw new GIException("EnrlPopulateExtEnrollmentService SERVICE : Exception while processing + " + fileName,
					e);
		}

		moveFileToArchiveOrFailure(archivePath, fileName);
		return Integer.toString(fileId);
	}

	private void createErrorFile(List<String> errorList, Long batchExecutionId, String fileName) throws GIException {
		if (null != errorList && !errorList.isEmpty()) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			String batchDate = sdf.format(new Date()).replace('|', '-').replace(':', '-') + "-" + batchExecutionId;
			StringBuilder stringbuilder = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(
					EnrollmentConstants.EXTERNAL_ENROLLMENT, EnrollmentConstants.TRANSFER_DIRECTION_IN);
			stringbuilder.append(File.separator);
			stringbuilder.append(EnrollmentConstants.FAILURE_FOLDER);
			stringbuilder.append(File.separator);
			stringbuilder.append(batchDate);
			EnrollmentUtils.createDirectory(stringbuilder.toString());
			stringbuilder.append(File.separator);
			stringbuilder.append("failed_records.csv");
			String consolidatedError = errorList.stream().collect(Collectors.joining(System.lineSeparator()));
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(stringbuilder.toString()), "utf-8"))) {
				writer.write(consolidatedError);
			} catch (Exception ex) {
				throw new GIException(ex);
			}
			prepareDetailMapAndSendNotification(fileName, stringbuilder.toString());
		}
	}

	public void updateSummery(Integer fileId, String status) {
		ExternalEnrollmentSummary summary = summaryRepository.findOne(fileId);
		summary.setStatus(status);
		summaryRepository.saveAndFlush(summary);
	}

	private int convertToBeanAndSave(String line, Integer fileId, List<ExternalEnrollmentCsvDTO> csvLineList,
			List<String> errorList) {
		List<ExternalEnrollmentCsvDTO> extCsvDtoList = null;
		int internalCounter = 0;
		CsvToBean<ExternalEnrollmentCsvDTO> csvToBeanDetail = new CsvToBean<ExternalEnrollmentCsvDTO>();

		if (null != line) {

			extCsvDtoList = csvToBeanDetail.parse(getDetailColumnMapping(),
					new CSVReader(new StringReader(line), SEPARATOR));

			if (null != extCsvDtoList && !extCsvDtoList.isEmpty()) {
				if (validateExtEnrollmentCSV(extCsvDtoList.get(0))) {
					csvLineList.add(extCsvDtoList.get(0));
				} else {
					errorList.add(line+SEPARATOR+getFailureResonText(extCsvDtoList.get(0)));
					return ++internalCounter;
				}

				if (csvLineList.size() >= 1000) {
					List<ExternalEnrollmentCsvDTO> csvLineSaveList = csvLineList.stream().collect(Collectors.toList());
					internalCounter = loadAndSaveExternalEnrollment(csvLineSaveList, fileId, errorList);
					csvLineList.clear();
					return internalCounter;
				}
			} else {
				errorList.add(line+SEPARATOR+getFailureResonText(null));
				return ++internalCounter;
			}

		}
		return internalCounter;
	}

	public int loadAndSaveExternalEnrollment(List<ExternalEnrollmentCsvDTO> csvLineSaveList, Integer fileId,
			List<String> errorList) {

		ExternalEnrollment externalEnrollment = null;
		List<ExternalEnrollment> extEnrollmentList = new ArrayList<>();

		Date dob = null;

		for (ExternalEnrollmentCsvDTO dto : csvLineSaveList) {
			try { 
				externalEnrollment = new ExternalEnrollment();
				externalEnrollment.setApplicantExternalId(dto.getApplicantExternalId());
				externalEnrollment.setHealthPlanId(dto.getHealthPlanId());
				externalEnrollment.setDentalPlanId(dto.getDentalPlanId());
				externalEnrollment.setHealthExchangeAssignedPolicyId(dto.getHealthExchangeAssignedPolicyId());
				externalEnrollment.setDentalExchangeAssignedPolicyId(dto.getDentalExchangeAssignedPolicyId());
				externalEnrollment.setTobaccoIndicator(dto.getTobaccoIndicator());
				externalEnrollment.setApplicantFirstName(dto.getApplicantFirstName());
				externalEnrollment.setApplicantLastName(dto.getApplicantLastName());
				externalEnrollment.setAgentNpn(dto.getAgentNpn());
				dob = isNoneBlank(dto.getApplicantDob())
						? DateUtil.StringToDate(dto.getApplicantDob(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD)
						: null;
				externalEnrollment.setApplicantDob(dob);
				externalEnrollment.setFileId(fileId);
				externalEnrollment.setApplicationExternalId(dto.getApplicationExternalId());


				extEnrollmentList.add(externalEnrollment);

			} catch (Exception ex) {
				LOGGER.error("Error in setting DTO data", ex);
				if(!"NV".equalsIgnoreCase(EnrollmentConfiguration.returnStateCode()))
				{
					errorList.add(dto.toCsv(SEPARATOR, EnrollmentConfiguration.returnStateCode())+SEPARATOR+"Mapping Error");
				}
				else
				{
					errorList.add(dto.getApplicantExternalId()+",\""+ ex.getMessage()+"\"");
				}
			}
		}

		extEnrollmentList.stream().forEach(new Consumer<ExternalEnrollment>() {
			@Override
			public void accept(ExternalEnrollment extEnrollment) {
				try {
					externalEnrollmentRepository.saveAndFlush(extEnrollment);
				}catch (org.springframework.dao.DataIntegrityViolationException die){
					if(!"NV".equalsIgnoreCase(EnrollmentConfiguration.returnStateCode()))
					{
						if("MN".equalsIgnoreCase(EnrollmentConfiguration.returnStateCode()))
						errorList.add(extEnrollment.toCsv(SEPARATOR, EnrollmentConfiguration.returnStateCode())+SEPARATOR+"Duplicate MEMBER_MNSURE_ID");
						else
						errorList.add(extEnrollment.toCsv(SEPARATOR, EnrollmentConfiguration.returnStateCode())+SEPARATOR+"Duplicate Member ID");	
					}
					else
					{
						errorList.add(extEnrollment.getApplicantExternalId()+",\""+ die.getMessage()+"\"");
					}
				}
				
				catch (Exception ex) {
					LOGGER.error("Error in saving ext enrollment data", ex);
					if(!"NV".equalsIgnoreCase(EnrollmentConfiguration.returnStateCode()))
					{
						errorList.add(extEnrollment.toCsv(SEPARATOR, EnrollmentConfiguration.returnStateCode())+SEPARATOR+"Internal Error");
					}
					else
					{
						errorList.add(extEnrollment.getApplicantExternalId()+",\""+ ex.getMessage()+"\"");
					}
				}
			}
		});

		return csvLineSaveList.size();
	}

	public Integer createSummary(String filePath, Long batchExecutionId, boolean isDuplicate) {
		ExternalEnrollmentSummary summary = new ExternalEnrollmentSummary();
		String fileName = Paths.get(filePath).getFileName().toString();
		summary.setFileName(fileName);
		summary.setBatchId(batchExecutionId);
		summary.setStatus("PROCESSING");
		summary = summaryRepository.saveAndFlush(summary);
		return summary.getId();
	}

	
	private String getFailureResonText(ExternalEnrollmentCsvDTO externalEnrollmentCsvDTO) {
		
		String reasonText="";
		if (EnrollmentConfiguration.isMnCall()){
			
			if (null != externalEnrollmentCsvDTO) {
				if (StringUtils.isBlank(externalEnrollmentCsvDTO.getApplicantExternalId()))
				{
					return  " Missing MEMBER_MNSURE_ID ";
				}
				if ( StringUtils.isBlank(externalEnrollmentCsvDTO.getTobaccoIndicator())) {
					return " Missing TOBACCO_USE_CODE ";
				} 
				if ( StringUtils.isBlank(externalEnrollmentCsvDTO.getHealthExchangeAssignedPolicyId()) && 
						StringUtils.isBlank(externalEnrollmentCsvDTO.getDentalExchangeAssignedPolicyId())) {
					return " Missing MEDICAL_SUBSCIBER_ID / DENTAL_SUBSCIBER_ID ";
				}
				if ( StringUtils.isNotBlank(externalEnrollmentCsvDTO.getHealthExchangeAssignedPolicyId()) && 
						StringUtils.isBlank(externalEnrollmentCsvDTO.getHealthPlanId())) {
					return " Missing MEDICAL_SERFF_PLAN_ID  ";
				}
				if ( StringUtils.isNotBlank(externalEnrollmentCsvDTO.getDentalExchangeAssignedPolicyId()) && 
						StringUtils.isBlank(externalEnrollmentCsvDTO.getDentalPlanId())) {
					return " Missing DENTAL_SERFF_PLAN_ID  ";
				}
			} else {
				return "Missing Required Fields";
			}
		}
		else
		{
			reasonText= "";
		}
		return reasonText;
		
	}
	
	private boolean validateExtEnrollmentCSV(ExternalEnrollmentCsvDTO externalEnrollmentCsvDTO) {

		if (null != externalEnrollmentCsvDTO) {
			if (EnrollmentConfiguration.isMnCall()
					&& StringUtils.isNotBlank(externalEnrollmentCsvDTO.getApplicantExternalId())
					&& StringUtils.isNotBlank(externalEnrollmentCsvDTO.getTobaccoIndicator())) {
				
				return StringUtils.isBlank(getFailureResonText(externalEnrollmentCsvDTO));
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ColumnPositionMappingStrategy<ExternalEnrollmentCsvDTO> getDetailColumnMapping() {
		ColumnPositionMappingStrategy<ExternalEnrollmentCsvDTO> strategy = new ColumnPositionMappingStrategy();
		strategy.setType(ExternalEnrollmentCsvDTO.class);
		if (EnrollmentConfiguration.isMnCall()) {
			strategy.setColumnMapping(MN_COLUMNS);
		}
		return strategy;
	}

	private String moveAllInFileToWipFolder() {

		StringBuilder fileNameBuilder = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(
				EnrollmentConstants.EXTERNAL_ENROLLMENT, EnrollmentConstants.TRANSFER_DIRECTION_IN);
		String inboundDir = fileNameBuilder.toString();
		File dir = new File(inboundDir);

		String wipFolderPath = inboundDir + File.separator + EnrollmentConstants.WIP_FOLDER_NAME;
		File wipFolder = new File(wipFolderPath);
		boolean wipPathValid = true;

		if (dir != null && !(wipFolder.exists())) {
			if (!wipFolder.mkdirs()) {
				wipPathValid = false;
			}
		}

		if (wipPathValid) {
			if (dir.isDirectory()) {
				File[] files = dir.listFiles(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(EnrollmentConstants.FILE_TYPE_CSV);
					}
				});
				for (File file : files) {
					if (file.isFile()) {
						File wipFile = new File(wipFolderPath + File.separator + file.getName());
						file.renameTo(wipFile);
					}
				}
			}

			if (wipFolder.list() != null && wipFolder.list().length > 0) {
				return wipFolderPath;
			} else {
				return null;
			}
		}
		return null;
	}

	private void moveFileToArchiveOrFailure(File filePath, String fileName) {
		if (filePath != null) {
			try {
				EnrollmentUtils.moveFile(Paths.get(fileName).getParent().toString(), filePath.toString(),
						new File(fileName));
			} catch (GIException e) {
				LOGGER.error("Exception occurred while Moving Reconciliation files: ", e);
			}
		}
	}

	private File createPath(Long jobId, String directoryName) {
		StringBuilder fileNameBuilder = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(
				EnrollmentConstants.EXTERNAL_ENROLLMENT, EnrollmentConstants.TRANSFER_DIRECTION_IN);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		String batchDate = sdf.format(new Date()).replace('|', '-').replace(':', '-') + "-" + jobId;

		String inboundDir = fileNameBuilder.toString();
		File dir = new File(inboundDir);

		String folderPath = inboundDir + File.separator + directoryName + File.separator + batchDate;
		File folder = new File(folderPath);
		boolean folderPathValid = true;

		if (dir != null && !(folder.exists())) {
			if (!folder.mkdirs()) {
				folderPathValid = false;
			}
		}

		if (folderPathValid) {
			return folder;
		} else {
			return null;
		}

	}

	private void moveAllWipFiletoFailureFolder(long jobExecutionId) {
		String wipFolderPathStr = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(
				EnrollmentConstants.EXTERNAL_ENROLLMENT, EnrollmentConstants.TRANSFER_DIRECTION_IN) + File.separator
				+ EnrollmentConstants.WIP_FOLDER_NAME;
		File wipFolder = new File(wipFolderPathStr);

		if (wipFolder.isDirectory() && wipFolder.exists()) {
			File[] files = wipFolder.listFiles();
			if(null != files && files.length > 0) {
				File failureFolderPath = createPath(jobExecutionId, EnrollmentConstants.FAILURE_FOLDER);
				for (File file : files) {
					if (file.isFile() && failureFolderPath != null && failureFolderPath.exists()) {
						moveFileToArchiveOrFailure(failureFolderPath, file.getPath());
					}
				}
			}
		}
	}

	@Override
	public void matchAndUpdateApplicants() {
		Date currentDate = new TSDate();
		externalEnrollmentRepository.matchApplicantsAndApplication(currentDate);
		if (Boolean.parseBoolean(DynamicPropertiesUtil
				.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.UPDATE_SSAP_APPLICANTS))) {
			externalEnrollmentRepository.updateSsapApplicantsForTobacco(EnrollmentConstants.Y, currentDate);
			externalEnrollmentRepository.updateSsapApplicantsForTobacco(EnrollmentConstants.N, currentDate);
		}
	}
	
	private void sendFailureNotificationEmail(Map<String, String> requestMap) throws GIException {
		LOGGER.info("Populate External Enrollment Job :: Notification email sending process");
		try{
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EMAIL_GRP_IRS_REPORTING);
			if(StringUtils.isEmpty(recipientEmailAddress)){
				throw new GIException("sendFailureNotificationEmail:: Null or Empty Email Receipient address refer application config 'enrollment.InternalEmailGroupFor1095Reporting'");
			}
			if(requestMap == null || (requestMap!= null && requestMap.isEmpty())){
				throw new GIException("sendFailureNotificationEmail:: Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			emailData.put("Subject",requestMap.get("Subject"));
			enrlPopulateExtEnrollmentNotification.setEmailData(emailData);
			enrlPopulateExtEnrollmentNotification.setRequestData(requestMap);
			Notice noticeObj = enrlPopulateExtEnrollmentNotification.generateEmail();
			Notification notificationObj = enrlPopulateExtEnrollmentNotification.generateNotification(noticeObj);
			LOGGER.info("Notice body :: "+noticeObj.getEmailBody());
			enrlPopulateExtEnrollmentNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex){
			throw new GIException(ex);
		}
	}
	
	/**
	 * Send notification mail after failure
	 * @param errorFileLocation 
	 * @param transferLogList
	 * @param reportType
	 * @param transferDirection
	 */
	private synchronized void prepareDetailMapAndSendNotification(String fileName, String errorFileLocation) {
		
		Map<String, String> emailDataMap=new HashMap<>();
		emailDataMap.put("File_Name",Paths.get(fileName).getFileName().toString());
		emailDataMap.put("File_Location",errorFileLocation);
		emailDataMap.put("Action", "Verify records in the failure file.");
		emailDataMap.put("Environment",GhixConstants.APP_URL);
		emailDataMap.put("Subject", "Populate External Enrollment Job : Records Failed");
		try {
			sendFailureNotificationEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending staging email notification :: ", e);
		}
	}

}
