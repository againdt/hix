(function (models) {
	models.Cart = Backbone.Model.extend({
		
		urlRoot : "employeeCartItems",
		url : function () {
			if(this.id)
				return 'employeeCartItems/'+this.id+'?'+(Math.random()*10);
			else
			return 'employeeCartItems?'+(Math.random()*10);
		},
		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"id"		: "",
			"name"		: "",
			"level"		: "",
			"premium"	: "",
			"grossPremium"	: "",
			"issuer"		: "",
			"issuerText"	: "",
			"networkType"	: "",
			"monthlyPayrollDeduction" : "",
			"oopEstimate"	:"",
			"estimatedTotalHealthCareCost" : "",
			"employerContribution"	: "",
			"planScore"	: "",
			"oopMax"	: "",
			"deductible"	: "",
			"totalDeductible"	 : "",
			"familyDeductible"	: "",
			"employeeId"	: ""
		}
	});
})(App.models);