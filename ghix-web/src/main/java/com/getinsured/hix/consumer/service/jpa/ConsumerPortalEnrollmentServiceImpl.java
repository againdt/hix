package com.getinsured.hix.consumer.service.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.consumer.service.ConsumerPortalEnrollementService;
import com.getinsured.hix.model.EmployerEnrollment.Status;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

/**
 * ConsumerPortal Enrollment Service integrates with Enrollment Service to fetch the 
 * Consumer Household Plan data
 * @author bhatia_s
 * @since 9/18/2013
 */
@Service("consumerPortalEnrollmentService")
public class ConsumerPortalEnrollmentServiceImpl implements ConsumerPortalEnrollementService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerPortalEnrollmentServiceImpl.class);

	@Autowired private GhixRestTemplate ghixRestTemplate;

	/**
	 * Invokes the Enrollment Rest service for finding enrollments based on CMHouseHoldID
	 * @param cmrHouseHoldId
	 * @return List of Enrollments
	 * @throws GIException
	 */
	public List<Enrollment> getHouseholdPlanDetails(Integer cmrHouseHoldId) throws GIException{
		LOGGER.info("Entering getHouseholdPlanDetails");
		
		List<Enrollment> enrollmentList = null;

		String getResp =  ghixRestTemplate.getForObject(EnrollmentEndPoints.FIND_ENROLLMENTS_BY_CMR_HOUSEHOLD_ID + cmrHouseHoldId, String.class);	
 		
      //HIX-93225, HIX-91520 
		XStream xstream = GhixUtils.getXStreamStaxObject();
        EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstream.fromXML(getResp);
 		
 		if (enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
 			enrollmentList = enrollmentResponse.getEnrollmentList();
 		}else{
			throw new GIException("Unable to get Employee Plan Details. Error Details: " + enrollmentResponse.getErrCode() + ":" + enrollmentResponse.getErrMsg());
		}
 		
 		return enrollmentList;
	}
	
	/**
	 * Preparing the Enrollment Plans Meta data from the Enrollment Response returned from
	 * Enrollment Service
	 */
	@Override
	public List< Map<String,String>> getHouseholdPlanMetaData(Integer cmrHouseHoldId) throws GIException {
		LOGGER.info("Entering getHouseholdPlanMetaData >> ");
		List< Map<String,String>> householdPlanMetaData = new ArrayList<Map<String,String>>();
		
		List<Enrollment> enrollmentList = this.getHouseholdPlanDetails(cmrHouseHoldId);
		LOGGER.info("enrollmentList size======== "+SecurityUtil.sanitizeForLogging(""+enrollmentList.size()));
		DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/y");
		DateTimeFormatter fmtPlanDisplay = DateTimeFormat.forPattern("MMddy");
		
		List<Status> activeStatus = new ArrayList<Status>();
		activeStatus.add(Status.ACTIVE);
		activeStatus.add(Status.PENDING);
		
		try {
			for(Enrollment enrollment:enrollmentList){

	 			Map<String,String> planMetaData = new HashMap<String, String>();
	 			planMetaData.put("planId", String.valueOf(enrollment.getEnrollmentPlan().getPlanId()));
	 			planMetaData.put("planName", enrollment.getPlanName());
	 			planMetaData.put("planType", enrollment.getEnrollmentPlan().getNetworkType());
	 			planMetaData.put("deductible", enrollment.getEnrollmentPlan().getIndivDeductible().toString());
	 			planMetaData.put("genericMedications", enrollment.getEnrollmentPlan().getGenericMedication());
	 			planMetaData.put("officeVisit", enrollment.getEnrollmentPlan().getOfficeVisit());
	 			planMetaData.put("oopMax", enrollment.getEnrollmentPlan().getIndivOopMax().toString());
	 			planMetaData.put("issuerName", enrollment.getInsurerName());
	 			String logoStr = "/admin/issuer/company/profile/logo/view/"+enrollment.getIssuerId();
	 			planMetaData.put("logoStr", logoStr);
	 			planMetaData.put("monthlyPremium", String.valueOf(Math.round(enrollment.getGrossPremiumAmt())));
	 			planMetaData.put("premiumTaxCredit", String.valueOf(Math.round(enrollment.getAptcAmt())));
	 			planMetaData.put("monthlyPayment", String.valueOf(Math.round(enrollment.getNetPremiumAmt())));

	 			Float grossPremiumAmt = enrollment.getGrossPremiumAmt();
	 			if(grossPremiumAmt == null){
	 				planMetaData.put("monthlyPremium", String.valueOf(grossPremiumAmt));
	 			}else{
	 				planMetaData.put("monthlyPremium", String.valueOf(Math.round(grossPremiumAmt)));
	 			}
	 			
	 			Float contribution = enrollment.getEmployerContribution();
	 			if(contribution == null){
	 				planMetaData.put("employerContribution", String.valueOf(contribution));
	 			}else{
	 				planMetaData.put("employerContribution", String.valueOf(Math.round(contribution)));
	 			}
	 			
	 			Float monthlyPayrollDeduction = enrollment.getEmployeeContribution();
	 			if(contribution == null){
	 				planMetaData.put("monthlyPayrollDeduction", String.valueOf(monthlyPayrollDeduction));
	 			}else{
	 				planMetaData.put("monthlyPayrollDeduction", String.valueOf(Math.round(monthlyPayrollDeduction)));
	 			}
	 			
	 			planMetaData.put("issuerId", String.valueOf(enrollment.getIssuerId()));
	 			LOGGER.info("enrollment.getIssuer() : "+SecurityUtil.sanitizeForLogging(""+enrollment.getIssuerId()));
	 			planMetaData.put("issuerPhone", enrollment.getEnrollmentPlan().getPhoneNumber());
	 			planMetaData.put("issuerSite", enrollment.getEnrollmentPlan().getIssuerSiteUrl());
	 			planMetaData.put("enrollmentId", String.valueOf(enrollment.getId()));
	 			
	 			String policyNumber = enrollment.getGroupPolicyNumber();
	 			if(policyNumber == null || policyNumber.equals("")){
	 				planMetaData.put("policyNumber", "");
	 			}else{
	 				planMetaData.put("policyNumber", enrollment.getGroupPolicyNumber());
	 			}
	 			
	 			Date benefitEndDate = enrollment.getBenefitEndDate();
	 			if(benefitEndDate == null){
	 				planMetaData.put("coverageEndDate", "");
	 			}else{
		 			DateTime enddt = new DateTime(benefitEndDate);
		 			planMetaData.put("coverageEndDate",fmt.print(enddt));
	 			}
	 			
	 			Date benefitStartDate = enrollment.getBenefitEffectiveDate();
	 			DateTime startdt = null;
	 			if(benefitStartDate == null){
	 				planMetaData.put("coverageStartDate", "");
	 			}else{
		 			startdt = new DateTime(benefitStartDate);
		 			planMetaData.put("coverageStartDate",fmt.print(startdt));
		 			planMetaData.put("formattedCoverageStartDate", fmtPlanDisplay.print(startdt));
	 			}
 			
	 			planMetaData.put("enrolleeNames", enrollment.getEnrolleeNames());

	 			LOGGER.info("HouseHold Plan Meta Data ======== "+SecurityUtil.sanitizeForLogging(planMetaData.toString()));
	 			LOGGER.info("HouseHold Plan benefitStartDate ======== "+SecurityUtil.sanitizeForLogging(planMetaData.get("coverageStartDate")));
	 			LOGGER.info("HouseHold Plan formatted benefitStartDate ======== "+SecurityUtil.sanitizeForLogging(planMetaData.get("formattedCoverageStartDate")));
	 			
	 			householdPlanMetaData.add(planMetaData);
			}
		} catch (Exception e) {
			LOGGER.error("exception in getting plan metadata from the plan management : ",e);
			throw new GIException("Unable to get Household Plan meta data for CMHouseholdId :: " + cmrHouseHoldId);
		}
		
		return householdPlanMetaData;
	}

}
