<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/provider.css" />" media="screen" />

    <!--  List Modal -->
    <form class="content modal-form" id="provider-list" name="" action="" method="GET"><!-- <c:url value="" /> -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">�</button>
            <h1><spring:message code='label.findHealthPlan'/></h1>
        </div>
    
        <div class="accordian">
            <div class="accordian-select">
                <div class="modal-body">
                    <div class="row-fluid">
                        <p><spring:message code='label.pageHeader'/></p>
                        <div class="controls">  
                            <label id="reason" class="accordion-heading radio span2" for="radio-yes">
                                <input type="radio" name="coverageType" id="radio-yes" value="emp_defined_contribution" /><!--  <c:if test="${empOrder.coverageType =='DEF_CONTRIB'}"> checked </c:if> -->
                                <strong><spring:message code='label.coverageTypeValYes'/></strong>
                            </label>
                        </div>
                        
                        <div class="controls">
                            <label class="accordion-heading radio span2" for="radio-no">
                                <input type="radio" name="coverageType" id="radio-no" value="defined_benefits"  /><!-- <c:if test="${empOrder.coverageType =='DEF_BEN'}"> checked </c:if> -->
                                <strong><spring:message code='label.coverageTypeValNo'/></strong>
                            </label>
                        </div>
                    </div><!-- .row-fluid -->
                </div><!-- .modal-body -->
            </div><!-- end #select -->
    
            <div id="group-yes" class="accordian-body collapse">
                <div class="modal-body">
                    <h3>Providers</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th><spring:message code='label.include'/></th>
                                <th><spring:message code='label.name'/></th>
                                <th><spring:message code='label.specality'/></th>
                                <th> &#35; <spring:message code='label.planAccepted'/></th>
                                <th><spring:message code='label.delete'/></th>
                            </tr>
                        </thead>
    
                        <tbody>
                            <tr>
                                <td>
                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="checkbox" value="option1" name="optionsCheckboxList1" />
                                        </div>
                                    </div>
                                </td>
                                <td>James Smith MD</td>
                                <td>Cardiology</td>
                                <td>25</td>
                                <td>
                                    <a href="#" class="btn btn-small delete"><i class="icon-trash icon"></i> Delete</a>
                                </td>
                            </tr>
    
                            <tr>
                                <td>
                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="checkbox" value="option2" name="optionsCheckboxList1" />
                                        </div>
                                    </div>
                                </td>
                                <td>Stanford Hospital</td>
                                <td>Multi-Specialty</td>
                                <td>45</td>
                                <td>
                                    <a href="#" class="btn btn-small delete"><i class="icon-trash icon"></i> Delete</a>
                                </td>
                            </tr>
    
                            <tr>
                                <td>
                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="checkbox" value="option3" name="optionsCheckboxList1" />
                                        </div>
                                    </div>
                                </td>
                                <td>Saul Williams DDS</td>
                                <td>Ortho</td>
                                <td>2</td>
                                <td>
                                    <a href="#" class="btn btn-small delete"><i class="icon-trash icon"></i> Delete</a>
                                </td>
                            </tr>
    
                            <tr>
                                <td colspan="5">
                                    <a href="search.jsp" class="btn btn-small">Add a New Preferred Provider</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- .modal-body -->
    
                <div class="modal-footer">
                    <input type="button" name="" id="" value="Add Checked Preferred Provider(s)" class="btn btn-primary" />
                </div>
            </div><!-- end #group-yes -->
    
            <div id="group-no" class="accordian-body collapse">
                <div class="modal-footer">
                    <a href="search.jsp" class="btn btn-primary">Start a Provider Search</a>
                </div>
            </div><!-- end #group-no -->
    
        </div><!-- accordian -->
    
    </form><!-- #provider-list -->
