<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>



<%@page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>

<link href="resources/css/ssap/tooltip.css" rel="stylesheet" type="text/css">
<link href="resources/css/ssap/opentip.css" rel="stylesheet" type="text/css" />
<link href="resources/css/ssap/reDeveloped.css" rel="stylesheet" type="text/css">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<script type="text/javascript" >
  var csrftoken;

  $(document).ready(function(){
    csrftoken = '${sessionScope.csrftoken}';

  });
</script>

<script type="text/javascript" src="resources/js/ssap/parsley.js"></script>
<!-- <script type="text/javascript" src="resources/js/ssap/parsley.extend.js"></script> -->
<script type="text/javascript" src="resources/js/ssap/waypoints.min.js" ></script>
<script type="text/javascript" src="resources/js/ssap/waypoints-sticky.min.js" ></script>
<script type="text/javascript" src="resources/js/ssap/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/ssap/jquery.i18n.properties-1.0.9.js"></script>

<!-- <script type="text/javascript" src="resources/js/ssap/minifiedssap.js"></script>-->
<script type="text/javascript" src="resources/js/ssap/ssap.js"></script>

<script type="text/javascript" src="resources/js/ssap/opentip-jquery-excanvas.min.js"></script>
<script type="text/javascript" src="resources/js/ssap/tooltipController.js"></script>
<%-- <script type="text/javascript" src="resources/js/ssap/bootstrap.min.js"></script>--%>
<%-- <script type="text/javascript" src="<c:url value="resources/js/ssap/latestResul.js" />"></script>--%>
<script type="text/javascript" src="<c:url value="resources/js/ssap/bootstrap-datepicker.js" />"></script>
<script type="text/javascript" src="resources/js/ssap/moment.min.js"></script>
<!-- <script type="text/javascript" src="resources/js/ssap/jquery.mask.js" ></script> -->
<script type="text/javascript" src="resources/js/ssap/jquery.mask.min.js" ></script>
<script type="text/javascript" src="resources/js/ssap/placeholder.js"></script>
<script type="text/javascript" src="resources/js/ssap/functions.js"></script>
<html>
<body>

<c:url value="/deleteapplicant" var="theUrlFordeleteApplicant">
  <c:param name="coverageYear">${coverageYear}</c:param>
</c:url>

<c:url value="/saveERT.nmhx" var="theUrlForSaveJsonData">
  <c:param name="coverageYear">${coverageYear}</c:param>
</c:url>

<c:url value="/ssap/isRidpVerified" var="theUrlForisRidpVerified">
  <c:param name="coverageYear">${coverageYear}</c:param>
</c:url>

<c:url value="/ssap/addCounties" var="theUrlForAddCounties">
  <c:param name="coverageYear">${coverageYear}</c:param>
</c:url>

<c:url value="/ssap/addCountiesForStates" var="theUrlForAddCountiesForStates">
  <c:param name="coverageYear">${coverageYear}</c:param>
</c:url>

<c:url value="/ssap/countycodedetails" var="theUrlForCountyCodeDetails">
  <c:param name="coverageYear">${coverageYear}</c:param>
</c:url>

<c:url value="/ssap/ping" var="theUrlForSession">
  <c:param name="coverageYear">${coverageYear}</c:param>
</c:url>
<c:url value="/ssap/getAlaskaAmericanStatesTribe" var="theUrlForAmericanAlaskaTribe">
  <c:param name="coverageYear">${coverageYear}</c:param>
</c:url>
<c:url value="/reviewSummarydownload/ssap/filedownloadbyid" var="theUrlForReviewSummaryPDF">

</c:url>

<%-- <c:url value="/shop/employee/addCounties" var="theUrlForAddCounties">
<c:param name="${df:csrfTokenParameter()}">
    <df:csrfToken plainToken="true"/>
  </c:param>
</c:url> --%>


<div class="gutter10 clearfix" id="ssapAppPages">
  <div id="progressDiv" class="hide row-fluid">
    <div class="span3"><span class="pull-right"><spring:message code="ssap.progressbar.yourProgress"/></span></div>
    <div id="progressBarDiv" class="span8">
      <span class="triangle-isosceles">0%</span>
      <div class="ssapProgressBar progress progress-info progress-striped">
        <div class="bar" style="width:0%;"></div>
      </div>
    </div>

  </div>
  <!--  <style>
   #rightpanel div {display: block !important}
 </style>  -->
  <div class="row-fluid" id="titlebar">
    <h1><span class="SSAPpgtitle"></span> <span class="nameOfHouseHold" id="headerHouseHoldName"></span></h1>
  </div>
  <div class="row-fluid">
    <jsp:include page="menu.jsp" />
    <div id="rightpanel" class="span9">
      <jsp:include page="pages/page1.jsp" />
      <jsp:include page="pages/page2.jsp" />
      <jsp:include page="pages/page3.jsp" />
      <jsp:include page="pages/page4.jsp" />
      <jsp:include page="pages/page5.jsp" />
      <jsp:include page="pages/page6.jsp" />
      <jsp:include page="pages/page7.jsp" />
      <jsp:include page="pages/page8.jsp" />
      <jsp:include page="pages/page9A.jsp" />
      <%-- <jsp:include page="pages/page9B.jsp" /> --%>
      <jsp:include page="pages/page10.jsp" />
      <jsp:include page="pages/page11.jsp" />
      <jsp:include page="pages/page12A.jsp" />
      <jsp:include page="pages/page12B.jsp" />
      <jsp:include page="pages/page13.jsp" />
      <jsp:include page="pages/page14.jsp" />
      <jsp:include page="pages/page15.jsp" />
      <jsp:include page="pages/page16.jsp" />
      <jsp:include page="pages/page17.jsp" />
      <jsp:include page="pages/page18.jsp" />
      <jsp:include page="pages/page19.jsp" />
      <jsp:include page="pages/page20.jsp" />
      <jsp:include page="pages/page21.jsp" />
      <jsp:include page="pages/page22.jsp" />
      <jsp:include page="pages/page23.jsp" />
      <jsp:include page="pages/page24.jsp" />
      <jsp:include page="pages/page25A.jsp" />
      <%-- <jsp:include page="pages/page25B.jsp" /> --%>
      <jsp:include page="pages/page26A.jsp" />
      <jsp:include page="pages/page26B.jsp" />
      <jsp:include page="pages/page27A.jsp" />
      <%-- <jsp:include page="pages/page27B.jsp" /> --%>
      <%-- <jsp:include page="pages/page28.jsp" /> --%>
      <%-- <jsp:include page="pages/page29.jsp" /> --%>
      <%-- <jsp:include page="pages/page30.jsp" /> --%>
      <%-- <jsp:include page="pages/page31A.jsp" /> --%>
      <jsp:include page="pages/page31B.jsp" />
      <%-- <jsp:include page="pages/page31C.jsp" /> --%>
      <jsp:include page="pages/page32.jsp" />
      <jsp:include page="pages/page33.jsp" />
      <jsp:include page="pages/page34.jsp" />
      <jsp:include page="pages/page35.jsp" />
      <jsp:include page="pages/page36.jsp" />
      <jsp:include page="pages/appscrBloodRel.jsp" />
      <jsp:include page="pages/page37.jsp" />

    </div>
  </div>

  <input type="button" value="testBLRE" style='display:none;' onClick="formSubmit();" class="btn btn-primary"/>
  <input type="hidden" value='${ssapApplicationJson}' id="ssapApplicationJson" name="ssapApplicationJson">
  <input type="hidden" value="${ssapCurrentPageId}" id="ssapCurrentPageId" name="ssapCurrentPageId">
  <input type="hidden" value="${ssapApplicationStatus}" id="ssapApplicationStatus" name="ssapApplicationStatus">
  <input type="hidden" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_APPLICATION_TYPE)%>" id="ssapApplicationType" name="ssapApplicationType">
  <input type="hidden" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)%>" id="exchamgeName" name="exchamgeName">
  <input type="hidden" value="${applicationMode}" id="applicationMode" name="applicationMode">
  <!-- broker -->
  <input type="hidden" value="${designatedBrokerLicenseNumber}" id="designatedBrokerLicenseNumber" name="designatedBrokerLicenseNumber">
  <input type="hidden" value="${designatedBrokerName}" id="designatedBrokerName" name="designatedBrokerName">
  <input type="hidden" value="${brokerFirstName}" id="brokerFirstName" name="brokerFirstName">
  <input type="hidden" value="${brokerLastName}" id="brokerLastName" name="brokerLastName">
  <input type="hidden" value="${internalBrokerId}" id="internalBrokerId" name="internalBrokerId">
  <!-- assister -->
  <input type="hidden" value="${designatedAssisterLicenseNumber}" id="designatedAssisterLicenseNumber" name="designatedAssisterLicenseNumber">
  <input type="hidden" value="${designatedAssisterName}" id="designatedAssisterName" name="designatedAssisterName">
  <input type="hidden" value="${assisterFirstName}" id="assisterFirstName" name="assisterFirstName">
  <input type="hidden" value="${assisterLastName}" id="assisterLastName" name="assisterLastName">
  <input type="hidden" value="${internalAssisterId}" id="internalAssisterId" name="internalAssisterId">
  <input type="hidden" value="${helpingEntity}" id="helpingEntity" name="helpingEntity">
  <input type="hidden" value='${alCountryOfIssuances}' id="alCountryOfIssuances" name="alCountryOfIssuances">
  <input type="hidden" value='${cmrHouseHold}' id="cmrHouseHold" name="cmrHouseHold">
  <input type="hidden" value="${csroverride}" id="csroverride" name="csroverride">
  <input type="hidden" value="${applicationSource}" id="applicationSource" name="applicationSource">
  <input type="hidden" value="${trackApplicationSource}" id="trackApplicationSource" name="trackApplicationSource">
  <input type="hidden" value="${applicantCounter}" id="applicantCounter" name="applicantCounter">
  <input type="hidden" value="${qepAllowedInterval}" id="qepAllowedInterval" name="qepAllowedInterval">
  <input type="hidden" value="${applicationType}" id="applicationType" name="applicationType">
  <input type="hidden" value="${coverageYear}" id="coverageYear" name="coverageYear">
  <input type="hidden" value="${mailingAddress.address1}" id="mailingAddressLine1" name="mailingAddressLine1">
  <input type="hidden" value="${mailingAddress.address2}" id="mailingAddressLine2" name="mailingAddressLine2">
  <input type="hidden" value="${mailingAddress.city}" id="mailingAddressCity" name="mailingAddressCity">
  <input type="hidden" value="${mailingAddress.state}" id="mailingAddressState" name="mailingAddressState">
  <input type="hidden" value="${mailingAddress.zip}" id="mailingAddressZip" name="mailingAddressZip">
  <input type="hidden" value="${mailingAddress.county}" id="mailingAddressCountyCode" name="mailingAddressCountyCode">
  <input type="hidden" value="no" id="isMailingAddressUpdated" name="isMailingAddressUpdated">
  <input type="hidden" value="${userEmailAddress}" id="userEmailAddress" name="userEmailAddress">
  <input type="hidden" value="${writtenLang}" id="writtenLang" name="writtenLang">
  <input type="hidden" value="${spokenLang}" id="spokenLang" name="spokenLang">
  <input type="hidden" value="${prefCommunication}" id="prefCommunication" name="prefCommunication">
  <input type="hidden" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" id="stateExchangeCode" name="stateExchangeCode">



</div>
<div class="row-fluid">
  <div class="form-actions noMargin clearfix ssapBottomNav span9 offset3">

    <!-- <div class="title_text_page" style="width: 96%;"></div> -->

    <div class="Continue_button1 span2" id="back_button_div">
      <a id="prevBtn" onClick="prev();" tabindex="0" class="btn pull-left" href="javascript:void(0)" style="display:none;">
        <spring:message code="ssap.main.back" text="Back"/></a>
    </div>

    <div class="pull-right">
      <div class="saveButton margin10-r" id="save_button_div" style="display:none;" onclick="saveData()">
        <a id="saveBtn" href="javascript:void(0)" onclick="" class="btn btn-primary"><spring:message code="ssap.main.save" text="Save"/></a>
      </div>

      <div class="Continue_button  margin5-l" id="countinue_button_div">
        <a id="contBtn" href="javascript:void(0)"  onclick="next();" tabindex="0" class="btn btn-primary"><spring:message code="ssap.main.continue" text="Continue"/></a>
      </div>
    </div>


    <div class="Cancel_button1" id="cancelButtonCancel" style="display:none;">
      <a href="javascript:void(0)" onClick="appscr58EditCancel()" class="btn margin10-r"><spring:message code="ssap.main.cancel" text="Cancel"/></a>
    </div>

    <div id="waiting" class="waiting" style='display:none;'>

      <div class="waiting-inner">
        <br>
        <span id="messageDiv"> <spring:message code="ssap.footer.processing" text="Processing please wait"/>  &nbsp; <img  src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/>
					 </span>
      </div>

    </div>
  </div>
</div>
<input id="houseHoldTRNo" type="hidden" name="houseHoldTRNo" value="1">
<span class="hide nameOfHouseHold"></span>

<div id="ssapSubmitModal" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  <div class="modal-body">
    <p align ="center">Processing please wait </p>
    <img  src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/>

  </div>
</div>
</body>
</html>

<script type="text/javascript">
  var d;
  $(document).ready(function(){
    d = "${model.dateOfServer}";
  });

  var theUrlFordeleteApplicant = '<c:out value="${theUrlFordeleteApplicant}" escapeXml="false"/>';
  var theUrlForSaveJsonData = '<c:out value="${theUrlForSaveJsonData}" escapeXml="false"/>';
  var theUrlForisRidpVerified =  '<c:out value="${theUrlForisRidpVerified}" escapeXml="false"/>';
  var theUrlForAddCounties =  '<c:out value="${theUrlForAddCounties}"/>';
  var theUrlForAddCountiesForStates =  '<c:out value="${theUrlForAddCountiesForStates}" escapeXml="false"/>';
  var theUrlForCountyCodeDetails =  '<c:out value="${theUrlForCountyCodeDetails}" escapeXml="false"/>';
  var theUrlForSession =  '<c:out value="${theUrlForSession}" escapeXml="false"/>';
  var theUrlForAmericanAlaskaTribe =  '<c:out value="${theUrlForAmericanAlaskaTribe}" escapeXml="false"/>';
  var theUrlForReviewSummaryPDF = '<c:out value="${theUrlForReviewSummaryPDF}" escapeXml="false"/>';


  function isInvalidCSRFToken(xhr) {
    var rv = false;
    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {
      alert($('Session is invalid').text());
      rv = true;
    }
    return rv;
  }
</script>

