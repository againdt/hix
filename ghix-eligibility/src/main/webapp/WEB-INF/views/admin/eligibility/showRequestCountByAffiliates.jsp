<html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script type="text/javascript">

$(function() {
	$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
	
	});

function validateDates(){

	var dateStart = document.getElementById('date1').value.split('/');
	
	if(dateStart != "" && dateStart.length > 0)
	{		
	var boolean1 = validateDate(dateStart);
	}

}

function validateDate(dateGiven){
	
	var today=new Date();

	var dob_mm = dateGiven[0];  
	var dob_dd = dateGiven[1];  
	var dob_yy = dateGiven[2]; 
	
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);

	if( (today.getFullYear() - 2) >  birthDate.getFullYear() ) {
	//alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}

	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) {
//	alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}

	if(today.getTime() < birthDate.getTime()){
//	alert('Please enter a valid date in MM/DD/YYYY format');
	return false; 
	}
	
}

function setWhereClause(type){
	
	var whereClause = '';
	
	switch (type) {		
		case 'B4' : 
			
			//Validate the dates
		    validateDates();	         
		         
			//Step 3: Get the Dates
			var date1 = document.getElementById('date1').value;
			
			if(date1)
			{	
				whereClause = " and to_char(E.creation_timestamp,'mm/dd/yyyy' )='"+date1+"'" ;		
			}else{
				whereClause = " and E.creation_timestamp >= sysdate-5 and E.creation_timestamp <= sysdate ";
			}
			
			break;
			
	}
	
	document.getElementById('whereClause').value = whereClause;
	//alert("whereClause : " + whereClause);
}
</script> 
<%
//Session Checking Begin -------------------------------------------------
session = request.getSession(false);
if(null == session)
{
	response.sendRedirect("index.jsp");
}
else
{
	String loggedin = (String)session.getAttribute( "loggedin");
	if(!"true".equals(loggedin))
	{		
		session.invalidate();	
		response.sendRedirect("../../index.jsp");
	}
}
//Session Checking End -------------------------------------------------
%>

<head>
<title>GHIX Prescreen Show Data for affiliates</title>
<style>
table#table1 {
	border: 1px solid #666;
	width: 80%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

th,td {
	padding: 2px 4px 2px 4px !important;
	text-align: left;
	vertical-align: top;	
}

thead tr {
	background-color: #fc0;
}

th.sorted {
	background-color: orange;
}

th a,th a:visited {
	color: black;
}

th a:hover {
	text-decoration: underline;
	color: black;
}

th.sorted a,th.sortable a {
	background-position: right;
	display: block;
	width: 100%;
}

tr.odd {
	background-color: #fff
}

tr.tableRowEven,tr.even {
	background-color: #fea
}

div.exportlinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	margin: 2px 0 10px 0;
	width: 79%;
}

span.export {
	padding: 0 4px 1px 20px;
	display: inline;
	display: inline-block;
	cursor: pointer;
}

span.pagebanner {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	margin-top: 10px;
	display: block;
	border-bottom: none;
}

span.pagelinks {
	background-color: #eee;
	border: 1px dotted #999;
	padding: 2px 4px 2px 4px;
	width: 79%;
	display: block;
	border-top: none;
	margin-bottom: -5px;
}

.group-1 {
    font-weight:bold;
    padding-bottom:10px;
    border-top:1px solid black;
}
.group-2 {
    font-style:italic;
    border-top: 1px solid black;

}
.subtotal-sum, .grandtotal-sum {
    font-weight:bold;
    text-align:right;
}
.subtotal-header {
    padding-bottom: 0px;
    border-top: 1px solid white;
}
.subtotal-label, .grandtotal-label {
    border-top: 1px solid white;
    font-weight: bold;
}
.grouped-table tr.even {
    background-color: #fff;
}
.grouped-table tr.odd {
    background-color: #fff;
}
.grandtotal-row {
    border-top: 2px solid black;
}
</style>
</head>
<body>
<form method="POST" action="showRequestCountByAffiliates">
		<h2>Select Date</h2>
		<table>
			<tr>
				<td><input type="text" id="date1" name="date1" size="10" maxlength="10">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');"></td>
			</tr>
		</table>		
		<input id="whereClause" name="whereClause" type="hidden">
</form>
<sql:query dataSource="jdbc/ghixDS" var="affiliates">
WITH t AS 
  (SELECT DISTINCT
               CONCAT(CONCAT('''',A.COMPANY_NAME),'''') AS AFFILIATE_NAME
          FROM elig_lead E, AFF_AFFILIATE A
         WHERE  E.AFFILIATE_ID = A.ID)
SELECT SUBSTR(MAX(AFFILIATE_NAME),2) AFFILIATES
FROM (SELECT SYS_CONNECT_BY_PATH(AFFILIATE_NAME, ',') AFFILIATE_NAME
      FROM ( SELECT AFFILIATE_NAME,
                    ROW_NUMBER() OVER (ORDER BY AFFILIATE_NAME) FILA
              FROM t)
      START WITH FILA = 1
      CONNECT BY PRIOR FILA = FILA - 1 )
</sql:query>

<c:forEach items="${affiliates.rows}" var="affiliate">
	<c:set var="companyName" value="${affiliate}"/>
</c:forEach>
<%
	String companyNames = pageContext.getAttribute("companyName").toString().replaceAll("\\{AFFILIATES=", "").replaceAll("\\}","");
%>
<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "and E.creation_timestamp >= sysdate-5 and E.creation_timestamp <= sysdate ";
	}
%>
<sql:query dataSource="jdbc/ghixDS" var="result1">
WITH ALL_OBJECTS_DATA AS (
SELECT TRUNC(E.CREATION_TIMESTAMP) AS "Request Date",
               A.COMPANY_NAME AS AFFILIATE_NAME
          FROM elig_lead E, AFF_AFFILIATE A
         WHERE  E.AFFILIATE_ID = A.ID <%=whereClause%>
)
SELECT * FROM ALL_OBJECTS_DATA
PIVOT (COUNT(*) FOR AFFILIATE_NAME IN (<%=companyNames%>))  ORDER BY "Request Date" DESC
</sql:query> 
	<b>Request count for Affiliates in last 5 days</b>
<br>
<display:table name="${result1.rows}"  requestURI="showRequestCountByAffiliates" id="table1" export="true" >
</display:table>
</body> 
</html>


