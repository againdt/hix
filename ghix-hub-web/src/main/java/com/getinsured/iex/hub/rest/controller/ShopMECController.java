package com.getinsured.iex.hub.rest.controller;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

/**
 * Controller class to handle synchronous request to State MEC module
 * 
 * @author Vishaka Tharani
 * @since 6-May-2014
 * 
 */
@Controller
public class ShopMECController extends MessageProcessor {

	
	@Autowired
	private WebServiceTemplate shopMECApplicantServiceTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ShopMECController.class);

	@RequestMapping(value = "/invokeShopMEC", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeSMEC(@RequestBody String sMECRequestJSON){
		
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge nesiMecBridge = HubServiceBridge.getHubServiceBridge("ShopMECRequest");
			handler = nesiMecBridge.getServiceHandler();
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(sMECRequestJSON);
			
			String nMECInputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			
			handler.setJsonInput(nMECInputJSON);
		
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("ShopMEC");
			requestRecord.setEndpointOperationName("ShopMEC Eligibility");
			requestRecord.setAccessIp((String) objInputJson.get("clientIp"));
			
			Map<String,Object> context = new HashMap<String, Object>();
			
			boolean validOrgCode = false;
			if(objInputJson.get("organizationCode") != null){
				String orgCode =  (String) objInputJson.get("organizationCode");
				
				if(orgCode != null && !orgCode.isEmpty()){
					context.put("organizationCode",orgCode);
					handler.setContext(context);
					validOrgCode = true;
				}
			}
			
			if(!validOrgCode){
				throw new HubServiceException("Organization Code is not provided");
			}
			requestRecord.setStatus("PENDING");
			if(objInputJson.get("applicationId") != null){
				requestRecord.setSsapApplicationId((Long) objInputJson.get("applicationId"));
			}
			response = this.executeRequest(requestRecord, handler, shopMECApplicantServiceTemplate);
				
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
}
