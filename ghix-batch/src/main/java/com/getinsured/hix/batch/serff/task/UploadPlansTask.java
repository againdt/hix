package com.getinsured.hix.batch.serff.task;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.serff.service.BatchDataLoadService;

/**
 * This initiates SERFF Batch Job to process requests received through UI
 * 
 * @author Shashikant
 * @since Dec-05-2013
 */
public class UploadPlansTask implements Tasklet{
	private static final Logger LOGGER = Logger.getLogger(UploadPlansTask.class);

	private BatchDataLoadService batchDataLoadService;
	
	public BatchDataLoadService getBatchDataLoadService() {
		return batchDataLoadService;
	}
	
	public void setBatchDataLoadService(BatchDataLoadService batchDataLoadService) {
		this.batchDataLoadService = batchDataLoadService;
	}
	
	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1)
			throws Exception {
		
		LOGGER.info("SERFF - Upload plan job started.");
		
		getBatchDataLoadService().processSerffPlanMgmtBatch();		
		
		LOGGER.info("SERFF - Upload plan job end.");
		
		return RepeatStatus.FINISHED;
	}
}
