<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
	<head>
	</head>
	<body>
	
		<!--  <a href="#" class="closeCompPopUp" onClick="window.parent.closeEmployeeSummaryPopup();">X</a>-->
		
		<div class="markCompleteHeader">
	    	<div class="header">
	            <h4 class="margin0 pull-left">${employeeName}</h4>
	           <!--   <button onClick="window.parent.closeEmployeeSummaryPopup(); aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>-->
	        	<a href="#" id="crossClose" class="dialogClose" onClick="window.parent.closeEmployeeSummaryPopup();">x</a>
	        </div>
		</div>
		<div id="empSumPopUp">
			<BR/>
		
			<BR/>
			<spring:message code="label.company" /> ${employeeCompanyName}
			<BR/>
			<spring:message code="label.phoneNumber" /> ${employeePhoneNumber}
			<BR/>
			<spring:message code="label.emailAddress" /> ${employeeEmail}
			<BR/>
			<input type="button"  class="btn pull-right" data-dismiss="modal" onClick="window.parent.closeEmployeeSummaryPopup();" value="<spring:message code="label.button.close" />" title="<spring:message code="label.button.close" />" />
			<BR/>
		</div>
	</body>
</html>