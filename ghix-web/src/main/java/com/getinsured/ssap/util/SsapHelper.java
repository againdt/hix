package com.getinsured.ssap.util;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.indportal.enums.SsapApplicationTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.ssap.model.eligibility.EligibilityRequest;
import com.getinsured.ssap.model.eligibility.RunMode;
import com.getinsured.timeshift.util.TSDate;

/**
 * Utility methods for SSAP.
 *
 * @author Yevgen Golubenko
 * @since 2019-09-23
 */
public class SsapHelper {
  private static final Logger log = LoggerFactory.getLogger(SsapHelper.class);

  public static long getApplicationId(SingleStreamlinedApplication application) {
    if (application != null && application.getSsapApplicationId() != null) {
      if (!application.getSsapApplicationId().equals("0")) {
        try {
          return Long.parseLong(application.getSsapApplicationId());
        } catch (Exception e) {
          log.error("Unable to parse supplied applicationId from JSON: {}, error: {}", application.getSsapApplicationId(), e.getMessage());
        }
      }
    }

    return 0L;
  }

  /**
   * Sets parameter of {@link EligibilityRequest} to a specific value that is determined by the following rules:
   * <ul>
   *  <li>In OEP window, set typeOfApplication to OEP.</li>
   *  <li>If new application (applicationType = QEP) and outside of OEP, set typeOfApplication to QEP</li>
   *  <li>If reporting a change on an existing application (applicationType = SEP) and Outside of OEP, set typeOfApplication to SEP</li>
   * </ul>
   * @param application {@link SingleStreamlinedApplication} object.
   * @param eligibilityRequest {@link EligibilityRequest} object.
   * @param runMode {@link RunMode} object.
   */
  public static void setApplicationTypeForEligibilityEngine(final SingleStreamlinedApplication application,
                                                            final EligibilityRequest eligibilityRequest,
                                                            final RunMode runMode) {

    if(application == null || eligibilityRequest == null) {
      log.error("Cannot set applicationType for eligibility request, because required parameters are application: {}, eligibilityRequest: {}", application, eligibilityRequest);
      return;
    }

    final String oepStartDateConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
    final String oepEndDateConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);

    Date oepStartDate = null;
    Date oepEndDate = null;

    if(!oepEndDateConfig.isEmpty()) {
      try {
        oepStartDate = DateUtils.parseDate(oepStartDateConfig, GhixConstants.REQUIRED_DATE_FORMAT);
      } catch (ParseException e) {
        log.error("Unable to parse configured OEP start date: {}, error: {}", oepStartDateConfig, e.getMessage());
      }
    }

    if(!oepEndDateConfig.isEmpty()) {
      try {
        oepEndDate = DateUtils.parseDate(oepEndDateConfig, GhixConstants.REQUIRED_DATE_FORMAT);
      } catch (ParseException e) {
        log.error("Unable to parse configured OEP end date: {}, error: {}", oepEndDateConfig, e.getMessage());
      }
    }

    if(oepStartDate != null && oepEndDate != null) {
      final Date now = new TSDate();
      final boolean inOepWindow = (now == oepStartDate || now.after(oepStartDate)) && (now == oepEndDate || now.before(oepEndDate));

      if(inOepWindow) {
        eligibilityRequest.setTypeOfApplication(SsapApplicationTypeEnum.OE);
      } else {
        if(application.getApplicationType().equals(SsapApplicationTypeEnum.QEP.name())) {
          eligibilityRequest.setTypeOfApplication(SsapApplicationTypeEnum.QEP);
        } else if(application.getApplicationType().equals(SsapApplicationTypeEnum.SEP.name())) {
          eligibilityRequest.setTypeOfApplication(SsapApplicationTypeEnum.SEP);
        }
      }

      log.info("Set typeOfApplication to " + eligibilityRequest.getTypeOfApplication() + "; in OEP window: {}", inOepWindow);
    }
  }
}
