package com.getinsured.hix.provider.service.zipcode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SolrZipcodeSearchController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SolrZipcodeSearchController.class);
	private static final String SEARCH_KEY = "zipcode";

	@Autowired
	private SolrZipCodeService solrZipCodeService;

	@RequestMapping(value = "/zipcode/search",method = RequestMethod.GET, headers="Accept=*/*")
	public @ResponseBody String findByZipCode(Model model, @RequestParam(value = SEARCH_KEY, required = true) String searchKey)
	{
		if(searchKey!=null && !(searchKey.isEmpty())){
			try {
				ZipCode zip = solrZipCodeService.findByZipCode(searchKey);
				return zip.toJSONString();
			} catch (Exception e) {
				LOGGER.error("Error finding zip code:",e);
				return null;					
			}
		}
		return null;
	}

	public SolrZipCodeService getZipService() {
		return solrZipCodeService;
	}

	public void setZipService(SolrZipCodeServiceImpl zipService) {
		this.solrZipCodeService = zipService;
	}
}