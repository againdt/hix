package com.getinsured.hix.batch.repository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.Notice;

@Repository
@Qualifier("batchNoticeRepository")
public interface BatchNoticeRepository extends JpaRepository<Notice, Integer> {
	
	@Query("FROM Notice as notice where notice.noticeType.notificationName = 'Additional Information for Non Financial Application' "
			+ "and notice.created >= :startDate and notice.created <= :endDate "
			+ "and notice.keyId = :moduleId and notice.keyName = 'INDIVIDUAL'")
	List<Notice> additionalInfoNoticesCreatedForModuleIdBetween(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("moduleId") Integer moduleId);

	@Query("select count(notice.id) FROM Notice as notice where notice.noticeType.notificationName = :notificationName "
			+ "and notice.created >= :startDate and notice.created <= :endDate "
			+ "and notice.keyId = :moduleId and notice.keyName = 'INDIVIDUAL'")
	long lCENoticeCreatedForModuleIdBetween(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("moduleId") Integer moduleId, @Param("notificationName") String notificationName);

	@Query("FROM Notice as notice WHERE notice.id in (:noticeIdList)")
	List<Notice> findNoticesByIdList(@Param("noticeIdList") List<Integer> noticeIdList);
}