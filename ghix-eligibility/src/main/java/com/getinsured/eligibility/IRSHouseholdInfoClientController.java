package com.getinsured.eligibility;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import us.gov.treasury.irs.common.IRSHouseholdGrpType;

import com.getinsured.eligibility.householdinfo.irs.*;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

@Controller
@RequestMapping("/irsHouseholdInfoClientController")
public class IRSHouseholdInfoClientController extends IRSHouseHoldUtil {
	private static final Logger LOGGER = Logger.getLogger(IRSHouseholdInfoClientController.class);

	@RequestMapping(value = "/requestToIrsHouseholdInfo", method = RequestMethod.POST)
	@ResponseBody
	public String callIrsHouseholdInfo(@RequestBody IRSHouseholdClientRequest irsHouseholdClientRequest) throws GIException {
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		LOGGER.debug("IRSHouseholdClientRequest invoked");
		return xstream.toXML(getIrsHouseholdInfo(irsHouseholdClientRequest));
	}
	
}
