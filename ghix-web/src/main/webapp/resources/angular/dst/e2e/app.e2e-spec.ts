import { WashingtonfullPage } from './app.po';

describe('washingtonfull App', () => {
  let page: WashingtonfullPage;

  beforeEach(() => {
    page = new WashingtonfullPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
