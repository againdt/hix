package com.getinsured.eligibility.at.ref.si;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.mes.service.QueueProcessingService;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.service.ReferralProcessingService;
import com.getinsured.eligibility.at.ref.service.ReferralSsapCmrLinkService;
import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AssisterType;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TimeShifterUtil;

/**
 * @author chopra_s
 *
 */
@Component("oeReferralProcessingService")
@Scope("singleton")
public class OeReferralProcessingService {
	private static final Logger LOGGER = LoggerFactory.getLogger(OeReferralProcessingService.class);

	@Autowired
	@Qualifier("referralProcessingService")
	private ReferralProcessingService referralProcessingService;

	@Autowired
	@Qualifier("referralSsapCmrLinkService")
	private ReferralSsapCmrLinkService referralSsapCmrLinkService;
	
	@Autowired
	private ExternalAssisterService externalAssisterService;
	
	@Autowired
	private QueueProcessingService queueProcessingService;

	@Autowired
	private ExceptionUtil exceptionUtil;
	
	public String processReferral(Message<String> message) {
		
		ReferralResponse referralResponse = new ReferralResponse();
		AccountTransferRequestDTO accountTransferRequest = null;
		long ssapApplicationId = 0;
		try {
			LOGGER.info("OeReferralProcessingService processReferral starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());
			referralResponse.getData().putAll(input.getData());
			
			// Retrieve request object from message which was set in step routing step.
			accountTransferRequest = (AccountTransferRequestDTO)referralResponse.getData().get(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
			referralResponse.getData().put(ReferralProcessingConstants.KEY_APPLICATIONS_WITH_SAME_ID, accountTransferRequest.getApplicationsWithSameId());

			if(GhixPlatformConstants.TIMESHIFT_ENABLED) {
				TimeShifterUtil.initializeTimeContext(accountTransferRequest);
			}
			// Get Organization Identification ID from Authorized Representative which is used as External Applicant ID in users
			referralResponse.getData().put(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID, referralProcessingService.getOrganizationIdentification(accountTransferRequest.getAccountTransferRequestPayloadType()));

			ssapApplicationId = referralProcessingService.executeOeReferral(accountTransferRequest);
			referralResponse.getData().put(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID, ssapApplicationId);

			// set the HouseholdCaseId in ReferralResponse 
			referralProcessingService.setExternalHouseholdCaseIdInReferralResponse(referralResponse,accountTransferRequest);
			
			// config for MN
			final String hasLinkUserStr =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
			
			// for MN if Assister tag is passed then set it in the map
			if (null != hasLinkUserStr && "TRUE".equalsIgnoreCase(hasLinkUserStr)) {
				if (null != accountTransferRequest.getAccountTransferRequestPayloadType().getAssister()) {
					referralResponse.getData().put(ReferralConstants.ASSISTER_INFO,
							accountTransferRequest.getAccountTransferRequestPayloadType().getAssister());
				}
			}
			
			// set mes procecssing required fields in referral response 
			referralResponse.getData().put(ReferralProcessingConstants.SPAN_INFO_DTO,queueProcessingService.populateATQueueInfoRequestDTO(accountTransferRequest));
						
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_CREATING_REFERRAL_APPLICATION + accountTransferRequest.getGiwsPayloadId());
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_CREATING_REFERRAL_APPLICATION).append(accountTransferRequest.getGiwsPayloadId()).append(ReferralProcessingConstants.REASON)
			        .append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error("Process OE referral "+errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		} finally {
			Map<String, Object> data = referralResponse.getData();
			data.put(ReferralProcessingConstants.KEY_GI_WS_PAYLOAD_ID, accountTransferRequest.getGiwsPayloadId());
			data.put(ReferralProcessingConstants.KEY_COMPARED_TO_APPLICATION_ID, accountTransferRequest.getCompareToApplicationId());
			data.put(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY, accountTransferRequest.getAccountTransferCategory());
			// Remove request object from message as further steps won't need it. It will be recreated when required.
			data.remove(ReferralProcessingConstants.ACCOUNT_TRANSFER_REQUEST_DTO);
		}

		final String response = EligibilityUtils.marshal(referralResponse);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("OeReferralProcessingService processReferral ends - Response is - {}".intern(), response);
		}

		return response;
	}

	public String processLinkCmr(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		long ssapApplicationId = 0;
		try {
			LOGGER.info("OeReferralProcessingService processLinkCmr starts ".intern());
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			String hhCaseId = (String) input.getData().get(ReferralConstants.HOUSE_HOLD_CASE_ID);

			Map<String, Object> mpData = null;
			ssapApplicationId = (long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID);
			
			if (referralSsapCmrLinkService.hasLinkUserEnable() && null != input.getData().get(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID)) {
				mpData = referralSsapCmrLinkService.executeOELinking((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), hhCaseId,
						input.getData().get(ReferralProcessingConstants.KEY_AUTH_REPS_ORGA_IDEN_ID).toString());
			}
			else {
				mpData = referralSsapCmrLinkService.executeOELinking((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), hhCaseId);
			}
			
			// call External Assister Designate/De-designate API
			final String hasLinkUserStr = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_LINK_USER);
			if (null != hasLinkUserStr && "TRUE".equalsIgnoreCase(hasLinkUserStr)) {
				AssisterType assisterInfo = (AssisterType) input.getData().get(ReferralConstants.ASSISTER_INFO);
				if(null != assisterInfo) {
					externalAssisterService.callExternalAssisterDesignateAPI(assisterInfo, (int) mpData.get(ReferralConstants.HOUSEHOLD_ID));	
				} else {
					externalAssisterService.callExternalAssisterDeDesignateAPI((int) mpData.get(ReferralConstants.HOUSEHOLD_ID));
				}
			}
			referralResponse.getData().put(ReferralProcessingConstants.REFERRAL_AUTOLINKING, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.AUTO_LINK)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.MULTIPLE_CMR)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR, ReferralUtil.convertToBoolean(mpData.get(ReferralProcessingConstants.NON_FINANCIAL_CMR)));
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID, ReferralUtil.convertToInt(mpData.get(ReferralProcessingConstants.NON_FINANCIAL_CMR_ID)));

			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_LINK_CMR + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_LINK_CMR).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_LINK_CMR);
			exceptionUtil.persistGiMonitorId(ssapApplicationId, e);
		}
		final String response = EligibilityUtils.marshal(referralResponse);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("OeReferralProcessingService processLinkCmr ends - Response is - {}".intern(), response);
		}

		return response;
	}
	
	private void initializeTimeContext(AccountTransferRequestDTO accountTransferRequest) {
		String user = accountTransferRequest.getUser();
		String offset = accountTransferRequest.getTsOffset();
		if(user.equalsIgnoreCase("na".intern())) {
			LOGGER.info("No user context available from the DTO, checking the request attributes if any".intern());
			ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
			if (sra!= null){
				HttpServletRequest req = sra.getRequest();
				user = req.getHeader("X-USER".intern());
				offset = req.getHeader("X-USER-OFFSET".intern());
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Checked the request context for TS context for user {} with offset {}", user, offset);
				}
				TimeShifterUtil.setUserOffset(user, offset);
				return;
			}
		}
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("TS Context available for user {} with offset {}",user,offset);
		}
		TimeShifterUtil.setUserOffset(user, offset);
	}
}
