<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="gutter10">
	<div class="row-fluid">
		<h1 class="gutter10"><a name="skip"></a>Issuer Administrator FAQs</h1>
		<small class="gutter10"><a href="<c:url value='/faqhome'/>"class="pull-right">FAQ Home</a></small>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
	    	<div class="header">
				<h4 class="margin0">&nbsp;</h4>
			</div>
			<df:csrfToken/>
					<ul class="nav nav-list faq-nav" id="nav">
						<li><a href="#issuerAdminGroup1"><spring:message code="label.issuerAdminGroup1" htmlEscape="false"/></a></li>
						<li><a href="#issuerAdminGroup2"><spring:message code="label.issuerAdminGroup2" htmlEscape="false"/></a></li>
					</ul>
		</div>
		<div class="span9 " id="rightpanel">
		
		
			<!-------------------------------group 1----------------------------------------------->
			<div id="issuerAdminGroup1">
				<div class="header">
					<h4><spring:message code="label.issuerAdminGroup1" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<!--------------------------- question 1 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminAddNewIssuerQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminAddNewIssuerA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminAddNewIssuerA3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminAddNewIssuerA4" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminAddNewIssuerA5" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminAddNewIssuerA6" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminAddNewIssuerA7" htmlEscape="false"/></li>
	  			</ul>
	  			
	  			<!--------------------------- question 2 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminViewInfoQ" htmlEscape="false"/></h4>
	  			</div>
				<ul>
	  				<li><spring:message code="label.issuerAdminViewInfoA2" htmlEscape="false"/></li>
	  			</ul>
	  			
				<!--------------------------- question 3 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminChangeInfoQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminChangeInfoA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeInfoA3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeInfoA4" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeInfoA5" htmlEscape="false"/></li>
	  			</ul>
	  			
	  			<!--------------------------- question 4 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminChangeInfoIMPQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminChangeInfoIMPA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeInfoIMPA3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeInfoIMPA4" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeInfoIMPA5" htmlEscape="false"/></li>
	  			</ul>
	  			
			
	  			
	  			<!--------------------------- question 5 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminChangeCertificationQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminChangeCertificationA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeCertificationA3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeCertificationA4" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminChangeCertificationA5" htmlEscape="false"/></li>
	  			</ul>				
				
			</div>
			

			<!-------------------------------group 2----------------------------------------------->
			<div id="issuerAdminGroup2">
				<div class="header">
					<h4><spring:message code="label.issuerAdminGroup2" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>	
				
				<!--------------------------- question 6 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminManagePlansQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminManagePlansA2" htmlEscape="false"/></li>
	  			</ul>
				<!--------------------------- question 7 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminSearchPlanQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminSearchPlanA1" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminSearchPlanA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminSearchPlanA3" htmlEscape="false"/></li>
	  			</ul>
	  			<!--------------------------- question 8 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminDetailedPlanQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminDetailedPlanA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminDetailedPlanA3" htmlEscape="false"/></li>
	  			</ul>
	  			<!--------------------------- question 9 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminViewBenefitsQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminViewBenefitsA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewBenefitsA3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewBenefitsA4" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewBenefitsA5" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewBenefitsA6" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewBenefitsA7" htmlEscape="false"/></li>
	  			</ul>
	  			<!--------------------------- question 10 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminViewRatesQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminViewRatesA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewRatesA3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewRatesA4" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewRatesA5" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewRatesA6" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminViewRatesA7" htmlEscape="false"/></li>
	  			</ul>
	  			<!--------------------------- question 11 ------------------------>
				<div class="lightgrey">
	  				<h4><spring:message code="label.issuerAdminCertifyPlanQ" htmlEscape="false"/></h4>
	  			</div>
	  			<ul>
	  				<li><spring:message code="label.issuerAdminCertifyPlanA2" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminCertifyPlanA3" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminCertifyPlanA4" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminCertifyPlanA5" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminCertifyPlanA6" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminCertifyPlanA7" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminCertifyPlanA8" htmlEscape="false"/></li>
	  				<li><spring:message code="label.issuerAdminCertifyPlanA9" htmlEscape="false"/></li>
	  			</ul>
	  			<p class="pull-right"><a id="role-top" href="#">Back to Top</a></p>
			</div>

		</div>
	</div>
</div>




<script>
$(document).ready(function(){
	document.title="Help for YHI";
});

$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});

</script>		