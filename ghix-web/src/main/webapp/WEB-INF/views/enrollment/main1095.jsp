<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<!-- The following stylesheet link has already been moved to shell.jsp. 
	If you are working on this page try removing the stylesheet link.
	If the page works fine without it, just delete the entire line along with this comment. -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<style type="text/css">
.form-border{
	    border: 1px solid #d3d3d3;
    border-top: none;
}
.search-applicant-results th, .search-applicant-results td {
  word-break: normal;
}
.effectiveYearHolder{
	overflow: hidden;
	height: 39px;
	margin-top:-12px;
}
.effectiveLabel{
	font-weight: normal;
	font-size: 12px;
}
.effectiveYearHolder select{
	margin-top:8px;
}
#consumerSearch .control-group{
	margin-bottom:5px !important;
}
.popover-content{
padding:0px !important;
}
.popover-content.ng-binding:before{
	margin:1px !important;
}
</style>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<div class="gutter10"  ng-app="enrollmentApp" ng-controller="enrollmentAppCtrl">
<!--  Start Search Screen -->
	<div id="searchScreen" ng-show="currentScreen=='search'">
	<div class="row-fluid">		
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out></p>
			</c:if>
			<br>
		</div>
		<div class="">
			<h1>
				1095-A Records &nbsp;				
			</h1>
		</div>
	</div>
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="header">
					<h4>
						Search
						<div class="pull-right effectiveYearHolder">
							<span class="effectiveLabel">Coverage Year</span>
							<span><select class="input-small"  id="coverageYear" ng-model="editToolJson.coverageYear" ng-options="year for year in coverageYears">							
							</select></span>
						</div>
					</h4>
			</div>
		</div>
		<div class="row-fluid">
			<form  method="POST" id="consumerSearch" name="consumerSearch" class="form-vertical gutter10 lightgray form-border" >
				<df:csrfToken/>
				<input type="hidden" id="effectiveYear" name="effectiveYear" value="${searchCriteria.effectiveYear}"></input>
				<div class="row-fluid">
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase">First Name (Recepient/Spouse)</label>
							<div class="controls">
								<input class="filterField" type="text" placeholder="First Name" ng-model="editToolJson.firstName" maxlength="40" />
							</div>
						</div>						
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase">Last Name (Recepient/Spouse)</label>
							<div class="controls">
								<input class="filterField" type="text" placeholder="Last Name" ng-model="editToolJson.lastName" maxlength="40" />
							</div>
						</div>						
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label">SSN (Recepient/Spouse)</label>
							<div class="controls">
								<input class="filterField" type="text" placeholder="SSN(last 4)" ng-model="editToolJson.ssn" maxlength="4" />
							</div>
						</div>
					</div>					
				</div>
				<div class="row-fluid">					
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase">Exchange Assigned Policy ID</label>
							<div class="controls">
								<input class="filterField" type="text" placeholder="Policy ID" ng-model="editToolJson.policyNumber" maxlength="10" />
							</div>
						</div>
					</div>	
					<div class="span4">
						<div class="control-group">
							<label class="control-label" >&nbsp;</label>
							<div class="controls">
								<button type="button" ng-click="resetSearchForm()" class="btn">Reset All</button>								
								<button type="button" ng-click="goSearch()" class="btn btn-primary margin10-l"><spring:message  code='label.go'/></button>
							</div>
						</div>
					</div>				
				</div>				
			</form>
		</div>
	</div>
	
	<div class="alert alert-info" ng-if="firstTimeSearch">
			Please specify search criteria.
	</div>
	<div class="alert alert-info ng-hide" ng-show="(!firstTimeSearch) && searchResult.length<1">
			No record found.
	</div>

	<div class="row-fluid ng-hide" ng-show="(!firstTimeSearch) && searchResult.length>0">
		<div class="span12" id="rightpanel">
			<table class="table table-striped search-applicant-results">
				<thead>
					<tr class="graydrkbg ">	
						<th style="cursor: pointer;"><div ng-click="setSortOrder()">Recipient Name <span ng-if="editToolJson.sortOrder=='ASC'"><img src="/hix/resources/images/i-aro-blu-sort-up.png" alt="State sort ascending" /></span> <span ng-if="editToolJson.sortOrder=='DESC'"> <img src="/hix/resources/images/i-aro-blu-sort-dwn.png" alt="State sort descending" /></span></div></th>
						<th>Recipient SSN</th>
						<th>Spouse Name</th>	
						<th>Spouse SSN</th>
						<th>Cov. Year</th>
						<th>Exchange Assigned<br/> Policy ID</th>
						<th>PDF Sent on<br/>(MM/DD/YYYY)</th>
						<th>1095-A</th>												
					</tr>					
				</thead>
				<tr  ng-repeat="singleResult in searchResult">
						<td>{{singleResult.recepientFirstName}}&nbsp;{{singleResult.recepientLastName}}</td>													
						<td>{{singleResult.recepientSsn}}</td>
						<td>{{singleResult.spouseFirstName}}&nbsp;{{singleResult.spouseLastName}}</td>													
						<td>{{singleResult.spouseSsn}}</td>
						<td>{{singleResult.coverageYear}}</td>
						<td>{{singleResult.policyNumber}}</td>
						<td>{{singleResult.pdfGeneratedOn}}</td>
						<td>
							<a href="javascript:void(0)" ng-click="getDetail(singleResult.id)">View </a>															
						</td>													
					</tr>
			</table>
			<div class="pagination txt-center">
				<div class="pagination"> <ul> <li></li><li ng-if="pageSet>1"><a href="javascript:void(0)" ng-click="prePage();">Prev</a></li>
				<li  class="{{page.activeClass}}"  ng-repeat="page in pages"><a href="javascript:void(0)" ng-click="pageClick(page.pageNo)"><span class="aria-hidden">Page </span>{{page.pageNo}}</a></li>
				<li ng-if="totalPages>(pageSet+pagesPerSet)"><a href="javascript:void(0)" ng-click="nextPage();" data-label="last page">Next</a></li>
				<li>&nbsp;&nbsp;[ Total Records : {{totalRecords}} ]</li>
				</ul></div>
			</div>
					
		</div>		
	</div>
	</div>
<!-- End of Search Screen -->

<!-- Start of Detail Screen -->
<div id="detailScreen" class="ng-hide" ng-show="currentScreen=='consumerDetail'">
	<form class="form-horizontal" id="frmConsuerDetail" name="frmConsuerDetail" action="" method="POST" novalidate>		
		<div id="rightpanel" class="span9-remove">
			<div class="">
			<h1>
				1095-A Records &nbsp;				
			</h1>
			</div>
			<div class="header">
                 <h4 class="pull-left">Recipient Information
                 </h4>
                 
                 <div ng-hide = "editDetailMod">
	                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="editDetail()">Edit</a>
	                 <span class="disabled" ng-if="enrollment1095Details.correctionIndicatorXml=='Y' || enrollment1095Details.correctionIndicatorPdf=='Y' || enrollment1095Details.resendPdfFlag=='Y' && enrollment1095Details.pdfFileName == null">
			                 <a class="btn btn-small pull-right disabled" title="Resend" href="javascript:void(0)">Resend</a>
			         </span>
			         <span ng-if="enrollment1095Details.correctionIndicatorXml!='Y' && enrollment1095Details.correctionIndicatorPdf!='Y' && enrollment1095Details.resendPdfFlag!='Y' && enrollment1095Details.pdfFileName != null">
		                 	<a class="btn btn-small pull-right" title="Resend" href="javascript:void(0)" ng-click="resendDetail()">Resend</a>
		             </span>
                 </div>
                 <!-- 
                 <div ng-show = "editDetailMod">
                 <a class="btn btn-small pull-right" title="Edit"  href="javascript:void(0)" ng-click="cancelEditl()">Cancel</a>
                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="saveDetail()">Save</a>                                 
                 </div> -->
            </div>
            <div class="gutter10">
			
				<table class="table-border-none table">
					<tbody>
						<tr>
							<td class="txt-right span4"> Recipient First Name <!-- <spring:message  code='label.columnHeaderName'/>-->:</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.firstName}} </strong></div>
							<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="<spring:message code="label.firstName" />" name="firstName" id="firstName" ng-model="recipent.firstName" ng-change="syncMembers(recipent.memberId,false);" maxlength="40" ng-pattern="/^[a-zA-Z '-]+$/" required />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.firstName.$error.required && frmConsuerDetail.firstName.$dirty" > 
									Please enter recipient first name
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.firstName.$error.pattern && frmConsuerDetail.firstName.$dirty" > 
									Please enter correct recipient first name
								</span>
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Recipient Middle Name <!-- <spring:message  code='label.columnHeaderName'/>-->:</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.middleName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Recipient Middle Name" name="recipientMiddleName" id="recipientMiddleName" ng-model="recipent.middleName" ng-change="syncMembers(recipent.memberId,false);"  maxlength="40" ng-pattern="/^[a-zA-Z '-]+$/" required  />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientMiddleName.$error.required && frmConsuerDetail.recipientMiddleName.$dirty" > 
									Please enter recipient middle name
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientMiddleName.$error.pattern && frmConsuerDetail.recipientMiddleName.$dirty" > 
									Please enter correct recipient middle name
								</span>
							</div>
							</td>
						</tr>						
						<tr>
							<td class="txt-right span4"> Recipient Last Name <!-- <spring:message  code='label.columnHeaderName'/>-->:</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.lastName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Recipient Last Name" name="recipientLastName" id="recipientLastName" ng-model="recipent.lastName" ng-change="syncMembers(recipent.memberId,false);"  maxlength="40" ng-pattern="/^[a-zA-Z '-]+$/" required  />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientLastName.$error.required && frmConsuerDetail.recipientLastName.$dirty" > 
									Please enter recipient last name
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientLastName.$error.pattern && frmConsuerDetail.recipientLastName.$dirty" > 
									Please enter correct recipient last name
								</span>
								
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Recipient SSN :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.ssn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" name="recipientSSN" id="recipientSSN" ng-model="recipent.ssn" ng-change="syncMembers(recipent.memberId,false);"  maxlength="11" required ui-mask="999-99-9999" ui-mask-placeholder ui-mask-placeholder-char="#" model-view-value="true" />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientSSN.$error.required && frmConsuerDetail.recipientSSN.$dirty" > 
									Please enter recipient ssn
								</span>
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Recipient Date Of Birth :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.birthDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField input-medium dob-input ng-invalid ng-invalid-required ng-valid-mask ng-dirty ng-valid-dob" date-validation="dob" type="text" name="recipientDateOfBirth" id="recipientDateOfBirth" ng-model="recipent.birthDate" ng-change="syncMembers(recipent.memberId,false);"  maxlength="40" required ui-mask="99/99/9999" model-view-value="true" placeholder="MM/DD/YYYY" />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientDateOfBirth.$error.required && frmConsuerDetail.recipientDateOfBirth.$dirty" > 
									Please enter recipient date of birth
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientDateOfBirth.$error.dob && frmConsuerDetail.recipientDateOfBirth.$pristine" > 
									Please enter valid recipient date of birth
								</span>
							</div>
							</td>
						</tr>
						<tr ng-show = "isSpouse">
							<td class="txt-right span4"> Spouse First Name :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.firstName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Spouse First Name" name="spouseFirstName" id="spouseFirstName" ng-model="spouse.firstName" ng-change="syncMembers(spouse.memberId,false);"   maxlength="40" ng-pattern="/^[a-zA-Z '-]+$/" required  />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseFirstName.$error.required && frmConsuerDetail.spouseFirstName.$dirty" > 
									Please enter spouse first name
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseFirstName.$error.pattern && frmConsuerDetail.spouseFirstName.$dirty" > 
									Please enter correct spouse first name
								</span>
							</div>
							</td>
						</tr>
						<tr ng-show = "isSpouse">
							<td class="txt-right span4"> Spouse Middle Name :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.middleName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Spouse Middle Name" name="spouseMiddleName" id="spouseMiddleName" ng-model="spouse.middleName" ng-change="syncMembers(spouse.memberId,false);" maxlength="40" ng-pattern="/^[a-zA-Z '-]+$/" required  />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseMiddleName.$error.required && frmConsuerDetail.spouseMiddleName.$dirty" > 
									Please enter spouse middle name
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseMiddleName.$error.pattern && frmConsuerDetail.spouseMiddleName.$dirty" > 
									Please enter correct spouse middle name
								</span>
							</div>
							</td>
						</tr>
						<tr ng-show = "isSpouse">
							<td class="txt-right span4"> Spouse Last Name :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.lastName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Spouse Last Name" name="spouseLastName" id="spouseLastName" ng-model="spouse.lastName" ng-change="syncMembers(spouse.memberId,false);" maxlength="40" ng-pattern="/^[a-zA-Z '-]+$/" required  />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseLastName.$error.required && frmConsuerDetail.spouseLastName.$dirty" > 
									Please enter spouse last name
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseLastName.$error.pattern && frmConsuerDetail.spouseLastName.$dirty" > 
									Please enter correct spouse last name
								</span>
							</div>
							</td>
						</tr>
						<tr ng-show = "isSpouse">
							<td class="txt-right span4"> Spouse SSN :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.ssn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text"  name="spouseSSN" id="spouseSSN" ng-model="spouse.ssn" ng-change="syncMembers(spouse.memberId,false);"  maxlength="11" required ui-mask="999-99-9999" ui-mask-placeholder ui-mask-placeholder-char="#" model-view-value="true"  />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseSSN.$error.required && frmConsuerDetail.spouseSSN.$dirty" > 
									Please enter spouse ssn
								</span>
							</div>
							</td>
						</tr>
						<tr ng-show = "isSpouse">
							<td class="txt-right span4"> Spouse Date Of Birth :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.birthDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" name="spouseDateOfBirth" id="spouseDateOfBirth" date-validation="dob" ng-model="spouse.birthDate" ng-change="syncMembers(spouse.memberId,false);" maxlength="40"  required ui-mask="99/99/9999" model-view-value="true" placeholder="MM/DD/YYYY" />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseDateOfBirth.$error.required && frmConsuerDetail.spouseDateOfBirth.$dirty" > 
									Please enter spouse date of birth
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.spouseDateOfBirth.$error.dob && !frmConsuerDetail.spouseDateOfBirth.$pristine" > 
									Please enter valid spouse date of birth
								</span>
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4" style="vertical-align: top;padding-top: 10px;"> Street Address :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.address1}} {{recipent.address2}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Recipient Address1" name="recipientaddress1" id="recipientaddress1" ng-model="recipent.address1"  maxlength="40" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$/" ng-blur="addressCheck()" required  /></br>
								<input class="filterField margin10-t" type="text" placeholder="Recipient Address2" name="recipientaddress2" id="recipientaddress2" ng-model="recipent.address2"  maxlength="40" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$/" ng-blur="addressCheck()" required  />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientaddress1.$error.required && frmConsuerDetail.recipientaddress1.$dirty" > 
									<spring:message code="indportal.communicationPreferences.addressError1" />
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientaddress1.$error.pattern && frmConsuerDetail.recipientaddress1.$dirty" > 
									<spring:message code="indportal.communicationPreferences.addressError2" />
								</span>
							</div>
							</td>
						</tr>						
						<tr>
							<td class="txt-right span4"> City :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.city}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="City" name="recipientCity" id="recipientCity" ng-model="recipent.city"  maxlength="40" ng-pattern="/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/" ng-blur="addressCheck()" required />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientCity.$error.required && frmConsuerDetail.recipientCity.$dirty"> 
									<spring:message code="indportal.communicationPreferences.cityError1" />
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientCity.$error.pattern && frmConsuerDetail.recipientCity.$dirty"> 
									<spring:message code="indportal.communicationPreferences.cityError2" />
								</span>
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> State :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.state}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="State" name="recipientState" id="recipientState" ng-model="recipent.state"  maxlength="40" ng-blur="addressCheck()"  required />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientState.$error.required && frmConsuerDetail.recipientState.$dirty"> 
									Please Enter State
								</span>
							</div>
							</td>
						</tr>						
						<tr>
							<td class="txt-right span4"> Zip :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.zip}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" name="recipientZip" id="recipientZip" ng-model="recipent.zip"  maxlength="5"  ng-blur="addressCheck()" ui-mask="99999" required />
								<span class="popover-content ng-binding zip-warning redBorder" ng-if="frmConsuerDetail.recipientZip.$error.required && frmConsuerDetail.recipientZip.$dirty"> 
								<spring:message code="indportal.communicationPreferences.zipCodeError1" />
							</span>
								
							</div>
							</td>
						</tr>						
					</tbody>
				</table>
			</div>
			<div class="header">
                 <h4 class="pull-left">Policy Information
                 </h4>                 
            </div>
            <div class="gutter10">
			
				<table class="table-border-none table">
					<tbody>
						<tr>
							<td class="txt-right span4"> Marketplace Assigned Policy Number :</td>
							<td><div><strong>{{enrollment1095Details.exchgAsignedPolicyId}} </strong></div></td>
						</tr>
						<tr>
							<td class="txt-right span4"> Policy Issuer's Name :</td>
							<td><div><strong>{{enrollment1095Details.policyIssuerName}} </strong></div></td>
						</tr>
						<tr>
							<td class="txt-right span4"> Policy Start Date :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.policyStartDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" ng-change="changeSD(this)" name="policyStartDate" id="policyStartDate" date-validation="PSDate" ng-model="enrollment1095Details.policyStartDate"  required ui-mask="99/99/9999" model-view-value="true" placeholder="MM/DD/YYYY" />
								<span class="popover-content ng-binding zip-warning redBorder" ng-show="frmConsuerDetail.policyStartDate.$error.PSDate && !frmConsuerDetail.policyStartDate.$pristine"> 
									Please enter valid policy start date
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-show="frmConsuerDetail.policyStartDate.$error.required "> 
									Please enter policy start date
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-show="frmConsuerDetail.policyStartDate.$error.sameEndStartDate"> 
									Policy start date same as end date
								</span>
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Policy End Date :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.policyEndDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" ng-change="changeED(this)" name="policyEndDate" id="policyEndDate" date-validation="PEDate" ps-dt="enrollment1095Details.policyStartDate" ng-model="enrollment1095Details.policyEndDate" required ui-mask="99/99/9999" model-view-value="true" placeholder="MM/DD/YYYY" />
								<span class="popover-content ng-binding zip-warning redBorder" ng-show="frmConsuerDetail.policyEndDate.$error.PEDate && !frmConsuerDetail.policyEndDate.$pristine"> 
									Please enter valid policy end date
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-show="frmConsuerDetail.policyEndDate.$error.required && !frmConsuerDetail.policyEndDate.$pristine"> 
									Please enter policy end date
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" ng-show="frmConsuerDetail.policyEndDate.$error.sameEndStartDate"> 
									Policy end date same as start date
								</span>
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Corrected Indicator :</td>
							<td><div><strong>{{enrollment1095Details.correctedCheckBoxIndicator}} </strong></div>
								
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Void Indicator :</td>
							<td><div><strong>{{enrollment1095Details.voidCheckboxindicator}} </strong></div>								
							</td>
						</tr>											
					</tbody>
				</table>
			</div>
			
			<div class="header">
                 <h4 class="pull-left">Covered Individuals
                 </h4>                
            </div>
            <div class="gutter10">
			
				<table class="table table-striped search-applicant-results">
					<thead>
						<tr class="graydrkbg ">
							<th >Covered Individual </th>
							<th >SSN</th>
							<th >Date Of Birth</th>	
							<th >Policy Start Date</th>
							<th >Policy End Date</th>																			
						</tr>
					</thead>
						<tr ng-repeat="member in members">
							<td><div ng-hide = "editDetailMod"><strong>{{member.firstName}}&nbsp;{{member.middleName}}&nbsp;{{member.lastName}}</strong></div>
								<div ng-show = "editDetailMod">
								<ng-form name="frm1">
								<input class="filterField" type="text" placeholder="First Name" name="memberFirstName" id="memberFirstName" ng-model="member.firstName"  ng-change="syncMembers(member.memberId,true);"  style="width: 100px;" ng-pattern="/^[a-zA-Z '-]+$/" required />&nbsp;
								<input class="filterField" type="text" placeholder="Middle Name" name="memberMiddleName" id="memberMiddleName" ng-model="member.middleName" ng-change="syncMembers(member.memberId,true);" style="width: 100px;" ng-pattern="/^[a-zA-Z '-]+$/" required />
								<input class="filterField" type="text" placeholder="Last Name" name="memberLastName" id="memberLastName" ng-model="member.lastName" ng-change="syncMembers(member.memberId,true);" style="width: 100px;" ng-pattern="/^[a-zA-Z '-]+$/" required />
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter first name" ng-if="frm1.memberFirstName.$error.required && frm1.memberFirstName.$dirty" > 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Invalid first name" ng-if="frm1.memberFirstName.$error.pattern && frm1.memberFirstName.$dirty" > 
									
								</span>
								
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter middle name" ng-if="frm1.memberMiddleName.$error.required && frm1.memberMiddleName.$dirty" > 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Invalid middle name" ng-if="frm1.memberMiddleName.$error.pattern && frm1.memberMiddleName.$dirty" > 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter last name" ng-if="frm1.memberLastName.$error.required && frm1.memberLastName.$dirty" > 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Invalid last name" ng-if="frm1.memberLastName.$error.pattern && frm1.memberLastName.$dirty" > 
									
								</span>
								</ng-form>
							</div>
							</td>							
							<td><div ng-hide = "editDetailMod"><strong>{{member.ssn}} </strong></div>
								<div ng-show = "editDetailMod">
								<ng-form name="frm2">
								<input class="filterField" type="text"  name="memberSSN" id="memberSSN" ng-model="member.ssn" ng-change="syncMembers(member.memberId,true);" style="width: 100px;"  maxlength="11" required ui-mask="999-99-9999" ui-mask-placeholder ui-mask-placeholder-char="#" model-view-value="true"  />
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter ssn" ng-if="frm2.memberSSN.$error.required && frm2.memberSSN.$dirty" > 
									
								</span>
								</ng-form>
							</div>
							</td>
							<td><div ng-hide = "editDetailMod"><strong>{{member.birthDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<ng-form name="frm3">
								<input class="filterField" type="text"  name="memberBirthDate" id="memberBirthDate" ng-model="member.birthDate" ng-change="syncMembers(member.memberId,true);" date-validation="dob"  style="width: 100px;"  required ui-mask="99/99/9999" model-view-value="true" placeholder="MM/DD/YYYY" />
								<span class="popover-content ng-binding zip-warning redBorder" title="Please enter date of birth" ng-if="frm3.memberBirthDate.$error.required && frm3.memberBirthDate.$dirty" > 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Please enter valid date of birth" ng-if="frm3.memberBirthDate.$error.dob && !frm3.memberBirthDate.$pristine" > 
									
								</span>
								</ng-form>
							</div>
							</td>
							<td><div ng-hide = "editDetailMod"><strong>{{member.coverageStartDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<ng-form name="frm4">
								<input ui-mask="99/99/9999" ng-model-options="{ updateOn: 'blur', debounce: { 'blur': 500 } }" ng-change="onMemberStartDateChange(enrollment1095Details,this,$index)" class="filterField" type="text" name="memberCoverageStartDate" id="memberCoverageStartDate"  ps-dt="enrollment1095Details.policyStartDate" pe-dt="enrollment1095Details.policyEndDate" ng-model="member.coverageStartDate"  style="width: 100px;"  required model-view-value="true" placeholder="MM/DD/YYYY" />
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter start date" ng-show="frm4.memberCoverageStartDate.$error.required && !frm4.memberCoverageStartDate.$pristine"> 
								
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter valid start date" id="validStartDate" ng-show=" frm4.memberCoverageStartDate.$error.validPolicyStartDate" > 
								</span>

								<span class="popover-content ng-binding zip-warning redBorder" title="Enter start date as per policy start date" id="validIndexedStartDate" ng-show=" frm4.memberCoverageStartDate.$error.validPolicyStartDate_{{$index}}" > 
								</span>
								</ng-form>
							</div>
							</td>							
							<td><div ng-hide = "editDetailMod"><strong>{{member.coverageEndDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<ng-form name="frm5">
								<input ui-mask="99/99/9999" class="filterField" type="text"  name="memberCoverageEndDate" id="memberCoverageEndDate"  ng-change="onMemberEndDateChange(enrollment1095Details,this,$index)" ms-dt="member.memberCoverageStartDate" ps-dt="enrollment1095Details.policyStartDate" pe-dt="enrollment1095Details.policyEndDate" ng-model="member.coverageEndDate"  style="width: 100px;"  required  model-view-value="true" placeholder="MM/DD/YYYY" />
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter end date" ng-show="frm5.memberCoverageEndDate.$error.required && !frm5.memberCoverageEndDate.$pristine"> 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" id="validEndDate" title="Enter valid end date" ng-show="frm5.memberCoverageEndDate.$error.validPolicyEndDate"> 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" id="validIndexedEndDate" title="Enter end date as per policy end date" ng-show="frm5.memberCoverageEndDate.$error.validPolicyEndDate_{{$index}}"> 
									
								</span>
								</ng-form>
							</div>
							</td>							
						</tr>						
				</table>
			</div>	
			
			<div class="header">
                 <h4 class="pull-left">Coverage Information
                 </h4>                 
            </div>
            <div class="gutter10" on-load="getTotals()">
			
				<table class="table table-striped search-applicant-results">
					<thead>
						<tr class="graydrkbg ">
							<th style="width: 80px;">Month </th>
							<th style="width: 80px;">Total </br>Gross Premium </br>(P1)</th>
							<th style="width: 30px;">EHB%</br>(P2)</th>	
							<th style="width: 100px;">Associated </br>Dental Paediatric</br>(P3)</th>
							<th style="width: 100px;"> Count of <br>paying members<br>in<br>Dental (P4)</th>
							<th style="width: 100px;">A. Monthly Premium <br/>Amount</th>
							<th style="width: 100px;">B. Monthly Premium</br>Amount of Second</br>Lowest Cost Silver</br>Plan (SLCSP)</th>
							<th style="width: 100px;">C. Monthly Advance </br>Payment of</br>Tax Credit</th>																										
						</tr>
					</thead>
						<tr ng-repeat="premium in enrollment1095Details.enrollmentPremiums">
							<td><div>{{premium.monthName}}</div></td>							
							<td><div ng-hide = "editDetailMod"><strong>{{premium.grossPremium}}</strong></div>
								<div ng-show = "editDetailMod">
								<ng-form name="frm6">
								<input class="filterField" type="text" name="grossPremium" id="grossPremium" ng-model="premium.grossPremium"  style="width: 50px;" required ng-pattern="/^\d+\.?\d{0,3}$/" ng-blur="changeTotal(premium.grossPremium,premium.ehbPercent,premium.pediatricEhbAmt,premium.accountableMemberDental,$index); getTotals()" />
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter premium" ng-if="frm6.grossPremium.$error.required && frm6.grossPremium.$dirty" > 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter premium(upto 3 decimal places)" ng-if="frm6.grossPremium.$error.pattern && frm6.grossPremium.$dirty" > 
									
								</span>
								</ng-form>
								</div>
							</td>
							<td><div>{{premium.ehbPercent}}</div></td>
							<td><div>{{premium.pediatricEhbAmt}}</div></td>
							<td>
								 <div ng-if="enrollment1095Details.coverageYear < 2017">
   										{{premium.accountableMemberDental}}
   								</div>
   								<div ng-if="enrollment1095Details.coverageYear >= 2017">
   										 N/A
   								</div>
							</td>
							<td>
								<div id="total_premium_{{$index}}">
   									<div ng-if="enrollment1095Details.coverageYear < 2017">
   										 {{(premium.grossPremium * premium.ehbPercent)+((premium.pediatricEhbAmt=='' ||premium.accountableMemberDental=='') ? 0 : (premium.pediatricEhbAmt * premium.accountableMemberDental)) | number : 2 }}
   									</div>
									<div ng-if="enrollment1095Details.coverageYear >= 2017">
   										 {{(premium.grossPremium * premium.ehbPercent)+(premium.pediatricEhbAmt) | number : 2 }}
   									</div>
								</div>
							</td>
							<td><div ng-hide = "editDetailMod">{{premium.slcspAmount}}</div>
								<div ng-show = "editDetailMod">
								<ng-form name="frm7">
								<input class="filterField" type="text"  name="slcspAmount" id="slcspAmount" ng-model="premium.slcspAmount" required  style="width: 50px;" ng-pattern="/^\d+\.?\d{0,3}$/" ng-change="getTotals()" />
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter premium(SLCSP)" ng-if="frm7.slcspAmount.$error.required && frm7.slcspAmount.$dirty" > 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter valid premium(upto 3 decimal places)" ng-if="frm7.slcspAmount.$error.pattern && frm7.slcspAmount.$dirty" > 
									
								</span>
								</ng-form>
								</div>
							</td>
							<td><div ng-hide = "editDetailMod">{{premium.aptcAmount}}</div>
								<div ng-show = "editDetailMod">
								<ng-form name="frm8">
								<input class="filterField" type="text" name="aptcAmount" id="aptcAmount" ng-model="premium.aptcAmount" required  style="width: 50px;" ng-pattern="/^\d+\.?\d{0,3}$/"  ng-change="getTotals()" />
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter monthly APTC" ng-if="frm8.aptcAmount.$error.required && frm8.aptcAmount.$dirty" > 
									
								</span>
								<span class="popover-content ng-binding zip-warning redBorder" title="Enter valid APTC(upto 3 decimal places)" ng-if="frm8.aptcAmount.$error.pattern && frm8.aptcAmount.$dirty" > 
									
								</span>
								</ng-form>
								</div>
							</td>
														
						</tr>	
						<tr>
							<td><h5 class="pull-left">Total</h5> </td>
							<td id="grossPremiumTotal"></td>
							<td></td>
							<td id="pediatricEhbAmtTotal"></td>
							<td></td>
							<td id="PremiumTotal"></td>
							<td id="slcspAmountTotal"></td>
							<td id="aptcAmountTotal"></td>
						</tr>					
				</table>
			</div>	
			
			
			<div class="header">
                 <h4 class="pull-left">Other
                 </h4>                
            </div>
            <div class="gutter10">
			
				<table class="table-border-none table">
					<tbody>
						<tr>
							<td class="txt-right span4"> Marketplace Identifier :</td>
							<td><div><strong>{{enrollment1095Details.marketPlaceIdentifier}} </strong></div>
						</tr>						
						<tr>
							<td class="txt-right span4"> Last Updated :</td>
							<td><div><strong>{{enrollment1095Details.updatedOn}} </strong></div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> PDF Generated on :</td>
							<td><div><strong>{{enrollment1095Details.pdfGeneratedOn}} </strong></div>								
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Last Sent to Consumer :</td>
							<td><div><strong>{{enrollment1095Details.pdfGeneratedOn}} </strong></div>								
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Last Sent to CMS :</td>
							<td><div><strong>{{enrollment1095Details.cmsXmlGeneratedOn}} </strong></div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> CMS XML Correction Indicator :</td>
							<td><div><strong>{{enrollment1095Details.correctionIndicatorXml}} </strong></div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Consumer PDF Correction Indicator :</td>
							<td><div><strong>{{enrollment1095Details.correctionIndicatorPdf}} </strong></div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Resend Indicator :</td>
							<td><div><strong>{{enrollment1095Details.resendPdfFlag}} </strong></div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Data Validation Status :</td>
							<td><div><strong>{{isActive}}</strong></div>
							</td>
						</tr>										
					</tbody>
				</table>
			</div>
			<div style="border-bottom: 1px solid #e7e7e7;">&nbsp; </div>
			<div></br>
                 <div class="pull-left"><a class="btn btn-small pull-right" title="Back to Search" href="javascript:void(0)" ng-click="backToSearch()">Back to Search</a>
                 </div>
                 <div ng-hide = "editDetailMod">
	                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="editDetail()">Edit</a>
	                 <span class="disabled" ng-if="enrollment1095Details.correctionIndicatorXml=='Y' || enrollment1095Details.correctionIndicatorPdf=='Y' || enrollment1095Details.resendPdfFlag=='Y' && enrollment1095Details.pdfFileName == null">
		                 <a class="btn btn-small pull-right disabled" title="Resend" href="javascript:void(0)">Resend</a>
		             </span>
		             <span ng-if="enrollment1095Details.correctionIndicatorXml!='Y' && enrollment1095Details.correctionIndicatorPdf!='Y' && enrollment1095Details.resendPdfFlag!='Y' && enrollment1095Details.pdfFileName != null">
	                 	<a class="btn btn-small pull-right" title="Resend" href="javascript:void(0)" ng-click="resendDetail()">Resend</a>
	                 </span>
                 </div>
                 <div ng-show = "editDetailMod">
                 <a class="btn btn-small pull-right" title="Cancel"  href="javascript:void(0)" ng-click="cancelEditl()">Cancel</a>
                 <a class="btn btn-small pull-right" title="Save" href="javascript:void(0)" ng-click="saveDetail()">Save</a>                 
                 </div>
            </div>			
		</div>		
	</form>	
</div>
<!-- End of Details Screen -->

<!-- Start of Dialog boxes -->
<div ng-show="modalForm.edit1095">
     <div id="ssapSubmitModal" modal-show="modalForm.edit1095" class="modal hide fade" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">
            
            <div class="modal-header" ng-if = "modalForm.subResult != 'loader'">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelCsrAction()">&times;</button>
              <h3 id="myModalLabel">
				<span>1095-A Edit</span>
			  </h3>
            </div>
		  <div ng-if="modalForm.subResult=='reason'">
            <div class="modal-body">
				<p ng-class="{fadeFont: fadeFont}">Please specify the reason for changing 1095-A infomation for this customer.</br>
					This will help keep track of updates to the customer's record.
				</p>
            </div>
            <div class="modal-body" >
                <span class="oRReason">Specify the reason for override. <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
                <br>
                <textarea class="overrideText" ng-model="modalForm.overrideComment" maxlength="{{maxLength}}" placeholder="Please explain the change you are making and why" required></textarea>
                <br>
                <div>
                  <a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">Cancel</a>
                  <button class="btn btn-primary pull-right" ng-click="saveContinue()" class="" ng-disabled = "!modalForm.overrideComment.length">Continue</button>
                  <span class="pull-left">{{maxLength - modalForm.overrideComment.length}} max character</span>
                </div>
            </div>
            </div>
            
			
            <div class="processingRequest txt-center ng-hide" ng-show="modalForm.subResult == 'loader'">
				
				<div class="modal-body">
					
					<img src="<c:url value="/resources/img/loadingGif.gif" />"
					width="100" height="100" alt="" />
					<p align ="center">Processing please wait </p>
	 			</div>
			</div>					
			<div ng-if = "modalForm.subResult == 'success'">
			  <div class="modal-body">
	    		<h3>Submission Successful!</h3>			        
	    		<p>Press OK button to close the window.</p>
	    	  </div>	
	       	  <div class="modal-footer">
	          	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()">OK</a>
	          </div>
			</div>
			<div ng-if = "modalForm.subResult == 'failure'">
			  <div class="modal-body">
	    		<h3> Failure!</h3>
	    		<p> While submitting error occured, please try later.</p>				
	    	  </div>	
	       	  <div class="modal-footer">
	          	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()">OK</a>
	          </div>
			</div>

        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
    </div>
  </div> 
<!-- End of Dialog boxes -->  

		<div id="addressValidationModal" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: block;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3><spring:message code="indportal.communicationPreferences.checkYourAddress" /></h3>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.communicationPreferences.cancel" /></button>
				<button class="btn btn-primary" data-dismiss="modal" ng-click="updateAddress()"><spring:message code="indportal.communicationPreferences.ok" /></button>
			</div>
		</div>
		
		<div id="noAddressModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3><spring:message code="indportal.communicationPreferences.addressNotFound" /></h3>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.communicationPreferences.close" /></button>
			</div>
		</div>

		<div id="validationModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Form Validation Failed</h3>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.communicationPreferences.close" /></button>
			</div>
		</div>
	
</div>




<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>

	
	<!-- CSR Modal-->
<script src="/hix/resources/angular/angular-file-upload-shim.min.js"></script>
<script src="/hix/resources/angular/angular-file-upload.min.js"></script>
<script src="/hix/resources/angular/mask.js"></script>
<script src="/hix/resources/angular/ui-bootstrap-tpls-0.9.0.min.js"></script>
<script src="<c:url value="/resources/js/moment.min.js" />"></script> 
<script type="text/javascript" src="/hix/resources/js/enrollment1095/enrollment1095.js"></script>
<script src="/hix/resources/js/angular-sanitize.min.js"></script>
<script>var baseURL = "${pageContext.request.contextPath}";</script>
<script type="text/javascript" src="/hix/resources/js/angular-route.js"></script>

