<script>
  $(document).ready(function() {
	  $('.remove').on('click', function() {
		 $(this).closest('tr').fadeOut(300);
	  })
  })
</script>

<script type="text/ng-template" id="numofdep-remove.jsp">
  <div class="gutter10">
    <div class="gutter10">
      <div class="gutter10">
        <table role="presentation" id="" class="table table-striped"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
          <tbody>
            <tr>
              <td><spring:message code="label.lce.firstname"/></td>
              <td><spring:message code="label.lce.lastname"/></td>
              <td><spring:message code="label.lce.relationship"/></td>
              <td><spring:message code="label.lce.seekingcoverage"/>?</td>
              <td><spring:message code="label.lce.edit"/></td>
            </tr>
            <tr ng-repeat="householdMember  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
              <td>{{householdMember.name.firstName}}</td>
            	<td>{{householdMember.name.lastName}}</td>
              <td>
            		<div ng-repeat="relationship in householdMember.bloodRelationtship">
  					      {{getRelation(relationship.relationshipCode)}}
  				      </div>
              </td>
              <td>
  						  <label class="radio inline" for="applyingforhealthcare">
  						    <input id="seekingCoverageTrue{{$index}}" name="seekingCoverage{{$index}}" type="radio" value="true" ng-value="true" ng-model="jsonObject.singleStreamlinedApplication.taxHousehold[$parent.$index].householdMember[$index].applyingForCoverageIndicator" />
  						    <label for="seekingCoverageTrue{{$index}}"><spring:message code="label.lce.yes"/></label>
  						  </label>
  						  <label class="radio inline" for="applyingforhealthcare">
  						    <input id="seekingCoverageFalse{{$index}}" name="seekingCoverage{{$index}}" type="radio" value="false" ng-value="false" ng-model="jsonObject.singleStreamlinedApplication.taxHousehold[$parent.$index].householdMember[$index].applyingForCoverageIndicator" />
  						    <label for="seekingCoverageFalse{{$index}}"><spring:message code="label.lce.no"/></label>
  						  </label>
  						</td>
  	          <td>
  	           <button class="btn-primary small" ng-click="addChangedApplicants(householdMember.personId,currentEvent.dateOfChange);activeDeathStatusChange(householdMember); householdMember.active=false"><spring:message code="label.lce.remove"/></button>
  	          </td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>