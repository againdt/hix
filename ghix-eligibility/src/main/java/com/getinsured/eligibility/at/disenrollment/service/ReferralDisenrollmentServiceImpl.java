package com.getinsured.eligibility.at.disenrollment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantNonQHPCodeSimpleType;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

@Service("referralDisenrollmentService")
public class ReferralDisenrollmentServiceImpl implements ReferralDisenrollmentService {

	private static final String BATCH = "Batch - ";

	private static final String DISENROLL_FAILURE = "Disenrollment Failure -";
	private static final String DISENROLL_SUCCESS= "Disenrollment Success -";
	private static final String REQUEST = "Request -";
	private static final String RESPONSE= "Response -";
	
	private static final Logger LOGGER = Logger.getLogger(ReferralDisenrollmentServiceImpl.class);

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private IntegrationLogService integrationLogService;

	private final static String DISENROLLMENT = "DISENROLLMENT";

	@Override
	public String invokeDisenrollmentAPI(Map<String, ApplicantEvent> applicantEventMap, SsapApplication enrolledApplication) {
		EnrollmentResponse enrollmentResponse;
		String responseValue = GhixConstants.RESPONSE_FAILURE;
		String enrollmentRequestXml = "";
		String enrollmentResponseXmlString = "";
		EnrollmentRequest enrollmentRequest = null;
		String message = "";
		try {
			XStream xStream = GhixUtils.getXStreamStaxObject();
			enrollmentRequest = createDisenrollmentRequest(enrolledApplication, applicantEventMap);
			LOGGER.info("Invoking  ind disenrollement Service for " + enrolledApplication.getId());
			enrollmentRequestXml = xStream.toXML(enrollmentRequest);
			enrollmentResponseXmlString = (ghixRestTemplate.exchange(EnrollmentEndPoints.DISENROLL_BY_APPLICATION_ID_URL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, enrollmentRequestXml)).getBody();

			if (StringUtils.isEmpty(enrollmentResponseXmlString)) {
				message = "Unable to disenroll. Response is - " + enrollmentResponseXmlString + " - for request - " + enrollmentRequestXml;
				return responseValue;
			}
			enrollmentResponse = (EnrollmentResponse) xStream.fromXML(enrollmentResponseXmlString);
			if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse.getStatus()) && enrollmentResponse.getErrCode() == 200) {
				responseValue = GhixConstants.RESPONSE_SUCCESS;
				message = "Disenrollment Success. Response  - " + enrollmentResponseXmlString + " - for request - " + enrollmentRequestXml;
			} else {
				message = "Unable to disenroll. Response - " + enrollmentResponseXmlString + " - for request - " + enrollmentRequestXml;
			}
		} catch (Exception e) {
			message = "Exception occurred while disenroll. Response - " + enrollmentResponseXmlString + " - for request - " + enrollmentRequestXml + " - " + ExceptionUtils.getFullStackTrace(e);

		} finally {
			logData(enrolledApplication.getId(), message, responseValue);
		}

		return responseValue;
	}
	
	
	@Override
	public String invokeDisenrollmentAPI(SsapApplication enrolledApplication, Date terminationDate) {
		EnrollmentResponse enrollmentResponse;
		String responseValue = GhixConstants.RESPONSE_FAILURE;
		String enrollmentRequestXml = "";
		String enrollmentResponseXmlString = "";
		EnrollmentRequest enrollmentRequest = null;
		String disenrollFailure = DISENROLL_FAILURE;
		String disenrollSuccess=DISENROLL_SUCCESS;
		String request = REQUEST;
		String response= RESPONSE;
		String batch = BATCH;
		StringBuilder message=  new StringBuilder().append(StringUtils.EMPTY);
		try {
			XStream xStream = GhixUtils.getXStreamStaxObject();
			enrollmentRequest = createDisenrollmentRequest(enrolledApplication, terminationDate);
			LOGGER.info("Invoking  ind disenrollement Service for " + enrolledApplication.getId());
			enrollmentRequestXml = xStream.toXML(enrollmentRequest);
			enrollmentResponseXmlString = (ghixRestTemplate.exchange(EnrollmentEndPoints.DISENROLL_BY_APPLICATION_ID_URL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, enrollmentRequestXml)).getBody();

			if (StringUtils.isEmpty(enrollmentResponseXmlString)) {
				 message.append(batch).append(disenrollFailure).append(response).append(enrollmentResponseXmlString).append(request).append(enrollmentRequestXml);
				return message.toString();
			}
			enrollmentResponse = (EnrollmentResponse) xStream.fromXML(enrollmentResponseXmlString);
			if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse.getStatus()) && enrollmentResponse.getErrCode() == 200) {
				responseValue = GhixConstants.RESPONSE_SUCCESS;
				message.append(batch).append(disenrollSuccess).append(response).append(enrollmentResponseXmlString).append(request).append(enrollmentRequestXml);
			} else {
				message.append(batch).append(disenrollFailure).append(response).append(enrollmentResponseXmlString).append(request).append(enrollmentRequestXml);
			}
		} catch (Exception e) {
			message.append(batch).append(disenrollFailure).append(response).append(enrollmentResponseXmlString).append(request).append(enrollmentRequestXml).append(ExceptionUtils.getFullStackTrace(e));

		} finally {
			logData(enrolledApplication.getId(), message.toString(), responseValue);
		}

		return responseValue;
	}
	
	
	@Override
	public String disenrollHealthOrDentalOnly(String[] arguments) {

		EnrollmentRequest enrollmentRequest = null;
		EnrollmentResponse enrollmentResponse;
		String disenrollFailure = DISENROLL_FAILURE;
		String disenrollSuccess = DISENROLL_SUCCESS;
		String request = REQUEST;
		String response = RESPONSE;
		String enrollmentRequestXml = "";
		String enrollmentResponseXmlString = "";
		String responseValue = GhixConstants.RESPONSE_FAILURE;
		StringBuilder message = new StringBuilder();

		long enrolledAppId = 0;
		long enrollmentId = 0;
		String terminationDateStr = StringUtils.EMPTY;
		String terminationReasonCode = StringUtils.EMPTY;

		try {
			enrolledAppId = Long.valueOf(arguments[0]);
			enrollmentId = Long.valueOf(arguments[1]);
			terminationDateStr = arguments[2];
			terminationReasonCode = arguments[3];
			boolean healthOnly = Boolean.valueOf(arguments[4]);

			if (enrolledAppId == 0 || enrollmentId == 0 || StringUtils.isEmpty(terminationDateStr) || StringUtils.isEmpty(terminationReasonCode)) {
				throw new GIRuntimeException("Enrolled application id or Enrollment Id or termination date or code is not correct" + enrolledAppId + " " + enrollmentId);
			}
	
			if(!checkTerminationDateIsValid(terminationDateStr)){
				throw new GIRuntimeException("Termination Date is not in MM/dd/yyyy format for " +enrolledAppId );
			}
			if (healthOnly) {
				message.append(ReferralConstants.HEALTH);
			} else {
				message.append(ReferralConstants.DENTAL);
			}

			XStream xStream = GhixUtils.getXStreamStaxObject();
			enrollmentRequest = createDisenrollmentRequest(enrollmentId, terminationDateStr, terminationReasonCode);
			LOGGER.info("Invoking  ind disenrollement Service for " + enrolledAppId);
			enrollmentRequestXml = xStream.toXML(enrollmentRequest);
			enrollmentResponseXmlString = (ghixRestTemplate.exchange(EnrollmentEndPoints.DISENROLL_BY_ENROLLMENT_URL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, enrollmentRequestXml)).getBody();

			if (StringUtils.isEmpty(enrollmentResponseXmlString)) {
				message.append(disenrollFailure).append(response).append(enrollmentResponseXmlString).append(request).append(enrollmentRequestXml);
				return message.toString();
			}
			enrollmentResponse = (EnrollmentResponse) xStream.fromXML(enrollmentResponseXmlString);
			if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse.getStatus()) && enrollmentResponse.getErrCode() == 200) {
				responseValue = GhixConstants.RESPONSE_SUCCESS;
				message.append(disenrollSuccess).append(response).append(enrollmentResponseXmlString).append(request).append(enrollmentRequestXml);
			} else {
				message.append(disenrollFailure).append(response).append(enrollmentResponseXmlString).append(request).append(enrollmentRequestXml);
			}
		} catch (Exception e) {
			message.append(disenrollFailure).append(response).append(enrollmentResponseXmlString).append(request).append(enrollmentRequestXml).append(ExceptionUtils.getFullStackTrace(e));

		} finally {
			logData(enrolledAppId, message.toString(), responseValue);
		}

		return responseValue;

	}
	
	
	
	private boolean checkTerminationDateIsValid(String terminationDateStr) {

		Date convertedDate = DateUtil.StringToDate(terminationDateStr, ReferralConstants.DEFDATEPATTERN);
		if (convertedDate != null) {
			return true;
		}
		return false;
	}


	private EnrollmentRequest createDisenrollmentRequest(long enrollmentId, String terminationDateStr, String terminationReasonCode) {
	   
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		List<Integer> idList = new ArrayList<Integer>();
		Map<String, Object> disEnroll = new HashMap<String, Object>();
		idList.add(Integer.parseInt(String.valueOf(enrollmentId)));
		disEnroll.put("TerminationDate", terminationDateStr);
		disEnroll.put("TerminationReasonCode", terminationReasonCode);
		enrollmentRequest.setIdList(idList);
		enrollmentRequest.setMapValue(disEnroll);
	    return enrollmentRequest;
    }


	private EnrollmentRequest createDisenrollmentRequest(SsapApplication enrolledApplication, Date terminationDate) {
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentDisEnrollmentDTO disEnrollDTO = new EnrollmentDisEnrollmentDTO();
		disEnrollDTO.setSsapApplicationid(enrolledApplication.getId());
		disEnrollDTO.setTerminationDate(ReferralUtil.formatDate(terminationDate, ReferralConstants.DEFDATEPATTERN));
		disEnrollDTO.setTerminationReasonCode(ReferralConstants.OTHER_DISENROLLMENT_CODE);
		enrollmentRequest.setEnrollmentDisEnrollmentDTO(disEnrollDTO);
		return enrollmentRequest;
	}
	
	

	private EnrollmentRequest createDisenrollmentRequest(SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap) {
		ApplicantEvent applicantEvent = ApplicationExtensionEventUtil.calculateMaxEventDateApplicantEvent(applicantEventMap);
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentDisEnrollmentDTO disEnrollDTO = new EnrollmentDisEnrollmentDTO();
		disEnrollDTO.setSsapApplicationid(enrolledApplication.getId());
		disEnrollDTO.setTerminationDate(calculateTerminationDate(applicantEventMap, applicantEvent,enrolledApplication.getCoverageYear()));
		disEnrollDTO.setTerminationReasonCode(ReferralConstants.OTHER_DISENROLLMENT_CODE);
		if (checkDeathEventOnly(applicantEventMap)) {
			disEnrollDTO.setTerminationReasonCode(ReferralConstants.DEATH_DISENROLLMENT_CODE);
			disEnrollDTO.setDeathDate(ReferralUtil.formatDate(applicantEvent.getEventDate(), ReferralConstants.DEFDATEPATTERN));
		}
		enrollmentRequest.setEnrollmentDisEnrollmentDTO(disEnrollDTO);
		return enrollmentRequest;
	}

	/**
	 * 
	 * Set date as month end when there is mixed event set max event date when all the applicants has death.
	 */
	private String calculateTerminationDate(Map<String, ApplicantEvent> applicantEventMap, ApplicantEvent applicantEvent, long coverageYear) {

		String terminationDate = "";
		Date eventDate = applicantEvent.getEventDate();
		DateTime date = null;
		if (!checkDeathEventOnly(applicantEventMap)) {
			date = TSDateTime.getInstance();
			eventDate = date.dayOfMonth().withMaximumValue().toDate();
		}
		
		/*This is to send coverage start date as term date if coverage is not yet started*/
		String terminateYear = ReferralUtil.formatDate(eventDate, "yyyy");
		long terminateNumYear = Long.parseLong(terminateYear);
		if(terminateNumYear<coverageYear) {
			 date = new DateTime((int)coverageYear, 1, 1, 0, 0, 0, 0);
			 eventDate = date.toDate();
		}
		
		terminationDate = ReferralUtil.formatDate(eventDate, ReferralConstants.DEFDATEPATTERN);
		
		return terminationDate;
	}

	private boolean checkDeathEventOnly(Map<String, ApplicantEvent> applicantEventMap) {
		Set<String> eventNames = new HashSet<String>();
		boolean deathEventOnly = false;

		for (ApplicantEvent appEvent : applicantEventMap.values()) {
			eventNames.add(appEvent.getCode());
		}

		if (ReferralUtil.collectionSize(eventNames) == ReferralConstants.ONE && eventNames.contains(ExtendedApplicantNonQHPCodeSimpleType.DEATH.value())) {
			deathEventOnly = true;
		}
		return deathEventOnly;
	}

	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, DISENROLLMENT, status, ssapApplicationId, null);
	}

}
