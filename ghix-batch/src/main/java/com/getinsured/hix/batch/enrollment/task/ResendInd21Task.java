package com.getinsured.hix.batch.enrollment.task;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.ResendInd21Service;

public class ResendInd21Task implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResendInd21Task.class);
	private ResendInd21Service resendInd21Service;

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1)throws Exception {
		LOGGER.info("ResendInd21Task.execute : START");
		Map<Integer, String> errorMap=null;
		try {
			long jobExecutionId=-1;
			jobExecutionId=arg1.getStepContext().getStepExecution().getJobExecutionId();
			errorMap = getResendInd21Service().sendCarrierUpdatedDataToAHBX(jobExecutionId);
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw e;
		}finally{
			if(errorMap != null && !errorMap.isEmpty()){
				getResendInd21Service().updateEnrolleeUpdateSendStatusForFailure(errorMap);
			}
		}
		return RepeatStatus.FINISHED;
	}

	public ResendInd21Service getResendInd21Service() {
		return resendInd21Service;
	}

	public void setResendInd21Service(ResendInd21Service resendInd21Service) {
		this.resendInd21Service = resendInd21Service;
	}
}
