<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>

    <%
        String householdCaseID =  (String) request.getSession().getAttribute("externalHouseholdCaseId");
    %>
<c:set var="householdCaseID" value="<%=householdCaseID%>" />
<c:set var="calheersEnv" value='<%=GhixConstants.CALHEERS_ENV%>'></c:set>
<div class="span3" id="sidebar">
	<input type="hidden" id="householdCaseID" name="householdCaseID" value="<%=householdCaseID%>">
	<div class="dropdown left-navigation-dropdown hidden-lg hidden-md hidden-sm">
		<i class="dropdown-toggle icon-bars" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-hidden="true" id="leftNavigationMenu"></i>
		<span class="welcome-msg"><spring:message code="indportal.portalhome.welcome" javaScriptEscape="true"/>, ${firstName}&nbsp;${lastName}</h1></span>
		<ul class="nav nav-list dropdown-menu left-navigation-menu" aria-labelledby="leftNavigationMenu">
			<sec:accesscontrollist hasPermission="IND_PORTAL_VIEW_AND_PRINT" domainObject="user">
			<li class="dropdown-item">
				<a id="go_to_dashboard" href="javascript:void(0)" ng-click="goDash()">
					<i class="icon-dashboard"></i>
					<spring:message code="indportal.portalhome.dashboard" javaScriptEscape="true"/>
				</a>
			</li>
			</sec:accesscontrollist>

			<sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
			<li class="dropdown-item">
				<a id="my_applications" href="#applications">
					<i class="icon-info-sign"></i>
					<spring:message code="indportal.portalhome.myapplications" javaScriptEscape="true"/>
				</a>
			</li>
		</sec:accesscontrollist>

			<li class="dropdown-item">
				<a id="my_enrollments" href="#enrollmenthistory">
					<i class="icon-check-sign"></i>
					<spring:message code="indportal.portalhome.myenrollments" javaScriptEscape="true"/>
				</a>
			</li>
			<!-- <li class="dropdown-item">
				<a id="pop_findLocalAssistance" href="#">
					<i class="icon-search"></i>
					<spring:message code="indportal.portalhome.findlocalassistance" javaScriptEscape="true"/>
				</a>
			</li> -->
			<li class="dropdown-item">
				<a id="appeal_info" href="https://www.coveredca.com/members/file-an-appeal-or-complaint/" target="_blank">
					<i class="icon-file-text-alt"></i>
					<spring:message code="indportal.portalhome.appealInformation" javaScriptEscape="true"/>
				</a>
			</li>

			<li class="dropdown-item">
				<a id="account_info" href="<c:url value='https://${calheersEnv}/static/lw-web/account-home?householdCaseId=${householdCaseID}'/>">
					<i class="icon-home"></i>
					<spring:message code="indportal.portalhome.myAccountInfo" javaScriptEscape="true"/>
				</a>
			</li>
		</ul>
	</div>



	<ul class="nav nav-list hidden-xs">
		<sec:accesscontrollist hasPermission="IND_PORTAL_VIEW_AND_PRINT" domainObject="user">
            <li>
				<a id="go_to_dashboard" href="javascript:void(0)" ng-click="goDash()">
					<i class="icon-dashboard"></i>
					<spring:message code="indportal.portalhome.dashboard" javaScriptEscape="true"/>
				</a>
			</li>
		</sec:accesscontrollist>
		<sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
			<li>
				<a id="my_applications" href="#applications">
					<i class="icon-info-sign"></i>
					<spring:message code="indportal.portalhome.myapplications" javaScriptEscape="true"/>
				</a>
			</li>
        </sec:accesscontrollist>
		<li>
			<a id="my_enrollments" href="#enrollmenthistory">
				<i class="icon-check-sign"></i>
				<spring:message code="indportal.portalhome.myenrollments" javaScriptEscape="true"/>
			</a>
		</li>
		<!-- <li>
			<a id="pop_findLocalAssistance" href="#">
				<i class="icon-search"></i>
				<spring:message code="indportal.portalhome.findlocalassistance" javaScriptEscape="true"/>
			</a>
		</li> -->
        <li>
			<a id="appeal_info" href="https://www.coveredca.com/members/file-an-appeal-or-complaint/" target="_blank">
				<i class="icon-file-text-alt"></i>
				<spring:message code="indportal.portalhome.appealInformation" javaScriptEscape="true"/>
			</a>
		</li>
        <li>
			<a id="account_info" href="<c:url value='https://${calheersEnv}/static/lw-web/account-home?householdCaseId=${householdCaseID}'/>">
				<i class="icon-home"></i>
				<spring:message code="indportal.portalhome.myAccountInfo" javaScriptEscape="true"/>
			</a>
		</li>
	</ul>
</div>
