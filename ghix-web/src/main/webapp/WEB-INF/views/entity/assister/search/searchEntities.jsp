<%@ page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/assister.css" />" media="screen"/>
<link href="<c:url value="/resources/css/chosen.css" />" rel="stylesheet" type="text/css" media="screen,print">
<%-- <link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />  --%>
<link href="<c:url value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />
<script src="/hix/resources/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="/hix/resources/js/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<c:if test='${stateCode == "CA"}'>
	<link href="<c:url value="/resources/css/broker-search.css" />" media="screen" rel="stylesheet" type="text/css" />
</c:if>
<style>
	body {
		background:#fff !important;
		padding-top: 0;
	}
	div.span6.well,div.span5.well {
		min-height: 290px;
	}
</style>
<c:choose>
	<c:when test='${stateCode == "CA"}'>
		<c:set value="col-xs-hidden col-sm-5 col-md-5 col-lg-5" var="cssClass"></c:set>
	</c:when>
	<c:otherwise>
		<c:set value="span5" var="cssClass"></c:set>
	</c:otherwise>
</c:choose>

<div class="container-fluid" id="search_page">
	<div class="row">
		<div class="search_title_big titlebar">
			<h1><spring:message code="label.assister.header"/>&nbsp;<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%></h1>
		</div>
	</div>
	<div class="row-fluid"  id="search_container">
		<div class="${cssClass} well" id="location_search">
			<form class="form-vertical" id="frmassistersearch" name="frmassistersearch" action="entitylist">
				<h4 class="search_title_small"><spring:message code="label.assister.searchbylocation"/></h4>
				<div class="gutter10">
					<div class="control-group">
						<label class="control-label" for="zipCode"><spring:message code="label.assister.zipcode"/>
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label>
						<div class="controls">
							<input type="text" name="zipCode" id="zipCode" value="${zipCode}" maxlength="5" onkeypress="return enterSubmit(event)">
						</div>
						<div id="zipCode_error"></div>
					</div>

					<div class="control-group">
						<c:if test='${stateCode == "CA"}'>
							<label for="Distancer" class="control-label"><spring:message code="label.brkdistanceinmiles"/></label>
						</c:if>
						<c:if test='${stateCode != "CA"}'>
							<label for="Distancer" class="control-label"><spring:message code="label.brkdistance"/> <span class="aria-hidden">miles</span></label>
						</c:if>
						<div class="controls">
							<select id="Distancer" name="distance">
								<option value="5"
									<c:if test="${distance == '5'}"> selected="selected"</c:if>>5</option>
								<option value="10"
									<c:if test="${distance == '10'}"> selected="selected"</c:if>>10</option>
								<option value="15"
									<c:if test="${distance == '15'}"> selected="selected"</c:if>>15</option>
								<option value="20"
									<c:if test="${distance == '20'}"> selected="selected"</c:if>>20</option>
								<option value="30"
									<c:if test="${distance == '30'}"> selected="selected"</c:if>>30</option>
								<option value="50"
									<c:if test="${distance == '50'}"> selected="selected"</c:if>>50</option>
								<option value="4000"
									<c:if test="${distance == '4000'}"> selected="selected"</c:if>><spring:message code="label.assister.assAny"/></option>
								<%-- <option value="30"
									<c:if test="${sessionScope.brokermaxdis == '30'}"> selected="selected"</c:if>>30</option>
								<option value="more"
									<c:if test="${sessionScope.brokermaxdis == 'more'}"> selected="selected"</c:if>>50+</option> --%>
							</select>
							<c:if test='${stateCode != "CA"}'>
								<span class="distance-unit"><spring:message code="label.miles"/></span>
							</c:if>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label" for="languageNames"><spring:message code="label.assister.languages"/></label>
						<div class="controls">
							<select id="languageNames" data-placeholder="<spring:message code="label.SelectAnOption"/>" data-no_results_text="<spring:message code="label.noResultFound"/>" name="languageNames"  class="chosen-select" multiple  tabindex="4" htmlEscape="false"></select>
							<input id="language" name="language"  type="hidden"/>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<input type="button" name="submit1" id="submit1" value="<spring:message  code='label.search'/>" class="btn btn-primary" />
						</div>
					</div>
				</div>
			</form>
		</div> <!-- end of location -->


		<c:choose>
			<c:when test='${stateCode == "CA"}'>
				<div class="hr-line"></div>
				<div class="col-xs-hidden col-sm-2 col-md-2 col-lg-2 center" id="search_center"><h6 class="col-xs-hidden"> <strong><spring:message code="label.assister.or"/></strong></h6> </div>
			</c:when>
			<c:otherwise>
				<div class="col-xs-hidden span1 center" id="search_center"><h6> <strong><spring:message code="label.assister.or"/></strong></h6> </div>
			</c:otherwise>
		</c:choose>

		<div class="${cssClass} well" id="name_search">
			<form class="form-vertical" id="frmassistersearchvalues" name="frmassistersearchvalues" action="entitylist">
				<h4 class="search_title_small"><spring:message code="label.assister.searchbyname"/></h4>
				<div class="gutter10">
					<div class="control-group">
						<label class="control-label" for="entityName"><spring:message code="label.assister.organizationname"/></label>
						<div class="controls">
							<input type="text" name="entityName"	id="entityName" value="${entityName}">
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<input type="submit" name="submit2" id="submit2" value="<spring:message  code='label.search'/>" class="btn btn-primary" />
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
<c:if test='${stateCode == "CA"}'>
	<div class="accordion" id="search_agent">
		<div class="accordion-group">
		    <div class="accordion-heading">
		      <a class="accordion-toggle" id="aid-searchByLocation" data-toggle="collapse" data-parent="#search_agent" href="#collapseOne">
		        <h4 class="search_title_small col-xs-10"><spring:message code="label.assister.searchbylocation"/></h4>
		        <i class="icon-chevron-right col-xs-2 pull-right"></i>
		      </a>
		    </div>
		    <div id="collapseOne" class="accordion-body collapse">
		      	<div class="accordion-inner">
		        	<form class="form-vertical" id="frmAssisterSearchAccordion" name="frmAssisterSearchAccordion" action="entitylist">
						<h4 class="search_title_small"><spring:message code="label.assister.searchbylocation"/></h4>
						<div class="gutter10">
							<div class="control-group">
								<label for="zipCodeAccordion" class="control-label"><spring:message code="label.assister.zipcode"/>
									<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
								</label>
								<div class="controls">
									<input type="text" id="zipCodeAccordion" name="zipCode" value="${zipCode}" maxlength="5">
								</div>
								<div id="zipCodeAccordion_error"></div>
							</div>

					<div class="control-group">
						<c:if test='${stateCode == "CA"}'>
							<label for="Distancer" class="control-label"><spring:message code="label.brkdistanceinmiles"/></label>
						</c:if>
						<c:if test='${stateCode != "CA"}'>
							<label for="Distancer" class="control-label"><spring:message code="label.brkdistance"/> <span class="aria-hidden">miles</span></label>
						</c:if>
						<div class="controls">
							<select id="Distancer" name="distance">
								<option value="5"
									<c:if test="${distance == '5'}"> selected="selected"</c:if>>5</option>
								<option value="10"
									<c:if test="${distance == '10'}"> selected="selected"</c:if>>10</option>
								<option value="15"
									<c:if test="${distance == '15'}"> selected="selected"</c:if>>15</option>
								<option value="20"
									<c:if test="${distance == '20'}"> selected="selected"</c:if>>20</option>
								<option value="30"
									<c:if test="${distance == '30'}"> selected="selected"</c:if>>30</option>
								<option value="50"
									<c:if test="${distance == '50'}"> selected="selected"</c:if>>50</option>
								<option value="4000"
									<c:if test="${distance == '4000'}"> selected="selected"</c:if>><spring:message code="label.assister.assAny"/></option>
								<%-- <option value="30"
									<c:if test="${sessionScope.brokermaxdis == '30'}"> selected="selected"</c:if>>30</option>
								<option value="more"
									<c:if test="${sessionScope.brokermaxdis == 'more'}"> selected="selected"</c:if>>50+</option> --%>
							</select>
							<c:if test='${stateCode != "CA"}'>
								<span class="distance-unit"><spring:message code="label.miles"/></span>
							</c:if>
						</div>
					</div>


					<div class="control-group">
						<label class="control-label" for="languageNamesAccordion"><spring:message code="label.assister.languages"/></label>
						<div class="controls">
							<select id="languageNamesAccordion" data-placeholder="<spring:message code="label.SelectAnOption"/>" data-no_results_text="<spring:message code="label.noResultFound"/>" name="languageNames"  class="chosen-select" multiple  tabindex="4" htmlEscape="false"></select>
							<input id="language" name="language"  type="hidden"/>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<input type="button" name="submit3" id="submit3" value="<spring:message  code='label.search'/>" class="btn btn-primary" />
						</div>
					</div>
				</div>
			</form>
		      	</div>
		    </div>
		</div><!-- end of accordion-group -->
	    <div class="accordion-group">
	    	<div class="accordion-heading">
	      		<a class="accordion-toggle" id="aid-searchByName" data-toggle="collapse" data-parent="#search_agent" href="#collapseTwo">
	        		<h4 class="search_title_small col-xs-10"><spring:message code="label.assister.searchbyname"/></h4>
	        		<i class="icon-chevron-right col-xs-2 pull-right"></i>
	      		</a>
    		</div>
	    	<div id="collapseTwo" class="accordion-body collapse">
	      		<div class="accordion-inner">
	        		<form class="form-vertical" id="frmassistersearchvalues" name="frmassistersearchvalues" action="entitylist">
						<div class="gutter10">
							<div class="control-group">
								<label class="control-label" for="entityNameAccordion"><spring:message code="label.assister.organizationname"/></label>
								<div class="controls">
									<input type="text" name="entityName" id="entityNameAccordion" value="${entityName}">
								</div>
							</div>
							<div class="control-group">
								<div class="controls">
									<input type="submit" name="submit2" id="submit2" value="<spring:message  code='label.search'/>" class="btn btn-primary" />
								</div>
							</div>
						</div>
					</form>
	      		</div>
	    	</div>
	 	</div>
	</div>
</c:if>
</div>

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->

<script type="text/javascript">
	function enterSubmit(e) {
	    if (e.keyCode == 13 || e.which == 13) {
	    	$("#entityName").val("");
			 $("#language").val($("#languageNames").val());
			 $("#frmassistersearch").submit();
	        return false;
	    }
	}

	var isMobile = false; //initiate as false
// device detection
	if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {isMobile = true;
	}
	if (isMobile) {
		$('#frmAssisterSearchAccordion').keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if ( (code==13) || (code==10))
            {
            	$("#frmAssisterSearchAccordion").submit();
            }
    	});
	}

	$('#submit1').click(function() {
		 $("#entityName").val("");
		 $("#language").val($("#languageNames").val());
		 $("#frmassistersearch").submit();

	});
	$('#submit3').click(function() {
		 $("#entityNameAccordion").val("");
		 $("#languageNamesAccordion").val($("#languageNames").val());
		 $("#frmAssisterSearchAccordion").submit();

	});

	$('#submit2').click(function() {
	 	$("#zipCode").val("");
		$("#languageNames").val("");
		$("#zipCode").rules("remove");
		$("#frmassistersearch").valid();
	});


    $('#search_agent .accordion-heading').on('touchstart click', function (e) {
      var $this = $(this);
      setTimeout(function(){
		if($('.accordion-toggle').hasClass('collapsed')) {
  			$this.find("i").removeClass("icon-chevron-down").addClass("icon-chevron-right");
     	 }
     	else {
			$this.find("i").removeClass("icon-chevron-right").addClass("icon-chevron-down");
     	}
      }, 0);
    });

function validateForm() {
	return {
		rules : {
			zipCode : {
				required : true,
	    	    number : true,
	        	minlength : 5,
	        	maxlength : 5,
	        	ZipCodeCheck : true
	        }
	  	},
		messages : {
			zipCode : {
			   required : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcode.aee' javaScriptEscape='true'/></span>",
			   number : "<span> <em class='excl'>!</em><spring:message  code='label.validateIsZipcodeNumber.aee' javaScriptEscape='true'/></span>",
	  		   minlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength.aee' javaScriptEscape='true'/></span>",
	  		   maxlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength.aee' javaScriptEscape='true'/></span>",
			   ZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateZipSyntax' javaScriptEscape='true'/></span>"
			}
		},
		submitHandler : function(form) {
		 	if($(form).valid()){
			 	form.submit();
		 	}
    	},
		errorClass: "error",
		onkeyup: false,
		errorPlacement : function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		}
	};
}


var frmAssisterSearchValidator = $("#frmassistersearch").validate(validateForm());
var frmAssisterSearchAccordionValidator = $("#frmAssisterSearchAccordion").validate(validateForm());

function zipcodeCheckMtd(value, element, param) {
	 var test=/^[0-9]*$/.test(value);
	 	if((value == '00000')||(!test)){
	  		return false;
	 	}
	 	if((value.length == 0)) {
			return true;
		} else if(isNaN(value)) {
			return false;
		} else {
			return true;
		}
		return true;
}

jQuery.validator.addMethod("ZipCodeCheck", zipcodeCheckMtd);


 //load the jquery chosen plugin
	function loadLanguages() {
		$("#languageNames").html('');
		$("#languageNamesAccordion").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${language}';

	    for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkCounties(respData[key]);
		      }
		      if(isSelected){
		    	  $('#languageNames').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		    	  $('#languageNamesAccordion').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#languageNames').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		    	  $('#languageNamesAccordion').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");

		      }
		 }

	     $('#languageNames').trigger("liszt:updated");
	     $('#languageNamesAccordion').trigger("liszt:updated");
	   }

	function checkCounties(county){
	     var counties='${language}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;

			     }
			 }
		   return found;
    }

	$(document).ready(function() {
		loadLanguages();
		$(".chosen-select").chosen();
	});


</script>
