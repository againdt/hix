
import { switchDirectionsActionTypes } from '../constants/actionTypes';

export const initialState = 'exact';

export default (state = initialState, action) => {
    switch(action.type) {
    case switchDirectionsActionTypes.CHANGE_DIRECTION:

        if(typeof action.direction !== 'string') {
            throw new Error('Not a string was provided as direction into directionReducer');
        }

        return action.direction;
    case switchDirectionsActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};