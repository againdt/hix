package com.getinsured.hix.indportal.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.getinsured.eligibility.startdates.dto.EligibilityDatesResponseDto.Status;

import lombok.Data;
import lombok.Getter;

@Data
public class IndPortalEligibilityDateDetails implements Serializable{

	@Getter
	private static final long serialVersionUID = 1L;

	@NotNull
	private String enrollmentStartDate;
	@NotNull
	private String enrollmentEndDate;
	@NotNull
	private String financialEffectiveDate;
	@NotNull
	private String cmrHouseholdId;
	@NotNull
	private String enrolledAppId;
	@NotNull
	private String sepAppId;
	@NotNull
	private Status status;
	@NotNull
	private String comments;
	
}
