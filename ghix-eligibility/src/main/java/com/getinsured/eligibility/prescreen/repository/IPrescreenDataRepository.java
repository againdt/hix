/**
 * 
 */
package com.getinsured.eligibility.prescreen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.prescreen.PrescreenData;

/**
 * Repository class for Preescreen Data
 * @author Nikhil Talreja
 * @since 19 April, 2013
 *
 */
@Repository
public interface IPrescreenDataRepository extends JpaRepository<PrescreenData, Long> {
	
	PrescreenData findBySessionId(String sessionId);
}
