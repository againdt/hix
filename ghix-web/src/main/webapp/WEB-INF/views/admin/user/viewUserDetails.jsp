<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration.SecurityConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>

<c:set var="isEmailChangeEnabled" value="<%=DynamicPropertiesUtil.getPropertyValue(SecurityConfigurationEnum.IS_EMAIL_CHANGE_ENABLED).toLowerCase()%>" />
<link rel="stylesheet" type="text/css" href="/hix/resources/css/datepicker.css">
<script type="text/javascript" src="/hix/resources/js/bootstrap-datepicker.js"></script>
<div class="row-fluid">
	<div class="gutter10">
			<h1 id="skip">Manage User Account</h1>
	</div>
</div>
<c:set var="userId" ><encryptor:enc value="${user.id}" isurl="true"/> </c:set>
<div class="row-fluid">
	<div class="span3" id="sidebar">
              <div class="gray">
                  <h4 class="margin0">Accounts</h4>
              </div>
              
              <ul class="nav nav-list">
                      <li class="active"><a name="skip">User Details</a></li>
                      <c:if test="${editRoles == 'true'}"><li><a href='<c:url value="/admin/manage/user/roles?userId=${userId}"/>'>User Roles</a></li></c:if>
              </ul>
              
              <c:if test="${logedUser.id==user.id}">
              <div class="margin30-t alert alert-info">
              	<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="information"> A user can't manage his/her own user information or roles. 
              </div>
              </c:if>  
	</div>
	<!-- end of span3 -->
	
	<div class="span9" id="rightpanel">
          	<div class="graydrkaction margin0">
                  <h4 class="span10">User Account Information</h4>
             </div>
		<div class="gutter10">
			<form class="form-horizontal" action="<c:url value="/admin/manage/user/edit" />" method="POST" id="editUserDetails" name="editUserDetails">
				<df:csrfToken/>				
				<%-- <div class="span12 control-group" id="roleEdit">
					<div class="span4">
						<label for="availableRoles">Available Roles</label>
						<select id="availableRoles" size="6">
	                    	<c:forEach var="assigned" items="${roles}">
								<option value="${assigned.name}">${assigned.label}</option>
							</c:forEach>
						</select>
					</div>
					
					<div class="span3" id="addRemoveDiv">
						<a role="button" tabindex="0" class="btn btn-small" id="addRole">Add a Role</a><br>
						<a role="button" tabindex="0" class="btn btn-small" id="removeRole">Remove a Role</a>
					</div>
					
					<div class="span4">												
							<label for="currentRoles">Current Roles</label>								
							<select id="currentRoles" size="6">
								<option disabled value="${defaultRole.name}">${defaultRole.label} (default)</option>
								<c:forEach var="assigned" items="${existingRoles}">
									<option value="${assigned.name}">${assigned.label}</option>
								</c:forEach>
							</select>
							
					</div>					
				</div> --%>
				
					
					
					<input type="hidden" name="accLockConfirm" id="accLockConfirm" value="<encryptor:enc value="${user.confirmed}"/>" />
			
				<input type="hidden" id="id" name="id" value="<encryptor:enc value="${user.id}"/>">
				<%-- 
				<div class="control-group">
					<label for="role" class="control-label">Role<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label>
					<div class="controls">
						<select size="1" id="role" name="role" class="input-medium">
							<c:forEach var="role" items="${roles}">
									<option value="${role.id}" <c:if test="${role.name ==user.userRole.toArray()[0].role.name}"> selected="selected" </c:if>>${role.name}</option>
							</c:forEach>
						</select>
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->
				
				 --%>
				 
				<div class="control-group">
					<label for="userName" class="control-label">User Name<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
						<input type="text" name="userName" id="userName" value="${user.userName}" readonly />						
					</div>	<!-- end of controls-->
					
					<div class="controls" id="accountLockedDiv">
					<%-- <c:choose> --%>
					<c:if test="${user.confirmed == '0'}">
					<c:if test="${user.status == 'Active'|| user.status == null}">
						<label for="accountLocked" class="checkbox">
						<c:choose>
						<c:when test="${logedUser.id!=user.id}">
							<input type="checkbox" name="accountLocked" id="accountLocked" value="<encryptor:enc value="${user.confirmed}"/>"
							<c:if test="${user.confirmed == '0'}">checked="checked"</c:if>
							class="updateAccLockVal">
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="accountLocked" id="accountLocked" value="<encryptor:enc value="${user.confirmed}"/>"
							<c:if test="${user.confirmed == '0'}">checked="checked"</c:if>
							class="updateAccLockVal" disabled>
						</c:otherwise>
						</c:choose>
						Locked</label>
						</c:if>
						</c:if>
						<%-- <c:otherwise>Hiii</c:otherwise> --%>
						<%-- </c:choose> --%>
					</div>
				</div>	<!-- end of control-group -->
				
				<%-- <div class="control-group">
					<label for="phone" class="control-label">Phone Number<img src="/hix/resources/img/requiredAsterisk.png" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
						<input type="text" name="phone" id="phone" value="${user.phone}" />						
					</div>	<!-- end of controls-->
				</div>	 --%>
				<div class="control-group">
					<label for="phone" class="control-label">Phone<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
					<input
							type="hidden" name="phone" value= "${user.phone}"
							id="phone" />
					
						<c:choose>
						<c:when test="${logedUser.id!=user.id}">
							<label for="phone1" class="hidden">&nbsp;</label>
							<input type="text" class="span2 inline input-mini" name="phone1" id="phone1" maxlength="3"/>
							
							<label for="phone2" class="hidden">&nbsp;</label>
							<input type="text" class="span2 inline input-mini" name="phone2" id="phone2" maxlength="3"/>
							
							<label for="phone3" class="hidden">&nbsp;</label>
							<input type="text" class="span2 inline input-mini" name="phone3" id="phone3" maxlength="4" />
						</c:when>
						<c:otherwise>
							<label for="phone1" class="hidden">&nbsp;</label>
							<input type="text" class="span2 inline input-mini" name="phone1" id="phone1" maxlength="3" readonly/>
							
							<label for="phone2" class="hidden">&nbsp;</label>
							<input type="text" class="span2 inline input-mini" name="phone2" id="phone2" maxlength="3" readonly/>
							
							<label for="phone3" class="hidden">&nbsp;</label>
							<input type="text" class="span2 inline input-mini" name="phone3" id="phone3" maxlength="4" readonly/>
						</c:otherwise>
						</c:choose>
						<div id="phone3_error"></div>
						<!--<div id="phone_error" class="help-inline"></div>-->						
					</div>	<!-- end of controls-->
				</div>
				<!-- end of control-group -->
				
				<div class="control-group">
					<label for="email" class="control-label">Email<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
						<%-- <c:choose>
					   		<c:when test="${isEmailChangeEnabled == 'true'}">
					    		<input type="text" name="email" id="email" value="${user.email}"/>
					    	</c:when>
					    	<c:otherwise> --%>
					     		<input type="text" name="email" id="email" value="${user.email}" readonly/>
					    	<%-- </c:otherwise>
					    </c:choose> --%>
						<div id="email_error" class="help-inline"></div>						
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->
				
				<div class="control-group">
					<label for="firstName" class="control-label">First Name<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
					<c:choose>
					<c:when test="${logedUser.id!=user.id}">
						<input type="text" name="firstName" id="firstName" value="${user.firstName}" onkeypress="resetErrorMsg(this)"/>		
					</c:when>
					<c:otherwise>
						<input type="text" name="firstName" id="firstName" value="${user.firstName}" onkeypress="resetErrorMsg(this)" readonly/>		
					</c:otherwise>
					</c:choose>
						<div id="firstName_error" class="help-inline"></div>				
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->
				
				<div class="control-group">
					<label for="lastName" class="control-label">Last Name<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
					<c:choose>
					<c:when test="${logedUser.id!=user.id}">
						<input type="text" name="lastName" id="lastName" value="${user.lastName}" onkeypress="resetErrorMsg(this)"/>
					</c:when>
					<c:otherwise>
						<input type="text" name="lastName" id="lastName" value="${user.lastName}" onkeypress="resetErrorMsg(this)" readonly/>
					</c:otherwise>
					</c:choose>
						<div id="lastName_error" class="help-inline"></div>						
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->
				
			<%-- 	
				<div class="control-group">
					<label for="startDate" class="required control-label">Start Date<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" 
					aria-required="false" aria-hidden="true"></label>
					<div class="controls">
						<div class="input-append date date-picker" id="date" >	
							<input class="span10"  type="text" name="startDate" id="startDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${user.startDate}" />" onkeypress="resetErrorMsg(this)" /> <span class="add-on"><i class="icon-calendar"></i></span>
							<div id="startDate_error" class="help-inline"></div>							
						</div>
					</div>
				</div>

				<div class="control-group">
					<label for="endDate" class="required control-label">End Date<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" 
					aria-required="false" aria-hidden="true"></label>
					<div class="controls">
						<div class="input-append date date-picker" id="date" >	
							<input class="span10"  type="text" name="endDate" id="endDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${user.endDate}" />" onkeypress="resetErrorMsg(this)" /> <span class="add-on"><i class="icon-calendar"></i></span>
							<div id="endDate_error" class="help-inline"></div>							
						</div>
					</div>
				</div>
				 --%>
	
				<div class="control-group">
					<label for="status" class="control-label">Status<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label>
					<div class="controls">
					<c:choose>
					<c:when test="${logedUser.id!=user.id}">
						<select size="1" id="status" name="status" class="input-medium">
							<option value="<encryptor:enc value="Active"/>" <c:if test="${user.status == 'Active'|| user.status == null}"> selected="selected" </c:if>>Active</option>							
							<option value="<encryptor:enc value="Inactive"/>" <c:if test="${user.status == 'Inactive'}"> selected="selected" </c:if>>Inactive</option>
							<option value="<encryptor:enc value="Inactive-dormant"/>" <c:if test="${user.status=='Inactive-dormant'}"> selected="selected" </c:if>>Inactive-dormant</option>
						</select>
						<div id="inactiveDorMessage" class="help-inline"></div>
					</c:when>
					<c:otherwise>
						<select size="1" id="status" name="status" class="input-medium" disabled>
							<option value="<encryptor:enc value="Active"/>" <c:if test="${user.status == 'Active'|| user.status == null}"> selected="selected" </c:if>>Active</option>							
							<option value="<encryptor:enc value="Inactive"/>" <c:if test="${user.status == 'Inactive'}"> selected="selected" </c:if>>Inactive</option>
							<option value="<encryptor:enc value="Inactive-dormant"/>" <c:if test="${user.status=='Inactive-dormant'}"> selected="selected" </c:if>>Inactive-dormant</option>
						</select>
					</c:otherwise>
					</c:choose>
						
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->	
				
				<c:if test="${stateCode == 'MN'}">
					<div class="control-group">
						<label for="extnAppUserId" class="control-label">External App Id</label>
						<div class="controls">
							<c:choose>
								<c:when test="${logedUser.id!=user.id}">
									<input type="text" name="extnAppUserId" id="extnAppUserId" value="${user.extnAppUserId}" />
								</c:when>
								<c:otherwise>
									<input type="text" name="extnAppUserId" id="extnAppUserId" value="${user.extnAppUserId}" readonly/>
								</c:otherwise>
							</c:choose>	
						</div>
					</div>
				</c:if>
						
				<div class="form-actions">
					<a href="<c:url value="/admin/manage/users"/>" id="cancelEditBtn" class="btn btn-medium"><spring:message  code="button.cancel"/></a>
					<c:choose>
					<c:when test="${logedUser.id!=user.id}">
						<input type="hidden" name="isEditAllowed" id="isEditAllowed" value="${editDetails}" />
						<c:if test="${editDetails == 'false'}"><input type="submit" disabled="disabled" name="submit" id="submit" value="<c:choose><c:when test="${stateCode == 'MN'}">Save</c:when><c:otherwise>Save and Send</c:otherwise></c:choose>" class="btn btn-primary" title="<c:choose><c:when test="${stateCode == 'MN'}">Save</c:when><c:otherwise>Save and Send</c:otherwise></c:choose>">
						</c:if>
						<c:if test="${editDetails == 'true'}"><input type="submit" name="submit" id="submit" value="<c:choose><c:when test="${stateCode == 'MN'}">Save</c:when><c:otherwise>Save and Send</c:otherwise></c:choose>" class="btn btn-primary" title="<c:choose><c:when test="${stateCode == 'MN'}">Save</c:when><c:otherwise>Save and Send</c:otherwise></c:choose>">
						</c:if>
					</c:when>
					</c:choose>
				</div>
		</form>
	</div>
</div><!--  end of span9 -->
</div><!-- end row-fluid -->


					 

<script type="text/javascript"> 
 $(document).ready(function(){
	$('.date-picker').datepicker({
		format:"mm/dd/yyyy",
/* 		showOn: 'both',
		buttonImageOnly: true */
	});
	var phone1='${user.phone}';
	var phone=phone1.toString();
	$("#phone1").val(phone.substring(0,3));
	$("#phone2").val(phone.substring(3,6));
	$("#phone3").val(phone.substring(6,10)); 
	$("#role").attr("disabled", true);
	
	
	/* If a role is selected in available roles list, then click "add a role" button to add the selected role to current roles list and remove from available roles list */
	$("#addRole").click(function(){
		var selectedOption = $("#availableRoles option:selected");
		if(selectedOption.val()){
			$("#currentRoles").append("<option value="+ selectedOption.attr("value") +">" + selectedOption.text() + "</option>");
			selectedOption.remove();
		}
		
		
	});
	
	 $("#status").change(function(){
		 var isEditAllowed = $("#isEditAllowed").val();
         var selectedOption = $("#status option:selected");
         
         if(selectedOption.val() && selectedOption.text().toLowerCase()==='inactive-dormant'){
                 $("#inactiveDorMessage").text("Inactive-dormant is a reserved status for system updates."+
                                 "The status indicates that the account has been inactivated by the system due to a specific period of inactivity."+
                                 " Please use the Inactive status to inactivate the account manually");
                 $("#submit").attr("disabled", "disabled");
         }else{
                 $("#inactiveDorMessage").text("");
                 if('true' == isEditAllowed) {
                     $("#submit").removeAttr("disabled");
             	 }
         }
 	});
	
	/* If a role is selected in current roles list, then click "remove a role" button to remove the selected role in current roles list and add to available roles list */
	$("#removeRole").click(function(){
		var selectedOption = $("#currentRoles option:selected");
		if(selectedOption.val()){
			var deleteRole = confirm("Do you want to remove this role?");
			if(deleteRole){
				$("#availableRoles").append("<option value="+ selectedOption.attr("value") +">" + selectedOption.text() + "</option>");
				selectedOption.remove();
			}
		}
		
	});
	
}); 
 
 jQuery('.updateAccLockVal').on('click', function(){
		if(jQuery('.updateAccLockVal').attr('checked') == 'checked'){
			$("#accLockConfirm").val("0");			
		}
		else{
			$("#accLockConfirm").val("1");	
		}
	  });
 
 function resetErrorMsg(element) {
		var elementId = element.id;
		
		$("#" + elementId + "_error").html('');
	}
 
 jQuery.validator.addMethod("validFirstNameCheck", function(value,
			element, param) {
		fName = $("#firstName").val();
		 var asciiReg = /^[a-zA-Z0-9]+$/;
		  if( !asciiReg.test(fName ) ) {
			return false;
		  } else {
			return true;
		  }
		return false;
	});	

	jQuery.validator.addMethod("validLastNameCheck", function(value,
			element, param) {
		lName = $("#lastName").val();
		 var asciiReg = /^[a-zA-Z0-9]+$/;
		  if( !asciiReg.test(lName ) ) {
			return false;
		  } else {
			return true;
		  }
		return false;
	});	
 
 jQuery.validator.addMethod("validEmailCheck", function(value, element, param) {
		password = $("#email").val();
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,25})?$/;
		if (!emailReg.test(password)) {
			return false;
		} else {
			return true;
		}
		return false;
	});
 
 jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	 phone1 = $("#phone1").val(); 
 	 phone2 = $("#phone2").val(); 
 	 phone3 = $("#phone3").val(); 
 
 	if( 
			(phone1 == "" || phone2 == "" || phone3 == "")  || 
			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)
 	  ){ 
 		return false; 
 	}
 	var phone = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
	
	$("#phone").val(phone);
	
 	return true;
});
 
 /* jQuery.validator.addMethod("validDate", function(value, element, param) {
		var startdatevalue = $("#startDate").val();
		var enddatevalue = $("#endDate").val();
		if(Date.parse(startdatevalue) <= Date.parse(enddatevalue)){
			return true;
		}
		else{
			return false;
		}
		
		}); */
 
 $("#editUserDetails")
 .validate(
 		{
 			rules : {
 				firstName : {
 					required : true,
					validFirstNameCheck : true
 				},
 				lastName : {
 					required : true,
					validLastNameCheck : true
 				},
 				email : {
 					required : true,
 					validEmailCheck : true
 				},
 					phone3 : {
						phonecheck : true
					}/* ,
 				endDate :{
 					required:true,
 					validDate:true
 					
 				},
				startDate :{
					required:true,
					
				} */
 			},
 			messages : {
 				firstName : {
 					required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>",
					validFirstNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstNameCheck' javaScriptEscape='true'/></span>"
 				},
 				lastName : {
 					required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>",
					validLastNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateLastNameCheck' javaScriptEscape='true'/></span>"
 				},
 				email : {
 					required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>",
 					validEmailCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEmailSyntax' javaScriptEscape='true'/></span>"
 				},
 					phone3 : {
						phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"
					}/* ,
 				endDate: {
 					required : "<span> <em class='excl'>!</em>End date required</span>",
 					validDate:"<span> <em class='excl'>!</em>End date should be grater than start date</span>"
 				},
				startDate: {
					required : "<span> <em class='excl'>!</em>Start date required</span>"
				} */
	 				
 	 					
 			},
 			onkeyup : false,
 			errorClass : "error",
 			errorPlacement : function(error, element) {
 				var elementId = element.attr('id');
 				error
 						.appendTo($("#" + elementId
 								+ "_error"));
 				$("#" + elementId + "_error").attr('class',
 						'error help-inline');
 			},
 			
 		});
 
</script>