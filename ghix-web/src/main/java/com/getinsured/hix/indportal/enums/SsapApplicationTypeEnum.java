/**
 * 
 */
package com.getinsured.hix.indportal.enums;

/**
 * @author vishwanath_s
 * HIX-86254
 */
public enum SsapApplicationTypeEnum {
	

	QEP("Initial Enrollment"), SEP("Special Enrollment"), OE("Open Enrollment");
	
	private String  value;
	
	private SsapApplicationTypeEnum(String s) {
		this.value = s;
	}
	
	public String value() {
		return value;
	}
	
	public static String value(String v) {
		if (v != null) {
			final SsapApplicationTypeEnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}
	
	private static SsapApplicationTypeEnum readValue(String v) {
		SsapApplicationTypeEnum data = null;
		for (SsapApplicationTypeEnum c : SsapApplicationTypeEnum.class.getEnumConstants()) {
			if (c.toString().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}
}
