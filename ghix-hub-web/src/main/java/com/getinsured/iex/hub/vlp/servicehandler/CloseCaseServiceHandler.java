package com.getinsured.iex.hub.vlp.servicehandler;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpcc_v33.CloseCaseResponseType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpcc_v33.ObjectFactory;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.vlp.wrapper.CloseCaseRequestWrapper;
import com.getinsured.iex.hub.vlp.wrapper.CloseCaseResponseWrapper;
import com.getinsured.iex.hub.vlp37.prefixmappers.VLPNamespacePrefixMapper;

public class CloseCaseServiceHandler extends AbstractServiceHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CloseCaseServiceHandler.class);
	private static ObjectFactory factory;
	private Jaxb2Marshaller marshaller;
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
	}
	
	public Object getRequestpayLoad() throws HubServiceException {
		Object request = null;
		try{
			Object closeCaseRequest = getNamespaceObjects().get(CloseCaseRequestWrapper.class.getName());
			if(closeCaseRequest == null){
				throw new HubServiceException("Request information not available");
			}
			CloseCaseRequestWrapper caseWrapper = (CloseCaseRequestWrapper)closeCaseRequest;
			request = caseWrapper.getSystemRepresentation();
		}
		catch(Exception e){
			throw new HubServiceException("Failed to generate the payload",e);
		}
	
		return request;
	}

	public String handleResponse(Object responseObject)
			throws HubServiceException {
		
		String message = null;
		try{
			@SuppressWarnings("unchecked")
			JAXBElement<CloseCaseResponseType> response = (JAXBElement<CloseCaseResponseType>)responseObject;
			CloseCaseResponseWrapper wrapper = new CloseCaseResponseWrapper(response.getValue());
			if(response != null){
				this.setResponseCode(wrapper.getResponseMetadata().getResponseCode());
				message = wrapper.toJSONString();
				this.requestStatus = RequestStatus.SUCCESS;
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Response CloseCase " + message);
				}
			}
			
			LOGGER.debug("Response message " + message);
		}catch(ClassCastException ce){
			message = "Class in response: " + responseObject.getClass().getName()+" Expecting:"+ CloseCaseResponseType.class.getName();
			this.requestStatus = RequestStatus.FAILED;
			throw new HubServiceException(message, ce);
		}
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		return "H95";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}
}
