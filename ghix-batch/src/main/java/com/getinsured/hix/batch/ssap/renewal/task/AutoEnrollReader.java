package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.ssap.renewal.service.AutoEnrollService;
import com.getinsured.hix.batch.ssap.renewal.util.AutoEnrollPartitionerParams;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.iex.ssap.model.RenewalApplication;

/**
 * Auto Enroll Reader class is used to get read Renewal Application from Partitioner.
 * 
 * @since July 19, 2019
 */
public class AutoEnrollReader implements ItemReader<RenewalApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutoEnrollReader.class);
	private List<RenewalApplication> readRenewalApplicationList;
	private int partition;
	private int loopCount;
	private int startIndex;
	private int endIndex;

	private AutoEnrollService autoEnrollService;
	private AutoEnrollPartitionerParams autoEnrollPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			partition = executionContext.getInt("partition");
			startIndex = executionContext.getInt("startIndex");
			endIndex = executionContext.getInt("endIndex");
			LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);

			List<Long> readRenewalApplicationIdList = null;

			if (null != autoEnrollPartitionerParams && CollectionUtils.isNotEmpty(autoEnrollPartitionerParams.getRenewalApplicationIdList())) {

				if (startIndex < autoEnrollPartitionerParams.getRenewalApplicationIdList().size()
						&& endIndex <= autoEnrollPartitionerParams.getRenewalApplicationIdList().size()) {
					readRenewalApplicationIdList = new CopyOnWriteArrayList<Long>(autoEnrollPartitionerParams.getRenewalApplicationIdList().subList(startIndex, endIndex));
				}
			}

			if (CollectionUtils.isNotEmpty(readRenewalApplicationIdList)) {
				List<List<Long>> chunkedListOfList = RenewalUtils.chunkList(readRenewalApplicationIdList, RenewalUtils.INNER_QUERY_CHUNK_SIZE);
				readRenewalApplicationList = new CopyOnWriteArrayList<RenewalApplication>();

				for (List<Long> chunkedList : chunkedListOfList) {
					readRenewalApplicationList.addAll(new CopyOnWriteArrayList<RenewalApplication>(autoEnrollService.getAutoRenewalApplListByIds(chunkedList)));
				}
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("readRenewalApplicationList is null: {}", CollectionUtils.isEmpty(readRenewalApplicationList));
			}
		}
	}

	@Override
	public RenewalApplication read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		boolean readerFlag = false;
		RenewalApplication readerRenewalApplication = null;

		if (CollectionUtils.isNotEmpty(readRenewalApplicationList)) {

			if (loopCount < readRenewalApplicationList.size()) {
				loopCount++;
				readerFlag = true;
				readerRenewalApplication = readRenewalApplicationList.get(loopCount - 1);
			}
		}

		if (!readerFlag) {
			LOGGER.warn("Unable to find Renewal Application IDs.");
		}
		return readerRenewalApplication;
	}

	public AutoEnrollService getAutoEnrollService() {
		return autoEnrollService;
	}

	public void setAutoEnrollService(AutoEnrollService autoEnrollService) {
		this.autoEnrollService = autoEnrollService;
	}

	public AutoEnrollPartitionerParams getAutoEnrollPartitionerParams() {
		return autoEnrollPartitionerParams;
	}

	public void setAutoEnrollPartitionerParams(AutoEnrollPartitionerParams autoEnrollPartitionerParams) {
		this.autoEnrollPartitionerParams = autoEnrollPartitionerParams;
	}
}
