<script type="text/ng-template" id="maritalstatus-complete.jsp">
  <div class="gutter10">
    <!-- <h3><spring:message code="label.lce.maritalstatus"/>: <spring:message code="label.lce.completed"/></h3> -->
    <spring:message code="label.lce.maritalStatusComplete"/>
  </div>
    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a role="button" tabindex="0" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/> {{nextButtonLifeEvent}}</a>
    </dl>
</script>