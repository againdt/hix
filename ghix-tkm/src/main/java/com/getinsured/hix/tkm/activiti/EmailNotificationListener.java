package com.getinsured.hix.tkm.activiti;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;

import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.tkm.service.TicketListService;
import com.getinsured.hix.tkm.service.TkmNotificationService;
 
 
public class EmailNotificationListener implements JavaDelegate {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(EmailNotificationListener.class.getName());
 
	private TkmNotificationService tkmNotificationService;
	
	private TicketListService ticketListService;
	
	public void setTicketListService(TicketListService ticketListService) {
		this.ticketListService = ticketListService;
	}
	
	public void setTkmNotificationService(TkmNotificationService tkmNotificationService) {
		this.tkmNotificationService = tkmNotificationService;
	}
	
	@Override
	public void execute(DelegateExecution execution) {
		log.info("Inside EmailNotificationListener commented as per HIX-63524");
	/*	TaskService taskService;
		try{
		
		taskService=execution.getEngineServices().getTaskService();
		List<Task> activeTaskList  = taskService.createTaskQuery().processInstanceId(execution.getProcessInstanceId())
				.list();
		TkmTickets tkmTickets = ticketListService.getTkmTicketsByActivitiTaskId(activeTaskList.get(0).getId()).get(0);
		String subject ="Ticket " + tkmTickets.getSubject()+" has pending tasks";

		//both user name and group name are null
		for (Task activeTasks : activeTaskList) {
			StringBuilder message = new StringBuilder("");
			message.append("<br> Your task are pending. <br/><br/>");
			message.append("Task Id: "+activeTasks.getId()+"<br>");
			message.append("Task Name: "+activeTasks.getName()+"<br>");
			message.append("</a>");
			String assignee = activeTasks.getAssignee();
			String owner = null;
			
			if(assignee == null) {
				//ASSUMPTION: Owner should always be a group (only one) in BPM file 
				List<IdentityLink> links = taskService.getIdentityLinksForTask(activeTasks.getId());
				for (IdentityLink identityLink : links) {
					List<Group> group = Context.getProcessEngineConfiguration().getIdentityService().createGroupQuery().groupName(identityLink.getGroupId()).list();
					owner = group.get(0).getName();
					
					if(owner != null) {
						break;
					}
				}
				if(owner!=null){
					List<User> users = Context.getProcessEngineConfiguration().getIdentityService().createUserQuery().memberOfGroup(owner).list();
					for (User user : users) {
						Map<String,String> emailTokens = new HashMap<String, String>();
						emailTokens.put("recipient", user.getEmail());
						emailTokens.put("subject", subject);
						emailTokens.put("name", user.getFirstName()+" "+user.getLastName());
						emailTokens.put("taskDetail",message.toString() );
						
						tkmNotificationService.sendEmailNotification(emailTokens,tkmTickets);	
					}
					
					break;
				}
			}	
			else{
				List<User> users = Context.getProcessEngineConfiguration().getIdentityService().createUserQuery().userId(assignee).list();
				User user =users.get(0);
				Map<String,String> emailTokens = new HashMap<String, String>();
				emailTokens.put("recipient", user.getEmail());
				emailTokens.put("subject", subject);
				emailTokens.put("name", user.getFirstName()+" "+user.getLastName());
				emailTokens.put("taskDetail",message.toString() );
				
				tkmNotificationService.sendEmailNotification(emailTokens,tkmTickets);	
			}
		}
		}
		catch(Exception e){
			log.info("Exception in notification listener: "+ e);
		}
 */
	}
 
}
