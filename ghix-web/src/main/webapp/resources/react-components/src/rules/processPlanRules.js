/**
 * Created by sellathurai_s on 9/4/18.
 */

import { Engine } from 'json-rules-engine';
import rulesData from './json/planRules.json';
import RulesUtil from './rulesUtil';

class ProcessPlanRules {

    constructor(facts){
        this.engine = new Engine();
        this.facts = facts;
        this.rulesData = rulesData;
        this.rulesUtil = new RulesUtil();
    }

    executeRules() {
        return new Promise((resolve) => {
            for (let i = 0; i < this.rulesData.length; i++) {
                this.engine.addRule({
                    conditions: {
                        any: [{
                            all: this.rulesUtil.createRule(this.rulesData[i].rules)
                        }]
                    },
                    event: this.rulesUtil.createEvent(this.rulesData[i].result)
                });
            }

            let displayData = [];
            this.engine
                .run(this.facts)
                .then(events => { // run() returns events with truthy conditions
                    if (events && events.length > 0) {
                        displayData = events.map(item => item.params);
                        resolve(displayData);
                    } else {
                        const returnData = {
                            "type": "SINGLE_COL",
                            "col1": "plans.health_no_plans",
                            "col2": "",
                            "col3": ""
                        };

                        displayData.push(returnData);
                        resolve(displayData);
                    }
                });
        });
    }
}

export { ProcessPlanRules as default};