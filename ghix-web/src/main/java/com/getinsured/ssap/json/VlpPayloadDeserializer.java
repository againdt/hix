package com.getinsured.ssap.json;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.ssap.model.hub.vlp.DocumentType;
import com.getinsured.ssap.model.hub.vlp.VlpPayload;
import com.getinsured.ssap.model.hub.vlp.documents.*;

/**
 * Deserializer for {@link VlpPayload} object that is saved in database column in bad JSON format.
 * <p>
 *   JSON format is sucky in database, because we save exactly what we send to DHS/HUB endpoint, and it
 *   requires some wacky stuff that apparently follows some Government XML structure which is wrong thing
 *   to do when we are dealing with stuff in our OWN GI ecosystem.
 * </p>
 * @author Yevgen Golubenko
 * @since 3/6/19
 */
public class VlpPayloadDeserializer extends JsonDeserializer<VlpPayload>
{
  private static final Logger log = LoggerFactory.getLogger(VlpPayloadDeserializer.class);

  private static final ObjectMapper om = new ObjectMapper();

  static {
    om.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);
  }

  /**
   * We store in database exactly what we send to HUB/DHS, which is badly-structured JSON.
   * Thus when we deserialize it, we need to turn that ugly JSON into our Object representation.
   *
   * @param jp {@link JsonParser}
   * @param ctxt {@link DeserializationContext}
   * @return VlpPayload object deserialized from ugly json that we sent to HUB/DHS.
   * @throws IOException if IO exception occurred.
   * @throws JsonProcessingException if some kind of processing exception happened.
   */
  @Override
  public VlpPayload deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException, JsonProcessingException
  {
    VlpPayload vlpPayload = new VlpPayload();
    JsonNode rootNode = jp.getCodec().readTree(jp);
    DhsDocument doc = null;

    if(rootNode.has("DHSID"))
    {
      JsonNode dhsStub = rootNode.get("DHSID");

      if(dhsStub.has(DocumentType.CERTIFICATE_OF_CITIZENSHIP.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.CERTIFICATE_OF_CITIZENSHIP.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), CertificateOfCitizenshipDocument.class);
      }
      else if(dhsStub.has(DocumentType.DS2019.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.DS2019.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), Ds2019Document.class);
      }
      else if(dhsStub.has(DocumentType.DS2019.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.DS2019.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), Ds2019Document.class);
      }
      else if(dhsStub.has(DocumentType.I20.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.I20.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), I20Document.class);
      }
      else if(dhsStub.has(DocumentType.I94.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.I94.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), I94Document.class);
      }
      else if(dhsStub.has(DocumentType.I94_IN_PASSPORT.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.I94_IN_PASSPORT.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), I94InPassportDocument.class);
      }
      else if(dhsStub.has(DocumentType.I327.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.I327.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), I327Document.class);
      }
      else if(dhsStub.has(DocumentType.I551.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.I551.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), I551Document.class);
      }
      else if(dhsStub.has(DocumentType.I571.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.I571.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), I571Document.class);
      }
      else if(dhsStub.has(DocumentType.I766.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.I766.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), I766Document.class);
      }
      else if(dhsStub.has(DocumentType.MACHINE_READABLE_IMMIGRANT_VISA.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.MACHINE_READABLE_IMMIGRANT_VISA.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), MachineReadableImmigrantVisaDocument.class);
      }
      else if(dhsStub.has(DocumentType.NATURALIZATION_CERTIFICATE.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.NATURALIZATION_CERTIFICATE.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), NaturalizationCertificateDocument.class);
      }
      else if(dhsStub.has(DocumentType.OTHER_ALIEN.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.OTHER_ALIEN.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), OtherAlienDocument.class);
      }
      else if(dhsStub.has(DocumentType.OTHER_I94.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.OTHER_I94.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), OtherI94Document.class);
      }
      else if(dhsStub.has(DocumentType.TEMPORARY_I551_STAMP.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.TEMPORARY_I551_STAMP.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), TemporaryI551StampDocument.class);
      }
      else if(dhsStub.has(DocumentType.UNEXPIRED_FOREIGN_PASSPORT.getDhsId())) {
        JsonNode docNode = dhsStub.get(DocumentType.UNEXPIRED_FOREIGN_PASSPORT.getDhsId());
        doc = om.readValue(jp.getCodec().treeAsTokens(docNode), UnexpiredForeignPassportDocument.class);
      }
    }
    else {
      log.warn("Json doesn't have DHSID node");
    }

    vlpPayload.setDhsDocument(doc);
    vlpPayload.setAka(rootNode.get("AKA").textValue());
    vlpPayload.setLastName(rootNode.get("LastName").textValue());
    vlpPayload.setFirstName(rootNode.get("FirstName").textValue());

    vlpPayload.setMiddleName(text(rootNode.get("MiddleName")));
    vlpPayload.setDateOfBirth(parseDate(rootNode.get("DateOfBirth").asText()));
    vlpPayload.setCasePOCFullName(text(rootNode.get("CasePOCFullName")));
    vlpPayload.setCasePOCPhoneNumber(text(rootNode.get("CasePOCPhoneNumber")));
    vlpPayload.setRequesterCommentsForHub(text(rootNode.get("RequesterCommentsForHub")));

    vlpPayload.setRequestGrantDateIndicator(rootNode.get("RequestGrantDateIndicator").asBoolean());
    vlpPayload.setRequestSponsorDataIndicator(rootNode.get("RequestSponsorDataIndicator").asBoolean());
    vlpPayload.setFiveYearBarApplicabilityIndicator(rootNode.get("FiveYearBarApplicabilityIndicator").asBoolean());

    return vlpPayload;
  }

  private Date parseDate(String date) {
    final SimpleDateFormat sdf = new SimpleDateFormat(DhsDocument.DATE_FORMAT);

    try
    {
      return sdf.parse(date);
    }
    catch (ParseException e)
    {
      log.error("Unable to parse date from JSON", e);
    }

    return null;
  }
  /*
  {"payload":
    {"AKA": "Della Gallagher A",
    "DHSID": {"I571DocumentID":
      {"CaseNumber": null,
        "AlienNumber": "660015499",
          "DocExpirationDate": "2022-01-01"}
      },
      "LastName": "A",
      "FirstName": "Della",
      "MiddleName": "Gallagher",
      "DateOfBirth": "1921-01-01",
      "CasePOCFullName": "Della Gallagher A",
      "CasePOCPhoneNumber": null,
      "RequesterCommentsForHub": null,
      "RequestGrantDateIndicator": true,
       "RequestSponsorDataIndicator": true,
       "FiveYearBarApplicabilityIndicator": true
      },
      "clientIp": null,
      "documentType": "I571",
      "applicationId": 651,
      "cms_partnerid" : "SSHIX_NV2DEV"
    }
   */

  private String text(JsonNode n) {
    if(n!= null && !n.isNull()) {
      String val = n.textValue();

      if(val != null && val.equals("null")) {
        return null;
      }

      return val;
    }

    return null;
  }
}
