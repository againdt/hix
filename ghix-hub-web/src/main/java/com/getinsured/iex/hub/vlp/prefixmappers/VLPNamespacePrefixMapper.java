package com.getinsured.iex.hub.vlp.prefixmappers;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class VLPNamespacePrefixMapper extends NamespacePrefixMapper{
	
	private static final Map<String, String> PREFIXES = new HashMap<String, String>();
	
	static{
		PREFIXES.put("http://vilpsav.ee.sim.dsh.cms.hhs.gov","vil");
		PREFIXES.put("http://vlp.ee.sim.dsh.cms.hhs.gov","vlp");
		
		PREFIXES.put("http://vclpsav.ee.sim.dsh.cms.hhs.gov","vcl");
		PREFIXES.put("http://vclpcc.ee.sim.dsh.cms.hhs.gov","vcl");
		
		PREFIXES.put("http://gcd.ee.sim.dsh.cms.hhs.gov","gcd");
		PREFIXES.put("http://vlpcoi.ee.sim.dsh.cms.hhs.gov","vlp");
	}
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		
		String prefix = PREFIXES.get(namespaceUri);
		
		if(prefix != null){
			return prefix;
		}
		else{
			return suggestion;
		}
	}
}
