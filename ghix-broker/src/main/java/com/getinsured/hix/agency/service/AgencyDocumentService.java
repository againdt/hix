package com.getinsured.hix.agency.service;

import java.util.List;

import com.getinsured.hix.model.agency.AgencyDocument;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;

public interface AgencyDocumentService {

	AgencyDocument saveAgencyDocument(AgencyDocument agencyDocument);

	AgencyDocument findById(Long id);

	List<AgencyDocument> findByAgencyId(Long agencyId);


}
