package com.getinsured.hix.tkm.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.hix.model.TkmQueueUsersResponse;
import com.getinsured.hix.model.TkmTicketsResponse;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;
import com.getinsured.hix.platform.util.JacksonUtils;

public class TkmUtil {
	
	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(TkmUtil.class);
	
	private TkmUtil() {}
	public static boolean getBooleanValueFromEmailConfig(String ticketStatusValue,String emailConfigValue) {
		Map<String, String> map = new HashMap<String, String>();
		boolean emailConfigFlag = false;
	
		try{
		map = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, String.class).readValue(emailConfigValue);
		String emailFlagValue = (String) map.get(ticketStatusValue);
		emailConfigFlag = Boolean.parseBoolean(emailFlagValue);
		}catch(Exception e)
		{
			LOGGER.error("exception occurred while sending mail",e);
			e.getMessage();
		}

		return emailConfigFlag;
	}
	
	/**
	 * 
	 * @param e
	 * @param maxLines
	 * @return
	 */
	public static String shortenedStackTrace(Exception e, int maxLines) {
		 if(e==null){
			 return "";
		 }
	     StringWriter writer = new StringWriter();
	     e.printStackTrace(new PrintWriter(writer));
	     String[] lines = writer.toString().split("\n");
	     StringBuilder sb = new StringBuilder();
	     for (int i = 0; i < Math.min(lines.length, maxLines); i++) {
	         sb.append(lines[i]).append("\n");
	     }
	     return sb.toString();
	 }
	 
	 /**
	  * Method to get FullName as per passed parameters
	  * @param firstName String
	  * @param middleName String
	  * @param lastName String
	  * @return String
	  */
	public static String getFullName(final String firstName, final String middleName, final String lastName)
	{
		StringBuffer fullName = new StringBuffer();
		if (StringUtils.isNotBlank(firstName))
		{
			fullName.append(firstName.trim());
		}

		if (StringUtils.isNotBlank(middleName))
		{
			if(fullName.length() != 0)
			{
				fullName.append(" ");
			}
			fullName.append(middleName.trim());
		}

		if (StringUtils.isNotBlank(lastName))
		{
			if(fullName.length() != 0)
			{
				fullName.append(" ");
			}
			fullName.append(lastName.trim());
		}
		return fullName.toString();
	}
	
	public static String getJsonForTkmTicketsResponse(TkmTicketsResponse tkmResp,String callingAPI){
		String tkmResponse="";
		try {
			tkmResponse = JacksonUtils.getJacksonObjectWriterForJavaType(TkmTicketsResponse.class).writeValueAsString(tkmResp);
		} catch (JsonProcessingException e) {
			LOGGER.error("Exception occurred in " + callingAPI + " API's Json Parsing",e);
		}
		
		return tkmResponse;
	}
	
	public static String getJsonForTkmQueueUsersResponse(TkmQueueUsersResponse tkmResp,String callingAPI){
		String tkmResponse="";
		try {
			tkmResponse = JacksonUtils.getJacksonObjectWriterForJavaType(TkmQueueUsersResponse.class).writeValueAsString(tkmResp);
			
		} catch (JsonProcessingException e) {
			LOGGER.error("Exception occurred in " + callingAPI + " API's Json Parsing",e);
			tkmResponse = "ERROR IN JASON PARSING of " + callingAPI + "	PARSING Exception is:" + e.getMessage();
		}
		
		return tkmResponse;
	}

}
