package com.getinsured.hix.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import com.getinsured.hix.model.Permission;
import com.getinsured.hix.model.PermissionsDto;

public class PermissionsMapper {

	public static PermissionsDto map(Permission permission) {
			PermissionsDto dto = new PermissionsDto();
			dto.setId(permission.getId());
			dto.setName(permission.getName());
						
			return dto;
	}
	
	public static List<PermissionsDto> map(Page<Permission> permissions) {
		List<PermissionsDto> dtos = new ArrayList<PermissionsDto>();
		for (Permission permission: permissions) {
			dtos.add(map(permission));
		}
		return dtos;
	}
}
