<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<div class="span3" id="sidebar">
	<div class="header">
		<h4><spring:message code="indportal.portalhome.mystuff" javaScriptEscape="true"/></h4>
	</div>
	<ul class="nav nav-list">
		<sec:accesscontrollist hasPermission="IND_PORTAL_VIEW_AND_PRINT" domainObject="user">
            <li><a href="javascript:void(0)" ng-click="goDash()"><i class="icon-dashboard"></i> <spring:message code="indportal.portalhome.dashboard" javaScriptEscape="true"/></a></li>
		</sec:accesscontrollist>
		<sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
			<li><a href="#applications"><i class="icon-info-sign"></i> <spring:message code="indportal.portalhome.myapplications" javaScriptEscape="true"/></a></li>
        </sec:accesscontrollist>    
		<li><a href="#enrollmenthistory"><i class="icon-check-sign"></i> My Enrollments</a></li>
        <sec:accesscontrollist hasPermission="IND_PORTAL_INBOX" domainObject="user">
			<li><a href="<c:url value="/inbox/home" />"><i class="icon-envelope"></i> <spring:message code="indportal.portalhome.myinbox" javaScriptEscape="true"/></a></li>
		</sec:accesscontrollist>
		<sec:accesscontrollist hasPermission="IND_PORTAL_MANAGE_PREFERENCES" domainObject="user">
            <li><a href="#mypreferences"><i class="icon-phone-sign"></i> My Preferences</a></li>
		</sec:accesscontrollist>
	</ul>
	      
	<div class="header margin20-t">
		<h4><spring:message code="indportal.portalhome.quicklinks" javaScriptEscape="true"/></h4>
	</div>
	<ul class="nav nav-list" ng-controller="indPortalAppCtrl">
		<div ng-show= "openDialoglce" id="openDialoglce">
			<div modal-show="openDialoglce" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="reportChangeModal" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
                        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        		<h3 id="reportChangeModal"> <spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></h3>
                      	</div>
                      	<div class="modal-body">
                      		<div>
                      			<p class="margin10-l"><spring:message code="indportal.portalhome.ifyouwouldliketomakechanges" javaScriptEscape="true"/></p>
								<div class="alert alert-info">
                        				<p><spring:message code="indportal.portalhome.moreInfoLifeEventsFin"  htmlEscape="false"/></p>
								</div> 
							</div>   
                      	</div>
                      	<div class="modal-footer">
                        		<button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
                        		<button class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
                      	</div>
					</div><!-- /.modal-content-->
				</div> <!-- /.modal-dialog-->
			</div>
		</div>
		<li><a id="pop_findLocalAssistance" href="#"><i class="icon-search"></i> <spring:message code="indportal.portalhome.findlocalassistance" javaScriptEscape="true"/></a></li>
	</ul>
            
	<!-- ACCESS CODE STARTS--->
	<div class="margin20-t">
		<form name="accessCodeForm" action="<c:url value="/referral/verifyaccesscode"/>" method="post" class="noBorder" ng-cloak>
			<df:csrfToken/>
			<div class="header">
				<h4 for="access-code" id="access-code-dashboard">
					<spring:message code="label.homePage.accessCode"/>
				</h4>
			</div>
	 	 	<div class="margin10-t">
		 	 	<input type="text" class="input-small margin10-l" id="access-code" name="accessCode" length="10" ng-focus required ng-model="accessCode" autocomplete="off">
		 	 	<input type="submit" class="btn margin10-l margin10-b" value="<spring:message code="label.homePage.submit"/>" ng-disabled="accessCodeForm.$invalid">
		 	 	<div class="help-inline" id="accessCode_error">
					<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.required && !accessCodeForm.accessCode.$focused">
						<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeRequired"/></span>
					</label>
					<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.pattern && !accessCodeForm.accessCode.$focused">
						<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeValidation"/></span>
					</label>
				</div>
	 	 	</div>
		</form>
	</div>
	<!-- ACCESS CODE ENDS--->

</div>