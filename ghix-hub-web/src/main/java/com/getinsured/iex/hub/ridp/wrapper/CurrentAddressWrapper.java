package com.getinsured.iex.hub.ridp.wrapper;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.LocationCityNameType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.PrimaryRequestInformationType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.RequestPayloadType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.StreetNameType;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.AddressType;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.LocationType;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.StreetType;
import com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.StructuredAddressType;
import com.getinsured.iex.hub.ridp.niem.niem.usps_states._2.USStateCodeSimpleType;
import com.getinsured.iex.hub.ridp.niem.niem.usps_states._2.USStateCodeType;
import com.google.gson.Gson;

/**
 * @author Sunil Desu
 * @since January 20, 2014
 * 
 */
public class CurrentAddressWrapper {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CurrentAddressWrapper.class);
	
	private static com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.ObjectFactory factory;
	private static com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory extFactory;
	private static com.getinsured.iex.hub.ridp.niem.niem.usps_states._2.ObjectFactory stateFactory;

	private LocationType locationType;
	private StructuredAddressType structuredAddressType;
	private StreetType streetType;
	private StreetNameType streetNameType;
	private LocationCityNameType locationCityNameType;
	private USStateCodeType usStateCodeType;

	private String streetName;
	private String locationCityName;
	private String locationStateUSPostalServiceCode;
	private String locationPostalCode;
	private String locationPostalExtensionCode;

	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	
	public CurrentAddressWrapper() {
		factory = new com.getinsured.iex.hub.ridp.niem.niem.niem_core._2.ObjectFactory();
		extFactory = new com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory();
		stateFactory = new com.getinsured.iex.hub.ridp.niem.niem.usps_states._2.ObjectFactory();

		this.locationType = factory.createLocationType();
		this.structuredAddressType = factory.createStructuredAddressType();
		AddressType addressType = factory.createAddressType();
		JAXBElement<StructuredAddressType> structuredAddressTypeJaxbElement = factory.createStructuredAddress(structuredAddressType);
		addressType.setAddressRepresentation(structuredAddressTypeJaxbElement);
		this.locationType.setLocationAddress(addressType);
	}

	@JsonIgnore
	public LocationType getSystemRepresentation() {
		return this.locationType;
	}

	public void setStreetName(String streetName) {
		if (streetName != null && streetName.trim().length() > 0) {
			streetType = factory.createStreetType();
			streetNameType = extFactory.createStreetNameType();
			streetNameType.setValue(streetName);
			streetType.setStreetName(streetNameType);
			JAXBElement<StreetType> jaxbStreetType = factory
					.createLocationStreet(streetType);
			structuredAddressType.setAddressDeliveryPoint(jaxbStreetType);

			this.streetName = streetName;
		}
	}

	public void setLocationCityName(String locationCityName) {
		if (locationCityName != null && locationCityName.trim().length() > 0)
			locationCityNameType = extFactory.createLocationCityNameType();
		locationCityNameType.setValue(locationCityName);
		structuredAddressType.setLocationCityName(locationCityNameType);

		this.locationCityName = locationCityName;
	}

	public void setLocationStateUSPostalServiceCode(
			String locationStateUSPostalServiceCode) {
		if (locationStateUSPostalServiceCode != null
				&& locationStateUSPostalServiceCode.trim().length() > 0) {
			usStateCodeType = stateFactory.createUSStateCodeType();
			usStateCodeType.setValue(USStateCodeSimpleType
					.valueOf(locationStateUSPostalServiceCode));
			JAXBElement<USStateCodeType> jaxbUSStateCodeType = factory
					.createLocationStateUSPostalServiceCode(usStateCodeType);
			structuredAddressType.setLocationState(jaxbUSStateCodeType);

			this.locationStateUSPostalServiceCode = locationStateUSPostalServiceCode;
		}
	}

	public void setLocationPostalCode(String locationPostalCode) {
		if (locationPostalCode != null
				&& locationPostalCode.trim().length() > 0) {
			structuredAddressType.setLocationPostalCode(locationPostalCode);

			this.locationPostalCode = locationPostalCode;
		}
	}

	public void setLocationPostalExtensionCode(
			String locationPostalExtensionCode) {
		if (locationPostalExtensionCode != null
				&& locationPostalExtensionCode.trim().length() > 0) {
			structuredAddressType
					.setLocationPostalExtensionCode(locationPostalExtensionCode);

			this.locationPostalExtensionCode = locationPostalExtensionCode;
		}
	}

	public String getStreetName() {
		return this.streetName;
	}

	public String getLocationCityName() {
		return this.locationCityName;
	}

	public String getLocationStateUSPostalServiceCode() {
		return this.locationStateUSPostalServiceCode;
	}

	public String getLocationPostalCode() {
		return this.locationPostalCode;
	}

	public String getLocationPostalExtensionCode() {
		return this.locationPostalExtensionCode;
	}

	public static CurrentAddressWrapper fromJsonString(String jsonString) throws IOException {
		return gson.fromJson(jsonString, CurrentAddressWrapper.class);
	}

	public String toJsonString() throws JsonProcessingException {
		CurrentAddressWrapper currentAddress = new CurrentAddressWrapper();
		currentAddress.setLocationCityName(this.locationCityName);
		currentAddress.setStreetName(this.streetName);
		currentAddress.setLocationPostalCode(this.locationPostalCode);
		currentAddress
				.setLocationPostalExtensionCode(this.locationPostalExtensionCode);
		currentAddress
				.setLocationStateUSPostalServiceCode(this.locationStateUSPostalServiceCode);
		return gson.toJson(currentAddress, CurrentAddressWrapper.class);

	}

	public static void main(String[] args) throws IOException {
		com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory fac = new com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory();
		CurrentAddressWrapper currentAddress = new CurrentAddressWrapper();
		currentAddress.setLocationCityName("city");
		currentAddress.setStreetName("street");
		currentAddress.setLocationPostalCode("12345");
		currentAddress.setLocationPostalExtensionCode("7890");
		currentAddress.setLocationStateUSPostalServiceCode("CA");

		PrimaryRequestInformationType req = fac
				.createPrimaryRequestInformationType();
		req.setCurrentAddress(currentAddress.getSystemRepresentation());

		JAXBElement<PrimaryRequestInformationType> pReq = fac
				.createPrimaryRequest(req);
		RequestPayloadType reqPayLoad = fac.createRequestPayloadType();
		reqPayLoad.setRequest(pReq);

		//JAXBElement<Object> reqObj = fac.createRequest(reqPayLoad);
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//m.marshal(reqObj, System.out);
			LOGGER.debug(currentAddress.toJsonString());
			CurrentAddressWrapper currentAddress2 = CurrentAddressWrapper.fromJsonString(currentAddress.toJsonString());
			LOGGER.debug(currentAddress2.getLocationCityName());
		} catch (JAXBException e) {
			LOGGER.error(e.getMessage(),e);
		}
		
	}

}
