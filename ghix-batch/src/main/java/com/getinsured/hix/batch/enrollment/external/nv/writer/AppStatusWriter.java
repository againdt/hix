package com.getinsured.hix.batch.enrollment.external.nv.writer;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.client.WebServiceIOException;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.indportal.dto.dm.application.result.ApplicationStatusData;
import com.getinsured.hix.indportal.dto.dm.application.result.BrokerDesignationData;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

public class AppStatusWriter implements ItemWriter<ApplicationStatusData>, ApplicationContextAware  {
	private static final Logger LOGGER = LoggerFactory.getLogger(AppStatusWriter.class);

	private String fileName;

	private String jobId;
	private static ApplicationContext applicationContext;
	private static JAXBContext jc = null;
	private static Marshaller marshaller = null;
	private SsapApplicationRepository ssapApplicationRepository;
	static {
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.erp.gov.cms.dsh.at.extension._1");
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);

		} catch (JAXBException e) {
			LOGGER.error("Error while initializing JAXBContext", e);
		}

	}
    public void write(List<? extends ApplicationStatusData> items) throws Exception { 
    	LOGGER.debug("Linking Application-broker starts for some applications"); 
		ssapApplicationRepository = (SsapApplicationRepository) applicationContext.getBean("ssapApplicationRepository");
		
        for(ApplicationStatusData data : items)
		{
        	if(data.getApplicationId() > 0)
        	{
				StringBuilder errorResponse = new StringBuilder();
				String status = "";
				try {
					ssapApplicationRepository.updateApplicationStatusById(ApplicationStatus.OPEN.toString(), data.getApplicationId());
					//LOGGER.debug("Linking Application {} broker {} done",data.getBrokerNPN(),data.getHouseholdEmail()); 
				} catch (WebServiceIOException wsIOE) {
					errorResponse.append("Error calling Account Transfer Endpoint:").append(wsIOE.getMessage());
				} catch (Exception e) {
					errorResponse.append("Error calling Account Transfer Endpoint:").append(e.getMessage());
				}
        	}
		}
        LOGGER.debug("Linking Application-broker ends for some applications"); 
    }

    private void updateErrorFiles(BrokerDesignationData data,String status) {
		if(!"SUCCESS".equalsIgnoreCase(status))
		{
			updateFailedJson(data.getJsonSource());
			//updateRequestXML(data);
		}
		String requestId = data.getHouseholdEmail();
		
		
		if(!StringUtils.isEmpty(data.getErrorMessage()))
		{
			updateErrorDetails(requestId+","+data.getErrorMessage());
		}
	}
    private void updateErrorDetails(String errorMessage) {
    	try {
    		
		    Path path = Paths.get(fileName);
		    String absolutePath = path.getParent().toString()+File.separator+jobId+"_failedRecords_descr_brokerLinking.csv";
		    path = Paths.get(absolutePath);
		    Files.write(path, (errorMessage + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final Exception e) {
			LOGGER.error("Error while writing the data for errorDescrFile : ",e);
			LOGGER.error("--- Errorlist print starts ---");
			LOGGER.error(errorMessage);
			LOGGER.error("--- Errorlist print ends ---");
		}
	}

	private void updateFailedJson(String errorList ) {
		try {
		    Path path = Paths.get(fileName);
		    String absolutePath = path.getParent().toString()+File.separator+jobId+"_failedJson_brokerLinking.txt";
		    path = Paths.get(absolutePath);
		    Files.write(path, (errorList+ System.lineSeparator()).getBytes(StandardCharsets.UTF_8), Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final Exception e) {
			LOGGER.error("Error while writing the data for errorDescrFile : ",e);
			LOGGER.error("--- Errorlist print starts ---");
			LOGGER.error(errorList);
			LOGGER.error("--- Errorlist print ends ---");
		}
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		applicationContext = context;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
}
