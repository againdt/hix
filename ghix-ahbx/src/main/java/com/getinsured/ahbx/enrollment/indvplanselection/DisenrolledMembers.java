//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.20 at 10:29:02 PM PDT 
//


package com.getinsured.ahbx.enrollment.indvplanselection;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for disenrolledMembers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="disenrolledMembers">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="disenrollMemberId" type="{http://ahbx.accenture.com/AHBXValidation}LongLength10"/>
 *         &lt;element name="disenrollmentId" type="{http://ahbx.accenture.com/AHBXValidation}StringLength10" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "disenrolledMembers", propOrder = {
    "disenrollMemberId",
    "disenrollmentId"
})
public class DisenrolledMembers {

    protected long disenrollMemberId;
    @XmlElement(required = true)
    protected List<String> disenrollmentId;

    /**
     * Gets the value of the disenrollMemberId property.
     * 
     */
    public long getDisenrollMemberId() {
        return disenrollMemberId;
    }

    /**
     * Sets the value of the disenrollMemberId property.
     * 
     */
    public void setDisenrollMemberId(long value) {
        this.disenrollMemberId = value;
    }

    /**
     * Gets the value of the disenrollmentId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the disenrollmentId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDisenrollmentId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDisenrollmentId() {
        if (disenrollmentId == null) {
            disenrollmentId = new ArrayList<String>();
        }
        return this.disenrollmentId;
    }

}
