package com.getinsured.hix.indportal.dto.dashboard;

import java.util.List;

import com.getinsured.hix.indportal.dto.dashboard.DashboardPlanDTO.PlanTypeEnum;

public class DashboardEnrollmentDTO {

	private String id;
	private PlanTypeEnum type;
	private Float aptc;
	private String csr;
	private String coverageStartDate;
	private String coverageEndDate;
	private boolean renewalEnrollment;
	private List<DashboardEnrolleeDTO> enrollees;
	private DashboardPlanDTO plan;
 	private Long ssapApplicationId;
 	private Long priorSsapApplicationId;
 	private String enrollmentStatus;
 	private boolean allowChangePlan;
 	private String enrollmentCreationDate;
 	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PlanTypeEnum getType() {
		return type;
	}

	public void setType(PlanTypeEnum type) {
		this.type = type;
	}

	public Float getAptc() {
		return aptc;
	}

	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}

	public String getCsr() {
		return csr;
	}

	public void setCsr(String csr) {
		this.csr = csr;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public String getCoverageEndDate() {
		return coverageEndDate;
	}

	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}

	public boolean getRenewalEnrollment() {
		return renewalEnrollment;
	}

	public void setRenewalEnrollment(boolean renewalEnrollment) {
		this.renewalEnrollment = renewalEnrollment;
	}

	public List<DashboardEnrolleeDTO> getEnrollees() {
		return enrollees;
	}

	public void setEnrollees(List<DashboardEnrolleeDTO> enrollees) {
		this.enrollees = enrollees;
	}

	public DashboardPlanDTO getPlan() {
		return plan;
	}

	public void setPlan(DashboardPlanDTO plan) {
		this.plan = plan;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public Long getPriorSsapApplicationId() {
		return priorSsapApplicationId;
	}

	public void setPriorSsapApplicationId(Long priorSsapApplicationId) {
		this.priorSsapApplicationId = priorSsapApplicationId;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}

	public boolean getAllowChangePlan() {
		return allowChangePlan;
	}

	public void setAllowChangePlan(boolean allowChangePlan) {
		this.allowChangePlan = allowChangePlan;
	}

	public String getEnrollmentCreationDate() {
		return enrollmentCreationDate;
	}

	public void setEnrollmentCreationDate(String enrollmentCreationDate) {
		this.enrollmentCreationDate = enrollmentCreationDate;
	}

}
