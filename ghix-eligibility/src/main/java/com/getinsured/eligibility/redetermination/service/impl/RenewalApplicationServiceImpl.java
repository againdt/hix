package com.getinsured.eligibility.redetermination.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.redetermination.service.RenewalApplicationService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.eligibility.renewal.service.SsapToConsiderService;
import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderRequestDTO;
import com.getinsured.hix.dto.eligibility.RenewalStatusNotToConsiderResponseDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.planmgmt.PediatricPlanResponseDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.RenewalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.RenewalApplicantRepository;
import com.getinsured.iex.ssap.repository.RenewalApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

@Service
public class RenewalApplicationServiceImpl implements RenewalApplicationService,RenewalConstants {
	
	@Autowired
	private RenewalApplicationRepository renewalApplicationRepository;

	@Autowired
	private RenewalApplicantRepository renewalApplicantRepository;
	
	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;
	
	@Autowired
	private LookupService lookupService;
	
	private static final String PLAN_YEAR = "planYear";
	private static final String HIOS_PLAN_ID = "hiosPlanId";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RenewalApplicationServiceImpl.class);
	
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	
	@Autowired
	GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	CoverageStartDateService coverageStartDateService;
	
	@Autowired 
	private Gson platformGson;	
	
	@Autowired
	private SsapToConsiderService ssapToConsiderService;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	
	
	@Override
	public RenewalApplication saveRenewalApplication(RenewalApplication renewalApplication) {
		return renewalApplicationRepository.save(renewalApplication);
	}
	
	@Override
	public RenewalApplication findBySsapApplicationId(Long ssapApplicationId) {
		return renewalApplicationRepository.findBySsapApplicationId(ssapApplicationId);
	}
	
	
	public Map<String,Object> getRenewalApplicationStatus(Long ssapApplicationId) throws GIException, Exception
	{
		Map<String,Object> renewalStatusMap = new HashMap<String, Object>();
		RenewalApplication renewalApplication = renewalApplicationRepository.findBySsapApplicationId(ssapApplicationId);
		if(renewalApplication == null)
		{
			Long renewalYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
			SsapApplication ssapApplication = ssapApplicationRepository.findByIdAndCoverageYear(renewalYear, ssapApplicationId);
			if(ssapApplication != null)
			{
				renewalApplication = ssapToConsiderService.executeToConsiderLogicForRenewal(ssapApplication,renewalYear, null);
				if(renewalApplication != null)
				{
					renewalStatusMap.put("healthRenewalStatus", renewalApplication.getHealthRenewalStatus());
					renewalStatusMap.put("dentalRenewalStatus", renewalApplication.getDentalRenewalStatus());
				}
			}
			else
			{
				throw new GIException("No SsapApplication found with APPLICATION_STATUS = 'ER' and APPLICATION_TYPE = 'OE' for id : " + ssapApplicationId + " and coverage year = " + renewalYear);
			}
		}
		else
		{
			renewalStatusMap.put("healthRenewalStatus", renewalApplication.getHealthRenewalStatus());
			renewalStatusMap.put("dentalRenewalStatus", renewalApplication.getDentalRenewalStatus());
		}
		return renewalStatusMap;
	}
	
	@Override
	public Set<String> dependentDentalAgeCheck(SsapApplication ssapApplication,SsapApplicant ssapApplicant,Set<String> dentalEnrolledMembers,Long renewalYear,String planId) throws GIException{
		
		return getEligibleEnrollee(dentalEnrolledMembers, ssapApplication, ssapApplicant, renewalYear,planId);
	}
	
	private Integer getReasonCode(String lookupValueCode)
	{
		Integer reasonCode =  lookupService.getlookupValueIdByTypeANDLookupValueCode(RENEWALS,lookupValueCode );
		return reasonCode;
	}
	
	private Set<String> getEligibleEnrollee(Set<String> activeDentalEnrollee,SsapApplication ssapApplication,SsapApplicant ssapApplicant,Long renewalYear,String planId) throws GIException{
		Set<String> eligibleDentalEnrollee = new HashSet<String>();
		Set<String> ageOutEnrollee  = new HashSet<String>();
		Set<String> ageOut26Enrollee  = new HashSet<String>();
		String childRelations = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_CHILDRELATIONCODE);
		Long previousCoverageYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
		String memberSource = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.CONSIDER_ALL_FROM_AT_OR_ENROLLMENT); // AT for ID and ENROLLMENT for MN,CA,NV
		String useExternalMemberId = DynamicPropertiesUtil.getPropertyValue(USE_MIGRATION_ENROLLMENTS);
		String memberId = TRUE.equalsIgnoreCase(useExternalMemberId) ? ssapApplicant.getExternalApplicantId() : ssapApplicant.getApplicantGuid();
		if(activeDentalEnrollee ==null || activeDentalEnrollee.isEmpty()){
			return activeDentalEnrollee;
		}
		
		String[] childRelationArr = childRelations.split(",");
		Set<String> bloodRelations = new HashSet<String>();
		for (String childRelation : childRelationArr) {
			bloodRelations.add(childRelation);
		}
		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
		
		List<BloodRelationship> relationShip = getBloodRelationship(members);
		if(relationShip == null){
			throw new GIRuntimeException("Can't find relationship for application :"+ssapApplication.getId());
		}
		
		Date coverageStartDate = getCoverageStartDate(ssapApplication);
		
		//check if child relationship build age out set 
		for (BloodRelationship bloodRelationship : relationShip) {
			if (bloodRelationship.getRelatedPersonId() != null && Integer.parseInt(bloodRelationship.getRelatedPersonId()) == 1) {
				if(bloodRelations.contains(bloodRelationship.getRelation())){	
					if (ssapApplicant.getPersonId() == Integer.parseInt(bloodRelationship.getIndividualPersonId())) {
						if (checkForAge19(ssapApplicant.getBirthDate(),coverageStartDate) && ssapApplicant.getApplyingForCoverage().equals(Y)  && (AT.equals(memberSource) || activeDentalEnrollee.contains(memberId))) {
							ageOutEnrollee.add(memberId);
							//build set for age more 26 
							if(checkForAge26(ssapApplicant.getBirthDate(),coverageStartDate)){
								ageOut26Enrollee.add(memberId);
							}
						}
					}
				}
			}
		}
		
		//no age out member
		if(!ageOutEnrollee.isEmpty()){
			//is child only plan
			boolean isChildOnly = isChildOnlyPlan(planId,previousCoverageYear);
			//if adult dental plan
			if(!isChildOnly){
				//Adult Dental Plan only members more than 26 will age out
				ageOutEnrollee = ageOut26Enrollee;
			}
		}
		//filter non seeking and age out members
		eligibleDentalEnrollee = filterNonSeekingMembers(ssapApplicant,ageOutEnrollee,activeDentalEnrollee);
		return eligibleDentalEnrollee;
	}

	public Date getCoverageStartDate(SsapApplication ssapApplication) throws GIException {
		Timestamp timestamp = coverageStartDateService.computeCoverageStartDate(new BigDecimal(ssapApplication.getId()), new HashMap<String,EnrollmentShopDTO>(), false, false);
		Calendar startDate = TSCalendar.getInstance();
		startDate.setTimeInMillis(timestamp.getTime());
		return startDate.getTime();
		
	}
	
	private List<BloodRelationship> getBloodRelationship(List<HouseholdMember> members) {
		for(HouseholdMember householdMember : members){
			if(householdMember.getPersonId() == 1){
				return householdMember.getBloodRelationship();
			}
		}
		return null;
	}
	
	
	private Set<String> filterNonSeekingMembers(SsapApplicant ssapApplicant,Set<String> ageOutEnrollee,Set<String> activeDentalEnrollee) {
		Set<String> eligibleDentalEnrollee = new HashSet<String>();
		String useExternalMemberId = DynamicPropertiesUtil.getPropertyValue(USE_MIGRATION_ENROLLMENTS);
		String memberSource = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.CONSIDER_ALL_FROM_AT_OR_ENROLLMENT); // AT for ID and ENROLLMENT for MN,CA,NV
		String memberId = TRUE.equalsIgnoreCase(useExternalMemberId) ? ssapApplicant.getExternalApplicantId() : ssapApplicant.getApplicantGuid();
		if(ssapApplicant.getApplyingForCoverage().equals(Y) && !ageOutEnrollee.contains(memberId) && (AT.equals(memberSource) || activeDentalEnrollee.contains(memberId)))
		{
			eligibleDentalEnrollee.add(memberId);
		}
		return eligibleDentalEnrollee;
	}
	
	
	private boolean isChildOnlyPlan(String cmsPlanId,Long coverageYear) {
		Map<String,Object> requestMap = new HashMap<>();
		requestMap.put(HIOS_PLAN_ID, cmsPlanId);
		requestMap.put(PLAN_YEAR, coverageYear);
		String request = platformGson.toJson(requestMap);
		PediatricPlanResponseDTO pediatricPlanResponseDTO=ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_PEDIATRIC_PLAN_INFO, request,PediatricPlanResponseDTO.class);
			return pediatricPlanResponseDTO.isChildOnly();
	}

	private boolean checkForAge19(Date birthDate, Date coverageStartDate) {
		Calendar dob = TSCalendar.getInstance();  
		dob.setTime(birthDate);  
		Calendar today = TSCalendar.getInstance();  
		today.setTime(coverageStartDate);
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);  
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
		  age--;  
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
		    && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
		  age--;  
		}
		
		return (age >= 19);
	}
	private boolean checkForAge26(Date birthDate, Date coverageStartDate) {
		Calendar dob = TSCalendar.getInstance();  
		dob.setTime(birthDate);  
		Calendar today = TSCalendar.getInstance();  
		today.setTime(coverageStartDate);
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);  
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;  
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
			age--;  
		}
		
		return (age >= 26);
	}

	/**
	 * Method is used to set RenewalStatus.NOT_TO_CONSIDER by SSAP Application ID and Plan Type in Renewal Application and Renewal Applicant tables.
	 */
	@Override
	public RenewalStatusNotToConsiderResponseDTO setNotToConsiderRenewalStatusByApplIdAndPlanType(RenewalStatusNotToConsiderRequestDTO requestDTO) {

		LOGGER.info("setNotToConsiderRenewalStatusByApplicationId() start");

		RenewalStatusNotToConsiderResponseDTO response = new RenewalStatusNotToConsiderResponseDTO();
		response.startResponse();
		response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
		response.setMessage("Failed to update Renewal Status");
		boolean updateRenewalStatus = false;

		try {

			if (!validateParamsOfSetNotToConsiderRenewalStatusAPI(requestDTO, response)) {
				return response;
			}

			Integer reasonCode = lookupService.getlookupValueIdByTypeANDLookupValueCode(RenewalLookup.TYPE.getCode(), RenewalLookup.CUSTOM_GROUPING_TO_MARK_NOT_TO_CONSIDER.getCode());

			if (null != reasonCode) {

				int updateCount = renewalApplicantRepository.updateApplicantRenewalStatusByApplIdAndPlanType(
						RenewalStatus.NOT_TO_CONSIDER, reasonCode, requestDTO.getSsapApplicationId(), requestDTO.getPlanType().toUpperCase());
	
				if (0 < updateCount) {

					if (RenewalConstants.HEALTH.equalsIgnoreCase(requestDTO.getPlanType())) {
						renewalApplicationRepository.updateHealthApplicationRenewalStatusByApplId(RenewalStatus.NOT_TO_CONSIDER, reasonCode, requestDTO.getSsapApplicationId());
						updateRenewalStatus = true;
					}
					else if (RenewalConstants.DENTAL.equalsIgnoreCase(requestDTO.getPlanType())) {
						renewalApplicationRepository.updateDentalApplicationRenewalStatusByApplId(RenewalStatus.NOT_TO_CONSIDER, reasonCode, requestDTO.getSsapApplicationId());
						updateRenewalStatus = true;
					}
				}
				else {
					response.setStatusCode(HttpStatus.NOT_FOUND);
					response.setMessage("No Record found for SSAP Application ID: " + requestDTO.getSsapApplicationId());
				}
			}
			else {
				response.setStatusCode(HttpStatus.NOT_FOUND);
				response.setMessage("Custom Grouping to Mark Not To Consider("+ RenewalLookup.CUSTOM_GROUPING_TO_MARK_NOT_TO_CONSIDER.getCode() +") - Reason Code is not found.");
			}
		}
		catch(Exception ex) {
			response.setMessage(response.getMessage() + ": " + ex.getMessage());
			LOGGER.error(response.getMessage(), ex);
		}
		finally {

			if (updateRenewalStatus) {
				response.setStatusCode(HttpStatus.OK);
				response.setMessage("Renewal Status is successfully updated with NOT_TO_CONSIDER for SSAP Application ID: " + requestDTO.getSsapApplicationId());
			}
			else {
				LOGGER.error(response.getMessage());
			}
			LOGGER.info("setNotToConsiderRenewalStatusByApplicationId() end with Update Renewal Status: " + updateRenewalStatus);
			response.endResponse();
		}
		return response;
	}

	/**
	 * Method is used to validated Parameters Of setNotToConsiderRenewalStatus API.
	 */
	private boolean validateParamsOfSetNotToConsiderRenewalStatusAPI(RenewalStatusNotToConsiderRequestDTO requestDTO,
			RenewalStatusNotToConsiderResponseDTO response) {

		boolean hasValidData = false;

		if (null == requestDTO) {
			response.setStatusCode(HttpStatus.BAD_REQUEST);
			response.setMessage("Request DTO is empty.");
			return hasValidData;
		}

		StringBuffer errorMessage = new StringBuffer("Invalid ");
		hasValidData = true;

		if (null == requestDTO.getSsapApplicationId() || 0 >= requestDTO.getSsapApplicationId()) {
			hasValidData = false;
			errorMessage.append("SSAP Application ID: " + requestDTO.getSsapApplicationId());
		}

		if (StringUtils.isBlank(requestDTO.getPlanType()) || 
				(!RenewalConstants.HEALTH.equalsIgnoreCase(requestDTO.getPlanType()) && !RenewalConstants.DENTAL.equalsIgnoreCase(requestDTO.getPlanType()))) {

			if (!hasValidData) {
				errorMessage.append(", ");
			}
			hasValidData = false;
			errorMessage.append("Plan Type: " + requestDTO.getPlanType());
		}

		if (!hasValidData) {
			response.setStatusCode(HttpStatus.BAD_REQUEST);
			response.setMessage(errorMessage.toString());
		}
		return hasValidData;
	}

	@Override
	public void markRenewalApplicationClose(long ssapApplicationId) {
		Integer reasonCode = lookupService.getlookupValueIdByTypeANDLookupValueCode(RenewalLookup.TYPE.getCode(),
				RenewalConstants.CLOSE_APPLICATION);
		int status = renewalApplicantRepository.updateRenewalStatusByApplicationId(RenewalStatus.CLOSED, reasonCode,
					ssapApplicationId, new Timestamp(new TSDate().getTime()));
		if (status >= 1) {
			renewalApplicationRepository.updateRenewalStatusByApplicationId(RenewalStatus.CLOSED,
					reasonCode, ssapApplicationId, new Timestamp(new TSDate().getTime()));
		}
	}
	
	@Override
	public void updateRenewalStatus(String plantType,Long ssapApplicationId,RenewalStatus renewalStatus) throws GIException
	{
		RenewalApplication renewalApplication = renewalApplicationRepository.findBySsapApplicationId(ssapApplicationId);
		if(renewalApplication != null && renewalStatus != null)
		{
			LOGGER.info("updateRenewalStatus() with plantType: {}, ssapApplicationId: {}, renewalStatus: {}", plantType, ssapApplicationId, renewalStatus);
			
			if(RenewalConstants.HEALTH.equalsIgnoreCase(plantType) && RenewalStatus.TO_CONSIDER.equals(renewalApplication.getHealthRenewalStatus()))
			{
				renewalApplication.setHealthRenewalStatus(renewalStatus);
				if(RenewalStatus.MANUAL_ENROLMENT.equals(renewalStatus))
				{
					renewalApplication.setHealthReasonCode(getReasonCode(USER_SHOPPED_PLANS));
				}
			}
			else if(RenewalConstants.DENTAL.equalsIgnoreCase(plantType))
			{
				renewalApplication.setDentalRenewalStatus(renewalStatus);
				if(RenewalStatus.MANUAL_ENROLMENT.equals(renewalStatus))
				{
					renewalApplication.setHealthReasonCode(getReasonCode(USER_SHOPPED_PLANS));
				}
			}
			
			renewalApplication.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
			renewalApplicationRepository.save(renewalApplication);
		}
	}
	
	@Override
	public RenewalStatus getRenewalStatus (String enrlRenewalFlag,RenewalStatus renewalApplicationStatus)
	{
		RenewalStatus renewalStatus = null;
		if(enrlRenewalFlag == null && ( RenewalStatus.TO_CONSIDER.equals(renewalApplicationStatus)))
		{
			renewalStatus = RenewalStatus.MANUAL_ENROLMENT;
		}
		else if("A".equalsIgnoreCase(enrlRenewalFlag) || "M".equalsIgnoreCase(enrlRenewalFlag))
		{
			renewalStatus = RenewalStatus.RENEWED;
		}
		return renewalStatus;
	}
}
