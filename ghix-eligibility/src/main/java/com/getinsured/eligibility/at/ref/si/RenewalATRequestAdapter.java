package com.getinsured.eligibility.at.ref.si;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("renewalATRequestAdapter")
@Scope("singleton")
@DependsOn("dynamicPropertiesUtil")
public class RenewalATRequestAdapter extends ATRequestAdapter {
	private static final Logger LOGGER = Logger.getLogger(RenewalATRequestAdapter.class);
	private String stateCode;

	@PostConstruct
	public void createStateContext() {
		stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	String getStateCode() {
		return stateCode;
	}

	@Override
	void populateAccountTransferProperties(AccountTransferRequestDTO accountTransferRequestDTO, ReferralResponse input) {
		LOGGER.info("renewalATRequestAdapter - populateAccountTransferProperties ");
		accountTransferRequestDTO.setRenewal(true);
		accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.RENEWAL.value());
		accountTransferRequestDTO.setMultipleCmr(ReferralUtil.convertToBoolean(input.getData().get(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE)));
		accountTransferRequestDTO.setNonFinancialCmr(ReferralUtil.convertToBoolean(input.getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR)));
		accountTransferRequestDTO.setNonFinancialCmrId(ReferralUtil.convertToInt(input.getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID)));
		accountTransferRequestDTO.setEnrolledApplicationId(ReferralUtil.convertToLong(input.getData().get(ReferralProcessingConstants.KEY_ENROLLED_APPLICATION_ID)));
		accountTransferRequestDTO.setRenewalCompareToApplicationId(ReferralUtil.convertToLong(input.getData().get(ReferralProcessingConstants.KEY_RENEWAL_COMPARED_TO_APPLICATION_ID)));
	}

}
