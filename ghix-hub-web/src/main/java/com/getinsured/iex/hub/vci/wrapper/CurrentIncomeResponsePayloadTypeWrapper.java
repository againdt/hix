package com.getinsured.iex.hub.vci.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentIncomeInfoType;
import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentIncomeResponsePayloadType;

/**
 * Wrapper class for com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.
 * CurrentIncomeResponsePayloadType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class CurrentIncomeResponsePayloadTypeWrapper implements JSONAware {

	private ResponseMetadataWrapper responseMetadata;
	private List<CurrentIncomeInfoTypeWrapper> responseInformation;

	private static final String METADATA_KEY = "ResponseMetadata";
	private static final String RESPONSE_KEY = "ResponseInformation";
	
	public CurrentIncomeResponsePayloadTypeWrapper(
			CurrentIncomeResponsePayloadType currentIncomeResponsePayloadType) {

		if (currentIncomeResponsePayloadType == null) {
			return;
		}
		
		this.responseMetadata = new ResponseMetadataWrapper(currentIncomeResponsePayloadType.getResponseMetadata());
		for(CurrentIncomeInfoType income : currentIncomeResponsePayloadType.getResponseInformation()){
			getResponseInformation().add(new CurrentIncomeInfoTypeWrapper(income));
		}

	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public List<CurrentIncomeInfoTypeWrapper> getResponseInformation() {
		if (responseInformation == null) {
			responseInformation = new ArrayList<CurrentIncomeInfoTypeWrapper>();
		}
		return this.responseInformation;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(METADATA_KEY, this.responseMetadata);
		obj.put(RESPONSE_KEY, this.responseInformation);
		return obj.toJSONString();
	}
	
}
