package com.getinsured.hix.batch;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.Nodes;

@Component
public class BatchNodeUtil {
	
	private static Logger LOGGER = LoggerFactory.getLogger(BatchNodeUtil.class);
	@PersistenceUnit private EntityManagerFactory emf;
	
	@SuppressWarnings("unchecked")
	public  JSONArray getBatchNodes() {
		JSONArray nodesJSON = null;
		try{
			EntityManager em = emf.createEntityManager();
			List<Nodes> listNodes = em.createQuery(
			        "SELECT node FROM Nodes node WHERE node.isEnabled = :enabled".intern())
			        .setParameter("enabled".intern(), true)
			        .getResultList();
			
			nodesJSON = getNodesJSON( listNodes);
			em.close();
		}catch(Exception e){
			LOGGER.error("Error in reading nodes: ",e);
		}
		
		return nodesJSON;
	}
	
	@SuppressWarnings("unchecked")
	private JSONArray getNodesJSON(List<Nodes> listNodes) {
		JSONArray nodesJSON = new JSONArray();
		JSONParser parser = new JSONParser();
		
		if(listNodes != null && listNodes.size() > 0){
			for(Nodes node: listNodes){
				/*if(nodes.getBaseUrl().compareToIgnoreCase("use_ip_address") != 0 ) {
					availableServers.put(nodes.getAlias(), nodes.getBaseUrl());
				}else {
					availableServers.put(nodes.getAlias(), "http://" + nodes.getIpAddresses());
				}*/
				try {
					String nodeJSONStr = node.toJSONString();
					LOGGER.info("node json = {}", nodeJSONStr);
					JSONObject nodeJSON = (JSONObject) parser.parse(nodeJSONStr);
					nodesJSON.add(nodeJSON);
				} catch (ParseException e) {
					LOGGER.error("Error in parsing nodes: ",e);
				}
			}
		}
		return nodesJSON;
	}
}
