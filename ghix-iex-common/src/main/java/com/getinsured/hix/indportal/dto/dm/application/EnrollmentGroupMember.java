package com.getinsured.hix.indportal.dto.dm.application;


public class EnrollmentGroupMember {
 private String personTrackingNumber;
 private Boolean subscriberIndicator;


 // Getter Methods 

 public String getPersonTrackingNumber() {
  return personTrackingNumber;
 }

 public Boolean getSubscriberIndicator() {
  return subscriberIndicator;
 }

 // Setter Methods 

 public void setPersonTrackingNumber(String personTrackingNumber) {
  this.personTrackingNumber = personTrackingNumber;
 }

 public void setSubscriberIndicator(Boolean subscriberIndicator) {
  this.subscriberIndicator = subscriberIndicator;
 }
}