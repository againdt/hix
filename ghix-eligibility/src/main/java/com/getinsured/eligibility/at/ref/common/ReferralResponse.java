package com.getinsured.eligibility.at.ref.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chopra_s
 * 
 */
public class ReferralResponse {
	public static final int NO_ERROR = 0;
	public static final int ERROR_CREATE = 1;
	public static final int ERROR_DEMOGRAPHIC = 2;
	public static final int ERROR_ADMIN_UPDATE = 3;
	public static final int ERROR_LINK_CMR = 4;
	public static final int ERROR_AT_ADAPTER = 5;
	public static final int ERROR_LCE_REFERRAL_DECISION = 6;
	public static final int ERROR_RENEWAL_DESCISION = 7;
	public static final int ERROR_ELG_DECISION_PROCESSOR = 8;
	public static final int ERROR_LCE_DEMO_HANDLER = 9;
	public static final int ERROR_LCE_DEMO_DOB_HANDLER = 10;
	public static final int ERROR_LCE_ELG_LOST_HANDLER = 11;
	public static final int ERROR_LCE_APTC_ONLY_HANDLER = 12;
	public static final int ERROR_LCE_CITIZENSHIP_HANDLER = 13;
	public static final int ERROR_LCE_RELATIONSHIP_HANDLER = 14;
	public static final int ERROR_LCE_ALL_CHANGES_HANDLER = 15;
	public static final int ERROR_LCE_NO_CHANGES_HANDLER = 16;
	public static final int ERROR_NF_DETERMINATION = 17;
	public static final int ERROR_NF_CONVERSION = 18;
	public static final int ERROR_NF_APTC_UPDATE = 19;
	public static final int ERROR_REN_NF_DETERMINATION = 20;
	public static final int ERROR_REN_NF_CONVERSION = 21;
	public static final int ERROR_STEP_ROUTER = 22;
	public static final int ERROR_MES_PROCESSING = 23;
	public static final String REFERRAL_ERROR = "REFERRAL_ERROR";
	public static final String REFERRAL_SUCCESS = "REFERRAL_SUCCESS";
	public static final String REFERRAL_SUCCESS_AUTO_LINK = "REFERRAL_SUCCESS_AUTO_LINK";
	public static final String SSAP_APPLICATION_ID = "ssapApplicationId";
	public static final String AT_PROCESS = "AT_PROCESS";
	public static final String MES_BATCH_PROCESS = "MES_BATCH_PROCESS";

	private int errorCode = ERROR_CREATE;
	private String message;
	private String headermessage = REFERRAL_ERROR;

	public String getHeadermessage() {
		return headermessage;
	}

	public void setHeadermessage(String headermessage) {
		this.headermessage = headermessage;
	}

	private Map<String, Object> data;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, Object> getData() {
		if (data == null) {
			data = new HashMap<String, Object>();
		}
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

}
