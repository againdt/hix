package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.GetCountryOfIssuanceListResponseType;


/**
 * This class is the wrapper for com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.model.GetCountryOfIssuanceListResponseType
 * 
 * HIX-34220
 * 
 * @author Nikhil Talreja
 * @since 09-Apr-2014
 *
 */
public class GetCountryOfIssuanceListResponseTypeWrapper implements JSONAware {
	
	private ResponseMetadataWrapper responseMetadata;
    private GetCountryOfIssuanceListResponseSetTypeWrapper getCountryOfIssuanceListResponseSet;
	
    private static final String RESPONSE_METADATA_KEY = "ResponseMetadata";
	private static final String COUNTRY_LIST_KEY = "GetCountryOfIssuanceListResponseSet";
	
	public GetCountryOfIssuanceListResponseTypeWrapper(GetCountryOfIssuanceListResponseType getCountryOfIssuanceListResponseType){
		
		if(getCountryOfIssuanceListResponseType == null){
			return;
		}
		
		this.responseMetadata = new ResponseMetadataWrapper(getCountryOfIssuanceListResponseType.getResponseMetadata());
		this.getCountryOfIssuanceListResponseSet = new GetCountryOfIssuanceListResponseSetTypeWrapper(getCountryOfIssuanceListResponseType.getGetCountryOfIssuanceListResponseSet());
		
	}
	
	
	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public GetCountryOfIssuanceListResponseSetTypeWrapper getGetCountryOfIssuanceListResponseSet() {
		return getCountryOfIssuanceListResponseSet;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();

		obj.put(RESPONSE_METADATA_KEY, this.responseMetadata);
		obj.put(COUNTRY_LIST_KEY, this.getCountryOfIssuanceListResponseSet);
		
		return obj.toJSONString();
	}
}
