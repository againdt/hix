<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
                    <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                    <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="label.issuerTitle"/></a></li>
                    <li><spring:message  code="pgheader.manageIssuers"/></li>
                </ul><!--page-breadcrumb ends-->
		<h1><a name="skip"></a><spring:message  code='pgheader.issuers'/>&nbsp;&nbsp;<small>${resultSize}&nbsp;<spring:message  code="pheader.totalIssuers"/></small></h1>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    	<div class="header">
			<h4>Refine Results</h4>
		</div>
	             <form class="form-vertical gutter10 lightgray" id="frmbrokerreg" name="frmbrokerreg" action="<c:url value="/admin/manageissuer" />" method="POST">
						<df:csrfToken/>
						<input type="hidden" id="sortBy" name="sortBy" value="lastUpdateTimestamp">
						<input type="hidden" id="sortOrder" name="sortOrder" value="DESC" >
						<input type="hidden" id="pageNumber" name="pageNumber" value="1">
						<input type="hidden" id="changeOrder" name="changeOrder" >
						<div class="control-group">
							<label for="issuerName" class="required control-label"><spring:message  code='label.name'/> <!-- <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --></label>
								<div class="controls">
									<input type="text" name="issuerName" id="issuerName" value="${issuerName}" class="span12">
									<div id="firstName_error" class=""></div>
								</div> <!-- end of controls-->
						</div>
						<!-- end of control-group -->
						<div class="control-group">
							<label for="status" class="control-label"><spring:message  code='label.status.plan'/></label>
							<div class="controls">
								<select id="status" class="span12" name="status">
									<option value="">Select Status</option>
									 <c:forEach var="issuerCertStatus" items="${issuerCertStatusList}">
								    	<option  value="${issuerCertStatus.key}" <c:if test="${status == issuerCertStatus.key}" >selected</c:if>>${issuerCertStatus.value}</option>
									 </c:forEach>									
								</select>
							</div> <!-- end of controls-->
						</div>
						<!-- end of control-group --> 
						<div class="txt-center">
 						<input type="submit" class="btn" value="<spring:message  code='label.go'/>"  title="<spring:message  code='label.go'/>">
 						</div>
				 </form>
				 <c:if test="${exchangeType != 'PHIX'}">
                 <a href="<c:url value="/admin/addissuer"/>" class="btn btn-primary span11" id="addNewIssuer" name="addNewIssuer"><spring:message  code='label.addNewIssuer'/></a>
                 </c:if>
	</div>
    	<c:set var="newSortOrder" value="${sortOrder}"></c:set>
        <div class="span9" id="rightpanel">
				<c:choose>
						<c:when test="${fn:length(issuerObj) > 0}">
							<table class="table table-striped">
								<thead>
									<tr class="header">
										<spring:message code="label.name" var="issuerName"/>
										<spring:message code="label.contractBegins" var="contractBegin"/>
										<spring:message code="label.contractEnd" var="contractEnd"/>
										<spring:message code="label.lastUpdated" var="lastUpdated"/>
										<spring:message code="label.status.plan" var="statusValue"/>
								 		<th class="sortable" scope="col" style="width: 150px;"><dl:sort title="${issuerName}" sortBy="name" customFunctionName="traverse" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th class="sortable" scope="col" style="width: 140px;"><dl:sort title="${contractBegin}" sortBy="certifiedOn" customFunctionName="traverse" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th class="sortable" scope="col" style="width: 130px;"><dl:sort title="${contractEnd}" sortBy="decertifiedOn" customFunctionName="traverse" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th class="sortable" scope="col" style="width: 120px;"><dl:sort title="${lastUpdated}" sortBy="lastUpdateTimestamp" customFunctionName="traverse" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<th class="sortable" scope="col" style="width: 75px;"><dl:sort title="${statusValue}" sortBy="certificationStatus" customFunctionName="traverse" sortOrder="${newSortOrder}"></dl:sort></th>
								     	<c:if test="${exchangeType != 'PHIX'}">
								     		<th class="sortable txt-center" scope="col"><span class="aria-hidden">Actions</span><i class="icon-cog"></i><i class="caret"></i></th>
								     	</c:if>
									</tr>
								</thead>
								<c:forEach items="${issuerObj}" var="issuerObj">
								<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
									<tr>
										<td id="name_${issuerObj.id}"><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>">${issuerObj.name}</a></td>
										<td align="center"><fmt:formatDate type="both" pattern="MMM dd, yyyy" value="${issuerObj.certifiedOn}" timeZone="${timezone}" /></td>
										<td align="center"><fmt:formatDate type="both" pattern="MMM dd, yyyy" value="${issuerObj.decertifiedOn}" timeZone="${timezone}" /></td>
										<td align="center"> <fmt:formatDate type="both" pattern="MMM dd, yyyy" value="${issuerObj.lastUpdateTimestamp}" timeZone="${timezone}" /></td>
										<td align="center">
											<c:forEach var="issuerCertStatus" items="${issuerCertStatusList}">
										  		<c:if test="${issuerObj.certificationStatus == issuerCertStatus.key}" >${issuerCertStatus.value}</c:if>
										  	</c:forEach>
										</td>
										<c:if test="${exchangeType != 'PHIX'}">
										<td>
										  	<div class="dropdown txt-center">
			                                    <a class="dropdown-toggle action-btn" data-toggle="dropdown" href="#" alt="dropdown toggle"><i class="icon-cog"></i><i class="caret"></i><span class="hide">Actions</span></a>
			                                    <ul class="dropdown-menu">
			                                        <li><a href="<c:url value="/admin/issuer/details/edit/${encIssuerId}"/>"><i class="icon-pencil"></i><spring:message  code='label.edit'/></a></li>
			                                        <li><a href="<c:url value="/admin/issuer/certification/status/edit/${encIssuerId}"/>"><i class="icon-refresh"></i><spring:message  code='label.updateStatus'/></a></li>
			                                    </ul>
			                                </div>
										</td>
										</c:if>
									</tr>
								
								</c:forEach>
							</table>
							<div class="center"><dl:paginate customFunctionName="traverse" resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/></div>
						</c:when>
					<c:otherwise>
						<div class="alert alert-info"><spring:message  code='issuer.resultList'/></div>
					</c:otherwise>
				</c:choose>
				</div>
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->

<div id="dialog-modal" style="display: none;">
  <%-- <p><spring:message  code='label.name'/>: ${sessionIssuerName}<br>
  Delegation Code is: ${sessionDelegationCode}
  </p> --%>
  <p>
  	<c:if test="${not empty sessionIssuerName}">
    	<spring:message  code='label.name'/>: ${sessionIssuerName} <br/>
	</c:if>
	<c:if test="${not empty sessionDelegationCode}">
    	Delegation Code is: ${sessionDelegationCode} <br/>
	</c:if>
	<c:if test="${not empty generateactivationlinkInfo}">
    	${generateactivationlinkInfo}
	</c:if>
	<c:if test="${not empty isIssuerCreated}">
    	${isIssuerCreated}
	</c:if>
	</p>
</div>
<div id="dialog-modal_error" style="display: none;">
<p>Message: ${sessionErrorInfo}</p>
</div>
<script type="text/javascript">
$(document).ready(function()
{
	/* alert("Delegation code is: "+'${sessionDelegationCode}'); */
	if('${sessionDelegationCode}' != null && '${sessionDelegationCode}' != '' && '${sessionIssuerName}' != null && '${sessionIssuerName}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		     modal: true,
			 title: "<spring:message  code='label.ahbxInfo'/>",
			 buttons: {
			   Ok: function() {
			    $( this ).dialog( "close" );
			   }
			   }
		    });
	}
	else if('${sessionErrorInfo}' != null && '${sessionErrorInfo}' != '')
	{
		$( "#dialog-modal_error" ).dialog({
		      height: 140,
		      modal: true,
		      title: "<spring:message  code='label.ahbxInfo'/>",
		      buttons: {
					Ok: function() {
					$( this ).dialog( "close" );
					}
					}
		    });			
	}
	else if('${generateactivationlinkInfo}' != null && '${generateactivationlinkInfo}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		  		modal: true,
		    	title: "<spring:message  code='label.registrationStatus'/>",
		    	buttons: {
		    	Ok: function() {
		    		$( this ).dialog( "close" );
		    	}
		    	}
		    });
	}
	else if('${isIssuerCreated}' != null && '${isIssuerCreated}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		  		modal: true,
		    	title: "<spring:message  code='label.registrationStatus'/>",
		    	buttons: {
		    	Ok: function() {
		    		$( this ).dialog( "close" );
		    	}
		    	}
		    });
	}
	
	$('body').click(function(){
		$('.popover').fadeOut();
	});
});

function traverse(url){
	var queryString = {};
	url.replace(
		    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
		    function($0, $1, $2, $3) { queryString[$1] = $3; }
		);
	 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
	 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
	 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
	 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
	
	$("#frmbrokerreg").submit();
}
</script>
