package com.getinsured.hix.batch.ssap.renewal.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.redetermination.service.RenewalApplicantService;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.hix.batch.ssap.renewal.dto.GenerateNoticeEnum;
import com.getinsured.hix.batch.ssap.renewal.dto.RenewalApplicationDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ObjectFactory;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.RenewalApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.timeshift.util.TSDate;

@Service
public class RenewalNotificationServiceImpl implements RenewalNotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RenewalNotificationServiceImpl.class);

	@Autowired
	private RenewalApplicationRepository renewalApplicationRepository;

	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;

	@Autowired
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private PreferencesService preferencesService;

	@Autowired
	private NoticeService noticeService;
	
	@Autowired
	private RenewalApplicantService renewalApplicantService;
	
	@Autowired
	private LookupService lookupService;

	ObjectFactory factory = new ObjectFactory();
	Integer existingMedicalEnrollmentId = null;
	SimpleDateFormat format = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
	public static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	private static final String CURRENT_YEAR = "currentYear";
	private static final String CURRENT_COVERAGE_YEAR = "currentCoverageYear";
	private static final String PREVIOUS_COVERAGE_YEAR = "previousCoverageYear";
	private static final String ANNONYMOUS_SHOOPING_START_DATE = "annonymousShoopingStartDate";
	private static final String EFFECTIVE_DATE = "effectiveDate";
	private static final String MMMM_D_YYYY = "MMMM d, yyyy";
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	private static final String STATE_NAME = "exgStateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String EXCHANGE_NAME = "exgName";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_ADDRESS_TWO = "exchangeAddress2";
	private static final String EXCHANGE_CITY_NAME = "exgCityName";
	private static final String EXCHANGE_ZIP = "zip";
	private static final String PRIMARY_APPLICANT_NAME = "primaryApplicantName";
	private static final String EXCHANGE_ADDRESS_EMAIL = "exchangeAddressEmail";
	private static final String USER_NAME = "userName";
	private static final String SPECIAL_ENROLLMENT_END_DATE = "SpecialEnrollmentEndDate";
	private static final String ES = "ES";
	private static final String LOCAL_ES = "es";
	private static final String DATE_FORMAT = "MMMM dd, YYYY";
	private static final String SPANISH_DATE = "spanishDate";
	private static final String ENGLISH_DATE = "englishDate";
	private static final String TODAYS_DATE = "todaysDate";
	private static final String HEALTH_STATUS = "healthStatus";
	private static final String DENTAL_STATUS = "dentalStatus";
	private static final String OE_START_DATE = "oep_start_date";
	private static final String OE_END_DATE = "oep_end_date";
	private static final String RENEWAL_YEAR = "renewal_year";
	private static final String PLANTYPE_HEALTH = "Health";
	private static final String PLANTYPE_DENTAL = "Dental";
	private static final String HEALTH_FALL_OUT_REASON_CODE_MAP = "healthFallOutReasonCodeMap";
	private static final String DENTAL_FALL_OUT_REASON_CODE_MAP = "dentalFallOutReasonCodeMap";
	private static final String HEALTH_GROUP_REASON_CODE_MAP = "healthGroupReasonCodeMap";
	private static final String GENERATE_NOTICE_FOR = "GENERATE_NOTICE_FOR";
	private static final String CASE_NUMBER = "caseNumber";
	private static final String HEALTH_REASON_CODES_IN_A_LIST = "healthFalloutCodes";
	private static final String DENTAL_REASON_CODES_IN_A_LIST = "dentalFalloutCodes";
	
	private static final String SUCCESS_AUTO_RENEWAL_NOTICE = "RenewalNotification";
	private static final String SUCCESS_AUTO_RENEWAL_NOTICE_EMAIL_CLASS = "RenewalNotification";
	
	private static final String SUCCESS_AUTO_RENEWAL_NOTICE_MN = "SuccessAutoRenewalNotice";
	private static final String SUCCESS_AUTO_RENEWAL_NOTICE_EMAIL_CLASS_MN = "Renewal Notice";
	private static final String FAILED_AUTO_RENEWAL_NOTICE_MN = "FailedAutoRenewalNotice";
	private static final String FAILED_AUTO_RENEWAL_NOTICE_EMAIL_CLASS_MN = "Renewals Notice: Plan not available";
	
	private static final String STATE_CODE = "MN";
	
	@Override
	public void processRenewalApplicationData(GenerateNoticeEnum generateNoticeFor, Long batchSize, String stateCode) {
		switch (generateNoticeFor) {
			case SUCCESS:
				callSuccessNotice(generateNoticeFor, batchSize, stateCode);
				break;
			case FAILURE:
				callFailureNotice(generateNoticeFor, batchSize, stateCode);
				break;
			case BOTH:
				callSuccessNotice(generateNoticeFor, batchSize, stateCode);
				callFailureNotice(generateNoticeFor, batchSize, stateCode);
				break;
			case ALL:
				callSuccessNotice(generateNoticeFor, batchSize, stateCode);
				break;
			default:
				break;
		}
	}
	
	private void callSuccessNotice(GenerateNoticeEnum generateNoticeFor, Long batchSize, String stateCode) {
		
		List<RenewalStatus> healthRenewalStatus;
		List<RenewalStatus> dentalRenewalStatus;
		List<RenewalApplication> list;
		
		if(STATE_CODE.equalsIgnoreCase(stateCode)) {
			healthRenewalStatus = new ArrayList<>(Arrays.asList(RenewalStatus.RENEWED, RenewalStatus.PARTIAL_RENEWED));
			dentalRenewalStatus = new ArrayList<>(Arrays.asList(RenewalStatus.RENEWED));
			list = renewalApplicationRepository.getRenewalApplicationList(healthRenewalStatus, dentalRenewalStatus, new PageRequest(0, batchSize.intValue()));
		}
		else {
			healthRenewalStatus = new ArrayList<>(Arrays.asList(RenewalStatus.RENEWED, RenewalStatus.MANUAL_ENROLMENT, RenewalStatus.PARTIAL_RENEWED, RenewalStatus.NOT_TO_CONSIDER));
			dentalRenewalStatus = new ArrayList<>(Arrays.asList(RenewalStatus.RENEWED, RenewalStatus.MANUAL_ENROLMENT, RenewalStatus.PARTIAL_RENEWED, RenewalStatus.NOT_TO_CONSIDER));
			list = renewalApplicationRepository.getRenewalApplicationListExceptMN(healthRenewalStatus, dentalRenewalStatus, new PageRequest(0, batchSize.intValue()));
		}
		
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Ids of RenewalApplication to be processed are below for SuccessNotice ");
			StringBuffer ids = new StringBuffer();
			list.forEach((s) -> ids.append(s.getId()).append(","));
			LOGGER.info(" " + ids.toString());
		}
		
		processRenewedAndManualApplicationList(generateNoticeFor, list, stateCode, GenerateNoticeEnum.SUCCESS);
	}
	
	private void callFailureNotice(GenerateNoticeEnum generateNoticeFor, Long batchSize, String stateCode) {
		List<RenewalStatus> healthRenewalStatus = new ArrayList<>(Arrays.asList(RenewalStatus.MANUAL_ENROLMENT));
		List<RenewalStatus> dentalRenewalStatus = new ArrayList<>(Arrays.asList(RenewalStatus.MANUAL_ENROLMENT));
		
		List<RenewalApplication> list = renewalApplicationRepository.getRenewalApplicationListForFailureNotice(healthRenewalStatus, dentalRenewalStatus, new PageRequest(0, batchSize.intValue()));
		
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("Ids of RenewalApplication to be processed are below for FailureNotice ");
			StringBuffer ids = new StringBuffer();
			list.forEach((s) -> ids.append(s.getId()).append(","));
			LOGGER.info(" " + ids.toString());
		}
		
		processRenewedAndManualApplicationList(generateNoticeFor, list, stateCode, GenerateNoticeEnum.FAILURE);
	}
	
	private void processRenewedAndManualApplicationList(GenerateNoticeEnum generateNoticeFor, List<RenewalApplication> list, String stateCode, GenerateNoticeEnum processRequest) {
		for (RenewalApplication renewalApplication : list) {
		  try {
			RenewalApplicationDTO renewalApplicationDTO = new RenewalApplicationDTO();
			renewalApplicationDTO.setGenerateNoticeFor(generateNoticeFor);
			
			RenewalStatus healthStatus = renewalApplication.getHealthRenewalStatus();
			RenewalStatus dentalStatus = renewalApplication.getDentalRenewalStatus();
			if(healthStatus != null)
				renewalApplicationDTO.setHealthRenewalStatus(healthStatus.getStatus());
			if(dentalStatus != null)
				renewalApplicationDTO.setDentalRenewalStatus(dentalStatus.getStatus());
			
			String oeStartDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE);
			String oeEndDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE);
			String renewalYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
			renewalApplicationDTO.setOeStartDate(oeStartDate);
			renewalApplicationDTO.setOeEndDate(oeEndDate);
			renewalApplicationDTO.setRenewalYear(renewalYear);
			
			Map<Long, List<String>> healthFallOutReasonCodesByMember = renewalApplicantService
					.getEnrollmentsWithMemberAndFalloutReasonCodes(renewalApplication.getSsapApplicationId(), PLANTYPE_HEALTH);
			Map<Long, List<String>> dentalFallOutReasonCodesByMember = renewalApplicantService
					.getEnrollmentsWithMemberAndFalloutReasonCodes(renewalApplication.getSsapApplicationId(), PLANTYPE_DENTAL);
			Map<Long, String> healthGroupReasonCodes = renewalApplicantService.getEnrollmentsWithGroupReasonCodes(renewalApplication.getSsapApplicationId());
			renewalApplicationDTO.setHealthFallOutReasonCodesByMember(healthFallOutReasonCodesByMember);
			renewalApplicationDTO.setDentalFallOutReasonCodesByMember(dentalFallOutReasonCodesByMember);
			renewalApplicationDTO.setHealthGroupReasonCodes(healthGroupReasonCodes);
			
			SsapApplication ssapApplication = ssapApplicationRepository.findById(renewalApplication.getSsapApplicationId());
			renewalApplicationDTO.setCaseNumber(ssapApplication.getCaseNumber());
			
		    renewalApplicationDTO.prepareHealthReasonCodes();
		    renewalApplicationDTO.preapreDentalReasonCodes();
			
			if(renewalApplicationDTO.getHealthCodes().size() == 0 && renewalApplication.getHealthReasonCode() != null) {
				LookupValue lookupValue = lookupService.findLookupValuebyId(renewalApplication.getHealthReasonCode());
				if (lookupValue != null) {
					Set<String> healthCodes = new HashSet<String>();
					healthCodes.add(lookupValue.getLookupValueLabel());
					renewalApplicationDTO.setHealthCodes(healthCodes);
				}
			}
			
			if(renewalApplicationDTO.getDentalCodes().size() == 0 && renewalApplication.getDentalReasonCode() != null) {
				LookupValue lookupValue = lookupService.findLookupValuebyId(renewalApplication.getDentalReasonCode());
				if (lookupValue != null) {
					Set<String> dentalCodes = new HashSet<String>();
					dentalCodes.add(lookupValue.getLookupValueLabel());
					renewalApplicationDTO.setDentalCodes(dentalCodes);
				}
			}
			
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("TriggerNotification for ssapApplication id ::::: "+ssapApplication.getId());
			}
			
			Integer noticeId = triggerNotification(getHousehold(ssapApplication), renewalApplicationDTO, stateCode, processRequest);
			
			if (noticeId != null) {
				switch (processRequest) {
					case SUCCESS:
						renewalApplication.setNoticeId(noticeId);
						break;
					case FAILURE:
						renewalApplication.setFailureNoticeId(noticeId);
						break;
					default:
						break;
				}
				renewalApplication.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
				renewalApplicationRepository.save(renewalApplication);
			}
			
		  }catch (Exception ex) {
			LOGGER.error("Error while processing renewalApplication id : " + renewalApplication.getId() + " Error is : "+ex);
			ex.printStackTrace();
		  }
	   }
	}

	private Household getHousehold(SsapApplication currentApplication) throws GIException, ParseException {
		Household household = factory.createAutoRenewalRequestHousehold();
		household.setApplicationId(currentApplication.getId());
		household.setHouseHoldContact(getHouseHoldContact(currentApplication));
		household.setIndividual(getIndividual(currentApplication));
		return household;
	}

	private HouseHoldContact getHouseHoldContact(SsapApplication currentApplication) {
		HouseHoldContact houseHoldContact = factory.createAutoRenewalRequestHouseholdHouseHoldContact();
		SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder
				.transformFromJson(currentApplication.getApplicationData());
		for (TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {
			for (HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {
				if (houseHoldMember.getPersonId() == 1) {
					if (houseHoldMember.getName() != null) {
						houseHoldContact.setHouseHoldContactFirstName(
								nullCheckedValue(houseHoldMember.getName().getFirstName()));
						houseHoldContact
								.setHouseHoldContactLastName(nullCheckedValue(houseHoldMember.getName().getLastName()));
						houseHoldContact.setHouseHoldContactMiddleName(
								nullCheckedValue(houseHoldMember.getName().getMiddleName()));
						houseHoldContact
								.setHouseHoldContactSuffix(nullCheckedValue(houseHoldMember.getName().getSuffix()));
					}
				}
			}
		}
		return houseHoldContact;
	}

	private Individual getIndividual(SsapApplication currentApplication) throws GIException, ParseException {
		Individual individual = factory.createAutoRenewalRequestHouseholdIndividual();
		Date coverageDate = ssapCloneApplicationService.getCoverageStartDate(currentApplication);
		individual.setCoverageStartDate(getFormatedDate(coverageDate));
		if (currentApplication.getCmrHouseoldId() != null) {
			individual.setHouseholdCaseId(currentApplication.getCmrHouseoldId().longValue());
		}
		return individual;
	}

	private String nullCheckedValue(String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		}
		return value;
	}

	private String getFormatedDate(Date date) throws ParseException {
		return format.format(date.getTime());
	}

	private Integer triggerNotification(Household household, RenewalApplicationDTO renewalApplicationDTO, String stateCode, GenerateNoticeEnum processRequest) throws NoticeServiceException {
		Integer noticeId = null;
		try {
			if (STATE_CODE.equalsIgnoreCase(stateCode)) {
				switch (processRequest) {
					case SUCCESS:
						noticeId = sendNotification(SUCCESS_AUTO_RENEWAL_NOTICE_MN + "_", SUCCESS_AUTO_RENEWAL_NOTICE_EMAIL_CLASS_MN, 
								   "cmr/" + household.getIndividual().getHouseholdCaseId() + "/" + SUCCESS_AUTO_RENEWAL_NOTICE_MN,
								   getSepEndDate(), household, renewalApplicationDTO);
						break;
					case FAILURE:
						noticeId = sendNotification(FAILED_AUTO_RENEWAL_NOTICE_MN + "_", FAILED_AUTO_RENEWAL_NOTICE_EMAIL_CLASS_MN, 
								   "cmr/" + household.getIndividual().getHouseholdCaseId() + "/" + FAILED_AUTO_RENEWAL_NOTICE_MN,
								   getSepEndDate(), household, renewalApplicationDTO);
						break;
					default:
						break;
				}
			}
			else {
				noticeId = sendNotification(SUCCESS_AUTO_RENEWAL_NOTICE + "_", SUCCESS_AUTO_RENEWAL_NOTICE_EMAIL_CLASS, 
						   "cmr/" + household.getIndividual().getHouseholdCaseId() + "/" + SUCCESS_AUTO_RENEWAL_NOTICE,
						   getSepEndDate(), household, renewalApplicationDTO);
			}
		} catch (Exception e) {
			LOGGER.error("Error while triggerNotification : ", e);
		}
		return noticeId;
	}

	private Integer sendNotification(String fileName, String templateName, String relativePath, String sepEndDate,
			Household household, RenewalApplicationDTO renewalApplicationDTO) throws Exception {
		LOGGER.info("Sending Notification : Starts");
		Integer noticeId = null;
		Map<String, Object> individualTemplateData = new HashMap<String, Object>();
		List<String> sendToEmailList = new ArrayList<String>();

		DateFormat dateFormat = null;
		DateFormat dateFormatForSpanish = null;

		try {
			individualTemplateData.put(LifeChangeEventConstant.NOTIFICATION_SEND_DATE, new Date());
			individualTemplateData.put(TemplateTokens.HOST, GhixEndPoints.GHIXWEB_SERVICE_URL);
			individualTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			individualTemplateData.put(STATE_NAME,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
			individualTemplateData.put(COUNTRY_NAME,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
			individualTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
			individualTemplateData.put(EXCHANGE_PHONE,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			individualTemplateData.put(EXCHANGE_NAME,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

			individualTemplateData.put(EXCHANGE_ADDRESS_ONE, DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			individualTemplateData.put(EXCHANGE_ADDRESS_TWO, DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_2));
			individualTemplateData.put(EXCHANGE_CITY_NAME,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
			individualTemplateData.put(EXCHANGE_ZIP,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));

			// initialize the date formatter
			dateFormat = new SimpleDateFormat(DATE_FORMAT);
			dateFormatForSpanish = new SimpleDateFormat(DATE_FORMAT, new Locale(LOCAL_ES, ES));
			// set English and Spanish date into template data
			Date todaysDate = new Date();
			individualTemplateData.put(ENGLISH_DATE, dateFormat.format(todaysDate));
			individualTemplateData.put(TODAYS_DATE, dateFormat.format(todaysDate));
			individualTemplateData.put(SPANISH_DATE, dateFormatForSpanish.format(todaysDate));

			// get name of primary applicant
			String name = household.getHouseHoldContact().getHouseHoldContactFirstName() + " "
					+ household.getHouseHoldContact().getHouseHoldContactLastName();
			individualTemplateData.put(PRIMARY_APPLICANT_NAME, name);
			individualTemplateData.put(USER_NAME, name);
			individualTemplateData.put(EXCHANGE_ADDRESS_EMAIL, DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			individualTemplateData.put(SPECIAL_ENROLLMENT_END_DATE, sepEndDate);
			individualTemplateData.put(EFFECTIVE_DATE,
					getCoverageDateForNotice(household.getIndividual().getCoverageStartDate()));
			individualTemplateData.put(ANNONYMOUS_SHOOPING_START_DATE, getShoppingDate());
			individualTemplateData.put(PREVIOUS_COVERAGE_YEAR, DynamicPropertiesUtil
					.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
			individualTemplateData.put(CURRENT_COVERAGE_YEAR, DynamicPropertiesUtil
					.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR);
			individualTemplateData.put(CURRENT_YEAR, String.valueOf(year));
			individualTemplateData.put("exchangeFAX",
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			individualTemplateData.put("ssapApplicationId", household.getApplicationId());

			// set renewal data
			individualTemplateData.put(HEALTH_STATUS, renewalApplicationDTO.getHealthRenewalStatus());
			individualTemplateData.put(DENTAL_STATUS, renewalApplicationDTO.getDentalRenewalStatus());
			individualTemplateData.put(OE_START_DATE, renewalApplicationDTO.getOeStartDate());
			individualTemplateData.put(OE_END_DATE, renewalApplicationDTO.getOeEndDate());
			individualTemplateData.put(RENEWAL_YEAR, renewalApplicationDTO.getRenewalYear());
			
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("HealthReasonCodesMaps   :::::::::::: ");
				for (Map.Entry<Long, List<String>> obj : renewalApplicationDTO.getHealthFallOutReasonCodesByMember().entrySet()) {
					Long key = obj.getKey();
					List<String> values = obj.getValue();
					LOGGER.info("enrollmentId        :::::::::::: " + key);
					for (String code : values) {
						LOGGER.info("memberId - code :::::::::::: " + code);
					}
				}
				LOGGER.info("DentalReasonCodesMaps   :::::::::::: ");
				for (Map.Entry<Long, List<String>> obj : renewalApplicationDTO.getDentalFallOutReasonCodesByMember().entrySet()) {
					Long key = obj.getKey();
					List<String> values = obj.getValue();
					LOGGER.info("enrollmentId        :::::::::::: " + key);
					for (String code : values) {
						LOGGER.info("memberId - code :::::::::::: " + code);
					}
				}
			}
			
			individualTemplateData.put(HEALTH_FALL_OUT_REASON_CODE_MAP, renewalApplicationDTO.getHealthFallOutReasonCodesByMember());
			individualTemplateData.put(DENTAL_FALL_OUT_REASON_CODE_MAP, renewalApplicationDTO.getDentalFallOutReasonCodesByMember());
			individualTemplateData.put(HEALTH_GROUP_REASON_CODE_MAP, renewalApplicationDTO.getHealthGroupReasonCodes());
			individualTemplateData.put(GENERATE_NOTICE_FOR, renewalApplicationDTO.getGenerateNoticeFor().toString());
			individualTemplateData.put(CASE_NUMBER, renewalApplicationDTO.getCaseNumber());
				
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("HealthReasonCodesInHTMLFormat :::::::::::: " + renewalApplicationDTO.constructCodesInHTMLFormat(renewalApplicationDTO.getHealthCodes()));
				LOGGER.info("DentalReasonCodesInHTMLFormat ::::::::::: " + renewalApplicationDTO.constructCodesInHTMLFormat(renewalApplicationDTO.getDentalCodes()));
			}
			
			individualTemplateData.put(HEALTH_REASON_CODES_IN_A_LIST, renewalApplicationDTO.constructCodesInHTMLFormat(renewalApplicationDTO.getHealthCodes()));
			individualTemplateData.put(DENTAL_REASON_CODES_IN_A_LIST, renewalApplicationDTO.constructCodesInHTMLFormat(renewalApplicationDTO.getDentalCodes()));
			
			// Populating Household details from Preferences
			Location location = new Location();
			PreferencesDTO preferencesDTO = preferencesService
					.getPreferences((int) household.getIndividual().getHouseholdCaseId(), false);
			location = getLocationFromDTO(preferencesDTO.getLocationDto());
			String emailId = preferencesDTO.getEmailAddress();
			sendToEmailList.add(emailId);

			fileName = fileName + System.currentTimeMillis() + LifeChangeEventConstant.PDF;
			LOGGER.info("Sending " + templateName + "for household " + household.getIndividual().getHouseholdCaseId());
			Notice notice = noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, individualTemplateData,
					relativePath, fileName, LifeChangeEventConstant.INDIVIDUAL,
					household.getIndividual().getHouseholdCaseId(), sendToEmailList,
					individualTemplateData.get(EXCHANGE_FULL_NAME) + "", name, location,
					preferencesDTO.getPrefCommunication());
			LOGGER.info("Notice : " + notice.getId());
			noticeId = notice.getId();
		} catch (Exception exception) {
			LOGGER.error("Error sending notification for " + templateName + " for household "
					+ household.getIndividual().getHouseholdCaseId(), exception);
			exception.printStackTrace();
		}

		LOGGER.info("Sending Notification : Ends");
		return noticeId;
	}

	private String getCoverageDateForNotice(String date) throws ParseException {
		DateFormat dateFormateForParse = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
		Date d = dateFormateForParse.parse(date);
		DateFormat df = new SimpleDateFormat(MMMM_D_YYYY);
		return df.format(d);
	}

	private String getShoppingDate() {
		int year = Integer.parseInt(
				DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
		DateFormat df = new SimpleDateFormat(MMMM_D_YYYY);
		return df.format(getDate(1, Calendar.OCTOBER, year));
	}

	private String getSepEndDate() {
		int year = Integer.parseInt(
				DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR));
		DateFormat df = new SimpleDateFormat(MMMM_D_YYYY);
		return df.format(getDate(15, Calendar.DECEMBER, year));
	}

	private Date getDate(int day, int month, int year) {
		Calendar date = Calendar.getInstance();
		date.set(Calendar.DAY_OF_MONTH, day);
		date.set(Calendar.YEAR, year);
		date.set(Calendar.MONTH, month);
		return date.getTime();

	}

	private Location getLocationFromDTO(LocationDTO locationDto) {
		if (locationDto == null) {
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}
}