<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>
<%@ page import="java.util.HashMap" %>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<script type="text/javascript" src="<c:url value="/resources/js/phoneUtils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/preserveImgAspectRatio.js" />"></script>

<!-- <meta http-equiv="Cache-Control" content="max-age=0" />
<meta http-equiv="Cache-Control" content="no-cache,no-store,must-revalidate" />
<META HTTP-EQUIV="Expires" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
<meta http-equiv="Pragma" content="no-cache" /> -->
<%
Broker mybroker = (Broker)request.getAttribute("broker");
long isProfileExists = 0;
if(mybroker != null)
{
 isProfileExists = mybroker.getId();

}
%>


<div class="gutter10">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a href="<c:url value="/broker/viewcertificationinformation"/>"><spring:message code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<div class="row-fluid">
		<h1 id="skip">${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
	</div>
	<div class="row-fluid broker-panel">
			<!-- #sidebar -->
		       <jsp:include page="brokerLeftNavigationMenu.jsp" />
		    <!-- #sidebar ENDS -->
			<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9" id="rightpanel">


				<div class="header"><h4 class="pull-left"><spring:message code="label.profile"/></h4>
				<c:if test="${( (broker.certificationStatus == 'Certified') || (broker.certificationStatus == 'Incomplete') ) and (empty broker.agencyId) }">
					<%-- <a class="btn btn-small pull-right" href="<c:url value="/broker/editprofile"/>"><spring:message  code="label.brkEdit"/></a> --%>
                    <button id="aid-editPage" class="btn btn-small pull-right" type="button"><spring:message code="label.brkEdit"/></button>
				</c:if>
				<c:if test="${ (isAgencyManager) and (broker.certificationStatus == 'Certified')    }">
					<jsp:useBean id="moduleParamsMap" class="java.util.HashMap"/>
					<c:set target="${moduleParamsMap}" property="brokerId" value="${broker.id}" />
					<surl:secureUrl paramMap="${moduleParamsMap}" var="ecyptedModuleParamsMap"/>
					<%-- <a class="btn btn-small pull-right" href="<c:url value="/broker/editprofile?ref=${ecyptedModuleParamsMap}"/>"><spring:message  code="label.brkEdit"/></a> --%>
                    <button id="aid-editMap" class="btn btn-small pull-right" type="button"><spring:message code="label.brkEdit"/></button>
				</c:if>
				</div>
				<div class="">
				<form class="form-vertical" id="frmviewbrokerprofile"
			name="frmviewbrokerprofile" action="viewprofile" method="POST">
				<div class="row-wrapper">
					<p class="gutter10"><spring:message  code="label.brkReviewEditText"/></p>
					<div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
						<table class="table table-border-none table-res" role="presentation">
							<tbody>
							<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
								<tr>
									<td class="txt-right"><spring:url value="/broker/photo/${encBrokerId}"
											var="photoUrl" /> <img id="brokerPhoto" src="${photoUrl}" alt="Agent thumbnail photo"
										class="brokerprofilephoto thumbnail" />
									</td>
									<td>
										<h4>${broker.user.firstName}&nbsp;${broker.user.lastName}</h4>
										<p>
											${broker.location.address1}
											<c:if
												test="${broker.location.address2!=null && broker.location.address2!=\"\"}">
												<br>${broker.location.address2}
										</c:if>
											<br> ${broker.location.city}, ${broker.location.state}
											${broker.location.zip} <br/>
										</p>
									</td>
								</tr>

								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message code="label.brkPhoneNumber"/></td>
											<c:if test="${phone1 != '' && phone1 != null}">
											<td><strong>(${phone1}) ${phone2} ${phone3}</strong></td>
											</c:if>
										</tr>
									</c:when>
									<c:otherwise>
								<tr>
									<td class="txt-right"><spring:message  code="label.brkPhoneNumber"/></td>
									<td><strong id="sPhoneNumber">(${phone1}) ${phone2}-${phone3} </strong></td>
								</tr>
									</c:otherwise>
								</c:choose>

								<tr>
									<td class="txt-right"><spring:message  code="label.brkpublicEmail"/></td>
									<td><strong>${broker.yourPublicEmail}</strong></td>
								</tr>

								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message code="label.brkClientServed"/></td>
											<td><strong>${broker.clientsServed}</strong></td>
										</tr>
									</c:when>
									<c:otherwise>
								<c:if test="${broker.clientsServed != '' && broker.clientsServed != null}">
								<tr>
									<td class="txt-right"><spring:message  code="label.brkAreaExpertise"/></td>
									<td><strong>${broker.clientsServed}</strong></td>
								</tr>
								</c:if>
									</c:otherwise>
								</c:choose>

								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message code="label.brkLanguageSpoken"/></td>
											<td><strong>${broker.languagesSpoken}</strong></td>
										</tr>
									</c:when>
									<c:otherwise>
								<c:if test="${broker.languagesSpoken != '' && broker.languagesSpoken != null}">
								<tr>
									<td class="txt-right"><spring:message  code="label.brkLanguageSpoken"/></td>
									<td><strong>${broker.languagesSpoken}</strong></td>
								</tr>
								</c:if>
									</c:otherwise>
								</c:choose>

								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message code="label.brkProductExpertises"/></td>
											<td><strong>${broker.productExpertise}</strong></td>
										</tr>
									</c:when>
									<c:otherwise>
								<c:if test="${broker.productExpertise != '' && broker.productExpertise != null}">
									<tr>
										<td class="txt-right"><spring:message  code="label.brkProductExpertises"/></td>
										<td><strong class="comma">${broker.productExpertise}</strong></td>
									</tr>
								</c:if>
									</c:otherwise>
								</c:choose>

								<c:choose>
									<c:when test="${ca_statecode}">
										<tr>
											<td class="txt-right"><spring:message code="label.brkWebsite"/></td>
											<td><strong>${broker.yourWebSite}</strong></td>
										</tr>
									</c:when>
									<c:otherwise>
								<c:if test="${broker.yourWebSite != '' && broker.yourWebSite != null}">
									<tr>
									<td class="txt-right"><spring:message  code="label.brkWebsite"/></td>
									<td><strong>${broker.yourWebSite}</strong></td>
								</tr>
								</c:if>
									</c:otherwise>
								</c:choose>

								<c:if test="${broker.education != '' && broker.education != null}">
									<tr>
										<td class="txt-right"><spring:message  code="label.brkEducation"/></td>
										<td><strong>${broker.education}</strong></td>
									</tr>
								</c:if>

								<c:if test="${broker.aboutMe != '' && broker.aboutMe != null}">
									<tr>
										<td class="txt-right"><spring:message  code="label.Aboutme"/></td>
										<td class="input-xlarge justified-text"><strong>${broker.aboutMe}</strong></td>
									</tr>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
				<!-- Showing comments -->
				<%-- 							<comment:view targetId="${broker.id}" targetName="BROKER"> --%>
				<%-- 							</comment:view> --%>
			</form>
			</div>
		</div>
	</div>
	<!-- #row-fluid -->
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#brkProfile").removeClass("link");
		$("#brkProfile").addClass("active");

		$("a[title='My Information']").parent().addClass("active");

		var brkId = ${broker.id};
		if (brkId != 0) {
			$('#certificationInfo').attr('class', 'done');
			$('#buildProfile').attr('class', 'active');
		} else {
			$('#certificationInfo').attr('class', 'visited');
			$('#buildProfile').attr('class', 'visited');
		};

		/* var paymentid = ${paymentId};
		if (paymentid != 0) {
			$('#paymentinfo').attr('class', 'done');
		} else {
			$('#paymentinfo').attr('class', 'visited');
		}; */

		var updatedProfile = '${updatedProfile}';

		if ('true' == updatedProfile) {
		var href = "/hix/broker/profileupdated";
		$(
			'<div id="success" class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body"><iframe id="success" src="'+ href
					+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:310px;"></iframe></div></div>').modal();
		}

        $('#aid-editPage').click(function(){
            window.location = "/hix/broker/editprofile";
        });
        $('#aid-editMap').click(function(){
            window.location = "/hix/broker/editprofile?ref=${ecyptedModuleParamsMap}";
        });
        

        formatAllPhoneNumbers(["sPhoneNumber"]);
        reloadImageURL();

	});
	
	function closeSuccess() {
		$("#success").remove();
		$('.modal-backdrop').remove();
		//window.location.replace = "/hix/broker/viewprofile";
	}

	$(".comma").text(function(i, val) {
	    return val.replace(/,/g, ", ");
	});

	/**
	* Function to reload image.
	*/
	function reloadImageURL() {
		var imageURL = $("#brokerPhoto").attr("src");

		if(null != imageURL && "undefined" !== imageURL) {
			if(-1 == imageURL.search("\\?ts=")) {
				imageURL = imageURL + "?ts=" + ((new Date()).getTime()) + "";
			}
			else {
				imageURL = imageURL.substring(0,imageURL.search("?ts=") + 3)  + ((new Date()).getTime()) + "";
			}

			$("#brokerPhoto").attr("src", imageURL);
		}
	}
</script>
