(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .factory('searchReconciliation', searchReconciliation);

    searchReconciliation.$inject = [];

    function searchReconciliation() {
        var issuerDetails = {};
        var enrollementDetails = {};

        return {
            setIssuer: setIssuer,
            getIssuer : getIssuer,
            setEnrollment: setEnrollment,
            getEnrollment: getEnrollment

        };

        function setIssuer(issuerInfo) {
            issuerDetails = issuerInfo;
        }

        function getIssuer() {
            return issuerDetails;
        }

        function setEnrollment(issuerInfo) {
            enrollementDetails = issuerInfo;
        }

        function getEnrollment() {
            return enrollementDetails;
        }
    }
})();