package com.getinsured.ssap.service;

import java.util.Date;
import java.util.Map;
import java.util.Properties;

import com.getinsured.hix.model.NoticeQueued;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.timeshift.TSCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.security.GhixRestTemplate;

import javax.annotation.PostConstruct;

/**
 * TODO: Missing JavaDoc.
 *
 * @author Yevgen Golubenko
 * @since 2019-07-08
 */
@Service("ssapNoticeService")
public class SsapNoticeServiceImpl implements SsapNoticeService {
  private static final Logger log = LoggerFactory.getLogger(SsapNoticeServiceImpl.class);
  private static String ELIGIBILITY_URL = null;
  private static String ADDITIONAL_INFO_PATH = "ssapintegration/notice/vlp/additionalinforequired/";

  private final Properties configProp;
  private final GhixRestTemplate ghixRestTemplate;
  private final NoticeService noticeService;
  private boolean deferEligibilitySending = false;

  @PostConstruct
  public void postConstruct() {
    deferEligibilitySending = "true".equals(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_DEFER_ELIGIBILITY_EMAIL));

    log.debug("Initialized SsapNoticeService properties");
  }

  @Autowired
  public SsapNoticeServiceImpl(final Properties configProp, final GhixRestTemplate ghixRestTemplate, final NoticeService noticeService) {
    this.configProp = configProp;
    this.ghixRestTemplate = ghixRestTemplate;
    this.noticeService = noticeService;

    if(ELIGIBILITY_URL == null) {
      ELIGIBILITY_URL = this.configProp.getProperty("ghixEligibilityServiceURL");
      if(ELIGIBILITY_URL == null || "".equals(ELIGIBILITY_URL)) {
        ELIGIBILITY_URL = "http://localhost:8081/ghix-eligibility/";
        log.warn("Unable to find ghixEligibilityServiceURL in configuration.properties, using default: {}", ELIGIBILITY_URL);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendAdditionalInformationNeeded(final String caseNumber) {
    final String url = String.format("%s%s%s", ELIGIBILITY_URL, ADDITIONAL_INFO_PATH, caseNumber);
    try {
      if (!deferEligibilitySending) {
        final Map<String, Object> response = ghixRestTemplate.getForObject(url, Map.class);
        log.info("Send Additional Information Needed Notification: {} => {}", response.get("result"), response.get("message"));
      }
    }
    catch(Exception e) {
      log.error("Error invoking: {}, error message: {}", url, e.getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendEligibilityNotification(final String caseNumber) {
    final String url = String.format("%s%s%s", ELIGIBILITY_URL, "atresponse/notifications/", caseNumber);

    try {
      final String response = ghixRestTemplate.getForObject(url, String.class);
      log.info("Send eligibility notification. url: {}, response: {}", url, response);
    }
    catch(Exception e) {
      log.error("Error invoking: {}, error message: {}", url, e.getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void sendEligibilityNotification(final Long applicationID, final String caseNumber) {
    try {
      if (deferEligibilitySending) {
        log.debug("sendEligibilityNotification::queuing eligibility notice");
        Date todayDate = TSCalendar.getInstance().getTime();
        todayDate = DateUtil.removeTime(todayDate);
        noticeService.scheduleQueuedNotice("UniversalEligibilityNotification", todayDate, NoticeQueued.TableNames.SSAP_APPLICATIONS, "ID", applicationID);
      } else {
        log.debug("sendEligibilityNotification::sending eligibility notice real time");
        sendEligibilityNotification(caseNumber);
      }
    } catch(Exception e) {
      log.error("sendEligibilityNotification::Error sending eligibility notification with error message: {}", e.getMessage());
    }
  }
}
