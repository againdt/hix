<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<style type="text/css">
.modal .header{
	border-radius: 6px 6px 0 0 !important;
    padding: 10px !important;
}
.modal .dialogClose{
	margin: -4px !important;
    padding: 4px;
}
.Ajaxloader{
	background-color: #F5F5F5;
    background-image: url("/hix/resources/images/Eli-loader.gif");
    background-position: center center;
    background-repeat: no-repeat;
    height: 769px;
    margin: -10% 0;
    opacity: 0.75;
    position: fixed;
    width: 100%;
    z-index: 2147483647;
}

</style>
<input type="hidden" id="ticketId" name="ticketId" value="${encTicketid}">
<div id="markCompleteDialogAction" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialogAction" aria-hidden="true" data-keyboard="false" data-backdrop="static">
		<div class="modal-header markCompleteHeader">
	       	<button aria-hidden="true" data-dismiss="modal" id="crossCloseComplete" class="close" type="button" onclick="cancelModel();">&times;</button>
	   		<h3><spring:message  code="header.markTaskAsComplete"/></h3>
	    </div>
	    <div id="redirect"></div>
	    <div class="modal-body">
			<div class="row-fluid">
				<h4 id="taskName" name="taskName"></h4>
			</div>
			<div class="row-fluid">
		  		<div class="span5">
					<div class="taskframesControls" id="taskframesControls"></div>
					<div  id="ticketListAttachmentViewerDiv">
						<iframe id="ticketListAttachmentViewer" style="width: 100%; height: auto;  border: 1px solid #e4e4e4;"></iframe> 
					</div>
					<input type="hidden" name="validateComments" id="validateComments" value="">
					
					<div class="control-group" >
						<label class="control-label"><spring:message code="text_task.notes"/>
						<img id="commentsReqd" src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
						<div class="controls">
							<textarea name="commentBox" id="commentBoxAction"  maxlength="1999"></textarea>
						</div>
					</div>
					
					<div class="taskPropertiesControls" id="taskPropertiesControls"></div>
					<div class="control-group" id="completeTaskloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top:-52px;margin-left: -47px;">
					    <img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
					</div>
				</div>
		  		<div class="span7">
		  			<div id="documentInfoUrlViewerDiv">
						<iframe id="documentInfoUrlViewer" style="width: 95%; height: 90%;  border: 1px solid #e4e4e4; margin-top: 30px;"></iframe> 
			 		</div> 
		  		</div>
	  		</div>
	  	</div>
	  	<div class="modal-footer clearfix">
	    	<button class="btn pull-left"   id="ticketListCompleteTaskCancelBtn" data-dismiss="modal" aria-hidden="true" onclick="cancelModel();"><spring:message code="button.cancel"/></button>
	    	<button class="btn btn-primary" id="ticketListtaskCompleteBtn" onclick="javascript:func();"><spring:message code="btnTxt.taskComplete"/></button>
	  	</div>
	</div>
<!-- Cancel confirmation dialog  -->
<!-- Cancel ticket dialog ends -->
<script type="text/javascript">

$('document').ready(function(){
	$('#ticketListAttachmentViewerDiv').css('display','none');
	$('#documentInfoUrlViewerDiv').css('display','none');
});

function cancelModel(){
	$('#markCompleteDialogAction').modal('hide');
	$('#commentBoxAction').val("");
	$('#ticketListAttachmentViewerDiv').hide();
	$('#ticketListAttachmentViewer').empty();
	$('#documentInfoUrlViewerDiv').hide();
	$('#documentInfoUrlViewer').empty();
	
}


var currentDocument;
var documentArray;
function getTicketActiveTaskData(){
	if($('#validateComments').val() == 'N'){
		$('#commentsReqd').hide();
	}
	else{
		$('#commentsReqd').show();
	}
	$('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden');
	var ticketId= $("#ticketId").val();
	$('#tkmErrorDiv').html("");
	var validateUrl = '<c:url value="/ticketmgmt/ticket/userTickettasklist"></c:url>';
	$.ajax(
	{
	url : validateUrl,
	type : "POST",
	data : {
		ticketId : ticketId,
		'${df:csrfTokenParameter()}' : '<df:csrfToken plainToken="true"/>'
		   },
		success: function(response,xhr)
		{
			$('.Ajaxloader').remove();
			$('body').css('overflow','auto');
			if(isInvalidCSRFToken(xhr))
				return;
			parseTicketActiveTaskData(response);
		},
		error: function(e)
		{
			$('.Ajaxloader').remove();
			$('body').css('overflow','auto');
				alert("Failed to Fetch Data");
			}
	});
}
function parseTicketActiveTaskData(response){
	var newresponse = JSON.parse(response);
	var myList = newresponse;
	var columnSet = [];
	var taskId;
	var actTaskId;
	var processInstId;
	var taskStatus;
	var taskName;
	var isAuthorized;
	 for (var i = 0 ; i < myList.length ; i++) {
	        var rowHash = myList[i];
	        for (var key in rowHash) {
	        	if(key=="TaskId"||key=="ActivitiTaskId"||key=="ProcessInstanceId"||key=="Status"||key=="TaskName"||key=="IsAuthorized"){
	        		columnSet.push(key);
	            	}
	        }
	    }
	 
	 for (var i = 0 ; i < myList.length ; i++) {
         for (var colIndex = 0 ; colIndex < columnSet.length ; colIndex++) {
        	 
	         	if(columnSet[colIndex]=="TaskId"){
	         		taskId= myList[i][columnSet[colIndex]];
	         	}
	         	if(columnSet[colIndex]=="ActivitiTaskId"){
	         		actTaskId= myList[i][columnSet[colIndex]];
	         	}
	         	if(columnSet[colIndex]=="ProcessInstanceId"){
	         		processInstId= myList[i][columnSet[colIndex]];
	         	}
	         	if(columnSet[colIndex]=="Status"){
	         		taskStatus=myList[i][columnSet[colIndex]];
	         	}
	         	if(columnSet[colIndex]=="TaskName"){
	         		taskName=myList[i][columnSet[colIndex]];
	         	}
	         	
	         	if(columnSet[colIndex]=="IsAuthorized"){
	         		isAuthorized=myList[i][columnSet[colIndex]];
	         	}
	         	
		 }
         
        /*  if(taskStatus==='Claimed'||taskStatus==='UnClaimed'){
         		if(isAuthorized=='false'){
         			alert("There are task(s) in the ticket which you are not authorized to claim/complete");
         			window.location.reload(true);
         			}
         		} */
         
	         if(taskStatus==='Claimed'){
	        	encTask(actTaskId, function(encActTaskId)
				{					
					completeTask(taskId,encActTaskId,processInstId);
				});	  			
	  		}
	
	         if(taskStatus==='UnClaimed'){
	        	 claimtask(taskId,actTaskId);
	  		}
	         
	         $("#taskName").text(taskName);
	 }
}

function claimtask(taskId,actTaskId)
{
	var myObject = new Object();
	myObject.ActivitiTaskId =actTaskId+"";
	myObject.TaskId =taskId+"";
	myObject.TicketId= $("#ticketId").val();
	
	
	$('#tkmErrorDiv').html("");
	var myString = JSON.stringify(myObject);
	var validateUrl = '<c:url value="/ticketmgmt/ticket/claimUserTask"></c:url>';

	var claimTaskUrl = validateUrl;
	var datToBeSent={
		taskObject : myString
	};
	datToBeSent["${df:csrfTokenParameter()}"]='<df:csrfToken plainToken="true" />';
	$('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden');
	$.ajax(
	{
	url : claimTaskUrl,
	type : "POST",
	data : datToBeSent,
		success: function(response,xhr)
		{
			if(isInvalidCSRFToken(xhr))
				return;
			notifyClaimResponse(myObject.TicketId);
		},
		error: function(e)
		{
			alert("Failed to Claim Ticket");
			$('.Ajaxloader').remove();
			$('body').css('overflow','auto');	
		}
	});
}

function encTask(actTaskId, callback)
{
	$('#tkmErrorDiv').html("");
	var validateUrl = '<c:url value="/ticketmgmt/ticket/getEncValue"> </c:url>';
	$('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden')
	var taskFormPropertiesUrl = validateUrl;
	$.ajax(
			{
			url : taskFormPropertiesUrl,
			type : "GET",
			data : {
				id : actTaskId
				   },
				success: function(response,xhr)
				{
					$('.Ajaxloader').remove();
					$('body').css('overflow','auto');					
					actTaskId = JSON.parse(response);
					return callback(actTaskId ); 
				},
				error: function(e)
				{
					$('.Ajaxloader').remove();
					$('body').css('overflow','auto');
					alert("Failed to Fetch Data");
				}
			});	
}

function completeTask(taskId,actTaskId,processInstId)
  {  
	actTask=actTaskId;
	tkmTaskId =taskId;
	actProInstId=processInstId;
	$('#tkmErrorDiv').html("");
	var validateUrl = '<c:url value="/ticketmgmt/ticket/getTaskFormProperties"> </c:url>';
	$('body').prepend('<div class="Ajaxloader"></div>').css('overflow','hidden')
	var taskFormPropertiesUrl = validateUrl;
	$.ajax(
			{
			url : taskFormPropertiesUrl,
			type : "POST",
			data : {
				taskId : actTask,
				'${df:csrfTokenParameter()}' : '<df:csrfToken plainToken="true"/>'
				   },
				success: function(response,xhr)
				{
					$('.Ajaxloader').remove();
					$('body').css('overflow','auto');
					if(isInvalidCSRFToken(xhr))
						return;
					buildPopUp(response);
				},
				error: function(e)
				{
					$('.Ajaxloader').remove();
					$('body').css('overflow','auto');
					alert("Failed to Fetch Data");
				}
			}); 

	
}

function buildPopUp(taskPropertiesResponse){
	$("#crossCloseComplete").attr("disabled",false);
	//$("#ticketListCompleteTaskCancelBtn").attr("disabled",false);
	$("#ticketListtaskCompleteBtn").attr("disabled",false);
	$('#taskPropertiesControls').empty();
	var newresponse = JSON.parse(taskPropertiesResponse);
	var popup = $('#taskPropertiesControls')
	var newSelect=null;
	var txtField =null;
	var td;
	var readDocFlag=false;
	if(document.getElementById("flag")==null){
		$('#taskPropertiesControls').append(td);	
	for (var i = 0 ; i < newresponse.length ; i++){
			var propertyLabel = document.createElement("label");
			propertyLabel.innerHTML=newresponse[i].name
			propertyLabel.className="control-label";
			
			if(newresponse[i].type=="enum"){
				var controlDiv = document.createElement("div");
				controlDiv.className = "control-group";
				controlDiv.appendChild(propertyLabel);
				
				//$('#taskPropertiesControls').append(propertyLabel);
				var divv = document.createElement("div");
				divv.className = "controls";
				newSelect = document.createElement("SELECT");
				$(newSelect).data("selectKey",newresponse[i].id);
				newSelect.name="selectField"
					for(j=0 ; j<newresponse[i].values.length;j++)
					{
					   var opt = document.createElement("option");
					   opt.value= j;
					   opt.innerHTML = newresponse[i].values[j]; // whatever property it has
					   newSelect.appendChild(opt);
					}
				//propertyLabel.htmlFor="selectid"
				divv.appendChild(newSelect);
				controlDiv.appendChild(divv)
				$('#taskPropertiesControls').append(controlDiv);
				//$('#taskPropertiesControls').append(newSelect);
				td=document.createElement("br");
				$('#taskPropertiesControls').append(td);
				
			}
			else{
				if(newresponse[i].id=="enableReadDocuments"||newresponse[i].id=="comments"){
					readDocFlag=true;
				}
				else{
					var controlDiv = document.createElement("div");
					controlDiv.className = "control-group";
					controlDiv.appendChild(propertyLabel);
					//$('#taskPropertiesControls').append(propertyLabel);
					var divv = document.createElement("div");
					divv.className = "controls";
					
					txtField = document.createElement("input");
					txtField.name="textField"
					$(txtField).data("txtKey",newresponse[i].id);
					
					//$('#taskPropertiesControls').append(txtField);
					divv.appendChild(txtField);
					controlDiv.appendChild(divv);
					
					$('#taskPropertiesControls').append(controlDiv);
					
					td=document.createElement("br");
					$('#taskPropertiesControls').append(td);	
				}
			}
		}
		$('#taskPropertiesControls').append(" <div hidden=true id=flag></div>");
		td=document.createElement("br");
	}
	if(readDocFlag){
		$('#markCompleteDialogAction').css({
			"width":560,
			"margin-left":"-280px"
		});	
	}
	else{
		$('#markCompleteDialogAction').css({
			"width":300,
			"margin-left":"-280px"
		});
	}
	if(readDocFlag){
	
		var validateUrl = "<c:url value='/ticketmgmt/ticket/fetchTicketDocuments'/>";
		var ticketId  = $("#ticketId").val();			
		$.ajax({
			url : validateUrl,
			type : "GET",
			data : {
				ticketId : ticketId
			},
			success: function(response){
					setDocumentViewer(JSON.parse(response));
			},
	  		error: function(e){
	  			alert("Failed to fetch attachments");
	   		},
	   	 async:   false
		});
		
	}
	
		$('#markCompleteDialogAction').modal('show');
		
}
function setDocumentViewer(response){
	
	currentDocument=0;
	documentArray=response.docPathlist;
	td=document.createElement("br");
	$('#taskPropertiesControls').append(td);
	
	var ticketDocuments = document.createElement("label");
	ticketDocuments.className="control-label";
	ticketDocuments.id="ticketDocuments";
	ticketDocuments.name="ticketDocuments";
	ticketDocuments.innerHTML="Attached Documents"
	ticketDocuments.style.display = "inline-block";
	
	
	
	var controlDiv = document.createElement("div");
	controlDiv.className = "control-group";
	controlDiv.appendChild(ticketDocuments);
	
	
	$('#taskframesControls').empty().append(controlDiv);
	
	$('#taskPropertiesControls').find('#ticketDocuments').removeAttr('style'); 
	
	var documentUrl = "${pageContext.request.contextPath}/ticketmgmt/ticket/viewDocument?documentId="+encodeURI(documentArray[currentDocument]);
	document.getElementById('ticketListAttachmentViewer').setAttribute("src", documentUrl);
	//$('#ticketListAttachmentViewer').show();
	$("#ticketListAttachmentViewerDiv").removeAttr('style').css("display", "block !important").css("width","500px");
	
	if(response.formParams.docInfoUrl != undefined && response.formParams.docInfoUrl != ""){
		
		var documentInfoUrl = "${pageContext.request.contextPath}"+response.formParams.docInfoUrl;
		document.getElementById('documentInfoUrlViewer').setAttribute("src", documentInfoUrl);
		
		$("#documentInfoUrlViewerDiv").removeAttr('style').css("display", "block !important");
		$("#ticketListAttachmentViewerDiv").css({
			"width":"360px"
		});
		$("#documentInfoUrlViewerDiv").css({
			"width":"508px",
			"margin-left":15,
			"height":380
		});
	
		$("#modal-body-row").css({
			"display":"table"
		});
		$("#modal-body-row>div.col-md-6").css({
			"display":"table-cell",
			"vertical-align":"top"
		});
		
		$('#markCompleteDialogAction').css({
			"width":900,
			"margin-left":"-450px"
		});
		
		/* $("#taskPropertiesControls").find(".control-label").css({
			"width":"28%"
		}); */
		
		
	
	}
	//document.getElementById('ticketListAttachmentViewer').setAttribute("display", "block"); 
	if(response.length>1){
		 var prevButton = document.createElement("input");
		    //Assign different attributes to the element. 
		    prevButton.type = "button";
		    prevButton.value = "<"; // Really? You want the default value to be the type string?
		    prevButton.name = "prev";  // And the name too?
		    prevButton.id = "prevButtonId";		
		    prevButton.onclick = prevDocumentView;
		    prevButton.disabled="disabled"
		    $('#taskPropertiesControls').append(prevButton);
		var nextButton = document.createElement("input");
	    //Assign different attributes to the element. 
	    nextButton.type = "button";
	    nextButton.value = ">"; // Really? You want the default value to be the type string?
	    nextButton.name = "next";  // And the name too?
	    nextButton.id = "nextButtonId";		
	    nextButton.onclick = nextDocumentView;
	    $('#taskPropertiesControls').append(nextButton);
	   
	}
	
	$('#ticketDocuments').css('font-weight','bold');
	
}
function nextDocumentView(){
	if(currentDocument>=documentArray.length-2){
		$('#taskPropertiesControls').find('#nextButtonId').attr('disabled','disabled');
		currentDocument++;
	}
	else{	
		currentDocument++;
	}
	var documentUrl = "${pageContext.request.contextPath}/ticketmgmt/ticket/viewDocument?documentId="+encodeURI(documentArray[currentDocument]);
	document.getElementById('ticketListAttachmentViewer').setAttribute("src", documentUrl);
	if($('#taskPropertiesControls').find('#prevButtonId').attr('disabled')=="disabled"&&currentDocument>0){
		$('#taskPropertiesControls').find('#prevButtonId').removeAttr('disabled');
	}
	
	
}
function prevDocumentView(){
	currentDocument--
	if($('#taskPropertiesControls').find('#nextButtonId').attr('disabled')=="disabled"){
		$('#taskPropertiesControls').find('#nextButtonId').removeAttr('disabled');
	}
	var documentUrl = "${pageContext.request.contextPath}/ticketmgmt/ticket/viewDocument?documentId="+encodeURI(documentArray[currentDocument]);
	document.getElementById('ticketListAttachmentViewer').setAttribute("src", documentUrl);
	if(currentDocument==0){
		$('#taskPropertiesControls').find('#prevButtonId').attr('disabled','disabled');
	}
}

function showdetail(docid) {
	//var  contextPath =  "http://localhost:8007/hix/";
	
	var  contextPath =  "<%=request.getContextPath()%>";
	var  serverName =  "<%=request.getServerName()%>";
	var  serverPort =  "<%=request.getServerPort()%>";
	

    var documentUrl = "http://"+serverName+":"+serverPort+contextPath + "/ticketmgmt/ticket/viewDocument?documentId="+encodeURI(docid);
    
	open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
}


function func(){
	
	
	var comments = document.getElementById('commentBoxAction').value;
	var validateComments = document.getElementById('validateComments').value;
	var commentLength=document.getElementById('commentBoxAction').value.length;
	if(validateComments == 'N' && comments == ''){
		commentLength=0;
	}
	
	var myObject = new Object();
	myObject.ActivitiTaskId = actTask+"";
	myObject.ProcessInstanceId=actProInstId+"";
	myObject.TaskId = tkmTaskId+"";
//	myObject.Comments=comments;
	myObject.TicketId= $("#ticketId").val();
	myObject.ValidateComments=validateComments;
	
	var dropdownfields =[]
	var textfields =[]
	var formPropertiesMap = {};
	
	
	dropdownfields = document.getElementsByName("selectField");

	textfields = document.getElementsByName("textField");

	var selectedValue = "";
	var elms = document.getElementById("taskPropertiesControls").getElementsByTagName("*");
	for (var i = 0; i < elms.length; i++) {
		if(elms[i].name=="selectField")
		dropdownfields = document.getElementsByName("selectField");
		else{
		if(elms[i].name=="selectField"){
			textfields = document.getElementsByName("textField");
		}
	   }
	}
	for(var i=0 ; i<dropdownfields.length; i++) {
	    selectedValue = dropdownfields[i].options[dropdownfields[i].selectedIndex].text;
	    formPropertiesMap[$(dropdownfields[i]).data("selectKey")] = selectedValue;
	}
	
	for(var i=0 ; i<textfields.length; i++) {
	    selectedValue = textfields[i].value;
	    formPropertiesMap[$(textfields[i]).data("txtKey")] = selectedValue;
	}

	
	
	
	myObject.formPropertiesMap = formPropertiesMap;
	
	var myStringJSON = JSON.stringify(myObject);
	var validateUrl = '<c:url value="/ticketmgmt/ticket/completeUserTask"></c:url>';
	var completeTaskUrl = validateUrl;
	if(comments=='' && validateComments == 'Y'){
		alert("Please provide comments");
	}
	
		else{
			$("#completeTaskloader").show();
			$("#crossCloseComplete").attr("disabled",true);
			//$("#ticketListCompleteTaskCancelBtn").attr("disabled",true);
			$("#ticketListtaskCompleteBtn").attr("disabled",true);
			$.ajax(
					{
					url : completeTaskUrl, 
					type : "POST",
					data : {
						taskObject : myStringJSON,
						comments : comments,
						commentLength : commentLength,
						${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />"
						   },
						success: function(response,xhr)
						{
							if(isInvalidCSRFToken(xhr))
								return;
							$("#completeTaskloader").hide();
							$('#markCompleteDialogAction').modal('hide');
							data = response.split('|');
							
							if(data[0] != 'SUCCESS' && response != null)	{
								$('#markCompleteDialog').modal('hide');
							   $('#tkmErrorDiv').html(data[1]);
							}
							if(data[0] == "SUCCESS"){
								if(data.length > 1){
									notifyCompleteResponse(false);
								}else{
								notifyCompleteResponse(true);
								}
							}
							
						},
							
							error: function(e)
							{
								$("#completeTaskloader").hide(); 			
								alert("Failed to Complete Ticket");
							}
					});
	
		}
	}

    function submitRedirectionForm(redirectUrl,caseNumber,csrftoken){
    	$('#redirect').append("<form id='redirectForm' action='/hix/ticketmgmt/ticket/redirection' method='POST'>");
        $('#redirect form').append("<input type='hidden'  name='redirectUrl' id='redirectUrl' />");
        $('#redirect form').append("<input type='hidden'  id='caseNumber' name='caseNumber' />");
        $('#redirect form').append("<input type='hidden'  id='csrftoken' name='csrftoken' />");
        $('#redirect form').append("<br><input type='submit' id='savebutton' value='Save' />");
        $('#redirectUrl').val(redirectUrl);
        $('#caseNumber').val(caseNumber);
        $('#csrftoken').val(csrftoken);
        $( "#redirectForm" ).submit();
    } 
 
</script>
