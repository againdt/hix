package com.getinsured.hix.indportal.dto;

import java.util.ArrayList;
import java.util.List;

public class PlanDetails {

	List<PlanSummaryDetails> activeHealthPlanDetails = new ArrayList<>();
	List<PlanSummaryDetails> activeDentalPlanDetails = new ArrayList<>();
	
	public List<PlanSummaryDetails> getActiveHealthPlanDetails() {
		return activeHealthPlanDetails;
	}
	public void setActiveHealthPlanDetails(List<PlanSummaryDetails> activeHealthPlanDetails) {
		this.activeHealthPlanDetails = activeHealthPlanDetails;
	}
	public List<PlanSummaryDetails> getActiveDentalPlanDetails() {
		return activeDentalPlanDetails;
	}
	public void setActiveDentalPlanDetails(List<PlanSummaryDetails> activeDentalPlanDetails) {
		this.activeDentalPlanDetails = activeDentalPlanDetails;
	}			
}
