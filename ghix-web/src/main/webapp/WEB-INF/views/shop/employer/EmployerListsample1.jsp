<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%
Integer pageSizeInt = (Integer)request.getAttribute("pageSize");
String reqURI = (String)request.getAttribute("reqURI");
%> 
<div id="maincontainer" class="container">
<div class="row">
	<div class="span4">
		<h2>User Administration</h2>
	</div>
	<div  class="span17">
	<!-- </div>
	<div> -->
		<form id="frmuserlist" name="frmuserlist" action="user" method="post">
			<df:csrfToken/>
			<fieldset>
			<legend></legend>
				<p></p>
				<br />
				<div class="profile">
					<div class="clearfix displayTable">
					<%-- <display:table pagesize="5" export="true" name="userlist" sort="list" id="data" requestURI="list"> --%>
					<display:table pagesize="<%=pageSizeInt %>" export="false" name="employerslist"  sort="external" id="employerTable" requestURI="<%=reqURI %>" partialList="true" size="resultSize">
					  <display:setProperty name="paging.banner.placement">bottom</display:setProperty>
					  <display:column property="id" title="ID" sortName="id" />
					  <display:column property="businessType" title="Business Type" sortable="true" sortName="businessType"/>
					  <display:column property="companyName" title="Company Name" sortable="true" sortName="companyName"/>
					  <display:column property="eid" title="Eid" sortable="true" sortName="eid"/>
					  <display:column property="fullTimeEmpCoverage" title="Full Time Coverage" sortable="true" sortName="fullTimeEmpCoverage"/>
					  <display:column property="created" title="Created" format="{0,date,MM/dd/yyyy}" sortable="true" sortName="created"/>
					</display:table>
					</div></div>
			</fieldset>
		</form>
	</div>
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>This information is required to determine initial eligibility to
					use the SHOP Exchange. All fields are subjected to a basic format
					validation of the input (e.g. a zip code must be a 5 digit number).
					The question mark icon which opens a light-box provides helpful
					information. The EIN number can be validated with an external data
					source if an adequate web API is available.</p>
			</div>
		</div>
	</div>
</div>
</div>