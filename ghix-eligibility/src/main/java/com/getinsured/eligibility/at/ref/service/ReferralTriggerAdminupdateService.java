package com.getinsured.eligibility.at.ref.service;

import java.util.List;

import com.getinsured.iex.ssap.Address;

/**
 * @author chopra_s
 *
 */
public interface ReferralTriggerAdminupdateService {
	void triggerAdminupdate(long enrolledApplicationId, long currentApplicationId, Boolean isDemo, Address updateMailingAddress, List<String> memberGuIdList ) throws Exception;
}
