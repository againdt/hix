
package com.getinsured.iex.hub.ssac.extension;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for TitleII requested yearly income information
 *             
 * 
 * <p>Java class for SSATitleIIRequestedYearInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSATitleIIRequestedYearInformationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}IncomeDate"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}YearlyIncomeAmount"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSATitleIIRequestedYearInformationType", propOrder = {
    "incomeDate",
    "yearlyIncomeAmount"
})
public class SSATitleIIRequestedYearInformationType
    extends ComplexObjectType
{

    @XmlElement(name = "IncomeDate", required = true)
    protected String incomeDate;
    @XmlElement(name = "YearlyIncomeAmount", required = true)
    protected BigDecimal yearlyIncomeAmount;

    /**
     * Gets the value of the incomeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncomeDate() {
        return incomeDate;
    }

    /**
     * Sets the value of the incomeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncomeDate(String value) {
        this.incomeDate = value;
    }

    /**
     * Gets the value of the yearlyIncomeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getYearlyIncomeAmount() {
        return yearlyIncomeAmount;
    }

    /**
     * Sets the value of the yearlyIncomeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setYearlyIncomeAmount(BigDecimal value) {
        this.yearlyIncomeAmount = value;
    }

}
