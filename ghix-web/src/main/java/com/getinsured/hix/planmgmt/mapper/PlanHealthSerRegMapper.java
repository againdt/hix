package com.getinsured.hix.planmgmt.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.RatingArea;
import com.getinsured.hix.model.Region;
import com.getinsured.hix.planmgmt.repository.IPlanRateRepository;
import com.getinsured.hix.planmgmt.service.RatingAreaService;
import com.getinsured.hix.planmgmt.service.RegionService;

@Component
public class PlanHealthSerRegMapper
{
	private static Map<String, Integer> columnKeys = null;
	private static final String MAXAGE = "maxAge";
	private static final String MINAGE = "minAge";
	private static final String TOBACCO = "tobacco";
	private static final String RATE = "rate";
	private static final String COUNTY = "county";
	private static final String ZIP = "zip";
	private static final String REGION = "Rating Area";
	//private static final String FAMILY_CHILD = "Child PMPM";
	private static final String FAMILY_MEMBER = "Member PMPM";
	//private static final String FAMILY_MEMBER_N_SPOUSE = "Member+Spouse PMPM";
	//private static final String FAMILY_MEMBER_N_CHILD = "Member+Children PMPM";
	//private static final String FAMILY_ALL = "Family PMPM";

	private static final int MAXAGE_VAL = 1;
	private static final int MINAGE_VAL = 2;
	private static final int TOBACCO_VAL = 3;
	private static final int RATE_VAL = 4;
	private static final int COUNTY_VAL = 5;
	private static final int ZIP_VAL = 6;
	private static final int REGION_VAL = 7;
	private static final int FAMILY_MEMBER_VAL = 8;
//	private static final int FAMILY_CHILD_VAL = 8;
//	private static final int FAMILY_MEMBER_VAL = 9;
//	private static final int FAMILY_MEMBER_N_SPOUSE_VAL = 10;
//	private static final int FAMILY_MEMBER_N_CHILD_VAL = 11;
//	private static final int FAMILY_ALL_VAL = 12;

	@Autowired private RegionService regionService;
	@Autowired private RatingAreaService ratingAreaService;
	@Autowired private IPlanRateRepository iPlanRateRepository;
	
	static 
	{
		columnKeys = new HashMap<String, Integer>();
		columnKeys.put( MAXAGE, MAXAGE_VAL);
		columnKeys.put( MINAGE, MINAGE_VAL );
		columnKeys.put( TOBACCO, TOBACCO_VAL );
		columnKeys.put( RATE, RATE_VAL );
		columnKeys.put( COUNTY, COUNTY_VAL );
		columnKeys.put( ZIP, ZIP_VAL );
		columnKeys.put( REGION, REGION_VAL );
		//columnKeys.put( FAMILY_CHILD, FAMILY_CHILD_VAL );
		columnKeys.put( FAMILY_MEMBER, FAMILY_MEMBER_VAL );
		//columnKeys.put( FAMILY_MEMBER_N_SPOUSE, FAMILY_MEMBER_N_SPOUSE_VAL );
		//columnKeys.put( FAMILY_MEMBER_N_CHILD, FAMILY_MEMBER_N_CHILD_VAL );
		//columnKeys.put( FAMILY_ALL, FAMILY_ALL_VAL );
	}
	
	/*	Commenting for SERFF changes
	public List<PlanHealthServiceRegion> mapData(List<List<String>> data, final List<String> columns) 
	{	
		//List<PlanHealthServiceRegion> regionsMap = new ArrayList<PlanHealthServiceRegion>();
		Map<String,PlanHealthServiceRegion> regionsMap = new HashMap<String, PlanHealthServiceRegion>();
		for(List<String> line : data)
		{
			int regionIdx = columns.indexOf(REGION);
			String regionName =  line.get(regionIdx);
			List<Region> regions = null;
			List<PlanHealthRate> rates = mapData(line, columns, regions);
			if (regionName != null && regionName.length()>0)
			{
				regions = getRegions(regionName);
				mergeRegions(regionsMap, rates, regions);
			}
			//merge rates list and regions which should return a map key = "regionName_county_zip" value = List<PlanHealthServiceRegion>
		}
		List<PlanHealthServiceRegion> regions = new ArrayList<PlanHealthServiceRegion>();
		Set<String> keys = regionsMap.keySet();
		int count=0;
		for (String key : keys)
		{
			PlanHealthServiceRegion reg = regionsMap.get(key);
			count = count+reg.getPlanHealthRates().size();
			regions.add(reg);
		}
		//form list of regions based on the map
		return regions;//newly created list of regions
	}*/

		
	public void mapAndSaveData(List<List<String>> data, final List<String> columns, Plan plan) {	
		for (List<String> line : data) {
			int regionIdx = columns.indexOf(REGION);
			String regionName = line.get(regionIdx);			
			List<Region> regions = null;
			List<PlanRate> rates = mapData(line, columns, regions);
			if (regionName != null && regionName.length() > 0) {				
				RatingArea ratingArea = ratingAreaService.getByRatingAreaID(regionName);				
				List<PlanRate> ratesCopy = copyRates(rates);
				for (PlanRate rate : ratesCopy) {					
					PlanRate planRate = new PlanRate();
					planRate.setMaxAge(rate.getMaxAge());
					planRate.setMinAge(rate.getMinAge());
					planRate.setEffectiveStartDate(plan.getStartDate());
					planRate.setEffectiveEndDate(plan.getEndDate());
					planRate.setTobacco(null);
					planRate.setRate(rate.getRate());
					planRate.setPlan(plan);
					planRate.setRatingArea(ratingArea);
										
					iPlanRateRepository.save(planRate);					
				}
			}
		}
	}
	
	/*	Commenting for SERFF changes
	private void mergeRegions(Map<String, PlanHealthServiceRegion> regionsMap, List<PlanHealthRate> rates, List<Region> regions) 
	{
		//for ()
		//iterate thru regions from region name
			
			 * String regionKey = regionName + "_" + regionCounty + "_" + regionZip;
			 * PlanHealthServiceRegion serviceRegion = regionsMap.get(regionKey);
			 * if region is null
			 * 		creat new
			 * 		add in map
			 * else
			 * 		getrates list
			 * 		iterate thru the rates list passed in arg
			 * 			set the serviceregion in it
			 * 		add this list to gatRates
			 
		for (Region region : regions)
		{
				Commenting for SERFF changes
			String regionKey = region.getRegionName() + "_" + region.getCounty() + "_" + region.getZip();
			PlanHealthServiceRegion serviceRegion = regionsMap.get(regionKey);
			ArrayList<PlanHealthRate> ratesCopy = copyRates(rates);
			if (serviceRegion == null)
			{
				serviceRegion = new PlanHealthServiceRegion();
				serviceRegion.setRegion(region);
				regionsMap.put(regionKey, serviceRegion);
				
				serviceRegion.setPlanHealthRates(ratesCopy);
				for(PlanHealthRate rate : ratesCopy)
				{
					rate.setPlanHealthServiceRegion(serviceRegion);
				}
			}
			else
			{
				for(PlanHealthRate rate : ratesCopy)
				{
					rate.setPlanHealthServiceRegion(serviceRegion);
				}
				List<PlanHealthRate> existingRates = serviceRegion.getPlanHealthRates();
				if (existingRates == null)
				{
					existingRates = new ArrayList<PlanHealthRate>();
					serviceRegion.setPlanHealthRates(existingRates);
				}
				existingRates.addAll(ratesCopy);
			}
		}
	}*/

	private ArrayList<PlanRate> copyRates(List<PlanRate> rates)
	{
		ArrayList<PlanRate> ratesCopy = new ArrayList<PlanRate>();
		for (PlanRate rate : rates)
		{
			ratesCopy.add(rate.clone());
		}
		return ratesCopy;
	}

	private List<PlanRate> mapData(List<String> line, List<String> columns, List<Region> regions)
	{
		Map<String,String> familyTypes = new HashMap<String,String>();
		String colName = null;
		int colIdx = 0;
		PlanRate rate = new PlanRate();
		for (String data : line)
		{
			colName = columns.get(colIdx);
			colIdx++;
			Integer colVal = columnKeys.get(colName);
			if (colVal == null)
			{
				colVal = 0;
			}
			switch (colVal) 
			{
				case MAXAGE_VAL : 
					rate.setMaxAge(Integer.parseInt(data));
					break;
				case MINAGE_VAL : 
					rate.setMinAge(Integer.parseInt(data));
					break;
				case TOBACCO_VAL : 
					rate.setTobacco(data);
					break;
				case RATE_VAL : 
					rate.setRate(Float.parseFloat(data));
					break;
				/*case FAMILY_CHILD_VAL : 
					if (data != null && data.length() > 0)
					{
						familyTypes.put(FAMILY_CHILD, data);
					}
					break;*/
				/*case FAMILY_MEMBER_VAL : 
					if (data != null && data.length() > 0)
					{
						familyTypes.put(FAMILY_MEMBER, data);
					}
					break;*/
				/*case FAMILY_MEMBER_N_SPOUSE_VAL : 
					if (data != null && data.length() > 0)
					{
						familyTypes.put(FAMILY_MEMBER_N_SPOUSE, data);
					}
					break;
				case FAMILY_MEMBER_N_CHILD_VAL : 
					if (data != null && data.length() > 0)
					{
						familyTypes.put(FAMILY_MEMBER_N_CHILD, data);
					}
					break;
				case FAMILY_ALL_VAL : 
					if (data != null && data.length() > 0)
					{
						familyTypes.put(FAMILY_ALL, data);
					}
					break;*/
			}
		}
		return formMultipleRates(rate, familyTypes);
	
	}

	private List<PlanRate> formMultipleRates(PlanRate rate, Map<String, String> familyTypes) {
		List<PlanRate> rates = new ArrayList<PlanRate>();
		for (String familyType : familyTypes.keySet())
		{
			String rateVal = familyTypes.get(familyType);
			PlanRate rateObj = rate.clone();
			rateObj.setRate(Float.parseFloat(rateVal));
			
			Integer colVal = columnKeys.get(familyType);
			if (colVal == null)
			{
				colVal = 0;
			}
			//switch (colVal) 
			//{

				/*case FAMILY_ALL_VAL:
					rateObj.setFamilyTiers(FamilyTiers.FAMILY);
					break;
				case FAMILY_CHILD_VAL : 
					rateObj.setFamilyTiers(FamilyTiers.CHILDONLY);
					break;
				case FAMILY_MEMBER_N_CHILD_VAL:
					rateObj.setFamilyTiers(FamilyTiers.MEMBERCHILDREN);
					break;
				case FAMILY_MEMBER_N_SPOUSE_VAL:
					rateObj.setFamilyTiers(FamilyTiers.MEMBERSPOUSE);
					break;*/
				/*case FAMILY_MEMBER_VAL:
					rateObj.setFamilyTiers(FamilyTiers.MEMBER);
					break;*/
			//}
			rates.add(rateObj);
		}
		return rates;
	}
	
	private List<Region> getRegions(String regionName) {
		return regionService.getRegionsByName(regionName);
	}
	
	
	/*public PlanHealthServiceRegion mapData(final List<String> data, final List<String> columns) 
	{	
		PlanHealthServiceRegion benefit = new PlanHealthServiceRegion();
		int index = 0;
		for (String column : columns) {
			
			mapData(data.get(index), column, benefit);
			index++;
		}
		return benefit;
	}

	private void mapData(String data, String column, PlanHealthServiceRegion healthBenefit)
	 {
			Integer colVal = columnKeys.get(column);
			if (colVal == null)
			{
				colVal = 0;
			}
			switch (colVal) {
	
			case MAXAGE_VAL:
				List<PlanHealthRate> rates = healthBenefit.getPlanHealthRates();
				if (rates == null) {
					rates = new ArrayList<PlanHealthRate>();
					healthBenefit.setPlanHealthRates(rates);
				}

				PlanHealthRate rate = null;
				if (rates.size() >=1)
				{
					rate = rates.get(0);
				}
				else
				{
					rate = new PlanHealthRate();
					rate.setPlanHealthServiceRegion(healthBenefit);
					rates.add(rate);
				}
				rate.setPlanHealthServiceRegion(healthBenefit);
				rate.setMaxAge(Integer.parseInt(data));
				break;
			case MINAGE_VAL:
				List<PlanHealthRate> rates1 = healthBenefit.getPlanHealthRates();
				if (rates1 == null) {
					rates1 = new ArrayList<PlanHealthRate>();
					healthBenefit.setPlanHealthRates(rates1);
				}
				PlanHealthRate rate1 = null;
				if (rates1.size() >=1)
				{
					rate1 = rates1.get(0);
				}
				else
				{
					rate1 = new PlanHealthRate();
					rate1.setPlanHealthServiceRegion(healthBenefit);
					rates1.add(rate1);
				}
				rate1.setPlanHealthServiceRegion(healthBenefit);
				rate1.setMinAge(Integer.parseInt(data));
				break;
			case TOBACCO_VAL:
				List<PlanHealthRate> rates2 = healthBenefit.getPlanHealthRates();
				if (rates2 == null) {
					rates2 = new ArrayList<PlanHealthRate>();
					healthBenefit.setPlanHealthRates(rates2);
				}

				PlanHealthRate rate2 = null;
				if (rates2.size() >=1)
				{
					rate2 = rates2.get(0);
				}
				else
				{
					rate2 = new PlanHealthRate();
					rate2.setPlanHealthServiceRegion(healthBenefit);
					rates2.add(rate2);
				}
				rate2.setPlanHealthServiceRegion(healthBenefit);
				rate2.setTobacco(data);
				break;
			case RATE_VAL:
				List<PlanHealthRate> rates3 = healthBenefit.getPlanHealthRates();
				if (rates3 == null) {
					rates3 = new ArrayList<PlanHealthRate>();
					healthBenefit.setPlanHealthRates(rates3);
				}

				PlanHealthRate rate3 = null;
				if (rates3.size() >=1)
				{
					rate3 = rates3.get(0);
				}
				else
				{
					rate3 = new PlanHealthRate();
					rate3.setPlanHealthServiceRegion(healthBenefit);
					rates3.add(rate3);
				}
				rate3.setPlanHealthServiceRegion(healthBenefit);
				rate3.setRate(Float.parseFloat(data));
				break;
			case COUNTY_VAL:
				healthBenefit.setCounty(data);
				break;
			case ZIP_VAL:
				healthBenefit.setZip(data);
				break;
			default:
	
				break;
			}
		}*/
}
