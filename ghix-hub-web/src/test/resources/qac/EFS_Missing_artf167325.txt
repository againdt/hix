{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "insurance": {
              "insurancePolicyEffectiveDate": "05/04/2006",
              "insurancePolicyExpirationDate": "05/04/2006"
            },
            "dateOfBirth": "05/04/2006",
            "socialSecurityCard": {
              "socialSecurityNumber": "231554368"
            },
            "name": {
              "lastName": "Bonham",
              "firstName": "Jonathan"
            }
          }
        ]
      }
    ]
  }
}