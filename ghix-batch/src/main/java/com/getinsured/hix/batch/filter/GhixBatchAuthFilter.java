/**
 * 
 */
package com.getinsured.hix.batch.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.providers.ExpiringUsernameAuthenticationToken;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.ws_trust.saml.GhixModuleAccessToken;
import com.getinsured.hix.ws_trust.saml.GhixSecureToken;

/**
 * @author Biswakesh
 *
 */
public class GhixBatchAuthFilter implements Filter {
	
	private static final String TOKEN_DATA = "tokenData";

	private static final String TOKEN_IV = "tokenIv";

	private static final Logger LOGGER = Logger.getLogger(GhixBatchAuthFilter.class);
	
	private UserService userService;
	
	@SuppressWarnings("unused")
	private FilterConfig filterConfig;
	

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		int userId = -1;
		String tokenIv = null;
		String tokenData = null;
		
		StringBuffer logoutUrlBuf = new StringBuffer(GhixConstants.APP_URL).append("account/user/logout");
		
		AccountUser accountUser = null;
		GhixSecureToken ghixSecureToken = null;
		GhixModuleAccessToken ghixModuleAccessToken = null;
		
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		
		String serverInfo = filterConfig.getServletContext().getServerInfo();
		

			try {
				if(!isUserAuthenticated(request)) {
					tokenIv = request.getParameter(TOKEN_IV);
					tokenData = request.getParameter(TOKEN_DATA);
					
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("BATCH FILTER: USR NOT AUTHENTICATED. STARTING AUTH CHECK");
					}
				
					if(StringUtils.isNotEmpty(tokenIv) && StringUtils.isNotEmpty(tokenData)) {
						ghixSecureToken = new GhixSecureToken(tokenIv, tokenData);
						ghixModuleAccessToken = new GhixModuleAccessToken(ghixSecureToken);
						
						userService = (UserService)WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext()).getBean("userService");
						userId = ghixModuleAccessToken.getModuleAccessUser(ghixSecureToken, userService);
						
						accountUser = userService.findById(userId);
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("BATCH FILTER: TKN PRESENT PRCSSNG");
						}
						if(null != accountUser) {
							GrantedAuthority authority = new SimpleGrantedAuthority(userService
									.getDefaultRole(accountUser).getName());
							List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
							authorities.add(authority);
							Authentication authentication = new ExpiringUsernameAuthenticationToken(
									null, accountUser, null, authorities);
							SecurityContextHolder.getContext().setAuthentication(authentication);
							if(LOGGER.isInfoEnabled()) {
								LOGGER.info("BATCH FILTER: AUTH SUCCESS PROCEEDING");
							}
							chain.doFilter(request, response);
						}
					} else {
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("BATCH FILTER: TKN ABSENT LOGGING OUT");
						}
						response.sendRedirect(logoutUrlBuf.toString());
					}
				} else {
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("BATCH FILTER: AUTH ALREADY DONE PROCEEDING");
					}
					chain.doFilter(request, response);
				}
			} catch (Exception ex) {
				LOGGER.error("ERR: WHILE PRCSSN TKN: ",ex);
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("BATCH FILTER: TKN PRCSSNG FAILED LOGGING OUT");
				}
				response.sendRedirect(logoutUrlBuf.toString());
			}

		}
	
	private boolean isUserAuthenticated(HttpServletRequest request) {
		boolean isAuthenticated = false;
		String name = null;
		Authentication authentication = null;
		
		authentication = SecurityContextHolder.getContext().getAuthentication();
		
		if(null != authentication) {
			Object principal = authentication.getPrincipal();
			if(principal instanceof String) {
				name = (String)principal;
			} else if(principal instanceof AccountUser) {
				name = ((AccountUser) principal).getEmail();
			}
			if(null != name && !name.isEmpty() && !name.equalsIgnoreCase("anonymousUser")) {
				isAuthenticated = true;
			}
			
			if (request.getServletPath().startsWith("/actuator")){
				isAuthenticated = true;
			}
		}
		
		return isAuthenticated;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		this.filterConfig = null;
	}

}
