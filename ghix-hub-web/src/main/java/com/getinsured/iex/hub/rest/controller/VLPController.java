package com.getinsured.iex.hub.rest.controller;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants.VLP_DHS_ID_TYPES;
import com.getinsured.iex.hub.vlp.servicehandler.COIServiceHandler;

@Controller
public class VLPController extends MessageProcessor  {
	
	@Autowired private WebServiceTemplate vlpServiceTemplate;
	@Autowired private WebServiceTemplate reverifyServiceTemplate;
	@Autowired private WebServiceTemplate resubServiceTemplate;
	@Autowired private WebServiceTemplate closeCaseServiceTemplate;
	@Autowired private WebServiceTemplate getCaseDetailsServiceTemplate;
	@Autowired private WebServiceTemplate getCountryOfIssuanceServiceTemplate;

	private static final Logger LOGGER = LoggerFactory.getLogger(VLPController.class);
	
	private static final String SERVICE_NAME = "VLP";
	private static final String TRUE = "true";
	private static final String CASE_NUMBER = "caseNumber";
	
	@RequestMapping(value="/invokeVerifiedLawfulPresence", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeVerifiedLawfulPresence(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("VERIFY_LAWFUL_PRESENCE");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus("PENDING");
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VerifyLawfulPresence");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			String objectIdentifier = (String)requestObj.get("documentType");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			if(payloadObj == null){
				throw new HubServiceException("No payload provided for VLP Staep 1");
			}
			request.setAccessIp(clientIp);
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			if(handler == null){
				throw new HubServiceException("No handler available for VLP Step 1 service");
			}
			handler.setJsonInput(payloadObj.toJSONString());
			Map<String,Object> context = new HashMap<String, Object>();
			context.put("objectIdentifier",objectIdentifier);
			handler.setContext(context);
			response = this.executeRequest(request, handler, this.vlpServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeTestVerifiedLawfulPresence", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeTestVerifiedLawfulPresence(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("VERIFY_LAWFUL_PRESENCE");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VerifylawfulPresence");
			vlpServiceBridge.processInputParameter("alienNumber", "991122334");
			vlpServiceBridge.processInputParameter("expiryDate", "2012-01-29");
			vlpServiceBridge.processInputParameter("firstName", "Steve");
			vlpServiceBridge.processInputParameter("lastName", "Bolhman");
			vlpServiceBridge.processInputParameter("dateOfBirth", "1987-02-02");
			vlpServiceBridge.processInputParameter("comments", "Initial verification request");
			vlpServiceBridge.processInputParameter("requesterCommentsForHub", "Initial verification request");
			vlpServiceBridge.processInputParameter("requestedCoverageStartDate","2014-01-03");
			vlpServiceBridge.processInputParameter("fiveYearBarApplicabilityIndicator", TRUE);
			vlpServiceBridge.processInputParameter("requestSponsorDataIndicator", TRUE);
			vlpServiceBridge.processInputParameter("requestGrantDateIndicator", TRUE);
			vlpServiceBridge.processInputParameter("categoryCode", "C40");
			vlpServiceBridge.processInputParameter("receiptNumber","ABC4567890123");
			
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			Map<String,Object> context = new HashMap<String, Object>();
			context.put("objectIdentifier",VLP_DHS_ID_TYPES.I551);
			handler.setContext(context);
			
			response = this.executeRequest(request, handler, this.vlpServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeCloseCaseRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeCloseCaseRequest(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("CLOSE_CASE");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("CloseCaseRequest");
			//vlpServiceBridge.processInputParameter(CASE_NUMBER, "6700000000156AX");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			vlpServiceBridge.processInputParameter(CASE_NUMBER, requestObj.get(CASE_NUMBER));
			
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			response = this.executeRequest(request, handler, this.closeCaseServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}	
		
		return response;
	}
	
	@RequestMapping(value="/invokeGetCaseDetailsRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeGetCaseDetailsRequest(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("GET_CASE_DETAILS");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("GetCaseDetailsRequest");
			//vlpServiceBridge.processInputParameter("caseNbr", "6700000000156AX");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			vlpServiceBridge.processInputParameter("caseNbr", requestObj.get(CASE_NUMBER));
			
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			response = this.executeRequest(request, handler, this.getCaseDetailsServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}		
		
		return response;
	}
	
	@RequestMapping(value="/invokeReverifyAgencyRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeReverifyAgencyRequest(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("REVERIFY_AGENCY_REQUEST");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("ReverifyAgencyRequest");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			request.setAccessIp(clientIp);
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			handler.setJsonInput(payloadObj.toJSONString());
			response = this.executeRequest(request, handler, this.reverifyServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeTestReverifyAgencyRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeTestReverifyAgencyRequest(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("REVERIFY_AGENCY_REQUEST");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			HubServiceBridge reverifyServiceBridge = HubServiceBridge.getHubServiceBridge("ReverifyAgencyRequest");
			reverifyServiceBridge.processInputParameter(CASE_NUMBER,"9901009990000CC");
			reverifyServiceBridge.processInputParameter("receiptNumber","AAA1200850001");
			reverifyServiceBridge.processInputParameter("alienNumber", "321654987");
			reverifyServiceBridge.processInputParameter("sevisid", "0000110429");
			reverifyServiceBridge.processInputParameter("passportNumber","59N199754");
			reverifyServiceBridge.processInputParameter("countryOfIssuance","MEXIC");
			reverifyServiceBridge.processInputParameter("citizenshipNumber","552559412");
			reverifyServiceBridge.processInputParameter("visaNumber","76254237");
			reverifyServiceBridge.processInputParameter("firstName", "Rosa");
			reverifyServiceBridge.processInputParameter("lastName", "Villa");
			reverifyServiceBridge.processInputParameter("dateOfBirth","1976-05-05");
			reverifyServiceBridge.processInputParameter("requestedCoverageStartDate", "2012-01-29");
			reverifyServiceBridge.processInputParameter("fiveYearBarApplicabilityIndicator", TRUE);
			reverifyServiceBridge.processInputParameter("requesterCommentsForHub", "Resubmitting the application");
			reverifyServiceBridge.processInputParameter("categoryCode", "C49");
			
			ServiceHandler handler = reverifyServiceBridge.getServiceHandler();
			response = this.executeRequest(request, handler, this.reverifyServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}	
		
		return response;
	}
	
	@RequestMapping(value="/invokeResubAgencyRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeResubAgencyRequest(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("RESUB_AGENCY_REQUEST");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("ResubAgencyRequest");
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(vlpRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get("clientIp");
			JSONObject payloadObj = (JSONObject)requestObj.get("payload");
			request.setAccessIp(clientIp);
			ServiceHandler handler = vlpServiceBridge.getServiceHandler();
			handler.setJsonInput(payloadObj.toJSONString());
			response = this.executeRequest(request, handler, this.resubServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeTestResubAgencyRequest", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeTestResubAgencyRequest(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("RESUB_AGENCY_REQUEST");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			HubServiceBridge resubServiceBridge = HubServiceBridge.getHubServiceBridge("ResubAgencyRequest");
			resubServiceBridge.processInputParameter(CASE_NUMBER,"1101099980000CC");
			resubServiceBridge.processInputParameter("sevisId", "9885989925");
			resubServiceBridge.processInputParameter("requestedCoverageStartDate","2014-01-03");
			resubServiceBridge.processInputParameter("fiveYearBarApplicabilityIndicator", TRUE);
			resubServiceBridge.processInputParameter("requesterCommentsForHub", "submit agency3 request");
			resubServiceBridge.processInputParameter("categoryCode", "C40");
			
			ServiceHandler handler = resubServiceBridge.getServiceHandler();
			response = this.executeRequest(request, handler, this.resubServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}	
		
		return response;
	}
	
	@RequestMapping(value="/getCountryOfIssuanceList", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> getCountryOfIssuanceList(@RequestBody String vlpRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("GET_COI_LIST");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		
		ResponseEntity<String> response = null;
		
		try{
			ServiceHandler handler = new COIServiceHandler();
			response = this.executeRequest(request, handler, this.getCountryOfIssuanceServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}	
		
		return response;
	}
	
}
