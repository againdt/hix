package com.getinsured.iex.hub.vci.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.PayPeriodInfoType;
import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.PayPeriodInformationType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.AnnualCompensationInformationType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class PayPeriodInfoTypeWrapper implements JSONAware{
	
	
	private List<PayPeriodInformationTypeWrapper> payPeriodInformation;
	
    private static final String PAY_PERIOD_KEY = "PayPeriodInformation";
    
	public PayPeriodInfoTypeWrapper(PayPeriodInfoType payPeriodInfoType){
		
		if(payPeriodInfoType == null){
			return;
		}
		
		for(PayPeriodInformationType payPeriod : payPeriodInfoType.getPayPeriodInformation()){
			getPayPeriodInformation().add(new PayPeriodInformationTypeWrapper(payPeriod));
		}
		
	}
	
	public List<PayPeriodInformationTypeWrapper> getPayPeriodInformation() {
        if (payPeriodInformation == null) {
            payPeriodInformation = new ArrayList<PayPeriodInformationTypeWrapper>();
        }
        return this.payPeriodInformation;
    }
	
	@SuppressWarnings("unchecked")
   	public String toJSONString() {
   		JSONObject obj = new JSONObject();
   		
   		JSONArray array = new JSONArray();
   		for(PayPeriodInformationTypeWrapper payPeriod : this.payPeriodInformation){
   			array.add(payPeriod);
   		}
   		obj.put(PAY_PERIOD_KEY, array);
   		
   		return obj.toJSONString();
   	}
	
}
