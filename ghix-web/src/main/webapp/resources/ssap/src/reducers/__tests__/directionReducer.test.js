
import directionReducer, { initialState } from '../directionReducer';
import { switchDirectionsActionTypes } from '../../constants/actionTypes';

const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const changeDirectionAction = {
    type: switchDirectionsActionTypes.CHANGE_DIRECTION,
    direction: 'increase'
};

const clearStateAction = {
    type: switchDirectionsActionTypes.CLEAR_STATE
};


describe('Test for directionReducer', () => {
    it('This reducer hasn\'t been invoked', () => {
        expect(directionReducer(initialState, anotherAction)).toBe(initialState);
    });

    it('Should switch direction', () => {
        expect(directionReducer(initialState, changeDirectionAction)).toBe(changeDirectionAction.direction);
    });

    it('Should clear state', () => {
        expect(directionReducer({ welcomeText: 'Hi there!' }, clearStateAction)).toBe(initialState);
    });
});