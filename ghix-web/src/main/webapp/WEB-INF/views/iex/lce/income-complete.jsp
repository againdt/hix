<script type="text/ng-template" id="income-complete.jsp">
  <div class="gutter10">
    <h3><spring:message code="label.lce.income"/>: <spring:message code="label.lce.completed"/></h3>
    <spring:message code="label.lce.yourinformationwillbesaved"/>

    <!-- BACK & NEXT BUTTON -->
     <dl class="dl-horizontal pull-right">
      <!-- <a class="btn btn-secondary" ng-click="back()">Back</a> --> <!-- on summary pages we are not showing backButton -->
      <a class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/> {{nextButtonLifeEvent}}</a>
    </dl>
  </div>
</script>