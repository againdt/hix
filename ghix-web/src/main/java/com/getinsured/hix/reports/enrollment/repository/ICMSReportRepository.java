package com.getinsured.hix.reports.enrollment.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.enrollment.EnrollmentCmsOut;
import com.getinsured.hix.reports.enrollment.dto.CMSErrorReportDTO;

@Repository
public interface ICMSReportRepository extends JpaRepository<EnrollmentCmsOut, Long> {
	
	@Query("select new com.getinsured.hix.reports.enrollment.dto.CMSErrorReportDTO( o.month, o.year, o.fileName, o.inboundAction, o.hiosIssuerId,	i.fileStatus,"
			+ " d.exchAssignPolicyId,	d.issuerAssignSubsriberId,	d.errorCode, d.errorDesc, d.elementInError,	d.additionalErrorInfo,"
			+ " d.createdOn ) FROM EnrollmentCmsOut o, EnrollmentCmsIn i join i.enrollmentCmsInDtlList d WHERE o.fileName = i.outFileName AND o.inboundAction != 'INACTIVE'" 
			+ " order by d.createdOn DESC")
	Page<CMSErrorReportDTO> getCMSErrorReport(Pageable p);
}
