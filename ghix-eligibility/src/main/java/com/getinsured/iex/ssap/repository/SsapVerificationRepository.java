package com.getinsured.iex.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapVerification;

@RestResource(path="SsapVerification")
public interface SsapVerificationRepository  extends JpaRepository<SsapVerification, Long> {

	List<SsapVerification> findBySsapApplicant(@Param("ssapApplicant") SsapApplicant ssapApplicant);



	@Query("Select sv " +
			" FROM SsapVerification as sv "+
			" inner join fetch sv.ssapVerificationGiwspayloads as svGiPayloads"+
			" inner join fetch sv.ssapApplicant as ssapApplicant"+
			" where ssapApplicant.id = :ssapApplicantId ")
	List<SsapVerification> getVerifications(@Param("ssapApplicantId") Long ssapApplicantId);

}
