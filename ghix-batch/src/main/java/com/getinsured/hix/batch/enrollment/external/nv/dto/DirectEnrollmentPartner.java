package com.getinsured.hix.batch.enrollment.external.nv.dto;

public class DirectEnrollmentPartner {
	 private String partnerIdentifier;
	 private String partnerType;


	 // Getter Methods 

	 public String getPartnerIdentifier() {
	  return partnerIdentifier;
	 }

	 public String getPartnerType() {
	  return partnerType;
	 }

	 // Setter Methods 

	 public void setPartnerIdentifier(String partnerIdentifier) {
	  this.partnerIdentifier = partnerIdentifier;
	 }

	 public void setPartnerType(String partnerType) {
	  this.partnerType = partnerType;
	 }
	}
