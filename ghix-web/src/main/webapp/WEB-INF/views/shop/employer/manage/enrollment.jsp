<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/shop-enrollment-data" prefix="enrollData"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="gutter10">
<div class="row-fluid">
	<ul class="breadcrumb txt-left">
		<li><a href="#">Employees</a> <span class="divider">/</span></li>
		<li><a href="/hix/shop/employer/manage/list">Manage List</a> <span
			class="divider">/</span></li>
		<li class="active">Edit</li>
	</ul>
	<h3 id="skip">${employee.name} 
		<small>
				<c:set var="status" value="${fn:toUpperCase(fn:substring(employee.status, 0, 1))}${fn:toLowerCase(fn:substring(employee.status, 1, -1))}" />
				${fn:replace(status,'_',' ')}
		</small>
	</h3>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
<div class="accordion graysidebar" id="accordion2">
          <div class="accordion-group">
            <div class="accordion-heading graydrkbg"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"> About this Employee</a> </div>
            <div id="collapseOne" class="accordion-body collapse in">
              <div class="accordion-inner">
                <ul class="nav nav-list">
                  <li><a href="<c:url value="/shop/employer/manage/employeeinfo/${employee.id}?appId=${empappID}" />">Employee Information</a></li>
                  <li><a href="<c:url value="/shop/employer/manage/dependents/${employee.id}?appId=${empappID}" /> ">Dependent Information (${dependentCount})</a></li>
                  <li class="active"><a>Employee Coverage</a></li>
                <%--   <li><a href="<c:url value="/shop/employer/manage/notifications/${employee.id}" />">Notification History	</a></li> --%>
                 </ul>
              </div>
            </div>
          </div>
        </div> 
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
      <h4 class="graydrkbg gutter10"><spring:message  code='label.empcoverage'/></h4>
          <div class="gutter10">
          <table >
          <tbody><tr>
          <td><b><c:if test="${(employeeApplication.status == 'ENROLLED')}"><spring:message  code='label.enrollmentperoid'/></c:if></b></td>
          </tr>
          <tr>
          <td>
		 <c:choose>
			<c:when test="${(employeeApplication.status == 'ENROLLED') }">
				<%-- <c:forEach items="${enrollmentList}" varStatus="status" var="enrollmentList"> --%>
					${employeeApplication.employerEnrollment.openEnrollmentStart} - ${employeeApplication.employerEnrollment.openEnrollmentEnd}
				<%-- </c:forEach> --%>
				
				<enrollData:view employeeApplication="${employeeApplication}" showtermoption="false" ></enrollData:view>
			</c:when>
			<c:otherwise>
			Not currently enrolled
			</c:otherwise>
		 </c:choose>          
          </td>
          </tr>
          <c:if test="${employeeApplication.status=='WAIVED'}">
          <tr>
          <td><b><spring:message  code='label.waivedhealthcov'/></b></td>
          </tr>
          <tr>
          <td><spring:message  code='label.reason'/>: ${employeeApplication.statusNotes}</td>
          </tr>
          </c:if>
          </tbody></table>
        </div>
        <!--gutter10--> 
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
</div>
</div> <!--gutter10--> 
<script type="text/javascript">
function imgSize(){
    $(".carrierlogo").each(function(){
		$(this).css('display','inline-block');
    });
}

$(function() {
	  $("#viewBenifitDetails").live('click',function(e) {
		  		$('#plandetails').remove();
	            e.preventDefault();
	            var href = $(this).attr('href') + "?iframe=no";
	            if (href.indexOf('#') != 0) {
	              $(
	                  '<div id="plandetails" class="modal bigModal"><div class="searchModal-header gutter10-lr"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class=""><iframe id="search" src="' + href + '" class="searchModal-body"></iframe></div></div>').modal();
	            }
	          });
	});

	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$('#plandetails').modal('hide');
		}  
	});
</script>
