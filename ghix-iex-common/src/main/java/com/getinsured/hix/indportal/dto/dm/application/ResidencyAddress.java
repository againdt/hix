package com.getinsured.hix.indportal.dto.dm.application;

public class ResidencyAddress {
	 private String residencyAddressSourceType;
	 private String streetName1;
	 private String cityName;
	 private String stateCode;
	 private String zipCode;
	 private String countyName;
	 private String countyFipsCode;


	 // Getter Methods 

	 public String getResidencyAddressSourceType() {
	  return residencyAddressSourceType;
	 }

	 public String getStreetName1() {
	  return streetName1;
	 }

	 public String getCityName() {
	  return cityName;
	 }

	 public String getStateCode() {
	  return stateCode;
	 }

	 public String getZipCode() {
	  return zipCode;
	 }

	 public String getCountyName() {
	  return countyName;
	 }

	 public String getCountyFipsCode() {
	  return countyFipsCode;
	 }

	 // Setter Methods 

	 public void setResidencyAddressSourceType(String residencyAddressSourceType) {
	  this.residencyAddressSourceType = residencyAddressSourceType;
	 }

	 public void setStreetName1(String streetName1) {
	  this.streetName1 = streetName1;
	 }

	 public void setCityName(String cityName) {
	  this.cityName = cityName;
	 }

	 public void setStateCode(String stateCode) {
	  this.stateCode = stateCode;
	 }

	 public void setZipCode(String zipCode) {
	  this.zipCode = zipCode;
	 }

	 public void setCountyName(String countyName) {
	  this.countyName = countyName;
	 }

	 public void setCountyFipsCode(String countyFipsCode) {
	  this.countyFipsCode = countyFipsCode;
	 }
	}
