<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@page import="com.getinsured.hix.model.DesignateBroker"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript">
	$(function() {
		$("#pop_findAgent, #pop_findLocalAssistance, #pop_getAssistance").click(function(e) {
			e.preventDefault();
			var href = '<c:out value="${completeURL}"/>';
			//console.log("hre"+href);
			openFindBrokerDialog(href);
		});
	});
		
	function openFindBrokerDialog(href){
		$('<div id="brokersearchBox" class="modal" tabindex="-1" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="searchModal-header"> <button type="button" class="agentClose close"><span aria-hidden="true">&times;</span><span class="aria-hidden">close</span></button></div><div><iframe id="search" src="' + href + '" class="searchModal-body"></iframe></div></div></div></div>').modal({show: true});
	}

	$(function() {
	  $("#incomeConsentYearsVal").change(function() {
	 	$("#submit").removeAttr("disabled");
		$("#thankyou-note").hide();
	  });

	  $("input[name='renewalConsent']").click(function() {
		$("#submit").removeAttr("disabled");
		$("#thankyou-note").hide();
	  });
	});
</script>

<%
String preventAddConsumer = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.GLOBAL_PREVENT_ADD_CONSUMER); 
String isConsentPageEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_IS_CONSENT_PAGE_ENABLED);
%>
<c:set var="preventAddConsumer" value="<%=preventAddConsumer%>" />
<c:set var="isConsentPageEnabled" value="<%=isConsentPageEnabled%>" />

<link rel="stylesheet" type="text/css" href="<gi:cdnurl value="/resources/css/general.css" />" media="screen" />

<c:choose>
<c:when test="${preventAddConsumer == 'true' && isConsentPageEnabled == 'N'}">
    <div class="landing-welcome">
        <div class="row-fluid">
            <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <h1>Welcome, ${firstName}&nbsp;${lastName}</h1>
                    <div class="register-welcome landing-signup clearfix">
                        <p>
                        Thank you for activating your Nevada Health Link account. Open enrollment for 2020 coverage begins
                        on November 1, 2019 and runs through December 15, 2019. You can window shop for 2020 health plans
                        now to see what options will be available in your area.
                        </p>
                        <div class="shopbutton-wrapper txt-center">
                            <a class="btn btn-fullwidth-xs" href="<c:url value='/preeligibility' />">Start Shopping <i class="icon-large icon-arrow-right"></i></a>
                        </div>
                </div>
            </div>
</c:when>
<c:otherwise>
  
<c:url value="/broker/search" var="completeURL">
   <c:param name="anonymousFlag" value=""/>
</c:url>

<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
	
	<c:if test="${incomeConsentChanged == true || renewalConsentChanged == true}">
		<div class="row-wrapper">
			<div class="alert alert-info" id="thankyou-note">
		 		<p><spring:message code="indportal.consent.editable.one"/></p>
		 		<p><spring:message code="indportal.consent.editable.two"/></p>
		 		<p><a class="btn btn-primary btn-fullwidth-xs" href="<c:url value='/preeligibility' />">Start Shopping</a></p>
		 	</div>
		 </div>		
 	</c:if>
 		
	<div class="header">
		<h4 class="pull-left" id="pageTitle">Welcome to Nevada Health Link</h4>
	</div>
		<div class="row-wrapper gutter10">
			<c:choose>
		 		<c:when test="${incomeConsentChanged == null && incomeConsent == 'true' && renewalConsentChanged == null && renewalConsent == 'true'}">
					<p><spring:message code="indportal.consent.welcome.noneditable"/></p>
				</c:when>
				<c:otherwise>
					<p><spring:message code="indportal.consent.welcome.editable.one"/></p>
					<p><spring:message code="indportal.consent.welcome.editable.two"/></p>
				</c:otherwise>
			</c:choose>
		</div>

        <div class="header">
            <h4 id="pageTitle">Get Free Help</h4>
        </div>

        <div class="row-wrapper gutter10">
        <c:choose>
            <c:when test="${dbProfileConsent != null && dbConsent!=null && dbConsent.status != 'InActive'}">
                    <c:if test="${brokerChanged != true}">
                    	<p><span><spring:message code="indportal.consent.ffm.broker" /></span></p>
                    </c:if>
                    <span class="strong">Broker Name: ${dbProfileConsent.user.firstName} ${dbProfileConsent.user.lastName}</span><br>
                    <c:if test="${dbProfileConsent.contactNumber != null}">
                        <span class="strong">Phone Number: (${fn:substring(dbProfileConsent.contactNumber,0,3)}) ${fn:substring(dbProfileConsent.contactNumber,3,12)}</span><br>
                    </c:if>
                    <span class="strong">E-mail: ${dbProfileConsent.user.email} </span>
            </c:when>
            <c:when test="${daProfileConsent != null && daConsent!=null && daConsent.status != 'InActive'}">
                    <c:if test="${brokerChanged != true}">
                    	<p><span><spring:message code="indportal.consent.ffm.broker"/></span></p>
                    </c:if>
                    <span class="strong">Broker Name: ${daProfileConsent.firstName} ${daProfileConsent.lastName}</span><br>
                    <c:if test="${daProfileConsent.primaryPhoneNumber != null}">
                        <span class="strong">Phone Number: (${fn:substring(daProfileConsent.primaryPhoneNumber,0,3)}) ${fn:substring(daProfileConsent.primaryPhoneNumber,3,12)}</span><br>
                    </c:if>
                    <span class="strong">E-mail: ${daProfileConsent.emailAddress} </span>
            </c:when>
            <c:otherwise>
                <p><span><spring:message code="indportal.nobroker.message.one" /></span></p>
                <p><span><spring:message code="indportal.nobroker.message.two" /></span></p>
                <p><a id="pop_findAgent" class="btn btn-primary btn-fullwidth-xs" href="#">Find Free Local Assistance</a></p>
            </c:otherwise>
        </c:choose>
        </div>
        
<form class="form-horizontal" id="frmConsent" name="frmConsent" action="<c:url value="/indportal/consent/submit"/>" method="POST">
		<df:csrfToken/>
		<input type="hidden" name="ssapApplicationId" id="ssapApplicationId" value="<encryptor:enc value="${ssapApplicationId}"/>" />
		<div class="header">
			<h4 class="pull-left" id="pageTitle">Consent</h4>
		</div>
			<div class="row-wrapper gutter10">
			<h5 class="strong">Automatic Renewal</h5>
			 <c:choose>
			 	<c:when test="${renewalConsentChanged == null && renewalConsent == 'true'}">
			 		<p><spring:message code="indportal.renewal.ffm.consent.hub" /></p>
			 	</c:when>
			 	<c:otherwise>
			 		<p><spring:message code="indportal.renewal.noffm.consent.hub" /></p>
			 		<div class="form-group strong">
						<input class="radio-inline" type="radio" name="renewalConsent" id="renewalConsent" value="true" <c:if test="${renewalConsent == true}"> checked="checked" </c:if> /> I Agree &nbsp;
  	        			<input class="radio-inline" type="radio" name="renewalConsent" id="renewalConsent" value="false" <c:if test="${renewalConsent == false}"> checked="checked" </c:if> /> I Disagree &nbsp;
						<div id="renewalConsent_error"></div>
					</div>
			 	</c:otherwise>
			 </c:choose>
		</div>
		
		<div class="row-wrapper gutter10">
		<c:if test="${financialAssistanceFlag == 'Y'}">
			<h5 class="strong">Verification of Tax Credit Eligibility</h5>
			
			 <c:choose>
			 	<c:when test="${incomeConsentChanged == null && incomeConsent == 'true'}">
			 		<p><spring:message code="indportal.finance.ffm.consent.hub" /></p>
			 	</c:when>
			 	<c:otherwise>
			 		<p><spring:message code="indportal.finance.noffm.consent.hub.one" /></p>
			 		<p><spring:message code="indportal.finance.noffm.consent.hub.two" /></p>
			 		
				 	<div class="form-group">
						<select class="input-large" name="incomeConsentYearsVal" id="incomeConsentYearsVal">
							<option value="5" <c:if test="${incomeConsentYearsDB == '' || incomeConsentYearsDB == '5'}"> selected </c:if>>5 Years</option>
							<option value="4" <c:if test="${incomeConsentYearsDB == '4'}"> selected </c:if>>4 Years</option>
							<option value="3" <c:if test="${incomeConsentYearsDB == '3'}"> selected </c:if>>3 Years</option>
							<option value="2" <c:if test="${incomeConsentYearsDB == '2'}"> selected </c:if>>2 Years</option>
							<option value="1" <c:if test="${incomeConsentYearsDB == '1'}"> selected </c:if>>1 Years</option>
							<option value="0" <c:if test="${incomeConsentYearsDB == '0'}"> selected </c:if>>Do not check</option>
						</select>
					</div>
			 	</c:otherwise>
			 </c:choose>
		</c:if>
		</div>
		
		
		<c:choose>
			<c:when test="${incomeConsentChanged == null && incomeConsent == 'true' && renewalConsentChanged == null && renewalConsent == 'true'}">
				<div class="row-wrapper">
					<p><spring:message code="indportal.consent.non.editable" /></p>
					<p><a class="btn btn-primary btn-fullwidth-xs" href="<c:url value='/preeligibility' />">Start Shopping</a></p>		
				</div>
			</c:when>
			<c:otherwise>
				<div class="row-wrapper gutter10" >
					<div class="form-group">
						<input type="submit" id="submit" name="submit" class="btn btn-primary btn-fullwidth-xs pull-right" disabled value="Update Consent"/>
					</div>
				</div>
			</c:otherwise>
		</c:choose>
	</form>
</div>
</c:otherwise>
</c:choose>
<script type="text/javascript">
var validator = $("#frmConsent").validate({
	ignore: ':hidden:not("#frmConsent")',
	onSubmit : true,
	rules: {
	  renewalConsent: "required"
	},
	messages : {
		renewalConsent :{
			required : "<span> <em class='excl'>!</em>Please select renewal consent</span>"
		}
	},
	errorClass : "error",
	errorPlacement : function(error, element) {
		var elementId = element.attr('id');
		error.appendTo($("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class', 'error');
	}
});
</script>