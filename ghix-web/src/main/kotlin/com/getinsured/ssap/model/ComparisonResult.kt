package com.getinsured.ssap.model

/**
 * Comparison Result types.
 *
 * @author Yevgen Golubenko
 * @since 8-10-2019
 */
enum class ComparisonResult {
  /**
   * Result means that SSAC verifications needs to be re-executed.
   */
  SSAC,

  /**
   * Result means that VLP verifications needs to be re-executed.
   */
  VLP,

  /**
   * Result means that IFSV verifications needs to be re-executed.
   */
  IFSV,

  /**
   * Result means that Non-ESI MEC verifications needs to be re-executed.
   */
  NON_ESI_MEC,

  /**
   * Means none of the verifications needs to be re-executed.
   */
  NONE
}
