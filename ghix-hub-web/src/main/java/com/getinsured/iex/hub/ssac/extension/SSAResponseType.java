
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.codes_schema.DeathConfirmationCodeSimpleType;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;



/**
 * A data type for the SSA related responses.
 * 
 * <p>Java class for SSAResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSAResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSNVerificationIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}DeathConfirmationCode" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonUSCitizenIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PersonIncarcerationInformationIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSATitleIIMonthlyIncomeInformationIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSATitleIIAnnualIncomeInformationIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSAQuartersOfCoverageInformationIndicator" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSAIncarcerationInformation" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSATitleIIMonthlyIncome" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSATitleIIYearlyIncome" minOccurs="0"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}SSAQuartersOfCoverage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSAResponseType", propOrder = {
    "ssnVerificationIndicator",
    "deathConfirmationCode",
    "personUSCitizenIndicator",
    "personIncarcerationInformationIndicator",
    "ssaTitleIIMonthlyIncomeInformationIndicator",
    "ssaTitleIIAnnualIncomeInformationIndicator",
    "ssaQuartersOfCoverageInformationIndicator",
    "ssaIncarcerationInformation",
    "ssaTitleIIMonthlyIncome",
    "ssaTitleIIYearlyIncome",
    "ssaQuartersOfCoverage"
})
public class SSAResponseType
    extends ComplexObjectType
{

    @XmlElement(name = "SSNVerificationIndicator")
    protected Boolean ssnVerificationIndicator;
    @XmlElement(name = "DeathConfirmationCode")
    protected DeathConfirmationCodeSimpleType deathConfirmationCode;
    @XmlElement(name = "PersonUSCitizenIndicator", namespace = "http://niem.gov/niem/niem-core/2.0", nillable = true)
    protected Boolean personUSCitizenIndicator;
    @XmlElement(name = "PersonIncarcerationInformationIndicator")
    protected Boolean personIncarcerationInformationIndicator;
    @XmlElement(name = "SSATitleIIMonthlyIncomeInformationIndicator")
    protected Boolean ssaTitleIIMonthlyIncomeInformationIndicator;
    @XmlElement(name = "SSATitleIIAnnualIncomeInformationIndicator")
    protected Boolean ssaTitleIIAnnualIncomeInformationIndicator;
    @XmlElement(name = "SSAQuartersOfCoverageInformationIndicator")
    protected Boolean ssaQuartersOfCoverageInformationIndicator;
    @XmlElement(name = "SSAIncarcerationInformation")
    protected SSAIncarcerationInformationType ssaIncarcerationInformation;
    @XmlElement(name = "SSATitleIIMonthlyIncome")
    protected SSATitleIIMonthlyIncomeType ssaTitleIIMonthlyIncome;
    @XmlElement(name = "SSATitleIIYearlyIncome")
    protected SSATitleIIYearlyIncomeType ssaTitleIIYearlyIncome;
    @XmlElement(name = "SSAQuartersOfCoverage")
    protected SSAQuartersOfCoverageType ssaQuartersOfCoverage;

    /**
     * Gets the value of the ssnVerificationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSNVerificationIndicator() {
        return ssnVerificationIndicator;
    }

    /**
     * Sets the value of the ssnVerificationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSNVerificationIndicator(Boolean value) {
        this.ssnVerificationIndicator = value;
    }

    /**
     * Gets the value of the deathConfirmationCode property.
     * 
     * @return
     *     possible object is
     *     {@link DeathConfirmationCodeSimpleType }
     *     
     */
    public DeathConfirmationCodeSimpleType getDeathConfirmationCode() {
        return deathConfirmationCode;
    }

    /**
     * Sets the value of the deathConfirmationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeathConfirmationCodeSimpleType }
     *     
     */
    public void setDeathConfirmationCode(DeathConfirmationCodeSimpleType value) {
        this.deathConfirmationCode = value;
    }

    /**
     * Gets the value of the personUSCitizenIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPersonUSCitizenIndicator() {
        return personUSCitizenIndicator;
    }

    /**
     * Sets the value of the personUSCitizenIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonUSCitizenIndicator(Boolean value) {
        this.personUSCitizenIndicator = value;
    }

    /**
     * Gets the value of the personIncarcerationInformationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPersonIncarcerationInformationIndicator() {
        return personIncarcerationInformationIndicator;
    }

    /**
     * Sets the value of the personIncarcerationInformationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonIncarcerationInformationIndicator(Boolean value) {
        this.personIncarcerationInformationIndicator = value;
    }

    /**
     * Gets the value of the ssaTitleIIMonthlyIncomeInformationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSATitleIIMonthlyIncomeInformationIndicator() {
        return ssaTitleIIMonthlyIncomeInformationIndicator;
    }

    /**
     * Sets the value of the ssaTitleIIMonthlyIncomeInformationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSATitleIIMonthlyIncomeInformationIndicator(Boolean value) {
        this.ssaTitleIIMonthlyIncomeInformationIndicator = value;
    }

    /**
     * Gets the value of the ssaTitleIIAnnualIncomeInformationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSATitleIIAnnualIncomeInformationIndicator() {
        return ssaTitleIIAnnualIncomeInformationIndicator;
    }

    /**
     * Sets the value of the ssaTitleIIAnnualIncomeInformationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSATitleIIAnnualIncomeInformationIndicator(Boolean value) {
        this.ssaTitleIIAnnualIncomeInformationIndicator = value;
    }

    /**
     * Gets the value of the ssaQuartersOfCoverageInformationIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getSSAQuartersOfCoverageInformationIndicator() {
        return ssaQuartersOfCoverageInformationIndicator;
    }

    /**
     * Sets the value of the ssaQuartersOfCoverageInformationIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSSAQuartersOfCoverageInformationIndicator(Boolean value) {
        this.ssaQuartersOfCoverageInformationIndicator = value;
    }

    /**
     * Gets the value of the ssaIncarcerationInformation property.
     * 
     * @return
     *     possible object is
     *     {@link SSAIncarcerationInformationType }
     *     
     */
    public SSAIncarcerationInformationType getSSAIncarcerationInformation() {
        return ssaIncarcerationInformation;
    }

    /**
     * Sets the value of the ssaIncarcerationInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSAIncarcerationInformationType }
     *     
     */
    public void setSSAIncarcerationInformation(SSAIncarcerationInformationType value) {
        this.ssaIncarcerationInformation = value;
    }

    /**
     * Gets the value of the ssaTitleIIMonthlyIncome property.
     * 
     * @return
     *     possible object is
     *     {@link SSATitleIIMonthlyIncomeType }
     *     
     */
    public SSATitleIIMonthlyIncomeType getSSATitleIIMonthlyIncome() {
        return ssaTitleIIMonthlyIncome;
    }

    /**
     * Sets the value of the ssaTitleIIMonthlyIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSATitleIIMonthlyIncomeType }
     *     
     */
    public void setSSATitleIIMonthlyIncome(SSATitleIIMonthlyIncomeType value) {
        this.ssaTitleIIMonthlyIncome = value;
    }

    /**
     * Gets the value of the ssaTitleIIYearlyIncome property.
     * 
     * @return
     *     possible object is
     *     {@link SSATitleIIYearlyIncomeType }
     *     
     */
    public SSATitleIIYearlyIncomeType getSSATitleIIYearlyIncome() {
        return ssaTitleIIYearlyIncome;
    }

    /**
     * Sets the value of the ssaTitleIIYearlyIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSATitleIIYearlyIncomeType }
     *     
     */
    public void setSSATitleIIYearlyIncome(SSATitleIIYearlyIncomeType value) {
        this.ssaTitleIIYearlyIncome = value;
    }

    /**
     * Gets the value of the ssaQuartersOfCoverage property.
     * 
     * @return
     *     possible object is
     *     {@link SSAQuartersOfCoverageType }
     *     
     */
    public SSAQuartersOfCoverageType getSSAQuartersOfCoverage() {
        return ssaQuartersOfCoverage;
    }

    /**
     * Sets the value of the ssaQuartersOfCoverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSAQuartersOfCoverageType }
     *     
     */
    public void setSSAQuartersOfCoverage(SSAQuartersOfCoverageType value) {
        this.ssaQuartersOfCoverage = value;
    }

}
