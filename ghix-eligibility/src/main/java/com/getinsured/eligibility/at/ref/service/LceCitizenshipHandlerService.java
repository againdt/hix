package com.getinsured.eligibility.at.ref.service;


import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;

/**
 * @author chopra_s
 * 
 */
@Component("lceCitizenshipHandlerService")
@Scope("singleton")
public class LceCitizenshipHandlerService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceCitizenshipHandlerService.class);

	@Autowired
	@Qualifier("lceAppExtensionEvent")
	private LceAppExtensionEvent lceAppExtensionEvent;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceCitizenshipHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		final Map<String, ApplicantEvent> applicantExtensionEvent = createSepEvent(currentApplication, lceProcessRequestDTO);
		updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);//HIX-108047
		
		updateAllowEnrollment(currentApplication, Y);
		final boolean deniedCase = checkEventDenied(currentApplication, applicantExtensionEvent);
		if (deniedCase) {
			LOGGER.info("Current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " is denied.");
			processDenial(currentApplication.getCaseNumber());
		} else {
			// trigger email for loss / gain of citizenship
			triggerEmailForChangeInCitizenship(currentApplication,lceProcessRequestDTO);			
		}
		super.closePreviousERApplication(currentApplication,lceProcessRequestDTO);
		LOGGER.info("LceCitizenshipHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	/**
	 * A method to trigger email for change in citizenship
	 * @param currentApplication
	 * @param lceProcessRequestDTO
	 */
	private void triggerEmailForChangeInCitizenship(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		// TODO : the below notice will be triggered in case of MN - we need to remove
		// this once content is finalized
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if (stateCode.equalsIgnoreCase("MN")) {
			// trigger LCE04 notice
			LOGGER.info("Trigger Email - Notice LCE04");
			triggerSEPEmail(currentApplication);
		} else {
			LOGGER.info("Trigger Email - Notice EE057");
			triggerChangeActionEmail(currentApplication, lceProcessRequestDTO, true);
		}
		
	}

	private boolean checkEventDenied(SsapApplication currentApplication, Map<String, ApplicantEvent> applicantExtensionEvent) {
		boolean isDenied = false;
		final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			isDenied = ApplicationExtensionEventUtil.checkSingleEventDenial(ssapApplicant, applicantExtensionEvent);
			if (isDenied) {
				break;
			}
		}
		return isDenied;
	}

	private Map<String, ApplicantEvent> createSepEvent(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("Create Citizenship event - " + currentApplication.getId());
		return lceAppExtensionEvent.createCitizenshipEvent(currentApplication, lceProcessRequestDTO.getApplicationExtension());
	}
}
