package com.getinsured.hix.batch.enrollment.service;

import static org.apache.commons.lang3.StringUtils.isNoneBlank;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.hix.batch.enrollment.util.EnrollmentReconciliationJobThread;
import com.getinsured.hix.dto.enrollment.EnrlReconSummaryUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSftpFileTransferDTO;
import com.getinsured.hix.enrollment.repository.IEnrlReconDataRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconErrorRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSummaryRepository;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.service.ReconciliationEmailNotificationService;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentSftpUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrlReconData;
import com.getinsured.hix.model.enrollment.EnrlReconError;
import com.getinsured.hix.model.enrollment.EnrlReconSummary;
import com.getinsured.hix.model.enrollment.EnrlReconSummary.SummaryStatus;
import com.getinsured.hix.model.enrollment.EnrollmentCmsFileTransferLog;
import com.getinsured.hix.model.enrollment.ReconDetailCsvDto;
import com.getinsured.hix.model.enrollment.ReconSummeryCsvDto;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.JiraUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
/**
 * 
 * @author meher_a
 *
 */
@Service("enrollmentReconciliationService")
public class EnrollmentReconciliationServiceImpl implements EnrollmentReconciliationService{
private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentReconciliationServiceImpl.class);
	
	private static final String JIRA_MESSAGE_BATCH_TRIGGER = "Unable to trigger GenerateSnapshot job from ReconPopulate Job (EnrollmentReconciliationService) ";
	private static final String SPACE = " ";
	
	@Autowired(required = true)
	private IEnrlReconDataRepository enrlReconDataRepository;
	
	@Autowired(required = true)
	private IEnrlReconErrorRepository enrlReconErrorRepository;
	
	@Autowired(required = true)
	private IEnrlReconSummaryRepository enrlReconSummaryRepository;
	
	@Autowired private 
	EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	
	@Autowired
	private EnrollmentBatchService enrollmentBatchService;
	
	@Autowired	
	private ReconciliationEmailNotificationService reconciliationEmailNotificationService;
	
	@Autowired 
	private CmsSftpService cmsSftpService;
	
	@Autowired 
	private Gson platformGson;
	
	@Value("#{configProp['enrollment.exchg.sftp.reconfolderPath']}")
	private String EXCHG_SFTP_RECON_FOLDERPATH;
	
	private static Type listType = new TypeToken<ArrayList<EnrollmentSftpFileTransferDTO>>() {}.getType();
	
	//private final CsvToBean<ReconDetailCsvDto> csvToBeanDetail = new CsvToBean<ReconDetailCsvDto>();
	private final CsvToBean<ReconSummeryCsvDto> csvToBeanSummery = new CsvToBean<ReconSummeryCsvDto>();
	
	private final String[] SUMMARY_COLUMN = new String[] { "recordCode",
			"tradingPartnerId", "spoeId", "tenantId", "hiosId",
			"qhpidLookupKey", "issuerExtractDate", "totalNumberOfRecords",
			"totalNumberOfSubscribers", "totalNumberOfDependentMembers",
			"totalPremiumAmount", "totalAppliedAptcAmount" };
	private final String[] DETAIL_COLUMN = new String[] { "recordCode",
			"tradingPartnerId", "spoeId", "tenantId", "hiosId",
			"qhpidLookupKey", "issuerExtractDate", "issuerExtractTime",
			"qiFirstName", "qiMiddleName", "qiLastName", "qiBirthDate",
			"qiGender", "qiSocialSecurityNumber", "subscriberIndicator",
			"individualRelationshipCode", "exchangeAssignedSubscriberId",
			"exchangeAssignedMemberId", "issuerAssignedSubscriberId",
			"issuerAssignedMemberId", "exchangeAssignedPolicyNumber",
			"issuerAssignedPolicyId", "residentialAddressLine1",
			"residentialAddressLine2", "residentialCityName",
			"residentialStateCode", "residentialZipCode",
			"mailingAddressLine1", "mailingAddressLine2",
			"mailingAddressCity", "mailingAddressStateCode",
			"mailingAddressZipCode", "residentialCountyCode", "ratingArea",
			"telephoneNumber", "tobaccoUseCode", "qhpIdentifier",
			"benefitStartDate", "benefitEndDate", "appliedAptcAmount",
			"appliedAptcEffectiveDate", "appliedAptcEndDate", "csrAmount",
			"csrEffectiveDate", "csrEndDate", "totalPremiumAmount",
			"totalPremiumEffectiveDate", "totalPremiumEndDate",
			"individualPremiumAmount", "individualPremiumEffectiveDate",
			"individualPremiumEndDate", "initialPremiumPaidStatus",
			"issuerAssignedRecordTraceNumber", "coverageYear",
			"paidThroughDate", "endOfYearTerminationIndicator",
			"agentBrokerName", "agentBrokerNpn" };
	
	@Override
	public void processReconciliationInFile(JobService jobService, StepExecution stepExecution,  JobLauncher jobLauncher, Job job) throws IOException, GIException, InterruptedException {
		
		Long jobId = stepExecution.getJobExecution().getJobId();
		//Moving the Previous files to failure folder
		moveAllWipFiletoFailureFolder(jobId);
		if(Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_SEND_REPORT_COPY_TO_EXCHG))) {
			copyToExchgFolder(jobId);
		}
		String duplicateFolderPath = moveAllDuplicateFiles(jobId);
		String wipFolderPath = moveAllInFileToWipFolder();
		
		long fileCount = 0;
		boolean processReconciliationFileInParallel = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
															EnrollmentConfiguration.EnrollmentConfigurationEnum.PROCESS_RECONCILIATION_FILE_IN_PARALLEL));
		ExecutorService executor = null;
		if(null != wipFolderPath){
			fileCount = Files.list(Paths.get(wipFolderPath)).count();
			if(fileCount > 1 && processReconciliationFileInParallel){
				executor = Executors.newFixedThreadPool(EnrollmentConstants.TEN);
			}else{
				executor = Executors.newFixedThreadPool(EnrollmentConstants.ONE);
			}
			
			File[] csvFiles = EnrollmentUtils.getFilesInAFolder(wipFolderPath, EnrollmentConstants.FILE_TYPE_IN);
			
			File archivePath = createReconPath(jobId, EnrollmentConstants.ARCHIVE_FOLDER);
			
			List<Callable<String>> taskList = new ArrayList<>();

			for (int index = 0; index < csvFiles.length; index++){
				taskList.add(new EnrollmentReconciliationJobThread(csvFiles[index].getPath(), jobId, jobService, stepExecution, archivePath, jobLauncher, job, this));
			}
			List<String>  processResultList = executor.invokeAll(taskList)
													  .stream()
													  .map(future -> { 
																	try{
																		return future.get();
																		
																		}catch(Exception ex){
																		
																			LOGGER.error("Exception occurred in processReconciliationInFile: ", ex);
																	    }
																	    return null;
																     } )
													  .filter(p -> p != null)
													  .collect(Collectors.toCollection(ArrayList<String> :: new));
			
			 executor.shutdown();
			
		}else{
			
			LOGGER.error("wipFolderPath is Null");
		}
		processDuplicateInboundFiles(duplicateFolderPath, jobId);
	}

	@Override
	public String populateReconciliationData(String fileName, Long batchExecutionId,JobService jobService, StepExecution stepExecution, File archivePath, JobLauncher jobLauncher, Job job) throws Exception {
		
		Integer fileId =  createSummery(fileName, batchExecutionId, false);
		Integer counter = 0;
		
		String batchJobStatus=null;
		
		if(jobService != null){
			batchJobStatus = jobService.getJobExecution(stepExecution.getJobExecutionId()).getStatus().name();
		}
		
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
									batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			
			updateSummeryForJobStop(fileId, stepExecution.getJobExecutionId());
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
		List<ReconDetailCsvDto> csvLineList = new ArrayList<ReconDetailCsvDto>();
		ReconSummeryCsvDto csvSummeryLine = null;
		
		try {
			
			if(fileId != 0 ){
				
				counter = Files.lines(Paths.get(fileName)) .map(line -> line.trim())
														   .filter(line -> line.length() > 0)
				 								           .map(line -> convertToBeanAndSave(line, fileId, csvLineList, csvSummeryLine))
				 								           .reduce(0, Integer::sum);

				counter =  counter + loadAndSaveEnrlReconData (csvLineList, fileId);
				
				moveBadEnrollmentFromDataTable(fileId);
				
				updateSummery(fileId , counter);
			}
			
		} catch (IOException e) {
			LOGGER.info("EnrollmentReconciliation SERVICE : IOException while reading + "+fileName);
			throw new GIException("EnrollmentReconciliation SERVICE : IOException while reading + "+fileName);
		}
		
		moveFileToArchiveOrFailure(archivePath, fileName);
		triggerSnapshotGenerationJob(fileId, jobLauncher, job);
		return Integer.toString(fileId);
		
	}

	@Override
	public void sendReconciliationNotification(Long jobId) throws GIException {
		
		Date lastRunDate = enrollmentBatchService.getJOBLastRunDate("enrlReconNotificationJob");
		List<Object[]> updateSummaryObjList = null;
		
		if(null != lastRunDate){
			updateSummaryObjList = enrlReconSummaryRepository.getUpdateSummaryByDate(lastRunDate);
		}else{
			lastRunDate = DateUtil.StringToDate(EnrollmentConstants.DEFAULT_BATCH_START_DATE, EnrollmentConstants.BATCH_DATE_FORMAT);
			
			updateSummaryObjList = enrlReconSummaryRepository.getUpdateSummaryByDate(lastRunDate);
		}
		
		if(updateSummaryObjList != null && !updateSummaryObjList.isEmpty()){
			List<EnrlReconSummaryUpdateDTO> enrlReconSummaryUpdateDTOList =  new ArrayList<>();
			EnrlReconSummaryUpdateDTO enrlReconSummaryUpdateDTO = null;
			
			for(Object[] updateSummaryObj : updateSummaryObjList){
				enrlReconSummaryUpdateDTO = new EnrlReconSummaryUpdateDTO();
				
				enrlReconSummaryUpdateDTO.setIssuerName(EnrollmentUtils.isNotNull(updateSummaryObj[EnrollmentConstants.ZERO]) ?
																	updateSummaryObj[EnrollmentConstants.ZERO].toString() : null);
				
				enrlReconSummaryUpdateDTO.setFileName(EnrollmentUtils.isNotNull(updateSummaryObj[EnrollmentConstants.ONE]) ? 
																	updateSummaryObj[EnrollmentConstants.ONE].toString() : null);
				
				enrlReconSummaryUpdateDTO.setDateReceived(EnrollmentUtils.isNotNull(updateSummaryObj[EnrollmentConstants.TWO]) ? 
																	DateUtil.dateToString((Date)updateSummaryObj[EnrollmentConstants.TWO], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY) : null);
				
				enrlReconSummaryUpdateDTO.setStatus(EnrollmentUtils.isNotNull(updateSummaryObj[EnrollmentConstants.THREE]) ? 
						                                           updateSummaryObj[EnrollmentConstants.THREE].toString() : null);
				
				enrlReconSummaryUpdateDTO.setDiscrepancyFileName(EnrollmentUtils.isNotNull(updateSummaryObj[EnrollmentConstants.FOUR]) ? 
						                                           updateSummaryObj[EnrollmentConstants.FOUR].toString() : null);
				
				enrlReconSummaryUpdateDTO.setDiscrepancySentDate(EnrollmentUtils.isNotNull(updateSummaryObj[EnrollmentConstants.FIVE]) ?
						                                           DateUtil.dateToString((Date)updateSummaryObj[EnrollmentConstants.FIVE], EnrollmentConstants.DATE_FORMAT_MM_DD_YYYY) : null);
				
				enrlReconSummaryUpdateDTOList.add(enrlReconSummaryUpdateDTO);
			}
			
			//SEND MAIL
			sendReconUpdateMail(jobId, enrlReconSummaryUpdateDTOList, lastRunDate);
			
		}
	}
	
	private void sendReconUpdateMail(Long jobId, List<EnrlReconSummaryUpdateDTO> enrlReconSummaryUpdateDTOList, Date lastRunDate) {
		Map<String, String> emailDataMap = new HashMap<>();
		boolean sendMail = false;
		Calendar cal = Calendar.getInstance();
		StringBuffer subjectBuilder = new StringBuffer();
		
		emailDataMap.put("jobId", jobId.toString());
		emailDataMap.put("Date", DateUtil.dateToString(lastRunDate, "MMMM dd, YYYY"));
		emailDataMap.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		emailDataMap.put(TemplateTokens.EXCHANGE_PHONE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		emailDataMap.put(TemplateTokens.EXCHANGE_ADDRESS_1,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		emailDataMap.put(TemplateTokens.EXCHANGE_ADDRESS_2,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		emailDataMap.put("exgCityName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		emailDataMap.put("exgStateName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		emailDataMap.put("zip",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		emailDataMap.put(TemplateTokens.EXCHANGE_URL,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		emailDataMap.put("exchangeFax",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		emailDataMap.put("exchangeAddressEmail",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		emailDataMap.put(TemplateTokens.EXCHANGE_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		
		String env = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECONCILIATIONENVIRONMENT);
		String environment = EnrollmentConstants.ENROLLMENT_ENVIRONMENT_MAIL_TEMPLATE_PROD;
		if(env != null && environment.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_ENVIRONMENT_TEST)){
			environment = EnrollmentConstants.ENROLLMENT_ENVIRONMENT_MAIL_TEMPLATE_QA;
		}
		
		emailDataMap.put("environment",environment);
		
		SimpleDateFormat formatter=new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		emailDataMap.put("spanishDate", formatter.format(new Date()));
		
		emailDataMap.put("month", String.format("%02d", cal.get(Calendar.MONTH)+1));
		emailDataMap.put("year", String.valueOf(cal.get(Calendar.YEAR)));
		emailDataMap.put("date", cal.getTime().toString());
		
		subjectBuilder.append("Reconciliation").append(SPACE).
		               append("Update").append(SPACE).
		               append("in").append(SPACE).
		               append(environment).append(SPACE).
		               append("for").append(SPACE).
		               append(String.format("%02d", cal.get(Calendar.MONTH) + 1)).append("/")
										  .append(String.valueOf(cal.get(Calendar.YEAR)));
		
		String tdPrefix = "<td style='border:1px solid #000;'>";
		String tdPostfix = "</td>";
		
		StringBuffer tableContentBuilder = new StringBuffer();
		for(EnrlReconSummaryUpdateDTO enrlReconSummaryUpdateDTO : enrlReconSummaryUpdateDTOList){
				tableContentBuilder.append("<tr>");
				
				tableContentBuilder.append(tdPrefix).append(enrlReconSummaryUpdateDTO.getIssuerName()).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(enrlReconSummaryUpdateDTO.getFileName()).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(enrlReconSummaryUpdateDTO.getDateReceived()).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(enrlReconSummaryUpdateDTO.getStatus()).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(enrlReconSummaryUpdateDTO.getDiscrepancyFileName()).append(tdPostfix);
				tableContentBuilder.append(tdPrefix).append(enrlReconSummaryUpdateDTO.getDiscrepancySentDate()).append(tdPostfix);
	
				tableContentBuilder.append("</tr>");
				
				sendMail = true;
		}
		
		emailDataMap.put("tableContent", tableContentBuilder.toString());
		emailDataMap.put("Subject", subjectBuilder.toString());
		
		if(sendMail){
			try {
				reconciliationEmailNotificationService.sendReconAlertEmail(emailDataMap);
			} catch (GIException e) {
				LOGGER.debug("Error sending reconciliation email notification :: ", e);
			}
		}
		
	}
	
	private void moveBadEnrollmentFromDataTable(Integer fileId) {
		List<Long> enrlList = enrlReconErrorRepository.getEnrollmentIdByFileId(fileId);
		List<EnrlReconData> enrlDataList = null;
		List<Integer> idList = null;
		List<EnrlReconError> enrlReconErrorList = null;
		
		if(null != enrlList && enrlList.size() > 0){
			for (Long enrlId : enrlList) {
				
				if(enrlId != null){
					
					enrlDataList = enrlReconDataRepository.getReconDataByEnrollmentId(enrlId, fileId);
					
					if(null != enrlDataList && !enrlDataList.isEmpty()){
						idList = new ArrayList<>();
						enrlReconErrorList =  new ArrayList<>();
						
						for (EnrlReconData enrlReconData : enrlDataList) {
							idList.add(enrlReconData.getId());
							
							EnrlReconError enrlReconError = new EnrlReconError();
							
							enrlReconError.setFileId(fileId);
							enrlReconError.setEnrollmentId(enrlReconData.getEnrollmentId());
							enrlReconError.setErrorMessage(" Move from Data table as one of membar data has error");
							enrlReconError.setInData(enrlReconData.getInData());
							enrlReconError.setSubscriberId(enrlReconData.getExchgAssignedSubId());
							
							enrlReconErrorList.add(enrlReconError);
						}
						
						enrlReconErrorRepository.save(enrlReconErrorList);
						enrlReconDataRepository.deleteEnrlReconData(idList);
					}
				}
			}
		}
	}

	private void updateSummeryForJobStop(Integer fileId, Long jobExecutionId) {
		EnrlReconSummary enrlReconSummary = enrlReconSummaryRepository.getReconSummaryByFileId(fileId);
		enrlReconSummary.setComments(" Job Stop by using JobKill Functionality  [ JobExecutionId : "+jobExecutionId+" ] ");
		enrlReconSummaryRepository.save(enrlReconSummary);
	}

	private void updateSummery(Integer fileId, Integer counter) {
		EnrlReconSummary enrlReconSummary = enrlReconSummaryRepository.getReconSummaryByFileId(fileId);
		
		enrlReconSummary.setTotalRecordsCnt(counter);
		
		Integer totalSuccessEnrlCount = enrlReconDataRepository.getTotalEnrlCount(fileId);
		Integer totalFailEnrlCount = enrlReconErrorRepository.getEnrollmentCountByFileId(fileId);
		
		enrlReconSummary.setRecCntFailed(totalFailEnrlCount);
		enrlReconSummary.setTotalEnrlCntInFile(totalSuccessEnrlCount + totalFailEnrlCount);
		
		Integer totalSubscriberCount = enrlReconDataRepository.getTotalSubscriberCount(fileId);
		enrlReconSummary.setTotalSubscriberCnt(totalSubscriberCount);
		
		Integer totalDependentCount = enrlReconDataRepository.getTotalDependentCount(fileId);
		enrlReconSummary.setTotalDependentCnt(totalDependentCount);
		
		enrlReconSummaryRepository.save(enrlReconSummary);
	}

	private int convertToBeanAndSave(String line, Integer fileId,List<ReconDetailCsvDto> csvLineList, ReconSummeryCsvDto csvSummeryLine) {
		List<ReconDetailCsvDto> reconDetailCsvDtoList = null;
		List<ReconSummeryCsvDto> reconSummeryCsvDtoList = null;
		int internalCounter = 0;
		CsvToBean<ReconDetailCsvDto> csvToBeanDetail = new CsvToBean<ReconDetailCsvDto>();
		
		if(null != line && line.startsWith("01")){
			
			reconDetailCsvDtoList =  csvToBeanDetail.parse(getDetailColumnMapping(), new CSVReader(new StringReader(line), '|'));
			
			if(null != reconDetailCsvDtoList && !reconDetailCsvDtoList.isEmpty()){
				
				if(validateReconDetailCsv(reconDetailCsvDtoList.get(0)) ){
					
					csvLineList.add(reconDetailCsvDtoList.get(0));
					
				}else{
					
					loadAndSaveEnrlReconError(reconDetailCsvDtoList.get(0), line, fileId," Error : Line Starts with 01 parse to ReconDetailCsvDto but has validation error");
					return ++internalCounter;
					
				}
				
				if (csvLineList.size() >= 1000) {
					
					List<ReconDetailCsvDto> csvLineSaveList = csvLineList.stream().collect(Collectors.toList());
					internalCounter = loadAndSaveEnrlReconData (csvLineSaveList,fileId);
					csvLineList.clear();
					
					return internalCounter;
				}
				
			}else{
				loadAndSaveEnrlReconError(null, line, fileId, "Error : Line Starts with 01 but has parseing error");
				return ++internalCounter;
			}

			
		}else if(null != line && line.startsWith("02")){
			
			reconSummeryCsvDtoList = csvToBeanSummery.parse(getSummeryColumnMapping(), new CSVReader(new StringReader(line), '|'));
			
			if(null != reconSummeryCsvDtoList && !reconSummeryCsvDtoList.isEmpty()){
				
				csvSummeryLine = reconSummeryCsvDtoList.get(0);
				saveSummary(csvSummeryLine , fileId);
				
			}else{
				
				loadAndSaveEnrlReconError(null, line, fileId, "Error : Line Starts with 02 but has parseing error");
			}

		}else{
			if(line.startsWith("Record Code")){
				// Header line Do Nothing
			}else{
				loadAndSaveEnrlReconError(null, line, fileId, "Error : Line Starts with Unexcpected recored code");
				return ++internalCounter;
			}
		}
		
		return internalCounter;
	}
	
private int loadAndSaveEnrlReconData(List<ReconDetailCsvDto> csvLineSaveList, Integer fileId) {
		
		EnrlReconData enrlReconData = null;
		List<EnrlReconData> enrlReconDataList = new ArrayList<EnrlReconData>();
		
	    SimpleDateFormat dateFormat = new SimpleDateFormat(EnrollmentConstants.DATE_FORMAT_YYYYMMDD);
	    dateFormat.setLenient(false);
	    Date coverageDate = null;
		
		for (ReconDetailCsvDto reconDetailCsvDto : csvLineSaveList) {
			try{
				enrlReconData = new EnrlReconData();
				//Requires this information for logging the exception
				enrlReconData.setFileId(fileId);
				
				enrlReconData.setInData(reconDetailCsvDto.toCsv('|'));
				
				enrlReconData.setEnrollmentId(isNoneBlank(reconDetailCsvDto.getExchangeAssignedPolicyNumber()) ?
						                      		Long.parseLong(reconDetailCsvDto.getExchangeAssignedPolicyNumber()) : null);
				
				enrlReconData.setExchgAssignedMemberId(isNoneBlank(reconDetailCsvDto.getExchangeAssignedMemberId()) ? 
													Integer.parseInt(reconDetailCsvDto.getExchangeAssignedMemberId()) : null);
				
				enrlReconData.setExchgAssignedSubId(isNoneBlank(reconDetailCsvDto.getExchangeAssignedSubscriberId()) ? 
						                            Integer.parseInt(reconDetailCsvDto.getExchangeAssignedSubscriberId()) : null );
				
				enrlReconData.setIssuerAssignedPolicyId(isNoneBlank(reconDetailCsvDto.getIssuerAssignedPolicyId()) ? 
						                            Long.parseLong(reconDetailCsvDto.getIssuerAssignedPolicyId()) : null);
				
				enrlReconData.setIssuerAssignedSubscriberId(isNoneBlank(reconDetailCsvDto.getIssuerAssignedSubscriberId()) ? 
													reconDetailCsvDto.getIssuerAssignedSubscriberId() : null);
				
				enrlReconData.setIssuerAssignedMemberId(isNoneBlank(reconDetailCsvDto.getIssuerAssignedMemberId()) ? 
													reconDetailCsvDto.getIssuerAssignedMemberId() : null);
				
				enrlReconData.setSubscriberIndicator(isNoneBlank(reconDetailCsvDto.getSubscriberIndicator()) ? 
																 reconDetailCsvDto.getSubscriberIndicator() : null);
				
				enrlReconData.setQhpIdentifier(isNoneBlank(reconDetailCsvDto.getQhpIdentifier()) ? 
						 								   reconDetailCsvDto.getQhpIdentifier() : null);
				
				enrlReconData.setBenefitStartDate(isNoneBlank(reconDetailCsvDto.getBenefitStartDate()) ? 
										  dateFormat.parse(reconDetailCsvDto.getBenefitStartDate()) : null);
				
				enrlReconData.setBenefitEndDate(isNoneBlank(reconDetailCsvDto.getBenefitEndDate()) ? 
										  dateFormat.parse(reconDetailCsvDto.getBenefitEndDate()) : null);
				
				enrlReconData.setTotPremiumEffDate(isNoneBlank(reconDetailCsvDto.getTotalPremiumEffectiveDate()) ? 
										  dateFormat.parse(reconDetailCsvDto.getTotalPremiumEffectiveDate()) : null);
				
				enrlReconData.setTotPremiumEndDate(isNoneBlank(reconDetailCsvDto.getTotalPremiumEndDate()) ? 
										  dateFormat.parse(reconDetailCsvDto.getTotalPremiumEndDate()) : null);
				
				enrlReconData.setIndivPremiumEffDate(isNoneBlank(reconDetailCsvDto.getTotalPremiumEffectiveDate()) ? 
										  dateFormat.parse(reconDetailCsvDto.getTotalPremiumEffectiveDate()) : null);
				
				enrlReconData.setIndivPremiumEndDate(isNoneBlank(reconDetailCsvDto.getTotalPremiumEndDate()) ? 
										  dateFormat.parse(reconDetailCsvDto.getTotalPremiumEndDate()) : null);
				
				coverageDate = isNoneBlank(reconDetailCsvDto.getBenefitStartDate()) ?  
										  dateFormat.parse(reconDetailCsvDto.getBenefitStartDate()) : null;
				
				enrlReconData.setCoverageYear(isNoneBlank(reconDetailCsvDto.getBenefitStartDate()) ? 
										  DateUtil.getYearFromDate(coverageDate) : null);
				
				enrlReconData.setRecordCode(isNoneBlank(reconDetailCsvDto.getRecordCode()) ? 
													Integer.parseInt(reconDetailCsvDto.getRecordCode()) : null);
				
				enrlReconDataList.add(enrlReconData);
				
			}catch(Exception ex){
				//suppressing the exception as we are saving the same in DB
				EnrlReconError enrlReconError = new EnrlReconError();
				enrlReconError.setEnrollmentId(enrlReconData!= null ? enrlReconData.getEnrollmentId(): null);
				enrlReconError.setFileId(fileId);
				enrlReconError.setInData(reconDetailCsvDto.toCsv());
				enrlReconError.setErrorMessage(EnrollmentUtils.shortStackTrace(ex, EnrollmentConstants.TWO_NINETY_NINE));
				enrlReconErrorRepository.save(enrlReconError);
			}
		}
		
		enrlReconDataList.stream().forEach(new Consumer<EnrlReconData>(){
			@Override
			public void accept(EnrlReconData enrlReconData) {
				try{
					enrlReconDataRepository.saveAndFlush(enrlReconData);
				}catch(Exception ex){
					//suppressing the exception as we are saving the same in DB
					EnrlReconError enrlReconError = new EnrlReconError();
					enrlReconError.setEnrollmentId(enrlReconData!= null ? enrlReconData.getEnrollmentId(): null);
					enrlReconError.setFileId(fileId);
					enrlReconError.setInData(enrlReconData.getInData());
					enrlReconError.setErrorMessage(EnrollmentUtils.shortStackTrace(ex, EnrollmentConstants.TWO_NINETY_NINE));
					enrlReconErrorRepository.save(enrlReconError);
				}
			}
		});
		
		return csvLineSaveList.size();
	}
	
	private Integer createSummery(String filePath, Long batchExecutionId, boolean isDuplicate) {
		EnrlReconSummary enrlReconSummary = new EnrlReconSummary();
		enrlReconSummary.setBatchId(batchExecutionId.intValue());
		
		String fileName = Paths.get(filePath).getFileName().toString();
		
		enrlReconSummary.setFileName(fileName);
		enrlReconSummary.setHiosIssuerId(getHiosIssuerId(fileName));
		
		LocalDate today = LocalDate.now();
		enrlReconSummary.setMonth(today.getMonthValue());
		enrlReconSummary.setYear(today.getYear());
		enrlReconSummary.setCoverageYear(getYear(fileName));
		if(isDuplicate) {
			enrlReconSummary.setStatus(SummaryStatus.DUPLICATE.toString());
		}
		
		enrlReconSummary = enrlReconSummaryRepository.save(enrlReconSummary);
		
		return enrlReconSummary.getId();
	}
	
	private Integer getYear(String fileName) {
		if(null != fileName){
			int lastIndex = fileName.lastIndexOf('_');
			String year = fileName.substring(lastIndex-4, lastIndex);
			if(NumberUtils.isNumber(year)){
				return Integer.valueOf(year);
			}
		}
		LOGGER.error("No coverage year present in filename");
		return new Integer(0);
	}

	private String getHiosIssuerId(String fileName) {
		if(null != fileName){
			int firstIndex = fileName.indexOf('_');
			int secondIndex = fileName.indexOf('_',firstIndex+1);
			String hiosIssuerId = fileName.substring(firstIndex+1, secondIndex);
			return hiosIssuerId;
		}
		return null;
	}
	
	private void saveSummary(ReconSummeryCsvDto csvSummeryLine, Integer fileId) {
		if(csvSummeryLine != null){
			EnrlReconSummary enrlReconSummary = enrlReconSummaryRepository.getReconSummaryByFileId(fileId);
			
		    SimpleDateFormat dateFormat = new SimpleDateFormat(EnrollmentConstants.DATE_FORMAT_YYYYMMDD);
		    dateFormat.setLenient(false);
			
			if(enrlReconSummary != null){
				try {
					if(Objects.isNull( enrlReconSummary.getHiosIssuerId() ) ){
						enrlReconSummary.setHiosIssuerId(isNoneBlank(csvSummeryLine.getHiosId()) ? csvSummeryLine.getHiosId() : null);
					}
					
					enrlReconSummary.setStatus(SummaryStatus.LOADED.toString());
					
					if(Objects.isNull( enrlReconSummary.getQhpid() ) ){
						enrlReconSummary.setQhpid(isNoneBlank(csvSummeryLine.getQhpidLookupKey()) ? csvSummeryLine.getQhpidLookupKey() : null);
					}
					
					if(Objects.isNull( enrlReconSummary.getIssuerTotalRecordsCnt() ) ){
						enrlReconSummary.setIssuerTotalRecordsCnt(isNoneBlank(csvSummeryLine.getTotalNumberOfRecords()) ? 
							  	Integer.parseInt(csvSummeryLine.getTotalNumberOfRecords()) : null);
					}else{
						Integer issuerTotalRecordsCnt = enrlReconSummary.getIssuerTotalRecordsCnt();
						enrlReconSummary.setIssuerTotalRecordsCnt(isNoneBlank(csvSummeryLine.getTotalNumberOfRecords()) ? 
							  	(Integer.parseInt(csvSummeryLine.getTotalNumberOfRecords()) + issuerTotalRecordsCnt) : issuerTotalRecordsCnt);
					}
					
					if(Objects.isNull( enrlReconSummary.getIssuerTotalDependentCnt() ) ){
						enrlReconSummary.setIssuerTotalDependentCnt(isNoneBlank(csvSummeryLine.getTotalNumberOfDependentMembers()) ? 
								 	Integer.parseInt(csvSummeryLine.getTotalNumberOfDependentMembers()) : null);
					}else{
						Integer issuerTotalDependentCnt = enrlReconSummary.getIssuerTotalDependentCnt();
						enrlReconSummary.setIssuerTotalDependentCnt(isNoneBlank(csvSummeryLine.getTotalNumberOfDependentMembers()) ? 
							 	(Integer.parseInt(csvSummeryLine.getTotalNumberOfDependentMembers()) + issuerTotalDependentCnt) : issuerTotalDependentCnt);
					}
					
					if(Objects.isNull( enrlReconSummary.getIssuerTotalSubscriberCnt() ) ){
						enrlReconSummary.setIssuerTotalSubscriberCnt(isNoneBlank(csvSummeryLine.getTotalNumberOfSubscribers()) ? 
									Integer.parseInt(csvSummeryLine.getTotalNumberOfSubscribers()) : null);
					}else{
						Integer issuerTotalSubscriberCnt = enrlReconSummary.getIssuerTotalSubscriberCnt();
						enrlReconSummary.setIssuerTotalSubscriberCnt(isNoneBlank(csvSummeryLine.getTotalNumberOfSubscribers()) ? 
									(Integer.parseInt(csvSummeryLine.getTotalNumberOfSubscribers()) + issuerTotalSubscriberCnt) : issuerTotalSubscriberCnt);
					}
					
					if(Objects.isNull( enrlReconSummary.getIssuerTotalAptc() ) ){
						enrlReconSummary.setIssuerTotalAptc(isNoneBlank(csvSummeryLine.getTotalAppliedAptcAmount()) ? 
								Double.parseDouble(csvSummeryLine.getTotalAppliedAptcAmount()) : null);
					}else{
						Double issuerTotalAptc = enrlReconSummary.getIssuerTotalAptc();
						enrlReconSummary.setIssuerTotalAptc(isNoneBlank(csvSummeryLine.getTotalAppliedAptcAmount()) ? 
								(Double.parseDouble(csvSummeryLine.getTotalAppliedAptcAmount()) + issuerTotalAptc) : issuerTotalAptc);
					}
					
					if(Objects.isNull( enrlReconSummary.getIssuerTotalGrossPremium() )){
						enrlReconSummary.setIssuerTotalGrossPremium(isNoneBlank(csvSummeryLine.getTotalPremiumAmount()) ? 
								Double.parseDouble(csvSummeryLine.getTotalPremiumAmount()) : null);
					}else{
						Double issuerTotalGrossPremium = enrlReconSummary.getIssuerTotalGrossPremium();
						enrlReconSummary.setIssuerTotalGrossPremium(isNoneBlank(csvSummeryLine.getTotalPremiumAmount()) ? 
								(Double.parseDouble(csvSummeryLine.getTotalPremiumAmount()) + issuerTotalGrossPremium) : issuerTotalGrossPremium);
					}
					
					enrlReconSummary.setIssuerExtractDate(isNoneBlank(csvSummeryLine.getIssuerExtractDate()) ? 
								  dateFormat.parse(csvSummeryLine.getIssuerExtractDate()) : EnrollmentUtils.getMonthStartDate(new Date()));
				    
					} catch (ParseException ex) {
				    	//suppressing the exception as we are saving the same in DB
						EnrlReconError enrlReconError = new EnrlReconError();
						enrlReconError.setFileId(fileId);
						enrlReconError.setInData(csvSummeryLine.toCsv('|'));
						enrlReconError.setErrorMessage(EnrollmentUtils.shortStackTrace(ex, EnrollmentConstants.TWO_NINETY_NINE));
						enrlReconErrorRepository.save(enrlReconError);
					}
				
				enrlReconSummaryRepository.save(enrlReconSummary);
			}else{
				EnrlReconError enrlReconError = new EnrlReconError();
				enrlReconError.setFileId(fileId);
				enrlReconError.setInData(csvSummeryLine.toCsv('|'));
				enrlReconErrorRepository.save(enrlReconError);
			}
		}
	}

	private void loadAndSaveEnrlReconError(ReconDetailCsvDto reconDetailCsvDto, String line, Integer fileId, String message) {
		
		EnrlReconError enrlReconError = new EnrlReconError();
		
		enrlReconError.setFileId(fileId);
		enrlReconError.setInData(line);
		
		if(null != reconDetailCsvDto){
			
			enrlReconError.setEnrollmentId(isNoneBlank(reconDetailCsvDto.getExchangeAssignedPolicyNumber()) ? 
					Long.parseLong(reconDetailCsvDto.getExchangeAssignedPolicyNumber()) : null);
			
			enrlReconError.setSubscriberId(isNoneBlank(reconDetailCsvDto.getExchangeAssignedSubscriberId()) ? 
                    Integer.parseInt(reconDetailCsvDto.getExchangeAssignedSubscriberId()) : null);
			
			/*enrlReconError.setRecordCode(isNoneBlank(reconDetailCsvDto.getRecordCode()) ? 
                    Integer.parseInt(reconDetailCsvDto.getRecordCode()) : null);*/

		}

		enrlReconError.setCreationTimestamp(new Date());
		enrlReconError.setErrorMessage(message);
		
		enrlReconErrorRepository.save(enrlReconError);
	}
	
	private boolean validateReconDetailCsv(ReconDetailCsvDto reconDetailCsvDto) {

		if (null != reconDetailCsvDto) {

			if (StringUtils.isNotBlank(reconDetailCsvDto.getRecordCode())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getHiosId())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getQiFirstName())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getQiLastName())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getQiBirthDate())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getIndividualRelationshipCode())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getExchangeAssignedPolicyNumber())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getQhpIdentifier())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getBenefitStartDate())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getBenefitEndDate())
					&& StringUtils.isNotBlank(reconDetailCsvDto.getCoverageYear())) {

				return true;

			} else {

				return false;
			}

		} else {

			return false;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ColumnPositionMappingStrategy<ReconDetailCsvDto> getDetailColumnMapping() {
		ColumnPositionMappingStrategy<ReconDetailCsvDto> strategy = new ColumnPositionMappingStrategy();
		strategy.setType(ReconDetailCsvDto.class);
		strategy.setColumnMapping(DETAIL_COLUMN);
		return strategy;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ColumnPositionMappingStrategy<ReconSummeryCsvDto> getSummeryColumnMapping() {
		ColumnPositionMappingStrategy<ReconSummeryCsvDto> strategy = new ColumnPositionMappingStrategy();
		strategy.setType(ReconSummeryCsvDto.class);
		strategy.setColumnMapping(SUMMARY_COLUMN);
		return strategy;
	}

	@Override
	public ReconDetailCsvDto convertStringToreconDetailCsvDto(String line){
		ReconDetailCsvDto detailCsvDto= null;
		if(line!=null){
			CsvToBean<ReconDetailCsvDto> csvToBeanDetail = new CsvToBean<ReconDetailCsvDto>();
			synchronized(this)
			{
				List<ReconDetailCsvDto> reconDetailCsvDtoList =  csvToBeanDetail.parse(getDetailColumnMapping(), new CSVReader(new StringReader(line), '|'));
			if(reconDetailCsvDtoList!=null && reconDetailCsvDtoList.size()>0){
				detailCsvDto=reconDetailCsvDtoList.get(0);
			}
		}
		}
		return detailCsvDto;
	}
	
	private String moveAllInFileToWipFolder() {

		StringBuilder reconciliationFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CARRIERRECON.toString(),
						                                                                                     EnrollmentConstants.TRANSFER_DIRECTION_IN);
		String inboundDir = reconciliationFileNameBuilder.toString();
		//String inboundDir ="D://vino/Workbench/ReconciliationPath/";
		File dir = new File(inboundDir);

		//Move all files from inboundReconciliationPath to WIP folder
		String wipFolderPath = inboundDir + File.separator + EnrollmentConstants.WIP_FOLDER_NAME;
		File wipFolder = new File(wipFolderPath);
		boolean wipPathValid = true;


		if (dir != null && !(wipFolder.exists())) {
			if(!wipFolder.mkdirs()){
				wipPathValid = false;
			}
		}

		if(wipPathValid){
			//Move files from inboundDir to WIP folder
			if (dir.isDirectory()) {
				File[] files = dir.listFiles(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(EnrollmentConstants.FILE_TYPE_IN);
					}
				});
				for (File file : files) {
					if(file.isFile()){
						File wipFile = new File(wipFolderPath + File.separator+ file.getName());
						file.renameTo(wipFile);
					}
				}
			}
			
			if(wipFolder.list()!= null && wipFolder.list().length > 0){
				return wipFolderPath;
			}else{
				return null;
			}
		}

		return null;
	}
	
	private void moveFileToArchiveOrFailure(File filePath, String fileName) {
		if(filePath != null){
			try {
				EnrollmentUtils.moveFile(Paths.get(fileName).getParent().toString(), filePath.toString(), new File(fileName));
			}catch (GIException e) {
				LOGGER.error("Exception occurred while Moving Reconciliation files: ", e);
		    }
		}
	}
	
	private File createReconPath(Long jobId, String directoryName) {
		StringBuilder reconciliationFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CARRIERRECON.toString(),
				 EnrollmentConstants.TRANSFER_DIRECTION_IN);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		String batchDate = sdf.format(new Date()).replace('|', '-').replace(':', '-')+"-"+jobId;
		
		String inboundDir = reconciliationFileNameBuilder.toString();
		File dir = new File(inboundDir);
		
		//Move all files from inboundReconciliationPath to WIP folder
		String archiveFolderPath = inboundDir + File.separator + directoryName + File.separator + batchDate;
		File archiveFolder = new File(archiveFolderPath);
		boolean archivePathValid = true;
		
		
		if (dir != null && !(archiveFolder.exists())) {
			if (!archiveFolder.mkdirs()) {
				archivePathValid = false;
			}
		}
		
		if(archivePathValid){
			return archiveFolder;
		}else{
			return null;
		}
		
	}
	
	private void logBug(String message, String description) {
		
		String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECONCILIATIONENVIRONMENT); //TODO change to Reconciliation
		
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		
		String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		
		if(environment!=null && environment.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_ENVIRONMENT_PRODUCTION) && isJiraEnabled){
			 
			String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
	         JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
	        		         Arrays.asList(fixVersion), 
	        		         description, 
	        		         message,
	        		         null);
		}
		
		enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.IRS,
				 message, description);
	}
	
	/**
	 * @author panda_p
	 * @since 30-MAR-2017
	 * @param cutOffDate
	 * 
	 * Deletes the recornData table records upto given cutOffDate
	 */
	@Override
	public void deleteReconData(String cutOffDate, long jobExecutionId) {
		enrlReconDataRepository.deleteEnrlReconDataByCutOffDate(cutOffDate);
	}
	
	private void moveAllWipFiletoFailureFolder(long jobExecutionId){
		String wipFolderPathStr = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CARRIERRECON.toString(),
				 EnrollmentConstants.TRANSFER_DIRECTION_IN) + File.separator + EnrollmentConstants.WIP_FOLDER_NAME;
		File wipFolder = new File(wipFolderPathStr);
		
		if (wipFolder.isDirectory() && wipFolder.exists()) {
			File failureFolderPath = createReconPath(jobExecutionId, EnrollmentConstants.FAILURE_FOLDER);
			File[] files = wipFolder.listFiles();
			for (File file : files) {
				if(file.isFile() && failureFolderPath != null && failureFolderPath.exists()){
					moveFileToArchiveOrFailure(failureFolderPath, file.getPath());
				}
			}
		}
	}
	
	/**
	 * Trigger snapshot generation batch job for a given file ID
	 * @param fileId
	 * @param jobLauncher
	 * @param job
	 */
	private void triggerSnapshotGenerationJob(Integer fileId, JobLauncher jobLauncher, Job job) {
		String jobChaining = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.
				EnrollmentConfigurationEnum.
				ENRL_RECON_BATCH_JOB_CHAINING);
		if(null != jobChaining && "false".equalsIgnoreCase(jobChaining)){
			LOGGER.info(" NOT Triggering GENERATE SNAPSHOT JOB  for batch File_ID : " + fileId);
			LOGGER.info(" ENRL_RECON_BATCH_JOB_CHAINING : "+ jobChaining);
		}else{
			LOGGER.info("Triggering GENERATE SNAPSHOT JOB  for File_ID : " + fileId);
			JobParameters jobParameters = new JobParametersBuilder()
					.addString("fileId", fileId.toString())
					.addLong("EXECUTION_DATE", new Date().getTime())
					.toJobParameters();
			JobExecution jobExecution;
			try {
				jobExecution = jobLauncher.run(job,jobParameters);
				LOGGER.info("Job complete with exit status : " + jobExecution.getExitStatus());
			} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
					| JobParametersInvalidException e) {
				LOGGER.error("Error in triggering snapshot generation batch job from  code", e);
				logBug(JIRA_MESSAGE_BATCH_TRIGGER, e.getMessage());
			}
		}
	}
	
	/**
	 * Process files in duplicate folder and archive them
	 * @param duplicateFolderPath
	 * @param jobId
	 */
	private void processDuplicateInboundFiles(String duplicateFolderPath, Long jobId) {
		if(null != duplicateFolderPath) {
			LOGGER.info("Processing duplicate file list from location :: "+ duplicateFolderPath);
			File dir = new File(duplicateFolderPath);
			if(dir.isDirectory()) {
				File[] files = dir.listFiles();
				for(File file : files) {
					try {
						createSummery(file.getPath(), jobId, true);
					}catch(Exception e) {
						LOGGER.error("Could not create entry for duplicate inbound file::" + file.getName(), e);
						logBug("Job ID :: "+jobId+"Could not create entry for duplicate inbound file::" + file.getName(), e.getMessage());
					}
				}
			}
		}
	}

	/**
	 * Move duplicate files to Duplicate folder
	 * @param jobId 
	 * @return duplicate folder path
	 */
	private String moveAllDuplicateFiles(Long jobId) {
		StringBuilder reconciliationFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CARRIERRECON.toString(),
						                                                                                     EnrollmentConstants.TRANSFER_DIRECTION_IN);
		String inboundDir = reconciliationFileNameBuilder.toString();
		File dir = new File(inboundDir);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		String batchDate = sdf.format(new Date()).replace('|', '-').replace(':', '-')+"-"+jobId;
		//Move all duplicate files from inboundReconciliationPath to duplicate folder
		String duplicateFolderPath = inboundDir + File.separator + EnrollmentConstants.RECON_DUPLICATE_FOLDER + File.separator + batchDate;
		File duplicateFolder = new File(duplicateFolderPath);
		boolean duplicatePathValid = true;
		LocalDate today = LocalDate.now();
		Integer processingMonth = today.getMonthValue();
		Integer processingYear = today.getYear();
		if (dir != null && !(duplicateFolder.exists())) {
			if(!duplicateFolder.mkdirs()){
				duplicatePathValid = false;
			}
		}
		Map<String, File> distinctInboundMap = new HashMap<>();
		if(dir.isDirectory() && duplicatePathValid) {
			File[] files = dir.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(EnrollmentConstants.FILE_TYPE_IN);
				}
			});
			for (File file : files) {
				if(file.isFile()){
					String hiosIssuerId = getHiosIssuerId(file.getName());
					Integer coverageYear = getYear(file.getName());
					if (distinctInboundMap.containsKey(hiosIssuerId)) {
						File existingFile = distinctInboundMap.get(hiosIssuerId);
						Integer existingCoverageYear = getYear(existingFile.getName());
						Date existing = getTimefromFileName(existingFile.getName());
						Date current = getTimefromFileName(file.getName());
						if (null != existing && null != current && current.after(existing)
								&& existingCoverageYear.intValue() == coverageYear.intValue()) {
							LOGGER.info("Duplicate file received : " + existingFile.getName());
							distinctInboundMap.get(hiosIssuerId).renameTo(new File(duplicateFolderPath + File.separator + existingFile.getName()));
							checkForDuplicate(hiosIssuerId, coverageYear, processingMonth, processingYear, file,
									distinctInboundMap, duplicateFolderPath);
						} else if (existingCoverageYear.intValue() != coverageYear.intValue()) {
							checkForDuplicate(hiosIssuerId, coverageYear, processingMonth, processingYear, file,
									distinctInboundMap, duplicateFolderPath);
						} else {
							file.renameTo(new File(duplicateFolderPath + File.separator + file.getName()));
						}
					}else {
						checkForDuplicate(hiosIssuerId, coverageYear, processingMonth, processingYear, file,
								distinctInboundMap, duplicateFolderPath);
					}
				}
			}
		}
		if(duplicateFolder.list()!= null && duplicateFolder.list().length > 0){
			return duplicateFolderPath;
		}else{
			duplicateFolder.delete();
			return null;
		}
	}

	private void checkForDuplicate(String hiosIssuerId, Integer coverageYear, Integer processingMonth,
			Integer processingYear, File file, Map<String, File> distinctInboundMap, String duplicateFolderPath) {
		Long count = enrlReconSummaryRepository.getFileCountByIssuerAndProcessingDate(hiosIssuerId, coverageYear, processingMonth, processingYear);
		if(null != count && count == 0) {
			LOGGER.info("Unique file received : " + file.getName());
			distinctInboundMap.put(hiosIssuerId, file);
		}else {
			LOGGER.info("Duplicate file received : " + file.getName());
			file.renameTo(new File(duplicateFolderPath + File.separator+ file.getName()));
		}		
	}

	private Date getTimefromFileName(String fileName) {
		if(null != fileName){
			int firstIndex = fileName.lastIndexOf('_');
			int secondIndex = fileName.lastIndexOf('.');
			String timeExtract = fileName.substring(firstIndex+1, secondIndex);
			return DateUtil.StringToDate(timeExtract, "yyyyMMddHHmmss");
		}
		return null;
	}
	
	private void copyToExchgFolder(Long batchExecutionId) {
		StringBuilder reconciliationFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CARRIERRECON.toString(),
				EnrollmentConstants.TRANSFER_DIRECTION_IN);
		String inboundDir = reconciliationFileNameBuilder.toString();
		String response = EnrollmentSftpUtil.uploadFilesByExtension(cmsSftpService.getExchgSftpParameters(), inboundDir, EXCHG_SFTP_RECON_FOLDERPATH, EnrollmentConstants.FILE_TYPE_IN, enrollmentGIMonitorUtil);
		List<EnrollmentSftpFileTransferDTO> fileTransferList = platformGson.fromJson(response, listType);
		cmsSftpService.logSftpTransfers(fileTransferList, batchExecutionId, EnrollmentCmsFileTransferLog.ReportType.CARRIERRECON.toString(), inboundDir, EXCHG_SFTP_RECON_FOLDERPATH);
	}

}
