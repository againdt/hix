package com.getinsured.eligibility.plan.service;

import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS2;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS3;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS4;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS5;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.ind19.util.service.EnrollmentsExtractionService;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemPersonResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdPersonRequest;
import com.getinsured.hix.dto.plandisplay.planavailability.PdRequestPlanAvailability;
import com.getinsured.hix.dto.plandisplay.planavailability.PdResponsePlanAvailability;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("batchPlanAvailabilityAdapter")
@Scope("singleton")
public class BatchPlanAvailabilityAdapter {
	private static final String DLT_NOT_RESPONSE_CODE = "104";
	private static final String HLT_NOT_RESPONSE_CODE = "103";
	private static final String HLT_DLT_NOT_RESPONSE_CODE = "102";
	private static final String BATCH_PLAN_AVAILABILITY = "BATCH_PLAN_AVAILABILITY";
	private static final String EXCEPTION = "Exception - ";
	private static final String SEPARATOR = " - ";
	private static final String PLAN_AVAILABILITY_RETURNED_ERROR_CODE = "planAvailability returned error code - ";
	private static final String SUCCESS_RESPONSE_CODE = "200";
	private static final String RESPONSE = "Response - ";
	private static final String REQUEST = "Request - ";
	private static final String NEW_LINE = "\n";
	private static final String SUCCESS = "SUCCESS";
	private static final String FAILURE = "FAILURE";

	private static final String PRIMARY_PERSON_ID = "1";
	private static final String NO_ELIGIBLE_MEMBERS_FOUND = "No Eligible members found!";
	private static final String PRIMARY_INFORMATION_NOT_FOUND = "Primary information not found!";
	private static final String NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION = "No Application Events found for application!";
	private static final String EMPTY_RESPONSE_FROM_PLAN_AVAILABILITY_API = "Empty response from planAvailability API";
	private static final String THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR = "There are no enrolled SSAPs for this household for coverage year - ";
	private static final String UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID = "Unable to find application for ssapApplicationId - ";

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private CoverageStartDateService coverageStartDateService;

	@Autowired
	private EnrollmentsExtractionService enrollmentsExtractionService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private IntegrationLogService integrationLogService;

	private static List<String> CHILD_RELATIONSHIP_CODES = Arrays.asList("05", "07", "09", "10", "17", "19");

	private static List<String> SPOUSE_RELATIONSHIP_CODES = Arrays.asList("01");
	private static List<String> SELF_RELATIONSHIP_CODES = Arrays.asList("18");

	public PlanAvailabilityAdapterResponse execute(PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {

		PdRequestPlanAvailability request;
		try {
			validatePlanAvailabilityAdapterRequest(planAvailabilityAdapterRequest);
			request = formRequest(planAvailabilityAdapterRequest);
		} catch (Exception e) {
			logData(planAvailabilityAdapterRequest.getSsapApplicationId(), e.getMessage(), FAILURE);
			throw new GIRuntimeException(e);
		}

		PdResponsePlanAvailability pdResponse = invokePlanAvailabilityAPI(request, planAvailabilityAdapterRequest.getSsapApplicationId());

		return formResponse(pdResponse, planAvailabilityAdapterRequest);
	}

	private PlanAvailabilityAdapterResponse formResponse(PdResponsePlanAvailability pdResponse, PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {

		Map<String, ApplicantPlanDto> healthPlanDisplayMap = map(pdResponse, PlanDisplayEnum.InsuranceType.HEALTH);
		Map<String, ApplicantPlanDto> dentalPlanDisplayMap = map(pdResponse, PlanDisplayEnum.InsuranceType.DENTAL);

		PlanAvailabilityAdapterResponse response = new PlanAvailabilityAdapterResponse();
		response.setHealthPlanDisplayMap(healthPlanDisplayMap);
		if (ReferralUtil.mapSize(healthPlanDisplayMap) != ReferralConstants.NONE) {
			response.setHealthPlanAvailable(true);
		}
		response.setDentalPlanDisplayMap(dentalPlanDisplayMap);
		if (ReferralUtil.mapSize(dentalPlanDisplayMap) != ReferralConstants.NONE) {
			response.setDentalPlanAvailable(true);
		}
		response.setCoverageStartDate(planAvailabilityAdapterRequest.getCoverageStartDate());
		response.setDentalEnrollmentId(planAvailabilityAdapterRequest.getDentalEnrollmentId());
		response.setHealthEnrollmentId(planAvailabilityAdapterRequest.getHealthEnrollmentId());
		response.setHealthEnrolled(planAvailabilityAdapterRequest.isHealthEnrolled());
		response.setDentalEnrolled(planAvailabilityAdapterRequest.isDentalEnrolled());
		return response;
	}

	private Map<String, ApplicantPlanDto> map(PdResponsePlanAvailability pdResponse, InsuranceType type) {

		Map<String, ApplicantPlanDto> planDisplayMap = new HashMap<String, ApplicantPlanDto>();
		if (pdResponse != null && pdResponse.getPdOrderItemResponse() != null) {
			for (PdOrderItemResponse plandisplayresponse : pdResponse.getPdOrderItemResponse()) {
				if (type == plandisplayresponse.getInsuranceType()) {
					if (ReferralUtil.listSize(plandisplayresponse.getPdOrderItemPersonResponseList()) != 0) {
						for (PdOrderItemPersonResponse pdApplicantDetails : plandisplayresponse.getPdOrderItemPersonResponseList()) {
							ApplicantPlanDto applicantPlanDto = populateApplicantPlanDto(pdApplicantDetails, plandisplayresponse);
							planDisplayMap.put(String.valueOf(pdApplicantDetails.getMemberGUID()), applicantPlanDto);
						}
					}
				}
			}
		}
		return planDisplayMap;
	}

	private ApplicantPlanDto populateApplicantPlanDto(PdOrderItemPersonResponse pdApplicantDetails, PdOrderItemResponse plandisplayresponse) {

		ApplicantPlanDto applicantPlanDto = new ApplicantPlanDto();
		applicantPlanDto.setMemberGUID(pdApplicantDetails.getMemberGUID());
		applicantPlanDto.setSubscriberFlag(pdApplicantDetails.getSubscriberFlag());
		applicantPlanDto.setPremium(pdApplicantDetails.getPremium());
		applicantPlanDto.setRegion(pdApplicantDetails.getRegion());
		applicantPlanDto.setInsuranceType(plandisplayresponse.getInsuranceType());
		applicantPlanDto.setPlanId(plandisplayresponse.getPlanId());
		applicantPlanDto.setPlanLevelMonthlySubsidy(plandisplayresponse.getMonthlySubsidy());
		applicantPlanDto.setPlanLevelPremiumAfterCredit(plandisplayresponse.getPremiumAfterCredit());
		applicantPlanDto.setPlanLevelPremiumBeforeCredit(plandisplayresponse.getPremiumBeforeCredit());

		return applicantPlanDto;
	}

	private PdRequestPlanAvailability formRequest(PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {
		final SsapApplication currentApplication = validate(planAvailabilityAdapterRequest.getSsapApplicationId());

		final SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		final List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();

		final SsapApplicant primaryApplicantDB = ReferralUtil.retreiveApplicant(currentApplication, ReferralConstants.PRIMARY);
		final HouseholdMember primaryApplicantJson = ReferralUtil.primaryMember(applicationData);

		if (primaryApplicantDB == null || primaryApplicantJson == null) {
			throw new GIRuntimeException(PRIMARY_INFORMATION_NOT_FOUND);
		}

		final Map<Long, SsapApplicant> eligibleApplicantDBRowMap = new HashMap<Long, SsapApplicant>();
		final Map<Long, HouseholdMember> eligibleApplicantJsonRowMap = new HashMap<Long, HouseholdMember>();
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus())) {
				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						eligibleApplicantJsonRowMap.put(applicant.getPersonId(), householdMember);
						break;
					}
				}
				eligibleApplicantDBRowMap.put(applicant.getPersonId(), applicant);
			}
		}

		if (eligibleApplicantJsonRowMap.isEmpty() || eligibleApplicantDBRowMap.isEmpty()) {
			throw new GIRuntimeException(NO_ELIGIBLE_MEMBERS_FOUND);
		}

		final List<BloodRelationship> relationshipList = getRelationships(primaryApplicantJson);

		PdRequestPlanAvailability request = populateRequest(currentApplication, primaryApplicantDB, primaryApplicantJson, eligibleApplicantDBRowMap, eligibleApplicantJsonRowMap, relationshipList, planAvailabilityAdapterRequest);

		return request;
	}

	private PdRequestPlanAvailability populateRequest(SsapApplication currentApplication, SsapApplicant primaryApplicantDB, HouseholdMember primaryApplicantJson, Map<Long, SsapApplicant> eligibleApplicantDBRowMap,
	        Map<Long, HouseholdMember> eligibleApplicantJsonRowMap, List<BloodRelationship> relationshipList, PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {

		PdRequestPlanAvailability request = new PdRequestPlanAvailability();
		request.setHouseholdGUID(currentApplication.getCmrHouseoldId().longValue());
		
		DateTime coverageStartDate = new DateTime(planAvailabilityAdapterRequest.getCoverageStartDate()).plusDays(ReferralConstants.ONE);
		
		request.setCoverageDate(ReferralUtil.formatDate(new TSDate(coverageStartDate.getMillis()), ReferralConstants.DEFDATEPATTERN));
		request.setShoppingType(ShoppingType.INDIVIDUAL);
		request.setEnrollmentType(EnrollmentType.S);
		if (planAvailabilityAdapterRequest.isHealthProcess()) {
			request.setCostSharing(currentApplication.getCsrLevel() != null ? mapCostSharing(currentApplication.getCsrLevel()) : null);
			request.setMaxSubsidy(currentApplication.getMaximumAPTC() != null ? currentApplication.getMaximumAPTC().floatValue() : null);
		}
		List<PdPersonRequest> pdPersonCRDTOList = new ArrayList<PdPersonRequest>();
		Set<Long> persons = new TreeSet<Long>(eligibleApplicantDBRowMap.keySet());

		for (Long personId : persons) {
			PdPersonRequest person = new PdPersonRequest();
			SsapApplicant applicantDBRow = eligibleApplicantDBRowMap.get(personId);
			HouseholdMember applicantJsonRow = eligibleApplicantJsonRowMap.get(personId);

			person.setCountyCode(getCounty(applicantDBRow, applicantJsonRow, primaryApplicantDB, primaryApplicantJson));
			person.setDob(ReferralUtil.formatDate(applicantJsonRow.getDateOfBirth(), ReferralConstants.DEFDATEPATTERN));
			if (planAvailabilityAdapterRequest.isDentalProcess() && isEnrollee(planAvailabilityAdapterRequest.getDentalEnrollees(),applicantDBRow.getApplicantGuid())) {
				person.setDentalEnrollmentId(planAvailabilityAdapterRequest.getDentalEnrollmentId());
			}
			if (planAvailabilityAdapterRequest.isHealthProcess()&& isEnrollee(planAvailabilityAdapterRequest.getHealthEnrollees(),applicantDBRow.getApplicantGuid())) {
				person.setHealthEnrollmentId(planAvailabilityAdapterRequest.getHealthEnrollmentId());
			}
			person.setMemberGUID(applicantDBRow.getApplicantGuid());
			person.setRelationship(getRelationShip(relationshipList, String.valueOf(applicantJsonRow.getPersonId())));
			person.setTobacco(StringUtils.equalsIgnoreCase(ReferralConstants.Y, applicantDBRow.getTobaccouser()) ? YorN.Y : YorN.N);
			person.setZipCode(getZipCode(applicantDBRow, applicantJsonRow, primaryApplicantDB, primaryApplicantJson));

			pdPersonCRDTOList.add(person);

		}
		request.setPdPersonCRDTOList(pdPersonCRDTOList);
		return request;
	}

	private boolean isEnrollee(List<String> enrollees, String applicantGuid) {
	    return (enrollees!=null && enrollees.contains(applicantGuid));
    }

	private List<BloodRelationship> getRelationships(HouseholdMember primaryApplicantJson) {
		List<BloodRelationship> relationshipList = new ArrayList<BloodRelationship>();
		for (BloodRelationship relationship : primaryApplicantJson.getBloodRelationship()) {
			if (PRIMARY_PERSON_ID.equals(relationship.getRelatedPersonId())) {
				relationshipList.add(relationship);
			}
		}

		return relationshipList;
	}

	private SsapApplication validate(Long ssapApplicationId) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException(UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID + ssapApplicationId);
		}

		SsapApplication currentApplication = ssapApplications.get(0);

		SsapApplicationEvent currentApplicationEvent = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(ssapApplicationId);

		if (currentApplicationEvent == null) {
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}
		return currentApplication;
	}

	private PdResponsePlanAvailability invokePlanAvailabilityAPI(PdRequestPlanAvailability request, Long ssapApplicationId) {
		PdResponsePlanAvailability pdResponsePlanAvailability = null;
		StringBuilder finalPayload = new StringBuilder();
		String status = FAILURE;
		try {
			ObjectMapper mapper = new ObjectMapper();
			String requestJson = mapper.writeValueAsString(request);
			finalPayload.append(REQUEST).append(requestJson);
			ResponseEntity<PdResponsePlanAvailability> response = ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.FIND_PLAN_AVAILABILITY, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON,
			        PdResponsePlanAvailability.class, request);

			if (response == null || response.getBody() == null) {
				throw new GIRuntimeException(EMPTY_RESPONSE_FROM_PLAN_AVAILABILITY_API);
			}

			pdResponsePlanAvailability = response.getBody();

			String responseJson = mapper.writeValueAsString(pdResponsePlanAvailability);

			finalPayload.append(NEW_LINE).append(RESPONSE).append(responseJson);

			if (!StringUtils.equals(pdResponsePlanAvailability.getErrCode(), SUCCESS_RESPONSE_CODE) && !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), HLT_DLT_NOT_RESPONSE_CODE)
			        && !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), HLT_NOT_RESPONSE_CODE) && !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), DLT_NOT_RESPONSE_CODE)) {
				throw new GIRuntimeException(PLAN_AVAILABILITY_RETURNED_ERROR_CODE + pdResponsePlanAvailability.getErrCode() + SEPARATOR + pdResponsePlanAvailability.getResponse());
			} else {
				status = SUCCESS;
			}

		} catch (Exception e) {
			finalPayload.append(NEW_LINE).append(EXCEPTION).append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		} finally {
			logData(ssapApplicationId, finalPayload.toString(), status);
		}
		return pdResponsePlanAvailability;
	}

	public SsapApplication getEnrolledSsapForCoverageYearAndHousehold(SsapApplication currentApplication) {
		SsapApplication enrolledApplication = ssapApplicationRepository.findEnrolledSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
		if (enrolledApplication == null) {
			throw new GIRuntimeException(THERE_ARE_NO_ENROLLED_SSA_PS_FOR_THIS_HOUSEHOLD_FOR_COVERAGE_YEAR + currentApplication.getCoverageYear());
		}
		return enrolledApplication;
	}

	private Relationship getRelationShip(List<BloodRelationship> relationshipList, String personId) {
		Relationship relationShipCode = null;
		for (BloodRelationship bloodRelationship : relationshipList) {
			if (personId.equals(bloodRelationship.getIndividualPersonId())) {
				if (SELF_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())) {
					relationShipCode = Relationship.SELF;
				} else if (SPOUSE_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())) {
					relationShipCode = Relationship.SPOUSE;
				} else if (CHILD_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())) {
					relationShipCode = Relationship.CHILD;
				} else {
					relationShipCode = Relationship.DEPENDENT;
				}
			}
		}
		return relationShipCode;
	}

	private String getZipCode(SsapApplicant applicantDBRow, HouseholdMember applicantJsonRow, SsapApplicant primaryApplicantDB, HouseholdMember primaryApplicantJson) {
		String zipCode = primaryApplicantJson.getHouseholdContact().getHomeAddress().getPostalCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null) {
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()) {
				zipCode = applicantJsonRow.getOtherAddress().getAddress().getPostalCode();
			}
		}
		return zipCode;
	}

	private String getCounty(SsapApplicant applicantDBRow, HouseholdMember applicantJsonRow, SsapApplicant primaryApplicantDB, HouseholdMember primaryApplicantJson) {
		String countyCode = primaryApplicantJson.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null) {
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()) {
				countyCode = applicantJsonRow.getOtherAddress().getAddress().getCounty();
			}
		}
		return countyCode;
	}

	private CostSharing mapCostSharing(String csrLevel) {
		CostSharing csLevel = null;
		switch (csrLevel) {
			case ReferralConstants.CS2:
				csLevel = CS2;
				break;
			case ReferralConstants.CS3:
				csLevel = CS3;
				break;
			case ReferralConstants.CS4:
				csLevel = CS4;
				break;
			case ReferralConstants.CS5:
				csLevel = CS5;
				break;
			case ReferralConstants.CS6:
				csLevel = CS6;
				break;
			default:
				break;
		}

		return csLevel;
	}

	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, BATCH_PLAN_AVAILABILITY, status, ssapApplicationId, null);
	}

	private void validatePlanAvailabilityAdapterRequest(PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {
		if (!planAvailabilityAdapterRequest.isHealthProcess() && !planAvailabilityAdapterRequest.isDentalProcess()) {
			throw new GIRuntimeException("Invalid planAvailabilityAdapterRequest ssap application id" + planAvailabilityAdapterRequest.getSsapApplicationId());
		}
		if (planAvailabilityAdapterRequest.getSsapApplicationId() == ReferralConstants.NONE) {
			throw new GIRuntimeException("Invalid planAvailabilityAdapterRequest ssap application id" + planAvailabilityAdapterRequest.getSsapApplicationId());
		}

		if (planAvailabilityAdapterRequest.getHealthEnrollmentId() == ReferralConstants.NONE && planAvailabilityAdapterRequest.getDentalEnrollmentId() == ReferralConstants.NONE) {
			throw new GIRuntimeException("Invalid planAvailabilityAdapterRequest health enrollment id" + planAvailabilityAdapterRequest.getHealthEnrollmentId() + ", dental enrollment id " + planAvailabilityAdapterRequest.getDentalEnrollmentId());
		}

		if (planAvailabilityAdapterRequest.getHealthEnrollmentId() != ReferralConstants.NONE && (planAvailabilityAdapterRequest.getHealthEnrollees() == null || planAvailabilityAdapterRequest.getHealthEnrollees().size() == ReferralConstants.NONE)) {
			throw new GIRuntimeException("Missing planAvailabilityAdapterRequest health enrollee list for " + planAvailabilityAdapterRequest.getHealthEnrollmentId());
		}

		if (planAvailabilityAdapterRequest.getDentalEnrollmentId() != ReferralConstants.NONE && (planAvailabilityAdapterRequest.getDentalEnrollees() == null || planAvailabilityAdapterRequest.getDentalEnrollees().size() == ReferralConstants.NONE)) {
			throw new GIRuntimeException("Missing planAvailabilityAdapterRequest dental enrollee list for " + planAvailabilityAdapterRequest.getDentalEnrollmentId());
		}

		if (planAvailabilityAdapterRequest.getCoverageStartDate() == null) {
			throw new GIRuntimeException("Missing planAvailabilityAdapterRequest coverage start date");
		}

	}

}
