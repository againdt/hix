import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// ActionCreators

import { changeData } from '../../actions/actionCreators';


// Utils

import bitWiseTools from '../bitWiseOperatorTools';
import normalizeNameOfPerson from '../normalizeNameOfPerson';
import normalizeStringToBooleanValue from '../normalizeStringToBooleanValue';

// Constants

import MASKS from '../../constants/masksConstants';



const FlagsTooltip = props => {
    const {
        data,
        householdMembers
    } = props;

    const updateTaxHouseholdCompositionFlags = (...args) => {
        const [ type, newState, hhmIndex, updatedFlags ] = args;
        const _thc = payload => {
            const convert = (...payload) => {
                return bitWiseTools.convertFlagsToValues.apply(this, payload);
            };
            return convert.apply(this, [payload, MASKS[type]]);
        };

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[hhmIndex]
            .taxHouseholdComposition = {
                ...newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[hhmIndex]
                    .taxHouseholdComposition,
                ..._thc(updatedFlags)
            };
    };

    const handleMasksClick = (type, hhmIndex) => {
        const newState = { ...data };

        const updatedFlags = bitWiseTools
            .setInitialValue(
                MASKS[type],
                prompt('Enter new value')
                    .split(',')
                    .map(v => normalizeStringToBooleanValue(v))
            );

        if(type === 'taxHouseholdComposition') {
            updateTaxHouseholdCompositionFlags(type, newState, hhmIndex, updatedFlags);
        }

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[hhmIndex]
            .flagBucket[`${type}Flags`] = updatedFlags;


        props.actions.changeData(newState);
    };

    const handleFlagClick = (type, name, hhmIndex) => {

        const newState = { ...data };

        const updatedFlags = bitWiseTools
            .getUpdatedFlags(
                householdMembers[hhmIndex].flagBucket[`${type}Flags`],
                [[MASKS[type][name], (v => normalizeStringToBooleanValue(v))(prompt('Enter new value'))]]
            );

        if(type === 'taxHouseholdComposition') {
            updateTaxHouseholdCompositionFlags(type, newState, hhmIndex, updatedFlags);
        }

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[hhmIndex]
            .flagBucket[`${type}Flags`] = updatedFlags;

        props.actions.changeData(newState);
    };


    return (
        <div className='hyper-navigation-tooltip-anchor'>
            F
            <ul className='hyper-navigation-tooltip'>
                <div className='list-caption'>
                    Flags current status
                </div>
                {
                    householdMembers.map((hhm, idx) => {
                        const {
                            flagBucket,
                            name,
                        } = hhm;

                        return (
                            <div
                                className='masks-wrapper'
                                key={`${name}_${idx}`}
                            >
                                <div className='hhm-masks'>
                                    {`Household Member ${normalizeNameOfPerson(name)}`}
                                </div>
                                {
                                    Object.entries(MASKS).map(([ maskType, masks ]) => {
                                        const flagsName = `${maskType}Flags`;
                                        return (
                                            <Fragment key={maskType}>
                                                <span
                                                    className='masks-type'
                                                    onClick={() => handleMasksClick(maskType, idx)}
                                                >
                                                    {
                                                        maskType
                                                    }
                                                    : flags =
                                                    {
                                                        flagBucket && flagBucket[flagsName] || 'No flags'
                                                    }
                                                </span>
                                                {
                                                    Object.entries(masks).map(indicator => {
                                                        const [ name, masks  ] = indicator;
                                                        const flags = flagBucket && flagBucket[flagsName] || 0;
                                                        return (
                                                            <li key={indicator}
                                                                style={{position: 'relative'}}
                                                            >
                                                                <span className='hyper-navigation-tooltip-number'>
                                                                    { name.split('Flags')[0] }
                                                                </span>
                                                                <span
                                                                    className='hyper-navigation-tooltip-masks'
                                                                    onClick={() => handleFlagClick(maskType, name, idx)}
                                                                >
                                                                    <strong>
                                                                        {
                                                                            (v => {
                                                                                return (
                                                                                    <strong style={v === undefined ? {color: 'red'} : {}}>
                                                                                        { String(v !== undefined ? v : 'not got').toLocaleUpperCase() }
                                                                                    </strong>
                                                                                );
                                                                            })(bitWiseTools.getFlagValue(flags)(masks))
                                                                        }
                                                                    </strong>
                                                                </span>
                                                            </li>
                                                        );
                                                    })
                                                }
                                            </Fragment>
                                        );
                                    })
                                }
                            </div>
                        );
                    })

                }
            </ul>
        </div>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(FlagsTooltip);