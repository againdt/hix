package com.getinsured.ssap.service

import com.getinsured.iex.ssap.*
import com.getinsured.ssap.model.ComparisonResult
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

/**
 * Implementation of [SsapComparisonService]
 *
 * @since 08-10-2019
 * @author Yevgen Golubenko
 */
@Service("ssapComparisonService")
class SsapComparisonServiceImpl : SsapComparisonService {
  override fun compare(newSsap: SingleStreamlinedApplication, oldSsap: SingleStreamlinedApplication): List<ComparisonResult> {
    val newMembers = getPrimaryTaxHouseholdMembers(newSsap)
    val oldMembers = getPrimaryTaxHouseholdMembers(oldSsap)

    val results = ArrayList<ComparisonResult>()
    val newHasMoreMembers = newMembers.size > oldMembers.size

    if(newHasMoreMembers) {
      ComparisonResult.values().filter { it != ComparisonResult.NONE }.forEach { cr -> results.add(cr) }
    } else {
      val newAppFinancial = newSsap.applyingForFinancialAssistanceIndicator

      if (newAppFinancial && isIfsvDataDifferent(newMembers, oldMembers)) {
        results.add(ComparisonResult.IFSV)
      }

      if (newAppFinancial && isNonEsiMecDataDifferent(newMembers, oldMembers)) {
        results.add(ComparisonResult.NON_ESI_MEC)
      }

      if (isSsacDataDifferent(newMembers, oldMembers)) {
        results.add(ComparisonResult.SSAC)
      }

      if (isVlpDataDifferent(newMembers, oldMembers)) {
        results.add(ComparisonResult.VLP)
      }
    }

    if (results.isEmpty()) {
      results.add(ComparisonResult.NONE)
    }

    return results
  }

  private fun isIfsvDataDifferent(one: List<HouseholdMember>, two: List<HouseholdMember>): Boolean {
    one.forEach { oneMember ->
      val twoMember = getHouseholdMemberByPersonId(two, oneMember.personId) ?: return@forEach

      when {
        isNameDifferent(oneMember.name, twoMember.name) -> return true
        isSsnDifferent(oneMember.socialSecurityCard, twoMember.socialSecurityCard) -> return true
        else -> {
          val incomeOne = HouseholdIncomeCalculator.calculateGrossIncome(listOf(oneMember))
          val incomeTwo = HouseholdIncomeCalculator.calculateGrossIncome(listOf(twoMember))

          if (incomeOne != incomeTwo) {
            return true
          }

          val expensesOne = HouseholdIncomeCalculator.calculateHouseholdExpenses(listOf(oneMember))
          val expensesTwo = HouseholdIncomeCalculator.calculateHouseholdExpenses(listOf(twoMember))

          if (expensesOne != expensesTwo) {
            return true
          }
        }
      }
    }

    return false
  }

  private fun isSsacDataDifferent(one: List<HouseholdMember>, two: List<HouseholdMember>): Boolean {
    one.forEach { oneMember ->
      val twoMember = getHouseholdMemberByPersonId(two, oneMember.personId) ?: return@forEach

      when {
        isNameDifferent(oneMember.name, twoMember.name) -> return true
        isSsnDifferent(oneMember.socialSecurityCard, twoMember.socialSecurityCard) -> return true
        isDateDifferent(oneMember.dateOfBirth, twoMember.dateOfBirth) -> return true
        isIncarcerationInfoDifferent(oneMember.incarcerationStatus, twoMember.incarcerationStatus) -> return true
        isCitizenshipInfoDifferent(oneMember.citizenshipImmigrationStatus, twoMember.citizenshipImmigrationStatus) -> return true
      }
    }

    return false
  }

  private fun isVlpDataDifferent(one: List<HouseholdMember>, two: List<HouseholdMember>): Boolean {
    one.forEach { oneMember ->
      val twoMember = getHouseholdMemberByPersonId(two, oneMember.personId) ?: return@forEach

      when {
        isNameDifferent(oneMember.name, twoMember.name) -> return true
        isDateDifferent(oneMember.dateOfBirth, twoMember.dateOfBirth) -> return true
        isCitizenshipInfoDifferent(oneMember.citizenshipImmigrationStatus, twoMember.citizenshipImmigrationStatus) -> return true
      }
    }
    return false
  }

  private fun isNonEsiMecDataDifferent(one: List<HouseholdMember>, two: List<HouseholdMember>): Boolean {
    one.forEach { oneMember ->
      val twoMember = getHouseholdMemberByPersonId(two, oneMember.personId) ?: return@forEach

      if (isNameDifferent(oneMember.name, twoMember.name)) {
        return true
      }

      if(isSsnDifferent(oneMember.socialSecurityCard, twoMember.socialSecurityCard)) {
        return true
      }

      if(isDateDifferent(oneMember.dateOfBirth, twoMember.dateOfBirth)) {
        return true
      }

      if(isGenderStringDifferent(oneMember.gender,twoMember.gender)) {
        return true
      }

      if(isHealthCoverageDifferent(oneMember.healthCoverage, twoMember.healthCoverage)) {
        return true
      }

      if(isAddressDifferent(oneMember.otherAddress?.address, twoMember.otherAddress?.address)) {
        return true
      }

      if(isAddressDifferent(oneMember.householdContact?.homeAddress, twoMember.householdContact?.homeAddress)) {
        return true
      }
    }

    return false
  }

  private fun isNameDifferent(one: Name?, two: Name?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      else -> one?.firstName != two?.firstName || one?.lastName != two?.lastName
    }
  }

  private fun isSsnDifferent(one: SocialSecurityCard?, two: SocialSecurityCard?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      else -> one?.socialSecurityNumber != two?.socialSecurityNumber
    }
  }

  private fun isDateDifferent(one: Date?, two: Date?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      else -> one != two
    }
  }

  private fun isGenderStringDifferent(one: String?, two: String?): Boolean {
    if((one == null) != (two == null)){
      return true
    }

    if((one != null && one.toLowerCase().startsWith("m")) && (two != null && !two.toLowerCase().startsWith("m")))  {
      return true
    }

    if((one != null && one.toLowerCase().startsWith("f")) && (two != null && !two.toLowerCase().startsWith("f")))  {
      return true
    }

    return false
  }

  private fun isIncarcerationInfoDifferent(one: IncarcerationStatus?, two: IncarcerationStatus?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      one?.incarcerationStatusIndicator != two?.incarcerationStatusIndicator -> true
      one?.useSelfAttestedIncarceration != two?.useSelfAttestedIncarceration -> true
      one?.incarcerationAsAttestedIndicator != two?.incarcerationAsAttestedIndicator -> true
      one?.incarcerationManualVerificationIndicator != two?.incarcerationManualVerificationIndicator -> true
      one?.incarcerationPendingDispositionIndicator != two?.incarcerationPendingDispositionIndicator -> true
      else -> false
    }
  }

  private fun isCitizenshipInfoDifferent(one: CitizenshipImmigrationStatus?, two: CitizenshipImmigrationStatus?): Boolean {
    // Even though it's an array, we only store 1 object in it, but for the sake of future proofing lets loop.
    // Loop however is no guaranteed to be ordered because it's an ArrayList and just Array on the UI.
    one?.eligibleImmigrationDocumentType?.forEachIndexed { index, oneDocumentType: EligibleImmigrationDocumentType? ->
      val twoDocumentType = two?.eligibleImmigrationDocumentType?.get(index)
      if(isEligibleImmigrationDocumentTypeDifferent(oneDocumentType, twoDocumentType)) {
        return true
      }
    }

    return when {
      (one == null) != (two == null) -> true
      one?.honorablyDischargedOrActiveDutyMilitaryMemberIndicator != two?.honorablyDischargedOrActiveDutyMilitaryMemberIndicator -> true
      one?.citizenshipStatusIndicator != two?.citizenshipStatusIndicator -> true
      one?.naturalizationCertificateIndicator != two?.naturalizationCertificateIndicator -> true
      one?.citizenshipManualVerificationStatus != two?.citizenshipManualVerificationStatus -> true
      one?.citizenshipAsAttestedIndicator != two?.citizenshipAsAttestedIndicator -> true
      one?.eligibleImmigrationStatusIndicator != two?.eligibleImmigrationStatusIndicator -> true
      one?.lawfulPresenceIndicator != two?.lawfulPresenceIndicator -> true
      one?.naturalizationCertificateAlienNumber != two?.naturalizationCertificateAlienNumber -> true
      one?.eligibleImmigrationDocumentSelected != two?.eligibleImmigrationDocumentSelected -> true
      one?.naturalizedCitizenshipIndicator != two?.naturalizedCitizenshipIndicator -> true
      one?.livedIntheUSSince1996Indicator != two?.livedIntheUSSince1996Indicator -> true
      one?.naturalizationCertificateNaturalizationNumber != two?.naturalizationCertificateNaturalizationNumber -> true
      isCitizenshipDocumentDifferent(one?.citizenshipDocument, two?.citizenshipDocument) -> true
      isOtherImmigrationDocumentDifferent(one?.otherImmigrationDocumentType, two?.otherImmigrationDocumentType) -> true
      else -> false
    }
  }

  private fun isEligibleImmigrationDocumentTypeDifferent(one: EligibleImmigrationDocumentType?, two: EligibleImmigrationDocumentType?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      one?.i94Indicator != two?.i94Indicator -> true
      one?.i20Indicator != two?.i20Indicator -> true
      one?.i327Indicator != two?.i327Indicator -> true
      one?.i551Indicator != two?.i551Indicator -> true
      one?.i571Indicator != two?.i571Indicator -> true
      one?.i766Indicator != two?.i766Indicator -> true
      one?.i797Indicator != two?.i797Indicator -> true
      one?.dS2019Indicator != two?.dS2019Indicator -> true
      one?.i94InPassportIndicator != two?.i94InPassportIndicator -> true
      one?.otherDocumentTypeIndicator != two?.otherDocumentTypeIndicator -> true
      one?.temporaryI551StampIndicator != two?.temporaryI551StampIndicator -> true
      one?.machineReadableVisaIndicator != two?.machineReadableVisaIndicator -> true
      one?.unexpiredForeignPassportIndicator != two?.unexpiredForeignPassportIndicator -> true
      else -> false
    }
  }

  private fun isCitizenshipDocumentDifferent(one: CitizenshipDocument?, two: CitizenshipDocument?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      isDateDifferent(one?.documentExpirationDate, two?.documentExpirationDate) -> true
      isNameDifferent(one?.nameOnDocument, two?.nameOnDocument) -> true
      one?.nameSameOnDocumentIndicator != two?.nameSameOnDocumentIndicator -> true
      one?.alienNumber != two?.alienNumber -> true
      one?.cardNumber != two?.cardNumber -> true
      one?.documentDescription != two?.documentDescription -> true
      one?.foreignPassportCountryOfIssuance != two?.foreignPassportCountryOfIssuance -> true
      one?.foreignPassportOrDocumentNumber != two?.foreignPassportOrDocumentNumber -> true
      one?.i94Number != two?.i94Number -> true
      one?.visaNumber != two?.visaNumber -> true
      one?.nameSameOnDocumentIndicator != two?.nameSameOnDocumentIndicator -> true
      one?.getSEVISId() != two?.getSEVISId() -> true
      one?.getSevisId() != two?.getSevisId() -> true
      else -> false
    }
  }

  private fun isOtherImmigrationDocumentDifferent(one: OtherImmigrationDocumentType?, two: OtherImmigrationDocumentType?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      one?.americanSamoanIndicator != two?.americanSamoanIndicator -> true
      one?.cubanHaitianEntrantIndicator != two?.cubanHaitianEntrantIndicator -> true
      one?.orrCertificationIndicator != two?.orrCertificationIndicator -> true
      one?.orrEligibilityLetterIndicator != two?.orrEligibilityLetterIndicator -> true
      one?.stayOfRemovalIndicator != two?.stayOfRemovalIndicator -> true
      one?.withholdingOfRemovalIndicator != two?.withholdingOfRemovalIndicator -> true
      else -> false
    }
  }

  private fun isHealthCoverageDifferent(one: HealthCoverage?, two: HealthCoverage?): Boolean {
    return when {
      one?.haveBeenUninsuredInLast6MonthsIndicator != two?.haveBeenUninsuredInLast6MonthsIndicator -> true
      one?.otherInsuranceIndicator != two?.otherInsuranceIndicator -> true
      one?.currentlyEnrolledInCobraIndicator != two?.currentlyEnrolledInCobraIndicator -> true
      one?.currentlyHasHealthInsuranceIndicator != two?.currentlyHasHealthInsuranceIndicator -> true
      one?.employerWillOfferInsuranceIndicator != two?.employerWillOfferInsuranceIndicator -> true
      one?.currentlyEnrolledInRetireePlanIndicator != two?.currentlyEnrolledInRetireePlanIndicator -> true
      one?.currentlyEnrolledInVeteransProgramIndicator != two?.currentlyEnrolledInVeteransProgramIndicator -> true
      one?.hasPrimaryCarePhysicianIndicator != two?.hasPrimaryCarePhysicianIndicator -> true
      one?.willBeEnrolledInCobraIndicator != two?.willBeEnrolledInCobraIndicator -> true
      one?.willBeEnrolledInVeteransProgramIndicator != two?.willBeEnrolledInVeteransProgramIndicator -> true
      one?.isOfferedHealthCoverageThroughJobIndicator != two?.isOfferedHealthCoverageThroughJobIndicator -> true
      one?.willBeEnrolledInRetireePlanIndicator != two?.willBeEnrolledInRetireePlanIndicator -> true
      one?.stateHealthBenefit != two?.stateHealthBenefit -> true
      one?.eligibleITU != two?.eligibleITU -> true
      one?.receivedITU != two?.receivedITU -> true
      one?.unpaidBill != two?.unpaidBill -> true
      one?.coveredDependentChild != two?.coveredDependentChild -> true
      one?.absentParent != two?.absentParent -> true
      isDateDifferent(one?.employerWillOfferInsuranceStartDate, two?.employerWillOfferInsuranceStartDate) -> true
      isDateDifferent(one?.employerWillOfferInsuranceCoverageStartDate, two?.employerWillOfferInsuranceCoverageStartDate) -> true
      isCurrentOtherInsuranceDifferent(one?.currentOtherInsurance, two?.currentOtherInsurance) -> true
      else -> false
    }
  }

  private fun isCurrentOtherInsuranceDifferent(one: CurrentOtherInsurance?, two: CurrentOtherInsurance?): Boolean {

    one?.otherStateOrFederalPrograms?.forEachIndexed { index, oneMember ->
      val twoMember = two?.otherStateOrFederalPrograms?.get(index) ?: return@forEachIndexed
      if(isOtherStateOrFederalProgramDifferent(oneMember, twoMember)) {
        return true
      }
    }

    return when {
      one?.isHasNonESI != two?.isHasNonESI -> true
      one?.peaceCorpsIndicator != two?.peaceCorpsIndicator -> true
      one?.medicareEligibleIndicator != two?.medicareEligibleIndicator -> true
      one?.nonofTheAbove != two?.nonofTheAbove -> true
      one?.otherStateOrFederalProgramIndicator != two?.otherStateOrFederalProgramIndicator -> true
      else -> false
    }
  }

  private fun isOtherStateOrFederalProgramDifferent(one: OtherStateOrFederalProgram?, two: OtherStateOrFederalProgram?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      one?.type != two?.type -> true
      one?.isEligible != two?.isEligible -> true
      one?.name != two?.name -> true
      else -> false
    }
  }

  private fun isAddressDifferent(one: Address?, two: Address?): Boolean {
    return when {
      (one == null) != (two == null) -> true
      one?.streetAddress1 != two?.streetAddress1 -> true
      one?.streetAddress2 != two?.streetAddress2 -> true
      one?.city != two?.city -> true
      one?.state != two?.state -> true
      one?.postalCode != two?.postalCode -> true
      one?.countyCode != two?.countyCode -> true
      one?.county != two?.county -> true
      one?.primaryAddressCountyFipsCode != two?.primaryAddressCountyFipsCode -> true
      else -> false
    }
  }


  private fun getPrimaryTaxHouseholdMembers(application: SingleStreamlinedApplication): List<HouseholdMember> {
    return application.taxHousehold[0].householdMember
  }

  private fun getHouseholdMemberByPersonId(members: List<HouseholdMember>, personId: Int): HouseholdMember? {
    // TODO: Once we complete multi-tax household logic, update this method to get members in primary's tax household.
    return members.find { m -> m.personId == personId }
  }
}
