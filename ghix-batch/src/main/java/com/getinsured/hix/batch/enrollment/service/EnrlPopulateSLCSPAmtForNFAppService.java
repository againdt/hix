package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

public interface EnrlPopulateSLCSPAmtForNFAppService {

	void processSLCSPAmountForNFApp(List<Long> ssapApplicationIdList, String coverageYear);

	void processSLCSPAmountForNFApp(String coverageYear);

}
