package com.getinsured.hix.batch.hub.exceptions;

public class RecordProcessingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public RecordProcessingException() {
		super();
	}

	public RecordProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

	public RecordProcessingException(String message) {
		super(message);
	}

	public RecordProcessingException(Throwable cause) {
		super(cause);
	}
}
