package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for naturalization number
 * 
 * @author Nikhil Talreja
 * @since 03-Feb-2014
 *
 */
public class NaturalizationNumberValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NaturalizationNumberValidator.class);
	
	/** 
	 * Validations for naturalization number
	 * 1. Should be a number
	 * 2. Should be between 7 and 12 digits
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String naturalizationNumber = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			naturalizationNumber = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(naturalizationNumber.length() < VLPServiceConstants.NAT_NUMBER_MIN_LEN
				|| naturalizationNumber.length() > VLPServiceConstants.NAT_NUMBER_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.NAT_NUMBER_LEN_ERROR_MESSAGE);
		}
		
		try{
			Long.parseLong(naturalizationNumber);
		}
		catch(NumberFormatException e){
			throw new InputDataValidationException(name + ": "+VLPServiceConstants.NON_NUMERIC_ERROR_MESSAGE, e);
		}
		
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
