package com.getinsured.iex.dto;

import com.getinsured.hix.model.Location;

public class SsapPreferencesDTO{
	private long householdId;
	private Location prefContactLocation;
	private String preferedWrittenLanguages;
	private String preferedSpokenLangages;
	private String preferedContactMethod;
	private boolean updateMailingAddress;
	private boolean updatePreferences;
	private int updatedBy;
	private boolean triggerMailingLocationApi;
	private String userName;
	
	public String getPreferedContactMethod() {
		return preferedContactMethod;
	}
	public void setPreferedContactMethod(String preferedContactMethod) {
		this.preferedContactMethod = preferedContactMethod;
	}
	
	public boolean isUpdatePreferences() {
		return updatePreferences;
	}
	public void setUpdatePreferences(boolean updatePreferences) {
		this.updatePreferences = updatePreferences;
	}
	public long getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(long householdId) {
		this.householdId = householdId;
	}
	public int getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getPreferedWrittenLanguages() {
		return preferedWrittenLanguages;
	}
	public void setPreferedWrittenLanguages(String preferedWrittenLanguages) {
		this.preferedWrittenLanguages = preferedWrittenLanguages;
	}
	public String getPreferedSpokenLangages() {
		return preferedSpokenLangages;
	}
	public void setPreferedSpokenLangages(String preferedSpokenLangages) {
		this.preferedSpokenLangages = preferedSpokenLangages;
	}
	public boolean isUpdateMailingAddress() {
		return updateMailingAddress;
	}
	public void setUpdateMailingAddress(boolean updateMailingAddress) {
		this.updateMailingAddress = updateMailingAddress;
	}
	public Location getPrefContactLocation() {
		return prefContactLocation;
	}
	public void setPrefContactLocation(Location prefContactLocation) {
		this.prefContactLocation = prefContactLocation;
	}
	public boolean isTriggerMailingLocationApi() {
		return triggerMailingLocationApi;
	}
	public void setTriggerMailingLocationApi(boolean triggerMailingLocationApi) {
		this.triggerMailingLocationApi = triggerMailingLocationApi;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}
