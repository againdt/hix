import React, { Fragment, useEffect, useState } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

// Components

import Form from './Form';
import PageHeader from '../../components/headers/PageHeader';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';
import SourceSummary from './pageIncludes/summary/SourceSummary';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData, clearMethods, setMethods, showModal } from '../../actions/actionCreators';

// Utils

import incomeCalculator from '../../utils/incomeCalculator';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import INPUT_TYPES from '../../constants/inputTypes';
import MODAL_TYPES from '../../constants/modalTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import { INCOME_TYPES } from '../../constants/incomeAndExpenceTypes';


const Page3_32_E1 = props => {

    const {
        data,
        data: {
            singleStreamlinedApplication: {
                coverageYear
            }
        },
        form: {
            Form: {
                isFormOnTopLevelValid,
                areAllInputsValid,
                isFormSubmitted
            }
        },
        householdMembers,
        page
    } = props;

    const {
        buttonContainer: ButtonContainer,
        isFirstVisit
    } = props;

    const {
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        incomes,
        name
    } = currentHhm;

    const incomesWithoutTribalOne = incomes.filter(income => income.type !== INCOME_TYPES.AI_AN);

    const [ localState, setLocalState ] = useState({
        isAnyIncome: isFirstVisit ? null : !!incomesWithoutTribalOne.length
    });

    const {
        isAnyIncome
    } = localState;

    const handleIncomeSourceDeletion = (member, incomeIndex) => {
        const newState = { ...data };

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .incomes
            .splice(incomeIndex, 1);

        const {
            incomes: currentIncomes,
            expenses: currentExpenses
        } = newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member];

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .annualTaxIncome
            .reportedIncome = incomeCalculator.getTotalIncome(currentIncomes, currentExpenses);

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .incomes
            .length === 0
                && setLocalState(state => ({
                    ...state,
                    isAnyIncome: null
                }));

        props.actions.changeData(newState);
    };

    const handleIncomeSourceEditing = incomeIndex => {

        props.actions.showModal(
            MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL,
            MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL,
            [
                incomes[incomeIndex],
                incomeIndex,
                'incomes',
                page
            ]
        );
    };

    const handleAddIncomeSourceClick = () => {
        props.actions.showModal(
            MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL,
            MODAL_TYPES.ADD_INCOME_OR_DEDUCTION_SOURCE_MODAL,
            [
                {},
                null,
                'incomes',
                page
            ]
        );
    };

    useEffect(() => {
        props.actions.setMethods('Page3_32_E1',
            {
                handleIncomeSourceDeletion,
                handleIncomeSourceEditing,
                handleAddIncomeSourceClick
            });

        return () => {
            props.actions.clearMethods('Page3_32_E1');
        };
    }, [ incomes.map(income => Object.values(income).toLocaleString()).toLocaleString() ]);

    const onFieldChange_local = newData => {
        setLocalState(state => ({
            ...state,
            ...newData
        }));
    };

    const checkIfDisabled = () => {
        return incomesWithoutTribalOne.length > 0 ? false : !isAnyIncome;
    };

    const checkTopLevelValidation = () => {
        switch(true) {
        case isAnyIncome === true:
            return incomesWithoutTribalOne.length > 0;
        case isAnyIncome === false:
            return true;
        case isAnyIncome === null:
            return false;
        }
    };

    return (
        <Fragment>
            <div id="iiIncomeSources" className="">
                <div className="subsection">
                    <PageHeader
                        text={`Income of ${normalizeNameOfPerson(name)}`}
                        headerClassName={'usa-heading-alt'}
                    />
                    <p>
                        <FormattedMessage
                            id="ssap.incomeSources.msg.one"
                            defaultMessage="People can get income in many ways. After you tell us about your
                           current income we will help you estimate income for all of {coverageYear}
                           so you can tell us if you expect changes."
                            values={{ coverageYear }}
                        />
                    </p>
                    <div className="usa grid usa-width-one-whole">
                        <div className="usa-width-one-third">
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeOne"
                                    defaultMessage="Job"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeTwo"
                                    defaultMessage="Pension"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeThree"
                                    defaultMessage="Rental or Royalty"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeFour"
                                    defaultMessage="Alimony Received"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeThirteen"
                                    defaultMessage="Scholarship"
                                />
                            </p>
                        </div>
                        <div className="usa-width-one-third">
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeFive"
                                    defaultMessage="Self Employment"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeSix"
                                    defaultMessage="Social Security Benefits"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeSeven"
                                    defaultMessage="Farming or Fishing"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeEight"
                                    defaultMessage="Investment"
                                />
                            </p>
                        </div>
                        <div className="usa-width-one-third">
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeNine"
                                    defaultMessage="Retirement"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeTen"
                                    defaultMessage="Capital Gains"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeEleven"
                                    defaultMessage="Unemployment"
                                />
                            </p>
                            <p>
                                <FormattedMessage
                                    id="ssap.incomeSources.msg.incomeTypeTwelve"
                                    defaultMessage="Other Income"
                                />
                            </p>
                        </div>
                    </div>



                    <Form topLevelValidity={checkTopLevelValidation()}>

                        {
                            incomesWithoutTribalOne.length === 0 ?

                                <div className="usa-width-one-whole  margin-tb-20">

                                    <p>
                                        <FormattedMessage
                                            id="ssap.incomeSources.msg.two"
                                            defaultMessage="Enter all your current Income Types"
                                        />
                                    </p>

                                    <ValidationErrorMessage
                                        isVisible={isFormSubmitted && !isFormOnTopLevelValid && areAllInputsValid}
                                        messageText={'You should add at least one income source or select \'No\''}
                                        errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                                    />

                                    <UniversalInput
                                        legend={{
                                            legendText: `Does ${normalizeNameOfPerson(name)} currently get any income?`
                                        }}
                                        label={{
                                            labelClass: ['usa-width-one-whole', 'layout--02', 'margin-t-0'],
                                            ulClass:['usa-width-one-whole'],
                                            showRequiredIcon: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'isAnyIncome',
                                            value: isAnyIncome,
                                            options: DROPDOWN_OPTIONS.YES_NO,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: localState,
                                            fields: []
                                        }}
                                        inputActions={{
                                            onFieldChange: onFieldChange_local
                                        }}
                                    />
                                </div> :

                                <div className="usa-width-one-whole  margin-tb-20">
                                    <p>
                                        <strong>
                                            <FormattedMessage
                                                id="ssap.incomeSources.msg.three"
                                                defaultMessage="Add another type of income or continue to review a summary of your current income."
                                            />
                                        </strong>
                                    </p>
                                    <SourceSummary
                                        buttonContainer={ButtonContainer}
                                        sources={incomesWithoutTribalOne}
                                        member={member}
                                        dropdownSourcesList={DROPDOWN_OPTIONS.INCOME_SOURCES}
                                        dropdownFrequencyList={DROPDOWN_OPTIONS.FREQUENCY}
                                        summaryType={'Income'}
                                    />
                                </div>
                        }

                    </Form>

                    <ButtonContainer
                        customButtonClassName={checkIfDisabled() ? 'btn-disabled' : ''}
                        type="addIncomeSourceButton"
                        args={[householdMembers, member]}
                        isDisabled={checkIfDisabled()}
                    />

                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        clearMethods,
        setMethods,
        showModal
    }, dispatch)
});

Page3_32_E1.defaultProps = {
    name: 'Page3_32_E1'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page3_32_E1);
