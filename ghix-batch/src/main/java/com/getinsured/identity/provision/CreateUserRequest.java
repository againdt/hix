//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.12.21 at 03:20:33 PM PST 
//

package com.getinsured.identity.provision;

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

public class CreateUserRequest {

	protected String remoteId;
	protected NameType name;
	protected XMLGregorianCalendar dateOfBirth;
	protected TaxIdentifierType ssn;
	protected PhoneType phone;
	protected CredentialsType credentials;
	protected List<AttributeType> attribute;
	protected String email;
	protected String username;

	public String getRemoteId() {
		return remoteId;
	}

	public void setRemoteId(String value) {
		this.remoteId = value;
	}

	public NameType getName() {
		return name;
	}

	public void setName(NameType value) {
		this.name = value;
	}

	public XMLGregorianCalendar getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(XMLGregorianCalendar value) {
		this.dateOfBirth = value;
	}

	public TaxIdentifierType getSsn() {
		return ssn;
	}

	public void setSsn(TaxIdentifierType value) {
		this.ssn = value;
	}

	public PhoneType getPhone() {
		return phone;
	}

	public void setPhone(PhoneType value) {
		this.phone = value;
	}

	public CredentialsType getCredentials() {
		return credentials;
	}

	public void setCredentials(CredentialsType value) {
		this.credentials = value;
	}

	public List<AttributeType> getAttribute() {
		if (attribute == null) {
			attribute = new ArrayList<AttributeType>();
		}
		return this.attribute;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String value) {
		this.email = value;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String value) {
		this.username = value;
	}

}
