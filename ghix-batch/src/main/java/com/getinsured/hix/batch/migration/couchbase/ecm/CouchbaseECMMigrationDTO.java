package com.getinsured.hix.batch.migration.couchbase.ecm;

public class CouchbaseECMMigrationDTO {
	private String tableName;
	private String columnName;
	private String category;
	private String subCategory;
	private String primaryIdColumn = "ID";

	public CouchbaseECMMigrationDTO(String tableName, String columnName, String category, String subCategory) {
		this.tableName = tableName;
		this.columnName = columnName;
		this.category = category;
		this.subCategory = subCategory;
	}

	public CouchbaseECMMigrationDTO(String tableName, String columnName, String category, String subCategory,
			String primaryIdColumn) {
		this.tableName = tableName;
		this.columnName = columnName;
		this.category = category;
		this.subCategory = subCategory;
		this.primaryIdColumn = primaryIdColumn;
	}

	public String getTableName() {
		return tableName.toUpperCase();
	}

	public String getColumnName() {
		return columnName.toUpperCase();
	}

	public String getCategory() {
		return category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public String getPrimaryIdColumn() {
		return primaryIdColumn.toUpperCase();
	}

}
