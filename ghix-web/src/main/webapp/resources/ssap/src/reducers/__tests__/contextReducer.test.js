

import contextReducer, { initialState } from '../contextReducer';
import { contextActionTypes } from '../../constants/actionTypes';


const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const addContextAction = {
    type: contextActionTypes.ADD_CONTEXT,
    name: 'Component',
    context: { component: {} }
};

const previousState = {
    'Component': {}
};

const removeContextAction = {
    type: contextActionTypes.REMOVE_CONTEXT,
    name: 'Component'
};

const clearStateAction = {
    type: contextActionTypes.CLEAR_STATE
};

describe('Test for contextReducer', () => {
    it('This reducer hasn\'t been invoked', () => {
        expect(contextReducer(initialState, anotherAction)).toEqual(initialState);
    });

    it('Should add context', () => {
        expect(contextReducer(initialState, addContextAction)).toEqual({
            ...initialState,
            [addContextAction.name]: addContextAction.context
        });
    });

    it('Should remove context', () => {
        expect(contextReducer(previousState, removeContextAction)).toEqual({
            ...(previousState => { delete previousState[removeContextAction.name]; return previousState; })
        });
    });

    it('Should clear state', () => {
        expect(contextReducer({ welcomeText: 'Hi there!' }, clearStateAction)).toEqual(initialState);
    });

});