package com.getinsured.eligibility;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.indportal.enrollment.api.IndPortalEnrollmentUtility;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicantEventService;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentRequest;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentService;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.Ind57Mapping;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.thoughtworks.xstream.XStream;

@Controller
@RequestMapping("/iex/lce")
public class LCEController {
	private static final Logger LOGGER = LoggerFactory.getLogger(LCEController.class);
	@Autowired private Gson platformGson;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired 
	private IntegrationLogService integrationLogService;
	
	@Autowired
	private ResourceCreator resourceCreator;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired
	@Qualifier("moveEnrollmentService")
	private MoveEnrollmentService moveEnrollmentService;
	
	@Autowired
	private IndPortalEnrollmentUtility indPortalEnrollmentUtility;
	
	@Autowired
	private LifeChangeEventApplicantEventService lifeChangeEventApplicantEventService;
	
	@RequestMapping(value = "/invokeind57", method = RequestMethod.POST)
	@ResponseBody
	public String invokeIND57(@RequestBody LifeChangeEventDTO requestDTO) throws Exception{
		LifeChangeEventDTO responseDTO = new LifeChangeEventDTO();
		String status = null;
		
		try {
			LOGGER.info("triggerAdminUpdate Starts for caseNumber " + requestDTO.getCaseNumber());
			final Ind57Mapping ind57Mapping = new Ind57Mapping();
			ind57Mapping.setCaseNumber(requestDTO.getCaseNumber());
			ind57Mapping.setUserName(requestDTO.getUserName());
			ind57Mapping.setMailingAddressChanged(requestDTO.getMailingAddressChanged());
			if(requestDTO.getMailingAddressChanged() != null && requestDTO.getMailingAddressChanged()){
				ind57Mapping.setUpdatedMailingAddressLocationID(requestDTO.getUpdatedMailingAddressLocationId());
			}
			status = indPortalEnrollmentUtility.invokeAdminUpdateEnrollment(ind57Mapping);
			
			if(null != status && status.equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
				 throw new GIException("IND57 execution failed.");
			}
			
			responseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
			
			LOGGER.info("triggerAdminUpdate Ends for caseNumber " + requestDTO.getCaseNumber());
		} catch(Exception exception){
			responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			integrationLogService.save(ExceptionUtils.getMessage(exception), "LCE", "FAILURE", requestDTO.getOldApplicationId(), null);
		}
		
		return JacksonUtils.getJacksonObjectWriterForJavaType(LifeChangeEventDTO.class).writeValueAsString(responseDTO);
	}
	
	@RequestMapping(value = "/getenrolledapplicants", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrolledApplicants(@RequestBody LifeChangeEventDTO requestDTO) throws Exception{
		LifeChangeEventDTO responseDTO = new LifeChangeEventDTO();
		
		try {
			responseDTO = invokeEnrollmentApi(requestDTO);
			responseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} catch(Exception exception){
			responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		
		return JacksonUtils.getJacksonObjectWriterForJavaType(LifeChangeEventDTO.class).writeValueAsString(responseDTO);
	}
	
	@RequestMapping(value = "/getsepeventnames", method = RequestMethod.POST)
	@ResponseBody
	public String getSepEventNames(@RequestBody SepRequestParamDTO  sepRequestParamDTO) throws Exception{
		
		List<SsapApplicantEvent> list =	lifeChangeEventApplicantEventService.getApplicantEventByApplicationCaseNumber(sepRequestParamDTO.getCaseNumber());
		BigDecimal individualIdDouble = new BigDecimal(sepRequestParamDTO.getHouseholdId());
		boolean individualTested=false;
		JsonArray jsonArray= new JsonArray();
		JsonObject jsonObject;
		JsonObject jsonRootObject = new JsonObject();
		JsonObject jsonStatusObject = new JsonObject();
		
		for(SsapApplicantEvent ssapApplicantEvent:list){
			if(individualTested || individualIdDouble.equals(ssapApplicantEvent.getSsapAplicationEvent().getSsapApplication().getCmrHouseoldId()) ){
				StringBuilder name = new StringBuilder();
				name.append(ssapApplicantEvent.getSsapApplicant().getFirstName());
				name.append(" ");
				name.append(ssapApplicantEvent.getSsapApplicant().getLastName());
				
				if(null != ssapApplicantEvent.getSsapApplicant().getNameSuffix() && !ssapApplicantEvent.getSsapApplicant().getNameSuffix().isEmpty()) {
					name.append(" ");
					name.append(ssapApplicantEvent.getSsapApplicant().getNameSuffix());
				}
						
				jsonObject=new JsonObject();

				String eventName = ssapApplicantEvent.getSepEvents().getLabel();

				String isStateSubsidyEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_SUBSIDY);

				if("Y".equalsIgnoreCase(isStateSubsidyEnabled)){
					if("APTC Auto update".equalsIgnoreCase(eventName)){
						eventName = "APTC/CAPS Auto update";
					}
				}

				jsonObject.addProperty("eventName", eventName);
				jsonObject.addProperty("applicantName", name.toString());
				jsonObject.addProperty("eventDate", LifeChangeEventUtil.formatTimestamp(  ssapApplicantEvent.getEventDate()) );
				
				jsonArray.add(jsonObject);	
				if(!individualTested){
					individualTested=true;
					jsonStatusObject.addProperty("response", "SUCCESS");	
				}
				
			}else{
				jsonStatusObject.addProperty("response", "FAILURE");
				jsonStatusObject.addProperty("message", "Unauthorized access");
			}
		}
		jsonRootObject.add("data", jsonArray);
		jsonRootObject.add("status", jsonStatusObject);
		//return gson.toJson(jsonRootObject);
		return platformGson.toJson(jsonArray);
	}
	
	@RequestMapping(value = "/moveenrollment", method = RequestMethod.POST)
	@ResponseBody
	public String moveEnrollment(@RequestBody LifeChangeEventDTO requestDTO){
		LifeChangeEventDTO responseDTO = new LifeChangeEventDTO();
		MoveEnrollmentRequest moveEnrollmentRequest;
		String responseJSON = null;
		try {
			moveEnrollmentRequest = new MoveEnrollmentRequest();
			moveEnrollmentRequest.setEnrolledSsapApplicationId(requestDTO.getOldApplicationId());
			moveEnrollmentRequest.setNewSsapApplicationId(requestDTO.getNewApplicationId());
			moveEnrollmentRequest.setUserName(requestDTO.getUserName());
			responseDTO.setStatus(moveEnrollmentService.process(moveEnrollmentRequest));
		} catch(Exception exception){
			responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		try {
			responseJSON = JacksonUtils.getJacksonObjectWriterForJavaType(LifeChangeEventDTO.class).writeValueAsString(responseDTO);
		} catch (JsonProcessingException exception) {
			LOGGER.error("Unexpected error occured in while converting Object to String JSON in ghix-eligibility." , exception); 
			throw new GIRuntimeException(exception, ExceptionUtils.getMessage(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return  responseJSON;
	}
	
	private LifeChangeEventDTO invokeEnrollmentApi(LifeChangeEventDTO lifeChangeEventDTO) throws Exception {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse;
		EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		enrolleeRequest.setSsapApplicationId(lifeChangeEventDTO.getOldApplicationId());
		ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID, lifeChangeEventDTO.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrolleeRequest));
		enrolleeResponse = (EnrolleeResponse) xstream.fromXML(response.getBody());
		
		if (null != enrolleeResponse && null!= enrolleeResponse.getStatus() && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
			enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
			return extractEnrolledData(enrollmentShopDTOs, executeCompare(lifeChangeEventDTO.getOldApplicationId()), lifeChangeEventDTO);
		} else {
			StringBuilder errorReason = new StringBuilder();
			if(null != enrolleeResponse) {
			errorReason.append(enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
			}
			throw new GIException("Unable to get Enrollment Plan Details. Error Details: " + errorReason);
		}
	}
	
	private CompareMainDTO executeCompare(long enrolledApplicationId) throws Exception {
		CompareMainDTO compareMainDTO = null;
		
		try {
			final SsapApplication enrolledApplication = loadSsapApplicants(enrolledApplicationId);
			compareMainDTO = transformCompareDto(enrolledApplication);
		} catch (Exception e) {
			throw e;
		}
		
		return compareMainDTO;
	}
	
	private LifeChangeEventDTO extractEnrolledData(List<EnrollmentShopDTO> enrollmentShopDTOs, CompareMainDTO compareMainDTO, LifeChangeEventDTO lifeChangeEventDTO) {
		Set<Long> personIds = new HashSet<>();

		for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs) {
			List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentShopDTO.getEnrolleeShopDTOList();
			for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
				for (CompareApplicantDTO enrolledApplicant : compareMainDTO.getEnrolledApplication().getApplicants()) {
					if (enrolleeMembers.getExchgIndivIdentifier().equals(enrolledApplicant.getApplicantGuid())) {
						personIds.add(enrolledApplicant.getPersonId());
					}
				}
			}
		}
		
		lifeChangeEventDTO.setPersonIds(personIds);
		
		return lifeChangeEventDTO;
	}
	
	private CompareMainDTO transformCompareDto(SsapApplication enrolledApplication) {
		CompareMainDTO compareMainDTO = new CompareMainDTO();
		compareMainDTO.setEnrolledApplication(transformApplication(enrolledApplication));
		return compareMainDTO;
	}
	
	private SsapApplication loadSsapApplicants(Long enApp) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(enApp);
	}
	
	public CompareApplicationDTO transformApplication(SsapApplication ssapApplication) {
		final CompareApplicationDTO compareApplicationDTO = new CompareApplicationDTO();
		ReferralUtil.copyProperties(ssapApplication, compareApplicationDTO);
		transformApplicants(compareApplicationDTO, ssapApplication);
		return compareApplicationDTO;
	}

	private void transformApplicants(CompareApplicationDTO compareApplicationDTO, SsapApplication ssapApplication) {
		CompareApplicantDTO compareApplicantDTO;
		for (SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()) {
			compareApplicantDTO = new CompareApplicantDTO();
			ReferralUtil.copyProperties(ssapApplicant, compareApplicantDTO);
			compareApplicationDTO.addApplicant(compareApplicantDTO);
		}
	}
}
