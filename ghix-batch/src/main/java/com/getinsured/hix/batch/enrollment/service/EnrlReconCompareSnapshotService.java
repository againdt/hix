/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;

/**
 * @author panda_p
 *
 */
public interface EnrlReconCompareSnapshotService {

	boolean compareFileSnapshot(Long fileId, Job job, JobLauncher jobLauncher) throws Exception;
}
