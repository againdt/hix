<script type="text/ng-template" id="maritalstatus-incomededuction.jsp">
	<div class="gutter10"  ng-controller="incomeDeductions" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
	  <h3><spring:message code="label.lce.incomededuction"/></h3>
	  <form class="form-horizontal" id="incomeDeduction" name="incomeDeduction" novalidate>
	    <p><spring:message code="label.lce.p1"/> <strong class="nameOfHouseHold">{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}</strong> <spring:message code="label.lce.pay"/></p>
	    <p><spring:message code="label.lce.p1"/></p>

	    <div class="control-group">
	      <label class="control-label" for="alimony"></label>
	      <div class="controls">
	        <label class="checkbox inline">
	          <!-- <input type="checkbox" name="alimony" id="alimony"  checkbox-group text-box-show="alimonyShow"  array-content="ALIMONY"   check-box-array="householdMember.detailedIncome.deductions.deductionType"> -->
	          <input type="checkbox"  name="alimony" id="alimony" custom-checkbox array-content="ALIMONY"   destination-array="deductionArray" ng-click="addRemoveElement($event);">
	          <spring:message code="label.lce.alimonypaid"/>
	        </label>
	      </div>

			  <!-- Show if alimony is selected -->
			  <div id="alimonyShow" ng-show="displayDeduction('ALIMONY')">
				<div class="control-group margin10-r">
			        <label class="control-label" for="typeOfRequest"><spring:message code="label.lce.alimonypaid"/>: </label>
			        <div class="controls">
			          <div class="input-prepend">
			            <span class="add-on">$</span>
			            <input type="text" class="span4 margin10-r" value="500" name="alimonyDeductionAmount" ng-model="householdMember.detailedIncome.deductions.alimonyDeductionAmount" ng-required="displayDeduction('ALIMONY')" numbers-Only ng-trim>
			            <select class="span5" name="alimonyDeductionAmountFrequency" ng-model="householdMember.detailedIncome.deductions.alimonyDeductionFrequency" ng-required="displayDeduction('ALIMONY')" ng-trim>
			              <option><spring:message code="label.lce.weekly"/>Weekly</option>
			              <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
			              <option><spring:message code="label.lce.monthly"/>Monthly</option>
			              <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			              <option><spring:message code="label.lce.yearly"/>Yearly</option>
			              <option><spring:message code="label.lce.onceonly"/>Once Only</option>
			            </select>
			          </div>
			        </div>
			        <div class="controls margin5-t" ng-show="submitted && incomeDeduction.alimonyDeductionAmount.$error.required">
			        	<span class="validation-error-text-container" ><spring:message code="label.lce.pleaseenteralimonydeduction"/></span>
				    </div>
				    <div class="controls margin5-t" ng-show="submitted && incomeDeduction.alimonyDeductionAmountFrequency.$error.required">
				       <span class="validation-error-text-container"><spring:message code="label.lce.alimonyfrequency"/></span>
				    </div>
			    </div>

			  </div><!-- Show if alimony is selected -->

	      <div class="controls">
		      <label class="checkbox inline" for="student">
		        <input type="checkbox" name="student"  id="student" array-content="STUDENT_LOAN_INTEREST"  custom-checkbox destination-array="deductionArray" ng-click="addRemoveElement($event);">
	            <spring:message code="label.lce.studentloanpaidinterest"/>
		      </label>
	      </div>

			  <!-- Show is student is selected -->
			  <div id="studentShow" ng-show="displayDeduction('STUDENT_LOAN_INTEREST')">
			   <div class="control-group margin10-t">
			        <label class="control-label" for="typeOfRequest"><spring:message code="label.lce.studentloaninterestamount"/>: </label>
			        <div class="controls">
			          <div class="input-prepend">
			            <span class="add-on">$</span>
			            <input type="text" class="span4 margin10-r" value="500" name="studentLoanInterestAmount" ng-model="householdMember.detailedIncome.deductions.studentLoanDeductionAmount" ng-required="displayDeduction('STUDENT_LOAN_INTEREST')" numbers-Only ng-trim>
			            <select class="span5" name="studentLoanInterestFrequency" ng-model="householdMember.detailedIncome.deductions.studentLoanDeductionFrequency" ng-required="displayDeduction('STUDENT_LOAN_INTEREST')" ng-trim>
			              <option><spring:message code="label.lce.weekly"/>Weekly</option>
			              <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
			              <option><spring:message code="label.lce.monthly"/>Monthly</option>
			              <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			              <option><spring:message code="label.lce.yearly"/>Yearly</option>
			              <option><spring:message code="label.lce.onceonly"/>Once Only</option>
			            </select>
			          </div>
			        </div>
			        <div class="controls margin5-t" ng-show="submitted && incomeDeduction.studentLoanInterestAmount.$error.required">
				        <span class="validation-error-text-container"><spring:message code="label.lce.studentloanerror1"/></span>
				    </div>
				    <div class="controls margin5-t" ng-show="submitted && incomeDeduction.studentLoanInterestFrequency.$error.required">
				       <span class="validation-error-text-container"><spring:message code="label.lce.studentloanerror2"/></span>
				    </div>
			    </div>

			 </div><!-- Show is student is selected -->


		    <div class="controls">
		      <label class="checkbox inline" for="other">
		        <input type="checkbox" name="other" id="other" array-content="{{otherDeductionType}}"   custom-checkbox destination-array="deductionArray" ng-click="addRemoveElement($event);">
	          <spring:message code="label.lce.otherdeductions"/>
		      </label>
		    </div>

			  <!-- Show if other is selected -->
			  <div id="otherShow" ng-show="displayOtherDeduction()">
			    <div class="control-group margin10-t">
			        <label class="control-label" for="typeOfRequest"><spring:message code="label.lce.otherdeductionstype"/>: </label>
			        <div class="controls">
			          <input type="text" class="span8" id="otherTextBox" name="otherDeductionType" ng-model="otherDeductionType" ng-blur="addOtherTextToArray();" ng-required="displayOtherDeduction()" ng-trim>
			        </div>
				    <div class="controls margin5-t" ng-show="submitted && incomeDeduction.otherDeductionType.$error.required">
				      <span class="validation-error-text-container"><spring:message code="label.lce.otherdeductionerror1"/></span>
				    </div>
		      	</div>

			    <div class="control-group margin10-t">
			        <label class="control-label">Amount: </label>
			        <div class="controls">
			          <div class="input-prepend">
			            <span class="add-on">$</span>
			            <input type="text" class="span4 margin10-r" value="" name="otherDeductionAmount" ng-model="householdMember.detailedIncome.deductions.otherDeductionAmount" ng-required="displayOtherDeduction()" numbers-Only ng-trim>
			            <select class="span5" name="otherDeductionAmountFrequency" ng-model="householdMember.detailedIncome.deductions.otherDeductionFrequency" ng-required="displayOtherDeduction()" ng-trim>
			              <option><spring:message code="label.lce.weekly"/>Weekly</option>
			              <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
			              <option><spring:message code="label.lce.monthly"/>Monthly</option>
			              <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			              <option><spring:message code="label.lce.yearly"/>Yearly</option>
			              <option><spring:message code="label.lce.onceonly"/>Once Only</option>
			            </select>
			          </div>
			        </div>
			        <div class="controls margin5-t" ng-show="submitted && incomeDeduction.otherDeductionAmount.$error.required">
			        	<span class="validation-error-text-container"><spring:message code="label.lce.otherdeductionerror2"/></span>
				    </div>
				   <div class="controls margin5-t" ng-show="submitted && incomeDeduction.otherDeductionAmountFrequency.$error.required">
				      <span class="validation-error-text-container"><spring:message code="label.lce.otherdeductionerror3"/></span>
				   </div>
			      </div>

			  </div><!-- Show if other is selected -->

		    <div class="controls">
		      <label class="checkbox inline" for="none">
		        <input type="checkbox" name="none" id="none"  ng-checked="areOtherEmpty();" ng-click="clearOthers();">
	          <spring:message code="label.lce.none"/>
		      </label>
		    </div>
	    </div>
	  </form>

	  <!-- BACK & NEXT BUTTON -->
	  <dl class="dl-horizontal pull-right">
	    <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
	    <a ng-show="incomeDeduction.$valid" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
	    <a ng-show="!incomeDeduction.$valid" class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
	  </dl>
	</div>
</script>