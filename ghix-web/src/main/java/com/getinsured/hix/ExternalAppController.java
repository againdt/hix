package com.getinsured.hix;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ExternalAppController {
	
	@RequestMapping(value = "/externalapp", method = RequestMethod.GET)
	protected String redirect_externalapp(Model model, Locale locale) 
	{
	    return "externalapp";
	}
	
	@RequestMapping(value = "/externalapp1", method = RequestMethod.GET)
	protected String redirect_externalapp1(Model model, Locale locale) 
	{
		//for time being hardcoded id, just for testing purpose
		model.addAttribute("id", 1);
       
		return "externalapp1";
	   
	}

	@RequestMapping(value = "/externalapp2", method = RequestMethod.GET)
	protected String redirect_externalapp2(Model model, Locale locale) 
	{
	    return "externalapp2";
	}
}
