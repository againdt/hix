package com.getinsured.hix.batch.enrollment.external.nv.dto;

public class Address {

    private String zipPlus4Code;
    private String streetName1;
    private String cityName;
    private String stateCode;
    private String countyFipsCode;
    private String definingAddressRelationshipRoleTypeCodeName;
    private String definingAddressCategoryTypeCodeName;

    public String getZipPlus4Code() {
        return zipPlus4Code;
    }

    public void setZipPlus4Code(String zipPlus4Code) {
        this.zipPlus4Code = zipPlus4Code;
    }

    public String getStreetName1() {
        return streetName1;
    }

    public void setStreetName1(String streetName1) {
        this.streetName1 = streetName1;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCountyFipsCode() {
        return countyFipsCode;
    }

    public void setCountyFipsCode(String countyFipsCode) {
        this.countyFipsCode = countyFipsCode;
    }

    public String getDefiningAddressRelationshipRoleTypeCodeName() {
        return definingAddressRelationshipRoleTypeCodeName;
    }

    public void setDefiningAddressRelationshipRoleTypeCodeName(String definingAddressRelationshipRoleTypeCodeName) {
        this.definingAddressRelationshipRoleTypeCodeName = definingAddressRelationshipRoleTypeCodeName;
    }

    public String getDefiningAddressCategoryTypeCodeName() {
        return definingAddressCategoryTypeCodeName;
    }

    public void setDefiningAddressCategoryTypeCodeName(String definingAddressCategoryTypeCodeName) {
        this.definingAddressCategoryTypeCodeName = definingAddressCategoryTypeCodeName;
    }
}
