package com.getinsured.eligibility.ssap.integration.at.client.service;

import net.minidev.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.LawfulPresenceDocumentType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.LawfulPresenceStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ImmigrationDocumentCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ImmigrationDocumentCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.iso_3166._2.CountryAlpha3CodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.iso_3166._2.CountryAlpha3CodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;

public class LawfulPresenceMapper {

	private static final String kEY_CITIZENSHIP_DOCUMENT = "citizenshipDocument";
	private static final String kEY_I94_NUMBER = "i94Number";
	private static final String kEY_FOREIGN_PASSPORT_OR_DOCUMENT_NUMBER = "foreignPassportOrDocumentNumber";
	private static final String kEY_ALIEN_NUMBER = "alienNumber";

	/**
	 * creates the LawfulPresenceStatusType element for the user.
	 * @param citizenshipImmigrationStatus
	 * @return LawfulPresenceStatusType
	 */
	public LawfulPresenceStatusType createLawfulPresenceStatusType(JSONObject citizenshipImmigrationStatus) {
		
		LawfulPresenceStatusType lawfulPresenceStatusType = AccountTransferUtil.insuranceApplicationObjFactory.createLawfulPresenceStatusType();
				
		lawfulPresenceStatusType.setLawfulPresenceStatusArrivedBefore1996Indicator(AccountTransferUtil.addBoolean(citizenshipImmigrationStatus.get("livedIntheUSSince1996Indicator")));
				
		LawfulPresenceDocumentType lawfulPresenceDocumentType = AccountTransferUtil.insuranceApplicationObjFactory.createLawfulPresenceDocumentType();
		ImmigrationDocumentCategoryCodeType immigrationDocumentCategoryCodeType = AccountTransferUtil.hixTypeFactory.createImmigrationDocumentCategoryCodeType(); 
		
		// nameSameOnDocumentIndicator
		lawfulPresenceDocumentType.setLawfulPresenceDocumentSameNameIndicator(AccountTransferUtil.addBoolean(((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get("nameSameOnDocumentIndicator")));
		
		IdentificationType documentPersonIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String documentPersonIdentificationString = AccountTransferUtil.basicFactory.createString();
		
		TextType documentPersonIdentificationCategoryText =AccountTransferUtil.niemCoreFactory.createTextType();
		
		IdentificationType documentNumberIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String documentNumberIdentificationString = AccountTransferUtil.basicFactory.createString();
		
		String eligibleImmigrationDocumentSelected = (java.lang.String) citizenshipImmigrationStatus.get("eligibleImmigrationDocumentSelected");
		
		if(SSAPIntegrationConstants.I551.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			documentNumberIdentificationString.setValue((String) ((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get("cardNumber"));
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER));
			documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
			lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
			
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_551.value());
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_551);
		}
		
		if(SSAPIntegrationConstants.DS2019.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			documentNumberIdentificationString.setValue((String) ((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_FOREIGN_PASSPORT_OR_DOCUMENT_NUMBER));
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_I94_NUMBER));
			documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
			lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
			
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.DS_2019.value());
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.DS_2019);
		}
		
		if(SSAPIntegrationConstants.UnexpForeignPassport.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			documentNumberIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_FOREIGN_PASSPORT_OR_DOCUMENT_NUMBER));
			documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
			lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
			documentPersonIdentificationString.setValue((String) ((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_I94_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.UNEXPIRED_FOREIGN_PASSPORT.value());
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.UNEXPIRED_FOREIGN_PASSPORT);
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String countryAlpha3CodeType= AccountTransferUtil.basicFactory.createString();
			countryAlpha3CodeType.setValue(((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get("foreignPassportCountryOfIssuance")));
			documentPersonIdentificationType.setIdentificationJurisdictionISO3166Alpha3Code(countryAlpha3CodeType);					
		}
		
		if(SSAPIntegrationConstants.TempI551.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			documentNumberIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_FOREIGN_PASSPORT_OR_DOCUMENT_NUMBER));
			documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
			lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.TEMPORARY_I_551_STAMP.value());	
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.TEMPORARY_I_551_STAMP);
		}
		
		if(SSAPIntegrationConstants.MacReadI551.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			documentNumberIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_FOREIGN_PASSPORT_OR_DOCUMENT_NUMBER));
			documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
			lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.MACHINE_READABLE_VISA.value());
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.MACHINE_READABLE_VISA);
		}
		
		if(SSAPIntegrationConstants.I94UnexpForeignPassport.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			documentNumberIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_FOREIGN_PASSPORT_OR_DOCUMENT_NUMBER));
			documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
			lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
			
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_I94_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_94_IN_PASSPORT.value());	
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_94_IN_PASSPORT);
		}
		
		if(SSAPIntegrationConstants.I571.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			//documentNumberIdentificationString.setValue((String) citizenshipImmigrationStatus.get(""));
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_571.value());	
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_571);
		}
		
		if(SSAPIntegrationConstants.I766.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			documentNumberIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get("cardNumber"));
			documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
			lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_766.value());	
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_766);
		}
		
		if(SSAPIntegrationConstants.I20.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			//documentNumberIdentificationString.setValue(); ? there is no other number available.ask abhinav how to populate this.
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_20.value());	
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_20);
		}
		
		if(SSAPIntegrationConstants.I94.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			//documentNumberIdentificationString.setValue((String) citizenshipImmigrationStatus.get(""));
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_I94_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_94.value());
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_94);
		}
		
		if(SSAPIntegrationConstants.I797.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			//documentNumberIdentificationString.setValue((String) citizenshipImmigrationStatus.get(""));
			if(StringUtils.isNotBlank((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_I94_NUMBER))){
				documentPersonIdentificationString.setValue((String) ((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_I94_NUMBER));
			}
			if(StringUtils.isNotBlank((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER))){
				documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER));
			}
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_797.value());	
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_797);
		}
		
		if(SSAPIntegrationConstants.I327.equalsIgnoreCase(eligibleImmigrationDocumentSelected)){
			//documentNumberIdentificationString.setValue((String) citizenshipImmigrationStatus.get(""));
			documentPersonIdentificationString.setValue((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get(kEY_ALIEN_NUMBER));
			documentPersonIdentificationCategoryText.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_327.value());	
			immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.I_327);
		}
		
		if(SSAPIntegrationConstants.NaturalizationCertificate.equalsIgnoreCase(eligibleImmigrationDocumentSelected)) {
            documentNumberIdentificationString.setValue(citizenshipImmigrationStatus.get("naturalizationCertificateNaturalizationNumber").toString());
            documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
            lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
            documentPersonIdentificationString.setValue(citizenshipImmigrationStatus.get("naturalizationCertificateAlienNumber").toString());
            documentPersonIdentificationCategoryText.setValue("Naturalization Certificate Number");
            immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.NATURALIZATION_CERTIFICATE);
		}
		
		if(SSAPIntegrationConstants.CitizenshipCertificate.equalsIgnoreCase(eligibleImmigrationDocumentSelected)) {
            documentNumberIdentificationString.setValue((String) citizenshipImmigrationStatus.get("certificateOfCitizenshipCertificateNumber"));
            documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
            lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
            documentPersonIdentificationString.setValue(citizenshipImmigrationStatus.get("certificateOfCitizenshipAlienNumber").toString());
            documentPersonIdentificationCategoryText.setValue("Certificate Of Citizenship");                    
            immigrationDocumentCategoryCodeType.setValue(ImmigrationDocumentCategoryCodeSimpleType.CERTIFICATE_OF_CITIZENSHIP);
		}
		
		

		if(lawfulPresenceStatusType.getLawfulPresenceStatusArrivedBefore1996Indicator() != null && lawfulPresenceStatusType.getLawfulPresenceStatusArrivedBefore1996Indicator().isValue() == true){
			documentPersonIdentificationType.setIdentificationID(documentPersonIdentificationString);
			documentPersonIdentificationType.setIdentificationCategoryText(documentPersonIdentificationCategoryText);
			if(documentPersonIdentificationType != null && documentPersonIdentificationType.getIdentificationID() != null && StringUtils.isNotBlank(documentPersonIdentificationType.getIdentificationID().getValue())) {
				lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().add(documentPersonIdentificationType);
				lawfulPresenceStatusType.getLawfulPresenceStatusImmigrationDocument().add(lawfulPresenceDocumentType);
			}
			if(immigrationDocumentCategoryCodeType != null && immigrationDocumentCategoryCodeType.getValue() != null && StringUtils.isNotBlank(immigrationDocumentCategoryCodeType.getValue().value())) {
				lawfulPresenceDocumentType.setLawfulPresenceDocumentCategoryCode(immigrationDocumentCategoryCodeType);
			}
		}

		EligibilityType eligibilityIndicator = AccountTransferUtil.insuranceApplicationObjFactory.createEligibilityType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean eligibilityIndicatorBoolean = AccountTransferUtil.basicFactory.createBoolean();
		Boolean lawfulPresenceEligibilityIndicator = ((java.lang.Boolean) citizenshipImmigrationStatus.get("eligibleImmigrationStatusIndicator")); 
					//|| ((java.lang.Boolean) citizenshipImmigrationStatus.get("(naturalizedCitizenshipindicator"));
		eligibilityIndicatorBoolean.setValue(lawfulPresenceEligibilityIndicator);
		eligibilityIndicator.setEligibilityIndicator(eligibilityIndicatorBoolean);
		lawfulPresenceStatusType.getLawfulPresenceStatusEligibility().add(eligibilityIndicator);
		return lawfulPresenceStatusType;
	}
}
