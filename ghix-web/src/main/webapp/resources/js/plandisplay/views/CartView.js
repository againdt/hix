(function (views) {
	views.Carts = Backbone.View.extend({
		el : '#cart',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'addOne','removeItems','selectAll','validateCart');
			var self = this;
			self.collection.fetch({
				success : function () {
					self.collection.setCartSummary();
				}
			});
			$('.remove').bind('click', this.removeItems);
			$('.selectAll').bind('click', this.selectAll);
			$('.validateCart').bind('click', this.validateCart);
			self.cartModel = options.cartModel;
			self.collection.bind('reset', self.addAll);
		},
		addAll : function () {
			var self = this;
			this.updateCartSummary();
			$('#cart').html('');
			self.collection.each (self.addOne);
			$('#loadDiv').hide();
		},
		addOne : function (model) {
			var self = this;
			
			var view = new Cart({model:model});
			$('#cart').append(view.render().el);
		},
		updateCartSummary : function () {
			var self = this;
			$('#planCount').html(self.collection.cartSummary.planCount);
			var detailSummary = self.collection.cartSummary.platinumLevel ? "All Platinum Level Plans ("+self.collection.cartSummary.platinumLevelCount+"), " : "";
			detailSummary += self.collection.cartSummary.goldLevel ? "All Gold Level Plans ("+self.collection.cartSummary.goldLevelCount+"), " : "";
			detailSummary += self.collection.cartSummary.silverLevel ? "All Silver Level Plans ("+self.collection.cartSummary.silverLevelCount+"), " : "";
			detailSummary += self.collection.cartSummary.bronzeLevel ? "All Bronze Level Plans ("+self.collection.cartSummary.bronzeLevelCount+"), " : "";
 
			detailSummary += self.collection.cartSummary.platinum ? self.collection.cartSummary.platinum+" Platinum Plans, ":"";
			detailSummary += self.collection.cartSummary.gold ? self.collection.cartSummary.gold+" Gold Plans, " : "";
			detailSummary += self.collection.cartSummary.silver ? self.collection.cartSummary.silver+" Silver Plans, " : "";
			detailSummary += self.collection.cartSummary.bronze ? self.collection.cartSummary.bronze+" Bronze Plans, " : "";
			
			$('#detailSummary').html(detailSummary.substring(0,detailSummary.length-2));
		},
		removeItems : function (e) {
			this.collection.removeSelectedItems();
			this.updateCartSummary();
		},
		selectAll : function (e) {
			this.collection.selectAllItems();
		},
		validateCart : function(){
			if(this.collection.cartItems.length <= 0){
				alert('There are no items in this cart');
				return false;
			}else
				return true;
		}
	});
	var Cart = Backbone.View.extend({
		tagName : 'tbody',
		
		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},

		render : function () {
			var template = _.template($('#tmpCart').html());
			
			if(this.model.get("planId") == 0){
				var template = _.template($('#tmpLevelCart').html());
			}
			if(this.model.get("employeePayInPreimum") < 0){
				this.model.set({employeePayInPreimum: 0});
			}
			$(this.el).html(template(this.model.toJSON()));
			
			return this;
		}
	});
})(App.views);