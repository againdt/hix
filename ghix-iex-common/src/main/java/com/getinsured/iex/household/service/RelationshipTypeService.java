package com.getinsured.iex.household.service;

/**
 * Defines Relationship Type Service.
 *
 * @author Dheeraj Maralkar
 */
public interface RelationshipTypeService {
  /**
   * Check relation is standard or nor for given relationship label 
   *
   * @param name String relationship label.
   */
  boolean isRelationshipStandardForLabel(final String label);
  
  /**
   * Check relation is standard or nor for given relationship code
   *
   * @param name String relationship code.
   */
  boolean isRelationshipStandardForCode(final String code);
}
