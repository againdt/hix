package com.getinsured.eligibility.ssap.integration.household.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.timeshift.TSCalendar;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.common.dto.SsapApplicationUpdateDTO;
import com.getinsured.eligibility.common.service.SsapApplicationUpdateHandler;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.eligibility.ssap.integration.notices.QEPNonFinancialNotificationService;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.dto.SsapSEPIntegrationDTO;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

@Component(value="updateNonFinancialApplicationStatusServiceActivator")
public class UpdateNonFinancialApplicationStatusServiceActivator {

    private static final String NON_FINANCIAL_ASSISTANCE = "N";
    
    private static final String APPEVENT_APPLICATION = "APPEVENT_APPLICATION";
    private static final String APPLICATION_ELIGIBILITY_FULL = "FULL_ELIGIBILITY_RECEIVED";
    private static final String APPLICATION_ELIGIBILITY_CONDITIONAL  = "CONDITIONAL_ELIGIBILITY_RECEIVED";
    private static final String APPLICATION_ELIGIBILITY_DENIED = "APPLICATION_ELIGIBILITY_DENIED";
		private static final String APPLICATION_ID  = "Application ID";
		private static final String APPLICATION_TYPE  = "Application Type";
		private static final String NON_FINANCIAL_APPLICATION  = "Non Financial Application";

	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateNonFinancialApplicationStatusServiceActivator.class);
	@Autowired private Gson platformGson;
    @Autowired
    private SsapApplicationRepository ssapApplicationRepository;

    @Autowired
    private SsapApplicantRepository ssapApplicantRepository;
    
    @Autowired
    private SsapApplicantEventRepository ssapApplicantEventRepository;
    
    @Autowired
    private SsapApplicationEventRepository ssapApplicationEventRepository;
    
    @Autowired
    private NonFinancialApplicationEligibilityService nonFinancialApplicationEligibilityService;
    
    @Autowired
    private SsapApplicationUpdateHandler ssapApplicationUpdateHandler;
    
    @Autowired
    private QEPNonFinancialNotificationService qepNonFinancialNotificationService;
    
    @Autowired 
    private SsapJsonBuilder ssapJsonBuilder;
    
    @Autowired 
    private RestTemplate restTemplate;
    
    @Autowired private AppEventService appEventService;
    @Autowired private LookupService lookupService;
    @Autowired private NoticeService noticeService;

    @SuppressWarnings("rawtypes")
    @ServiceActivator
    public void receive(final Message message) throws Exception {
			String applicationId = message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER).toString();
			SsapApplication ssapApplication = ssapApplicationRepository.getApplicationsById(Long.valueOf(applicationId)).get(0);
			SingleStreamlinedApplication ssapJSON = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
			SsapApplicationEvent applicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(ssapApplication.getId());
			Object ssapSepIntegrationDto = message.getHeaders().get("SSAP_SEP_INTEGRATION_DTO");

			boolean NEW_ELIGIBILITY_ENGINE = "NV".equals(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

    	if(NEW_ELIGIBILITY_ENGINE) {
    		LOGGER.info("NEW eligibility decision engine, doing only verification checks and SEP callbacks.");
				List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplication.getId());
				boolean allApplicantsVerified = nonFinancialApplicationEligibilityService.isVerificationDone(ssapApplicants, ssapJSON);

				if(allApplicantsVerified) {
					LOGGER.debug("SSAP id: {} all verifications done.", applicationId);

					ssapApplication.setEligibilityReceivedDate(new TSDate());
					ssapApplication.setEligibilityStatus(EligibilityStatus.AE);
					ssapApplication = ssapApplicationRepository.save(ssapApplication);

					logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_FULL, ssapApplication);
				} else {
					LOGGER.debug("SSAP id: {} is still pending verifications.", applicationId);
					if(EligibilityStatus.DE.equals(ssapApplication.getEligibilityStatus())) {
						logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_DENIED, ssapApplication);
					} else if(EligibilityStatus.CAE.equals(ssapApplication.getEligibilityStatus())){
						logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_CONDITIONAL, ssapApplication);
					}
				}

				LOGGER.info("Invoking SEP callback if applicable, for SSAP id: {}", ssapApplication.getId());
				invokeSEPCallbackIfApplicable(ssapApplication, applicationEvent, ssapSepIntegrationDto);
			}
    	else {
    		LOGGER.info("Using OLD eligibility decision logic");
				if (elgibilityDeterminationRequired(ssapApplication, applicationEvent)) {

					SsapApplicationUpdateDTO ssapApplicationUpdate = nonFinancialApplicationEligibilityService.determineEligibility(ssapApplication);
					List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplication.getId());
					boolean allApplicantsVerified = nonFinancialApplicationEligibilityService.isVerificationDone(ssapApplicants, ssapJSON);

					if (allApplicantsVerified) {
						LOGGER.debug("NonFinancial application status application id: " + applicationId + " all verifications are done.");
						ssapApplicationUpdate.setEligibilityReceivedDate(new TSDate());
						ssapApplicationUpdate.setEligibilityStatus(EligibilityStatus.AE);

						logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_FULL, ssapApplication);

					} else {
						LOGGER.debug("NonFinancial application status application id: {} is still pending verifications.", applicationId);
						if (EligibilityStatus.DE.equals(ssapApplicationUpdate.getEligibilityStatus())) {
							logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_DENIED, ssapApplication);
						} else if (EligibilityStatus.CAE.equals(ssapApplicationUpdate.getEligibilityStatus())) {
							logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_CONDITIONAL, ssapApplication);
						}

					}
					ssapApplication = ssapApplicationUpdateHandler.updateSsapApplicationData(ssapApplicationUpdate, ssapApplication.getId());

					// Call Eligibility notification service to send notifications.
					if (!applicationStatusEquals(ssapApplication, RenewalStatus.DEPENDENT_AGE_CHECK_DONE) && !(ssapApplication.getDentalRenewalStatus() != null && ssapApplication.getDentalRenewalStatus().equals(RenewalStatus.DEPENDENT_AGE_CHECK_DONE))) {
						nonFinancialApplicationEligibilityService.sendInitialEligibilityNotification(ssapApplication.getCaseNumber());

						if (EligibilityStatus.CAE == ssapApplication.getEligibilityStatus()) {
							try {
								nonFinancialApplicationEligibilityService.sendAdditionalInfoEligibilityNotification(ssapApplication.getCaseNumber());
							} catch (Exception ex) {
								LOGGER.error("Error sending additional info notice for casenumber : " + ssapApplication.getCaseNumber(), ex);
							}
						}

					}

					invokeSEPCallbackIfApplicable(ssapApplication, applicationEvent, ssapSepIntegrationDto);
				}
				else{
					if (EligibilityStatus.CAE == ssapApplication.getEligibilityStatus()) {
						try {
							nonFinancialApplicationEligibilityService.sendAdditionalInfoEligibilityNotification(ssapApplication.getCaseNumber());
						} catch (Exception ex) {
							LOGGER.error("Error sending additional info notice for casenumber : " + ssapApplication.getCaseNumber(), ex);
						}
					}
				}
			}
    }

	private boolean elgibilityDeterminationRequired(SsapApplication ssapApplication, SsapApplicationEvent applicationEvent) { 
		SsapApplicationEventTypeEnum eventType = applicationEvent.getEventType();
		if(NON_FINANCIAL_ASSISTANCE.equals(ssapApplication.getFinancialAssistanceFlag()) && EligibilityStatus.PE.equals(ssapApplication.getEligibilityStatus())) {
			if(SsapApplicationEventTypeEnum.SEP.equals(eventType)) {
				return checkIfAnyApplicantEventRequiresRerunEligibility(ssapApplication, applicationEvent);
			}
			return true;
		}
		return false;
	}

	private boolean checkIfAnyApplicantEventRequiresRerunEligibility(SsapApplication ssapApplication, SsapApplicationEvent applicationEvent) {
		List<SsapApplicantEvent> applicantEvents = ssapApplicantEventRepository.findByApplicationEventId(applicationEvent.getId());
		for (SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
			if("Y".equals(ssapApplicantEvent.getSepEvents().getEligibilityRerunRequired())) {
				return true;
			}
		}
		return false;
	}

	private void invokeSEPCallbackIfApplicable(SsapApplication ssapApplication, SsapApplicationEvent applicationEvent, Object ssapSepIntegrationDto) {
		if(SsapApplicationEventTypeEnum.SEP.equals(applicationEvent.getEventType())) {
	        try {

				if (ssapSepIntegrationDto instanceof String &&
						!ssapSepIntegrationDto.toString().equalsIgnoreCase("null")) {

					SsapSEPIntegrationDTO ssapSEPIntegrationDTOJson = platformGson.fromJson(ssapSepIntegrationDto.toString(), SsapSEPIntegrationDTO.class);
	            	String response = restTemplate.postForObject(GhixEndPoints.SsapEndPoints.LCE_PROCESSOR_ENDPOINT, ssapSEPIntegrationDTOJson, String.class);

	            	LOGGER.info("SEP Callback Reponse: {}", response);
				} else {
					LOGGER.warn("invokeSEPCallbackIfApplicable::ssapSepIntegrationDto was null");
				}
	        } catch(JsonSyntaxException e){
	        	LOGGER.error("ssapSepIntegrationDto JSON Syntax Exception");
			}
	        catch (Exception exception) {
	            LOGGER.error("Error invoking notification service for sending eligibility notifications", exception);
	        }
		} 
		
		if(applicationStatusEquals(ssapApplication, RenewalStatus.DEPENDENT_AGE_CHECK_DONE) || (ssapApplication.getDentalRenewalStatus()!=null && ssapApplication.getDentalRenewalStatus().equals(RenewalStatus.DEPENDENT_AGE_CHECK_DONE))) {
			if(applicationStatusEquals(ssapApplication, RenewalStatus.DEPENDENT_AGE_CHECK_DONE)){
				ssapApplication.setRenewalStatus(RenewalStatus.TO_CONSIDER);
			}
			if(ssapApplication.getDentalRenewalStatus()!=null && ssapApplication.getDentalRenewalStatus().equals(RenewalStatus.DEPENDENT_AGE_CHECK_DONE)){
				ssapApplication.setDentalRenewalStatus(RenewalStatus.TO_CONSIDER);
			}
			ssapApplicationRepository.save(ssapApplication);
		}
		//for OE/QEP call the ssapIntegationCall to trigger the QLE validations.
		if(SsapApplicationEventTypeEnum.QEP.equals(applicationEvent.getEventType())) {
	        try {
	            restTemplate.postForObject(GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_INTEGRATION_CALLBACK,ssapApplication.getCaseNumber(),String.class);
	        }catch (Exception exception) {
	            LOGGER.error("Error invoking ssap callback for QEP service: "+ssapApplication.getCaseNumber(), exception);
	        }
		} 
		
	}
	
	private void logIndividualAppEvent(String eventName, String eventType,SsapApplication ssapApplication){
	       
		try {

			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			EventInfoDto eventInfoDto = new EventInfoDto();
			
			Map<String, String> mapEventParam = new HashMap<>();
			mapEventParam.put(APPLICATION_ID,String.valueOf(ssapApplication.getId()));
			mapEventParam.put(APPLICATION_TYPE,NON_FINANCIAL_APPLICATION);
			
			if(null !=ssapApplication.getCmrHouseoldId()){
				eventInfoDto.setModuleId(ssapApplication.getCmrHouseoldId().intValue());	
			}
			
			eventInfoDto.setModuleName("INDIVIDUAL");
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);
			
		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event"+e.getMessage(), e);
			//throw new GIRuntimeException(e);
		}
	}	
	
	private boolean applicationStatusEquals(SsapApplication currentApplication, RenewalStatus renewalStatus) {			
		return currentApplication != null && currentApplication.getRenewalStatus() != null && currentApplication.getRenewalStatus().equals(renewalStatus);
	}		
}
