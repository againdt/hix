import React, {useEffect} from 'react';

function DocumentTitle ({title}) {

    useEffect(() => {
        document.title = title;
    }, [title]);

    return (
        <></>
    );
}

export default DocumentTitle;
