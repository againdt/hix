<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>

<%@ page import="java.util.HashMap" %>

<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1>
			<spring:message code="label.referral.pageheader" />
		</h1>
	</div>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4>
					<spring:message code="label.referral.sideheader" />
				</h4>
			</div>
			<ul class="nav nav-list">
				<li><spring:message code="label.dynamicSecurityQuestion.securityQuestions" />
				</li>
				<li class="active"><spring:message
						code="label.referral.linktoapplication" /></li>
			</ul>
		</div>
		<div id="rightpanel" class="span9 dashboard-rightpanel">
			<div class="header">
				<h4>Link to Application</h4>
			</div>
			<div class="gutter10 center-text">
				<p>This application has been successfully linked to your account.</p>



				<c:choose>
					<c:when test="${currentRoleName != 'INDIVIDUAL' }">
					<c:if test="${not empty firstName}" >
						<button aria-hidden="true" id="crossCloseComplete"
							class="btn btn-primary" type="button" href="#markCompleteDialog"
							data-toggle="modal">Continue</button>
							</c:if>
					</c:when>
					<c:otherwise>
						<p>
							<a href="<c:url value='/indportal' />" class="btn btn-primary">Continue</a>
						</p>
					</c:otherwise>
				</c:choose>

			</div>
		</div>

	</div>
</div>

<form name="dialogForm" id="dialogForm" method="POST"
	action="<c:url value="/account/user/switchUserRole" />"
	novalidate="novalidate">
	<df:csrfToken />
	<div id="markCompleteDialog" class="modal hide fade" tabindex="-1"
		role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
		<div class="markCompleteHeader">
			<div class="header">
				<h4 class="margin0 pull-left">View Member Account</h4>
				<button aria-hidden="true" data-dismiss="modal" id="crossClose"
					class="dialogClose" title="x" type="button">x</button>
			</div>
		</div>

		<div class="modal-body clearfix gutter0-tb">
			<div class="control-group">
				<div class="controls">
					Clicking "Member View" will take you to the Member's portal for
					${firstName} ${lastName}.<br /> Through this portal you will be
					able to take actions on behalf of the member.<br /> Proceed to
					Member view?
				</div>
			</div>
		</div>
		
		<jsp:useBean id="moduleParamsMap" class="java.util.HashMap"/>
		<c:set target="${moduleParamsMap}" property="switchToModuleName" value="individual" />
		<c:set target="${moduleParamsMap}" property="switchToModuleId" value="${householdId}" />
		<c:set target="${moduleParamsMap}" property="switchToResourceName" value="${firstName} ${lastName}" />
		<surl:secureUrl paramMap="${moduleParamsMap}" var="ecyptedModuleParamsMap"/>
		
		
		<div class="modal-footer clearfix">
			<input class="pull-left" type="checkbox" id="checkConsumerView" name="checkConsumerView">
			<div class="pull-left">&nbsp; Don't show this message again.</div>
			<button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
			<button class="btn btn-primary" type="submit">Member View</button>
			<input type="hidden" name="ref" id="ref" value="${ecyptedModuleParamsMap}" />
		</div>
	</div>
</form>



