import React, { Fragment } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';


const MultiTaxHouseholdModal = props => {

    const {
        modal: {
            data: [
                filers,
                primaryTaxFilerPersonId
            ],
            callbacks,
            callbacks: [ , hideModal ]
        },
    } = props;

    const handleHideClick = () => {
        hideModal();
    };

    const handleSave = () => {
        callbacks.forEach(cb => cb());
    };

    const primaryTaxFilerName = (() => {
        return (ptf => ptf.filerFullName)(filers.find(f => f.filerPersonId === primaryTaxFilerPersonId));
    })();

    const getNonPrimaryTaxFilers = () => {
        return filers
            .filter(f => f.filerPersonId !== primaryTaxFilerPersonId && f.isFilingTaxes)
            .map(f => {

                const {
                    filerFullName,
                    filerPersonId
                } = f;

                return (
                    <p key={filerPersonId}>
                        - The household members on &nbsp;
                        <strong>{ filerFullName }</strong>
                        &apos;s Tax return will not be considered for healthcare coverage on this application.
                        If they want to be considered for healthcare coverage, then they should submit a separate
                        application.
                    </p>
                );
            });
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal__pre_eligibility">
                Information about your Household
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>

            <div id="rsPreEligibilityModal" className="usa-grid">
                <div className="modal__body">
                    <p>
                        Only the household members who are on &nbsp;
                        <strong>{ primaryTaxFilerName }</strong>
                        &apos; s Tax return will be considered for subsidy determination.
                    </p>
                    {
                        getNonPrimaryTaxFilers()
                    }
                </div>
            </div>
            <div className="modal__footer">
                <div className='usa-grid controls'>
                    <div className='usa-offset-one-third txt-left'>
                        <Button
                            className='modal-cancel-btn usa-button-secondary'
                            onClick={handleHideClick}
                        >
                            Cancel
                        </Button>
                        <Button
                            className='modal-confirm-btn'
                            onClick={handleSave}
                        >
                            Save
                        </Button>
                    </div>
                </div>
            </div>

        </Fragment>
    );
};

export default MultiTaxHouseholdModal;