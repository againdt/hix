import { messagesActionTypes } from '../constants/constants';

const initialState = null;

export default (state = initialState, action) => {
    switch (action.type) {
        case (messagesActionTypes.SET_MESSAGES):
            return action.messages;
        case (messagesActionTypes.CLEAR_STATE):
            return initialState;
        default:
            return state;
    }
};