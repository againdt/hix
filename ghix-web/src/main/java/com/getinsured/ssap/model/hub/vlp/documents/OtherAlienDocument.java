package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.Date;
import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * Other Alien Document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class OtherAlienDocument extends DhsDocument
{
  public OtherAlienDocument()
  {
    super.setDocumentType(DocumentType.OTHER_ALIEN);
  }

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonProperty("PassportCountry")
  private String passportCountry;

  @JsonProperty("PassportNumber")
  private String passportNumber;

  @JsonProperty("CountryOfIssuance")
  private String countryOfIssuance;

  @JsonProperty("SEVISID")
  private String sevisId;

  @JsonProperty("DocDescReq")
  private String docDescReq;

  @JsonFormat(pattern = DATE_FORMAT)
  @JsonProperty("DocExpirationDate")
  private Date docExpirationDate;

  @Override
  public boolean isValid()
  {
    if(!validPassportInformation(passportCountry, passportNumber, countryOfIssuance)) {
      log.error("When passportCountry supplied: {}, passport number: {} and county of issuance: {} are required",
          this.passportCountry, this.passportNumber, this.countryOfIssuance);
      return false;
    }

    if(!notBlank(this.alienNumber) || !notBlank(this.docDescReq))
    {
      log.error("alienNumber: {} and docDescReq: {} are required", this.alienNumber, this.docDescReq);
      return false;
    }

    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final OtherAlienDocument document = new OtherAlienDocument();

    document.setAlienNumber(validValue(citizenshipDocument.getAlienNumber()));
    document.setPassportNumber(validValue(citizenshipDocument.getForeignPassportOrDocumentNumber()));
    document.setCountryOfIssuance(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setPassportCountry(validValue(citizenshipDocument.getForeignPassportCountryOfIssuance()));
    document.setSevisId(getSevisId(citizenshipDocument));
    document.setDocDescReq(validValue(citizenshipDocument.getDocumentDescription()));
    document.setDocExpirationDate(citizenshipDocument.getDocumentExpirationDate());

    return document;
  }

  public String getAlienNumber()
  {
    return alienNumber;
  }

  public void setAlienNumber(final String alienNumber)
  {
    this.alienNumber = alienNumber;
  }

  public String getPassportCountry()
  {
    return passportCountry;
  }

  public void setPassportCountry(final String passportCountry)
  {
    this.passportCountry = passportCountry;
  }

  public String getPassportNumber()
  {
    return passportNumber;
  }

  public void setPassportNumber(final String passportNumber)
  {
    this.passportNumber = passportNumber;
  }

  public String getCountryOfIssuance()
  {
    return countryOfIssuance;
  }

  public void setCountryOfIssuance(final String countryOfIssuance)
  {
    this.countryOfIssuance = countryOfIssuance;
  }

  public String getSevisId()
  {
    return sevisId;
  }

  public void setSevisId(final String sevisId)
  {
    this.sevisId = sevisId;
  }

  public String getDocDescReq()
  {
    return docDescReq;
  }

  public void setDocDescReq(final String docDescReq)
  {
    this.docDescReq = docDescReq;
  }

  public Date getDocExpirationDate()
  {
    return docExpirationDate;
  }

  public void setDocExpirationDate(final Date docExpirationDate)
  {
    this.docExpirationDate = docExpirationDate;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", OtherAlienDocument.class.getSimpleName() + "[", "]")
        .add("alienNumber='" + alienNumber + "'")
        .add("passportCountry='" + passportCountry + "'")
        .add("passportNumber='" + passportNumber + "'")
        .add("countryOfIssuance='" + countryOfIssuance + "'")
        .add("sevisId='" + sevisId + "'")
        .add("docDescReq='" + docDescReq + "'")
        .add("docExpirationDate=" + docExpirationDate)
        .toString();
  }
}
