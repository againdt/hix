<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page import="java.util.List, com.getinsured.hix.model.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>


<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}
#container-wrap::after {top: 0px !important}
.sidebar-nav {
	padding: 9px 0;
}
#menu {
	display: none;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}

.no-health-plan {
	background: #f9f7c1;
	border: 1px dashed #ccc;
	padding: 36px 20px 30px;
}

.no-dental-plan {
	border: 1px dashed #ccc;
	padding: 36px 20px 30px;
}

.cart {
	border: 1px solid #c9c9c9;
}

h3 {
	line-height: 25px;
	margin: 0
}

/* .darkblue {
	background: #0c9bd2;
	color: #fff;
} */

.darkgreen {
	background: #008080;
	color: #fff;
}

.SILVER {
	background: url(../resources/img/silver-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.GOLD {
	background: url(../resources/img/gold-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.BRONZE {
	background: url(../resources/img/bronze-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.CATASTROPHIC {
	background: url(../resources/img/catastrophic-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.PLATINUM {
	background: url(../resources/img/platinum-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.NOLABEL {
	height: 50px;
}
.planimg {
	margin-top: 20px;
	margin-left: 20px;
	padding-bottom: 10px;
}

.closeout {
	position: relative;
	top: 24px;
	right: 10px;
	margin: -19px 0 0 0;
}

.mini-cart {
	width: 380px;
	border: 4px solid #4f4f4f;
	background: #fff;
	position: absolute;
	z-index: 10;
}

.mini-cart h3 {
	color: #fff;
}

.mini-cart .header {
	background: #4f4f4f;
}
#container-wrap .container {
	margin-top:0px!important;
}
.view-broker {
	top: 8px!important;
	z-index: 10;
}

</style>

<c:set var="iexCustomGroupingConfig" value='<%=DynamicPropertiesUtil.getPropertyValue("iex.customGrouping")%>'></c:set>
<div class="gutter10">
	<c:if test="${errorMsg != ''}">
		<div class="row-fluid">
			<div style="font-size: 14px; color: red">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</div>
		</div>
	</c:if>

<c:if test="${errorMsg == ''}">
	<div class="row-fluid" id="titlebar">
		<h1><spring:message code='enroll.orderConfirm.confirm.text' /></h1>
	</div>

	<div class="row-fluid">
			<div class="span12" id="rightpanel">
					<c:choose>
						<c:when test="${not empty enrollmentRenewalFlg && enrollmentRenewalFlg == enrollment_manual_renewal}">
							<p id="manualOrderconfirmText"><spring:message code='enroll.orderConfirm.confirm.manual.value'/></p>
						</c:when>
						<c:when test="${enrollment_type == enrollment_type_individual}">
							<p id="individualOrderconfirmText"><spring:message code='enroll.orderConfirm.confirm.individual.value'/></p>
						</c:when>
						<c:when test="${enrollment_type == enrollment_type_medical}">
							<p id="medicalOrderconfirmText"><spring:message code='medical.health.orderconfirm'/></p>
						</c:when>
					</c:choose>
						<form name="frmorderconfirm" id="frmorderconfirm" action="orderconfirmsubmit" method="post">
							<df:csrfToken/>
							<input type="hidden" name="cart_id" id="cart_id" value="${cartId}">
							<input type="hidden" name="redirectLink" id="redirectLink" value="${redirectLink}">
							<c:set var="isDentalPlan" value="false" />


							<c:if test="${fn:length(enrollmentMap) gt 0}">
								<c:forEach var="enrollmentMapEntry" items="${enrollmentMap}">
									<c:set var="enrollmentlist" value="${enrollmentMapEntry.value}"></c:set>
									<c:if test="${fn:length(enrollmentlist) gt 0}">
										<div class="cart">
											<div class="header">
												<h4> <spring:message code='enroll.orderConfirm.${enrollmentMapEntry.key}' /></h4>
											</div>
											<c:forEach items="${enrollmentlist}" var="enrollmentObj">
												<fmt:formatDate pattern="MM/dd/yyyy" value="${enrollmentObj.benefitEffectiveDate}" var="effDate" />
												<fmt:formatDate pattern="MM/dd/yyyy" value="${enrollmentObj.financialEffectiveDate}" var="finEffDate" />
												<div class="gutter10">
													<div class="health-plan">
														<c:choose>
																<c:when test="${enrollmentMapEntry.key == 'HEALTH'}">
																	<input type="hidden" name="healthPlanInsurer" id="healthPlanInsurer" value="${enrollmentObj.insurerName}" />
																	<input type="hidden" name="healthPlanName" id="healthPlanName" value="${enrollmentObj.planName}" />
																	<input type="hidden" name="healthPlanLevel" id="healthPlanLevel" value="${enrollmentObj.planLevel}" />
																	<input type="hidden" name="healthPlanPresent" id="healthPlanPresent" value="true"/>
																</c:when>
																<c:when test="${enrollmentMapEntry.key == 'DENTAL'}">
																	<input type="hidden" name="dentalPlanInsurer" id="dentalPlanInsurer" value="${enrollmentObj.insurerName}" />
																	<input type="hidden" name="dentalPlanName" id="dentalPlanName" value="${enrollmentObj.planName}" />
																	<input type="hidden" name="dentalPlanLevel" id="dentalPlanLevel" value="${enrollmentObj.planLevel}" />
																	<input type="hidden" name="dentalPlanPresent" id="dentalPlanPresent" value="true"/>
																</c:when>
																<c:otherwise>
																	<input type="hidden" name="healthPlanInsurer" id="healthPlanInsurer" value=""/>
																	<input type="hidden" name="healthPlanName" id="healthPlanName" value=""/>
																	<input type="hidden" name="healthPlanLevel" id="healthPlanLevel" value=""/>

																	<input type="hidden" name="dentalPlanInsurer" id="dentalPlanInsurer" value=""/>
																	<input type="hidden" name="dentalPlanName" id="dentalPlanName" value=""/>
																	<input type="hidden" name="dentalPlanLevel" id="dentalPlanLevel" value=""/>

																	<input type="hidden" name="healthPlanPresent" id="healthPlanPresent" value="false"/>
																	<input type="hidden" name="dentalPlanPresent" id="dentalPlanPresent" value="false"/>
																</c:otherwise>
															</c:choose>
																<c:choose>
																	<c:when test="${enrollment_type == enrollment_type_individual}">
																		<c:if test="${fn:containsIgnoreCase(enrollmentObj.insuranceTypeLkp.lookupValueLabel,'DENTAL')}">
																			<c:set var="isDentalPlan" value="true" />
																		</c:if>
																		<p>
																		    <strong>${enrolleeNameMap[enrollmentObj.id]}</strong>
																		    <c:choose>
																			    <c:when test="${enrollmentObj.showFinancialEffectivedate == true}">
																			    	<span class="pull-right"><spring:message code='enroll.orderConfirm.effDate' /> &nbsp;<strong>${finEffDate}</strong></span>
																			    </c:when>
																			    <c:otherwise>
																			    	<span class="pull-right"><spring:message code='enroll.orderConfirm.effDate' /> &nbsp;<strong>${effDate}</strong></span>
																			    </c:otherwise>
																		    </c:choose>
																		</p>
																	</c:when>
																	<c:when test="${enrollment_type == enrollment_type_medical}">

																		<c:if test="${fn:containsIgnoreCase(enrollmentObj.insuranceTypeLkp.lookupValueLabel,'DENTAL')}">
																			<c:set var="isDentalPlan" value="true" />
																		</c:if>

																		<p>
																		    <strong>${enrolleeNameMap[enrollmentObj.id]}</strong>
																		     <c:choose>
																			    <c:when test="${enrollmentObj.showFinancialEffectivedate == true}">
																			    	<span class="pull-right"><spring:message code='medical.orderconfirm.effDate' />: &nbsp;<strong>${finEffDate}</strong></span>
																			    </c:when>
																			    <c:otherwise>
																			    	<span class="pull-right"><spring:message code='medical.orderconfirm.effDate' />: &nbsp;<strong>${effDate}</strong></span>
																			    </c:otherwise>
																		    </c:choose>
																		</p>
																	</c:when>
																</c:choose>
															<%-- 	<c:if test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
																	<div class="pull-right closeout">
																		<div class="remove pull-left">
																			<spring:message code='enroll.orderConfirm.remove' />
																		</div>
																		<button type="button" title="x" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

																	</div>
																</c:if> --%>
															<table class="table table-border" >
																	<tbody>
																		<tr>
																			<c:choose>
																					<c:when test="${enrollmentObj.planLevel == 'GOLD' || enrollmentObj.planLevel == 'SILVER' || enrollmentObj.planLevel == 'BRONZE' || enrollmentObj.planLevel == 'PLATINUM' || enrollmentObj.planLevel == 'CATASTROPHIC'}">
																						<td class="padding0 ${enrollmentObj.planLevel}">
																							<div class="planimg margin30-l">
																								<img class="issuer-logo carrierlogo hide " src='<c:url value="/admin/issuer/company/profile/logo/view/${enrollmentObj.issuerId}"/>' alt='${enrollmentObj.insurerName}' onload="resizeimage()" />
																								<br>
																							</div>
																						</td>
																					</c:when>
																					<c:when test="${enrollmentObj.planLevel == 'HIGH' || enrollmentObj.planLevel == 'LOW'}">
																						<td class="padding0">
																							<div class="planimg margin30-l">
																								<img class="issuer-logo carrierlogo hide" src='<c:url value="/admin/issuer/company/profile/logo/view/${enrollmentObj.issuerId}"/>' alt='${enrollmentObj.insurerName}' onload="resizeimage()" />
																								<br>
																							</div>
																						</td>
																					</c:when>
																			</c:choose>
																			<td class="span3">
																			<strong>${enrollmentObj.insurerName}</strong><br>${enrollmentObj.planName}
																			</td>
																			<td class="txt-right span3">
																			    <spring:message code='enroll.orderConfirm.mnthlyPremium' /><br>
																			</td>
																			<td class="txt-right span2">
																				<c:choose>
																					<c:when test="${enrollment_type == enrollment_type_individual}">
																						<strong>$ <fmt:formatNumber pattern="#,##0.00" value="${enrollmentObj.grossPremiumAmt}" minFractionDigits="2" maxFractionDigits="2"/></strong>
																					</c:when>
																					<c:when test="${enrollment_type == enrollment_type_medical}">
																						<strong>$ <fmt:formatNumber pattern="#,##0.00" value="${enrollmentObj.netPremiumAmt}" minFractionDigits="2" maxFractionDigits="2"/></strong>
																					</c:when>
																				</c:choose>
																			</td>
																		</tr>
																<%-- 		<c:if test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
																			<tr class="individual-subtotal">
																				<td>&nbsp;</td>
																				<td>&nbsp;</td>

																				<td>
																					<c:choose>
																							<c:when
																								test="${enrollment_type == enrollment_type_individual}">
																								<spring:message code='enroll.orderConfirm.payment' />
																							</c:when>
																					 </c:choose>
																				</td>

																				<td colspan="2" class="txt-right">
																					$<fmt:formatNumber pattern="#,##0.00" value="${enrollmentObj.netPremiumAmt}" minFractionDigits="2"  maxFractionDigits="2"/>
																				</td>
																			</tr>
																	   </c:if> --%>
																	   <c:if test="${fn:length(enrollmentMap) gt 0 || fn:length(enrollmentlist) gt 0}">
																			<tr>
																				<td class="txt-right" colspan="5">
																						<c:choose>
																					<c:when test="${enrollment_type == enrollment_type_individual && enrollmentMapEntry.key != 'DENTAL'}">
																					    <c:if test="${ShowHealthPaymentButton == 'true'}">
																						    <c:set var="isPayButton" value="true"/>
																							<a id ="healthPaymentBtn" class="btn btn-primary jsPaymentBtn pay_health_google_tracking" href="<c:url value="/finance/paymentRedirect?finEnrlDataKey=finEnrlHltDataKey"/>" onclick="payHealthExternalDlEvent()"><spring:message code='enroll.orderConfirm.health.makePayment'/></a>
																						</c:if>
																						<c:if test="${finEnrlHltDataKey == null || finEnrlHltDataKey == ''}">
																							<p>
																	                         <span class="pull-left"><spring:message code='	enroll.nm.payment.noButton.message.value'/></span>
																	                        </p>
																						 </c:if>
																						 <c:if test="${ShowHealthPaymentButton == 'false'}">
																						      <div class="alert alert-info hide-for-ms">
												                                                  <spring:message code='enroll.nm.payment.offline.message_3.value'/>
											                                                  </div>
										                                                 </c:if>
																					 </c:when>
		
																					<c:when test="${enrollment_type == enrollment_type_individual && enrollmentMapEntry.key == 'DENTAL'}">
																					     	<c:if test="${ShowDentalPaymentButton == 'true'}">
																						     <c:set var="isPayButton" value="true"/>
																							     <a id = "dentalPaymentBtn" class="btn btn-primary jsPaymentBtn pay_dental_google_tracking" href="<c:url value="/finance/paymentRedirect?finEnrlDataKey=finEnrlDntDataKey"/>" onclick="payHealthExternalDlEvent()"><spring:message code='enroll.orderConfirm.dental.makePayment'/></a>
																	                        </c:if>
																	                        <c:if test="${finEnrlDntDataKey == null || finEnrlDntDataKey == ''}">
																							  <p>
																	                          <span class="pull-left"><spring:message code='enroll.nm.payment.noButton.message.value'/></span>
																	                          </p>
																							</c:if>
																							<c:if test="${ShowDentalPaymentButton == 'false'}">
																						      <div class="alert alert-info hide-for-ms">
												                                                  <spring:message code='enroll.nm.payment.offline.message_3.value'/>
											                                                  </div>
										                                                 </c:if>
		
																					</c:when>
																				</c:choose>
																				</td>
																			</tr>
																		</c:if>
																	</tbody>
																</table>
													</div>
												</div>
											</c:forEach>
										</div>
									</c:if>
								</c:forEach>
								<div class="cart gutter10">
								<c:choose>

										<%--Totals Table for Individual  --%>
										<c:when test="${enrollment_type == enrollment_type_individual}">
											<table class="table table-border-none table-condensed totalTable">
												<tr class="individual-subtotal">
											<%-- 	<c:choose>
												<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
													<td>
														<spring:message code="enroll.orderConfirm.allPlans" />
													</td>
												</c:when>
												<c:otherwise>
													<td>&nbsp;</td>
												</c:otherwise>
												</c:choose> --%>
												<td class="span2">&nbsp;</td>
												<td class="txt-right">
													<c:choose>
														<c:when test="${not empty enrollmentRenewalFlg && enrollmentRenewalFlg == enrollment_manual_renewal}">
															<spring:message code='enroll.orderConfirm.totalMonthlyPremium.manual' />
														</c:when>
														<c:otherwise>
															<spring:message code='enroll.orderConfirm.totalMonthlyPremium.text' />
														</c:otherwise>
													</c:choose>
												</td>
												<td class="txt-right">
													<span class="margin15-r">	$
														<fmt:formatNumber pattern="#,##0.00" value="${totalMonthlyPremium}"
															minFractionDigits="2"  maxFractionDigits="2"/>
													</span>
												</td>
											</tr>
											<c:if test="${totalAptcContribution gt 0}">
												<tr class="individual-subtotal">
													<td class="span2">&nbsp;</td>
													<td class="txt-right">
														<c:choose>
															<c:when test="${not empty enrollmentRenewalFlg && enrollmentRenewalFlg == enrollment_manual_renewal}">
																<spring:message code='enroll.orderConfirm.mnthlyTaxCredit.manual' />
															</c:when>
															<c:otherwise>
																<spring:message code='enroll.orderConfirm.mnthlyTaxCredit.text' />
															</c:otherwise>
														</c:choose>
													</td>
													<td class="txt-right">
															<span class="margin15-r">
															-$
															<fmt:formatNumber pattern="#,##0.00" value="${totalAptcContribution}"
																minFractionDigits="2"  maxFractionDigits="2"/>
															</span>

													</td>
												</tr>
											</c:if>

                                                <c:if test="${totalStateSubsidyContribution gt 0}">
                                                    <tr class="individual-subtotal">
                                                        <td class="span2">&nbsp;</td>
                                                        <td class="txt-right">
                                                            <c:choose>
                                                                <c:when test="${not empty enrollmentRenewalFlg && enrollmentRenewalFlg == enrollment_manual_renewal}">
                                                                    <spring:message code='enroll.orderConfirm.mnthlyStateSubsidy.manual' />
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <spring:message code='enroll.orderConfirm.mnthlyStateSubsidy' />
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                        <td class="txt-right">
															<span class="margin15-r">
															-$
															<fmt:formatNumber pattern="#,##0.00" value="${totalStateSubsidyContribution}"
                                                                              minFractionDigits="2"  maxFractionDigits="2"/>
															</span>

                                                        </td>
                                                    </tr>
                                                </c:if>
											<tr class="individual-cart-total">
											<%-- <c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td class="txt-right">
														<spring:message code="enroll.orderConfirm.cartTotal" />
												</td>
											</c:when>
											<c:otherwise>
												<td>&nbsp;</td>
											</c:otherwise>
											</c:choose> --%>
											<td class="cart-total-heading">&nbsp;</td>
												<td class="cart-total-heading txt-right"><spring:message code='enroll.orderConfirm.totalPayment' /></td>
												<td class="cart-total-heading txt-right">
													<strong>
														$
														<fmt:formatNumber pattern="#,##0.00" value="${totalCost}"
															minFractionDigits="2"  maxFractionDigits="2"/>
													</strong>
												</td>
											</tr>
										</table>
									</c:when>

									<%--Totals Table for Medical  --%>
									<c:when test="${enrollment_type == enrollment_type_medical}">
										<table class="table table-border-none table-condensed totalTable">
											<tr class="individual-cart-total">
												<td>&nbsp;</td>
												<td class="txt-right"><spring:message code='enroll.orderConfirm.totalPayment' /></td>
												<td class="txt-right">
													<strong>$<fmt:formatNumber pattern="#,##0.00" value="${totalCost}" minFractionDigits="2"  maxFractionDigits="2"/></strong>
												</td>
											</tr>
										</table>
									</c:when>
								</c:choose>
								</div>
							</c:if>
							<br>
							<c:if test="${showChngPlans==true}">
							<h4 class="graydrkbg gutter10 marginTop20">
								<spring:message code='enroll.orderConfirm.mngPlanChng' />
							</h4>
							<div class="gutter10">
								<c:choose>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<p><spring:message code='enroll.orderConfirm.individual.mngPlanChng.value' /></p>
									</c:when>
									<c:when test="${enrollment_type == enrollment_type_medical && isDentalPlan==false }">
										<p><spring:message code='medical.orderconfirm.health.mngPlanChng.value' /></p>
									</c:when>
									<c:when test="${enrollment_type == enrollment_type_medical && isDentalPlan==true }">
										<p><spring:message code='medical.orderconfirm.dental.mngPlanChng.value' /></p>
									</c:when>
								</c:choose>
							</div>
							</c:if>

							<c:if test="${fn:length(disEnrollmentMap) gt 0}">
								<h4 class="graydrkbg gutter10 marginTop20">
									<spring:message code='enroll.orderConfirm.disenrollmentsFromPlanTitle' />
								</h4>
								<div class="gutter10">
									<p>
										<spring:message code='enroll.orderConfirm.disenrollmentsFromPlan' />
									</p>

									<p>
									<c:forEach var="disEnrollment" items="${disEnrollmentMap}">
  										&nbsp<b><c:out value="${disEnrollment.key}"/>: &nbsp <c:out value="${disEnrollment.value}"/></b><br>
									</c:forEach>
									</p>
								</div>
							</c:if>

							<c:if test="${showDisclaimers == true}">
								<h4 class="graydrkbg gutter10 marginTop20">
									<spring:message code='enroll.orderConfirm.disclaimer' />
								</h4>
								<div class="gutter10">
									<p>
										<spring:message code='enroll.orderConfirm.individual.disclaimer.value' />
									</p>
								</div>
							</c:if>
							<div class="form-actions margin-top50">
								<div class="controls pull-right">
									<%-- <button type="button" class="btn btn-primary gtm_shopmore_members" title="<spring:message code='enroll.orderConfirm.shopForMoreMembers' />">
										 <spring:message code='enroll.orderConfirm.shopForMoreMembers' /> </button> --%>
									<c:if test="${iexCustomGroupingConfig == 'ON'}">
										<button type="button" class="btn btn-primary gtm_shopmore_members" id="aid_shopMoreMembers" title="<spring:message code='enroll.orderConfirm.shopForMoreMembers' text="Shop For More Members"/>">
											<spring:message code='enroll.orderConfirm.shopForMoreMembers' text="Shop For More Members" />
										</button>
									</c:if>
									<button type="button" class="btn print_google_tracking" id="aid_print" onclick="window.print()" title="<spring:message code='enroll.orderConfirm.print' />"> <i class="icon-print"></i>
										 <spring:message code='enroll.orderConfirm.print' />
									</button>
								    <button type="button"
										onclick="gotoparent()" class="btn btn-primary gtm_orderconfirm_continue" id="aid_dashboard" title="<spring:message code='go.to.dashboard' />"><spring:message code='go.to.dashboard' />
									</button>
								</div>
							</div>
							<c:if test="${isPayButton == 'true'}">
							  <div class="alert">
							    <spring:message code='enroll.nm.payment.offline.message_1.value'/> <spring:message code='enroll.nm.payment.offline.message_2.value'/>
							  	<p class="txt-right">
							   		<c:url value="/indportal" var="exitPayOffLineURL"/>
						        	<a href='${exitPayOffLineURL}' class="btn btn-primary"><spring:message code='enroll.orderConfirm.exit.pay.offline'/></a>
							  	</p>
							  </div>
							</c:if>
						</form>
			</div>
		</div>
		<%--<c:if test="${isDentalPlan==true}">
					<script>
						$("#medicalOrderconfirmText").html("<spring:message code='medical.dental.orderconfirm' htmlEscape='false'/>");
						$("#individualOrderconfirmText").html("<spring:message code='enroll.orderConfirm.confirm.individual.dental.value' htmlEscape='false'/>");
						$("#manualOrderconfirmText").html("<spring:message code='enroll.orderConfirm.confirm.manual.dental.value' htmlEscape='false'/>");
					</script>
		</c:if> --%>
	</c:if>
</div>

<script type="text/javascript">

	$(document).ready(function() {

		title = document.getElementsByTagName("title")[0].innerHTML;

		document.title = title + ' - Order Confirmation Page';

		// Variables for dataLayer
		var healthPlanInfo = $("#healthPlanInsurer").val() + ' | ' + $("#healthPlanName").val() + ' | ' + $("#healthPlanLevel").val();
		var dentalPlanInfo = $("#dentalPlanInsurer").val() + ' | ' + $("#dentalPlanName").val() + ' | ' + $("#dentalPlanLevel").val();
		var healthPlanPresent = $("#healthPlanPresent").val();
		var dentalPlanPresent = $("#dentalPlanPresent").val();
		var planInfo = '';

		if(healthPlanPresent && dentalPlanPresent) {
			planInfo = healthPlanInfo + ' :: ' + dentalPlanInfo;
		}
		if(healthPlanPresent && !dentalPlanPresent) {
			planInfo = healthPlanInfo;
		}
		if(!healthPlanPresent && dentalPlanPresent) {
			planInfo = dentalPlanInfo;
		}

		// Data layer variable to be pushed at the end of shell.jsp
    	dl_pageCategory = 'Order Confirmation';

    	// Checkout Step 5
		dl_eventStack.push({
			'event': 'checkout',
			'eventCategory': 'Plan Selection - ecommerce',
			'eventAction': 'Confirmation Pageview',
			'eventLabel': planInfo,
			'ecommerce': {
				'checkout': {
					'actionField': {'step': 5}
				}
			}
		});

		$('.info').tooltip();

		$('.remove').hide();
		$('.close').hover(function() {
			$(this).prev('div').toggle();
		});

	});
	$('.gtm_shopmore_members').click(function(){
    	window.location = "/hix/indportal#/customGrouping";
    })

	function gotoparent() {

		window.location.href = "/hix/indportal";
	}


	function payHealthExternalDlEvent() {
		// Checkout Step 6
		window.dataLayer.push({
			'event': 'checkout',
			'eventCategory': 'CalHEERS - ecommerce',
			'eventAction': 'Pay Now Button Click',
			'eventLabel': 'Order Confirmation Page',
			'ecommerce': {
				'checkout': {
					'actionField': {'step': 6}
				}
			}
		});
	}

</script>
<!-- Code Starts for Payment Redirection-->
<script type="text/javascript">
$(function(){
  $('.jsPaymentBtn').on('click',function(e){
	e.preventDefault();

    me = $(this);

	//Make the button as disable state
	me.attr('disabled',true);
	me.unbind('click');

	var curURL = me.attr('href');

	//Open URL in new window
	window.open(curURL,"_blank", 'CarrierRedirection',"width:1000px,height:680px");

	//Reset href for disable state:
	me.attr('href','javascript:void(0)');

  });
});
//gtm_householdCaseID
function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {
		alert($('Session is invalid').text());
		rv = true;
	}
	return rv;
}
</script>
<!-- Code End for Payment Redirection-->

<script type="text/javascript">

function resizeimage() {
	$(".carrierlogo").each(function(){
        var width = $(this).width();
        var height = $(this).height();
        var maxWidth = 160;
		var maxHeight = 60;

		// Check if the current width is larger than the max
	    if(width > maxWidth){
	        ratio = maxWidth / width; // get ratio for scaling image
	        $(this).width(maxWidth); // Set new width
	        $(this).height((height * ratio)); // Scale height based on ratio
	        height = height * ratio; // Reset height to match scaled image
	    	width = width * ratio; // Reset width to match scaled image
	    }

		// Check if current height is larger than max
		if(height > maxHeight){
		  ratio = maxHeight / height; // get ratio for scaling image
		  $(this).height(maxHeight); // Set new height
		  $(this).width((width * ratio)); // Scale width based on ratio
		}

		if($(this).parents('td')){
    		$(this).parents('td').width(maxWidth);
    	}


		$(this).css('display','block');
    });
}
</script>




