package com.getinsured.ssap.model.hub.vlp.documents;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.ssap.model.hub.vlp.DocumentType;

/**
 * Naturalization Certificate document.
 *
 * @author Yevgen Golubenko
 * @since 2/15/19
 */
public class NaturalizationCertificateDocument extends DhsDocument
{
  public NaturalizationCertificateDocument() {
    super.setDocumentType(DocumentType.NATURALIZATION_CERTIFICATE);
  }

  @JsonProperty("AlienNumber")
  private String alienNumber;

  @JsonProperty("NaturalizationNumber")
  private String naturalizationNumber;

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isValid()
  {
   if(!notBlank(this.alienNumber) || !notBlank(this.naturalizationNumber))
   {
     log.error("alienNumber: {}, nuralizationNumber: {} are required", this.alienNumber, this.naturalizationNumber);
     return false;
   }

   return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public DhsDocument from(final CitizenshipDocument citizenshipDocument)
  {
    final NaturalizationCertificateDocument document = new NaturalizationCertificateDocument();

    document.setAlienNumber(validValue(citizenshipDocument.getAlienNumber()));
    document.setNaturalizationNumber(validValue(citizenshipDocument.getForeignPassportOrDocumentNumber()));

    return document;
  }

  public String getAlienNumber()
  {
    return alienNumber;
  }

  public void setAlienNumber(final String alienNumber)
  {
    this.alienNumber = alienNumber;
  }

  public String getNaturalizationNumber()
  {
    return naturalizationNumber;
  }

  public void setNaturalizationNumber(final String naturalizationNumber)
  {
    this.naturalizationNumber = naturalizationNumber;
  }

  @Override
  public String toString()
  {
    return new StringJoiner(", ", NaturalizationCertificateDocument.class.getSimpleName() + "[", "]")
        .add("alienNumber='" + alienNumber + "'")
        .add("naturalizationNumber='" + naturalizationNumber + "'")
        .toString();
  }
}
