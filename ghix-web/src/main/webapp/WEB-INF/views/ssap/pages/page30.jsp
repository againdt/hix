<%@ include file="../include.jsp" %>
   
        

  <div id="appscr80" style='display: none;'>         
                            
           <div class="titleCSSForPages">
				<label class="titleForPages" class="labelML15"><spring:message code="ssap.page30.taxFilerAndOther" text="Tax Filer & Other Additional Questions for" /> <span class="nameOfHouseHold"></span></label>
		      	<div class="Hr_ui_design"><div class="Hr_ui_design1"></div> </div>
			</div>          
                   
                   <form>
                    
                   
                   
                   
                     <div class="input_tetx_area"><label class="cobaapp"><spring:message code="ssap.page30.providingASSANMayText1" text=" [Tax filer name] indicated [he/she] is the claiming tax filer for [Applicant(s) names]; however a Social Security number (SSN) hasn't been entered for [Tax filer name]." /> <spring:message code="ssap.page30.providingASSANMayText2" text="Providing a SSN may help get a better idea of how much help you can get in paying for health insurance coverage." /> <spring:message code="ssap.page30.providingASSANMayText3" text="The SSN you provide won't be used to verify citizenship or immigration status." /> <spring:message code="ssap.page30.providingASSANMayText4" text="Does [Tax filer name] want to provide one now" />?</label> </div>
                     
                    
  
                       <div class="input_tetx_area"> 
 <span class="marginLeft28"> <input type="radio" id="ssnIndicator80Yes" name="ssnIndicator80" value="yes" checked="checked" onclick="hideSSNOnPage80();"> <spring:message code="ssap.yes" text="Yes" />
  <input type="radio" id="ssnIndicator80No" name="ssnIndicator80" value="no" onclick="hideSSNOnPage80()"><spring:message code="ssap.no" text="No" /></span></div>
        
          
       <!--   <div class="Hr_ui_design"><div class="Hr_ui_design1"></div> </div>  -->
         
         
         <div id="ssnOnPage80">
    <div class="input_tetx_area"> <label class="cobaapp_title_ui_color labelMT23"><spring:message code="ssap.page30.socialSecurityNumber" text="Social Security Number" />:</label> </div>
         
                     
               <div class="input_tetx_area">
                     <div class="input_page_auto">
   <input type="text" placeHolder="xxx" id="ssn1_80" class="input_row_area_small" name="ssn1_80" />
  <input type="text" placeHolder="xx" id="ssn2_80" class="input_row_area_small" name="ssn2_80"/>
   <input type="text" placeHolder="xxxx" id="ssn3_80" class="input_row_area_small" name="ssn3_80"/>
   </div>
   
   </div>
   </div>
    <div class="input_tetx_area"><label class="cobaapp labelMT23"> <spring:message code="ssap.page30.inOrderToGetPayText1" text="In order for [Applicant(s) names] to get help paying for health insurance, each person must file a tax return or be claimed as a dependent on someone else's tax return." /> <spring:message code="ssap.page30.doYouWantToChangeText2" text="Do you want to change your answers about how [Applicant(s) names] will file taxes for" /> <span class='coverageYear'></span>?</label> </div>
                     
                    
  
                       <div class="input_tetx_area">
                       <span class="marginLeft28"> 
 <input type="radio" name="changeDecisionAboutFileTax" value="Yes" id="male_0"> <spring:message code="ssap.yes" text="Yes" />
 <input type="radio" name="changeDecisionAboutFileTax" value="No" id="male_1" checked><spring:message code="ssap.no" text="No" /> </span></div>
 
 
 <div class="input_tetx_area"><label class="cobaapp labelMT23"> <spring:message code="ssap.page30.inOrderToGetPayForHealthText1" text="In order for [Applicant(s) names] to get help paying for health insurance, [he/she] must file a joint federal income tax return with (his or her) spouse." /> <spring:message code="ssap.page30.inOrderToGetPayForHealthText2" text="Do you want to change your answers about how [Applicant(s) names] will file taxes for" /> <span class='coverageYear'></span>?

</label> </div>
                     
                    
  
                       <div class="input_tetx_area"><span class="marginLeft28">
  <input type="radio" name="changeDecisionAboutFileTaxInJointFederalIncome" value="Yes" id="male_0"> <spring:message code="ssap.yes" text="Yes" />
  <input type="radio" name="changeDecisionAboutFileTaxInJointFederalIncome" value="No" id="male_1" checked><spring:message code="ssap.no" text="No" /></span></div>
 
 
 
 <div class="input_tetx_area"> <label class="cobaapp_title_ui_color labelMT23" ><spring:message code="ssap.page30.whereWill" text="Where will" /> <span class="nameOfHouseHold"></span> <spring:message code="ssap.page30.beLivingIn" text="be living in New Mexico" />? </label> </div>
         
                     
                    <div class="input_tetx_area">
                     <div class="input_page_auto">
   <input type="text" class="input_row_area_small" placeholder="<spring:message code='ssap.city' text='City' />" id="appscr80City" name="appscr80City" >
  <input type="text"  class="input_row_area_small" placeholder="<spring:message code='ssap.zip' text='Zip' />" id="appscr80Zip" name="appscr80Zip">
   <input type="text" placeholder="<spring:message code='ssap.State' text='State' />" class="input_row_area_small" id="appscr80State" name="appscr80State">
     <select class="select_row_options">
       <option value="0"><spring:message code="ssap.County" text="County" /></option>
				<option value="Bernalillo"><spring:message code="ssap.county.Bernalillo" text="Bernalillo" /></option>
				<option value="Catron"><spring:message code="ssap.county.Catron" text="Catron" /></option>
				<option value="Chaves"><spring:message code="ssap.county.Chaves" text="Chaves" /></option>
				<option value="Cibola"><spring:message code="ssap.county.Cibola" text="Cibola" /></option>
				<option value="Colfax"><spring:message code="ssap.county.Colfax" text="Colfax" /></option>
				<option value="Curry"><spring:message code="ssap.county.Curry" text="Curry" /></option>
				<option value="De Baca"><spring:message code="ssap.county.DeBaca" text="De Baca" /></option>
				<option value="Dona Ana "><spring:message code="ssap.county.DonaAna" text="Dona Ana" /> </option>
				<option value="Eddy"><spring:message code="ssap.county.Eddy" text="Eddy" /></option>
				<option value="Grant"><spring:message code="ssap.county.Grant" text="Grant" /></option>
				<option value="Guadalupe"><spring:message code="ssap.county.Guadalupe" text="Guadalupe" /></option>
				<option value="Harding"><spring:message code="ssap.county.Harding" text="Harding" /></option>
				<option value="Hidalgo"><spring:message code="ssap.county.Hidalgo" text="Hidalgo" /></option>
				<option value="Lea"><spring:message code="ssap.county.Lea" text="Lea" /></option>
				<option value="Lincoln"><spring:message code="ssap.county.Lincoln" text="Lincoln" /></option>
				<option value="Los Alamos "><spring:message code="ssap.county.LosAlamos" text="Los Alamos" /> </option>
				<option value="Luna "><spring:message code="ssap.county.Luna" text="Luna" /> </option>
				<option value="McKinley "><spring:message code="ssap.county.McKinley" text="McKinley" /> </option>
				<option value="Mora "><spring:message code="ssap.county.Mora" text="Mora" /> </option>
				<option value="Otero "><spring:message code="ssap.county.Otero" text="Otero" /> </option>
				<option value="Quay "><spring:message code="ssap.county.Quay" text="Quay" /> </option>
				<option value="Rio Arriba "><spring:message code="ssap.county.RioArriba" text="Rio Arriba" /> </option>
				<option value="Roosevelt "><spring:message code="ssap.county.Roosevelt" text="Roosevelt" /> </option>
				<option value="Sandoval "><spring:message code="ssap.county.Sandoval" text="Sandoval" /> </option>
				<option value="San Juan "><spring:message code="ssap.county.SanJuan" text="San Juan" /> </option>
				<option value="San Miguel "><spring:message code="ssap.county.SanMiguel" text="San Miguel" /> </option>
				<option value="Santa Fe "><spring:message code="ssap.county.SantaFe" text="Santa Fe" /> </option>
				<option value="Sierra "><spring:message code="ssap.county.Sierra" text="Sierra" /> </option>
				<option value="Socorro "><spring:message code="ssap.county.Socorro" text="Socorro" /> </option>
				<option value="Taos "><spring:message code="ssap.county.Taos" text="Taos" /> </option>
				<option value="Torrance "><spring:message code="ssap.county.Torrance" text="Torrance" /> </option>
				<option value="Union "><spring:message code="ssap.county.Union " text="Union " /></option>
				<option value="Valencia "><spring:message code="ssap.county.Valencia" text="Valencia" /> </option>
				<option value="other"><spring:message code="ssap.county.Other" text="Other" /></option>		
    </select>
   </div>
   
    </div> 
 <div class="input_tetx_area"> <label class="cobaapp_title_ui_color"> </label> </div>
         
                  
                   </form>
                  
         
         </div>
            