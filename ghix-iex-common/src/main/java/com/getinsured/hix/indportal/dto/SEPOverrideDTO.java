package com.getinsured.hix.indportal.dto;

import java.util.List;

/**
 * @author vishwanath_s
 * Class to store SEP Override DTO
 */
public class SEPOverrideDTO {

	private String sepStartDate;
	private String sepEndDate;
	private String caseNumber;
	private Integer coverageYear;
	private String overrideCommentText;
	private String applicationType;
	private Boolean isFinancial;
	private Integer userId;
	private List<ApplicantEventsDTO> applicantEvents;
	/**
	 * @return the sepStartDate
	 */
	public String getSepStartDate() {
		return sepStartDate;
	}
	/**
	 * @param sepStartDate the sepStartDate to set
	 */
	public void setSepStartDate(String sepStartDate) {
		this.sepStartDate = sepStartDate;
	}
	/**
	 * @return the sepEndDate
	 */
	public String getSepEndDate() {
		return sepEndDate;
	}
	/**
	 * @param sepEndDate the sepEndDate to set
	 */
	public void setSepEndDate(String sepEndDate) {
		this.sepEndDate = sepEndDate;
	}
	/**
	 * @return the caseNumber
	 */
	public String getCaseNumber() {
		return caseNumber;
	}
	/**
	 * @param caseNumber the caseNumber to set
	 */
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	/**
	 * @return the coverageYear
	 */
	public Integer getCoverageYear() {
		return coverageYear;
	}
	/**
	 * @param coverageYear the coverageYear to set
	 */
	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}
	/**
	 * @return the overrideCommentText
	 */
	public String getOverrideCommentText() {
		return overrideCommentText;
	}
	/**
	 * @param overrideCommentText the overrideCommentText to set
	 */
	public void setOverrideCommentText(String overrideCommentText) {
		this.overrideCommentText = overrideCommentText;
	}
	/**
	 * @return the applicantEvents
	 */
	public List<ApplicantEventsDTO> getApplicantEvents() {
		return applicantEvents;
	}
	/**
	 * @param applicantEvents the applicantEvents to set
	 */
	public void setApplicantEvents(List<ApplicantEventsDTO> applicantEvents) {
		this.applicantEvents = applicantEvents;
	}
	/**
	 * @return the applicationType
	 */
	public String getApplicationType() {
		return applicationType;
	}
	/**
	 * @param applicationType the applicationType to set
	 */
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}
	/**
	 * @return the isFinancial
	 */
	public Boolean getIsFinancial() {
		return isFinancial;
	}
	/**
	 * @param isFinancial the isFinancial to set
	 */
	public void setIsFinancial(Boolean isFinancial) {
		this.isFinancial = isFinancial;
	}
	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
