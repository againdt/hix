package com.getinsured.ssap.model.hub.vlp;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes VLP request that will be sent to HUB service.
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VlpRequest {
  private long applicationId;

  private DocumentType documentType;

  private String clientIp;

  @JsonProperty("cms_partner")
  private String cmsPartnerId;

  @JsonProperty("CasePOCFullName")
  private String casePOCFullName;

  @JsonProperty("CasePOCPhoneNumber")
  private String casePOCPhoneNumber;

  private VlpPayload payload;

  public long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(final long applicationId) {
    this.applicationId = applicationId;
  }

  public DocumentType getDocumentType() {
    return documentType;
  }

  public void setDocumentType(final DocumentType documentType) {
    this.documentType = documentType;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setClientIp(final String clientIp) {
    this.clientIp = clientIp;
  }

  public VlpPayload getPayload() {
    return payload;
  }

  public void setPayload(final VlpPayload payload) {
    this.payload = payload;
    if (this.payload != null && this.payload.getDhsDocument() != null) {
      this.documentType = this.payload.getDhsDocument().getDocumentType();
    }
  }

  public String getCmsPartnerId() {
    return cmsPartnerId;
  }

  public void setCmsPartnerId(String cmsPartnerId) {
    this.cmsPartnerId = cmsPartnerId;
  }

  public String getCasePOCFullName() {
    return casePOCFullName;
  }

  public void setCasePOCFullName(String casePOCFullName) {
    this.casePOCFullName = casePOCFullName;
  }

  public String getCasePOCPhoneNumber() {
    return casePOCPhoneNumber;
  }

  public void setCasePOCPhoneNumber(String casePOCPhoneNumber) {
    this.casePOCPhoneNumber = casePOCPhoneNumber;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", VlpRequest.class.getSimpleName() + "[", "]")
      .add("applicationId=" + applicationId)
      .add("documentType=" + documentType)
      .add("clientIp='" + clientIp + "'")
      .add("cmsPartnerId='" + cmsPartnerId + "'")
      .add("casePOCFullName='" + casePOCFullName + "'")
      .add("casePOCPhoneNumber='" + casePOCPhoneNumber + "'")
      .add("payload=" + payload)
      .toString();
  }
}
