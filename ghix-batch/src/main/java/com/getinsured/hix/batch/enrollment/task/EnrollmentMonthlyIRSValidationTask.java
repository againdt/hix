package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyIRSBatchService;
import com.getinsured.hix.batch.enrollment.skip.EnrollmentIRSOut;

/**
 * 
 * @since 26/03/2016 
 * Class to validate generated monthly XML against XSD
 *
 */
public class EnrollmentMonthlyIRSValidationTask implements Tasklet
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyIRSValidationTask.class);
	private EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService;
	private EnrollmentIRSOut enrollmentIRSOut;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		LOGGER.info("MonthlyIRSValidation Started.....");
		Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecution().getJobId();
		if(!enrollmentMonthlyIRSBatchService.generatedXMLsValid(jobId)){
			enrollmentIRSOut.setGeneratedXMLsValid(Boolean.FALSE);
		}
		return RepeatStatus.FINISHED;
	}

	/**
	 * @return the enrollmentMonthlyIRSBatchService
	 */
	public EnrollmentMonthlyIRSBatchService getEnrollmentMonthlyIRSBatchService() {
		return enrollmentMonthlyIRSBatchService;
	}

	/**
	 * @param enrollmentMonthlyIRSBatchService the enrollmentMonthlyIRSBatchService to set
	 */
	public void setEnrollmentMonthlyIRSBatchService(EnrollmentMonthlyIRSBatchService enrollmentMonthlyIRSBatchService) {
		this.enrollmentMonthlyIRSBatchService = enrollmentMonthlyIRSBatchService;
	}

	/**
	 * @return the enrollmentIRSOut
	 */
	public EnrollmentIRSOut getEnrollmentIRSOut() {
		return enrollmentIRSOut;
	}

	/**
	 * @param enrollmentIRSOut the enrollmentIRSOut to set
	 */
	public void setEnrollmentIRSOut(EnrollmentIRSOut enrollmentIRSOut) {
		this.enrollmentIRSOut = enrollmentIRSOut;
	}
}
