package com.getinsured.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * Repository for {@link SsapApplicant}.
 * @author Yevgen Golubenko
 * @since 3/5/19
 */
@Repository
public interface ApplicantRepository extends JpaRepository<SsapApplicant, Long>
{
  /**
   * Finds {@link SsapApplicant} list for given SSAP id.
   * @param applicationId SSAP id.
   * @return list of applicants ({@link SsapApplicant}) for given SSAP id.
   */
  @Query("SELECT applicant FROM SsapApplicant AS applicant WHERE applicant.ssapApplication.id = :applicationId AND applicant.onApplication = 'Y'")
  List<SsapApplicant> findBySsapApplicationId(@Param("applicationId") final Long applicationId);

  /**
   * Returns {@link SsapApplicant} for given GUID.
   * @param applicantGuid applicant GUID.
   * @return {@link SsapApplicant} or {@code null}.
   */
  SsapApplicant findByApplicantGuid(final String applicantGuid);
}
