<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->	

	<div class="container">
		<div class="row-fluid">
			<div class="span3">
				<ul class="nav nav-list">
					<li class="nav-header">
						<spring:message  code="label.brkagentdesignation"/> 
					</li>
				</ul>
			</div><!-- end of span3 -->

			<div class="span9">
				<div class="page-header">
					<h1><a name="skip"></a></h1>
					<br />
				</div><!-- end of page-header -->

				<div class="alert alert-info">
					<p><spring:message  code="label.brkalreadydedesignated"/>  </p>
				</div><!-- .alert -->
				<a href="<c:url value="/broker/search"/>" class="btn btn-primary"><spring:message  code="label.brkbacktosearch"/>  </a>
			</div><!-- end of span9 -->
		</div><!--  end of .row-fluid -->
	</div><!-- end of .container -->