<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page
	import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page
	import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.util.DisplayUtil"%>
<%@page
	import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page
	import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor"%>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>



<style>
#masthead .nav-collapse li {
	display: none;
}

#masthead .nav-collapse li.dropdown,#masthead .nav-collapse li.dropdown li
	{
	display: block;
}

#menu {
	display: none !important;
}

p {
	font-size: 15px;
	line-height: 24px;
}

.icon-wrapper {
	border-bottom: 1px solid rgba(0, 0, 0, 0.05);
	width: 100%;
	display: block;
	margin-bottom: 2em;
}

.icon-wrapper .icon-cogs {
	font-size: 40px !important;
	line-height: 100px;
	margin: 0 !important;
}
</style>


<div class="gutter10">
	<div class="row-fluid">
		<div class="span10 offset1 errorPage" id="rightpanel">
			<div class="alert alert-info margin20-t margin20-b txt-center">
				<div class="icon-wrapper">
					<i class="icon-cogs"></i>
				</div>
				<h1>We could not process your access code.</h1>
				<div>
					<c:if
						test="${sessionScope.enrolledAppSess == 'Y' or sessionScope.ActiveAppPresent =='Y'}">
						<div class="modal-body" style="text-align: left;">
							<c:if test="${sessionScope.enrolledAppSess == 'Y'}">
								<ul>
									<li><spring:message code="msg.referral.verify.content7" /></li>
									<li><spring:message code="msg.referral.verify.content8" /></li>
								</ul>


							</c:if>
							<c:if test="${sessionScope.ActiveAppPresent == 'Y'}">
								<ul>
									<li><spring:message code="msg.referral.verify.content7" /></li>
									<li><spring:message code="msg.referral.verify.content8" /></li>
								</ul>


							</c:if>

						</div>
						<c:choose>
							<c:when test="${currentRoleName != 'INDIVIDUAL' }">
									<c:choose>
										<c:when test="${sessionScope.isUserSwitchToOtherView  == 'Y' }">
												<p>
													<a href="<c:url value='/indportal' />" class="btn btn-primary">Continue</a>
												</p>
										</c:when>
										<c:otherwise>	
											<button aria-hidden="true" id="crossCloseComplete"
										       class="btn btn-primary" type="button"
										    href="#markCompleteDialog" data-toggle="modal">Continue</button>
										</c:otherwise>
									</c:choose>
							</c:when>
							<c:otherwise>
								<p>
									<a href="<c:url value='/indportal' />" class="btn btn-primary">Continue</a>
								</p>
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${ssnORdObConflict == 'Y'  }">
						<div class="modal-body" style="text-align: left;">
								<ul>
									<li><spring:message code="msg.referral.verify.additional" /></li>
								</ul>
						</div>
						<c:if test="${currentRoleName == 'INDIVIDUAL' }">
					    	<a href="<c:url value='/indportal' />" class="btn btn-primary">Continue</a>
					    </c:if>
					</c:if>
					
					<c:if test="${emptyQuestionMap == 'Y'  }">
						<p class="margin20-tb">${errorMsg}</p>
					    <br />
					    <c:if test="${currentRoleName == 'INDIVIDUAL' }">
					    	<a href="<c:url value='/indportal' />" class="btn btn-primary">Continue</a>
					    </c:if>
					    
					</c:if>
					
				</div>



				<form name="dialogForm" id="dialogForm" method="POST"
					action="<c:url value="/account/user/switchUserRole" />"
					novalidate="novalidate">
					<df:csrfToken />
					<div id="markCompleteDialog" class="modal hide fade" tabindex="-1"
						role="dialog" aria-labelledby="markCompleteDialog"
						aria-hidden="true">
						<div class="markCompleteHeader">
							<div class="header">
								<h4 class="margin0 pull-left">View Member Account</h4>
								<button aria-hidden="true" data-dismiss="modal" id="crossClose"
									class="dialogClose" title="x" type="button">x</button>
							</div>
						</div>

						<div class="modal-body clearfix gutter0-tb">
							<div class="control-group">
								<div class="controls" style="text-align:left;">
									Clicking "Member View" will take you to the Member's portal for
									${firstName} ${lastName}.<br /> Through this portal you will
									be able to take actions on behalf of the member.<br /> Proceed
									to Member view?
								</div>
							</div>
						</div>
						
						<jsp:useBean id="moduleParamsMap" class="java.util.HashMap"/>
						<c:set target="${moduleParamsMap}" property="switchToModuleName" value="individual" />
						<c:set target="${moduleParamsMap}" property="switchToModuleId" value="${householdId}" />
						<c:set target="${moduleParamsMap}" property="switchToResourceName" value="${firstName} ${lastName}" />
						<surl:secureUrl paramMap="${moduleParamsMap}" var="ecyptedModuleParamsMap"/>
						
						
						<div class="modal-footer clearfix">
							<input class="pull-left" type="checkbox" id="checkConsumerView" name="checkConsumerView">
							<div class="pull-left">&nbsp; Don't show this message again.</div>
							<button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
							<button class="btn btn-primary" type="submit">Member View</button>
							<input type="hidden" name="ref" id="ref" value="${ecyptedModuleParamsMap}" />
						</div>
		
					</div>
				</form>



				<c:if test="${not empty verifyAccess}">

					<p class="margin20-tb">${errorMsg}</p>
					<br />
					<c:choose>
						<c:when test="${not empty invalidCaptcha}">
							<a href="<c:url value='/' />" class="btn btn-primary">Try
								Again</a>
						</c:when>
						<c:when test="${not empty invalidAccessCode}">

							<c:choose>
								<c:when
									test="${role == 'INDIVIDUAL' || moduleName=='INDIVIDUAL'}">
									<a href="<c:url value='/indportal' />" class="btn btn-primary">Try
										Again</a>
								</c:when>
								<c:when test="${role == 'ASSISTER'}">
									<a href="<c:url value='/entity/assister/dashboard' />"
										class="btn btn-primary">Try again</a>
								</c:when>
								<c:when test="${role == 'BROKER'}">
									<a href="<c:url value='/broker/dashboard' />"
										class="btn btn-primary">Try again</a>
								</c:when>

								<c:when
									test="${role == 'CSR' or fn:containsIgnoreCase(role, 'CSR')}">
									<a href="<c:url value='/ticketmgmt/ticket/ticketlist' />"
										class="btn btn-primary">Try again</a>
								</c:when>
								<c:otherwise>
									<a href="<c:url value='/' />" class="btn btn-primary">Try
										Again</a>
								</c:otherwise>
							</c:choose>
						</c:when>

						<c:when test="${not empty locked}">

							<c:choose>
								<c:when test="${role == 'INDIVIDUAL'}">
									<a href="<c:url value='/indportal' />" class="btn btn-primary">Goto
										Dashboard</a>
								</c:when>
								<c:when test="${role == 'BROKER'}">
									<a href="<c:url value='/broker/dashboard' />"
										class="btn btn-primary">Goto Dashboard</a>
								</c:when>
								<c:when test="${role == 'ASSISTER'}">
									<a href="<c:url value='/entity/assister/dashboard' />"
										class="btn btn-primary">Try again</a>
								</c:when>

								<c:otherwise>
									<a href="<c:url value='/' />" class="btn btn-primary">Continue</a>
								</c:otherwise>
							</c:choose>

						</c:when>

						<c:otherwise>
							<a href="<c:url value='/' />" class="btn btn-primary">Continue</a>
						</c:otherwise>
					</c:choose>
				</c:if>


				<c:if test="${not empty securityQuestions}">
					<p class="margin20-tb">${errorMsg}</p>
					<br />
					<c:choose>
						<c:when test="${not empty invalidCaptcha}">
							<a
								href="<c:url value="/referral/dynamicsecurityquestions/${encAccessCode}" />"
								class="btn btn-primary btn-large">Try Again</a>
						</c:when>
						<c:when test="${not empty invalidAnswers}">
							<a
								href="<c:url value="/referral/dynamicsecurityquestions/${encAccessCode}" />"
								class="btn btn-primary btn-large">Try Again</a>
						</c:when>
						<c:when test="${not empty locked}">
							<c:choose>
								<c:when test="${role == 'INDIVIDUAL'}">
									<a href="<c:url value='/indportal' />" class="btn btn-primary">Continue</a>
								</c:when>
								<c:when test="${role == 'BROKER'}">
									<a href="<c:url value='/broker/dashboard' />"
										class="btn btn-primary">Continue</a>
								</c:when>
								<c:when test="${role == 'ASSISTER'}">
									<a href="<c:url value='/entity/assister/dashboard' />"
										class="btn btn-primary">Try again</a>
								</c:when>
								<c:otherwise>
									<a href="<c:url value='/' />" class="btn btn-primary">Continue</a>
								</c:otherwise>
							</c:choose>

						</c:when>

						<c:otherwise>
							<a href="<c:url value='/' />" class="btn btn-primary">Continue</a>
						</c:otherwise>
					</c:choose>
				</c:if>











				<%-- 	<c:if test="${not empty tryAgain}">
						<p class="margin20-tb">${errorMsg}</p>
						<br />
						<c:choose>
							<c:when test="${empty captchaLogin}">
								<c:if test="${empty invalidAccessCode}">

								</c:if>

								<a
									href="<c:url value="/referral/dynamicsecurityquestions/${encAccessCode}" />"
									class="btn btn-primary btn-large">Try Again</a>
							</c:when>
							<c:otherwise>
								<a href="<c:url value='/' />" class="btn btn-primary">Try
									Again</a>
							</c:otherwise>
						</c:choose>
					</c:if>

					<c:if test="${not empty locked}">
						<p class="margin20-tb">${errorMsg}</p>
						<br />
						<c:choose>
							<c:when test="${role == 'INDIVIDUAL'}">
								<a href="<c:url value='/indportal' />" class="btn btn-primary">Go
									To Dashboard</a>
							</c:when>
							<c:when test="${role == 'AGENT'}">

							</c:when>
							<c:otherwise>
								<a href="<c:url value='/' />" class="btn btn-primary">Continue</a>
							</c:otherwise>
						</c:choose>
					</c:if> --%>
			</div>
		</div>
	</div>
</div>