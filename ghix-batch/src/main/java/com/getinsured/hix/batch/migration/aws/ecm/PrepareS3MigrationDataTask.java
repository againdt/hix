package com.getinsured.hix.batch.migration.aws.ecm;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.getinsured.hix.batch.migration.aws.ecm.S3DataConfig.MigrationStepEnum;
import com.getinsured.hix.batch.migration.couchbase.ecm.CouchbaseECMMigrationDTO;
import com.getinsured.hix.batch.migration.couchbase.ecm.JDBCUtil;

public class PrepareS3MigrationDataTask extends BaseEcmS3MigrationClass {
	private static final String ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION = "Encountered Exception in the process of preparing the database for Migration";

	private static final String PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION_COMPLETED_SUCCESSFULLY = "Process of preparing the database for Migration completed successfully";

	private static final String INITIATE_THE_PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION = "Initiate the process of preparing the database for Migration";

	private static final Logger LOGGER = LoggerFactory.getLogger(PrepareS3MigrationDataTask.class);

	private JdbcTemplate jdbcTemplate;

	public PrepareS3MigrationDataTask() {
		super(MigrationStepEnum.IDENTIFY);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	void startExecution() throws Exception {
		Connection con = null;
		try {
			LOGGER.info(INITIATE_THE_PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION);
			con = jdbcTemplate.getDataSource().getConnection();
			con.setAutoCommit(true);
			for (CouchbaseECMMigrationDTO tableDTO : S3DataConfig.migrationTables.values()) {
				LOGGER.info(
						"Clearing from Table - " + tableDTO.getTableName() + ", Column - " + tableDTO.getColumnName());
				clearData(tableDTO, con);
			}

			for (CouchbaseECMMigrationDTO tableDTO : S3DataConfig.migrationTables.values()) {
				LOGGER.info(
						"Inserting in Table - " + tableDTO.getTableName() + ", Column - " + tableDTO.getColumnName());
				prepareData(tableDTO, con);
			}

			LOGGER.info("Identify Duplicate ecm ids");
			handleDuplicateEcm(con);

			LOGGER.info(PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION_COMPLETED_SUCCESSFULLY);
		} catch (Exception e) {
			LOGGER.error(ENCOUNTERED_EXCEPTION_IN_THE_PROCESS_OF_PREPARING_THE_DATABASE_FOR_MIGRATION, e);
			throw e;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e2) {

				}
			}
		}

	}
	
	private void handleDuplicateEcm(Connection con) throws SQLException {

		String updateStatus = "Update " + S3DataConfig.S3_MIGRATION + " SET " + S3DataConfig.MIGRATION_STATUS +" ='" 
		+ S3DataConfig.MigrationStatusEnum.DUPLICATE.name() + "'"
		+" WHERE " + S3DataConfig.MIGRATION_STATUS + " = '" + S3DataConfig.MigrationStatusEnum.NOTSTARTED.name() + "'";
		
		int count = JDBCUtil.executeUpdate(con, updateStatus);
		
		LOGGER.info("HANDLE DUPLICATE STEP 1-" + count + " RECORDS OF AWS_MIGRATION TABLE ARE UPDATED AS DUPLICATE");
		
		updateStatus = "Update " + S3DataConfig.S3_MIGRATION + " SET " + S3DataConfig.MIGRATION_STATUS +" = '" 
		+ S3DataConfig.MigrationStatusEnum.NOTSTARTED.name() + "'"
		+ " WHERE " + S3DataConfig.MIGRATION_ID + " IN (select min("+ S3DataConfig.MIGRATION_ID + ") FROM " 
		+ S3DataConfig.S3_MIGRATION
		+" WHERE " + S3DataConfig.MIGRATION_STATUS + " = '" + S3DataConfig.MigrationStatusEnum.DUPLICATE.name() + "'"
		+ " GROUP BY " + S3DataConfig.EXISTING_ECM_ID + ")" ;
		
		count = JDBCUtil.executeUpdate(con, updateStatus);
		LOGGER.info("HANDLE DUPLICATE STEP 2-" + count + " RECORDS OF COUCH_MIGRATION TABLE ARE UPDATED AS NOTSTARTED");
	}

    private void prepareData(CouchbaseECMMigrationDTO tableDTO, Connection con) throws SQLException {
		StringBuilder st = new StringBuilder(100);
		st.append("INSERT INTO ").append(S3DataConfig.S3_MIGRATION)
				.append("(MIGRATION_ID,TABLE_NAME,COLUMN_NAME,PRIMARY_ID,EXISTING_ECM_ID,CREATION_TIMESTAMP,LAST_UPDATED_TIMESTAMP) SELECT ")
				.append("NEXTVAL('S3_MIGRATION_SEQ'),'").append(tableDTO.getTableName()).append("','")
				.append(tableDTO.getColumnName()).append("',").append(tableDTO.getPrimaryIdColumn()).append(",")
				.append(tableDTO.getColumnName()).append(",CURRENT_TIMESTAMP,CURRENT_TIMESTAMP").append(" from ")
				.append(tableDTO.getTableName()).append(" where ").append(tableDTO.getColumnName())
				.append(" is not null and COALESCE(").append(tableDTO.getColumnName()).append(", 'NULL') != 'NULL' and (")
				.append(tableDTO.getColumnName())
				.append(" not like '").append(S3DataConfig.ALFRESCO_ID_PREFIX).append("%')")
				.append(" and 0 = (select count(x.MIGRATION_ID) from ")
				.append(S3DataConfig.S3_MIGRATION).append(" x where x.PRIMARY_ID = ")
				.append(tableDTO.getPrimaryIdColumn()).append(" and x.TABLE_NAME = '").append(tableDTO.getTableName())
				.append("' and x.COLUMN_NAME = '").append(tableDTO.getColumnName()).append("')");

		LOGGER.info("Insert data from Table - " + tableDTO.getTableName() + ", Column - " + tableDTO.getColumnName()
				+ ", Query - \n " + st.toString());

		int countOfRecords = JDBCUtil.executeUpdate(con, st.toString());
		LOGGER.info("Total Records - " + countOfRecords);
		st = null;
	}

	private void clearData(CouchbaseECMMigrationDTO tableDTO, Connection con) throws SQLException {
		StringBuilder st = new StringBuilder(100);
		st.append("delete from ").append(S3DataConfig.S3_MIGRATION).append(" where TABLE_NAME = '")
				.append(tableDTO.getTableName()).append("' and COLUMN_NAME = '").append(tableDTO.getColumnName())
				.append("' and MIGRATION_STATUS in ('").append(S3DataConfig.MigrationStatusEnum.NOTSTARTED.name())
				.append("','").append(S3DataConfig.MigrationStatusEnum.DUPLICATE.name()).append("')");
		LOGGER.info("Clear data from Table - " + tableDTO.getTableName() + ", Column - " + tableDTO.getColumnName()
				+ ", Query - \n " + st.toString());
		int countOfRecords = JDBCUtil.executeUpdate(con, st.toString());
		LOGGER.info("Total Records - " + countOfRecords);
		st = null;
	}

}
