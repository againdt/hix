package com.getinsured.hix.broker.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.service.jpa.ConsumerComposite;
import com.getinsured.hix.broker.utils.ApplicationStatus;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.broker.BrokerBOBDetailsDTO;
import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.employees.EmployeeDTO;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.NoticeType;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTickets.TicketPriority;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.AEEConfiguration.AEEConfigurationEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ConsumerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.TicketMgmtUtils;
import com.getinsured.iex.household.service.PreferencesService;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * Handles requests for the application home page.
 */
@SuppressWarnings("unused")
@Controller
public class DesignateSearchController {

	private static final String ESIGNATURE_SUBMIT_END = "esignatureSubmit : END";
	private static final String ERROR_OCCURED_IN_RESETTING_SEARCH_CRITERIA = "Error occured in resetting search criteria";
	private static final String PHONE3 = "phone3";
	private static final String PHONE2 = "phone2";
	private static final String PHONE1 = "phone1";
	private static final int TWO = 2;
	private static final int TEN = 10;
	private static final int SIX = 6;
	private static final int THREE = 3;
	private static final int ZERO = 0;
	private static final String TKM_WORKFLOW_DEFAULT = "TKM_WORKFLOW_DEFAULT";
	private static final String ACCESS_IS_DENIED = "Access is denied.";
	private static final String AGENT_LN = "AgentLN";

	private static final String AGENT_FN = "AgentFN";

	private static final String AGENT_DEDESIGNATION_INDIVIDUAL_NOTICE = "AgentDedesignationIndividualNotice";

	private static final String AGENT_DE_DESIGNATION_NOTICE_INDIVIDUAL = "Agent De-designation Notice (Individual)";

	private static final String INDIVIDUAL_LN = "IndividualLN";

	private static final String INDIVIDUAL_MN = "IndividualMN";

	private static final String INDIVIDUAL_FN = "IndividualFN";

	private static final String EXCHANGE_PHONE = "exchangePhone";

	private static final String EXCHANGE_NAME = "exchangeName";

	private static final String PDF = ".pdf";

	private static final String NOTIFICATION_SEND_DATE = "notificationDate";

	private static final String SWITCH_TO_MODULE_NAME = "switchToModuleName";

	private static final String ACCEPTED = "accepted";

	private static final String INDIVIDUAL = "Individual";

	private static final String BOTH = "Both";

	private static final String EMPLOYEE_OBJECT_FOR_LEFT_NAV = "employeeObjectForLeftNav";

	private static final String TABLE_TITLE = "tableTitle";

	private static final String SEARCH_CRITERIA = "searchCriteria";

	private static final String COMPANY_NAME_MAP = "companyNameMap";

	private static final String CONTACT_NAME_MAP = "contactNameMap";

	private static final String Y2 = "y";

	private static final String COMPANY_NAME = "companyName";

	private static final String BROKER_SESSION = "brokerSession";

	private static final String APPLICANT_NAME = "applicantName";

	private static final String ID2 = "id";

	private static final String ERROR_MSG = "errorMsg";

	private static final String BRKPAGE = "brkpage";

	private static final String FILEPATH = "filepath";

	private static final String CURRENTPAGE = "currentpage";

	private static final String Y = "Y";

	private static final String DISTANCE = "distance";

	private static final String CURRENT_PAGE = "currentPage";

	private static final String RESULT_SIZE = "resultSize";

	private static final String SEARHZIPCODE = "searhzipcode";

	private static final String BROKERMAXDIS = "brokermaxdis";

	private static final String SEARCH_LANGUAGES = "searchLanguages";

	private static final String SEARCH_DISTANCE = "searchDistance";

	private static final String SEARCH_LAST_NAME = "searchLastName";

	private static final String SEARCH_FIRSTNAME = "searchFirstname";

	private static final String BROKERSEARCHPARAM = "brokersearchparam";

	private static final String USESESSION = "usesession";

	private static final String YES = "yes";

	private static final String EXT_INDIVIDUAL = "extIndividual";

	private static final String N = "N";

	private static final String RECORD_TYPE = "recordType";

	private static final String RECORD_ID = "recordId";

	private static final String INDIVIDUALS2 = "Individuals";

	private static final String PAGE_NUMBER = "pageNumber";

	private static final String INDIVIDUALS = "individuals";

	private static final String REQ_URI = "reqURI";

	private static final String PAGE_SIZE2 = "pageSize";

	private static final String RECORD_COUNT = "recordCount";

	private static final String BROKERLIST = "brokerlist";

	private static final String TRUE = "TRUE";

	private static final String FROM_MODULE = "fromModule";

	private static final String APPROVED_BROKERS_LIST = "approvedbrokerslist";

	private static final String BROKERS_LIST = "brokerslist";

	private static final String PAGE_TITLE = "page_title";

	private static final String REDIRECT_EMPLOYERS = "redirect:/broker/employers?desigStatus=";

	private static final String REDIRECT_INDIVIDUALS = "redirect:/broker/individuals?desigStatus=";

	private static final String DESIG_STATUS = "desigStatus";

	private static final String PENDING = "Pending";

	private static final String BROKER = "broker";

	private static final String REDIRECT_BROKER_DESIGNATECONFIRM = "forward:/broker/designateconfirm";

	private static final String INACTIVE_TABLE_TITLE = "Inactive ";

	private static final String ACTIVE_TABLE_TITLE = "Active ";

	private static final String PENDING_TABLE_TITLE = "Pending ";

	private static final String EMPLOYERS_MODULE = "employers";

	private static final String INDIVIDUALS_MODULE = INDIVIDUALS;

	private static final String ESIGN_NAME = "esignName";

	private static final String FIRSTNAME = "firstName";

	private static final String LASTNAME = "lastName";

	private static final String BROKER_ID = "brokerId";

	private static final String DESIGNATED_BROKER_NAME = "designatedBrokerName";
	private static final String DESIGNATED_ASSISTER_NAME = "designatedAssisterName";

	private static final String DESIGNATED_BROKER_PROFILE = "designatedBrokerProfile";

	private static final String CHECK_DILOG = "checkDilog";

	private static final String ZERO_STRING ="0";

	public static final Integer PAGE_SIZE = 10;
	private static final String LAST_VISITED_URL = "lastVisitedURL";

	//Used for getEmployeeDetails and getEmployeeEnrollment Details starts
	public static final String BROKER_FIRST_NAME = "brokerFirstName";
	public static final String EMPLOYEE_INFO = "objEmployee";
	public static final String LIST_EMPLOYEE_DETAILS = "listEmployeeDetails";
	public static final String ENROLLMENT_SHOP_DTO = "enrollmentShopDTO";
	public static final String GET_ENROLLMENT_DETAIL_BY_EMPLOYEE_ID = "enrollee/findenrolleebyemployeeid";
	//Used for getEmployeeDetails and getEmployeeEnrollment Details ends



	public static final String ENROLLMENT_STATUS = "enrollment_status";
	private static final String STATUS = "status";
	private static final Logger LOGGER = LoggerFactory.getLogger(DesignateSearchController.class);
	public static final String ENTITY_ENROLLMENT_STATUS = "AEE_ENROLLMENT_STATUS";
	public static final String CLIENTS_SERVED_EMPLOYERS = "Employers";
	public static final String CLIENTS_SERVED_INDIVIDUALS_FAMILIES = "Individuals / Families";
	private static final String EXCEPTION_USER_NOT_LOGGED_IN = "User not logged in";
	public static final String BROKER_CERTIFICATION_STATUS_CERTIFIED = "Certified";

	private static final String EMPLOYEE = "Employee";
	private static final String EMPLOYEE_ENROLLMENT_STATUS = "ENROLLMENT_STATUS";
	private static final String ENROLL_STATUSLIST = "enrollmentStatuslist";
	private static final String EMPLOYEELIST = "employeelist";
	private static final String EMPLOYEES = "employees";
	private static final String EMPLOYEE_NAME = "employeeName";
	private static final String EMPLOYEE_COMPANY_NAME = "employeeCompanyName";
	private static final String EMPLOYEE_PHONE_NUMBER = "employeePhoneNumber";
	private static final String EMPLOYEE_EMAIL = "employeeEmail";
	private static final String LANGUAGES_LIST = "languagesList";

	private static final String IS_ASSISTER_DESIGNATED = "isAssisterDesignated";
	private static final String ENROLLMENT_STATUS_PARAMETER = "enrollment_status";
	private static final String EXCEPTION_IS = ", Exception is: ";
	private static final String EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER = "Exception occurred while getting logged in user";
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static final String INDIVIDUAL_SWITCHEDMODULE = "individual";
	private static boolean isScimEnabled = GhixPlatformConstants.SCIM_ENABLED;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private DesignateService designateService;
	@Autowired
	private UserService userService;
	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	@Autowired
	private LookupService lookupService;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private EERestCallInvoker restClassCommunicator;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private NoticeTypeRepository noticeTypeRepo;

	@Autowired
	private NoticeService noticeService;

	@Autowired
	IBrokerRepository brokerRepository;

	@Autowired
	private RoleService roleService;

	@Autowired
	private AccountActivationService accountActivationService;

	@Autowired
	private TicketMgmtUtils ticketMgmtUtils;

	@Autowired
	private SCIMUserManager scimUserManager;
	
	@Autowired 
	private BrokerIndTriggerService brokerIndTriggerService;
	
	@Autowired private Gson platformGson;
	
	@Autowired
	private EntityUtils entityUtils;
	
	@Autowired
	private PreferencesService preferencesService;

	/**
	 * Handles the designate request for Broker from Employer/Individual
	 * modules.
	 *
	 * @param id
	 *            broker Id
	 * @param broker
	 *            Broker Model object
	 * @param result
	 *            the BindingResult object 
	 * @param model
	 *            the Model object
	 * @param request
	 * @return URL for navigation to appropriate result page
	 * @throws GIException
	 */
	@RequestMapping(value = "/broker/designate/{encBrokerId}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_DESIGNATE_EMPLOYER')")
	public String designate(@PathVariable("encBrokerId") String encBrokerId, @ModelAttribute(BROKER) Broker broker, BindingResult result,
			Model model, HttpServletRequest request) {

		LOGGER.info("designate : START");

		int brkId;
		AccountUser user = null;
		Employer employer = null;
		Household household = null;
		DesignateBroker designateBroker = null;
		String decryptBrokerId = null;

		String recordId = null;
		String recordType = null;
		boolean isEmployer = false;
		boolean isIndividual = false;
		boolean isSwitchedToEmployer= false;
		boolean isSwitchedToIndividual = false;

		try{
			decryptBrokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);

			Validate.isTrue(StringUtils.isNotBlank(decryptBrokerId));

			brkId = Integer.parseInt(decryptBrokerId);
			LOGGER.debug("Broker Designation Request for given Broker Id.");
			
			user = userService.getLoggedInUser();
		}catch(NumberFormatException nfe){
			String errMsg = "Failed to decrypt Broker id. "+ EXCEPTION_IS +nfe;
			LOGGER.error(errMsg);
			LOGGER.info("designate: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}  catch (InvalidUserException ex) {
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			recordId = (String) request.getSession().getAttribute(RECORD_ID);
			recordType = (String) request.getSession().getAttribute(RECORD_TYPE);
			LOGGER.info("Record ID : "+recordId+ " : Record Type : "+recordType);
			if (recordId != null && recordType != null) {
				Integer externalId = Integer.valueOf(recordId);
				
				if (BrokerConstants.EXT_EMPLOYER.equalsIgnoreCase(recordType)) {
					designateBroker = designateService.findBrokerByExternalEmployerId(externalId);
				} else if (BrokerConstants.EXT_INDIVIDUAL.equalsIgnoreCase(recordType)) {
					designateBroker = designateService.findBrokerByExternalIndividualId(externalId);
				}
			}
		} else {

			String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
			String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
			boolean switchedRole=false;
			if(isUserSwitchToOtherView != null && "y".equalsIgnoreCase(isUserSwitchToOtherView.trim())){
				switchedRole=true;
			}

			LOGGER.debug("Base product call for Broker Designation");

			Validate.notNull(user);

			// Check if user is Employer or Individual
			isEmployer = userService.hasUserRole(user, RoleService.EMPLOYER_ROLE);
			isIndividual = userService.hasUserRole(user, RoleService.INDIVIDUAL_ROLE);

			if(switchedRole && !isIndividual){
				LOGGER.debug("User has switched role");
				if(user.getActiveModuleName().equalsIgnoreCase( RoleService.INDIVIDUAL_ROLE)  && (stateExchangeType.equalsIgnoreCase(BOTH) || stateExchangeType.equalsIgnoreCase(INDIVIDUAL))){
					LOGGER.debug("User has switched to Indivodual");
					isSwitchedToIndividual = true;
					designateBroker = designateService.findBrokerByIndividualId(user.getActiveModuleId());
				}
			} else if (isIndividual && user.getActiveModuleName().equalsIgnoreCase( RoleService.INDIVIDUAL_ROLE)  && (stateExchangeType.equalsIgnoreCase(BOTH) || stateExchangeType.equalsIgnoreCase(INDIVIDUAL))) {
				LOGGER.debug("User is an Individual");
				household = getHouseHoldById(user.getActiveModuleId());
				if(household != null){
					designateBroker = designateService.findBrokerByIndividualId(household.getId());
				}
			}
		}

		// If there is no designate record for Broker, it can be designated
		if (designateBroker != null) {
			designateBroker.setUpdated(new TSDate());
			LOGGER.debug("Designate Broker not null. Redirecting to Message screen");

			if (!designateBroker.getStatus().toString().equals(Status.InActive.toString())) {
				LOGGER.info("designate : END");
				return "forward:/broker/designatemessage";
			}
		} else {

			LOGGER.debug("Creating new DesignateBroker object");

			designateBroker = new DesignateBroker();
		}
		
		designateBroker.setCreated(new TSDate());
		designateBroker.setCreatedBy(user.getId());

		
		
		
		// Populate DesignateBrokerObject and persist it
		Broker brokerObj = brokerService.getBrokerDetail(brkId);
		request.getSession().setAttribute(DESIGNATED_BROKER_PROFILE, brokerObj);
		String esignName = (String) request.getSession().getAttribute(ESIGN_NAME);

		if (recordId != null && recordType != null) {
			Integer externalId = Integer.valueOf(recordId);
			if (BrokerConstants.EXT_EMPLOYER.equalsIgnoreCase(recordType)) {
				designateBroker.setExternalEmployerId(externalId);
			} else if (BrokerConstants.EXT_INDIVIDUAL.equalsIgnoreCase(recordType)) {
				designateBroker.setExternalIndividualId(externalId);
			}
		} else {
			if(isEmployer) {
				Validate.notNull(employer);
				designateBroker.setEmployerId(employer.getId());
			}
			else if(isSwitchedToEmployer) {
				designateBroker.setEmployerId(user.getActiveModuleId());
			}
			else {
				designateBroker.setEmployerId(null);
			}

			if(isIndividual){
				Validate.notNull(household);
				designateBroker.setIndividualId(household.getId());
			}else if(isSwitchedToIndividual){
				designateBroker.setIndividualId(user.getActiveModuleId());
			}else{
				//No Individual Or No Switch to Individual
				designateBroker.setIndividualId(null);
			}
		}

		designateBroker.setBrokerId(brkId);
		designateBroker.setStatus(Status.Pending);
		designateBroker.setEsignBy(esignName);
		designateBroker.setEsignDate(new TSDate());
		designateBroker.setShow_switch_role_popup('Y');

		LOGGER.warn("Populating the DesignateBroker object and persisting it");

		try
		{
			LOGGER.warn("Populating the DesignateBroker object and persisting it : "+designateBroker);
			EntityResponseDTO entityResponseDTO = designateService.saveDesignateBroker(designateBroker);
			
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				if(entityResponseDTO==null || BrokerMgmtUtils.isEmpty(entityResponseDTO.getStatus()) || BrokerConstants.RESPONSE_FAILURE.equals(entityResponseDTO.getStatus())){
					return "redirect:/broker/designationerror";
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("Exception ocurred while designating Individual.", giexception);
            throw giexception;
		}

		request.getSession().setAttribute("designateBroker", designateBroker);
		request.getSession().removeAttribute("showAgentPromotion");
		request.getSession().removeAttribute(ESIGN_NAME);

		LOGGER.warn("DB save successful. Redirecting to Broker Designation confirmation screen");

		LOGGER.warn("designate : END");

		return REDIRECT_BROKER_DESIGNATECONFIRM;
	}
	/**
	 * Error message is displayed if IND47 fails
	 * 
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/designationerror", method = RequestMethod.GET)
	//@PreAuthorize(HAS_PERMISSION_MODEL_USER_DESIGNATE_BROKER)
	public String displayErrorPage(Model model, HttpServletRequest request) {
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Designation");
		LOGGER.info("displayErrorPage: START ");
		
		LOGGER.info("displayErrorPage: END ");
		return "broker/designationerror";
	}

	/**
	 * Handles the de-designate request for Broker from Employer/Individual
	 * modules.
	 *
	 * @param encryptedId
	 *            employer / individual id
	 * @param broker
	 *            {@link Broker}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@GiAudit(transactionName = "Dedesignate", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.DEDESIGNATE_AGENT)
	@RequestMapping(value = "/broker/dedesignate/{encryptedId}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_DE_DESIGNATE_EMPLOYER')")
	@ResponseBody
	public String dedesignate(@PathVariable("encryptedId") String encryptedId, @ModelAttribute(BROKER) Broker broker,
			BindingResult result, Model model, HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("dedesignate : START");

		int brkId;
		String decryptedId = null;
		AccountUser user = null;
		Household household = null;
		DesignateBroker designateBroker = null;
		try{
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			brkId = Integer.parseInt(decryptedId);
			Broker brokerObj = brokerService.getBrokerDetail(brkId);

			try {
				user = userService.getLoggedInUser();
			} catch (InvalidUserException ex) {
				LOGGER.error("User not logged in", ex);
				throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
			
			Validate.notNull(user,"User object is null.");
			
			if (userService.hasUserRole(user, RoleService.INDIVIDUAL_ROLE)) {
				household = getHouseHoldByUserId(user.getId());
				if(household != null){
					designateBroker = designateService.findBrokerByIndividualId(household.getId());
				}
			}else {
				/**Broker role has been added to this if condition for resolving HIX-29747
				 * Following code de-designates a broker for an employer by an 'admin' user.
				 */

				try {

					String switchedModuleName = user.getActiveModuleName();
					Validate.notNull(user.getActiveModuleName(), "The switched module type information is missing in the request.");
					int currSwitchedModuleId = user.getActiveModuleId();
					if(INDIVIDUAL_SWITCHEDMODULE.equalsIgnoreCase(switchedModuleName)) {

						Validate.notNull(user.getActiveModuleId(),"The switched account identifier is missing in the request.");
						designateBroker = designateService.findBrokerByIndividualId(currSwitchedModuleId);
						household = getHouseHoldById(currSwitchedModuleId);
					}
				}
				catch(Exception ex) {
					LOGGER.warn("Exception encountered while identifying dedesignation user information", ex);
					throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
				}
			}

			if (designateBroker != null && user != null) {
				designateBroker.setBrokerId(brkId);
				designateBroker.setStatus(Status.InActive);
				designateBroker.setUpdated(new TSDate());
				designateBroker.setUpdatedBy(user.getId());

				// Call the enrollment rest API here.If enrollment update fails, then throw an exception and do not
				// save designation information.

				brokerMgmtUtils.updateEnrollmentDetails(brokerObj, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, user.getUsername(),
						new Long(household.getId()), BrokerConstants.AGENT_ROLE, BrokerConstants.REMOVEACTIONFLAG);

				designateService.saveDesignateBroker(designateBroker);

				request.getSession().setAttribute(DESIGNATED_BROKER_PROFILE, brokerObj);
				request.getSession().setAttribute("designateBroker", designateBroker);

				if(DynamicPropertiesUtil.getPropertyValue(AEEConfigurationEnum.INDIVIDUAL_AGENT_DE_DESIGNATION_NOTICE).equalsIgnoreCase("Y")) {
					sendDedesignationNoticeToIndividual(household,brokerObj);
				}
				request.getSession().setAttribute("showAgentPromotion", true);
			}
		}catch (NumberFormatException nfe){
			String errMsg = "Failed to decrypt BrokerId. " + EXCEPTION_IS +nfe;
			LOGGER.error(errMsg);
			LOGGER.info("dedesignate: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while handling the decline designation request for Employers/Individuals : ", giexception);
			throw giexception;
		}
		LOGGER.info("dedesignate : END");
		return "success";
	}

	private boolean sendDedesignationNoticeToIndividual(Household household, Broker brokerObj){
		LOGGER.info("sendEmployerlDesignationStatusNotice : START");
		boolean opStatus = false;
		String noticeTemplateName = AGENT_DE_DESIGNATION_NOTICE_INDIVIDUAL;
		String templateLocation = AGENT_DEDESIGNATION_INDIVIDUAL_NOTICE;
		Map<String, Object> templateData = new HashMap<String, Object>();
		try{

			SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
			templateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));

			templateData.put(INDIVIDUAL_FN, household.getFirstName());

			/*condition check for Individual Middle name For null and empty*/

			if(household.getMiddleName()!=null && !household.getMiddleName().isEmpty()){
				templateData.put(INDIVIDUAL_MN, household.getMiddleName());
			}else{
				templateData.put(INDIVIDUAL_MN, "");
			}
			templateData.put(INDIVIDUAL_LN, household.getLastName());

			templateData.put(AGENT_FN, brokerObj.getUser().getFirstName());
			templateData.put(AGENT_LN, brokerObj.getUser().getLastName());

			templateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			templateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));


			NoticeType noticeType = noticeTypeRepo.findByNameAndLanguage(noticeTemplateName, GhixLanguage.US_EN);

			Validate.notNull(noticeType, "NoticeType not found for given template in repository.");

			String ecmfileName = noticeTemplateName + TimeShifterUtil.currentTimeMillis() + PDF;
			
			List<String> toList = new LinkedList<String>();
			toList.add(brokerObj.getUser().getEmail());

			StringBuilder toFullName = new StringBuilder();
			toFullName.append(brokerObj.getUser().getFirstName()).append(" ");

			toFullName.append(brokerObj.getUser().getLastName());


			StringBuilder fromFullName = new StringBuilder();
			fromFullName.append(household.getFirstName()).append(" ");
			if(household.getMiddleName() != null){
				fromFullName.append(household.getMiddleName()).append(" ");
			}
			fromFullName.append(household.getLastName());

			noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, templateData, templateLocation, ecmfileName, BROKER,
					brokerObj.getId(), toList, fromFullName.toString(), toFullName.toString());

			opStatus = true;
			LOGGER.info("sendEmployerlDesignationStatusNotice : END");
		}catch(Exception e){
			opStatus = false;
			LOGGER.error("Error while sendEmployerlDesignationStatusNotice.", e);
		}
		return opStatus;

	}

	/**
	 * Retrieves broker details based on broker id
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param brokerId
	 *            Broker ID
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 * @throws Exception
	 */
	@RequestMapping(value = "/broker/detail", method = RequestMethod.POST)
	//@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_BROKER')")
	public String brokerDetail(@ModelAttribute(BROKER) Broker broker,
			@RequestParam(value = "id", required = false) Integer brokerId, Model model, HttpServletRequest request)
					throws GIRuntimeException {
		LOGGER.info("brokerDetail : START");
		try{
			String anonymousFlag = null;

			model.addAttribute(PAGE_TITLE, " Getinsured Health Exchange: Broker Details");

			anonymousFlag = this.getAnaonymousFlagStatus(request, false);
			LOGGER.warn("In brokerDetail method anonymousFlag: "+anonymousFlag);

			if(null == brokerId) {
				if(null != anonymousFlag && "Y".equals(anonymousFlag) && ( broker !=null && broker.getId() !=0)) {
					LOGGER.info("Populating broker ID for anonymous user.");

					brokerId = broker.getId();
				}
			}

			Validate.notNull(broker);

			LOGGER.debug("Getting Broker Object for given broker id ::");
			Broker myBroker = (broker != null && broker.getId() == 0) ? brokerService.getBrokerDetail(brokerId)
					: brokerService.getBrokerDetail(broker.getId());

			LOGGER.debug("Got the Broker Object for given broker id.");
			model.addAttribute(FILEPATH, "/broker/photo/" + broker.getId());

			String productExperties = myBroker.getProductExpertise();
			if (productExperties != null) {
				List<String> productExpertiesList = EntityUtils.splitStringValues(productExperties, ",");
				StringBuilder productExpertiesStrBuilder = new StringBuilder();
				for (String productExpertiesListStr : productExpertiesList) {
					productExpertiesStrBuilder.append(productExpertiesListStr);
					productExpertiesStrBuilder.append(",");
					productExpertiesStrBuilder.append(" ");
				}
				productExperties = productExpertiesStrBuilder.substring(ZERO, productExpertiesStrBuilder.length() - TWO);
			}
			myBroker.setProductExpertise(productExperties);

			String tmpLanSpoken= myBroker.getLanguagesSpoken();

			if(tmpLanSpoken != null){
				List<String> langList = EntityUtils.splitStringValues(tmpLanSpoken, ",");
				StringBuilder langStrBuilder = new StringBuilder();

				for (String langListStr : langList) {
					langStrBuilder.append(langListStr);
					langStrBuilder.append(",");
					langStrBuilder.append(" ");
				}
				tmpLanSpoken = langStrBuilder.substring(ZERO, langStrBuilder.length() - TWO);
			}
			myBroker.setLanguagesSpoken(tmpLanSpoken);

			model.addAttribute(BROKER, myBroker);
			model.addAttribute(BRKPAGE, request.getParameter(CURRENTPAGE));
			String isSameBroker = "false";

			try {
				if(null != anonymousFlag && "Y".equals(anonymousFlag.trim())) {
					Broker brokerDetails = brokerService.getBrokerDetail(broker.getId());
					model.addAttribute(DESIGNATED_BROKER_NAME, brokerDetails.getUser().getFullName());
					model.addAttribute("isAnonymousUser", Y);
				}
				else {
					processBrokerDetail(broker, model, request, myBroker);
				}
			}  catch (Exception ex) {
				LOGGER.error("Exception in /broker/detail :: ", ex);
				throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

			}

			if (!model.containsAttribute("isSameBroker")) {
				model.addAttribute("isSameBroker", isSameBroker);
			}

			if (!model.containsAttribute(IS_ASSISTER_DESIGNATED)) {
				model.addAttribute(IS_ASSISTER_DESIGNATED, N);
			}
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException in /broker/detail : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception in /broker/detail : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}


		LOGGER.info("brokerDetail : END");

		return "broker/detail";
	}

	/**
	 * Method to retrieve Assister designation detail by individual identifier.
	 *
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterResponseDTO The AssisterResponseDTO instance.
	 * @throws Exception
	 */
	private AssisterDesignateResponseDTO retrieveAssisterDesignationDetailByIndividualId(
			AssisterRequestDTO assisterRequestDTO) throws GIRuntimeException {

		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		LOGGER.info("REST call start to the GHIX-ENITY module to retreive Assister Designation details.");
		try{
			assisterDesignateResponseDTO = restClassCommunicator.retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);

			if(assisterDesignateResponseDTO != null && assisterDesignateResponseDTO.getResponseCode()==EntityConstants.RESPONSE_CODE_FAILURE){
				LOGGER.error("Exception occured in retrieving designatedAssister details by individual id .");
				throw new GIRuntimeException("DesignateSearchController: Exception occured in retrieving designatedAssister details.");
			}

			LOGGER.debug("REST call Ends to the GHIX-ENITY module.");
		}
		catch(Exception ex){
			LOGGER.error("Exception in retrieving designatedAssister details, REST response code is: "+
					((assisterDesignateResponseDTO!=null)?assisterDesignateResponseDTO.getResponseCode():"assisterDesignateResponseDTO is null"));
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return assisterDesignateResponseDTO;
	}



	private void processBrokerDetail(Broker broker, Model model,HttpServletRequest request, Broker myBroker) throws GIRuntimeException {

		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			String recordId = (String) request.getSession().getAttribute(
					RECORD_ID);
			LOGGER.warn("Record ID : "+recordId);
			Integer externalId = Integer.valueOf(recordId);
			DesignateBroker designateBroker = designateService.findBrokerByExternalIndividualId(externalId);

			LOGGER.debug("Got the designate Broker by externalId  :: "+ designateBroker);
			
			AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setIndividualId(Integer.valueOf(recordId));
			AssisterDesignateResponseDTO assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
			DesignateAssister designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
			boolean isAssisterDesignated = isAssisterDesignated(assisterDesignateResponseDTO);
			processDesignateBroker(designateBroker, isAssisterDesignated, assisterDesignateResponseDTO.getAssister(), model, broker.getId());
		} else {
			AccountUser user;
			try {
				user = userService.getLoggedInUser();
			} catch (InvalidUserException ex) {
				throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
			String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
			String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
			boolean switchedRole=false;
			if(isUserSwitchToOtherView != null && "y".equalsIgnoreCase(isUserSwitchToOtherView.trim())){
				switchedRole=true;
			}
			if (user.getActiveModuleName()!=null && user.getActiveModuleName().equalsIgnoreCase(RoleService.INDIVIDUAL_ROLE) && (stateExchangeType.equalsIgnoreCase(INDIVIDUAL) || stateExchangeType.equalsIgnoreCase(BOTH) )) {
				try {
					processDesignateBrokerForIndividual(model, myBroker, user,switchedRole);
				} catch(GIRuntimeException giexception){
					LOGGER.error("GIRuntimeException in /broker/detail : ", giexception);
					throw giexception;
				}
				catch (Exception ex) {
					throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
				}
			}
		}
	}

	/**
	 * If current user is individual then finds the individual details from
	 * Consumer module using rest call
	 * @param model
	 * @param myBroker
	 * @param user
	 * @param switchedRole
	 * @throws Exception
	 */
	private void processDesignateBrokerForIndividual(Model model,
			Broker myBroker, AccountUser user, boolean switchedRole) throws GIRuntimeException {
		Household household = null;
		DesignateBroker designateBroker = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		boolean isAssisterDesignated = false;

		//int userId = switchedRole?user.getActiveModuleId():user.getId();
		LOGGER.debug("User is Indivodual");
		//get household from userId
		if(user != null) {
			designateBroker = designateService.findBrokerByIndividualId(user.getActiveModuleId());
			assisterRequestDTO.setIndividualId(user.getActiveModuleId());
			assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
			isAssisterDesignated = isAssisterDesignated(assisterDesignateResponseDTO);
		}

		Validate.notNull(assisterDesignateResponseDTO);
		Validate.notNull(myBroker);

		processDesignateBroker(designateBroker, isAssisterDesignated, assisterDesignateResponseDTO.getAssister(), model, myBroker.getId());

	}

	private Household getHouseHoldByUserId(int userId){
		Household household = null;
		XStream xstream = null;
		LOGGER.debug("Rest call to get householeId starts");
		//get the household using rest call
		try{
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_USER_ID, String.valueOf(userId), String.class);
			xstream = GhixUtils.getXStreamStaxObject();
			if(response != null){
				household = (Household) xstream.fromXML(response);
			}
		}catch(Exception ex){
			LOGGER.error("Rest call to find household failed", ex);
		}
		LOGGER.debug("rest call to get householeId ends");
		return household;
	}

	/**
	 * Decides when to display 'Designate' button on broker details page.
	 *
	 * @param designateBroker
	 *            {@link DesignateBroker}
	 * @param isAssisterDesignated
	 * 			boolean isAssisterDesignated
	 * @param assisterDetails
	 * 			Assister assisterDetails
	 * @param model
	 *            Model object
	 * @param myBrokerId
	 *            Broker ID
	 * @return Model
	 */
	private Model processDesignateBroker(DesignateBroker designateBroker, boolean isAssisterDesignated, Assister assisterDetails, Model model, int myBrokerId) {

		LOGGER.info("processDesignateBroker : START");

		if (designateBroker != null) {
			if (designateBroker.getStatus().toString().equals(DesignateBroker.Status.InActive.toString()) && !isAssisterDesignated) {
				model.addAttribute(DESIGNATED_BROKER_NAME, "");
				model.addAttribute(DESIGNATED_ASSISTER_NAME, "");
			}
			else if(isAssisterDesignated){
				model.addAttribute(IS_ASSISTER_DESIGNATED, Y);
				if(assisterDetails!=null){
					model.addAttribute(DESIGNATED_ASSISTER_NAME, assisterDetails.getFirstName() + " "+ assisterDetails.getLastName());
				}
			}
			else {
				Broker brokerDetails = brokerService.getBrokerDetail(designateBroker.getBrokerId());
				if(brokerDetails != null){
					model.addAttribute("isBrokerDesignated", "Y");
					model.addAttribute(DESIGNATED_BROKER_NAME, brokerDetails.getUser().getFirstName() + " "
							+ brokerDetails.getUser().getLastName());

					if (brokerDetails.getId() == myBrokerId) {
						model.addAttribute("isSameBroker", "true");
					}
				}
			}
		}
		else if(isAssisterDesignated){
			model.addAttribute(IS_ASSISTER_DESIGNATED, Y);
			if(assisterDetails!=null){
				model.addAttribute(DESIGNATED_ASSISTER_NAME, assisterDetails.getFirstName() + " "+ assisterDetails.getLastName());
			}
		}
		else {
			model.addAttribute(DESIGNATED_BROKER_NAME, "");
		}

		LOGGER.info("processDesignateBroker : END");

		return model;
	}

	/**
	 * Function to check whether an individual is
	 * already designated to CEC or not.
	 * @param assisterDesignateResponseDTO The AssisterDesignateResponseDTO instance.
	 * @return boolean flag
	 */

	private boolean isAssisterDesignated(AssisterDesignateResponseDTO assisterDesignateResponseDTO){

		DesignateAssister designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
		if(designateAssister != null && !designateAssister.getStatus().toString().equals(DesignateAssister.Status.InActive.toString())){
			return true;
		}
		return false;
	}

	/**
	 * Handles the accept designation request for Employer / Individual
	 *
	 * @param encryptedId
	 *            ID of household/employer
	 * @param encFromModule
	 *            Employer / Individual
	 * @param prevStatus
	 *            Status page on which the control is
	 * @param request
	 * @return URL for navigation to appropriate result page
	 * @throws GIException
	 */
	@GiAudit(transactionName = "Approve", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.DESIGNATE_AGENT)
	@RequestMapping(value = "/broker/approve/{encryptedId}/{encFromModule}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_APPROVE_EMPLOYER_CONSUMER')")
	public String approve(@PathVariable("encryptedId") String encryptedId, @PathVariable("encFromModule") String encFromModule,
			@RequestParam(value = "prevStatus", required = false) String prevStatus, HttpServletRequest request) throws GIRuntimeException, AccessDeniedException {

		LOGGER.info("approve : START");

		AccountUser user = null;
		String decryptedId = null;
		String decryptedFromModule = null;
		int id;
		DesignateBroker designateBroker = null;
		Broker brokerObj = null;
		try {
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			decryptedFromModule = ghixJasyptEncrytorUtil.decryptStringByJasypt(encFromModule);
			id = Integer.parseInt(decryptedId);
			
			LOGGER.info("Individual ID : "+id);
			LOGGER.info("Module Name : "+decryptedFromModule);

			user = userService.getLoggedInUser();

			if("Y".equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW))){
				brokerObj = brokerService.getBrokerDetail(user.getActiveModuleId());
			}else{
				brokerObj = brokerService.findBrokerByUserId(user.getId());
			}
			if(BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(brokerObj)){
				throw new AccessDeniedException(ACCESS_IS_DENIED);
			}
			designateBroker = findDesignateBrokerBasedOnModule(id, decryptedFromModule);
			
			LOGGER.info("Designate Broker Record : "+designateBroker);

			if (designateBroker != null) {

				designateBroker.setStatus(Status.Active);
				designateBroker.setUpdated(new TSDate());
				designateBroker.setUpdatedBy(user.getId());

				//Enrollment update is not required for CA
				LOGGER.info("Broker approving individual. Calling Enrollment api start");
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					LOGGER.info("Invoking Update Enrollment");
					brokerMgmtUtils.updateEnrollmentDetails(brokerObj, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, user.getUsername(),
							designateBroker.getExternalIndividualId().longValue(), BrokerConstants.AGENT_ROLE, BrokerConstants.ADDACTIONFLAG );

				} else {
					brokerMgmtUtils.updateEnrollmentDetails(brokerObj, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, user.getUsername(),
							designateBroker.getIndividualId().longValue(), BrokerConstants.AGENT_ROLE, BrokerConstants.ADDACTIONFLAG );
				}
				

				designateService.saveDesignateBroker(designateBroker);
				
				//Notice should not be generated for CA
				if(ModuleUserService.INDIVIDUAL_MODULE.equalsIgnoreCase(decryptedFromModule) && !BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE) ) {
					LOGGER.info("Sending designation acceptance notification.");
					boolean opStatus = sendIndividualDesignationStatusNotice(request, brokerObj, id, ACCEPTED);

					if(opStatus) {
						LOGGER.info("Designation accptance notification sent.");
					}
					else {
						LOGGER.info("Designation accptance notification not sent.");
					}
				}
				
			}
		} catch(AccessDeniedException accessDeniedException){
			LOGGER.error("Exception occured while accepting designation request for Employer / Individual : ", accessDeniedException);
			throw accessDeniedException;
		} catch (InvalidUserException e) {
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER, e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} catch (NumberFormatException nfe){
			String errMsg = "Failed to decrypt Id. Exception is: "+nfe;
			LOGGER.error(errMsg);
			LOGGER.info("approve: END ");
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while accepting designation request for Employer/Individual : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while accepting designation request for Employer / Individual : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		request.setAttribute(DESIG_STATUS, prevStatus);
		LOGGER.info("approve: END");

		return redirectBasedOnModule(decryptedFromModule, prevStatus);

	}
	/**
	 * Method to send designation status notice of Individual- Agent designation interaction.
	 *
	 * @param user The current User instance.
	 * @param consumerId The current consumer identifier.
	 * @param designationStatus The associated designation status.
	 * @return opStatus The current operation status.
	 */
	private boolean sendIndividualDesignationStatusNotice(HttpServletRequest request, Broker currBroker, int consumerId, String designationStatus) {
		LOGGER.info("sendIndividualAgentDesignationStatusNotice : START");
		boolean opStatus = false;
		String noticeTemplateName = null;
		StringBuilder individualAddress = new StringBuilder();
		Map<String, Object> consumerAPIRequestMap = new LinkedHashMap<String, Object>();
		Map<String, Object> templateData = new HashMap<String, Object>();
		ConsumerResponse individualResponse = null;
		Household currIndividual =  null;
		XStream xstream = GhixUtils.getXStreamStaxObject();

		try {
			Validate.notNull(currBroker, "No broker record for current user found in repository.");

			if(ACCEPTED.equalsIgnoreCase(designationStatus)) {
				noticeTemplateName = "Individual-Agent Designation Accepted Notice";

				templateData.put("uRLIndividualPortalAgentInformation", GhixEndPoints.GHIXWEB_SERVICE_URL + "broker/viewbrokerprofile/"+ ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(currBroker.getId())));
			}
			else {
				noticeTemplateName = "Individual-Agent Designation Declined Notice";

				templateData.put("uRLIndividualPortalLogIn", GhixEndPoints.GHIXWEB_SERVICE_URL + "account/user/login");
			}

			LOGGER.debug("Rest call to get householeId starts");
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, String.valueOf(consumerId), String.class);

			if(StringUtils.isNotBlank(response)) {
				individualResponse = (ConsumerResponse) xstream.fromXML(response);

				Validate.notNull(individualResponse);

				currIndividual = individualResponse.getHousehold();
			}
			else {
				LOGGER.warn("Invalid ReST response received when finding consumer by identifier.");
			}

			LOGGER.debug("rest call to get householeId ends");

			Validate.notNull(currIndividual, "No individual record found in repository.");

			Location individualLocation = currIndividual.getLocation();

			if(null != individualLocation) {
				individualAddress.append(individualLocation.getAddress1()).append(", <br/>");

				if(StringUtils.isNotBlank(individualLocation.getAddress2())) {
					individualAddress.append(individualLocation.getAddress2()).append(", <br/>");
				}

				if(StringUtils.isNotBlank(individualLocation.getCounty())) {
					individualAddress.append(individualLocation.getCounty()).append(", <br/>");
				}

				individualAddress.append(individualLocation.getState()).append(", ");
				individualAddress.append(individualLocation.getZip()).append(", <br/> <br/>");
			}

			consumerAPIRequestMap.put(BrokerConstants.MODULE_NAME, ModuleUserService.BROKER_MODULE);
			consumerAPIRequestMap.put(BrokerConstants.STATUS, BrokerConstants.STATUS_PENDING);
			consumerAPIRequestMap.put(BrokerConstants.MODULE_ID, currBroker.getId());
			consumerAPIRequestMap.put(BrokerConstants.SENDER_OBJ, currBroker);
			consumerAPIRequestMap.put(BrokerConstants.INDIVIDUAL_ID, consumerId);


			SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
			templateData.put("notificationDate", sdf.format(new TSDate()));

			templateData.put("agentFN", currBroker.getUser().getFirstName());
			templateData.put("agentLN", currBroker.getUser().getLastName());
			templateData.put("agentBusinessName", currBroker.getCompanyName());

			templateData.put("individualFN", currIndividual.getFirstName());
			templateData.put("individualMN", (null == currIndividual.getMiddleName())? StringUtils.EMPTY: currIndividual.getMiddleName());
			templateData.put("individualLN", currIndividual.getLastName());
			templateData.put("addressContent", individualAddress);
			templateData.put("designationStatus", designationStatus);

			templateData.put("exchangeName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			templateData.put("exchangePhone", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));

			templateData.put(TemplateTokens.HOST, GhixEndPoints.GHIXWEB_SERVICE_URL);
			templateData.put("exchangeFullName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));

			templateData.put("exchangeAddress1", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			templateData.put("cityName" , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
			templateData.put("stateName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
			templateData.put("countryName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
			templateData.put("pinCode" , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
			templateData.put("exchangeURL", GhixEndPoints.GHIXWEB_SERVICE_URL);

			Map<String, String> templateDataModified = getTemplateDataMap(templateData);

			NoticeType noticeType = noticeTypeRepo.findByNameAndLanguage(noticeTemplateName, GhixLanguage.US_EN);

			Validate.notNull(noticeType, "NoticeType not found for the template in repository.");

			String fileName = noticeTemplateName + TimeShifterUtil.currentTimeMillis() + ".pdf";

			PreferencesDTO  preferencesDTO  =  preferencesService.getPreferences(currIndividual.getId(), false);
			List<String> toList = new LinkedList<String>();
			toList.add(preferencesDTO.getEmailAddress());
			Location location = brokerMgmtUtils.getLocationFromDTO(preferencesDTO.getLocationDto());
			
			StringBuilder toFullName = new StringBuilder();
			toFullName.append(currIndividual.getFirstName()).append(" ");

			if(null != currIndividual.getMiddleName()) {
				toFullName.append(currIndividual.getMiddleName()).append(" ");
			}

			toFullName.append(currIndividual.getLastName());

			noticeService.createModuleNotice(noticeType.getEmailClass(), GhixLanguage.US_EN, templateData, "AgentIndividualDesignationStatus", fileName, INDIVIDUAL.toLowerCase(),
					currIndividual.getId(), toList, (currBroker.getUser().getFullName()), toFullName.toString(),location,preferencesDTO.getPrefCommunication());

			opStatus = true;
		}
		catch(Exception ex){
			opStatus = false;
			LOGGER.error("Rest call to find household failed.", ex);
		}
		finally {
			LOGGER.info("sendIndividualAgentDesignationStatusNotice : END");
		}

		return opStatus;
	}

	private Map<String, String> getTemplateDataMap(Map<String, Object> templateData) {
		Map<String, String> templateDataModified = new HashMap<String, String>();
		Iterator<Entry<String, Object>> it = templateData.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			if(pairs.getValue() != null) {
				templateDataModified.put(pairs.getKey(), pairs.getValue().toString());
			}
		}
		return templateDataModified;
	}


	private String getTemplateContentWithTokensReplaced(Map<String, Object> tokens, String location) {
		InputStream inputStreamUrl = null;
		String templateContent = null;
		Template tmpl = null;
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		String responseData = null;

		try {
			Resource resource = appContext.getResource("classpath:"+location);
			inputStreamUrl = resource.getInputStream();
			templateContent = IOUtils.toString(inputStreamUrl, "UTF-8");
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
			responseData = sw.toString();
		}
		catch (Exception e) {
			LOGGER.error("Exception occurred." , e);
		}
		finally {
			IOUtils.closeQuietly(inputStreamUrl);
			IOUtils.closeQuietly(sw);
		}

		return responseData;
	}
	/**
	 * Handle the decline designation request for Employers / Individuals.
	 *
	 * @param encDesigId
	 *            ID of the designated user
	 * @param encFromModule
	 *            Employer / Individual
	 * @param desigStatus
	 *            Designation Status
	 * @param request
	 * @return URL for navigation to appropriate result page
	 */
	@GiAudit(transactionName = "Decline", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.DEDESIGNATE_AGENT)
	@RequestMapping(value = "/broker/decline/{desigId}/{fromModule}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_DECLINE_EMPLOYER_CONSUMER')")
	public String decline(@PathVariable("desigId") String encDesigId, @PathVariable(FROM_MODULE) String encFromModule,
			@RequestParam(value = "prevStatus", required = false) String desigStatus, HttpServletRequest request) throws GIRuntimeException{

		LOGGER.info("decline : START");
		AccountUser user = null;
		String decryptedId = null;
		String fromModule = null;
		int desigId;
		try {
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encDesigId);
			fromModule = ghixJasyptEncrytorUtil.decryptStringByJasypt(encFromModule);
			desigId = Integer.parseInt(decryptedId);
			user = userService.getLoggedInUser();


			Broker broker = null;
			if("Y".equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW))){
				broker = brokerService.getBrokerDetail(user.getActiveModuleId());
			}else{
				broker = brokerService.findBrokerByUserId(user.getId());
			}
			if(BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(broker)){
				throw new AccessDeniedException(ACCESS_IS_DENIED);
			}

			DesignateBroker designateBroker = findDesignateBrokerBasedOnModule(desigId, fromModule);
			Map<String, Object> brokerTemplateData = new HashMap<String, Object>();
			if (designateBroker != null) {

				designateBroker.setStatus(Status.InActive);
				designateBroker.setUpdated(new TSDate());
				designateBroker.setUpdatedBy(user.getId());

				designateService.saveDesignateBroker(designateBroker);

				if(null != desigStatus && desigStatus.equals(PENDING) && ModuleUserService.INDIVIDUAL_MODULE.contains(fromModule) && !BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)) {

					LOGGER.info("Sending designation decline notification.");
					boolean opStatus = sendIndividualDesignationStatusNotice(request, broker, desigId, "denied");

					if(opStatus) {
						LOGGER.info("Designation decline notification sent.");
					}
					else {
						LOGGER.info("Designation decline notification not sent.");
					}
				} else if(!BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){

					//get the notification template
					brokerTemplateData = brokerMgmtUtils.createTemplateMap(broker);

					// Get the record of Individual associated with this agent
					//Send the notification to this individual.
					Map<String, Object> searchCriteriaMap = new HashMap<String, Object>();
					searchCriteriaMap.put(BrokerConstants.MODULE_NAME, BrokerConstants.SENDER_BROKER_MODULE);
					searchCriteriaMap.put(BrokerConstants.STATUS, BrokerConstants.STATUS_INACTIVE);
					searchCriteriaMap.put(BrokerConstants.SENDER_OBJ, broker);
					searchCriteriaMap.put(BrokerConstants.MODULE_ID, broker.getId());
					searchCriteriaMap.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf(designateBroker.getIndividualId()));
					String noticeTemplateName = BrokerConstants.INDIVIDUAL_NOTIFICATION_TEMPLATE_NAME;

					// Send the notifications to all these individuals
					brokerMgmtUtils.sendNotificationsToIndividuals(searchCriteriaMap, noticeTemplateName, brokerTemplateData);
				}
			}

		} catch(AccessDeniedException accessDeniedException){
			LOGGER.error("Exception occured while handling the decline designation request for Employers / Individuals : ", accessDeniedException);
			throw accessDeniedException;
		} catch (InvalidUserException ex) {
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER, ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} catch(NumberFormatException nfe){
			String errMsg = "Error in decrypting desigId, " + EXCEPTION_IS +nfe;
			LOGGER.error("Exception occurred while declining", errMsg);
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while handling the decline designation request for Employers/Individuals : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while handling the decline designation request for Employers / Individuals : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("decline : END");

		return redirectBasedOnModule(fromModule, desigStatus);
	}

	/**
	 * Handles accept bulk requests for Employers / Individuals.
	 *
	 * @param ids
	 *            {@link List} of designation ids
	 * @param fromModule
	 *            Employer / Individual
	 * @param desigStatus
	 *            Designation status
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/approveRequests/{fromModule}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_APPROVE_EMPLOYER_CONSUMER')")
	public String approveRequests(@RequestParam("ids") String ids, @PathVariable(FROM_MODULE) String fromModule,
			@RequestParam(value = DESIG_STATUS, required = false) String desigStatus) {

		LOGGER.info("approveRequests : START");
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException ex) {
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER, ex);
		}

		Validate.notNull(user);

		//check if broker is not in certified state
		Broker broker = brokerService.findBrokerByUserId(user.getId());
		if(BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(broker)) {
			throw new AccessDeniedException(ACCESS_IS_DENIED);
		}
		if (ids != null && !"".equals(ids)) {
			List<String> items = null;
			if (ids != null && ids.contains(",")) {
				items = Arrays.asList(ids.split("\\s*,\\s*"));
				if (items != null && !items.isEmpty()) {

						for (int i = 0; i < items.size(); i++) {
							DesignateBroker designateBroker = findDesignateBrokerBasedOnModule(
									Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(items.get(i))), fromModule);
							if (designateBroker != null) {
								designateBroker.setStatus(Status.Active);
								designateBroker.setUpdated(new TSDate());
								designateBroker.setUpdatedBy(user.getId());
								try
								{
									designateService.saveDesignateBroker(designateBroker);
								} catch(GIRuntimeException giexception) {
									LOGGER.error("Exception ocurred while designating Individual.", giexception);
						            throw giexception;
								}
							}
						}
				}
			}
		}
		LOGGER.info("approveRequests : END");

		return redirectBasedOnModule(fromModule, desigStatus);
	}

	/**
	 * Handles decline bulk of requests for Employers / Individuals.
	 *
	 * @param ids
	 *            List of employer/individual ids
	 * @param fromModule
	 *            From which page the control is coming
	 * @param desigStatus
	 *            value of which status page is opened
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/declineRequests/{fromModule}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_DECLINE_EMPLOYER_CONSUMER')")
	public String declineRequests(@RequestParam("ids") String ids,
			@RequestParam(value = "prevStatus", required = false) String desigStatus,
			@PathVariable(FROM_MODULE) String fromModule) throws GIRuntimeException {

		LOGGER.info("declineRequests : START");

		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
		} catch (InvalidUserException ex) {
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER, ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		//check if broker is not in certified state
		Broker broker = brokerService.findBrokerByUserId(user.getId());
		if(BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(broker)){
			throw new AccessDeniedException(ACCESS_IS_DENIED);
		}
		if (ids != null && !"".equals(ids)) {
			List<String> items = null;
			if (ids != null && ids.contains(",")) {
				items = Arrays.asList(ids.split("\\s*,\\s*"));
				if (items != null && !items.isEmpty()) {

						for (int i = 0; i < items.size(); i++) {
							DesignateBroker designateBroker = findDesignateBrokerBasedOnModule(
									Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(items.get(i))), fromModule);
							if (designateBroker != null) {
								designateBroker.setStatus(Status.InActive);
								designateBroker.setUpdated(new TSDate());
								designateBroker.setUpdatedBy(user.getId());
								try
								{
									designateService.saveDesignateBroker(designateBroker);
								}
								catch(GIRuntimeException giexception){
									LOGGER.error("Exception ocurred while designating Individual.", giexception);
									throw giexception;
								}
							}
						}

				}
			}
		}

		LOGGER.info("declineRequests : END");

		return redirectBasedOnModule(fromModule, desigStatus);
	}

	/**
	 * Redirects request to page from which module it has come.
	 *
	 * @param fromModule
	 *            From which page the control is coming
	 * @param prevStatus
	 *            value of which status page is opened
	 * @return URL for navigation to appropriate result page
	 */
	private String redirectBasedOnModule(String fromModule, String prevStatus) {

		LOGGER.info("redirectBasedOnModule : START");

		if (prevStatus == null || "".equals(prevStatus)) {
			prevStatus = PENDING;
		}
		if (fromModule != null && !"".equals(fromModule) && fromModule.equalsIgnoreCase(EMPLOYERS_MODULE)) {
			LOGGER.info("redirectBasedOnModule : END");
			return REDIRECT_EMPLOYERS + prevStatus;
		}

		LOGGER.info("redirectBasedOnModule : END");

		return REDIRECT_INDIVIDUALS + prevStatus;
	}

	/**
	 * Retrieves {@link DesignateBroker} based on module from where it is
	 * called.
	 *
	 * @param desigId
	 *            Employer/Individual id
	 * @param fromModule
	 *            From which page the control is coming
	 * @return {@link DesignateBroker}
	 */
	private DesignateBroker findDesignateBrokerBasedOnModule(Integer desigId, String fromModule) {

		LOGGER.info("findDesignateBrokerBasedOnModule : START");

		DesignateBroker designateBroker = null;
		if (fromModule != null && !"".equals(fromModule)) {
			if (fromModule.equalsIgnoreCase(EMPLOYERS_MODULE)) {
				designateBroker = designateService.findBrokerByEmployerId(desigId);
			} else if (fromModule.equalsIgnoreCase(INDIVIDUALS_MODULE)) {
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					designateBroker = designateService.findBrokerByExternalIndividualId(desigId);
				}else{
					designateBroker = designateService.findBrokerByIndividualId(desigId);
				}
			}
		}

		LOGGER.info("findDesignateBrokerBasedOnModule : END");

		return designateBroker;
	}

	/**
	 * Redirects to confirm decline pop up
	 *
	 * @param encyptedDesigId
	 *            Employer/Individual id
	 * @param desigName
	 * @param desigStatus
	 *            value of which status page is opened
	 * @param encFromModule
	 *            From which page the control is coming
	 * @param broker
	 *            {@link Broker} object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@GiAudit(transactionName = "Decline Popup", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.DEDESIGNATE_AGENT)
	@RequestMapping(value = "/broker/declinePopup", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_DECLINE_EMPLOYER_CONSUMER')")
	public String declinePopup(@RequestParam("desigId") String encyptedDesigId,
			@RequestParam(value = "desigName", required = false) String desigName,
			@RequestParam(value = "prevStatus", required = false) String desigStatus,
			@RequestParam(value = "fromModule", required = false) String encFromModule,
			@ModelAttribute(BROKER) Broker broker , Model model, HttpServletRequest request) throws GIRuntimeException {


		LOGGER.info("declinePopup : START");
		try{
			//String decryptedFromModule = null;
			/*String desigId = null;

		desigId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encyptedDesigId);
		//decryptedFromModule = ghixJasyptEncrytorUtil.decryptStringByJasypt(encFromModule);

		if (desigId != null && desigId.contains(",")) {
			int lastIdx = desigId.length() - 1;
			char last = desigId.charAt(lastIdx);
			if (last == ',') {
				desigId = desigId.substring(0, desigId.length() - 1);
			}
		}		*/

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				model.addAttribute("firstName",request.getParameter("firstName"));
				model.addAttribute("lastName",request.getParameter("lastName"));
			}
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			request.setAttribute("desigId", encyptedDesigId);
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while redirecting to confirm decline pop up : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while redirecting to confirm decline pop up : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("declinePopup : END");

		return "broker/declinePopup";
	}

	/**
	 * @param firstName
	 *            First Name of Employer / Individual
	 * @param lastName
	 *            Last Name of Employer / Individual
	 * @param brokerId
	 *            Id of Broker
	 * @param broker
	 *            {@link Broker} object
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation to appropriate result page
	 */
	@GiAudit(transactionName = "Dedesignate Popup", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.DEDESIGNATE_AGENT)
	@RequestMapping(value = "/broker/dedesignatePopup", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_DE_DESIGNATE_EMPLOYER')")
	public String dedesignatePopup(@RequestParam(BROKER_ID) String encBrokerId, @ModelAttribute(BROKER) Broker broker, Model model) throws GIRuntimeException {

		LOGGER.info("dedesignatePopup : START");
		String brokerId = null;
		String firstName =null;
		String lastName = null;

		try{
			brokerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);
			Broker brokerFromDB = brokerService.getBrokerDetail( Integer.parseInt(brokerId) );
			
			if(brokerFromDB.getUser() == null) {
				firstName = brokerFromDB.getFirstName();
				lastName = brokerFromDB.getLastName();
			}else {
				firstName = brokerFromDB.getUser().getFirstName();
				lastName = brokerFromDB.getUser().getLastName();
			}
			
			model.addAttribute("brokerName", firstName + ' ' + lastName);
			model.addAttribute(BROKER_ID, brokerId);
		}catch(NumberFormatException nfe){
			String errMsg = "Error occured in decrypting broker id. " + EXCEPTION_IS +nfe;
			LOGGER.error("dedesignatePopup - "+errMsg);
			throw new GIRuntimeException(nfe, ExceptionUtils.getFullStackTrace(nfe), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}catch(GIRuntimeException giexception){
			LOGGER.error("Exception occured while redirecting to confirm decline pop up in dedesignatePopup method: ", giexception);
			throw giexception;
		}catch(Exception exception){
			LOGGER.error("Exception occured while displaying de-designate pop up : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("dedesignatePopup : END");

		return "broker/dedesignatePopup";
	}



	/**
	 * To reset all search criteria
	 * @param desigStatus String designation status
	 * @param model {@link Model}
	 * @param request {@link HttpServletRequest}
	 * @return URL for navigation to employers page
	 */
	//@RequestMapping(value = "/broker/employers/resetall", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_EDIT_BROKER')")
	public String resetAll(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model,
			HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("resetAll : START");
		try {
			model.addAttribute(DESIG_STATUS, desigStatus == null ? Status.Pending.toString() : desigStatus);
		} catch(GIRuntimeException giruntimeexception){
			LOGGER.error("GIRuntimeException occured while retreiving list of designated employers : ", giruntimeexception);
			throw giruntimeexception;
		}catch (Exception exception) {
			LOGGER.error(ERROR_OCCURED_IN_RESETTING_SEARCH_CRITERIA, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("resetAll : END");

		return "redirect:/broker/employers";
	}

	/**
	 * Method to reset all employee's search criteria
	 * @param desigStatus String designation status
	 * @param model {@link Model}
	 * @param request {@link HttpServletRequest}
	 * @return URL for navigation to employers page
	 */
	//@RequestMapping(value = "/broker/employees/resetall", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_EMPLOYEE')")
	public String resetAllEmployeesSearchCriteria(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model,
			HttpServletRequest request) {

		LOGGER.info("resetAll : START");
		try {
			model.addAttribute(DESIG_STATUS, desigStatus == null ? Status.Pending.toString() : desigStatus);
		} catch(GIRuntimeException giruntimeexception){
			LOGGER.error("GIRuntimeException occured in resetting search criteria in resetAllEmployeesSearchCriteria", giruntimeexception);
			throw giruntimeexception;
		}catch (Exception exception) {
			LOGGER.error(ERROR_OCCURED_IN_RESETTING_SEARCH_CRITERIA, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("resetAll : END");

		return "redirect:/broker/employees";
	}

	/**
	 * Method to reset all Individual's search criteria
	 * @param desigStatus String designation status
	 * @param model {@link Model}
	 * @param request {@link HttpServletRequest}
	 * @return URL for navigation to employers page
	 */
	@RequestMapping(value = "/broker/individuals/resetall", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	public String resetAllIndividualsSearchCriteria(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model,
			HttpServletRequest request) {

		LOGGER.info("Individuals resetAll Search Criteria : START");
		try {
			 if(desigStatus != null && !desigStatus.equalsIgnoreCase("Pending") && !desigStatus.equalsIgnoreCase("InActive")){
					throw new GIRuntimeException("Invalid Designation Status");
			 }
			model.addAttribute(DESIG_STATUS, desigStatus == null ? Status.Pending.toString() : desigStatus);
		} catch(GIRuntimeException giruntimeexception){
			LOGGER.error(ERROR_OCCURED_IN_RESETTING_SEARCH_CRITERIA, giruntimeexception);
			throw giruntimeexception;
		}catch (Exception exception) {
			LOGGER.error(ERROR_OCCURED_IN_RESETTING_SEARCH_CRITERIA, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("Individuals resetAll Search Criteria : END");

		return "redirect:/broker/individuals";
	}

	/**
	 * Retrieves list of designated individuals
	 *
	 * @param desigStatus
	 *            Status of designated individual to search for
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/individuals", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	@GiAudit(transactionName = "Invalid Designation Status",
	 eventType = EventTypeEnum.BROKER,
	 eventName = EventNameEnum.EE_AGENT)
	public String getIndividuals(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model,
			HttpServletRequest request) throws GIRuntimeException{

		LOGGER.info("getIndividuals : START");

		try{
			Map<String, Object> consumerAPIRequestMap = new HashMap<String, Object>();
			BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO = new BrokerBOBSearchParamsDTO();
			List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
			Integer iResultCt = null;
			int iResultCount = -1;
			String tableTitle = null;
			Integer pageSize = PAGE_SIZE;
			Integer startRecord = 0;
			int currentPage = 1;
			Broker brokerObj = new Broker();
			request.setAttribute(PAGE_SIZE2, pageSize);
			request.setAttribute(REQ_URI, INDIVIDUALS);

			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individuals");
			model.addAttribute(BrokerConstants.ELIGIBILITY_STATUS_LIST,  EligibilityStatus.values());
			model.addAttribute(BrokerConstants.APPLICATION_STATUS_LIST,  ApplicationStatus.values());

			String param = request.getParameter(PAGE_NUMBER);
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1) * pageSize;
				currentPage = Integer.parseInt(param);
			}
			AccountUser user = null;
			
			try {
				if(StringUtils.isBlank(desigStatus)){
					return "broker/errorPage";
				}else if(!desigStatus.equalsIgnoreCase("Pending") && !desigStatus.equalsIgnoreCase("InActive")){
					return "broker/errorPage";
				}
				
				desigStatus = XssHelper.stripXSS(desigStatus);
				user = userService.getLoggedInUser();
				String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
				if(isUserSwitchToOtherView != null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)){
					brokerObj = brokerService.getBrokerDetail(userService.getLoggedInUser().getActiveModuleId());
				}else{
					brokerObj = brokerService.findBrokerByUserId(user.getId());
				}

				if(BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(brokerObj)) {
					throw new AccessDeniedException(ACCESS_IS_DENIED);
				}
				
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE) && user.getActiveModuleName().equalsIgnoreCase(RoleService.BROKER_ROLE) && BrokerMgmtUtils.isBrokerStatusInActive(brokerObj)){
					throw new AccessDeniedException(ACCESS_IS_DENIED);
				}

				if (brokerObj == null) {
					brokerObj = new Broker();
					brokerObj.setUser(user);
				}
			} catch (InvalidUserException ex) {
				LOGGER.error("Exception occurred while searching for Broker by user id.", ex);
				throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

			}

			Map<String, Object> searchCriteriaMap = this.createSearchCriteriaForIndividuals(request, brokerObj.getId(), desigStatus,brokerBOBSearchParamsDTO);

			Map<String, Object> houseHoldMap = null;
			String previousSortBy = request.getParameter("previousSortBy");
			String previousSortOrder = request.getParameter("previousSortOrder");
			boolean isSortChanged = false;
			
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				List<Long> individualsId = null;
				brokerBOBSearchParamsDTO.setDesignationStatus(desigStatus);
				brokerBOBSearchParamsDTO.setPageNumber(currentPage);
				List<Long> nextIndividualIds = null; 
				
				//sort IND72 feature
				if(brokerBOBSearchParamsDTO !=null && previousSortBy == null && previousSortOrder == null){
					previousSortBy = brokerBOBSearchParamsDTO.getSortBy();
					previousSortOrder = brokerBOBSearchParamsDTO.getSortOder();
				}else if(brokerBOBSearchParamsDTO!=null && brokerBOBSearchParamsDTO.getSortBy() != null && brokerBOBSearchParamsDTO.getSortOder() != null 
						&&  !brokerBOBSearchParamsDTO.getSortBy().equalsIgnoreCase(previousSortBy) &&  !brokerBOBSearchParamsDTO.getSortOder().equalsIgnoreCase(previousSortOrder)){
					isSortChanged = true;
					currentPage = 1;
					brokerBOBSearchParamsDTO.setPageNumber(currentPage);
				}
				
				if((currentPage%PAGE_SIZE) !=1 && request.getParameterValues("individualsIdAtClient") !=null && request.getParameter("landingFromPrev") == null){
					nextIndividualIds = new ArrayList<Long>();
					individualsId = new ArrayList<Long>();
					for(String tmpStrId:request.getParameterValues("individualsIdAtClient")){
						individualsId.add(Long.parseLong(tmpStrId));
					}
					iResultCount = request.getParameter("totalCountAtClient")!= null ? !request.getParameter("totalCountAtClient").isEmpty()? Integer.parseInt(request.getParameter("totalCountAtClient")) :0:0;   
					if(individualsId.size() > 0 && !isSortChanged){
						int size = individualsId.size();
						if(size <= PAGE_SIZE){
							nextIndividualIds = individualsId;
						} else if(size > ((currentPage-1)%PAGE_SIZE)  *PAGE_SIZE){
							nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, ((currentPage-1)%PAGE_SIZE) * PAGE_SIZE);
						}else{
							nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, individualsId.size());
						}
						brokerBOBSearchParamsDTO.setIndividualIds(nextIndividualIds);	
					}
					
				}else if(request.getParameter("landingFromPrev") != null && "true".equalsIgnoreCase( request.getParameter("landingFromPrev") )){
					brokerBOBSearchParamsDTO.setPageNumber(currentPage-PAGE_SIZE+1);
				}
				
				BrokerBobResponseDTO brokerBobResponseDTO =	brokerIndTriggerService.triggerInd72(brokerBOBSearchParamsDTO);
				
				if(request.getParameter("landingFromPrev") != null && "true".equalsIgnoreCase( request.getParameter("landingFromPrev") ) && brokerBobResponseDTO!=null &&
						brokerBobResponseDTO.getIndividualsId() !=null && brokerBobResponseDTO.getIndividualsId().size() > 0){
					
					brokerBOBSearchParamsDTO.setPageNumber(currentPage);
					 
					individualsId = brokerBobResponseDTO.getIndividualsId();
					iResultCount = brokerBobResponseDTO.getCount();
					int size = individualsId.size();
					if(size <= PAGE_SIZE){
						nextIndividualIds = individualsId;
					} else if(size > ((currentPage-1)%PAGE_SIZE)  *PAGE_SIZE){
						nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, ((currentPage-1)%PAGE_SIZE) * PAGE_SIZE);
					}else{
						nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, individualsId.size());
					}
					 
					brokerBOBSearchParamsDTO.setIndividualIds(nextIndividualIds);
					
					brokerBobResponseDTO =	brokerIndTriggerService.triggerInd72(brokerBOBSearchParamsDTO);
				}
				
				if(brokerBobResponseDTO!=null){
					List<BrokerBOBDetailsDTO> brokerBOBDetailsDTO = brokerBobResponseDTO.getBrokerBobDetailsDTO();
					
					if( brokerBobResponseDTO.getIndividualsId() !=null && brokerBobResponseDTO.getIndividualsId().size() > 0){
						individualsId = brokerBobResponseDTO.getIndividualsId();
					}
					
					if( (currentPage%PAGE_SIZE)==1){
						iResultCount = brokerBobResponseDTO.getCount();
					}
					
					if(null != individualsId && individualsId.size() > 0){
						model.addAttribute("individualsId", individualsId);
					}
					
					houseHoldObjList = getConsumerCompositList(brokerBOBDetailsDTO) ;
				}
				
				if(iResultCount >1){
					tableTitle = setTableTitle(desigStatus, "Requests");
				}
				else{
					tableTitle = setTableTitle(desigStatus, "Request");
				}
				
			}else{
				
				searchCriteriaMap.put(BrokerConstants.START_PAGE, startRecord);
				searchCriteriaMap.put(STATUS, desigStatus);
				consumerAPIRequestMap.put(SEARCH_CRITERIA, searchCriteriaMap);
				
				houseHoldMap = brokerMgmtUtils.retrieveConsumerList(consumerAPIRequestMap);
				
				if(houseHoldMap!=null && !houseHoldMap.isEmpty()){
					houseHoldObjList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
					iResultCt = (Integer) Integer.parseInt((String)houseHoldMap.get(BrokerConstants.TOTAL_RECORD_COUNT));
				}

				if (iResultCt != null) {
					iResultCount = iResultCt.intValue();
				}

				if(iResultCount >1){
					tableTitle = setTableTitle(desigStatus, INDIVIDUALS2);
				}
				else{
					tableTitle = setTableTitle(desigStatus, INDIVIDUAL);
				}
			}
			
			model.addAttribute("previousSortOrder", previousSortOrder);
			model.addAttribute("previousSortBy", previousSortBy);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			model.addAttribute("ID_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE));
			model.addAttribute("NV_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE));
			model.addAttribute(RESULT_SIZE, iResultCount);
			model.addAttribute(BrokerConstants.HOUSEHOLDLIST,houseHoldObjList);
			model.addAttribute(BrokerConstants.SORT_ORDER, (String) searchCriteriaMap.get(BrokerConstants.SORT_ORDER));
			model.addAttribute(TABLE_TITLE, tableTitle);
			model.addAttribute(CURRENT_PAGE, currentPage);
			model.addAttribute(DESIG_STATUS, desigStatus == null ? Status.Pending.toString() : desigStatus);
			model.addAttribute(SEARCH_CRITERIA, searchCriteriaMap);
			model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		} catch(AccessDeniedException accessDeniedException){
			LOGGER.error("Exception occured while retrieving list of designated individuals in getIndividuals method: ", accessDeniedException);
			throw accessDeniedException;
		} catch(GIRuntimeException giexception){
			LOGGER.error("Exception occured while retrieving list of designated individuals in getIndividuals method: ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving list of designated individuals : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getIndividuals : END");


			return "broker/individuals";

	}

	private List<ConsumerComposite> getConsumerCompositList(List<BrokerBOBDetailsDTO> brokerBOBDetailsDTO) {
		List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");//BrokerConstants.DATE_FORMAT  TODO: Need to get confirmation from AHBX about date format.
		ConsumerComposite tmpConsumerComposite=null;
		
		if(brokerBOBDetailsDTO!=null){
			for(BrokerBOBDetailsDTO tmpBrokerBOBDetailsDTO:brokerBOBDetailsDTO){
				tmpConsumerComposite= new ConsumerComposite();
				
				tmpConsumerComposite.setId(tmpBrokerBOBDetailsDTO.getIndividualId());
				
				tmpConsumerComposite.setFirstName(tmpBrokerBOBDetailsDTO.getFirstName());
				tmpConsumerComposite.setLastName(tmpBrokerBOBDetailsDTO.getLastName());
				
				tmpConsumerComposite.setEmailAddress(tmpBrokerBOBDetailsDTO.getEmailAddress());
				tmpConsumerComposite.setPhoneNumber(tmpBrokerBOBDetailsDTO.getPhoneNumber());
				
				tmpConsumerComposite.setAddress1(tmpBrokerBOBDetailsDTO.getAddress1());
				tmpConsumerComposite.setAddress2(tmpBrokerBOBDetailsDTO.getAddress2());
				tmpConsumerComposite.setCity(tmpBrokerBOBDetailsDTO.getCity());
				tmpConsumerComposite.setState(tmpBrokerBOBDetailsDTO.getState());
				tmpConsumerComposite.setZip(tmpBrokerBOBDetailsDTO.getZipCode());
				 
				tmpConsumerComposite.setApplicationStatus(tmpBrokerBOBDetailsDTO.getApplicationStatus());
				tmpConsumerComposite.setEligibilityStatus(tmpBrokerBOBDetailsDTO.getEligibilityStatus());
				
				tmpConsumerComposite.setFamilySize(tmpBrokerBOBDetailsDTO.getNoOfHouseholdMembers()); 
				
				if(null != tmpBrokerBOBDetailsDTO.getRequestSentDt() && !tmpBrokerBOBDetailsDTO.getRequestSentDt().isEmpty()){
					try {
						tmpConsumerComposite.setRequestSent(  sdf.parse(tmpBrokerBOBDetailsDTO.getRequestSentDt())  );//??? What will be date format so we can parse it to Date object
					} catch (ParseException e) {
						LOGGER.error("Exception occured while parsing RequestSentDt from IND72 response: ", e);
					} 				
				}
				
				if(null!= tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt() && !tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt().isEmpty() ){
					try {
						tmpConsumerComposite.setInActiveSince(sdf.parse(tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt()));//??? What will be date format so we can parse it to Date object
					} catch (ParseException e) {
						LOGGER.error("Exception occured while parsing InActiveSince from IND72 response: ", e);
					}
				}
				
				tmpConsumerComposite.setCecFirstName(tmpBrokerBOBDetailsDTO.getEcFirstName());
				tmpConsumerComposite.setCecLastName(tmpBrokerBOBDetailsDTO.getEcLastName());
				
				houseHoldObjList.add(tmpConsumerComposite);
			}
		}
		
		
		return houseHoldObjList;
	}


	/**
	 * Method to determine the Sort Order for Criteria.
	 *
	 * @param request The HttpServletRequest instance.
	 * @return sortOrder The sort order value.
	 */
	private String determineSortOrderForCriteria(HttpServletRequest request) {
		String sortOrder =  (request.getParameter(BrokerConstants.SORT_ORDER) == null) ?  BrokerConstants.ASC : request.getParameter(BrokerConstants.SORT_ORDER);
		sortOrder = ("".equals(sortOrder)) ? BrokerConstants.ASC : sortOrder;
		String changeOrder =  (request.getParameter(BrokerConstants.CHANGE_ORDER) == null) ?  BrokerConstants.FALSE : request.getParameter(BrokerConstants.CHANGE_ORDER);
		sortOrder = (changeOrder.equalsIgnoreCase(TRUE) ) ? (sortOrder.equals(BrokerConstants.DESC) ? BrokerConstants.ASC : BrokerConstants.DESC ): sortOrder;
		return sortOrder;
	}

	/**
	 * Retrieves list of designated employees on employee list page for broker
	 *
	 * @param desigStatus
	 *            Status of designated employee to search for
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	//@RequestMapping(value = "broker/employees", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_EMPLOYEE')")
	public String getEmployees(@RequestParam(value = DESIG_STATUS, required = false, defaultValue="Active") String desigStatus, Model model,
			HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("getEmployees : START");

		/*
		 * When is this method called?
		 * This Method is called while getting a list of 'Active' employees for broker/agent : http://localhost:8080/hix/broker/employees?desigStatus=Active
		 * This Method is called while getting a list of 'InActive' employees for broker/agent : http://localhost:8080/hix/broker/employees?desigStatus=InActive
		 * This Method is called to Show 'Employee Details' for broker/agent
		 * This Method is called to Show 'Employee Summary' for broker/agent
		 */

		try{
			Integer pageSize = PAGE_SIZE;
			request.setAttribute(PAGE_SIZE2, pageSize);
			request.setAttribute(REQ_URI, EMPLOYEES);
			String param = request.getParameter(PAGE_NUMBER);

			Integer startRecord = 0;
			int currentPage = 1;

			Broker brokerObj = new Broker();

			AccountUser user = null;

			Map<String, Object> searchCriteria = null;

			List<EmployeeDTO> employeeList = null;

			List<EmployeeDTO> employeeListForPagination = null;

			String tableTitle = null;

			List<LookupValue> enrollmentStatuslist = null;

			//Get the list for populating drop down values
			enrollmentStatuslist = lookupService.getLookupValueList(EMPLOYEE_ENROLLMENT_STATUS);

			model.addAttribute(ENROLL_STATUSLIST, enrollmentStatuslist);

			//set page title
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Employees");

			//pagination stuff
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1) * pageSize;
				currentPage = Integer.parseInt(param);
			}

			//get broker from currently logged in user
			try {

				user = userService.getLoggedInUser();

				if ( user != null ) {

					String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
					if(isUserSwitchToOtherView != null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)){
						brokerObj = brokerService.getBrokerDetail(userService.getLoggedInUser().getActiveModuleId());
					}else{
						brokerObj = brokerService.findBrokerByUserId(user.getId());
					}

					if (brokerObj == null) {
						brokerObj = new Broker();
						brokerObj.setUser(user);
					}
				}
			} catch (InvalidUserException ex) {
				LOGGER.error("InvalidUserException occurred while searching for Broker by user id in getEmployees method.", ex);
				throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);

			}

			//Get search criteria from left nav first name, last name, enrollment status and logged in broker's id
			searchCriteria = this.createSearchCriteria(request, brokerObj.getId());

			//add enrollment status
			String newStatus = (request.getParameter(ENROLLMENT_STATUS_PARAMETER) == null) ? "" : request.getParameter(ENROLLMENT_STATUS_PARAMETER);
			if (!"".equals(newStatus)) {
				searchCriteria.put(ENROLLMENT_STATUS_PARAMETER, newStatus);
			}


			//find employees by criteria above
			if (searchCriteria != null && !searchCriteria.isEmpty()) {
				employeeList = designateService.findEmployees(searchCriteria, desigStatus);

				//update the enrollment status, effective date and renewal date in employee list
			}

			//pagination stuff: get  list for pagination
			employeeListForPagination = createPaginationForEmployeeList(pageSize, startRecord, employeeList);

			//set the table title like 'Employees 2 Active Employees'
			tableTitle = setTableTitle(desigStatus, EMPLOYEE);

			//set model attributes
			setModelAttributesForEmployeeListPage(model, searchCriteria, desigStatus, currentPage, employeeList, employeeListForPagination,
					tableTitle);
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving list of designated employees on employee list page for broker : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving list of designated employees on employee list page for broker in getEmployees method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getEmployees : END");

		return "broker/employees";
	}

	/**
	 * Retrieves contents of employee summary pop up page for broker
	 *
	 * @param employeeName
	 *            Employee Name
	 * @param employeeCompanyName
	 *            Employee Company Name
	 * @param employeePhoneNumber
	 *            Employee Phone Number
	 * @param employeeEmail
	 *            Employee Email
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "broker/employeesummarypopup", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_EMPLOYEE')")
	public String getEmployeeSummary(@RequestParam(value = DESIG_STATUS, required = false, defaultValue="Active") String desigStatus,
			@RequestParam(value = EMPLOYEE_NAME) String employeeName,
			@RequestParam(value = EMPLOYEE_COMPANY_NAME) String employeeCompanyName,
			@RequestParam(value = EMPLOYEE_PHONE_NUMBER) String employeePhoneNumber,
			@RequestParam(value = EMPLOYEE_EMAIL) String employeeEmail,
			Model model,
			HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("employeeSummaryPopup : START");

		try{
			model.addAttribute(EMPLOYEE_NAME, employeeName);
			model.addAttribute(EMPLOYEE_COMPANY_NAME, employeeCompanyName);
			model.addAttribute(EMPLOYEE_PHONE_NUMBER, employeePhoneNumber);
			model.addAttribute(EMPLOYEE_EMAIL, employeeEmail);
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while  retrieving contents of employee summary pop up page for broker : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while  retrieving contents of employee summary pop up page for broker in getEmployeeSummary method  : ", exception);
			throw new GIRuntimeException(exception,ExceptionUtils.getFullStackTrace(exception),Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("employeeSummaryPopup : END");

		return "broker/employeesummarypopup";
	}


	/**
	 * Method for pagination of employee list on employee list page for broker
	 *
	 * @param pageSize
	 *            Page size
	 * @param startRecord
	 *            Start of record on page
	 * @param employeeList
	 *            {@link List} of {@link EmployeeDTO}
	 * @return List of employees {@link EmployeeDTO}
	 */
	private List<EmployeeDTO> createPaginationForEmployeeList(Integer pageSize, Integer startRecord,
			List<EmployeeDTO> employeeList) {

		LOGGER.info("createPaginationForEmployeeList : START");

		List<EmployeeDTO> employeeListForPagination = new ArrayList<EmployeeDTO>();
		int i = 0;

		if ( employeeList != null && !employeeList.isEmpty()) {

			for (int index = startRecord; i < pageSize; index++, i++) {

				if (index < employeeList.size()) {
					employeeListForPagination.add(employeeList.get(index));
				} else {
					break;
				}
			}
		}

		LOGGER.info("createPaginationForEmployeeList : END");

		return employeeListForPagination;
	}

	/**
	 * Method to set {@link Model} object with attributes for employee list page for broker
	 *
	 * @param model
	 *            Model object
	 * @param searchCriteria
	 *            Criteria added by user for search
	 * @param desigStatus
	 *            Designation Status
	 * @param currentPage
	 *            Current page number
	 * @param employeeList
	 *            list of EmployeeDTO objects
	 *            {@link EmployeeDTO}
	 * @param employeeListForPagination
	 * 			  list of EmployeeDTO objects for pagination
	 *            {@link EmployeeDTO}
	 * @param tableTitle
	 *            Table Employer / Individual
	 */
	private void setModelAttributesForEmployeeListPage(Model model, Map<String, Object> searchCriteria,
			String desigStatus, int currentPage, List<EmployeeDTO> employeeList,
			List<EmployeeDTO> employeeListForPagination, String tableTitle) {

		LOGGER.info("setModelAttributesForEmployeeListPage : START");

		model.addAttribute(SEARCH_CRITERIA, searchCriteria);
		model.addAttribute(DESIG_STATUS, desigStatus);

		if (employeeListForPagination != null && !employeeListForPagination.isEmpty() ) {

			int iResultCount = employeeList.size();


			model.addAttribute(CURRENT_PAGE, currentPage);
			model.addAttribute(RESULT_SIZE, iResultCount);
			model.addAttribute(EMPLOYEELIST, employeeListForPagination);
		}

		model.addAttribute(TABLE_TITLE, tableTitle);

		LOGGER.info("setModelAttributesForEmployeeListPage : END");
	}





	/**
	 * Set {@link Model} object with attributes
	 *
	 * @param desigStatus
	 *            Designation Status
	 * @param model
	 *            Model object
	 * @param currentPage
	 *            Current page number
	 * @param brokerListAndRecordCount
	 *            DesignateBroker list and count
	 * @param searchCriteria
	 *            Criteria added by user for search
	 * @param brokers
	 *            {@link DesignateBroker}
	 * @param tableTitle
	 *            Table Employer / Individual
	 * @param contactNameMap
	 *            Employer / Individual Contact Name
	 * @param companyNameMap
	 *            Employer Company Name
	 */
	private void setModelAttributes(String desigStatus, Model model, int currentPage,
			Map<String, Object> brokerListAndRecordCount, Map<String, Object> searchCriteria,
			List<DesignateBroker> brokers, String tableTitle, Map<Integer, String> contactNameMap,
			Map<Integer, String> companyNameMap) {

		LOGGER.info("setModelAttributes : START");

		if (brokers != null) {
			Integer iResultCt = (Integer) brokerListAndRecordCount.get(RECORD_COUNT);
			int iResultCount = -1;

			if (iResultCt != null) {
				iResultCount = iResultCt.intValue();
			}
			setBrokersList(desigStatus, model, brokers);
			model.addAttribute(RESULT_SIZE, iResultCount);
			model.addAttribute(CURRENT_PAGE, currentPage);
			model.addAttribute(DESIG_STATUS, desigStatus == null ? Status.Pending.toString() : desigStatus);
			model.addAttribute(CONTACT_NAME_MAP, contactNameMap);
			model.addAttribute(COMPANY_NAME_MAP, companyNameMap);
			model.addAttribute(SEARCH_CRITERIA, searchCriteria);
			model.addAttribute(TABLE_TITLE, tableTitle);
		}

		LOGGER.info("setModelAttributes : END");
	}



	/**
	 * Sets brokers list to model
	 *
	 * @param desigStatus
	 *            Desingation status
	 * @param model
	 *            Model object
	 * @param brokers
	 *            {@link DesignateBroker}
	 */
	private void setBrokersList(String desigStatus, Model model, List<DesignateBroker> brokers) {

		LOGGER.info("setBrokersList : START");

		if (Status.Active.toString().equalsIgnoreCase(desigStatus)) {
			model.addAttribute(APPROVED_BROKERS_LIST, brokers);
			model.addAttribute(BROKERS_LIST, null);
		} else {
			model.addAttribute(BROKERS_LIST, brokers);
			model.addAttribute(APPROVED_BROKERS_LIST, null);
		}
		LOGGER.info("setBrokersList : END");
	}

	/**
	 * Creates pagination
	 *
	 * @param pageSize
	 *            Page size
	 * @param startRecord
	 *            Start of record on page
	 * @param brokersList
	 *            {@link List} of {@link DesignateBroker}
	 * @return List of designated broker {@link DesignateBroker}
	 */
	private List<DesignateBroker> createPagination(Integer pageSize, Integer startRecord,
			List<DesignateBroker> brokersList) {

		LOGGER.info("createPagination : START");

		List<DesignateBroker> brokers = new ArrayList<DesignateBroker>();
		int i = 0;

		if ( brokersList != null && !brokersList.isEmpty() ) {

			for (int index = startRecord; i < pageSize; index++, i++) {
				if (index < brokersList.size()) {
					brokers.add(brokersList.get(index));
				} else {
					break;
				}
			}
		}

		LOGGER.info("createPagination : END");

		return brokers;
	}

	/**
	 * Sets table title for display
	 *
	 * @param desigStatus
	 *            Designation Status
	 * @param moduleName
	 *            For which module title is to be set
	 * @return String Table title
	 */
	private String setTableTitle(String desigStatus, String moduleName) {

		LOGGER.info("setTableTitle : START");

		String tableTitle = "";
		if (desigStatus != null) {
			if (desigStatus.equals(Status.Pending.toString())) {
				tableTitle = PENDING_TABLE_TITLE + moduleName;
			} else if (desigStatus.equals(Status.Active.toString())) {
				tableTitle = ACTIVE_TABLE_TITLE + moduleName;
			} else if (desigStatus.equals(Status.InActive.toString())) {
				tableTitle = INACTIVE_TABLE_TITLE + moduleName;
			}
		} else {
			tableTitle = PENDING_TABLE_TITLE + moduleName;
		}

		LOGGER.info("setTableTitle : END");

		return tableTitle;
	}



	/**
	 * Displays confirmation screen after designation is done.
	 *
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/designateconfirm", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_DESIGNATE_EMPLOYER')")
	public String designateconfirm(Model model, HttpServletRequest request) {

		LOGGER.info("designateconfirm : START ");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Designation Confirmation");

		LOGGER.info("designateconfirm : END");

		return "broker/designateconfirm";
	}

	/**
	 * If employer / individual have already been designated to an Broker then
	 * designation message is shown.
	 *
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/designatemessage", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_DESIGNATE_EMPLOYER')")
	public String designatemessage(Model model, HttpServletRequest request) {

		LOGGER.info("designatemessage : START ");

		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Designation");
		// System.out.println("-**-broker/designatemessage ");

		LOGGER.info("designatemessage : END ");

		return "broker/designatemessage";
	}

	/**
	 * Displays search page. If search is external then saves request parameters
	 * in the session
	 *
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/search", method = { RequestMethod.GET, RequestMethod.POST })
	//@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_BROKER')")
	public String brokerSearchScreen(Model model, HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("brokerSearchScreen : START ");
		String anonymousFlag = null;
		String redirectUrl = "broker/search";
		AccountUser user = null;
		String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
		String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
		boolean isSwtichedToOtherRole = false;
		String recordId = null;
		
		try
		{

			if (RequestMethod.GET.toString().equalsIgnoreCase(request.getMethod()) && request.getSession() != null) {
				request.getSession().removeAttribute(BROKERSEARCHPARAM);
				request.getSession().removeAttribute(BROKERMAXDIS);
			}

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				anonymousFlag = request.getParameter(BrokerConstants.ANONYMOUS_FLAG) != null ? request.getParameter(BrokerConstants.ANONYMOUS_FLAG) : request.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG) + "";
			} else {
				anonymousFlag = this.getAnaonymousFlagStatus(request, true);
			}
			
			//add anonymous flag to session
			request.getSession().setAttribute(BrokerConstants.ANONYMOUS_FLAG,anonymousFlag);
			// Anonymous user
			String encypteRrecordId = request.getParameter(RECORD_ID); 
			String recordType = request.getParameter(RECORD_TYPE) != null ? request.getParameter(RECORD_TYPE) : request.getSession().getAttribute(RECORD_TYPE) + "";

			LOGGER.warn("Encrypted Record ID : "+encypteRrecordId+" : Record Type : "+recordType+" : Anonymous Flag : "+anonymousFlag);
			
			if (request.getSession().getAttribute(RECORD_ID) != null) {
				recordId = request.getSession().getAttribute(RECORD_ID) + "";	
			}
			// Anonymous user
			if(null != anonymousFlag && !anonymousFlag.trim().isEmpty() && anonymousFlag.equalsIgnoreCase(N)) {
				try {
					//user is not anonymous get the logged in user
					if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
						if(!EntityUtils.isEmpty(encypteRrecordId)){
							recordId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encypteRrecordId);
						}

						LOGGER.warn("Decrypted Record ID : "+recordId+" : Record Type : "+recordType+" : Anonymous Flag : "+anonymousFlag);
						
						request.getSession().setAttribute(RECORD_ID, recordId);
						request.getSession().setAttribute(RECORD_TYPE, recordType);
						
						redirectUrl = "redirect:/entity/locateassister/locateassistancetype";
					} else {
						user =  userService.getLoggedInUser();
						//if user is individual
						if(user!=null && user.getActiveModuleName()!=null && user.getActiveModuleName().equalsIgnoreCase(RoleService.INDIVIDUAL_ROLE) && (stateExchangeType.equalsIgnoreCase(BOTH) || stateExchangeType.equalsIgnoreCase(INDIVIDUAL)) ){
							//is user switched to other role
							if(isUserSwitchToOtherView != null && "y".equalsIgnoreCase(isUserSwitchToOtherView.trim())){
								isSwtichedToOtherRole=true;
							}
							//set record id
							if(request.getSession().getAttribute(RECORD_ID) == null){
								request.getSession().setAttribute(RECORD_ID,String.valueOf(isSwtichedToOtherRole?user.getActiveModuleId():user.getId()));
							}
							//set record type
							if(request.getSession().getAttribute(RECORD_TYPE) == null){
								request.getSession().setAttribute(RECORD_TYPE,user.getActiveModuleName());
							}
							//set redirect url to locate assistance
							redirectUrl = "forward:/entity/locateassister/locateassistancetype";
						}else{
							LOGGER.info("brokerSearchScreen : USER Object is NULL from userService.getLoggedInUser() ");
						}
					}
				}catch (InvalidUserException ex) {
					LOGGER.error("Exception occured while retriving user object.", ex);
					throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
				}
			}else{
				//user is Anonymous
				if((stateExchangeType.equalsIgnoreCase(BOTH) || stateExchangeType.equalsIgnoreCase(INDIVIDUAL))){
					//set redirect url to locate assistance
					redirectUrl = "forward:/entity/locateassister/locateassistancetype";
				}
			}
			LOGGER.info("brokerSearchScreen : END");
		}
		catch(GIRuntimeException giexception) {
			LOGGER.error("GIRuntimeException occured while fetching Agents : ", giexception);
			throw giexception;
		} catch(Exception exception) {
			LOGGER.error("Exception occured while fetching Agents in brokerSearchScreen method : ", exception	);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return redirectUrl;
	}

	/**
	 * Displays search page. If search is external then saves request parameters
	 * in the session
	 *
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/search/individual", method = { RequestMethod.GET, RequestMethod.POST })
	//@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_BROKER')")
	public String brokerSearchIndividual(Model model, HttpServletRequest request) throws GIRuntimeException{

		LOGGER.info("brokerSearchIndividual : START ");

		try{
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Search");

			String recordId = (String) request.getSession().getAttribute(RECORD_ID);
			String recordType = (String) request.getSession().getAttribute(RECORD_TYPE);
			String anonymousFlag = (String) request.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			
			model.addAttribute(LANGUAGES_LIST, loadLanguages());
			
			LOGGER.warn("Record ID : "+recordId+" : Record Type : "+recordType+" : Anonymous Flag : "+anonymousFlag);
			
			if (anonymousFlag != null) {
				if (N.equalsIgnoreCase(anonymousFlag) && recordId != null && recordType != null) {
					
					LOGGER.info("Invoking IND52 : Record ID : "+recordId+" : Record Type : "+recordType+" : Anonymous Flag : "+anonymousFlag);
					
					// Trigger for I IND52
					// Calling AHBX to retrieve and save external Individual detail
					if (EXT_INDIVIDUAL.equalsIgnoreCase(recordType)) {
						LOGGER.info("External Individual BOB Detail AHBX service call Starts : ");
						String response = brokerMgmtUtils.retrieveAndSaveExternalIndividualDetail(Long.valueOf(recordId));
						LOGGER.debug("IND52:External Individual BOB Detail AHBX service call completed.");
					}
				}
			}
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while displaying search page : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while displaying search page in brokerSearchIndividual method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("brokerSearchIndividual : END");

		return "broker/search";
	}

	/**
	 * Displays broker search page
	 *
	 * @param status designation status
	 * @return URL for navigation to search page
	 */
	@RequestMapping(value = "/broker/search/Emp", method = RequestMethod.GET)
	//	@PreAuthorize("hasPermission(#model, '')")
	public String brokerSearchEmp(@RequestParam String status) {

		LOGGER.info("brokerSearchEmp : START");

		LOGGER.info("brokerSearchEmp : END");

		return "broker/search";
	}

	/**
	 * Searches certified brokers based on client served field. If client served
	 * for broker is blank, then record will not be shown to the employer /
	 * individual
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param model
	 *            Model Object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 * @throws CloneNotSupportedException
	 */
	@SuppressWarnings({ "unchecked"})
	@RequestMapping(value = "/broker/searchlist",method = RequestMethod.GET)
	//@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_BROKER')")
	public String brokerSearch(@ModelAttribute(BROKER) Broker broker, Model model, HttpServletRequest request) throws GIRuntimeException {

		LOGGER.info("brokerSearch : START ");

		try{
			Broker broker1 = (Broker) request.getSession().getAttribute(BROKER_SESSION);

			String distance = "";

			request.getSession().removeAttribute(BROKER_SESSION);
			if (broker1 != null) {
				broker = broker1;
			}
			Broker brkObj = null;
			Broker brokerSearchParam = (Broker) request.getSession().getAttribute(BROKERSEARCHPARAM);
			String sessionUse = request.getParameter(USESESSION);
			if (sessionUse != null && YES.equalsIgnoreCase(sessionUse) && brokerSearchParam!=null ) {
				brkObj = brokerSearchParam;
			} else {
				brkObj = (Broker) broker.clone();
			}
			model.addAttribute(PAGE_TITLE, " Getinsured Health Exchange: Broker Search Results");
			Integer pageSize = PAGE_SIZE;
			request.setAttribute(PAGE_SIZE2, pageSize);
			request.setAttribute(REQ_URI, "searchlist");
			String param = request.getParameter(PAGE_NUMBER);
			Integer startRecord = 0;
			int currentPage = 1;
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1) * pageSize;
				currentPage = Integer.parseInt(param);
			}

			brkObj.setCertificationStatus(BROKER_CERTIFICATION_STATUS_CERTIFIED);

			String recordId = (String) request.getSession().getAttribute(RECORD_ID);
			String recordType = (String) request.getSession().getAttribute(RECORD_TYPE);
			String anonymousFlag = (String) request.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG);
			
			LOGGER.warn("Record ID : "+recordId+" : Record Type : "+recordType+" : Anonymous Flag : "+anonymousFlag);
			
			if(null != anonymousFlag && !anonymousFlag.trim().isEmpty() && anonymousFlag.equalsIgnoreCase(N))
			{
				brkObj = populateClientsServed(brkObj, recordType);
			}

			if ( request.getSession().getAttribute(BROKERMAXDIS) != null ) {

				distance = (String)request.getSession().getAttribute(BROKERMAXDIS);
			}

			if ( distance != null && !distance.trim().isEmpty() ) {

				brkObj.setDistance(distance);
			}

			if ( request.getParameter(DISTANCE) != null && !request.getParameter(DISTANCE).trim().isEmpty() ) {

				brkObj.setDistance(request.getParameter(DISTANCE));
			}

			Map<String, Object> brokerListAndRecordCount = brokerService.searchBroker(anonymousFlag, brkObj, startRecord, pageSize);

			Validate.notNull(brokerListAndRecordCount);

			List<Broker> brokers = (List<Broker>) brokerListAndRecordCount.get(BROKERLIST);
			if(brokers!=null){
				for(Broker tmpBroker :brokers){
					String productExperties = tmpBroker.getProductExpertise();
					if (productExperties != null) {
						List<String> productExpertiesList = EntityUtils.splitStringValues(productExperties, ",");
						StringBuilder productExpertiesStrBuilder = new StringBuilder();
						for (String productExpertiesListStr : productExpertiesList) {
							productExpertiesStrBuilder.append(productExpertiesListStr);
							productExpertiesStrBuilder.append(",");
							productExpertiesStrBuilder.append(" ");
						}
						productExperties = productExpertiesStrBuilder.substring(0, productExpertiesStrBuilder.length() - 2);
					}
					tmpBroker.setProductExpertise(productExperties);

					String tmpLanSpoken= tmpBroker.getLanguagesSpoken();

					if(tmpLanSpoken != null){
						List<String> langList = EntityUtils.splitStringValues(tmpLanSpoken, ",");
						StringBuilder langStrBuilder = new StringBuilder();

						for (String langListStr : langList) {
							langStrBuilder.append(langListStr);
							langStrBuilder.append(",");
							langStrBuilder.append(" ");
						}
						tmpLanSpoken = langStrBuilder.substring(0, langStrBuilder.length() - 2);
					}
					tmpBroker.setLanguagesSpoken(tmpLanSpoken);
				}
			}
			int iResultCount = 0;

			if (brokerListAndRecordCount != null && !brokerListAndRecordCount.isEmpty()) {

				iResultCount = (Integer) brokerListAndRecordCount.get(RECORD_COUNT);
			}

			model.addAttribute(BROKERS_LIST, brokers);

			if (brkObj != null) {
				if (brkObj.getUser() != null) {
					model.addAttribute(SEARCH_FIRSTNAME, brkObj.getUser().getFirstName());
					model.addAttribute(SEARCH_LAST_NAME, brkObj.getUser().getLastName());
				} else {
					model.addAttribute(SEARCH_FIRSTNAME, "");
					model.addAttribute(SEARCH_LAST_NAME, "");
					model.addAttribute(SEARCH_LANGUAGES, "");
				}

				if (brkObj.getLanguagesSpoken() != null) {
					model.addAttribute(SEARCH_LANGUAGES, entityUtils.checkLanguage(brkObj.getLanguagesSpoken()));
				} else {
					model.addAttribute(SEARCH_LANGUAGES, "");
				}

				int zip = getIntegerFromString(brkObj.getLocation() != null ? brkObj.getLocation().getZip() : ZERO_STRING);

				if (getIntegerFromString(brkObj.getDistance()) > 0 && zip > 0) {

					model.addAttribute(SEARCH_DISTANCE, brkObj.getDistance());

				} else {

					model.addAttribute(SEARCH_DISTANCE, "");
				}

			}

			if(null != brkObj) {
				if (brkObj.getLocation() != null && brkObj.getLocation().getZip() != null && brkObj.getLocation().getZip().trim().length() > 0) {
					model.addAttribute(SEARHZIPCODE, brkObj.getLocation().getZip());
				}
				else {
					model.addAttribute(SEARHZIPCODE, "");
				}

				request.getSession().setAttribute(BROKERMAXDIS, brkObj.getDistance());
			}
			else {
				model.addAttribute(SEARHZIPCODE, "");
			}
			model.addAttribute(RESULT_SIZE, iResultCount);
			model.addAttribute(CURRENT_PAGE, currentPage);
			request.getSession().setAttribute(BROKERSEARCHPARAM, brkObj);
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while searching  certified brokers based on client : ", giexception);
			throw giexception;
		} catch(CloneNotSupportedException cloneNotSupported){
			LOGGER.error("CloneNotSupportedException occured while searching  certified brokers based on client : ", cloneNotSupported);
			throw new GIRuntimeException(cloneNotSupported, ExceptionUtils.getFullStackTrace(cloneNotSupported), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}catch(Exception exception){
			LOGGER.error("Exception occured while searching  certified brokers based on client : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}


		LOGGER.info("brokerSearch : END");

		return "broker/list";
	}

	/**
	 * Method to get Integer from String
	 * @param str string
	 * @return integer
	 */
	private int getIntegerFromString(String str) {

		int integer = -1;

		if ( isNotEmptyString(str) ) {

			integer = Integer.parseInt(str);
		}

		return integer;
	}

	/**
	 * Method to check if string is not empty
	 * @param str String
	 * @return <code>true</code> if not empty string, else <code>false</code>
	 */
	private boolean isNotEmptyString(String str) {

		boolean notEmptyString = false;

		if ( str != null && !str.trim().isEmpty() ) {

			notEmptyString = true;
		}

		return notEmptyString;
	}

	/**
	 * Method to populate clients served for broker search
	 * @param broker Broker Object
	 * @param recordType record type
	 * @return Broker Object
	 */
	private Broker populateClientsServed(Broker broker, String recordType) throws GIRuntimeException {
		AccountUser user = null;

		try {
			String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
			user = userService.getLoggedInUser();

			if (userService.hasUserRole(user, RoleService.EMPLOYER_ROLE)) {

				broker.setClientsServed(CLIENTS_SERVED_EMPLOYERS);
			} else if (userService.hasUserRole(user, RoleService.INDIVIDUAL_ROLE) && (stateExchangeType.equalsIgnoreCase(BOTH) || stateExchangeType.equalsIgnoreCase(INDIVIDUAL))) {
				broker.setClientsServed(CLIENTS_SERVED_INDIVIDUALS_FAMILIES);
			}

		} catch (InvalidUserException ex) {
			LOGGER.error(EXCEPTION_USER_NOT_LOGGED_IN, ex);
			//throw new GIRuntimeException(null, ex,ExceptionUtils.getFullStackTrace(ex),Severity.HIGH);
		}

		return broker;
	}

	/**
	 * Searches certified brokers based on client served field. If client served
	 * for broker is blank, then record will not be shown to the employer /
	 * individual
	 *
	 * @param broker
	 *            {@link Broker}
	 * @param model
	 *            Model Object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/search/searchlist", method = { RequestMethod.GET, RequestMethod.POST })
	//	@PreAuthorize("hasPermission(#model, '')")
	public String brokerSearchForIndividual(@RequestParam(value = DISTANCE, required = false) String distance, @ModelAttribute(BROKER) Broker broker, Model model,
			HttpServletRequest request) {

		LOGGER.info("brokerSearchForIndividual : START ");

		request.getSession().setAttribute(BROKER_SESSION, broker);
		request.getSession().setAttribute(BROKERMAXDIS, distance);

		LOGGER.info("brokerSearchForIndividual : END");

		return "redirect:/broker/searchlist";
	}

	/**
	 * Creates search criteria
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param id
	 *            broker id
	 * @return {@link Map} of search parameter with value
	 */
	private Map<String, Object> createSearchCriteria(HttpServletRequest request, int id) {

		LOGGER.info("createSearchCriteria : START");

		Map<String, Object> searchCriteria = new HashMap<String, Object>();

		String brokerId = String.valueOf(id);
		String companyName = (request.getParameter(COMPANY_NAME) == null) ? "" : request.getParameter(COMPANY_NAME);

		String firstName = (request.getParameter(FIRSTNAME) == null) ? "" : request.getParameter(FIRSTNAME);

		String lastName = (request.getParameter(LASTNAME) == null) ? "" : request.getParameter(LASTNAME);

		String status = (request.getParameter("status") == null) ? "" : request.getParameter("status");
		String enrollmentStatus = (request.getParameter(ENROLLMENT_STATUS) == null) ? "" : request
				.getParameter(ENROLLMENT_STATUS);
		if (!"".equals(brokerId)) {
			searchCriteria.put(BROKER_ID, brokerId);
		}
		if (!"".equals(companyName)) {
			searchCriteria.put(COMPANY_NAME, companyName);
		}
		if (!"".equals(firstName)) {
			searchCriteria.put(FIRSTNAME, firstName);
		}
		if (!"".equals(lastName)) {
			searchCriteria.put(LASTNAME, lastName);
		}
		if (!"".equals(status)) {
			searchCriteria.put(STATUS, status);
		}
		if (!"".equals(enrollmentStatus)) {
			searchCriteria.put(ENROLLMENT_STATUS, enrollmentStatus);
		}
		
		//Tmp object
		BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO= new BrokerBOBSearchParamsDTO();
		setDates(searchCriteria, request,brokerBOBSearchParamsDTO);

		LOGGER.info("createSearchCriteria : END");

		return searchCriteria;
	}


	/**
	 * Creates search criteria for Individuals
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param id
	 *            broker id
	 * @param brokerBOBSearchParamsDTO 
	 * @return {@link Map} of search parameter with value
	 */
	private Map<String, Object> createSearchCriteriaForIndividuals(HttpServletRequest request, int id, String desigStatus, BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {

		LOGGER.info("DesignateSearchController.createSearchCriteriaForIndividuals : START");

		Map<String, Object> searchCriteria = new HashMap<String, Object>();
		String sortBy = null;
		

		if(desigStatus!=null){
			if(desigStatus.equalsIgnoreCase(DesignateBroker.Status.Active.toString())){
				sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) || request.getParameter(BrokerConstants.SORT_BY).isEmpty() ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
			}
			else if(desigStatus.equalsIgnoreCase(DesignateBroker.Status.Pending.toString())){
				sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) || request.getParameter(BrokerConstants.SORT_BY).isEmpty() ?  BrokerConstants.REQUESTSENT : request.getParameter(BrokerConstants.SORT_BY);
			}
			else if(desigStatus.equalsIgnoreCase(DesignateBroker.Status.InActive.toString())){
				sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) || request.getParameter(BrokerConstants.SORT_BY).isEmpty() ?  BrokerConstants.INACTIVESINCE : request.getParameter(BrokerConstants.SORT_BY);
			}
			else{
				sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) || request.getParameter(BrokerConstants.SORT_BY).isEmpty() ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
			}
		}
		else{
			LOGGER.info("DesignateSearchController.createSearchCriteriaForIndividuals - Designation Status is Null.");
			sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
		}
		String sortOrder = determineSortOrderForCriteria(request);

		searchCriteria.put(BrokerConstants.SORT_BY, sortBy);
		searchCriteria.put(BrokerConstants.SORT_ORDER, sortOrder);
		brokerBOBSearchParamsDTO.setSortBy(sortBy);
		brokerBOBSearchParamsDTO.setSortOder(sortOrder);
		
		String brokerId = String.valueOf(id);
		searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.BROKER);
		brokerBOBSearchParamsDTO.setRecordType(BrokerConstants.AGENT);//According to new WSDL
		String firstName = (request.getParameter(FIRSTNAME) == null) ? "" : request.getParameter(FIRSTNAME);
		String lastName = (request.getParameter(LASTNAME) == null) ? "" : request.getParameter(LASTNAME);
		String eligiblityStatus = (request.getParameter(BrokerConstants.ELIGIBILITY_STATUS) == null) ? "" : request.getParameter(BrokerConstants.ELIGIBILITY_STATUS);
		String applicationStatus = (request.getParameter(BrokerConstants.APPLICATION_STATUS) == null) ? "" : request.getParameter(BrokerConstants.APPLICATION_STATUS);

		if (!"".equals(brokerId)) {
			searchCriteria.put(BrokerConstants.MODULE_ID, brokerId);
			brokerBOBSearchParamsDTO.setRecordId(Long.parseLong(brokerId));
		}
		if (!"".equals(firstName)) {
			searchCriteria.put(FIRSTNAME, firstName);
			brokerBOBSearchParamsDTO.setFirstName(firstName);
		}
		if (!"".equals(lastName)) {
			searchCriteria.put(LASTNAME, lastName);
			brokerBOBSearchParamsDTO.setLastName(lastName);
		}
		if(!"".equals(eligiblityStatus)){
			searchCriteria.put(BrokerConstants.ELIGIBILITY_STATUS, eligiblityStatus);
			brokerBOBSearchParamsDTO.setEligibilityStatus(eligiblityStatus);
		}
		if(!"".equals(applicationStatus)){
			searchCriteria.put(BrokerConstants.APPLICATION_STATUS, applicationStatus);
			brokerBOBSearchParamsDTO.setCurrentStatus(applicationStatus);
		}

		setDates(searchCriteria, request,brokerBOBSearchParamsDTO);

		LOGGER.info("DesignateSearchController.createSearchCriteriaForIndividuals : END");

		return searchCriteria;
	}

	/**
	 * Sets dates in search criteria
	 *
	 * @param searchCriteria
	 *            {@link Map} of search criteria
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param brokerBOBSearchParamsDTO 
	 */
	private void setDates(Map<String, Object> searchCriteria, HttpServletRequest request, BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {

		LOGGER.info("setDates : START");

		String requestSentFrom = (request.getParameter(BrokerConstants.REQUEST_SENT_FROM) == null) ? "" : request
				.getParameter(BrokerConstants.REQUEST_SENT_FROM);
		String requestSentTo = (request.getParameter(BrokerConstants.REQUEST_SENT_TO) == null) ? "" : request
				.getParameter(BrokerConstants.REQUEST_SENT_TO);
		String inactiveDateFrom = (request.getParameter(BrokerConstants.INACTIVE_DATE_FROM) == null) ? "" : request
				.getParameter(BrokerConstants.INACTIVE_DATE_FROM);
		String inactiveDateTo = (request.getParameter(BrokerConstants.INACTIVE_DATE_TO) == null) ? "" : request
				.getParameter(BrokerConstants.INACTIVE_DATE_TO);

		if (!"".equals(requestSentFrom)) {
			searchCriteria.put(BrokerConstants.REQUEST_SENT_FROM, requestSentFrom);
			brokerBOBSearchParamsDTO.setRequestSentFrmDt(requestSentFrom);
		}
		if (!"".equals(requestSentTo)) {
			searchCriteria.put(BrokerConstants.REQUEST_SENT_TO, requestSentTo);
			brokerBOBSearchParamsDTO.setRequestSentToDt(requestSentTo);
		}
		if (!"".equals(inactiveDateFrom)) {
			searchCriteria.put(BrokerConstants.INACTIVE_DATE_FROM, inactiveDateFrom);
			brokerBOBSearchParamsDTO.setInactiveSinceFrmDt(inactiveDateFrom);
		}
		if (!"".equals(inactiveDateTo)) {
			searchCriteria.put(BrokerConstants.INACTIVE_DATE_TO, inactiveDateTo);
			brokerBOBSearchParamsDTO.setInactiveSinceToDt(inactiveDateTo);
		}

		LOGGER.info("setDates : END");
	}

	/**
	 * Forwards to esignature page
	 *
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param id
	 *            Broker Id
	 * @return URL for navigation to appropriate result page
	 */
	@GiAudit(transactionName = "Esignature", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.DESIGNATE_AGENT)
	@RequestMapping(value = "/broker/esignature/{encBrokerId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_DESIGNATE_EMPLOYER')")
	public String esignature(Model model, HttpServletRequest request, @PathVariable("encBrokerId") String encBrokerId) throws GIRuntimeException {

		LOGGER.info("esignature : START");

		AccountUser user = new AccountUser();
		try{
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Electronic Signature");

			String recordId = (String) request.getSession().getAttribute(RECORD_ID);
			String recordType = (String) request.getSession().getAttribute(RECORD_TYPE);
			
			LOGGER.warn("Record ID : "+recordId+ " : Record Type : "+recordType);
			
			String errorMsg = "";
			String id = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);

			LOGGER.debug("Request Parameters recived ");

			try {
				user = userService.getLoggedInUser();	
				
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					model.addAttribute(APPLICANT_NAME, (user.getFirstName() + ' ' + user.getLastName()));
				} else {
					if(userService.hasUserRole(user, RoleService.INDIVIDUAL_ROLE)){
	                    Household household = getHouseHoldById(user.getActiveModuleId());
	                    model.addAttribute(APPLICANT_NAME, (household.getFirstName() + ' ' + household.getLastName()));
					} else {					
	                    model.addAttribute(APPLICANT_NAME, (user.getFirstName() + ' ' + user.getLastName()));
					}
				}

				StringBuilder logger = new StringBuilder();

				logger.append("Adding User details to Model");

				LOGGER.debug(logger.toString());
			} catch (InvalidUserException ex) {
				LOGGER.error("InvalidUserException while getting logged in user details. ", ex);
				throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
			model.addAttribute(ERROR_MSG, errorMsg);
			model.addAttribute(ID2, id);
			model.addAttribute(BROKER, brokerService.getBrokerDetail(Integer.parseInt(id)));
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));

			LOGGER.debug("Redirecting to E-signature page");
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while forwarding to esignature page : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while forwarding to esignature page in esignature method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}



		LOGGER.info("esignature : END");

		return "broker/esignature";
	}

	/**
	 * After e-signature flow redirects to designate functionality
	 *
	 * @param response
	 * @param model
	 *            Model Object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param id
	 *            Broker Id
	 * @return URL for navigation to appropriate result page
	 */
	@GiAudit(transactionName = "Esignature Submit", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.DESIGNATE_AGENT)
	@RequestMapping(value = "/broker/esignatureSubmit/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_DESIGNATE_EMPLOYER')")
	public void esignatureSubmit(Model model, HttpServletRequest request, @PathVariable(ID2) String id,HttpServletResponse response) throws GIRuntimeException  {

		LOGGER.info("esignatureSubmit : START");

		String esignName = request.getParameter(ESIGN_NAME);

		LOGGER.debug("E-signature name:" + esignName);

		request.getSession().setAttribute(ESIGN_NAME, esignName);
		try {
			//response.sendRedirect(request.getContextPath()+"/broker/designate/" + id);
			RequestDispatcher rd = request.getRequestDispatcher("/broker/designate/" + id);
			rd.forward(request, response);
			LOGGER.info(ESIGNATURE_SUBMIT_END);
		} catch (IOException ex) {
			LOGGER.error("Failed to redirect to designate",ex);
			LOGGER.info(ESIGNATURE_SUBMIT_END);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while esignature Submit : ", giexception);
			LOGGER.info(ESIGNATURE_SUBMIT_END);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while Submiting esignature : ", exception	);
			LOGGER.info(ESIGNATURE_SUBMIT_END);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

	}

	/**
	 * Request handler method to fetch Languages matching the given query/lookup term from repository.
	 *
	 * @param query The query term.
	 * @param response The HttpServletResponse instance.
	 * @return String The languages matching the query term.
	 */
	@RequestMapping(value = "broker/getlanguage", method = RequestMethod.GET, headers = "Accept=*/*")
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_BROKER')")
	public @ResponseBody
	String getLanguageList(@RequestParam("matched") String query, HttpServletResponse response) {
		LOGGER.info("getLanguageList : START");
		List<String> languages = null;

		LOGGER.info("Getting the language spoken list ");
		List<String> languageList = lookupService.populateLanguageNames(query);

		if (null == languageList) {
			languages = new ArrayList<String>();
		} else {
			languages = languageList;
		}

		LOGGER.info("getLanguageList: END");
		return brokerMgmtUtils.getLanguageSpokenList(query, response, languages);
	}

	/**
	 * Method 'switchToBrokerView' to switch role from agent(csr) to Employer
	 *
	 * @param model The current Model instance.
	 * @param request The current HttpServletRequest instance.
	 * @return String The actual request processing URL.
	 */
	//@RequestMapping(value = "/broker/employer/dashboard", method ={RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_EMPLOYER')")
	public String switchToBrokerView(Model model, HttpServletRequest request) {
		if(request.getSession().getAttribute(CHECK_DILOG)==null){
			String checkDilog= request.getParameter("checkEmployerView");
			request.getSession().setAttribute(CHECK_DILOG, checkDilog);
		}

		String switchToModuleId = request.getParameter("switchToModuleId");
		String switchToResourceName = request.getParameter("switchToResourceName");
		if("-1".equals(switchToModuleId)&&"SettingUpNewEmployer".equals(switchToResourceName)){
			request.getSession().setAttribute("newEmployerByAgent", true);
		}

		return "forward:/account/user/switchUserRole";
	}

	/**
	 * Method 'switchToIndividual' to switch role from agent(csr) to Individual
	 *
	 * @param model The current Model instance.
	 * @param request The current HttpServletRequest instance.
	 * @return String The actual request processing URL.
	 */
	@RequestMapping(value = "/broker/individual/dashboard", method ={RequestMethod.POST})
//	@PreAuthorize("hasPermission(#model, 'BROKER_VIEW_INDIVIDUAL')")
	public String switchToIndividualView(Model model, HttpServletRequest request) {
		final int _0 = 0;
		AccountUser user;
		DesignateBroker designatedBroker;

		String switchToModuleId = request.getParameter("switchToModuleId");
		String switchToResourceName = request.getParameter("switchToResourceName");
		String checkDilog= request.getParameter("checkIndividualView");
		String switchToModuleName= request.getParameter("switchToModuleName");
		Validate.notNull(switchToModuleId);

		String decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(switchToModuleId);
		int individualId = Integer.parseInt(decryptedId);

		HashMap<String,String> paramMap= new HashMap<String,String>(3);
		paramMap.put("switchToModuleName", switchToModuleName);
		paramMap.put("switchToModuleId", decryptedId);
		paramMap.put("switchToResourceName", switchToResourceName);
		
		
		try {
			if(StringUtils.isNotBlank(checkDilog)) {
				checkDilog = checkDilog.trim().toUpperCase();

				user = userService.getLoggedInUser();

				Validate.notNull(user);

				designatedBroker = designateService.findBrokerByIndividualId(individualId);

				Validate.notNull(designatedBroker);

				if(checkDilog.charAt(_0) == Y.charAt(_0) || checkDilog.charAt(_0) == N.charAt(_0)) {
					designatedBroker.setShow_switch_role_popup(checkDilog.charAt(_0));
				}
				else {
					designatedBroker.setShow_switch_role_popup(Y.charAt(_0));
				}

				designatedBroker.setUpdated(new TSDate());
				designatedBroker.setUpdatedBy(user.getId());

				designateService.saveDesignateBroker(designatedBroker);
			}
			if(null != request && null != request.getSession()) {
				request.getSession().setAttribute(LAST_VISITED_URL, "/broker/bookofbusiness?desigStatus=Active");
			}
		}
		catch(Exception exception){
			LOGGER.error("Exception occured while switching to Individual : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "forward:/account/user/switchUserRole?ref="+GhixAESCipherPool.encryptParameterMap(paramMap);
	}

	/**
	 * Method to determine user/viewer's anonymous flag status.
	 *
	 * @param cRequest The current HttpServletRequest instance.
	 * @return cAnonymousFlag The current user/viewer's anonymous flag.
	 */
	private String getAnaonymousFlagStatus(HttpServletRequest cRequest, boolean isSearchRequest) {
		String cAnonymousFlag = Y;

		LOGGER.info("DesignateSearchController :::: INSIDE getAnaonymousFlagStatus  START ");
		
		LOGGER.warn("Anonymous Flag : "+cRequest.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG));
		
		if(null != cRequest) {
			if(isSearchRequest) {
				// fix for issue HIX-17411 - if param is not present in request parameter, check in session
				cAnonymousFlag = cRequest.getParameter(BrokerConstants.ANONYMOUS_FLAG) != null ? cRequest.getParameter(BrokerConstants.ANONYMOUS_FLAG)
						: cRequest.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG) + "";
			}
			else {
				if(null != cRequest.getSession() && null != cRequest.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG)) {
					cAnonymousFlag = cRequest.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG).toString().trim();
				}
			}
		}

		if(null != SecurityContextHolder.getContext() && null != SecurityContextHolder.getContext().getAuthentication() && null != SecurityContextHolder.getContext().getAuthentication().getPrincipal() && !BrokerConstants.ANONYMOUS_USER.equals(SecurityContextHolder.getContext().getAuthentication().getPrincipal())) {
			cAnonymousFlag = N;
		}
		else {
			// No Logged In User in CA needed as It's Locate Assistance URL
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				
				cAnonymousFlag = cRequest.getParameter(BrokerConstants.ANONYMOUS_FLAG) != null ? cRequest.getParameter(BrokerConstants.ANONYMOUS_FLAG)
						: cRequest.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG) + "";
				
				LOGGER.warn("GET RECORDID for STATE CODE CA "+cAnonymousFlag);
				
				
			}else{
				cAnonymousFlag = Y;
			}
			
		}

		LOGGER.info("DesignateSearchController :::: INSIDE getAnaonymousFlagStatus  END ");
		
		return cAnonymousFlag;
	}

	private String loadLanguages(){
		LOGGER.info("getLanguageList : START");
		String jsonLangData = null;

		List<String> languageSpokenList = null;

		languageSpokenList = lookupService.populateLanguageNames("");
		LOGGER.info("getLanguageList : END");
		jsonLangData = platformGson.toJson(languageSpokenList);

		return jsonLangData;
	}

	/**
	 * Retrieves individuals details.
	 *
	 * @param encryptedId
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate result page
	 * @throws InvalidUserException
	 * @throws GIException
	 */
	@RequestMapping(value = "/broker/editindividual/{encryptedId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String editIndividualCaseDetails(@PathVariable("encryptedId") String encryptedId, Model model, HttpServletRequest request) {

		LOGGER.info("individual contact info page ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individuals Details Page");
		String decryptedId = null;
		int houseHoldId;
		AccountUser user  = null;
		try{
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			houseHoldId = Integer.parseInt(decryptedId);
			user = userService.getLoggedInUser();
			if(user.getActiveModuleName().equalsIgnoreCase(BROKER) && !validateBroker(houseHoldId, request)){
				return "redirect:"+ getDashboardPage(request);
			}
			getIndividualDetails(houseHoldId, model);
			request.getSession().setAttribute(LAST_VISITED_URL, "/individual/individualcase/"+encryptedId);
		}
		catch (NumberFormatException exNF){
			String errMsg = "Failed to decrypt houseHoldId. Exception is: "+exNF;
			LOGGER.error(errMsg);
			throw new GIRuntimeException(exNF, ExceptionUtils.getFullStackTrace(exNF), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(InvalidUserException exIU) {
			LOGGER.error("InvalidUser exception encountered in editIndividualCaseDeatils request.");
			throw new GIRuntimeException(exIU, ExceptionUtils.getFullStackTrace(exIU), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred in editIndividualCaseDeatils method.");
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "broker/individuals/editindividualcasedetails";

	}

	/**
	 * Retrieves individual details and set it to model object
	 *
	 * @param id
	 *            Individual Identification Number
	 * @param model
	 *            Model
	 */
	private void getIndividualDetails(Integer id, Model model) {

		LOGGER.info("getIndividualDetails : STARTS");

		Integer startRecord = 0;
		Broker broker = null;
		ConsumerComposite consumerComposite = null;
		DesignateBroker designateBroker = null;
		final String IS_INDIVIDUAL_USER_EXISTS = "isIndividualUserExists";
		designateBroker = designateService.findBrokerByIndividualId(id);

		if(designateBroker!= null){
			broker = brokerRepository.findById(designateBroker.getBrokerId());
		}

		Map<String, Object> map = new HashMap<>();
		Map<String, Object> searchCriteria = new HashMap<String, Object>();
		searchCriteria.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf(id));

		if(null != broker) {
			searchCriteria.put(BrokerConstants.MODULE_ID, broker.getId());
		}

		searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.BROKER);
		searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
		map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);
		List<ConsumerComposite> individualList = new ArrayList<ConsumerComposite>();
		Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);

		if(houseHoldMap!=null && houseHoldMap.size()>0) {
			individualList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
			consumerComposite =  new ConsumerComposite();
			if(!individualList.isEmpty()){
				consumerComposite = individualList.get(0);
			}
		}

		model.addAttribute("individualUser", consumerComposite);
		if(null == consumerComposite || 0 == consumerComposite.getConsumerUserId()) {
			model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, false);
		}
		else {
			model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, true);
		}
		if (null != consumerComposite && StringUtils.isNotBlank(consumerComposite.getPhoneNumber())) {
			model.addAttribute(PHONE1, consumerComposite.getPhoneNumber().substring(ZERO, THREE));
			model.addAttribute(PHONE2, consumerComposite.getPhoneNumber().substring(THREE, SIX));
			model.addAttribute(PHONE3, consumerComposite.getPhoneNumber().substring(SIX, TEN));
		}

		if(designateBroker != null)
		{
			model.addAttribute(STATUS, designateBroker != null ? designateBroker.getStatus() : "");
			String showSwitchRolePopup = designateBroker != null ? designateBroker.getShow_switch_role_popup().toString() : "Y";
			model.addAttribute("showSwitchRolePopup", showSwitchRolePopup);
			model.addAttribute("brokerId", designateBroker.getBrokerId());

			model.addAttribute("isConsumerActivated", isConsumerAccountActivated(designateBroker.getBrokerId(), designateBroker.getIndividualId()));
		}

		LOGGER.info("getIndividualDetails : ENDS");
	}

	protected boolean validateBroker(Integer id, HttpServletRequest request)
			throws InvalidUserException {

		boolean validBroker=false;
		String switchToModuleName = (String) request.getSession().getAttribute(SWITCH_TO_MODULE_NAME);
		//if logged in user is broker/CEC... then check if this broker/CEC is designated to same broker/CEC...or send broker/CEC to defaultLanding page
		AccountUser user = userService.getLoggedInUser();

		if(BROKER.equalsIgnoreCase(switchToModuleName)) {
			//if user is came here with switching then get broker with active-module-id
			Broker broker;
			if("Y".equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW))){
				broker=brokerService.getBrokerDetail(user.getActiveModuleId());
			}else{
				broker=brokerService.findBrokerByUserId(user.getId());
			}

			DesignateBroker designateBroker=designateService.findBrokerByIndividualId(id);
			if(broker ==null || designateBroker==null){
				validBroker=false;
			}else if(   broker.getId()!=designateBroker.getBrokerId() || "InActive".equalsIgnoreCase(designateBroker.getStatus().toString())  ){
				validBroker=false;
			}else{
				validBroker=true;
			}

		}
		return validBroker;
	}

	/**
	 * Method to get dashboard page.
	 *
	 * @param request The current HttpServletRequest instance.
	 * @return <String> The landing page.
	 */
	private String getDashboardPage(HttpServletRequest request) {
		String roleName=(String) request.getSession().getAttribute(SWITCH_TO_MODULE_NAME);
		Role activeRole = roleService.findRoleByName(StringUtils.upperCase( roleName));
		if(BROKER.equalsIgnoreCase(StringUtils.upperCase(roleName))){
			request.getSession().removeAttribute(LAST_VISITED_URL);
			//This if has been added to keep last_visited functionality of Agent switch to employer.
		}
		return activeRole.getLandingPage();
	}

	/**
	 * Retrieves individuals details.
	 *
	 *@param encryptedId
	 * @param request The current HttpServletRequest instance.
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/broker/saveindividualcasedetails/{encryptedId}", method = {RequestMethod.POST })
	public String saveIndividualCaseDetails(@PathVariable("encryptedId") String encryptedId, Model model, HttpServletRequest request) {
		LOGGER.info("individual contact info page ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individuals Details Page");
		String decryptedId = null;
		int houseHoldId;
		AccountUser user  = null;

		try{
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);

			validateIndividualCaseDetailData(request);

			houseHoldId = Integer.parseInt(decryptedId);
			user = userService.getLoggedInUser();

			if(user.getActiveModuleName().equalsIgnoreCase(BROKER) && !validateBroker(houseHoldId, request)){
				return "redirect:"+ getDashboardPage(request);
			}

			saveIndividualDetails(request, houseHoldId, user);
			request.getSession().setAttribute(LAST_VISITED_URL, "/individual/individualcase/"+encryptedId);
		}
		catch (Exception ex){
			String errMsg = "Failed to decrypt houseHoldId. Exception is: "+ex.getMessage();
			LOGGER.error(errMsg, ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "redirect:/individual/individualcase/" + encryptedId;
	}

	/**
	 * Method to validate Individual case details data.
	 *
	 * @param request The current HttpServletRequest instance.
	 * @throws GIRuntimeException The GIRuntimeException instance.
	 */
	private void validateIndividualCaseDetailData(HttpServletRequest request) {
		boolean hasValidationError = false;

		hasValidationError = validatePhoneNumber(request);

		if(!hasValidationError) {
			hasValidationError = validateEmail(request);
		}

		if(hasValidationError) {
			Exception ex=new GIException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
			throw new GIRuntimeException(ex , ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}


	/**
	 * Method to validate email.
	 *
	 * @param request The HttpServletRequest instance.
	 * @return hasValidationError The validation error presence flag.
	 */
	private boolean validateEmail(HttpServletRequest request) {
		boolean hasValidationError = false;
		String email = request.getParameter("email");

		if(org.apache.commons.lang3.StringUtils.isBlank(email)) {
			hasValidationError = true;
		}
		else {
			email = email.trim();

			try {
				if(!Pattern.matches("^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", email)) {
					hasValidationError = true;
				}
			}
			catch(PatternSyntaxException exPS) {
				LOGGER.error("PatternSyntaxException exception occurred.", exPS);
				hasValidationError = true;
			}
		}

		return hasValidationError;
	}


	/**
	 * Method to validate phone number.
	 *
	 * @param request The HttpServletRequest instance.
	 * @return hasValidationError The validation error presence flag.
	 */
	private boolean validatePhoneNumber(HttpServletRequest request) {
		boolean hasValidationError = false;
		String phoneNumber = request.getParameter(PHONE1)+request.getParameter(PHONE2)+request.getParameter(PHONE3);

		if(org.apache.commons.lang3.StringUtils.isBlank(phoneNumber)) {
			hasValidationError = true;
		}
		else {
			phoneNumber = phoneNumber.trim();

			try {
				//Integer intPhoneNumber = Integer.valueOf(phoneNumber);
				if(phoneNumber == null || !phoneNumber.matches("^[1-9]{1}[0-9]{9}$")){
					hasValidationError = true;
				}
				/*
				 * if the phone number exceeds certain value it gives NumberFormatException
				 * so commenting this part as a part of HIX-57149
				 * if(phoneNumber.startsWith("0")) {
					hasValidationError = true;
				}
				else if(phoneNumber.length() != 10) {
					hasValidationError = true;
				}*/
			}
			catch(NumberFormatException exNF) {
				LOGGER.error("NumberFormat exception occurred.", exNF);
				hasValidationError = true;
			}
		}

		return hasValidationError;
	}

	/**
	 * Saves individual details.
	 *
	 * @param request The current HttpServletRequest instance.
	 * @param id The consumer identifier.
	 * @param user The current AccountUser instance.
	 */
	private void saveIndividualDetails(HttpServletRequest request, Integer id, AccountUser user) {

		LOGGER.info("saveIndividualDetails : STARTS");

		try {
			Household household = getHouseHoldById(id);
			//update Household instance
			String phoneNumber = request.getParameter(PHONE1)+request.getParameter(PHONE2)+request.getParameter(PHONE3);
			String email = request.getParameter("email");

			if(null != household) {
				household.setPhoneNumber(phoneNumber);
				household.setEmail(email);

				household.setUpdatedBy(user.getId());
				household.setUpdated(new TSDate(System.currentTimeMillis()));

				household = ghixRestTemplate.postForObject(ConsumerServiceEndPoints.SAVE_HOUSEHOLD_RECORD, household, Household.class);

				if(null != household) {
					DesignateBroker designateBroker=designateService.findBrokerByIndividualId(id);
					Broker broker=brokerService.getBrokerDetail(designateBroker.getBrokerId());

					HashMap<String, Object> parameters = new HashMap<>();
					parameters.put("household", household);
					parameters.put("isAgent", true);
					parameters.put(BROKER, broker);

					String result = brokerMgmtUtils.sendAccountActivationMail(parameters);

					if("SUCCESS".equalsIgnoreCase(result)) {
						LOGGER.info("Activation link successfully sent to individual.");
					}
					else {
						LOGGER.info("Activation link sending to individual was unsuccessful this time.");
					}
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception while saving individual details", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			LOGGER.info("saveIndividualDetails : ENDS");
		}

	}

	/**
	 * Method to get Household for given identifier.
	 *
	 * @param householdId The Household identifier.
	 * @return household The Household instance.
	 */
	private Household getHouseHoldById(int householdId){
		Household household = null;
		XStream xstream = null;
		LOGGER.debug("Rest call to getHouseHoldById starts");

		try{
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, String.valueOf(householdId), String.class);
			xstream = GhixUtils.getXStreamStaxObject();
			if(response != null){
				ConsumerResponse consumerResponse = (ConsumerResponse) xstream.fromXML(response);

				if(null != consumerResponse) {
					household = consumerResponse.getHousehold();
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Rest call to find household failed", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			LOGGER.debug("rest call to getHouseHoldById ends");
		}

		return household;
	}

	/**
	 * Handles the POST request for sending activation link.
	 *
	 * @param encryptedId The encrypted consumer identifier.
	 * @param request The HttpServletRequest instance.
	 * @param response The HttpServletResponse instance.
	 * @return <String> The view name.
	 */
	@RequestMapping(value = "/broker/sendconsumeractivationlink/{encryptedId}", method = {RequestMethod.POST })
	//@PreAuthorize("hasPermission(#model, 'ADD_NEW_CONSUMER')")
	@ResponseBody public String sendConsumerActivationLink(@PathVariable("encryptedId") String encryptedId, HttpServletRequest request, HttpServletResponse response){
		String errorMsg = StringUtils.EMPTY;
		PrintWriter out  = null;
		try {
			out  = response.getWriter();

			request.getSession().setAttribute(LAST_VISITED_URL, "/individual/individualcase/"+encryptedId);
			String decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			Integer householdId = Integer.parseInt(decryptedId);
			AccountUser user = userService.getLoggedInUser();

			if(user.getActiveModuleName().equalsIgnoreCase(BROKER) && !validateBroker(householdId, request)){
				return "redirect:"+ getDashboardPage(request);
			}

			if(null == householdId || 0 == householdId) {
				errorMsg = "Invalid HouseHold identifier.";
			}
			else {
				Household household = getHouseHoldById(householdId);

				DesignateBroker designateBroker = designateService.findBrokerByIndividualId(householdId);
				Broker broker = brokerService.getBrokerDetail(designateBroker.getBrokerId());

				HashMap<String, Object> parameters = new HashMap<>();
				parameters.put("household", household);
				parameters.put("isAgent", true);
				parameters.put(BROKER, broker);

				String result = brokerMgmtUtils.sendAccountActivationMail(parameters);

				if("SUCCESS".equalsIgnoreCase(result)) {
					LOGGER.info("Activation link successfully sent to individual.");
				}
				else {
					LOGGER.info("Activation link sending to individual was unsuccessful this time.");
				}

				errorMsg = result;
			}

			out.print(errorMsg);
		}
		catch (Exception ex) {
			LOGGER.error("Unable to send the activation link: ", ex);
			errorMsg = "Failure";

			if(out !=null) {
				out.print(errorMsg);
			}

			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally{
			IOUtils.closeQuietly(out);
		}

		return errorMsg;
	}

	/**
	 * Method to check if Consumer has activated account or not.
	 *
	 * @param brokerId The broker identifier.
	 * @param householdId The Household identifier.
	 * @return isConsumerActivated The Consumer activated status flag.
	 */
	private boolean isConsumerAccountActivated(Integer brokerId, Integer householdId) {
		boolean isConsumerActivated = true;

		AccountActivation accountActivation =  accountActivationService.getAccountActivationByCreatedObjectId(householdId, GhixRole.INDIVIDUAL.getRoleName(), brokerId, GhixRole.BROKER.getRoleName());

		if(null == accountActivation  || !accountActivation.getStatus().equals(AccountActivation.STATUS.PROCESSED)) {
			isConsumerActivated = false;
		}

		return isConsumerActivated;
	}

//	@GiAudit(transactionName = "Create Ticket Form", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.)
	@RequestMapping(value = "/broker/ticket/create", method = { RequestMethod.GET})
	@PreAuthorize("hasPermission(#model, 'BROKER_ADD_TICKET')")
	@ResponseBody
	public String createTicketForm( ) {
		String resposneString = null;
		StringBuffer responseStringBuff = new StringBuffer();

		try {
					responseStringBuff.append("{\"category\":"+platformGson.toJson(getRequestTypeCategoryMap()));
					responseStringBuff.append(",\"priority\" : "+platformGson.toJson(getPriorityList())+ "}");
					resposneString = responseStringBuff.toString();
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while processing createTicketForm.", giexception);
			throw giexception;
		}
		catch(Exception exception){
			LOGGER.error("Exception occured while processing createTicketForm.", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return resposneString;
	}

	@RequestMapping(value = "/broker/ticket/create/{encryptedId}/{encFromModule}", method = {RequestMethod.POST})
	@PreAuthorize("hasPermission(#model, 'BROKER_ADD_TICKET')")
	@ResponseBody
	public String createTicket(@PathVariable("encryptedId") String encryptedId, @PathVariable("encFromModule") String encFromModule, Model model, HttpServletRequest request) {
		int id;
		String decryptedId = null, decryptedFromModule = null, createdticketNumber = null;
		AccountUser user = null;
		Broker brokerObj = null;
		DesignateBroker designateBroker = null;
		TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
		String category = request.getParameter("requestTypeModel");
		String type = request.getParameter("reqSubTypeModel");
		String priority = request.getParameter("priorityModel");
		String subject = request.getParameter("subjectModel");
		String description = request.getParameter("descriptionModel");
		try {
			decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			id = Integer.parseInt(decryptedId);

			Validate.isTrue(id>0);

			decryptedFromModule = ghixJasyptEncrytorUtil.decryptStringByJasypt(encFromModule);

			Validate.isTrue(StringUtils.isNotBlank(decryptedFromModule));

			user = userService.getLoggedInUser();

			Validate.notNull(user);

			if(userService.hasUserRole(user, RoleService.BROKER_ROLE) || userService.hasUserRole(user, RoleService.ADMIN_ROLE) || userService.hasUserRole(user, RoleService.BROKER_ADMIN_ROLE) || userService.hasUserRole(user, RoleService.OPERATIONS_ROLE)) {
				if(null != decryptedFromModule && decryptedFromModule.trim().equalsIgnoreCase(INDIVIDUALS)) {
					if(Y.equalsIgnoreCase((String)request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW))){
						brokerObj = brokerService.getBrokerDetail(user.getActiveModuleId());
					}
					else{
						brokerObj = brokerService.findBrokerByUserId(user.getId());
					}

					if(BrokerMgmtUtils.isBrokerCertificationStatusNotCertified(brokerObj)){
						throw new AccessDeniedException(ACCESS_IS_DENIED);
					}

					Role agentRoleInstance = roleService.findRoleByName(RoleService.BROKER_ROLE); //Creater role should always be broker

					Validate.notNull(agentRoleInstance);

					designateBroker = findDesignateBrokerBasedOnModule(id, decryptedFromModule);

					Validate.notNull(designateBroker, "Individual has not designated this agent.");
					Validate.notNull(designateBroker.getIndividualId());

					Validate.isTrue(id == designateBroker.getIndividualId().intValue(), "Agent cannot process non designated Individuals.");

					tkmTicketsRequest.setSubject(subject);
					//Required by TKM module
					tkmTicketsRequest.setComment(tkmTicketsRequest.getSubject());
					/*Request type*/
					tkmTicketsRequest.setCategory(category);
					/*Request Sub Type*/
					tkmTicketsRequest.setType(type);

					tkmTicketsRequest.setModuleName("HOUSEHOLD");
					tkmTicketsRequest.setModuleId(designateBroker.getIndividualId());

					tkmTicketsRequest.setPriority(priority);
					//TicketDetails
					tkmTicketsRequest.setDescription(description);
					//CreatedByRole
					tkmTicketsRequest.setUserRoleId(agentRoleInstance.getId()); //Creater role needs to be broker
					tkmTicketsRequest.setCreatedBy(user.getId()); //Logged user

					tkmTicketsRequest.setRequester(brokerObj.getUser().getId()); //Requestor will also be broker for broker to get notification
					tkmTicketsRequest.setLastUpdatedBy(user.getId());

					TkmTickets tkmResponse = ticketMgmtUtils.createNewTicket(tkmTicketsRequest);

					Validate.notNull(tkmResponse);

					createdticketNumber = tkmResponse.getNumber();
					LOGGER.debug("Ticket ID is :" + (tkmResponse != null? createdticketNumber: "null"));
				}
			}
		}
		catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while processing createTicket", giexception);
			throw giexception;
		}
		catch(Exception exception){
			LOGGER.error("Exception occured while processing createTicket.", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return createdticketNumber;
	}

	/**
	 * Method to populate request priorities.
	 *
	 * @return priorityList The priorities.
	 */
	private List<String> getPriorityList() {
		List<String> priorityList = new LinkedList<String>();
		TicketPriority[] ticketPriorities= TkmTickets.TicketPriority.values();

		Validate.notNull(ticketPriorities);
		Validate.notEmpty(ticketPriorities);

		for(TicketPriority ticketPriority: ticketPriorities) {
			priorityList.add(ticketPriority.getTicketPriority());
		}

		return priorityList;
	}

	/**
	 * Method to populate request category types and sub-types.
	 *
	 * @return requestTypeCategoryMap The RequestType Category Map.
	 */
	private Map<String, List<String>> getRequestTypeCategoryMap() {
		List<String> requestTypeCategories = new LinkedList<String>();
		Map<String, List<String>> requestTypeCategoryMap = new HashMap<String, List<String>>();
		List<String> requestSubTypes = null;
		List<TkmWorkflows> ticketTypeList = ticketMgmtUtils.getTicketTypeList();

		Validate.notNull(ticketTypeList);
		Validate.notEmpty(ticketTypeList);

		for(TkmWorkflows tkmWorkflows: ticketTypeList) {
			//TKM_WORKFLOW_DEFAULT tickets
			if(null != tkmWorkflows && null != tkmWorkflows.getPermission() && TKM_WORKFLOW_DEFAULT.equalsIgnoreCase(tkmWorkflows.getPermission())) {
				//HIX-98074: Restricting "Request" category to not to show up in Contact YHI pop up
				if(!tkmWorkflows.getCategory().equalsIgnoreCase("Request")){
					requestTypeCategories.add(tkmWorkflows.getCategory());
				}
			}
		}

		if(!requestTypeCategories.isEmpty()) {
			for(String requestTypeCategory: requestTypeCategories) {
				if(null != requestTypeCategory && !requestTypeCategory.isEmpty()) {
					requestSubTypes = new LinkedList<String>();

					for(TkmWorkflows tkmWorkflows: ticketTypeList) {
						if(null != tkmWorkflows && null != tkmWorkflows.getPermission() && TKM_WORKFLOW_DEFAULT.equalsIgnoreCase(tkmWorkflows.getPermission()) && requestTypeCategory.equals(tkmWorkflows.getCategory())) {
							requestSubTypes.add(tkmWorkflows.getType());
						}
					}

					requestTypeCategoryMap.put(requestTypeCategory, requestSubTypes);
				}
			}
		}

		return requestTypeCategoryMap;
	}

	@RequestMapping(value = "/broker/sendindividualactivationlink/{encryptedId}", method = {RequestMethod.POST })
	//@PreAuthorize("hasPermission(#model, 'ADD_NEW_CONSUMER')")
	@ResponseBody
	public String processIndividualActivationLink(@PathVariable("encryptedId") String encryptedId, HttpServletRequest request){
		String responseMsg = StringUtils.EMPTY;
		try {

			String decryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);
			Integer householdId = Integer.parseInt(decryptedId);

			if(validateIndividualDetails(request)){
				return "Invalied input";
			}

			String phoneNumber = request.getParameter(PHONE1)+request.getParameter(PHONE2)+request.getParameter(PHONE3);
			String email = request.getParameter("email");

			if(isUserExistInSys(email)){
				return "userExists";
			}

			AccountUser user = userService.getLoggedInUser();

			if(null == householdId || 0 == householdId) {
				return "Invalid HouseHold identifier.";
			}

			Household household = getHouseHoldById(householdId);

			household.setEmail(email);
			household.setPhoneNumber(phoneNumber);
			household = updateIndividual(household,user);

			responseMsg = sendActivationlink(household);

			}catch(Exception e){
				LOGGER.error("Exception occured while sending individual activation link:", e);
				throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
		return responseMsg;
	}

	private Household updateIndividual(Household household, AccountUser user){
		household.setUpdatedBy(user.getId());
		household.setUpdated(new TSDate(System.currentTimeMillis()));

		household = ghixRestTemplate.postForObject(ConsumerServiceEndPoints.SAVE_HOUSEHOLD_RECORD, household, Household.class);
		return household;
	}

	private boolean validateIndividualDetails(HttpServletRequest request){
		boolean hasValidationError = false;

		hasValidationError = validatePhoneNumber(request);

		if(!hasValidationError) {
			hasValidationError = validateEmail(request);
		}
		return hasValidationError;
	}

	private boolean isUserExistInSys(String email) {
		AccountUser emailObj = null;
		boolean isUserPresent = false;

		if (isScimEnabled) {
			try {
				emailObj = scimUserManager.findByEmail(email.toLowerCase());
				if(emailObj != null) {
					isUserPresent = true;
				}
			} catch (Exception ex) {
				LOGGER.error("ERR OCCURED OR USR NOT FOUND", ex);
			}

		} else {
			try {
				emailObj = userService.findByUserName(email.toLowerCase());
				if (emailObj != null) {
					isUserPresent = true;
				}
			} catch (Exception e) {
				LOGGER.error(
						"Exception occured while sending individual activation link:",
						e);
				throw new GIRuntimeException(e,
						ExceptionUtils.getFullStackTrace(e), Severity.HIGH,
						GIRuntimeException.Component.AEE.toString(), null);
			}
		}
		return isUserPresent;
	}

	private String sendActivationlink(Household household) {
		String responseMsg = StringUtils.EMPTY;
		try {
			if (null != household) {
				DesignateBroker designateBroker = designateService.findBrokerByIndividualId(household.getId());
				Broker broker = brokerService.getBrokerDetail(designateBroker.getBrokerId());

				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("household", household);
				parameters.put("isAgent", true);
				parameters.put(BROKER, broker);

				String result = brokerMgmtUtils
						.sendAccountActivationMail(parameters);

				if ("SUCCESS".equalsIgnoreCase(result)) {
					LOGGER.info("Activation link successfully sent to individual.");
					responseMsg = "linkSent";
				} else {
					LOGGER.info("Activation link sending to individual was unsuccessful this time.");
					responseMsg = "linkSentFail";
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while sending individual activation link:", e);
			throw new GIRuntimeException(e,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return responseMsg;
	}

	 @RequestMapping(value = "/broker/viewbrokerprofile/{encBrokerId}", method = RequestMethod.GET)
 	 public String viewprofile(Model model, @PathVariable(value = "encBrokerId") String encBrokerId) {
              LOGGER.info("View Broker Profile");
              model.addAttribute(PAGE_TITLE,
                              "Getinsured Health Exchange:Employer Home Page");
              String id = null;
              try{
                      id = ghixJasyptEncrytorUtil.decryptStringByJasypt(encBrokerId);

                      Broker brokerObj = new Broker();
                      // added to show broker details from employer menu navigation
                      if (id != null && !"".equals(id)) {
                              brokerObj = brokerService.getBrokerDetail(Integer.valueOf(id));
                      }

                      model.addAttribute("broker", brokerObj);
                      model.addAttribute("locationObj", brokerObj.getLocation());
              }catch(NumberFormatException nfe){
                      String errMsg = "Error occured in decrypting broker id. Decrypted value is: "+ id +", Exception is: "+nfe;
                      LOGGER.error("View Broker Profile - "+errMsg);
              }

              return "broker/viewbrokerprofile";
      }
}
