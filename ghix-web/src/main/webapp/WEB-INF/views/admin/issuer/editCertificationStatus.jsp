<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>

<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
<c:set var="unEncPaymentId" value="${curent_payment_id}" />
	<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
				<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
				<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
				<li><spring:message  code="pgheader.certificationStatus"/></li>
			</ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1>${issuerObj.name}</h1>
			</div>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="certStatus"/>
				</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
	           	<div class="header">
	                   <h4 class="pull-left"><spring:message  code="pgheader.certificationStatus"/></h4>
	                 <c:choose>
	                  <c:when test="${exchangeType == 'PHIX'}">
	                 	<a class="btn btn-primary btn-small pull-right margin5-lr" href="javascript:void(0)" onclick="javascript:submitFormForPHIX();" ><spring:message  code="label.save"/></a>
 					  </c:when>                 	
	                 <c:otherwise>
	                 	<a class="btn btn-primary btn-small pull-right margin5-lr" href="javascript:void(0)" onclick="javascript:submitForm();" ><spring:message  code="label.save"/></a>
	                 </c:otherwise>
	                </c:choose>				
						<a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/certification/status/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a>
	               </div>
				<div class="gutter10">
					<form class="form-horizontal" id="frmIssuerCertStatus" name="frmIssuerCertStatus" action="<c:url value="/admin/changeissuerstatus"/>" method="POST">
						<df:csrfToken/>
						
						<input type="hidden" id="issuerId" name="issuerId" value="<encryptor:enc value="${issuerObj.id}"></encryptor:enc>"/>
						<input type="hidden" id="target_name" name="target_name" value="<encryptor:enc value="ISSUER"></encryptor:enc>"/>
						<input type="hidden" id="target_id" name="target_id" value="<encryptor:enc value="${issuerObj.id}"></encryptor:enc>"/> 
						<input type="hidden" id="curent_payment_id" name="curent_payment_id" value="<encryptor:enc value="${curent_payment_id}"></encryptor:enc>"/>
						
						<div class="control-group">
							<label class="control-label" for="issuerName"><spring:message  code="label.issuer"/></label>
							<div class="controls paddingT5"><strong>${issuerObj.name}</strong></div>
						</div><!-- end of control-group-->   
										 
						<div class="control-group">
							<label class="control-label" for="comp-logo"><spring:message  code="label.NAICCompanyCode"/></label>
							<div class="controls paddingT5"><strong>${issuerObj.naicCompanyCode}</strong></div>
						</div><!-- end of control-group-->
						
						<div class="control-group">
							<label class="control-label" for="state"><spring:message  code="label.NAICGroupCode"/></label>
							<div class="controls paddingT5"><strong>${issuerObj.naicGroupCode}</strong></div>
						</div><!-- end of control-group-->                    
							  
						<div class="control-group">
							<label class="control-label" for="currentStatus"><spring:message  code="label.currentStatus"/></label>
							<div class="controls paddingT5"><strong>${issuerObj.certificationStatus}</strong></div>
						</div><!-- end of control-group-->
						
						<div class="control-group">
							<label class="control-label" for="certAuthority"><spring:message  code="label.issuerUploadAuthority"/></label>
							<div class="controls paddingT5">
								<c:forEach var="authorityFileObj" items="${authorityFile}">
									<div><a href="<c:url value="/admin/document/filedownload?documentId=${authorityFileObj.uploadedFileLink}&fileName=${authorityFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
								</c:forEach>
							</div>
						</div><!-- end of control-group-->
						
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.issuerUploadMarketingReq"/></label>
							<div class="controls paddingT5">
								<c:forEach items="${marketingFile}" var="marketingFileObj">
									<div><a href="<c:url value="/admin/document/filedownload?documentId=${marketingFileObj.uploadedFileLink}&fileName=${marketingFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
								</c:forEach>
							</div>
						</div><!-- end of control-group-->
						
						<div class="control-group">
							<label class="control-label"><spring:message  code="label.financialDisclosures"/></label>
							<div class="controls paddingT5">
								<c:forEach items="${financeDisclosuresFile}" var="disclosuresFileObj">
									<div><a href="<c:url value="/admin/document/filedownload?documentId=${disclosuresFileObj.uploadedFileLink}&fileName=${disclosuresFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
								</c:forEach>
							</div>
						</div><!-- end of control-group-->
						
						<div class="control-group">
							<label class="control-label" for="currentStatus"><spring:message  code="label.issuerUploadAccreditation"/></label>
							<div class="controls paddingT5">
								<c:forEach items="${accreditationFile}" var="accreditationFileObj">
									<div><a href="<c:url value="/admin/document/filedownload?documentId=${accreditationFileObj.uploadedFileLink}&fileName=${accreditationFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
								</c:forEach>
							</div>
						</div><!-- end of control-group-->
											
						<div class="control-group">
							<label class="control-label" for="currentStatus"><spring:message  code="label.organizationData"/></label>
							<div class="controls paddingT5">
								<c:forEach items="${organizationFiles}" var="organizationFileObj">
									<div><a href="<c:url value="/admin/document/filedownload?documentId=${organizationFileObj.uploadedFileLink}&fileName=${organizationFileObj.uploadedFileName}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
								</c:forEach>
							</div>
						</div><!-- end of control-group-->
						
						<div class="control-group">
							<label class="control-label" for="certificationStatus"><spring:message  code="label.certificationStatus"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10px" height="10px" alt="Required!" /></label>
							<div class="controls paddingT5">
								<select id="certificationStatus" name="certificationStatus">
									   <option value=""><spring:message  code="label.selectUpperCase" /></option>
									  <c:forEach var="certStatusObj" items="${certificationStatusList}">
										<option value="${certStatusObj.key}">${certStatusObj.value}</option>
									</c:forEach>
								</select>
								<div id="certificationStatus_error" class="help-inline"></div>
							</div>
						</div><!-- end of control-group-->
						
						<div class="control-group">
							<label class="control-label" for="comment_text"><spring:message  code="label.comment"/></label>
							<div class="controls paddingT5">
								<textarea class="input-xlarge" name="comment_text" id="comment_text" rows="4" cols="40" style="resize: none;" 
								maxlength="500" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();" onKeyPress="return(this.value.length < 500 );"></textarea>
								<div id="comment_text_error"  class="help-inline"></div>
								<div id="chars_left"><spring:message  code="label.charLeft"/> <b><spring:message  code="label.characterLeft"/></b></div>
							</div>
						</div><!-- end of control-group-->
						
						
						<div class="control-group">
							<label class="control-label" for="certSuppDoc"><spring:message  code="label.uploadFile"/></label>
							<div class="controls paddingT5">
							<input type="file" class="input-file" id="certSuppDoc" name="certSuppDoc" accept=".jpg,.jpeg,.png,.doc,.docx,.xls,.xlsx,.pdf" onchange="validateFileupload(this);" >
							<button type="submit" id="btnCertSuppDoc" name="btnCertSuppDoc" class="btn" ><spring:message code='label.upload'/></button>
							<div id="certSuppDoc_display">${certiSuppDoc}</div>
							<div id="certSuppDoc_error"  class="help-inline"></div>
							<input type="hidden" id="hdnCertSuppDoc" name="hdnCertSuppDoc" value=""/>
							<input type="hidden" id="fileTypes" name="fileTypes" value="<c:forEach var="fileType" items="${mimeTypeArray}">${fileType} </c:forEach>"/>
							<input type="hidden" id="successFullyUploaded" name="successFullyUploaded" value="${successFullyUploaded}"/>
							</div>
						</div><!-- end of control-group-->
					</form>				
				</div>
				<div class="form-actions">
				<a class="btn cancel-btn" href="<c:url value="/admin/issuer/certification/status/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a>
				<c:choose>
					<c:when test="${exchangeType == 'PHIX'}">
						<a class="btn btn-primary" href="javascript:void(0)" onclick="javascript:submitFormForPHIX();" ><spring:message  code="label.save"/></a>
					</c:when>
					<c:otherwise>
						<a class="btn btn-primary" href="javascript:void(0)" onclick="javascript:submitForm();" ><spring:message  code="label.save"/></a>
					</c:otherwise>
				</c:choose>
				</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
	
	<div id="de-certify-modal" class="modal hide fade in" style="display:none;">
		<div class="modal-header">
			<a class="close statusModalClose">&times;</a>
			<h3>
				<spring:message code='title.changeStatus'/>
			</h3>
		</div>
		<div class="modal-body">
			<spring:message code="label.certificationChangeWarning"/>
		</div>
		<div class="modal-footer">
			<input type="button" class="btn btn-primary btn-small statusSubmit" value="Yes" />
			<input type="button" class="btn statusModalClose btn-small" value="No">
		</div>
	</div>	
	
	<div id="certi-status-modal" class="modal hide fade in" style="display:none;">
		<div class="modal-header">
			<a class="close statusModalClose">&times;</a>
			<h3>
				<spring:message code='title.changeCertificationStatus'/>
			</h3>
		</div>
		<div class="modal-body">
			<spring:message code="label.certificationUpdatestatus"/>
		</div>
		<div class="modal-footer">
			<input type="button" class="btn btn-primary btn-small statusSubmit" value="Yes" />
			<input type="button" class="btn statusModalClose btn-small" value="No">
		</div>
	</div>
	
	
	<div id="activeEnrollmentErrDiv" class="modal hide fade in" style="display:none;">
		<div class="modal-header">
			<a class="close" data-dismiss="modal" data-original-title="">&times;</a>
			<h3 class="span5">
				<spring:message code="err.modelTitleError" />
			</h3>
		</div>
		<div class="modal-body" id="displayContent">
			<spring:message code="err.activeEnrollment" />
		</div>
		<div class="footer margin20-b txt-center">
			<a class="btn btn-primary" data-dismiss="modal"><spring:message code="label.close" /></a>
		</div>
	</div>

</div>

<%
	String activeEnrollment = request.getParameter("activeEnrollment");
	if (StringUtils.isNotBlank(activeEnrollment)) {
%>
	<script type="text/javascript">
	    $("#activeEnrollmentErrDiv").modal("show"); 
    </script>
<% 
 } 
%>

<script type="text/javascript">
var financialInfo = '${isFinancialInfoRequired}';
var commentMaxLen = 500;
var csrValue = $("#csrftoken").val();

function validateFileupload(input_element){
    var el = document.getElementById("certSuppDoc_error");
    var fileName = input_element.value;
  	var fileType=input_element.files[0].type;
 	var allowedFileTypes = document.getElementById("fileTypes").value.trim().split(" ");
    for(var i = 0; i < allowedFileTypes.length; i++)
    {
        if(allowedFileTypes[i]==fileType)
        {
            el.innerHTML = "";
            return;
        }
    }
    input_element.value = '';
    el.innerHTML="Failed to upload file. Please make sure that the file of correct type is selected.";
   
}
$(document).ready(function(){
	$("#btnCertSuppDoc").click(function(){
		if($("#certSuppDoc").val() != ""){  // if file browsed then do upload 
			$("#certSuppDoc_error").html("");
			$('#frmIssuerCertStatus').ajaxSubmit({
				url: "<c:url value='/admin/uploadissuercertsuppdoc?issuerid=${issuerObj.id}'/>",				
				data: {"csrftoken":csrValue},
				success: function(responseText){ /* For IE*/
						if(responseText != ""){
							var responseArray = responseText.split('|'); // split the response
							if(responseArray[0]=="FAILURE" || responseArray[0]=="SIZE_FAILURE"){                                                       
				                  $('#certSuppDoc_error').html("File size exceeds the limit");
				                }else if(responseArray[0]=="INVALID_FILE_TYPE"){
									$('#certSuppDoc_error').html("Failed to upload file. Please make sure that the file of correct type is selected.");
								}
				                else{
									$("#certSuppDoc_display").text(responseArray[0]); // show original file name
									$("#hdnCertSuppDoc").val(responseArray[1]); // store DMS id
									$("#successFullyUploaded").val("successFullyUploaded");
									$('#certSuppDoc').attr({ value: '' });
				                }
						}else{
							$("#certSuppDoc_error").html("Failed to upload file. Please make sure that the file of correct type is selected.");
						}	
		       	},error: function(responseText){  /* For FF*/
		       		if(responseText.responseText.indexOf('Missing CSRF token') !== -1){
		       			$("#certSuppDoc_error").html("Missing CSRF token");
		       		}
		       		else{
			       		var parsedJSON = responseText;
			       		var val = parsedJSON.responseText.split("|");
			       		if(val != ""){
			                if(val[0]=="FAILURE" || val[0]=="SIZE_FAILURE"){                                                       
			                  $('#certSuppDoc_error').html("File size exceeds the limit");
			                }else if(val[0]=="INVALID_FILE_TYPE"){
								$('#certSuppDoc_error').html("Failed to upload file. Please make sure that the file of correct type is selected.");
							}
			                else{
					           $("#certSuppDoc_display").text(val[0]); // show original file name
					           $("#hdnCertSuppDoc").val(val[1]); // store DMS id
					           $("#successFullyUploaded").val("successFullyUploaded");
				            }
				         }else{
				          	$("#certSuppDoc_error").html("Failed to upload file. Please make sure that the file of correct type is selected.");
				         }
		       		}
		       	}
			});
		}else{
			$("#certSuppDoc_error").html("Please select file to upload");
		}	
	  return false; 
	});	// button click close
	
	
});

function submitForm(){		
	 if(($("#certificationStatus").val() == "RECERTIFIED" || $("#certificationStatus").val() == "CERTIFIED") &&  '${unEncPaymentId}' == 0  ){
			if('${isFinancialInfoRequired}' == 'YES'){
				alert("<spring:message  code='alert.certificationWarning' javaScriptEscape='true'/>");
				return false;
			}else if(financialInfo == 'NO') {
				$("#frmIssuerCertStatus").submit();
			}
	}else{
		$("#frmIssuerCertStatus").submit();
	} 
}

function submitFormForPHIX(){	
		$("#frmIssuerCertStatus").submit();
}


// form validation
var validator = $("#frmIssuerCertStatus").validate({ 
	rules : {
		certificationStatus : {required : true}
	},
	messages : {
		certificationStatus : { required : "<span><em class='excl'>!</em><spring:message  code='err.selectCertStatus' javaScriptEscape='true'/></span>"}
		},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');		
	},
	/* submitHandler: function(form) {
		submitComments(form);	// submit comments		       
    } */
});

// Update remaining characters for comments
function updateCharCount(){	
	var currentLen = $.trim(document.getElementById("comment_text").value).length;
	var charLeft = commentMaxLen - currentLen;
	if(currentLen > commentMaxLen) {
		$("#comment_text").val($("#comment_text").val().substr(0, commentMaxLen));
		$('#chars_left').html('Characters left <b>' + 0 + '</b>' );
    }
	$('#chars_left').html('Characters left <b>' + charLeft + '</b>' );
}

$("#certificationStatus").change(function(){
	if($("#certificationStatus").val()=="DECERTIFIED"){
		$("#de-certify-modal").modal("show");
	}else if($("#certificationStatus").val()!=""){
		$("#certi-status-modal").modal("show");
	}
});

$('.statusModalClose').click(function(){
	$("#certificationStatus").val("");
	hideCertifiedStatusModal();
});

$('.statusSubmit').click(function(){
	hideCertifiedStatusModal();
});

function hideCertifiedStatusModal(){
	$("#de-certify-modal, #certi-status-modal").modal("hide");
}

</script>
