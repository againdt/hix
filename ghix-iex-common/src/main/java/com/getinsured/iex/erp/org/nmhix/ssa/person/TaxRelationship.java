package com.getinsured.iex.erp.org.nmhix.ssa.person;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxRelationship", propOrder = {
    "toHousehold",
    "fromHousehold",
    "relationshipType",
    "married",
    "marriedTo"
    
})
public class TaxRelationship {

	 @XmlElement(required = true)
	 protected HouseholdMember toHousehold;
	 @XmlElement(required = true)
	 protected HouseholdMember fromHousehold;
	 @XmlElement(required = true)
	 protected RelationshipTypeEnum relationshipType;
	 
	 protected HouseholdMember married;
	 protected HouseholdMember marriedTo;
	 
	 
	public HouseholdMember getMarried() {
		return married;
	}
	public void setMarried(HouseholdMember married) {
		this.married = married;
	}
	public HouseholdMember getMarriedTo() {
		return marriedTo;
	}
	public void setMarriedTo(HouseholdMember marriedTo) {
		this.marriedTo = marriedTo;
	}
	public HouseholdMember getToHousehold() {
		return toHousehold;
	}
	public void setToHousehold(HouseholdMember toHousehold) {
		this.toHousehold = toHousehold;
	}
	public HouseholdMember getFromHousehold() {
		return fromHousehold;
	}
	public void setFromHousehold(HouseholdMember fromHousehold) {
		this.fromHousehold = fromHousehold;
	}
	public RelationshipTypeEnum getRelationshipType() {
		return relationshipType;
	}
	public void setRelationshipType(RelationshipTypeEnum relationshipType) {
		this.relationshipType = relationshipType;
	}
}
