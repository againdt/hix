<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<c:if test="${activationLinkResult == 'SUCCESS'}">
<script type="text/javascript">
$(document).ready(function() {
	 $('<div id="sendActivationLinkpopup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="sendActivationLinkpopup" aria-hidden="true"><div class="markCompleteHeader"><div class="header"><h4 class="margin0 pull-left"><spring:message code="label.sendActivationEmailStatus"/></h4><button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button></div></div><div class="modal-body"><div class="control-group">	<div class="controls"><spring:message code="label.sendActivationLinkMessage"/></div></div></div><div class="modal-footer clearfix"><button class="btn btn" data-dismiss="modal" aria-hidden="true">  <spring:message code="label.agent.details.okbtn"/>  </button></div></div>').modal();
});
</script>
</c:if>
<script type="text/javascript">


  $(function() {
    $(".newcommentiframe").click(function(e){
          e.preventDefault();
          var href = $(this).attr('href');
          if (href.indexOf('#') != 0) {
             $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" scrolling="no"  src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal();
      }
    });
  });
  
  function setPopupValue() {
		if(document.getElementById("frmPopup").elements['empchkshowpopup'].checked) {
			document.frmPopup.showPopupInFuture.value = "N";
		}
  }
    
  
  function closeIFrame() {
	  
	  $("#newcommentiframe, .modal-backdrop").remove();
	  window.location.href = '/hix/broker/employer/employercomments/'+'${encryptedId}';
  }
  function closeCommentBox()
  {	  	  
	  $("#newcommentiframe").remove();	  
	  var url = '/hix/broker/employer/employercomments/'+'${encryptedId}';
	  window.location.assign(url);
  }
</script>
<!--start page-breadcrumb -->
<div class="gutter10-lr">
	<c:set var="encryptedId" ><encryptor:enc value="${employer.id}" isurl="true"/> </c:set>
	<c:set var="encFromModule" ><encryptor:enc value="employer" isurl="true"/> </c:set>
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/broker/employers"/>"><spring:message code="label.employers"/></a></li>
			<li><spring:message code="label.brkactive"/></li>
			<li>${employer.name}</li>
			<li><spring:message code="label.agent.employers.Summary"/></li>
		</ul>
	</div>
	<!--page-breadcrumb ends-->

	<div class="row-fluid">
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>


	<!--  Latest UI -->
	<c:if test="${message != null}">
		<div class="errorblock alert alert-info">
			<p>${message}</p>
		</div>
	</c:if>
     <div class="row-fluid">
	    <div id="sidebar" class="span3">
	       <div class="nav nav-list">
		      <h1>${employer.name}</h1>
		   </div>
		</div>
	 </div>
	 <%-- <h1>
		<span class="span3">${employer.name}</span> <small class="span9"><spring:message code="label.brkcontactname"/>:
			${employer.contactFirstName} ${employer.contactLastName}</small>
	</h1>  --%>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4><spring:message code="label.agent.employers.aboutThisEmployer"/></h4>
			</div>
			<ul class="nav nav-list">
				<%--                        <li class="active"><a href="<c:url value="/shop/employer/employercasesummary/${employer.id}"/>">Summary</a></li> --%>
				<%--                        <li><a href="<c:url value="/shop/employer/employercasecontactinfo/${employer.id}"/>">Contact Info</a></li> --%>
				<li class="active"><spring:message code="label.agent.employers.Summary"/></li>
				<li><a
					href="/hix/broker/employer/employercontactinfo/${encryptedId}"><spring:message code="label.agent.employers.contactInformation"/></a></li>
				<%-- <c:if test="${!isNMCall}">
						<li><c:url value="/inbox/secureInboxSearch" var="searchURL">
							<c:param name="searchText" value="${employer.contactFirstName} ${employer.contactLastName}"/>
					    </c:url> <a href="${searchURL}"><spring:message code="label.agent.employers.Messages"/></a></li>
				</c:if>	 --%>						
				<!--  <li><a href="/hix/shop/employer/composemessage/${employer.id}">Notifications</a></li> -->
				<li><a
					href="/hix/broker/employer/employercomments/${encryptedId}"><spring:message code="label.agent.employers.Comments"/></a></li>
			</ul>
			<br>
			<div class="header">
				<h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
			</div>
			<ul class="nav nav-list">
				  <li>
				  	<c:choose>
						<c:when test="${showSwitchRolePopup == \"N\"}">
			                 <c:if test="${showPopupInFuture == null}">  
								<a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a>
								
							</c:if>
							<c:if test="${showPopupInFuture != null}">
						 		<a href="<c:url value="/broker/employer/dashboard?switchToModuleName=employer&switchToModuleId=${employer.id}&switchToResourceName=${employer.name}&showPopupInFuture=on"/>" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a>
							</c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${showPopupInFuture == null}">
								<a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a>  
							</c:if>
							<c:if test="${showPopupInFuture != null}">
						 		<a href="<c:url value="/broker/employer/dashboard?switchToModuleName=employer&switchToModuleId=${employer.id}&switchToResourceName=${employer.name}&showPopupInFuture=on"/>" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a>
							</c:if>
						</c:otherwise>
					</c:choose>
				 </li>				 
				<%-- <c:if test="${!isNMCall}">
					<li><a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();">
					<i class="icon-envelope-unread"></i> <spring:message code="label.agent.employers.ComposeMessage"/></a></li>
				</c:if>	 --%>			 
				<!--  <li><a href="#new-msg" class="btn span11 btn-block btn-primary" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();">-->

				<li><a name="addComment"
					href="<c:url value="/broker/employer/newcomment?encryptedId=${encryptedId}&target_name=DESIGNATEBROKER&employerName=${employer.name}"/>"
					id="addComment" class="newcommentiframe"> <i
						class="icon-comment"></i><spring:message code="label.agent.employers.NewComment"/>
				</a></li>			
				<c:if test="${employerActivated == false}">
					<li><a href="<c:url value="/broker/sendActivationLink/${encryptedId}"/>" class=""><spring:message code="label.sendActivationLink"/></a></li>										
				</c:if>
			</ul>
		</div>
		<!-- Modal -->
		<%-- Secure Inbox Start--%>
		<%-- <jsp:include page="../../inbox/includeInboxCompose.jsp"></jsp:include> --%>
		<%-- Secure Inbox End--%>

		<div class="span9" id="rightpanel">
			 <div class="header">
			       <h4 class="span11"><spring:message code="label.agent.employers.Summary"/></h4>	
		     </div>
		    <form class="form-horizontal" id="" name="" action="" method="POST">
		    	<div class="gutter10">
				 <table class="table table-border-none">	
					<tbody>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.agent.employers.BusinessName"/></th>
							<td><strong>${employer.name}</strong></td>
						</tr>
						<tr>
							<th class="span4 txt-right" scope="row"> <spring:message code="label.agent.employers.FederalEIN"/></th>
							<td><strong>${employer.federalEIN}</strong></td>
						</tr>
						<tr class="nmhide"> <!-- HIX-18969 -->
							<th class="span4 txt-right" scope="row"><spring:message code="label.agent.employers.StateEIN"/></th>
							<td><strong>${employer.stateEIN}</strong></td>
						</tr>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.agent.employers.TotalEmployees"/></th>
							<td><strong>${totalEmp}</strong></td>
						</tr>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.agent.employers.AverageSalary"/></th>
							<td>
								<strong> 
									<c:choose>
										<c:when test="${'N/A' eq avgSal}">${avgSal}</c:when>
										<c:otherwise>
											<c:if test="${fn:contains(avgSal, '$')}"> ${avgSal} </c:if> <c:if test="${not fn:contains(avgSal, '$')}"> $${avgSal} </c:if>
										</c:otherwise>
									</c:choose>
								</strong>
							</td>
						</tr>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.agent.employers.EligibilityStatus"/></th>
							<td><strong>${employer.eligibilityStatus}</strong></td>
						</tr>
					</tbody>
				</table>
				</div>
				<input type="hidden" name="employerName" id="employerName"
					value="${employer.name}" />
			</form>

			<!-- Modal -->
				<jsp:include page="employerviewmodal.jsp" />
			<!-- Modal end -->
		</div>
	</div>
	<!-- row-fluid -->
</div>


<!-- gutter10 -->
<!--  Latest UI -->