(function(){
	'use strict';
	
	angular.module("resetPasswordApp").controller('AuthenticationInformationCtrl', AuthenticationInformationCtrl);
	angular.module("resetPasswordApp").controller('EmailSentCtrl', EmailSentCtrl);
	
	AuthenticationInformationCtrl.$inject = ['$http', '$state','$scope'];
	function AuthenticationInformationCtrl($http, $state,$scope){
		var vm = this;
		
		var validationUrl = '/hix/crm/consumer/validatepwdresetquestions/' + $('#encHouseHoldId').val();
		var alternateEmailUrl = '/hix/crm/consumer/sendresetpasswordemail/' + $('#encHouseHoldId').val();
		
		vm.answers = {};
		
		vm.submitForm = submitForm;
		vm.submitAlternateEmail = submitAlternateEmail;
		
		_init();
		
		
		function _init(){
			var responseJson = angular.fromJson($('#responseJson').val());
			
			vm.showQuestions = false;
			if(responseJson.STATUS === 'ERROR'){
				vm.showServerErrorMessage = true;
				vm.serverErrorMessage = responseJson.ERROR_MESSAGE;
			}
			else if(responseJson.STATUS === 'ACCOUNT_LOCKED'){
				vm.accountLock = true;
			}
			else{
				vm.showQuestions = true;
			vm.questions = responseJson.QUESTIONS;
		}
		
		}
		
		function submitForm(){
			vm.loader = true;
			vm.showValidationErrorMessage = false;
			vm.showServerErrorMessage = false;
			
			var promise = $http.post(validationUrl, vm.answers);
			
			promise.success(function(response){
				vm.loader = false;
				if(response.STATUS === 'OK'){
					if(response.OVERRIDE_EMAIL){
						//CSR supervisor
						//Reset data
						vm.alternateEmail = {};
						
						$('#alternateEmailModal').modal({
							backdrop: 'static', 
							keyboard: false
						})
					}else{
						goSuccessEmailPage(response.EMAIL_ID);
					}
	
				}else if(response.STATUS === 'ALLOW_RETRY'){
					vm.questions = response.QUESTIONS;
					vm.dynamicSecurityQuestions.$setPristine();
					vm.showQuestions = true;
					vm.showValidationErrorMessage = true;
					vm.remainingRetries = response.REMAINING_RETRIES;
					vm.answers = {};
				}else if(response.STATUS === 'ERROR'){
					vm.showServerErrorMessage = true;
					vm.showQuestions = false;
					vm.serverErrorMessage = response.ERROR_MESSAGE;
				}else{
					vm.accountLock = true;
					vm.showQuestions = false;
				}
				
			});

			promise.error(function(){
				vm.loader = false;
			});
		}

		function submitAlternateEmail(){
			vm.loader = true;
			var promise = $http.post(alternateEmailUrl, vm.alternateEmail);
			
			promise.success(function(response){
				vm.loader = false;
				goSuccessEmailPage(response.EMAIL_ID);	
			});

			promise.error(function(response){
				vm.loader = false;
				alert(response.ERROR_MESSAGE);
			});
		}
		
		function goSuccessEmailPage(email){
			$state.go('successEmail', {email: email});
	}
	};
	
	
	EmailSentCtrl.$inject = ['$stateParams'];
	function EmailSentCtrl($stateParams){
		var vm = this;
		
		vm.email = $stateParams.email;
	};
	
})();
