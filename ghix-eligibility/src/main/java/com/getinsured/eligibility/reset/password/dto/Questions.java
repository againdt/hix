package com.getinsured.eligibility.reset.password.dto;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class Questions {
	@NotEmpty @Valid
	private List<Question> questionList;
}
