/************************************************************

 # CommonAction type for State clearing

 ************************************************************/
const CLEAR_STATE = 'CLEAR_STATE';
export const commonActionTypes = {
  CLEAR_STATE
};

/************************************************************

 Spinner action types

 ************************************************************/
const START_SPINNER = 'START_SPINNER';
const STOP_SPINNER = 'STOP_SPINNER';

export const spinnerActionTypes = {
  START_SPINNER,
  STOP_SPINNER,
  CLEAR_STATE
};

/************************************************************

 Help On Demand action types

 ************************************************************/
const SET_ON_DEMAND_DATA = 'SET_ON_DEMAND_DATA';
const SUBMIT_ON_DEMAND_DATA = 'SUBMIT_ON_DEMAND_DATA';
const SUBMIT_ACTIVE_STATUS = 'SUBMIT_ACTIVE_STATUS';

export const onDemandActionTypes = {
  SET_ON_DEMAND_DATA,
  SUBMIT_ON_DEMAND_DATA,
  SUBMIT_ACTIVE_STATUS
};

const SET_BROKER_CONNECT_DATA = 'SET_BROKER_CONNECT_DATA';
const SUBMIT_BROKER_CONNECT_DATA = 'SUBMIT_BROKER_CONNECT_DATA';

export const brokerConnectActionTypes = {
  SET_BROKER_CONNECT_DATA,
  SUBMIT_BROKER_CONNECT_DATA
};

const SET_BROKER_AVAILABILITY = 'SET_BROKER_AVAILABILITY';

export const brokerAvailabilityActionTypes = {
  SET_BROKER_AVAILABILITY
};

/************************************************************

 Alert action types

 ************************************************************/

const SET_ALERT = 'SET_ALERT';
const REMOVE_ALERT = 'REMOVE_ALERT';

export const alertActionTypes = {
  SET_ALERT,
  REMOVE_ALERT,
  CLEAR_STATE
};

/************************************************************

 Messages action types

 ************************************************************/

const SET_MESSAGES = 'SET_MESSAGES';

export const messagesActionTypes = {
  SET_MESSAGES,
  CLEAR_STATE
};
