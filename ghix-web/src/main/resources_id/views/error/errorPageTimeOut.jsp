<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isErrorPage="true" %>

<div class="row-fluid">
	<div class="txt-center margin100">
		<p>For your security, we have timed out your session. Please log in or try again.</p>
        <div class="margin30-t">
			<a class="btn margin20-r" href="<c:url value="/account/user/login" />">Log In</a> <a href="<c:url value="/" />" class="btn btn-primary margin20-l">Go To Homepage</a>
		</div>
	</div>
</div>