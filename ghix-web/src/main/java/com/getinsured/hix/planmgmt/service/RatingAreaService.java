package com.getinsured.hix.planmgmt.service;

import com.getinsured.hix.model.RatingArea;

public interface RatingAreaService {
	@Deprecated
	RatingArea getByRatingAreaID(String strRatingArea);
	
	RatingArea getByRatingAreaAndState(String ratingArea,String state);


}
