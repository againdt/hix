package com.getinsured.hix.batch.enrollment.skip;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component("enrollment834Out")
public class Enrollment834Out {
	private  Map<String,List<Integer>> issuerEnrollmentIdMap=null;
	
	public Enrollment834Out(){
		issuerEnrollmentIdMap= new HashMap<>();
	}
	
	public synchronized void putToIssuerEnrollmentIdMap(String key, List<Integer> value){
		issuerEnrollmentIdMap.put(key, value);
	}
	public synchronized void putAllToIssuerEnrollmentIdMap(Map<String,List<Integer>> issuerEnrollmentIdMap){
		this.issuerEnrollmentIdMap.putAll(issuerEnrollmentIdMap);
	}	
	
	public Map<String,List<Integer>> getIssuerEnrollmentIdMap(){
		return issuerEnrollmentIdMap;
	}
	
	public synchronized void clearIssuerEnrollmentIdMap(){
		if(issuerEnrollmentIdMap!=null && !issuerEnrollmentIdMap.isEmpty()){
			this.issuerEnrollmentIdMap.clear();
		}
	}
}
