(function() {
    'use strict';

    angular.module('popoverDirective', [])
    .directive('giPopover', giPopover);


    function giPopover(){
        var ddo = {
            restrict: 'A',
	        link: function(scope, element, attrs) {
                if (attrs.popoverHtml && attrs.popoverHtml.match(/[a-zA-Z]+/g) ) {
                    $(element).popover({
                        trigger: 'hover',
                        html: true,
                        content: attrs.popoverHtml,
                        placement: attrs.popoverPlacement,
                        "is-open" : true
                    });
                } else {
                    $(element).popover({
                        "is-open" : false
                    });
                }
                // $('body').on('mouseover', 'a.accordion-toggle', function(){
                //     $(element).popover({
                //         trigger: 'hover',
                //         html: true,
                //         content: attrs.popoverHtml,
                //         placement: attrs.popoverPlacement,
                //         "is-open" : true
                //     });
                // })
            }
        };
        return ddo;
    }



})();
