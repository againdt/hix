<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div id="broker-edit-paymentinfo" class="gutter10">
	<script src="../resources/js/jquery.autoSuggest.js" type="text/javascript"></script>
	<script src="../resources/js/jquery.autoSuggest.minified.js" type="text/javascript"></script>

	<div class="row-fluid">		
		<c:choose>
				<c:when test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == null || broker.certificationStatus ==''}">			
					<h1>New Agent Registration</h1>
				</c:when>
				<c:otherwise>	
					<h1>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1> 
				</c:otherwise>
		 </c:choose>			 
	</div>

	<div class="row-fluid">
		<!-- #sidebar -->      
			<jsp:include page="brokerLeftNavigationMenu.jsp" />
		<!-- #sidebar ENDS -->
			
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="span10">Step 3: Payment Information</h4>
			</div>
		
			<div class="row-fluid">
				<div class="gutter10">
					<p>Bank Account Details For Direct Bank deposit/Withdrawal </p>
				</div>
				
			<form class="form-horizontal gutter10" id="frmbrokereditpayment" name="frmbrokereditpayment"  action="<c:url value="/broker/paymentinfo"/>" method="POST">
			<input type="hidden" id="paymentId" name="paymentId" value="${paymentMethodsObj.id}"/>
			<input type="hidden" id="redirectTo" name="redirectTo" value="editpaymentinfo"/>
								   <div class="control-group">
                                       <label for="bankName" class="control-label"><spring:message  code="label.bankName"/></label>
                                       <div class="controls">
                                       	<input type="text" class="input-xlarge" id="bankName" name="financialInfo.bankInfo.bankName" size="30" value="${paymentMethodsObj.financialInfo.bankInfo.bankName}">
                                       	<div id="bankName_error"></div>
                                       </div>
                                   </div>
                                   
                                   <div class="control-group">
                                        <label for="bankABARoutingNumber" class="control-label"><spring:message  code="label.routingNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> 
							<a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover"  href="#"> <i class="icon-question-sign"></i></a></label>
                                       <div class="controls">
                                       	<input type="text" class="input-xlarge" id="bankABARoutingNumber" name="financialInfo.bankInfo.routingNumber"  value="${paymentMethodsObj.financialInfo.bankInfo.routingNumber}" maxlength="9" size="30">
                                       	<div id="bankABARoutingNumber_error"></div>
                                       </div>
                                   </div>
                                   
                                   <div class="control-group">
                                       <label for="accountNumber" class="control-label"><spring:message  code="label.accountNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> 
							<a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover"  href="#"> <i class="icon-question-sign"></i></a></label>
                                       <div class="controls">
                                       	<input type="text" class="input-xlarge" id="bankAccountNumber" name="financialInfo.bankInfo.accountNumber" maxlength="20" size="30" value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}">
                                       	<div class="" id="bankAccountNumber_error"></div>
                                       </div>
                                   </div>
                                   
                                   <div class="control-group">
                                       <label for="bankAccountName" class="control-label"><spring:message  code="label.nameofaccount"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                       <div class="controls">
                                       	<input type="text" class="input-xlarge" id="bankAccountName" name="financialInfo.bankInfo.nameOnAccount" size="30" value="${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}">
                                       	<div id="bankAccountName_error"></div>
                                       </div>
                                   </div>
                                   
                                    <div class="control-group">
                                       <label for="bankAccountType" class="control-label"><spring:message  code="label.accountType"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                       <div class="controls">
                                          <select name="financialInfo.bankInfo.accountType" id="bankAccountType">
											<option value="">Select</option>
											<option value="C" <c:if test="${'C' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> SELECTED </c:if>>Checking</option>
											<option value="S" <c:if test="${'S' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> SELECTED </c:if>>Saving</option>													
										  </select>
											<div id="bankAccountType_error"></div>
                                       </div>
                                   </div>
					<br>
				<div class="form-actions">
						<input type="button" name="back" id="back" value="&lt; Back" title="&lt; Back" class="btn" onClick="window.location.href='/hix/broker/buildprofile'"/>
						<a class="btn btn-primary" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>										
				</div><!-- end of form-actions -->
				
			</form>
		</div><!-- end of span9 -->
	</div><!--  end of row-fluid -->
 </div>
		
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p>This information is required to determine initial eligibility to use the SHOP Exchange. All fields are subjected to a basic format validation of the input (e.g. a zip code must be a 5 digit number). The question mark icon which opens a light-box provides helpful information. The EIN number can be validated with an external data source if an adequate web API is available.</p>
				</div>
			</div>
		</div><!--  end of .notes -->
	
<script type="text/javascript">
$('.code').popover({ html: true,
	  trigger: 'hover',
	  placement: 'top',
	  content: function(){return '<img src="'+$(this).data('img') + '" />';}
});
	$('.brktip').tooltip();
	
	$(document).ready(function() {	
		$("#brkPaymentInfo").removeClass("link");
		$("#brkPaymentInfo").addClass("active");
		
		jQuery.validator.addMethod("financialInfo.bankInfo.nameOnAccount", function(value, element, param) {			
				if(value.contains(",")) {			   
				    return true;
				}
				else
				{				 
					return false; 
				}
			});
		});
	
	$(document).ready(function() {
		
		var brkId = ${broker.id};

		if(brkId != 0) {
			$('#certificationInfo').attr('class', 'done');
			$('#buildProfile').attr('class', 'done');
		} else {
			$('#certificationInfo').attr('class', 'visited');
			$('#buildProfile').attr('class', 'visited');
		};
		
	});
	
	function shiftbox(element,nextelement) {
		maxlength = parseInt(element.getAttribute('maxlength'));
		if(element.value.length == maxlength) {
			nextelement = document.getElementById(nextelement);
			nextelement.focus();
		}
	}
		
	function submitForm() {	

		$("#frmbrokereditpayment").submit();			
	}
	
	var validator = $("#frmbrokereditpayment").validate({ 
		onkeyup : false,
		onclick : false,
		rules : {
			"financialInfo.bankInfo.nameOnAccount" : {required : true, "financialInfo.bankInfo.nameOnAccount" : true},				
			"financialInfo.bankInfo.accountNumber" : {required : true, number : true},
			"financialInfo.bankInfo.routingNumber" : {required : true, number : true, minlength : 9,  maxlength : 9},
			"financialInfo.bankInfo.accountType" : {required : true}
		},
		messages : {
			"financialInfo.bankInfo.nameOnAccount" : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAcctName' javaScriptEscape='true'/></span>",
			"financialInfo.bankInfo.nameOnAccount" :"<span><em class='excl'>!</em>Please enter Lastname,Firstname</span>"},					
			"financialInfo.bankInfo.accountNumber" : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAcctNumber' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.routingNumber" : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankABARoutingNumber' javaScriptEscape='true'/></span>"},
			"financialInfo.bankInfo.accountType" : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAccountType' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		}; 
	});	
	
	
</script>

