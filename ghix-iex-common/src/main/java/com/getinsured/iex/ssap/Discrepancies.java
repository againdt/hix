
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Discrepancies {

    @Expose
    private Boolean hasWageOrSalaryBeenCutIndicator=false;
    @Expose
    private String explanationForJobIncomeDiscrepancy;
    @Expose
    private Boolean didPersonEverWorkForEmployerIndicator=false;
    @Expose
    private Boolean stopWorkingAtEmployerIndicator=false;
    @Expose
    private Boolean hasHoursDecreasedWithEmployerIndicator=false;
    @Expose
    private String explanationForDependantDiscrepancy;
    @Expose
    private Boolean seasonalWorkerIndicator=false;
    @Expose
    private Boolean hasChangedJobsIndicator=false;

    public Boolean getHasWageOrSalaryBeenCutIndicator() {
        return hasWageOrSalaryBeenCutIndicator;
    }

    public void setHasWageOrSalaryBeenCutIndicator(Boolean hasWageOrSalaryBeenCutIndicator) {
        this.hasWageOrSalaryBeenCutIndicator = hasWageOrSalaryBeenCutIndicator;
    }

    public String getExplanationForJobIncomeDiscrepancy() {
        return explanationForJobIncomeDiscrepancy;
    }

    public void setExplanationForJobIncomeDiscrepancy(String explanationForJobIncomeDiscrepancy) {
        this.explanationForJobIncomeDiscrepancy = explanationForJobIncomeDiscrepancy;
    }

    public Boolean getDidPersonEverWorkForEmployerIndicator() {
        return didPersonEverWorkForEmployerIndicator;
    }

    public void setDidPersonEverWorkForEmployerIndicator(Boolean didPersonEverWorkForEmployerIndicator) {
        this.didPersonEverWorkForEmployerIndicator = didPersonEverWorkForEmployerIndicator;
    }

    public Boolean getStopWorkingAtEmployerIndicator() {
        return stopWorkingAtEmployerIndicator;
    }

    public void setStopWorkingAtEmployerIndicator(Boolean stopWorkingAtEmployerIndicator) {
        this.stopWorkingAtEmployerIndicator = stopWorkingAtEmployerIndicator;
    }

    public Boolean getHasHoursDecreasedWithEmployerIndicator() {
        return hasHoursDecreasedWithEmployerIndicator;
    }

    public void setHasHoursDecreasedWithEmployerIndicator(Boolean hasHoursDecreasedWithEmployerIndicator) {
        this.hasHoursDecreasedWithEmployerIndicator = hasHoursDecreasedWithEmployerIndicator;
    }

    public String getExplanationForDependantDiscrepancy() {
        return explanationForDependantDiscrepancy;
    }

    public void setExplanationForDependantDiscrepancy(String explanationForDependantDiscrepancy) {
        this.explanationForDependantDiscrepancy = explanationForDependantDiscrepancy;
    }

    public Boolean getSeasonalWorkerIndicator() {
        return seasonalWorkerIndicator;
    }

    public void setSeasonalWorkerIndicator(Boolean seasonalWorkerIndicator) {
        this.seasonalWorkerIndicator = seasonalWorkerIndicator;
    }

    public Boolean getHasChangedJobsIndicator() {
        return hasChangedJobsIndicator;
    }

    public void setHasChangedJobsIndicator(Boolean hasChangedJobsIndicator) {
        this.hasChangedJobsIndicator = hasChangedJobsIndicator;
    }

}
