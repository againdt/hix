<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.lang3.time.DateUtils"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%@page import="java.util.Properties"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%!private static ApplicationContext ctx = null;%>
<%!private static Properties prop = null;%>
<%
	if (ctx == null) {
		ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	}
	if (prop == null) {
		prop = (Properties) ctx.getBean("configProp");
	}
%>

 <script src="//www.google.com/recaptcha/api.js?render=explicit&onload=vcRecaptchaApiLoaded" async defer></script>
<%
  String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
  String enrollmentStartDate = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OPEN_ENROLLMENT_START_DATE);
  String showIdaLinkLogo = "Y";
  if(!"".equals(enrollmentStartDate)){
	  Date startDate = DateUtils.parseDateStrictly(enrollmentStartDate, new String[]{"MM/dd/yyyy"});
	  if(new Date().before(startDate)){
		  showIdaLinkLogo = "N";
	  }
  }
  pageContext.setAttribute("showIdaLinkLogo", showIdaLinkLogo);
%>
<c:set var="stateExchangeType" value="<%=stateExchangeType%>" />
<c:set var="showIdaLinkLogo" value="<%=showIdaLinkLogo%>" />

 <script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'custom',
    custom_theme_widget: 'recaptcha_widget'
 };
 </script>
<div class="row-fluid" role="main">
	<div class="landing-signup">
		<h1><spring:message code="label.homePage.headerCopy"/></h1>
		<div class="row-fluid" role="main">
			<c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'individual'.equalsIgnoreCase(stateExchangeType)}">
				<div class="register-group clearfix">
					<c:set var="individualRoleName" ><encryptor:enc value="individual" isurl="true"/> </c:set>
					<div class="span2 iconfamily-wrapper">
						<div class="icons-family"></div>
					</div>
					<div class="span6 maintext-wrapper">
						<h2><spring:message code="label.homePage.individual"/></h2> 
						<p><spring:message code="label.homePage.individualCopy"/></p>
					</div>
					<div class="shopbutton-wrapper pull-right">
						<a class="btn" href="<c:url value='/preeligibility' />">START SHOPPING<i class="icon-large icon-arrow-right"></i><span class="aria-hidden">Individual</span></a>
					</div>
				</div>
		    </c:if>
		    
		    <c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'shop'.equalsIgnoreCase(stateExchangeType)}">
 				<div class="register-group"> 
 					<c:set var="employerRoleName" ><encryptor:enc value="employer" isurl="true"/> </c:set>
 					<div class="icons-employer"></div>
 					<p>
 						<span><spring:message code="label.homePage.employer"/></span> <br>
 						<spring:message code="label.homePage.employerCopy"/>
 					</p>
 					<a class="btn pull-right" href="<c:url value='/account/signup/${employerRoleName}' />"><i class="icon-large icon-arrow-right"></i><span class="aria-hidden">Employer</span></a>
 				</div>
         	</c:if> 
		</div> <!-- end main -->
			
		<div class="row-fluid">	
			<div class="span6">
	 	 		<div class="register-signin">
	 	 			<p>
	 	 				<i class="icon-lock"></i> <spring:message code="label.homePage.haveAccountQuestion"/> 
	 	 				<a class="btn btn-primary" href="<c:url value='/account/user/login'/>"><spring:message code="label.homePage.haveAccountAction"/></a>
	 	 			</p>
	 	 			<c:if test="${showIdaLinkLogo == 'Y' }">
		 	 			<div>
		 	 				<img src="<gi:cdnurl value="/resources/img/idaLink_logo.png"/>" alt="Welcome to idalink! Your online portal for managing benefits from Idaho's Department of Health and Welfare. idaLink" title="idalink"  />
		 	 				<small><spring:message code="label.homePage.haveAccountOption"/></small>
		 	 			</div>
		 	 		</c:if>	
	 	 		</div>
	 	 	</div> 	
	 	 	
	 	 	<div class="span6">
	 	 		<div class="access-code register-signin" ng-app="accessCodeApp" ng-controller="accessCodeController" ng-cloak>
	 	 			<form id="access-code-form" name="accessCodeForm" action="<c:url value="/referral/verifyaccesscode"/>" method="post" novalidate autocomplete="off" class="margin30-t">
	 	 				<df:csrfToken/>
		 	 			<p for="access-code">
		 	 				<i class="icon-lock"></i><spring:message code="label.homePage.haveAccessCode"/>
		 	 			</p>
		 	 			<input type="text" id="accessCode" name="accessCode" length="10" required ng-focus ng-pattern="/^[0-9]{10}$/" ng-model="accessCode" ng-keypress="formSubmit($event)">
		 	 			<br>
		 	 			
		 	 			<div class="help-inline" id="accessCode_error" style="min-height: 35px">
							<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.required && !accessCodeForm.accessCode.$focused">
								<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeRequired"/></span>
							</label>
							<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.pattern && !accessCodeForm.accessCode.$focused">
								<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeValidation"/></span>
							</label>
						</div>
						
		 	 			<div class="error help-inline">
		 	 				<c:if test="${not empty errorMsg}">
   								${errorMsg}
							</c:if>
		 	 			</div>
							
						<!--custom captcha starts-->
						<div vc-recaptcha key="siteKey" on-success="updateRecaptchaValidation()" class="inline-block"></div>
						<!--custom captcha ends-->

						<div id="access-code-button">
			 	 			<input type="submit" class="btn btn-primary" value="Submit" ng-disabled="accessCodeForm.$invalid || !isRecaptchaValid">
		 	 			</div>
	 	 			</form>
	 	 			<div id="info" style="color: red;"></div>
				</div>

						
	 	 	</div> <!-- end span6 -->
	 	 </div> <!-- end row-fluid -->
		
	</div> 	<!-- end landing-signup -->	
</div>	<!-- end main -->
			
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-recaptcha.min.js" />"></script>

<script>
	
var accessCodeApp = angular.module('accessCodeApp', ['vcRecaptcha']);

accessCodeApp.controller('accessCodeController', function($scope) {
	$scope.siteKey = '<%=prop.getProperty("Recaptcha.siteKey")%>';

	$scope.isRecaptchaValid = false;
	$scope.updateRecaptchaValidation = function(){
		$scope.isRecaptchaValid = true;
	}
	
	$scope.formSubmit = function($event){
		 if($event.which === 13) {
			 $event.preventDefault();
			 if($scope.accessCodeForm.$valid === true){
				 $('#access-code-form').submit();
			}else if($scope.accessCodeForm.accessCode.$valid === false){
				$scope.accessCodeForm.accessCode.$dirty = true;				
			}
			
         }
		
	}
	
	
});

accessCodeApp.directive('ngFocus', [function() {
	var FOCUS_CLASS = "ng-focused";
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$focused = false;
			element.bind('focus', function(evt) {
				element.addClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = true;
				});
			}).bind('blur', function(evt) {
				element.removeClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = false;
				});
			});
		}
	};
}]);
</script>
