package com.getinsured.hix.broker;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Scope;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.notification.NotificationAgent;

/**
 * BrokerConfimationEmail extends NotificationAgent 
 * overrides the getSingleData() method of NotificationAgent
 * used to for Email header such as from,To . 
 */
@Component
@Scope("prototype")
public class BrokerConfirmationEmail extends NotificationAgent {
	private AccountUser userObj;
	private Map<String, String> singleData;
	
	/**
	 * set user object to current instance
	 * @param userObj instance of AccountUser
	 */
	public void setUserObj(AccountUser userObj) {
		this.userObj = userObj;
	}
	
	/**
	 * Overrides the implementation of NotificationAgent
	 */
	public Map<String, String> getSingleData() {
		Map<String,String> bean = new HashMap<String, String>();
		bean.put("name",userObj.getFirstName()+" "+userObj.getLastName() );
		bean.put("id",Integer.toString(userObj.getId()));
		setTokens(bean);

		Map<String, String> data = new HashMap<String, String>();
		data.put("To", this.userObj.getEmail());
		data.put("UserId", Integer.toString(userObj.getId()));

		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
	}
	
	/**
	 * Set the single data to current instance
	 * @param singleData instance of Map<String,String>
	 */
	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}
}