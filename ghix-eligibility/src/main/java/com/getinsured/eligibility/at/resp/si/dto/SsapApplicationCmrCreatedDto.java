package com.getinsured.eligibility.at.resp.si.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author chopra_s
 * 
 */
public class SsapApplicationCmrCreatedDto implements Comparable<SsapApplicationCmrCreatedDto> {
	private long ssapApplicationId;
	private Timestamp cmrCreatedDate;
	private Timestamp applicationCreatedDate;
	private BigDecimal cmrId;

	private static final int EQUAL = 0;

	public SsapApplicationCmrCreatedDto(long ssapApplicationId, Timestamp cmrCreatedDate, Timestamp applicationCreatedDate, BigDecimal cmrId) {
		this.ssapApplicationId = ssapApplicationId;
		this.cmrCreatedDate = cmrCreatedDate;
		this.applicationCreatedDate = applicationCreatedDate;
		this.cmrId = cmrId;
	}

	public long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public Timestamp getCmrCreatedDate() {
		return cmrCreatedDate;
	}

	public BigDecimal getCmrId() {
		return cmrId;
	}

	public Timestamp getApplicationCreatedDate() {
		return applicationCreatedDate;
	}

	public void setApplicationCreatedDate(Timestamp applicationCreatedDate) {
		this.applicationCreatedDate = applicationCreatedDate;
	}

	@Override
	public int compareTo(SsapApplicationCmrCreatedDto o) {
		if (o.getCmrCreatedDate().compareTo(this.getCmrCreatedDate()) == EQUAL) {
			return o.getApplicationCreatedDate().compareTo(this.getApplicationCreatedDate());
		}
		return o.getCmrCreatedDate().compareTo(this.getCmrCreatedDate());
	}

	@Override
	public String toString() {
		return "SsapApplicationCmrCreatedDto [ssapApplicationId=" + ssapApplicationId + ", cmrCreatedDate=" + cmrCreatedDate + ", applicationCreatedDate=" + applicationCreatedDate + ", cmrId=" + cmrId + "]";
	}
}
