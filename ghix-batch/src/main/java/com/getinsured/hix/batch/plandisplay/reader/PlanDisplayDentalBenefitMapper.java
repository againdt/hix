package com.getinsured.hix.batch.plandisplay.reader;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.solr.common.SolrInputDocument;
import org.springframework.jdbc.core.RowMapper;

public class PlanDisplayDentalBenefitMapper implements RowMapper<PlanDisplayDentalBenefitMapper>{
	
	private int id ;
	private int planDentalId;
	private int planId;
	private String name ;
	private String networkLimitation ;
	private String networkLimitationAttribute ;
	private String networkExceptions ;
	private Date effStartDate; 
	private Date effEndDate ;
	private String networkSubjDeduct ;
	private String networkExclFrmMoop ;
	private String nonnetworkSubjDeduct ;
	private String nonnetworkExclFrmMoop ;
	private String isEHB ;
	private String isCovered ;
	private String minStay ;
	private String explanation ;
	private String subjectToInNetworkDuductible ;
	private String excludedFromInNetworkMoop ;
	private String subjectToOutNetworkDeductible ;
	private String excludedFromOutOfNetworkMoop ;
	private String networkT1CopayVal ;
	private String networkT1CopayAttr ;
	private String networkT1CoinsurVal ;
	private String networkT1CoinsurAttr; 
	private String networkT2CopayVal ;
	private String networkT2CopayAttr ;
	private String networkT2CoinsurVal ;
	private String networkT2CoinsurAttr ;
	private String outOfNetworkCopayVal ;
	private String outOfNetworkCopayAttr ;
	private String outOfNetworkCoinsurVal ;
	private String outOfNetworkCoinsurAttr ;
	private String networkT1display ;
	private String networkT2display ;
	private String outOfNetworkDisplay ;
	private String networkT1TileDisplay ;
	
	/*private String limitExcepDisplay;               
	private String coinsuranceInNetworkTier1;      
	private String coinsuranceInNetworkTier2;      
	private String coinsuranceOutOfNetwork;        
	private String copayInNetworkTier1;            
	private String copayInNetworkTier2;            
	private String copayOutOfNetwork ;*/

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPlanDentalId() {
		return planDentalId;
	}

	public void setPlanDentalId(int planDentalId) {
		this.planDentalId = planDentalId;
	}

	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNetworkLimitation() {
		return networkLimitation;
	}

	public void setNetworkLimitation(String networkLimitation) {
		this.networkLimitation = networkLimitation;
	}

	public String getNetworkLimitationAttribute() {
		return networkLimitationAttribute;
	}

	public void setNetworkLimitationAttribute(String networkLimitationAttribute) {
		this.networkLimitationAttribute = networkLimitationAttribute;
	}

	public String getNetworkExceptions() {
		return networkExceptions;
	}

	public void setNetworkExceptions(String networkExceptions) {
		this.networkExceptions = networkExceptions;
	}

	public Date getEffStartDate() {
		return new Date(effStartDate.getTime());
	}

	public void setEffStartDate(Date effStartDate) {
		this.effStartDate = new Date(effStartDate.getTime());
	}

	public Date getEffEndDate() {
		return new Date(effEndDate.getTime());
	}

	public void setEffEndDate(Date effEndDate) {
		this.effEndDate = new Date(effEndDate.getTime());
	}

	public String getNetworkSubjDeduct() {
		return networkSubjDeduct;
	}

	public void setNetworkSubjDeduct(String networkSubjDeduct) {
		this.networkSubjDeduct = networkSubjDeduct;
	}

	public String getNetworkExclFrmMoop() {
		return networkExclFrmMoop;
	}

	public void setNetworkExclFrmMoop(String networkExclFrmMoop) {
		this.networkExclFrmMoop = networkExclFrmMoop;
	}

	public String getNonnetworkSubjDeduct() {
		return nonnetworkSubjDeduct;
	}

	public void setNonnetworkSubjDeduct(String nonnetworkSubjDeduct) {
		this.nonnetworkSubjDeduct = nonnetworkSubjDeduct;
	}

	public String getNonnetworkExclFrmMoop() {
		return nonnetworkExclFrmMoop;
	}

	public void setNonnetworkExclFrmMoop(String nonnetworkExclFrmMoop) {
		this.nonnetworkExclFrmMoop = nonnetworkExclFrmMoop;
	}

	public String getIsEHB() {
		return isEHB;
	}

	public void setIsEHB(String isEHB) {
		this.isEHB = isEHB;
	}

	public String getIsCovered() {
		return isCovered;
	}

	public void setIsCovered(String isCovered) {
		this.isCovered = isCovered;
	}

	public String getMinStay() {
		return minStay;
	}

	public void setMinStay(String minStay) {
		this.minStay = minStay;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public String getSubjectToInNetworkDuductible() {
		return subjectToInNetworkDuductible;
	}

	public void setSubjectToInNetworkDuductible(String subjectToInNetworkDuductible) {
		this.subjectToInNetworkDuductible = subjectToInNetworkDuductible;
	}

	public String getExcludedFromInNetworkMoop() {
		return excludedFromInNetworkMoop;
	}

	public void setExcludedFromInNetworkMoop(String excludedFromInNetworkMoop) {
		this.excludedFromInNetworkMoop = excludedFromInNetworkMoop;
	}

	public String getSubjectToOutNetworkDeductible() {
		return subjectToOutNetworkDeductible;
	}

	public void setSubjectToOutNetworkDeductible(
			String subjectToOutNetworkDeductible) {
		this.subjectToOutNetworkDeductible = subjectToOutNetworkDeductible;
	}

	public String getExcludedFromOutOfNetworkMoop() {
		return excludedFromOutOfNetworkMoop;
	}

	public void setExcludedFromOutOfNetworkMoop(String excludedFromOutOfNetworkMoop) {
		this.excludedFromOutOfNetworkMoop = excludedFromOutOfNetworkMoop;
	}

	public String getNetworkT1CopayVal() {
		return networkT1CopayVal;
	}

	public void setNetworkT1CopayVal(String networkT1CopayVal) {
		this.networkT1CopayVal = networkT1CopayVal;
	}

	public String getNetworkT1CopayAttr() {
		return networkT1CopayAttr;
	}

	public void setNetworkT1CopayAttr(String networkT1CopayAttr) {
		this.networkT1CopayAttr = networkT1CopayAttr;
	}

	public String getNetworkT1CoinsurVal() {
		return networkT1CoinsurVal;
	}

	public void setNetworkT1CoinsurVal(String networkT1CoinsurVal) {
		this.networkT1CoinsurVal = networkT1CoinsurVal;
	}

	public String getNetworkT1CoinsurAttr() {
		return networkT1CoinsurAttr;
	}

	public void setNetworkT1CoinsurAttr(String networkT1CoinsurAttr) {
		this.networkT1CoinsurAttr = networkT1CoinsurAttr;
	}

	public String getNetworkT2CopayVal() {
		return networkT2CopayVal;
	}

	public void setNetworkT2CopayVal(String networkT2CopayVal) {
		this.networkT2CopayVal = networkT2CopayVal;
	}

	public String getNetworkT2CopayAttr() {
		return networkT2CopayAttr;
	}

	public void setNetworkT2CopayAttr(String networkT2CopayAttr) {
		this.networkT2CopayAttr = networkT2CopayAttr;
	}

	public String getNetworkT2CoinsurVal() {
		return networkT2CoinsurVal;
	}

	public void setNetworkT2CoinsurVal(String networkT2CoinsurVal) {
		this.networkT2CoinsurVal = networkT2CoinsurVal;
	}

	public String getNetworkT2CoinsurAttr() {
		return networkT2CoinsurAttr;
	}

	public void setNetworkT2CoinsurAttr(String networkT2CoinsurAttr) {
		this.networkT2CoinsurAttr = networkT2CoinsurAttr;
	}

	public String getOutOfNetworkCopayVal() {
		return outOfNetworkCopayVal;
	}

	public void setOutOfNetworkCopayVal(String outOfNetworkCopayVal) {
		this.outOfNetworkCopayVal = outOfNetworkCopayVal;
	}

	public String getOutOfNetworkCopayAttr() {
		return outOfNetworkCopayAttr;
	}

	public void setOutOfNetworkCopayAttr(String outOfNetworkCopayAttr) {
		this.outOfNetworkCopayAttr = outOfNetworkCopayAttr;
	}

	public String getOutOfNetworkCoinsurVal() {
		return outOfNetworkCoinsurVal;
	}

	public void setOutOfNetworkCoinsurVal(String outOfNetworkCoinsurVal) {
		this.outOfNetworkCoinsurVal = outOfNetworkCoinsurVal;
	}

	public String getOutOfNetworkCoinsurAttr() {
		return outOfNetworkCoinsurAttr;
	}

	public void setOutOfNetworkCoinsurAttr(String outOfNetworkCoinsurAttr) {
		this.outOfNetworkCoinsurAttr = outOfNetworkCoinsurAttr;
	}

	public String getNetworkT1display() {
		return networkT1display;
	}

	public void setNetworkT1display(String networkT1display) {
		this.networkT1display = networkT1display;
	}

	public String getNetworkT2display() {
		return networkT2display;
	}

	public void setNetworkT2display(String networkT2display) {
		this.networkT2display = networkT2display;
	}

	public String getOutOfNetworkDisplay() {
		return outOfNetworkDisplay;
	}

	public void setOutOfNetworkDisplay(String outOfNetworkDisplay) {
		this.outOfNetworkDisplay = outOfNetworkDisplay;
	}

	public String getNetworkT1TileDisplay() {
		return networkT1TileDisplay;
	}

	public void setNetworkT1TileDisplay(String networkT1TileDisplay) {
		this.networkT1TileDisplay = networkT1TileDisplay;
	}

	@Override
	public PlanDisplayDentalBenefitMapper mapRow(ResultSet result, int arg1) throws SQLException {
		
		PlanDisplayDentalBenefitMapper obj = new PlanDisplayDentalBenefitMapper();
		
		obj.name = result.getString("NAME");
		obj.id = result.getInt("ID");
		obj.planDentalId = result.getInt("plan_dental_id");
		obj.planId = result.getInt("plan_Id");
		obj.effEndDate = new Date(result.getDate("EFFECTIVE_END_DATE").getTime());
		obj.effStartDate =  new Date(result.getDate("EFFECTIVE_START_DATE").getTime());
		obj.excludedFromInNetworkMoop = result.getString("EXCLUDED_FROM_IN_NET_MOOP");
		obj.excludedFromOutOfNetworkMoop = result.getString("EXCLUDED_FROM_OUT_OF_NET_MOOP");
		obj.explanation = result.getString("EXPLANATION");
		obj.isCovered = result.getString("is_covered");
		obj.isEHB = result.getString("is_ehb");
		obj.minStay = result.getString("min_stay");
		obj.networkExceptions = result.getString("network_exceptions");
		obj.networkLimitation = result.getString("limitation");
		obj.networkLimitationAttribute = result.getString("limitation_attr");
		obj.networkT1CoinsurAttr = result.getString("NETWORK_T1_COINSURANCE_ATTR");
		obj.networkT1CoinsurVal = result.getString("NETWORK_T1_COINSURANCE_VAL");
		obj.networkT1CopayAttr = result.getString("NETWORK_T1_COPAY_ATTR");
		obj.networkT1CopayVal = result.getString("NETWORK_T1_COPAY_VAL");
		obj.networkT1display = result.getString("network_t1_display");
		obj.networkT1TileDisplay = result.getString("network_t1_tile_display");
		obj.networkT2CoinsurAttr = result.getString("NETWORK_T2_COINSURANCE_ATTR");
		obj.networkT2CoinsurVal = result.getString("NETWORK_T2_COINSURANCE_VAL");
		obj.networkT2CopayAttr = result.getString("NETWORK_T2_COPAY_ATTR");
		obj.networkT2CopayVal = result.getString("NETWORK_T2_COPAY_VAL");
		obj.networkT2display = result.getString("network_t2_display");
		obj.networkSubjDeduct = result.getString("NETWORK_SUBJ_DEDUCT");
		obj.networkExclFrmMoop = result.getString("NETWORK_EXCL_FRM_MOOP");
		obj.nonnetworkSubjDeduct = result.getString("NONNETWORK_SUBJ_DEDUCT");
		obj.nonnetworkExclFrmMoop = result.getString("NONNETWORK_EXCL_FRM_MOOP");
		obj.outOfNetworkCoinsurAttr = result.getString("OUTNETWORK_COINSURANCE_ATTR");
		obj.outOfNetworkCoinsurVal = result.getString("OUTNETWORK_COINSURANCE_VAL");
		obj.outOfNetworkCopayAttr = result.getString("OUTNETWORK_COPAY_ATTR");
		obj.outOfNetworkCopayVal = result.getString("OUTNETWORK_COPAY_VAL");
		obj.outOfNetworkDisplay = result.getString("outnetwork_display");
		obj.subjectToInNetworkDuductible = result.getString("subject_to_in_net_deductible");
		obj.subjectToOutNetworkDeductible = result.getString("subject_to_out_net_deductible");
		return obj;
	}
	
	public SolrInputDocument getSolrInputDocument(){
		SolrInputDocument solrDoc = new SolrInputDocument();
		solrDoc.addField("name", this.name);
		solrDoc.addField("id", this.id);
		solrDoc.addField("planDentalId",this.planDentalId);
		solrDoc.addField("planId",this.planId);
		solrDoc.addField("effEndDate", this.effEndDate);					 		
		solrDoc.addField("effStartDate", this.effStartDate);						
		solrDoc.addField("excludedFromInNetworkMoop", this.excludedFromInNetworkMoop);			
		solrDoc.addField("excludedFromOutOfNetworkMoop", this.excludedFromOutOfNetworkMoop);     
		solrDoc.addField("explanation", this.explanation);						
		solrDoc.addField("isCovered", this.isCovered);					 		
		solrDoc.addField("isEHB", this.isEHB);					 			
		solrDoc.addField("minStay", this.minStay);				 			
		solrDoc.addField("networkExceptions", this.networkExceptions);					
		solrDoc.addField("networkLimitation", this.networkLimitation);					
		solrDoc.addField("networkLimitationAttribute", this.networkLimitationAttribute);       
		solrDoc.addField("networkT1CoinsurAttr", this.networkT1CoinsurAttr);				
		solrDoc.addField("networkT1CoinsurVal", this.networkT1CoinsurVal);				
		solrDoc.addField("networkT1CopayAttr", this.networkT1CopayAttr);			 		
		solrDoc.addField("networkT1CopayVal", this.networkT1CopayVal);					
		solrDoc.addField("networkT1display", this.networkT1display);					
		solrDoc.addField("networkT1TileDisplay", this.networkT1TileDisplay);				
		solrDoc.addField("networkT2CoinsurAttr", this.networkT2CoinsurAttr);			
		solrDoc.addField("networkT2CoinsurVal", this.networkT2CoinsurVal);			
		solrDoc.addField("networkT2CopayAttr", this.networkT2CopayAttr);				
		solrDoc.addField("networkT2CopayVal", this.networkT2CopayVal);				
		solrDoc.addField("networkT2display", this.networkT2display);
		solrDoc.addField("networkSubjDeduct", this.networkSubjDeduct);
		solrDoc.addField("networkExclFrmMoop", this.networkExclFrmMoop);
		solrDoc.addField("nonnetworkSubjDeduct", this.nonnetworkSubjDeduct);
		solrDoc.addField("nonnetworkExclFrmMoop", this.nonnetworkExclFrmMoop);
		solrDoc.addField("outOfNetworkCoinsurAttr", this.outOfNetworkCoinsurAttr);		
		solrDoc.addField("outOfNetworkCoinsurVal", this.outOfNetworkCoinsurVal);			
		solrDoc.addField("outOfNetworkCopayAttr", this.outOfNetworkCopayAttr);			
		solrDoc.addField("outOfNetworkCopayVal", this.outOfNetworkCopayVal);			
		solrDoc.addField("outOfNetworkDisplay", this.outOfNetworkDisplay);			
		solrDoc.addField("subjectToInNetworkDuductible", this.subjectToInNetworkDuductible);       
		solrDoc.addField("subjectToOutNetworkDeductible", this.subjectToOutNetworkDeductible);
		return solrDoc;
	}
	
}
