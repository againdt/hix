package com.getinsured.hix.consumer.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.getinsured.hix.consumer.service.ConsumerPortalEnrollementService;
import com.getinsured.hix.consumer.service.jpa.ConsumerPortalEnrollmentServiceImpl;

/**
 * JSP Tag Library class for displaying the Household Enrollment plan data
 * @author bhatia_s
 * @since 9/18/2013
 */
public class ConsumerPortalEnrollmentData extends TagSupport {
	private static final long serialVersionUID = 1L;
	
    private JspWriter out;
    private ApplicationContext applicationContext;
    private StringTemplateGroup templateGroup;
    private ConsumerPortalEnrollementService consumerPortalEnrollementService;
    private HttpServletRequest request;	
	
	@Override
	public int doStartTag() throws JspException {
		this.setProperties();
		try {
			
			HttpSession session = null;
			session = pageContext.getSession();
			
			// Retreive the CMRHouseHoldID from Session
			int cmrHouseHoldLeadId = Integer.valueOf(session.getAttribute(ConsumerPortalUtil.HOUSEHOLD_ID).toString());
			
			// Getting the HouseHoldEnrollment plans data
			List< Map<String,String>> householdPlanMetaData = this.consumerPortalEnrollementService.getHouseholdPlanMetaData(cmrHouseHoldLeadId);
			this.request = (HttpServletRequest)pageContext.getRequest();
			for (Iterator<Map<String, String>> iterator = householdPlanMetaData.iterator(); iterator.hasNext();) {
				Map<String, String> planData = iterator.next();
				
				// Setting the path to the Plan logo image
				String logoStr = planData.get("logoStr");
				logoStr = pageContext.getServletContext().getContextPath() + logoStr;
				planData.put("logoStr", logoStr);
				
				String formattedBenefitStartDt = planData.get("formattedCoverageStartDate");
				
				StringTemplate enrollTpl = templateGroup.getInstanceOf("consumerportalenrollpage");
				enrollTpl.setAttributes(planData);
				
				// Setting the Plan Display URL Link
				enrollTpl.setAttribute("PlanUrl", this.request.getContextPath() + "/private/planinfo/" + formattedBenefitStartDt + "/" + planData.get("planId"));
				this.out.println(enrollTpl.toString());
			}
			
	        } catch (Exception ex) {
	            throw new JspException("Error in HouseHold MyPlans dashboard tag", ex);
	        }
	    return SKIP_BODY;
	}
	
	private void setProperties(){
    	this.out     = pageContext.getOut();
    	this.applicationContext = RequestContextUtils.getWebApplicationContext(
    			pageContext.getRequest(),
				pageContext.getServletContext()
			);
    	this.consumerPortalEnrollementService = applicationContext.getBean("consumerPortalEnrollmentService",ConsumerPortalEnrollmentServiceImpl.class);
    	this.templateGroup = new StringTemplateGroup("enrollpage",pageContext.getServletContext().getRealPath("resources/sampleTemplates"));
	}
}
