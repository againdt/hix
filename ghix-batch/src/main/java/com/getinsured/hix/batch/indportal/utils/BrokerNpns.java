package com.getinsured.hix.batch.indportal.utils;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

public class BrokerNpns {

	@Value("#{jobParameters[skipPattern]}")
	private String skipPattern;
	
    public String getSkipPattern() {
		return skipPattern;
	}
    
    private List<String> brokerNpns;
    
    public List<String> getBrokerNpns() {
		return brokerNpns;
	}
	@PostConstruct
    public void prepareList()
    {
    	if(StringUtils.isNotEmpty(skipPattern) && !"OverrideAll".equalsIgnoreCase(skipPattern))
    	{
    		brokerNpns = Arrays.asList(skipPattern.split("\\s*~\\s*"));
    	}
    	
    }
    
}
