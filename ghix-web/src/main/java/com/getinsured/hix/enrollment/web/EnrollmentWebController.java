package com.getinsured.hix.enrollment.web;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.TaxYear;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

@Controller
public class EnrollmentWebController {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentWebController.class);
	private static final String TAX_YEAR_OBJ = "taxYearObj";
	private static final String CART_ID = "cartId";
	private static final String ENROLLMENT_TYPE = "enrollment_type";
	private static final String ENROLLMENT_LIST = "EnrollmentList";
	private static final String ENROLLEE_NAME_MAP = "EnroleeNameMap";
	private static final String DIS_ENROLLMENT_LIST = "DisEnrollmentList";
	private static final String HEALTH = "Health";
	private static final String DENTAL = "Dental";
	private static final String ENR_ERROR_MSG = "enrErrorMsg";
	private static final String ENR_ERROR_MSG_2 = "enrErrorMsg2";
	private static final String EXCHANGE_TYPE_STATE = "STATE";
	private static final String EXCHANGE_TYPE_PHIX = "PHIX";
	private static final String EXCHANGE_STATE_CA = "CA";
	private static final String EXCHANGE_STATE_ID = "ID";
	private static final String EXCHANGE_STATE_NV = "NV";
	private static final String EXCHANGE_STATE_MN = "MN";
	private static final String FINANCE_ENROLLMENT_HEALTH_KEY = "finEnrlHltDataKey";
	private static final String FINANCE_ENROLLMENT_DENTAL_KEY = "finEnrlDntDataKey";
	public static final String HAS_STATE_SUBSIDY = "hasStateSubsidy";
	private static Boolean DISPLAY_FILE_TAX_RETURN;
	private static Boolean DISPLAY_SIGNATURE_PIN;
	private static final String DOCUMENT_URL= "/download/logobyid/"; // document download url
	@Value("#{configProp['appUrl']}") 
	private String appUrl;
	
	//	@Autowired private RestTemplate restTemplate;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private UserService userService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired private GIMonitorService gIMonitorService;
	
	/*@Value("#{configProp['enrollment.ahbxWsdlCallEnable']}")
	private boolean ENRL_AHBX_WSDL_CALL_ENABLE;
	
	@Value("#{configProp['enrollment.ldapCallEnable']}")
	private boolean ENROLL_LDAP_CALL_ENABLE;*/
	
	@Value("#{configProp['tenant.auth.userName']}")
	private String BENEFIT_BAY_USER;
	/**
	 * @author panda_Pratap
	 * @since 14 Feb 2013
	 * 
	 * This method is called by shop module for creating enrollment
	 * 
	 * @param planselectionid
	 * @return
	 */
	@RequestMapping(value = "/enrollment/createemployeeenrollment/{planselectionid}", method = RequestMethod.GET)
	public String creatEemployeeEnrollment(@PathVariable String planselectionid) {

		return "redirect:/shop/employee/planselection/completeapplication";
	}
	
	
	/**
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/enrollment/showesignature", method ={ RequestMethod.GET, RequestMethod.POST })
	public String showesignature(Model model, HttpServletRequest request) {
		
		LOGGER.info("inside Show Electronic Signature ");
		
		DISPLAY_FILE_TAX_RETURN =  Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue("enrollment.displayProperties.displayFileTaxReturn"));
		DISPLAY_SIGNATURE_PIN =  Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue("enrollment.displayProperties.displaySignaturePIN"));
		
		String errorMsg = "";
		
		HttpSession session = request.getSession();
		TaxYear taxYearObj = null;
		
		Date currentDate = new TSDate();
	

		//GETTING REQUEST PARAMETERS FROM THE REQUEST 
		String coveragedate = isNotNullAndEmpty(request.getParameter("coverage_date")) ? request.getParameter("coverage_date") : (String) session.getAttribute("coveragedate");
		String cartIdStr = null != request.getParameter("cart_id") && !request.getParameter("cart_id").isEmpty()
				? request.getParameter("cart_id")
				: (String) session.getAttribute(CART_ID); //HIX-109329
		String taxFilersName = isNotNullAndEmpty(request.getParameter("taxFilersName")) ? request.getParameter("taxFilersName") : (String) session.getAttribute("taxFilersName");
		String taxCreditTerm = isNotNullAndEmpty(request.getParameter("taxCreditTerm")) ? request.getParameter("taxCreditTerm") : (String) session.getAttribute("taxCreditTerm");
		String enrollmentType = (request.getParameter(ENROLLMENT_TYPE) != null)? request.getParameter(ENROLLMENT_TYPE) : GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL;
		
		String aptcVal = isNotNullAndEmpty(request.getParameter("aptcVal")) ? request.getParameter("aptcVal")
				: (isNotNullAndEmpty(session.getAttribute("aptc"))
						? String.valueOf((Float) session.getAttribute("aptc"))
						: null);
		if(isNotNullAndEmpty(aptcVal)){
			model.addAttribute("aptc", aptcVal);
			Float aptc = Float.valueOf(aptcVal.trim());
			session.setAttribute("aptc", aptc);
		}else{
			model.addAttribute("aptc", "");
		}
		
		setStateSubsidyFlags(model, request, session);
		
		// ADDING TAXFILERSNAME TO THE SESSION
		if(taxFilersName != null){
			model.addAttribute("taxFilersName", taxFilersName);
			session.setAttribute("taxFilersName", taxFilersName);
		}
		// ADDING TAXCREDITTERM TO THE SESSION
		if(taxCreditTerm != null){
			model.addAttribute("taxCreditTerm", taxCreditTerm);
			session.setAttribute("taxCreditTerm", taxCreditTerm);
		}
		// ADDING ENROLLMENT_TYPE TO THE SESSION
		if(enrollmentType != null && !enrollmentType.isEmpty()){
			enrollmentType = enrollmentType.toUpperCase();
			session.setAttribute(ENROLLMENT_TYPE, enrollmentType);
//			LOGGER.info("enrollment Type received : "+enrollmentType);
		}else{
			LOGGER.info("enrollment Type received  is null ");
		}
		
		try {
			// ADDING CART_ID TO THE SESSION
			if(cartIdStr == null || cartIdStr.isEmpty()){
				throw new Exception ("Invalid Cart Id received : " + cartIdStr);
			}
			else{
				session.setAttribute(CART_ID, cartIdStr);
			}
		}
		catch (Exception e) {
			LOGGER.debug("showesignature error : "+ e);
			return "redirect:hix/j_spring_security_logout";
		}
		
		try{
			// ADDING TAX_YEAR TO THE SESSION
			if(coveragedate !=null && !(coveragedate.isEmpty())){
				session.setAttribute("coveragedate", coveragedate);
				taxYearObj = getTaxYear(coveragedate);
			}else{
				throw new Exception ("Coverage date is null ");
			}
			model.addAttribute(TAX_YEAR_OBJ,taxYearObj);
			session.setAttribute(TAX_YEAR_OBJ, taxYearObj);
			LOGGER.info("Successfully taxYear is added to session");
		}
		catch(Exception e){
			LOGGER.error("" + e);
		}
		model.addAttribute("errorMsg", errorMsg);
		model.addAttribute(CART_ID, cartIdStr);
		model.addAttribute("showpin", DISPLAY_SIGNATURE_PIN);
		model.addAttribute("showFileTaxReturn", DISPLAY_FILE_TAX_RETURN);
		model.addAttribute(ENROLLMENT_TYPE, enrollmentType);
		model.addAttribute("enrollment_type_shop", GhixConstants.ENROLLMENT_TYPE_SHOP);
		model.addAttribute("enrollment_type_individual", GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
		//model.addAttribute("enrollment_type_medical", GhixConstants.ENROLLMENT_TYPE_MEDICAL);
		model.addAttribute("showMenu", false);
		if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME)!=null){
			model.addAttribute("stateName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME).trim());
		}
		
		String healthPlanIssuer = request.getParameter("healthPlanIssuer") != null ? request.getParameter("healthPlanIssuer") : null;
		String healthPlanName = request.getParameter("healthPlanName") != null ? request.getParameter("healthPlanName") : null;
		String healthPlanLevel = request.getParameter("healthPlanLevel") != null ? request.getParameter("healthPlanLevel") : null;

		String dentalPlanIssuer = request.getParameter("dentalPlanIssuer") != null ?  request.getParameter("dentalPlanIssuer") : null;
		String dentalPlanName = request.getParameter("dentalPlanName") != null ?  request.getParameter("dentalPlanName") : null;
		String dentalPlanLevel = request.getParameter("dentalPlanLevel") != null ?  request.getParameter("dentalPlanLevel") : null;

		model.addAttribute("healthPlanIssuer", healthPlanIssuer);
		session.setAttribute("healthPlanIssuer", healthPlanIssuer);
		model.addAttribute("healthPlanName", healthPlanName);
		session.setAttribute("healthPlanName", healthPlanName);
		model.addAttribute("healthPlanLevel", healthPlanLevel);
		session.setAttribute("healthPlanLevel", healthPlanLevel);

		model.addAttribute("dentalPlanIssuer", dentalPlanIssuer);
		session.setAttribute("dentalPlanIssuer", dentalPlanIssuer);
		model.addAttribute("dentalPlanName", dentalPlanName);
		session.setAttribute("dentalPlanName", dentalPlanName);
		model.addAttribute("dentalPlanLevel", dentalPlanLevel);
		session.setAttribute("dentalPlanLevel", dentalPlanLevel);

		model.addAttribute("currentDate", currentDate);
		
		return getEsignaturePageUrl();
	}

	private void setStateSubsidyFlags(Model model, HttpServletRequest request, HttpSession session) {
		String stateSubsidyVal = request.getParameter("stateSubsidyVal");
		model.addAttribute(HAS_STATE_SUBSIDY, false);
		session.setAttribute(HAS_STATE_SUBSIDY, false);
		if (isNotNullAndEmpty(stateSubsidyVal)){
			model.addAttribute(HAS_STATE_SUBSIDY, true);
			session.setAttribute(HAS_STATE_SUBSIDY, true);
		}		
	}

	@RequestMapping(value = "/enrollment/esignature", method = RequestMethod.POST)
	public String esignatureSubmit(Model model, HttpServletRequest request) throws GIException{
		LOGGER.info("inside Electronic Signature post  ");
		
		String cartIdStr = "";
		String enrollmentType = "";
		boolean enrl_AHBX_WSDL_CALL_ENABLE= new Boolean( DynamicPropertiesUtil.getPropertyValue("enrollment.ahbxWsdlCallEnable"));
		boolean enroll_LDAP_CALL_ENABLE=new Boolean( DynamicPropertiesUtil.getPropertyValue("enrollment.ldapCallEnable"));
		HttpSession session = request.getSession();
		model.addAttribute("isBBUser",isPHIXBBUser());
		try {
			if (!session.getAttribute(CART_ID).equals("")) {
				cartIdStr = (String) session.getAttribute(CART_ID);
				if(StringUtils.isNotBlank(cartIdStr) && cartIdStr.startsWith(GhixJasyptEncrytorUtil.GENC_TAG_START)){
					cartIdStr = ghixJasyptEncrytorUtil.decryptGencStringByJasypt(cartIdStr);
				}
				enrollmentType = session.getAttribute(ENROLLMENT_TYPE).toString();
			}
		} catch (Exception e) {
			return getEsignaturePageUrl();
		}

		String pin_esig = request.getParameter("pin_esig");
		String applicant_esig = request.getParameter("applicant_esig");

		if(enrl_AHBX_WSDL_CALL_ENABLE && enroll_LDAP_CALL_ENABLE){
			try {
				AccountUser actUser = userService.getLoggedInUser();
				LOGGER.info("User ID = " +actUser.getId());
				if(actUser!=null && (actUser.getExternPin()!=null && actUser.getExternPin().equalsIgnoreCase(pin_esig))){
					LOGGER.info(" Esign Pin is valid , user's PIN : " + pin_esig + " PIN from user service : " + actUser.getExternPin());
				}else{
					String userEnteredPIN = pin_esig != null ? pin_esig : "null";
					String accountUserPIN = actUser.getExternPin() != null ? actUser.getExternPin() : "null";
					LOGGER.info("  Esign Pin is not valid , user's PIN : " + userEnteredPIN + " PIN from user service : " + accountUserPIN );
					LOGGER.info("  invalid Esign Pin  Message  : "+DynamicPropertiesUtil.getPropertyValue("enrollment.errMessage.E-002"));

					model.addAttribute("errorMsg", "");
					model.addAttribute(CART_ID,cartIdStr );
					model.addAttribute("pin_esig", pin_esig);
					model.addAttribute("applicant_esig", applicant_esig);
					model.addAttribute("showpin", DISPLAY_SIGNATURE_PIN);
					model.addAttribute("showFileTaxReturn", DISPLAY_FILE_TAX_RETURN);
					model.addAttribute(ENROLLMENT_TYPE, enrollmentType);
					model.addAttribute("enrollment_type_shop", GhixConstants.ENROLLMENT_TYPE_SHOP);
					model.addAttribute("enrollment_type_individual", GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
					model.addAttribute(TAX_YEAR_OBJ,session.getAttribute(TAX_YEAR_OBJ));
					model.addAttribute("taxFilersName", session.getAttribute("taxFilersName"));
					model.addAttribute("taxCreditTerm", session.getAttribute("taxCreditTerm"));
					model.addAttribute("showMenu", false);
					model.addAttribute(ENR_ERROR_MSG, "Your enrollment could not be processed");
					if(DynamicPropertiesUtil.getPropertyValue("enrollment.errMessage.E-002")!=null && 
							!DynamicPropertiesUtil.getPropertyValue("enrollment.errMessage.E-002").equalsIgnoreCase("")
						){
						model.addAttribute(ENR_ERROR_MSG_2, DynamicPropertiesUtil.getPropertyValue("enrollment.errMessage.E-002"));
					}else{
						model.addAttribute(ENR_ERROR_MSG_2, "The PIN you entered does not match the one for your account" );
					}
					
					String aptcVal = (isNotNullAndEmpty(session.getAttribute("aptc")))
							? String.valueOf((Float) session.getAttribute("aptc"))
							: null;
					if (isNotNullAndEmpty(aptcVal)) {
						model.addAttribute("aptc", aptcVal);
					} else {
						model.addAttribute("aptc", "");
					}
					
					if (isNotNullAndEmpty(session.getAttribute(HAS_STATE_SUBSIDY))) {
						model.addAttribute(HAS_STATE_SUBSIDY, session.getAttribute(HAS_STATE_SUBSIDY));
					}

					if (isNotNullAndEmpty(session.getAttribute("healthPlanIssuer"))) {
						model.addAttribute("healthPlanIssuer", session.getAttribute("healthPlanIssuer"));
					}

					if (isNotNullAndEmpty(session.getAttribute("healthPlanName"))) {
						model.addAttribute("healthPlanName", session.getAttribute("healthPlanName"));
					}

					if (isNotNullAndEmpty(session.getAttribute("healthPlanLevel"))) {
						model.addAttribute("healthPlanLevel", session.getAttribute("healthPlanLevel"));
					}

					if (isNotNullAndEmpty(session.getAttribute("dentalPlanIssuer"))) {
						model.addAttribute("dentalPlanIssuer", session.getAttribute("dentalPlanIssuer"));
					}

					if (isNotNullAndEmpty(session.getAttribute("dentalPlanName"))) {
						model.addAttribute("dentalPlanName", session.getAttribute("dentalPlanName"));
					}

					if (isNotNullAndEmpty(session.getAttribute("dentalPlanLevel"))) {
						model.addAttribute("dentalPlanLevel", session.getAttribute("dentalPlanLevel"));
					}
	
					Date currentDate = new TSDate();
					model.addAttribute("currentDate", currentDate);

					model.addAttribute("fininfoId", "");
					model.addAttribute("employeeFullName", "");

					Map<String, Object> mapOfModel = model.asMap();
					LOGGER.info("The current map in the models " + mapOfModel);

					return getEsignaturePageUrl();
				}

			} catch (InvalidUserException e) {
				LOGGER.error("Esign Pin failure - Error Message : " + e.getMessage());
				LOGGER.error("Cause : " + e.getCause());
			}
		}//end of ahbx call true
		
		String _POST_RESP = null;
		EnrollmentResponse  enrollmentResponse= null ;
		try {
			Map<String,Object> inputval = new HashMap<String,Object>();
			inputval.put("orderId",cartIdStr);
			inputval.put("pin_esig", pin_esig);
			inputval.put("applicant_esig", applicant_esig);
			_POST_RESP = ghixRestTemplate.postForObject(EnrollmentEndPoints.CREATE_INDIVIDUAL_ENROLLMENT_URL, inputval, String.class);

			XStream xstream = GhixUtils.getXStreamStaxObject();
			enrollmentResponse = (EnrollmentResponse) xstream.fromXML(_POST_RESP);
		} catch (RestClientException re) {
			LOGGER.error("" ,re);
			enrollmentResponse= new EnrollmentResponse();
			enrollmentResponse.setModuleStatusCode("E-003");
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Error occured while calling enrollment creation \n "+re.getStackTrace());
		}catch (Exception e) {
			LOGGER.error("" ,e);
			enrollmentResponse= new EnrollmentResponse();
			enrollmentResponse.setModuleStatusCode("E-003");
			enrollmentResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			enrollmentResponse.setErrMsg("Error occured while calling enrollment creation \n "+e.getStackTrace());
		}

		if (enrollmentResponse!=null && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
			List<Enrollment> enrollmentList = enrollmentResponse.getEnrollmentList();
			List<Map<String, String>> disEnrollmentList = enrollmentResponse.getDisEnrollmentList(); 
			if(enrollmentList != null){
				session.setAttribute(ENROLLMENT_LIST, enrollmentList);
				session.setAttribute(ENROLLEE_NAME_MAP, enrollmentResponse.getEnrolleeNameMap());
			}
			if(disEnrollmentList != null){
				session.setAttribute(DIS_ENROLLMENT_LIST, disEnrollmentList);
			}

			return "redirect:/enrollment/orderconfirm";

		}else{
			if(enrollmentResponse!=null && enrollmentResponse.getModuleStatusCode()!=null){
				if (enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-002")|| enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-001") 
						|| enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-003") || enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-006")){
					model.addAttribute("errorMsg", "");
					model.addAttribute(CART_ID,cartIdStr );
					model.addAttribute("pin_esig", pin_esig);
					model.addAttribute("applicant_esig", applicant_esig);
					model.addAttribute("showpin", DISPLAY_SIGNATURE_PIN);
					model.addAttribute("showFileTaxReturn", DISPLAY_FILE_TAX_RETURN);
					model.addAttribute(ENROLLMENT_TYPE, enrollmentType);
					model.addAttribute("enrollment_type_shop", GhixConstants.ENROLLMENT_TYPE_SHOP);
					model.addAttribute("enrollment_type_individual", GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
					model.addAttribute(TAX_YEAR_OBJ,session.getAttribute(TAX_YEAR_OBJ));
					model.addAttribute("taxFilersName", session.getAttribute("taxFilersName"));
					model.addAttribute("taxCreditTerm", session.getAttribute("taxCreditTerm"));
					model.addAttribute("showMenu", false);

					LOGGER.info("Enrollment Web Controller  failure Message: "+enrollmentResponse.getErrMsg()+enrollmentResponse.getModuleStatusCode());
					String gimErrCode = null;
					if(enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("E-001")){
						if(enrollmentResponse.getErrMsg()!=null){//IND20 Failed : 
							/*String []errArray = enrollmentResponse.getErrMsg().split("~");
							LOGGER.error("IND20 Error message : "+ errArray[1]);
							model.addAttribute(ENR_ERROR_MSG, "Your enrollment could not be processed");
							model.addAttribute(ENR_ERROR_MSG_2, errArray[2] +" Error time : " + new TSDate());*/
							gimErrCode= "ENRL-00006";
						}
					}else{
						/*model.addAttribute(ENR_ERROR_MSG, "Your enrollment could not be processed");
						model.addAttribute(ENR_ERROR_MSG_2, "Error Code : 000. Error time : " + new TSDate());*/
						gimErrCode= "ENRL-00003";

						LOGGER.error("Enrollment failed  - Error Code : "+enrollmentResponse.getModuleStatusCode() +" Error time : " + new TSDate());
					}
					GIMonitor gim= null;

					try{
						gim = gIMonitorService.saveOrUpdateErrorLog(gimErrCode, new TSDate(), null, "---Failed transaction CART_ID :  "+cartIdStr +" --- \n "+ enrollmentResponse.getErrMsg(), userService.getLoggedInUser(), "/enrollment/esignature", "ENROLLMENT", null);
					}catch(Exception e){
						LOGGER.error("Error logging to GI Monitor "+ e.getMessage());
					}
					model.addAttribute(ENR_ERROR_MSG, "Your enrollment could not be processed");
					model.addAttribute(ENR_ERROR_MSG_2, gim!=null ? gim.getId() : "" );
					
					if (enrollmentResponse.getModuleStatusCode().equalsIgnoreCase("EN-SSAP-001")) {
						model.addAttribute("applicationErrorStatus", true);
					} else {
						model.addAttribute("applicationErrorStatus", false);
					}
					
				}
			}
			return getEsignaturePageUrl();
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value= "/enrollment/orderconfirm", method = RequestMethod.GET)
	public String orderconfirm(Model model, HttpServletRequest request) {
		LOGGER.info("inside Enrollment Order Confirm metod");
		HttpSession session = request.getSession();
		Date PaymentDate = null;
		String enrollmentType = GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL;
		
		/*
		HIX-109329
		if(session.getAttribute(CART_ID) ==null ){
			return "redirect:hix/j_spring_security_logout";
		}*/
	
		if(session.getAttribute(ENROLLMENT_TYPE) != null && session.getAttribute(ENROLLMENT_TYPE).toString().trim().length() >0 ){
			enrollmentType = session.getAttribute(ENROLLMENT_TYPE).toString().trim().toUpperCase();
		}
		Map<String, List<Enrollment>> enrollmentMap = new TreeMap<String, List<Enrollment>>();
		Map<String, String> disEnrollMap = new HashMap<String, String>();
		Map<Integer,String> enrolleeNameMap = new HashMap<Integer, String>();
		if(session.getAttribute(ENROLLMENT_LIST) != null){
			List<Enrollment>  enrollmentList = (List<Enrollment>) session.getAttribute(ENROLLMENT_LIST);
			enrollmentMap = splitEnrollmantbyPLAN(enrollmentList, enrollmentType,model);
		}
		if(session.getAttribute(ENROLLEE_NAME_MAP) != null){
			enrolleeNameMap =  (Map<Integer, String>) session.getAttribute(ENROLLEE_NAME_MAP);
		}
		
		if(session.getAttribute(DIS_ENROLLMENT_LIST) != null){
			List<Map<String, String>> disEnrollmentList = (List<Map<String, String>>) session.getAttribute(DIS_ENROLLMENT_LIST);
			disEnrollMap = splitDisEnrollmentByPlan(disEnrollmentList);
		}
				
		model.addAttribute("isBBUser",isPHIXBBUser());
		model.addAttribute("enrollmentMap",enrollmentMap);
		model.addAttribute("disEnrollmentMap",disEnrollMap);
		model.addAttribute(CART_ID, session.getAttribute(CART_ID));
		model.addAttribute(ENROLLMENT_TYPE, enrollmentType);
		model.addAttribute("enrolleeNameMap",enrolleeNameMap);
		model.addAttribute("errorMsg", "");
		model.addAttribute("showDisclaimers", Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue("enrollment.displayProperties.displayConfirmationDisclaimers")));
		model.addAttribute("showChngPlans", Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue("enrollment.displayProperties.displayConfirmationMakingChangesToYourPlans")));
		model.addAttribute("redirectLink", GhixConstants.AFTER_ENROLLMENT_ACN_REDIRECT_LINK);
		model.addAttribute("enrollment_type_individual", GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
		LOGGER.info("Redirecting to  Enrollment Order Confirm page");

		if(session != null){
			session.removeAttribute("aptc");
//			session.removeAttribute(CART_ID);
//			session.removeAttribute(ENROLLMENT_TYPE);
			session.removeAttribute(TAX_YEAR_OBJ);
			session.removeAttribute("taxFilersName");
			session.removeAttribute("taxCreditTerm");
//			session.removeAttribute(ENROLLMENT_LIST);
//			session.removeAttribute(ENROLLEE_NAME_MAP);
//			session.removeAttribute(DIS_ENROLLMENT_LIST);
		}
		// populating EnrollmentPaymentDTO and adding to the session for the payment process.  
		if(enrollmentType.equalsIgnoreCase(GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL) && enrollmentMap!=null){
			EnrollmentPaymentDTO enrollmentPaymentDTO = null;
			if(enrollmentMap.get(Plan.PlanInsuranceType.HEALTH.toString())!=null){
				enrollmentPaymentDTO = getEnrollmentPaymentDTO((List<Enrollment>)enrollmentMap.get(Plan.PlanInsuranceType.HEALTH.toString()));
				session.setAttribute(FINANCE_ENROLLMENT_HEALTH_KEY, enrollmentPaymentDTO);
				if(enrollmentPaymentDTO != null && isNotNullAndEmpty(enrollmentPaymentDTO.getPaymentUrl())){

					model.addAttribute("ShowHealthPaymentButton", "true");
				}
				else{
					model.addAttribute("ShowHealthPaymentButton", "false");
				}
			}

			if(enrollmentMap.get(Plan.PlanInsuranceType.DENTAL.toString())!=null){
				enrollmentPaymentDTO = getEnrollmentPaymentDTO((List<Enrollment>)enrollmentMap.get(Plan.PlanInsuranceType.DENTAL.toString()));
				session.setAttribute(FINANCE_ENROLLMENT_DENTAL_KEY, enrollmentPaymentDTO);
				enrollmentPaymentDTO =  (EnrollmentPaymentDTO) session.getAttribute(FINANCE_ENROLLMENT_DENTAL_KEY);
				if(enrollmentPaymentDTO != null && isNotNullAndEmpty(enrollmentPaymentDTO.getPaymentUrl())){

					model.addAttribute("ShowDentalPaymentButton", "true");
				}
				else{
					model.addAttribute("ShowDentalPaymentButton", "false");
				}
			}

			if(enrollmentPaymentDTO != null && enrollmentPaymentDTO.getBenefitEffectiveDate() != null){
				Date benefitEffectiveDate = enrollmentPaymentDTO.getBenefitEffectiveDate();
				if(benefitEffectiveDate != null){
					PaymentDate = getPaymentDate(benefitEffectiveDate);
				}
			}
		}
		
		model.addAttribute(FINANCE_ENROLLMENT_HEALTH_KEY,FINANCE_ENROLLMENT_HEALTH_KEY);
		model.addAttribute(FINANCE_ENROLLMENT_DENTAL_KEY,FINANCE_ENROLLMENT_DENTAL_KEY);
		
		if(PaymentDate!=null){
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		    String formattedPaymentDate = sdf.format(PaymentDate);
			model.addAttribute("paymentDate",formattedPaymentDate);
		}

		return getOrderConfirmPageUrl();
	}
   private Date getPaymentDate(Date benefitEffectiveDate)
    {   Date rtnDate = null;
    	Calendar currentDate = TSCalendar.getInstance();
    	currentDate.setTime(benefitEffectiveDate);
    	currentDate.set(Calendar.MONTH, -1);
    	currentDate.set(Calendar.DAY_OF_MONTH, 15);  
    	rtnDate =  currentDate.getTime();
    	return rtnDate;
    }
	protected String getOrderConfirmRedirectUrl(List<Enrollment> enrollmentList){
		String redirectUrl = "";
		
		if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equalsIgnoreCase(EXCHANGE_TYPE_PHIX)){
			if(enrollmentList != null && enrollmentList.size() > 1){
				Enrollment enrollment = enrollmentList.get(0);
				SingleIssuerResponse issuerDto = getIssuerInfoById(enrollment.getIssuerId());
				if(issuerDto != null){
					redirectUrl = issuerDto.getPaymentUrl();
				}
			}
		}
		
		else if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equalsIgnoreCase(EXCHANGE_TYPE_STATE) 
				&& DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_CA)){
			redirectUrl = GhixConstants.AFTER_ENROLLMENT_ACN_REDIRECT_LINK;
		}
		
		return redirectUrl;
	}

	private boolean isPHIXBBUser(){
		boolean isBBUser=false;
		try{
			
			if(BENEFIT_BAY_USER!=null && (DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE)!=null && DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equalsIgnoreCase(EXCHANGE_TYPE_PHIX))){
				AccountUser actUser=userService.getLoggedInUser();
				if(actUser!=null && actUser.getUsername()!=null &&actUser.getUsername().equalsIgnoreCase(BENEFIT_BAY_USER) ){
					isBBUser=true;
				}
			}
			
		}catch(Exception e){
			LOGGER.error("Exception while trying to get logged in user in isBBUser()"+e.toString());
		}
		return isBBUser;
	}
	private Map<String, String> splitDisEnrollmentByPlan(List<Map<String, String>> disEnrollmentList) {
		StringBuffer healthDisEnrollMembernames = new StringBuffer();
		StringBuffer dentalDisEnrollMembernames = new StringBuffer();
		if(disEnrollmentList != null){
			for (Map<String, String> disEnrollMap : disEnrollmentList) {
				if(disEnrollMap != null){
					if(disEnrollMap.get("planType").compareToIgnoreCase(HEALTH) == 0){
						if(healthDisEnrollMembernames.length() > 0){
							healthDisEnrollMembernames.append(", ");
						}

						healthDisEnrollMembernames.append(disEnrollMap.get("fName"));
						healthDisEnrollMembernames.append(" ");
						healthDisEnrollMembernames.append(disEnrollMap.get("lName"));
					}
					else if(disEnrollMap.get("planType").compareToIgnoreCase(DENTAL) == 0){
						if(dentalDisEnrollMembernames.length() > 0){
							dentalDisEnrollMembernames.append(", ");
						}

						dentalDisEnrollMembernames.append(disEnrollMap.get("fName"));
						dentalDisEnrollMembernames.append(" ");
						dentalDisEnrollMembernames.append(disEnrollMap.get("lName"));
					}
				}
			}
		}
		Map<String, String> disEnrollMap = new HashMap<String, String>();
		if(healthDisEnrollMembernames.length() > 0){
			disEnrollMap.put(HEALTH, healthDisEnrollMembernames.toString());
		}

		if(dentalDisEnrollMembernames.length() > 0){
			disEnrollMap.put(DENTAL, dentalDisEnrollMembernames.toString());
		}
		return disEnrollMap;
	}

	private Map<String, List<Enrollment>> splitEnrollmantbyPLAN(List<Enrollment> enrollmentList , String enrollment_type,Model model){
		Map<String, List<Enrollment>> enrollmentMap= new TreeMap<String, List<Enrollment>>( new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				int result = 0;
				if(null != o1 && null != o2){
					return - o1.compareTo(o2);
				}
				return result;
			}
		});
		Float totalAptcContribution = new Float(0.00f);
		Float totalCost = new Float(0.00f);
		Float totalEmployeerContribution = new Float(0.00f);
	    Float totalMonthlyPremium = new Float(0.00f);

		BigDecimal totalStateSubsidyContribution = null;

		List<Enrollment> healthEnrollmentList=new ArrayList<Enrollment>();
		List<Enrollment> dentalEnrollmentList=new ArrayList<Enrollment>();
		List<Enrollment> lifeEnrollmentList=new ArrayList<Enrollment>();
		List<Enrollment> visionEnrollmentList=new ArrayList<Enrollment>();
		List<Enrollment> ameEnrollmentList=new ArrayList<Enrollment>();
		List<Enrollment> stmEnrollmentList=new ArrayList<Enrollment>();
		List<Enrollment> medicareEnrollmentList=new ArrayList<Enrollment>();
		if(enrollmentList!=null){
			for(Enrollment enr : enrollmentList){
				if(enr.getPlanId()!=null){
					enr.setPlanId(enr.getPlanId());
				}
				if(enr.getPlanLevel() != null){
					enr.setPlanLevel(enr.getPlanLevel().toUpperCase().trim());
				}
				if(null != enr.getIssuerId()){
					
					enr.setIssuerLogo(appUrl + DOCUMENT_URL+ ghixJasyptEncrytorUtil.encryptStringByJasypt(""+enr.getIssuerId()));
					
				}
				if(enrollment_type.compareToIgnoreCase(GhixConstants.ENROLLMENT_TYPE_SHOP) == 0){
					if(enr.getGrossPremiumAmt() != null){
						totalCost += enr.getGrossPremiumAmt();
						totalMonthlyPremium += enr.getGrossPremiumAmt();
					}
					if(enr.getEmployerContribution() != null){
						totalEmployeerContribution += enr.getEmployerContribution();
						totalCost -= enr.getEmployerContribution();
					}
				}
				else if(enrollment_type.compareToIgnoreCase(GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL) == 0){
					if(enr.getGrossPremiumAmt()!=null){
						totalCost += enr.getGrossPremiumAmt();
						totalMonthlyPremium += enr.getGrossPremiumAmt();
					}
					if(enr.getAptcAmt()!=null){
						totalAptcContribution += enr.getAptcAmt();
						totalCost -= enr.getAptcAmt();
					}
					if(enr.getStateSubsidyAmt() != null){
						if(totalStateSubsidyContribution != null) {
							totalStateSubsidyContribution = totalStateSubsidyContribution.add(enr.getStateSubsidyAmt());
						}
						else{
							totalStateSubsidyContribution = enr.getStateSubsidyAmt();
						}
						totalCost -= totalStateSubsidyContribution.floatValue();
					}
				}
				//if to total cost is negative then setting it to zero (0).
				if(totalCost<0){
					totalCost=0.00f;
				}
				//			  if(enr.getPlan() != null && enr.getPlan().getInsuranceType()!=null){
				if(enr.getInsuranceTypeLkp() != null && enr.getInsuranceTypeLkp().getLookupValueLabel() != null){
					String insuranceType = enr.getInsuranceTypeLkp().getLookupValueLabel();
					if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
						healthEnrollmentList.add(enr);
					}
					else if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
						dentalEnrollmentList.add(enr);
					}
					else if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.LIFE.toString())){
						lifeEnrollmentList.add(enr);
					}
					else if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.VISION.toString())){
						visionEnrollmentList.add(enr);
					}
					else if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.AME.toString())){
						ameEnrollmentList.add(enr);
					}
					else if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.STM.toString())){
						stmEnrollmentList.add(enr);
					}
					else if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.MEDICARE.toString())){
						medicareEnrollmentList.add(enr);
					}
				}
			}
			if(healthEnrollmentList.size()>0){
				enrollmentMap.put(Plan.PlanInsuranceType.HEALTH.toString(), healthEnrollmentList);
			}
			if(dentalEnrollmentList.size()>0){
				enrollmentMap.put(Plan.PlanInsuranceType.DENTAL.toString(), dentalEnrollmentList);
			}
			if(lifeEnrollmentList.size()>0){
				enrollmentMap.put(Plan.PlanInsuranceType.LIFE.toString(), lifeEnrollmentList);
			}
			if(visionEnrollmentList.size()>0){
				enrollmentMap.put(Plan.PlanInsuranceType.VISION.toString(), visionEnrollmentList);
			}
			if(ameEnrollmentList.size()>0){
				enrollmentMap.put(Plan.PlanInsuranceType.AME.toString(), ameEnrollmentList);
			}
			if(stmEnrollmentList.size()>0){
				enrollmentMap.put(Plan.PlanInsuranceType.STM.toString(), stmEnrollmentList);
			}
			if(medicareEnrollmentList.size()>0){
				enrollmentMap.put(Plan.PlanInsuranceType.MEDICARE.toString(), medicareEnrollmentList);
			}
		}
		
		model.addAttribute("totalCost", totalCost);
		model.addAttribute("totalMonthlyPremium", totalMonthlyPremium);
		model.addAttribute("totalEmployerContribution", totalEmployeerContribution);
		model.addAttribute("totalAptcContribution", totalAptcContribution);
		if(totalStateSubsidyContribution != null){
		    model.addAttribute("totalStateSubsidyContribution", totalStateSubsidyContribution);
        }
		return enrollmentMap;
	}
	@RequestMapping(value = "/enrollment/adminupdateconfirm", method = RequestMethod.GET)
	public String admunUpdateConfirm() {

		return "enrollment/adminupdateconfirm";
	}
	
	protected String getOrderConfirmPageUrl(){
        String orderConfirmUrl = "enrollment/orderconfirm";
        
        if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equalsIgnoreCase(EXCHANGE_TYPE_PHIX)){
               orderConfirmUrl = "enrollment/phix_orderconfirm";
               
        }else if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_ID)
        		|| DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_NV) 
        		|| DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_MN)){
               orderConfirmUrl = "enrollment/ghix_id_orderconfirm";
               
        }else if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_CA)){
               orderConfirmUrl = "enrollment/ghix_ca_orderconfirm";
               
        }       
        
        return orderConfirmUrl;
	}
 
	 protected String getEsignaturePageUrl(){
        String orderConfirmUrl = "enrollment/esignature";

        if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE).equalsIgnoreCase(EXCHANGE_TYPE_PHIX)){
               orderConfirmUrl = "enrollment/phix_esignature";
        
        }else if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_ID)
        		|| DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_NV)){
        	orderConfirmUrl = "enrollment/ghix_id_esignature";
        	
        }if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_MN)){
         	 orderConfirmUrl = "enrollment/ghix_mn_esignature";
          	 
       }else if(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(EXCHANGE_STATE_CA)){ 
               orderConfirmUrl = "enrollment/ghix_ca_esignature";
        }
        return orderConfirmUrl;
	 }
	private TaxYear getTaxYear(String coverageDate){
		
		int currentYear = 0;
		DateFormat formatter = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
		Date date = null;

		try {
			date = formatter.parse(coverageDate);
		} catch (ParseException e) {
			LOGGER.info( e + "esignature Coverage Date Parseing ");
		}

		if(date != null)
		{
			Calendar c = TSCalendar.getInstance();
			c.setTime(date);
			currentYear = c.get(Calendar.YEAR);
		}
		//LOGGER.info("Calling Tax filling year REST service with coverage date : "+currentYear);
		String taxYearResp = ghixRestTemplate.postForObject(EnrollmentEndPoints.FIND_TAX_FILING_DATE_FOR_YEAR_URL, Integer.toString(currentYear),String.class);
		//LOGGER.info("Calling Tax filling year REST service SUCCESSFULL and response is : " + taxYearResp);

		Gson gson = new Gson();
		TaxYear taxYearObj = new TaxYear();
		taxYearObj = gson.fromJson(taxYearResp, taxYearObj.getClass());

		//LOGGER.info("Tax Year Resp is : " + taxYearObj);
		return taxYearObj;
	}
	
	/**
	 * Get issuer information from Plan Management API
	 * @param issuerId
	 * @return SingleIssuerResponse
	 */
	private SingleIssuerResponse getIssuerInfoById(final Integer issuerId){
		SingleIssuerResponse singleIssuerResponse = new SingleIssuerResponse();
		IssuerRequest issuerRequest = new IssuerRequest();
		issuerRequest.setId(issuerId.toString());
		try{
			singleIssuerResponse =  ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUERS_BY_ID, issuerRequest, SingleIssuerResponse.class);
		}catch(Exception e){
			LOGGER.error("Error fetching issuer information from plan management API", e);
		}
		return singleIssuerResponse;
	}
	
	private <T> boolean isNotNullAndEmpty(T e){
		boolean isNotNUll=false;
		if((e!=null) && !(e.toString().isEmpty()) && !(e.toString().equalsIgnoreCase("null"))){
			isNotNUll=true;
		}
		return isNotNUll;
	}
	
	private EnrollmentPaymentDTO getEnrollmentPaymentDTO(List<Enrollment> enrollmentList){
		EnrollmentPaymentDTO enrollmentPaymentDTO = new EnrollmentPaymentDTO();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		if(null  != enrollmentList && !enrollmentList.isEmpty()){
			EnrollmentResponse enrollmentResponse = null;
			String xmlResponse = null;
			final Map<String, String> request = new HashMap<String,String>();
			request.put("id", String.valueOf(enrollmentList.get(0).getId()));
			xmlResponse = ghixRestTemplate.getForObject(GhixEndPoints.ENROLLMENT_URL+"/enrollment/getEnrollmentPaymentInfoByID/{id}", String.class,request);
			enrollmentResponse = (EnrollmentResponse)xstream.fromXML(xmlResponse);
			if (null != enrollmentResponse && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				enrollmentPaymentDTO = enrollmentResponse.getEnrollmentPaymentDTO();
			}else{
				LOGGER.error("Could not retrieve enrollment payment DTO");
			}
		}
		return enrollmentPaymentDTO;
	}
}
