function showPieChart(enrolled, waived_qualified, waived_unqualified, noresponse){
	        chart = new Highcharts.Chart({
	            chart: {
	                renderTo: 'container',
	                plotBackgroundColor: null,
	                plotBorderWidth: null,
	                plotShadow: false,
	                height: 280,
	                width:690
	            },
	            title: {
	                text: ''
	            },
	            colors: [
	                 	'#4f81bd',
	                 	'#c0504d',
	                 	'#9bbb59',
	                 	'#8064a2'
	                 	],
	                 	credits: {
	                             enabled: false
	                         },
	            tooltip: {
	                formatter: function() {
	                    return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(0)+' %';
	                }
	            },
	            exporting:{ buttons: {
					 printButton:{ enabled:false },
					 exportButton: { enabled:false }
				 	}
	  			},
	            plotOptions: {
	                pie: {
	                    allowPointSelect: true,
	                    cursor: 'pointer',
	                    dataLabels: {
	                        enabled: true,
	                        color: '#000000',
	                        connectorColor: '#000000',
	                        formatter: function() {
	                            return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(0) +' %';
	                        }
	                    },
	                    showInLegend: true,
	                }
	            },
	            
	            series: [{
	                type: 'pie',
	                name: 'Employee Coverage',
	                data: [
	                    ['Enrolled',   enrolled],
	                    ['Waived: Qualifying Coverage ', waived_qualified],
	                    ['Waived: No Qualified Coverage ', waived_unqualified],
	                    ['No Response',  noresponse]
	                ]
	            }]
	        });
	        
	        //for accessibility
	        $(".highcharts-legend").attr("aria-hidden","true");
}	   