package com.getinsured.hix.entity.admin.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.entity.DesignateEntityDTO;
import com.getinsured.hix.dto.entity.EntityRequestDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.entity.admin.service.AssisterIndTriggerService;
import com.getinsured.hix.entity.assister.controller.AHBXRestCallInvoker;
import com.getinsured.hix.entity.assister.repository.IAssisterRepository;
import com.getinsured.hix.entity.enrollmententity.repository.ISiteRepository;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.Site.site_type;
import com.getinsured.hix.platform.feature.GiFeature;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

@Service
public class AssisterIndTriggerServiceImpl implements AssisterIndTriggerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterIndTriggerServiceImpl.class);
	
	private static final int TWO_HUNDRED = 200;
	
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private IAssisterRepository assisterRepository;
	@Autowired
	private ISiteRepository siteRepository;
	@Autowired
	private AHBXRestCallInvoker aHBXRestCallInvoker;
	
	@GiFeature("ahbx.aee.ind35.enabled")
	@Override
	public String triggerInd35(Assister assister) {
		List<EntityRequestDTO> entityRequestDTOList = new ArrayList<EntityRequestDTO>();

		String wsResponse = null;
		
		populateAssister(entityRequestDTOList, assister);
		/*
		 * Currently logging response received from AHBX. TODO Need to log
		 * response in db
		 */
		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For Assiter : ").append(assister.getId()).append(" : ");

		LOGGER.info("IND35: Sending Assister Details to ghix-ahbx. Assister ID : "+assister.getId());
		try {
			String strEntityRequestDTOList = GhixUtils.xStreamHibernateXmlMashaling().toXML(entityRequestDTOList);
			wsResponse = restTemplate.postForObject(GhixEndPoints.AHBXEndPoints.SEND_ENTITY_DETAIL,
					strEntityRequestDTOList, String.class);

			strResponse.append("\nResponse is: ").append(wsResponse);

/*			LOGGER.info("IND35: Successfully sent Assister Details over Rest call to Ghix-Ahbx. Received following response : "
					+ strResponse.toString());
*/
			LOGGER.info("IND35: Successfully sent Assister Details over Rest call to Ghix-Ahbx. ");

		} catch (Exception exception) {

			/**
			 * TODO TBD how to handle network breakdown
			 */
			LOGGER.error("IND35: Send Assister Details Web Service failed: Exception is :" , exception);
		}
		return wsResponse;

	}

	private void populateAssister(List<EntityRequestDTO> entityRequestDTOList, Assister assister) { 

		LOGGER.info("Populate Assisters associated with Entity");

		if (assister != null) {
			LOGGER.debug("Create Assister request DTO");
			EntityRequestDTO entityAssisterRequestDTO = new EntityRequestDTO();

			entityAssisterRequestDTO.setRecordId(assister.getId());
			entityAssisterRequestDTO.setRecordType("Assister");
			entityAssisterRequestDTO.setBusinessLegalName(assister.getEntity().getBusinessLegalName());

			entityAssisterRequestDTO.setFirstName(assister.getFirstName());
			entityAssisterRequestDTO.setLastName(assister.getLastName());
			entityAssisterRequestDTO.setEmail(assister.getEmailAddress());
			entityAssisterRequestDTO.setEntityIdNumber(assister.getEntity() != null ? Long.valueOf(assister.getEntity()
					.getId()) : null);

			entityAssisterRequestDTO.setFunctionalIdentifier(assister.getAssisterNumber());
			entityAssisterRequestDTO.setStatus(assister.getStatus());
			

			// Assuming Entity Mailing Location to be Primary Site Location
			if (assister.getMailingLocation() != null) {
				entityAssisterRequestDTO.setAddressLine1(assister.getMailingLocation().getAddress1());
				entityAssisterRequestDTO.setAddressLine2(assister.getMailingLocation().getAddress2());
				entityAssisterRequestDTO.setCity(assister.getMailingLocation().getCity());
				entityAssisterRequestDTO.setState(assister.getMailingLocation().getState());
				entityAssisterRequestDTO.setZipCode(String.valueOf(assister.getMailingLocation().getZip()));
			} else {
				entityAssisterRequestDTO.setAddressLine1("");
				entityAssisterRequestDTO.setAddressLine2("");
				entityAssisterRequestDTO.setCity("");
				entityAssisterRequestDTO.setState("");
				entityAssisterRequestDTO.setZipCode("");
			}

			String contactNumber = "";
			if (assister.getPrimaryPhoneNumber() != null && assister.getPrimaryPhoneNumber().contains("-")) {
				contactNumber = assister.getPrimaryPhoneNumber().replaceAll("-", "");
			} else {
				contactNumber = assister.getPrimaryPhoneNumber();
			}
			entityAssisterRequestDTO.setPhoneNumber(contactNumber);

			entityAssisterRequestDTO.setCertiStatusCode(assister.getCertificationStatus());

			if (assister.getCertificationStatus() != null
					&& EntityConstants.STATUS_CERTIFIED.equalsIgnoreCase(assister.getCertificationStatus().toString())) {
				entityAssisterRequestDTO.setCertiStartDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
						assister.getCertificationDate()));
				entityAssisterRequestDTO.setCertificationNumber(assister.getCertificationNumber());
			} else {
				entityAssisterRequestDTO.setCertiStartDate(null);
			}

			if (assister.getStatusChangeDate() != null) {
				entityAssisterRequestDTO.setStatusDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
						assister.getStatusChangeDate()));
			} else {
				entityAssisterRequestDTO.setStatusDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
						new TSDate()));
			}

			if (assister.getCertificationStatus() != null && EntityConstants.STATUS_CERTIFIED.equalsIgnoreCase(assister.getCertificationStatus().toString())) {
					Calendar date = TSCalendar.getInstance();
					date.setTime(assister.getCertificationDate());
					date.add(Calendar.YEAR, 1);
					Date newdate = date.getTime();
					entityAssisterRequestDTO.setCertiEndDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
							newdate));
				entityAssisterRequestDTO.setCertiRenewalDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN, assister.getReCertificationDate()));
				} else {
					entityAssisterRequestDTO.setCertiEndDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
							new TSDate()));
				entityAssisterRequestDTO.setCertiRenewalDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN, new TSDate()));
			}

			// TODO - Hard-coded it to "False" until available in Payment
			// Information
			entityAssisterRequestDTO.setDirectDepositFlag("False");

			if (assister.getCertificationStatus() != null
					&& EntityConstants.STATUS_PENDING.equalsIgnoreCase(assister.getCertificationStatus())) {
				entityAssisterRequestDTO.setRecordIndicator("New");
			} else {
				entityAssisterRequestDTO.setRecordIndicator("Update");
			}

			LOGGER.debug("Add assister into request DTO");
			entityRequestDTOList.add(entityAssisterRequestDTO);
		}
	
	}
	
	@GiFeature("ahbx.aee.ind54.enabled")
	@Override
	public Assister triggerInd54(Assister assister) {
		LOGGER.info("Looking up Primary Site from DB");

		// Lookup Primary Site information
		List<Site> siteList = siteRepository
				.findByEnrollmentEntityAndSiteType(assister
						.getEntity().getId(), site_type.PRIMARY.toString());

		// Retrieving primary site from the list
		Site primarySite = null;
		if (siteList != null && !siteList.isEmpty()) {
			primarySite = siteList.get(0);
			LOGGER.debug("Retrieving Primary Site from the list");
		}

		LOGGER.info("Send Assister Information call starts");
		String wsResponse = aHBXRestCallInvoker
				.sendAssisterInfoForDelegationCode(assister,
						primarySite);
		LOGGER.info("Send Assister Information call ends.");
		
		DelegationResponse delegationResponse = (DelegationResponse) GhixUtils
				.xStreamHibernateXmlMashaling().fromXML(wsResponse);

		String delegationCode = null;
		if (delegationResponse != null
				&& delegationResponse.getResponseCode() == TWO_HUNDRED) {
			delegationCode = delegationResponse.getDelegationCode();
			assister.setDelegationCode(delegationCode);
			assister = assisterRepository.save(assister);

			LOGGER.debug("Successfully persisted the delegation code into DB");
		}
		
		return assister;
	}
	
	@GiFeature("ahbx.aee.ind47.enabled")
	@Override
	public EntityResponseDTO triggerInd47(DesignateAssister designateAssister) {
		EntityResponseDTO entityResponseDTO = new EntityResponseDTO();
		DesignateEntityDTO designateEntityDTO = null;
		LOGGER.info("IND47: Sending Assister Designation Details to ghix-ahbx : Assister ID : "+designateAssister.getAssisterId());

		designateEntityDTO = populateEntityAssisterDto(designateAssister);

		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For Assister : ").append(designateAssister.getAssisterId()).append(" : ");

		try {
			String wsResponse = restTemplate.postForObject(GhixEndPoints.AHBXEndPoints.SEND_ENTITY_DESIGNATION_DETAIL,
					designateEntityDTO, String.class);

			if(!EntityUtils.isEmpty(wsResponse)){
				entityResponseDTO = (EntityResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(wsResponse);
			}
			
			LOGGER.info("IND47: Successfully sent Assister Designation Details over Rest call to Ghix-Ahbx. ");
		} catch (Exception exception) {
			LOGGER.error("IND47: Send Assister Designation Details Web Service failed: Exception is :"
					, exception);
			throw exception;
		}
		
		return entityResponseDTO;
	}

	/**
	 * Populates {@link DesignateEntityDTO} from {@link DesignateAssister} to
	 * send it to AHBX through REST API
	 * 
	 * @param {@link DesignateAssister} object
	 * @return {@link DesignateEntityDTO} containing request parameters to send
	 *         to AHBX
	 */
	private DesignateEntityDTO populateEntityAssisterDto(DesignateAssister designateAssister) {
		DesignateEntityDTO designEntityDto = new DesignateEntityDTO();

		// Populate the DesignateEntityDTO object
		designEntityDto.setBrokerAssisterId(designateAssister.getAssisterId());
		designEntityDto.setStatus(designateAssister.getStatus().toString());
		designEntityDto.setIndividualId(designateAssister.getIndividualId());

		// Adding record type as per latest AHBX WSDL update to differentiate if
		// request
		// is associated with agent or assister
		designEntityDto.setRecordType(EntityConstants.ASSISTER);

		return designEntityDto;
	}
}
