package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.I94UnexpForeignPassportDocumentID10Type;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for I94DocumentID2Type
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class I94UnexpForeignPassportDocumentID10Wrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(I94UnexpForeignPassportDocumentID10Wrapper.class);
	
	private ObjectFactory factory;
	private I94UnexpForeignPassportDocumentID10Type i94UnexpForeignPassportDocumentID10Type;
	
	private String i94Number;
	private String visaNumber;
	private String passportNumber;
	private String countryOfIssuance;
	private String sevisid;
	private String expiryDate;

	private static final String I94_NUMBER_KEY = "I94Number";
	private static final String VISA_NUMBER_KEY = "VisaNumber";
	private static final String PASSPORT_NUMBER_KEY = "PassportNumber";
	private static final String COUNTRY_KEY = "CountryOfIssuance";
	private static final String SEVIS_ID_KEY = "SEVISID";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	

	public I94UnexpForeignPassportDocumentID10Wrapper(){
		this.factory = new ObjectFactory();
		this.i94UnexpForeignPassportDocumentID10Type = factory.createI94UnexpForeignPassportDocumentID10Type();
	}
	
	public void setI94Number(String i94Number) throws HubServiceException {
		
		if(i94Number == null){
			throw new HubServiceException("No i94 number provided");
		}
		
		this.i94Number = i94Number;
		this.i94UnexpForeignPassportDocumentID10Type.setI94Number(this.i94Number);
	}
	
	public String getI94Number() {
		return i94Number;
	}
	
	public void setVisaNumber(String visaNumber) throws HubServiceException{
		
		if(visaNumber == null){
			throw new HubServiceException("No Visa number provided");
		}
		
		this.visaNumber = visaNumber;
		this.i94UnexpForeignPassportDocumentID10Type.setVisaNumber(visaNumber);
	}
	
	public String getVisaNumber() {
		return visaNumber;
	}
	
	public void setPassportNumber(String passportNumber) throws HubServiceException{
		
		if(passportNumber == null){
			throw new HubServiceException("No Passport number provided");
		}
		
		this.passportNumber = passportNumber;
		this.i94UnexpForeignPassportDocumentID10Type.setPassportNumber(passportNumber);
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	
	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException{
		
		if(countryOfIssuance == null){
			throw new HubServiceException("No Country of Issuance provided");
		}
		
		this.countryOfIssuance = countryOfIssuance;
		this.i94UnexpForeignPassportDocumentID10Type.setCountryOfIssuance(countryOfIssuance);
	}
	
	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}
	
	public void setSevisid(String sevisid) throws HubServiceException{

		if(sevisid == null){
			throw new HubServiceException("No SEVIS ID provided");
		}
		
		this.sevisid = sevisid;
		this.i94UnexpForeignPassportDocumentID10Type.setSEVISID(this.sevisid);
		
	}
	
	public String getSevisid() {
		return sevisid;
	}

	public void setExpiryDate(String expiryDate) {
		
		try{
			this.i94UnexpForeignPassportDocumentID10Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I94 Unexp Foreing Passport Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	
	public I94UnexpForeignPassportDocumentID10Type getI94UnexpForeignPassportDocumentID10Type(){
		return this.i94UnexpForeignPassportDocumentID10Type;
	}
	
	/**
	 * Converts the JSON String to I94DocumentID2Wrapper object
	 * 
	 * @param jsonString - String representation for I94DocumentID2Wrapper
	 * @return I94DocumentID2Wrapper Object
	 */
	public static I94UnexpForeignPassportDocumentID10Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I94DocumentID2Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		I94UnexpForeignPassportDocumentID10Wrapper wrapper = new I94UnexpForeignPassportDocumentID10Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(I94_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.i94Number = (String)tmpObj;
			wrapper.i94UnexpForeignPassportDocumentID10Type.setI94Number(wrapper.i94Number);
		}
		
		
		tmpObj = obj.get(VISA_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.visaNumber = (String)tmpObj;
			wrapper.i94UnexpForeignPassportDocumentID10Type.setVisaNumber(wrapper.visaNumber);
		}
		
		tmpObj = obj.get(PASSPORT_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.passportNumber = (String)tmpObj;
			wrapper.i94UnexpForeignPassportDocumentID10Type.setPassportNumber(wrapper.passportNumber);
		}
		
		tmpObj = obj.get(COUNTRY_KEY);
		if(tmpObj != null){
			wrapper.countryOfIssuance = (String)tmpObj;
			wrapper.i94UnexpForeignPassportDocumentID10Type.setCountryOfIssuance(wrapper.countryOfIssuance);
		}
		
		tmpObj = obj.get(SEVIS_ID_KEY);
		if(tmpObj != null){
			wrapper.sevisid = (String)tmpObj;
			wrapper.i94UnexpForeignPassportDocumentID10Type.setSEVISID(wrapper.sevisid);
		}
	
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.i94UnexpForeignPassportDocumentID10Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating I94DocumentID2Wrapper from JSON, Invalid expiration date",e);
			}
			
		}
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(VISA_NUMBER_KEY, this.visaNumber);
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static I94UnexpForeignPassportDocumentID10Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		I94UnexpForeignPassportDocumentID10Wrapper wrapper = new I94UnexpForeignPassportDocumentID10Wrapper();
		wrapper.setI94Number((String) jsonObj.get(I94_NUMBER_KEY));
		wrapper.setSevisid((String) jsonObj.get(SEVIS_ID_KEY));
		wrapper.setVisaNumber((String) jsonObj.get(VISA_NUMBER_KEY));
		wrapper.setPassportNumber((String) jsonObj.get(PASSPORT_NUMBER_KEY));
		wrapper.setCountryOfIssuance((String) jsonObj.get(COUNTRY_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(VISA_NUMBER_KEY, this.visaNumber);
		obj.put(PASSPORT_NUMBER_KEY, this.passportNumber);
		obj.put(COUNTRY_KEY, this.countryOfIssuance);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
	
}
