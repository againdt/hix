package com.getinsured.iex.hub.smec.servicehandler;

import java.util.Iterator;

import org.json.simple.parser.ParseException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ssap.iex.HouseholdMember;
import com.getinsured.iex.hub.platform.ssap.iex.SSAP;
import com.getinsured.iex.hub.platform.ssap.iex.SSAPTaxHouseHold;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.ObjectFactory;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.VerifyNonESIMECRequest;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.VerifyNonESIMECResponse;
import com.getinsured.iex.hub.smec.prefixmappers.SMECNamespaceMapper;
import com.getinsured.iex.hub.smec.wrapper.NonESIMECIndividualTypeWrapper;
import com.getinsured.iex.hub.smec.wrapper.NonESIMECResponseTypeWrapper;
import com.getinsured.iex.hub.smec.wrapper.VerifyNonESIMECRequestWrapper;

public class SMECApplicantServiceHandler extends
		com.getinsured.iex.hub.platform.AbstractServiceHandler {
	
	private Jaxb2Marshaller marshaller;
	public Object getRequestpayLoad() throws HubServiceException {
		HubServiceException he = null;
		Object response = null;
		try {
			response = getPayLoadFromJSONInput();
		} catch (HubServiceException e) {
			he = e;
		} 
		if (he != null) {
			throw he;
		}
		return response;
	}


	public Object getPayLoadFromJSONInput()
			throws HubServiceException {

		SSAP ssap = null;
		try{
			ssap = new SSAP(getJsonInput());
		}catch(ParseException ex){
			listJSONKeys();
			throw new HubServiceException("Failed to process input data ["+ex.getMessage()+"]", ex);
		}
		
		ObjectFactory objectFactory = new ObjectFactory();
		VerifyNonESIMECRequest request = objectFactory.createVerifyNonESIMECRequest();
		this.setServiceName("SMEC");
		HouseholdMember houseHoldMember = null;
		SSAPTaxHouseHold taxHousehold = null;
		
		String orgCode = (String) getContext().get("organizationCode");
		
		Iterator<SSAPTaxHouseHold> householdCursor = ssap.houseHoldIterator();
		while(householdCursor.hasNext()){
			taxHousehold = householdCursor.next();
			Iterator<HouseholdMember> memberCursor = taxHousehold.iterator();
			
			while(memberCursor.hasNext()){
					VerifyNonESIMECRequestWrapper verifyNonESIMECRequestWrapper = new VerifyNonESIMECRequestWrapper();
					houseHoldMember = memberCursor.next();
					NonESIMECIndividualTypeWrapper nonESIMECIndividualTypeWrapper = new NonESIMECIndividualTypeWrapper();
					try {
						this.extractSSAPData(houseHoldMember, nonESIMECIndividualTypeWrapper);
						this.extractSSAPData(houseHoldMember.getSsnInfo(), nonESIMECIndividualTypeWrapper);
					} catch (HubMappingException e) {
						throw new HubServiceException(e.getMessage(), e);
					}
					verifyNonESIMECRequestWrapper.setNonESIMECIndividualTypeWrapper(nonESIMECIndividualTypeWrapper);
					nonESIMECIndividualTypeWrapper.setOrganizationCode(orgCode);
					request.setNonESIMECRequest(verifyNonESIMECRequestWrapper.getSystemRepresentation());
			}
		}
		return request;
	}

	public String handleResponse(Object responseObject)
			throws HubServiceException {
		
		String response =  null;
		VerifyNonESIMECResponse payload = (VerifyNonESIMECResponse) responseObject;
		
		HubServiceException he = null;
		NonESIMECResponseTypeWrapper payloadWrapper;
		try {
			payloadWrapper = new NonESIMECResponseTypeWrapper(payload.getNonESIMECResponse());
			this.setResponseCode(payloadWrapper.getResponseCode());
			this.requestStatus = RequestStatus.SUCCESS;
			response =  payloadWrapper.toJSONString();
		} catch (HubServiceException e) {
			this.requestStatus  = RequestStatus.FAILED;
			he = e;
		}
		if (he != null) {
			throw he;
		}
		return response;
	}


	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}


	@Override
	public String getServiceIdentifier() {
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov",new SMECNamespaceMapper() {
				
			});
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov",new SMECNamespaceMapper());
		}
		return this.marshaller;
	}
}
