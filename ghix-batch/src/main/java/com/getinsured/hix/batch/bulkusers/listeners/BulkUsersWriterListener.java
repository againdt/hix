package com.getinsured.hix.batch.bulkusers.listeners;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;
import com.getinsured.identity.provision.CreateUserRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BulkUsersWriterListener implements ItemWriteListener<CreateUserRequest>, StepExecutionListener {

	private static final Logger logger = LoggerFactory.getLogger(BulkUsersWriterListener.class);
	private StepExecution stepExecution;

	public BulkUsersWriterListener() {
	}

	@Override
	public void beforeWrite(List<? extends CreateUserRequest> items) {
	}

	@Override
	public void afterWrite(List<? extends CreateUserRequest> items) {
	}

	@Override
	public void onWriteError(Exception exception, List<? extends CreateUserRequest> items) {
		List<String> lstRemoteIds = null;
		
		String exceptionAsString = CommonUtil.convertExceptionToString(exception);
		String jobProcessId = "";
		try {
			if(items != null) {
				lstRemoteIds = items.stream().map(x -> x.getRemoteId()).collect(Collectors.toList());
			}
		} catch (Exception e) {
			jobProcessId = CommonUtil.getJobProcessId(stepExecution.getJobExecution());
			logger.error("onWriterError() jobProcessId : "+jobProcessId+"  -->lstRemoteIds exception : "+CommonUtil.convertExceptionToString(e));			
		}
		jobProcessId = CommonUtil.getJobProcessId(stepExecution.getJobExecution());
		logger.error("onWriteError() jobProcessId : "+jobProcessId+" --> Total No. Of Users : " + ((items != null)? items.size() : 0) + ". \nFailed RemoteIds are : "
				+ ((lstRemoteIds != null) ? lstRemoteIds : "RemoteIds are not found.") + "\nException Details : "
				+ exceptionAsString);
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// TODO Auto-generated method stub
		return null;
	}

}
