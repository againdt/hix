package com.getinsured.hix.batch.enrollment.util;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.enrollment.cms.dsh.sbmi.PolicyType;
import com.getinsured.hix.batch.enrollment.service.EnrollmentCmsOutService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;

public class EnrollmenCmsXmlJobThread implements Callable<List<PolicyType>> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmenCmsXmlJobThread.class);
	
	List<Integer> enrollmentIdList;
	int year;
	int issuerId;
	int index;
	int fileNumber;
	Map<String, String> enrollmentSkipMap;
	EnrollmentCmsOutService enrollmentCmsOutService;
	Long batchExecutionId;
	
	
	public EnrollmenCmsXmlJobThread(List<Integer> enrollmentIdList, int year, int issuerId, int index, Map<String, String> enrollmentSkipMap,Long batchExecutionId, EnrollmentCmsOutService enrollmentCmsOutService) {
		super();
		this.enrollmentIdList = enrollmentIdList;
		this.year = year;
		this.issuerId = issuerId;
		this.index = index;
		this.enrollmentSkipMap = enrollmentSkipMap;
		this.enrollmentCmsOutService = enrollmentCmsOutService;
		this.batchExecutionId=batchExecutionId;
	}

	@Override
	public List<PolicyType> call() throws Exception{
		try {
			return enrollmentCmsOutService.makePolicyTypeList(enrollmentIdList, year, issuerId, enrollmentSkipMap, batchExecutionId);
		} catch (Exception e) {
			if(e.getMessage()!=null && e.getMessage().contains(EnrollmentConstants.BATCH_STOP_MSG)){
				throw e;
			}
			enrollmentSkipMap.put(enrollmentIdList.toString(), EnrollmentUtils.shortenedStackTrace(e, 4));
			LOGGER.error("Exception occurred in EnrollmenCmsXmlJobThread: ", e);
		}
		return null;
	}

}
