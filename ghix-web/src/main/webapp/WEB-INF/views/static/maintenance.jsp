<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" 
	  xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Your Health Idaho</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" />">
  	<link rel="stylesheet" href="<c:url value="/resources/css/screen.css" />">
  <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
    
<body>
	<div id="masthead" class="topbar">
      	<div class="navbar-inner">
        	<div class="container" style="width: auto;">
          		<a class="brand" href="http://www.yourhealthidaho.org"><img class="brand" src='<c:url value="/resources/img/logo_id.png" />' /></a>
          		<ul class="nav pull-right">
            		<li id="fat-menu" class="dropdown">
              			<a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">Get Assistance </a>
              			<ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">1-855-944-3246</a></li>
              			</ul>
            		</li>
          		</ul>
        	</div>
      	</div>
	</div>
	
	<div id="container-wrap" class="row-fluid">
		<div class="container">
			<div class="gutter10">
				<div class="alert alert-info">
				  	<h3>We are building for the future.</h3>
				  	<p>Our webpage is temporarily unavailable during an update. Please check back later.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div id="footer" class="container">
    <footer class="container" role="contentinfo">
 			<div class="row-fluid">
 			</div>
 		</footer>
	</div>
	
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.10.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
</body>
</html>