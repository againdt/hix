<div class="gutter10-l">
	<div class="well-step">
		<div class="txt-left">
			<p>Revise para ver si usted tiene una factura debida.</p>
		</div> <!-- End /.blurb -->
		
		<div class="pie-Descrip clearfix">
			<h4>Que sigue?</h4>
			<div class="gutter10-lr">
				<div class="span3" id="enroll" style="$currentNavData.BUTTON_STYLE$">
				   <button onClick="window.location.href='/hix/shop/employer/employerinvoice'" class="btn btn-primary">Ver Facturas &gt;</button> 
				</div>
			</div><!-- End -->
		</div> <!-- End /.blurb -->   
	</div><!--End /.well-step-->
</div><!--End /.gutter10-l--> 
<script type="text/javascript">
showPieChart();
</script>