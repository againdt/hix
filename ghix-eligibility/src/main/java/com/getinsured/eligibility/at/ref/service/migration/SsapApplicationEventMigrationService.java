package com.getinsured.eligibility.at.ref.service.migration;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

public interface SsapApplicationEventMigrationService {

	SsapApplicationEvent getSsapApplicationEventsByApplicationId(Long ssapApplicationId);

	SsapApplicationEvent createDemoOnlyApplicationEvent(Long ssapApplicationId, Long enrolledApplicationId);

	SsapApplicationEvent createDeniedHouseholdApplicationEvent(Long ssapApplicationId);

	SsapApplicationEvent createDoBApplicationEvent(Long ssapApplicationId);

	SsapApplicationEvent createDuplicateOnlyApplicationEvent(Long ssapApplicationId, Long enrolledApplicationId);

	SsapApplicationEvent createAptcOnlyApplicationEvent(SsapApplication ssapApplication, SepEvents event, Date applicantEventDate, Date applicantReportDate);

	SsapApplicationEvent createCitizenshipApplicationEvent(SsapApplication ssapApplication, Map<String, ApplicantEvent> applicantExtensionEvent);

	SsapApplicationEvent createDeniedHouseholdApplicationEvent(SsapApplication currentApplication, Map<String, ApplicantEvent> applicantEventMap);

	SsapApplicationEvent createRelationshipApplicationEvent(SsapApplication currentApplication);

	SsapApplicationEvent setChangePlan(Long ssapApplicationId);

	SsapApplicationEvent createApplicationEventForAllChanges(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents);

	SsapApplicationEvent createDeniedHouseholdApplicationEventForMixedAndNone(SsapApplication ssapApplication, Map<String, ApplicantEvent> applicantEventMap);

	SsapApplicationEvent createAndUpdateApplicationEventForNonFinancialQE(SsapApplication currentApplication,SsapApplicationEventTypeEnum eventType,String changePlan);

	SsapApplicationEvent updateSsapApplicationEventForNonFinancial(SsapApplication currentApplication, SsapApplicationEventTypeEnum eventType,String changePlan);

	SsapApplicationEvent createApplicationEventForAllChanges(SsapApplication currentApplication,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents, boolean autoRemoveEvents);

	SsapApplicationEvent createApplicationEventForCSChanges(SsapApplication currentApplication,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents, boolean pureCsChange);

	SsapApplicationEvent createApplication_Applicant_SepEvents(Long ssapApplicationId,
			Map<String, SsapApplicantEvent> applicantEventMap, Date eventDate, Date sepEndDate, Integer createdBy,
			boolean keepOnly);

	SsapApplicationEvent createApplication_Applicant_QepEvents(Long ssapApplicationId,
			Map<String, SsapApplicantEvent> applicantEventMap, Date eventDate, Date sepEndDate, Integer createdBy);
}

