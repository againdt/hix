package com.getinsured.eligibility.redetermination.service;

import java.util.List;

import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepTransientDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

public interface SsapCloneApplicantEventService {
	
	List<SsapApplicantEvent> cloneSsapApplicantEvents(SsapApplication currentApplication, SsapApplicationEvent ssapApplicationEvent, List<SsapApplicant> ssapApplicants, List<RequestParamDTO> events, SepTransientDTO sepTransientDTO) throws Exception;
}
