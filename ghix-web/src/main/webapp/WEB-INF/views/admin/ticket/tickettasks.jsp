<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl" %>
<%@ taglib uri="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@ page isELIgnored="false" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil" %>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration" %>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum" %>
<style>
    .Ajaxloader {
        background-color: #F5F5F5;
        background-image: url("/hix/resources/images/Eli-loader.gif");
        background-position: center center;
        background-repeat: no-repeat;
        height: 769px;
        margin: -10% 0;
        opacity: 0.75;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
    }
</style>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>

<%
    String timeZone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);
%>
<script type="text/javascript">
    var showAndHideRowNum = 0;
</script>
<div class="gutter10">
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><spring:message code="lbl.viewTickets"/></li>
        </ul><!--page-breadcrumb ends-->
        <div class="row-fluid">
            <h1>${Subject}</h1>
            <div class="row-fluid">
                <jsp:include page="ticketQuickActionBar.jsp">
                    <jsp:param value="${ticketId}" name="ticketId"/>
                    <jsp:param value="${tkmTicketsObj.tkmWorkflows.type}" name="ticketType"/>
                    <jsp:param value="${tkmTicketsObj.tkmWorkflows.category}" name="ticketCategory"/>
                </jsp:include>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3" id="sidebar">
            <c:set var="tkmTicketsObj" value="${tkmTicketsObj}" scope="request"/>
            <jsp:include page="ticketsummary.jsp">
                <jsp:param value="${ticketId}" name="ticketId"/>
                <jsp:param value="../ticketlist?pageNumber=${gotoPageNumber}" name="backButtonUrl"/>
            </jsp:include>
            <input type="hidden" id="ticketId" name="ticketId" value="${param.ticketId}">
        </div>
        <div class="span9" id="rightpanel">

            <div style="font-size: 14px; color: red" id="tkmErrorDiv">
                <c:if test="${errorMsg != null && errorMsg != ''}">
                    <p><c:out value="${errorMsg}"></c:out><p/>
                    <c:remove var="errorMsg"/>
                </c:if>
            </div>
            <!-- 	<div id="tskerr" style="font-size: 14px; color: red"> </div>	 -->
            <form method="POST">

                <table class="table table-striped">
                    <thead>
                    <tr class="header">
                        <th><spring:message code="tblheader_task.taskName"/></th>
                        <th><spring:message code="tblheader_task.startDate"/></th>
                        <th><spring:message code="tblheader_task.endDate"/></th>
                        <th><spring:message code="tblheader_task.assignee"/></th>
                        <th><spring:message code="tblheader_task.action"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${Task_Data}" var="task" varStatus="vs">
                        <tr>
                                <%-- <span onClick="getTicketTaskData(${ticket.id});"><b>v</b></span> --%>
                            <td> ${task.TaskName}</td>
                                <%-- <td> ${task.StartDate}</td>	 --%>
                            <td><fmt:formatDate value="${task.StartDate}" pattern="MM-dd-yyyy hh:mm aa"
                                                timeZone="<%=timeZone%>"/></td>
                                <%-- <td> ${task.EndDate}</td> --%>
                            <td><fmt:formatDate value="${task.EndDate}" pattern="MM-dd-yyyy hh:mm aa"
                                                timeZone="<%=timeZone%>"/></td>
                            <td> ${task.Assignee}</td>

                            <c:choose>

                                <c:when test="${task.Status=='Claimed'&&task.TaskUser=='true'}">
                                    <c:set var="encActvitiTaskId"><encryptor:enc value="${task.ActivitiTaskId}"
                                                                                 isurl="true"/> </c:set>
                                    <td><input type="button" name="btn1"
                                               value="<spring:message code="option_tktTask.markAsComplete"/> "
                                               class="btn btn-primary btn-small"
                                               onclick="javascript:completeTicketTask('${tkmTicketsObj.tkmWorkflows.type}', ${task.TaskId},'${encActvitiTaskId}', ${task.ProcessInstanceId});"/>
                                    </td>
                                </c:when>

                                <c:when test="${task.Status=='UnClaimed'&&(task.TaskUser=='true'||task.TaskQueue=='true')}">
                                    <td><input type="button" name="btn1"
                                               value="<spring:message code="option_tktTask.claim"/>"
                                               class="btn btn-primary btn-small"
                                               onclick="javascript:claimtask(${task.TaskId},${task.ActivitiTaskId});"/>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td>&nbsp;</td>
                                </c:otherwise>
                            </c:choose>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </form>

        </div>
    </div>
</div>


<div id="markCompleteDialog" style="overflow:auto" class="modal hide fade" tabindex="-1" role="dialog"
     aria-labelledby="markCompleteDialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="markCompleteHeader">
        <div class="header">
            <h4 class="pull-left"><spring:message code="header.markTaskAsComplete"/></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossCloseTaskComplete" class="close" title="x"
                    type="button" onclick="cancelModel();"><spring:message code="btn.crossClose"/></button>
        </div>
    </div>
    <div id="redirect"></div>
    <div class="modal-body">

        <div id="modal-body-row">
            <div class="col-md-6">
                <div class="taskframesControls2" id="taskframesControls2"></div>
                <div id="taskAttachmentViewerDiv">
                    <iframe id="taskAttachmentViewer"
                            style="width: 100%; height: auto;  border: 2px solid #e4e4e4;"></iframe>
                </div>
                <div class="controls">
                    <div class="control-group" id="markCompleteDialogcontrols">
                        <c:choose>
                            <c:when test="${tkmTicketsObj.tkmWorkflows.type == 'Qle Validation'}">
                                <div class="control-group" id="commentsBlock">
                                    <label class="control-label"><spring:message code="text_task.notes"/></label>
                                    <div class="controls">
                                        <textarea name="commentBox" id="commentBoxActionId" maxlength="1999"></textarea>
                                    </div>
                                    <input type="hidden" name="validateComments" id="validateComments" value="N">
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="control-group">
                                    <label class="control-label span4"><spring:message code="text_task.notes"/><img
                                            src="<c:url value="/resources/img/requiredAsterisk.png" />"
                                            alt="Required!"/></label>
                                    <div class="controls">
                                        <textarea name="commentBox" id="commentBoxActionId" maxlength="1999"></textarea>
                                    </div>
                                    <input type="hidden" name="validateComments" id="validateComments" value="Y">
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>

                </div>

                <div class="control-group" id="completeTktTaskloader"
                     style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top:-52px;margin-left: -47px;">
                    <img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80"
                         height="80"/>
                </div>

            </div>
            <div class="col-md-6">
                <div id="documentInfoUrlViewerDiv2">
                    <iframe id="documentInfoUrlViewer2"
                            style="width: 100%; height: 100%;  border: 2px solid #e4e4e4;"></iframe>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer clearfix">
        <button class="btn pull-left" id="taskCompleteCancelBtn" data-dismiss="modal" aria-hidden="true"
                onclick="cancelModel();"><spring:message code="button.cancel"/></button>
        <button class="btn btn-primary" id="completetaskBtn" onclick="javascript:func();"><spring:message
                code="btnTxt.taskComplete"/></button>
    </div>
</div>
<script type="text/javascript">

    function cancelModel() {
        $('#markCompleteDialog').modal('hide');
        $('#commentBoxActionId').val("");
        $('#documentInfoUrlViewerDiv2').hide();
        $('#documentInfoUrlViewer2').empty();
        $('#taskAttachmentViewerDiv').hide();
        $('#taskAttachmentViewer').empty();
        $('#taskframesControls2').empty();
        $('#flag').remove();
    }

    var actTask;
    var tkmTaskId;
    var actProInstId;
    var currentDocument;
    var documentArray;

    $(document).ready(function () {
        $("#tasklist").addClass("active navmenu");
        $('#taskAttachmentViewerDiv').css('display', 'none');
        $('#documentInfoUrlViewerDiv2').css('display', 'none');

    });

    function claimtask(taskId, actTaskId) {
        var myObject = new Object();
        myObject.ActivitiTaskId = actTaskId + "";
        myObject.TaskId = taskId + "";
        myObject.TicketId = ${ticketId}+"";

        var myString = JSON.stringify(myObject);
        var validateUrl = '<c:url value="/ticketmgmt/ticket/claimUserTask"></c:url>';
        var claimTaskUrl = validateUrl;
        $('body').prepend('<div class="Ajaxloader"></div>').css('overflow', 'hidden');
        $.ajax(
            {
                url: claimTaskUrl,
                type: "POST",
                data: {
        ${df:csrfTokenParameter()} :
        "<df:csrfToken plainToken="true" />",
            taskObject
    :
        myString
    },
        success: function (response, xhr) {
            if (isInvalidCSRFToken(xhr))
                return;
            if (response != "" && response != null) {
                $('#tkmErrorDiv').html(response);
                $('.Ajaxloader').remove();
                $('body').css('overflow', 'auto');
            } else {
                window.location.reload(true);
            }
        }
    ,
        error: function (e) {
            alert("Failed to Fetch Data");
            $('.Ajaxloader').remove();
            $('body').css('overflow', 'auto');
        }
    })
        ;
    }

    function completeTicketTask(ticketQueueName, taskId, encActvitiTaskId, taskProcessInstanceId) {
        if (ticketQueueName == "Qle Validation") {
            completeTask(taskId, encActvitiTaskId, taskProcessInstanceId);
        } else {
            completeVerifyTask(taskId, encActvitiTaskId, taskProcessInstanceId);
        }
    }

    function isInvalidCSRFToken(xhr) {
        var rv = false;
        if (xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {
            alert($('Session is invalid').text());
            rv = true;
        }
        return rv;
    }


    function updateDocumentLabel() {
        if (document.getElementById("documentLabelDiv") != null) {
            document.getElementById("documentLabelDiv").innerHTML = currentDocument + 1 + " out of " + documentArray.length;
        }
    }

    function nextDocumentView() {
        if (currentDocument >= documentArray.length - 2) {
            $('#markCompleteDialogcontrols').find('#nextButtonId').attr('disabled', 'disabled');
            currentDocument++;
        } else {
            currentDocument++;
        }
        var documentUrl = "${pageContext.request.contextPath}/ticketmgmt/ticket/viewDocument?documentId=" + encodeURI(documentArray[currentDocument]);
        document.getElementById('taskAttachmentViewer').setAttribute("src", documentUrl);
        if ($('#markCompleteDialogcontrols').find('#prevButtonId').attr('disabled') == "disabled" && currentDocument > 0) {
            $('#markCompleteDialogcontrols').find('#prevButtonId').removeAttr('disabled');
        }
        updateDocumentLabel();

    }

    function prevDocumentView() {
        currentDocument--
        if ($('#markCompleteDialogcontrols').find('#nextButtonId').attr('disabled') == "disabled") {
            $('#markCompleteDialogcontrols').find('#nextButtonId').removeAttr('disabled');
        }
        var documentUrl = "${pageContext.request.contextPath}/ticketmgmt/ticket/viewDocument?documentId=" + encodeURI(documentArray[currentDocument]);
        document.getElementById('taskAttachmentViewer').setAttribute("src", documentUrl);
        if (currentDocument == 0) {
            $('#markCompleteDialogcontrols').find('#prevButtonId').attr('disabled', 'disabled');
        }
        updateDocumentLabel();
    }

    function showdetail(docid) {
        //var  contextPath =  "http://localhost:8007/hix/";

        var contextPath = "<%=request.getContextPath()%>";
        var serverName = "<%=request.getServerName()%>";
        var serverPort = "<%=request.getServerPort()%>";


        var documentUrl = "http://" + serverName + ":" + serverPort + contextPath + "/ticketmgmt/ticket/viewDocument?documentId=" + encodeURI(docid);

        open(documentUrl, "_blank", "directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
    }

    function showAndHideRow(id) {

        var hideId = id;
        /* var showHideBool; */
        if (document.getElementById(hideId).style.display == 'none') {
            document.getElementById(hideId).style.display = '';
        } else {
            document.getElementById(hideId).style.display = 'none';
        }
    }

    function hideRow(id) {
        //alert($('#id').size()+"      "+id);
        if (id != 'hide0') {
            document.getElementById(id).style.display = 'none';
        }
    }

    function submitRedirectionForm(redirectUrl, caseNumber, csrftoken) {
        $('#redirect').append("<form id='redirectForm' action='/hix/ticketmgmt/ticket/redirection' method='POST'>");
        $('#redirect form').append("<input type='hidden'  name='redirectUrl' id='redirectUrl' />");
        $('#redirect form').append("<input type='hidden'  id='caseNumber' name='caseNumber' />");
        $('#redirect form').append("<input type='hidden'  id='csrftoken' name='csrftoken' />");
        $('#redirect form').append("<br><input type='submit' id='savebutton' value='Save' />");
        $('#redirectUrl').val(redirectUrl);
        $('#caseNumber').val(caseNumber);
        $('#csrftoken').val(csrftoken);
        $("#redirectForm").submit();
    }


</script>
<script type="text/javascript">
    function addAllColumnHeaders(myList, id) {
        var columnSet = [];
        var headerTr$ = $('<tr/>');

        for (var i = 0; i < myList.length; i++) {
            var rowHash = myList[i];
            for (var key in rowHash) {
                if (key == "QueueId" || key == "UserId" || key == "TaskId" || key == "ActivitiTaskId" || key == "ProcessInstanceId" || key == "Comments" || key == "FormProperties") {
                    continue;
                }
                if ($.inArray(key, columnSet) == -1) {
                    columnSet.push(key);
                    headerTr$.append($('<th/>').html(key));
                }
            }
        }
        $("#excelDataTable" + id).append(headerTr$);

        return columnSet;
    }
</script>
