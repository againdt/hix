Cypress.Commands.add("tobacco", () => {
    cy.log("start tobacco")
    cy.get('#' + cy.h.year + '_tab_content > .ds-u-border--2 > .ds-u-clearfix > .ds-u-lg-float--right > .ds-l-row > #further-action-btn > span').click()
    cy.verifyTobaccoPage()
    cy.get('[id="tobaccoUser-no"]').not('[disabled]').check().should('be.checked')
    cy.get('.form-horizontal > .gutter20 > strong > .pull-right > .btn').click()
    cy.log("end tobacco")
})

Cypress.Commands.add('medGroupsEnroll', (medOnly, options) =>{
    cy.log("start medGroupsEnroll")
    for (const eligGroup of cy.h.groups.eligible) {
        //TODO: figure out a better way to wait for page elements
        cy.wait(3000)
        cy.get('#aid_uneg_shop_for_members').click()
            .then(() => {
                cy.wait(3000)
                cy.get('#aid_custom_grouping_failure_dialog > .modal-dialog > .modal-content > .modal-footer > .btn').should('not.exist')
            })

        cy.url()
            .then (url => {
                let new_url = url.replace("/__/private/setHousehold", "/hix/private/setHousehold")
                cy.log("1. The url is " + new_url)
                cy.navigateTo(new_url)
            })
        cy.get('#skipButton').click()
        cy.wait(3000)

        if (options){
            cy.setMedicalEnrollmentOptions(options)
        }

        cy.get('[title="ADD"]').first().click()
        cy.wait(5000)

        cy.get('body')
            .then((body) => {
                if (body.find('[data-automation-id="continueDentalPlan"]').length > 0 && !medOnly) {
                    cy.get('[data-automation-id="continueDentalPlan"]').click()
                }
                else if (body.find('#aid_continueToCart_02').length > 0 && medOnly) {
                    cy.get('#aid_continueToCart_02').click()
                    cy.get('#checkout').click()
                    cy.get('#catch-all-light-box > .modal-footer > .btn-primary').click()
                    cy.get('.gutter10 > .row-fluid > .control-group > .controls > #applicant_esig').click()
                    cy.get('.gutter10 > .row-fluid > .control-group > .controls > #applicant_esig').type('a a')
                    cy.get('.row-fluid > #frmesign > #rightpanel > .form-actions > #submitButton').click()
                    cy.get('.goto_grouping_google_tracking').click()
                }
                else {
                    cy.get('#aid-continueCart_02').click()

                    cy.get('#checkout').click()
                    cy.get('#catch-all-light-box > .modal-footer > .btn-primary').click()
                    cy.get('.gutter10 > .row-fluid > .control-group > .controls > #applicant_esig').click()
                    cy.get('.gutter10 > .row-fluid > .control-group > .controls > #applicant_esig').type('a a')
                    cy.get('.row-fluid > #frmesign > #rightpanel > .form-actions > #submitButton').click()
                    cy.get('.goto_grouping_google_tracking').click()
                }
            })
    }
    cy.log("end medGroupsEnroll")
})

Cypress.Commands.add('dentalEnroll', () =>{
    cy.log("start dentalEnroll")
    cy.wait(3000)
    cy.get('body')
        .then((body) => {
            if (body.find('#aid_dental_shop_btn_dental_').length > 0){
                cy.get('#aid_dental_shop_btn_dental_').click()
            }
        })

    cy.wait(3000)
    cy.url().then (url => {
        let new_url = url.replace("/__/private/setHousehold", "/hix/private/setHousehold")
        cy.log("2. The url is " + new_url)
        cy.navigateTo(new_url)
    })
    cy.wait(5000)
    cy.get('#shopping-modal-update').click({ force: true })
    cy.url()
        .then (url => {
            cy.log("2. the url is " + url)
            let new_url = url.replace("/__/planselection", "/hix/private/planselection")
            cy.navigateTo(new_url)
        })

    //running with electron: #shopping-modal-update stays visible - using options {forse: true}
    // takes care of it
    cy.get('[title="ADD"]').first().click({ force: true })
    cy.get('[data-automation-id="continueCartBtn"]').click()
    cy.get('#checkout').click()
    cy.get('#catch-all-light-box > .modal-footer > .btn-primary').click()
    cy.get('.gutter10 > .row-fluid > .control-group > .controls > #applicant_esig').click()
    cy.get('.gutter10 > .row-fluid > .control-group > .controls > #applicant_esig').type('a a')
    cy.get('.row-fluid > #frmesign > #rightpanel > .form-actions > #submitButton').click()
    cy.get('#rightpanel > #frmorderconfirm > .form-actions > .controls > .goto_dashboard_google_tracking').click()
    cy.log("end dentalEnroll")
})

Cypress.Commands.add("enrollIntoMedicalOnly", (options) => {
    cy.tobacco()
    cy.medGroupsEnroll(true, options)
    cy.h.appStatus = 'med_full_enroll'
})

Cypress.Commands.add("enrollIntoMedicalAndDental", (options) => {
    //Tabacco page
    cy.tobacco()
    //Custom Groups
    cy.medGroupsEnroll(false, options)
    cy.dentalEnroll()
    cy.h.appStatus = 'med_full_enroll_dent'
    cy.contains('#' + cy.h.year + '_tab_content > .ds-u-border--2 > .ds-u-clearfix > .ds-u-lg-float--right > .ds-l-row > #further-action-btn > span', 'CHANGE PLANS')
})

Cypress.Commands.add("enrollIntoDentalOnly", () => {
    cy.tobacco()
    //click on Enroll into dental
    cy.get('#rightpanel > .ps-detail > .tabs > li > #tab-1').click()
    cy.dentalEnroll()
    cy.h.appStatus = 'enroll_dent'
    cy.contains('#' + cy.h.year + '_tab_content > .ds-u-border--2 > .ds-u-clearfix > .ds-u-lg-float--right > .ds-l-row > #further-action-btn > span', 'CONTINUE SHOPPING')
})