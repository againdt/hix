package com.getinsured.iex.household.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.RelationshipType;

/**
 * Repository for {@link RelationshipType}.
 * @author Dheeraj Maralkar
 */
@Repository
public interface RelationshipTypeRepository extends JpaRepository<RelationshipType, Long>
{
  /**
   * Finds {@link RelationshipType} for given relationship code.
   * @param label Relationship type code.
   * @return relationship type ({@link RelationshipType}) for given code.
   */
  RelationshipType findOneByLookupValueLookupValueCode(String code);
  
  /**
   * Finds {@link RelationshipType} for given relationship label.
   * @param label Relationship type label.
   * @return relationship type ({@link RelationshipType}) for given label.
   */
  RelationshipType findOneByLookupValueLookupValueLabelIgnoreCase(String label);
}
