package com.getinsured.eligibility.at.ref.dto;

/**
 * @author chopra_s
 * 
 */
public class NFProcessDTO {

	private long currentApplicationId;

	private long nfApplicationId;

	private boolean multipleCmr = false;

	private boolean nonFinancialCmr = false;

	private String accountTransferCategory;

	private int nfCmrId;

	private int matchingCmrId;
	
	private boolean isCSChangeDetected;
	
	private CompareMainDTO compareMainDTO;
	
	private boolean isValidationDone;
	
	private boolean isAutoLink = false;
	
	private String caseNumber;

	public NFProcessDTO() {
	}

	public int getMatchingCmrId() {
		return matchingCmrId;
	}

	public void setMatchingCmrId(int matchingCmrId) {
		this.matchingCmrId = matchingCmrId;
	}

	public int getNfCmrId() {
		return nfCmrId;
	}

	public void setNfCmrId(int nfCmrId) {
		this.nfCmrId = nfCmrId;
	}

	public String getAccountTransferCategory() {
		return accountTransferCategory;
	}

	public void setAccountTransferCategory(String accountTransferCategory) {
		this.accountTransferCategory = accountTransferCategory;
	}

	public long getCurrentApplicationId() {
		return currentApplicationId;
	}

	public void setCurrentApplicationId(long currentApplicationId) {
		this.currentApplicationId = currentApplicationId;
	}

	public long getNfApplicationId() {
		return nfApplicationId;
	}

	public void setNfApplicationId(long nfApplicationId) {
		this.nfApplicationId = nfApplicationId;
	}

	public boolean isMultipleCmr() {
		return multipleCmr;
	}

	public void setMultipleCmr(boolean multipleCmr) {
		this.multipleCmr = multipleCmr;
	}

	public boolean isNonFinancialCmr() {
		return nonFinancialCmr;
	}

	public void setNonFinancialCmr(boolean nonFinancialCmr) {
		this.nonFinancialCmr = nonFinancialCmr;
	}
	
	public boolean isValidationDone() {
		return isValidationDone;
	}

	public void setValidationDone(boolean isValidationDone) {
		this.isValidationDone = isValidationDone;
	}
	
	public boolean isAutoLink() {
		return isAutoLink;
	}

	public void setAutoLink(boolean isAutoLink) {
		this.isAutoLink = isAutoLink;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NFProcessDTO [currentApplicationId=").append(currentApplicationId)
			.append(", nfApplicationId=").append(nfApplicationId)
			.append(", multipleCmr=").append(multipleCmr)
			.append(", nonFinancialCmr=").append(nonFinancialCmr)
			.append(", nfCmrId=").append(nfCmrId)
			.append(", matchingCmrId=").append(matchingCmrId)
			.append(", nfApplicationId=").append(nfApplicationId)
			.append(", isValidationDone=").append(isValidationDone)
			.append(", isAutoLink=").append(isAutoLink)
		        .append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountTransferCategory == null) ? 0 : accountTransferCategory.hashCode());
		result = prime * result + (int) (currentApplicationId ^ (currentApplicationId >>> 32));
		result = prime * result + matchingCmrId;
		result = prime * result + (multipleCmr ? 1231 : 1237);
		result = prime * result + (int) (nfApplicationId ^ (nfApplicationId >>> 32));
		result = prime * result + nfCmrId;
		result = prime * result + (nonFinancialCmr ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NFProcessDTO other = (NFProcessDTO) obj;
		if (accountTransferCategory == null) {
			if (other.accountTransferCategory != null)
				return false;
		} else if (!accountTransferCategory.equals(other.accountTransferCategory))
			return false;
		if (currentApplicationId != other.currentApplicationId)
			return false;
		if (matchingCmrId != other.matchingCmrId)
			return false;
		if (multipleCmr != other.multipleCmr)
			return false;
		if (nfApplicationId != other.nfApplicationId)
			return false;
		if (nfCmrId != other.nfCmrId)
			return false;
		if (nonFinancialCmr != other.nonFinancialCmr)
			return false;
		return true;
	}

	public boolean isCSChangeDetected() {
		return isCSChangeDetected;
	}

	public void setCSChangeDetected(boolean isCSChangeDetected) {
		this.isCSChangeDetected = isCSChangeDetected;
	}

	public CompareMainDTO getCompareMainDTO() {
		return compareMainDTO;
	}

	public void setCompareMainDTO(CompareMainDTO compareMainDTO) {
		this.compareMainDTO = compareMainDTO;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	
}
