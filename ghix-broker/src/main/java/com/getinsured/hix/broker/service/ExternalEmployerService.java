package com.getinsured.hix.broker.service;

import com.getinsured.hix.model.ExternalEmployer;

/**
 * Encapsulates service layer method calls for External Employer 
 */
public interface ExternalEmployerService {
	
	/**
	 * Finds external employer based on employer case id(same as external employer id)
	 * 
	 * @param externalEmpId
	 * @return ExternalEmployer
	 * 
	 */
	ExternalEmployer findByExternalEmployerId(long externalEmpId);
}