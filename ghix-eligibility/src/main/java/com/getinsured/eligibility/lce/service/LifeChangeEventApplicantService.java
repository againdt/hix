package com.getinsured.eligibility.lce.service;

import java.util.List;
import java.util.Map;

import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public interface LifeChangeEventApplicantService {
	
	List<SsapApplicant> cloneSsapApplicants(SsapApplication currentApplication, List<RequestParamDTO> events, Long userId, SepResponseParamDTO sepResponseParamDTO) throws Exception;
	
	SsapApplicant cloneSsapApplicant(Long personId, JsonObject householdMember, Map<Long, List<RequestParamDTO>> changedApplicantsWithEvents, Map<Long, SsapApplicant> applicantsBeforeCloneMap, SsapApplication currentApplication, Long userId, SepResponseParamDTO sepResponseParamDTO) throws Exception;
	
	Map<Long, List<RequestParamDTO>> getChangedApplicantsWithEvents(List<RequestParamDTO> events, JsonArray householdMembers) throws Exception;
	
	SsapApplicant saveSsapApplicant(SsapApplicant ssapApplicant);
}
