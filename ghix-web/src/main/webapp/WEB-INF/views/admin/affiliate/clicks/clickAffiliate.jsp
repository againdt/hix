<%@page import="java.util.ArrayList"%>
<%@page import="antlr.collections.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.util.*"   %>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
.dob label {display: inline;}
</style>





<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>

<script type="text/javascript" src="<:url value="/resources/js/affiliate/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/affiliate/highlight.min.js" />"></script>




<div class="">


   <div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
	</div>

	<form class="form-horizontal" id="frmaffiliateclick" name="frmaffiliateclick" action="<c:url value="/affiliate/clicktracker" />" method="POST">
		<fieldset id="individual_member_0">
		<div class="left-col-w" style="width:470px; float:left;">
			
			<div class="control-group">
				<label for="affiliateId" class="control-label">AffilaiteId <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="affiliateId" maxlength="5" name="affiliateId" value="1" data-required="true" data-type="number" data-trigger="keyup" data-rangelength="[5,5]" data-rangelength-message="This value should be 5 digits" "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="affiliateFlowId" class="control-label">AffilaiteFlowId <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="affiliateFlowId" maxlength="5" name="affiliateFlowId" value="3" data-required="true" data-type="number" data-trigger="keyup" data-rangelength="[5,5]" data-rangelength-message="This value should be 5 digits" "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			 <div class="control-group">
				<label for="affiliateHouseholdId" class="control-label">AffilaiteHousehodId <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="affiliateHouseholdId" maxlength="5" name="affiliateHouseholdId" value="3" data-required="true" data-type="number" data-trigger="keyup" data-rangelength="[5,5]" data-rangelength-message="This value should be 5 digits" "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="LeadId" class="control-label">Lead Id <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="LeadId" maxlength="5" name="LeadId" value="1" data-required="true" data-type="number" data-trigger="keyup" data-rangelength="[5,5]" data-rangelength-message="This value should be 5 digits" "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="familySize" class="control-label">Family Size <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="familySize" maxlength="5" name="familySize" value="1" data-required="true" data-type="number" data-trigger="keyup" data-rangelength="[5,5]" data-rangelength-message="This value should be 5 digits" "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="memberData" class="control-label">Member data <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="memberData"  name="memberData" value="abc" data-required="true"  data-trigger="keyup"  "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			 <div class="control-group">
				<label for="annualHouseholdIncome" class="control-label">Affiliate Household Income <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="annualHouseholdIncome"  name="annualHouseholdIncome" value="3" data-required="true" data-type="number" data-trigger="keyup" data-rangelength="[1,10]" data-rangelength-message="This value should be 5 digits" "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
			<div class="control-group">
				<label for="email" class="control-label">Email <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="email" name="email" value="n@vimo.com" data-trigger="change" data-required="true" data-type="email"  class="xlarge" size="60">
			</div>
			
				<div class="control-group">
				<label for="Phone" class="control-label">Phone <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="phone" name="phone" maxlength="10" value="5252525252" data-required="Phone"  data-type="phone" class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
				<div class="control-group">
				<label for="Zip" class="control-label">County <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="county" maxlength="5" name="county" value="MARATHON" data-required="true"  data-trigger="keyup"  "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
				<div class="control-group">
				<label for="Zip" class="control-label">Zip <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				<input type="text" id="zip" maxlength="5" name="zip" value="52052" data-required="true" data-type="number" data-trigger="keyup" data-rangelength="[5,5]" data-rangelength-message="This value should be 5 digits" "class="parsley-validated parsley-error"  class="xlarge" size="60">
			</div>
			
		   <div class="control-group">
			<input type="submit" class="btn btn-primary btn-small" id="save-user" value="Save">
			<input type="button" class="btn btn-primary btn-small" id="cancel-affiliate" value="Cancel">
			</div>
			
			
			
		</div>
		
		</fieldset>
		
	
	</form>
</div>

<script type="text/javascript">
	  
	   $('#frmaffiliateclick').parsley( 'addListener', {
			onFormSubmit: function (isFormValid, event, ParsleyForm) {
				// if field is not visible, do not apply Parsley validation!
				if(isFormValid === "true"){
					var url='<c:url value="/affiliate/clicktracker" />';
					 $.post(url,function() {
						location.href='<c:url value="/affiliate/clicktracker" />'
					 });
				}
			} 
		}); 
		
	   
	   $('#cancel-affiliate').click(function (){
			  window.location='<c:url value="/admin/affiliate/manage"/>'
		  });

</script>

    