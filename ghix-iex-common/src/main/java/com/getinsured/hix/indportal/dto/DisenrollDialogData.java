package com.getinsured.hix.indportal.dto;

public class DisenrollDialogData {

	private String groupEnrollmentId;
	private String isPlanEffectuated = "N";
	private String endOfCurrentMonth;
	private String endOfNextMonth;
	private String endOfMonthAfterNext;
	private String planName;
	private String hiosIssuerId;
	private String issuerName;

	public String getGroupEnrollmentId() {
		return groupEnrollmentId;
	}
	public void setGroupEnrollmentId(String groupEnrollmentId) {
		this.groupEnrollmentId = groupEnrollmentId;
	}		
	public String getIsPlanEffectuated() {
		return isPlanEffectuated;
	}
	public void setIsPlanEffectuated(String isPlanEffectuated) {
		this.isPlanEffectuated = isPlanEffectuated;
	}
	public String getEndOfCurrentMonth() {
		return endOfCurrentMonth;
	}
	public void setEndOfCurrentMonth(String endOfCurrentMonth) {
		this.endOfCurrentMonth = endOfCurrentMonth;
	}
	public String getEndOfNextMonth() {
		return endOfNextMonth;
	}
	public void setEndOfNextMonth(String endOfNextMonth) {
		this.endOfNextMonth = endOfNextMonth;
	}
	public String getEndOfMonthAfterNext() {
		return endOfMonthAfterNext;
	}
	public void setEndOfMonthAfterNext(String endOfMonthAfterNext) {
		this.endOfMonthAfterNext = endOfMonthAfterNext;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;
	}
}
