package com.getinsured.eligibility.at.mes.common;

public class MESProcessingConstants {

	// AT status for processing
	public static final String AT_PROCESSING_TYPE_CLOSED = "close";
	public static final String AT_PROCESSING_TYPE_REALTIME = "realtime";
	public static final String AT_PROCESSING_TYPE_QUEUE = "queue";
	
	public static final String EE_PACKAGE_NAME = "com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.";
	
	public static final String ERROR_CALLING_DETERMINE_PROCESSING_TYPE = "Error calling determine processing type ";
	
	public static final String REASON = " - Reason - ";
	
	public static int SIXTY_DAYS= 60;
	public static int FIFTY_NINE_DAYS= 59;
	
}
