package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.OtherCase2DocumentID1Type;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;

/**
 * Wrapper Class for OtherCase2DocumentID1Type
 * 
 * @author Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class OtherCase2DocumentID1Wrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OtherCase2DocumentID1Wrapper.class);
	
	private ObjectFactory factory;
	private OtherCase2DocumentID1Type otherCase2DocumentID1Type;
	
	private String i94Number;
	private String sevisid;
	private PassportCountryWrapper passportCountry;
	private String passportNumber;
	private String countryOfIssuance;
	private String docDescReq;
	private String expiryDate;

	private static final String I94_NUMBER_KEY = "I94Number";
	private static final String SEVIS_ID_KEY = "SEVISID";
	private static final String PASSPORT_COUNTRY_KEY = "PassportCountry";
	private static final String DOC_DESC_REQ = "DocDescReq";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	

	public OtherCase2DocumentID1Wrapper(){
		this.factory = new ObjectFactory();
		this.otherCase2DocumentID1Type = factory.createOtherCase2DocumentID1Type();
		this.passportCountry = new PassportCountryWrapper();
	}
	
	public void setI94Number(String i94Number) throws HubServiceException {
		
		if(i94Number == null){
			throw new HubServiceException("No i94 number provided");
		}
		
		this.i94Number = i94Number;
		this.otherCase2DocumentID1Type.setI94Number(this.i94Number);
	}
	
	public String getI94Number() {
		return i94Number;
	}
	
	public void setSevisid(String sevisid) throws HubServiceException{

		if(sevisid == null){
			throw new HubServiceException("No SEVIS ID provided");
		}
		
		this.sevisid = sevisid;
		this.otherCase2DocumentID1Type.setSEVISID(this.sevisid);
		
	}
	
	public String getSevisid() {
		return sevisid;
	}
	
	public void setPassportCountry(String passportCountry) throws HubServiceException{
		
		if(passportCountry == null){
			throw new HubServiceException("No Passport Country provided");
		}
		
		this.passportCountry = PassportCountryWrapper.fromJsonString(passportCountry);
		this.otherCase2DocumentID1Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public PassportCountryWrapper getPassportCountry() {
		return passportCountry;
	}
	
	public void setPassportNumber(String passportNumber) throws HubServiceException{
		
		if(passportNumber == null){
			throw new HubServiceException("No Passport number provided");
		}
		
		this.passportNumber = passportNumber;
		this.passportCountry.setPassportNumber(passportNumber);
		this.otherCase2DocumentID1Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public String getPassportNumber() {
		return passportNumber;
	}
	
	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException{
		
		if(countryOfIssuance == null){
			throw new HubServiceException("No Country of Issuance provided");
		}
		
		this.countryOfIssuance = countryOfIssuance;
		this.passportCountry.setCountryOfIssuance(countryOfIssuance);
		this.otherCase2DocumentID1Type.setPassportCountry(this.passportCountry.getPassportCountryType());
	}
	
	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}	
	
	public void setDocDescReq(String docDescReq) throws HubServiceException{
		
		if(docDescReq == null){
			throw new HubServiceException("No Doc description provided");
		}
		
		this.docDescReq = docDescReq;
		this.otherCase2DocumentID1Type.setDocDescReq(docDescReq);
	}
	
	public String getDocDescReq() {
		return docDescReq;
	}

	public void setExpiryDate(String expiryDate) {
		
		try{
			this.otherCase2DocumentID1Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I20 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	
	public OtherCase2DocumentID1Type getOtherCase2DocumentID1Type(){
		return this.otherCase2DocumentID1Type;
	}
	
	/**
	 * Converts the JSON String to I20DocumentID26Wrapper object
	 * 
	 * @param jsonString - String representation for I94DocumentID2Wrapper
	 * @return I20DocumentID26Wrapper Object
	 */
	public static OtherCase2DocumentID1Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I20DocumentID26Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		OtherCase2DocumentID1Wrapper wrapper = new OtherCase2DocumentID1Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(I94_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.i94Number = (String)tmpObj;
			wrapper.otherCase2DocumentID1Type.setI94Number(wrapper.i94Number);
		}
		
		tmpObj = obj.get(SEVIS_ID_KEY);
		if(tmpObj != null){
			wrapper.sevisid = (String)tmpObj;
			wrapper.otherCase2DocumentID1Type.setSEVISID(wrapper.sevisid);
		}
		
		tmpObj = obj.get(PASSPORT_COUNTRY_KEY);
		if(tmpObj != null){
			JSONObject passportCountryObj = (JSONObject)tmpObj;
			wrapper.passportCountry = PassportCountryWrapper.fromJSONObject(passportCountryObj);
			wrapper.otherCase2DocumentID1Type.setPassportCountry(wrapper.passportCountry.getPassportCountryType());
		}
		
		tmpObj = obj.get(DOC_DESC_REQ);
		if(tmpObj != null){
			wrapper.docDescReq = (String)tmpObj;
			wrapper.otherCase2DocumentID1Type.setDocDescReq(wrapper.docDescReq);
		}
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj == null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.otherCase2DocumentID1Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating I20DocumentID26Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(DOC_DESC_REQ, this.docDescReq);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static OtherCase2DocumentID1Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		OtherCase2DocumentID1Wrapper wrapper = new OtherCase2DocumentID1Wrapper();
		wrapper.setI94Number((String) jsonObj.get(I94_NUMBER_KEY));
		wrapper.setDocDescReq((String) jsonObj.get(DOC_DESC_REQ));
		wrapper.setSevisid((String) jsonObj.get(SEVIS_ID_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		wrapper.setPassportCountry((String) jsonObj.get(PASSPORT_COUNTRY_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(DOC_DESC_REQ, this.docDescReq);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
	
}
