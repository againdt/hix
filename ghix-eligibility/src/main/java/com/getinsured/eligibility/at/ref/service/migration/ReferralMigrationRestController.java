package com.getinsured.eligibility.at.ref.service.migration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.iex.ssap.model.SsapIntegrationLog;
import com.getinsured.timeshift.util.TSDate;

@Controller("referralMigrationRestController")
@RequestMapping("/referralmigrationresponse")
public class ReferralMigrationRestController {

	private static final String SUCCCESS = "SUCCCESS";

	private static final String FAILURE = "FAILURE";

	private static final String ACCOUNT_TRANSFER_REFERRAL = "ACCOUNT_TRANSFER_REFERRAL";

	private static final Logger LOGGER = Logger
			.getLogger(ReferralMigrationRestController.class);

	@Autowired private IntegrationLogService integrationLogService;
	@Autowired
	private LinkEnrollmentProcessor linkEnrollmentProcessor;

	@RequestMapping(value = "/success", method = RequestMethod.POST)
	@ResponseBody
	public String processSuccess(@RequestBody String actionMappingXmlString) {
		LOGGER.info("ReferralProcessingRestController processSuccess - starts \n actionMappingXmlString - "
				+ actionMappingXmlString);

		ReferralResponse referralResponse = (ReferralResponse) EligibilityUtils
				.unmarshal(actionMappingXmlString);

		final String result = referralResponse.getMessage()
				+ "  - ssapApplicationId: "
				+ referralResponse.getData().get(
						ReferralResponse.SSAP_APPLICATION_ID);

		Long ssap_pk = (Long) referralResponse.getData().get("SSAP_APPLICATION_ID");
		Integer giWsPayloadId = (Integer) referralResponse.getData().get("GI_WS_PAYLOAD_ID");
		
		linkEnrollmentProcessor.linkEnrollment(ssap_pk);

		saveIntegrationLog(actionMappingXmlString, ACCOUNT_TRANSFER_REFERRAL, SUCCCESS, ssap_pk, giWsPayloadId != null ? giWsPayloadId.longValue() : null);
		
		return result;
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.POST)
	@ResponseBody
	public String processError(@RequestBody String errorXmlString) {

		LOGGER.info("ReferralProcessingRestController processError - starts \n errorXmlString - "
				+ errorXmlString);

		ReferralResponse referralResponse = (ReferralResponse) EligibilityUtils
				.unmarshal(errorXmlString);

		LOGGER.info("ReferralProcessingRestController - processError errorReason - "
				+ referralResponse.getMessage());

		Long ssap_pk = (Long) referralResponse.getData().get("SSAP_APPLICATION_ID");
		Integer giWsPayloadId = (Integer) referralResponse.getData().get("GI_WS_PAYLOAD_ID");

		saveIntegrationLog(errorXmlString, ACCOUNT_TRANSFER_REFERRAL, FAILURE, ssap_pk, giWsPayloadId != null ? giWsPayloadId.longValue() : null);

		return "ReferralProcessingRestController - processError errorReason - "
				+ referralResponse.getMessage();
	}
	
	public void saveIntegrationLog(String payload, String serviceName, String status, Long ssap_pk, Long giWsPayloadId) {
		SsapIntegrationLog iL = new SsapIntegrationLog();
		iL.setCreatedDate(new TSDate());
		iL.setPayload(payload);
		iL.setServiceName(serviceName);
		iL.setStatus(status);
		iL.setSsapApplicationId(ssap_pk);
		iL.setGiWsPayloadId(giWsPayloadId);
		integrationLogService.save(iL);
	}

}
