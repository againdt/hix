package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.eligibility.at.server.endpoint.schemavalidator.AccountTransferRequestValidator;
import com.getinsured.eligibility.at.server.endpoint.schemavalidator.SchematronValidator;
import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
//import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean;

/**
 * Transforms a SSAP JSON to Account Transfer JAXB
 * @author Ahtesham
 *
 */
@Component(value = "accountXferRequestTransformer")
public class AccountTransferRequestTransformer implements Serializable {
	private static final long serialVersionUID = 2965812227185532007L;
	private static Logger lOGGER = Logger.getLogger(AccountTransferRequestTransformer.class);

	@Autowired
	private AccountTransferSoapWebClient accountTransferSoapWebClient;

	@Autowired
	AccountTransferMapper accountTransferMapper;

	private static JAXBContext jc = null;
	private static Marshaller marshaller = null;
	static {
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.erp.gov.cms.dsh.at.extension._1");
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		} catch (JAXBException e) {
			lOGGER.error(" ",e);
		}

	}

	@ServiceActivator
	public String sendRequest(Message<?> message) throws GIException,JAXBException, TransformerException {
		lOGGER.debug("Invoking transformer" + message.getPayload());
		String ssapJSON = message.getHeaders().get("SSAP_JSON").toString();
		Long applicationId = Long.valueOf(message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER).toString());
		AccountTransferRequestPayloadType accountTransferJaxb = accountTransferMapper.populateAccountTransferRequest( message.getPayload().toString(), ssapJSON);
		//schema validation of the request
		List<String> errorList = AccountTransferRequestValidator.validate(accountTransferJaxb);

		AccountTransferResponsePayloadType atResponse = null;
		StringWriter st = null;
		StringWriter st1 = null;

		st1 = new StringWriter();
		marshaller.marshal(accountTransferJaxb, st1);
		lOGGER.debug(new StringBuilder().append("AT Request XML: ").append(st1).toString());
		//if(errorList.isEmpty()){
			// Schematron validation of the request
		errorList.addAll(SchematronValidator.runSchematronValidationOutBound(st1.toString()));
//		}
		if(errorList.isEmpty() ) {
			lOGGER.debug(" flow action- " + message.getHeaders().get("FLOW_ACTION"));
			if(!message.getHeaders().get("FLOW_ACTION").toString().equalsIgnoreCase("[translateOnly]")){
				try {
					// Call State Service
					atResponse = accountTransferSoapWebClient.send(accountTransferJaxb, applicationId, null);
				} catch (WebServiceIOException wsIOE) {
					lOGGER.error(wsIOE);
					lOGGER.error(wsIOE.getMessage());
					StringBuilder errorResponse = new StringBuilder();
					errorResponse.append("Error calling Account Transfer Endpoint:").append(wsIOE.getMessage());
					return errorResponse.toString();
				} catch (SoapFaultClientException sfce) {
					lOGGER.error(sfce);
					lOGGER.error("Fault Code: " + sfce.getFaultCode());
					lOGGER.error("Fault Reason: " + sfce.getFaultStringOrReason());
					lOGGER.error("Fault Role: " + sfce.getSoapFault().getFaultActorOrRole());
					StringBuilder errorResponse = new StringBuilder();
					errorResponse.append("Error calling Account Transfer Endpoint:").append(" Fault Code: ").append(sfce.getFaultCode().toString());
					errorResponse.append(" Fault Reason: " ).append(sfce.getFaultStringOrReason()).append(" Fault Role: ").append(sfce.getSoapFault().getFaultActorOrRole().toString());
					return errorResponse.toString();
				}
				if(atResponse != null) {
					  st = new StringWriter();
					  marshaller.marshal(atResponse,st);
				}

			  }	 else {
				  atResponse = formResponse("", errorList);
				  st = new StringWriter();
				  marshaller.marshal(atResponse,st);
				  st1 = new StringWriter();
				  marshaller.marshal(accountTransferJaxb, st1);
			  }
		  } else {

			  atResponse = formResponse("FAILURE", errorList);

			  st = new StringWriter();
			  marshaller.marshal(atResponse,st);
			  st1 = new StringWriter();
			  marshaller.marshal(accountTransferJaxb, st1);

		  }
		  String op1 = st != null ? st.toString() : "";
		  String op2 = st1 != null ? st1.toString() : "";
		  lOGGER.debug("output >>> " + op1 + op2);
		  return op1 + op2;

	}

	private AccountTransferResponsePayloadType formResponse(String responseValue, List<String> errorList) {

		AccountTransferResponsePayloadType response = new AccountTransferResponsePayloadType();
		response.setAtVersionText("2.4");

		ResponseMetadataType value = new ResponseMetadataType();
		TextType tt = new TextType();
		tt.setValue(responseValue);
		value.setResponseCode(tt);

		if (!errorList.isEmpty()) {
			TextType responseErrorDescrip = new TextType();
			responseErrorDescrip.setValue("one or more validation failed");
			value.setResponseDescriptionText(responseErrorDescrip);

			List<TextType> tdsResponseDescriptionTextList = new ArrayList<>();

			for (String error : errorList) {
				TextType errorDescrip = new TextType();
				errorDescrip.setValue(error);
				tdsResponseDescriptionTextList.add(errorDescrip);
			}

			value.getTDSResponseDescriptionText().addAll(
					tdsResponseDescriptionTextList);
		}

		response.setResponseMetadata(value);
		return response;
	}

	/*// @ServiceActivator
	public String receive(Message<String> message) throws IOException,
			PathNotFoundException, ParseException,
			DatatypeConfigurationException, JAXBException, GIException {
		lOGGER.debug("Invoking transformer");
		AccountTransferRequestPayloadType accountTransferJaxb = new AccountTransferMapper().populateAccountTransferRequest(message.getPayload(),message.getHeaders().get(SSAPIntegrationConstants.SSAP_JSON_HEADER).toString());
		StringWriter st = new StringWriter();
		marshaller.marshal(accountTransferJaxb, st);
		return st.toString();

	}
*/



}
