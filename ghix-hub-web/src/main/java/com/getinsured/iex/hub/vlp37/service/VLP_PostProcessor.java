package com.getinsured.iex.hub.vlp37.service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.FileBasedConfiguration;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.utils.GhixHubConstants;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.CloseCaseResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpsav.ResubmitResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpsav.ResubmitResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ReverificationResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ReverificationResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationIndividualResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationIndividualResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationRequestSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.ArrayOfErrorResponseMetadataType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.ErrorResponseMetadataType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.InitiateAdditionalVerifRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.InitiateAdditionalVerifResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.ResponseMetadataType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.InitiateThirdVerifRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.InitiateThirdVerifResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r.RetrieveStep2CaseResolutionRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r.RetrieveStep2CaseResolutionResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r.RetrieveStep2CaseResolutionResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.RetrieveStep3CaseResolutionRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.RetrieveStep3CaseResolutionResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.RetrieveStep3CaseResolutionResponseType;
import com.getinsured.iex.hub.vlp37.model.VLP37CaseLog;
import com.getinsured.iex.hub.vlp37.repository.IVLP37CaseLogRepository;

@Component
public class VLP_PostProcessor{
	@Autowired
	private IVLP37CaseLogRepository vlp37CaseRepo;
	
	@Autowired
	private VLP37Service vlp37Service;
	
	private ExecutorService executor = Executors.newFixedThreadPool(10);
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@PreDestroy
	public void cleanUp() {
		try {
			logger.info("Attemting to stop and shutdpwn the VLP Response post processor");
			this.executor.awaitTermination(20, TimeUnit.SECONDS);
			logger.info("Done!");
		} catch (InterruptedException e) {
			logger.info("Error encountered while attempting to shoutdown the VLP Response post processor Error message: {}", e.getMessage());
		}
	}
	
	public void submit(InitialVerificationRequestType step1Request,
			InitialVerificationResponseType step1Resp) {
		if (logger.isInfoEnabled()) {
			logger.info("Request submitted for Step 2/ 3 invocation. Action text {}");
		}
		// Step 1 Request
		List<InitialVerificationRequestSetType> step1RequestSetTypeList = step1Request
				.getInitialVerificationRequestSet();
		int reqLen = step1RequestSetTypeList.size();

		// Step 1 Response
		InitialVerificationResponseSetType step1ResponseSet = step1Resp.getInitialVerificationResponseSet();
		List<InitialVerificationIndividualResponseType> step1IndividualResponsesList = step1ResponseSet
				.getInitialVerificationIndividualResponse();

		// Now process all the individual responses
		InitialVerificationRequestSetType step1RequestSet = null;
		InitialVerificationIndividualResponseType step1IndividualResponse = null;
		InitialVerificationIndividualResponseSetType step1InitialVerificationResponse = null;
		for (int i = 0; i < reqLen; i++) {
			HashMap<String, Object> context = new HashMap<>();
			step1RequestSet = step1RequestSetTypeList.get(i);
			step1IndividualResponse = step1IndividualResponsesList.get(i);
			String vlpCode = step1IndividualResponse.getLawfulPresenceVerifiedCode();
			String respCode = step1IndividualResponse.getResponseMetadata().getResponseCode();
			String caseNumber = null;
			if (logger.isInfoEnabled()) {
				logger.info("Processing response for code: {}, VLP Code {}", respCode, vlpCode);
			}
			step1InitialVerificationResponse = step1IndividualResponse.getInitialVerificationIndividualResponseSet();
			if (step1InitialVerificationResponse != null) {
				caseNumber = step1InitialVerificationResponse.getCaseNumber();
				int eligCode = step1InitialVerificationResponse.getEligStatementCd().intValue();

				if (logger.isInfoEnabled()) {
					logger.info("Eligibility code received {} for case number {}", eligCode, caseNumber);
				}
				if (caseNumber == null) {
					logger.error("Individual response received without the case number");
					continue;
				}
				String pocName = step1RequestSet.getCasePOCFullName();
				String pocNumber = step1RequestSet.getCasePOCPhoneNumber();
				context.put("CasePOCFullName", pocName);
				context.put("CasePOCPhoneNumber", pocNumber);
				VLP37CaseLog log = this.vlp37CaseRepo.getCaseLog(caseNumber);
				if(log != null) {
					String status = log.getStatus();
					logger.info("Case number {} received already exists with status {} with elg code", caseNumber, status,log.getEscCode());
					if(status.equals(RequestStatus.ADDL_VERIFY_FAILED.toString()) || status.equals(RequestStatus.THIRD_VERIFY_FAILED.toString())) {
						logger.info("Initiating processing next step");
						executor.submit(new ProcessorTask(this.getOverrideEligibilityCode(eligCode), respCode, caseNumber, context));
					}else {
						logger.info("No further processing required for case {}",caseNumber);
					}
				}else {
					log = new VLP37CaseLog();
					log.setEscCode(eligCode);
					log.setCaseNumber(caseNumber);
					log.setStatus(RequestStatus.INIT_VERIFY_COMPLETE.toString());
					this.vlp37CaseRepo.saveAndFlush(log);
					executor.submit(new ProcessorTask(this.getOverrideEligibilityCode(eligCode), respCode, caseNumber, context));
				}
				this.handleVerifiedLawfulPresenceCode(vlpCode, caseNumber, context);
			}
			
		}
	}
	
	/**
	 * Override is available in debug mode only
	 * @param eligCode
	 * @return
	 */
	private int getOverrideEligibilityCode(int eligCode) {
		if(!logger.isDebugEnabled()) {
			return eligCode;
		}
		String overrideEligCode = FileBasedConfiguration.getConfiguration().getProperty("fdsh.mock.eligStatementCode");
		if(overrideEligCode != null) {
			try {
				int override = Integer.parseInt(overrideEligCode.trim());
				if(override > 0) {
					logger.error("Overriding existing eligibility statement code {} with value {}",eligCode,overrideEligCode);
					return override;
				}
			}catch(NumberFormatException ne) {
				logger.error("Can not override the existing eligibility statement code {} from value {}",eligCode,overrideEligCode);
			}
		}
		return eligCode;
	}
	
	public  void submit(ResubmitResponseType resubmitResp,Map<String,Object> context) {
		String reqCaseNumber = (String) context.get("CaseNumber");
		if(logger.isInfoEnabled()) {
			logger.info("Resubmit response submitted for Step 2/ 3 evaluation");
		}
		String caseNumber = null;
		ResubmitResponseSetType responseSet = resubmitResp.getResubmitResponseSet();
		String responseCode = resubmitResp.getResponseMetadata().getResponseCode();
		if(responseSet != null) {
			caseNumber = responseSet.getCaseNumber();
			if(!caseNumber.equalsIgnoreCase(reqCaseNumber)) {
				throw new RuntimeException("Help, the request case number "+reqCaseNumber+" does not match with the Response case number "+caseNumber+"");
			}
		}
		
		if(responseCode.equalsIgnoreCase("HS000000")) { 
			int eligCode = responseSet.getEligStatementCd().intValue();
			this.updateCaseLog(RequestStatus.RESUBMIT_COMPLETE, caseNumber, eligCode);
			executor.submit(new ProcessorTask(getOverrideEligibilityCode(eligCode), responseCode, responseSet.getCaseNumber(), context));
		}else {
			this.updateCaseLog(RequestStatus.HUB_ERROR_RESPONSE, caseNumber, -1);
			logger.info("Received response code for Resubmit {} Don't know how to process, Response says {}, TDS Response:",
					responseCode,
					resubmitResp.getResponseMetadata().getResponseDescriptionText(),
					resubmitResp.getResponseMetadata().getTDSResponseDescriptionText());
		}
	}
	
	
	
	
	public  void submit(ReverificationResponseType reverifyResp, Map<String,Object> context) {
		String reqCaseNumber = (String) context.get("CaseNumber");
		if(logger.isInfoEnabled()) {
			logger.info("Reverification Request submitted for Step 2/ 3 evaluation for case {}");
		}
		if(logger.isDebugEnabled()) {
			logger.debug("Request Context {}", context);
		}
		ReverificationResponseSetType responseSet = reverifyResp.getReverificationResponseSet();
		String responseCode = reverifyResp.getResponseMetadata().getResponseCode();
		if(responseCode.equals("HS000000")) {
			String caseNumber = responseSet.getCaseNumber();
			if(!caseNumber.equalsIgnoreCase(reqCaseNumber)) {
				throw new RuntimeException("Help, the request case number "+reqCaseNumber+" does not match with the Response case number "+caseNumber+"");
			}
			int eligCode = responseSet.getEligStatementCd().intValue();
			this.updateCaseLog(RequestStatus.REVERIFY_COMPLETE, caseNumber, eligCode);
			executor.submit(new ProcessorTask(getOverrideEligibilityCode(eligCode), responseCode, caseNumber, context));
		}else {
			this.updateCaseLog(RequestStatus.HUB_ERROR_RESPONSE, reqCaseNumber, -1);
			logger.info("Received response code {} for Reverification, Don't know how to process, Response says {}, TDS Response",responseCode,  
					reverifyResp.getResponseMetadata().getResponseDescriptionText(),
					reverifyResp.getResponseMetadata().getTDSResponseDescriptionText());
		}
		
	}
	
	private void updateCaseLog(RequestStatus status, String caseNumber, int eligCode) {
		VLP37CaseLog vlpLog = this.vlp37CaseRepo.getCaseLog(caseNumber);
		if(vlpLog == null) {
			vlpLog = new VLP37CaseLog();
			vlpLog.setCaseNumber(caseNumber);
			vlpLog.setEscCode(eligCode);
			vlpLog.setStatus(status.toString());
		}else {
			if(vlpLog.getStatus().equals(status.getStatusCode()) && vlpLog.getEscCode() == eligCode && eligCode != -1) {
				logger.info("Case Number {} is already in {} state, nothing to do",caseNumber,status.getStatusCode());
				return;
			}
			if(eligCode > 0) {
				vlpLog.setEscCode(eligCode);
			}
			vlpLog.setStatus(status.toString());
		}
		this.vlp37CaseRepo.saveAndFlush(vlpLog);
	}
	
	private void processEligCode(int eligCode, String respCode,String caseNumber, Map<String,Object> context) {
		logger.info("Processing eligibility statement code {}",eligCode);
		VLP37CaseLog vlpLog = this.vlp37CaseRepo.getCaseLog(caseNumber);
		boolean updateRequired = true;
		String currentStatus = null;
		if(vlpLog == null) {
			vlpLog = new VLP37CaseLog();
			vlpLog.setCaseNumber(caseNumber);
		}else {
			currentStatus = vlpLog.getStatus();
		}
		vlpLog.setEscCode(eligCode);
		switch(eligCode) {
			case 5:{
				// Step 2
				if(currentStatus != null && currentStatus.equalsIgnoreCase(RequestStatus.ADDL_VERIFY_ACKNOLEDGED.toString())) {
					logger.info("The case {} is already acknoledged for step 2, nothing to do here", caseNumber);
					updateRequired = false;
					break;
				}
				if(GhixHubConstants.VLP_2_3_Enabled) {
					logger.info("Initiating Step 2 Request");
					try {
						String step2Payload = this.getStep2Payload(context, caseNumber);
						RequestStatus status = this.vlp37Service.invokeInitiateAddVerifRequest(step2Payload);
						vlpLog.setStatus(status.toString());
					}catch(Exception be) {
						logger.error("Failed to invoke Step 2",be);
						vlpLog.setStatus(RequestStatus.ADDL_VERIFY_FAILED.toString());
					}
				}else {
					logger.info("Step 2 auto invocation is not enabled, no post processing required");
				}
				break;
			}
			case 44:{
				// Step 3
				if(currentStatus != null && currentStatus.equalsIgnoreCase(RequestStatus.THIRD_VERIFY_ACKNOLEDGED.toString())) {
					logger.info("The case {} is already acknoledged for step 3, nothing to do here");
					updateRequired = false;
					break;
				}
				vlpLog.setStatus(RequestStatus.THIRD_VERIFY_REQUIRED.toString());
				break;
			}
			case 32:{
				if(currentStatus != null && currentStatus.equalsIgnoreCase(RequestStatus.RESUBMIT_SAVIS.toString())) {
					logger.info("The case {} is already marked for Resubmit, nothing to do here", caseNumber);
					updateRequired = false;
					break;
				}
					
				vlpLog.setStatus(RequestStatus.RESUBMIT_SAVIS.toString());
				break;
			}
			case 37:
			case 40:{
				if(currentStatus != null && currentStatus.equalsIgnoreCase(RequestStatus.RE_VERIFY.toString())) {
					logger.info("The case {} is already Marked for ReVerify, nothing to do here");
					updateRequired = false;
					break;
				}
				vlpLog.setStatus(RequestStatus.RE_VERIFY.toString());
				break;
			}
			default:{
				vlpLog.setStatus(Integer.toString(eligCode)+"_PENDING CLOSE");
				if(logger.isInfoEnabled()) {
					logger.info("No handlers available for processing the code: {} received for Case Number {} ",respCode, caseNumber);
				}
			}
		}
		if(updateRequired) {
			vlp37CaseRepo.saveAndFlush(vlpLog);
		}
	}
	
	@SuppressWarnings("unchecked")
	private String getStep2Payload(Map<String,Object> context, String caseNumber) throws BridgeException {
		JSONObject payload = new JSONObject();
		JSONObject data = new JSONObject();
		String pocName = (String) context.get("CasePOCFullName");
		String pocPhone = (String) context.get("CasePOCPhoneNumber");
		if(logger.isDebugEnabled()) {
			logger.debug("Received POC name {} and number {}", pocName, pocPhone);
		}
		data.put("CaseNumber", caseNumber);
		data.put("RequestSponsorDataIndicator", false);
		data.put("RequestGrantDateIndicator", false);
		data.put("CasePOCFullName", context.get("CasePOCFullName"));
		data.put("CasePOCPhoneNumber", context.get("CasePOCPhoneNumber"));
		payload.put("payload", data);
		payload.put(MessageProcessor.APPLICATION_ID_KEY, context.get(MessageProcessor.APPLICATION_ID_KEY));
		return payload.toJSONString();
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	private String getStep3Payload(Map<String,Object> context, String caseNumber) throws BridgeException {
		JSONObject payload = new JSONObject();
		JSONObject data = new JSONObject();
		data.put("CaseNumber", caseNumber);
		data.put("RequestSponsorDataIndicator", false);
		data.put("RequestGrantDateIndicator", false);
		data.put("CasePOCFullName", context.get("CasePOCFullName"));
		data.put("CasePOCPhoneNumber", context.get("CasePOCPhoneNumber"));
		data.put("CasePOCAddress1","1305 Terra Bella Ave");
		data.put("CasePOCCity","Mountain View");
		data.put("CasePOCState","CA");
		data.put("CasePOCZipCode","94043");
		payload.put(MessageProcessor.APPLICATION_ID_KEY, context.get(MessageProcessor.APPLICATION_ID_KEY));
		payload.put("payload", data);
		return payload.toJSONString();
	}
	/**
	 * 
	 * @param Step2Request
	 * @param Step2Response
	 * @return
	 */
	public RequestStatus submit(InitiateAdditionalVerifRequestType step2Request, InitiateAdditionalVerifResponseType step2Response) {
		String caseNumber = step2Request.getCaseNumber();
		ResponseMetadataType responseMetadata = step2Response.getResponseMetadata();
		if(responseMetadata != null) {
			String responseCode = responseMetadata.getResponseCode();
			if(logger.isInfoEnabled()) {
				logger.info("Received response code for Step 2 "+responseCode);
			}
			VLP37CaseLog log = this.vlp37CaseRepo.getCaseLog(caseNumber);
			if(responseCode.equalsIgnoreCase("HS000000")) {
				log.setStatus(RequestStatus.ADDL_VERIFY_ACKNOLEDGED.toString());
				this.vlp37CaseRepo.saveAndFlush(log);
				return RequestStatus.ADDL_VERIFY_ACKNOLEDGED;
			}else {
				ArrayOfErrorResponseMetadataType errorResponse = step2Response.getArrayOfErrorResponseMetadata();
				if(errorResponse != null) {
					List<ErrorResponseMetadataType> allErrorResponses = errorResponse.getErrorResponseMetadata();
					for(ErrorResponseMetadataType errorMetadata: allErrorResponses) {
						logger.error(errorMetadata.getErrorResponseCode());
						logger.error(errorMetadata.getErrorResponseDescriptionText());
						logger.error(errorMetadata.getErrorTDSResponseDescriptionText());
					}
				}
			}
		}
		return RequestStatus.HUB_ERROR_RESPONSE;
	}
	
	/**
	 * 
	 * @param Step3Request
	 * @param Step3Response
	 * @return
	 */
	public RequestStatus submit(InitiateThirdVerifRequestType step3Request, InitiateThirdVerifResponseType step3Response) {
		String caseNumber = step3Request.getCaseNumber();
		com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.ResponseMetadataType responseMetadata = step3Response.getResponseMetadata();
		if(responseMetadata != null) {
			String responseCode = responseMetadata.getResponseCode();
			if(logger.isInfoEnabled()) {
				logger.info("Received response code for Step 3 "+responseCode);
			}
			VLP37CaseLog log = this.vlp37CaseRepo.getCaseLog(caseNumber);
			if(responseCode.equalsIgnoreCase("HS000000")) {
				log.setStatus(RequestStatus.THIRD_VERIFY_ACKNOLEDGED.toString());
				this.vlp37CaseRepo.saveAndFlush(log);
				return RequestStatus.THIRD_VERIFY_ACKNOLEDGED;
			}else {
				com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.ArrayOfErrorResponseMetadataType errorResponse = step3Response.getArrayOfErrorResponseMetadata();
				if(errorResponse != null) {
					List<com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.ErrorResponseMetadataType> allErrorResponses = errorResponse.getErrorResponseMetadata();
					for(com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv.ErrorResponseMetadataType errorMetadata: allErrorResponses) {
						logger.error(errorMetadata.getErrorResponseCode());
						logger.error(errorMetadata.getErrorResponseDescriptionText());
						logger.error(errorMetadata.getErrorTDSResponseDescriptionText());
					}
				}
			}
		}
		return RequestStatus.HUB_ERROR_RESPONSE;
	}
	
	private class ProcessorTask implements Runnable{
		
		private int eligCode;
		private String caseNumber;
		private Map<String, Object> context;
		private String respCode;
		
		ProcessorTask(int eligCode, String respCode, String caseNumber, Map<String,Object> context){
			this.eligCode = eligCode;
			this.respCode=respCode;
			this.caseNumber = caseNumber;
			this.context = context;
		}
		@Override
		public void run() {
			processEligCode(eligCode,respCode,caseNumber,context);
		}
		
	}
	
	private class CloseCaseTask implements Runnable{
		private String caseNumber;
		private Map<String, Object> context;
		
		CloseCaseTask(String caseNumber, Map<String,Object> context){
			this.caseNumber = caseNumber;
			this.context = context;
		}
		@Override
		public void run() {
			boolean success = false;
			for(int i = 0; i < 3; i++) {
				success = closeCase(caseNumber,context);
				if(success) {
					logger.info("Case {} closed sucessfully", caseNumber);
					break;
				}
				logger.info("Failed closing the case {} attempting #{}/3", caseNumber, i);
			}
			if(success) {
				logger.info("Successfully closed case {}", caseNumber);
			}
			
		}
	}

	@SuppressWarnings("unchecked")
	private boolean closeCase(String caseNumber, Map<String, Object> context) {
		JSONObject obj = new JSONObject();
		JSONObject payload = new JSONObject();
		payload.put("caseNumber", caseNumber);
		obj.put(MessageProcessor.APPLICATION_ID_KEY, context.get(MessageProcessor.APPLICATION_ID_KEY));
		obj.put("payload", payload);
		ResponseEntity<String> response = this.vlp37Service.invokeCloseCaseRequest(obj.toJSONString());
		if(!response.getStatusCode().is2xxSuccessful()) {
			return false;
		}
		
		VLP37CaseLog caseLog = this.vlp37CaseRepo.getCaseLog(caseNumber);
		caseLog.setStatus(RequestStatus.CASE_CLOSED.toString());
		this.vlp37CaseRepo.saveAndFlush(caseLog);
		return true;
	}

	public void submit(CloseCaseResponseType closeCaseresponse, Map<String, Object> context) {
		String caseNumber = (String) context.get("CaseNumber");
		com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.ResponseMetadataType closeCaseMetadata = closeCaseresponse.getResponseMetadata();
		String responseCode = closeCaseMetadata.getResponseCode();
		VLP37CaseLog caseLog = this.vlp37CaseRepo.getCaseLog(caseNumber);
		if(caseLog == null) {
			throw new HubServiceException("No existing log found for case "+caseNumber);
		}
		if(responseCode.equalsIgnoreCase("HS000000")) {
			caseLog.setStatus(RequestStatus.CASE_CLOSED.toString());
		}else {
			caseLog.setStatus(RequestStatus.CASE_CLOSED_FAILED.toString());
		}
		this.vlp37CaseRepo.saveAndFlush(caseLog);
	}

	/**
	 * Step 2 Response handler
	 * @param RetrieveStep2CaseResolutionRequestType reqObj
	 * @param RetrieveStep2CaseResolutionResponseType step2Response
	 * @param Map context
	 */
	public void submit(RetrieveStep2CaseResolutionRequestType reqObj, RetrieveStep2CaseResolutionResponseType step2Response,
			Map<String, Object> context) {
		String caseNumber = reqObj.getCaseNumber();
		com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr2r.ResponseMetadataType metadata = step2Response.getResponseMetadata();
		String responseCode = metadata.getResponseCode();
		if(responseCode.equalsIgnoreCase("HS000000")) {
			this.updateCaseLog(RequestStatus.ADDL_VERIFY_RESPONSE_RETRIEVED, caseNumber,  -1);
			RetrieveStep2CaseResolutionResponseSetType responseSet = step2Response.getRetrieveStep2CaseResolutionResponseSet();
			if(responseSet != null) {
				String vlpCode = step2Response.getRetrieveStep2CaseResolutionResponseSet().getLawfulPresenceVerifiedCode();
				this.handleVerifiedLawfulPresenceCode(vlpCode, caseNumber,  context);
			}
			
		}else {
			this.updateCaseLog(RequestStatus.ADDL_VERIFY_RESPONSE_RETRIEVED_FAILED, caseNumber, -1);
		}
		
	}
	
	/**
	 * Step 3 Response handler
	 * @param reqObj
	 * @param step3Response
	 * @param context
	 */
	public void submit(RetrieveStep3CaseResolutionRequestType reqObj, RetrieveStep3CaseResolutionResponseType step3Response,
			Map<String, Object> context) {
		String caseNumber = reqObj.getCaseNumber();
		com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpr3r.ResponseMetadataType metadata = step3Response.getResponseMetadata();
		String responseCode = metadata.getResponseCode();
		if(responseCode.equalsIgnoreCase("HS000000")) {
			this.updateCaseLog(RequestStatus.ADDL_VERIFY_RESPONSE_RETRIEVED, caseNumber,  -1);
			RetrieveStep3CaseResolutionResponseSetType responseSet = step3Response.getRetrieveStep3CaseResolutionResponseSet();
			if(responseSet != null) {
				String vlpCode = step3Response.getRetrieveStep3CaseResolutionResponseSet().getLawfulPresenceVerifiedCode();
				this.handleVerifiedLawfulPresenceCode(vlpCode, caseNumber,  context);
			}
		}else {
			this.updateCaseLog(RequestStatus.ADDL_VERIFY_RESPONSE_RETRIEVED_FAILED, caseNumber, -1);
		}
	}
	
	private void handleVerifiedLawfulPresenceCode(String vlpCode, String caseNumber, Map<String, Object> context) {
		switch(vlpCode) {
		case "Y":
		case "N":
		case "X":
			if(caseNumber != null) {
				logger.info("Received VLP Code {} for case number {} attempting to close the case", vlpCode,caseNumber);
				this.executor.submit(new CloseCaseTask(caseNumber, context));
			}else {
				logger.info("No case number received for VLP code {}", vlpCode);
			}
			break;
		default:
			logger.info("Received VLP Code {} for case number {}", vlpCode,caseNumber);
		}
	}

}
