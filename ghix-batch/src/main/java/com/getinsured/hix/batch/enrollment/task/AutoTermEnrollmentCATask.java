package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentCaAutoTermService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.util.exception.GIException;

public class AutoTermEnrollmentCATask implements Tasklet {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AutoTermEnrollmentCATask.class);
	private EnrollmentCaAutoTermService enrollmentCaAutoTermService;
	private JobService jobService;
	private String hiosIssuerId;
	private Boolean autoTermRenewals;
	private Boolean generate834;
	
	String yearStr;
	String yearEndDate;
	
	public String getYearStr() {
		return yearStr;
	}
	public void setYearStr(String yearStr) {
		this.yearStr = yearStr;
	}
	
	public String getHiosIssuerId() {
		return hiosIssuerId;
	}
	
	public void setHiosIssuerId(String hiosIssuerId) {
		this.hiosIssuerId = hiosIssuerId;	
	}

	public Boolean getAutoTermRenewals() {
		return autoTermRenewals;
	}
	
	public void setAutoTermRenewals(Boolean autoTermRenewals) {
		this.autoTermRenewals = autoTermRenewals;
	}
	
	public Boolean getGenerate834() {
		return generate834;
	}
	
	public void setGenerate834(Boolean generate834) {
		this.generate834 = generate834;
	}
	
	public EnrollmentCaAutoTermService getEnrollmentCaAutoTermService() {
		return enrollmentCaAutoTermService;
	}
	
	public void setEnrollmentCaAutoTermService(
			EnrollmentCaAutoTermService enrollmentCaAutoTermService) {
		this.enrollmentCaAutoTermService = enrollmentCaAutoTermService;
	}

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1)throws GIException {
		LOGGER.info("AutoTermEnrollmentCATask.execute : START");
		try {
			long jobExecutionId=-1;
			jobExecutionId=arg1.getStepContext().getStepExecution().getJobExecutionId();
			if(yearStr == null || yearStr.isEmpty() || !EnrollmentUtils.isNumeric(yearStr)){
				LOGGER.error("201 : Please provide valid year");
				throw new GIException("Please provide valid year");
			}else if(yearStr.length()!=4){
				LOGGER.error("202 : Please provide valid year format YYYY");
				throw new GIException("Please provide valid year");
			}
			else{
				
				String batchJobStatus=null;
				if(jobService != null && jobExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
				}
				
				enrollmentCaAutoTermService.processAutoTermEnrollment(Integer.parseInt(yearStr), jobExecutionId, hiosIssuerId, autoTermRenewals, generate834);
			}
		} catch (Exception e) {
			LOGGER.error("AutoTermEnrollmentCATask.execute:" + e.getMessage(),e);
			throw new GIException("AutoTermEnrollmentCATask failed ",e);
		}
		LOGGER.info("AutoTermEnrollmentCATask.execute : END");
		return RepeatStatus.FINISHED;
	}
	public JobService getJobService() {
		return jobService;
	}
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

}
