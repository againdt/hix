//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.02.19 at 12:21:44 PM IST 
//


package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Type for Array of Benefits objects.
 * 
 * <p>Java class for ArrayOfBenefitsListArrayType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfBenefitsListArrayType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BenefitsListArray" type="{http://gcd.ee.sim.dsh.cms.hhs.gov}BenefitsListArrayType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfBenefitsListArrayType", propOrder = {
    "benefitsListArray"
})
public class ArrayOfBenefitsListArrayType {

    @XmlElement(name = "BenefitsListArray")
    protected List<BenefitsListArrayType> benefitsListArray;

    /**
     * Gets the value of the benefitsListArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the benefitsListArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBenefitsListArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BenefitsListArrayType }
     * 
     * 
     */
    public List<BenefitsListArrayType> getBenefitsListArray() {
        if (benefitsListArray == null) {
            benefitsListArray = new ArrayList<BenefitsListArrayType>();
        }
        return this.benefitsListArray;
    }

}
