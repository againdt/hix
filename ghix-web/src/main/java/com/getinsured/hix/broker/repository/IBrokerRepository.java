package com.getinsured.hix.broker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Broker;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IBrokerRepository extends RevisionRepository<Broker, Integer, Integer>, JpaRepository<Broker, Integer> {

	Broker findById(int brokerId);

	Broker findByUserId(int userId);

	Broker findByLicenseNumber(String licenseNumber);
	
	@Query("FROM Broker where licenseNumber = :licenseNo")
	List<Broker> findBrokersByLicenseNumber(@Param("licenseNo") String licenseNo);

	@Query("FROM Broker where licenseNumber = :licenseNo and certificationStatus != 'InComplete' and certificationStatus != 'Incomplete' ")
	List<Broker> findCompleteBrokersByLicenseNumber(@Param("licenseNo") String licenseNo);


	@Query(nativeQuery=true,
			value= "SELECT b.id, u.first_name, u.LAST_NAME, b.company_name, b.federal_ein," +
			"b.license_number, b.license_renewal_date, b.certification_status, b.contactnumber," +
			"l.ADDRESS1, l.ADDRESS2, l.city, l.state, l.zip,u.email, b.communication_pref," +
			"count(CASE WHEN d.status = 'Pending' THEN brokerId END) as pending_designations," +
			"count(CASE WHEN d.status = 'Active' THEN brokerId END) as active_designations," + 
			"count(CASE WHEN d.status = 'Inactive' THEN brokerId END) as inactive_designations from brokers b " +
			"left join users u on u.id = b.userid "+ 
			"left join locations l on l.id = b.location_id " +  
			"left join designate_broker d on d.brokerid = b.id " +
			"group by b.id, u.first_name, u.LAST_NAME, b.company_name, b.federal_ein, b.license_number,  b.license_renewal_date, b.certification_status," +
			"b.contactnumber, l.ADDRESS1, l.ADDRESS2, l.city, l.state, l.zip, u.email, b.communication_pref " +
			"order by u.LAST_NAME asc")
	List<Object[]> getBrokerExportList();
	
	@Query(nativeQuery=true,
			value= "select b.ID, (CASE WHEN b.userid is not null THEN u.FIRST_NAME ELSE b.FIRST_NAME END) as first_name, " +
					"(CASE WHEN b.userid is not null THEN u.LAST_NAME ELSE b.LAST_NAME END) as last_name, "+
					"b.STATUS, b.LICENSE_NUMBER, b.CERTIFICATION_STATUS," + 
					"count(CASE WHEN d.status = 'Pending' OR d.status = 'Active' THEN brokerId END) as consumers_count "+
					"from brokers b "+
					"left join users u on u.id = b.userid "+
					"left join DESIGNATE_BROKER d on d.BROKERID = b.ID "+ 
					"where b.agency_id =  :agencyId "+ 
					"group by b.ID, (CASE WHEN b.userid is not null THEN u.FIRST_NAME ELSE b.FIRST_NAME END), "+
					"(CASE WHEN b.userid is not null THEN u.LAST_NAME ELSE b.LAST_NAME END),u.FIRST_NAME, u.LAST_NAME, b.STATUS, b.LICENSE_NUMBER, b.CERTIFICATION_STATUS "+  
					"order by u.FIRST_NAME asc")
	List<Object[]> getBrokersForAgency(@Param("agencyId") long agencyId);
	
//	@Query(nativeQuery=true,
//			value= "select id, firstName, lastName, status, license_number, certification_status, consumers_count from "+
//					"(select b.ID, (CASE WHEN b.userid is not null THEN u.FIRST_NAME ELSE b.FIRST_NAME END) as firstName, " +
//					"(CASE WHEN b.userid is not null THEN u.LAST_NAME ELSE b.LAST_NAME END) as lastName, "+
//					"b.STATUS, b.LICENSE_NUMBER, b.CERTIFICATION_STATUS," + 
//					"count(CASE WHEN d.status = 'Pending' OR d.status = 'Active' THEN brokerId END) as consumers_count, "+
//					"row_number() over (order by CASE WHEN b.userid is not null THEN u.FIRST_NAME ELSE b.FIRST_NAME END) r "+
//					"from brokers b "+
//					"left join users u on u.id = b.userid "+
//					"left join DESIGNATE_BROKER d on d.BROKERID = b.ID "+ 
//					"where b.agency_id =  :agencyId "+ 
//					"group by b.ID, (CASE WHEN b.userid is not null THEN u.FIRST_NAME ELSE b.FIRST_NAME END), "+
//					"(CASE WHEN b.userid is not null THEN u.LAST_NAME ELSE b.LAST_NAME END),u.FIRST_NAME, u.LAST_NAME, b.STATUS, b.LICENSE_NUMBER, b.CERTIFICATION_STATUS "+  
//					"order by u.FIRST_NAME asc) " +
//					"where r > :startRecord and r <= :endRecord")
//	List<Object[]> getBrokersForAgency(@Param("agencyId") long agencyId, @Param("startRecord") Integer startRecord, @Param("endRecord") Integer endRecord);
	@Query("select count(*) from Broker b join b.user.userRole ur where b.agencyId = :agencyId and ur.role.name = :agencyManagerRole")
	int getAgencyManagerCount(@Param("agencyId") Long agencyId,@Param("agencyManagerRole") String agencyRole);
	
}