package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpcc_v33.CloseCaseResponseSetType;

public class CloseCaseResponseSetWrapper implements JSONAware {
	private static final Logger LOGGER = LoggerFactory.getLogger(CloseCaseResponseSetWrapper.class);
	private String clientSoftwareVersion;
	public CloseCaseResponseSetWrapper(CloseCaseResponseSetType responseSet) {
		if(responseSet != null){
		this.clientSoftwareVersion = responseSet.getWebServSftwrVer();
		}else{
			LOGGER.error("CloseCaseResponseSetType is received as null");
	}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("CLIENT_SOFTWARE_VER", this.clientSoftwareVersion);
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put("CLIENT_SOFTWARE_VER", this.clientSoftwareVersion);
		return obj;
	}

}
