package com.getinsured.ssap.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.hix.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.enums.SsapApplicationTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.jpa.UserServiceImpl;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.financial.Expense;
import com.getinsured.iex.ssap.financial.Income;
import com.getinsured.iex.ssap.financial.type.ExpenseType;
import com.getinsured.iex.ssap.financial.type.Frequency;
import com.getinsured.iex.ssap.financial.type.IncomeType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.eligibility.EligibilityRequest;
import com.getinsured.ssap.model.eligibility.RunMode;
import com.getinsured.ssap.repository.EligibilityProgramRepository;
import com.getinsured.ssap.util.FamilyBuilder;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.ssap.util.SSLUtil;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link HouseholdIncomeCalculator}.
 *
 * @author Yevgen Golubenko
 * @since 6/14/19
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({DynamicPropertiesUtil.class})
@PowerMockIgnore({"javax.net.ssl.*", "javax.security.*"})
public class HouseholdIncomeCalculatorTest {
  private static final Logger log = LoggerFactory.getLogger(HouseholdIncomeCalculatorTest.class);
  private static final String TEST_SERVER_URL = "https://nv2dev.eng.vimo.com/ghix-eligibility-engine/";
  @Mock
  private Properties configProp;

  @Mock
  private GhixRestTemplate ghixRestTemplate;

  @Mock
  private SsapApplicantRepository ssapApplicantRepository;

  @Mock
  private EligibilityProgramRepository eligibilityProgramRepository;

  @Mock
  private SsapApplicationRepository ssapApplicationRepository;

  @Mock
  private HubIntegration hubIntegration;

  @Mock
  private SsapNoticeService ssapNoticeService;

  @Mock
  private EventCreationService eventCreationService;

  @Mock
  private UserServiceImpl userService;

  @Mock
  private AptcHistoryRepository aptcHistoryRepository;

  private ExchangeEligibilityDeterminationService exchangeEligibilityDeterminationService;

  private ObjectMapper om = new ObjectMapper();

  private EligibilityEngineIntegrationServiceImpl eligibilityEngineIntegrationService;


  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    PowerMockito.mockStatic(DynamicPropertiesUtil.class);
    SSLUtil.turnOffSslChecking();


    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    when(DynamicPropertiesUtil.getPropertyValue(any(IEXConfiguration.IEXConfigurationEnum.class))).then(invocation -> {
      final IEXConfiguration.IEXConfigurationEnum constant = invocation.getArgument(0, IEXConfiguration.IEXConfigurationEnum.class);
      if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE) {
        calendar.roll(Calendar.MONTH, -1);
        log.info("[base] Returning OEP start date: " + calendar.getTime());
      } else if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) {
        calendar.roll(Calendar.MONTH, 2);
        log.info("[base] Returning OEP end date: " + calendar.getTime());
      }

      return new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT).format(calendar.getTime());
    });

    when(ghixRestTemplate.postForObject(anyString(), any(EligibilityRequest.class), anyObject())).thenAnswer(invocation -> {
      RestTemplate restTemplate = new RestTemplate();

      final String url = invocation.getArgument(0, String.class);
      final EligibilityRequest request = invocation.getArgument(1, EligibilityRequest.class);
      final Class response = invocation.getArgument(2, Class.class);

      return restTemplate.postForObject(url, request, response);
    });


    when(configProp.getProperty("endPointEligibilityDetermination")).thenReturn(TEST_SERVER_URL);

    when(ssapApplicantRepository.findByApplicantGuid(anyString())).thenAnswer(invocation -> {

      String requestedGuid = invocation.getArgument(0, String.class);

      SsapApplicant applicant = new SsapApplicant();
      applicant.setId(new Random().nextLong());
      applicant.setApplicantGuid(requestedGuid);

      return applicant;
    });

    when(eligibilityProgramRepository.save(any(EligibilityProgram.class))).thenAnswer(invocation -> {
      EligibilityProgram eligibilityProgram = invocation.getArgument(0, EligibilityProgram.class);
      log.info("Saving eligibility program record into database: {}", eligibilityProgram);
      eligibilityProgram.setId(new Random().nextLong());
      return eligibilityProgram;
    });

    when(ssapApplicationRepository.save(any(SsapApplication.class))).thenAnswer(invocation -> {
      SsapApplication application = invocation.getArgument(0, SsapApplication.class);
      log.info("Saving SsapApplication: {}", application);
      return application;
    });

    when(ssapApplicationRepository.findOne(anyLong())).thenAnswer(invocation -> {
      Long applicationId = invocation.getArgument(0, Long.class);
      SsapApplication application = new SsapApplication();
      log.info("Returning empty SsapApplication for id: {}", applicationId);
      return application;
    });

    when(ssapApplicationRepository.getOne(anyLong())).thenAnswer(invocation -> {
      Long applicationId = invocation.getArgument(0, Long.class);
      SsapApplication application = getSampleApplication(applicationId);
      return application;
    });

    when(ssapApplicantRepository.save(any(SsapApplicant.class))).thenAnswer(invocation -> {
      SsapApplicant applicant = invocation.getArgument(0, SsapApplicant.class);
      log.info("Saving SSAP applicant: {}", applicant);
      return applicant;
    });

    when(ssapApplicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {
      Long applicationId = invocation.getArgument(0, Long.class);
      List<SsapApplicant> applicantList = new ArrayList<>();
      log.info("{Mock ssapApplicantRepository.findBySsapApplicationId} Returning empty applicant list for ssap id: {}", applicationId);
      return applicantList;
    });

    exchangeEligibilityDeterminationService = new ExchangeEligibilityDeterminationService();
    om.enable(SerializationFeature.INDENT_OUTPUT);

    eligibilityEngineIntegrationService = new EligibilityEngineIntegrationServiceImpl(ghixRestTemplate,
      configProp, ssapApplicantRepository, eligibilityProgramRepository,
      ssapApplicationRepository, exchangeEligibilityDeterminationService,
      hubIntegration, eventCreationService, ssapNoticeService, userService, aptcHistoryRepository);
    eligibilityEngineIntegrationService.postConstruct();
  }

  private RestTemplate restTemplate() {
    RestTemplate restTemplate = new RestTemplate();
    return restTemplate;
  }

  private SsapApplication getSampleApplication(long applicationId) {
    SingleStreamlinedApplication application = sampleApplication(applicationId);

    SsapApplication ssapApplication = new SsapApplication();
    ssapApplication.setId(applicationId);

    JsonUtil.setApplicationData(ssapApplication, application);

    ssapApplication.setFinancialAssistanceFlag(application.getApplyingForFinancialAssistanceIndicator() == Boolean.TRUE ? "Y" : "N");

    return ssapApplication;
  }

  private SingleStreamlinedApplication sampleApplication(long applicationId) {
    SingleStreamlinedApplication app = new SingleStreamlinedApplication();
    app.setSsapApplicationId(String.valueOf(applicationId));
    app.setApplicationType(SsapApplicationTypeEnum.OE.name());
    List<TaxHousehold> taxHouseholds = new ArrayList<>(1);
    TaxHousehold taxHousehold = null;

    switch (Math.toIntExact(applicationId)) {
      case 1:
        taxHousehold = new TaxHousehold();
        taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_H_Household());
        taxHouseholds.add(taxHousehold);
        break;
      case 2:
        taxHousehold = new TaxHousehold();
        taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_E_Household());
        taxHouseholds.add(taxHousehold);
        break;
      case 3:
        taxHousehold = new TaxHousehold();
        taxHousehold.setHouseholdMember(FamilyBuilder.buildHusbandWifeAndTwoKidsMarriedFilingJointly());
        taxHouseholds.add(taxHousehold);
        break;
      default:
        throw new RuntimeException("application id: " + applicationId + " is not supported");
    }

    app.setTaxHousehold(taxHouseholds);
    app.setCoverageYear(2019);
    app.setPrimaryTaxFilerPersonId(1);
    app.setJointTaxFilerSpousePersonId(2);
    app.setRidpVerified(true);
    app.setApplyingForFinancialAssistanceIndicator(true);

    return app;
  }

  @Test
  public void calculateTotalEarnedIncome() {
    List<Income> incomes = new ArrayList<>();

    Income job = new Income();
    job.setType(IncomeType.JOB);
    job.setFrequency(Frequency.YEARLY);
    job.setAmount(100_000_00L);
    job.setSourceName("GetInsured, Inc.");

    Income selfEmployment = new Income();
    selfEmployment.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment.setFrequency(Frequency.DAILY);
    selfEmployment.setCyclesPerFrequency(1); // 1 day per week
    selfEmployment.setAmount(100_00L);
    // total: $105,200

    Income socialSecurityDisability = new Income();
    socialSecurityDisability.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    socialSecurityDisability.setFrequency(Frequency.MONTHLY);
    socialSecurityDisability.setAmount(500_00L);
    // total: $105,200, SSN Benefits are not counted for earned/unearned income ($6,000/year)

    Income scholarship = new Income();
    scholarship.setType(IncomeType.SCHOLARSHIP);
    scholarship.setFrequency(Frequency.YEARLY);
    scholarship.setAmount(10_000_00L);
    scholarship.setRelatedExpense(5_000_00L);
    // total: $105,200, Scholarship is not considered earned income. ($5,000/year)

    incomes.add(job);
    incomes.add(selfEmployment);
    incomes.add(socialSecurityDisability);
    incomes.add(scholarship);

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setAmount(1_000_00L);
    expense.setFrequency(Frequency.MONTHLY);
    expense.setType(ExpenseType.ALIMONY);
    // $93,200

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);

    List<HouseholdMember> members = new ArrayList<>();
    members.add(hhm);
    long totalIncome = HouseholdIncomeCalculator.calculateTotalEarnedIncome(members);
    Assert.assertTrue("Total earned income should be > 0", totalIncome > 0L);
    Assert.assertEquals("Total earned income not correct", 93_200L, totalIncome);
    Assert.assertEquals("Income list was modified by the method", 4, hhm.getIncomes().size());
    Assert.assertEquals("Expense list was modified by the method", 1, hhm.getExpenses().size());
  }

  @Test
  public void calculateTotalUnearnedIncome() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment = new Income();
    selfEmployment.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment.setFrequency(Frequency.DAILY);
    selfEmployment.setCyclesPerFrequency(4); // 4 days per week
    selfEmployment.setAmount(100_00L);
    // $4,800, ignored, self employment is earned

    Income socialSecurityDisability = new Income();
    socialSecurityDisability.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    socialSecurityDisability.setFrequency(Frequency.MONTHLY);
    socialSecurityDisability.setAmount(500_00L);
    // $6,000

    Income scholarship = new Income();
    scholarship.setType(IncomeType.SCHOLARSHIP);
    scholarship.setFrequency(Frequency.YEARLY);
    scholarship.setAmount(8_000_00L);
    scholarship.setRelatedExpense(5_000_00L);
    // $3,000

    Income alimony = new Income();
    alimony.setType(IncomeType.ALIMONY);
    alimony.setFrequency(Frequency.WEEKLY);
    alimony.setAmount(125_00L);
    // $6,500

    incomes.add(selfEmployment);
    incomes.add(socialSecurityDisability);
    incomes.add(scholarship);
    incomes.add(alimony);
    // $15,500

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(25_31L);
    expense.setFrequency(Frequency.MONTHLY);
    // $303.72

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);
    // $15,196.28 (inc - exp)
    List<HouseholdMember> members = new ArrayList<>();
    members.add(hhm);

    long totalIncome = HouseholdIncomeCalculator.calculateTotalUnearnedIncome(members);
    Assert.assertTrue("Total unearned income should be > 0", totalIncome > 0L);
    Assert.assertEquals("Total unearned income not correct", 15_196L, totalIncome);
    Assert.assertEquals("Income list was modified by the method", 4, hhm.getIncomes().size());
    Assert.assertEquals("Expense list was modified by the method", 1, hhm.getExpenses().size());
  }

  @Test
  public void calculateTotalUnearnedIncomeWithoutSsnBenefits() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment = new Income();
    selfEmployment.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment.setFrequency(Frequency.DAILY);
    selfEmployment.setCyclesPerFrequency(4); // 4 days per week
    selfEmployment.setAmount(100_00L);
    // $4,800, ignored, self employment is not counted as unearned income.

    Income socialSecurityDisability = new Income();
    socialSecurityDisability.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    socialSecurityDisability.setFrequency(Frequency.MONTHLY);
    socialSecurityDisability.setAmount(500_00L);
    // $6,000, ignored, SSN benefits will not be counted

    Income scholarship = new Income();
    scholarship.setType(IncomeType.SCHOLARSHIP);
    scholarship.setFrequency(Frequency.YEARLY);
    scholarship.setAmount(8_000_00L);
    scholarship.setRelatedExpense(5_000_00L);
    // $3,000

    Income alimony = new Income();
    alimony.setType(IncomeType.ALIMONY);
    alimony.setFrequency(Frequency.WEEKLY);
    alimony.setAmount(125_00L);
    // $6,500

    incomes.add(selfEmployment);
    incomes.add(socialSecurityDisability);
    incomes.add(scholarship);
    incomes.add(alimony);
    // $9,500

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(25_31L);
    expense.setFrequency(Frequency.MONTHLY);
    // $303.72

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);
    // $9,196.28 (inc - exp)
    List<HouseholdMember> members = new ArrayList<>();
    members.add(hhm);

    long totalIncome = HouseholdIncomeCalculator.calculateTotalUnearnedIncomeWithoutSsnBenefits(members);
    Assert.assertTrue("Total unearned income (no ssn benefits) should be > 0", totalIncome > 0L);
    Assert.assertEquals("Total unearned income (no ssn benefits) not correct", 9_196L, totalIncome);
    Assert.assertEquals("Income list was modified by the method", 4, hhm.getIncomes().size());
    Assert.assertEquals("Expense list was modified by the method", 1, hhm.getExpenses().size());
  }

  @Test
  public void calculateTotalIncomeWithoutTribalPortion() {
    List<Income> incomes = new ArrayList<>();

    Income anaiIncome = new Income();
    anaiIncome.setType(IncomeType.AI_AN);
    anaiIncome.setFrequency(Frequency.MONTHLY);
    anaiIncome.setAmount(1_000_00L);
    anaiIncome.setTribalAmount(10_00L);
    // $12,000, Tribal: $12,000

    Income selfEmployment = new Income();
    selfEmployment.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment.setFrequency(Frequency.DAILY);
    selfEmployment.setCyclesPerFrequency(5);
    selfEmployment.setTribalAmount(10_00L);
    selfEmployment.setAmount(100_00L);
    // $26,000, Tribal: $2,600

    Income socialSecurityDisability = new Income();
    socialSecurityDisability.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    socialSecurityDisability.setFrequency(Frequency.MONTHLY);
    socialSecurityDisability.setAmount(500_00L);
    // $6,000, Tribal: $0

    Income scholarship = new Income();
    scholarship.setType(IncomeType.SCHOLARSHIP);
    scholarship.setFrequency(Frequency.YEARLY);
    scholarship.setAmount(8_000_00L);
    scholarship.setRelatedExpense(5_000_00L);
    // $3,000, Tribal: $0

    Income alimony = new Income();
    alimony.setType(IncomeType.ALIMONY);
    alimony.setFrequency(Frequency.WEEKLY);
    alimony.setAmount(125_00L);
    // $6,500, Tribal: $0

    incomes.add(anaiIncome);
    incomes.add(selfEmployment);
    incomes.add(socialSecurityDisability);
    incomes.add(scholarship);
    incomes.add(alimony);
    // $53,500, Tribal: $14,600

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(25_31L);
    expense.setFrequency(Frequency.MONTHLY);
    // $303.72

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);
    // $53,196.28 (inc - exp), - tribal: $38,596.28
    List<HouseholdMember> members = new ArrayList<>();
    members.add(hhm);

    long totalIncome = HouseholdIncomeCalculator.calculateTotalIncomeWithoutTribalPortion(members);
    Assert.assertTrue("Total income w/o tribal should be > 0", totalIncome > 0L);
    Assert.assertEquals("Total income w/o tribal not correct", 38_596L, totalIncome);
    Assert.assertEquals("Income list was modified by the method", 5, hhm.getIncomes().size());
    Assert.assertEquals("Expense list was modified by the method", 1, hhm.getExpenses().size());
  }

  @Test
  public void calculateTotalHouseholdTribalIncome() {
    List<Income> incomes = new ArrayList<>();

    Income anaiIncome = new Income();
    anaiIncome.setType(IncomeType.AI_AN);
    anaiIncome.setFrequency(Frequency.MONTHLY);
    anaiIncome.setAmount(1_233_31L);
    // Tribal: $14,799.72

    Income selfEmployment = new Income();
    selfEmployment.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment.setFrequency(Frequency.DAILY);
    selfEmployment.setCyclesPerFrequency(5);
    selfEmployment.setTribalAmount(10_00L);
    selfEmployment.setAmount(100_00L);
    // Tribal: $2,600

    Income socialSecurityDisability = new Income();
    socialSecurityDisability.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    socialSecurityDisability.setFrequency(Frequency.MONTHLY);
    socialSecurityDisability.setAmount(500_00L);
    // $6,000, Tribal: $0

    Income scholarship = new Income();
    scholarship.setType(IncomeType.SCHOLARSHIP);
    scholarship.setFrequency(Frequency.YEARLY);
    scholarship.setAmount(8_000_00L);
    scholarship.setRelatedExpense(5_000_00L);
    // Tribal: $0

    Income alimony = new Income();
    alimony.setType(IncomeType.ALIMONY);
    alimony.setFrequency(Frequency.WEEKLY);
    alimony.setAmount(125_00L);
    // $6,500, Tribal: $0

    incomes.add(anaiIncome);
    incomes.add(selfEmployment);
    incomes.add(socialSecurityDisability);
    incomes.add(scholarship);
    incomes.add(alimony);
    // Tribal: $17,399.72

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(25_31L);
    expense.setFrequency(Frequency.MONTHLY);
    // $303.72

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);
    // Tribal income - expenses: $5,529.59

    List<HouseholdMember> members = new ArrayList<>();
    members.add(hhm);

    long totalIncome = HouseholdIncomeCalculator.calculateTotalHouseholdTribalIncome(members);
    Assert.assertTrue("Total tribal income be > 0", totalIncome > 0L);
    Assert.assertEquals("Total tribal income amount not correct", 17_400L, totalIncome); // 17,399.75 almost ,400 with round().
    Assert.assertEquals("Income list was modified by the method", 5, hhm.getIncomes().size());
    Assert.assertEquals("Expense list was modified by the method", 1, hhm.getExpenses().size());
  }

  @Test
  public void calculateTotalHouseholdIncome() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment1 = new Income();
    selfEmployment1.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment1.setFrequency(Frequency.MONTHLY);
    selfEmployment1.setTribalAmount(200_00L);
    selfEmployment1.setAmount(1_233_31L);
    // 14799.72

    Income selfEmployment2 = new Income();
    selfEmployment2.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment2.setFrequency(Frequency.DAILY);
    selfEmployment2.setCyclesPerFrequency(7);
    selfEmployment2.setTribalAmount(10_00L);
    selfEmployment2.setAmount(100_00L);
    // 36400

    incomes.add(selfEmployment1);
    incomes.add(selfEmployment2);
    // 51199.72

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(25_31L);
    expense.setFrequency(Frequency.MONTHLY);
    // $303.72

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);
    // Total income-expenses: 50896

    List<HouseholdMember> members = new ArrayList<>();
    members.add(hhm);

    long totalIncome = HouseholdIncomeCalculator.calculateTotalHouseholdIncome(members);
    Assert.assertTrue("Total income be > 0", totalIncome > 0L);
    Assert.assertEquals("Total income amount not correct", 50_896L, totalIncome);
    Assert.assertEquals("Income list was modified by the method", 2, hhm.getIncomes().size());
    Assert.assertEquals("Expense list was modified by the method", 1, hhm.getExpenses().size());
  }

  @Test
  public void incomeAboveTaxFilingThreshold_Under_65_Not_Blind_Above() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment1 = new Income();
    selfEmployment1.setType(IncomeType.JOB);
    selfEmployment1.setFrequency(Frequency.MONTHLY);
    selfEmployment1.setAmount(1_233_31L);
    selfEmployment1.setRelatedExpense(233_31L);
    // 14799.72

    Income selfEmployment2 = new Income();
    selfEmployment2.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment2.setFrequency(Frequency.DAILY);
    selfEmployment2.setCyclesPerFrequency(7); // 7 days per week
    selfEmployment2.setAmount(100_00L);
    // 36400

    Income ssnDisabilityIncome = new Income();
    ssnDisabilityIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    ssnDisabilityIncome.setFrequency(Frequency.MONTHLY);
    ssnDisabilityIncome.setAmount(1000_00L);

    Income capitalGainIncome = new Income();
    capitalGainIncome.setType(IncomeType.CAPITAL_GAIN);
    capitalGainIncome.setFrequency(Frequency.YEARLY);
    capitalGainIncome.setAmount(100_00L);

    incomes.add(selfEmployment1);
    incomes.add(selfEmployment2);
    incomes.add(capitalGainIncome);
    // 51299.72

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(25_31L);
    expense.setFrequency(Frequency.MONTHLY);
    // $303.72

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);

    boolean aboveThreshold = HouseholdIncomeCalculator.incomeAboveTaxFilingThreshold(hhm, 2019);
    Assert.assertTrue("Income shouldn't be above tax filing threshold", aboveThreshold);
  }


  @Test
  public void incomeAboveTaxFilingThreshold_Over_65_Not_Blind_Above() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment1 = new Income();
    selfEmployment1.setType(IncomeType.JOB);
    selfEmployment1.setFrequency(Frequency.MONTHLY);
    selfEmployment1.setAmount(1_233_31L);
    selfEmployment1.setRelatedExpense(233_31L);
    // 14799.72

    Income selfEmployment2 = new Income();
    selfEmployment2.setType(IncomeType.SELF_EMPLOYMENT);
    selfEmployment2.setFrequency(Frequency.DAILY);
    selfEmployment2.setCyclesPerFrequency(7); // 7 days per week
    selfEmployment2.setAmount(100_00L);
    // 36400

    Income ssnDisabilityIncome = new Income();
    ssnDisabilityIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    ssnDisabilityIncome.setFrequency(Frequency.MONTHLY);
    ssnDisabilityIncome.setAmount(1000_00L);

    Income capitalGainIncome = new Income();
    capitalGainIncome.setType(IncomeType.CAPITAL_GAIN);
    capitalGainIncome.setFrequency(Frequency.YEARLY);
    capitalGainIncome.setAmount(100_00L);

    incomes.add(selfEmployment1);
    incomes.add(selfEmployment2);
    incomes.add(capitalGainIncome);
    // 51299.72

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(25_31L);
    expense.setFrequency(Frequency.MONTHLY);
    // $303.72

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(10, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);

    boolean aboveThreshold = HouseholdIncomeCalculator.incomeAboveTaxFilingThreshold(hhm, 2019);
    Assert.assertTrue("Income shouldn't be above tax filing threshold", aboveThreshold);
  }

  @Test
  public void incomeAboveTaxFilingThreshold_Under_65_Blind_Above() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment1 = new Income();
    selfEmployment1.setType(IncomeType.JOB);
    selfEmployment1.setFrequency(Frequency.MONTHLY);
    selfEmployment1.setAmount(20_000_00L);

    Income ssnDisabilityIncome = new Income();
    ssnDisabilityIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    ssnDisabilityIncome.setFrequency(Frequency.MONTHLY);
    ssnDisabilityIncome.setAmount(1000_00L);

    incomes.add(selfEmployment1);
    incomes.add(ssnDisabilityIncome);

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(100_00L);
    expense.setFrequency(Frequency.MONTHLY);
    // $1,200

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(true);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);

    boolean aboveThreshold = HouseholdIncomeCalculator.incomeAboveTaxFilingThreshold(hhm, 2019);
    Assert.assertTrue("Income shouldn't be above tax filing threshold", aboveThreshold);
  }

  @Test
  public void incomeAboveTaxFilingThreshold_Over_65_Blind_Above() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment1 = new Income();
    selfEmployment1.setType(IncomeType.JOB);
    selfEmployment1.setFrequency(Frequency.MONTHLY);
    selfEmployment1.setAmount(20_000_00L);

    Income ssnDisabilityIncome = new Income();
    ssnDisabilityIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    ssnDisabilityIncome.setFrequency(Frequency.MONTHLY);
    ssnDisabilityIncome.setAmount(1000_00L);

    incomes.add(selfEmployment1);
    incomes.add(ssnDisabilityIncome);

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(100_00L);
    expense.setFrequency(Frequency.MONTHLY);
    // $1,200

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(25, 3, 25));
    hhm.setBlindOrDisabled(true);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);

    boolean aboveThreshold = HouseholdIncomeCalculator.incomeAboveTaxFilingThreshold(hhm, 2019);
    Assert.assertTrue("Income shouldn't be above tax filing threshold", aboveThreshold);
  }

  @Test
  public void incomeAboveTaxFilingThreshold_Under_65_Blind_Below() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment1 = new Income();
    selfEmployment1.setType(IncomeType.JOB);
    selfEmployment1.setFrequency(Frequency.YEARLY);
    selfEmployment1.setAmount(1_000_00L);

    Income ssnDisabilityIncome = new Income();
    ssnDisabilityIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    ssnDisabilityIncome.setFrequency(Frequency.MONTHLY);
    ssnDisabilityIncome.setAmount(10_00L);

    incomes.add(selfEmployment1);
    incomes.add(ssnDisabilityIncome);

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(100_00L);
    expense.setFrequency(Frequency.YEARLY);
    // $100

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(true);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);

    boolean aboveThreshold = HouseholdIncomeCalculator.incomeAboveTaxFilingThreshold(hhm, 2019);
    Assert.assertFalse("Income shouldn't be above tax filing threshold", aboveThreshold);
  }

  @Test
  public void incomeAboveTaxFilingThreshold_Under_65_Not_Blind_Below() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment1 = new Income();
    selfEmployment1.setType(IncomeType.JOB);
    selfEmployment1.setFrequency(Frequency.YEARLY);
    selfEmployment1.setAmount(900_00L);

    Income ssnDisabilityIncome = new Income();
    ssnDisabilityIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    ssnDisabilityIncome.setFrequency(Frequency.MONTHLY);
    ssnDisabilityIncome.setAmount(10_00L);

    incomes.add(selfEmployment1);
    incomes.add(ssnDisabilityIncome);

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(100_00L);
    expense.setFrequency(Frequency.YEARLY);
    // $100

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(80, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);

    boolean aboveThreshold = HouseholdIncomeCalculator.incomeAboveTaxFilingThreshold(hhm, 2019);
    Assert.assertFalse("Income shouldn't be above tax filing threshold", aboveThreshold);
  }

  @Test
  public void incomeAboveTaxFilingThreshold_Over_65_Not_Blind_Below() {
    List<Income> incomes = new ArrayList<>();

    Income selfEmployment1 = new Income();
    selfEmployment1.setType(IncomeType.JOB);
    selfEmployment1.setFrequency(Frequency.YEARLY);
    selfEmployment1.setAmount(1_000_00L);

    Income ssnDisabilityIncome = new Income();
    ssnDisabilityIncome.setType(IncomeType.SOCIAL_SECURITY_DISABILITY);
    ssnDisabilityIncome.setFrequency(Frequency.MONTHLY);
    ssnDisabilityIncome.setAmount(10_00L);

    incomes.add(selfEmployment1);
    incomes.add(ssnDisabilityIncome);

    List<Expense> expenses = new ArrayList<>();
    Expense expense = new Expense();
    expense.setType(ExpenseType.STUDENT_LOAN_INTEREST);
    expense.setAmount(100_00L);
    expense.setFrequency(Frequency.YEARLY);
    // $100

    expenses.add(expense);

    HouseholdMember hhm = new HouseholdMember();
    hhm.setDateOfBirth(new Date(10, 3, 25));
    hhm.setBlindOrDisabled(false);
    hhm.setIncomes(incomes);
    hhm.setExpenses(expenses);

    boolean aboveThreshold = HouseholdIncomeCalculator.incomeAboveTaxFilingThreshold(hhm, 2019);
    Assert.assertFalse("Income shouldn't be above tax filing threshold", aboveThreshold);
  }

  @Test
  public void calculateIncome_testCase_H() {
    SingleStreamlinedApplication application = new SingleStreamlinedApplication();
    List<TaxHousehold> taxHouseholds = new ArrayList<>();
    TaxHousehold taxHousehold = new TaxHousehold();
    taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_H_Household());

    taxHouseholds.add(taxHousehold);
    application.setTaxHousehold(taxHouseholds);
    application.setCoverageYear(2019);

    List<HouseholdMember> members = application.getTaxHousehold().get(0).getHouseholdMember();
    Assert.assertNotNull("members should not be null", members);

    int primaryTaxFilerPersonId = application.getTaxHousehold().get(0).getPrimaryTaxFilerPersonId();
    int householdSize = HouseholdSizeCalculator.calculateSize(members, primaryTaxFilerPersonId);
    long householdIncome = HouseholdIncomeCalculator.calculateTotalHouseholdIncome(members);

    System.out.println("Household size: " + householdSize + "; household income: " + householdIncome);

    for (int i = 0; i < members.size(); i++) {
      HouseholdMember member = members.get(i);
      long incomeNoTribal = HouseholdIncomeCalculator.calculateTotalIncomeWithoutTribalPortion(Collections.singletonList(member));
      long totalIncome = HouseholdIncomeCalculator.calculateTotalHouseholdIncome(Collections.singletonList(member));
      long grossIncome = HouseholdIncomeCalculator.calculateGrossIncome(Collections.singletonList(member));
      long tribalIncome = HouseholdIncomeCalculator.calculateTotalHouseholdTribalIncome(Collections.singletonList(member));
      long magiIncome = HouseholdIncomeCalculator.calculateMagi(members, member, Math.toIntExact(application.getCoverageYear()), 1) / 12;
      System.out.println("Magi income (monthly): " + magiIncome + "; Individual income: " + totalIncome + "; No tribal: " + incomeNoTribal + "; gross: " + grossIncome + "; tribal income: " + tribalIncome);
      switch (member.getPersonId()) {
        case 1:
        case 2:
        case 3:
        case 4:
          Assert.assertEquals("Household income for {} is incorrect", 3_762L, magiIncome);
          break;
        default:
          break;
      }
    }
  }

  @Test
  public void calculateIncome_testCase_H_fromUI() throws Exception {
    final ClassPathResource classPathResource = new ClassPathResource("/income/test-case-h.json");
    final ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    //  Unrecognized field "primaryContact" (class com.getinsured.iex.ssap.HouseholdMember)

    SingleStreamlinedApplication application = objectMapper.readValue(classPathResource.getInputStream(), SingleStreamlinedApplication.class);
    Assert.assertNotNull("Unable to construct SingleStreamlinedApplication from JSON file", application);
    final List<HouseholdMember> members = application.getTaxHousehold().get(0).getHouseholdMember();
    int primaryTaxFilerPersonId = application.getTaxHousehold().get(0).getPrimaryTaxFilerPersonId();
    int householdSize = HouseholdSizeCalculator.calculateSize(members, primaryTaxFilerPersonId);
    long householdIncome = HouseholdIncomeCalculator.calculateTotalHouseholdIncome(members);

    System.out.println("Household size: " + householdSize + "; household income: " + householdIncome);

    for (HouseholdMember member : members) {
      long incomeNoTribal = HouseholdIncomeCalculator.calculateTotalIncomeWithoutTribalPortion(Collections.singletonList(member));
      long totalIncome = HouseholdIncomeCalculator.calculateTotalHouseholdIncome(Collections.singletonList(member));
      long grossIncome = HouseholdIncomeCalculator.calculateGrossIncome(Collections.singletonList(member));
      long tribalIncome = HouseholdIncomeCalculator.calculateTotalHouseholdTribalIncome(Collections.singletonList(member));
      System.out.println("Individual income: " + totalIncome + "; No tribal: " + incomeNoTribal + "; gross: " + grossIncome + "; tribal income: " + tribalIncome);
    }
  }

  @Test
  public void calculateIncome_testCase_E() throws Exception {
    SingleStreamlinedApplication application = new SingleStreamlinedApplication();
    List<TaxHousehold> taxHouseholds = new ArrayList<>();
    TaxHousehold taxHousehold = new TaxHousehold();
    taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_E_Household());

    taxHouseholds.add(taxHousehold);
    application.setTaxHousehold(taxHouseholds);
    application.setCoverageYear(2019L);

    List<HouseholdMember> members = application.getTaxHousehold().get(0).getHouseholdMember();

    for (int i = 0; i < members.size(); i++) {
      HouseholdMember member = members.get(i);

      long magiIncome = HouseholdIncomeCalculator.calculateMagi(members, member, Math.toIntExact(application.getCoverageYear()), 1) / 12;

      switch (member.getPersonId()) {
        case 1:
        case 2:
          Assert.assertEquals("Grandparent/Son hh income is incorrect", 3_100L, magiIncome);
          break;
        case 3:
          Assert.assertEquals("Grandchild hh income is incorrect", 600, magiIncome);
          break;
        default:
          break;
      }
    }
  }

  @Test
  public void calculateIncomeUnclaimedChild() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("income/family-mfj-unclaimed-kid.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    application.getTaxHousehold().get(0).getHouseholdMember().stream().filter(hhm -> hhm.getPersonId() == 3).forEach(hhm -> {
      Assert.assertTrue(hhm.isSeeksQhp());
    });

    long income = HouseholdIncomeCalculator.calculateTotalHouseholdIncomeForEligibility(application);
    Assert.assertEquals("Household income should be $30,000", 30000, income);

    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    log.info("Eligibility request: {}", om.writeValueAsString(eligibilityRequest));
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Eligibility request hh income should be 30,000", 30_000L, eligibilityRequest.getHouseholdIncome());
  }

  /*
>    Gerald Rivers
    EDITYearly income in 2020
    $30,000.00

>    Marry Jane
    EDITYearly income in 2020
    $23,800.00

    Family Second Member
    EDITYearly income in 2020
    $97,600.00

    Wife Second Member           ++ ?
    EDITYearly income in 2020
    $7,200.00

>    Son One
    EDITYearly income in 2020
    $3,000.00

    Daughter Second
    EDITYearly income in 2020
    $600.00

> 30,000 + 23,800 + 3,000 = 56,800
  97,600 + 7,200 + 600 = 105,400
   */
  @Test
  public void incomeTwoHH_MFJ_OneChild_Each() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("income/two-hh-mfj-with-kids.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    application.getTaxHousehold().get(0).getHouseholdMember().stream().filter(hhm -> hhm.getPersonId() == 3).forEach(hhm -> {
      Assert.assertTrue(hhm.isSeeksQhp());
    });

    long income = HouseholdIncomeCalculator.calculateTotalHouseholdIncomeForEligibility(application);
    Assert.assertEquals("Household income should be $56,800", 56_800L, income);


    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    log.info("Eligibility request: {}", om.writeValueAsString(eligibilityRequest));
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Eligibility request hh income should be 56,800", 56_800L, eligibilityRequest.getHouseholdIncome());
  }

  @Test
  public void incomeTwoHH_Kid_UnderIncomeThreshold() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("income/dad-kid-under-threshold.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    long income = HouseholdIncomeCalculator.calculateTotalHouseholdIncomeForEligibility(application);
    Assert.assertEquals("Household income should be $22,000", 22_000L, income); // This is debatable, perhaps -10k because QA says child should not be included

    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    log.info("Eligibility request: {}", om.writeValueAsString(eligibilityRequest));
    Assert.assertNotNull(eligibilityRequest);
  }
}
