<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld"
	prefix="util"%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/shopstyles.css"/>" />

<div class="gutter10">
<div class="row-fluid">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<util:nav pageName="${page_name}"></util:nav>
			</div>
			<util:selection pageName="${page_name}"></util:selection>
		</div>
		<!--sidebar-->
			<div class="span9" id="rightpanel">
		<h4 class="graydrkbg gutter10">
			 <a href="<c:url value="/shop/employer/plandisplay/plantier" />" class="btn"> <spring:message code = "label.back"/></a>
            <span class="offset1"><spring:message code = "label.selectYourPlan"/></span> 
            <a href="<c:url value="/shop/employer/plandisplay/benchmarkplan" />" class="btn btn-primary getSelectedTier pull-right"> <spring:message  code="label.next"/> </a>
            </h4>
            
            		
		
		<div class="gutter10">
			<!-- <p>Choosing a reference plan within a level allows you to lock in your costs.  Your total costs are calculated as if everyone in your roster will choose the reference plan, but your employees still have the option to choose any plan in the level.  The employee's share of their premiums will vary depending on their ages and the specific plans they choose.</p>
			<p>Benefits of the Reference Plan:</p>
			<ul>
			<li>Financial predictability for employers</li>
			<li>Choice for employees</li>
			<li>Fairness for employees of all ages</li>
			</ul>-->
			<p><spring:message code = "label.nextStep"/></p>
			<ul>
				<li><b><spring:message code = "label.locksInCost"/></b> <spring:message code = "label.amtCalculated"/></li>
				<li><b><spring:message code = "label.flexibility"/></b> <spring:message code = "label.empOptions"/></li>
				<li><b><spring:message code = "label.fairness"/></b> <spring:message code = "label.yourContri"/></li>
			</ul>

			
		</div>
		<!--gutter10-->
	</div>
	<!--rightpanel-->
	</div>
<!--row-fluid-->
</div>
</div>