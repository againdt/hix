/*
 * Javascript for pre launch results page
 */

$(document).ready(function () {
	
	 $('input, textarea').placeholder();
	 
	 $('.numbersOnlyField').keypress(function (e) {
	        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	            return false;
	        }
	});
	
	//validations for pre launch page
	$("#updateLead").removeAttr("disabled");
	$("#updateLead").html("Submit");
    $("#updateMessage").html("");
    
 	//Validation method for letters only validation
	$.validator.addMethod("lettersOnly", function(value, element) {
		return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
	}, "Field must contain only letters");
    
	$.validator.addMethod("emailValidation",function(value, element){
		
		if(!value){
			return true;
		}
		
		var emailPattern = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!emailPattern.test(value)){
			return false;
		}
		return true;
	},"Invalid email address");
	
	$.validator.addMethod("phoneValidation",function(value, element){
		
		var phoneNumber = $("#leadPhone").val();
		
		if( phoneNumber && (isNaN(phoneNumber) || $.trim(phoneNumber).length != 10)){
			return false;
		}
		
		return true;
	},"Please enter a valid phone number");
	
	$.validator.addMethod("isOkToCallValidation",function(value, element){
		
		var phoneNumber = $("#leadPhone").val();
		
		if(phoneNumber && !isNaN(phoneNumber) && $.trim(phoneNumber).length == 10
				&& !$('input:checkbox[name="isOkToCall"]').is(':checked')){
			return false;
		}
		
		return true;
	},"Please select the checkbox to agree to receive communications from GetInsured.com");
	
    $("#preLaunchForm").validate({
        onsubmit: true,
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        
        rules: {
        	leadName: {
        		required: true,
            },
            leadEmail : {
            	required: true,
            	emailValidation : true
            },
            leadPhone : {
            	phoneValidation:true
            },
        },
        messages : {
        	leadName: {
        		required : "<span>Please enter your name</span>",
            },
            leadEmail : {
            	required : "<span>Please enter your email address</span>",
            	emailValidation : "Please enter a valid email address"
            },
            leadPhone : {
            	phoneValidation:"<span>Please enter a valid phone number</span>",
            }, 
        },
        errorPlacement: function (error, element) {
        	
        	//For phone number, show error in common div
        	if(element.attr("id") == 'leadPhone1' || element.attr("id") == 'leadPhone2' || element.attr("id") == 'leadPhone3'){
        		$("#leadPhone_error").html(error.html()).show();
        	} 
        	else{
        		$("#" + element.attr("id") + "_error").html(error.html()).show();
        	}
        },
        success : function(error, element) {
			/*hides the error message on a valid input*/
			var elementId = $(element).attr('id');
			
			//For phone number, hide error in common div
        	if(elementId == 'leadPhone1' || elementId == 'leadPhone2' || elementId == 'leadPhone3'){
        		elementId = "leadPhone";
        	} 
			
			else if (elementId == null || elementId == '') {
				elementId = $(element).attr('name');
			}
		
			
			$('#' + elementId + '_error').hide();
			
		}
    });
	
});

//AJAX call to update lead record
function updateLeadRecord(){
	
	if (!$("#preLaunchForm").valid()) {
		return;
	}
	
	var pathURL = "updateContactDetailsInEligLeadRecord";
	
	$("#updateLead").attr("disabled","disabled");
	$("#updateLead").html("Thank You !");
	
	var isOkToCall = 'N';
	if ($('input:checkbox[name="isOkToCall"]').is(':checked')) {
		isOkToCall = 'Y';
	}	
	
	$.ajax({
        type: "POST",
        url: pathURL,
        data: {
        	leadName : $("#leadName").val(),
        	leadEmail : $("#leadEmail").val(), 
        	leadPhone : $("#leadPhone").val(),
        	isOkToCall : isOkToCall
        },
        success: function (data) {
        	if(data == "success"){
        		$("#updateMessage").html("");
        		$("#preLaunchForm").submit();
        	}
        	else{
        		$("#updateMessage").html("Your details could not be updated this time. Please try again later");
        		$("#updateMessage").show();
        		$("#updateLead").removeAttr("disabled");
        		$("#updateLead").html("Submit");
        	}
        },
        error: function (jqXHR, textStatus, errorThrown) {
        	$("#updateLead").removeAttr("disabled");
    		$("#updateLead").html("Submit");
        }
    });
	
}