<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page isELIgnored="false"%>

	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/assister.css" />" media="screen" />
	
	<div class="container">
		<form class="form-horizontal form-only" id="frmmanagebroker" name="frmmanagebroker" action="brokerlist" method="POST">
			<!-- start of secondary navbar -->
			<ul class="nav nav-pills">
				<li class="active"><a href="../broker/managebroker">Manage Agents</a></li>
			</ul>
			<!-- end of secondary navbar -->

			<div class="row-fluid">
				<div class="page-header">
					<h1><a name="skip" class="skip">Manage Agents</a></h1>
				</div><!-- end of page-header -->

				<div class="profile">
					<div class="control-group">
						<label for="user.firstName" class="control-label"><spring:message code="label.brkFirstNames"/>:</label>
						<div class="controls">
							<input type="text" name="user.firstName" id="user.firstName" value="${user.firstName}" class="input-xlarge" size="30" />
						</div><!-- end of controls -->
					</div><!-- end of control-group -->

					<div class="control-group">
						<label for="user.lastName" class="control-label"><spring:message code="label.brkLastNames"/> :</label>
						<div class="controls">
							<input type="text" name="user.lastName" id="user.lastName" value="${user.lastName}" class="input-xlarge" size="30" />
						</div><!-- end of controls -->
					</div><!-- end of control-group -->

					<div class="control-group">
						<label for="status" class="control-label"><spring:message code="label.brkCertificationStatus"/>:</label>
						<div class="controls">
							<select id="status" name="certificationStatus" path="statuslist" class="input-xlarge">
								<option value="" selected="selected">Select</option>
								<c:forEach items="${statuslist}" var="value">
								<option value="${value}">${value}</option>
								</c:forEach>
							</select>
						</div><!-- end of controls -->
					</div>

					<div class="form-actions">
						 <input type="submit" name="submit" id="submit" value="<spring:message  code='label.search'/>" class="btn btn-primary" title="<spring:message  code='label.search'/>"/> 
					</div><!-- end of form-actions -->

				</div><!-- end of .profile -->

				<div id="brokerlist">
					<input type="hidden" name="id" id="id" value="" />
					<c:choose>
						<c:when test="${fn:length(brokerslist) > 0}">
							<table class="table">
								<thead>
									<tr class="header">
										<th scope="col"><spring:message code="label.brkBrokerName"/> </th>
										<th scope="col"><spring:message code="label.brkSubmittedOn"/></th>
										<th scope="col"><spring:message code="label.brkCertifiedOn"/></th>
										<th scope="col"><spring:message code="label.brkDeCertifiedOn"/></th>
										<th scope="col"><spring:message code="label.brkStatus"/> </th>
										<th scope="col"><spring:message code="label.brkEditStatus"/></th>
									</tr>
								</thead>
								<c:forEach items="${brokerslist}" var="broker">
									<tr>
										<td><b>${broker.user.firstName}&nbsp;${broker.user.lastName}</b></td>
										<td><c:out value="${broker.applicationDate}" /></td>
										<td><c:out value="${broker.certificationDate}" /></td>
										<td><c:out value="N/A" /></td>
										<td><c:out value="${broker.certificationStatus}" /></td>
										<td><button type="button" title="Update" class="btn btn-small" onClick="location.href='updatecertificationstatus'"><i class="icon-pencil"></i> Update</button></td>
										
									</tr>
								</c:forEach>
							</table>
						</c:when>
						<c:otherwise>
							<b>No matching records found.</b>
						</c:otherwise>
					</c:choose>
				</div>
			</div><!-- end of .row-fluid -->

			<script>
				$(function() {
					$('.datepick').each(function() {
						$(this).datepicker({
							showOn : "button",
							buttonImage : "../resources/images/calendar.gif",
							buttonImageOnly : true
						});
					});
				});
			</script>

		</form>
	</div><!-- end of .container -->
