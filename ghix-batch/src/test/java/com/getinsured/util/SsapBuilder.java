package com.getinsured.util;

import org.springframework.core.io.ClassPathResource;

import com.getinsured.batch.util.JsonUtil;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;

/**
 * Helper class to build {@link SingleStreamlinedApplication} from given JSON file
 * that is taken from database ssap_applications.application_data column
 *
 * @author Yevgen Golubenko
 * @since 8/9/19
 */
public class SsapBuilder {
  public static SingleStreamlinedApplication sampleApplicationFromJson(final String jsonFile) {
    final String json = getJsonFromFile(jsonFile);
    final SingleStreamlinedApplication application = JsonUtil.getSingleStreamlinedApplicationFromJson(json);
    return application;
  }

  public static String getJsonFromFile(final String jsonFile) {
    final ClassPathResource classPathResource = new ClassPathResource(String.format("/households/%s", jsonFile));
    final String json = JsonUtil.readFile(classPathResource);
    return json;
  }
}
