package com.getinsured.iex.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SsapApplicantResource extends ResourceSupport {

	public SsapApplicantResource() {

	}

	private String applyingForCoverage;

	private Date birthDate;

	

	private String citizenshipImmigrationStatus;

	private BigDecimal citizenshipImmigrationVid;

	
	private Timestamp creationTimestamp;
	 
	private String deathStatus;

	private BigDecimal deathVerificationVid;

	private String externalApplicantId;

	private String firstName;

	private String gender;

	private String householdContactFlag;

	private BigDecimal householdContactVid;

	private String incarcerationStatus;

	private BigDecimal incarcerationVid;

	private String incomeVerificationStatus;

	private BigDecimal incomeVerificationVid;

	private String lastName;
	
	private Timestamp lastUpdateTimestamp;
	 
	private BigDecimal mailiingLocationId;

	private String married;

	private String mecVerificationStatus;

	private BigDecimal mecVerificationVid;

	private String middleName;

	private BigDecimal otherLocationId;

	private String residencyStatus;

	private BigDecimal residencyVid;

	private String ssnVerificationStatus;

	private BigDecimal ssnVerificationVid;

	private String tobaccouser;
	
	@JsonInclude(Include.NON_NULL)
	private String ssapApplication;

	private long personId;

	private String vlpVerificationStatus;
	private BigDecimal vlpVerificationVid;
	
	private String nativeAmericanFlag;
	private String ssn;
	
	private boolean citizenshipAsAttestedIndicator;
	private boolean americanIndianAlaskaNativeIndicator;
	
	private String phoneNumber;
	
	private String mobilePhoneNumber;
	
	private String emailAddress;
	
	private String fullTimeStudent;
	
	private String relationship;
	
	private String preferredTimeToContact;
	
	private String applicantGuid;
	
	private String nativeAmericanVerificationStatus;
	
	private String ecnNumber;
	
	private String hardshipExempt;
	
	private String eligibilityStatus;
	
	private String csrLevel;
	
	private String onApplication;
	
	private String status;
	
	private Integer createdBy;
	
	private Integer lastUpdatedBy;
	private String nameSuffix;
	
	private String personType;
	
	private String nonEsiMecVerificationStatus;

	private BigDecimal nonEsiMecVerificationVid;

	public String getNonEsiMecVerificationStatus() {
		return nonEsiMecVerificationStatus;
	}

	public void setNonEsiMecVerificationStatus(String nonEsiMecVerificationStatus) {
		this.nonEsiMecVerificationStatus = nonEsiMecVerificationStatus;
	}

	public BigDecimal getNonEsiMecVerificationVid() {
		return nonEsiMecVerificationVid;
	}

	public void setNonEsiMecVerificationVid(BigDecimal nonEsiMecVerificationVid) {
		this.nonEsiMecVerificationVid = nonEsiMecVerificationVid;
	}
	public String getNameSuffix() {
		return nameSuffix;
	}

	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getNativeAmericanVerificationStatus() {
		return nativeAmericanVerificationStatus;
	}

	public void setNativeAmericanVerificationStatus(String nativeAmericanVerificationStatus) {
		this.nativeAmericanVerificationStatus = nativeAmericanVerificationStatus;
	}
	
	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}

	public BigDecimal getVlpVerificationVid() {
		return vlpVerificationVid;
	}
	
	public void setVlpVerificationVid(BigDecimal vlpVerificationVid) {
		this.vlpVerificationVid = vlpVerificationVid;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFullTimeStudent() {
		return fullTimeStudent;
	}

	public void setFullTimeStudent(String fullTimeStudent) {
		this.fullTimeStudent = fullTimeStudent;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getPreferredTimeToContact() {
		return preferredTimeToContact;
	}

	public void setPreferredTimeToContact(String preferredTimeToContact) {
		this.preferredTimeToContact = preferredTimeToContact;
	}
	
	public boolean isCitizenshipAsAttestedIndicator() {
		return citizenshipAsAttestedIndicator;
	}
	public void setCitizenshipAsAttestedIndicator(
			boolean citizenshipAsAttestedIndicator) {
		this.citizenshipAsAttestedIndicator = citizenshipAsAttestedIndicator;
	}
	public boolean isAmericanIndianAlaskaNativeIndicator() {
		return americanIndianAlaskaNativeIndicator;
	}
	public void setAmericanIndianAlaskaNativeIndicator(
			boolean americanIndianAlaskaNativeIndicator) {
		this.americanIndianAlaskaNativeIndicator = americanIndianAlaskaNativeIndicator;
	}
	
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getVlpVerificationStatus() {
		return vlpVerificationStatus;
	}

	public void setVlpVerificationStatus(String vlpVerificationStatus) {
		this.vlpVerificationStatus = vlpVerificationStatus;
	}

	public String getApplyingForCoverage() {
		return applyingForCoverage;
	}

	public void setApplyingForCoverage(String applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}

	public String getCitizenshipImmigrationStatus() {
		return citizenshipImmigrationStatus;
	}

	public void setCitizenshipImmigrationStatus(
			String citizenshipImmigrationStatus) {
		this.citizenshipImmigrationStatus = citizenshipImmigrationStatus;
	}

	public BigDecimal getCitizenshipImmigrationVid() {
		return citizenshipImmigrationVid;
	}

	public void setCitizenshipImmigrationVid(
			BigDecimal citizenshipImmigrationVid) {
		this.citizenshipImmigrationVid = citizenshipImmigrationVid;
	}

	public String getDeathStatus() {
		return deathStatus;
	}

	public void setDeathStatus(String deathStatus) {
		this.deathStatus = deathStatus;
	}

	public BigDecimal getDeathVerificationVid() {
		return deathVerificationVid;
	}

	public void setDeathVerificationVid(BigDecimal deathVerificationVid) {
		this.deathVerificationVid = deathVerificationVid;
	}

	public String getExternalApplicantId() {
		return externalApplicantId;
	}

	public void setExternalApplicantId(String externalApplicantId) {
		this.externalApplicantId = externalApplicantId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHouseholdContactFlag() {
		return householdContactFlag;
	}

	public void setHouseholdContactFlag(String householdContactFlag) {
		this.householdContactFlag = householdContactFlag;
	}

	public BigDecimal getHouseholdContactVid() {
		return householdContactVid;
	}

	public void setHouseholdContactVid(BigDecimal householdContactVid) {
		this.householdContactVid = householdContactVid;
	}

	public String getIncarcerationStatus() {
		return incarcerationStatus;
	}

	public void setIncarcerationStatus(String incarcerationStatus) {
		this.incarcerationStatus = incarcerationStatus;
	}

	public BigDecimal getIncarcerationVid() {
		return incarcerationVid;
	}

	public void setIncarcerationVid(BigDecimal incarcerationVid) {
		this.incarcerationVid = incarcerationVid;
	}

	public String getIncomeVerificationStatus() {
		return incomeVerificationStatus;
	}

	public void setIncomeVerificationStatus(String incomeVerificationStatus) {
		this.incomeVerificationStatus = incomeVerificationStatus;
	}

	public BigDecimal getIncomeVerificationVid() {
		return incomeVerificationVid;
	}

	public void setIncomeVerificationVid(BigDecimal incomeVerificationVid) {
		this.incomeVerificationVid = incomeVerificationVid;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public BigDecimal getMailiingLocationId() {
		return mailiingLocationId;
	}

	public void setMailiingLocationId(BigDecimal mailiingLocationId) {
		this.mailiingLocationId = mailiingLocationId;
	}

	public String getMarried() {
		return married;
	}

	public void setMarried(String married) {
		this.married = married;
	}

	public String getMecVerificationStatus() {
		return mecVerificationStatus;
	}

	public void setMecVerificationStatus(String mecVerificationStatus) {
		this.mecVerificationStatus = mecVerificationStatus;
	}

	public BigDecimal getMecVerificationVid() {
		return mecVerificationVid;
	}

	public void setMecVerificationVid(BigDecimal mecVerificationVid) {
		this.mecVerificationVid = mecVerificationVid;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public BigDecimal getOtherLocationId() {
		return otherLocationId;
	}

	public void setOtherLocationId(BigDecimal otherLocationId) {
		this.otherLocationId = otherLocationId;
	}

	public String getResidencyStatus() {
		return residencyStatus;
	}

	public void setResidencyStatus(String residencyStatus) {
		this.residencyStatus = residencyStatus;
	}

	public BigDecimal getResidencyVid() {
		return residencyVid;
	}

	public void setResidencyVid(BigDecimal residencyVid) {
		this.residencyVid = residencyVid;
	}

	public String getSsnVerificationStatus() {
		return ssnVerificationStatus;
	}

	public void setSsnVerificationStatus(String ssnVerificationStatus) {
		this.ssnVerificationStatus = ssnVerificationStatus;
	}

	public BigDecimal getSsnVerificationVid() {
		return ssnVerificationVid;
	}

	public void setSsnVerificationVid(BigDecimal ssnVerificationVid) {
		this.ssnVerificationVid = ssnVerificationVid;
	}

	public String getTobaccouser() {
		return tobaccouser;
	}

	public void setTobaccouser(String tobaccouser) {
		this.tobaccouser = tobaccouser;
	}

	public String getSsapApplication() {
		return ssapApplication;
	}

	public void setSsapApplication(String ssapApplication) {
		this.ssapApplication = ssapApplication;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public String getNativeAmericanFlag() {
		return nativeAmericanFlag;
	}

	public void setNativeAmericanFlag(String nativeAmericanFlag) {
		this.nativeAmericanFlag = nativeAmericanFlag;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public Object _links;

	public Object get_links() {
		return _links;
	}

	public void set_links(Object _links) {
		this._links = _links;
	}

	public String getEcnNumber() {
		return ecnNumber;
	}

	public void setEcnNumber(String ecnNumber) {
		this.ecnNumber = ecnNumber;
	}

	public String getHardshipExempt() {
		return hardshipExempt;
	}

	public void setHardshipExempt(String hardshipExempt) {
		this.hardshipExempt = hardshipExempt;
	}

	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getOnApplication() {
		return onApplication;
	}

	public void setOnApplication(String onApplication) {
		this.onApplication = onApplication;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}
	
}
