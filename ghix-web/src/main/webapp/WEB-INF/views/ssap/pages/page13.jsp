<%@ include file="../include.jsp" %>
 <div id="appscr63" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h5 class="titleForPages">
				<strong class="nameOfHouseHold"></strong>&apos;<spring:message code="ssap.page13.parentCaretakerRelatives" text="s- Parent/Caretaker Relatives"/>
			</h5>
		</div>
		
		<form id="appscr63Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.pageHeader.familyAndHousehold" text="Family and Household" /></h4>
			</div>
			<div class="gutter10">
			<div class="control-group">
				<fieldset>
				<legend>
					<spring:message code="ssap.page13.does" text="Does"/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page13.liveWithAgeUnder19" text="live with at least one child under age 19 and is"/>
					<strong id="appscr63ChildHeShe"></strong>
					<spring:message code="ssap.page13.theMainCareTaker" text="the main person taking care of that child?"/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</legend>
				<div class="margin5-l">
					<label class="radio"for="appscr63AtLeastOneChildUnder19Yes">
						<input type="radio" value="yes" id="appscr63AtLeastOneChildUnder19Yes" name="appscr63AtLeastOneChildUnder19Indicator" onchange="parentCareTakerHideShow()" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#appscr63AtLeastOneChildUnder19YesErrorDiv"/>
						<spring:message code="ssap.page13.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr63AtLeastOneChildUnder19No">
						<input type="radio" value="no" id="appscr63AtLeastOneChildUnder19No" name="appscr63AtLeastOneChildUnder19Indicator" onchange="parentCareTakerHideShow()" />
						<spring:message code="ssap.page13.no" text="No"/>
					</label>
					<div id="appscr63AtLeastOneChildUnder19YesErrorDiv"></div>
				</div>
				</fieldset>
			</div><!-- control-group ends -->
			
			<div class="control-group parentCareTaker">
				<fieldset>
				<legend>
					<spring:message code="ssap.page13.whoDoes" text="Who does"/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page13.liveWithandTakeCare" text="live with and take care of?"/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</legend>
				<div class="margin5-l" id="liveWithChildUnderAgeView"></div>
				<%-- <div class="margin5-l">
					<!-- <label>
						<input type="checkbox" id="takeCareOf_applicantOrNonapplicant" name="takeCareOf" value="[Applicant or Non-applicant FNLNS]">
						<span class="nameOfHouseHold"></span>
					</label> -->
					<label>
						<input type="checkbox" id="takeCareOf_anotherChild" name="takeCareOf" value="Another child" onclick="method4AnotherChildCheckBox();">
						<spring:message code="ssap.page13.anotherChild" text="Another child"/>
					</label>
				</div> --%>
				</fieldset>
			</div><!-- control-group ends -->
			
			
			<div class="parentOrCaretakerBasicDetailsContainer hide">
				<h4>
					<spring:message code="ssap.page13.what" text="What"/>&apos;<spring:message code="ssap.page13.nameAndDOBOfOneChild" text="s the name and date of birth of one child that "/>
					<strong class="nameOfHouseHold"></strong>&nbsp;
					<spring:message code="ssap.page13.liveWithAndTakeCareOf" text="lives with and takes care of?"/>
				</h4>
				<div class="control-group margin20-t">
					<label for="parent-or-caretaker-firstName" class="required control-label">
						<spring:message code='ssap.firstName' text='First Name' />&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" placeholder="<spring:message code='ssap.firstName' text='First Name' />" value="" name="parentOrCaretakerFirstName" id="parent-or-caretaker-firstName" class="input-large" onkeyup="updateFullNameOfOtherChild()" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.firstNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.firstNameRequired'/>" />
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label for="parent-or-caretaker-middleName" class="control-label">
						<spring:message code='ssap.middleName' text='Middle Name' />
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" placeholder="<spring:message code='ssap.middleName' text='Middle Name' />" value="" name="parentOrCaretakerMiddleName" id="parent-or-caretaker-middleName" class="input-large"  onkeyup="updateFullNameOfOtherChild()" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="<spring:message code='ssap.error.middleNameValid'/>"/>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label for="parent-or-caretaker-lastName" class="required control-label">
						<spring:message code='ssap.lastName' text='Last Name' />&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label><!-- end of label -->
					<div class="controls">
						<input type="text" placeholder="<spring:message code='ssap.lastName' text='Last Name' />" value="" name="parentOrCaretakerLastName" id="parent-or-caretaker-lastName" class="input-large" onkeyup="updateFullNameOfOtherChild()" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$"  data-parsley-pattern-message="<spring:message code='ssap.error.lastNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.lastNameRequired'/>" />
						<span class="errorMessage"></span>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label for="parent-or-caretaker-suffix" class="control-label">
						<spring:message code="ssap.suffix" text="Suffix" />
					</label><!-- end of label -->
					<div class="controls">
						<select name="parentOrCaretakerSuffix" id="parent-or-caretaker-suffix" class="input-large">
							<option value=""><spring:message code="ssap.suffix" text="Suffix" /></option>
							<option value="Sr."><spring:message code="ssap.Sr" text="Sr." /></option>
							<option value="Jr."><spring:message code="ssap.Jr" text="Jr." /></option>
							<option value="III"><spring:message code="ssap.III" text="III" /></option>
							<option value="IV"><spring:message code="ssap.IV" text="IV" /></option>
						</select>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label for="date" class="required control-label" for="parent-or-caretaker-dob">
						<spring:message code="ssap.dateOfBirth" text="Date of Birth" />&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label><!-- end of label -->
					<div class="controls">
						<div class="input-append date date-picker" data-date="">
							<input type="text" id="parent-or-caretaker-dob" name="parentOrCaretakerDateOfBirth" class="input-medium dob" parsley-americandate="true" placeholder="<spring:message code='ssap.DOB' text='MM/DD/YYYY' />" data-parsley-errors-container="#parent-or-caretaker-dob-error-div" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.dobRequired'/>" />
							<span  class="add-on dateSpan"><i class="icon-calendar"></i></span>
							<div id="parent-or-caretaker-dob-error-div"></div>
						</div>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
			
			<div class="control-group">
				<p class="pull-left gutter5-t">
					<span class="nameOfHouseHold"></span><spring:message code="ssap.page13.isThe" text=" is the"/>
				</p>
				<label for="applicant-relationship" class="aria-hidden">Relationship</label>
				<div class="margin5-lr pull-left">
					<select name="applicantRelationship" id="applicant-relationship" data-parsley-errors-container="#applicant-relationship-error-div" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.relationshipRequired'/>">
						<option value=""><spring:message code="ssap.page13.selectRelationship" text="Select Relationship"/></option>
						<option value="01"><spring:message code="ssap.page13.spouse" text="Spouse"/></option>
						<option value="03"><spring:message code="ssap.page13.parent" text="Parent"/></option>
						<option value="04"><spring:message code="ssap.page13.grandParent" text="Grandparent"/></option>
						<option value="05"><spring:message code="ssap.page13.grandChild" text="Grandchild"/></option>
						<option value="06"><spring:message code="ssap.page13.uncleAunt" text="Uncle/aunt"/></option>
						<option value="07"><spring:message code="ssap.page13.nephewNiece" text="Nephew/niece"/></option>
						<option value="08"><spring:message code="ssap.page13.firstCousin" text="First cousin"/></option>
						<option value="09"><spring:message code="ssap.page13.adoptedChild" text="Adopted child (son or daughter)"/></option>
						<option value="10"><spring:message code="ssap.page13.fosterChild" text="Foster child (foster son or foster daughter)"/></option>
						<option value="11"><spring:message code="ssap.page13.sonDaughterInLaw" text="Son-in-law or daughter-in-law"/></option>
						<option value="12"><spring:message code="ssap.page13.brotherSisterInLaw" text="Brother-in-law or sister-in-law"/></option>
						<option value="13"><spring:message code="ssap.page13.motherFatherInLaw" text="Mother-in-law or father-in law"/></option>
						<option value="14"><spring:message code="ssap.page13.sibling" text="Sibling (brother or sister)"/></option>
						<option value="15"><spring:message code="ssap.page13.Ward" text="Ward"/></option>
						<option value="16"><spring:message code="ssap.page13.stepParent" text="Stepparent (stepfather or stepmother)"/></option>
						<option value="17"><spring:message code="ssap.page13.stepChild" text="Stepchild (stepson or stepdaughter)"/></option>
						<option value="18"><spring:message code="ssap.page13.self" text="Self"/></option>
						<option value="19"><spring:message code="ssap.page13.childSonDaughter" text="Child (son or daughter)"/></option>
						<option value="23"><spring:message code="ssap.page13.sponsoredDependent" text="Sponsored dependent"/></option>
						<option value="24"><spring:message code="ssap.page13.minorDependent" text="Dependent of a minor dependent"/></option>
						<option value="25"><spring:message code="ssap.page13.formerSpouse" text="Former spouse"/></option>
						<option value="26"><spring:message code="ssap.page13.guardian" text="Guardian"/></option>
						<option value="31"><spring:message code="ssap.page13.courtAppointedGuardian" text="Court-appointed guardian"/></option>
						<option value="38"><spring:message code="ssap.page13.collateralDependent" text="Collateral dependent"/></option>
						<option value="53"><spring:message code="ssap.page13.domesticPartner" text="Domestic partner"/></option>
						<option value="60"><spring:message code="ssap.page13.annuitant" text="Annuitant"/></option>
						<option value="D2"><spring:message code="ssap.page13.trustee" text="Trustee"/></option>
						<option value="G8"><spring:message code="ssap.page13.unspecifiedRelationship" text="Unspecified relationship"/></option>
						<option value="G9"><spring:message code="ssap.page13.unspecifiedRelative" text="Unspecified relative"/></option>
						<option value="03-53"><spring:message code="ssap.page13.parentDomesticPartner" text="Parent's domestic partner"/></option>
						<option value="53-19"><spring:message code="ssap.page13.childOfDomesticPartner" text="Child of domestic partner"/></option>

						<%-- <option value="Brother/sister"><spring:message code="ssap.page13.brotherSister" text="Brother/sister"/></option>
						<option value="stepbrother"><spring:message code="ssap.page13.stepBrother" text="Step Brother"/></option>
						<option value="stepsister"><spring:message code="ssap.page13.stepSister" text="Step Sister"/></option>				
						<option value="Other relative"><spring:message code="ssap.page13.otherRelative" text="Other relative"/></option>
						<option value="Other unrelated"><spring:message code="ssap.page13.otherUnrelated" text="Other unrelated"/></option> --%>
					</select>
				</div>
				<p class="pull-left gutter5-t">
					<spring:message code="ssap.page13.of" text="of"/> <span class="fullNameOfOtherChild"></span>
				</p>
				<div class="span12" id="applicant-relationship-error-div"></div>
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<fieldset>
				<legend>
					<spring:message code="ssap.page13.does" text="Does"/>
					<strong class="fullNameOfOtherChild"></strong>
					<spring:message code="ssap.page13.liveWith2BirthOrAdoptive" text="live with 2 birth or adoptive parents?"/>&nbsp;<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</legend>
				<div class="margin5-l">
					<label class="radio" for="appscr63BirthOrAdoptiveParentsYes">
						<input type="radio" value="yes" id="appscr63BirthOrAdoptiveParentsYes" name="birthOrAdoptiveParents" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#appscr63BirthOrAdoptiveParentsYesErrorDiv"/>
						<spring:message code="ssap.page13.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr63BirthOrAdoptiveParentsNo">
						<input type="radio" value="no" id="appscr63BirthOrAdoptiveParentsNo" name="birthOrAdoptiveParents" />
						<spring:message code="ssap.page13.no" text="No"/>
					</label>
					<div id="appscr63BirthOrAdoptiveParentsYesErrorDiv"></div>
				</div>
				</fieldset>
			</div><!-- control-group ends -->
				
				
				
				</div>
			</div><!-- parentOrCaretakerBasicDetailsContainer ends -->
		</form>
	</div>
</div>