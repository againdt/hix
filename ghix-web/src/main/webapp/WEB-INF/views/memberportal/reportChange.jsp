<div id="report-change" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 id="myModalLabel">Report a change</h3>
                </div>
                
                <div class="modal-body">
	              <p>You will be re-directed to the Federally Facilitated Marketplace to report the change to your household information.</p>
				
				 <p>You will return to Getinsured as soon as you're done.</p>	
				</div><!-- /modal-body -->

				<div class="modal-footer">
					<form action="http://www.vimo.com/">
						<input type="hidden" name="memberName" id="memberName" value="${memberName}"/>
                		<button class="btn pull-left" data-dismiss="modal" aria-hidden="true">Cancel</button>						
              			<button type="submit" class="btn btn-primary pull-right" aria-hidden="true" alt="Go to Federal Marketplace">Go to Federal Marketplace</button>
              		</form>
              	</div>
              	           
</div>


<%-- <div id="concierge-thanku" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 id="myModalLabel">GetInsured Concierge</h3>
                </div>
                
                <div class="modal-body">
                	<p>Thanks for contacting us!</p>
 
					<p>We'll get back to you as soon as possible (typically within 2 business days), and do our best to help.</p>
 
					<p>If you need immediate help, please call us at [phone number] or start an online chat with a Getinsured Concierge representative.</p>
				</div><!-- /modal-body -->

				<div class="modal-footer">
					<form action="http://www.vimo.com/">
						<input type="hidden" name="memberName" id="memberName" value="${memberName}"/>
                		<button class="btn pull-left" data-dismiss="modal" aria-hidden="true">Cancel</button>						
              			<button type="submit" class="btn btn-primary pull-right" aria-hidden="true" alt="Go to Federal Marketplace">Next</button>
              		</form>
              	</div>
              	           
</div> --%>