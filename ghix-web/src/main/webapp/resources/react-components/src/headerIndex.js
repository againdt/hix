import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/Header';
import { CookiesProvider } from 'react-cookie';

console.log("myHeaderID" + document.getElementById('myHeaderID'));
const node = document.getElementById('myHeaderID');
const userData = node.dataset.react ? JSON.parse(node.dataset.react) : {};

ReactDOM.render(<CookiesProvider><Header user={userData.user} welcome={sayHello} /></CookiesProvider>, document.getElementById('myHeaderID'));