<!DOCTYPE html>
<!--
~ Copyright (c) 2005-2014, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
~
~ WSO2 Inc. licenses this file to you under the Apache License,
~ Version 2.0 (the "License"); you may not use this file except
~ in compliance with the License.
~ You may obtain a copy of the License at
~
~    http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing,
~ software distributed under the License is distributed on an
~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~ KIND, either express or implied.  See the License for the
~ specific language governing permissions and limitations
~ under the License.
-->
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>
<%@ page import="org.wso2.carbon.identity.application.common.util.IdentityApplicationConstants"%>

<fmt:bundle basename="org.wso2.carbon.identity.application.authentication.endpoint.i18n.Resources">

    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Your Health Idaho</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
         <link href="/authenticationendpoint/assets/css/bootstrap-responsive.css" rel="stylesheet"  type="text/css" media="screen,print" />
		 <link href='//fonts.googleapis.com/css?family=Droid+Serif:400,700|Merriweather|Raleway:400,600' rel='stylesheet' type='text/css' media="screen,print" />
		 <link href="/authenticationendpoint/assets/css/bootstrap_ID.css" media="screen,print" rel="stylesheet" type="text/css" />
		 <link href="/authenticationendpoint/assets/css/ghixcustom.css" rel="stylesheet"  type="text/css" media="screen,print" />
		 <link href="/authenticationendpoint/assets/css/quickfixes_id.css" media="screen,print" rel="stylesheet" type="text/css" /> 
        
        
        <!--[if lt IE 8]>
        <link href="css/localstyles-ie7.css" rel="stylesheet">
        <![endif]-->

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="assets/js/html5.js"></script>
        <![endif]-->
        <script src="/authenticationendpoint/assets/js/jquery-1.7.1.min.js"></script>
        <script src="/authenticationendpoint/js/scripts.js"></script>
        <script src="/authenticationendpoint/assets/js/bootstrap.min.js"></script>
	<style>
		div.different-login-container a.truncate {
			width: 148px;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		
		.dl-horizontal dt {
			font-weight: normal;
		}
		
		.dl-horizontal dd {
			font-weight: bold;
		}
		
		#account-settings dt,#account-settings dd {
			margin-bottom: 15px;
		}
		
		#acc-settings-info dt {
			width: 160px;
		}
		
		#acc-settings-info dd {
			margin-left: 180px;
			font-weight: bold;
		}
		
		#change-email label,#change-email input,#change-password label,#change-password input
			{
			margin: 10px 0;
		}
		
		#change-questions .form-group {
			float: left;
			margin: 5px 0;
		}
	
		.subHeaderSignUp small img {
    			width: 18% !important;
    			height: 100%;
    			float: right;
    			position: relative;
    			top: -50px;
    			right: -35px;
  		}
	</style>

    </head>

    <body id="user-login">
    
	<!-- Header -->
	<div id="masthead" class="topbar">
		<div class="navbar navbar-fixed-top navbar-inverse" id="masthead"
			role="banner">
			<input type="hidden" id="exchangeName" value="Your Health Idaho">

			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<ul class="nav">
						<li><a href="http://www.yourhealthidaho.org/"><img class="brand"
								src="/authenticationendpoint/assets/img/logo_id.png" alt="Your Health Idaho" /></a><span
								style="display: none;">Your Health Idaho</span></li>
					</ul>
					<div class="nav-collapse collapse pull-right">  
						<ul class="nav pull-right clearfix" id="second-menu-nm">
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">Get Assistance <b class="caret"></b></a>
								<ul class="dropdown-menu">
								  <li><a href="https://idahohix.yourhealthidaho.org/hix/faq">Help Center</a></li>		 	
								  <li><a href="#" style="cursor:default">Consumers <i class="icon-phone"></i> 1-855-944-3246</a></li>
								</ul>
							</li> 
						</ul>
					</div>	
					
				</div>
			</div>
		</div>
	</div>
	<!-- End Header -->
<!-- 
	<div class="header-text">
        Login
    </div>
    
    <div class="container">
		<div class="row">
			<div class="span12">
				<h1>Login to continue</h1>
			</div>
		</div>
    </div> -->
    <!-- container -->
    <%@ page import="java.util.Map" %>
    <%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder" %>
    <%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.Constants" %>

    <%
        String queryString = request.getQueryString();
        Map<String, String> idpAuthenticatorMapping = null;
        if (request.getAttribute("idpAuthenticatorMap") != null) {
            idpAuthenticatorMapping = (Map<String, String>)request.getAttribute("idpAuthenticatorMap");
        }
        
        String errorMessage = "Your log in attempt was not successful, because the username/password does not match. Please try again.";
        String loginFailed = "false";
        
        if (CharacterEncoder.getSafeText(request.getParameter(Constants.AUTH_FAILURE)) != null &&
                "true".equals(CharacterEncoder.getSafeText(request.getParameter(Constants.AUTH_FAILURE)))) {
            loginFailed = "true";
            
            if(CharacterEncoder.getSafeText(request.getParameter(Constants.AUTH_FAILURE_MSG)) != null){
                errorMessage = (String) CharacterEncoder.getSafeText(request.getParameter(Constants.AUTH_FAILURE_MSG));
                
                if (errorMessage.equalsIgnoreCase("login.fail.message")) {
                    errorMessage = "Your log in attempt was not successful, because the username/password does not match. Please try again.";
                }
            }
        }
        
        String newUser = "false";
        String user_created = CharacterEncoder.getSafeText(request.getParameter("RelayState"));
        String userCreatedMessage = "";
        if(user_created != null && user_created.equalsIgnoreCase("true")){
        	 newUser = "true";
        	 userCreatedMessage = "Your account was successfully created. Please login to continue.";
        }
        
        //To show date on footer
        DateFormat formatter = new SimpleDateFormat("yyyy");
        String now = formatter.format(new Date());
        
      	//Check for account locking message
        String errorCode = CharacterEncoder.getSafeText(request.getParameter("errorCode"));
		if (errorCode != null && errorCode.contains("17003")) {
			loginFailed = "true";
			errorMessage = "Your account has been locked due to several unsuccessful login attempts. Please try again later.";
		}
    %>

    <script type="text/javascript">
        function doLogin() {
            var loginForm = document.getElementById('loginForm');
            loginForm.submit();
        }
    </script>
    
<% 

boolean hasLocalLoginOptions = false; 
List<String> localAuthenticatorNames = new ArrayList<String>();

if (idpAuthenticatorMapping.get(IdentityApplicationConstants.RESIDENT_IDP_RESERVED_NAME) != null){
	String authList = idpAuthenticatorMapping.get(IdentityApplicationConstants.RESIDENT_IDP_RESERVED_NAME);
	if (authList!=null){
		localAuthenticatorNames = Arrays.asList(authList.split(","));
	}
}


%>
<div id="container-wrap" class="row-fluid">
<div class="container">
<div class="row-fluid" id="titlebar">
		<h1 class="custom-margin">Log In</h1>	
		
		<% if ("true".equals(newUser)) { %>
			<div class="errorblock alert alert-info">
				<%=userCreatedMessage%>
			</div>
		<% } %>
</div>
<%if(localAuthenticatorNames.contains("BasicAuthenticator")){ %>
    <div id="local_auth_div">
<%} %>

		
		<div class="row-fluid">
		<div class="span10 offset1" id="rightpanel">
        <form action="../../commonauth" method="post" id="loginForm" class="form-horizontal box-loose" autocomplete="off" >
            <%
                if(localAuthenticatorNames.size()>0) {

                    if(localAuthenticatorNames.size()>0 && localAuthenticatorNames.contains("OpenIDAuthenticator")){
                    	hasLocalLoginOptions = true;
            %>

            <div class="row-fluid">
                <div class="span7">
                
                        <%@ include file="openid.jsp" %>

                </div>
                
            </div>

            <%
            } else if(localAuthenticatorNames.size()>0 && localAuthenticatorNames.contains("BasicAuthenticator")) {
            	hasLocalLoginOptions = true;
            %>
			<% if ("true".equals(loginFailed)) { %>
		            <div class="errorblock alert alert-info">
		                <%=errorMessage%>
		            </div>
			    <% } %>
            <div class="row-fluid">
		            
			  
                <div class="span8">
                        
<!-- Start - BA_NEW -->
<%
	if (CharacterEncoder.getSafeText(request.getParameter("username")) == null
			|| "".equals(CharacterEncoder.getSafeText(request.getParameter("username").trim()))) {
%>

<%
	loginFailed = CharacterEncoder.getSafeText(request.getParameter("loginFailed"));
	if (loginFailed != null) {
%>
<div class="alert alert-error subHeaderSignUp">
	<fmt:message key='<%=CharacterEncoder.getSafeText(request.getParameter("errorMessage"))%>' />
</div>
<%
	}
%>

<!-- Username -->
<div class="control-group">
	<label class="control-label" for="username">Email Address</label>

	<div class="controls">
		<input class="input-xlarge" type="text" id='username' name="username" size='30' />
	</div>
</div>

<%
	} else {
%>

<input type="hidden" id='username' name='username'
	value='<%=CharacterEncoder.getSafeText(request.getParameter("username"))%>' />

<%
	}
%>

<!--Password-->
<div class="control-group">
	<label class="control-label" for="password">Password</label>

	<div class="controls">
		<input type="password" id='password' name="password" class="input-xlarge" size='30' />
	</div>
</div>

<div class="control-group">
	<input type="hidden" name="sessionDataKey" value='<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>' />
	<div class="controls">
		<input type="submit" id="submit" onclick="convert()" 
			value="Log In"
			class="btn btn-primary btn-large margin10-t">
	</div>
</div>
<%
    //YHI Aliases
    Map<String,String> yhiEntityIds = new HashMap<String,String>();
    yhiEntityIds.put("YHIDEMO","https://iddemo.ghixqa.com/hix/");
    yhiEntityIds.put("YHI_QA","https://idqa.ghixqa.com/hix/");
    yhiEntityIds.put("YHI_QA2","https://idqa.getinsured.com/hix/");
    yhiEntityIds.put("YHISIT","https://idsit.ghixqa.com/hix/");
    yhiEntityIds.put("YHIUAT","https://iduat.getinsured.com/hix/");
    yhiEntityIds.put("YHIE2E1","https://ide2e.ghixqa.com/hix/");
    yhiEntityIds.put("YHICARRIER","https://idcarrier.getinsured.com/hix/");
    yhiEntityIds.put("YHIQA_STAGE","https://idqa-stage.ghixqa.com/hix/");
    yhiEntityIds.put("IDSTAGE","https://idstage.ghixtest.com/hix/");
    yhiEntityIds.put("YHI","https://idahohix.yourhealthidaho.org/hix/");
    yhiEntityIds.put("YHISEC","https://yhisec.getinsured.com/hix/");
    yhiEntityIds.put("YHIPWW_DEV","https://idssodev.ghixqa.com/hix/");
    yhiEntityIds.put("YHISSODEV","https://idssodev.ghixqa.com/hix/");
    yhiEntityIds.put("IEXDEV","https://iexdev.getinsured.com/hix/");
    
                                                            
    String relyingParty = CharacterEncoder.getSafeText(request.getParameter("relyingParty"));
    String redirectUrl = null;
                                                                 
    //Check for relying party
    if(relyingParty != null && !relyingParty.isEmpty()){
		
		relyingParty = relyingParty.toUpperCase();
    	
    	//Add cookie to response
    	Cookie relyingPartyCookie = new Cookie("relyingParty", relyingParty);
		relyingPartyCookie.setPath("/");
		relyingPartyCookie.setMaxAge(36000);
		response.addCookie(relyingPartyCookie);

		request.getSession().invalidate();
		request.getSession().setAttribute("relyingParty",relyingParty);
    	redirectUrl = yhiEntityIds.get(relyingParty);
    	//Relying party is something else other than YHI
    	if(redirectUrl == null){
    		redirectUrl = "http://localhost:8080/hix/";
	}
    }                                                                         												%>
<div class="control-group">
	<div class="controls">
		<a href="<%=redirectUrl%>account/user/forgotpassword"><small>Forgot Password?</small></a>
	</div>
</div>

<script type="text/javascript">
function convert() 
{
	var str = document.getElementById("username").value;
	document.getElementById("username").value = str.toLowerCase();
}
</script>
<!-- End - BA_NEW -->


                </div>
				<div class="span4 subHeaderSignUp nmhide mshide" style="border:0px !important">
					<strong>Already have an account?</strong>
					<p style="margin-bottom:20px">You can use your Idalink username and password.</p>
      				<img style="" title="idalink" alt="Welcome to idalink! Your online portal for managing benefits from Idaho's Department of Health and Welfare. idaLink" height="" width="" src="/authenticationendpoint/assets/img/idaLink_logo.png">
      			</div>
            <%
            } 
            } 
            %>

    <%if(idpAuthenticatorMapping.get(IdentityApplicationConstants.RESIDENT_IDP_RESERVED_NAME) != null){ %>        
	</div>
	<%} %>
	<%
        if ((hasLocalLoginOptions && localAuthenticatorNames.size() > 1) || (!hasLocalLoginOptions)
        		|| (hasLocalLoginOptions && idpAuthenticatorMapping.size() > 1)) {
    	%>
  <div class="container">
	<div class="row">
		<div class="span12">
		    <% if(hasLocalLoginOptions) { %>
			<h2>Other login options:</h2>
			<%} else { %>
			<script type="text/javascript">
			    document.getElementById('local_auth_div').style.display = 'block';
			</script>
			<%} %>
		</div>
	</div>
    </div>
    </div>	
	<div class="container different-login-container">
            <div class="row">
                                               
                    <%
                        for (Map.Entry<String, String> idpEntry : idpAuthenticatorMapping.entrySet())  {
                            if(!idpEntry.getKey().equals(IdentityApplicationConstants.RESIDENT_IDP_RESERVED_NAME)) {
                            	String idpName = idpEntry.getKey();
                            	boolean isHubIdp = false;
                            	if (idpName.endsWith(".hub")){
                            		isHubIdp = true;
                            		idpName = idpName.substring(0, idpName.length()-4);
                            	}
                    %>
                              <div class="span3">
                                    <% if (isHubIdp) { %>
                                    <a href="#"  class="main-link"><%=idpName%></a>
                                    <div class="slidePopper" style="display:none">
				                        <input type="text" id="domainName" name="domainName"/>
				                        <input type="button" class="btn btn-primary go-btn" onClick="javascript: myFunction('<%=idpName%>','<%=idpEntry.getValue()%>','domainName')" value="Go" />
			                        </div>
			                        <%}else{ %>
			                              <a onclick="javascript: handleNoDomain('<%=idpName%>','<%=idpEntry.getValue()%>')"  class="main-link truncate" style="cursor:pointer" title="<%=idpName%>"><%=idpName%></a>			                        
			                        <%} %>
		                      </div>
                            <%}else if(localAuthenticatorNames.size()>0 && localAuthenticatorNames.contains("IWAAuthenticator")) {
                            %>
                            	<div class="span3">
                                <a onclick="javascript: handleNoDomain('<%=idpEntry.getKey()%>','IWAAuthenticator')"  class="main-link" style="cursor:pointer">IWA</a>
	                            </div>
	                       <% 
                            }

                         }%>
                        
                    
               
            </div>
	    <% } %>
        </form>
        </div>
    </div>

    <script>
 	$(document).ready(function(){
		$('.main-link').click(function(){
			$('.main-link').next().hide();
			$(this).next().toggle('fast');
			var w = $(document).width();
			var h = $(document).height();
			$('.overlay').css("width",w+"px").css("height",h+"px").show();
		});
		$('.overlay').click(function(){$(this).hide();$('.main-link').next().hide();});
	
	});
        function myFunction(key, value, name)
        {
	    var object = document.getElementById(name);	
	    var domain = object.value;


            if (domain != "")
            {
                document.location = "../../commonauth?idp=" + key + "&authenticator=" + value + "&sessionDataKey=<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>&domain=" + domain;
            } else {
                document.location = "../../commonauth?idp=" + key + "&authenticator=" + value + "&sessionDataKey=<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>";
            }
        }
        
        function handleNoDomain(key, value)
        {


          document.location = "../../commonauth?idp=" + key + "&authenticator=" + value + "&sessionDataKey=<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>";
            
        }
        
    </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-43319506-1', 'auto', {
  'allowLinker': true
});
ga('send', 'pageview');
</script>
</div>
</div></div>
	<div id="footer">
		<footer class="container" role="contentinfo">
			<div class="row-fluid">
				<div class="pull-left">
					<ul class="nav nav-pills">
						<li id="copyrights">&copy;<%=now%> Your Health Idaho</li>
						<li><a
							href="http://www.yourhealthidaho.org/privacy-policy/"
							class="margin20-l">Privacy Policy</a></li>
					</ul>
				</div>
	
	
			</div>
		</footer>
	</div>	
</body>
    </html>

</fmt:bundle>

