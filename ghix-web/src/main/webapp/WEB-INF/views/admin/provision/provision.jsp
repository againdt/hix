<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src='<c:url value="/resources/js/lce/ui-bootstrap-tpls-0.11.0.js"/>'></script>

<style>
.table th a {
  font-weight: bold;
  color: #0092b3 !important;
}

#rulesTable .privileged {
	background-color: #f9f9f9;
}

</style>

<input type="hidden" id="csrftoken" name="csrftoken" value="${sessionScope.csrftoken}"/>

<div ng-app="roleProvisionApp" ng-controller='roleProvisionCtrl' class="gutter10" ng-cloak>
	<h1>Provision Configuration <small>{{data.length}} total rules</small></h1>
	
	<p ng-if="localData.error && !localData.loading" class="alert alert-error">
		Fail to update the provisioning rule, please try again later.
	</p>
	
	<div ng-if="localData.loading" class="center margin50-t">
		<img src="<c:url value="/resources/images/loader_gray.gif"/>"  alt="loading"/>
	</div>
	
	<div ng-if="!localData.loading" class="row-fluid">
		<form novalidate name="roleForm" class="form-horizontal margin20-t">
		  	<div class="control-group">
		  		<label class="control-label" for="provisioningRole">Select Provisioning Role</label>
		  		<div class="controls">
			  		<select id="provisioningRole" name="provisioningRole" ng-model="newRole.provisioning_role_label" required>
			  			<option value="">Select One</option>			
			  			<option ng-repeat="provisioningRole in provisioningRoleList" value="{{provisioningRole.label}}">{{provisioningRole.label}}</option>
			  		</select>	
		  		</div>
		  	</div>
		  	
		  	<div class="control-group">
		  		<label class="control-label" for="managedRole">Select Managed Role</label>
		  		<div class="controls">
			  		<select id="managedRole" name="managedRole" ng-model="newRole.managed_role_label" required>	
			  			<option value="">Select One</option>
			  			<option ng-repeat="managedRole in managedRoleList" value="{{managedRole.label}}">{{managedRole.label}}</option>
			  		</select>	
		  		</div>
		  	</div>
	  		
		  	<div class="control-group">
		  		<div class="controls">
			  		<label for="allowCreate">
			  			<input type="checkbox" id="allowCreate" ng-model="newRole.allow_add">
						Allow Create 
						<a provision-tooltip data-toggle="tooltip" href="javascript:void(0);" data-placement="right" rel="tooltip" data-original-title="Allow Create: Allows the user to add a user with this role and ability to Add/Remove this role from an existing user.">
							<i class="icon-question-sign"></i>
						</a>	
								  			
			  		</label>
			  		<label for="allowUpdate">
			  			<input type="checkbox" id="allowUpdate" ng-model="newRole.allow_update">
			  			Allow Update 
			  			<a provision-tooltip class="ttclass" data-toggle="tooltip" href="javascript:void(0);" data-placement="right" rel="tooltip" data-original-title="Allow Update: Allows the user to update the user details and not the role.">
			  				<i class="icon-question-sign"></i>
			  			</a>
			  		</label>
		  		</div>
		  	</div>
		  	
		  	<div class="control-group">
		  		<div class="controls">
			  		<button type="submit" class="btn btn-primary" ng-click="saveChange()" ng-disabled="roleForm.$invalid || hasSameRule()">Add</button>
		  		</div>
		  	</div>
		</form>
		
		<p class="alert alert-info"><img alt="required" src="/hix/resources/images/requiredAsterix.png"> In the highlighted rows, managed role is also privileged role.</p>
		
		<button type="button" class="btn btn-primary pull-right" ng-click="resetNewRole()">Show All Rules</button>
		<table id="rulesTable" class="table">
			<tr class="header">
				<th style="width:200px">
					<a href="javascript:void(0)" ng-click="orderByField='provisioning_role_label'; reverseSort = !reverseSort">Provisioning Role
						<span ng-if="orderByField == 'provisioning_role_label' && !reverseSort"><img src="<c:url value="/resources/images/i-aro-blu-sort-up.png"/>" alt="State sort ascending"></span>
						<span ng-if="orderByField == 'provisioning_role_label' && reverseSort"><img src="<c:url value="/resources/images/i-aro-blu-sort-dwn.png"/>" alt="State sort descending"></span>
					</a>
				</th style="width:200px">
				<th>
					<a href="javascript:void(0)" ng-click="orderByField='managed_role_label'; reverseSort = !reverseSort">Managed Role
						<span ng-if="orderByField == 'managed_role_label' && !reverseSort"><img src="<c:url value="/resources/images/i-aro-blu-sort-up.png"/>" alt="State sort ascending"></span>
						<span ng-if="orderByField == 'managed_role_label' && reverseSort"><img src="<c:url value="/resources/images/i-aro-blu-sort-dwn.png"/>" alt="State sort descending"></span>
					</a>
				</th>
				<th>Allow Create</th><th>Allow Update</th><th>Enabled</th><th style="width:15px"></th>
			</tr>
			<tr ng-if="noRuleFound() && !localData.tableLoading">
				<td colspan="6" >No match found, please add a new rule.</td>
			</tr>
			<tr ng-repeat="roleInfo in data | orderBy:orderByField:reverseSort | endWith : this : localData.currentPage*localData.pageSize" ng-if="!localData.tableLoading && RulesFilter(roleInfo)" ng-class="roleInfo.is_managing_privileged ? 'privileged':''"> 
		 		<td>{{roleInfo.provisioning_role_label}}</td>
		 		<td>{{roleInfo.managed_role_label}}</td>
				<td><input type="checkbox" ng-model="roleInfo.allow_add" ng-change="saveChange(roleInfo, $index)"></td>
				<td><input type="checkbox" ng-model="roleInfo.allow_update" ng-change="saveChange(roleInfo, $index)"></td>
				<td><input type="checkbox" ng-model="roleInfo.active" ng-change="saveChange(roleInfo, $index)"></td>
				<td><img ng-if="localData.updateLoading == $index" src="<c:url value="/resources/img/loading.gif"/>"  alt="loading"/></td>
			</tr>
			<tr ng-if="localData.tableLoading">
				<td colspan="6" style="padding-left: 42%;"><img src="<c:url value="/resources/images/loader_gray.gif"/>"  alt="loading"/></td>
			</tr>
		</table>
		
		<div class="pagination center" ng-if="!newRole.provisioning_role_label && !newRole.managed_role_label">
    		<pagination items-per-page="10" total-items="localData.totalRules" ng-model="localData.currentPage"></pagination>
    	</div>
	
	</div>
</div>

<script>
var roleProvisionApp = angular.module('roleProvisionApp', ['ui.bootstrap']);

var csrfToken = $('#csrftoken').val();

roleProvisionApp.controller('roleProvisionCtrl', function($scope, httpService) {	
	$scope.reverseSort = false;
	
	$scope.localData = {
		loading : true,	
		updateLoading : -1,
		tableLoading : false,
		error : true,
		totalRules : 0,
		currentPage : 1,
		pageSize : 10
	}
	
	$scope.newRole = {
		active: true,
		allow_add: false,
		allow_update: false,
		managed_role: "",
		managed_role_label: "",
		provisioning_role: "",
		provisioning_role_label: ""	
	};
	
	//make ajax call get data
	httpService.apiGetResponse("/hix/admin/user/privileges").success(function(response){
		$scope.localData.error = false;
		
		$scope.data = response.provisioning_rules;
		
		$scope.localData.totalRules =  $scope.data.length;
		
		
		$scope.provisioningRoleList = response.provisioning_roles;
		$scope.managedRoleList = response.managed_roles;
		
		$scope.localData.loading = false;
		
	}).error(function(response){
		$scope.localData.loading = false;
		$scope.localData.error = true;
	});
	
   
    $scope.saveChange = function(roleInfo, index){
    	var data = null;
    	
		$scope.localData.updateLoading = index;
		
    	if(roleInfo === undefined){ //save new role 
    		
    		$scope.localData.tableLoading = true;
    	
			data = {
    				provisioning_rules : [			
							$scope.newRole	
    					]
					};
        	        	
        	for(var i=0;i<$scope.provisioningRoleList.length;i++){
        		if($scope.provisioningRoleList[i].label === data.provisioning_rules[0].provisioning_role_label){
        			data.provisioning_rules[0].provisioning_role = $scope.provisioningRoleList[i].name;
        			break;
        		}
        	}
        	
        	for(var i=0;i<$scope.managedRoleList.length;i++){
        		if($scope.managedRoleList[i].label === data.provisioning_rules[0].managed_role_label){
        			data.provisioning_rules[0].managed_role = $scope.managedRoleList[i].name;
        			break;
        		}
        	}
        	
        	
    	}else{ //update current role
    		data = {
    				provisioning_rules : [			
						roleInfo	
    				]
  				  };
    	} 
    	
    	//make ajax call update data
    	httpService.apiPostResponse("/hix/admin/user/updatePrivileges", angular.toJson(data), csrfToken).success(function(response){
    		$scope.localData.error = false;
    		$scope.localData.updateLoading = -1;
    		
    		//need to get the id for new role to table, so need to make one more api call
    		if(roleInfo === undefined){
    			httpService.apiGetResponse("/hix/admin/user/privileges").success(function(response){
        			$scope.localData.error = false;
        			$scope.data = response.provisioning_rules;
        			/* $scope.localData.totalRules =  $scope.numberOfRulesShowed(); */
        			
        			$scope.localData.tableLoading = false;
        		}).error(function(){
        			$scope.localData.error = true;
        			$scope.localData.tableLoading = false;
        		});
    		}
    		
		}).error(function(response){
			$scope.localData.error = true;
			$scope.localData.updateLoading = -1;
			$scope.localData.tableLoading = false;
		});    	
    }
    
	$scope.RulesFilter = function(roleInfo){
		if(	(!$scope.newRole.provisioning_role_label && !$scope.newRole.managed_role_label) || 
   			((roleInfo.provisioning_role_label === $scope.newRole.provisioning_role_label && !$scope.newRole.managed_role_label) || 
   			(roleInfo.managed_role_label === $scope.newRole.managed_role_label && !$scope.newRole.provisioning_role_label)) ||
   			(roleInfo.provisioning_role_label === $scope.newRole.provisioning_role_label && roleInfo.managed_role_label === $scope.newRole.managed_role_label)
    	){
    		return true;
    	}else{
    		return false;
    	}
    	
    }
    
	
    $scope.hasSameRule = function(){
    	var provisioningRole = $scope.newRole.provisioning_role_label;
    	var managedRole = $scope.newRole.managed_role_label;
    	for(var i = 0; i < $scope.data.length; i++){
    		if(provisioningRole === $scope.data[i].provisioning_role_label && managedRole === $scope.data[i].managed_role_label){
    			return true;
    		}
    	}
    	
    	return false;
    } 
    
    $scope.noRuleFound = function(){
    	var provisioningRole = $scope.newRole.provisioning_role_label;
    	var managedRole = $scope.newRole.managed_role_label;
    	
    	if(!provisioningRole && managedRole){ //managed role is selected
    		for(var i = 0; i < $scope.data.length; i++){
        		if(managedRole === $scope.data[i].managed_role_label){
        			return false;
        		}
        	}
    		return true;
    	}else if(!managedRole && provisioningRole){//provision role is selected
    		for(var i = 0; i < $scope.data.length; i++){
        		if(provisioningRole === $scope.data[i].provisioning_role_label){
        			return false;
        		}
        	}
    		return true;
    	}else if(provisioningRole && managedRole){ //both selected
    		if($scope.hasSameRule()){
    			return false;
    		}else{
    			return true;
    		} 
    	}
    }
    
    $scope.resetNewRole = function(){
    	//reset
		$scope.newRole = {
			active: true,
			allow_add: false,
			allow_update: false,
			managed_role: "",
			managed_role_label: "",
			provisioning_role: "",
			provisioning_role_label: ""	
		}; 
    }
    
    
});

roleProvisionApp.factory("httpService",function($http){
	
	var apiGetRequest = function(url) {
		return $http({
			method: "GET",
			url: url
		});
	};
	
	var apiPostRequest = function(url, data, csrfToken) {
		return $http({
			method: "POST",
			url: url,
			data: data,
			headers: {
				   'csrftoken': csrfToken
				 }
		});	
	};
	
	return {
		apiGetResponse: function(url) {
			return apiGetRequest(url);
		},
		apiPostResponse: function(url, data, csrfToken) {
			return apiPostRequest(url, data, csrfToken);
		}
	};
});


roleProvisionApp.filter('endWith', function() {
    return function(rules, scope, end) {
    	if(scope.newRole.provisioning_role_label || scope.newRole.managed_role_label){
    		return rules;
    	}else{
    		end = +end; //parse to int
            return rules.slice(end-10, end);
    	}
    }
});


roleProvisionApp.directive('provisionTooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                $(element).tooltip('show');
            }, function(){
                $(element).tooltip('hide');
            });
        }
    };
});

</script>
