
package com.getinsured.iex.ssap;


import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Name {

	@Pattern(regexp = "^[a-zA-Z]{0,45}$")
    @Expose
    private String middleName;
    @NotEmpty
    @Pattern(regexp = "^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$")
    @Expose
    private String lastName;
    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z]{1,45}$")
    @Expose
    private String firstName;
    @Expose
    private String suffix;

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = StringUtils.trim(middleName);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = StringUtils.trim(lastName);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = StringUtils.trim(firstName);
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        return "Name{" +
            "middleName='" + middleName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", firstName='" + firstName + '\'' +
            ", suffix='" + suffix + '\'' +
            '}';
    }
}
