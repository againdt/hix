package com.getinsured.eligibility.at.ref.service;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.model.Plan;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("lceElgLostPostHandlerService")
@Scope("singleton")
public class LceElgLostPostHandlerService extends LceElgLostHandlerService implements LceProcessHandlerService {
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	private static final Logger LOGGER = Logger.getLogger(LceElgLostPostHandlerService.class);

	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceElgLostPostHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		final SsapApplication enrolledApplication = loadApplication(enrolledApplicationId);
		if (enrolledApplication.getApplicationStatus().equals(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode()) 
				&& currentApplication.getApplicationStatus().equals(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode())){
		postProcess(currentApplication, enrolledApplication, lceProcessRequestDTO);
		}
		LOGGER.info("LceElgLostPostHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}
	
	
	
	void postProcess(SsapApplication currentApplication, SsapApplication enrolledApplication, LCEProcessRequestDTO lceProcessRequestDTO){
		
		//HIX-115439 Calling IND71G instead of IND71
		String changePlan = "Y";
		String previousDentalApplicationStatus = enrolledApplication.getApplicationDentalStatus();
		String previousApplicationStatus = enrolledApplication.getApplicationStatus();
		if(super.isAutoCSChangeAllowed(lceProcessRequestDTO)) {
				Date coverageStartDate = updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
				final AptcUpdate aptcUpdate = populateAptcUpdateRequest(currentApplication, lceProcessRequestDTO.getEnrolledApplicationId(),coverageStartDate);
				final boolean status = ind71GCall(lceProcessRequestDTO.getEnrolledApplicationId(), lceProcessRequestDTO.getCurrentApplicationId(),
						currentApplication.getCoverageYear(), aptcUpdate,changePlan) ;
					if (status) {
						LOGGER.info("IND71G update successful for  " + currentApplication.getId());
						processCSSuccess(currentApplication.getId(), lceProcessRequestDTO.getEnrolledApplicationId(),previousApplicationStatus,previousDentalApplicationStatus);
						triggerNonFinConversionNoticeWithAptcIneligibile(currentApplication.getCaseNumber());
					} else {
						LOGGER.info("IND71G update failed for  " + currentApplication.getId());
						updateCurrentAppToER(currentApplication);
						updateAllowEnrollment(currentApplication, Y);
						triggerSEPEmail(currentApplication);
					}
		}else {
			triggerNonFinConversionNoticeWithAptcCsrIneligibile(currentApplication.getCaseNumber());
		}

	}

}
