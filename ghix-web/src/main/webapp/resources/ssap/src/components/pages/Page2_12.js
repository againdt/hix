import React, { Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData, fetchTribes } from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import CONFIG from '../../.env';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import ENDPOINTS from '../../constants/endPointsList';
import EMPTY_OBJECTS from '../../constants/emptyObjects';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../constants/validationErrorMessages';



/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'American Indians and Alaskan Native' Page
 *
 */
const Page2_12 = props => {

    const {
        data,
        form: {
            Form: {
                isFormSubmitted,
                isEligiblyForSave
            }
        },
        householdMembers,
        tribes
    } = props;

    const {
        isFirstVisit
    } = props;

    // Local State of Functional Component

    const [localState, setLocalState] = useState({
        noneOfTheAbove: isFirstVisit ? false : householdMembers.every(member => !member.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator)
    });

    const { noneOfTheAbove } = localState;

    // Fetching tribes:

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            tribes.length === 0 && props.actions.fetchTribes(`${window.location.origin}${ENDPOINTS.TRIBES_ENDPOINT}`);
        }
    }, []);

    // Updating some props in JSON with the simultaneous check if
    // <memberOfFederallyRecognizedTribeIndicator> is false (to clear <americanIndianAlaskaNative> object)

    const onFieldChange = (newData, indexInArray) => {

        const currentState = newData.americanIndianAlaskaNative.state;

        if (currentState !== householdMembers[indexInArray].americanIndianAlaskaNative.state) {
            newData.americanIndianAlaskaNative.tribeName = null;
        }

        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = newData;
        props.actions.changeData(newState);

        setLocalState(state => ({
            ...state,
            noneOfTheAbove: householdMembers.every(member => !member.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator)
        }));
    };

    // Updating status of members if they are checked or not
    // If at least one of them is checked - uncheck 'None of Above' checkbox
    // If this member unchecked - clear <americanIndianAlaskaNative> object related to this member

    const onFieldChange_member = (newData, indexInArray) => {
        setLocalState(state => ({
            ...state,
            noneOfTheAbove: householdMembers.every(member => !member.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator),
        }));

        const newState = {...data };

        if(!newData.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator) {

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[indexInArray]
                .americanIndianAlaskaNative = cloneObject(EMPTY_OBJECTS.TRIBE_STATUS);

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[indexInArray]
                .incomes = newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[indexInArray]
                    .incomes
                    .map(i => ({
                        ...i,
                        tribalAmount: 0
                    }));
        } else {
            newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = newData;
        }
        props.actions.changeData(newState);
    };

    // Updating status of 'None of Above' checkbox and if it is checked:
    // a) uncheck all household members
    // b) clear all <americanIndianAlaskaNative> objects in household members
    // c) save changes to Redux Store

    const onFieldChange_none_of_above = newData => {

        setLocalState(state => ({
            ...state,
            noneOfTheAbove: newData.noneOfTheAbove
        }));

        const newState = {...data };

        const updatedHouseholdMembers =
            householdMembers
                .map(member => {
                    member.americanIndianAlaskaNative = cloneObject(EMPTY_OBJECTS.TRIBE_STATUS);
                    return member;
                });
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember = updatedHouseholdMembers;
        props.actions.changeData(newState);
    };

    // Iterating through all household members and providing for rendering data
    // UI is bound to Local State if household member is checked or not

    const getHouseholdMembers = (householdMembers, tribes) => {

        const { noneOfTheAbove } = localState;

        const combinedMembers = [];

        householdMembers.forEach((member, i) => {
            const {
                americanIndianAlaskaNative: {
                    tribeName,
                    state,
                    memberOfFederallyRecognizedTribeIndicator
                },
                name
            } = member;

            const memberName = `${normalizeNameOfPerson(name)}'s`;

            combinedMembers.push(
                <Fragment key={`${name.firstName}_${i}`}>
                    <ul className="usa-unstyled-list">
                        <li>
                            <UniversalInput
                                label={{
                                    text: normalizeNameOfPerson(name)
                                }}
                                inputData={{
                                    isInputRequired: false,
                                    type: INPUT_TYPES.CHECKBOX,
                                    name: 'memberOfFederallyRecognizedTribeIndicator',
                                    classes:[''],
                                    checked: memberOfFederallyRecognizedTribeIndicator,
                                    value: memberOfFederallyRecognizedTribeIndicator,
                                    currentData: member,
                                    fields: ['americanIndianAlaskaNative'],
                                }}
                                inputActions={{
                                    onFieldChange: onFieldChange_member
                                }}
                                indexInArray={i}
                            />
                        </li>
                    </ul>

                    {/************************************************************
                     Begin - If #memberOfFederallyRecognizedTribeIndicator (true)
                     *************************************************************/}

                    {
                        memberOfFederallyRecognizedTribeIndicator &&

                        <Fragment>
                            <div className="usa-width-one-whole margin-b-40 gi-dropdown-layout--02">
                                <fieldset>
                                    <legend>
                                        Select&nbsp;
                                        <span><strong>
                                            { memberName }
                                        </strong> membership</span>
                                    </legend>
                                    <UniversalInput
                                        label={{
                                            text: 'State',
                                            isLabelRequired: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.DROPDOWN,
                                            name: 'state',
                                            value: state,
                                            options: DROPDOWN_OPTIONS.STATES_FOR_TRIBES(tribes),
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: cloneObject(member),
                                            fields: ['americanIndianAlaskaNative']
                                        }}
                                        errorClasses={['margin-b-0']}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                        indexInArray={i}
                                    />
                                    <UniversalInput
                                        label={{
                                            text: 'Tribe Name',
                                            isLabelRequired: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.DROPDOWN,
                                            name: 'tribeName',
                                            value: tribeName,
                                            options: DROPDOWN_OPTIONS.TRIBES(tribes, state),
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: member,
                                            fields: ['americanIndianAlaskaNative']
                                        }}
                                        errorClasses={['margin-b-0']}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                        indexInArray={i}
                                    />
                                </fieldset>
                            </div>
                        </Fragment>

                    }

                    {/************************************************************
                     End - If #memberOfFederallyRecognizedTribeIndicator (true)
                     *************************************************************/}

                </Fragment>
            );

        });

        combinedMembers.push(
            <UniversalInput
                key={`None_of_above_${combinedMembers.length + 1}`}
                label={{
                    text: 'None of the Above',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: 'noneOfTheAbove',
                    checked: noneOfTheAbove,
                    value: noneOfTheAbove,
                    currentData: Object.assign({}, localState),
                    fields: [],
                }}
                inputActions={{
                    onFieldChange: onFieldChange_none_of_above
                }}
            />
        );
        return combinedMembers;
    };

    const checkTopLevelValidation = () => {
        return noneOfTheAbove || householdMembers.some(member => member.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator);
    };

    return (
        <Fragment>
            <div id="fhAdditionalInfo" className="fade-in ">
                <div className="subsection">
                    <Form topLevelValidity={checkTopLevelValidation()}>
                        <p className="required-true">
                            Are any of the people below American Indian/Alaska Native?
                        </p>
                        <ValidationErrorMessage
                            isVisible={isFormSubmitted && !isEligiblyForSave}
                            messageText={VALIDATION_ERROR_MESSAGES.ANY_VALUE}
                            errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                        />
                        {
                            getHouseholdMembers(householdMembers, tribes)
                        }
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    exception: state.exception,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    tribes: state.tribes
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        fetchTribes
    }, dispatch)
});

Page2_12.defaultProps = {
    name: 'Page2_12'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_12);
