import {
  SELECT_CHOICE,
  CLOSED_VALUE,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY,
  SUNDAY
} from '../constants/AvailabilityConstants';

export const buildAvailabilityHoursRequest = (isAvailable, values) => {
  let body = {
    isAvailable,
    brokerAvailabilities: [
      {
        day: MONDAY,
        fromTime:
          values.MondayStart === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.MondayStart,
        toTime:
          values.MondayEnd === SELECT_CHOICE ? CLOSED_VALUE : values.MondayEnd
      },
      {
        day: TUESDAY,
        fromTime:
          values.TuesdayStart === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.TuesdayStart,
        toTime:
          values.TuesdayEnd === SELECT_CHOICE ? CLOSED_VALUE : values.TuesdayEnd
      },
      {
        day: WEDNESDAY,
        fromTime:
          values.WednesdayStart === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.WednesdayStart,
        toTime:
          values.WednesdayEnd === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.WednesdayEnd
      },
      {
        day: THURSDAY,
        fromTime:
          values.ThursdayStart === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.ThursdayStart,
        toTime:
          values.ThursdayEnd === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.ThursdayEnd
      },
      {
        day: FRIDAY,
        fromTime:
          values.FridayStart === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.FridayStart,
        toTime:
          values.FridayEnd === SELECT_CHOICE ? CLOSED_VALUE : values.FridayEnd
      },
      {
        day: SATURDAY,
        fromTime:
          values.SaturdayStart === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.SaturdayStart,
        toTime:
          values.SaturdayEnd === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.SaturdayEnd
      },
      {
        day: SUNDAY,
        fromTime:
          values.SundayStart === SELECT_CHOICE
            ? CLOSED_VALUE
            : values.SundayStart,
        toTime:
          values.SundayEnd === SELECT_CHOICE ? CLOSED_VALUE : values.SundayEnd
      }
    ]
  };
  return body;
};
