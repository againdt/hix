package com.getinsured.ahbx.platform.ecm.webclient;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.ahbx.platform.ecm.CheckinAttachedFile;
import com.getinsured.ahbx.platform.ecm.CheckinAttachedFileResponse;
import com.getinsured.ahbx.platform.ecm.CheckinFile;
import com.getinsured.ahbx.platform.ecm.CheckinFileResponse;
import com.getinsured.ahbx.platform.ecm.mapper.CreateContentMapper;
import com.getinsured.ahbx.util.AHBXUtils;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformResponse;

/**
 *
 * Class to invoke AHBX ECM SOAP Service.
 *
 * @author Ekram Ali Kazi
 *
 */
@SuppressWarnings("deprecation")
@Component
public class CreateContentAction {

	private static final Logger LOGGER = Logger.getLogger(CreateContentAction.class);

	@Value("#{configProp['ecmServiceWSDL']}")
	private String wsdlUrl;

	@Autowired
	private WebServiceTemplate webServiceTemplate;
	//@Autowired private GIHTTPAuthClient gihttpAuthClient;
	
	//@Autowired private SoapWebClient<JAXBElement<CheckinFileResponse>> soapWebClientV1;
	//@Autowired private SoapWebClient<JAXBElement<CheckinAttachedFileResponse>> soapWebClient;

	public String createContent(PlatformRequest platformRequest) {
		
		/**
		 * Initiate Platform Response Object, to be returned to the caller
		 */
		PlatformResponse platformResponse = new PlatformResponse();
		platformResponse.startResponse();
		platformResponse.setAppID(wsdlUrl);

		/**
		 * Populate AHBX CheckinFile Request Object
		 */
		JAXBElement<CheckinAttachedFile> checkinAttachedFileRequest = formAHBXRequest(platformRequest);
		//LOGGER.info("CheckinAttachedFile request - " + checkinAttachedFileRequest.getValue());

		JAXBElement<CheckinAttachedFileResponse> ahbxResponse = null;

		try {
			webServiceTemplate.setDefaultUri(wsdlUrl);
			/*The below line is added for basic authentication for ECM.
			 *  As per request commenting this on 15 Jan 2014 */
            //webServiceTemplate.setMessageSender(new CommonsHttpMessageSender(gihttpAuthClient.getHttpClient()));
            ahbxResponse = (JAXBElement<CheckinAttachedFileResponse>) webServiceTemplate.marshalSendAndReceive(checkinAttachedFileRequest);
//			ahbxResponse = soapWebClient.send(checkinAttachedFileRequest, wsdlUrl,"SHARED-SERVICES-ECM-CREATE", webServiceTemplate);
		} catch (WebServiceIOException wsIOE) {
			LOGGER.error(wsIOE);
			LOGGER.error("IOException Stacktrace : " + ExceptionUtils.getFullStackTrace(wsIOE));
			platformResponse.setModuleStatusCode("P-E101");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(wsIOE.getMessage());
		} catch (SoapFaultClientException sfce) {
			LOGGER.error(sfce);
			LOGGER.error("Fault Code : " + sfce.getFaultCode());
			LOGGER.error("Fault Reason : " + sfce.getFaultStringOrReason());
			LOGGER.error("Fault Role : " + sfce.getSoapFault().getFaultActorOrRole());
			LOGGER.error("Fault Stacktrace : " + ExceptionUtils.getFullStackTrace(sfce));
			platformResponse.setModuleStatusCode("P-E102");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(sfce.getMessage());
		} catch (Exception e) {
			LOGGER.error(e);
			platformResponse.setModuleStatusCode("P-E103");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(e.getMessage());
		}

		//LOGGER.info("CheckinAttachedFile response - " + ahbxResponse);

		/**
		 * If AHBX Response found, populate Platform Response Object, in this
		 * case AhbxEcmId
		 */

		if (ahbxResponse != null) {
			String abhxEcmId = processAHBXResponse(ahbxResponse);
			platformResponse.setModuleStatusCode("P-E000");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
			platformResponse.setAhbxEcmId(abhxEcmId);
		}

		platformResponse.endResponse();

		return AHBXUtils.marshal(platformResponse);
	}

	public String createContentV1(PlatformRequest platformRequest) {

		/**
		 * Initiate Platform Response Object, to be returned to the caller
		 */
		PlatformResponse platformResponse = new PlatformResponse();
		platformResponse.startResponse();
		platformResponse.setAppID(wsdlUrl);

		/**
		 * Populate AHBX CheckinFile Request Object
		 */
		JAXBElement<CheckinFile> checkinFileRequest = formAHBXRequestV1(platformRequest);
		//LOGGER.info("CheckinFile request - " + checkinFileRequest.getValue());

		JAXBElement<CheckinFileResponse> ahbxResponse = null;

		try {
			webServiceTemplate.setDefaultUri(wsdlUrl);
			ahbxResponse = (JAXBElement<CheckinFileResponse>) webServiceTemplate.marshalSendAndReceive(checkinFileRequest);
//			ahbxResponse = soapWebClientV1.send(checkinFileRequest, wsdlUrl,"SHARED-SERVICES-ECM-CREATE", webServiceTemplate);
		} catch (WebServiceIOException wsIOE) {
			LOGGER.error(wsIOE);
			platformResponse.setModuleStatusCode("P-E101");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(wsIOE.getMessage());
		} catch (SoapFaultClientException sfce) {
			LOGGER.error(sfce);
			LOGGER.error("Fault Code : " + sfce.getFaultCode());
			LOGGER.error("Fault Reason : " + sfce.getFaultStringOrReason());
			LOGGER.error("Fault Role : " + sfce.getSoapFault().getFaultActorOrRole());
			platformResponse.setModuleStatusCode("P-E102");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(sfce.getMessage());
		} catch (Exception e) {
			LOGGER.error(e);
			platformResponse.setModuleStatusCode("P-E103");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			platformResponse.setErrMsg(e.getMessage());
		}

		//LOGGER.info("CheckinFile response - " + ahbxResponse);

		/**
		 * If AHBX Response found, populate Platform Response Object, in this
		 * case AhbxEcmId
		 */

		if (ahbxResponse != null) {
			String abhxEcmId = processAHBXResponseV1(ahbxResponse);
			platformResponse.setModuleStatusCode("P-E000");
			platformResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
			platformResponse.setAhbxEcmId(abhxEcmId);
		}

		platformResponse.endResponse();

		return AHBXUtils.marshal(platformResponse);
	}
	private String processAHBXResponse(JAXBElement<CheckinAttachedFileResponse> ahbxResponse) {
		//LOGGER.info("CheckinAttachedFile response - " + ahbxResponse.getValue());
		return CreateContentMapper.map(ahbxResponse);
	}

	private JAXBElement<CheckinAttachedFile> formAHBXRequest(PlatformRequest platformRequest) {
		return CreateContentMapper.map(platformRequest.getEcmRequest());
	}

	private String processAHBXResponseV1(JAXBElement<CheckinFileResponse> ahbxResponse) {
		//LOGGER.info("CheckinFile response - " + ahbxResponse.getValue());
		return CreateContentMapper.mapV1(ahbxResponse);
	}
	
	private JAXBElement<CheckinFile> formAHBXRequestV1(PlatformRequest platformRequest) {
		return CreateContentMapper.mapV1(platformRequest.getEcmRequest());
	}

}
