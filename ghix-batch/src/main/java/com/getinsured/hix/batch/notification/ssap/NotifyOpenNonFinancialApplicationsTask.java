package com.getinsured.hix.batch.notification.ssap;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.model.SsapApplication;

public class NotifyOpenNonFinancialApplicationsTask extends StepExecutionListenerSupport implements Tasklet {

	private static final Logger lOGGER = Logger.getLogger(NotifyOpenNonFinancialApplicationsTask.class);
	private SsapApplicationRepository ssapApplicationRepository;
	private OpenNonFinancialApplicationNoticeService openNonFinancialApplicationNoticeService;
	
	private final List<String> openApplicationStatuses = Arrays.asList(ApplicationStatus.OPEN.getApplicationStatusCode() , 
			ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),
			ApplicationStatus.SIGNED.getApplicationStatusCode(),
			ApplicationStatus.SUBMITTED.getApplicationStatusCode());
	private String caseNumbers;
	@Override
	public void beforeStep(StepExecution stepExecution) {
		  JobParameters jobParameters = stepExecution.getJobParameters();
		  caseNumbers = jobParameters.getString("case_numbers");
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		lOGGER.debug(this.getClass().getName()+" started at " + new Timestamp(System.currentTimeMillis()));
		List <SsapApplication>  applications = new ArrayList<>();
		if(StringUtils.isNotBlank(caseNumbers)){
			List<String> cases =  convertStringTokenToList(caseNumbers,",");//Arrays.asList(caseNumbers.split(","));
			for(String caseNumber : cases){
				if(!StringUtils.isNumeric(caseNumber)){
					lOGGER.info(" invalid case number " + caseNumber);
				}else{
					try {
						List <SsapApplication>  apps = ssapApplicationRepository.findByCaseNumber(caseNumber);
						if(!apps.isEmpty()){
							applications.add( apps.get(0));
						}
					} catch (Exception e) {
						lOGGER.error(" error while fetching the application " + e);
					}
				}
			}			
			
		}else{
			applications = ssapApplicationRepository.getOpenApplications("N", openApplicationStatuses);
		}
		lOGGER.debug(" found  "+applications.size()+ " to process ");
		for(SsapApplication ssapApplication : applications){
			if(StringUtils.isNotBlank(ssapApplication.getCaseNumber())){
				try {
					String ecmid = openNonFinancialApplicationNoticeService.generateNoticeDocument(ssapApplication);
					lOGGER.debug("sent email for ecmid " + ecmid); 
				} catch (Exception e) {
					lOGGER.error(e);
				}
			}
		}
		lOGGER.debug(this.getClass().getName()+" finishing at " + new Timestamp(System.currentTimeMillis()));
		return RepeatStatus.FINISHED;
	}
	
	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(
			SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	public OpenNonFinancialApplicationNoticeService getOpenNonFinancialApplicationNoticeService() {
		return openNonFinancialApplicationNoticeService;
	}

	public void setOpenNonFinancialApplicationNoticeService(
			OpenNonFinancialApplicationNoticeService openNonFinancialApplicationNoticeService) {
		this.openNonFinancialApplicationNoticeService = openNonFinancialApplicationNoticeService;
	}
	
	 private List<String> convertStringTokenToList(String strParam1,
             String strParam2) {
     List<String> objData = new ArrayList<String>();
     if (strParam1 != null) {
             objData = new ArrayList<String>();
             StringTokenizer st = new StringTokenizer(strParam1, strParam2);
             while (st.hasMoreTokens()) {
                     objData.add(st.nextToken().toUpperCase());
             }
             st = null;
     }
     return objData;
}
}
