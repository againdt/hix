/**
 * Created by sellathurai_s on 9/5/18.
 */

class ProcessApplicationStatus {

    constructor(tabData, config) {
        this.tabData = tabData;
        this.config = config;
    }

    getAppStatus() {
        return this.tabData.applicationStatus ? this.tabData.applicationStatus : "NO";
    }

    getAppYear() {
        return this.tabData.applicationYear;
    }

    getHHMembersCount() {
        if(this.tabData.applicants) {
            return this.tabData.applicants ? this.tabData.applicants.length: 0;
        }

        return 0;
    }

    getPeriod() {
        return this.tabData.isInsideOEWindow ? "OE" : "OUTSIDE OE";
    }

    getAppStatusData(appObject){
        appObject.applicationStatus = this.getAppStatus();
        appObject.applicationYear = this.getAppYear();
        appObject.hhMemebersTotal = this.getHHMembersCount();
        appObject.applicantsInfo = this.getApplicationInfo();
        appObject.period = this.getPeriod();

        return appObject;
    }

    getApplicationStatus(){
        let appObject =  {
            "applicationStatus": "N/A",
            "applicationYear": "",
            "hhMemebersTotal": 0,
        };

        return this.getAppStatusData(appObject);
    }

    getApplicationStatusInfo() {
        return this.getApplicationStatus();
    }

    getApplicationInfo() {
        if(this.tabData.applicants) {
            return this.tabData.applicants.map(x => {return (x.firstName + " " + x.middleName + " " + x.lastName + " " + x.nameSuffix)});
        }
        return "";
    }
}

export { ProcessApplicationStatus as default};