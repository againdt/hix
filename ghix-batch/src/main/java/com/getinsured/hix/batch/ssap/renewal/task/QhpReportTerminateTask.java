package com.getinsured.hix.batch.ssap.renewal.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.ssap.renewal.service.QhpReportService;

/**
 * QHP Report Terminate class is used to clean Old QHP Reports data using using Job Execution ID.
 * 
 * @since May 23, 2019
 */
public class QhpReportTerminateTask implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(QhpReportTerminateTask.class);

	private QhpReportService qhpReportService;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		Long jobExecutionId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		LOGGER.debug("To terminate Batch Job Execution ID: {}", jobExecutionId);

		boolean hasCleanedOldData = qhpReportService.cleanOldQHPReportDataByJobId(jobExecutionId);

		if (hasCleanedOldData) {
			LOGGER.info("Successfully clean Old QHP Reports data from database.");
		}
		else {
			LOGGER.error("Failed clean Old QHP Reports data from database.");
		}
		return RepeatStatus.FINISHED;
	}

	public QhpReportService getQhpReportService() {
		return qhpReportService;
	}

	public void setQhpReportService(QhpReportService qhpReportService) {
		this.qhpReportService = qhpReportService;
	}
}
