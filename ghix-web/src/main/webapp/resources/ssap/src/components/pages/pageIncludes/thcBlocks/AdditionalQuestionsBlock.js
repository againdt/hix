import React, { Fragment } from 'react';
import _ from 'lodash';


// Components

import UniversalInput from '../../../../lib/UniversalInput';


// Utils

import normalizeNameOfPerson from '../../../../utils/normalizeNameOfPerson';


// Constants

import DROPDOWN_OPTIONS from '../../../../constants/dropdownOptions';
import INPUT_TYPES from '../../../../constants/inputTypes';
import VALIDATION_TYPES from '../../../../constants/validationTypes';



const AdditionalQuestionsBlock = props => {

    const {
        currentData,
        currentData: {
            name
        },
        progressValues: {
            provideMoreInfo,
            parentExtraQuestion,
            siblingExtraQuestion
        },
        ...restProps
    } = props;

    const cleanedRestProps = _.omit(restProps, 'children');

    return (
        <div>
            <UniversalInput
                legend={{
                    legendText:
                        <span>
                            Do you want to provide more information about the family members who live with &nbsp;
                            <strong>
                                {normalizeNameOfPerson(name)}
                            </strong>
                            ?
                        </span>
                }}
                label={{
                    text: '',
                    labelClass: ['usa-width-one-whole', 'layout--02'],
                    ulClass: ['usa-width-one-whole margin-b-20'],
                    showRequiredIcon: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.RADIO_BUTTONS,
                    classes:['disp-styl-none'],
                    name: 'provideMoreInfo',
                    value: provideMoreInfo,
                    options: DROPDOWN_OPTIONS.YES_NO,
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: currentData,
                    fields: []
                }}
                {...cleanedRestProps}
            />

            {
                provideMoreInfo &&

                    <Fragment>
                        <UniversalInput
                            legend={{
                                legendText:
                                    <span>
                                        Does  &nbsp;
                                        <strong>
                                            {normalizeNameOfPerson(name)} &nbsp;
                                        </strong>
                                        live with another parent and/or stepparent?
                                    </span>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole margin-b-20'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                classes:['disp-styl-none'],
                                name: 'parentExtraQuestion',
                                value: parentExtraQuestion,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: []
                            }}
                            {...cleanedRestProps}
                        />
                        <UniversalInput
                            legend={{
                                legendText:
                                    <span>
                                        Does &nbsp;
                                        <strong>
                                            {normalizeNameOfPerson(name)} &nbsp;
                                        </strong>
                                        live with brothers or sisters who are under age 19? (Include stepbrothers, stepsisters, half-brothers, half-sisters)?
                                    </span>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole margin-b-20'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                classes:['disp-styl-none'],
                                name: 'siblingExtraQuestion',
                                value: siblingExtraQuestion,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: []
                            }}
                            {...cleanedRestProps}
                        />
                    </Fragment>
            }
        </div>
    );
};

export default AdditionalQuestionsBlock;
