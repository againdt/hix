package com.getinsured.ssap.service;

import com.getinsured.ssap.model.financial.IfsvResponse;

/**
 * Income and Family Size Verification (IFSV) service definition.
 *
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
public interface IfsvService {

  /**
   * Invokes IFSV for given {@link com.getinsured.iex.ssap.model.SsapApplication} id.
   * @param ssapApplicationId SSAP id.
   */
  IfsvResponse invoke(final long ssapApplicationId);
}
