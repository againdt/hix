<link href="resources/css/slideshow-css.css" rel="stylesheet"  type="text/css" />
<script type="text/javascript" src="resources/js/slideshow.js"></script>
<style>
body {
padding-top:25px;
}

.clear-left {
clear:left;
}

.img-polaroid {
margin:10px;
}

figcaption {
font-size:80%;
font-weight:bold;
text-align:center;
}

figcaption span {
display:block;
line-height:10px;
}

#navhelp .navbar-inner {
margin-top:15px;
}

h3 {
font-size: em
}
</style>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<div class="row-fluid" role="main">
			<div id="gallery">
				<div id="slides">
					<div class="slide">
					<img src="<c:url value="/resources/images/family.png" />" width="" height="" alt="Individual & Families"/>
						<span class="left">
							<%-- <h2><spring:message code="label.individuals"/></h2>
							--%>	
							<h2><spring:message code="label.beginHeader"/> <%-- <ecm:staticcontent contentPath="module_home/page_ghix_home/label_beginHeader"/>--%></h2>
			 				<%-- <p><spring:message code="label.individualstext"/></p>
						 --%>	
						<!--  <p>View your insurance options and seek government assistance for coverage.<p>
						 --> <p> <spring:message code="label.beginText"/> <%--<ecm:staticcontent contentPath="module_home/page_ghix_home/label_beginText"/>--%>
						 </p>
						 <p>
						 <a class="btn btn-primary large" href="<c:url value="/account/signup/individual" />"><spring:message code="label.applynowbtn"/></a> 
						 <a class="btn btn-primary large" href="<c:url value="/plandisplay/anonymous" />"><spring:message code="label.browseplansbtn"/></a>
						 </p>
							<!--<p class="small">Already a member? <a href="/eligibilitynm/e0-sign-in" data-toggle="modal" data-target="#myModal">Sign In</a></p>
						 	<p><a class="btn btn-primary large" href="<c:url value="/individuals/signup" />">First time here? Sign Up</a></p>
							<p class="small">Already a member? <a href="<c:url value="account/user/login" />">Sign In</a></p> -->
							<p class="small"><spring:message code="label.alreadymember"/> 
								<a href="<c:url value="account/user/login" />"><spring:message code="label.signin"/></a>
							</p>
						</span>
					</div>
					<div class="slide"><img src="<c:url value="/resources/images/employerpng.png" />" width="" height="" alt="Employers" title="Employers"  />
						<span class="left">
							<%-- <h2><spring:message code="label.employers"/></h2>
							 --%><h2><spring:message code="label.employerHeader"/></h2>
							<%-- <p><spring:message code="label.employerstext"/></p>
							 --%>
							 <p><spring:message code="label.employerText"/>
							</p>
							<p><a class="btn btn-primary large" href="<c:url value="/account/signup/employer" />"><spring:message code="label.firsttimehere"/></a>
							</p>
							
							<p class="small"><spring:message code="label.alreadymember"/> <a href="<c:url value="account/user/login" />"><spring:message code="label.signin"/></a></p>
						</span>
					</div>
					<div class="slide"><img src="<c:url value="/resources/images/youngadult.jpg" />"  width="" height="" alt="Employees" title="Employees"  />
						<span class="left">
							<%-- <h2><spring:message code="label.employees"/></h2>
							 --%>
						<h2><spring:message code="label.employeeHeader"/></h2>
							
							<%-- <p><spring:message code="label.employeestext"/></p>
							 --%>
							 <p><spring:message code="label.employeeText"/>
							 </p>
							 <p><a class="btn btn-primary large" href="#" data-toggle="modal" data-target="#myModal"><spring:message code="label.firsttimehere"/></a></p>
							<p class="small"><spring:message code="label.alreadymember"/> <a href="#" data-toggle="modal" data-target="#myModal"><spring:message code="label.signin"/></a></p>
						<!--  <p><a class="btn btn-primary large">First time here? Sign Up</a> 
							<p class="small">Already a member? <a href="<c:url value="account/user/login" />">Sign In</a></p> -->
						</span>
					</div>
					<div class="slide"> <img src="<c:url value="/resources/images/brokerspng.png" />"  width="" height="" alt="Brokers" title="Brokers" />
						<span class="left">
							<%-- <h2><spring:message code="label.broker"/></h2>
							 --%><h2><spring:message code="label.brokerHeader"/></h2>
							<%-- <p><spring:message code="label.brokertext"/></p>
							 --%>
							 <p><spring:message code="label.brokerText"/>
							 </p><!--<p><a class="btn btn-primary large" href="<c:url value="/broker/signup" />">First time here? Sign Up</a> -->
							<p><a class="btn btn-primary large" href="<c:url value="/account/signup/broker" />"><spring:message code="label.firsttimehere"/></a>
							<p class="small"><spring:message code="label.alreadymember"/> <a href="<c:url value="account/user/login" />"><spring:message code="label.signin"/></a></p>
						</span>
					</div>
					<!-- <div class="slide"><img src="<c:url value="/resources/images/Eli-providers-L.jpg" />" width="465" height="274" alt="Healthcare providers" title="Healthcare providers" />
						<span class="left">
							<h2>Compare Doctors &amp; Hospitals</h2>
							<p>Find Doctors &amp; Dentists, Compare Hospital Quality of Care, Compare Procedure  Costs</p>
							<p><a class="btn btn-primary large" href="<c:url value="/provider/search/" />">Get Started</a> 
							<p class="small">Doctors and Hospital Administrators <a href="#" data-toggle="modal" data-target="#myModal">Register</a></p>
							<p class="small">Returning Doctors and Hospital Administrators <a href="#" data-toggle="modal" data-target="#myModal">Sign In</a></p>						
						</span>
					</div> -->
					<%-- <div class="slide"><img src="<c:url value="/resources/images/Eli-insurers-L.jpg" />" width="465" height="274" alt="A couple filling out a form" title="A couple filling out a form"  /> 
						<span class="left">
							<h2><spring:message code="label.insurers"/></h2>
							<p><spring:message code="label.insurerstext"/> </p>
							<p><a class="btn btn-primary large" href="<c:url value="/planmgmt/carrier/signup" />">First time here? Sign Up</a> 
							<p class="small"><spring:message code="label.alreadymember"/> <a href="<c:url value="account/user/login" />"><spring:message code="label.signin"/></a></p>
						</span>			   	
					</div>
					 --%>
				</div>
				<div id="menu">
					<ul>
						<li class="menuItem"><a href="" title="Individuals and families"><spring:message code="label.individuals"/> <%--<ecm:staticcontent contentPath="module_home/page_ghix_home/label_individuals"/> --%></a></li>
						<li class="menuItem"><a href=""><spring:message code="label.employers"/></a></li>
						<li class="menuItem"><a href=""><spring:message code="label.employees"/></a></li>
						<li class="menuItem"><a href=""><spring:message code="label.broker"/></a></li>
						<!-- <li class="menuItem"><a href="">Doctors &amp; Hospitals</a></li> -->
						<%-- <li class="menuItem last"><a  href=""><spring:message code="label.insurers"/></a></li>--%></ul>
				</div>
	 	 	</div>
	
		<!-- Example row of columns -->
		
		<div class="row-fluid" id="content-bottom">
			<div class="gutter10">
				<div class="span9">
<%-- 					<h3><spring:message  code="label.messagefromgovernor"/></h3> --%>
						<h1> Getinsured Health Exchange Solution</h1>
<!-- 						<div class="img-polaroid  pull-left"> -->
<%-- 						<img  class="" alt="Office of the Governor of State" src="<c:url value="/resources/images/nm_governor.jpg" />"> --%>
<%-- 						<figcaption class=""> <span><spring:message code="label.governor"/></span></figcaption>	 --%>
<!-- 						</div>	 -->
<%-- 						<p><spring:message code="label.messagefromgovernorbody"/></p> --%>
<%-- 						<p><spring:message code="label.messagefromgovernorbody2"/> <a href="http://www.nmhia.com/">http://www.nmhia.com/</a></p> --%>
							<p>This page represents the starting point for interaction between the Exchange and all its constituencies&mdash;Individuals &amp; Families, Employers, Employees, and Brokers.  The page is structured so that each constituency can easily navigate to their view of the Exchange.  
						
					</div>
					<div class="span3">
							<%-- <h3><spring:message code="label.securedataheader"/></h3>
							<p><spring:message code="label.securedatabody"/></p>   
						
							<h3><spring:message code="label.privacyprotectionheader"/></h3>
							<p><spring:message code="label.privacyprotectionbody"/></p>
						 --%>	
                          <h3><spring:message code="label.newsEvents"/></h3>
                         <!--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
                        -->   <ul class="eventsSmall">
    						<li class="vevent festival first last future today">    
                            <a href="#" class="url" title="#">
                            <span class="calSheet calSheetSmall">
                        <abbr class="dtstart" title="20120127T0150Z">
                            <span title="Friday 4 January 2013">
                                <span class="month">Jan</span>
                                <span class="day">11</span>
                            </span>
                        </abbr>
                  			  </span> <strong class="summary"><spring:message code="label.newsHeader1"/></strong><br>
                            </a><small class="location adr">
<%--                             <spring:message code="label.newsBody1"/> --%>
								State is on track to establish a health insurance exchange this year now that the federal...
                                </small>
                            </li>
                            		<li class="vevent festival first last future today">    
                            <a href="#" class="url" title="#">
                            <span class="calSheet calSheetSmall">
                        <abbr class="dtstart" title="20120127T0150Z">
                            <span title="Friday 11 January 2012">
                                <span class="month">Jan</span>
                                <span class="day">4</span>
                            </span>
                        </abbr>
                  			  </span>            <strong class="summary"><spring:message code="label.newsHeader2"/></strong><br>
                            </a>        <small class="location adr">
                                <spring:message code="label.newsBody2"/></small>
                            </li>
                   		 </ul>
                   <!-- events -->
					</div>
				</div>
			
			
			<div class="notes" style="display:none">
					<div class="span">
						<p>This page represents the starting point for interaction between the Exchange and all its constituencies&mdash;Employers, Employees, Broker, Navigators, Carriers, State residents, Exchange operators, Doctors, Physician Clinics and Hospitals.  The page is structured so that each constituency can easily navigate to their view of the Exchange.  
						</p>
					</div>
			</div>
		</div>
			
<!-- 		<button type="button" data-toggle="modal" data-target="#myModal">Launch modal</button> -->
	    <div id="myModal" class="modal hide fade">
		    <div class="modal-header">
		   		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    	<h3>&nbsp;</h3>
		    </div>
		    <div class="modal-body">
		    	<h3 class="alert alert-info"><spring:message code="label.featureNotAvailable"/></h3>
		    </div>
		    <div class="modal-footer">
			    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.close"/></a>
		    </div>
	    </div>		
	</div><!-- end of container -->
	
<script type="text/javascript">
		$('#myModal').modal("hide")
</script>
