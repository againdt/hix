package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.batch.ssap.renewal.service.CloneService;
import com.getinsured.hix.batch.ssap.renewal.util.ClonePartitionerParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * Clone Writer class is used to process and write Clone data.
 * 
 * @since July 30, 2019
 */
public class CloneWriter implements ItemWriter<SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(CloneWriter.class);
	private long jobExecutionId = -1;
	private static final String PROCESSING_RESULT = "processingResult";

	private JobService jobService;
	private CloneService cloneService;
	private ClonePartitionerParams clonePartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		jobExecutionId = stepExecution.getJobExecution().getId();
	}

	@Override
	public void write(List<? extends SsapApplication> autoEnrollListToWrite) throws Exception {

		String batchJobStatus = null;
		if (jobService != null && jobExecutionId != -1) {
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}

		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {
			throw new UnexpectedJobExecutionException(EnrollmentConstants.BATCH_STOP_MSG);
		}

		Map<String, String> result = new HashMap<String, String>();
		Map<String, Long> summary = new HashMap<String, Long>();
		List<Map<String, String>> jsonResultSuccess = new ArrayList<Map<String, String>>();
		List<Map<String, String>> jsonResultFailure = new ArrayList<Map<String, String>>();
		String processingResult = null;
		Long count = null;

		if (CollectionUtils.isNotEmpty(autoEnrollListToWrite)) {

			for (SsapApplication renewalApplication : autoEnrollListToWrite) {

				try {
					result = cloneService.cloneSsap(renewalApplication, clonePartitionerParams.getRenewalYear(),
							clonePartitionerParams.getCloneToNFA(),
							clonePartitionerParams.getIsCloneProgramEligibility());
				}
				catch (Exception e) {
					result = new HashMap<String, String>();
					result.put(PROCESSING_RESULT, RenewalStatus.ERROR.toString());
					result.put("exception", ExceptionUtils.getFullStackTrace(e));
					cloneService.logToGIMonitor(e, 50003, Long.toString(renewalApplication.getId()));
				}

				if (result != null) {
					processingResult = result.get(PROCESSING_RESULT);
				}

				if (processingResult != null && !processingResult.equals(RenewalStatus.ERROR.toString())) {
					jsonResultSuccess.add(result);
				}
				else {
					jsonResultFailure.add(result);
				}

				if (processingResult != null) {
					count = summary.get(processingResult);

					if (count == null) {
						count = 1L;
						summary.put(processingResult, count);
					}
					else {
						count++;
						summary.put(processingResult, count);
					}
				}
				cloneService.logData(jsonResultSuccess, jsonResultFailure, summary);
			}
		}
		LOGGER.info("Successfully execute Clone Job.");
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public CloneService getCloneService() {
		return cloneService;
	}

	public void setCloneService(CloneService cloneService) {
		this.cloneService = cloneService;
	}

	public ClonePartitionerParams getClonePartitionerParams() {
		return clonePartitionerParams;
	}

	public void setClonePartitionerParams(ClonePartitionerParams clonePartitionerParams) {
		this.clonePartitionerParams = clonePartitionerParams;
	}
}
