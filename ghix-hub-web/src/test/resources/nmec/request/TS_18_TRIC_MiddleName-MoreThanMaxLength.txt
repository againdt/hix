{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload":{
  "taxHousehold": [
    {
      "householdMember": [ {
        
          "dateOfBirth": "09/17/1960",
          "name": {
            "firstName": "Peter",
            "middleName": "This is a test to validate middle name exceeds the max lenth od 50 varchars",
            "lastName": "England"
          },
          "socialSecurityCard": {
            "socialSecurityNumber": "222224761"
          },
          "gender": "M",
        "householdContact": {
          "homeAddress": {
            "state": "MD"
          }
        },
        "insurance": {
          "insurancePolicyEffectiveDate": "01/01/2014",
          "insurancePolicyExpirationDate": "12/31/2014"
        },
        "organization": [
          {
            "organizationCode": "TRIC"
          }
        ]
      }  ]
    }
  ]
}}