/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;

import com.getinsured.hix.dto.enrollment.ExternalEnrollmentCsvDTO;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Service to process external enrollment flat file
 * @author negi_s
 *
 */
public interface EnrlPopulateExtEnrollmentService {
	
	/**
	 * Processes the external flat file received
	 * @param jobService
	 * @param stepExecution
	 * @param replace
	 * @throws IOException
	 * @throws GIException
	 * @throws InterruptedException
	 */
	void processExternalCSV(JobService jobService, StepExecution stepExecution, String replace)
			throws IOException, GIException, InterruptedException;
	
	/**
	 * Populates the file records to the external enrollment table
	 * @param fileName
	 * @param batchExecutionId
	 * @param jobService
	 * @param stepExecution
	 * @param archivePath
	 * @return
	 * @throws Exception
	 */
	String populateExtEnrollmentData(String fileName, Long batchExecutionId, JobService jobService,
			StepExecution stepExecution, File archivePath) throws Exception;
	
	/**
	 * Updates applicant matched flag and the tobacco usage indicator in the ssap applicants table
	 */
	void matchAndUpdateApplicants();
	
	void updateSummery(Integer fileId, String status);
	
	Integer createSummary(String filePath, Long batchExecutionId, boolean isDuplicate);
	
	int loadAndSaveExternalEnrollment(List<ExternalEnrollmentCsvDTO> csvLineSaveList, Integer fileId, List<String> errorList);

}
