package com.getinsured.hix.entity.web.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.hix.broker.service.BrokerIndTriggerService;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.service.jpa.ConsumerComposite;
import com.getinsured.hix.broker.utils.ApplicationStatus;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.broker.BrokerBOBDetailsDTO;
import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.DesignateAssister.Status;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.household.service.PreferencesService;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import bsh.StringUtil;

/**
 * Handles requests for the Locate Assister Pages.
 */
@SuppressWarnings("unused")
@Controller
public class LocateAssisterController {
	private static final String APPLICANT_NAME = "applicantName";

	private static final String IS_INDIVIDUAL_USER_EXISTS = "isIndividualUserExists";

	private static final String IS_CONSUMER_ACTIVATED = "isConsumerActivated";

	private static final String PRI_CONTACT_PRIMARY_PHONE_NUMBER3 = "priContactPrimaryPhoneNumber3";

	private static final String PRI_CONTACT_PRIMARY_PHONE_NUMBER2 = "priContactPrimaryPhoneNumber2";

	private static final String PRI_CONTACT_PRIMARY_PHONE_NUMBER1 = "priContactPrimaryPhoneNumber1";

	private static final String REDIRECT_ENTITY_ASSISTER_INDIVIDUALS_DESIG_STATUS = "redirect:/entity/assister/individuals?desigStatus=";

	private static final String RETURN_FROM_REST_CALL = "Return from REST call : ";

	private static final String RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS = "Retrieving Assister Detail REST Call Starts.";

	private static final String RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS = "Retrieving Assister Detail REST Call Ends.";

	private static final String PREV_STATUS = "prevStatus";

	private static final String HAS_PERMISSION_MODEL_USER_DESIGNATE_ASSISTER = "hasPermission(#model, 'USER_DESIGNATE_ASSISTER')";

	private static final String HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL = "hasPermission(#model, 'ASSISTER_VIEW_INDIVIDUAL')";

	private static final String DESIG_ID = "desigId";

	private static final Logger LOGGER = LoggerFactory.getLogger(LocateAssisterController.class);

	private static final String PAGE_TITLE = "page_title";
	private static final String ENROLLMENTENTITY = "enrollmentEntity";
	private static final String PAGE_NUMBER = "pageNumber";
	private static final String NO_OF_ASSISTERS = "noOfAssisters";
	private static final String SHOW_ASSISTER_LIST = "showAssisterList";
	private static final String LIST_OF_ASSISTERS = "listOfAssisters";
	private static final String ASSISTERS_WITH_LANGUAGES = "listOfAssistersWithLanguages";
	private static final String ENROLLMENTENTITY_DETAIL = "enrollmentEntityDetail";
	private static final String ASSISTER = "assister";
	private static final String ASSISTER_LANGUAGES = "assisterLanguages";
	private static final String SITE = "site";
	private static final String ENTITY_NAME = "entityName";
	private static final String LANGUAGE = "language";
	private static final String ZIPCODE = "zipCode";
	private static final String DISTANCE = "distance";
	private static final String RESULT_SIZE = "resultSize";
	private static final String DATE_FORMAT = "MM/dd/yyyy";
	private static final String DESIG_STATUS = "desigStatus";
	private static final String ESIGN_NAME = "esignName";
	private static final String FIRSTNAME = "firstName";
	private static final String LASTNAME = "lastName";
	private static final String ASSISTER_ID = "assisterId";
	public static final Integer PAGE_SIZE = 10;
	private static final String NAVIGATION_MENU_MAP = "navigationmenumap";
	private static final String LANGUAGES_LIST = "languagesList";
	private static final String ANONYMOUS_FLAG = "anonymousFlag";
	private static final String EXT_INDIVIDUAL = "extIndividual";
	private static final String RECORD_TYPE = "recordType";
	private static final String RECORD_ID = "recordId";
	private static final String SHOW_DESIGNATE = "ShowDesignate";
	private static final String ALREADY_DESIGNATED_NAME = "alreadyDesignatedName";
	private static final String SAME_ASSISTER = "isSameAssister";
	private static final String ECMRELATIVEPATH = "individualnotifications";
	private static final String FALSE = "false";
	private static final String LIST = "list";
	private static final String REQ_URI = "reqURI";
	private static final String SWITCH_ACC_URL = "switchAccUrl";
	private static final String SHOW_SWITCH_ROLE_POPUP = "showSwitchRolePopup";
	private static final String STATUS = "status";
	private static final String Y = "Y";
	private static final String EXTERNAL_INDIVIDUAL = "externalIndividual";
	private static final String INDIVIDUAL_ID = "individualId";
	private static final String CURRENT_PAGE = "currentPage";
	private static final String SEARCH_CRITERIA = "searchCriteria";
	private static final String INDIVIDUAL_LIST = "individualList";
	private static final String INDIVIDUALS = "individuals";
	private static final String ID = "id";
	private static final String YEAR = "year";
	private static final String DAY = "day";
	private static final String MONTH = "month";
	private static final String TRUE = "true";
	private static final String PAGE_SIZE_ATTR = "pageSize";
	private static final String NO_HOUSEHOLD_RECORD_FOUND_FOR_THE_USER_IN_REPOSITORY = "No Household record found for the user in repository.";
    private static final String LAST_VISITED_URL_BEFOR_SWITCH = "lastVisitedURLBeforSwitch";
	private static final String NOTIFICATION_TEMPLATE_NAME = "IndividualDedesignationTemplateEnrollmentCounselor";
	private static final String EXCHANGE_NAME = "exchangeName";
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	private static final String EXCHANGE_ADDRESS_LINE_ONE = "exchangeAddressLineOne";
	private static final String STATE_NAME = "stateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_CITY_NAME = "cityName";
	private static final String EXCHANGE_PIN_CODE = "pinCode";
	private static final String ENROLLMENTENTITY_NAME = "EntityName";
	private static final String NOTIFICATION_SEND_DATE = "notificationDate";
	private static final String INDIVIDUAL_FRST_NAME = "IndividualFN";
	private static final String INDIVIDUAL_MIDDLE_NAME = "IndividualMN";
	private static final String INDIVIDUAL_LST_NAME = "IndividualLN";
	private static final String INDIVIDUAL_DESIG_STATUS = "DesignationStatus";
	private static final String COUNSELOR_FIRST_NAME = "EnrollmentCounselorFN";
	private static final String COUNSELOR_LAST_NAME = "EnrollmentCounselorLN";
	private static final String ENROLL_ENTITY_NAME = "EnrollmentEntityName";
	private static final String COUNCELOR_INFO_URL = "URLIndividualPortalCounselorInformation";
	private static final String INDIVIDUAL_LOGIN_URL = "URLIndividualPortalLogIn";
	public static final String PRIMARYSITE = "PRIMARY";
	private static final String ASSISTER_NAME = "counselorName";
	public static final String REASON_FOR_DE_DESIGNATION = "ReasonForDeDesignation";
	public static final String INDIVIDUAL ="individual";
	public static final String ACCEPTED_NOTICE_CLASS_NAME ="IndividualEntityOrCounselorDesignationAcceptedNotice";
	public static final String DECLINED_NOTICE_CLASS_NAME ="IndividualEntityOrCounselorDesignationDeclinedNotice";
	public static final String DATE_FORMAT_PATTERN = "MMMMMMMMM dd, yyyy";
	public static final String DENIED = "Denied";
	public static final String ACCEPTED = "Accepted";
	private static final String REDIRECT = "redirect:";
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static final String EXCEPTION_OCCURRED_WHILE_SEARCHING_ENTITIES = "Exception occured while searching entities : ";
	private static final String EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST = "Exception occured while declining user designation request : ";
	private static final int ZERO = 0;
	private static final int TWO = 2;
	private static final int THREE = 3;
	private static final int SIX = 6;
	private static final int TEN = 10;

	@Autowired private Gson platformGson;
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private EERestCallInvoker restClassCommunicator;
	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private DesignateService designateService;
	@Autowired
	private BrokerService brokerService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private EntityUtils entityUtils;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	private AccountActivationService accountActivationService;
	@Autowired 
	private BrokerIndTriggerService brokerIndTriggerService;
	@Autowired
	private PreferencesService preferencesService;
	
	/**
	 * Renders the Locate Assistance Type selection page
	 *
	 * @param model
	 * @param request
	 * @return URL String
	 */
	@RequestMapping(value = "/entity/locateassister/locateassistancetype", method = { RequestMethod.GET })
	public String locateAssisterOrAgentForIndividual(Model model, HttpServletRequest request) {
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		model.addAttribute(LANGUAGES_LIST, loadLanguages());
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Broker Search");
		return "entity/assister/locateassistancetype";
	}

	private String loadLanguages(){
		LOGGER.info("getLanguageList : START");
		String jsonLangData = null;

		List<String> languageSpokenList = null;

		languageSpokenList = lookupService.populateLanguageNames("");
		LOGGER.info("getLanguageList : END");
		jsonLangData = platformGson.toJson(languageSpokenList);

		return jsonLangData;
	}
	
	/**
	 * Renders Entities Search Page
	 *
	 * @param entityName
	 * @param language
	 * @param zipCode
	 * @param distance
	 * @param model
	 * @return URL String
	 */
	@RequestMapping(value = "/entity/locateassister/searchentities", method = { RequestMethod.GET })
	public String searchAssisters(@RequestParam(value = ENTITY_NAME, required = false) String entityName,
			@RequestParam(value = LANGUAGE, required = false) String language,
			@RequestParam(value = ZIPCODE, required = false) String zipCode,
			@RequestParam(value = DISTANCE, required = false) String distance,
			Model model, HttpServletRequest request) {

		LOGGER.info("searchAssisters: START");

		try {
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				String recordId = (String) request.getSession()
						.getAttribute(RECORD_ID);
				String recordType = (String) request.getSession().getAttribute(
						RECORD_TYPE);
				String anonymousFlag = (String) request.getSession().getAttribute(
						ANONYMOUS_FLAG);
				if (anonymousFlag != null) {
					if ("N".equalsIgnoreCase(anonymousFlag) && recordId != null
							&& recordType != null) {
						if (EXT_INDIVIDUAL.equalsIgnoreCase(recordType)) {
							LOGGER.info("External Individual BOB Detail AHBX service call Starts : ");
							String response = brokerMgmtUtils
									.retrieveAndSaveExternalIndividualDetail(Long
											.valueOf(recordId));
							LOGGER.info("IND52:External Individual BOB Detail AHBX service call completed");
						}
					}
				}
			}
			
			model.addAttribute(LANGUAGES_LIST, loadLanguagesData());
			LOGGER.info("Inside Entity Search Page");

			model.addAttribute(ENTITY_NAME, XssHelper.stripXSS(entityName));
			model.addAttribute(LANGUAGE, StringEscapeUtils.escapeJavaScript(entityUtils.checkLanguage(language)));
			model.addAttribute(ZIPCODE, XssHelper.stripXSS(zipCode));
			model.addAttribute(DISTANCE, XssHelper.stripXSS(distance));

		} catch(GIRuntimeException giexception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_SEARCHING_ENTITIES, giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_SEARCHING_ENTITIES, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("searchAssisters: END");

		return "entity/assister/searchentities";
	}
	
	private void validateRequestParams(HttpServletRequest request){
		List<String> parameterNames = new ArrayList<String>(request.getParameterMap().keySet());
		if(parameterNames!=null){
			List<String> params = new ArrayList<>();
			params.add(ENTITY_NAME);
			params.add(LANGUAGE);
			params.add(ZIPCODE);
			params.add(DISTANCE);
			params.add("submit2");
			params.add("pageNumber");
			params.add("pageSize");
			params.add("languageNames");
			
			for(String param:parameterNames){
				if(!params.contains(param)){
					throw new GIRuntimeException("Invalid Parameter");
				}
			}
		}
	}

	/**
	 * Fetches list of Entities based on entered Search Criteria
	 *
	 * @param entityName
	 * @param language
	 * @param zipCode
	 * @param distance
	 * @param result
	 * @param model
	 * @param request
	 * @return URL String
	 */
	@RequestMapping(value = "/entity/locateassister/entitylist", method = {RequestMethod.GET})
	public String enrollmententityList(@RequestParam(value = ENTITY_NAME, required = false) String entityName,
			@RequestParam(value = LANGUAGE, required = false) String language,
			@RequestParam(value = ZIPCODE, required = false) String zipCode,
			@RequestParam(value = DISTANCE, required = false) String distance, Model model, HttpServletRequest request) {

		LOGGER.info("enrollmententityList: START");

		LOGGER.info("Inside EE manage list");
		Integer pageSize = PAGE_SIZE;
		request.setAttribute(PAGE_SIZE_ATTR, pageSize);
		request.setAttribute(REQ_URI, LIST);
		String param = request.getParameter(PAGE_NUMBER);
		Integer startRecord = 0;
		int currentPage = 1;
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
			currentPage = Integer.parseInt(param);
			model.addAttribute(PAGE_NUMBER, param);
		} else {
			model.addAttribute(PAGE_NUMBER, 1);
		}

		try {
			validateRequestParams(request);
			
			entityName = XssHelper.stripXSS(entityName);
			language = XssHelper.stripXSS(language);
			zipCode = XssHelper.stripXSS(zipCode);
			distance = XssHelper.stripXSS(distance);

			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setPageSize(pageSize);
			enrollmentEntityRequestDTO.setStartRecord(startRecord);
			enrollmentEntityRequestDTO.setEntityName(entityName);
			enrollmentEntityRequestDTO.setLanguage(language);
			
			if (entityName != null && !"".equals(entityName)) {
				if(entityName.contains("&")){
					String entityParts[] = entityName.split("\\&");
					if(entityParts.length > 1 && entityParts[1]!=null && entityParts[1].contains("=")){
						throw new GIRuntimeException("Invalid Entity Name");
					}
				}
			}
			
			if (language != null && !"".equals(language)) {
				if(language.contains("&")){
					throw new GIRuntimeException("Invalid Language");
				}
			}
			
			if (zipCode != null && !"".equals(zipCode)) {
				if(!StringUtils.isNumeric(zipCode) || zipCode.length()!=5){
					throw new GIRuntimeException("Invalid Zip Code");
				}
				enrollmentEntityRequestDTO.setZipCode(zipCode);
			}

			if (distance != null && !"".equals(distance)) {
				if(!StringUtils.isNumeric(distance)){
					throw new GIRuntimeException("Invalid Distance");
				}
	            enrollmentEntityRequestDTO.setDistance(Integer.valueOf(distance));
			}

			String enrollmentEntiyRequest = JacksonUtils.getJacksonObjectWriter(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.info("Calling ghix-entity to retrieve Enrollment Entity list");
			String response = restClassCommunicator.searchEntities(enrollmentEntiyRequest);
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReader(EnrollmentEntityResponseDTO.class).readValue(response);

			model.addAttribute("entitySearchResult",
					enrollmentEntityResponseDTO.getListOfEnrollEntityAssisterSearchResult());
			model.addAttribute("totalResultCount", enrollmentEntityResponseDTO.getTotalEnrollmentEntities());

			model.addAttribute(RESULT_SIZE, enrollmentEntityResponseDTO.getTotalEnrollmentEntities());

			model.addAttribute(ENTITY_NAME, entityName);
			model.addAttribute(LANGUAGE, StringEscapeUtils.escapeJavaScript(language));
			model.addAttribute(ZIPCODE, zipCode);
			model.addAttribute(DISTANCE, distance);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while searching entities : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while searching entities in enrollmententityList : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("Redirecting to Search Enrollment Entity list page. ");
		LOGGER.info("enrollmententityList: END");

		return "entity/assister/viewentitylist";
	}

	/**
	 * Fetches Enrollment Entity Details for the given ID
	 *
	 * @param model
	 * @param request
	 * @param entityId
	 * @param siteId
	 * @param entityName
	 * @param language
	 * @param zipCode
	 * @param distance
	 * @param encrytedEnrollmentEntityid
	 * @param encrytedSiteId
	 * @return URL String
	 */
	@RequestMapping(value = "/entity/locateassister/entitydetail/{encrytedEnrollmentEntityid}/{encrytedSiteId}", method = RequestMethod.GET)
	public String entityDetail(@RequestParam(value = ENTITY_NAME, required = false) String entityName,
			@RequestParam(value = LANGUAGE, required = false) String language,
			@RequestParam(value = ZIPCODE, required = false) String zipCode,
			@RequestParam(value = DISTANCE, required = false) String distance, Model model, HttpServletRequest request,
			@PathVariable("encrytedEnrollmentEntityid") String encrytedEnrollmentEntityid, @PathVariable("encrytedSiteId") String encrytedSiteId,
			HttpServletResponse response) {

		LOGGER.info("entityDetail: START");

		try {
			validateRequestParams(request);
			
			if (entityName != null && !"".equals(entityName)) {
				if(entityName.contains("&")){
					String entityParts[] = entityName.split("\\&");
					if(entityParts.length > 1 && entityParts[1]!=null && entityParts[1].contains("=")){
						throw new GIRuntimeException("Invalid Entity Name");
					}
				}	
			}
			
			if (language != null && !"".equals(language)) {
				if(language.contains("&")){
					throw new GIRuntimeException("Invalid Language");
				}
			}
			
			if (zipCode != null && !"".equals(zipCode)) {
				if(!StringUtils.isNumeric(zipCode) || zipCode.length()!=5){
					throw new GIRuntimeException("Invalid Zip Code");
				}
			}

			if (distance != null && !"".equals(distance)) {
				if(!StringUtils.isNumeric(distance)){
					throw new GIRuntimeException("Invalid Distance");
				}
			}
			
			Integer entityId = null;
			Integer siteId=null;
			
			try {
				entityId=encrytedEnrollmentEntityid != null && !"".equals(encrytedEnrollmentEntityid) ?Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedEnrollmentEntityid)):null;

				Validate.notNull(entityId);

				siteId=encrytedSiteId != null && !"".equals(encrytedSiteId)? Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedSiteId)):null;

				Validate.notNull(siteId);
			} catch(GIRuntimeException giexception){
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return "entity/assister/errorPage";
			} 

			model.addAttribute(ENTITY_NAME, XssHelper.stripXSS(entityName));
			model.addAttribute(LANGUAGE, StringEscapeUtils.escapeJavaScript(language));
			model.addAttribute(ZIPCODE, XssHelper.stripXSS(zipCode));
			model.addAttribute(DISTANCE, XssHelper.stripXSS(distance));

			setEnrollmentEntityDetailAndAssisters(model, entityId, siteId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving entity detail : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving entity detail in entityDetail: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		model.addAttribute(SHOW_ASSISTER_LIST, FALSE);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(" entityDetail -> Anonymous Flag: " + request.getSession().getAttribute(ANONYMOUS_FLAG));
			LOGGER.info("entityDetail: END");
		}

		return "entity/assister/entitydetail";
	}

	/**
	 * Fetches list of Assisters for given Enrollment Entity
	 *
	 * @param entityId
	 * @param siteId
	 * @param model
	 * @param request
	 * @param entityName
	 * @param language
	 * @param zipCode
	 * @param distance
	 * @param encrytedEnrollmentEntityId
	 * @param encrytedenrollmentEntityDetailsiteId
	 * @return URL String
	 */
	@RequestMapping(value = "/entity/locateassister/viewassisterlist/{encrytedEnrollmentEntityId}/{encrytedenrollmentEntityDetailsiteId}", method = RequestMethod.GET)
	public String listOfAssistersByEnrollmentEntity(@RequestParam(value = ENTITY_NAME, required = false) String entityName,
			@RequestParam(value = LANGUAGE, required = false) String language,
			@RequestParam(value = ZIPCODE, required = false) String zipCode,
			@RequestParam(value = DISTANCE, required = false) String distance,
			@PathVariable("encrytedEnrollmentEntityId") String encrytedEnrollmentEntityId,
			@PathVariable("encrytedenrollmentEntityDetailsiteId") String encrytedenrollmentEntityDetailsiteId, 
			Model model, HttpServletRequest request, HttpServletResponse response) {

		LOGGER.info("listOfAssistersByEnrollmentEntity: START");

		try {

			Integer entityId = null;
			Integer siteId=null;
			try {
				entityId=encrytedEnrollmentEntityId !=null && !"".equals(encrytedEnrollmentEntityId)? Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedEnrollmentEntityId)):null;
				Validate.notNull(entityId);
				siteId=encrytedenrollmentEntityDetailsiteId != null && !"".equals(encrytedenrollmentEntityDetailsiteId)? Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedenrollmentEntityDetailsiteId)):null;
				Validate.notNull(siteId);
			} catch(GIRuntimeException giexception){
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return "entity/assister/errorPage";
			} 
			
			Integer pageSize = PAGE_SIZE;
			request.setAttribute(PAGE_SIZE_ATTR, pageSize);
			String param = request.getParameter(PAGE_NUMBER);
			Integer startRecord = 0;
			int currentPage = 1;
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1) * pageSize;
				currentPage = Integer.parseInt(param);
				model.addAttribute(PAGE_NUMBER, param);
			} else {
				model.addAttribute(PAGE_NUMBER, 1);
			}
			
			validateRequestParams(request);
			
			if (entityName != null && !"".equals(entityName)) {
				if(entityName.contains("&")){
					String entityParts[] = entityName.split("\\&");
					if(entityParts.length > 1 && entityParts[1]!=null && entityParts[1].contains("=")){
						throw new GIRuntimeException("Invalid Entity Name");
					}
				}
			}
			
			if (language != null && !"".equals(language)) {
				if(language.contains("&")){
					throw new GIRuntimeException("Invalid Language");
				}
			}

			if (zipCode != null && !"".equals(zipCode)) {
				if(!StringUtils.isNumeric(zipCode) || zipCode.length()!=5){
					throw new GIRuntimeException("Invalid Zip Code");
				}
			}

			if (distance != null && !"".equals(distance)) {
				if(!StringUtils.isNumeric(distance)){
					throw new GIRuntimeException("Invalid Distance");
				}
			}
			
			entityName = XssHelper.stripXSS(entityName);
			language = XssHelper.stripXSS(language);
			zipCode = XssHelper.stripXSS(zipCode);
			distance = XssHelper.stripXSS(distance);

			AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setEnrollmentEntityId(entityId);
			assisterRequestDTO.setSiteId(siteId);
			assisterRequestDTO.setPageSize(pageSize);
			assisterRequestDTO.setStartRecord(startRecord);

			LOGGER.info("Calling ghix-entity to retrieve list of Assisters for Enrollment Entity");
			String assisterRequest = JacksonUtils.getJacksonObjectWriter(AssisterRequestDTO.class).writeValueAsString(assisterRequestDTO);
			String assisterResponse = restClassCommunicator
					.retrieveListOfAssistersByEnrollmentEntityAndSiteId(assisterRequest);
			AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterResponse);
			LOGGER.debug(RETURN_FROM_REST_CALL);
			//add model attributes

			if(null == assisterResponseDTO) {
				LOGGER.warn("No Assiter Response DTO instance encountered for the request.");
			}
			else {
				model.addAttribute(LIST_OF_ASSISTERS, assisterResponseDTO.getListOfAssisters());
				model.addAttribute(ASSISTERS_WITH_LANGUAGES, assisterResponseDTO.getAssisterWithLanguages());
				model.addAttribute(RESULT_SIZE, assisterResponseDTO.getNoOfAssistersForEEAndSite());
			}

			Site site = new Site();
			site.setId(siteId);
			model.addAttribute(SITE, site);
			model.addAttribute(SHOW_ASSISTER_LIST, TRUE);
			model.addAttribute(ENTITY_NAME, entityName);
			model.addAttribute(LANGUAGE, StringEscapeUtils.escapeJavaScript(language));
			model.addAttribute(ZIPCODE, zipCode);
			model.addAttribute(DISTANCE, distance);
			//add enrollment entity and assister details to model
			setEnrollmentEntityDetailAndAssisters(model, entityId, siteId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving assister list : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving assister list for Enrollment Entity: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("listOfAssistersByEnrollmentEntity: END");

		return "entity/assister/entitydetail";
	}

	/**
	 * Fetches the given Assister's detail
	 *
	 * @param entityName The entity's name.
	 * @param language The entity's language.
	 * @param zipCode The entity's zip code.
	 * @param distance The entity's distance range.
	 * @param assisterId The Assister identifier.
	 * @param siteId The site identifier.
	 * @param model The current Model instance.
	 * @param request The current HttpServletRequest instance
	 * @param encrytedAssisterId
	 * @param encrytedPrimaryAssisterSiteId
	 *
	 * @return String The view name.
	 * @throws GIException The GIException instance.
	 */
	@RequestMapping(value = "/entity/locateassister/assisterdetail/{encrytedAssisterId}/{encrytedPrimaryAssisterSiteId}", method = RequestMethod.GET)
	public String assisterDetail(@RequestParam(value = ENTITY_NAME, required = false) String entityName,
			@RequestParam(value = LANGUAGE, required = false) String language,
			@RequestParam(value = ZIPCODE, required = false) String zipCode,
			@RequestParam(value = DISTANCE, required = false) String distance,
			@PathVariable("encrytedAssisterId") String encrytedAssisterId, @PathVariable("encrytedPrimaryAssisterSiteId") String encrytedPrimaryAssisterSiteId,
			Model model, HttpServletRequest request, HttpServletResponse response) throws GIException {

		LOGGER.info("assisterDetail: START");
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

		try {
			Integer assisterId=null;
			Integer siteId=null;
			try{
				LOGGER.warn("Assister id: "+encrytedAssisterId);
				assisterId=encrytedAssisterId != null && !"".equals(encrytedAssisterId)?Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedAssisterId)):null;
				Validate.notNull(assisterId);
				LOGGER.warn("PrimarySite id: "+encrytedPrimaryAssisterSiteId);
				siteId= encrytedPrimaryAssisterSiteId !=null && !"".equals(encrytedPrimaryAssisterSiteId)? Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(encrytedPrimaryAssisterSiteId)):null;
			} catch(GIRuntimeException giexception){
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				return "entity/assister/errorPage";
			} 
			
			Validate.notNull(siteId);
			assisterRequestDTO.setId(assisterId);
			assisterRequestDTO.setSiteId(siteId);

			AssisterResponseDTO assisterResponseDTO = getAssisterDetail(assisterRequestDTO);

			if(null == assisterResponseDTO) {
				LOGGER.warn("No Assister Response DTO instance encountered for the request.");
			}
			else {
				// added for language word wrap
				if(assisterResponseDTO.getAssisterLanguages() != null)
				{
					String languageStr = assisterResponseDTO.getAssisterLanguages().getSpokenLanguages();
					assisterResponseDTO.getAssisterLanguages().setSpokenLanguages(languageWordWrap(languageStr));
				}
				Site site = new Site();
				site.setId(siteId);

				populateModelForAssisterDetail(entityName, language, zipCode, distance,model, assisterResponseDTO, site);
			}

			String anonymousFlag = (String) request.getSession().getAttribute(ANONYMOUS_FLAG);
			//LOGGER.info(" assisterDetail -> Anonymous Flag: " + anonymousFlag);
			DesignateBroker designateBroker = null;

			if (null != anonymousFlag && "N".equalsIgnoreCase(anonymousFlag)) {

				AccountUser currentUser = entityUtils.getLoggedInUser();
				String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
				String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
				boolean switchedRole=false;
				if(isUserSwitchToOtherView != null && isUserSwitchToOtherView.trim().equalsIgnoreCase("y")) {
					switchedRole=true;
				}

				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					String recordId = (String) request.getSession().getAttribute("recordId");
					designateBroker = designateService.findBrokerByExternalIndividualId(Integer.valueOf(recordId));
					
					processDesignation(assisterId, assisterRequestDTO, designateBroker, model);
				} else {
					if(null == currentUser) {
						LOGGER.warn("No user found associated with the session.");
					}
					else {
						Household household = null;

						if(null != currentUser.getActiveModuleName() && currentUser.getActiveModuleName().equalsIgnoreCase( RoleService.INDIVIDUAL_ROLE)  && null != stateExchangeType && stateExchangeType.equalsIgnoreCase(INDIVIDUAL))
						{
							if(!switchedRole) {

								household = getHouseholdByUserId(currentUser);
								assisterRequestDTO.setIndividualId(household.getId());
								designateBroker = designateService.findBrokerByIndividualId(household.getId());
							}
							else {
								designateBroker = designateService.findBrokerByIndividualId(currentUser.getActiveModuleId());
								assisterRequestDTO.setIndividualId(currentUser.getActiveModuleId());
							}

						}
						processDesignation(assisterId, assisterRequestDTO, designateBroker, model);
					}
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving assister detail : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving assister detail : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("assisterDetail: END");
		return "entity/assister/assisterdetail";
	}
	
	private void processDesignation(Integer assisterId, AssisterRequestDTO assisterRequestDTO, DesignateBroker designateBroker, Model model) throws GIException{
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);

		Validate.notNull(assisterDesignateResponseDTO);

		DesignateAssister designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
		boolean isBrokerDesignated = isBrokerDesignatedToIndividual(designateBroker);

		if (designateAssister != null) {
			if (designateAssister.getStatus().toString().equals(DesignateAssister.Status.InActive.toString()) && !isBrokerDesignated) {
				model.addAttribute(SHOW_DESIGNATE, "Y");
				model.addAttribute(ALREADY_DESIGNATED_NAME, "");
			}
			else if(isBrokerDesignated){
				model.addAttribute(SHOW_DESIGNATE, "N");

				if(null != designateBroker) {
					Broker brokerDetails = brokerService.getBrokerDetail(designateBroker.getBrokerId());

					if( null != brokerDetails && null != brokerDetails.getUser()) {
						model.addAttribute(ALREADY_DESIGNATED_NAME, brokerDetails.getUser().getFirstName()+ " "+ brokerDetails.getUser().getLastName());
					}
				}
			}
			else
			{
				model.addAttribute(SHOW_DESIGNATE, "N");
				Assister assister = getAssisterById(Integer.valueOf(designateAssister.getAssisterId()));
				if(assister != null) {
					if(assister.getId()==assisterId){
						model.addAttribute(SAME_ASSISTER, "Y");
					}
					model.addAttribute(ALREADY_DESIGNATED_NAME, assister.getFirstName() + " " + assister.getLastName());
				}
			}
		}
		else if (isBrokerDesignated) {

				model.addAttribute(SHOW_DESIGNATE, "N");

				if(null != designateBroker) {
					Broker brokerDetails = brokerService.getBrokerDetail(designateBroker.getBrokerId());

					if( null != brokerDetails && null != brokerDetails.getUser()) {
						model.addAttribute(ALREADY_DESIGNATED_NAME, brokerDetails.getUser().getFirstName()+ " "+ brokerDetails.getUser().getLastName());
					}
				}
		}
		else
		{
			model.addAttribute(SHOW_DESIGNATE, "Y");
			model.addAttribute(ALREADY_DESIGNATED_NAME, "");
		}
	}

	private boolean isBrokerDesignatedToIndividual(DesignateBroker designateBroker){

		if (designateBroker != null && !designateBroker.getStatus().toString().equals(DesignateBroker.Status.InActive.toString())) {
			return true;
		}

		return false;
	}



	/**
	 * Method 'getHouseholdByUserId' to get HouseHold instance for a given user identifier.
	 *
	 * @param currentUser The current AccountUser instance.
	 * @return individual The current Household instance.
	 * @throws GIException The current GIException instance.
	 */
	private Household getHouseholdByUserId(AccountUser currentUser)
			throws GIException {
		Household individual = entityUtils.getHouseHoldByUserId(currentUser.getId());

		if(null == individual) {
			LOGGER.error(NO_HOUSEHOLD_RECORD_FOUND_FOR_THE_USER_IN_REPOSITORY);
			throw new GIException(NO_HOUSEHOLD_RECORD_FOUND_FOR_THE_USER_IN_REPOSITORY);
		}
		return individual;
	}

	/**
	 * Method to populate Model for AssisterDetail.
	 *
	 * @param entityName The entity name.
	 * @param language The language.
	 * @param zipCode The zipcode.
	 * @param distance The distance.
	 * @param model The current Model instance.
	 * @param assisterResponseDTO The current AssisterResponseDTO instance.
	 * @param site The Site instance.
	 */
	private void populateModelForAssisterDetail(String entityName,
			String language, String zipCode, String distance, Model model,
			AssisterResponseDTO assisterResponseDTO, Site site) {
		model.addAttribute(SITE, site);
		model.addAttribute(ASSISTER, assisterResponseDTO.getAssister());
		model.addAttribute(ASSISTER_LANGUAGES, assisterResponseDTO.getAssisterLanguages());
		model.addAttribute(ENTITY_NAME, entityName);
		model.addAttribute(LANGUAGE, StringEscapeUtils.escapeJavaScript(language));
		model.addAttribute(ZIPCODE, zipCode);
		model.addAttribute(DISTANCE, distance);
		model.addAttribute(ENROLLMENTENTITY, assisterResponseDTO.getAssister().getEntity());
	}

	/**
	 * Method to retrieve Assister designation detail by individual identifier.
	 *
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterDesignateResponseDTO The AssisterDesignateResponseDTO instance.
	 * @throws Exception
	 */
	private AssisterDesignateResponseDTO retrieveAssisterDesignationDetailByIndividualId(
			AssisterRequestDTO assisterRequestDTO) throws GIException {
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		LOGGER.info("REST call start to the GHIX-ENITY module to retreive Assister Designation details.");

		try {
			assisterDesignateResponseDTO = restClassCommunicator.retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);

			if(assisterDesignateResponseDTO != null && assisterDesignateResponseDTO.getResponseCode()==EntityConstants.RESPONSE_CODE_FAILURE){
					throw new Exception();
			}

			LOGGER.debug("REST call Ends to the GHIX-ENITY module.");
		} catch(Exception ex){
			LOGGER.error("Exception in retrieving designatedAssister details ");
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return assisterDesignateResponseDTO;
	}

	/**
	 * Method to get Assister detail.
	 *
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterResponseDTO The AssisterResponseDTO instance.
	 */
	private AssisterResponseDTO getAssisterDetail(
			AssisterRequestDTO assisterRequestDTO) {
		LOGGER.info("Calling ghix-entity to retrieve Assister Detail");
		String assisterResponse = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
		AssisterResponseDTO assisterResponseDTO;
		try {
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterResponse);
		} catch (Exception ex) {
			LOGGER.error("Unexpected error occured in while processing response from get assister details." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.debug(RETURN_FROM_REST_CALL + assisterResponseDTO);
		return assisterResponseDTO;
	}

	/**
	 * Renders the Assister E-Signature Page.
	 *
	 * @param model
	 * @param request
	 * @param assisterId
	 * @return URL string
	 * @throws GIException
	 */
	//@GiAudit(transactionName = "Assister ESignature", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/eSignature", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_USER_DESIGNATE_ASSISTER)
	public String assisterESignature(Model model,
			@RequestParam(value = ASSISTER_ID, required = false) String assisterId,
			HttpServletRequest request) throws GIException {

		LOGGER.info("assisterESignature: START");

		LOGGER.info("Assister Electronic Signature ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Assister Electronic Signature");
		
		String eSignatureURL;

		try {
			AccountUser user = entityUtils.getLoggedInUser();

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				model.addAttribute(APPLICANT_NAME, (user.getFirstName() + ' ' + user.getLastName()));
				String recordId = (String) request.getSession().getAttribute(RECORD_ID);
				LOGGER.info("Record ID : "+recordId);
				eSignatureURL = "entity/assister/esignatureforca";
			} else {
				if(userService.hasUserRole(user, RoleService.INDIVIDUAL_ROLE)){
	                Household household = getHouseholdByUserId(user);
	                model.addAttribute(APPLICANT_NAME, (household.getFirstName() + ' ' + household.getLastName()));
	            }else {
	                model.addAttribute(APPLICANT_NAME, (user.getFirstName() + ' ' + user.getLastName()));
	            }
				
				eSignatureURL = "entity/assister/esignature";
			}

			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			model.addAttribute(ASSISTER_ID, assisterId);
			model.addAttribute(ASSISTER, getAssisterById(Integer.valueOf(assisterId)));

			setCurrentDate(model);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured on eSignature page : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured on eSignature page in assisterESignature method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.debug("Redirecting to Assister E-signature page");

		LOGGER.info("assisterESignature: END");

		return eSignatureURL;
	}

	/**
	 * Designates the Assister with given Individual
	 *
	 * @param assisterId The assister's identifier.
	 * @param esignName The esignature name.
	 * @param assister The Assister instance.
	 * @param result The BindingResult instance.
	 * @param model The model instance.
	 * @param request The HttpServletRequest instance.
	 * @return String The view name.
	 * @throws GIException The GIException instance.
	 */
	//@GiAudit(transactionName = "Designate Assister", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/eSignature", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_USER_DESIGNATE_ASSISTER)
	public String designateAssister(@RequestParam(value = ID, required = false) String assisterId,
			@RequestParam(value = ESIGN_NAME, required = false) String esignName,
			HttpServletRequest request) throws GIException {

		LOGGER.info("designateAssister: START");

		DesignateAssister designateAssister = null;
		AccountUser currentUser = null;
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		try {
			
			if(EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
				String recordId = (String) request.getSession().getAttribute(RECORD_ID);
				LOGGER.info("Record ID : "+recordId);
				//TODO: Temporary code to track session info.
				if(null== recordId){
					LOGGER.info("Tracking invalid session -> "+request.getSession());
				}
				assisterRequestDTO.setIndividualId(Integer.valueOf(recordId));
			} else {
				currentUser = entityUtils.getLoggedInUser();

				if(null == currentUser) {
					LOGGER.error("No user found. User information is missing");
					throw new GIException("User information is missing");
				}
				
				String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
				String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
				boolean switchedRole=false;
				if(isUserSwitchToOtherView != null && isUserSwitchToOtherView.trim().equalsIgnoreCase("y")) {
					switchedRole=true;
				}
				if(currentUser.getActiveModuleName().equalsIgnoreCase( RoleService.INDIVIDUAL_ROLE)  && stateExchangeType.equalsIgnoreCase(INDIVIDUAL))
				{
					if(!switchedRole) {

						Household individual = getHouseholdByUserId(currentUser);
						assisterRequestDTO.setIndividualId(individual.getId());
					}
					else {
						assisterRequestDTO.setIndividualId(currentUser.getActiveModuleId());
					}
				}
			}
			
			assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);

			designateAssister = assisterDesignateResponseDTO.getDesignateAssister();

			if (designateAssister != null && !designateAssister.getStatus().toString().equals(DesignateAssister.Status.InActive.toString())) {
				//LOGGER.debug("Designate Assister not null. Redirecting to Message screen");
				LOGGER.info("designateAssister: END");

				return "forward:/entity/assister/designatemessage";
			}

			assisterRequestDTO.setId(Integer.valueOf(assisterId));
			assisterRequestDTO.setEsignName(esignName);

			LOGGER.info("Calling ghix-entity to Designate Assister");
			String requestXML = GhixUtils.xStreamHibernateXmlMashaling().toXML(assisterRequestDTO);
			String response = restClassCommunicator.designateAssister(requestXML);
			LOGGER.info(RETURN_FROM_REST_CALL);
			AssisterResponseDTO designateResponse = (AssisterResponseDTO) GhixUtils.xStreamHibernateXmlMashaling().fromXML(
					response);

			if(designateResponse!=null && !BrokerMgmtUtils.isEmpty(designateResponse.getStatus()) && EntityConstants.RESPONSE_SUCCESS.equalsIgnoreCase(designateResponse.getStatus())){
				return "forward:/entity/assister/designateconfirm?assisterName=" + designateResponse.getAssister().getFirstName() + " " + designateResponse.getAssister().getLastName();
			}
		}catch(NumberFormatException nfe){
			LOGGER.error("NumberFormatException occured while saving eSignature data : ", nfe);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving eSignature data : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving eSignature data in designateAssister method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("designateAssister: END");

		return "forward:/entity/assister/designationerror";
	}

	/**
	 * Error message is displayed if IND47 fails
	 *
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/entity/assister/designationerror", method= {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize(HAS_PERMISSION_MODEL_USER_DESIGNATE_ASSISTER)
	public String displayErrorPage(Model model, HttpServletRequest request) {
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Assister Designation");
		LOGGER.info("displayErrorPage: START ");

		LOGGER.info("displayErrorPage: END ");
		return "entity/assister/designationerror";
	}

	/**
	 * Displays confirmation screen after designation is done.
	 *
	 * @param model
	 *            Model object
	 * @param assisterName
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL string
	 */
	@RequestMapping(value = "/entity/assister/designateconfirm", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize(HAS_PERMISSION_MODEL_USER_DESIGNATE_ASSISTER)
	public String designateconfirm(@RequestParam(value = "assisterName", required = false) String assisterName,
			Model model) {

		LOGGER.info("designateconfirm: START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Assister Designation Confirmation");

		LOGGER.info("Inside Assister Designate Confirm");

		model.addAttribute("assisterName", assisterName);
		
		model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));

		LOGGER.info("designateconfirm: END");

		return "entity/assister/designateconfirm";
	}

	/**
	 * If individual have already been designated to an Assister then
	 * designation message is shown.
	 *
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/entity/assister/designatemessage", method = {RequestMethod.GET,RequestMethod.POST})
	@PreAuthorize(HAS_PERMISSION_MODEL_USER_DESIGNATE_ASSISTER)
	public String designatemessage(Model model) {

		LOGGER.info("designatemessage: START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Assister Designation");

		LOGGER.info("Inside Assister Designate Message");

		LOGGER.info("designatemessage: END");

		return "entity/assister/designatemessage";
	}

	/**
	 * Displays Individual Info.
	 *
	 * @param model
	 *            Model object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param encryptedId
	 * @return URL string
	 * @throws GIException
	 */
	@RequestMapping(value = "/entity/assister/individualinfo/{id}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String individualInfo(@PathVariable(ID) String encryptedId, Model model,
			HttpServletRequest request) throws GIException {

		LOGGER.info("individualInfo: START");

		Assister assisterObjFromUserId =null;
		DesignateAssister designateAssister=null;
		ConsumerComposite consumerComposite = null;
		Integer startRecord = 0;
		AssisterRequestDTO assisterRequestDTO = null;
		boolean isIndividualUserExists = true;
		try {
			int id = decryptId(encryptedId);

			AccountUser user = entityUtils.getLoggedInUser();
			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setModuleId(user.getId());
			AssisterResponseDTO assisterResponseDTOFromUserId = retrieveAssisterObjectByUserId(assisterRequestDTO);
			assisterObjFromUserId = assisterResponseDTOFromUserId.getAssister();
			if(EntityUtils.isAssisterCertified(assisterObjFromUserId)){
				throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
			}

			if(assisterObjFromUserId!= null){
				//get Designated assister from household id and assister id.
				assisterRequestDTO = new AssisterRequestDTO();
				assisterRequestDTO.setId(assisterObjFromUserId.getId());
				assisterRequestDTO.setIndividualId(id);
				assisterRequestDTO.setDesigStatus(DesignateAssister.Status.Active);

				AssisterDesignateResponseDTO assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
				designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
				
				if(designateAssister==null || DesignateAssister.Status.InActive.equals(designateAssister.getStatus())){
					return "redirect:/entity/assister/individuals?desigStatus=Active";
				}
			}

			consumerComposite = fetchConsumerCompositeInstance(assisterObjFromUserId, designateAssister, startRecord, id);
			long userId = consumerComposite.getConsumerUserId();

			if(userId==0){
				model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, false);
			}else{
				model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, true);
			}
			if(designateAssister == null) {
				return REDIRECT + getDashboardPage(request);
			}

			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individual Detail Page");
			model.addAttribute(EXTERNAL_INDIVIDUAL, consumerComposite);
			model.addAttribute(IS_CONSUMER_ACTIVATED, Boolean.toString(isAccountActivated(id,designateAssister.getAssisterId())));
			showIndividualView(model, id);

			LOGGER.info("individualInfo: END");

			request.getSession().setAttribute(LAST_VISITED_URL_BEFOR_SWITCH, "/entity/assister/individualinfo/"+encryptedId);
		}catch(AccessDeniedException accessDeniedException){
			LOGGER.error("Exception occured while accepting designation request for Employer / Individual : ", accessDeniedException);
			throw accessDeniedException;
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving individual information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving individual information in individualInfo method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "entity/assister/individualinfo";
	}

	/**
	 * Displays Individual Info.
	 *
	 * @param encryptedId The encrypted User identifier.
	 * @param assisterId The Assister identifier.
	 * @param individualId The individual identifier.
	 * @param model The Model instance.
	 * @param request The HttpServletRequest instance.
	 * @return <String> The view name.
	 */
	@RequestMapping(value = "/entity/assister/indsummaryinfo/{id}", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String getIndividualSummaryInfo(@PathVariable(ID) String encryptedId, Model model,
			HttpServletRequest request) {

		LOGGER.info("individualSummaryInfo: START");

		Assister assisterObjFromUserId =null;
		DesignateAssister designateAssister=null;
		ConsumerComposite consumerComposite = null;
		Integer startRecord = 0;
		AssisterRequestDTO assisterRequestDTO = null;

		try {
			int id = decryptId(encryptedId);

			AccountUser user = entityUtils.getLoggedInUser();
			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setModuleId(user.getId());
			AssisterResponseDTO assisterResponseDTOFromUserId = retrieveAssisterObjectByUserId(assisterRequestDTO);
			assisterObjFromUserId = assisterResponseDTOFromUserId.getAssister();

			if(assisterObjFromUserId!= null){
				//get Designated assister from household id and assister id.
				assisterRequestDTO = new AssisterRequestDTO();
				assisterRequestDTO.setId(assisterObjFromUserId.getId());
				assisterRequestDTO.setIndividualId(id);
				assisterRequestDTO.setDesigStatus(DesignateAssister.Status.Active);

				AssisterDesignateResponseDTO assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
				designateAssister = assisterDesignateResponseDTO.getDesignateAssister();

			}

			consumerComposite = fetchConsumerCompositeInstance(assisterObjFromUserId, designateAssister, startRecord, id);
			long userId = consumerComposite.getConsumerUserId();

			if(userId==0){
				model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, false);
			}else{
				model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, true);
			}
			if(designateAssister == null) {
				return REDIRECT + getDashboardPage(request);
			}

			model.addAttribute("priContactEmailAddress", consumerComposite.getEmailAddress());
			if(null != consumerComposite.getPhoneNumber() && !consumerComposite.getPhoneNumber().isEmpty()){
				model.addAttribute(PRI_CONTACT_PRIMARY_PHONE_NUMBER1,	consumerComposite.getPhoneNumber().substring(ZERO, THREE));
				model.addAttribute(PRI_CONTACT_PRIMARY_PHONE_NUMBER2, consumerComposite.getPhoneNumber().substring(THREE, SIX));
				model.addAttribute(PRI_CONTACT_PRIMARY_PHONE_NUMBER3, consumerComposite.getPhoneNumber().substring(SIX, TEN));
			}

			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individual Detail Page");
			model.addAttribute(EXTERNAL_INDIVIDUAL, consumerComposite);
			model.addAttribute(IS_CONSUMER_ACTIVATED, Boolean.toString(isAccountActivated(id,designateAssister.getAssisterId())));
			showIndividualView(model, id);

			LOGGER.info("individualSummaryInfo: END");

			request.getSession().setAttribute(LAST_VISITED_URL_BEFOR_SWITCH, "/entity/assister/individualinfo/"+encryptedId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving individual information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving individual information in individualInfo method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "entity/assister/indsummaryinfo";
	}

	/**
	 * Displays Individual Info.
	 *
	 * @param encryptedId The encrypted User identifier.
	 * @param model The Model instance.
	 * @param request The HttpServletRequest instance.
	 * @param priContactEmailAddress
	 * @param priContactPrimaryPhoneNumber1
	 * @param priContactPrimaryPhoneNumber2
	 * @param priContactPrimaryPhoneNumber3
	 * @return <String> The view name.
	 */
	@RequestMapping(value = "/entity/assister/indsummaryinfo/{id}", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String saveIndividualSummaryInfo(@PathVariable(ID) String encryptedId,
			Model model,
			@RequestParam(value = "priContactEmailAddress", required = false) String priContactEmailAddress,
			@RequestParam(value = PRI_CONTACT_PRIMARY_PHONE_NUMBER1, required = false) String priContactPrimaryPhoneNumber1,
			@RequestParam(value = PRI_CONTACT_PRIMARY_PHONE_NUMBER2, required = false) String priContactPrimaryPhoneNumber2,
			@RequestParam(value = PRI_CONTACT_PRIMARY_PHONE_NUMBER3, required = false) String priContactPrimaryPhoneNumber3,
			HttpServletRequest request) {

		LOGGER.info("individualSummaryInfo: START");

		Assister assister =null;
		int consumerId = 0;
		AssisterRequestDTO assisterRequestDTO = null;
		Household currIndividual =  null;
		Household household = null;
		ConsumerResponse individualResponse = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		try {
			consumerId = decryptId(encryptedId);
			AccountUser user = entityUtils.getLoggedInUser();

			Validate.notNull(user);

			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setModuleId(user.getId());
			AssisterResponseDTO assisterResponseDTOFromUserId = retrieveAssisterObjectByUserId(assisterRequestDTO);

			Validate.notNull(assisterResponseDTOFromUserId);

			assister = assisterResponseDTOFromUserId.getAssister();

			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, String.valueOf(consumerId), String.class);

			individualResponse = (ConsumerResponse) xstream.fromXML(response);

			Validate.notNull(individualResponse);

			currIndividual = individualResponse.getHousehold();

			Validate.notNull(currIndividual);

			validateIndividualInfo(request);

			currIndividual.setEmail(priContactEmailAddress);
			currIndividual.setPhoneNumber(priContactPrimaryPhoneNumber1+priContactPrimaryPhoneNumber2+priContactPrimaryPhoneNumber3);

			household = saveHouseHoldInformation(currIndividual);

			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individual Detail Page");
			model.addAttribute(EXTERNAL_INDIVIDUAL, household);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put(ASSISTER, assister);
			parameters.put("household", household);
			parameters.put("isAgent", false);

			brokerMgmtUtils.sendAccountActivationMail(parameters);

			LOGGER.info("individualSummaryInfo: END");

			request.getSession().setAttribute(LAST_VISITED_URL_BEFOR_SWITCH, "/entity/assister/individualinfo/"+encryptedId);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving individual information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving individual information in individualInfo method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "redirect:/entity/assister/individualinfo/"+ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(consumerId));
	}

	private void validateIndividualInfo(HttpServletRequest request) {

		LOGGER.info("Validate individual summary info: START");
		String email = request.getParameter("priContactEmailAddress");
		if(email == null || !email.matches("^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")){

			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}

		String phoneNumber = request.getParameter("priContactPrimaryPhoneNumber1")+request.getParameter("priContactPrimaryPhoneNumber2")+request.getParameter("priContactPrimaryPhoneNumber3");
		if(phoneNumber == null || !phoneNumber.matches("^[1-9]{1}[0-9]{9}$")){

			throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
		LOGGER.info("Validate individual summary info: End");
	}

	private Household saveHouseHoldInformation(Household houseHold) {
		LOGGER.info("retrieveHouseholdInstance : START");
		String response;
		Household householdAfterSave =null;
		try{
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(Household.class);
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(Household.class);
			LOGGER.debug("REST_CALL_TO_GET_HOUSEHOLE_ID_STARTS");
				response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.SAVE_HOUSEHOLD_RECORD, writer.writeValueAsString(houseHold), String.class);
				householdAfterSave=reader.readValue(response);

				LOGGER.debug("REST_CALL_TO_GET_HOUSEHOLE_ID_ENDS");

		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving individual information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving individual information in individualInfo method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("retrieveHouseholdInstance : END");
		return householdAfterSave;
	}

	private boolean isAccountActivated(Integer consumerId, Integer assisterId) {
		boolean isConsumerActivated =true;

		AccountActivation accountActivation =  accountActivationService.getAccountActivationByCreatedObjectId(consumerId, RoleService.INDIVIDUAL_ROLE, assisterId, GhixRole.ASSISTER.getRoleName());

		if(accountActivation == null || !accountActivation.getStatus().equals(AccountActivation.STATUS.PROCESSED))
		{
			isConsumerActivated = false;
		}
		return isConsumerActivated;
	}

	/**
	 * Handles the GET/POST request for sending activation link.
	 *
	 * @param id
	 * @param encryptedId
	 * @param responseText
	 * @throws InvalidUserException
	 * @throws GIException
	 */
	@RequestMapping(value = "/assister/individual/sendactivationlink/{id}", method = {RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public void  sendActivationLink(@PathVariable("id") String encryptedId, HttpServletResponse responseText) throws GIException{
		LOGGER.info("Sending Account Activation Link: START" );
		Assister assister =null;
		int consumerId = 0;
		AssisterRequestDTO assisterRequestDTO = null;
		Household currIndividual =  null;
		ConsumerResponse individualResponse = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String activationLinkResult = null;
		responseText.setContentType("text/plain");
		try{
			consumerId = decryptId(encryptedId);
			AccountUser user = entityUtils.getLoggedInUser();
			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setModuleId(user.getId());
			AssisterResponseDTO assisterResponseDTOFromUserId = retrieveAssisterObjectByUserId(assisterRequestDTO);
			assister = assisterResponseDTOFromUserId.getAssister();

			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, String.valueOf(consumerId), String.class);
			if(StringUtils.isNotBlank(response)) {
				individualResponse = (ConsumerResponse) xstream.fromXML(response);
				currIndividual = individualResponse.getHousehold();
			}

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put(ASSISTER, assister);
			parameters.put("household", currIndividual);
			parameters.put("isAgent", false);
			activationLinkResult = brokerMgmtUtils.sendAccountActivationMail(parameters);

			if (activationLinkResult != null) {
				LOGGER.info("Account activation link sent to the employer...");
				activationLinkResult = "SUCCESS";
			}else
			{
				activationLinkResult = "FAILURE";
			}
			responseText.getWriter().println(activationLinkResult);
	} catch(GIRuntimeException giexception){

		LOGGER.error("GIRuntimeException occured while saving individual information : ", giexception);

	} catch(Exception exception){
		LOGGER.error("Exception occured while saving individual information in individualInfo method : ", exception);

	}
	LOGGER.info("Sending Account Activation Link : END");

}


	/**
	 * @param assisterObjFromUserId
	 * @param designateAssister
	 * @param consumerComposite
	 * @param startRecord
	 * @param id
	 * @return
	 */
	private ConsumerComposite fetchConsumerCompositeInstance(
			Assister assisterObjFromUserId,
			DesignateAssister designateAssister,
			Integer startRecord, int id) {
		ConsumerComposite consumerComposite = new ConsumerComposite();

		if(designateAssister!= null && DesignateAssister.Status.Active.toString().equalsIgnoreCase(designateAssister.getStatus().toString())){
			Map<String, Object> map = new HashMap<>();

			Map<String, Object> searchCriteria = new HashMap<String, Object>();
			searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.ASSISTER);
			searchCriteria.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf(id));
			searchCriteria.put(BrokerConstants.MODULE_ID, assisterObjFromUserId.getId());
			searchCriteria.put(BrokerConstants.START_PAGE, startRecord);

			map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);

			List<ConsumerComposite> individualList = new ArrayList<ConsumerComposite>();
			Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);

			if(houseHoldMap!=null && !houseHoldMap.isEmpty()) {
				individualList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);

				if(!individualList.isEmpty()){
					consumerComposite = individualList.get(0);
				}
			}
		}
		return consumerComposite;
	}

	/**
	 * Method to decrypt encrypted identifier.
	 *
	 * @param encryptedId The encrypted identifier.
	 * @return decryptedId The decrypted identifier.
	 * @throws NumberFormatException The NumberFormatException instance.
	 */
	private int decryptId(String encryptedId) throws NumberFormatException {
		Validate.isTrue(StringUtils.isNotBlank(encryptedId));

		String strDecryptedId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedId);

		Validate.isTrue(StringUtils.isNotBlank(strDecryptedId));

		Integer integerDecryptedId = Integer.valueOf(strDecryptedId);

		Validate.notNull(integerDecryptedId);

		return integerDecryptedId.intValue();
	}

	/**
	 *  Method to retrieve Assister object by User Identifier.
	 *
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterResponseDTOFromUserId The AssisterResponseDTO instance.
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private AssisterResponseDTO retrieveAssisterObjectByUserId(AssisterRequestDTO assisterRequestDTO) throws JsonProcessingException, IOException {
		//LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS_ASSISTER_REQUEST_DTO_IN_REQUEST + assisterRequestDTO);
		// get enrollment entity detail
		String assisterDetailsResponse;
		
		if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
			assisterDetailsResponse = restClassCommunicator.retrieveAssisterRecordByUserId(assisterRequestDTO);
		} else {
			assisterDetailsResponse = restClassCommunicator.retrieveAssisterObjectByUserId(assisterRequestDTO);
		}
		AssisterResponseDTO assisterResponseDTOFromUserId = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		//LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS_RESPONSE_RECEIVED + assisterResponseDTOFromUserId);
		return assisterResponseDTOFromUserId;
	}


	/**
	 * Retrieves individual comments.
	 *
	 * @param idc
	 * @param model
	 * @param request
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/entity/assister/individualcomments/{id}", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String getIndividualComments(@PathVariable(ID) String idc, Model model, HttpServletRequest request) {

		LOGGER.info("getIndividualComments: START");

		LOGGER.info("individual comments page ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individuals Comments");
		LOGGER.info("Inside Individual Detail Page");

		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		AssisterDesignateResponseDTO assisterDesignateResponseDTO;
		DesignateAssister designateAssister=null;
		ConsumerComposite consumerComposite = null;
		Integer startRecord = 0;
		try {
			int id = decryptId(idc);
			assisterRequestDTO.setIndividualId(id);
			assisterRequestDTO.setDesigStatus(DesignateAssister.Status.Active);

			assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
			designateAssister = assisterDesignateResponseDTO.getDesignateAssister();

			AccountUser user = entityUtils.getLoggedInUser();
			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setModuleId(user.getId());
			AssisterResponseDTO assisterResponseDTOFromUserId = retrieveAssisterObjectByUserId(assisterRequestDTO);
			Assister assisterObjFromUserId = assisterResponseDTOFromUserId.getAssister();

			//houseHold = retrieveHouseholdInstance(designateAssister, houseHold, id);
			consumerComposite = fetchConsumerCompositeInstance(assisterObjFromUserId, designateAssister, startRecord, id);
			long userId = consumerComposite.getConsumerUserId();

			if(userId==0){
				model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, false);
			}else{
				model.addAttribute(IS_INDIVIDUAL_USER_EXISTS, true);
			}
			if(designateAssister == null ||
					(designateAssister!= null && DesignateAssister.Status.InActive.toString().equalsIgnoreCase(designateAssister.getStatus().toString()))){
				return "redirect:" + getDashboardPage(request);
			}
			model.addAttribute(EXTERNAL_INDIVIDUAL, consumerComposite);
			model.addAttribute(IS_CONSUMER_ACTIVATED, Boolean.toString(isAccountActivated(id,designateAssister.getAssisterId())));
			// call for IND66
			showIndividualView(model, id);

			LOGGER.info("getIndividualComments: END");

			request.getSession().setAttribute(LAST_VISITED_URL_BEFOR_SWITCH, "/entity/assister/individualcomments/"+idc);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving individual comments : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving individual comments in getIndividualComments method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "entity/assister/individualcomments";
	}

	/**
	 * @param designateAssister
	 * @param houseHold
	 * @param id
	 * @return
	 * @throws RestClientException
	 */
	/*private Household retrieveHouseholdInstance(
			DesignateAssister designateAssister, Household houseHold, int id)
			throws RestClientException {
		String response;
		if(designateAssister!= null && DesignateAssister.Status.Active.toString().equalsIgnoreCase(designateAssister.getStatus().toString())){
			LOGGER.debug("Rest call to get householeId starts");
			//get the household using rest call
			response = ghixRestTemplate.postForObject( GhixEndPoints.CONSUMER_SERVICE_URL+"consumer/findHouseholdById", String.valueOf(id), String.class);
			if(response != null){
				ConsumerResponse consumerResponse=(ConsumerResponse)xstream.fromXML(response);
				houseHold = consumerResponse.getHousehold();
			}
			LOGGER.debug("rest call to get householeId ends");
		}
		return houseHold;
	}*/

	private String getDashboardPage(HttpServletRequest request) {
		String roleName=(String) request.getSession().getAttribute("switchToModuleName");
		Role activeRole = roleService.findRoleByName(StringUtils.upperCase( roleName));
		/*if("ASSISTER".equalsIgnoreCase(StringUtils.upperCase(roleName))){
			//request.getSession().removeAttribute(LAST_VISITED_URL);
			//This if has been added to keep last_visited functionality of Agent switch to employer.
		}*/
		//return activeRole.getLandingPage();
		return activeRole.getLandingPage();
	}
	/**
	 * Retrieve {@link DesignateAssister} based on Individual Id to show individual view in IND66
	 *
	 * @param model
	 * @param designId
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	private void showIndividualView(Model model, Integer desigId) throws JsonProcessingException, IOException {

		LOGGER.info("showIndividualView: START");

		LOGGER.info("inside show individual view");
		DesignateAssister designateAssister = null;
		String showSwitchRolePopup = Y;

		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setId(desigId);

		LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
		String assisterDetailsResponse = restClassCommunicator
				.retrieveAssisterDesignationDetailsForRequest(assisterRequestDTO);
		AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);
		designateAssister = assisterResponseDTO.getDesignateAssister();

		if(designateAssister != null) {
			model.addAttribute(STATUS, designateAssister.getStatus());
			showSwitchRolePopup = designateAssister.getShow_switch_role_popup().toString();
			model.addAttribute(ASSISTER_ID, designateAssister.getAssisterId());

			//model.addAttribute(BrokerConstants.ENCRYPTED_ASSISTER_ID, ghixJasyptUtil.encryptString(String.valueOf(designateAssister.getAssisterId())));
			//model.addAttribute(BrokerConstants.ENCRYPTED_EXT_INDIVIDUAL_ID, ghixJasyptUtil.encryptString(String.valueOf(designateAssister.getIndividualId())));
		}
		model.addAttribute(SHOW_SWITCH_ROLE_POPUP, showSwitchRolePopup);

//		Fetch switch account url from configuration
		String ahbxSwitchAccountUrl = GhixConstants.SWITCH_URL_IND66;
		model.addAttribute(SWITCH_ACC_URL, ahbxSwitchAccountUrl);

		LOGGER.info("showIndividualView: END");
	}

	/**
	 *
	 * @param targetId
	 * @param targetName
	 * @param individualName
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/assister/newcomment", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ASSISTER_MANAGE_INDIVIDUAL')")
	public String addNewComment(@RequestParam(value = "target_id", required = false) String encTargetId,
			@RequestParam(value = "target_name", required = false) String targetName, Model model) {

		LOGGER.info("addNewComment: START");
		
		Integer targetId = null;
		try {
			String strTargetId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encTargetId);
			targetId = Integer.parseInt(strTargetId);
		} catch(GIRuntimeException giexception) {
			LOGGER.error("Cannot edit Broker's Certification Status : ", giexception);
			throw giexception;
		} catch(Exception exception) {
			LOGGER.error("Cannot edit Broker's Certification Status : ", exception);			
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}    

		LOGGER.info("individual add new comment popup");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange:Individual Add Comment Page");
		model.addAttribute("target_id", targetId);
		model.addAttribute("target_name", targetName);

		LOGGER.info("addNewComment: END");

		return "entity/assister/addnewcomment";
	}

	private void setEnrollmentEntityDetailAndAssisters(Model model, Integer entityId, Integer siteId) throws IOException {

		LOGGER.info("setEnrollmentEntityDetailAndAssisters: START");

		EnrollmentEntity enrollmentEntity = new EnrollmentEntity();
		enrollmentEntity.setId(entityId);
		Site site = new Site();
		site.setId(siteId);

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
		enrollmentEntityRequestDTO.setSite(site);

		LOGGER.info("Calling ghix-entity to retrieve Enrollment Entity Detail");
		String request = JacksonUtils.getJacksonObjectWriter(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);
		String response = restClassCommunicator.retrieveEnrollmentEntityDetailByID(request);
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReader(EnrollmentEntityResponseDTO.class).readValue(response);
		LOGGER.debug(RETURN_FROM_REST_CALL + enrollmentEntityResponseDTO);

		LOGGER.info("Calling ghix-entity to retrieve list of Assisters for Enrollment Entity");
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setEnrollmentEntityId(entityId);
		assisterRequestDTO.setSiteId(siteId);
		String assisterRequest = JacksonUtils.getJacksonObjectWriter(AssisterRequestDTO.class).writeValueAsString(assisterRequestDTO);

		String assisterResponse = restClassCommunicator.retrieveNoOfAssistersByEnrollmentEntityAndSite(assisterRequest);
		AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterResponse);

		LOGGER.debug(RETURN_FROM_REST_CALL + assisterResponseDTO);

		//Added to word wrap language Problem
		if(enrollmentEntityResponseDTO.getEnrollEntityAssisterSearchResult() != null)
		{
			String languageString =  enrollmentEntityResponseDTO.getEnrollEntityAssisterSearchResult().getSiteLanguages().getSpokenLanguages();
			enrollmentEntityResponseDTO.getEnrollEntityAssisterSearchResult().getSiteLanguages().setSpokenLanguages(languageWordWrap(languageString));
			languageString =  enrollmentEntityResponseDTO.getEnrollEntityAssisterSearchResult().getSiteLanguages().getWrittenLanguages();
			enrollmentEntityResponseDTO.getEnrollEntityAssisterSearchResult().getSiteLanguages().setWrittenLanguages(languageWordWrap(languageString));
		}
		model.addAttribute(ENROLLMENTENTITY_DETAIL, enrollmentEntityResponseDTO.getEnrollEntityAssisterSearchResult());
		model.addAttribute(NO_OF_ASSISTERS, assisterResponseDTO.getNoOfAssistersForEEAndSite());

		LOGGER.info("setEnrollmentEntityDetailAndAssisters: END");
	}

	private String languageWordWrap(String languageString){
		String newString = null;
		if(languageString != null){
			newString = "";
			String[] splittedString = languageString.split(",");

			for(int i= 0; i<splittedString.length;i++){
				if(!splittedString[i].equals("null")){
					if(i == 0){
						newString += splittedString[i];
					}else{
						newString += "," + splittedString[i];
					}
				}

			}
			newString = newString.replaceAll(",", ", ");
		}
		return newString;

	}

	private Assister getAssisterById(Integer assisterId) {

		LOGGER.info("getAssisterById: START");

		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setId(assisterId);

		AssisterResponseDTO assisterResponseDTO = getAssisterDetail(assisterRequestDTO);

		LOGGER.info("getAssisterById: END");

		return assisterResponseDTO.getAssister();
	}

	private void setCurrentDate(Model model) {

		LOGGER.info("setCurrentDate: START");

		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		Date date = new TSDate();
		String str = dateFormat.format(date);

		String[] dates = str.split("/");

		model.addAttribute(MONTH, dates[0]);
		model.addAttribute(DAY, dates[1]);
		model.addAttribute(YEAR, dates[TWO]);

		LOGGER.info("setCurrentDate: END");
	}

	/**
	 * Retrieves list of designated individuals
	 *
	 * @param desigStatus
	 *            Status of designated individual to search for
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/entity/assister/individuals", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String getAssisterIndividuals(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus,
			Model model, HttpServletRequest request) {

		LOGGER.info("getAssisterIndividuals: START");
		String param = request.getParameter(PAGE_NUMBER);//Page number variable for paging state.
		Integer startRecord = 0;//Sr. No of record to be shown on view. Default is 0.
		int currentPage = 1;//Current page. Comes from request in PAGE_NUMBER. Default is 1.
		String defaultDesigStatus = DesignateAssister.Status.Pending.toString();//Default pending designation.
		int iResultCount = -1;
		List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
		Integer iResultCt = null;
		String tableTitle = null;

		request.setAttribute(PAGE_SIZE_ATTR, PAGE_SIZE);
		request.setAttribute(REQ_URI, INDIVIDUALS);

		//Initiate startRecord and currentPage using request parameter.
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * PAGE_SIZE;
			currentPage = Integer.parseInt(param);
		}

		BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO = new BrokerBOBSearchParamsDTO();
		
		try {
			Assister assisterObj ;
			Map<String, Object> searchCriteria;
			AccountUser user = entityUtils.getLoggedInUser();
			
			LOGGER.info("Assister User ID : "+user.getId());

			//Get assister object from logged in user. OR
			//Get assister from switched user
			AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setModuleId(user.getId());

			AssisterResponseDTO assisterResponseDTO = retrieveAssisterObjectByUserId(assisterRequestDTO);
			assisterObj = assisterResponseDTO.getAssister();

			if(EntityUtils.isAssisterCertified(assisterObj)){
				throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
			}

			//From retrieved assister object get designated household to this assister.

			Map<String, Object> map = new HashMap<>();
			searchCriteria = this.createSearchCriteriaForIndividuals(request, assisterObj.getId(), desigStatus, brokerBOBSearchParamsDTO);
			String previousSortBy = request.getParameter("previousSortBy");
			String previousSortOrder = request.getParameter("previousSortOrder");
			boolean isSortChanged = false;

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				List<Long> individualsId = null;
				brokerBOBSearchParamsDTO.setDesignationStatus(desigStatus);
				brokerBOBSearchParamsDTO.setPageNumber(currentPage);
				List<Long> nextIndividualIds = null; 
				//sort IND72 feature
				if(brokerBOBSearchParamsDTO !=null && previousSortBy == null && previousSortOrder == null){
					previousSortBy = brokerBOBSearchParamsDTO.getSortBy();
					previousSortOrder = brokerBOBSearchParamsDTO.getSortOder();
				}else if(brokerBOBSearchParamsDTO!=null && brokerBOBSearchParamsDTO.getSortBy() != null && brokerBOBSearchParamsDTO.getSortOder() != null 
						&&  !brokerBOBSearchParamsDTO.getSortBy().equalsIgnoreCase(previousSortBy) &&  !brokerBOBSearchParamsDTO.getSortOder().equalsIgnoreCase(previousSortOrder)){
					isSortChanged = true;
					currentPage = 1;
					brokerBOBSearchParamsDTO.setPageNumber(currentPage);
				}
				
				if((currentPage%PAGE_SIZE)!=1 && request.getParameterValues("individualsIdAtClient") !=null && request.getParameter("landingFromPrev") == null){
					nextIndividualIds = new ArrayList<Long>();
					individualsId= new ArrayList<Long>();
					for(String tmpStrId:request.getParameterValues("individualsIdAtClient")){
						individualsId.add(Long.parseLong(tmpStrId));
					}
					iResultCount = request.getParameter("totalCountAtClient")!= null ? !request.getParameter("totalCountAtClient").isEmpty()? Integer.parseInt(request.getParameter("totalCountAtClient")) :0:0;
					if(individualsId.size() > 0 && !isSortChanged){
						int size = individualsId.size();
						if(size <= PAGE_SIZE){
							nextIndividualIds = individualsId;
						} else if(size > ((currentPage-1)%PAGE_SIZE)  *PAGE_SIZE){
							nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, ((currentPage-1)%PAGE_SIZE) * PAGE_SIZE);
						}else{
							nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, individualsId.size());
						}
						brokerBOBSearchParamsDTO.setIndividualIds(nextIndividualIds);	
					}
					
				}else if(request.getParameter("landingFromPrev") != null && "true".equalsIgnoreCase( request.getParameter("landingFromPrev") )){
					brokerBOBSearchParamsDTO.setPageNumber(currentPage-PAGE_SIZE+1);
				}
				BrokerBobResponseDTO brokerBobResponseDTO =	brokerIndTriggerService.triggerInd72(brokerBOBSearchParamsDTO);
				
				if(request.getParameter("landingFromPrev") != null && "true".equalsIgnoreCase( request.getParameter("landingFromPrev") ) && brokerBobResponseDTO!=null &&
						brokerBobResponseDTO.getIndividualsId() !=null && brokerBobResponseDTO.getIndividualsId().size() > 0){
					
					brokerBOBSearchParamsDTO.setPageNumber(currentPage);
					 
					individualsId = brokerBobResponseDTO.getIndividualsId();
					iResultCount = brokerBobResponseDTO.getCount();
					int size = individualsId.size();
					if(size <= PAGE_SIZE){
						nextIndividualIds = individualsId;
					} else if(size > ((currentPage-1)%PAGE_SIZE)  *PAGE_SIZE){
						nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, ((currentPage-1)%PAGE_SIZE) * PAGE_SIZE);
					}else{
						nextIndividualIds = individualsId.subList( ((currentPage-2)%PAGE_SIZE) * PAGE_SIZE, individualsId.size());
					}
					 
					brokerBOBSearchParamsDTO.setIndividualIds(nextIndividualIds);
					
					brokerBobResponseDTO =	brokerIndTriggerService.triggerInd72(brokerBOBSearchParamsDTO);
				}
				
				if(brokerBobResponseDTO!=null){
					List<BrokerBOBDetailsDTO> brokerBOBDetailsDTO = brokerBobResponseDTO.getBrokerBobDetailsDTO();
					if( brokerBobResponseDTO.getIndividualsId() !=null && brokerBobResponseDTO.getIndividualsId().size() > 0){
						individualsId = brokerBobResponseDTO.getIndividualsId();
					}
					
					if((currentPage%PAGE_SIZE)==1){
						iResultCount = brokerBobResponseDTO.getCount();
					}
					
					if(null != individualsId && individualsId.size() > 0){
						model.addAttribute("individualsId", individualsId);
					}
					
					houseHoldObjList = getConsumerCompositList(brokerBOBDetailsDTO) ;
					
				}
			} else {
				searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
				searchCriteria.put(BrokerConstants.STATUS, desigStatus);
				map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);

				Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);

				if(houseHoldMap!=null && !houseHoldMap.isEmpty()){
					houseHoldObjList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
					iResultCt = (Integer) Integer.parseInt((String)houseHoldMap.get(BrokerConstants.TOTAL_RECORD_COUNT));
				}

				if (iResultCt != null) {
					iResultCount = iResultCt.intValue();
				}
			}

			tableTitle = populateTabTitle(iResultCount);

			// Set in model
			model.addAttribute("previousSortOrder", previousSortOrder);
			model.addAttribute("previousSortBy", previousSortBy);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			model.addAttribute("ID_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE));
			model.addAttribute("NV_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE));
			
			model.addAttribute(BrokerConstants.TABLE_TITLE, tableTitle);
			model.addAttribute(RESULT_SIZE, iResultCount);
			model.addAttribute(INDIVIDUAL_LIST,houseHoldObjList);
			model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individuals");
			model.addAttribute(BrokerConstants.ELIGIBILITY_STATUS_LIST,  EligibilityStatus.values());
			model.addAttribute(BrokerConstants.APPLICATION_STATUS_LIST,  ApplicationStatus.values());
			model.addAttribute(BrokerConstants.SORT_ORDER, (String) searchCriteria.get(BrokerConstants.SORT_ORDER));
			model.addAttribute(SEARCH_CRITERIA, searchCriteria);
			// setting designation status to pending if it's null/empty
			model.addAttribute(DESIG_STATUS, (null == desigStatus || desigStatus.isEmpty()? defaultDesigStatus:desigStatus));
			model.addAttribute(CURRENT_PAGE, currentPage);
			assisterNavigationMap(request, assisterObj);

		}catch(AccessDeniedException accessDeniedException){
			LOGGER.error("Exception occured while accepting designation request for Employer / Individual : ", accessDeniedException);
			throw accessDeniedException;
		}catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving individuals : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving individuals in getAssisterIndividuals method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getAssisterIndividuals: END");

		return "entity/assister/individuals";
	}
	
	private List<ConsumerComposite> getConsumerCompositList(List<BrokerBOBDetailsDTO> brokerBOBDetailsDTO) {
		List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");//BrokerConstants.DATE_FORMAT  TODO: Need to get confirmation from AHBX about date format.
		ConsumerComposite tmpConsumerComposite=null;
		
		if(brokerBOBDetailsDTO!=null){
			for(BrokerBOBDetailsDTO tmpBrokerBOBDetailsDTO:brokerBOBDetailsDTO){
				tmpConsumerComposite= new ConsumerComposite();
				tmpConsumerComposite.setId(tmpBrokerBOBDetailsDTO.getIndividualId());
				tmpConsumerComposite.setCecId(tmpBrokerBOBDetailsDTO.getBrokerId());
				tmpConsumerComposite.setFirstName(tmpBrokerBOBDetailsDTO.getFirstName());
				tmpConsumerComposite.setLastName(tmpBrokerBOBDetailsDTO.getLastName());
				tmpConsumerComposite.setCecFirstName(tmpBrokerBOBDetailsDTO.getEcFirstName()!=null?tmpBrokerBOBDetailsDTO.getEcFirstName():"");
				tmpConsumerComposite.setCecLastName(tmpBrokerBOBDetailsDTO.getEcLastName()!=null?tmpBrokerBOBDetailsDTO.getEcLastName():"");
				tmpConsumerComposite.setEmailAddress(tmpBrokerBOBDetailsDTO.getEmailAddress());
				tmpConsumerComposite.setPhoneNumber(tmpBrokerBOBDetailsDTO.getPhoneNumber());
				
				tmpConsumerComposite.setAddress1(tmpBrokerBOBDetailsDTO.getAddress1());
				tmpConsumerComposite.setAddress2(tmpBrokerBOBDetailsDTO.getAddress2());
				tmpConsumerComposite.setCity(tmpBrokerBOBDetailsDTO.getCity());
				tmpConsumerComposite.setState(tmpBrokerBOBDetailsDTO.getState());
				tmpConsumerComposite.setZip(tmpBrokerBOBDetailsDTO.getZipCode());
				 
				tmpConsumerComposite.setApplicationStatus(tmpBrokerBOBDetailsDTO.getApplicationStatus());
				tmpConsumerComposite.setEligibilityStatus(tmpBrokerBOBDetailsDTO.getEligibilityStatus());
				
				tmpConsumerComposite.setFamilySize(tmpBrokerBOBDetailsDTO.getNoOfHouseholdMembers()); 
				
				if(null != tmpBrokerBOBDetailsDTO.getRequestSentDt() && !tmpBrokerBOBDetailsDTO.getRequestSentDt().isEmpty()){
					try {
						tmpConsumerComposite.setRequestSent(  sdf.parse(tmpBrokerBOBDetailsDTO.getRequestSentDt())  );//??? What will be date format so we can parse it to Date object
					} catch (ParseException e) {
						LOGGER.error("Exception occured while parsing RequestSentDt from IND72 response: ", e);
					} 				
				}
				
				if(null!= tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt() && !tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt().isEmpty() ){
					try {
						tmpConsumerComposite.setInActiveSince(sdf.parse(tmpBrokerBOBDetailsDTO.getInactiveSinceFrmDt()));//??? What will be date format so we can parse it to Date object
					} catch (ParseException e) {
						LOGGER.error("Exception occured while parsing InActiveSince from IND72 response: ", e);
					}
				}
				
				houseHoldObjList.add(tmpConsumerComposite);
			}
		}
		
		return houseHoldObjList;
	}

	/**
	 * @param iResultCount
	 * @return
	 */
	private String populateTabTitle(int iResultCount) {
		String tableTitle;
		if(iResultCount >1){
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				tableTitle = BrokerConstants.REQUESTS;
			}else{
				tableTitle = BrokerConstants.INDIVIDUALS;
			}
		}
		else{
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				tableTitle = BrokerConstants.REQUEST;
			}else{
				tableTitle = BrokerConstants.INDIVIDUAL;
			}
		}
		return tableTitle;
	}

	/**
	 * Method to determine the Sort Order for Criteria.
	 *
	 * @param request The HttpServletRequest instance.
	 * @return sortOrder The sort order value.
	 */
	private String determineSortOrderForCriteria(HttpServletRequest request) {
		String sortOrder =  (request.getParameter(BrokerConstants.SORT_ORDER) == null) ?  BrokerConstants.ASC : request.getParameter(BrokerConstants.SORT_ORDER);
		sortOrder = ("".equals(sortOrder)) ? BrokerConstants.ASC : sortOrder;
		String changeOrder =  (request.getParameter(BrokerConstants.CHANGE_ORDER) == null) ?  FALSE : request.getParameter(BrokerConstants.CHANGE_ORDER);
		sortOrder = (changeOrder.equalsIgnoreCase(TRUE) ) ? (sortOrder.equals(BrokerConstants.DESC) ? BrokerConstants.ASC : BrokerConstants.DESC ): sortOrder;
		return sortOrder;
	}

	/**
	 * Method to get Individual list for Assister.
	 *
	 * @param assisterRequestNewDTO The AssisterRequestDTO instance.
	 * @return assisterResponseDTO The AssisterResponseDTO instance.
	 */
/*	private AssisterResponseDTO getIndividualListForAssister(AssisterRequestDTO assisterRequestNewDTO) {
		AssisterResponseDTO assisterResponseDTO;
		// Get Individual List
		String individualListAndRecordCountStr = restClassCommunicator.getIndividualListForAssister(assisterRequestNewDTO);
		assisterResponseDTO = (AssisterResponseDTO) GhixUtils.xStreamHibernateXmlMashaling().fromXML(individualListAndRecordCountStr);
		return assisterResponseDTO;
	}*/

	/**
	 * Retrieves list of designated individuals
	 *
	 * @param desigStatus
	 * @param model
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/entity/assister/resetall", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String resetIndividuals(@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model) {

		LOGGER.info("resetIndividuals: START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individuals");

		LOGGER.info("resetIndividuals: END");

		return REDIRECT_ENTITY_ASSISTER_INDIVIDUALS_DESIG_STATUS + desigStatus;
	}

	/**
	 * Creates search criteria for Individuals
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param id
	 *            broker id
	 * @return {@link Map} of search parameter with value
	 */
	private Map<String, Object> createSearchCriteriaForIndividuals(HttpServletRequest request, int id, String desigStatus, BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {

		LOGGER.info("LocateAssisterController.createSearchCriteriaForIndividuals : START");

		Map<String, Object> searchCriteria = new HashMap<String, Object>();

		String sortBy = null;

		if(desigStatus!=null){
				if(desigStatus.equalsIgnoreCase(DesignateAssister.Status.Active.toString())){
					sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
				}
				else if(desigStatus.equalsIgnoreCase(DesignateAssister.Status.Pending.toString())){
					sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.REQUESTSENT : request.getParameter(BrokerConstants.SORT_BY);
				}
				else if(desigStatus.equalsIgnoreCase(DesignateAssister.Status.InActive.toString())){
					sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.INACTIVESINCE : request.getParameter(BrokerConstants.SORT_BY);
				}
				else{
					sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
				}
		}
		else{
			LOGGER.info("LocateAssisterController.createSearchCriteriaForIndividuals - Designation Status is Null.");
			sortBy =  (request.getParameter(BrokerConstants.SORT_BY) == null) ?  BrokerConstants.FIRST_NAME : request.getParameter(BrokerConstants.SORT_BY);
		}
		String sortOrder = determineSortOrderForCriteria(request);

		searchCriteria.put(BrokerConstants.SORT_BY, sortBy);
		searchCriteria.put(BrokerConstants.SORT_ORDER, sortOrder);
		brokerBOBSearchParamsDTO.setSortBy(sortBy);
		brokerBOBSearchParamsDTO.setSortOder(sortOrder);

		String assisterId = String.valueOf(id);
		searchCriteria.put(BrokerConstants.MODULE_NAME, BrokerConstants.ASSISTER);
		brokerBOBSearchParamsDTO.setRecordType(BrokerConstants.ASSISTER);

		String firstName = (request.getParameter(FIRSTNAME) == null) ? "" : request.getParameter(FIRSTNAME);
		String lastName = (request.getParameter(LASTNAME) == null) ? "" : request.getParameter(LASTNAME);
		String eligiblityStatus = (request.getParameter(BrokerConstants.ELIGIBILITY_STATUS) == null) ? "" : request.getParameter(BrokerConstants.ELIGIBILITY_STATUS);
		String applicationStatus = (request.getParameter(BrokerConstants.APPLICATION_STATUS) == null) ? "" : request.getParameter(BrokerConstants.APPLICATION_STATUS);

		if (!"".equals(assisterId)) {
			searchCriteria.put(BrokerConstants.MODULE_ID, assisterId);
			brokerBOBSearchParamsDTO.setRecordId(Long.parseLong(assisterId));
		}
		if (!"".equals(firstName)) {
			searchCriteria.put(FIRSTNAME, firstName);
			brokerBOBSearchParamsDTO.setFirstName(firstName);
		}
		if (!"".equals(lastName)) {
			searchCriteria.put(LASTNAME, lastName);
			brokerBOBSearchParamsDTO.setLastName(lastName);
		}
		if(!"".equals(eligiblityStatus)){
			searchCriteria.put(BrokerConstants.ELIGIBILITY_STATUS, eligiblityStatus);
			brokerBOBSearchParamsDTO.setEligibilityStatus(eligiblityStatus);
		}
		if(!"".equals(applicationStatus)){
			searchCriteria.put(BrokerConstants.APPLICATION_STATUS, applicationStatus);
			brokerBOBSearchParamsDTO.setCurrentStatus(applicationStatus);
		}
		setDates(searchCriteria, request, brokerBOBSearchParamsDTO);

		LOGGER.info("LocateAssisterController.createSearchCriteriaForIndividuals : END");

		return searchCriteria;
	}

	/**
	 * Sets dates in search criteria
	 *
	 * @param searchCriteria
	 *            {@link Map} of search criteria
	 * @param request
	 *            {@link HttpServletRequest}
	 */
	private void setDates(Map<String, Object> searchCriteria, HttpServletRequest request, BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {

		LOGGER.info("setDates : START");

		String requestSentFrom = (request.getParameter(BrokerConstants.REQUEST_SENT_FROM) == null) ? "" : request
				.getParameter(BrokerConstants.REQUEST_SENT_FROM);
		String requestSentTo = (request.getParameter(BrokerConstants.REQUEST_SENT_TO) == null) ? "" : request
				.getParameter(BrokerConstants.REQUEST_SENT_TO);
		String inactiveDateFrom = (request.getParameter(BrokerConstants.INACTIVE_DATE_FROM) == null) ? "" : request
				.getParameter(BrokerConstants.INACTIVE_DATE_FROM);
		String inactiveDateTo = (request.getParameter(BrokerConstants.INACTIVE_DATE_TO) == null) ? "" : request
				.getParameter(BrokerConstants.INACTIVE_DATE_TO);

		if (!"".equals(requestSentFrom)) {
			searchCriteria.put(BrokerConstants.REQUEST_SENT_FROM, requestSentFrom);
			brokerBOBSearchParamsDTO.setRequestSentFrmDt(requestSentFrom);
		}
		if (!"".equals(requestSentTo)) {
			searchCriteria.put(BrokerConstants.REQUEST_SENT_TO, requestSentTo);
			brokerBOBSearchParamsDTO.setRequestSentToDt(requestSentTo);
		}
		if (!"".equals(inactiveDateFrom)) {
			searchCriteria.put(BrokerConstants.INACTIVE_DATE_FROM, inactiveDateFrom);
			brokerBOBSearchParamsDTO.setInactiveSinceFrmDt(inactiveDateFrom);
		}
		if (!"".equals(inactiveDateTo)) {
			searchCriteria.put(BrokerConstants.INACTIVE_DATE_TO, inactiveDateTo);
			brokerBOBSearchParamsDTO.setInactiveSinceToDt(inactiveDateTo);
		}

		LOGGER.info("setDates : END");
	}

	/**
	 * Handles the accept designation request for Employer / Individual
	 *
	 * @param encDesigId
	 * @param model
	 * @param request
	 * @param prevStatus
	 *            Status page on which the control is
	 * @return URL for navigation to appropriate result page
	 */
	//@GiAudit(transactionName = "Assister Approve User", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/approve/{id}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ASSISTER_APPROVE_USER')")
	public String approve(@PathVariable("id") String encDesigId, Model model,
			@RequestParam(value = PREV_STATUS, required = false) String prevStatus, HttpServletRequest request) {

		LOGGER.info("approve: START");

		AssisterResponseDTO assisterResponseDTO = null;
		String assisterDetailsResponse = null;
		AccountUser user = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

		try {

			user = entityUtils.getLoggedInUser();
			int desigId= Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encDesigId));
			assisterRequestDTO.setId(desigId);

			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
			assisterDetailsResponse = restClassCommunicator
					.retrieveAssisterDesignationDetailsForRequest(assisterRequestDTO);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);

			Validate.notNull(assisterResponseDTO);

			DesignateAssister designateAssister = assisterResponseDTO.getDesignateAssister();

			if (designateAssister != null) {
				designateAssister.setStatus(Status.Active);
				assisterRequestDTO.setDesignateAssister(designateAssister);
				assisterDetailsResponse = restClassCommunicator.saveAssisterDesignation(assisterRequestDTO);
				
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
					String contextPath=request.getContextPath();
					sendApprovedMailNotification(designateAssister.getAssisterId(), assisterDetailsResponse,contextPath,user.getActiveModuleName());
				}
			}

			request.setAttribute(DESIG_STATUS, prevStatus);
			model.addAttribute(DESIG_STATUS, prevStatus);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while appoving individual request : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while appoving individual request : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("approve: END");

		return "redirect:/entity/assister/individuals";
	}

	/**
	 *
	 * Method for Entity to accept his/her assister's designation request of user.
	 *
	 * @param encAssisterId The encrypted assister identifier.
	 * @param encDesigId The encrypted designation identifier.
	 * @param prevStatus The previous designation status.
	 * @param desigStatus The designation status.
	 * @param model Then Model instance.
	 * @param request The HttpServletRequest instance.
	 * @return String The view name
	 *
	 */
	//@GiAudit(transactionName = "Entity Approve User", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/approve/{assisterId}/{designateId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ENTITY_ASSISTER_APPROVE_USER')")
	public String entityApprove(@PathVariable("assisterId") String encAssisterId,
			@PathVariable("designateId") String encDesigId,
			@RequestParam(value = PREV_STATUS, required = false) String prevStatus,
			Model model, HttpServletRequest request) {

		LOGGER.info("entityApprove: START");

		String assisterDetailsResponse = null;
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

		try {
			Integer assisterId = encAssisterId!=null && !encAssisterId.isEmpty() ?Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encAssisterId)):null;
			Integer desigId = encDesigId!=null && !encDesigId.isEmpty() ?Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encDesigId)):null;

			Validate.notNull(assisterId, "Assister identifier is missing in the request.");

			//check entity-Assister association
			AccountUser user = entityUtils.getLoggedInUser();

			Validate.notNull(user, "User not found.");

			assisterRequestDTO.setId(assisterId);

			assisterResponseDTO = getAssisterDetail(assisterRequestDTO);

			EntityUtils.checkEntityAssisterAssociation(assisterId.toString(), user, assisterResponseDTO);

			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setId(desigId);

		//	LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS_ASSISTER_REQUEST_DTO_IN_REQUEST + assisterRequestDTO);
			assisterDetailsResponse = restClassCommunicator.retrieveAssisterDesignationDetailsForRequest(assisterRequestDTO);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		//	LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS_RESPONSE_RECEIVED + assisterResponseDTO);

			Validate.notNull(assisterResponseDTO, "DesignateAssister response not found.");

			DesignateAssister designateAssister = assisterResponseDTO.getDesignateAssister();

			if (designateAssister != null) {
				designateAssister.setStatus(Status.Active);
				assisterRequestDTO.setDesignateAssister(designateAssister);
				assisterDetailsResponse = restClassCommunicator.saveAssisterDesignation(assisterRequestDTO);
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
					String contextPath=request.getContextPath();
					sendApprovedMailNotification(designateAssister.getAssisterId(), assisterDetailsResponse,contextPath,user.getActiveModuleName());
				}
			}

			request.setAttribute(DESIG_STATUS, prevStatus);
			model.addAttribute(DESIG_STATUS, prevStatus);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while appoving individual request : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while appoving individual request : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("entityApprove: END");

		return "redirect:/entity/enrollmententity/individuals";
	}

	/**
	 * This method sends notification mail when pending request is accepted by assister/CEC/councillor
	 * @param desigId
	 * @param assisterDetailsResponse
	 * @throws Exception
	 */
	private void sendApprovedMailNotification(int desigId,String assisterDetailsResponse,String contextPath, String activeModuleName) throws Exception {

		LOGGER.info("sendApprovedMailNotification: START");
		AssisterResponseDTO assisterResponseDTO;
		AssisterRequestDTO assisterRequestDTO;
		String assisterName = null;
		String entityName = null;
		EnrollmentEntity entity = null;
		try {
			AssisterResponseDTO assisterResponse=JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);

			Validate.notNull(assisterResponse);

			if(assisterResponse != null && assisterResponse.getDesignateAssister()!=null && assisterResponse.getDesignateAssister().getStatus() !=null && DesignateAssister.Status.Active.toString().equalsIgnoreCase(assisterResponse.getDesignateAssister().getStatus().toString()) ){
				//Find individual details whose request has been approved.
				List<String> sendToEmailList  = new LinkedList<String>();
				ConsumerComposite hhIndividual= new ConsumerComposite();
				Map<String, Object> map = new HashMap<>();
				Integer startRecord = 0;
				Map<String, Object> searchCriteria = new HashMap<String, Object>();
				searchCriteria.put(BrokerConstants.MODULE_NAME, "ASSISTER");
				searchCriteria.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf( assisterResponse.getDesignateAssister().getIndividualId()));
				searchCriteria.put(BrokerConstants.MODULE_ID,assisterResponse.getDesignateAssister().getAssisterId());
				searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
				map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);
				List<ConsumerComposite> individualList = new ArrayList<ConsumerComposite>();
				Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);
				Map<String, Object> individualTemplateData = new HashMap<String, Object>();

				if(houseHoldMap!=null && !houseHoldMap.isEmpty()) {
					individualList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
					//hhIndividual =  new ConsumerComposite();
					hhIndividual = individualList.get(0);
				}

				if(hhIndividual!=null){
					
					PreferencesDTO  preferencesDTO  =  preferencesService.getPreferences((int)hhIndividual.getId(), false);
	     			Location location = EntityUtils.getLocationFromDTO(preferencesDTO.getLocationDto());
					sendToEmailList.add(preferencesDTO.getEmailAddress());

					assisterRequestDTO = new AssisterRequestDTO();
					assisterRequestDTO.setId(desigId);

					assisterResponseDTO = getAssisterDetail(assisterRequestDTO);
					int entityId = assisterResponseDTO.getAssister().getEntity().getId();
					if(0 != entityId){
						entity = entityUtils.getEnrollmentEntityById(entityId);
					}
					Validate.notNull(entity);
					String encrytedAssisterId=ghixJasyptEncrytorUtil.encryptStringByJasypt(assisterResponseDTO.getAssister().getId()+"");
					String encrytedPrimaryAssisterSiteId=ghixJasyptEncrytorUtil.encryptStringByJasypt(assisterResponseDTO.getAssister().getPrimaryAssisterSite().getId()+"");

					String uRLIndividualPortalCounselorInformation= GhixEndPoints.GHIXWEB_SERVICE_URL+"entity/locateassister/assisterdetail/"+encrytedAssisterId+"/"+encrytedPrimaryAssisterSiteId+"?profileView=Y";

					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_PATTERN);
					individualTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
					individualTemplateData.put(INDIVIDUAL_FRST_NAME, hhIndividual.getFirstName());
					individualTemplateData.put(INDIVIDUAL_MIDDLE_NAME, "");//No Middle name in ConsumerComposite //		 ${IndividualMN}
					individualTemplateData.put(INDIVIDUAL_LST_NAME, hhIndividual.getLastName());
					individualTemplateData.put(INDIVIDUAL_DESIG_STATUS,ACCEPTED);
					individualTemplateData.put(COUNSELOR_FIRST_NAME, assisterResponseDTO.getAssister().getFirstName());
					individualTemplateData.put(COUNSELOR_LAST_NAME,assisterResponseDTO.getAssister().getLastName());
					individualTemplateData.put(ENROLL_ENTITY_NAME, assisterResponseDTO.getAssister().getEntity().getBusinessLegalName());
					individualTemplateData.put(COUNCELOR_INFO_URL,uRLIndividualPortalCounselorInformation);
					individualTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					individualTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					individualTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
					individualTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
					individualTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
					individualTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
					individualTemplateData.put(EXCHANGE_URL,  DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
					individualTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					individualTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
					Map<String, String> templateDataModified = getTemplateDataMap(individualTemplateData);
					assisterName = assisterResponseDTO.getAssister().getFirstName() +" "+ assisterResponseDTO.getAssister().getLastName();
					entityName = entity.getUser().getFirstName() +" "+ entity.getUser().getLastName();
					if(ASSISTER.equalsIgnoreCase(activeModuleName)){
						noticeService.createModuleNotice(ACCEPTED_NOTICE_CLASS_NAME, GhixLanguage.US_EN, individualTemplateData, ECMRELATIVEPATH,"individualdesignationnotification_A_" + TimeShifterUtil.currentTimeMillis() + ".pdf",INDIVIDUAL , assisterResponse.getDesignateAssister().getIndividualId(), sendToEmailList, assisterName, hhIndividual.getFirstName(),location,preferencesDTO.getPrefCommunication());
					} else if("assisterenrollmententity".equalsIgnoreCase(activeModuleName)){
						noticeService.createModuleNotice(ACCEPTED_NOTICE_CLASS_NAME, GhixLanguage.US_EN, individualTemplateData, ECMRELATIVEPATH,"individualdesignationnotification_A_" + TimeShifterUtil.currentTimeMillis() + ".pdf",INDIVIDUAL , assisterResponse.getDesignateAssister().getIndividualId(), sendToEmailList, entityName, hhIndividual.getFirstName(),location,preferencesDTO.getPrefCommunication());
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Exception occurred while sending request approved notification :", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("sendApprovedMailNotification: END");
	}

	/**
	 * Retrieves individuals details.
	 *
	 * @param id
	 * @param model
	 * @param request
	 * @param desigStatus
	 * @return URL for navigation to appropriate result page
	 */
	@RequestMapping(value = "/entity/assister/viewindividualinfo/{id}", method = { RequestMethod.GET,
			RequestMethod.POST })
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_INDIVIDUAL)
	public String viewIndividualDetail(@PathVariable("id") String id,
			@RequestParam(value = DESIG_STATUS, required = false) String desigStatus, Model model , HttpServletRequest request) throws GIException {

		LOGGER.info("viewIndividualDetail: START");

		Integer indvidualId = id!=null && !id.equals("") ?Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(id)):null;
		
		if(null != indvidualId){
			model.addAttribute("encryptedIndividualId",id);
		}
		
		LOGGER.info("individual contact info page ");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Individuals Details Page");
		LOGGER.info("Inside Individual Detail Page");

		Assister assisterObjFromUserId = new Assister();
		Household houseHold=null ;
		XStream xstream;
		String response=null ;

		try {
			AccountUser user = entityUtils.getLoggedInUser();

			AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setModuleId(user.getId());
			AssisterResponseDTO assisterResponseDTOFromUserId = retrieveAssisterObjectByUserId(assisterRequestDTO);
			assisterObjFromUserId = assisterResponseDTOFromUserId.getAssister();

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
				houseHold= new Household();
				houseHold.setId(indvidualId);
				
				houseHold.setEmail(request.getParameter("emailAddress")!=null?request.getParameter("emailAddress"):"");
				houseHold.setFirstName(request.getParameter("firstName")!=null?request.getParameter("firstName"):"");
				houseHold.setLastName(request.getParameter("lastName")!=null?request.getParameter("lastName"):"" );
				houseHold.setPhoneNumber(request.getParameter("phoneNumber")!=null?request.getParameter("phoneNumber"):"");
				model.addAttribute("caStateCode",BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
			}else{
				LOGGER.debug("Rest call to get householeId starts");
				//get the household using rest call
				response = ghixRestTemplate.postForObject( GhixEndPoints.CONSUMER_SERVICE_URL+"consumer/findHouseholdById", String.valueOf(indvidualId), String.class);
				xstream = GhixUtils.getXStreamStaxObject();
				if(response != null){
					ConsumerResponse consumerResponse=(ConsumerResponse)xstream.fromXML(response);
					houseHold = consumerResponse.getHousehold();

				}
				LOGGER.debug("Rest call to get householeId ends");
			}
			

			//LOGGER.debug(RETURN_FROM_REST_CALL + response);
			model.addAttribute("externalIndividual", houseHold);
			model.addAttribute(DESIG_STATUS, desigStatus);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing individual info : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing individual info inviewIndividualDetail method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewIndividualDetail: END");

		return "entity/assister/viewindividualinfo";
	}

	/**
	 * Handle the decline designation request for Employers / Individuals.
	 *
	 * @param desigId
	 *            ID of the designated user
	 * @param fromModule
	 *            Employer / Individual
	 * @param desigStatus
	 *            Designation Status
	 * @param encDesigId
	 * @param request
	 * @return URL for navigation to appropriate result page
	 */
	//@GiAudit(transactionName = "Assister Decline User", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/decline/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ASSISTER_DECLINE_USER')")
	public String decline(@PathVariable("id") String encDesigId,
			@RequestParam(value = PREV_STATUS, required = false) String desigStatus, HttpServletRequest request) {

		LOGGER.info("decline: START");

		AssisterResponseDTO assisterResponseDTO = null;
		String assisterDetailsResponse = null;
		AccountUser user = null;
		Assister assisterObj = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

		final String EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST = "Exception occured while declining user designation request : ";

		try {
			Integer desigId = encDesigId!=null && !"".equals(encDesigId) ?Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encDesigId)):null;
			assisterRequestDTO.setId(desigId);

			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
			assisterDetailsResponse = restClassCommunicator
					.retrieveAssisterDesignationDetailsForRequest(assisterRequestDTO);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);

			Validate.notNull(assisterResponseDTO);

			DesignateAssister designateAssister = assisterResponseDTO.getDesignateAssister();

			Validate.notNull(designateAssister);

			designateAssister.setStatus(Status.InActive);
			assisterRequestDTO.setDesignateAssister(designateAssister);

			assisterDetailsResponse = restClassCommunicator.saveAssisterDesignation(assisterRequestDTO);
			
			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
				user = entityUtils.getLoggedInUser();
				if (designateAssister != null && "Pending".equalsIgnoreCase(desigStatus)) {

					sendRequestDeniedNotification(designateAssister.getAssisterId(), assisterDetailsResponse,user.getActiveModuleName());
				}


				//Get assister object from logged in user. OR
				//Get assister from switched user
				assisterRequestDTO.setModuleId(user.getId());

			    assisterResponseDTO = retrieveAssisterObjectByUserId(assisterRequestDTO);

			    Validate.notNull(assisterResponseDTO);

				assisterObj = assisterResponseDTO.getAssister();

			    Validate.notNull(assisterObj);

				//From retrieved assister object get designated household to this assister.

				if("Active".equalsIgnoreCase(desigStatus)){

					LOGGER.info("send CEC termination notice: START");
					// Get the list of Individuals associated with this Enrollment counselor
					String noticeTemplateName = NOTIFICATION_TEMPLATE_NAME;
					Map<String, Object> inividualAPIRequestMap = new HashMap<String, Object>();
			        inividualAPIRequestMap.put(BrokerConstants.MODULE_NAME, BrokerConstants.ASSISTER);
			        inividualAPIRequestMap.put(BrokerConstants.STATUS, BrokerConstants.STATUS_INACTIVE);
			        inividualAPIRequestMap.put(BrokerConstants.MODULE_ID, assisterObj.getId());
			        inividualAPIRequestMap.put(BrokerConstants.SENDER_OBJ, assisterObj);
			        inividualAPIRequestMap.put(BrokerConstants.INDIVIDUAL_ID, Integer.toString(desigId));

		            Map<String, Object> assisterTemplateData = new HashMap<String, Object>();
		     		String assistername = assisterObj.getFirstName() +" "+assisterObj.getLastName();
		     		assisterTemplateData.put(ASSISTER_NAME, assistername);
		     		assisterTemplateData.put(ENROLLMENTENTITY_NAME, assisterObj.getEntity().getEntityName());

		     		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_PATTERN);
		     		assisterTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
		     		assisterTemplateData.put(REASON_FOR_DE_DESIGNATION, "Your Counselor "+assistername+ " does not wish to continue this relationship");

		     		// tokens required for the notification template
		     		assisterTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		     		assisterTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		     		assisterTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		     		assisterTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
		     		assisterTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
		     		assisterTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
		     		assisterTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		     		// HIX-20774

		     		assisterTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

		     		assisterTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		     		assisterTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		     		assisterTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		     		assisterTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
		     		Map<String, String> templateDataModified = getTemplateDataMap(assisterTemplateData);

		            brokerMgmtUtils.sendNotificationsToIndividuals(inividualAPIRequestMap, noticeTemplateName, assisterTemplateData);
		     		LOGGER.info("send CEC termination notice: END");
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST, giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		request.setAttribute(DESIG_STATUS, desigStatus);

		LOGGER.info("decline: END");

		return REDIRECT_ENTITY_ASSISTER_INDIVIDUALS_DESIG_STATUS + desigStatus;
	}


	/**
	 * Method for Entity to decline his/her assister's designation request by user.
	 *
	 * @param encAssisterId The encrypted assister identifier.
	 * @param encDesigId The encrypted designation identifier.
	 * @param desigStatus The designation status.
	 * @param model The Model instance.
	 * @param request The HttpServletRequest instance.
	 * @return String The view name
	 */
	//@GiAudit(transactionName = "Entity Decline User", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/decline/{assisterId}/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ENTITY_ASSISTER_DECLINE_USER')")
	public String entityDecline(@PathVariable("assisterId") String encAssisterId,
			@PathVariable("id") String encDesigId,
			@RequestParam(value = PREV_STATUS, required = false) String desigStatus,HttpServletRequest request) {

		LOGGER.info("entityDecline: START");

		String assisterDetailsResponse = null;
		AssisterResponseDTO assisterResponseDTO = null;
		AccountUser user = null;
		Assister assisterObj = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

		try {
			Integer assisterId = encAssisterId!=null && !encAssisterId.isEmpty() ?Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encAssisterId)):null;
			Integer desigId = encDesigId!=null && !encDesigId.isEmpty() ?Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encDesigId)):null;

			Validate.notNull(assisterId, "Assister identifier is missing in the request.");

			user = entityUtils.getLoggedInUser();

			assisterRequestDTO.setId(assisterId);

			assisterResponseDTO = getAssisterDetail(assisterRequestDTO);

			Validate.notNull(assisterResponseDTO);

			Assister assister = assisterResponseDTO.getAssister();

			Validate.notNull(assister, "Assister record not found in repository.");

			entityUtils.checkEntityAssisterAssociation(assisterId.toString(), user, assisterResponseDTO);

			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setId(desigId);

			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
			assisterDetailsResponse = restClassCommunicator
					.retrieveAssisterDesignationDetailsForRequest(assisterRequestDTO);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);

			Validate.notNull(assisterResponseDTO);

			DesignateAssister designateAssister = assisterResponseDTO.getDesignateAssister();

			Validate.notNull(designateAssister);

			designateAssister.setStatus(Status.InActive);
			assisterRequestDTO.setDesignateAssister(designateAssister);

			assisterDetailsResponse = restClassCommunicator.saveAssisterDesignation(assisterRequestDTO);

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
				if (designateAssister != null && "Pending".equalsIgnoreCase(desigStatus)) {
					sendRequestDeniedNotification(designateAssister.getAssisterId(), assisterDetailsResponse,user.getActiveModuleName());
				}

				Validate.notNull(assister.getUser(), "Invalid assister record.");

				//Get assister object from logged in user. OR
				//Get assister from switched user
				assisterRequestDTO.setModuleId(assister.getUser().getId());

			    assisterResponseDTO = retrieveAssisterObjectByUserId(assisterRequestDTO);

			    Validate.notNull(assisterResponseDTO);

				assisterObj = assisterResponseDTO.getAssister();

				Validate.notNull(assisterObj);

				//From retrieved assister object get designated household to this assister.

				if("Active".equalsIgnoreCase(desigStatus)){

					LOGGER.info("send CEC termination notice: START");
					// Get the list of Individuals associated with this Enrollment counselor
					String noticeTemplateName = NOTIFICATION_TEMPLATE_NAME;
					Map<String, Object> inividualAPIRequestMap = new HashMap<String, Object>();
			        inividualAPIRequestMap.put(BrokerConstants.MODULE_NAME, BrokerConstants.ASSISTER);
			        inividualAPIRequestMap.put(BrokerConstants.STATUS, BrokerConstants.STATUS_INACTIVE);
			        inividualAPIRequestMap.put(BrokerConstants.MODULE_ID, assisterObj.getId());
			        inividualAPIRequestMap.put(BrokerConstants.SENDER_OBJ, assisterObj);
			        inividualAPIRequestMap.put(BrokerConstants.INDIVIDUAL_ID, Integer.toString(desigId));

		            Map<String, Object> assisterTemplateData = new HashMap<String, Object>();
		     		String assistername = assisterObj.getFirstName() +" "+assisterObj.getLastName();
		     		assisterTemplateData.put(ASSISTER_NAME, assistername);
		     		assisterTemplateData.put(ENROLLMENTENTITY_NAME, assisterObj.getEntity().getEntityName());

		     		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_PATTERN);
		     		assisterTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
		     		assisterTemplateData.put(REASON_FOR_DE_DESIGNATION, "Your Counselor "+assistername+ " does not wish to continue this relationship");

		     		// tokens required for the notification template
		     		assisterTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		     		assisterTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		     		assisterTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		     		assisterTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
		     		assisterTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
		     		assisterTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
		     		assisterTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		     		// HIX-20774

		     		assisterTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

		     		assisterTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		     		assisterTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		     		assisterTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		     		assisterTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
		     		assisterTemplateData.put("IsEntity", true);
		     		Map<String, String> templateDataModified = getTemplateDataMap(assisterTemplateData);

		            brokerMgmtUtils.sendNotificationsToIndividuals(inividualAPIRequestMap, noticeTemplateName, assisterTemplateData);
		     		LOGGER.info("send CEC termination notice: END");
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST, giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DECLINING_USER_DESIGNATION_REQUEST, exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		request.setAttribute(DESIG_STATUS, desigStatus);

		LOGGER.info("entityDecline: END");

		return "redirect:/entity/enrollmententity/individuals?desigStatus=" + desigStatus;
	}

	/**
	 * This method sends notification mail to individual about denial of designation request.
	 * @param desigId
	 * @param assisterDetailsResponse
	 * @throws Exception
	 */
	private void sendRequestDeniedNotification(Integer desigId,String assisterDetailsResponse, String activeModuleName) throws Exception {
		LOGGER.info("sendRequestDeniedNotification: START");

		try {
			String assisterName = null;
			AssisterResponseDTO assisterResponseDTO;
			EnrollmentEntity entity = null;
			String entityName = null;
			AssisterResponseDTO assisterResponse=JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			if(assisterResponse != null && assisterResponse.getDesignateAssister()!=null && assisterResponse.getDesignateAssister().getStatus() !=null && DesignateAssister.Status.InActive.toString().equalsIgnoreCase(assisterResponse.getDesignateAssister().getStatus().toString()) ){

				List<String> sendToEmailList  = new LinkedList<String>();
				ConsumerComposite hhIndividual=new ConsumerComposite();

				Map<String, Object> map = new HashMap<>();
				Integer startRecord = 0;
				Map<String, Object> searchCriteria = new HashMap<String, Object>();
				searchCriteria.put(BrokerConstants.MODULE_NAME, "ASSISTER");
				searchCriteria.put(BrokerConstants.INDIVIDUAL_ID, String.valueOf( assisterResponse.getDesignateAssister().getIndividualId()));
				searchCriteria.put(BrokerConstants.MODULE_ID,assisterResponse.getDesignateAssister().getAssisterId());
				searchCriteria.put(BrokerConstants.START_PAGE, startRecord);
				map.put(BrokerConstants.SEARCH_CRITERIA, searchCriteria);
				List<ConsumerComposite> individualList = new ArrayList<ConsumerComposite>();
				Map<String, Object> houseHoldMap = brokerMgmtUtils.retrieveConsumerList(map);
				Map<String, Object> individualTemplateData = new HashMap<String, Object>();
				if(houseHoldMap!=null && !houseHoldMap.isEmpty()) {
					individualList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
					//hhIndividual =  new ConsumerComposite();
					hhIndividual = individualList.get(0);
				}

				if(hhIndividual!=null){
					
					PreferencesDTO  preferencesDTO  =  preferencesService.getPreferences((int)hhIndividual.getId(), false);
	     			Location location = EntityUtils.getLocationFromDTO(preferencesDTO.getLocationDto());
					sendToEmailList.add(preferencesDTO.getEmailAddress());
					
					AssisterRequestDTO assisterRequestDTOCoun = new AssisterRequestDTO();
					assisterRequestDTOCoun.setId(desigId);

					assisterResponseDTO = getAssisterDetail(assisterRequestDTOCoun);
					int entityId = assisterResponseDTO.getAssister().getEntity().getId();
					if(0 != entityId){
						entity = entityUtils.getEnrollmentEntityById(entityId);
					}
					Validate.notNull(entity);

					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_PATTERN);
					individualTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
					individualTemplateData.put(INDIVIDUAL_FRST_NAME, hhIndividual.getFirstName());
					individualTemplateData.put(INDIVIDUAL_MIDDLE_NAME, "");//No Middle name in ConsumerComposite //		 ${IndividualMN}
					individualTemplateData.put(INDIVIDUAL_LST_NAME, hhIndividual.getLastName());
					individualTemplateData.put(INDIVIDUAL_DESIG_STATUS,DENIED);
					individualTemplateData.put(COUNSELOR_FIRST_NAME, assisterResponseDTO.getAssister().getFirstName());
					individualTemplateData.put(COUNSELOR_LAST_NAME,assisterResponseDTO.getAssister().getLastName());
					individualTemplateData.put(ENROLL_ENTITY_NAME, assisterResponseDTO.getAssister().getEntity().getBusinessLegalName());
					//individualTemplateData.put(COUNCELOR_INFO_URL,"");/**Need to take counceelrrurl**/
					individualTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					individualTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					individualTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
					individualTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
					individualTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
					individualTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
					individualTemplateData.put(EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
					individualTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					individualTemplateData.put(INDIVIDUAL_LOGIN_URL,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));/**Need to take url**/
					individualTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
					Map<String, String> templateDataModified = getTemplateDataMap(individualTemplateData);
					assisterName = assisterResponseDTO.getAssister().getFirstName() +" "+ assisterResponseDTO.getAssister().getLastName();
					entityName = entity.getUser().getFirstName() +" "+ entity.getUser().getLastName();
					if(ASSISTER.equalsIgnoreCase(activeModuleName)){
						noticeService.createModuleNotice(DECLINED_NOTICE_CLASS_NAME, GhixLanguage.US_EN, individualTemplateData, ECMRELATIVEPATH,"individualdesignationnotification_D_" + TimeShifterUtil.currentTimeMillis() + ".pdf", INDIVIDUAL, assisterResponse.getDesignateAssister().getIndividualId(), sendToEmailList, assisterName, hhIndividual.getFirstName(),location,preferencesDTO.getPrefCommunication());
					} else if("assisterenrollmententity".equalsIgnoreCase(activeModuleName)){
						noticeService.createModuleNotice(DECLINED_NOTICE_CLASS_NAME, GhixLanguage.US_EN, individualTemplateData, ECMRELATIVEPATH,"individualdesignationnotification_D_" + TimeShifterUtil.currentTimeMillis() + ".pdf", INDIVIDUAL, assisterResponse.getDesignateAssister().getIndividualId(), sendToEmailList, entityName, hhIndividual.getFirstName(),location,preferencesDTO.getPrefCommunication());
					}

				}
			}
		}catch(Exception e){
			LOGGER.error("Exception occurred while sending request denial notification mail : " , e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("sendRequestDeniedNotification: END");
	}

	/**
	 * Handles accept bulk requests for Employers / Individuals.
	 *
	 * @param ids
	 *            {@link List} of designation ids
	 * @param prevStatus
	 * @return URL for navigation to appropriate result page
	 */
	//@GiAudit(transactionName = "Assister Approve User Request", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/approveRequests", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ASSISTER_APPROVE_USER')")
	public String approveRequests(@RequestParam("ids") String ids,
			@RequestParam(value = PREV_STATUS, required = false) String prevStatus) {

		LOGGER.info("approveRequests: START");

		AssisterRequestDTO assisterRequestDTO = null;
		AssisterResponseDTO assisterResponseDTO = null;
		String assisterDetailsResponse = null;
		List<String> items = null;

		try {
			if (ids != null && !"".equals(ids) && ids.contains(",")) {
				items = Arrays.asList(ids.split("\\s*,\\s*"));

				if (items != null && !items.isEmpty()) {
					for (int i = 0; i < items.size(); i++) {
						assisterRequestDTO = new AssisterRequestDTO();
						assisterRequestDTO.setId(Integer.valueOf(items.get(i)));

						LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
						assisterDetailsResponse = restClassCommunicator
								.retrieveAssisterDesignationDetailsForRequest(assisterRequestDTO);
						assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
						LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);
						DesignateAssister designateAssister = assisterResponseDTO.getDesignateAssister();

						if (designateAssister != null) {
							designateAssister.setStatus(Status.Active);
							assisterRequestDTO.setDesignateAssister(designateAssister);
							assisterDetailsResponse = restClassCommunicator.saveAssisterDesignation(assisterRequestDTO);
						}
					}
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while approving individual request : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while approving individual request in approveRequests method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("approveRequests: END");

		return REDIRECT_ENTITY_ASSISTER_INDIVIDUALS_DESIG_STATUS + prevStatus;
	}

	/**
	 * Handles decline bulk of requests for Employers / Individuals.
	 *
	 * @param ids
	 *            List of employer/individual ids
	 * @param desigId
	 * @param desigStatus
	 *            value of which status page is opened
	 * @return URL for navigation to appropriate result page
	 */
	//@GiAudit(transactionName = "Assister Decline User Request", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/declineRequests", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ASSISTER_DECLINE_USER')")
	public String declineRequests(@RequestParam("ids") String ids, @PathVariable("id") Integer desigId,
			@RequestParam(value = PREV_STATUS, required = false) String desigStatus) {

		LOGGER.info("declineRequests: START");

		AssisterRequestDTO assisterRequestDTO = null;
		AssisterResponseDTO assisterResponseDTO = null;
		String assisterDetailsResponse = null;
		List<String> items = null;

		try {
			if (ids != null && !"".equals(ids) && ids.contains(",")) {
				items = Arrays.asList(ids.split("\\s*,\\s*"));
				if (items != null && !items.isEmpty()) {
					for (int i = 0; i < items.size(); i++) {
						assisterRequestDTO = new AssisterRequestDTO();
						assisterRequestDTO.setId(desigId);

						LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
						assisterDetailsResponse = restClassCommunicator
								.retrieveAssisterDesignationDetailsForRequest(assisterRequestDTO);
						assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
						LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_ENDS);

						Validate.notNull(assisterResponseDTO);

						DesignateAssister designateAssister = assisterResponseDTO.getDesignateAssister();

						if (designateAssister != null) {
							designateAssister.setStatus(Status.InActive);
							assisterRequestDTO.setDesignateAssister(designateAssister);
							assisterDetailsResponse = restClassCommunicator.saveAssisterDesignation(assisterRequestDTO);
						}
					}
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while declining request : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while declining request indeclineRequests method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("declineRequests: END");

		return REDIRECT_ENTITY_ASSISTER_INDIVIDUALS_DESIG_STATUS + desigStatus;
	}

	/**
	 * Redirects to confirm decline pop up
	 *
	 * @param desigId
	 *            Individual id
	 * @param desigName
	 * @param desigStatus
	 *            value of which status page is opened
	 * @param fromModule
	 *            From which page the control is coming
	 * @param broker
	 *            {@link Assister} object
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to appropriate result page
	 */
	//@GiAudit(transactionName = "Assister Decline User Popup", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/declinePopup", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ASSISTER_DECLINE_USER')")
	public String declinePopup(@RequestParam(DESIG_ID) String desigId,
			HttpServletRequest request) {

		LOGGER.info("declinePopup: START");

		try {
			String designationId=desigId !=null && !"".equals(desigId)?ghixJasyptEncrytorUtil.decryptStringByJasypt(desigId):null;

			String desigIdSubString=null;
			if (designationId != null && designationId.contains(",")) {
				int lastIdx = designationId.length() - 1;
				char last = designationId.charAt(lastIdx);
				if (last == ',') {
					desigIdSubString = designationId.substring(0, designationId.length() - 1);
				}
			}
			request.setAttribute(DESIG_ID, desigIdSubString);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while processing declinePopup(...) : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while processing declinePopup(...) : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("declinePopup: END");

		return "entity/assister/declinePopup";
	}

	/**
	 * Method for Entity to confirm decline of his/her assister's designation request by user.
	 *
	 * @param encAssisterId The encrypted assister identifier.
	 * @param encDesigId The encrypted designation identifier.
	 * @param desigName The designation name.
	 * @param desigStatus The designation status.
	 * @param assister The Assister instance.
	 * @param request The HttpServletRequest instance.
	 * @return String The view name
	 */
	//@GiAudit(transactionName = "Entity Decline User Popup", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/declinePopup/{assisterId}/{desigId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ENTITY_ASSISTER_DECLINE_USER')")
	public String entityDeclinePopup(@PathVariable("assisterId") String encAssisterId,
			@PathVariable(DESIG_ID) String encDesigId, HttpServletRequest request) {

		LOGGER.info("entityDeclinePopup: START");
		String desigIdSubString=null;
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

		try {
			Integer assisterId = encAssisterId!=null && !encAssisterId.isEmpty() ?Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(encAssisterId)):null;
			String designationId=encDesigId !=null && !"".equals(encDesigId)?ghixJasyptEncrytorUtil.decryptStringByJasypt(encDesigId):null;

			Validate.notNull(assisterId, "Assister identifier is missing in the request.");

			AccountUser user = entityUtils.getLoggedInUser();

			assisterRequestDTO.setId(assisterId);

			assisterResponseDTO = getAssisterDetail(assisterRequestDTO);

			entityUtils.checkEntityAssisterAssociation(assisterId.toString(), user, assisterResponseDTO);

			if (designationId != null && designationId.contains(",")) {
				int lastIdx = designationId.length() - 1;
				char last = designationId.charAt(lastIdx);
				if (last == ',') {
					desigIdSubString = designationId.substring(0, designationId.length() - 1);
				}
			}

			request.setAttribute(DESIG_ID, desigIdSubString);
			request.setAttribute(ASSISTER_ID, assisterId);
			request.setAttribute("isEntity", "y");
			request.setAttribute("consumerFirstName", request.getParameter("firstName"));
			request.setAttribute("consumerLastName", request.getParameter("lastName"));
			request.setAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while processing declinePopup(...) in entityDeclinePopup method: ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while processing declinePopup in entityDeclinePopup method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("entityDeclinePopup: END");

		return "entity/assister/declinePopup";
	}

	private void assisterNavigationMap(HttpServletRequest request, Assister assister) {

		LOGGER.info("assisterNavigationMap: START");

		Map<String, Boolean> map = null;
		if (assister != null) {
			map = getLeftNavigationMenuMap(assister);
		} else {
			map = new HashMap<String, Boolean>();
		}

		Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(NAVIGATION_MENU_MAP);

		if (navigationMenuMap != null) {
			map.putAll(navigationMenuMap);
		}
		request.getSession().setAttribute(EntityConstants.NAVIGATION_MENU_MAP, map);

		LOGGER.info("assisterNavigationMap: END");
	}

	private Map<String, Boolean> getLeftNavigationMenuMap(Assister assister) {

		LOGGER.info("getLeftNavigationMenuMap: START");

		AssisterResponseDTO assisterResponseDTO = null;

		Map<String, Boolean> map = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setAssister(assister);

		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
			String requestXML = writer.writeValueAsString(assisterRequestDTO);

			LOGGER.debug("Populate Left Navigation Map REST Call Starts.");
			String response = restClassCommunicator.assisterPopulateNavigationMenuMap(requestXML);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(response);
			map = assisterResponseDTO.getNavigationMenuMap();
			LOGGER.debug("Populate Left Navigation Map REST Call Ends.");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Populating Left Navigation Map : " , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getLeftNavigationMenuMap: END");

		return map;
	}

	private String loadLanguagesData(){
        LOGGER.info("getLanguageList : START");
        String jsonData = null;

	    
	    List<String> languageSpokenList = null;

	    languageSpokenList = lookupService.populateLanguageNames("");
        LOGGER.info("getLanguageList : END");
        jsonData = platformGson.toJson(languageSpokenList);

        return jsonData;
	}

	private Map<String, String> getTemplateDataMap(Map<String, Object> templateData) {
		Map<String, String> templateDataModified = new HashMap<String, String>();
	    Iterator<Entry<String, Object>> it = templateData.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
	        if(pairs.getValue() != null) {
	        	templateDataModified.put(pairs.getKey(), pairs.getValue().toString());
	        }
	    }
		return templateDataModified;
	}
}
