package com.getinsured.eligibility.ssap.integration.household.enricher;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.jayway.jsonpath.JsonPath;

@Component(value = "headerEnricher")
public class HeaderEnricher {

	private static final String[] OE_QEP_EVENTS = {SsapApplicationEventTypeEnum.OE.value(), SsapApplicationEventTypeEnum.QEP.value()};
	private static final String RERUN_ELIGIBILITY_ONLY = "RERUN_ELIGIBILITY_ONLY";
	private static final Logger LOGGER = Logger.getLogger(HeaderEnricher.class);

	/**
	 * provides the applicationID in the request to SSAPApplications RS
	 *
	 * @param message
	 * @return
	 */
	public String enrichApplicationId(Message<?> message) throws GIException {
		String ssapApplicationId = "";
		LOGGER.debug(" ssapApplication :: "+ message);
		ssapApplicationId= (String) JsonPath.read(message.getPayload().toString(),"$.singleStreamlinedApplication.ssapApplicationId");

		if (!StringUtils.isNotEmpty(ssapApplicationId)) {
		LOGGER.error("ssapApplicationId is null or empty:"+ message.getPayload().toString());
		throw new GIException("ssapApplicationId is null or empty");
	}
		LOGGER.debug("applicationId:" + ssapApplicationId);
		return ssapApplicationId;
	}

	/**
	 * Checks if correlation Id is coming in the request message and returns it.
	 * If not present, creates a new correlationId and sends that.
	 * @param message
	 * @return
	 * @throws ParseException
	 */
	public String enrichCorrelationId(Message<String> message) throws ParseException {
		String request = message.getPayload();
		JSONParser parser = new JSONParser();
		JSONObject originalJSONObject = (JSONObject) parser.parse(request);
		String correlationId = originalJSONObject.get(
				SSAPIntegrationConstants.CORRELATION_ID_HEADER).toString();
		if(StringUtils.isBlank(correlationId)) {
			correlationId = UUID.randomUUID().toString();
		}
		return correlationId;
	}

	@SuppressWarnings("rawtypes")
	public String enrichApplicationIdfromMap(Message<?> message) throws ParseException {
		JSONParser parser = new JSONParser();
		String ssapApplicationId = "";
		String ssapJSON = null;
		if(message.getPayload() instanceof java.util.Map){
			LinkedMultiValueMap messagelist = (org.springframework.util.LinkedMultiValueMap)message.getPayload();
			if(null != messagelist && null != messagelist.get("load")) {
				ssapJSON = messagelist.get("load").get(0).toString();
			}
		} else {
			ssapJSON = message.getPayload().toString();
		}
		org.json.simple.JSONObject originalJSONObject = (org.json.simple.JSONObject) parser.parse(ssapJSON);
		originalJSONObject = (org.json.simple.JSONObject) originalJSONObject.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION);
		ssapApplicationId = originalJSONObject.get(SSAPIntegrationConstants.SSAP_APPLICATION_ID).toString();
		LOGGER.debug("SSAP JSON: " + ssapJSON + " applicationId:" + ssapApplicationId);
		return ssapApplicationId;
	}

	@SuppressWarnings("rawtypes")
	public String enrichSSAPJSON(Message<?> message) throws GIException {
		String SSAP_JSON = "";
		if(message.getPayload() instanceof java.util.Map){
			LinkedMultiValueMap messagelist = (org.springframework.util.LinkedMultiValueMap)message.getPayload();
			LOGGER.debug(messagelist.get("load"));
			if(null != messagelist && null != messagelist.get("load")) {
				SSAP_JSON = messagelist.get("load").get(0).toString();
			}

		}else{
			SSAP_JSON  = message.getPayload().toString();
			//TODO capture condition is DB returns null for ssapApplicationData
		}
		LOGGER.debug("SSAP_JSON:" + SSAP_JSON);
		return SSAP_JSON;
	}

	/*
	 * Extract SSAP application Id from the SSAP JSON
	 */
	public String enrichSsapApplicationId(Message<String> message)
			throws ParseException {
		String request = message.getPayload();
		JSONParser parser = new JSONParser();
		JSONObject originalJSONObject = (JSONObject) parser.parse(request);
		String ssapJson = originalJSONObject.get(
				SSAPIntegrationConstants.SSAP_JSON).toString();
		originalJSONObject = (JSONObject) parser.parse(ssapJson);
		originalJSONObject = (JSONObject) originalJSONObject
				.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION);
		return originalJSONObject.get(
				SSAPIntegrationConstants.SSAP_APPLICATION_ID).toString();
	}

	/*
	 * The JPA query in ssap-household-integration-config.xml can return one row or multiple SsapApplicantEvent objects:
	 * 		<jpa:retrieving-outbound-gateway
			entity-manager-factory="integrationEntityManagerfactory"
			jpa-query="select sace from SsapApplicantEvent as sace inner join fetch sace.ssapAplicationEvent as sape where sape.ssapApplication.id = :applicationId">
			<jpa:parameter expression="T(java.lang.Long).parseLong(headers.get('SSAP_APPLICATION_ID'))" name="applicationId"></jpa:parameter>
		   </jpa:retrieving-outbound-gateway>
	 *
	 * This method converts the single SsapApplicantEvent object returned to a List<SsapApplicantEvent> so that the response from the jpa-query
	 * can be processed in a consistent way.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	public List<SsapApplicantEvent> enrichApplicantEvent(final Message message) {
		if (message.getPayload() instanceof SsapApplicantEvent) {
			return new ArrayList<SsapApplicantEvent>() {
				{
					add((SsapApplicantEvent) message.getPayload());
				}
			};
		}
		return (List<SsapApplicantEvent>) message.getPayload();
	}

	/*
	 * Removes the members who are not affected by the current life change
	 * event. This is decided by fetching the list of applicants from the
	 * SSAP_APPLICATION_EVENTS table and then selecting only these members from
	 * the SSAP JSON.
	 */
	@SuppressWarnings("unchecked")
	public String filteredSsapJson(Message<String> message) throws ParseException {
		JSONParser parser = new JSONParser();
		String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.SSAP_JSON_HEADER).toString();
		String applicationEventType = message.getHeaders().get("APPLICATION_EVENT_TYPE").toString();
		List<SsapApplicantEvent> ssapApplicantEvents = (List<SsapApplicantEvent>) message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICANT_DATA_HEADER);
		JSONObject originalJSONObject = (JSONObject) parser.parse(ssapJSON);
		JSONObject ssapJSONObject = (JSONObject) originalJSONObject.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION);
		JSONArray taxHouseholds = (JSONArray) ssapJSONObject.get(SSAPIntegrationConstants.TAX_HOUSEHOLD);
		JSONArray updatedTaxHouseholds = new JSONArray();

		for (Object taxHousehold : taxHouseholds) {
			JSONObject household = (JSONObject) taxHousehold;
			populateVerificationHeaders(originalJSONObject, household);
			JSONArray householdMembers = (JSONArray) household.get(SSAPIntegrationConstants.HOUSEHOLD_MEMBER);
			JSONArray selectedHouseholdMembers = new JSONArray();
			for (Object householdMember : householdMembers) {
				JSONObject member = (JSONObject) householdMember;
				// HIX-119466: Removed data check
				// if (isMemberAffectedByEvent(member, ssapApplicantEvents, applicationEventType) && Boolean.valueOf(member.get(SSAPIntegrationConstants.APPLYING_FOR_COVERAGE_INDICATOR).toString())) {
				selectedHouseholdMembers.add(member);
				//}
			}

			if (selectedHouseholdMembers.isEmpty()) {
				if(Arrays.asList(OE_QEP_EVENTS).contains(applicationEventType)) {
					throw new IllegalStateException("No household members to verify");
				} else if(SsapApplicationEventTypeEnum.SEP.value().equals(applicationEventType)) {
					return RERUN_ELIGIBILITY_ONLY;
				}
			} else {
				household.put(SSAPIntegrationConstants.HOUSEHOLD_MEMBER, selectedHouseholdMembers);
			}
			updatedTaxHouseholds.add(taxHouseholds);
		}
		ssapJSONObject.put(SSAPIntegrationConstants.TAX_HOUSEHOLD, taxHouseholds);
		originalJSONObject.put(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION, ssapJSONObject);
		return originalJSONObject.toJSONString();
	}

	private void populateVerificationHeaders(JSONObject originalJSONObject, JSONObject household) {
		JSONArray householdCompareResults = (JSONArray) household.get("requiredVerifications");
		if(householdCompareResults!=null && householdCompareResults.size()>0)
		{
			householdCompareResults.forEach(result ->{
			switch ((String)result)
			{
			case "SSAC" :
				originalJSONObject.put("ssac_verification","true");
				break;
			case "VLP" :
				originalJSONObject.put("vlp_verification","true");
				break;	
			default: 
				originalJSONObject.put("financialAssistance_verification","true");
				originalJSONObject.put("nonFinancial_verification","true");
				originalJSONObject.put("residency_verification","true");
				break;
			}	
			});
		}
		else
		{
			// By Default all the verifications should be triggered
			originalJSONObject.put("ssac_verification","true");
			originalJSONObject.put("vlp_verification","true");
			originalJSONObject.put("financialAssistance_verification","true");
			originalJSONObject.put("nonFinancial_verification","true");
			originalJSONObject.put("residency_verification","true");
			
		}
	}

	/*
	 * Extracts ClientIp field from SSAP JSON
	 */
	public String enrichClientIp(Message<String> message)
			throws ParseException, UnknownHostException {
		JSONParser parser = new JSONParser();
		String request = message.getPayload();
		String clientIp = null;
		JSONObject originalJSONObject = (JSONObject) parser.parse(request);
		String ssapJson = originalJSONObject.get(
				SSAPIntegrationConstants.SSAP_JSON).toString();
		originalJSONObject = (JSONObject) parser.parse(ssapJson);
		originalJSONObject = (JSONObject) originalJSONObject
				.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION);
		if (null != originalJSONObject.get(SSAPIntegrationConstants.CLIENT_IP)) {
			clientIp = originalJSONObject.get(
					SSAPIntegrationConstants.CLIENT_IP).toString();
		}
		if (StringUtils.isBlank(clientIp)) {
			clientIp = InetAddress.getLocalHost().getHostAddress();
		}
		return clientIp;
	}

	/*
	 * Extracts SSAP JSON from request message
	 */
	public String enrichSsapJson(Message<String> message) throws ParseException {
		JSONParser parser = new JSONParser();
		String request = message.getPayload();
		JSONObject originalJSONObject = (JSONObject) parser.parse(request);
		return originalJSONObject.get(SSAPIntegrationConstants.SSAP_JSON)
				.toString();
	}

	/*
	 * Extracts SSAP JSON from request message
	 */
	public String enrichSsapSepIntegrationDTO(Message<String> message) throws ParseException {
		JSONParser parser = new JSONParser();
		String request = message.getPayload();
		JSONObject originalJSONObject = (JSONObject) parser.parse(request);
		return originalJSONObject.get("SSAP_SEP_INTEGRATION_DTO").toString();
	}

	/*
	 * Extracts APPLICATION_EVENT_ID field from request message
	 */
	public String enrichApplicationEventId(Message<String> message) throws ParseException {
		JSONParser parser = new JSONParser();
		String request = message.getPayload();
		JSONObject originalJSONObject = (JSONObject) parser.parse(request);
		return originalJSONObject.get(
				SSAPIntegrationConstants.APPLICATION_EVENT_ID).toString();
	}

	/*
	 * Extracts APPLICATION_EVENT_TYPE field from request message
	 */
	public String enrichApplicationEventType(Message<String> message) throws ParseException {
		JSONParser parser = new JSONParser();
		String request = message.getPayload();
		JSONObject originalJSONObject = (JSONObject) parser.parse(request);
		return originalJSONObject.get(
				SSAPIntegrationConstants.APPLICATION_EVENT_TYPE).toString();
	}

	private boolean isMemberAffectedByEvent(JSONObject member, List<SsapApplicantEvent> ssapApplicantEvents, String applicationEventType) {
		for (SsapApplicantEvent applicantEvent : ssapApplicantEvents) {
			if(applicantEvent.getSsapApplicant().getPersonId() == Long.parseLong(member.get(SSAPIntegrationConstants.PERSON_ID).toString())) {
				// If OEP/QEP, then the only thing to check if the person Id from the json matches the applicant in the event. If so, member is affected by event and we have to run verifications.
				// For SEP, we check if there is some verification needed for the SepEvent associated with the applicant event. If so, member is affected by event and we have to run verifications.
				if(Arrays.asList(OE_QEP_EVENTS).contains(applicationEventType) ||
						(SsapApplicationEventTypeEnum.SEP.value().equals(applicationEventType) && null != applicantEvent.getSepEvents().getVerificationsRequired())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * used in APTC calculation for CSR determination.
	 * @param message
	 * @return
	 * @throws ParseException
	 */
	public boolean addNativeAmericanInfo(Message<String> message)throws ParseException {
		boolean isNativeAmericanFamily= false;

		List<net.minidev.json.JSONObject> houseHolds = JsonPath.read(message.getPayload().toString(),"$.singleStreamlinedApplication.taxHousehold[*].householdMember[*]");
		for(net.minidev.json.JSONObject household : houseHolds){
			if((Boolean) ((net.minidev.json.JSONObject)household.get("specialCircumstances")).get("americanIndianAlaskaNativeIndicator") && (Boolean)household.get("applyingForCoverageIndicator") ){
				isNativeAmericanFamily = true;
			}
		}
		return isNativeAmericanFamily;
	}

	public String addFPLValue(Message<String> message)throws ParseException {
		String ret = (String)JsonPath.read(message.getPayload().toString(),"$.payload.Income.incomeFederalPovertyLevelPercent");
		return ret;
	}
}
