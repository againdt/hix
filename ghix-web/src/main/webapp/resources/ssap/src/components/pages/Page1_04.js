import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Form from './Form';
import PageHeader from '../headers/PageHeader';
import UniversalInput from '../../lib/UniversalInput';
import {changeData} from '../../actions/actionCreators';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';

// Components

// ActionCreators

// Utils

// Constants


/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Applicants' Page
 *
 */
const Page1_04 = props => {

    const {
        data,
        data: {
            singleStreamlinedApplication: currentData,
            singleStreamlinedApplication: {
                applyingForhouseHold
            }
        },
        headOfHousehold: {
            name
        }
    } = props;

    /**
     *
     * @param {Object} newData - updated <singleStreamlinedApplication> object
     *
     * Updating <applyingForhouseHold> props in JSON
     */
    const onFieldChange = newData => {
        const newState = Object.assign({}, data);
        newState.singleStreamlinedApplication = newData;
        props.actions.changeData(newState);
    };

    return (
        <Fragment>
            <div id="saPrimaryContact" className="">
                <div className="subsection">
                    <PageHeader
                        text="Who needs health insurance?"
                        headerClassName={'usa-heading-alt'}
                    />
                    <Form>
                        <UniversalInput
                            legend={{
                                legendText: 'Please select one below'
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--03', 'padding-tb-0'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'applyingForhouseHold',
                                value: applyingForhouseHold,
                                options: [
                                    {
                                        value: 'houseHoldContactOnly',
                                        text: `${normalizeNameOfPerson(name)} only`
                                    },
                                    {
                                        value: 'otherFamilyMember',
                                        text: `${normalizeNameOfPerson(name)} and other family members`
                                    },
                                    {
                                        value: 'otherExcludingHousehold',
                                        text: `Other family members, not ${normalizeNameOfPerson(name)}`
                                    }
                                ],
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: []
                            }}
                            errorClasses={['margin-l-20-ve margin-b-10']}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    headOfHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0]
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page1_04.defaultProps = {
    name: 'Page1_04'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page1_04);
