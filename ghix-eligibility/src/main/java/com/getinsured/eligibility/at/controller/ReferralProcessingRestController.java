package com.getinsured.eligibility.at.controller;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.at.mes.queue.dto.ATSpanInfoRequestDTO;
import com.getinsured.eligibility.at.mes.service.AtSpanProcessingService;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.resp.si.dto.AtAdditionalParamsDTO;
import com.getinsured.eligibility.at.service.GenericAtStrategyImpl;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.enums.AccountTransferProcessStatus;
import com.getinsured.eligibility.enums.AccountTransferQueueStatus;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.model.AccountTransferSpanInfo;
import com.getinsured.iex.ssap.model.SsapIntegrationLog;
import com.getinsured.iex.util.ReferralUtil;

/**
 *
 *
 * @author chopra_s
 *
 */
@Controller("referralProcessingRestController")
@RequestMapping("/referralresponse")
public class ReferralProcessingRestController {

	private static final String SUCCCESS = "SUCCCESS";

	private static final String FAILURE = "FAILURE";

	private static final String ACCOUNT_TRANSFER_REFERRAL = "ACCOUNT_TRANSFER_REFERRAL";

	private static final Logger LOGGER = Logger
			.getLogger(ReferralProcessingRestController.class);

	@Autowired private IntegrationLogService integrationLogService;

	@Autowired
	AtSpanProcessingService atSpanProcessingService;
	
	@Autowired
	GIWSPayloadRepository giwsPayloadRepository;
	
	@Autowired private GenericAtStrategyImpl genericAtStrategyImpl;
	
	@RequestMapping(value = "/success", method = RequestMethod.POST)
	@ResponseBody
	public String processSuccess(@RequestBody String actionMappingXmlString) {
		LOGGER.info("ReferralProcessingRestController processSuccess - starts \n actionMappingXmlString - "
				+ actionMappingXmlString);

		ReferralResponse referralResponse = (ReferralResponse) EligibilityUtils
				.unmarshal(actionMappingXmlString);

		final String result = referralResponse.getMessage()
				+ "  - ssapApplicationId: "
				+ referralResponse.getData().get(
						ReferralResponse.SSAP_APPLICATION_ID);

		Long ssap_pk = (Long) referralResponse.getData().get("SSAP_APPLICATION_ID");
		Integer giWsPayloadId = (Integer) referralResponse.getData().get("GI_WS_PAYLOAD_ID");

		saveIntegrationLog(actionMappingXmlString, ACCOUNT_TRANSFER_REFERRAL, SUCCCESS, ssap_pk, giWsPayloadId != null ? giWsPayloadId.longValue() : null);

		String processRequester = (String)referralResponse.getData().get(ReferralProcessingConstants.PROCESSOR_REQUESTER);
		this.updateCurrentAndProcessNextSpan(processRequester, AccountTransferProcessStatus.SUCCESS,referralResponse.getMessage(),ssap_pk);	
		
		return result;
	}

	@RequestMapping(value = "/error", method = RequestMethod.POST)
	@ResponseBody
	public String processError(@RequestBody String errorXmlString) {

		LOGGER.info("ReferralProcessingRestController processError - starts \n errorXmlString - "
				+ errorXmlString);

		ReferralResponse referralResponse = (ReferralResponse) EligibilityUtils
				.unmarshal(errorXmlString);

		LOGGER.info("ReferralProcessingRestController - processError errorReason - "
				+ referralResponse.getMessage());

		Long ssap_pk = (Long) referralResponse.getData().get("SSAP_APPLICATION_ID");
		Integer giWsPayloadId = (Integer) referralResponse.getData().get("GI_WS_PAYLOAD_ID");

		saveIntegrationLog(errorXmlString, ACCOUNT_TRANSFER_REFERRAL, FAILURE, ssap_pk, giWsPayloadId != null ? giWsPayloadId.longValue() : null);
		
		String processRequester = (String)referralResponse.getData().get(ReferralProcessingConstants.PROCESSOR_REQUESTER);
		this.updateCurrentAndProcessNextSpan(processRequester, AccountTransferProcessStatus.FAILED,referralResponse.getMessage(),ssap_pk);

		return "ReferralProcessingRestController - processError errorReason - "
				+ referralResponse.getMessage();
	}
	
	public void saveIntegrationLog(String payload, String serviceName, String status, Long ssap_pk, Long giWsPayloadId) {
		SsapIntegrationLog iL = new SsapIntegrationLog();
		iL.setCreatedDate(new TSDate());
		iL.setPayload(payload);
		iL.setServiceName(serviceName);
		iL.setStatus(status);
		iL.setSsapApplicationId(ssap_pk);
		iL.setGiWsPayloadId(giWsPayloadId);
		integrationLogService.save(iL);
	}

	public void updateCurrentAndProcessNextSpan(String processRequester,AccountTransferProcessStatus status, String errorMessage, long ssapApplicationId){
		String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
		if("true".equalsIgnoreCase(mutipleEligibiltySpanEnabled)){
			ATSpanInfoRequestDTO atSpanInfo = new ATSpanInfoRequestDTO();
			atSpanInfo.setSsapApplicationId(ssapApplicationId);
			atSpanInfo.setErrorMessage(errorMessage);
			atSpanInfo.setProcessStatus(status);
			atSpanInfo.setQueueStatus(ReferralProcessingConstants.REQUESTER_MES_BATCH.equalsIgnoreCase(processRequester) ? AccountTransferQueueStatus.PROCESSED_BATCH : null);
			AccountTransferSpanInfo nextATSpanInfo = atSpanProcessingService.updateCurrentAndFetchNextSpan(atSpanInfo);
			if(nextATSpanInfo != null){
				this.processNextATSpan(nextATSpanInfo);
			}
		}
	}

	private void processNextATSpan(AccountTransferSpanInfo nextATSpanInfo) {
		Integer giwsPayloadId = nextATSpanInfo.getGiWsPayloadId();
		GIWSPayload giwsPayload = giwsPayloadRepository.findById(giwsPayloadId.intValue());
		try {
			AccountTransferRequestPayloadType request = null;
			if(!"NV".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)))
			{
				String requestBody = ReferralUtil.extractSoapBoady(giwsPayload.getRequestPayload());
				request = ReferralUtil.unmarshal(requestBody);
			}
			else
			{
				request = ReferralUtil.getAccountTransferRequestObject(giwsPayload.getRequestPayload());
			}
			
			AtAdditionalParamsDTO atAdditionalParams = new AtAdditionalParamsDTO();
			atAdditionalParams.setGiWsPayloadId(giwsPayloadId);
			atAdditionalParams.setAtSpanInfoId(nextATSpanInfo.getId());
			genericAtStrategyImpl.process(request, atAdditionalParams);
		} catch (Exception e) {
			LOGGER.error("EXCEPTION OCCURRED WHILE PROCESSING NEXT SPAN ID " + nextATSpanInfo.getId(), e);
		}
	}
}
