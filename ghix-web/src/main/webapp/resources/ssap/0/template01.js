import React, { Fragment, PureComponent } from 'react';
import autobind from 'auto-bind';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// import Form from './Form';                                   if we have inputs on UI
// import UniversalInput from '../../lib/UniversalInput';       if we have inputs on UI
// import SubHeader from '../headers/SubHeader';                if we have subheaders on UI
//
// import {  } from '../../actions/actionCreators';             if we need to use actionCreators to update Redux store
//
// import {  } from '../../constants/dropdownOptions';          if we need to use Dropdown component in our UI

class _Name extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {

        };
        autobind(this);
    }
    render () {
        return (
            <Fragment>

            </Fragment>
        );
    }
}

const mapStateToProps = () => ({
    // Here we connect to certain fields in our App store
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        // Here we connect to certain Action Creator which updates our App store
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(_Name);


/// Fragments of code


// const validator = validators.find(v => v.validatorType === this.props.inputData.validationType);
// console.log(value, valueToMatch);
// console.log(validator.verifier);
//
// {validationType && validators.find(v => v.validatorType === validationType).message}


