package com.getinsured.hix.batch.ssap.service;

import com.getinsured.hix.platform.util.exception.GIException;

public interface CleanApplicationVerificationsService {
    void cleanVerificationsAndUpdateApplicationEligibility(String applicationCreationDate) throws GIException;
}
