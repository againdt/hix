import React, { Fragment } from 'react';
import moment from 'moment';

// Components

import SummaryItem from './SummaryItem';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import helperFunctions from '../../utils/helperFunctions';
import SubHeader from '../headers/SubHeader';
import FLOW_TYPES from '../../constants/flowTypes';
import {getOptionsForHardcodedState} from '../../constants/dropdownOptions';



const FamilyAndHouseholdSummary_NV = props => {

    const {
        householdMembers,
        buttonContainer: ButtonContainer,
        flowType,
        PAGES,
        tribes
    } = props;

    return householdMembers.map((member, i) => {
        const {
            applyingForCoverageIndicator,
            dateOfBirth,
            disabilityIndicator,
            fullTimeStudent,
            name,
            gender,
            medicaidChipDenial,
            medicaidDeniedDate,
            medicaidDeniedDueToImmigration,
            parentCaretaker,
            changeInImmigrationSince5Year,
            changeInImmigrationSinceMedicaidDenial,
            blindOrDisabled,
            longTermCare,
            americanIndianAlaskaNative : {
                memberOfFederallyRecognizedTribeIndicator,
                tribeName,
                state
            },
            ethnicityAndRace : {
                hispanicLatinoSpanishOriginIndicator
            },
            socialSecurityCard: {
                firstNameOnSSNCard,
                middleNameOnSSNCard,
                lastNameOnSSNCard,
                suffixOnSSNCard,
                socialSecurityNumber,
                nameSameOnSSNCardIndicator,
                reasonableExplanationForNoSSN
            },
            specialCircumstances: {
                pregnantIndicator,
                numberBabiesExpectedInPregnancy,
                everInFosterCareIndicator,
                ageWhenLeftFosterCare,
                gettingHealthCareThroughStateMedicaidIndicator,
                fosterCareState
            },
            citizenshipImmigrationStatus: {
                citizenshipStatusIndicator,
                naturalizedCitizenshipIndicator,
                lawfulPresenceIndicator,
                livedIntheUSSince1996Indicator,
                honorablyDischargedOrActiveDutyMilitaryMemberIndicator,
                citizenshipDocument: {
                    alienNumber,
                    sevisId,
                    nameOnDocument,
                    documentDescription,
                    nameSameOnDocumentIndicator,
                    visaNumber,
                    documentExpirationDate,
                    i94Number,
                    foreignPassportCountryOfIssuance,
                    cardNumber
                },
                otherImmigrationDocumentType: {
                    orrcertificationIndicator,
                    cubanHaitianEntrantIndicator,
                    orreligibilityLetterIndicator,
                    stayOfRemovalIndicator,
                    americanSamoanIndicator,
                    withholdingOfRemovalIndicator
                }
            },
        } = member;
        const NameOnSSNCard = {
            firstName: firstNameOnSSNCard,
            middleName: middleNameOnSSNCard,
            lastName: lastNameOnSSNCard,
            suffix: suffixOnSSNCard
        };

        const fiveYearsAgo = moment().subtract(5, 'years').format('YYYY');
        const age = moment().diff(dateOfBirth, 'years');
        const {
            stateFullNameForSummary,
            tribeNameForSummary
        } = (state => state ? { stateFullNameForSummary: state.name, tribeNameForSummary: state.tribes.find(t => t.tribeCode === tribeName).tribeName  }: {})(tribes.find(tr => tr.code === state));
        const [ stateAbbr ] = fosterCareState ? getOptionsForHardcodedState(fosterCareState) : [{}];
        const isFullTimeStudent = () => {
            console.log('applyingForCoverageIndicator',applyingForCoverageIndicator);
            const age = moment().diff(dateOfBirth, 'years');
            return age >= 18 && age <= 22 && applyingForCoverageIndicator;
        };
        console.log('isFullTimeStudent', isFullTimeStudent());
        return (
            <div className='summary__block' key={`${member.name.firstName}${i}`}>
                <div className="usa-grid usa-width-one-whole summary__header">
                    <SubHeader
                        text={
                            <span>
                                {normalizeNameOfPerson(name)}
                                {i === 0 ? '(Primary Contact)' : ''}
                            </span>
                        }
                        headerClassName={'usa-heading-alt'}
                        additionalElement={(
                            <ButtonContainer
                                type="editButton_secondary"
                                args={[i, PAGES.PERSONAL_INFORMATION_PAGE]}
                            />
                        )}
                        additionalElementClassName={'header-btn'}
                    />
                </div>
                <SummaryItem
                    textCode="label.applyingForCoverage"
                    value={applyingForCoverageIndicator ? 'Yes' : 'No'}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                <SummaryItem
                    textCode="label.gender"
                    value={`${gender && gender.slice(0, 1).toUpperCase()}${gender && gender.slice(1)}`}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                <SummaryItem
                    textCode="label.ssn"
                    value={helperFunctions.getValueForSSN(socialSecurityNumber, reasonableExplanationForNoSSN)}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                <SummaryItem
                    textCode="label.ssn.is.name"
                    value={nameSameOnSSNCardIndicator  ? 'Yes' : 'No'}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}
                />
                {
                    nameSameOnSSNCardIndicator !== null && !nameSameOnSSNCardIndicator &&
                    <SummaryItem
                        textCode="label.ssn.name.shown"
                        value={normalizeNameOfPerson(NameOnSSNCard)}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />

                }
                {
                    nameSameOnSSNCardIndicator === null || nameSameOnSSNCardIndicator &&
                    <SummaryItem
                        textCode="label.ssn.name.shown"
                        value={normalizeNameOfPerson(name)}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />

                }
                {applyingForCoverageIndicator &&
                    <Fragment>
                        <SummaryItem
                            textCode="label.citizen.us"
                            value={citizenshipStatusIndicator ? 'Yes' : 'No'}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />

                        <SummaryItem
                            textCode="label.citizen.naturalized"
                            value={naturalizedCitizenshipIndicator ? 'Yes' : 'No'}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        {
                            alienNumber &&
                            <SummaryItem
                                textCode="label.alien.number"
                                value={alienNumber}
                                classes2={{
                                    spanClass: 'className1 className001',
                                    divClass1: 'summary__label',
                                    divClass2: 'summary__value'
                                }}
                            />
                        }
                        {
                            sevisId &&
                            <SummaryItem
                                textCode="label.naturalization.number"
                                value={sevisId}
                                classes2={{
                                    spanClass: 'className1 className001',
                                    divClass1: 'summary__label',
                                    divClass2: 'summary__value'
                                }}
                            />
                        }

                        {
                            !citizenshipStatusIndicator &&

                            <SummaryItem
                                textCode="label.immigration.status"
                                value={lawfulPresenceIndicator ? 'Yes' : 'No'}
                                classes2={{
                                    spanClass: 'className1 className001',
                                    divClass1: 'summary__label',
                                    divClass2: 'summary__value'
                                }}
                            />
                        }
                        {lawfulPresenceIndicator &&
                        <Fragment>
                            {
                                documentDescription &&
                                <SummaryItem
                                    textCode="label.document.description"
                                    value={documentDescription}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                            {
                                visaNumber &&
                                <SummaryItem
                                    textCode="label.visa.number"
                                    value={visaNumber}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                            {
                                foreignPassportCountryOfIssuance &&
                                <SummaryItem
                                    textCode="label.passport.foreign"
                                    value={foreignPassportCountryOfIssuance}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                            {
                                documentExpirationDate &&
                                <SummaryItem
                                    textCode="label.document.expiration.date"
                                    value={moment(documentExpirationDate).format('MM/DD/YYYY')}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                            {
                                i94Number &&
                                <SummaryItem
                                    textCode="label.i94.number"
                                    value={i94Number}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                            {
                                cardNumber &&
                                <SummaryItem
                                    textCode="label.card.number"
                                    value={cardNumber}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                            {
                                orrcertificationIndicator &&
                                <SummaryItem
                                    textCode="ssap.familyHousehold.cuban.orrCertification"
                                    value={orrcertificationIndicator ? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                            }
                            {
                                cubanHaitianEntrantIndicator &&
                                <SummaryItem
                                    textCode="label.cuban.haitian"
                                    value={cubanHaitianEntrantIndicator ? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                            }
                            {   orreligibilityLetterIndicator &&
                                <SummaryItem
                                    textCode="label.refugee.resettlement"
                                    value={orreligibilityLetterIndicator ? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                            }
                            {stayOfRemovalIndicator &&
                                <SummaryItem
                                    textCode="label.staying.issued"
                                    value={stayOfRemovalIndicator ? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                            }
                            {americanSamoanIndicator &&
                                <SummaryItem
                                    textCode="label.american.samoa"
                                    value={americanSamoanIndicator ? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                            }
                            {
                                withholdingOfRemovalIndicator &&
                                <SummaryItem
                                    textCode="label.withholding.removal"
                                    value={withholdingOfRemovalIndicator? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                            }
                            {
                                nameSameOnDocumentIndicator !== null && !nameSameOnDocumentIndicator &&
                                <SummaryItem
                                    textCode="label.name.shown.document"
                                    value={normalizeNameOfPerson(nameOnDocument)}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                            {
                                livedIntheUSSince1996Indicator &&
                                <SummaryItem
                                    textCode="label.residence.primary.since.1996"
                                    value={livedIntheUSSince1996Indicator ? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                            {
                                honorablyDischargedOrActiveDutyMilitaryMemberIndicator &&
                                <SummaryItem
                                    textCode="label.military.discharged"
                                    value={honorablyDischargedOrActiveDutyMilitaryMemberIndicator ? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />

                            }
                        </Fragment>
                        }
                    </Fragment>
                }
                {
                    <SummaryItem
                        textCode="label.hispanic.latino.origin"
                        value={hispanicLatinoSpanishOriginIndicator ? 'Yes' : 'No'}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />
                }
                {
                    <SummaryItem
                        textCode='label.american.indian.alaskan.native'
                        value= {memberOfFederallyRecognizedTribeIndicator ? 'Yes' : 'No'}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />
                }
                {
                    memberOfFederallyRecognizedTribeIndicator &&
                    <Fragment>
                        <SummaryItem
                            textCode="label.membership.info"
                            value=""
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        <SummaryItem
                            textCode="label.state"
                            value={stateFullNameForSummary}
                            classes="grid-offset-1"
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        <SummaryItem
                            textCode="label.tribe.name"
                            value={tribeNameForSummary}
                            classes="grid-offset-1"
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                    </Fragment>
                }

                {flowType === FLOW_TYPES.FINANCIAL_FLOW &&
                <Fragment>
                    { parentCaretaker &&
                        <SummaryItem
                            textCode='label.parentCaretaker'
                            value= {parentCaretaker ? 'Yes' : 'No'}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                    }
                    {applyingForCoverageIndicator &&
                    <Fragment>
                        <SummaryItem
                            textCode="label.medicaid.not.eligible"
                            value={medicaidChipDenial ? 'Yes' : 'No'}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                        {
                            medicaidChipDenial &&
                            <Fragment>
                                <SummaryItem
                                    textCode="label.medicaid.denied.chip"
                                    value={moment(medicaidDeniedDate).format('MM/DD/YYYY')}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                                <SummaryItem
                                    category={`Was found not eligible for Medicaid or CHIP based on immigration status
                                            since ${fiveYearsAgo}`}
                                    value={medicaidDeniedDueToImmigration ? 'Yes' : 'No'}
                                    classes2={{
                                        spanClass: 'className1 className001',
                                        divClass1: 'summary__label',
                                        divClass2: 'summary__value'
                                    }}
                                />
                                {medicaidDeniedDueToImmigration &&
                                    <Fragment>
                                        <SummaryItem
                                            category={`Had a change in immigration status since ${fiveYearsAgo}`}
                                            value={changeInImmigrationSince5Year ? 'Yes' : 'No'}
                                            classes2={{
                                                spanClass: 'className1 className001',
                                                divClass1: 'summary__label',
                                                divClass2: 'summary__value'
                                            }}
                                        />
                                        { changeInImmigrationSince5Year &&
                                        <SummaryItem
                                            textCode="label.immigration.status.change.medicaid"
                                            value={changeInImmigrationSinceMedicaidDenial ? 'Yes' : 'No'}
                                            classes2={{
                                                spanClass: 'className1 className001',
                                                divClass1: 'summary__label',
                                                divClass2: 'summary__value'
                                            }}
                                        />
                                        }
                                    </Fragment>
                                }

                            </Fragment>
                        }
                    </Fragment>
                    }

                    {
                        gender && gender.toUpperCase() === 'FEMALE' &&
                        <SummaryItem
                            textCode="label.pregnant.is"
                            value={pregnantIndicator ? 'Yes' : 'No'}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                    }
                    { pregnantIndicator &&
                    <SummaryItem
                        textCode="label.pregnant.num.babies"
                        value={ numberBabiesExpectedInPregnancy}
                        classes2={{
                            spanClass: 'className1 className001',
                            divClass1: 'summary__label',
                            divClass2: 'summary__value'
                        }}
                    />
                    }
                    {
                        applyingForCoverageIndicator &&
                        <SummaryItem
                            textCode="label.disability.has"
                            value={disabilityIndicator ? 'Yes' : 'No'}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                    }

                    {
                        applyingForCoverageIndicator && age >= 18 && age <= 25 &&
                        <Fragment>
                            <SummaryItem
                                textCode="label.foster.care"
                                value={everInFosterCareIndicator ? 'Yes' : 'No'}
                                classes2={{
                                    spanClass: 'className1 className001',
                                    divClass1: 'summary__label',
                                    divClass2: 'summary__value'
                                }}
                            />
                            {
                                everInFosterCareIndicator &&
                                <Fragment>
                                    <SummaryItem
                                        textCode="label.foster.care.system.state"
                                        value={stateAbbr.text}
                                        classes2={{
                                            spanClass: 'className1 className001',
                                            divClass1: 'summary__label',
                                            divClass2: 'summary__value'
                                        }}
                                    />
                                    <SummaryItem
                                        textCode="label.health.medicaid"
                                        value={gettingHealthCareThroughStateMedicaidIndicator ? 'Yes' : 'No'}
                                        classes2={{
                                            spanClass: 'className1 className001',
                                            divClass1: 'summary__label',
                                            divClass2: 'summary__value'
                                        }}
                                    />
                                    <SummaryItem
                                        textCode="label.foster.care.age"
                                        value={ageWhenLeftFosterCare}
                                        classes2={{
                                            spanClass: 'className1 className001',
                                            divClass1: 'summary__label',
                                            divClass2: 'summary__value'
                                        }}
                                    />
                                </Fragment>
                            }
                        </Fragment>
                    }


                    { isFullTimeStudent() &&
                        <SummaryItem
                            textCode='label.fullTimeStudent'
                            value= {fullTimeStudent ? 'Yes' : 'No'}
                            classes2={{
                                spanClass: 'className1 className001',
                                divClass1: 'summary__label',
                                divClass2: 'summary__value'
                            }}
                        />
                    }
                    
                    {applyingForCoverageIndicator &&
                        <Fragment>
                            <SummaryItem
                                textCode="label.disability.physical.mental.health"
                                value={blindOrDisabled ? 'Yes' : 'No'}
                                classes2={{
                                    spanClass: 'className1 className001',
                                    divClass1: 'summary__label',
                                    divClass2: 'summary__value'
                                }}
                            />
                            <SummaryItem
                                textCode="label.activities.help"
                                value={longTermCare ? 'Yes' : 'No'}
                                classes2={{
                                    spanClass: 'className1 className001',
                                    divClass1: 'summary__label',
                                    divClass2: 'summary__value'
                                }}
                            />
                        </Fragment>
                    }
                </Fragment>
                }
            </div>

        );
    });
};

export default FamilyAndHouseholdSummary_NV;
