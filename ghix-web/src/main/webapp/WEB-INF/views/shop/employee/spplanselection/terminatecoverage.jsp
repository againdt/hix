<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/shop-enrollment-data" prefix="enrollData"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link href="/hix/resources/css/datepicker.css" rel="stylesheet"  type="text/css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker-new.js" />"></script>
<jsp:useBean id="now" class="java.util.Date" />
<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${employeeApplication.name}</span></h1>
  <div class="row-fluid">
    
    <div class="span3" id="sidebar">
      <util:myStuff employeeApplication="${employeeApplication}"></util:myStuff>
      <util:quickLinks employeeApplication="${employeeApplication}"></util:quickLinks>
    </div><!--/span3-->
    
	<div id="rightpanel" class="span9">   
	
		<div class="header">  
			<h4>My Plans Terminate 
			<c:if test="${insuranceType=='HEALTH' }">	Health and </c:if> 
			<c:if test="${insuranceType=='HEALTH' || insuranceType=='DENTAL' }">Dental </c:if>
				coverage</h4>        
		</div>

		<div class="gutter10">
		<!--Health Plan-->
			<c:forEach var="planData" items="${employeePlanMetaData}">
			<div id="cov${insuranceType}plan" class="span6 info-block">
				  <div class="gutter10">
					   <div class="header"  style='height:auto;'><h4>${planData.enrolleeNames}</h4></div>
					   <div class="gutter10 "> 
						  <div id="plan-selected" class="alert alert-info">
							<div class="info-header center">
								<img src="${planData.logoStr}" alt="${planData.issuerName}" class="resize-img carrierlogo">
								<p><a href="#">${planData.planName}</a></p>
								<h5 class="payment">${planData.monthlyPremium}/mo</h5>
							</div>
							<!--Mask If the plan is terminated-->
							<c:if test="${planData.enrollmentStatus=='TERM'}">
							<div class="mask" id="plan-mask-sample"><img alt="This enrollment is terminated" src="/hix/resources/img/terminate_stamp.png"></div>
							</c:if>
							
							<div class="data-display well">
								 <dl class="dl-horizontal">
									<dt>Plan Type:</dt>
									<dd id="plan-type">${planData.planType}</dd>
									<dt>Office Visit:</dt>
									<dd id="off-visit">${planData.officeVisit}</dd>
									<dt>Generic Medications:</dt>
									<dd id="generic-medic">${planData.genericMedications}</dd>
									<dt>Deductible:</dt>
									<dd id="deductible">${planData.deductible}</dd>
									<dt>Max Out-of-Pocket:</dt>
									<dd id="m-o-p">${planData.oopMax}</dd>
								  </dl>
							</div> 
							<div class="center">
					        	<a href="${planData.PlanUrl}" data-toggle="modal" id="viewBenifitDetails_${planData.planId}" class="btn" alt="View Benifit Details" target="_blank">
					        		View Benefit Details
					        	</a>
	      					</div>                   
						  </div>
						  <div class="gutter10-tb">
								
						  </div>
						</div>               
				  </div> <!-- .gutter10-->
			</div>
			</c:forEach>
			
			<form class="form-horizontal" id="formterminateemp" name="formterminateemp" action="<c:url value='/shop/employee/spplanselection/terminatecoverageSubmit'/>" method="post" >
			<df:csrfToken/>
			<input type="hidden" id="insuranceType" name="insuranceType" value="${insuranceType}" />
			<input type="hidden" name="applicationId" id="applicationId" value="${employeeApplication.id}"/>
			<div class="row-fluid">
				
				<div class="gutter10 alert-error margin20-b">             
						<p><span><strong>Please Note: </strong>A health plan can be terminated only when you experience a qualifying event.If you have experienced one please select an event and the date to continue</span></p>            
				</div>
						
					<div class="control-group">
						<label class="control-label" for="planLevel">Event</label>
						<div class="controls">
								<select id="eventName" name="eventName" class="input-large pull-left" >
								    <option value="">Select Event </option>
						 			<option value="Marriage">Marriage</option>
						  			<option value="Birth">Birth</option>
						  			<option value="Adoption">Adoption</option>
						  			<option value="Divorce">Divorce</option>
						  			<option value="Death">Death of a Family Member</option>
						  			<option value = "TerminationofBenefit">Termination of Benefits</option>
						  			<c:if test="${nativeAmr == true}">
						  				<option value = "PlanChange">Plan Change</option>
						  			</c:if>
								</select>
								<span id="eventName_error"></span>
						</div>
					</div>
							
					<div class="control-group">
						<label class="control-label" for="planLevel">Date</label>
						<div class="controls">
							<div class="input-append date date-picker" data-date="" data-date-format="mm/dd/yyyy">
							<input class="input-small" type="text" name="eventDate" id="eventDate">
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<span id="eventDate_error"></span>
						</div>
					</div>
						
					<div class="control-group">
						<input type="checkbox" id="employeeAttestation" name="attestation" class="pull-left">
						<label for="attestation" class="margin20-l">I attest that I want to terminate employer coverage, and I understand that I will be unable to enroll again until the next open enrollment season, unless I experience a qualifying agent .</label>
						<span id="employeeAttestation_error"></span>
					</div>
								
					<div class="control-group">
							<label class="control-label">Your Signature</label>
						<div class="controls">
							<input type="text" class="input-large" id="esignName" name="esignName" size="30" placeholder="John Doe">
						</div>
						<div id="esignName_error" style="width: 1800px;"></div>
					</div>
							
					<div class="control-group">
						<label class="control-label" for="planLevel">Date</label>
						<div class="controls">
							<input type="text" name="name" id="esignDate" class="input-small" value="<fmt:formatDate value="${now}" type="both" pattern="MM/dd/yyyy" />" readonly/>
						</div>
					</div>
			  </div>
			
			<div class="form-actions">
				<input name="enrollIds" value="${enrollmentIds}" type="hidden"/>
				<a class="btn" href="/hix/shop/employee/home/${employeeApplication.id}">Cancel</a>
				<a class="btn btn-primary" href="javascript:formsubmit();">Confirm Termination</a>
			</div>	

		</form>	
		</div>
  
	</div><!-- Right Panel-->
  </div><!--/row-fluid-->    
</div><!--/main .gutter10-->

<script type="text/javascript">
$(document).ready(function(){
	
	   $('.date-picker').datepicker({
			startDate: '-30d',
			endDate:'+0d',
			format:'mm/dd/yyyy',
			autoclose:true,
			forceParse: false
	   });
});

function imgSize(){
    $(".carrierlogo").each(function(){
        var width = $(this).width();
        var height = $(this).height();
        var maxWidth = 160;
		var maxHeight = 60;
		
		// Check if the current width is larger than the max
	    if(width > maxWidth){
	        ratio = maxWidth / width; // get ratio for scaling image
	        $(this).width(maxWidth); // Set new width
	        $(this).height((height * ratio)); // Scale height based on ratio
	        height = height * ratio; // Reset height to match scaled image
	    	width = width * ratio; // Reset width to match scaled image
	    }
		
		// Check if current height is larger than max
		if(height > maxHeight){                                     
		  ratio = maxHeight / height; // get ratio for scaling image
		  $(this).height(maxHeight); // Set new height
		  $(this).width((width * ratio)); // Scale width based on ratio    
		}
	    
		$(this).css('display','block');
    });
}
var validator = $("#formterminateemp").validate({ 
	
 	onkeyup: false,
	onclick: false,
	onfocusout: false,
	onSubmit: true,
	
	rules : {'eventName' : { required : true},
 			 'eventDate' :{required : true, date: true},
			 'esignName' : {required : true},
			 'esignDate' : {required:true},
			 'attestation' : {required: true} 
	},
	messages : {
		'eventName' : {  required : "<span> <em class='excl'>!</em>Please select an Event</span>"},
		'eventDate': {required :  "<span> <em class='excl'>!</em>Plese select an Event Date</span>", 
		'date':  "<span> <em class='excl'>!</em>Plese select a valid Event Date</span>"},
		'esignName': { required : "<span> <em class='excl'>!</em>Please enter your first name and last name </span>"},
		'attestation' :  { required : "<span> <em class='excl'>!</em>Please accept all Attestation </span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class',	'error span10');
	}	
});

function formsubmit(){
	$('#formterminateemp').submit();
}

</script>

