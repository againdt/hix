package com.getinsured.eligibility.qlevalidation;

import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;

import com.getinsured.eligibility.at.ref.dto.EnrollmentAttributesDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.model.SepEvents.ChangeType;
import com.getinsured.hix.model.Plan;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;


public final class QLEAutomationChangesHandlerDetermination {

	private static final Logger LOGGER = Logger.getLogger(QLEAutomationChangesHandlerDetermination.class);

	private QLEAutomationChangesHandlerDetermination() {
	}

	private static final int CHANGE_IN_CSR_LEVEL_INDEX = 0;
	private static final int ADD_INDEX = 1;
	private static final int REMOVE_INDEX = 2;
	private static final int UPDATED_ZIP_COUNTY_INDEX = 3;
	private static final int CHANGE_IN_CITIZENSHIP_STATUS_INDEX = 4;
	private static final int CHANGE_IN_BLOOD_RELATION_INDEX = 5;
	private static final int UPDATED_DOB_INDEX = 6;
	
	private static final int CHANGE_SIZE = 7;

	public enum AllChangesHandlerType {
		SINGLE, MIXED, MANUAL, DENIAL, PURE_REMOVE, CS_CHANGE;
	}
	
	public static AllChangesHandlerType handlerToInvoke(EnrollmentAttributesDTO enrolledApplicationAttributes, SsapApplicationEvent applicationEvent){
		
		final List<SsapApplicantEvent> applicantEvents = applicationEvent.getSsapApplicantEvents();
		
		int[] changeCountDecisionTable = populateChangeCountDecisionTable(applicantEvents);
		
		if (LOGGER.isInfoEnabled()){
			LOGGER.info("CHANGE COUNT DECISION TABLE - " +  Arrays.toString(changeCountDecisionTable));
		}
		
		if (!SsapEnrolleeHandler.isEnrolledForHealth(enrolledApplicationAttributes) 
				|| checkIfCatastrophicPlan(enrolledApplicationAttributes)
				|| isAddRemoveDetected(changeCountDecisionTable)
				|| isAddAddressDetected(changeCountDecisionTable) || isRemoveAddressDetected(changeCountDecisionTable)){
			if (LOGGER.isInfoEnabled()){
				LOGGER.info("Manual flow detected - Consumer need to manual perform enrollment - Either dental only or catastrophic or add/remove together.");
			}
			return AllChangesHandlerType.MANUAL;
		}
		
		if (runCsChangeRulesAndDetermineAutomation(changeCountDecisionTable, enrolledApplicationAttributes)){
			if (LOGGER.isInfoEnabled()){
				LOGGER.info("CSR Automation detected, preparing for IND71 call.");
			}
			
			return AllChangesHandlerType.CS_CHANGE;
		}
		
		if (checkPureRemove(changeCountDecisionTable)){
			if (!checkIfCatastrophicPlan(enrolledApplicationAttributes) && SsapEnrolleeHandler.isEnrolledForHealth(enrolledApplicationAttributes)) {
				if (LOGGER.isInfoEnabled()){
					LOGGER.info("Multiple remove applicants determined and proceed with automation.");
				}
				return AllChangesHandlerType.PURE_REMOVE;
			}
		}
		
		//if (isSingleAddDetected(changeCountDecisionTable) || isPureZipCountyDetected(changeCountDecisionTable)){
		if (isSingleAddDetected(changeCountDecisionTable) && changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] == 0 && !checkIfCatastrophicPlan(enrolledApplicationAttributes)){
			if (LOGGER.isInfoEnabled()){
				LOGGER.info("Single change detected - Add and proceed with automation.");
			}
			return AllChangesHandlerType.SINGLE;
		} 
		
		if (isPureZipCountyDetected(changeCountDecisionTable) && changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] == 0 && !checkIfCatastrophicPlan(enrolledApplicationAttributes)){
			if (LOGGER.isInfoEnabled()){
				LOGGER.info("Only Zip change detected - proceed with automation.");
			}
			return AllChangesHandlerType.SINGLE;
		} 
		
		if (LOGGER.isInfoEnabled()){
			LOGGER.info("Manual flow detected - Consumer need to manual perform enrollment.");
		}
		return AllChangesHandlerType.MANUAL;
			
	}

	private static int[] populateChangeCountDecisionTable(List<SsapApplicantEvent> applicantEvents) {
		int[] changeCountDecisionTable = new int[CHANGE_SIZE];
		
		for (SsapApplicantEvent applicantEvent : applicantEvents) {
			if (ChangeType.valueOf(applicantEvent.getSepEvents().getChangeType()) == ChangeType.ADD){
				changeCountDecisionTable[ADD_INDEX] = ++changeCountDecisionTable[ADD_INDEX];
			} else if (ChangeType.valueOf(applicantEvent.getSepEvents().getChangeType()) == ChangeType.REMOVE) {
				changeCountDecisionTable[REMOVE_INDEX] = ++changeCountDecisionTable[REMOVE_INDEX];
			} else if (ChangeType.valueOf(applicantEvent.getSepEvents().getChangeType()) == ChangeType.OTHER){
				/* For other, refer to applicant events to populate change matrix */
				// TODO: may need to revisit this logic based on events instead of applicant status. For R4.7 we have only single add case.
				ApplicantStatusEnum temp = ApplicantStatusEnum.fromValue(applicantEvent.getSsapApplicant().getStatus());
				if (ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL == temp){
					changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] = ++changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX];
				//} else if (ApplicantStatusEnum.UPDATED_ZIP_COUNTY == temp){
				} else if ("CHANGE_IN_ADDRESS".equalsIgnoreCase(applicantEvent.getSepEvents().getName())){
					changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] = ++changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX];
				} else if (ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS == temp){
					changeCountDecisionTable[CHANGE_IN_CITIZENSHIP_STATUS_INDEX] = ++changeCountDecisionTable[CHANGE_IN_CITIZENSHIP_STATUS_INDEX];
				} else if (ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION == temp) {
					changeCountDecisionTable[CHANGE_IN_BLOOD_RELATION_INDEX] = ++changeCountDecisionTable[CHANGE_IN_BLOOD_RELATION_INDEX];
				} else if (ApplicantStatusEnum.UPDATED_DOB == temp) {
					changeCountDecisionTable[UPDATED_DOB_INDEX] = ++changeCountDecisionTable[UPDATED_DOB_INDEX];
				}
			}
		}
		
		return changeCountDecisionTable;
	}
	
	private static boolean runCsChangeRulesAndDetermineAutomation(int[] changeCountDecisionTable, 
		EnrollmentAttributesDTO enrolledApplicationAttributes){
		
		boolean decision = false;
		//StringUtils.equalsIgnoreCase(ReferralConstants.CS1, enrolledApplicationAttributes.getCostSharing()) ||
		//Removed this OR condition as part of HIX-106303
		if (isCsrChangeDetected(changeCountDecisionTable)){
			if ( (enrolledApplicationAttributes.getHealthPlanId() == 0 &&  enrolledApplicationAttributes.getDentalPlanId() == 0)
					|| checkIfCatastrophicPlan(enrolledApplicationAttributes)
					|| isNonAutomationChange(changeCountDecisionTable) 
					|| isAddRemoveDetected(changeCountDecisionTable)
					|| isAddAddressDetected(changeCountDecisionTable) || isRemoveAddressDetected(changeCountDecisionTable)){
				decision = false;
			} else if (isSingleAddDetected(changeCountDecisionTable)) {
				decision = true;
			} else if (isRemovesDetected(changeCountDecisionTable)){
				decision = true;
			} else if (isPureZipCountyDetected(changeCountDecisionTable)){
				decision = true;
			} else if (isPureCsrChange(changeCountDecisionTable)) {
				decision = true;
			}
		}
		
		return decision;
		
	}
	
	private static boolean isNonAutomationChange(int[] changeCountDecisionTable) {
		return changeCountDecisionTable[CHANGE_IN_CITIZENSHIP_STATUS_INDEX] != 0
				|| changeCountDecisionTable[CHANGE_IN_BLOOD_RELATION_INDEX] != 0 || changeCountDecisionTable[UPDATED_DOB_INDEX] != 0;
	}
	
	private static boolean isPureZipCountyDetected(int[] changeCountDecisionTable) {
		return changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] != 0
				&& changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] == 0
				&& changeCountDecisionTable[ADD_INDEX] == 0 && changeCountDecisionTable[REMOVE_INDEX] == 0
				&& changeCountDecisionTable[CHANGE_IN_CITIZENSHIP_STATUS_INDEX] == 0
				&& changeCountDecisionTable[CHANGE_IN_BLOOD_RELATION_INDEX] == 0
				&& changeCountDecisionTable[UPDATED_DOB_INDEX] == 0;
	}
	
	private static boolean isSingleAddDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) 
				&& changeCountDecisionTable[ADD_INDEX] == 1 
				&& changeCountDecisionTable[REMOVE_INDEX] == 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] == 0;
	}
	
	private static boolean isRemovesDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] == 0 && changeCountDecisionTable[REMOVE_INDEX] != 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] == 0;
	}
	
	private static boolean isAddRemoveDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] != 0 && changeCountDecisionTable[REMOVE_INDEX] != 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] == 0;
	}
	
	private static boolean isAddAddressDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] != 0 && changeCountDecisionTable[REMOVE_INDEX] == 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] != 0;
	}
	
	private static boolean isRemoveAddressDetected(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) && changeCountDecisionTable[ADD_INDEX] == 0 && changeCountDecisionTable[REMOVE_INDEX] != 0
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] != 0;
	}
	
	private static boolean isPureCsrChange(int[] changeCountDecisionTable) {
		return !isNonAutomationChange(changeCountDecisionTable) 
				&& changeCountDecisionTable[ADD_INDEX] == 0  && changeCountDecisionTable[REMOVE_INDEX] == 0 
				&& changeCountDecisionTable[UPDATED_ZIP_COUNTY_INDEX] == 0
				&& changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] != 0;
	}

	private static boolean isCsrChangeDetected(int[] changeCountDecisionTable) {
		return changeCountDecisionTable[CHANGE_IN_CSR_LEVEL_INDEX] != 0;
	}


	private static boolean checkIfCatastrophicPlan(EnrollmentAttributesDTO enrolledApplicationAttributes) {
		return SsapEnrolleeHandler.isEnrolledForHealth(enrolledApplicationAttributes) && Plan.PlanLevel.CATASTROPHIC.toString().equals(enrolledApplicationAttributes.getHealthPlanLevel());
	}
	
	private static boolean checkPureRemove(int[] changeCountDecisionTable) {
		return isRemovesDetected(changeCountDecisionTable) && !isCsrChangeDetected(changeCountDecisionTable);
	}

}
