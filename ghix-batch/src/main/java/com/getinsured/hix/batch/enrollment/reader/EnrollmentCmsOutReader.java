package com.getinsured.hix.batch.enrollment.reader;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.service.EnrollmentCmsOutService;

public class EnrollmentCmsOutReader  implements ItemReader<Integer> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentCmsOutReader.class);
	private EnrollmentCmsOutService enrollmentCmsOutService;
	List<Integer> enrollmentIdList;	
	Integer partition;
	Integer issuerId;
	Integer year;
	Integer month;
	int loopCount;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			partition =ec.getInt("partition");
			issuerId=ec.getInt("issuerId");
			year=ec.getInt("year");
			month =ec.getInt("month");

			if(null != issuerId && null != year){
				enrollmentIdList= enrollmentCmsOutService.getEnrollmentIdByIssuerAndYear(issuerId, year);
			}
			if(null == enrollmentIdList || enrollmentIdList.isEmpty()){
				enrollmentCmsOutService.logInAactiveIssuer(issuerId, year, month, (Integer)ec.getInt("batchYear"), stepExecution.getJobExecutionId());
			}
			LOGGER.info("EnrollmentCmsOutReader : Thread Name: "+Thread.currentThread().getName() +" IssuerId: "+issuerId+" Year: "+ year  +" No Of Enrollment: " +enrollmentIdList.size());
		}
	}

	@Override
	public Integer read() throws Exception, UnexpectedInputException,ParseException, NonTransientResourceException {
		if(null != enrollmentIdList && ! enrollmentIdList.isEmpty()){
			if(loopCount<enrollmentIdList.size()){
				loopCount++;
				return enrollmentIdList.get(loopCount-1);
				
			}else{
				return null;
			}
		}
		return null;
	}
	
	public EnrollmentCmsOutService getEnrollmentCmsOutService() {
		return enrollmentCmsOutService;
	}

	public void setEnrollmentCmsOutService(EnrollmentCmsOutService enrollmentCmsOutService) {
		this.enrollmentCmsOutService = enrollmentCmsOutService;
	}

}
