(function() {
'use strict';
    angular.module('brokerAdminApp')
    .controller('AgencyCtrl', AgencyCtrl);

    AgencyCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'truncFilter', 'timeValidateFilter', 'dataStorageService', '$translate'];

    function AgencyCtrl($scope, $http, $state, $stateParams, truncFilter, timeValidateFilter, dataStorageService, $translate) {
        var vm = this;
        var switchParam1 = "/hix/admin/broker/dashboard?switchToModuleName=agency_manager&switchToModuleId=";
        var switchParam2 = "&switchToResourceName=";

        $scope.callViewAgencyAccountModal = callViewAgencyAccountModal;
        $scope.viewAgencyAccountModalPath = "/hix/resources/html/agency/templates/viewAgencyAccountModal.html";
        vm.isShowMsgCheckboxChecked = isShowMsgCheckboxChecked;
        vm.switchToAgencyView = switchToAgencyView;
        vm.switchURL = "";

        vm.showPopupInFutureChecked = $("#showPopupInFutureForAgency").val();
        vm.agencyName = dataStorageService.getItem('agencyName');
        vm.encryptedAgencyId = dataStorageService.getItem('encryptedAgencyId');
        vm.agencyName = dataStorageService.getItem('businessLegalName');
        vm.noPopupUrl = switchParam1 + vm.encryptedAgencyId + switchParam2 + vm.agencyName + "&showPopupInFuture=N";
        function callViewAgencyAccountModal() {
        	$('#viewAgencyAccountModal').modal('show');
        }
        function switchToAgencyView() {
            vm.isShowMsgCheckboxChecked(vm.encryptedAgencyId, vm.agencyName);
            window.location.href = vm.switchURL;

        }
        function isShowMsgCheckboxChecked(encryptedAgencyId, agencyName) {
            if($("#showMsgInFuture").attr('checked')) {
                vm.switchURL = switchParam1+ encryptedAgencyId + switchParam2+ agencyName+"&showPopupInFuture=N";
            } else {
                vm.switchURL = switchParam1+ encryptedAgencyId + switchParam2+ agencyName+"&showPopupInFuture=Y";
            }

        }
    }
})();
