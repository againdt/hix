(function() {
    'use strict';

    angular.module('datepickerComponent', [])
        .directive('datepicker', datepicker);

    function datepicker() {
        var ddo = {
            require: 'ngModel',
            link : function(scope, element, attr, ngModel) {
                element.datepicker({
                    onSelect: function(dateText) {
                        scope.$apply(function() {
                            ngModel.$setViewValue(dateText);
                        });
                    }
                });
            }
        };

        return ddo;
    }


})();
