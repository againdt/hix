package com.getinsured.eligibility.lce.service;

import java.util.List;

import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepTransientDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

public interface LifeChangeEventApplicantEventService {
	
	List<SsapApplicantEvent> cloneSsapApplicantEvents(SsapApplication currentApplication, SsapApplicationEvent ssapApplicationEvent, List<SsapApplicant> ssapApplicants, List<RequestParamDTO> events, SepTransientDTO sepTransientDTO) throws Exception;
	
	List<SsapApplicantEvent> getApplicantEventByApplicationCaseNumber(String caseNumber);
	
	List<SsapApplicantEvent> getSsapApplicantEvents(long ssapApplicationId,String sepEventName);
	
	String saveAddTaxDepReason(List<SsapApplicantEvent> ssapApplicantEventList, String targetName, String reasonText,String commenterName,long commentedBy)throws Exception;
}
