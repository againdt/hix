package com.getinsured.eligibility.at.ref.service;

/**
 * @author chopra_s
 * 
 */
public interface RenewalDecisionService {
	boolean execute(long applicationId, boolean cmrAutoLinking, boolean multipleCmr, long enrolledId, long comparedToId);
}
