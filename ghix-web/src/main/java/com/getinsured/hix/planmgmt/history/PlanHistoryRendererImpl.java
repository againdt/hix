package com.getinsured.hix.planmgmt.history;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Network.NetworkType;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.provider.service.ProviderNetworkService;
import com.getinsured.hix.planmgmt.service.RegionService;
import com.getinsured.hix.planmgmt.util.DateUtil;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.util.PlanMgmtConstants;

/**
 * This class is used to populate the List<Map<String, Object>> base on the data got from REV_history or AUD tables.
 * Popultaed List<Map<String, Object>> can be used to display the corresponding data as History pages in UI
 * The data currently taken is whole data.This should have been paginated to give optimum results.
 * @author save_h
 */
@Service
public class PlanHistoryRendererImpl implements HistoryRendererService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanHistoryRendererImpl.class);
	

	@Autowired private RegionService regionService;
	@Autowired private ProviderNetworkService providerNetworkService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	private String appUrl = GhixConstants.APP_URL;
	/*private static final String COMMENT_TEXT_PRE = "<a onclick=\"getComment('";
	private static final String COMMENT_TEXT_POST = "');\" href='#modalBox' data-toggle='modal'>Comment</a>";
	public static final String TIMESTAMP_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String TIMESTAMP_PATTERN2 = "yyyy-MM-dd";
	public static final String TIMESTAMP_PATTERN3 = "MMM dd, yyyy";
	private static final String DOWNLOAD_FILE_PRETEXT1 = "<a target='_top' href='";
	//private static final String DOWNLOAD_FILE_PRETEXT2 = "ecm/filedownloadbyid?uploadedFileId=";
	//private static final String DOWNLOAD_FILE_PRETEXT3 = "'>Download File</a>";
	private static final String DOWNLOAD_FILE_POSTTEXT_BROCHURE = "' target='_blank'>Download File</a>";
	public static final String REPOSITORY_NAME = "IPlanRepository";
	public static final String MODEL_NAME = "com.getinsured.hix.model.Plan";*/
	
	/*public static final int ENROLLMENT_AVAIL = 1;
	public static final int UPDATE_DATE = 2;
	public static final int EFFECTIVE_DATE = 3;
	public static final int USER_NAME = 4;
	public static final int COMMENT = 5;
	public static final int ISSUERPLANNUMBER = 6;
	public static final int STATUS = 7;
	public static final int STATUS_FILE = 8;
	public static final int FN_ENROLLMENT_AVAIL_VAL = 9;
	public static final int FN_STATUS_VAL = 10;
	public static final int FN_DISPLAYVAL = 11;
	public static final int PROVIDER_NETWORK_NAME = 12;
	public static final int PROVIDER_NETWORK_TYPE = 13;
	public static final int PROVIDER_NETWORK_ID = 14;
	public static final int FN_BROCHURE_VAL = 15;
	public static final int START_DATE = 16;
	public static final int FN_EFFECTIVE_START_DATE_VAL= 17;
	public static final int BROCHURE_UCM_ID_VAL = 18;
	public static final int FUTURE_ENROLLMENT_AVAIL = 19;
	public static final int ISSUER_VERIFICATION_STATUS_VAL = 20;
	public static final int FUTURE_ERL_AVAIL_DATE_COL_VAL = 21;
	public static final int FN_EOC_DOC_VAL = 22;*/
	
	
	/*public static final String ENROLLMENT_AVAIL_COL = "enrollmentAvail";
	public static final String FUTURE_ENROLLMENT_AVAIL_COL = "futureEnrollmentAvail";
	public static final String FUTURE_ERL_AVAIL_DATE_COL = "futureErlAvailEffDate";
	public static final String UPDATED_DATE_COL = "lastUpdateTimestamp";
	public static final String EFFECTIVE_DATE_COL = "enrollmentAvailEffDate";
	public static final String USER_NAME_COL = "userName";
	public static final String UPDATED_BY_COL = "lastUpdatedBy";
	public static final String COMMENT_COL = "commentId";
	public static final String ISSUERPLANNUMBER_COL = "issuerPlanNumber";
	public static final String STATUS_COL = "status";
	public static final String STATUS_FILE_COL = "supportFile";
	public static final String DISPLAYVAL_COL = "displayVal";
	public static final String DISPLAYFIELD_COL = "displayField";
	public static final String PROVIDER_NETWORK_NAME_COL = "providerName";
	public static final String PROVIDER_NETWORK_TYPE_COL = "providerType";
	public static final String PROVIDER_NETWORK_ID_COL = "providerNetworkId";
	public static final String BROCHURE_COL = "brochure";
	public static final String START_DATE_COL = "startDate";
	public static final String BROCHURE_UCM_ID_CAL = "brochureUCmId";
	public static final String ISSUER_VERIFICATION_STATUS_COL ="issuerVerificationStatus";
	public static final String ISSUER_VERIFICATION_STATUS ="Verified";
	public static final String PENDING = "No";
	public static final String VERIFIED = "Yes";
	public static final String EOC_DOC_NAME_COL = "eocDocName";
	public static final String EOC_DOC_UCM_ID_CAL = "eocDocId";*/
	
	/*private static String DOCUMENT_DOWNLOAD_URL_START="<a href='#' onClick=\"showdetail('";
	private static String DOCUMENT_DOWNLOAD_ACC_DOCID= "download/document?documentId="; //"admin/document/filedownload?documentId=";
	private static String DOCUMENT_DOWNLOAD_CLOSING_HREF="');\">";
	private static String DOCUMENT_DOWNLOAD_URL_END="Download File </a>";*/
	
	private Map<Integer, Object[]> providers;
	/**
	 * Populates the List<Map<String, Object>> which can be used to display the corresponding data as History pages in UI
	 * @param data
	 * 			List<Map<String, String>> input of the entire revision history data.
	 * @param compareColumns
	 * 			List<String> of column names based on which the filteration of @param data should be done.
	 * @param columnsToDisplay
	 * 			List<Integer> of column ids specified in corresponding HistoryService, which will indicate newly prepared map should contain what all keys.
	 * @return
	 * 			List<Map<String, Object>> Filtered data based on @param data, @param compareColumns and @param columnsToDisplay.
	 */
	@Override
	public List<Map<String, Object>> processData(List<Map<String, String>> data, List<String> compareColumns, List<Integer> columnsToDisplay) 
	{
		List<Map<String, String>> orderedData = new ArrayList<Map<String,String>>(data);
		int size = orderedData.size();
		Map<String, String> firstElement = null;
		Map<String, String> secondElement = null;
		List<Map<String, Object>> processedData = new ArrayList<Map<String,Object>>();
		try {
			if (StringUtils.isAlpha(compareColumns.get(compareColumns.size()-1))) {
				compareColumns.remove(compareColumns.size()-1);
			}
			if (StringUtils.isNumeric(compareColumns.get(compareColumns.size()-1))) {
				compareColumns.remove(compareColumns.size()-1);
			}
		} catch (Exception e) {
			LOGGER.warn(e.getMessage());
		}
		try {
			for(int i=0; i<size-1;i++){
				if(i == 0 ){
					firstElement = orderedData.get(i);
					secondElement = orderedData.get(i+1);
					for(String keyColumn : compareColumns){
						boolean valueChanged;
						if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.ENROLLMENT_AVAIL_COL)){
							valueChanged = isEnrollmentChange(firstElement,secondElement,keyColumn);
						}else if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.BROCHURE_COL)) {
							// We need to also compare brochureUCmId column so that we have added following method for comparison
							valueChanged = isBroucherAdded(firstElement,secondElement,keyColumn);
						}else if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.EOC_DOC_NAME_COL)) {
							valueChanged = isEocDocAdded(firstElement,secondElement,keyColumn);
						}else {
							valueChanged = isEqual(firstElement,secondElement,keyColumn);
						}
						if(!valueChanged){
							Map<String, Object> processedMap1 = formMap(firstElement,keyColumn, columnsToDisplay);
							processedData.add(processedMap1);
							Map<String, Object> processedMap2 = formMap(secondElement,keyColumn, columnsToDisplay);
							processedData.add(processedMap2);
						}else {
							if(compareColumns.size() == 2 && StringUtils.equals(PlanMgmtConstants.STATUS_COL, compareColumns.get(0)) && StringUtils.equals(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, compareColumns.get(1))){
								LOGGER.debug("Enrollments are available.");
							}else{ 
								Map<String, Object> processedMap = formMap(firstElement,keyColumn, columnsToDisplay);
								processedData.add(processedMap);
							}
						}
					}
				}else {
					firstElement = orderedData.get(i);
					secondElement = orderedData.get(i+1);
					for (String keyColumn : compareColumns){
						boolean valueChanged;
						if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.ENROLLMENT_AVAIL_COL)){
							valueChanged = isEnrollmentChange(firstElement,secondElement,keyColumn);
						}else if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.BROCHURE_COL)) {
							valueChanged = isBroucherAdded(firstElement,secondElement,keyColumn);
						}else if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.EOC_DOC_NAME_COL)) {
							valueChanged = isEocDocAdded(firstElement,secondElement,keyColumn);
						}else {
							valueChanged = isEqual(firstElement,secondElement,keyColumn);
						}
						if(!valueChanged){
							Map<String, Object> processedMap = formMap(secondElement,keyColumn, columnsToDisplay);
							processedData.add(processedMap);
						}
					}
				}
			}
			if(size == 1){
				firstElement = orderedData.get(0);
				if(compareColumns.size() == 2 && StringUtils.equals(PlanMgmtConstants.STATUS_COL, compareColumns.get(0)) && StringUtils.equals(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, compareColumns.get(1))){
					Map<String, Object> processedMap = formMap(firstElement,compareColumns.get(0), columnsToDisplay);
					processedData.add(processedMap);
				}else {
					for(String keyColumn : compareColumns){
						Map<String, Object> processedMap = formMap(firstElement,keyColumn, columnsToDisplay);
						processedData.add(processedMap);
					}
				}
			}
		} catch (Exception e) {LOGGER.error("Error occured",e);}
		Collections.reverse(processedData);
		return processedData;
	}
	
	private boolean isEocDocAdded(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		boolean flag = true;
		if(!firstElement.get(keyColumn).equals(secondElement.get(keyColumn))){
			flag = false;
		}
		 if(firstElement.get(PlanMgmtConstants.EOC_DOC_UCM_ID_CAL) != null && secondElement.get(PlanMgmtConstants.EOC_DOC_UCM_ID_CAL) != null && !firstElement.get(PlanMgmtConstants.EOC_DOC_UCM_ID_CAL).equals(secondElement.get(PlanMgmtConstants.EOC_DOC_UCM_ID_CAL))){
			 flag = false;
		 }
		 return flag;
	}
	
	/**
	 * Compares values of maps firstElement and secondElement for key keyColumn.
	 */
	private boolean isEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) 
	{
		return firstElement.get(keyColumn).equals(secondElement.get(keyColumn));
	}
	
	private boolean isBroucherAdded(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		boolean flag = true;
		if(!firstElement.get(keyColumn).equals(secondElement.get(keyColumn))){
			flag = false;
		}
		// Here we are comparing brochureUCmId column so when we manually upload brochure then we store DMS id in brochureUCmId column
		if(firstElement.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL) != null && secondElement.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL) != null && !firstElement.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL).equals(secondElement.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL))){
			flag = false;
		}
		return flag;
	}
	
	private boolean isEnrollmentChange(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		boolean flag = true;
		if(!firstElement.get(keyColumn).equals(secondElement.get(keyColumn))){
			flag = false;
		}
		if(firstElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL) != null && secondElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL) != null && !firstElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL).equals(secondElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL))){
			flag = false;
		}
		if(firstElement.get(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL) != null && secondElement.get(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL) != null && !firstElement.get(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL).equals(secondElement.get(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL))){
			flag = false;
		}	
		return flag;
	}
	
	/**
	 * Forms map for given key.
	 * The newly formed map contains key-value pairs based on the list provided as columnsToDisplay.
	 */
	private Map<String,Object> formMap(Map<String,String> firstMap,String key, List<Integer> columnsToDisplay)
	{
		Map<String, Object> values = new HashMap<String, Object>();
		String fileUrl= null;
		for (int colId : columnsToDisplay)
		{
			switch (colId) 
			{
			case PlanMgmtConstants.ENROLLMENT_AVAIL_INT : 
				String enrollAvail = firstMap.get(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
				enrollAvail = getEnrollAvail(enrollAvail);
				values.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, enrollAvail);
				break;
			case PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_INT: 
				String futurEenrollAvail = firstMap.get(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL);
				enrollAvail = getEnrollAvail(futurEenrollAvail);
				values.put(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL,enrollAvail);
				
				futurEenrollAvail = getValue(firstMap,key, colId);
				if (futurEenrollAvail != null) {
					values.put(PlanMgmtConstants.DISPLAYVAL_COL, futurEenrollAvail);
				}
				
				break;
			case PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL_VAL_INT: 
				String futurErlEffDate = firstMap.get(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL);
				SimpleDateFormat fErlEffDateFormat = new SimpleDateFormat();
				fErlEffDateFormat.applyPattern(PlanMgmtConstants.TIMESTAMP_PATTERN1);
				if(futurErlEffDate.isEmpty()){
					 values.put(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL,PlanMgmtConstants.EMPTY_STRING);
				}else{
					values.put(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL, DateUtil.changeFormat(futurErlEffDate, PlanMgmtConstants.TIMESTAMP_PATTERN1, PlanMgmtConstants.TIMESTAMP_PATTERN3));
					String  valueEnrollAvailDate = getValue(firstMap,key, colId);
					if (valueEnrollAvailDate != null) {
						values.put(PlanMgmtConstants.DISPLAYVAL_COL, valueEnrollAvailDate);
					}
				}
				break;
			case PlanMgmtConstants.UPDATE_DATE_INT : 
				String dt = firstMap.get(PlanMgmtConstants.UPDATED_DATE_COL);
				values.put(PlanMgmtConstants.UPDATED_DATE_COL, DateUtil.StringToDate(dt, PlanMgmtConstants.TIMESTAMP_PATTERN1));
				break;
				
			case PlanMgmtConstants.EFFECTIVE_DATE_INT:
				String effDate = null;
				effDate = firstMap.get(PlanMgmtConstants.EFFECTIVE_DATE_COL);
				if(StringUtils.isBlank(effDate)){
					values.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, PlanMgmtConstants.EMPTY_STRING);
				}else{
					if(effDate.contains("00:00:")){
						values.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, DateUtil.changeFormat(effDate, PlanMgmtConstants.TIMESTAMP_PATTERN1, PlanMgmtConstants.TIMESTAMP_PATTERN3));
					}else{
						values.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, DateUtil.changeFormatWithTimezone(effDate, PlanMgmtConstants.TIMESTAMP_PATTERN1, PlanMgmtConstants.TIMESTAMP_PATTERN3));
					}
				}
				break;
				
			case PlanMgmtConstants.USER_NAME_INT:
				String userId = firstMap.get(PlanMgmtConstants.UPDATED_BY_COL);
				values.put(PlanMgmtConstants.USER_NAME_COL,  regionService.getUserName(userId));
				break;
				
			case PlanMgmtConstants.COMMENT_INT :
				String commentId = firstMap.get(PlanMgmtConstants.COMMENT_COL);
				if(commentId != null && commentId.length() > 0) {
					values.put(PlanMgmtConstants.COMMENT_COL, PlanMgmtConstants.COMMENT_TEXT_PRE+commentId+PlanMgmtConstants.COMMENT_TEXT_POST);
				} else {
					values.put(PlanMgmtConstants.COMMENT_COL, "");
				}
				break;

			case PlanMgmtConstants.STATUS_FILE_INT : 
				String statusFile = null;
				statusFile = firstMap.get(PlanMgmtConstants.STATUS_FILE_COL);
				if(statusFile != null && statusFile.length() > 0) {
					fileUrl=PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_URL_START+appUrl+PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_ACC_DOCID + ghixJasyptEncrytorUtil.encryptStringByJasypt(statusFile) + PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_CLOSING_HREF + PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_URL_END;
					values.put(PlanMgmtConstants.STATUS_FILE_COL, fileUrl);
				} else {
					values.put(PlanMgmtConstants.STATUS_FILE_COL, fileUrl);
				}
				break;
			case PlanMgmtConstants.STATUS_INT : 
				String status = firstMap.get(PlanMgmtConstants.STATUS_COL);
				status = getStatus(status);
				values.put(PlanMgmtConstants.STATUS_COL, status);
				break;
			case PlanMgmtConstants.FN_ENROLLMENT_AVAIL_VAL_INT :
				String  valueEnrollAvail = getValue(firstMap,key, colId);
				if (valueEnrollAvail != null) {
					values.put(PlanMgmtConstants.DISPLAYVAL_COL, valueEnrollAvail);
				}
				break;
			case PlanMgmtConstants.FN_STATUS_VAL_INT :
				String  valueStatus = getValue(firstMap,key, colId);
				if (valueStatus != null) {
					values.put(PlanMgmtConstants.DISPLAYVAL_COL, valueStatus);
				}
				break;
			case PlanMgmtConstants.FN_EFFECTIVE_START_DATE_VAL_INT : 
				String  valueDate = getValue(firstMap,key, colId);
				if (valueDate != null) {
					values.put(PlanMgmtConstants.DISPLAYVAL_COL, valueDate);
				}
				break;
			case PlanMgmtConstants.FN_BROCHURE_VAL_INT : 
				String  value = getValue(firstMap,key, colId);
				if (value != null) {
					values.put(PlanMgmtConstants.DISPLAYVAL_COL, value);
				}
				break;
			case PlanMgmtConstants.FN_DISPLAYVAL_INT:
				String displayVal = getDisplayVal(key);
				values.put(PlanMgmtConstants.DISPLAYFIELD_COL, displayVal);
				break;
			case PlanMgmtConstants.PROVIDER_NETWORK_NAME_INT:
				String providerId = firstMap.get(PlanMgmtConstants.PROVIDER_NETWORK_ID_COL);
				String name = getProviderDetail(providerId, PlanMgmtConstants.PROVIDER_NETWORK_NAME_INT);
				values.put(PlanMgmtConstants.PROVIDER_NETWORK_NAME_COL, name);
				break;

			case PlanMgmtConstants.PROVIDER_NETWORK_TYPE_INT:
				String providerId1 = firstMap.get(PlanMgmtConstants.PROVIDER_NETWORK_ID_COL);
				String type = getProviderDetail(providerId1, PlanMgmtConstants.PROVIDER_NETWORK_TYPE_INT);
				values.put(PlanMgmtConstants.PROVIDER_NETWORK_TYPE_COL, type);
				break;
				
			case PlanMgmtConstants.START_DATE_INT:
				String effStartDate = null;
				effStartDate = firstMap.get(PlanMgmtConstants.START_DATE_COL);
				if(StringUtils.isBlank(effStartDate)){
					values.put(PlanMgmtConstants.START_DATE_COL, PlanMgmtConstants.EMPTY_STRING);
				}else{
					values.put(PlanMgmtConstants.START_DATE_COL, DateUtil.changeFormatWithTimezone(effStartDate, PlanMgmtConstants.TIMESTAMP_PATTERN1, PlanMgmtConstants.TIMESTAMP_PATTERN3));
				}
				break;
			 case PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_VAL_INT:
	 	 	 	String  value1 = getValue(firstMap,key,colId);
	 	 	 	if (value1 != null) {
	 	 	 		values.put(PlanMgmtConstants.DISPLAYVAL_COL, value1);
				}
	 	 	 	break;
	 	 	 	
			 case PlanMgmtConstants.FN_EOC_DOC_VAL_INT:
				 String  value2 = getValue(firstMap,key,colId);
				 if (value2 != null) {
					 values.put(PlanMgmtConstants.DISPLAYVAL_COL, value2);
				 }
				break;
			}
		}
		return values;
	}
	public Object[] getProvider(String providerNetworkId) {
		Object[] providerData = null;
		Integer provNetId = Integer.parseInt(providerNetworkId);
		if (providers.containsKey(provNetId)) {
			providerData = providers.get(provNetId);
		}
		else
		{
			providerData = getProviderFromDB(provNetId);
			providers.put(provNetId, providerData);
		}
		return providerData;
	}

	private Object[] getProviderFromDB(Integer providerNetworkId) {
		return providerNetworkService.getProvNetDetailsById(providerNetworkId);
	}

	private String getProviderDetail(String providerNetworkId, int providerColKey) {
		
		String value = null;
		if(providerNetworkId.length()>0)
		{
			Object[] providerData = getProvider(providerNetworkId);
			if (providerColKey == PlanMgmtConstants.PROVIDER_NETWORK_NAME_INT)
			{
				value = (String) providerData[0];
			}
			else if (providerColKey == PlanMgmtConstants.PROVIDER_NETWORK_TYPE_INT)
			{
				value = ((NetworkType)providerData[1]).name();
			}
		}
		return value;
	}

	private String getEnrollAvail(String enrollAvail) {
		String avail = null;
		if (Plan.EnrollmentAvail.AVAILABLE.toString().equals(enrollAvail))
		{
			avail = PlanMgmtConstants.AVAILABLE; //"Available";
		}
		else if (Plan.EnrollmentAvail.DEPENDENTSONLY.toString().equals(enrollAvail))
		{
			avail = PlanMgmtConstants.DEPENDENTSONLY; //"Dependents Only";
		}
		else if (Plan.EnrollmentAvail.NOTAVAILABLE.toString().equals(enrollAvail))
		{
			avail = PlanMgmtConstants.NOTAVAILABLE; //"Not Available";
		}
		return avail;
	}

	private String getStatus(String status) {
		String sts = null;
		if (Plan.PlanStatus.CERTIFIED.toString().equals(status))
		{
			sts = PlanMgmtConstants.CERTIFIED; //"Certified";
		}
		else if (Plan.PlanStatus.DECERTIFIED.toString().equals(status))
		{
			sts = PlanMgmtConstants.DECERTIFIED; //"De-certified";
		}
		else if (Plan.PlanStatus.INCOMPLETE.toString().equals(status))
		{
			sts = PlanMgmtConstants.INCOMPLETE; //"Incomplete";
		}else if (Plan.PlanStatus.LOADED.toString().equals(status))
		{
			sts = PlanMgmtConstants.LOADED; //"Loaded";
		}
		return sts;
	}
	
	private String getValue(Map<String,String> firstMap,String key, int colId) 
	{
		String value = null;
		switch (colId) 
		{
		case PlanMgmtConstants.FN_ENROLLMENT_AVAIL_VAL_INT : 
			if (PlanMgmtConstants.ENROLLMENT_AVAIL_COL.equals(key))
			{
				String enrollAvail = firstMap.get(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
				value = getEnrollAvail(enrollAvail);
			}
			break;
			
			
		case PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_INT : 
			if (PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL.equals(key))
			{
				String enrollAvail = firstMap.get(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL);
				value = getEnrollAvail(enrollAvail);
			}
			break;
			
		case PlanMgmtConstants.FN_EFFECTIVE_START_DATE_VAL_INT :
			if (PlanMgmtConstants.EFFECTIVE_DATE_COL.equals(key))
			{
				Date dateValue = null;
				String effectiveDate = firstMap.get(PlanMgmtConstants.EFFECTIVE_DATE_COL);
				if(effectiveDate.isEmpty()){
					value = PlanMgmtConstants.EMPTY_STRING;
				}else{
					dateValue = DateUtil.StringToDate(effectiveDate, PlanMgmtConstants.TIMESTAMP_PATTERN2);
					value = DateUtil.dateToString(dateValue, PlanMgmtConstants.TIMESTAMP_PATTERN3);
				}
			}
			break;
			
		case PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL_VAL_INT :
			if (PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL.equals(key))
			{
				Date dateValue = null;
				String effectiveDate = firstMap.get(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL);
				if(effectiveDate.isEmpty()){
					value = PlanMgmtConstants.EMPTY_STRING;
				}else{
					dateValue = DateUtil.StringToDate(effectiveDate, PlanMgmtConstants.TIMESTAMP_PATTERN2);
					value = DateUtil.dateToString(dateValue, PlanMgmtConstants.TIMESTAMP_PATTERN3);
				}
			}
			break;
		case PlanMgmtConstants.FN_STATUS_VAL_INT :
			if (PlanMgmtConstants.STATUS_COL.equals(key))
			{
				String status = firstMap.get(PlanMgmtConstants.STATUS_COL);
				value = getStatus(status);
			}
			break;
		case PlanMgmtConstants.FN_BROCHURE_VAL_INT : 
			if (PlanMgmtConstants.BROCHURE_COL.equals(key)){
				
				// Here We are displaying download link based on brochure column here sequence is necessary because
				// When we get plan from SERFF we are getting brochure link or file and that link is gets stored in brochure. And after that we are storing DMS id in
				// brochureUCmId column so that afterwards we are ignoring brochure column value and we are displaying brochureUCmId value
				String brochure = firstMap.get(PlanMgmtConstants.BROCHURE_COL);
				
				String manualUploadedBroucger = null;
				
				try {
					manualUploadedBroucger = firstMap.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL);
				} catch (Exception e) {
					manualUploadedBroucger = null;
					LOGGER.warn(e.getMessage());
				}
				
				value = "";
				
				if(StringUtils.isNotBlank(brochure)){
					if(brochure.matches("(?i).*http.*")){
						value = PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT1 +  brochure + PlanMgmtConstants.DOWNLOAD_FILE_POSTTEXT_BROCHURE;						
					}else{
						value = PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT1 + "http://" + brochure + PlanMgmtConstants.DOWNLOAD_FILE_POSTTEXT_BROCHURE;
					}										
				}
				
				if(StringUtils.isNotBlank(manualUploadedBroucger)) {
					/*value = DOWNLOAD_FILE_PRETEXT1 + appUrl + DOWNLOAD_FILE_PRETEXT2 + manualUploadedBroucger + DOWNLOAD_FILE_PRETEXT3;
					LOGGER.debug("Download file path :== "+value);*/
					value=PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_URL_START+appUrl+PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_ACC_DOCID + ghixJasyptEncrytorUtil.encryptStringByJasypt(manualUploadedBroucger) + PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_CLOSING_HREF + PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_URL_END;
					LOGGER.debug("Download file path :== " + SecurityUtil.sanitizeForLogging(String.valueOf(value)));
				}
				
			}
			break;
		case PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_VAL_INT :   
	 	 	 if (PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL.equals(key))
	 	 	 {
	 	 	 	String issuerVerificationStatus = firstMap.get(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL);
	 	 	 	value = getIssuerVerificationStatus(issuerVerificationStatus);
	 	 	 }
	 	 	 break;
		case PlanMgmtConstants.FN_EOC_DOC_VAL_INT :
			 if (PlanMgmtConstants.EOC_DOC_UCM_ID_CAL.equals(key)){
				 value = firstMap.get(PlanMgmtConstants.EOC_DOC_UCM_ID_CAL);
				 if(StringUtils.isNotBlank(value)) {
					 value = PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_URL_START + appUrl + PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_ACC_DOCID + ghixJasyptEncrytorUtil.encryptStringByJasypt(value) + PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_CLOSING_HREF + PlanMgmtConstants.HISTORY_DOCUMENT_DOWNLOAD_URL_END;
					 LOGGER.debug("Download file path :== " + SecurityUtil.sanitizeForLogging(String.valueOf(value)));
				 }
			 }
			 break;
		}
		return value;
	}

	private String getDisplayVal(String key) 
	{
		String displayVal = null;
		if (key.equals(PlanMgmtConstants.STATUS_COL))
		{
			displayVal = PlanMgmtConstants.PLAN_STATUS; //"Plan Status";
		}
		else if (key.equals(PlanMgmtConstants.ENROLLMENT_AVAIL_COL))
		{
			displayVal = PlanMgmtConstants.ENROLLMENT_AVAILABILITY; //"Enrollment Availability";
		}
		else if (key.equals(PlanMgmtConstants.BROCHURE_COL))
		{
			displayVal = PlanMgmtConstants.PLAN_BROCHURE; //"Plan Brochure";
		}
		else if (key.equals(PlanMgmtConstants.EFFECTIVE_DATE_COL))
		{
			displayVal= PlanMgmtConstants.EFFECTIVE_DATE; //"Effective Date";
		}
		else if (key.equals(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL))
	 	{
			displayVal= PlanMgmtConstants.ISSUER_VERIFICATION_STATUS;
	 	}
		else if(key.equals(PlanMgmtConstants.FUTURE_ENROLLMENT_AVAIL_COL))
	 	{
			displayVal = PlanMgmtConstants.FUTURE_ENROLLMENT_AVAILABILITY; //"Future Enrollment Availability";
	 	}
		else if(key.equals(PlanMgmtConstants.FUTURE_ERL_AVAIL_DATE_COL))
		{
			displayVal = PlanMgmtConstants.FUTURE_EFFECTIVE_DATE; //"Future Effective Date";
	 	}
		else if(key.equals(PlanMgmtConstants.EOC_DOC_UCM_ID_CAL))
		{
			displayVal = PlanMgmtConstants.PLAN_EOC_DOCUMENT;//"Plan EOC Document";
		}
		return displayVal;
	}
	
	 /**
	  * Returns Issuer Verification status
	  * @param issuerVerificationStatus of type String
	  * @return of type String
	  */
	 private String getIssuerVerificationStatus(String issuerVerificationStatus) {
	 	String status = null;
	 	if (Plan.IssuerVerificationStatus.PENDING.toString().equals(issuerVerificationStatus))
	 	{
	 		status = PlanMgmtConstants.PENDING_NO; 
	 	}
	 	else if (Plan.IssuerVerificationStatus.VERIFIED.toString().equals(issuerVerificationStatus))
	 	{
	 		status = PlanMgmtConstants.VERIFIED_YES; 
	 	}               
	 	return status;
	}
}
