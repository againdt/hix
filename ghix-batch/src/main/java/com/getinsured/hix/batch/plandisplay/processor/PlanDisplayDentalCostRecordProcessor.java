package com.getinsured.hix.batch.plandisplay.processor;

import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.getinsured.hix.batch.plandisplay.reader.PlanDisplayDentalCostMapper;

public class PlanDisplayDentalCostRecordProcessor implements ItemProcessor<PlanDisplayDentalCostMapper, SolrInputDocument>{
	private static Logger logger = LoggerFactory.getLogger(PlanDisplayDentalCostRecordProcessor.class);

	@Override
	public SolrInputDocument process(PlanDisplayDentalCostMapper fromReader) throws Exception {
		logger.info("In process ---> "+fromReader.getName());
		return prepareSolrDocument(fromReader);
	}
	
	private SolrInputDocument prepareSolrDocument(PlanDisplayDentalCostMapper providerData){
		
		SolrInputDocument solrInputDocument = providerData.getSolrInputDocument();
		logger.info("Processing Data completed : "+providerData.getName());
		return solrInputDocument;
	}
}
