<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
 <div class="row">
   		 <div class="span4">
      		<h2>Congratulations, Frank Markovich</h2>
    		 </div>
   		 	<div class="span12">
   		 		<form class="form-stacked">
              <df:csrfToken/>
   		 			<fieldset>
     			<legend>Your information has been received in the Getinsured Health Exchange.</legend>
     			
     				<p>Our records indicate that ACME Inc. has chosen to provide a <a href="#" rel="popover" data-original-title="Defined Contribution" data-content="An annual amount paid in monthly increments by your employer towards the costs of your health plan premiums. This amount will show as a credit against any plan premium you select on the Getinsured Health Exchange."><strong><u>defined contribution</u></strong></a> toward your health insurance coverage in the amount of <strong>&#36;100.00 per month.</strong> Click confirm and continue below to begin shopping for a health care plan.
     				</p>

     				
     				<div class="actions">
     					<a class="btn large primary" href="http://plans.test.vimo.com/profile.php?sp=yes&flow=mn-exchange&ZipCoded=1&affid=278&zipcode=55101&server_name=test.vimo.com&coveragestartdate=10/31/2014&line_index=1&num_child=0&state=AL&first_name=Frank&last_name=Markovich&currently_insured=no&email=Frank.Markovich@test.com&day_phone=650+230+0022&street1=test&insert_familymembers_name=no&chk_sav_accnt=no&relation_1=applicant&pregnancy=0&applicant_gender=M&applicant_dob_mm=01&applicant_dob_dd=01&applicant_dob_yy=1966&applicant_heightFT=5&applicant_heightIN=6&applicant_weight=165&applicant_smoker=0&applicant_rx=0&applicant_health=0&defined_contribution=100">Confirm and Continue</a>
     				</div>
     				</fieldset>
     			</form>
			</div>
			
	<div class="notes" style="display:none">
		<div class="row">

			<div class="span">
		      <p>The information is submitted and the Employee is informed about what coverage options the Employer selected for him. 
		      </p>
		    </div>
	    </div>
	</div>				
			
	</div>

<script type="text/JavaScript">
    $(".open").click(function () {
          $(this).next().slideToggle("slow");
    });
</script> 