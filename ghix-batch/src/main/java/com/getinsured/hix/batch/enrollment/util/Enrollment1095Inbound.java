package com.getinsured.hix.batch.enrollment.util;

import java.io.File;
import java.io.FileFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsBatchService;
import com.getinsured.hix.enrollment.repository.IEnrollment1095AudRepository;
import com.getinsured.hix.enrollment.repository.IEnrollment1095Repository;
import com.getinsured.hix.enrollment.repository.IEnrollmentIn1095Repository;
import com.getinsured.hix.enrollment.repository.IEnrollmentOut1095Repository;
import com.getinsured.hix.enrollment1095.service.Enrollment1095NotificationService;
import com.getinsured.hix.platform.lookup.service.LookupService;


/**
 * Abstract class for 1095 inbound XML processing
 * @author negi_s
 *
 */
public abstract class Enrollment1095Inbound {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095Inbound.class);

	protected static final String MANIFEST = "manifest";
	
	protected static final String ENROLLMENT_1095_ERROR_CODE = "ENROLLMENT_1095_ERROR_CODE";
	
	@Autowired protected IEnrollmentIn1095Repository enrollmentIn1095Repository;
	@Autowired protected IEnrollmentOut1095Repository enrollmentOut1095Repository;
	@Autowired protected IEnrollment1095Repository enrollment1095Repository;
	@Autowired protected IEnrollment1095AudRepository enrollment1095AudRepository;
	@Autowired protected Enrollment1095NotificationService enrollment1095NotificationService;
	@Autowired protected EnrollmentAnnualIrsBatchService enrollmentAnnualIrsBatchService;
	@Autowired protected LookupService lookupService;
	
	public abstract void updateEnrollmentIn1095ForAckAndNack(File file, String fileType, Long jobExecutionId);
	public abstract void processEnrollmentIn1095ForOutResponse(File file, String responseFileName, String outResponsePath, Long jobExecutionId);
	
	protected File[] getNonManifestResponseFile(String absolutePath) {
		File[] files = null;
		try{
			File folder= new File(absolutePath);
			files = folder.listFiles( new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					if(pathname.isFile() && (pathname.getName().contains(MANIFEST) || pathname.getName().contains(".zip")) ){
						return false;
					}
					return true;
				}
			});
		}catch(Exception e){
			LOGGER.error("Error getting files in location", e);
		}
		return files;
	}

	
}
