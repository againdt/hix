
const BLOOD_RELATIONSHIPS_ENDPOINT = '/hix/newssap/relationships';
const BLOOD_RELATIONSHIPS_ENDPOINT_WHOLE_LIST = '/hix/newssap/commonRelationships';
const CHANGE_LANGUAGE = '/hix/private/changeLang';
const COUNTY_ENDPOINT = '/hix/newssap/countiesforstate/';
const COUNTIES_FOR_STATE_ENDPOINT = '/hix/newssap/countiesforstate/';
const COUNTIES_FOR_STATES_ENDPOINT = '/hix/newssap/countiesforstates/';
const COUNTRIES_ENDPOINT = '/hix/hub/getCountryOfIssuanceList';
const DASHBOARD_ENDPOINT = '/hix/account/user/getDashBoard';
const ENVIRONMENT_VARIABLES_FOR_HEADER = '/hix/ui/environment';
const ENVIRONMENT_VARIABLES_FOR_8888_PORT = 'http://localhost:8080/hix/ui/environment';
const FLOW_ENDPOINT = '/hix/newssap/configuration';
const INITIAL_DATA_ENDPOINT = '/hix/newssap/application/';
const MEMBER_AFFORDABILITY = '/hix/newssap/configuration/affordability/';
const PRE_ELIGIBILITY =  '/hix/newssap/preeligibility/';
const RIDP_ENDPOINT = '/hix/ridp/verification';
const SAVE_DATA_ENDPOINT = '/hix/newssap/save?csrftoken=';
const SESSION_PING = '/hix/ui/ping';
const TRIBES_ENDPOINT = '/hix/newssap/stateswithtribes';
const VALIDATE_ADDRESS_ENDPOINT = '/hix/address/validate?csrftoken=';

const WRONG_ENDPOINT = '/hix/newssap/fghsjjsj';     // For testing only

export default {
    BLOOD_RELATIONSHIPS_ENDPOINT,
    BLOOD_RELATIONSHIPS_ENDPOINT_WHOLE_LIST,
    CHANGE_LANGUAGE,
    COUNTY_ENDPOINT,
    COUNTIES_FOR_STATE_ENDPOINT,
    COUNTIES_FOR_STATES_ENDPOINT,
    COUNTRIES_ENDPOINT,
    DASHBOARD_ENDPOINT,
    ENVIRONMENT_VARIABLES_FOR_HEADER,
    ENVIRONMENT_VARIABLES_FOR_8888_PORT,
    INITIAL_DATA_ENDPOINT,
    MEMBER_AFFORDABILITY,
    FLOW_ENDPOINT,
    PRE_ELIGIBILITY,
    RIDP_ENDPOINT,
    SAVE_DATA_ENDPOINT,
    SESSION_PING,
    TRIBES_ENDPOINT,
    VALIDATE_ADDRESS_ENDPOINT,
    WRONG_ENDPOINT                                  // For testing only
};