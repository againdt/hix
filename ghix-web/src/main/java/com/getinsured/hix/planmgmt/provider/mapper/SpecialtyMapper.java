package com.getinsured.hix.planmgmt.provider.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.Specialty;
import com.getinsured.hix.model.Specialty.GroupName;
import com.getinsured.hix.platform.file.mapper.Mapper;

public class SpecialtyMapper extends Mapper<Specialty> 
{
	private static Map<String, Integer> columnKeys = null;
	
	private static final String SPECIALITY_NAME = "Name";
	private static final String SPECIALITY_DESCRIPTION = "Description";
	private static final String SPECIALITY_GROUP = "Group";
	
	/* Provider Table Column Entries */
	private static final String DATASOURCE_REF_ID = "Ref ID";
	
	private static final int SPECIALITY_NAME_VAL = 1;
	private static final int SPECIALITY_DESCRIPTION_VAL =2;
	private static final int SPECIALITY_GROUP_VAL =3;
	
	/* Provider Table Column Entry key's */
	private static final int DATASOURCE_REF_ID_VAL = 4;
	
	static {
		columnKeys = new HashMap<String, Integer>();
		columnKeys.put(SPECIALITY_NAME, SPECIALITY_NAME_VAL);
		columnKeys.put(SPECIALITY_DESCRIPTION, SPECIALITY_DESCRIPTION_VAL);
		columnKeys.put(SPECIALITY_GROUP, SPECIALITY_GROUP_VAL);
		columnKeys.put(DATASOURCE_REF_ID, DATASOURCE_REF_ID_VAL);
	}

	public Specialty mapData(final List<String> data, final List<String> columns) {
		
		Specialty specialty = new Specialty();
		
		int index = 0;
		for (String column : columns)
		{
			mapData(data.get(index), column, specialty);
			index++;
		}
		return specialty;
	}
	
	private void mapData(String data, String column,Specialty specialty) { 
		
		int colVal = columnKeys.get(column.replace("\"", ""));
		String trimData = data.trim();
		
		switch (colVal) {
			
			case SPECIALITY_NAME_VAL:
				specialty.setName(trimData);
			break;
			case SPECIALITY_DESCRIPTION_VAL:
				specialty.setDescription(trimData);
			break;
			case SPECIALITY_GROUP_VAL:
				if(trimData.equalsIgnoreCase(GroupName.DOCTOR.toString())){
					specialty.setGroupName(GroupName.DOCTOR);
				}
				else if(trimData.equalsIgnoreCase(GroupName.DENTIST.toString())){
					specialty.setGroupName(GroupName.DENTIST);
				}
			break;
			case DATASOURCE_REF_ID_VAL:
				specialty.setDatasourceRefId(trimData);
			break;
			default:
				break;
		}
	}
}
