package com.getinsured.hix.batch.provider;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FieldMetadata {

	private String name;
	private String type;
	private boolean isMendatory;
	private boolean hasMultipleValues;
	private String scope = null;
	private boolean gi_modified = false;

	private List<ProviderDataFieldProcessor> processors;
	
	private Logger logger = LoggerFactory.getLogger(FieldMetadata.class);
	private String identifier;
	private boolean solrAware = false;
	private boolean validationRequired = true;
//	private ValidationContext context;
	
	public void setSolrAware(boolean solrAware) {
		this.solrAware = solrAware;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isMendatory() {
		return isMendatory;
	}
	public void setMendatory(boolean isMendatory) {
		this.isMendatory = isMendatory;
	}
	public boolean isHasMultipleValues() {
		return hasMultipleValues;
	}
	public void setHasMultipleValues(boolean hasMultipleValues) {
		this.hasMultipleValues = hasMultipleValues;
	}
	
	public int getProcessorCount(){
		return this.processors.size();
	}
	public ProviderDataFieldProcessor getProcessor(int index) {
		if(this.processors == null || index >= this.processors.size()){
			return null;
		}
		return this.processors.get(index);
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	
	public void addProcessor(ProviderDataFieldProcessor processor) {
		if(this.processors == null){
			this.processors = new ArrayList<ProviderDataFieldProcessor>();
		}
		logger.debug("Adding processor for field:"+this.name+" at index:"+processor.getIndex());
		this.processors.add(processor.getIndex(), processor);
	}
	
	public boolean fieldInScope(int readerScope) throws InvalidOperationException{
		if(this.scope == null){
			return true;
		}
		switch(readerScope){
		case ProviderFieldsMetadata.FACILITY:
			if(this.scope.equalsIgnoreCase("facility")){
				return true;
			}
			break;
		case ProviderFieldsMetadata.INDIVIDUAL:
			if(this.scope.equalsIgnoreCase("individual")){
				return true;
			}
			break;
			default:
				throw new InvalidOperationException("Invalid reader scope ["+readerScope+"] expected 0 or 1");
		}
		return false;
	}
		
	public String toString(){
		return "Name:"+this.name+",Type:"+this.type+",Mandatory:"+this.isMendatory+",MultipleValues:"+this.hasMultipleValues;
	}
	public void setIdentifier(String tmpVal) {
		this.identifier = tmpVal;
	}
	
	public String getIdentifier(){
		return this.identifier;
	}
	public boolean isSolrAware() {
		return this.solrAware;
	}
	public boolean isGi_modified() {
		return gi_modified;
	}
	public void setGi_modified(boolean gi_modified) {
		this.gi_modified = gi_modified;
	}
	public void setValidationRequired(boolean validationRequired) {
		this.validationRequired = validationRequired;
	}
	public boolean getValidationRequired() {
		return this.validationRequired;
	}
}
