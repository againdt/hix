package com.getinsured.hix.entity.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.hix.dto.finance.PaymentMethodDTO;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Encapsulates service layer calls for multiple tables like ee_assisters,
 * ee_documents etc.
 */
public interface EntityService {

	/**
	 * Saves photo on ECM and creates/updates ee_documents table and updates ee_assisters with document id
	 * 
	 * @param assisterId
	 *            Assister ID
	 * @param fileInput            
	 *            Assister Photo
	 * @return the Entity Document ID
	 * @throws Exception 
	 */
	void saveAssisterPhoto(int assisterId, MultipartFile fileInput) throws Exception;
	
	/**
	 * Retrieves Existing Assister photo from ee_assisters table
	 * 
	 * @param assisterId
	 *            Assister ID
	 * @return the Assister photo
	 * @throws GIException
	 * @throws Exception 
	 */
	byte[] getAssisterPhotoById(int assisterId) throws GIException, Exception;
	
	/**
	 * Generate and Send Assister Activation Link
	 * 
	 * @param Assister assister
	 * @throws GIException
	 */
	String generateAssisterActivationLink(Assister assister) throws GIException;
	
	void createEntityStatusChangeNotice(EnrollmentEntity enrollmentEntity, String priorCertificationStatus) throws NoticeServiceException, JsonProcessingException, IOException;
	
	void createCECStatusChangeNotice(Assister asister, String priorCertificationStatus) throws NoticeServiceException, JsonProcessingException, IOException;
	
	List<PaymentMethodDTO> getPaymentMethodMigrationData(int pageSize);
	
}
