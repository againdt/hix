
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CHIP",
    "MEDC",
    "PECO",
    "BHPC",
    "MEDI",
    "VHPC",
    "TRIC",
})
public class MecResults implements Serializable
{

    @JsonProperty("CHIP")
    private NonEsiMecVerification cHIP;
    @JsonProperty("MEDC")
    private NonEsiMecVerification mEDC;
    @JsonProperty("PECO")
    private NonEsiMecVerification pECO;
    @JsonProperty("BHPC")
    private NonEsiMecVerification bHPC;
    @JsonProperty("MEDI")
    private NonEsiMecVerification mEDI;
    @JsonProperty("VHPC")
    private NonEsiMecVerification vHPC;
    @JsonProperty("TRIC")
    private NonEsiMecVerification tRIC;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -8626717826512944116L;

    @JsonProperty("CHIP")
    public NonEsiMecVerification getCHIP() {
        return cHIP;
    }

    @JsonProperty("CHIP")
    public void setCHIP(NonEsiMecVerification cHIP) {
        this.cHIP = cHIP;
    }

    @JsonProperty("MEDC")
    public NonEsiMecVerification getMEDC() {
        return mEDC;
    }

    @JsonProperty("MEDC")
    public void setMEDC(NonEsiMecVerification mEDC) {
        this.mEDC = mEDC;
    }

    @JsonProperty("PECO")
    public NonEsiMecVerification getPECO() {
        return pECO;
    }

    @JsonProperty("PECO")
    public void setPECO(NonEsiMecVerification pECO) {
        this.pECO = pECO;
    }

    @JsonProperty("BHPC")
    public NonEsiMecVerification getBHPC() {
        return bHPC;
    }

    @JsonProperty("BHPC")
    public void setBHPC(NonEsiMecVerification bHPC) {
        this.bHPC = bHPC;
    }

    @JsonProperty("MEDI")
    public NonEsiMecVerification getMEDI() {
        return mEDI;
    }

    @JsonProperty("MEDI")
    public void setMEDI(NonEsiMecVerification mEDI) {
        this.mEDI = mEDI;
    }

    @JsonProperty("VHPC")
    public NonEsiMecVerification getVHPC() {
        return vHPC;
    }

    @JsonProperty("VHPC")
    public void setVHPC(NonEsiMecVerification vHPC) {
        this.vHPC = vHPC;
    }

    @JsonProperty("TRIC")
    public NonEsiMecVerification getTRIC() {
        return tRIC;
    }

    @JsonProperty("TRIC")
    public void setTRIC(NonEsiMecVerification tRIC) {
        this.tRIC = tRIC;
    }
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cHIP", cHIP).append("mEDC", mEDC).append("pECO", pECO).append("bHPC", bHPC).append("mEDI", mEDI).append("vHPC", vHPC).append("additionalProperties", additionalProperties).toString();
    }

   
}
