(function (views) {
	views.DetailView = Backbone.View.extend({

		events : {
			'click .back-to-all-plans-link-detail' : 'backToAll'
		},
		el : '.back-to-all-plans-link-container',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'addOne','detailPlan');
			var self = this;

			//self.tmpl = _.template($('#tmpDetail').html());
			//var html = this.tmpl(self.collection.info());
			//$(this.el).html(html);

			$('.ps-detail').on('click', '.toggle', function() {
				var me = $(this);
				var toggleDiv = me.closest('.ps-detail__group').find('.toggle-div');
				var header = me.closest('div');

				if(me.hasClass('icon-chevron-up')) {
					me.removeClass('icon-chevron-up').addClass('icon-chevron-down');
					header.css('background-color', '#fff').css('border-bottom', '1px solid #f0f0f0');
				}else {
					me.removeClass('icon-chevron-down').addClass('icon-chevron-up');
					header.css('background-color', '#f0f0f0').css('border-bottom', 'none');
				}
				toggleDiv.slideToggle( "fast" );
			});
		},
		addAll : function () {
			var self = this;
			$('#detailHead').show();
			$('#detail-plan').show();

			$('#mainSummary').empty();
			$('#mainSummaryCmpHead').hide();

			$('#benefitHead').hide();

			var planType = self.collection.models[0].get("planType");
			if(planType =="DENTAL"){
				self.removeDentalDetail();
				//$('.back-to-all-plans-link-container').append($('#tmpDetail').html());
				self.collection.each (self.dentalAddOne);
			}else{
				self.removeDetail();
				//$('.back-to-all-plans-link-container').append($('#tmpDetail').html());
				self.collection.each (self.addOne);
			}

			$('.addToCart').bind('click', self.addToCart);
		},

		addOne : function (model) {
			var self = this;

			var PERSON_COUNT = $('#PERSON_COUNT').val();
			model.set({"PERSON_COUNT": PERSON_COUNT});

			var PLAN_TYPE = $('#PLAN_TYPE').val();
			model.set({"PLAN_TYPE": PLAN_TYPE});

			var ISSUBSIDIZED = $('#ISSUBSIDIZED').val();
			model.set({"ISSUBSIDIZED":ISSUBSIDIZED});

			var pgrmType = $('#PGRM_TYPE').val();
			model.set({"PGRM_TYPE":pgrmType});

			var elegibilityComplete = $('#elegibilityComplete').val();
			model.set({"elegibilityComplete":elegibilityComplete});

			var providerLinkOnOff = $('#providerLinkOnOff').val();
			model.set({"providerLinkOnOff":providerLinkOnOff});

			$('#planDetailName').html(model.get("name"));
			var detailSummary = new DetailSummary({model:model});

			$('.planNameWrapper').append(detailSummary.getPlanName());
			//$('#tile').append(detailSummary.getTile());
			$('.ps-detail__highlights-container').append(detailSummary.getTile());

			$('#estimatedCostDetail').append(detailSummary.getEstimatedCost());
			//$('#totalExpenseEstimate').text(model.toJSON().estimatedTotalHealthCareCost);


			$('#providerSearchDetail').append(detailSummary.getProviderSearch());
			$('#productNameDetail').append(detailSummary.getPlanName());
			$('#productTypeDetail').append(detailSummary.getProductType());
			$('#csrDetail').append(detailSummary.getCsr());
			$('#hsaTypeDetail').append(detailSummary.getHSAType());
			$('#discountsDetail').append(detailSummary.getDiscount());
			$('#qualityRatingDetail').append(detailSummary.getQualityRating());
			$('#drugDetail').append(detailSummary.getDrugDetails());
			$('#networkTransparencyRatingDetail').append(detailSummary.getNetworkTransparencyRating());

			$('#havingBaby').append(detailSummary.getHavingBabySBC());
			$('#havingDiabetes').append(detailSummary.getHavingDiabetesSBC());
			$('#treatmentSimpleFracture').append(detailSummary.getHavingFractureSBC());

			$('.providerRows').remove();
			//$('.providerHeader').after(detailSummary.getProviderSearchInfo());
			$('.ps-detail__providers-list').html(detailSummary.getProviderSearchInfo());


			$('#planDeductible1Detail').append(detailSummary.getDeductible1());
			$('#planDeductible2Detail').append(detailSummary.getDeductible2());
			$('#planDeductible3Detail').append(detailSummary.getDeductible3());
			$('#planDeductible4Detail').append(detailSummary.getDeductible4());
			$('#planDeductible5Detail').append(detailSummary.getDeductible5());
			$('#planDeductible6Detail').append(detailSummary.getDeductible6());
			$('#planDeductible7Detail').append(detailSummary.getDeductible7());
			$('#planDeductible8Detail').append(detailSummary.getDeductible8());
			$('#planDeductible9Detail').append(detailSummary.getDeductible9());
			$('#planDeductible10Detail').append(detailSummary.getDeductible10());
			$('#planDeductible11Detail').append(detailSummary.getDeductible11());
			$('#planDeductible12Detail').append(detailSummary.getDeductible12());
			$('#planDeductibleOtherDetail').append(detailSummary.getOptionalDeductible());

			$('#doctorVisit1Detail').append(detailSummary.getDoctorVisit1());
			$('#doctorVisit2Detail').append(detailSummary.getDoctorVisit2());
			$('#doctorVisit3Detail').append(detailSummary.getDoctorVisit3());
			$('#doctorVisit4Detail').append(detailSummary.getDoctorVisit4());

			$('#test1Detail').append(detailSummary.getTest1());
			$('#test2Detail').append(detailSummary.getTest2());
			$('#test3Detail').append(detailSummary.getTest3());

			$('#drug1Detail').append(detailSummary.getDrug1());
			$('#drug2Detail').append(detailSummary.getDrug2());
			$('#drug3Detail').append(detailSummary.getDrug3());
			$('#drug4Detail').append(detailSummary.getDrug4());
			$('#drug5Detail').append(detailSummary.getDrug5());

			$('#outpatient1Detail').append(detailSummary.getOutpatient1());
			$('#outpatient2Detail').append(detailSummary.getOutpatient2());
			$('#outpatientServicesOfficeVisitsDetail').append(detailSummary.getOutpatientServicesOfficeVisits());

			$('#urgent1Detail').append(detailSummary.getUrgent1());
			$('#urgent2Detail').append(detailSummary.getUrgent2());
			$('#urgent3Detail').append(detailSummary.getUrgent3());
			$('#urgentProfessionalFeeDetail').append(detailSummary.getUrgentProfessionalFee());

			$('#hospital1Detail').append(detailSummary.getHospital1());
			$('#hospital2Detail').append(detailSummary.getHospital2());

			$('#mentalHealth1Detail').append(detailSummary.getMentalhealth1());
			$('#mentalHealth2Detail').append(detailSummary.getMentalhealth2());
			$('#mentalHealthInpatientProfFeeDetail').append(detailSummary.getMentalHealthInpatientProfFee());
			$('#mentalHealth3Detail').append(detailSummary.getMentalhealth3());
			$('#mentalHealth4Detail').append(detailSummary.getMentalhealth4());
			$('#mentalHealthSubDisorderInpProfFeeDetail').append(detailSummary.getMentalhealthSubDisorderInpProfFee());

			$('#pregnancy1Detail').append(detailSummary.getPregnancy1());
			$('#pregnancy2Detail').append(detailSummary.getPregnancy2());
			$('#pregnancyInpatientProfFeeDetail').append(detailSummary.getPregnancyInpatientProfFee());

			$('#specialNeed1Detail').append(detailSummary.getSpecialNeed1());
			$('#specialNeed2Detail').append(detailSummary.getSpecialNeed2());
			$('#specialNeed3Detail').append(detailSummary.getSpecialNeed3());
			$('#specialNeed4Detail').append(detailSummary.getSpecialNeed4());
			$('#specialNeed5Detail').append(detailSummary.getSpecialNeed5());
			$('#specialNeed6Detail').append(detailSummary.getSpecialNeed6());
			$('#specialNeed7Detail').append(detailSummary.getSpecialNeed7());
			$('#specialNeed8Detail').append(detailSummary.getSpecialNeed8());
			$('#specialNeed9Detail').append(detailSummary.getSpecialNeed9());
			$('#specialNeed10Detail').append(detailSummary.getSpecialNeed10());
			$('#specialNeed11Detail').append(detailSummary.getSpecialNeed11());
			$('#specialNeed12Detail').append(detailSummary.getSpecialNeed12());
			$('#specialNeed13Detail').append(detailSummary.getSpecialNeed13());
			$('#specialNeed14Detail').append(detailSummary.getSpecialNeed14());

			$('#childrensVision1Detail').append(detailSummary.getChildrensVision1());
			$('#childrensVision2Detail').append(detailSummary.getChildrensVision2());
			$('#hearingAidsDetail').append(detailSummary.getHearingAids());

			$('#childrensDental1Detail').append(detailSummary.getChildrensDental1());
			$('#childrensDental2Detail').append(detailSummary.getChildrensDental2());
			$('#childrensDental3Detail').append(detailSummary.getChildrensDental3());
			$('#childrensDental4Detail').append(detailSummary.getChildrensDental4());
			$('#childrensDental5Detail').append(detailSummary.getChildrensDental5());
			$('#childrensDental6Detail').append(detailSummary.getChildrensDental6());
			$('#childrensDental7Detail').append(detailSummary.getChildrensDental7());
			$('#childrensDental8Detail').append(detailSummary.getChildrensDental8());
			$('#childrensDental9Detail').append(detailSummary.getChildrensDental9());

			$('#simplifiedDeductibleDetail').append(detailSummary.getSimplifiedDeductibleDetail());
			$('#simplifiedSeparateDrugDeductibleDetail').append(detailSummary.getSimplifiedSeparateDrugDeductibleDetail());
			$('#simplifiedOOPMaxDetail').append(detailSummary.getSimplifiedOOPMaxDetail());
			$('#simplifiedOtherDeductibleDetail').append(detailSummary.getSimplifiedOtherDeductibleDetail());
			$('#simplifiedMaxCostPerPrescriptionDetail').append(detailSummary.getSimplifiedMaxCostPerPrescriptionDetail());
			$('#providerHeatMapDetailCount').append(detailSummary.getProviderHeatMapCountTpl());
			$('#providerHeatMapDetailButton').append(detailSummary.getProviderHeatMapButtonTpl());
			if($('#providerMapConfig').val() == 'ON'){
			providerDensity(model.toJSON().networkKey,model.toJSON().planId);
			}
		},

		dentalAddOne : function (model) {
			var self = this;

			var PERSON_COUNT = $('#PERSON_COUNT').val();
			model.set({"PERSON_COUNT": PERSON_COUNT});

			var PLAN_TYPE = $('#PLAN_TYPE').val();
			model.set({"PLAN_TYPE": PLAN_TYPE});

			var ISSUBSIDIZED = $('#ISSUBSIDIZED').val();
			model.set({"ISSUBSIDIZED":ISSUBSIDIZED});

			var pgrmType = $('#PGRM_TYPE').val();
			model.set({"PGRM_TYPE":pgrmType});

			var elegibilityComplete = $('#elegibilityComplete').val();
			model.set({"elegibilityComplete":elegibilityComplete});

			var providerLinkOnOff = $('#providerLinkOnOff').val();
			model.set({"providerLinkOnOff":providerLinkOnOff});

			$('#planDetailName').html(model.get("name"));
			var detailSummary = new DentalDetailSummary({model:model});

			$('.planNameWrapper').append(detailSummary.getPlanName());
			$('#productNameDetail').append(detailSummary.getPlanName());
			//$('#tile').append(detailSummary.getTile());
			$('.ps-detail__highlights-container').append(detailSummary.getTile());

			$('#planTypeDetail').append(detailSummary.getPlanType());
			$('#planTierDetail').append(detailSummary.getPlanTier());
			$('#premiumTypeDetail').append(detailSummary.getPremiumType());
			$('#planDeductibleFlyDetail').append(detailSummary.getDeductibleFly());
			$('#planDeductibleIndDetail').append(detailSummary.getDeductibleInd());
			$('#planOopMaxFlyDetail').append(detailSummary.getOopMaxFly());
			$('#planOopMaxIndDetail').append(detailSummary.getOopMaxInd());
			$('#planAdultDeductibleIndDetail').append(detailSummary.getPlanAdultDeductibleInd());
			$('#planAdultDeductibleFlyDetail').append(detailSummary.getPlanAdultDeductibleFly());
			$('#planAdultAnnualBenefitLimitIndDetail').append(detailSummary.getPlanAdultAnnualBenefitLimitInd());
			$('#planAdultAnnualBenefitLimitFlyDetail').append(detailSummary.getPlanAdultAnnualBenefitLimitFly());
			$('#planAdultOutOfPocketMaxIndDetail').append(detailSummary.getPlanAdultOutOfPocketMaxInd());
			$('#planAdultOutOfPocketMaxFlyDetail').append(detailSummary.getPlanAdultOutOfPocketMaxFly());
			$('#majorDentalCareAdultDetail').append(detailSummary.getMajorDentalCareAdult());
			$('#orthodontiaAdultDetail').append(detailSummary.getOrthodontiaAdult());
			$('#accidentalDentalDetail').append(detailSummary.getAccidentalDental());
			$('#basicDentalCareAdultDetail').append(detailSummary.getBasicDentalCareAdult());
			$('#rtnDentalAdultDetail').append(detailSummary.getRtnDentalAdult());
			$('#majorDentalCareChildDetail').append(detailSummary.getMajorDentalCareChild());
			$('#orthodontiaChildDetail').append(detailSummary.getOrthodontiaChild());
			$('#dentalCheckupChildrenDetail').append(detailSummary.getDentalCheckupChildren());
			$('#basicDentalCareChildDetail').append(detailSummary.getBasicDentalCareChild());


		},


		detailPlan: function () {
		  /*detail view should have atleast 2 plans to detail*/
		  if(this.collection.detailModels.length > 0){
			this.removeDetail();
			this.collection.page=1;
			this.collection.resetFieldFilter();
			this.collection.showDetail=true;
			this.collection.pager();
			//$('#sort').hide();
			$('#view').val('detail');
			this.addAll();
			this.collection.updateCompareSummary();
			this.collection.preserveOtherOptions();

			this.animateToTop();

		  }
		  else{ alert(plan_display['pd.label.selectOnePlan']);}
		},

		animateToTop: function(){
			$('body, html').animate({
				scrollTop: 0
			},0);
		},

		addToCart: function (e) {
			e.target.pageName= plan_display['pd.label.title.detail'];
			App.views.plans.addToCart(e);
		},

		backToAll : function(){
		  /*set detail view to false*/
		  this.removeDetail();
		  //$('#detailHead').hide();
		  //$('#sort').show();
		  this.collection.showDetail=false;
		  this.collection.resetFieldFilter();
		  this.collection.loadIssuers();
		  this.collection.preserveOtherOptions();
		  App.views.plans.filter();
		  location.hash = "";

		  //this.addingStyleToMiddleTile();
		},

		addingStyleToMiddleTile: function(){
        	/*
        		Clear the tiles after every 3rd Tile and add after new clearfix element to clear the float layout
        		and css class to every second tile:
        	*/
			$('#mainSummary').find('.jsTilesClearFix').remove();
			$('#mainSummary .tile').css({marginLeft: 0, marginRight:0}); //Required to overwrite .tile:nth-child(3n-1) from css as it is using on other pages
			$('#mainSummary .tile:nth-of-type(2n)').removeClass('middleTile'); //First remove clearfix and middleTile class as event fires twice so needed these code
        	$('#mainSummary .tile:nth-of-type(3n)').after('<div class="clearfix jsTilesClearFix"></div>');
        	$('#mainSummary .tile:nth-of-type(2n)').addClass('middleTile');
		},

		removeDetail : function(){
			//$('.planNameWrapper').find('.details').remove();
			//$('#detail').html("");
			//$('#tile').find('.details').remove();
			$('#estimatedCostDetail').find('.details').remove();
			$('#providerSearchDeÎtail').find('.details').remove();
			$('#productNameDetail').find('.details').remove();
			$('#productTypeDetail').find('.details').remove();
			$('#csrDetail').find('.details').remove();
			$('#hsaTypeDetail').find('.details').remove();
			$('#discountsDetail').find('.details').remove();
			$('#qualityRatingDetail').find('.details').remove();
			$('#drugDetail').find('.details').remove();
			$('#networkTransparencyRatingDetail').find('.details').remove();

			$('#havingBaby').find('.details').remove();
			$('#havingDiabetes').find('.details').remove();
			$('#treatmentSimpleFracture').find('.details').remove();

			$('.providerRows').remove();


			$('.ps-detail__highlights-container').html("");




			$('#planDeductible1Detail').find('.details').remove();
			$('#planDeductible2Detail').find('.details').remove();
			$('#planDeductible3Detail').find('.details').remove();
			$('#planDeductible4Detail').find('.details').remove();
			$('#planDeductible5Detail').find('.details').remove();
			$('#planDeductible6Detail').find('.details').remove();
			$('#planDeductible7Detail').find('.details').remove();
			$('#planDeductible8Detail').find('.details').remove();
			$('#planDeductible9Detail').find('.details').remove();
			$('#planDeductible10Detail').find('.details').remove();
			$('#planDeductible11Detail').find('.details').remove();
			$('#planDeductible12Detail').find('.details').remove();
			$('#planDeductibleOtherDetail').find('.plan-details').remove();

			$('#doctorVisit1Detail').find('.details').remove();
			$('#doctorVisit2Detail').find('.details').remove();
			$('#doctorVisit3Detail').find('.details').remove();
			$('#doctorVisit4Detail').find('.details').remove();

			$('#test1Detail').find('.details').remove();
			$('#test2Detail').find('.details').remove();
			$('#test3Detail').find('.details').remove();

			$('#drug1Detail').find('.details').remove();
			$('#drug2Detail').find('.details').remove();
			$('#drug3Detail').find('.details').remove();
			$('#drug4Detail').find('.details').remove();
			$('#drug5Detail').find('.details').remove();

			$('#outpatient1Detail').find('.details').remove();
			$('#outpatient2Detail').find('.details').remove();
			$('#outpatientServicesOfficeVisitsDetail').find('.details').remove();

			$('#urgent1Detail').find('.details').remove();
			$('#urgent2Detail').find('.details').remove();
			$('#urgent3Detail').find('.details').remove();
			$('#urgentProfessionalFeeDetail').find('.details').remove();

			$('#hospital1Detail').find('.details').remove();
			$('#hospital2Detail').find('.details').remove();

			$('#mentalHealth1Detail').find('.details').remove();
			$('#mentalHealth2Detail').find('.details').remove();
			$('#mentalHealthInpatientProfFeeDetail').find('.details').remove();
			$('#mentalHealth3Detail').find('.details').remove();
			$('#mentalHealth4Detail').find('.details').remove();
			$('#mentalHealthSubDisorderInpProfFeeDetail').find('.details').remove();

			$('#pregnancy1Detail').find('.details').remove();
			$('#pregnancy2Detail').find('.details').remove();
			$('#pregnancyInpatientProfFeeDetail').find('.details').remove();

			$('#specialNeed1Detail').find('.details').remove();
			$('#specialNeed2Detail').find('.details').remove();
			$('#specialNeed3Detail').find('.details').remove();
			$('#specialNeed4Detail').find('.details').remove();
			$('#specialNeed5Detail').find('.details').remove();
			$('#specialNeed6Detail').find('.details').remove();
			$('#specialNeed7Detail').find('.details').remove();
			$('#specialNeed8Detail').find('.details').remove();
			$('#specialNeed9Detail').find('.details').remove();
			$('#specialNeed10Detail').find('.details').remove();
			$('#specialNeed11Detail').find('.details').remove();
			$('#specialNeed12Detail').find('.details').remove();
			$('#specialNeed13Detail').find('.details').remove();
			$('#specialNeed14Detail').find('.details').remove();

			$('#childrensVision1Detail').find('.details').remove();
			$('#childrensVision2Detail').find('.details').remove();
			$('#hearingAidsDetail').find('.details').remove();

			$('#childrensDental1Detail').find('.details').remove();
			$('#childrensDental2Detail').find('.details').remove();
			$('#childrensDental3Detail').find('.details').remove();
			$('#childrensDental4Detail').find('.details').remove();
			$('#childrensDental5Detail').find('.details').remove();
			$('#childrensDental6Detail').find('.details').remove();
			$('#childrensDental7Detail').find('.details').remove();
			$('#childrensDental8Detail').find('.details').remove();
			$('#childrensDental9Detail').find('.details').remove();

			$('#simplifiedDeductibleDetail').find('.details').remove();
			$('#simplifiedSeparateDrugDeductibleDetail').find('.details').remove();
			$('#simplifiedOOPMaxDetail').find('.details').remove();
			$('#simplifiedOtherDeductibleDetail').find('.details').remove();
			$('#simplifiedMaxCostPerPrescriptionDetail').find('.details').remove();

			$('#providerHeatMapDetail').find('.details').empty();
		},

		removeDentalDetail : function(){
			//$('.back-to-all-plans-link-container').html("");
			$('#tile').find('.details').remove();
			$('#planTypeDetail').find('.details').remove();
			$('#planTierDetail').find('.details').remove();
			$('#premiumTypeDetail').find('.details').remove();
			$('#planDeductibleFlyDetail').find('.details, .column-placeholder').remove();
			$('#planDeductibleIndDetail').find('.details, .column-placeholder').remove();
			$('#planOopMaxFlyDetail').find('.details, .column-placeholder').remove();
			$('#planOopMaxIndDetail').find('.details, .column-placeholder').remove();
			$('#planAdultDeductibleIndDetail').find('.details, .column-placeholder').remove();
			$('#planAdultAnnualBenefitLimitIndDetail').find('.details, .column-placeholder').remove();
			$('#planAdultOutOfPocketMaxIndDetail').find('.details, .column-placeholder').remove();
			$('#planAdultDeductibleFlyDetail').find('.details, .column-placeholder').remove();
			$('#planAdultAnnualBenefitLimitFlyDetail').find('.details, .column-placeholder').remove();
			$('#planAdultOutOfPocketMaxFlyDetail').find('.details, .column-placeholder').remove();
			$('#planDeductibleOtherDetail').find('.details').remove();
			$('#orthodontiaAdultDetail').find('.details').remove();
			$('#majorDentalCareAdultDetail').find('.details').remove();
			$('#accidentalDentalDetail').find('.details').remove();
			$('#basicDentalCareAdultDetail').find('.details').remove();
			$('#rtnDentalAdultDetail').find('.details').remove();
			$('#orthodontiaChildDetail').find('.details').remove();
			$('#majorDentalCareChildDetail').find('.details').remove();
			$('#dentalCheckupChildrenDetail').find('.details').remove();
			$('#basicDentalCareChildDetail').find('.details').remove();
		}
	});

	if($('#insuranceType').val() == 'DENTAL'){

		var DentalDetailSummary = Backbone.View.extend({
			planNameTpl: _.template($('#planNameTpl').html()),
			tileTpl: _.template($('#tileTpl').html()),

			planTypeTpl: _.template($('#planTypeTplDetail').html()),
			planTierTpl: _.template($('#planTierTplDetail').html()),
			premiumTypeTpl: _.template($('#premiumTypeTplDetail').html()),
			deductibleIndTpl: _.template($('#deductibleIndTplDetail').html()),
			deductibleFlyTpl: _.template($('#deductibleFlyTplDetail').html()),
			benefitTpl: _.template($('#benefitTplDetail').html()),

			initialize: function() {
				this.model.bind('change', this.render, this);
				this.model.bind('destroy', this.remove, this);
			},

			render : function () {
				return this;
			},

			getPlanName : function () { return this.planNameTpl(this.model.toJSON()); },
			getTile : function () { return this.tileTpl(this.model.toJSON()); },
			getPlanType : function () {return this.planTypeTpl({NETWORK_TYPE :  this.model.toJSON().networkType});},
			getPlanTier : function () {return this.planTierTpl({LEVEL :  this.model.toJSON().level});},
			getPremiumType: function () {return this.premiumTypeTpl({DENTAL_GURANTEE :  this.model.toJSON().dentalGuarantee});},
			getDeductibleInd : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductibleFly : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getOopMaxInd : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getOopMaxFly : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getPlanAdultDeductibleInd : function () {
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] ? this.model.toJSON().optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] : null;
					return this.deductibleIndTpl({DEDUCTIBLE : duductible});
				}else{
					$('#planAdultDeductibleIndDetail').hide() ;
				}},
			getPlanAdultDeductibleFly : function () {
				if($('#stateCode').val() != 'ID'){
					var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] ? this.model.toJSON().optionalDeductible['DEDUCTIBLE_DENTAL_ADULT'] : null;
					return this.deductibleFlyTpl({DEDUCTIBLE : duductible});
				}else{
					$('#planAdultDeductibleFlyDetail').hide() ;
				}},
			getPlanAdultAnnualBenefitLimitInd : function () {
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['Adult Annual Benefit Limit'] ? this.model.toJSON().optionalDeductible['Adult Annual Benefit Limit'] : null;
				return this.deductibleIndTpl({DEDUCTIBLE : duductible});
				}else{
					$('#planAdultAnnualBenefitLimitIndDetail').hide() ;
				}},
			getPlanAdultAnnualBenefitLimitFly : function () {
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['Adult Annual Benefit Limit'] ? this.model.toJSON().optionalDeductible['Adult Annual Benefit Limit'] : null;
					return this.deductibleFlyTpl({DEDUCTIBLE : duductible});
				}else{
					$('#planAdultAnnualBenefitLimitFlyDetail').hide() ;
				}},
			getPlanAdultOutOfPocketMaxInd : function () {
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['Adult Out-of-pocket maximum'] ? this.model.toJSON().optionalDeductible['Adult Out-of-pocket maximum'] : null;
				return this.deductibleIndTpl({DEDUCTIBLE : duductible});
				}else{
					$('#planAdultOutOfPocketMaxIndDetail').hide() ;
				}},
			getPlanAdultOutOfPocketMaxFly : function () {
				if($('#stateCode').val() != 'ID'){
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 && this.model.toJSON().optionalDeductible['Adult Out-of-pocket maximum'] ? this.model.toJSON().optionalDeductible['Adult Out-of-pocket maximum'] : null;
					return this.deductibleFlyTpl({DEDUCTIBLE : duductible});
				}else{
					$('#planAdultOutOfPocketMaxFlyDetail').hide() ;
				}},
			getAccidentalDental : function () {
				if($('#stateCode').val() == 'ID' || $('#stateCode').val() == 'MN'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.ACCIDENTAL_DENTAL,
				BENEFIT2 : this.model.toJSON().planTier2.ACCIDENTAL_DENTAL,
				BENEFITOUT : this.model.toJSON().planOutNet.ACCIDENTAL_DENTAL,
				APPLIES : this.model.toJSON().planAppliesToDeduct.ACCIDENTAL_DENTAL,
				EXPLANATION : this.model.toJSON().benefitExplanation.ACCIDENTAL_DENTAL,
				EXCLUSION : this.model.toJSON().netwkException.ACCIDENTAL_DENTAL,
				BENEFIT_NAME: "",
				PrimaryCareCostSharingAfterSetNumberVisits: this.model.toJSON().primaryCareCostSharingAfterSetNumberVisits,
				PrimaryCareDeductOrCoinsAfterSetNumberCopays: this.model.toJSON().primaryCareDeductOrCoinsAfterSetNumberCopays,
				NC: false
				});
				}else{
					$('#accidentalDentalDetail').hide() ;
				}},
			getBasicDentalCareAdult : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_ADULT,
				BENEFIT2 : this.model.toJSON().planTier2.BASIC_DENTAL_CARE_ADULT,
				BENEFITOUT : this.model.toJSON().planOutNet.BASIC_DENTAL_CARE_ADULT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.BASIC_DENTAL_CARE_ADULT,
				EXPLANATION : this.model.toJSON().benefitExplanation.BASIC_DENTAL_CARE_ADULT,
				EXCLUSION : this.model.toJSON().netwkException.BASIC_DENTAL_CARE_ADULT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getBasicDentalCareChild : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_CHILD,
				BENEFIT2 : this.model.toJSON().planTier2.BASIC_DENTAL_CARE_CHILD,
				BENEFITOUT : this.model.toJSON().planOutNet.BASIC_DENTAL_CARE_CHILD,
				APPLIES : this.model.toJSON().planAppliesToDeduct.BASIC_DENTAL_CARE_CHILD,
				EXPLANATION : this.model.toJSON().benefitExplanation.BASIC_DENTAL_CARE_CHILD,
				EXCLUSION : this.model.toJSON().netwkException.BASIC_DENTAL_CARE_CHILD,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getDentalCheckupChildren : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.DENTAL_CHECKUP_CHILDREN,
				BENEFIT2 : this.model.toJSON().planTier2.DENTAL_CHECKUP_CHILDREN,
				BENEFITOUT : this.model.toJSON().planOutNet.DENTAL_CHECKUP_CHILDREN,
				APPLIES : this.model.toJSON().planAppliesToDeduct.DENTAL_CHECKUP_CHILDREN,
				EXPLANATION : this.model.toJSON().benefitExplanation.DENTAL_CHECKUP_CHILDREN,
				EXCLUSION : this.model.toJSON().netwkException.DENTAL_CHECKUP_CHILDREN,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },

			getMajorDentalCareAdult : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_ADULT,
				BENEFIT2 : this.model.toJSON().planTier2.MAJOR_DENTAL_CARE_ADULT,
				BENEFITOUT : this.model.toJSON().planOutNet.MAJOR_DENTAL_CARE_ADULT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.MAJOR_DENTAL_CARE_ADULT,
				EXPLANATION : this.model.toJSON().benefitExplanation.MAJOR_DENTAL_CARE_ADULT,
				EXCLUSION : this.model.toJSON().netwkException.MAJOR_DENTAL_CARE_ADULT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getMajorDentalCareChild : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_CHILD,
				BENEFIT2 : this.model.toJSON().planTier2.MAJOR_DENTAL_CARE_CHILD,
				BENEFITOUT : this.model.toJSON().planOutNet.MAJOR_DENTAL_CARE_CHILD,
				APPLIES : this.model.toJSON().planAppliesToDeduct.MAJOR_DENTAL_CARE_CHILD,
				EXPLANATION : this.model.toJSON().benefitExplanation.MAJOR_DENTAL_CARE_CHILD,
				EXCLUSION : this.model.toJSON().netwkException.MAJOR_DENTAL_CARE_CHILD,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getOrthodontiaAdult : function () {
				if($('#stateCode').val() == 'ID' || $('#stateCode').val() == 'MN'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.ORTHODONTIA_ADULT,
				BENEFIT2 : this.model.toJSON().planTier2.ORTHODONTIA_ADULT,
				BENEFITOUT : this.model.toJSON().planOutNet.ORTHODONTIA_ADULT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.ORTHODONTIA_ADULT,
				EXPLANATION : this.model.toJSON().benefitExplanation.ORTHODONTIA_ADULT,
				EXCLUSION : this.model.toJSON().netwkException.ORTHODONTIA_ADULT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				});
				}else{
					$('#orthodontiaAdultDetail').hide() ;
				}	},

			getOrthodontiaChild : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.ORTHODONTIA_CHILD,
				BENEFIT2 : this.model.toJSON().planTier2.ORTHODONTIA_CHILD,
				BENEFITOUT : this.model.toJSON().planOutNet.ORTHODONTIA_CHILD,
				APPLIES : this.model.toJSON().planAppliesToDeduct.ORTHODONTIA_CHILD,
				EXPLANATION : this.model.toJSON().benefitExplanation.ORTHODONTIA_CHILD,
				EXCLUSION : this.model.toJSON().netwkException.ORTHODONTIA_CHILD,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getRtnDentalAdult : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.RTN_DENTAL_ADULT,
				BENEFIT2 : this.model.toJSON().planTier2.RTN_DENTAL_ADULT,
				BENEFITOUT : this.model.toJSON().planOutNet.RTN_DENTAL_ADULT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.RTN_DENTAL_ADULT,
				EXPLANATION : this.model.toJSON().benefitExplanation.RTN_DENTAL_ADULT,
				EXCLUSION : this.model.toJSON().netwkException.RTN_DENTAL_ADULT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); }
		});
	}else{
		var DetailSummary = Backbone.View.extend({
			template: _.template($('#tmpDetail').html()),

			planNameTpl: _.template($('#planNameTpl').html()),
			tileTpl: _.template($('#tileTpl').html()),
			estimatedCostTpl: _.template($('#estimatedCostTplDetail').html()),
			providerSearchTpl: _.template($('#providerSearchLinkTplDetail').html()),
			productTypeTpl: _.template($('#productTypeTplDetail').html()),
			csrTpl: _.template($('#csrTplDetail').html()),
			hsaTypeTpl: _.template($('#hsaTypeTpl').html()),
			qualityRatingTplDetail: _.template($('#qualityRatingTplDetail').html()),
			drugTplDetail: _.template($('#drugTplDetail').html()),
			discountTpl: _.template($('#discountTplDetail').html()),
			deductibleIndTpl: _.template($('#deductibleIndTplDetail').html()),
			deductibleFlyTpl: _.template($('#deductibleFlyTplDetail').html()),
			benefitTpl: _.template($('#benefitTplDetail').html()),
			deductibleTpl: _.template($('#deductibleTplDetail').html()),
			simplifiedDeductibleTpl: _.template($('#simplifiedDeductibleTplDetail').html()),
			simplifiedOtherDeductibleTpl: _.template($('#simplifiedOtherDeductibleTplDetail').html()),
			maxCoinseForSpecialtyDrugsTpl: _.template($('#maxCoinseForSpecialtyDrugsTplDetail').html()),
			providerSearchInfoTpl: _.template($('#providerSearchInfoTplDetail').html()),
			networkTransparencyTpl: _.template($('#networkTransparencyTpl').html()),
			providerHeatMapCountTpl: _.template($('#providerHeatMapCountTpl').html()),
			providerHeatMapButtonTpl: _.template($('#providerHeatMapButtonTpl').html()),

			havingBabySBC :  _.template($('#havingBabySBC').html()),
			havingDiabetesSBC :  _.template($('#havingDiabetesSBC').html()),
			havingFractureSBC :  _.template($('#havingFractureSBC').html()),

			initialize: function() {
				this.model.bind('change', this.render, this);
				this.model.bind('destroy', this.remove, this);
			},

			render : function () {
				return this;
			},

			getPlanName : function () { return this.planNameTpl(this.model.toJSON()); },
			getTile : function () { return this.tileTpl(this.model.toJSON()); },
			getEstimatedCost : function () {
				if($('#stateCode').val() != 'ID'){
					return this.estimatedCostTpl({EXPENSE_ESTIMATE :  this.model.toJSON().estimatedTotalHealthCareCost, ANNUAL_PREMIUM : this.model.toJSON().annualPremiumAfterCredit});
				} else {
					return this.estimatedCostTpl({EXPENSE_ESTIMATE :  this.model.toJSON().expenseEstimate, ANNUAL_PREMIUM : this.model.toJSON().annualPremiumAfterCredit});
				}
			},
			getProviderSearch : function () {return this.providerSearchTpl({PROVIDER_LINK :  this.model.toJSON().providerLink});},
			getProductType : function () {return this.productTypeTpl({NETWORK_TYPE :  this.model.toJSON().networkType});},
			getCsr :  function () {return this.csrTpl({CSR :  this.model.toJSON().costSharing});},
			getHSAType : function () {return this.hsaTypeTpl({HSA :  this.model.toJSON().hsa});},
			getDiscount: function () {return this.discountTpl({COSTSHARING :  this.model.toJSON().costSharing});},
			getQualityRating : function () {return this.qualityRatingTplDetail({issuerQualityRating :  this.model.toJSON().issuerQualityRating});},
			getDrugDetails : function () {return this.drugTplDetail({prescriptionResponseList :  this.model.toJSON().prescriptionResponseList});},
			getProviderSearchInfo : function () {return this.providerSearchInfoTpl(this.model.toJSON());},
			getNetworkTransparencyRating : function () {return this.networkTransparencyTpl({networkTransparencyRating :  this.model.toJSON().networkTransparencyRating });},

			getHavingBabySBC : function () { return this.havingBabySBC({
				TOTAL_COST : this.model.toJSON().havingBabySBC,
			}); },
			getHavingDiabetesSBC : function () { return this.havingDiabetesSBC({
				TOTAL_COST : this.model.toJSON().havingDiabetesSBC,
			}); },
			getHavingFractureSBC : function () { return this.havingFractureSBC({
				TOTAL_COST : this.model.toJSON().simpleFractureSBC,
			}); },
			getProviderHeatMapCountTpl : function () {
				return this.providerHeatMapCountTpl({PROVIDER_NETWORK_KEY : this.model.toJSON().networkKey, PLAN_ID : this.model.toJSON().planId});
			},
			getProviderHeatMapButtonTpl : function () {
				return this.providerHeatMapButtonTpl({PROVIDER_NETWORK_KEY : this.model.toJSON().networkKey, PLAN_ID : this.model.toJSON().planId});
			},

			getDeductible1 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_INTG_MED_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible2 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_INTG_MED_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible3 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_INTG_MED_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible4 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_INTG_MED_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible5 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible6 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible7 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible8 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_DRUG : null; return this.deductibleIndTpl({DEDUCTIBLE : duductible}); },
			getDeductible9 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible10 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.DEDUCTIBLE_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible11 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_MEDICAL : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getDeductible12 : function () { var duductible = this.model.toJSON().planCosts != null ? this.model.toJSON().planCosts.MAX_OOP_DRUG : null; return this.deductibleFlyTpl({DEDUCTIBLE : duductible}); },
			getOptionalDeductible : function () { var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 ? this.model.toJSON().optionalDeductible : $('#planDeductibleOtherDetail').hide() ; return this.deductibleTpl({DEDUCTIBLES : duductible}); },


			getSimplifiedDeductibleDetail: function(){
				var PLAN_COSTS = this.model.toJSON().planCosts;
				var deductible = null;
				if(PLAN_COSTS.DEDUCTIBLE_INTG_MED_DRUG != null){
					deductible =  PLAN_COSTS.DEDUCTIBLE_INTG_MED_DRUG;
				} else if(PLAN_COSTS.DEDUCTIBLE_MEDICAL != null){
					deductible =  PLAN_COSTS.DEDUCTIBLE_MEDICAL;
				}
				return this.simplifiedDeductibleTpl({DEDUCTIBLE : deductible, IS_SEPARATE_DEDUCTIBLE: false});
			},

			getSimplifiedSeparateDrugDeductibleDetail: function(){
				var PLAN_COSTS = this.model.toJSON().planCosts;
				var deductible = null;
				if(PLAN_COSTS.DEDUCTIBLE_DRUG != null || $('#stateCode').val() === 'CT'){
					deductible =  PLAN_COSTS.DEDUCTIBLE_DRUG;
					$('#simplifiedSeparateDrugDeductibleDetail').show() ;
				}else{
					$('#simplifiedSeparateDrugDeductibleDetail').hide() ;
				}
				return this.simplifiedDeductibleTpl({DEDUCTIBLE : deductible, IS_SEPARATE_DEDUCTIBLE: true});
			},

			getSimplifiedOOPMaxDetail: function(){
				var PLAN_COSTS = this.model.toJSON().planCosts;
				var deductible = null;
				if(PLAN_COSTS.MAX_OOP_INTG_MED_DRUG != null){
					deductible =  PLAN_COSTS.MAX_OOP_INTG_MED_DRUG;
				} else if(PLAN_COSTS.MAX_OOP_MEDICAL != null){
					deductible =  PLAN_COSTS.MAX_OOP_MEDICAL;
				}
				return this.simplifiedDeductibleTpl({DEDUCTIBLE : deductible, IS_SEPARATE_DEDUCTIBLE: false});
			},

			getSimplifiedOtherDeductibleDetail: function(){
				var PLAN_COSTS = this.model.toJSON().planCosts;
				var duductible = this.model.toJSON().optionalDeductible != null && Object.keys(this.model.toJSON().optionalDeductible).length > 0 ? this.model.toJSON().optionalDeductible : $('#simplifiedOtherDeductibleDetail').hide() ;
				return this.simplifiedOtherDeductibleTpl({DEDUCTIBLES : deductible});
			},

			getSimplifiedMaxCostPerPrescriptionDetail : function () {return this.maxCoinseForSpecialtyDrugsTpl({
				BENEFIT1 : this.model.toJSON().maxCoinseForSpecialtyDrugs,
				BENEFITOUT : null
				});},

			getDoctorVisit1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.PRIMARY_VISIT,
				BENEFIT2 : this.model.toJSON().planTier2.PRIMARY_VISIT,
				BENEFITOUT : this.model.toJSON().planOutNet.PRIMARY_VISIT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.PRIMARY_VISIT,
				EXPLANATION : this.model.toJSON().benefitExplanation.PRIMARY_VISIT,
				EXCLUSION : this.model.toJSON().netwkException.PRIMARY_VISIT,
				BENEFIT_NAME: "PRIMARY_VISIT",
				PrimaryCareCostSharingAfterSetNumberVisits: this.model.toJSON().primaryCareCostSharingAfterSetNumberVisits,
				PrimaryCareDeductOrCoinsAfterSetNumberCopays: this.model.toJSON().primaryCareDeductOrCoinsAfterSetNumberCopays,
				NC: false
				}); },
			getDoctorVisit2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.SPECIAL_VISIT,
				BENEFIT2 : this.model.toJSON().planTier2.SPECIAL_VISIT,
				BENEFITOUT : this.model.toJSON().planOutNet.SPECIAL_VISIT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.SPECIAL_VISIT,
				EXPLANATION : this.model.toJSON().benefitExplanation.SPECIAL_VISIT,
				EXCLUSION : this.model.toJSON().netwkException.SPECIAL_VISIT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getDoctorVisit3 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.OTHER_PRACTITIONER_VISIT,
				BENEFIT2 : this.model.toJSON().planTier2.OTHER_PRACTITIONER_VISIT,
				BENEFITOUT : this.model.toJSON().planOutNet.OTHER_PRACTITIONER_VISIT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.OTHER_PRACTITIONER_VISIT,
				EXPLANATION : this.model.toJSON().benefitExplanation.OTHER_PRACTITIONER_VISIT,
				EXCLUSION : this.model.toJSON().netwkException.OTHER_PRACTITIONER_VISIT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getDoctorVisit4 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.PREVENT_SCREEN_IMMU,
				BENEFIT2 : this.model.toJSON().planTier2.PREVENT_SCREEN_IMMU,
				BENEFITOUT : this.model.toJSON().planOutNet.PREVENT_SCREEN_IMMU,
				APPLIES : this.model.toJSON().planAppliesToDeduct.PREVENT_SCREEN_IMMU,
				EXPLANATION : this.model.toJSON().benefitExplanation.PREVENT_SCREEN_IMMU,
				EXCLUSION : this.model.toJSON().netwkException.PREVENT_SCREEN_IMMU,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },

			getTest1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.LABORATORY_SERVICES,
				BENEFIT2 : this.model.toJSON().planTier2.LABORATORY_SERVICES,
				BENEFITOUT : this.model.toJSON().planOutNet.LABORATORY_SERVICES,
				APPLIES : this.model.toJSON().planAppliesToDeduct.LABORATORY_SERVICES,
				EXPLANATION : this.model.toJSON().benefitExplanation.LABORATORY_SERVICES,
				EXCLUSION : this.model.toJSON().netwkException.LABORATORY_SERVICES,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getTest2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.IMAGING_XRAY,
				BENEFIT2 : this.model.toJSON().planTier2.IMAGING_XRAY,
				BENEFITOUT : this.model.toJSON().planOutNet.IMAGING_XRAY,
				APPLIES : this.model.toJSON().planAppliesToDeduct.IMAGING_XRAY,
				EXPLANATION : this.model.toJSON().benefitExplanation.IMAGING_XRAY,
				EXCLUSION : this.model.toJSON().netwkException.IMAGING_XRAY,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getTest3 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.IMAGING_SCAN,
				BENEFIT2 : this.model.toJSON().planTier2.IMAGING_SCAN,
				BENEFITOUT : this.model.toJSON().planOutNet.IMAGING_SCAN,
				APPLIES : this.model.toJSON().planAppliesToDeduct.IMAGING_SCAN,
				EXPLANATION : this.model.toJSON().benefitExplanation.IMAGING_SCAN,
				EXCLUSION : this.model.toJSON().netwkException.IMAGING_SCAN,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },

			getDrug1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.GENERIC,
				BENEFIT2 : this.model.toJSON().planTier2.GENERIC,
				BENEFITOUT : this.model.toJSON().planOutNet.GENERIC,
				APPLIES : this.model.toJSON().planAppliesToDeduct.GENERIC,
				EXPLANATION : this.model.toJSON().benefitExplanation.GENERIC,
				EXCLUSION : this.model.toJSON().netwkException.GENERIC,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getDrug2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.PREFERRED_BRAND,
				BENEFIT2 : this.model.toJSON().planTier2.PREFERRED_BRAND,
				BENEFITOUT : this.model.toJSON().planOutNet.PREFERRED_BRAND,
				APPLIES : this.model.toJSON().planAppliesToDeduct.PREFERRED_BRAND,
				EXPLANATION : this.model.toJSON().benefitExplanation.PREFERRED_BRAND,
				EXCLUSION : this.model.toJSON().netwkException.PREFERRED_BRAND,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getDrug3 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.NON_PREFERRED_BRAND,
				BENEFIT2 : this.model.toJSON().planTier2.NON_PREFERRED_BRAND,
				BENEFITOUT : this.model.toJSON().planOutNet.NON_PREFERRED_BRAND,
				APPLIES : this.model.toJSON().planAppliesToDeduct.NON_PREFERRED_BRAND,
				EXPLANATION : this.model.toJSON().benefitExplanation.NON_PREFERRED_BRAND,
				EXCLUSION : this.model.toJSON().netwkException.NON_PREFERRED_BRAND,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getDrug4 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.SPECIALTY_DRUGS,
				BENEFIT2 : this.model.toJSON().planTier2.SPECIALTY_DRUGS,
				BENEFITOUT : this.model.toJSON().planOutNet.SPECIALTY_DRUGS,
				APPLIES : this.model.toJSON().planAppliesToDeduct.SPECIALTY_DRUGS,
				EXPLANATION : this.model.toJSON().benefitExplanation.SPECIALTY_DRUGS,
				EXCLUSION : this.model.toJSON().netwkException.SPECIALTY_DRUGS,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },

			getDrug5 : function () {return this.maxCoinseForSpecialtyDrugsTpl({
				BENEFIT1 : this.model.toJSON().maxCoinseForSpecialtyDrugs,
				BENEFITOUT : null
				});},

			getOutpatient1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.OUTPATIENT_FACILITY_FEE,
				BENEFIT2 : this.model.toJSON().planTier2.OUTPATIENT_FACILITY_FEE,
				BENEFITOUT : this.model.toJSON().planOutNet.OUTPATIENT_FACILITY_FEE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.OUTPATIENT_FACILITY_FEE,
				EXPLANATION : this.model.toJSON().benefitExplanation.OUTPATIENT_FACILITY_FEE,
				EXCLUSION : this.model.toJSON().netwkException.OUTPATIENT_FACILITY_FEE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getOutpatient2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.OUTPATIENT_SURGERY_SERVICES,
				BENEFIT2 : this.model.toJSON().planTier2.OUTPATIENT_SURGERY_SERVICES,
				BENEFITOUT : this.model.toJSON().planOutNet.OUTPATIENT_SURGERY_SERVICES,
				APPLIES : this.model.toJSON().planAppliesToDeduct.OUTPATIENT_SURGERY_SERVICES,
				EXPLANATION : this.model.toJSON().benefitExplanation.OUTPATIENT_SURGERY_SERVICES,
				EXCLUSION : this.model.toJSON().netwkException.OUTPATIENT_SURGERY_SERVICES,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },

			getOutpatientServicesOfficeVisits : function () {
				if($('#stateCode').val() != 'ID'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.OUTPATIENT_SERVICES_OFFICE_VISIT,
				BENEFIT2 : this.model.toJSON().planTier2.OUTPATIENT_SERVICES_OFFICE_VISIT,
				BENEFITOUT : this.model.toJSON().planOutNet.OUTPATIENT_SERVICES_OFFICE_VISIT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.OUTPATIENT_SERVICES_OFFICE_VISIT,
				EXPLANATION : this.model.toJSON().benefitExplanation.OUTPATIENT_SERVICES_OFFICE_VISIT,
				EXCLUSION : this.model.toJSON().netwkException.OUTPATIENT_SERVICES_OFFICE_VISIT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); }else{
					$('#outpatientServicesOfficeVisitsDetail').hide() ;
				}
			},

			getUrgent1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.EMERGENCY_SERVICES,
				BENEFIT2 : this.model.toJSON().planTier2.EMERGENCY_SERVICES,
				BENEFITOUT : this.model.toJSON().planOutNet.EMERGENCY_SERVICES,
				APPLIES : this.model.toJSON().planAppliesToDeduct.EMERGENCY_SERVICES,
				EXPLANATION : this.model.toJSON().benefitExplanation.EMERGENCY_SERVICES,
				EXCLUSION : this.model.toJSON().netwkException.EMERGENCY_SERVICES,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getUrgent2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.EMERGENCY_TRANSPORTATION,
				BENEFIT2 : this.model.toJSON().planTier2.EMERGENCY_TRANSPORTATION,
				BENEFITOUT : this.model.toJSON().planOutNet.EMERGENCY_TRANSPORTATION,
				APPLIES : this.model.toJSON().planAppliesToDeduct.EMERGENCY_TRANSPORTATION,
				EXPLANATION : this.model.toJSON().benefitExplanation.EMERGENCY_TRANSPORTATION,
				EXCLUSION : this.model.toJSON().netwkException.EMERGENCY_TRANSPORTATION,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getUrgent3 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.URGENT_CARE,
				BENEFIT2 : this.model.toJSON().planTier2.URGENT_CARE,
				BENEFITOUT : this.model.toJSON().planOutNet.URGENT_CARE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.URGENT_CARE,
				EXPLANATION : this.model.toJSON().benefitExplanation.URGENT_CARE,
				EXCLUSION : this.model.toJSON().netwkException.URGENT_CARE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getUrgentProfessionalFee : function () {
				if($('#stateCode').val() != 'ID'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.EMERGENCY_ROOM_PROFESSIONAL_FEE,
				BENEFIT2 : this.model.toJSON().planTier2.EMERGENCY_ROOM_PROFESSIONAL_FEE,
				BENEFITOUT : this.model.toJSON().planOutNet.EMERGENCY_ROOM_PROFESSIONAL_FEE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.EMERGENCY_ROOM_PROFESSIONAL_FEE,
				EXPLANATION : this.model.toJSON().benefitExplanation.EMERGENCY_ROOM_PROFESSIONAL_FEE,
				EXCLUSION : this.model.toJSON().netwkException.EMERGENCY_ROOM_PROFESSIONAL_FEE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); }else{
					$('#urgentProfessionalFeeDetail').hide();
				}
				},

			getHospital1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.INPATIENT_HOSPITAL_SERVICE,
				BENEFIT2 : this.model.toJSON().planTier2.INPATIENT_HOSPITAL_SERVICE,
				BENEFITOUT : this.model.toJSON().planOutNet.INPATIENT_HOSPITAL_SERVICE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.INPATIENT_HOSPITAL_SERVICE,
				EXPLANATION : this.model.toJSON().benefitExplanation.INPATIENT_HOSPITAL_SERVICE,
				EXCLUSION : this.model.toJSON().netwkException.INPATIENT_HOSPITAL_SERVICE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getHospital2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.INPATIENT_PHY_SURGICAL_SERVICE,
				BENEFIT2 : this.model.toJSON().planTier2.INPATIENT_PHY_SURGICAL_SERVICE,
				BENEFITOUT : this.model.toJSON().planOutNet.INPATIENT_PHY_SURGICAL_SERVICE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.INPATIENT_PHY_SURGICAL_SERVICE,
				EXPLANATION : this.model.toJSON().benefitExplanation.INPATIENT_PHY_SURGICAL_SERVICE,
				EXCLUSION : this.model.toJSON().netwkException.INPATIENT_PHY_SURGICAL_SERVICE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },

			getMentalhealth1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.MENTAL_HEALTH_OUT,
				BENEFIT2 : this.model.toJSON().planTier2.MENTAL_HEALTH_OUT,
				BENEFITOUT : this.model.toJSON().planOutNet.MENTAL_HEALTH_OUT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.MENTAL_HEALTH_OUT,
				EXPLANATION : this.model.toJSON().benefitExplanation.MENTAL_HEALTH_OUT,
				EXCLUSION : this.model.toJSON().netwkException.MENTAL_HEALTH_OUT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getMentalhealth2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.MENTAL_HEALTH_IN,
				BENEFIT2 : this.model.toJSON().planTier2.MENTAL_HEALTH_IN,
				BENEFITOUT : this.model.toJSON().planOutNet.MENTAL_HEALTH_IN,
				APPLIES : this.model.toJSON().planAppliesToDeduct.MENTAL_HEALTH_IN,
				EXPLANATION : this.model.toJSON().benefitExplanation.MENTAL_HEALTH_IN,
				EXCLUSION : this.model.toJSON().netwkException.MENTAL_HEALTH_IN,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getMentalHealthInpatientProfFee : function () {
				if($('#stateCode').val() != 'ID'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE,
				BENEFIT2 : this.model.toJSON().planTier2.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE,
				BENEFITOUT : this.model.toJSON().planOutNet.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE,
				EXPLANATION : this.model.toJSON().benefitExplanation.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE,
				EXCLUSION : this.model.toJSON().netwkException.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); }else{
					$('#mentalHealthInpatientProfFeeDetail').hide();
				}
				},
			getMentalhealth3 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.SUBSTANCE_OUTPATIENT_USE,
				BENEFIT2 : this.model.toJSON().planTier2.SUBSTANCE_OUTPATIENT_USE,
				BENEFITOUT : this.model.toJSON().planOutNet.SUBSTANCE_OUTPATIENT_USE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.SUBSTANCE_OUTPATIENT_USE,
				EXPLANATION : this.model.toJSON().benefitExplanation.SUBSTANCE_OUTPATIENT_USE,
				EXCLUSION : this.model.toJSON().netwkException.SUBSTANCE_OUTPATIENT_USE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getMentalhealth4 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.SUBSTANCE_INPATIENT_USE,
				BENEFIT2 : this.model.toJSON().planTier2.SUBSTANCE_INPATIENT_USE,
				BENEFITOUT : this.model.toJSON().planOutNet.SUBSTANCE_INPATIENT_USE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.SUBSTANCE_INPATIENT_USE,
				EXPLANATION : this.model.toJSON().benefitExplanation.SUBSTANCE_INPATIENT_USE,
				EXCLUSION : this.model.toJSON().netwkException.SUBSTANCE_INPATIENT_USE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },

			getMentalhealthSubDisorderInpProfFee : function () {
				if($('#stateCode').val() != 'ID'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE,
				BENEFIT2 : this.model.toJSON().planTier2.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE,
				BENEFITOUT : this.model.toJSON().planOutNet.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE,
				EXPLANATION : this.model.toJSON().benefitExplanation.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE,
				EXCLUSION : this.model.toJSON().netwkException.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				});}else{
					$('#mentalHealthSubDisorderInpProfFeeDetail').hide();
				}
				},

			getPregnancy1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.PRENATAL_POSTNATAL,
				BENEFIT2 : this.model.toJSON().planTier2.PRENATAL_POSTNATAL,
				BENEFITOUT : this.model.toJSON().planOutNet.PRENATAL_POSTNATAL,
				APPLIES : this.model.toJSON().planAppliesToDeduct.PRENATAL_POSTNATAL,
				EXPLANATION : this.model.toJSON().benefitExplanation.PRENATAL_POSTNATAL,
				EXCLUSION : this.model.toJSON().netwkException.PRENATAL_POSTNATAL,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getPregnancy2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.DELIVERY_IMP_MATERNITY_SERVICES,
				BENEFIT2 : this.model.toJSON().planTier2.DELIVERY_IMP_MATERNITY_SERVICES,
				BENEFITOUT : this.model.toJSON().planOutNet.DELIVERY_IMP_MATERNITY_SERVICES,
				APPLIES : this.model.toJSON().planAppliesToDeduct.DELIVERY_IMP_MATERNITY_SERVICES,
				EXPLANATION : this.model.toJSON().benefitExplanation.DELIVERY_IMP_MATERNITY_SERVICES,
				EXCLUSION : this.model.toJSON().netwkException.DELIVERY_IMP_MATERNITY_SERVICES,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getPregnancyInpatientProfFee : function () {
				if($('#stateCode').val() != 'ID'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE,
				BENEFIT2 : this.model.toJSON().planTier2.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE,
				BENEFITOUT : this.model.toJSON().planOutNet.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE,
				EXPLANATION : this.model.toJSON().benefitExplanation.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE,
				EXCLUSION : this.model.toJSON().netwkException.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				});}else{
					$('#pregnancyInpatientProfFeeDetail').hide();
				}
				},

			getSpecialNeed1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.HOME_HEALTH_SERVICES,
				BENEFIT2 : this.model.toJSON().planTier2.HOME_HEALTH_SERVICES,
				BENEFITOUT : this.model.toJSON().planOutNet.HOME_HEALTH_SERVICES,
				APPLIES : this.model.toJSON().planAppliesToDeduct.HOME_HEALTH_SERVICES,
				EXPLANATION : this.model.toJSON().benefitExplanation.HOME_HEALTH_SERVICES,
				EXCLUSION : this.model.toJSON().netwkException.HOME_HEALTH_SERVICES,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getSpecialNeed2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.OUTPATIENT_REHAB_SERVICES,
				BENEFIT2 : this.model.toJSON().planTier2.OUTPATIENT_REHAB_SERVICES,
				BENEFITOUT : this.model.toJSON().planOutNet.OUTPATIENT_REHAB_SERVICES,
				APPLIES : this.model.toJSON().planAppliesToDeduct.OUTPATIENT_REHAB_SERVICES,
				EXPLANATION : this.model.toJSON().benefitExplanation.OUTPATIENT_REHAB_SERVICES,
				EXCLUSION : this.model.toJSON().netwkException.OUTPATIENT_REHAB_SERVICES,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getSpecialNeed3 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.HABILITATION,
				BENEFIT2 : this.model.toJSON().planTier2.HABILITATION,
				BENEFITOUT : this.model.toJSON().planOutNet.HABILITATION,
				APPLIES : this.model.toJSON().planAppliesToDeduct.HABILITATION,
				EXPLANATION : this.model.toJSON().benefitExplanation.HABILITATION,
				EXCLUSION : this.model.toJSON().netwkException.HABILITATION,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getSpecialNeed4 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.SKILLED_NURSING_FACILITY,
				BENEFIT2 : this.model.toJSON().planTier2.SKILLED_NURSING_FACILITY,
				BENEFITOUT : this.model.toJSON().planOutNet.SKILLED_NURSING_FACILITY,
				APPLIES : this.model.toJSON().planAppliesToDeduct.SKILLED_NURSING_FACILITY,
				EXPLANATION : this.model.toJSON().benefitExplanation.SKILLED_NURSING_FACILITY,
				EXCLUSION : this.model.toJSON().netwkException.SKILLED_NURSING_FACILITY,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getSpecialNeed5 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.DURABLE_MEDICAL_EQUIP,
				BENEFIT2 : this.model.toJSON().planTier2.DURABLE_MEDICAL_EQUIP,
				BENEFITOUT : this.model.toJSON().planOutNet.DURABLE_MEDICAL_EQUIP,
				APPLIES : this.model.toJSON().planAppliesToDeduct.DURABLE_MEDICAL_EQUIP,
				EXPLANATION : this.model.toJSON().benefitExplanation.DURABLE_MEDICAL_EQUIP,
				EXCLUSION : this.model.toJSON().netwkException.DURABLE_MEDICAL_EQUIP,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getSpecialNeed6 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.HOSPICE_SERVICE,
				BENEFIT2 : this.model.toJSON().planTier2.HOSPICE_SERVICE,
				BENEFITOUT : this.model.toJSON().planOutNet.HOSPICE_SERVICE,
				APPLIES : this.model.toJSON().planAppliesToDeduct.HOSPICE_SERVICE,
				EXPLANATION : this.model.toJSON().benefitExplanation.HOSPICE_SERVICE,
				EXCLUSION : this.model.toJSON().netwkException.HOSPICE_SERVICE,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getSpecialNeed7 : function () {
				if($('#stateCode').val() == 'ID'){
				return this.benefitTpl({
	            BENEFIT1 : this.model.toJSON().planTier1.CHIROPRACTIC,
	            BENEFIT2 : this.model.toJSON().planTier2.CHIROPRACTIC,
	            BENEFITOUT : this.model.toJSON().planOutNet.CHIROPRACTIC,
	            APPLIES : this.model.toJSON().planAppliesToDeduct.CHIROPRACTIC,
	            EXPLANATION : this.model.toJSON().benefitExplanation.CHIROPRACTIC,
	            EXCLUSION : this.model.toJSON().netwkException.CHIROPRACTIC,
	            BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",     NC: false
	            });}else{
					$('#specialNeed7Detail').hide();
				}  },
	        getSpecialNeed8 : function () { return this.benefitTpl({
	            BENEFIT1 : this.model.toJSON().planTier1.ACUPUNCTURE,
	            BENEFIT2 : this.model.toJSON().planTier2.ACUPUNCTURE,
	            BENEFITOUT : this.model.toJSON().planOutNet.ACUPUNCTURE,
	            APPLIES : this.model.toJSON().planAppliesToDeduct.ACUPUNCTURE,
	            EXPLANATION : this.model.toJSON().benefitExplanation.ACUPUNCTURE,
	            EXCLUSION : this.model.toJSON().netwkException.ACUPUNCTURE,
	            BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",     NC: false
	                }); },
	        getSpecialNeed9 : function () { return this.benefitTpl({
	            BENEFIT1 : this.model.toJSON().planTier1.REHABILITATIVE_SPEECH_THERAPY,
	            BENEFIT2 : this.model.toJSON().planTier2.REHABILITATIVE_SPEECH_THERAPY,
	            BENEFITOUT : this.model.toJSON().planOutNet.REHABILITATIVE_SPEECH_THERAPY,
	            APPLIES : this.model.toJSON().planAppliesToDeduct.REHABILITATIVE_SPEECH_THERAPY,
	            EXPLANATION : this.model.toJSON().benefitExplanation.REHABILITATIVE_SPEECH_THERAPY,
	            EXCLUSION : this.model.toJSON().netwkException.REHABILITATIVE_SPEECH_THERAPY,
	            BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",     NC: false
	                }); },
	        getSpecialNeed10 : function () { return this.benefitTpl({
	            BENEFIT1 : this.model.toJSON().planTier1.REHABILITATIVE_PHYSICAL_THERAPY,
	            BENEFIT2 : this.model.toJSON().planTier2.REHABILITATIVE_PHYSICAL_THERAPY,
	            BENEFITOUT : this.model.toJSON().planOutNet.REHABILITATIVE_PHYSICAL_THERAPY,
	            APPLIES : this.model.toJSON().planAppliesToDeduct.REHABILITATIVE_PHYSICAL_THERAPY,
	            EXPLANATION : this.model.toJSON().benefitExplanation.REHABILITATIVE_PHYSICAL_THERAPY,
	            EXCLUSION : this.model.toJSON().netwkException.REHABILITATIVE_PHYSICAL_THERAPY,
	            BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",     NC: false
	                }); },
	        getSpecialNeed11 : function () { return this.benefitTpl({
	            BENEFIT1 : this.model.toJSON().planTier1.WELL_BABY,
	            BENEFIT2 : this.model.toJSON().planTier2.WELL_BABY,
	            BENEFITOUT : this.model.toJSON().planOutNet.WELL_BABY,
	            APPLIES : this.model.toJSON().planAppliesToDeduct.WELL_BABY,
	            EXPLANATION : this.model.toJSON().benefitExplanation.WELL_BABY,
	            EXCLUSION : this.model.toJSON().netwkException.WELL_BABY,
	            BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",     NC: false
	                }); },
	        getSpecialNeed12 : function () { return this.benefitTpl({
	            BENEFIT1 : this.model.toJSON().planTier1.ALLERGY_TESTING,
	            BENEFIT2 : this.model.toJSON().planTier2.ALLERGY_TESTING,
	            BENEFITOUT : this.model.toJSON().planOutNet.ALLERGY_TESTING,
	            APPLIES : this.model.toJSON().planAppliesToDeduct.ALLERGY_TESTING,
	            EXPLANATION : this.model.toJSON().benefitExplanation.ALLERGY_TESTING,
	            EXCLUSION : this.model.toJSON().netwkException.ALLERGY_TESTING,
	            BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",     NC: false
	                }); },
	        getSpecialNeed13 : function () { return this.benefitTpl({
	            BENEFIT1 : this.model.toJSON().planTier1.DIABETES_EDUCATION,
	            BENEFIT2 : this.model.toJSON().planTier2.DIABETES_EDUCATION,
	            BENEFITOUT : this.model.toJSON().planOutNet.DIABETES_EDUCATION,
	            APPLIES : this.model.toJSON().planAppliesToDeduct.DIABETES_EDUCATION,
	            EXPLANATION : this.model.toJSON().benefitExplanation.DIABETES_EDUCATION,
	            EXCLUSION : this.model.toJSON().netwkException.DIABETES_EDUCATION,
	            BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",     NC: false
	                }); },
	       getSpecialNeed14 : function () { return this.benefitTpl({
	            BENEFIT1 : this.model.toJSON().planTier1.NUTRITIONAL_COUNSELING,
	            BENEFIT2 : this.model.toJSON().planTier2.NUTRITIONAL_COUNSELING,
	            BENEFITOUT : this.model.toJSON().planOutNet.NUTRITIONAL_COUNSELING,
	            APPLIES : this.model.toJSON().planAppliesToDeduct.NUTRITIONAL_COUNSELING,
	            EXPLANATION : this.model.toJSON().benefitExplanation.NUTRITIONAL_COUNSELING,
	            EXCLUSION : this.model.toJSON().netwkException.NUTRITIONAL_COUNSELING,
	            BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",     NC: false
	                }); },

			getChildrensVision1 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.RTN_EYE_EXAM_CHILDREN,
				BENEFIT2 : this.model.toJSON().planTier2.RTN_EYE_EXAM_CHILDREN,
				BENEFITOUT : this.model.toJSON().planOutNet.RTN_EYE_EXAM_CHILDREN,
				APPLIES : this.model.toJSON().planAppliesToDeduct.RTN_EYE_EXAM_CHILDREN,
				EXPLANATION : this.model.toJSON().benefitExplanation.RTN_EYE_EXAM_CHILDREN,
				EXCLUSION : this.model.toJSON().netwkException.RTN_EYE_EXAM_CHILDREN,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getChildrensVision2 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.GLASSES_CHILDREN,
				BENEFIT2 : this.model.toJSON().planTier2.GLASSES_CHILDREN,
				BENEFITOUT : this.model.toJSON().planOutNet.GLASSES_CHILDREN,
				APPLIES : this.model.toJSON().planAppliesToDeduct.GLASSES_CHILDREN,
				EXPLANATION : this.model.toJSON().benefitExplanation.GLASSES_CHILDREN,
				EXCLUSION : this.model.toJSON().netwkException.GLASSES_CHILDREN,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },
			getHearingAids : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.HEARING_AIDS,
				BENEFIT2 : this.model.toJSON().planTier2.HEARING_AIDS,
				BENEFITOUT : this.model.toJSON().planOutNet.HEARING_AIDS,
				APPLIES : this.model.toJSON().planAppliesToDeduct.HEARING_AIDS,
				EXPLANATION : this.model.toJSON().benefitExplanation.HEARING_AIDS,
				EXCLUSION : this.model.toJSON().netwkException.HEARING_AIDS,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: false
				}); },

			getChildrensDental1 : function () {
				if($('#stateCode').val() == 'ID'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.ACCIDENTAL_DENTAL,
				BENEFIT2 : this.model.toJSON().planTier2.ACCIDENTAL_DENTAL,
				BENEFITOUT : this.model.toJSON().planOutNet.ACCIDENTAL_DENTAL,
				APPLIES : this.model.toJSON().planAppliesToDeduct.ACCIDENTAL_DENTAL,
				EXPLANATION : this.model.toJSON().benefitExplanation.ACCIDENTAL_DENTAL,
				EXCLUSION : this.model.toJSON().netwkException.ACCIDENTAL_DENTAL,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				});}else{
					$('#childrensDental1Detail').hide();
				} },
			getChildrensDental2 : function () {
				if($('#stateCode').val() == 'ID' || $('#stateCode').val() == 'MN'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_ADULT,
				BENEFIT2 : this.model.toJSON().planTier2.BASIC_DENTAL_CARE_ADULT,
				BENEFITOUT : this.model.toJSON().planOutNet.BASIC_DENTAL_CARE_ADULT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.BASIC_DENTAL_CARE_ADULT,
				EXPLANATION : this.model.toJSON().benefitExplanation.BASIC_DENTAL_CARE_ADULT,
				EXCLUSION : this.model.toJSON().netwkException.BASIC_DENTAL_CARE_ADULT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				}); }else{
					$('#childrensDental2Detail').hide();
				}},
			getChildrensDental3 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.BASIC_DENTAL_CARE_CHILD,
				BENEFIT2 : this.model.toJSON().planTier2.BASIC_DENTAL_CARE_CHILD,
				BENEFITOUT : this.model.toJSON().planOutNet.BASIC_DENTAL_CARE_CHILD,
				APPLIES : this.model.toJSON().planAppliesToDeduct.BASIC_DENTAL_CARE_CHILD,
				EXPLANATION : this.model.toJSON().benefitExplanation.BASIC_DENTAL_CARE_CHILD,
				EXCLUSION : this.model.toJSON().netwkException.BASIC_DENTAL_CARE_CHILD,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				}); },
			getChildrensDental4 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.DENTAL_CHECKUP_CHILDREN,
				BENEFIT2 : this.model.toJSON().planTier2.DENTAL_CHECKUP_CHILDREN,
				BENEFITOUT : this.model.toJSON().planOutNet.DENTAL_CHECKUP_CHILDREN,
				APPLIES : this.model.toJSON().planAppliesToDeduct.DENTAL_CHECKUP_CHILDREN,
				EXPLANATION : this.model.toJSON().benefitExplanation.DENTAL_CHECKUP_CHILDREN,
				EXCLUSION : this.model.toJSON().netwkException.DENTAL_CHECKUP_CHILDREN,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				}); },
			getChildrensDental5 : function () {
				if($('#stateCode').val() == 'ID' || $('#stateCode').val() == 'MN'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_ADULT,
				BENEFIT2 : this.model.toJSON().planTier2.MAJOR_DENTAL_CARE_ADULT,
				BENEFITOUT : this.model.toJSON().planOutNet.MAJOR_DENTAL_CARE_ADULT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.MAJOR_DENTAL_CARE_ADULT,
				EXPLANATION : this.model.toJSON().benefitExplanation.MAJOR_DENTAL_CARE_ADULT,
				EXCLUSION : this.model.toJSON().netwkException.MAJOR_DENTAL_CARE_ADULT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				}); }else{
					$('#childrensDental5Detail').hide();
				}},
			getChildrensDental6 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.MAJOR_DENTAL_CARE_CHILD,
				BENEFIT2 : this.model.toJSON().planTier2.MAJOR_DENTAL_CARE_CHILD,
				BENEFITOUT : this.model.toJSON().planOutNet.MAJOR_DENTAL_CARE_CHILD,
				APPLIES : this.model.toJSON().planAppliesToDeduct.MAJOR_DENTAL_CARE_CHILD,
				EXPLANATION : this.model.toJSON().benefitExplanation.MAJOR_DENTAL_CARE_CHILD,
				EXCLUSION : this.model.toJSON().netwkException.MAJOR_DENTAL_CARE_CHILD,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				}); },
			getChildrensDental7 : function () {
				if($('#stateCode').val() == 'ID'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.ORTHODONTIA_ADULT,
				BENEFIT2 : this.model.toJSON().planTier2.ORTHODONTIA_ADULT,
				BENEFITOUT : this.model.toJSON().planOutNet.ORTHODONTIA_ADULT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.ORTHODONTIA_ADULT,
				EXPLANATION : this.model.toJSON().benefitExplanation.ORTHODONTIA_ADULT,
				EXCLUSION : this.model.toJSON().netwkException.ORTHODONTIA_ADULT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				});}else{
					$('#childrensDental7Detail').hide();
				} },
			getChildrensDental8 : function () { return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.ORTHODONTIA_CHILD,
				BENEFIT2 : this.model.toJSON().planTier2.ORTHODONTIA_CHILD,
				BENEFITOUT : this.model.toJSON().planOutNet.ORTHODONTIA_CHILD,
				APPLIES : this.model.toJSON().planAppliesToDeduct.ORTHODONTIA_CHILD,
				EXPLANATION : this.model.toJSON().benefitExplanation.ORTHODONTIA_CHILD,
				EXCLUSION : this.model.toJSON().netwkException.ORTHODONTIA_CHILD,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				}); },
			getChildrensDental9 : function () {
				if($('#stateCode').val() == 'ID'){
				return this.benefitTpl({
				BENEFIT1 : this.model.toJSON().planTier1.RTN_DENTAL_ADULT,
				BENEFIT2 : this.model.toJSON().planTier2.RTN_DENTAL_ADULT,
				BENEFITOUT : this.model.toJSON().planOutNet.RTN_DENTAL_ADULT,
				APPLIES : this.model.toJSON().planAppliesToDeduct.RTN_DENTAL_ADULT,
				EXPLANATION : this.model.toJSON().benefitExplanation.RTN_DENTAL_ADULT,
				EXCLUSION : this.model.toJSON().netwkException.RTN_DENTAL_ADULT,
				BENEFIT_NAME: "", PrimaryCareCostSharingAfterSetNumberVisits: "", PrimaryCareDeductOrCoinsAfterSetNumberCopays: "",	NC: true
				}); }else{
					$('#childrensDental9Detail').hide();
				}}

		});
	}

	var providerDensity = function(networkKey, planId){
		if(networkKey == null || networkKey == ""){
			$("#providerHeatDetail_"+planId+"_"+networkKey).html("N/A");
            $("#viewMapDetail_"+planId+"_"+networkKey).remove();
		}else{

            var requestData = {
                            "searchKey" : "",
                            "userZip" : $("#zipcode").val(),
                            "currentPage" : 1,
                            "pageSize" : 1,
                            "otherDistance" : $("#providerDetailDistance").val(),
                            "searchType" : 'doctor',
                            "networkId": networkKey
                        };
			$.ajax({
				type: 'GET',
				url: '/hix/provider/doctors/search',
				data: requestData,
                dataType: 'json',
                async: false,
				success: function(data) {
					if(data){
						if(data.total_hits == 0){
							$("#providerHeatDetail_"+planId+"_"+networkKey).html("N/A");
							$("#viewMapDetail_"+planId+"_"+networkKey).remove();
						}else{
							$("#providerHeatDetail_"+planId+"_"+networkKey).html(addCommas(data.total_hits));
						}
					}
				},
				error: function(){
				$("#providerHeatDetail_"+planId+"_"+networkKey).html("N/A");
                    $("#viewMapDetail_"+planId+"_"+networkKey).remove();
				}
			});
		}
	};

	function addCommas(input) {
	    if (_.isUndefined(input)) return "";
	    return input.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
})(App.views);
