import { DrugSearchTypeaheadComponent } from './components/drug-search-typeahead.component';
import { DoctorCardComponent } from './components/doctor-card.component';
import { DocSearchTypeaheadComponent } from './components/doc-search-typeahead/doc-search-typeahead.component';
import { InputGroupComponents } from './components/input-group.component';
import { SubmitSetComponent } from './components/forms/submit-set/submit-set.component';
import { HeaderComponent } from './components/header/header.component';
import { DocWhoComponent } from './components/forms/doc-who/doc-who.component';
import { DrugWhichComponent } from './components/forms/drug-which/drug-which.component';
import { PrescriptionSearchComponent } from './components/forms/prescription-search/prescription-search.component';
import { DocVisitComponent } from './components/forms/doc-visit/doc-visit.component';
import { MainComponent } from './components/main/main.component';
import { Routes, RouterModule } from '@angular/router';
import { Http } from '@angular/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader } from '@ngx-translate/core';
import { TranslateModule } from '@ngx-translate/core';
import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';


export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const appRoutes: Routes = [
    {
        path: "dst", component: MainComponent
    },
    {
        path: "**", component: MainComponent
    }
]
describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                MainComponent,
                DocVisitComponent,
                PrescriptionSearchComponent,
                DocWhoComponent,
                DrugWhichComponent,
                HeaderComponent,
                SubmitSetComponent,

                InputGroupComponents,
                DocSearchTypeaheadComponent,
                DrugSearchTypeaheadComponent,
                DoctorCardComponent,
            ],
            imports: [
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [Http]
                    }
                }),
                RouterModule.forRoot(appRoutes),
            ]
        }).compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it(`should have as title 'app works!'`, async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('app works!');
    }));

    it('should render title in a h1 tag', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('app works!');
    }));
});
