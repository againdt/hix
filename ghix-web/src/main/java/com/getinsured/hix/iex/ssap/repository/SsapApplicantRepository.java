package com.getinsured.hix.iex.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.ssap.model.SsapApplicant;

/**
 * 
 * @author Abhijeet Duraphe
 
 */
@Repository
public interface SsapApplicantRepository extends JpaRepository<SsapApplicant, Long> {
	/**
	 * Returns list of applicant's for given SSAP id.
	 *
	 * @param ssapApplicationId SSAP id.
	 * @return list of applicant's for given id.
	 */
	List<SsapApplicant> findBySsapApplicationId(final long ssapApplicationId);

	/**
	 * Finds applicant by given GUID.
	 * @param applicantGuid applicant GUID.
	 * @return {@link SsapApplicant}.
	 */
	SsapApplicant findByApplicantGuid(final String applicantGuid);

	@Query("Select applicant from SsapApplicant as applicant where applicant.applicantGuid = :applicantGuid and applicant.ssapApplication.id = :applicationId")
	SsapApplicant findByApplicantGuidAndAppId(@Param("applicantGuid") String applicantGuid, @Param("applicationId") long applicationId);

	/**
	 * Returns list of applicant's for given SSAP id and Applying for coverage.
	 *
	 * @param ssapApplicationId SSAP id, applyingForCoverage.
	 * @return list of applicant's for given id.
	 */
	List<SsapApplicant> findBySsapApplicationIdAndApplyingForCoverage(final long ssapApplicationId, String applyingForCoverage);

}
