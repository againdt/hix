export default name => {

    const {
        firstName,
        middleName,
        lastName,
        suffix
    } = name;

    const isExist = value => (value !== '' && value !== null) ? ` ${value}` : '';

    return `${firstName ? firstName : ''}${isExist(middleName)}${lastName ? ` ${lastName}` : ''}${isExist(suffix)}`;
};