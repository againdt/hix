<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page isELIgnored="false"%>
<link type="text/css" rel="stylesheet"
	href="<c:url value="/resources/css/qdp.css" />" />
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
<div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message code="label.plans"/></a></li>
            <li><a href="#"><spring:message code='label.addNewQDP'/></a></li>
            <li><spring:message code='label.linkDentalPlanRates'/></li>
        </ul><!--page-breadcrumb ends-->
		<h1><a name="skip"></a><spring:message code='label.addNewQDP'/></h1>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
				<div class="header">
					<!--  beginning of side bar -->
					<h4 class="margin0"><spring:message code='label.steps'/></h4>
				</div>
					<ol class="nav nav-list">
						<li class="done"><a href="#"><spring:message code='label.linkDentalPlanDetails'/></a></li>
						<li class="done"><a href="#"><spring:message code='label.linkDentalPlanBenefits'/></a></li>
						<li class="active"><a href="#"><spring:message code='label.linkDentalPlanRates'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.linkDentalProviderNetwork'/></a></li>
						<li class="link"><a href="#"><spring:message code='label.reviewandconfirm'/></a></li>
					</ol>
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
			<div class="header">
            	<h4 class="margin0"><spring:message code='label.step3'/>: <spring:message code='label.linkDentalPlanRates'/></h4>
            </div>
				<form class="form-horizontal" id="frmUploadRates" name="frmUploadRates" action="<c:url value="/admin/planmgmt/submitqdprates" />" method="POST">
                	<df:csrfToken/>
                	<table class="table table-border-none marginL5">
						<tr>
							<td class="span3 txt-right"><spring:message code='label.issuer'/></td>
							<td class="span9"><strong>${issuerName}</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message code='label.planName'/></td>
							<td><strong>${planName}</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message code='label.planNumber'/></td>
							<td><strong>${planNumber}</strong></td>
						</tr>
					</table>
                        
					<input type="hidden" id="rateFile" name="rateFile" /> 
                    <input type="hidden" id="id" name="id" value="${planId}" /> 
                    <input type="hidden" id="isEdit" name="isEdit" value="${isEdit}" />
				</form>
				<form class="form-horizontal form-bottom" id="frmRates" name="frmRates" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
					<df:csrfToken/>
                    <div class="control-group">
                        <label for="planRates" class="control-label"><spring:message code='label.planRate' /> </label>
                        <div class="controls">
                            <span>
                                <input type="file" id="rates" name="rates" class="input-file"> &nbsp; <button type="button" title="<spring:message code='label.upload'/>" class="btn" onclick="validCsv('#rates','#frmRates','#rateFile_error')"><spring:message code='label.upload'/></button>
                                &nbsp;<a href=<c:url value="/resources/sampleTemplates/qdp_rates.csv" />><spring:message code='label.downloadTemplate' /></a> 
                                <input type="hidden" name="fileType" value="rates" />
                            </span>
                            <div id="rateFile_error" class="">${fileError}</div>
                            <div id="ratesFileName" class="help">${ratesFileName}</div>
                        </div>
                        <!-- end of controls-->
                    </div>
                    <!-- end of control-group -->

                    <div class="form-actions">
                        <button type="submit" name="submitBtn" id="submitBtn" class="btn btn-primary paddingTB6" onclick="formSubmit()" ><spring:message code='label.saveAndContinue' javaScriptEscape='true'/></button>
                    </div>
                    <!-- end of form-actions -->
					
				</form>
			</div>
			<!-- end of span9 -->
	
		</div>
		<!--  end of row-fluid -->
	</div>

<script type="text/javascript">
	function validCsv(fileElementId, formElementId, errorElementId) {
		var ext = $(fileElementId).val().split('.').pop().toLowerCase();
		var allow = new Array('csv');
		if (jQuery.inArray(ext, allow) == -1) {
			$(errorElementId)
					.html(
							"<label class='error'><span> <em class='excl'>!</em><spring:message code='label.fileTypeError'/></span></label>");
			return false;
		} else {
			$(formElementId).submit();
		}
	}

	function formSubmit() {
		$("#rateFile_error").html("");
		$("#frmUploadRates").submit();
	}
	$(document).ready(function() {
		var accreditationUpload = {
			//target:        '#certiName',   // target element(s) to be updated with server response 
			beforeSubmit : submitAccreditationRequest, // pre-submit callback 
			success : submitAccreditationResponse
		// post-submit callback 
		};
		$('#frmRates').ajaxForm(accreditationUpload);
		isFileReq();
	});

	function submitAccreditationResponse(responseText, statusText, xhr, $form) {
		var val = responseText.split(",");
		if (val[0] != 'Error') {
			$("#ratesFileName").text(val[1]);
			$("#rateFile").val(val[2]);
		} else {
			$("#rateFile_error")
					.html(
							"<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
		}
	}

	function submitAccreditationRequest(formData, jqForm, options) {
		var fileName = $('#rates').val();
		if (fileName.length > 0) {
			return true;
		}
		return false;
	}

	function isFileReq() {
		if ("false" == '${isEdit}') {
			$("#rateFile").rules("add", "required");
		}
	}

	var validator = $("#frmUploadRates")
			.validate(
					{
						ignore : "",
						rules : {
							rateFile : false
						},
						messages : {
							rateFile : {
								required : "<span> <em class='excl'>!</em><spring:message code='error.rateFileRequired'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error").attr('class',
									'error help-block');
						}
					});
	jQuery("#frmRates").validate({});

	$('.complete').each(
			function() {
				var completeStep = $(this).html();
				var replaceExpr = /[0-9]*\./gi;
				$(this).html(
						completeStep.replace(replaceExpr,
								'<i class="icon icon-ok"></i>'));
			})
</script>
