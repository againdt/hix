<link href="resources/css/slideshow-css.css" rel="stylesheet"  type="text/css" />
<script type="text/javascript" src="resources/js/slideshow.js"></script>
<style type="text/css">

body {
padding-top:25px;
}

#main .container {
    min-height: 225px;
}

.clear-left {
clear:left;
}

.img-polaroid {
margin:10px;
}

figcaption {
font-size:80%;
font-weight:bold;
text-align:center;

}

figcaption span {
display:block;
line-height:10px;
}
</style>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<div class="row-fluid">
			<div id="gallery">
				<div id="slides">
					<div class="slide"> <img src="<gi:cdnurl value="/resources/images/Eli-brokers-L.jpg" />"  width="465" height="274" alt="Group of call-center people" title="Group of call-center people" />
						<span class="left">
							<h2>Consumers</h2>
							<p>Helping Californians enroll in the highest quality health plans and government assistance.</p>
							<p><a class="btn btn-primary large" href="<c:url value="/account/signup/consumer" />">First time here? Sign Up</a> 
							<p class="small">Already a member? <a href="<c:url value="account/user/login" />">Sign In</a></p>
						</span>
					</div>

					<div class="slide"> <img src="<gi:cdnurl value="/resources/images/Eli-brokers-L.jpg" />"  width="465" height="274" alt="Group of call-center people" title="Group of call-center people" />
						<span class="left">
							<h2>Agent &amp; Navigators</h2>
							<p>Helping Californians enroll in the highest quality health plans and government assistance.</p>
							<p><a class="btn btn-primary large" href="<c:url value="/account/signup/broker" />">First time here? Sign Up</a> 
							<p class="small">Already a member? <a href="<c:url value="account/user/login" />">Sign In</a></p>
						</span>
					</div>
					<div class="slide"><img src="<gi:cdnurl value="/resources/images/Eli-providers-L.jpg" />" width="465" height="274" alt="Healthcare providers" title="Healthcare providers" />
						<span class="left">
							<h2>Compare Doctors &amp; Hospitals</h2>
							<p>Find Doctors &amp; Dentists, Compare Hospital Quality of Care, Compare Procedure  Costs</p>
							<p><a class="btn btn-primary large" href="<c:url value="/provider/search/" />">Get Started</a> 
							<p class="small">Doctors and Hospital Administrators <a href="#" data-toggle="modal" data-target="#myModal">Register</a></p>
							<p class="small">Returning Doctors and Hospital Administrators <a href="#" data-toggle="modal" data-target="#myModal">Sign In</a></p>						
						</span>
					</div>
					<div class="slide"><img src="<gi:cdnurl value="/resources/images/Eli-insurers-L.jpg" />" width="465" height="274" alt="A couple filling out a form" title="A couple filling out a form"  /> 
						<span class="left">
							<h2>Insurers</h2>
							<p>Manage the health plans you want to offer on the California Exchange. </p>
							<%-- <p><a class="btn btn-primary large" href="<c:url value="/planmgmt/carrier/signup" />">First time here? Sign Up</a>  --%>
							<p class="small">Already a member? <a href="<c:url value="account/user/login" />">Sign In</a></p>
						</span>			   	
					</div>
				</div>
				<div id="menu">
					<ul>
						<li class="menuItem"><a href="">Consumers</a></li>
						<li class="menuItem"><a href="">Agent</a></li>
						<li class="menuItem"><a href="">Doctors &amp; Hospitals</a></li>
						<li class="menuItem last"><a href="">Insurers</a></li>
					</ul>
				</div>
	 	 	</div>

			<!-- Example row of columns -->
			<!-- <div class="row-fluid">
				<div class="gutter10">
					<div class="span9">
						<h3>Message from the Governor</h3>
					
							<div class="img-polaroid  pull-left">
							<img  class="" alt="Office of the Insurance Commissioner of Minnesota" src="<gi:cdnurl value="/resources/images/mn_governor.jpg" />">
							<figcaption class="">Mark Dayton <span>Governor</span></figcaption>	
							</div>	
							<p>It is an honor to offer my fellow Minnesotans the opportunity to shop for health care coverage on the Minnesota Health Care Exchange.  As the father of two grown sons, I know the importance of providing health care for your family.  The Affordable Care Act now provides Minnesotans with the opportunity to provide 100&#37; health care coverage to our citizens.  For those who in the past have found insurance coverage unaffordable, the ACA provides assistance through tax credits and cost sharing subsidies.  To find out if you are eligible for this help, you must use the exchange.  I hope you will secure coverage for your family as soon as possible.</p>
							
						<p>Governor Dayton has devoted his life to improving social equality and economic opportunity for all Americans.  Governor Dayton has made securing health care for Minnesotans one of his priorities since taking office. To learn more about the Affordable Care Act or the Health Insurance Exchange, go to:  <a href="http://mn.gov/commerce/insurance/topics/medical/exchange/index.jsp" target="_blank">http://mn.gov/commerce/insurance/topics/medical/exchange/index.jsp</a></p>
					</div>
					<div class="span3">
							<h3>Secure Data Exchange</h3>
							<p>On the following pages you will be asked to provide your social security number (SSN) and date of birth.  This information is necessary to use the exchange. By providing your SSN and date of birth, the Exchange can securely connect with federal and state agencies including the Social Security Administration (SSA) and the Internal Revenue Service (IRS) to gather information about your eligibility and save time during the application process. </p>   
						
							<h3>Privacy Protection</h3>
							<p>The Minnesota Health Insurance Exchange safeguards your information provided through the exchange. To protect your privacy, we will ensure that this information will only be accessible  by Minnesota employees who are directly involved in helping to determine if you qualify for help paying for health care costs. All of this information is securely stored with multiple protections against unauthorized access. </p>
					</div>
				</div> -->
			
			
			<div class="notes" style="display:none">
					<div class="span">
						<p>This page represents the starting point for interaction between the Exchange and all its constituencies&mdash;Employers, Employees, Agent, Navigators, Carriers, State residents, Exchange operators, Doctors, Physician Clinics and Hospitals.  The page is structured so that each constituency can easily navigate to their view of the Exchange.  
						</p>
					</div>
			</div>
		</div>
		

	    <div id="myModal" class="modal hide fade">
		    <div class="modal-header">
		   		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    	<h3>&nbsp;</h3>
		    </div>
		    <div class="modal-body">
		    	<h3 class="alert alert-info">This feature is not available in this prototype.</h3>
		    </div>
		    <div class="modal-footer">
			    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
		    </div>
	    </div>		
		
		
	</div><!-- end of container -->
</div>
	
	<script type="text/javascript">
		$('#myModal').modal("hide")
	</script>
