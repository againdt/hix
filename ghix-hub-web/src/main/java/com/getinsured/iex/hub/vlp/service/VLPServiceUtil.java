/**
 * 
 */
package com.getinsured.iex.hub.vlp.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.utils.GhixUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Utility class for the VLP service operations
 * 
 * @author Nikhil Talreja
 * @since 14-Jan-2014
 *
 */

@SuppressWarnings("unchecked")
public final class VLPServiceUtil {
	
	private VLPServiceUtil(){
		
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VLPServiceUtil.class);
	
	/**
	 * Checks if the date is in the required format
	 * Creates an XML Gregorian Calendar Object type from the date string
	 * 
	 * @author - Nikhil Talreja
	 * @since - 14-Jan-2014
	 *
	 * @param date - Date as string
	 * @param dateFormat - Date format of the string
	 * @return XMLGregorianCalendar form of the date
	 * 
	 * @throws ParseException - If the beginning of the specified string cannot be parsed as the required format
	 * @throws DatatypeConfigurationException -  If the implementation is not available or cannot be instantiated.
	 */
	public static XMLGregorianCalendar stringToXMLDate(String date, String dateFormat) throws ParseException, DatatypeConfigurationException{
		
		if(date == null){
			return null;
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		sdf.parse(date);
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
	}
	
	/**
	 * Checks if the date is in the required format
	 * Converts XML Gregorian Calendar Object type to required date string type
	 * 
	 * @author - Nikhil Talreja
	 * @since - 10-Feb-2014
	 *
	 * @param date - XMLGregorianCalendar Date
	 * @param dateFormat - Date format of the string
	 * @return String form of the date
	 * 
	 * @throws ParseException - If the beginning of the specified string cannot be parsed as the required format
	 * @throws DatatypeConfigurationException -  If the implementation is not available or cannot be instantiated.
	 */
	public static String xmlDateToString(XMLGregorianCalendar date, String dateFormat) throws ParseException, DatatypeConfigurationException{
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		return sdf.format(date.toGregorianCalendar().getTime());
	}
	
	/**
	 * Map to hold response codes returned from HUB
	 * 
	 * These will be used by the service response handler
	 * 
	 */
	private static Map<String,String> responseCodes = null;
	
	static{
		
		VLPServiceUtil vLPServiceUtil = new VLPServiceUtil();
		
		InputStream in = vLPServiceUtil.getClass().getClassLoader().getResourceAsStream("VLPResponseCodes.xml");
		StringWriter writer = new StringWriter();
		try {
			IOUtils.copy(in, writer);
		} catch (IOException e) {
			LOGGER.error("Unable to load response codes from file",e);
		}
		
		XStream xStream = GhixUtils.getXStreamStaxObject();
		xStream.alias("responseCodes", Map.class);
		xStream.registerConverter(vLPServiceUtil.new MapEntryConverter());
		
		responseCodes = (Map<String, String>) xStream.fromXML(writer.toString());
		
		if(responseCodes != null){
			LOGGER.debug("Loaded " + responseCodes.size() +" response codes");
		}
		else{
			LOGGER.debug("No response codes loaded");
		}
	}
	
	/**
	 * Returns the response description for a response code
	 * 
	 * @author Nikhil Talreja
	 * @since 07-Feb-2014
	 */
	public static String getResponseDescription(String responseCode){
		return responseCodes.get(responseCode);
	}
	
	/**
	 * Converter class used for xStream API
	 * xStream API is used to convert .xml file containing response codes
	 * to the static map being used to hold the response codes
	 * 
	 * @author Nikhil Talreja
	 * @since 07-Feb-2014
	 */
	class MapEntryConverter implements Converter {
		@SuppressWarnings("rawtypes")
		public boolean canConvert(final Class clazz) {
			return AbstractMap.class.isAssignableFrom(clazz);
		}

		public void marshal(final Object value,
				final HierarchicalStreamWriter writer,
				final MarshallingContext context) {

			final Map<String, String> map = (AbstractMap<String, String>) value;
			for (final Entry<String, String> entry : map.entrySet()) {
				writer.startNode(entry.getKey().toString());
				writer.setValue(entry.getValue().toString());
				writer.endNode();
			}

		}

		public Object unmarshal(final HierarchicalStreamReader reader,
				final UnmarshallingContext context) {

			final Map<String, String> map = new HashMap<String, String>();

			while (reader.hasMoreChildren()) {
				reader.moveDown();
				map.put(reader.getNodeName(), reader.getValue());
				reader.moveUp();
			}
			return map;
		}
	}
	
	public static void main(String[] args) throws IOException {
		LOGGER.info(VLPServiceUtil.getResponseDescription("HE010037"));
	}
	
}
