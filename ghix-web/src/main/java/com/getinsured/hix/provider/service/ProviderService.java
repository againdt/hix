package com.getinsured.hix.provider.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.Facility;
import com.getinsured.hix.model.Physician;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Provider;
import com.getinsured.hix.model.Specialty;

public interface ProviderService {

	/* Doctor search methods */

	List<Physician> findDoctors(String name, String gender, int specialities,
			String languages, String doctorNearZip);

	List<Physician> findDoctorsForNetworkAndPlan(Integer planId, Integer networkId,
			String name, String gender, Integer specialities, String languages,
			String dentistNearZip);

	/* Dentist search methods */

	List<Physician> findDentists(String name, String gender, int specialities,
			String languages, String dentistNearZip);

	List<Physician> findDentistsForNetworkAndPlan(int planId, int networkId,
			String name, String gender, int specialities, String languages,
			String dentistNearZip);

	/* Hospital/Facilities search methods */

	List<Facility> findHospitals(String name, String hospitalType,
			String hospitalNearZip);

	List<Facility> findHospitalsForNetworkAndPlan(Integer planId, Integer networkId,
			String name, String hospitalType, String hospitalNearZip);

	Facility findFacility(int id);

	/* Both Doctor/Dentist search methods */

	Physician findPhysician(int id);

	/* Specialties search methods */

	List<Specialty> getParentSpecialtiesByGroupName(String groupName);

	List<Specialty> getChildSpecialtiesByParentId(String groupName,
			int parentSpecialtyId);

	Specialty findSpecialtyById(int specialtyId);

	boolean isChildSpecialtiesExists(String groupName, int specialtyId);

	/* Plan, Network,..etc related search methods */

	Plan getPlanByNetworkId(int networkId);
	
	Page<Physician> getPhysicianOrDentist(String name, boolean isFullName, String cityName, String zipCode, String physicianType, Pageable pageable);

	Page<Facility> findFacility(String name, String cityName, String zipCode, String type, Pageable pageable);
	
	List<Specialty> getSpecialityList();
	
	String getProviderByGroupKey(String groupKey,Provider.ProviderType providerType);

	Page<ProviderLoadStatusDTO> getProviderLoadStatusDataList(int page, int size);

	boolean uploadProviderFileAndSaveTrackingDataInDB(MultipartFile fileToUpload, String selectedYear, String selectedType, int createdBy, StringBuffer errorMessage);

	Page<ManageProvidersDTO> getProviderDataList(String nameFilter, String specialityFilter, String networkIdFilter, int page, int size);

	Page<ManageFacilitiesDTO> getFacilityDataList(String facilityFilter, String specialityFilter, String networkIdFilter, int page, int size);
}
