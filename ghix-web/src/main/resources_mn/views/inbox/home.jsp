<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>


<div class="gutter10-tb">
	<%-- <div class="l-page-breadcrumb">
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a
					href="<c:url value="/broker/employers"/>"><spring:message
							code="label.employers" /></a></li>
				<li>Active</li>
				<li>${searchText}</li>
				<li>Message</li>
			</ul>
		</div>
	</div> --%>
<c:if test="${message != null}">
<div class="errorblock alert alert-info">
	<p>
		${message}
	</p>
</div>
</c:if>
	<div class="row-fluid">
		<a href="<c:url value="/indportal#"/>" class="cp-link border-b"><i class="icon-caret-left"></i> <spring:message code="pd.label.title.backTodashboard"/></a>
	</div>
	<div class="row-fluid">
		<div class="pull-left" id="notificationMessages">
			<h3>
				<spring:message code="pd.label.title.notifications"/>
				<small>
					<c:choose>
						<c:when test="${page_title == 'Searched messages'}">
							<span class="num-inbox-items">${totalRecords + 0}</span> items has been searched
						</c:when>
						<c:when test="${page_title == 'Sent Messages'}">
							<span class="num-inbox-items">${totalRecords + 0}</span> items in Sent
						</c:when>
						<c:when test="${page_title == 'Message Drafts'}">
							<span class="num-inbox-items">${totalRecords + 0}</span> items in Drafts
						</c:when>
						<c:when test="${page_title == 'Archived messages'}">
							<span class="num-inbox-items">${totalRecords + 0}</span> items in Archived
						</c:when>
						<c:otherwise>
							<span class="num-inbox-items">${totalRecords + 0}</span> <spring:message code="pd.inbox.items.in.inbox"/>;
							<span class="num-unread-items unreadRecords">${totalUnreadRecords + 0}</span> <spring:message code="pd.inbox.unread"/>
						</c:otherwise>
					</c:choose> 
				</small>
			</h3>
		</div>
		<c:if test="${page_title != 'Archived messages' && page_title != 'Searched messages'}">
			<div class="pull-right gutter10">
				<div class="dropdown gutter10 bulk-action" style="display: none;">
					<span>(<span class="selected"></span> items selected) &nbsp;</span> 
					<a class="dropdown-toggle btn btn-primary btn-small" data-toggle="dropdown" href="#">
						Bulk Action <i class="icon-cog icon-white"></i><i class="caret icon-white"></i>
					</a>
	
					<ul class="pull-right dropdown-menu" role="menu">
						<li><a href="javascript: bulkActions('Archive');"><i class="icon-briefcase"></i> Archive</a></li>
						
						<c:if test="${page_title == 'Inbox'}">
							<li><a href="javascript: bulkActions('Read');"><i class="icon-folder-close"></i> Mark Read</a></li>
							<li><a href="javascript: bulkActions('Unread');"><i class="icon-folder-open"></i> Mark Unread</a></li>
						</c:if>
					</ul>
				</div>
			</div>
		</c:if>
	</div>
	<div class="row-fluid">
		<form class="form-vertical" id="frmSecureInbox" name="frmSecureInbox" action="${pageContext.request.contextPath}/inbox/secureInboxSend" method="post" enctype="multipart/form-data">
			<df:csrfToken/>
			<input type="hidden" id="messageId" name="messageId" />
			<input type="hidden" id="actionName" name="actionName" />
			<input type="hidden" id="pageNumber" name="pageNumber" value="${pageNumber}" />
			<input type="hidden" id="sortOrder" name="sortOrder" value="${sortOrder}" />
			<input type="hidden" id="sortBy" name="sortBy" value="${sortBy}" />
			<input type="hidden" id="page_title" name="page_title" value="${page_title}" />
			<input type="hidden" id="totalRecords" name="totalRecords" value="${totalRecords + 0}" />
			
			<div>
				<div class="span3" id="sidebar">
					<div class="search">
						<div class="header">
							<h4><spring:message code="pd.inbox.search"/></h4>
						</div>
						<div class="gutter10 ${page_title == 'Searched messages' ? 'sidenavActive' : 'gray graybg'} border-custom">
						<label for="searchText" class="aria-hidden"><spring:message code="pd.inbox.search"/></label>
							<div class="control-group">
								<div class="input-append">
									<input type="text" id="searchText" name="searchText" value="${searchText}" class="span9">
									<button class="btn" onclick="return doInboxSearch();">
										<span class="aria-hidden">search</span><i class="icon-search"></i>
									</button>
								</div>
							</div>
						</div>
						<!-- /.search -->
					</div>
					<div class="folders">
					<div class="header">
						<h4><spring:message code="pd.inbox.folders"/></h4>
					</div>
						
						<div class="gray graybg">
							<ul class="nav nav-list">
								<li class="${page_title == 'Inbox' ? 'active' : ''}">
									<a href="secureInbox">
										<spring:message code="pd.inbox.title"/> <span class="badge num-inbox-items unreadRecords">${totalUnreadRecords + 0}</span>
									</a>
								</li>
<%-- 								<li class="${page_title == 'Sent Messages' ? 'active' : ''}"><a href="secureInboxSent">Sent</a></li> --%>
<%-- 								<li class="${page_title == 'Message Drafts' ? 'active' : ''}"><a href="secureInboxDraft">Drafts</a></li> --%>
<%-- 								<li class="${page_title == 'Archived messages' ? 'active' : ''}"><a href="secureInboxArchive">Archive</a></li> --%>
							</ul>
						</div>
						<!-- /.folders-->
					</div>
<!-- 					<a href="#new-msg" class="btn span11 btn-block btn-primary" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();"> -->
<!-- 						<i class="icon-envelope icon-white"></i> New Message -->
<!-- 					</a> -->
				</div>

				<!-- /.sidebar-->
				<div class="span9" id="rightpanel">
					<jsp:include page="${includePage}"></jsp:include>
				</div>
				<!--gutter10 -->
			</div>
		</form>
	</div>
	<!--rightpanel -->
</div>
<!--gutter10-lr -->

<script type="text/javascript">
//Hide bulk button
	$(document).ready(function() {
		setPageTitle('<spring:message code="indportal.portalhome.myinbox"/>');
	});
	
	$('.bulk-action').hide();

	//Assign class checed to checked table row
	$('input:checkbox').change(function() {
		
		if ($(this).is(":checked")) {
			$(this).parents('tbody tr').addClass("checked");
		}
		else {
			$(this).parents('tbody tr').removeClass("checked");
		}
	});

	// Add multiple select / deselect functionality
	$(function() {
		$('.checkall').click(function() {
			
			if ($('input.checkbox').attr('checked', this.checked)) {
				$('.checkbox').parents('tbody tr').toggleClass("checked");
			}
			else if ($('input.checkbox').attr('checked', this.checked)) {
				$('.checkbox').parents('tbody tr').toggleClass("checked");
			}
			showHideBulkAction();
		});
	});

	//Show bulk button
	$('input:checkbox').change(function() {
		showHideBulkAction();
	});
	
	function showHideBulkAction() {
		
		var checkbox = $('.checkbox:checked').length;
		$('.selected').html(checkbox);

		if (checkbox > 0) {
			$('.bulk-action').show();
		}
		else if (checkbox < 1) {
			$('.bulk-action').hide();
		}
	}
	
	function bulkActions(actionName) {

		if(actionName) {
			
			var chkSize = $('.checkbox:checked').length;
			
			if(chkSize && chkSize > 0) {
				
				var chkValues = "";
				
				for(var i = 0; i < chkSize; i++) {
					
					if($('.checkbox:checked')[i] && $('.checkbox:checked')[i].value) {
						chkValues += $('.checkbox:checked')[i].value + ",";
					}
				}
				
				if(chkValues) {
					singleAction(actionName, chkValues);
				}
			}
		}
	}
	
	function singleAction(actionName, messageIds) {
		
		var formObj = document.forms["frmSecureInbox"];
		var messageIdNode = formObj.elements["messageId"];
		var actionNameNode = formObj.elements["actionName"];
		
		if (messageIdNode && actionNameNode && messageIds && actionName) {
			messageIdNode.value = messageIds;
			actionNameNode.value = actionName;
			doSubmit('frmSecureInbox', 'secureInboxAction');
		}
	}
	
	function callPrint() {
		var contentId = "rightpanel";
		var prtContent = document.getElementById(contentId);
		var WinPrint = window.open('', '', 'letf=100,top=100,width=600,height=600');
		WinPrint.document.write(prtContent.innerHTML);
		WinPrint.document.close();
		WinPrint.focus();
		
		setTimeout(function(){WinPrint.print()},800);	
		//WinPrint.close() 

	}

	function doReadSubmit(messageId) {

		var formObj = document.forms["frmSecureInbox"];
		var messageIdNode = formObj.elements["messageId"];
		//var page_title = formObj.elements["page_title"];

		if (messageIdNode && !isNaN(messageId)) {
			messageIdNode.value = messageId;
			doSubmit('frmSecureInbox', 'secureInboxRead');
		}
	}
	
	function doInboxSearch() {
		var searchTextNode = document.forms["frmSecureInbox"].elements["searchText"];
		
		if(null != searchTextNode && "undefined" != searchTextNode && null != searchTextNode.value && "undefined" != searchTextNode.value && "" !== searchTextNode.value) {
			window.location = window.encodeURI('secureInboxSearch?searchText=' + searchTextNode.value);
		}
		
		return false;
	}

	function doSubmit(formName, url) {

		if (formName && url) {
			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
	}
</script>