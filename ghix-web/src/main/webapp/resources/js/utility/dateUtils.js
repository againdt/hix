/**
 * Date Utility script.
 * @version 1.0
 * @since Sep 19,2013
 */

/**
 * Function to trim beginning and trailing spaces
 * 
 * @param rawStr The raw String
 * @returns {String} The processes String. 
 */
function trim(rawStr) {
        return rawStr.replace(/^\s+|\s+$/g,"");
}

/**
 * Function to format given date into 'mm/dd/yyyy' format.
 * 
 * @param rawDate The raw date.
 * @returns The formatted date.
 */
function formatDate(rawDate) {
	var patternTS = /(\d{1,4}-\d{1,2}-\d{1,2})+\s(\d{1,2}:\d{1,2}:\d{1,2}.\d{1})/mg; /** yyyy-dd-mm hh:mm:ss.0 */
	var patternDate = /\d{1,4}-\d{1,2}-\d{1,2}/mg;
	
	if(patternTS.test(rawDate)) {
		var arrTS = rawDate.split(" ");
	
		if(null != arrTS && "undefined" != arrTS && arrTS.length > 0) {
			arrTS[0] = trim(arrTS[0]);
			
			if(patternDate.test(arrTS[0])) {
				var arrDate = arrTS[0].split("-");

				rawDate =  arrDate[1] + "/" + arrDate[2] + "/" + arrDate[0];
			}
		}
	}
	
	return rawDate;
}

/**
 * Function to format date into standard format.
 * 
 * @param dateWrapperIds The array of date wrapper element identifiers to format.
 * @returns {Boolean} The status of operation.
 */
function formatDates(dateWrapperIds) {
	var vDate, eleNumber;
	
	if(null != dateWrapperIds && "undefined" != dateWrapperIds) {
		if(typeof(dateWrapperIds) === "object" && null != dateWrapperIds.length && "undefined" != dateWrapperIds.length && dateWrapperIds.length > 0) {
			for(var i = 0; i < dateWrapperIds.length; i++) {	
				eleNumber = document.getElementById(dateWrapperIds[i]);
				
				if(null != eleNumber && "undefined" != eleNumber) {
					vDate = eleNumber.innerHTML;
					
					if(null != vDate && "undefined" != vDate && "" != vDate) {
						eleNumber.innerHTML = formatDate(trim(vDate));
					}
				}
			}
		}
	}
	
	return true;
}