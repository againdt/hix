<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
       		<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
       		<li><spring:message  code="pgheader.financialInformation"/></li>
        </ul><!--page-breadcrumb ends-->
    </div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1>${issuerObj.name}</h1>
			</div>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="financialInfo"/>
				</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span8"><spring:message  code="pgheader.financialInformation"/></h4>
                    <c:set var="encPaymentId" ><encryptor:enc value="${paymentMethodsObj.id}" isurl="true"/> </c:set>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/financial/info/edit/${encIssuerId}?paymentId=${encPaymentId}"/>"><spring:message  code="label.edit"/></a>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a> 
                </div>
				<div class="gutter10">

				<form class="form-horizontal" id="frmIssuerAcctFinInfo" name="frmIssuerAcctFinInfo" action="#" method="POST">
                    <div class="control-group">
						<label class="control-label" for="account-name"><spring:message  code="label.paymentMethodName"/></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.paymentMethodName}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-name"><spring:message  code="label.bankName"/></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.financialInfo.bankInfo.bankName}</strong></div>
					</div><!-- end of control-group-->      			              
                    
                    
                    <div class="control-group">
						<label class="control-label" for="bank-acc-num"><spring:message  code="label.bankAcctNumber"/><a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover" href="#"><i class="icon-question-sign"></i></a></label>
						<div class="controls paddingT5 breakword"><strong>${paymentMethodsObj.financialInfo.bankInfo.accountNumber}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-routing-num"><spring:message  code="label.abaRoutingNumber"/><a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover" href="#"><i class="icon-question-sign"></i></a></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.financialInfo.bankInfo.routingNumber}</strong></div>
					</div><!-- end of control-group-->
					
					<div class="control-group">
						<label class="control-label" for="bank-name"><spring:message  code="label.nameOnAccount"/></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}</strong></div>
					</div><!-- end of control-group-->           
					
					 <div class="control-group">
						<label class="control-label" for="bank-routing-num"><spring:message  code="label.bankAcctType"/></label>
						<div class="controls paddingT5"><strong>
							<c:if test="${'C' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> <spring:message code="label.bankAcctTypeChecking"/> </c:if>
							<c:if test="${'S' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> <spring:message code="label.bankAcctTypeSaving"/> </c:if>						
						</strong></div>
					</div><!-- end of control-group-->
					
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>

<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>
<script type="text/javascript">
$('.code').popover({ html: true,
	  trigger: 'hover',
	  placement: 'top',
	  content: function(){return '<img src="'+$(this).data('img') + '" />';}
});
</script>
