package com.getinsured.hix.batch.bulkusers.common;

import java.io.IOException;
import java.io.Writer;
import java.util.List;


import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.file.FlatFileHeaderCallback;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import com.getinsured.identity.provision.CsvUserStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CsvUserStatusWriteIntoFile implements StepExecutionListener {

	private static final Logger logger = LoggerFactory.getLogger(CsvUserStatusWriteIntoFile.class);
	protected String csvJobsOutputFileDirectory;
	protected String outputFileName;
	protected String inputFileName;
	protected StepExecution stepExecution;

	public synchronized boolean writeIntoCsvFile(List<CsvUserStatus> lstCsvUsersStatus, String csvUserStatusHeader) throws Exception {
		String jobProcessId = CommonUtil.getJobProcessId(stepExecution.getJobExecution());
		if (lstCsvUsersStatus != null) {
			Resource outputResource = new FileSystemResource(
					csvJobsOutputFileDirectory + "/" + this.outputFileName + ".csv");

			// set fileName into CsvUserStatus
			lstCsvUsersStatus.stream().forEach(temp -> temp.setFileName(this.inputFileName));

			FlatFileItemWriter<CsvUserStatus> writer = new FlatFileItemWriter<>();
			writer.setSaveState(true);
			CsvUserExceptionHeader headerCallback = new CsvUserExceptionHeader();
			headerCallback.setHeader(csvUserStatusHeader);
			writer.setHeaderCallback(headerCallback);
			writer.setResource(outputResource);
			
			logger.info("Writing Data Into Output CSV file. jobProcessId : "+jobProcessId+"[Total No. Of Users Status : " + lstCsvUsersStatus.size()
					+ ". Output File Name : " + outputResource.getFile().getAbsolutePath() + "]");

			writer.setAppendAllowed(true);

			writer.setLineAggregator(new DelimitedLineAggregator<CsvUserStatus>() {
				{
					setDelimiter(",");
					setFieldExtractor(new BeanWrapperFieldExtractor<CsvUserStatus>() {
						{
							setNames(
									new String[] { "remoteId", "status", "exceptionType", "errorMessage", "fileName" });
						}
					});
				}
			});

			writer.open(this.stepExecution.getExecutionContext());
			writer.write(lstCsvUsersStatus);
			writer.close();
		} else {
			logger.info("Users Status Data Not Found. jobProcessId : "+jobProcessId);
		}
		return true;
	}

	class CsvUserExceptionHeader implements FlatFileHeaderCallback {
		private String header;

		public void setHeader(String str) {
			this.header = str;
		}

		@Override
		public void writeHeader(Writer writer) throws IOException {
			writer.write(this.header);
		}

	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {

		return null;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}

	public String getCsvJobsOutputFileDirectory() {
		return csvJobsOutputFileDirectory;
	}

	public void setCsvJobsOutputFileDirectory(String csvJobsOutputFileDirectory) {
		this.csvJobsOutputFileDirectory = csvJobsOutputFileDirectory;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

	public String getInputFileName() {
		return inputFileName;
	}

	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}
}
