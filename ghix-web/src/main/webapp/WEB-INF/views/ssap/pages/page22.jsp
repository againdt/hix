<%@ include file="../include.jsp" %>
<div id="appscr72" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages"><strong class="nameOfHouseHold"></strong>&#39;s <spring:message code="ssap.page22.currentIncomeDetails" text="Current Income Details" /></h4>
		</div>
		<form id="appscr72Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="alert alert-info">
				<div class="row-fluid">
					<div class="span4">
						<h3><spring:message code="ssap.page22.totalIncome" text="Total Income" /></h3>
					</div>
					<div class="txt-right">
						<strong id="totalIncomeAt72LabelHead"></strong>
					</div>
				</div>
			
				<div class="row-fluid">
					<div class="span9">
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page22.employerTypeState" text="(employer, type, state government, other - if applicable)" />
					</div>
					<div class="txt-right">
						<strong id="totalIncomeAt72Label"></strong>
					</div>
				</div>
			</div>
		
			<h4 id="incomeDeductions_headlabel">
				<spring:message code="ssap.page22.incomeDeductions" text="Income Deductions" />
			</h4>
			<div class="gutter10-lr">
			<div class="control-group">
				<p id="incomeDeductions_textlabel">
					<spring:message code="ssap.page22.if" text="If" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page22.paysForCertainThings" text="pays for certain things that can be deducted on an income tax return, telling us about them could make the cost of health insurance a little lower. What does" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page22.paysFor" text="pay for?" /> 
					<small> <spring:message code="ssap.page22.checkAllThatApply" text="(check all that apply.)" /></small><img src="<c:url value="/resources/images/requiredAsterix.png" />"  width="10" height="10" alt="Required!" aria-hidden="true" />
				</p>
				<div id="incomeDeductions_div">
				<div class="gutter10-l">
					<label class="capitalize-none">
						<input type="checkbox" name="incomeDeduction" onclick="appscr72CheckBoxAction($(this))"  id="appscr72alimonyPaid" parsley-mincheck="1" />
						<span><spring:message code="ssap.page22.alimonyPaid" text="Alimony paid" /></span>
					</label>
					
					<div id="appscr72alimonyPaidTR" class="hide gutter10 margin10-t">
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page22.amount" text="Amount:" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
								<strong>$</strong>
							</label><!-- end of label -->
							<div class="controls" data-rel="multipleInputs">
								<input type="text" placeholder="<spring:message code="ssap.page22.dollarAmount" text="Dollar Amount" />" onclick="appscr72CheckBoxAction($(this))" id="alimonyAmountInput" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>"  data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired' />"/>
								<!-- <ul id="parsley-alimony-deduction" class="parsley-error-list">
									<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
									</li>
								</ul> -->
								<span class="errorMessage"></span>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page22.howOften" text="How often?" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
							</label><!-- end of label -->
							<div class="controls"data-rel="multipleInputs">
								<select id="alimonyFrequencySelect" class="input-medium" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field'/>"
								aria-label="Frequency options">
									<option selected value=""><spring:message code="ssap.selectFrquency" text="Select Frequency" /></option>
									<option value="Weekly"><spring:message code="ssap.weekly" text="Weekly" /> </option>
									<option value="EveryTwoWeeks"><spring:message code="ssap.every2Week" text="Every 2 week" /> </option>
									<option value="Monthly"><spring:message code="ssap.monthly" text="Monthly" />  </option>
									<option value="Yearly"><spring:message code="ssap.yearly" text="Yearly" />  </option>
									<option value="OneTimeOnly"><spring:message code="ssap.oneTimeOnly" text="One time only" /> </option>
									<option value="TwiceAMonth"><spring:message code="ssap.twiceAMonth" text="Twice a month" /> </option>
								</select>
								<span class="errorMessage"></span>
								<!-- <ul id="parsley-alimony-deduction-frequency" class="parsley-error-list" style="display:none;">
									<li class="required" style="display: list-item;">
										<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
									</li>
								</ul> -->
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
					</div><!-- appscr72alimonyPaidTR ends -->
					
					
					<label class="capitalize-none">
						<input type="checkbox" name="incomeDeduction" id="appscr72studentLoanInterest" onclick="appscr72CheckBoxAction($(this))" />
						<span><spring:message code='ssap.page22.studentLoanInterest' text='Student loan interest paid' /></span>
					</label>
					
					<div id="appscr72studentLoanInterestTR" class="hide gutter10">
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page22.amount" text="Amount:" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
								<strong>$</strong>
							</label><!-- end of label -->
							<div class="controls" data-rel="multipleInputs">
								<input type="text" placeholder="<spring:message code="ssap.page22.dollarAmount" text='Dollar Amount' />" id="studentLoanInterestAmountInput" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>"/>
								<!-- <ul id="parsley-alimony-deduction" class="parsley-error-list">
									<li class="required" style="display: list-item;">
									<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
									</li>
								</ul> -->
								<span class="errorMessage"></span>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page22.howOften" text="How often?" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
							</label><!-- end of label -->
							<div class="controls" data-rel="multipleInputs">
								<select id="studentLoanInterestFrequencySelect" class="input-medium" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field'/>"
								aria-label="Frequency options">
									<option selected value=""><spring:message code="ssap.selectFrquency" text="Select Frequency" /></option>
									<option value="Weekly"><spring:message code="ssap.weekly" text="Weekly" /> </option>
									<option value="EveryTwoWeeks"><spring:message code="ssap.every2Week" text="Every 2 week" /> </option>
									<option value="Monthly"><spring:message code="ssap.monthly" text="Monthly" />  </option>
									<option value="Yearly"><spring:message code="ssap.yearly" text="Yearly" />  </option>
									<option value="OneTimeOnly"><spring:message code="ssap.oneTimeOnly" text="One time only" /> </option>
									<option value="TwiceAMonth"><spring:message code="ssap.twiceAMonth" text="Twice a month" /> </option>
								</select>
								<!-- <ul id="parsley-studentLoan-deduction-frequency" class="parsley-error-list" style="display:none;">
									<li class="required" style="display: list-item;">
										<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
									</li>
								</ul> -->
								<span class="errorMessage"></span>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
					</div><!-- appscr72studentLoanInterestTR ends -->
					
					<label class="capitalize-none">
						<input type="checkbox" name="incomeDeduction" id="appscr72otherDeduction" onclick="appscr72CheckBoxAction($(this))"  />
						<spring:message code="ssap.page22.otherDeduction" text="Other deduction" /> <strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page22.couldTakeHisHer" text="could take on [his/her]" />  <span class='coverageYear'></span> 
						<spring:message code="ssap.page22.taxReturn" text="tax return" />
					</label>
					
					<div id="appscr72otherDeductionTR" class="hide gutter10">
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page22.whatOtherDeductionsDoHave" text="What other deductions do you have?" />
							</label><!-- end of label -->
							<div id ="incomeDeductionsDivErrorDiv"></div>
							<div class="controls" data-rel="multipleInputs">
								<input id="deductionTypeInput" class="input-medium" type="text" name="" data-parsley-pattern="^[A-Za-z ]{5,256}$" data-parsley-pattern-message="<spring:message code='ssap.error.otherDeductionsValid'/>" placeholder="<spring:message code='ssap.page22.otherDeductionPlace' text='Other Deductions' />"  data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.otherDeductionsRequired'/>" />
								<span class="errorMessage"></span>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page22.amount" text="Amount:" />
								<strong>$</strong>
							</label><!-- end of label -->
							<div class="controls" data-rel="multipleInputs">
								<input type="text" placeholder="<spring:message code='ssap.page22.dollarAmount' text='Dollar Amount' />"  id="deductionAmountInput" class="input-medium currency" data-parsley-pattern="^[0-9\,\.]{1,18}$" data-parsley-pattern-message="<spring:message code='ssap.error.amountValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.amountRequired'/>" />
								<!-- <ul id="parsley-other-deduction" class="parsley-error-list" style="display:none;">
									<li class="required" style="display: list-item;">
										<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
									</li>
								</ul> -->
								<span class="errorMessage"></span>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.page22.howOften" text="How often?" /><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
							</label><!-- end of label -->
							<div class="controls" data-rel="multipleInputs">
								<select id="deductionFrequencySelect" class="input-medium" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field'/>"
								aria-label="Frequency options">
									<option selected value=""><spring:message code="ssap.selectFrquency" text="Select Frequency" /></option>
									<option value="Weekly"><spring:message code="ssap.weekly" text="Weekly" /> </option>
									<option value="EveryTwoWeeks"><spring:message code="ssap.every2Week" text="Every 2 week" /> </option>
									<option value="Monthly"><spring:message code="ssap.monthly" text="Monthly" />  </option>
									<option value="Yearly"><spring:message code="ssap.yearly" text="Yearly" />  </option>
									<option value="OneTimeOnly"><spring:message code="ssap.oneTimeOnly" text="One time only" /> </option>
									<option value="TwiceAMonth"><spring:message code="ssap.twiceAMonth" text="Twice a month" /> </option>
								</select>
								<!-- <ul id="parsley-studentLoan-deduction-frequency" class="parsley-error-list" style="display:none;">
									<li class="required" style="display: list-item;">
										<span class="validation-error-text-container"><em class="validation-error-icon">!</em>This value is required.</span>
									</li>
								</ul> -->
								<span class="errorMessage"></span>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						<p>
							<spring:message code="ssap.page22.youShouldntInclude" text="You shouldn't include a cost that you already considered in your answer to net self-employment or rental income." />
						</p>
						
					</div><!-- appscr72otherDeductionTR ends -->
					
					<label class="checkbox hide">
						<input type="checkbox" onclick="appscr72DisableDeductions($(this))" id="appscr72incomeNone" />
						<spring:message code="ssap.page22.none" text="None" />
					</label>
					<div class="errorMessage"></div>
				</div>
				</div>
			</div><!-- control-group ends -->
			</div>
			
			<h4 class="hide">
				<spring:message code="ssap.page22.additionalIncomeQuestions" text="Additional Income Questions" />
			</h4>
			<div class="gutter10-lr">
			<p class="hide">
				<spring:message code="ssap.page22.ofTheIncomeInThe" text="Of the income in the summary table, is any money from any of these sources?" />
			</p>
			<div class="control-group hide">
				<div class="margin5-l">
					<label>
						<input type="checkbox" />
						<span>
							<spring:message code="ssap.page22.perCapitaPayments" text="Per capita payments from the tribe that come from natural resources, usage rights, leases or royalties " />
						</span>
					</label>
					
					<label>
						<input type="checkbox"/>
						<span>
							<spring:message code="ssap.page22.paymentsFromNaturalResources" text="Payments from natural resources, farming, ranching, fishing, leases, or royalties from land designated as Indian trust land by the &nbsp;Department of Interior (including reservations and former reservations) " />
						</span>
					</label>
					
					<label>
						<input type="checkbox" id="anotherIncomeTypeSource" onclick="money4OtherIncometypeCheckBox()" checked="checked" />
						<span>
							<spring:message code="ssap.page22.moneyFromSelling" text="Money from selling things that have cultural significance" />
						</span>
					</label>
				</div>
			</div><!-- control-group ends -->
			
			<div class="incomeTypeDetailcontainer margin10-l hide">
				<div class="control-group">
					<p>
						<spring:message code="ssap.page22.selectIncomeType" text="Select income type:" />&nbsp;
						<small><spring:message code="ssap.page22.checkAllThatApply" text="(check all that apply.)" /></small><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
					</p>
					<div class="margin20-l">
						<label>
							<input type="checkbox" id="Financial_None1" />
							<span><spring:message code="ssap.page22.none" text="None" /></span>
						</label>
						
						<label>
							<input type="checkbox"  id="Financial_Job1" checked="checked" /> 
							<span><spring:message code="ssap.page22.job" text="Job" /></span>
						</label>
						
						<%-- <label>
							<input type="checkbox"  id="Financial_Self_Employment1" checked="checked" />
							<span><spring:message code="ssap.page22.selfEmplyment" text="Self-employment" /></span>
						</label> --%>
						
						<label>
							<input type="checkbox"  id="Financial_Self_Employment1" checked="checked" />
							<span><spring:message code="ssap.page22.selfEmplyment" text="Self-employment" /></span>
						</label>
						
						<label>
							<input type="checkbox"  id="Financial_SocialSecuritybenefits1" checked="checked" />
							<span><spring:message code="ssap.page22.socialSecurity" text="Social Security benefits" /></span>
						</label>
						
						<label>
							<input type="checkbox" id="Financial_Unemployment1" checked="checked" />
							<span><spring:message code="ssap.page22.unemployment" text="Unemployment" /></span>
						</label>
						
						<label>
							<input type="checkbox"  id="Financial_Retirement_pension1" checked="checked" />
							<span> <spring:message code="ssap.page22.retirementOrPension" text="Retirement/pension" /></span>
						</label>
						
						<label>
							<input type="checkbox"  id="Financial_Capitalgains1" checked="checked" />
							<span><spring:message code="ssap.page22.capitalGains" text="Capital gains" /></span>
						</label>
						
						<label>
							<input type="checkbox"  id="Financial_InvestmentIncome1"  checked="checked" />
							<span><spring:message code="ssap.page22.InvestmentIncome" text="Investment income" /></span>
						</label>
						
						<label>
							<input type="checkbox" id="Financial_RentalOrRoyaltyIncome1" checked="checked" />
							<span><spring:message code="ssap.page22.rentalOrRoyalty" text="Rental or royalty income" /></span>
						</label>
						
						<label>
							<input type="checkbox" id="Financial_FarmingOrFishingIncome1" checked="checked" />
							<span><spring:message code="ssap.page22.farmingOrFishingIncome" text="Farming or fishing income" /></span>
						</label>
						
						<label>
							<input type="checkbox"  id="Financial_AlimonyReceived1" checked="checked" />
							<span><spring:message code="ssap.page22.alimonyReceived" text="Alimony received" /></span>
						</label>
						
						<label>
							<input type="checkbox" id="Financial_OtherIncome1" checked="checked" />
							<span> <spring:message code="ssap.page22.otherIncome" text="Other income" /></span>
						</label>
					</div>
				</div><!-- control-group ends -->
			</div><!-- incomeTypeDetailcontainer ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page22.basedOnWhatYou" text="Based on what you told us, if" />
					<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page22.incomeIsSteady" text="'s income is steady month-to-month, then it's about" />&nbsp;<strong class="amountAt72PerYear"></strong>
					<spring:message code="ssap.page22.perYearIsThis" text="per year. Is this how much you think" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page22.willGetIn" text="will get in" />
					<span class='coverageYear'></span>?
				</p>
				<div class="margin5-l controls">
					<label for="aboutIncomeSppedYes" class="radio">
						<input type="radio"  name="aboutIncomeSpped" id="aboutIncomeSppedYes" value="yes" onclick="appscr72incomeHighOrLow()" data-parsley-errors-container="#aboutIncomeSppedDivErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/>
						<spring:message code="ssap.yes" text="Yes" />
					</label>
					<label class="radio" for="aboutIncomeSppedNo">
						<input type="radio"  name="aboutIncomeSpped" id="aboutIncomeSppedNo" value="no" onclick="appscr72incomeHighOrLow()"/>
						<spring:message code="ssap.no" text="No" />
					</label>
					<div id="aboutIncomeSppedDivErrorDiv"></div>
					<span class="errorMessage"></span>
				</div>
			</div><!-- control-group ends -->
			
			<div class="control-group hide">
				<p>
					<spring:message code="ssap.page22.accordingToFederal" text="According to federal income tax return data," />
					<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page22.incomeWas" text="'s income was" />
					<span class="irsIncome2013"></span> in 2013.
					<spring:message code="ssap.page22.basedOnWhatYouKnow" text="Based on what you know today, how much do you think" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page22.willMakeIn" text="will make in" />
					<span class='coverageYear'></span>?
				</p>
				<div class="gutter10">
				<label class="control-label checkbox" for="expectedIncomeByHouseHoldMember">
					<spring:message code="ssap.page22.amount" text="Amount:" />
					<strong>$</strong>
				</label><!-- end of label -->
				<div class="controls">
					<input id="expectedIncomeByHouseHoldMember" name="" type="text" placeholder="<spring:message code='ssap.page22.dollarAmount' text='Dollar Amount' />" value="" class="input-medium" />
					<label>
						<input type="checkbox" onclick="appscr72BasedOnAmountCB()" /><span><spring:message code="ssap.iDontKnow" text="I don't know" /></span>
					</label>
				</div><!-- end of controls-->
				</div>
			</div><!-- control-group ends -->
			
			<div class="control-group hide" id="incomeHighOrLowDiv">
				<p>
					<spring:message code="ssap.page22.doYouExpectYour" text="Do you expect your income in" />
					<strong class="coverageYear"></strong>
					<spring:message code="ssap.page22.toBeHigherOr" text="to be higher or lower than" />&nbsp;<strong class="amountAt72PerYear"></strong>
					<spring:message code="ssap.page22.perYear" text="per year" /><span class="irsIncome2013 hide"></span>,&nbsp;<spring:message code="ssap.page22.whichIsBasedOn" text="which is based on your monthly income" />?
				</p>
				<div class="margin5-l controls">
					<label>
						<input type="radio"  name="incomeHighOrLow"  value="higher" data-parsley-errors-container="#incomeHighOrLowErrorDiv" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' text='This value is required.' />"/>
						<spring:message code="ssap.page22.higher" text="Higher" />
					</label>
					<label class="radio" for="incomeHighOrLow">
						<input type="radio"  name="incomeHighOrLow" id="incomeHighOrLow" value="lower"/>
						<spring:message code="ssap.page22.lower" text="Lower" />
					</label>
					<div id= "incomeHighOrLowErrorDiv"></div>
					<span class="errorMessage"></span>
				</div>
			</div><!-- control-group ends -->
			</div>
		</form>
	</div>
          <div class="Main_ui_design hide">
                   <form>
					
                   <div class="input_tetx_area" style='display:none;'>
                     <br>
                    <p class="cobaappDarkWithoutMarginTop"><spring:message code="ssap.page22.basedOnWhatYouKnow" text="Based on what you know today, how much do you think" /> <span class="nameOfHouseHold"></span><spring:message code="ssap.page22.willMakeIn" text="will make in" /> <span class='coverageYear'></span>?</p>
                   <br> 
                   </div>
                   <div class="input_tetx_area" style='display:none;'>
                   <label class="cobaappDarkWithoutMarginTop marginLeft28" id="basedOnWhatYouKnow"><spring:message code="ssap.page22.amount" text="Amount:" /> </label>
                    </div>
                   
                    <div class="input_tetx_area" style='display:none;'>
                    <label class="cobaappDark marginLeft28" for="expectedIncomeByHouseHoldMember_old">$</label>
                    <div class="input_row_area1"></div>
                    <input type="text" placeholder="<spring:message code='ssap.page22.dollarAmount' text='Dollar Amount' />" id="expectedIncomeByHouseHoldMember_old" name="expectedIncomeByHouseHoldMember_old" onKeyUp="appscr72basedOnAmountInput()" class="input_row_area_left_amount">
                    </div>
                    
                     <div class="input_tetx_area New_dot_alin" style='display:none;'>
					 <input id="basedOnAmountCB" type="checkbox" onclick="appscr72BasedOnAmountCB()"  /> 
					 <label class="cobaappLabel" for="basedOnAmountCB"><spring:message code="ssap.iDontKnow" text="I don't know" /></label>                    
                     </div>
                   
                   <div class="input_tetx_area">
                     <br>
                    <p class="cobaappDarkWithoutMarginTop"></p>
                    <br>
                    <br>
                       
                   </div>
                    <div class="input_tetx_area">
                    <br><br>
                    </div>
                   </form>
         </div>
</div>