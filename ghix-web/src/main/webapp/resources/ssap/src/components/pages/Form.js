import React, { Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// ActionCreators

import {
    addContext,
    changePage,
    changeData,
    clearFormSubmission,
    deleteForm,
    removeContext,
    saveData,
    submitForm,
    validateForm
} from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';

// Constants

import CONFIG from '../../.env';
import ENDPOINTS from '../../constants/endPointsList';



const Form  = props => {

    // Props from Parent Component (usually it is Page)

    const {
        topLevelValidity,
    } = props;

    // Props from Redux Store

    const {
        console: {
            isFormValidationShown,
            isInvalidInputsShown
        },
        data,
        data: {
            singleStreamlinedApplication,
            singleStreamlinedApplication: {
                ssapApplicationId,
                maxAchievedPage
            }
        },
        form: {
            Form: {
                isEligiblyForSave,
                isFormSubmitted,
                registeredInputs
            }
        },
        environment: {
            impersonation
        },
        page
    } = props;

    const [ localState, setState ] = useState({
        isGoingToExitWithoutSaving: false,
        frozenData: cloneObject(data),
    });

    useEffect(() => {
        props.actions.addContext(Form, 'Form');
        return () => {
            props.actions.removeContext('Form');
            props.actions.deleteForm('Form');
        };
    }, []);

    // Scrolling to the first error message on Form submit

    useEffect(() => {
        if(isFormSubmitted && !isEligiblyForSave) {
            const element = [...document.getElementsByClassName('error--msg')]
                .filter(message => message.className.match(/shown error--msg/))[0];
            element.scrollIntoView({ behavior: 'smooth' });
        }
    }, [isFormSubmitted]);

    // Up to date primitive of registered inputs values to add as dependency for watching

    const registeredInputsToString = registeredInputs.map(input => input.valueOf()).toString();

    // Watching for changes in inputs and auto-validation Form

    useEffect(() => {
        props.actions.validateForm(checkIfEligible(), 'Form');
    }, [registeredInputsToString, topLevelValidity]);

    useEffect(() => {
        isFormSubmitted && isEligiblyForSave && props.actions.clearFormSubmission('Form');
    }, [isEligiblyForSave]);

    const getBody = destinationPage => {

        let pageForSave;

        switch(destinationPage) {
        case 'increase':
            pageForSave = page + 1; break;
        case 'decrease':
            pageForSave = page - 1; break;
        default:
            pageForSave = destinationPage; break;
        }

        return {
            'application': JSON.stringify({
                ...singleStreamlinedApplication,
                currentPage: pageForSave,
                maxAchievedPage: maxAchievedPage < pageForSave ? pageForSave : maxAchievedPage
            }),
            'applicationId': ssapApplicationId,
            'csrftoken': window.csrf_token,
            'csrOverride': impersonation
        };
    };

    Form.onFormSubmit = (destinationPage, callbacks, exceptionAction) => {
        props.actions.submitForm('Form');
        if (isEligiblyForSave) {
            if(!exceptionAction) {
                if(CONFIG.ENV.DEVELOPMENT) {
                    alert('Form is eligible for saving...');
                    props.actions.changePage(destinationPage);
                    callbacks && callbacks.forEach(callback => {
                        callback();
                    });
                }
                if(CONFIG.ENV.PRODUCTION) {
                    console.log('Saving...');
                    props.actions.saveData(
                        callbacks || [],
                        `${window.location.origin}${ENDPOINTS.SAVE_DATA_ENDPOINT}${window.csrf_token}`,
                        getBody(destinationPage)
                    );
                }
            } else {
                exceptionAction();
            }
        } else {
            // props.actions.clearFormSubmission();
        }
    };

    Form.onFormExit = callbacks => {
        props.actions.changeData(localState.frozenData);
        setState(state => ({
            ...state,
            isGoingToExitWithoutSaving: true,
        }));
        callbacks && callbacks.forEach(callback => {
            callback();
        });
    };

    const checkIfEligible = () => {
        if(registeredInputs.length === 0) {
            return {
                sEligiblyForSave: true,
                isFormOnTopLevelValid: true,
                areAllInputsValid: true
            };
        }
        const isApprovedByTopLevel = topLevelValidity === undefined ? true : topLevelValidity;
        let result = true;
        registeredInputs.forEach(input => {
            if (input.inputIsValid === false || (input.isInputRequired && (input.inputValue === '' || input.inputValue === null))) {
                isInvalidInputsShown && console.log(input);
                result = false;
            }
        });
        return {
            isEligiblyForSave: isApprovedByTopLevel && result,
            isFormOnTopLevelValid: isApprovedByTopLevel,
            areAllInputsValid: result,
        };
    };

    // Consoles start

    if (registeredInputs.length > 0) {
        isFormValidationShown && console.log('Form Top level validity: ', topLevelValidity);
        isFormValidationShown && console.log('Form Registered inputs: ',
            registeredInputs.map(input => ({
                inputIsValid: input.inputIsValid,
                isInputRequired: input.isInputRequired,
                inputName: input.inputName,
                onFormExit: input.onFormExit
            })));
    }

    isFormValidationShown && console.log('Form eligible: ', registeredInputs.length === 0 ? true : isEligiblyForSave);

    // Consoles end

    const updateReactChildren = children => {
        return React.Children.map(children, child => {
            if(!React.isValidElement(child)) {
                return child;
            }
            let childProps = {};
            if(
                React.isValidElement(child)
                && (typeof child.props.inputData === 'object' ||
                    typeof child.props.currentData === 'object')
            ){
                childProps = {
                    parentForm: {
                        name: 'Form'
                    }
                };
            }
            childProps.children = updateReactChildren(child.props.children);
            return React.cloneElement(child, childProps);
        });
    };

    return (
        <Fragment>
            { updateReactChildren(props.children) }
        </Fragment>
    );
};

const mapStateToProps = state => ({
    console: state.console,
    data: state.data,
    form: state.form,
    environment: state.headerLinks,
    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        addContext,
        changeData,
        changePage,
        clearFormSubmission,
        deleteForm,
        removeContext,
        saveData,
        submitForm,
        validateForm
    }, dispatch)
});

Form.defaultProps = {
    name: 'Form'
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
