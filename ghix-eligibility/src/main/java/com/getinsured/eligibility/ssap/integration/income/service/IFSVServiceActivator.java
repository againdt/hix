package com.getinsured.eligibility.ssap.integration.income.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;

@Component(value = "ifsvRequestServiceActivator")
public class IFSVServiceActivator {
	
private static final Logger LOGGER = LoggerFactory.getLogger(IFSVServiceActivator.class);
	
	@ServiceActivator
	@SuppressWarnings("unchecked")
	public String receive(Message<String> message) throws IOException, ParseException {
		LOGGER.debug(">>>>>>>>>>>>>>>>>> Income flow received: " + message.getPayload());
		String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.SSAP_JSON_HEADER).toString();
		JSONParser parser = new JSONParser();
		String applicationId = message.getHeaders().get(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER).toString();
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("applicationId", Long.parseLong(applicationId));
		jsonObj.put(SSAPIntegrationConstants.CLIENT_IP, message.getHeaders().get(SSAPIntegrationConstants.CLIENT_IP_HEADER).toString());
		JSONObject ssapJSONObject = (JSONObject)parser.parse(ssapJSON);
		ssapJSONObject = (JSONObject) ssapJSONObject.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION);
		JSONArray taxHouseholds = (JSONArray)ssapJSONObject.get(SSAPIntegrationConstants.TAX_HOUSEHOLD);
		JSONArray updatedTaxHouseholds = new JSONArray();
		Map<Integer, String> memberTaxFilerCategoryMap = getTaxFilerCodesForMembers(ssapJSONObject);
		
		for(Object taxHousehold : taxHouseholds) {
			JSONObject household = (JSONObject) taxHousehold;
			JSONArray householdMembers = (JSONArray) household.get(SSAPIntegrationConstants.HOUSEHOLD_MEMBER);
			JSONArray updatedHouseholdMembers = new JSONArray();
			for(Object householdMember : householdMembers) {
				JSONObject member = (JSONObject) householdMember;
				
				// Remove the applicants who did not provide SSN
                JSONObject socialSecurityCard = (JSONObject)member.get(SSAPIntegrationConstants.SOCIAL_SECURITY_CARD);
                if (null != socialSecurityCard
                        && null != socialSecurityCard.get(SSAPIntegrationConstants.SOCIAL_SECURITY_NUMBER)
                        && StringUtils.isNotBlank(socialSecurityCard.get(SSAPIntegrationConstants.SOCIAL_SECURITY_NUMBER).toString())) {
                    
                    String taxFilerCategoryCode = memberTaxFilerCategoryMap.get(Integer.valueOf(member.get("personId").toString()));
                    if(null != taxFilerCategoryCode) {
                        member.put(SSAPIntegrationConstants.TAX_FILER_CATEGORY_CODE, taxFilerCategoryCode);
                    }
                    
                    // TODO: Do this at one single place. Ideally SSAP should just pass true or false
                    if(null != socialSecurityCard.get(SSAPIntegrationConstants.NAME_SAME_ON_SSN_CARD_INDICATOR)) {
                        String nameSameOnSSNCardIndicator = socialSecurityCard.get(SSAPIntegrationConstants.NAME_SAME_ON_SSN_CARD_INDICATOR).toString();
                        if(nameSameOnSSNCardIndicator.equalsIgnoreCase("yes")) {
                            socialSecurityCard.put(SSAPIntegrationConstants.NAME_SAME_ON_SSN_CARD_INDICATOR, true);
                        }
                        if(nameSameOnSSNCardIndicator.equalsIgnoreCase("no")) {
                            socialSecurityCard.put(SSAPIntegrationConstants.NAME_SAME_ON_SSN_CARD_INDICATOR, false);
                        }
                        member.put(SSAPIntegrationConstants.SOCIAL_SECURITY_CARD, socialSecurityCard);
                    }
                    
                    JSONObject nameObject = (JSONObject)member.get("name");
                    if(null != nameObject && null != nameObject.get("middleName") && StringUtils.isBlank(nameObject.get("middleName").toString())) {
                        nameObject.remove("middleName");
                    }
                    
                    updatedHouseholdMembers.add(member);
                }
			}
			if(updatedHouseholdMembers.size() > 0) {
				household.put(SSAPIntegrationConstants.HOUSEHOLD_MEMBER, updatedHouseholdMembers);
			} else {
				LOGGER.error("No household members to verify Income");
				throw new IllegalStateException("No household members to verify Income");
			}
			updatedTaxHouseholds.add(taxHouseholds);
		}
		ssapJSONObject.put(SSAPIntegrationConstants.TAX_HOUSEHOLD, taxHouseholds);
		// TODO: Remove the hard-coding of requestID
		ssapJSONObject.put("requestID", "113");
		jsonObj.put("payload", ssapJSONObject);
		LOGGER.debug(">>>>>>>>>>>>>>>>>> IFSV Request: " + jsonObj.toJSONString());
		return jsonObj.toJSONString();
	}
	
	private Map<Integer, String> getTaxFilerCodesForMembers(JSONObject application) {
	    Map<Integer, String> memberTaxFilerCategoryMap = new HashMap<>();
        JSONArray houseHolds = (JSONArray)((JSONObject)((JSONArray)application.get("taxHousehold")).get(0)).get("householdMember");
        int primaryTaxFilerId = (Integer)((JSONObject)((JSONArray)application.get("taxHousehold")).get(0)).get("primaryTaxFilerPersonId");
        memberTaxFilerCategoryMap.put(primaryTaxFilerId, "PRIMARY");
        if(primaryTaxFilerId > 0) {
            Object planToFileFTRJontlyIndicator = ((JSONObject)houseHolds.get(primaryTaxFilerId-1)).get("planToFileFTRJontlyIndicator");
            if(null != planToFileFTRJontlyIndicator) {
                if(Boolean.valueOf(planToFileFTRJontlyIndicator.toString())) {
                    int spousePersonId = Integer.valueOf(((JSONObject)((JSONObject)houseHolds.get(primaryTaxFilerId-1)).get("taxFiler")).get("spouseHouseholdMemberId").toString());
                    memberTaxFilerCategoryMap.put(spousePersonId, "SPOUSE");
                }
            }
            JSONArray taxDependants = (JSONArray)application.get("taxFilerDependants");
            for(Object taxDependent : taxDependants) {
                memberTaxFilerCategoryMap.put(Integer.valueOf(taxDependent.toString()), "DEPENDENT");
            }
            
        }
        return memberTaxFilerCategoryMap;
	}
}
