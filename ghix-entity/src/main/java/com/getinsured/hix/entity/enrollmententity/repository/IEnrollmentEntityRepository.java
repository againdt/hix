package com.getinsured.hix.entity.enrollmententity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.entity.EnrollmentEntity;

public interface IEnrollmentEntityRepository extends RevisionRepository<EnrollmentEntity, Integer, Integer>,
		JpaRepository<EnrollmentEntity, Integer> {

	EnrollmentEntity findById(int enrollmentEntityId);

	EnrollmentEntity findByUserId(int userId);

	@Query("FROM EnrollmentEntity e where e.user.id = :userId")
	EnrollmentEntity findEnrollmentEntityByUserId(@Param("userId") Integer userId);
	
	EnrollmentEntity findByEntityName(String entityName);

}
