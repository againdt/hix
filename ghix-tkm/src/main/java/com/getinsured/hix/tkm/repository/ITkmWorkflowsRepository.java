package com.getinsured.hix.tkm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.TkmWorkflows;

@Repository
public interface ITkmWorkflowsRepository extends JpaRepository<TkmWorkflows, Integer>
{
	@Query("SELECT DISTINCT t.type FROM TkmWorkflows t")
	List<String> getDistinctTicketType();
	
	@Query("FROM TkmWorkflows tW WHERE tW.category = :category AND tW.type = :type")
	TkmWorkflows findByCriteria(@Param("type") String type, @Param("category") String category);

	@Query("FROM TkmWorkflows tW WHERE tW.type = :type")
	TkmWorkflows findByCriteria(@Param("type") String ticketType);	

	List<TkmWorkflows> findByStatus(TkmWorkflows.TicketStatus status);	

}
