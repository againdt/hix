package com.getinsured.ssap.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.financial.nmec.HomeAddress;
import com.getinsured.ssap.model.financial.nmec.HouseholdContact;
import com.getinsured.ssap.model.financial.nmec.Insurance;
import com.getinsured.ssap.model.financial.nmec.MecResults;
import com.getinsured.ssap.model.financial.nmec.Name;
import com.getinsured.ssap.model.financial.nmec.NonEsiMecErrorMessageDetail;
import com.getinsured.ssap.model.financial.nmec.NonEsiMecHouseholdMember;
import com.getinsured.ssap.model.financial.nmec.NonEsiMecPayload;
import com.getinsured.ssap.model.financial.nmec.NonEsiMecTaxHousehold;
import com.getinsured.ssap.model.financial.nmec.NonEsiMecVerificationRequest;
import com.getinsured.ssap.model.financial.nmec.NonEsiMecVerificationResponse;
import com.getinsured.ssap.model.financial.nmec.SocialSecurityCard;
import com.getinsured.ssap.model.hub.vlp.ApplicantVerificationStatus;
import com.getinsured.ssap.model.hub.vlp.response.ResponseCode;
import com.getinsured.ssap.model.hub.vlp.response.ResponseMetadata;
import com.getinsured.ssap.repository.EligibilityProgramRepository;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;

/**
 * Implementation of {@link NonEsiMecService}.
 *
 * @author Suresh Kancherla
 * @since 8/19/19
 */
@Service("NonEsiMecService")
public class NonEsiMecServiceImpl implements NonEsiMecService {
	private static final Logger log = LoggerFactory.getLogger(NonEsiMecServiceImpl.class);
	private final SsapApplicationRepository ssapApplicationRepository;
	private final SsapApplicantRepository ssapApplicantRepository;
	private final EligibilityProgramRepository eligibilityProgramRepository;
	private final GhixRestTemplate restTemplate;
	private final Properties configProp;

	private static String hubIntegrationUrl;
	private static final ObjectMapper om = new ObjectMapper();
	private static final String MALE = "M";
	private static final String FEMALE = "F";

	@Autowired
	public NonEsiMecServiceImpl(final SsapApplicationRepository ssapApplicationRepository,
			final SsapApplicantRepository ssapApplicantRepository,
			final EligibilityProgramRepository eligibilityProgramRepository, final GhixRestTemplate restTemplate,
			final Properties configProp) {
		this.ssapApplicationRepository = ssapApplicationRepository;
		this.ssapApplicantRepository = ssapApplicantRepository;
		this.eligibilityProgramRepository = eligibilityProgramRepository;
		this.restTemplate = restTemplate;
		this.configProp = configProp;
	}

	@PostConstruct
	public void postConstruct() {
		hubIntegrationUrl = this.configProp.getProperty("ghixHubIntegrationURL");

		if (hubIntegrationUrl == null || "".equals(hubIntegrationUrl)) {
			log.error("Unable to get AWS Hub integration URL from configuration.properties, using default");
			hubIntegrationUrl = "https://7ntk685owc.execute-api.us-gov-west-1.amazonaws.com/nv/ghix/";
		}

		// Remove last slash from hub integration URL.
		if (hubIntegrationUrl.endsWith("/")) {
			log.info("Removing last slash from hub integration url: {}", hubIntegrationUrl);
			hubIntegrationUrl = hubIntegrationUrl.substring(0, hubIntegrationUrl.length() - 1);
			log.info("After removing last slash from hub integration url: {}", hubIntegrationUrl);
		}

		hubIntegrationUrl += "/invokeNMEC";
		om.enable(SerializationFeature.INDENT_OUTPUT);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
  @Transactional
	public NonEsiMecVerificationResponse invoke(final long ssapApplicationId) {
		log.info("Invoking NonEsiMecService  for application id: {}", ssapApplicationId);
		assert ssapApplicationId > 0 : "ssapApplicationId should be > 0";
		final SsapApplication ssapApplication = ssapApplicationRepository.getOne(ssapApplicationId);

		if (ssapApplication == null) {
			log.error("Unable to find SSAP by given application id: {}", ssapApplicationId);
			return null;
		} else {
			if (ssapApplication.getFinancialAssistanceFlag() == null
					|| ssapApplication.getFinancialAssistanceFlag().equalsIgnoreCase("N")) {
				NonEsiMecVerificationResponse verificationResponse = new NonEsiMecVerificationResponse();
				ResponseMetadata responseMetadata = new ResponseMetadata();
				responseMetadata.setResponseCode(ResponseCode.EXCEPTION);
				responseMetadata.setResponseDescriptionText(String.format(
						"This SSAP [%d] doesn't have financial assistance flag set to Y: %s, skipping NonEsiMecService call.",
						ssapApplication.getId(), ssapApplication.getFinancialAssistanceFlag()));
				verificationResponse.setAdditionalProperty(ResponseCode.EXCEPTION.getName(),
						responseMetadata.getResponseDescriptionText());
				return verificationResponse;
			}

			log.info("Invoking NMEC for given application id: {}", ssapApplicationId);
		}

		final NonEsiMecVerificationRequest nmecRequest = getRequestForApplication(ssapApplication);
		
		if (nmecRequest == null) {
				NonEsiMecVerificationResponse verificationResponse = new NonEsiMecVerificationResponse();
				ResponseMetadata responseMetadata = new ResponseMetadata();
				responseMetadata.setResponseCode(ResponseCode.EXCEPTION);
				responseMetadata.setResponseDescriptionText(String.format(
						" SSAP [%d] doesn't have any Members eligible for Non ESI Verifications, skipping NonEsiMecService call.",
						ssapApplication.getId() ));
				verificationResponse.setAdditionalProperty(ResponseCode.EXCEPTION.getName(),
						responseMetadata.getResponseDescriptionText());
				return verificationResponse;
			}
		try {
			log.info("Sending NMEC request for URL: {}, Request {}", hubIntegrationUrl,
					om.writeValueAsString(nmecRequest));
		} catch (JsonProcessingException e1) {
			log.info("unable to parse nmecRequest " + nmecRequest);
		}

		NonEsiMecVerificationResponse nmecResponse = null;

		try {
			nmecResponse = restTemplate.postForObject(hubIntegrationUrl, nmecRequest,
					NonEsiMecVerificationResponse.class);
			log.info("Got NMEC response: {}", nmecResponse);
			if (nmecResponse == null) {
				throw new Exception("NMEC Response was null");
			}
			Map<String, MecResults> resultSsnMap = new HashMap<String, MecResults>();
			nmecResponse.getMemberResponseSet().forEach(memberResponse -> {
				resultSsnMap.put(memberResponse.getSsn(), memberResponse.getMecResults());
			});
			log.debug("resultsMap: {} ", resultSsnMap);
			/*
			 * Map<String, Set<MecResults>> map =
			 * nmecResponse.getMemberResponseSet().stream()
			 * .collect(Collectors.groupingBy(MemberResponseSet::getSsn,
			 * Collectors.mapping(MemberResponseSet::getMecResults, Collectors.toSet())));
			 */
			List<HouseholdMember> householdMembers = getMembersForVerification(ssapApplication);

			Map<String, String> guidSsnMap = new HashMap<String, String>();
			householdMembers.forEach(member -> {
				guidSsnMap.put(member.getApplicantGuid(), member.getSocialSecurityCard().getSocialSecurityNumber());
			});
			log.debug("guidMap"+guidSsnMap.toString());
			/*
			 * For each GUID , get the SSN and retrieve the results from resultSsnMap If ssn
			 * is null then mark as DMI and NOT_VERIFIED If there is an SSN and results
			 * resultmap has an entry then parse all the results - If one of the source's
			 * verification is NOT_VERFIED then mark the applicant as DMI and NOT_VERIFIED -
			 * Else if all the sources are verified then MARK the applicant as VERIFIED
			 */
			guidSsnMap.keySet().forEach(guid -> {
				if (guidSsnMap.get(guid) != null) {
					// SSN corresponding to applicant guid not empty
					if (resultSsnMap.containsKey(guidSsnMap.get(guid))
							&& resultSsnMap.get(guidSsnMap.get(guid)) != null) {

            final SsapApplicant applicant = ssapApplicantRepository.findByApplicantGuidAndAppId(guid, ssapApplication.getId());

            if(applicant != null) {
              // HUB responded with Verification results for the given SSN
              MecResults result = resultSsnMap.get(guidSsnMap.get(guid));
              List<String> sourceResultList = getVerficationsFromSources(result);
              log.info(String.format("# of sources received from hub response for entry %s is %d", guidSsnMap.get(guid), sourceResultList.size()));
              long verifiedSourcesCount = sourceResultList.stream()
                .filter(c -> c.equalsIgnoreCase(ApplicantVerificationStatus.VERIFIED.getValue()))
                .count();
              log.info("No.of Hub Sources verified: " + verifiedSourcesCount);
              if (verifiedSourcesCount == sourceResultList.size()) {
                // All sources are verified, Mark the applicant as Verified
                log.info("Marking SSAP Applicant NMEC STATUS for {} as Verified", guid);
                applicant.setNonEsiMecVerificationStatus(ApplicantVerificationStatus.VERIFIED.getValue());
              } else { // DMI,mark the applicant as NOT_VERIFIED
                log.info("Marking SSAP Applicant NMEC STATUS for {} as Not Verified", guid);
                applicant.setNonEsiMecVerificationStatus(
                  ApplicantVerificationStatus.NOT_VERIFIED.getValue());
              }

              ssapApplicantRepository.saveAndFlush(applicant);
            } else {
              log.warn("Unable to find SsapApplicant record by guid: {} and ssap id: {}", guid, ssapApplication.getId());
            }
					} else {
						// TODO : Check do we need to set any indicator at applicant.
						// Verification not invoked for the applicant or Hub not returned any response
						// for the member	
						// No verification required as the member having Non-ESI MEC coverage flag set
					}
				} else {
					final SsapApplicant applicant = ssapApplicantRepository.findByApplicantGuidAndAppId(guid,ssapApplication.getId());
					if(applicant != null) {
            // SSN is empty case, Since verification is not invoked, set to NOT_VERIDIED and
            // DMI case.
            log.info("Marking SSAP Applicant NMEC STATUS for {} as Not Verified", guid);
            applicant.setNonEsiMecVerificationStatus(ApplicantVerificationStatus.NOT_VERIFIED.getValue());
            ssapApplicantRepository.saveAndFlush(applicant);
          } else {
            log.warn("Unable to find SsapApplicant record by guid: {} and ssap id: {}", guid, ssapApplication.getId());
          }
				}
			});
		} catch (HttpClientErrorException e) {
			log.error("HTTP Client exception occurred", e);
			// log.error("Exception details", e);
			nmecResponse = new NonEsiMecVerificationResponse();
			nmecResponse.setErrorMessageDetail(getNonEsiMecErrorDetailsFromException(e));
			nmecResponse.setAdditionalProperty(ResponseCode.EXCEPTION.getName(), ResponseCode.EXCEPTION.getMessage());
		} catch (HttpServerErrorException se) {
			log.error("HTTP Server exception occurred", se);
			// log.error("Exception details", se);
			nmecResponse = new NonEsiMecVerificationResponse();
			nmecResponse.setErrorMessageDetail(getNonEsiMecErrorDetailsFromException(se));
			nmecResponse.setAdditionalProperty(ResponseCode.COMMUNICATION_ERROR.getName(),
					ResponseCode.COMMUNICATION_ERROR.getMessage());

		} catch (Exception ex) {
			log.error("Exception occurred", ex);
			// log.error("Exception details", ex);
			nmecResponse = new NonEsiMecVerificationResponse();
			nmecResponse.setErrorMessageDetail(getNonEsiMecErrorDetailsFromException(ex));
		}

		return nmecResponse;
	}

	public List<String> getVerficationsFromSources(MecResults result) {
		List<String> list = new ArrayList<String>();
		/*
		 * Uncomment this block when ever data sources for MEDC/CHIP are ready 
		 * HIX-117543 to Ignore and 
		 * HIX-117544 to add it back
		*/
		
		if(result.getCHIP()!=null && result.getCHIP().getMecVerified()!=null )
		list.add(result.getCHIP().getMecVerified());
		
		if(result.getMEDC()!=null && result.getMEDC().getMecVerified()!=null )
		list.add(result.getMEDC().getMecVerified());
		
		if(result.getPECO()!=null && result.getPECO().getMecVerified()!=null )
		list.add(result.getPECO().getMecVerified());
		
		if(result.getBHPC()!=null && result.getBHPC().getMecVerified()!=null )
		list.add(result.getBHPC().getMecVerified());
		
		if(result.getMEDI()!=null && result.getMEDI().getMecVerified()!=null )
		list.add(result.getMEDI().getMecVerified());
		
		if(result.getVHPC()!=null && result.getVHPC().getMecVerified()!=null )
		list.add(result.getVHPC().getMecVerified());
		
		if(result.getTRIC()!=null && result.getTRIC().getMecVerified()!=null )
		list.add(result.getTRIC().getMecVerified());
		return list;
	}

	/**
	 * Constructs {@link NonEsiMecVerificationRequest} object from given
	 * {@link SsapApplication} object.
	 *
	 * @param ssapApplication {@link SsapApplication} object to construct request
	 *                        from.
	 * @return Constructed {@link NonEsiMecVerificationRequest} object that is ready
	 *         to be send to NMEC service.
	 */
	public NonEsiMecVerificationRequest getRequestForApplication(final SsapApplication ssapApplication) {
		final SingleStreamlinedApplication singleStreamlinedApplication = JsonUtil
				.parseApplicationDataJson(ssapApplication);
		final NonEsiMecVerificationRequest nmecRequest = new NonEsiMecVerificationRequest();
		nmecRequest.setApplicationId(ssapApplication.getId());

		List<Map<String, String>> dobSsnList = new ArrayList<>();

		if (singleStreamlinedApplication != null) {
			nmecRequest.setClientIp(singleStreamlinedApplication.getClientIp());

			List<NonEsiMecHouseholdMember> nonEsiMecmembers = new LinkedList<>();

			// Get the Household Members for verfication
			// NOTE : Excluding the public program eligible members
			List<HouseholdMember> householdMembers = getMembersForVerification(ssapApplication);

			List<HouseholdMember> nmecEligibleMember = new ArrayList<HouseholdMember>();

			householdMembers.forEach(hhm -> {
				if (hhm.getSocialSecurityCard() != null
						&& hhm.getSocialSecurityCard().getSocialSecurityNumber() != null) {
					if (hhm.getHealthCoverage() != null && hhm.getHealthCoverage().getCurrentOtherInsurance() != null
							&& !hhm.getHealthCoverage().getCurrentOtherInsurance().isHasNonESI()) {
						nmecEligibleMember.add(hhm);
					}
				}
			});
			nmecEligibleMember.forEach(hhm -> {

				if (hhm == null) {
					log.error("Household member is null");
				}

				assert hhm != null : "Household member is null";

				Map<String, String> ssnDob = new HashMap<>();

				NonEsiMecHouseholdMember member = new NonEsiMecHouseholdMember();
				member.setDateOfBirth(DateUtil.dateToString(hhm.getDateOfBirth(), "MM/dd/yyyy"));
				member.setGender(hhm.getGender().equalsIgnoreCase("MALE") ? MALE
						: hhm.getGender().equalsIgnoreCase("FEMALE") ? FEMALE : "");//

				if (hhm.getName() != null) {
					if (hhm.getName().getMiddleName() != null && !hhm.getName().getMiddleName().equals("")) {
						member.setName(new Name(hhm.getName().getFirstName(), hhm.getName().getMiddleName(),
								hhm.getName().getLastName()));
					} else {
						member.setName(new Name(hhm.getName().getFirstName(), hhm.getName().getLastName()));
					}
				}

				if (hhm.getSocialSecurityCard() != null
						&& hhm.getSocialSecurityCard().getSocialSecurityNumber() != null) {
					member.setSocialSecurityCard(
							new SocialSecurityCard(hhm.getSocialSecurityCard().getSocialSecurityNumber()));

				} else {
					log.warn(
							"Can't set NMEC household member's social security card from application household because it's null/blank");
				}
				member.setHouseholdContact(new HouseholdContact());
				member.getHouseholdContact().setHomeAddress(new HomeAddress());
				member.getHouseholdContact().getHomeAddress()
						.setState(hhm.getHouseholdContact().getHomeAddress().getState());
				member.setOrganization(null);// Don't set organization code as we need to verify all the sources
				member.setInsurance(new Insurance());
				member.getInsurance().setInsurancePolicyEffectiveDate(getEffectiveDate(hhm.getApplicantGuid(), "START",
						String.valueOf(ssapApplication.getCoverageYear()), ssapApplication.getId()));
				member.getInsurance().setInsurancePolicyExpirationDate(getEffectiveDate(hhm.getApplicantGuid(), "END",
						String.valueOf(ssapApplication.getCoverageYear()), ssapApplication.getId()));
				nonEsiMecmembers.add(member);
			});
		   if(nonEsiMecmembers.isEmpty())
		   {
			   // No need to invoke Verification as none of the members in the Household  are eligible for NMEC verifications. 
			   return null;
		   }
		   NonEsiMecPayload payload = new NonEsiMecPayload();
			List<NonEsiMecTaxHousehold> nonEsiMecTaxHouseholds = new ArrayList<NonEsiMecTaxHousehold>();
			NonEsiMecTaxHousehold nonEsiMecTaxHousehold = new NonEsiMecTaxHousehold();
			nonEsiMecTaxHousehold.setHouseholdMember(nonEsiMecmembers);
			nonEsiMecTaxHouseholds.add(nonEsiMecTaxHousehold);
			payload.setTaxHousehold(nonEsiMecTaxHouseholds);

			nmecRequest.setPayload(payload);
		} else {
			log.error(
					"Cannot construct NMEC Request. Given SsapApplication doesn't contain valid SingleStreamlinedApplication in it's data column");
		}

		return nmecRequest;
	}

	private List<HouseholdMember> getMembersForVerification(final SsapApplication ssapApplication) {
		final SingleStreamlinedApplication singleStreamlinedApplication = JsonUtil
				.parseApplicationDataJson(ssapApplication);
		List<HouseholdMember> householdMembers = HouseholdUtil
				.filterHouseholdMembersForVerifications(singleStreamlinedApplication);
		return householdMembers;
	}

	private String getEffectiveDate(String applicantGuid, String type, String coverageYear, Long applicationId) {

		EligibilityProgram eligProgram = null;
		SsapApplicant applicant = ssapApplicantRepository.findByApplicantGuidAndAppId(applicantGuid, applicationId);
		if (applicant != null && applicant.getId() > 0) {
			eligProgram = eligibilityProgramRepository.findByEligibilityTypeAndEligibilityIndicatorAndSsapApplicantId(
					"ExchangeEligibilityType", "TRUE", applicant.getId());
		}
		if (type.equalsIgnoreCase("START")) {
			if (eligProgram!=null && eligProgram.getEligibilityStartDate() != null) {
				return DateUtil.dateToString(eligProgram.getEligibilityStartDate(), "MM/dd/yyyy");
			} else {
				log.info(String.format(
						"Start Date is not available in Program Eligibility for applicantGuid %s , Defaulting to 01/01 for CoverageYear %s",
						applicantGuid, coverageYear));
				return "01/01/" + coverageYear;
			}

		}
		if (type.equalsIgnoreCase("END")) {
			if (eligProgram!=null && eligProgram.getEligibilityEndDate() != null) {
				return DateUtil.dateToString(eligProgram.getEligibilityEndDate(), "MM/dd/yyyy");
			} else {
				log.info(String.format(
						"End Date is not available in Program Eligibility for applicantGuid %s , Defaulting to 12/31 for CoverageYear %s",
						applicantGuid, coverageYear));
				return "12/31/" + coverageYear;

			}

		}
		return null;
	}

	/**
	 * Parses exception and returns list of {@link NonEsiMecErrorMessageDetail}.
	 * 
	 * @param exception exception.
	 * @return list of {@link NonEsiMecErrorMessageDetail}.
	 */
	public static List<NonEsiMecErrorMessageDetail> getNonEsiMecErrorDetailsFromException(Exception exception) {
		NonEsiMecErrorMessageDetail errorMessageDetail = new NonEsiMecErrorMessageDetail();
		ResponseMetadata responseMetadata = new ResponseMetadata();

		if (exception instanceof HttpServerErrorException) {
			HttpServerErrorException serverErrorException = (HttpServerErrorException) exception;
			responseMetadata.setResponseDescriptionText(serverErrorException.getMessage());
			if (serverErrorException.getStatusCode().is5xxServerError()) {
				responseMetadata.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
			} else {
				responseMetadata.setResponseCode(ResponseCode.EXCEPTION);
			}
		} else {
			responseMetadata.setResponseCode(ResponseCode.EXCEPTION);
			responseMetadata.setResponseDescriptionText(exception.getMessage());
		}

		errorMessageDetail.setResponseMetadata(responseMetadata);

		return Collections.singletonList(errorMessageDetail);
	}
}
