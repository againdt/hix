package com.getinsured.hix.batch.ssap.service;

import java.util.List;

import com.getinsured.hix.platform.util.exception.GIException;

public interface UpdateEnrlEventForExternalAppIdsService {
    void processUpdateEnrlEvent(String ssapAppIdsFilePath, Long jobId, List<String> issuerIds) throws GIException;
}
