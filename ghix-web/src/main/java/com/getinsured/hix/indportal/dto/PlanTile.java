package com.getinsured.hix.indportal.dto;

public class PlanTile {

	private String planName;
	private float monthlyPremium;
	private float netPremium;
	private String planType;
	private String officeVisit;
	private String genericMedication;
	private String deductible;
	private String outOfPocket;
	private String issuerLogo;
	private boolean favoritePlanSelected;
	private int planId;
	private String coverageDate;
	private String coverageStartDate;
	private String coverageEndDate;
	private String issuerContactNo;
	private String enMessage;
	
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public float getMonthlyPremium() {
		return monthlyPremium;
	}
	public void setMonthlyPremium(float monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}
	public float getNetPremium() {
		return netPremium;
	}
	public void setNetPremium(float netPremium) {
		this.netPremium = netPremium;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getOfficeVisit() {
		return officeVisit;
	}
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	public String getGenericMedication() {
		return genericMedication;
	}
	public void setGenericMedication(String genericMedication) {
		this.genericMedication = genericMedication;
	}
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	public String getOutOfPocket() {
		return outOfPocket;
	}
	public void setOutOfPocket(String outOfPocket) {
		this.outOfPocket = outOfPocket;
	}
	public String getIssuerLogo() {
		return issuerLogo;
	}
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	public boolean getFavoritePlanSelected() {
		return favoritePlanSelected;
	}
	public void setFavoritePlanSelected(boolean favoritePlanSelected) {
		this.favoritePlanSelected = favoritePlanSelected;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getCoverageDate() {
		return coverageDate;
	}
	public void setCoverageDate(String coverageDate) {
		this.coverageDate = coverageDate;
	}
	public String getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public String getCoverageEndDate() {
		return coverageEndDate;
	}
	public void setCoverageEndDate(String coverageEndDate) {
		this.coverageEndDate = coverageEndDate;
	}
	public String getIssuerContactNo() {
		return issuerContactNo;
	}
	public void setIssuerContactNo(String issuerContactNo) {
		this.issuerContactNo = issuerContactNo;
	}
	public String getEnMessage() {
		return enMessage;
	}
	public void setEnMessage(String enMessage) {
		this.enMessage = enMessage;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PlanTile [planName=");
		builder.append(planName);
		builder.append(", monthlyPremium=");
		builder.append(monthlyPremium);
		builder.append(", netPremium=");
		builder.append(netPremium);
		builder.append(", planType=");
		builder.append(planType);
		builder.append(", officeVisit=");
		builder.append(officeVisit);
		builder.append(", genericMedication=");
		builder.append(genericMedication);
		builder.append(", deductible=");
		builder.append(deductible);
		builder.append(", outOfPocket=");
		builder.append(outOfPocket);
		builder.append(", issuerLogo=");
		builder.append(issuerLogo);
		builder.append(", favoritePlanSelected=");
		builder.append(favoritePlanSelected);
		builder.append(", planId=");
		builder.append(planId);
		builder.append(", coverageDate=");
		builder.append(coverageDate);
		builder.append(", coverageStartDate=");
		builder.append(coverageStartDate);
		builder.append(", coverageEndDate=");
		builder.append(coverageEndDate);
		builder.append(", issuerContactNo=");
		builder.append(issuerContactNo);
		builder.append(", enMessage=");
		builder.append(enMessage);
		builder.append("]");
		return builder.toString();
	}
	
}
