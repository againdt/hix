/**
 * 
 */
package com.getinsured.hix.batch.enrollment.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentMonthlyIRSInboundBatchService;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public class EnrollmentMonthlyIRSInboundProcessingTask implements Tasklet{

	private EnrollmentMonthlyIRSInboundBatchService enrollmentMonthlyIRSInboundBatchService;
	private Job job;
	private JobLauncher jobLauncher; 
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyIRSInboundProcessingTask.class);

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		List<BatchJobExecution> batchExecutionList = enrollmentMonthlyIRSInboundBatchService.getRunningBatchList("monthlyIRSOutJob");
		if(batchExecutionList != null && batchExecutionList.size() == 0){
			try{
				LOGGER.info("Processing Inbound Response Tasklet");
				StepContext stepContext = chunkContext.getStepContext();
				StepExecution stepExecution = stepContext.getStepExecution();

				synchronized(enrollmentMonthlyIRSInboundBatchService){
					enrollmentMonthlyIRSInboundBatchService.processInboundResponse(stepExecution.getJobExecutionId(),  jobLauncher, job);
				}

			}catch(GIException e){
				throw new GIException("Error in Processing Inbound Response", e);
			}
		}else{
			throw new GIException("Error in Processing Inbound Response :: An instance of the monthlyIRSOutJob is still running!");
		}
		return RepeatStatus.FINISHED;
	}

	/**
	 * @return the enrollmentMonthlyIRSInboundBatchService
	 */
	public EnrollmentMonthlyIRSInboundBatchService getEnrollmentMonthlyIRSInboundBatchService() {
		return enrollmentMonthlyIRSInboundBatchService;
	}

	/**
	 * @param enrollmentMonthlyIRSInboundBatchService the enrollmentMonthlyIRSInboundBatchService to set
	 */
	public void setEnrollmentMonthlyIRSInboundBatchService(
			EnrollmentMonthlyIRSInboundBatchService enrollmentMonthlyIRSInboundBatchService) {
		this.enrollmentMonthlyIRSInboundBatchService = enrollmentMonthlyIRSInboundBatchService;
	}
	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}

	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}
}
