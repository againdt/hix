package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.QhpReportService;
import com.getinsured.hix.batch.ssap.renewal.util.QhpReportPartitionerParams;
import com.getinsured.hix.dto.enrollment.QhpReportDTO;

/**
 * QHP Report Processor class is used to get QHP Reports data in DTO class from database.
 * 
 * @since June 14, 2019
 */
@Component("renewalQHPReportProcessor")
public class QhpReportProcessor implements ItemProcessor<Integer, List<QhpReportDTO>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(QhpReportProcessor.class);

	private QhpReportService qhpReportService;
	private QhpReportPartitionerParams qhpReportPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Processor ");

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			int partition = executionContext.getInt("partition");
			int startIndex = executionContext.getInt("startIndex");
			int endIndex = executionContext.getInt("endIndex");
			LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);
		}
	}

	@Override
	public List<QhpReportDTO> process(Integer readerEnrollmentId) throws Exception {
		List<QhpReportDTO> qhpReportList = qhpReportService.getEnrollmentAndEnrolleeDataToGenerateQHPReports(readerEnrollmentId);

		if (CollectionUtils.isNotEmpty(qhpReportList)) {
			LOGGER.debug("Successfully get {} QHP Report records from Enrollment and Enrollee tables.", qhpReportList.size());
		}
		else {
			LOGGER.warn("Unable to find QHP Reports data to write in database.");
		}
		return qhpReportList;
	}

	public QhpReportService getQhpReportService() {
		return qhpReportService;
	}

	public void setQhpReportService(QhpReportService qhpReportService) {
		this.qhpReportService = qhpReportService;
	}

	public QhpReportPartitionerParams getQhpReportPartitionerParams() {
		return qhpReportPartitionerParams;
	}

	public void setQhpReportPartitionerParams(QhpReportPartitionerParams qhpReportPartitionerParams) {
		this.qhpReportPartitionerParams = qhpReportPartitionerParams;
	}
}