package com.getinsured.hix.planmgmt.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.timeshift.TimeShifterUtil;

public class PlanHealthMapper {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PlanHealthMapper.class);

	private static Map<String, Integer> columnKeys = null;

	private static final String BENEFIT_NAME = "Benefits";
	public static final String DB_DATE_FORMAT = "yyyy-MM-dd";
	public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";

	/* Renamed Fields Constants */
	private static final String LIMITATION = "Limitation( Numeric Value)";
	private static final String LIMITATIONATTRIBUTE = "Limitation Attribute";
	private static final String EXCEPTIONS = "Exceptions";

	private static final String SUBJECTTOINNETWORKDEDUCTIBLE = "Subject to Deductible (Tier 1)";
	private static final String EXCLUDEFROMINNETWORKMOOP = "Excluded from In Network MOOP";
	private static final String SUBJECTTOOUTOFNETWORKDEDUCTIBLE = "Subject to Deductible (Tier 2)";
	private static final String EXCLUDEDFROMOUTOFNETWORKMOOP = "Excluded from Out of Network MOOP";
	private static final String COMBINEDINANDOUTOFNETWORK = "Combined In and Out of Network";
	private static final String COMBINEDINANDOUTOFNETWORKATTRIBUTE = "Combined In and Out of Network Attribute";
	private static final String INNETWORKINDIVIDUAL = "In Network - Individual";
	private static final String INNETWORKFAMILY = "In Network - Family";
	private static final String INNETWORKTIERTWOKINDIVIDUAL = "In Network (Tier 2) - Individual";
	private static final String INNETWORKTIERTWOKFAMILY = "In Network (Tier 2) - Family ";
	private static final String OUTNETWORKINDIVIDUAL = "Out of Network - Individual";
	private static final String OUTNETWORKFAMILY = "Out of Network - Family";
	private static final String COMBINEDINANDOUTOFNETWORKINDIVIDUAL = "Combined In/Out Network - Individual";
	private static final String COMBINEDINANDOUTOFNETWORKFAMILY = "Combined In/Out Network - Family";

	/* New Fields Constants start here */
	private static final String NETWORKT1COPAYATTR = "Network Tier1 Copay Attribute";
	private static final String NETWORKT1COPAYVAL = "Network Tier1 Copay Value";
	private static final String NETWORKT1COINSURANCEATTR = "Network Tier1 Coinsurance Attribute";
	private static final String NETWORKT1COINSURANCEVAL = "Network Tier1 Coinsurance Value";

	private static final String NETWORKT2COPAYATTR = "Network Tier2 Copay Attribute";
	private static final String NETWORKT2COPAYVAL = "Network Tier2 Copay Value";
	private static final String NETWORKT2COINSURANCEATTR = "Network Tier2 Coinsurance Attribute";
	private static final String NETWORKT2COINSURANCEVAL = "Network Tier2 Coinsurance Value";

	private static final String OUTNETWORKCOPAYVAL = "Out Of Network Copay Value";
	private static final String OUTNETWORKCOPAYATTR = "Out Of Network Copay Attribute";
	private static final String OUTNETWORKCOINSURANCEVAL = "Out Of Network Coinsurance Value";
	private static final String OUTNETWORKCOINSURANCEATTR = "Out Of Network Coinsurance Attribute";
	/* New Fields Constants end here */

	// for new Report Format
	
		private static final  String PLANNAMEANDNUMBER = "Plan Name and Number";
	private static final  String PLANSTARTDATE = "Plan Start Date";
	private static final  String PLANENDDATE = "Plan End Date";
		private static final  String INNETWORKTIER1 = "In Network Tier 1";
		private static final  String INNETWORKTIER2 = "In Network Tier 2";
		private static final  String OUTOFNETWORK = "Out of Network";
		private static final  String COMBINEDINOUTNETWORK = "Combined In/Out Network";
		private static final  String DEDUCTIBLES = "Deductibles";
		private static final  String INDIVIDUAL = "Individual";
		private static final  String FAMILY = "Family";
		private static final  String DEFAULTCOINSURANCE = "Default Coinsurance";
		private static final  String EMPTYSTRING = " ";
		private static final  String INNETWORK = "In Network";
		private static final  String TIER1 = "Tier 1";
		private static final  String TIER2 = "Tier 2";
	
	/* Renamed values for Benefit Attribute */
	private static final int LIMITATION_VAL = 1;
	private static final int LIMITATIONATTRIBUTE_VAL = 2;
	private static final int EXCEPTIONS_VAL = 3;

	private static final int SUBJECTTOINNETWORKDEDUCTIBLE_VAL = 4;
	private static final int EXCLUDEFROMINNETWORKMOOP_VAL = 5;

	private static final int BENEFIT_NAME_VAL = 6;
	private static final int SUBJECTTOOUTOFNETWORKDEDUCTIBLE_VAL = 7;
	private static final int EXCLUDEDFROMOUTOFNETWORKMOOP_VAL = 8;
	private static final int COMBINEDINANDOUTOFNETWORK_VAL = 9;
	private static final int COMBINEDINANDOUTOFNETWORKATTRIBUTE_VAL = 10;
	private static final int INNETWORKINDIVIDUAL_VAL = 11;
	private static final int INNETWORKFAMILY_VAL = 12;
	private static final int INNETWORKTIERTWOKINDIVIDUAL_VAL = 13;
	private static final int INNETWORKTIERTWOKFAMILY_VAL = 14;
	private static final int OUTNETWORKINDIVIDUAL_VAL = 15;
	private static final int OUTNETWORKFAMILY_VAL = 16;
	private static final int COMBINEDINANDOUTOFNETWORKINDIVIDUAL_VAL = 17;
	private static final int COMBINEDINANDOUTOFNETWORKFAMILY_VAL = 18;

	/* New Fields Constants value start here */
	private static final int NETWORKT1COPAYATTR_VAL = 19;
	private static final int NETWORKT1COPAYVAL_VAL = 20;
	private static final int NETWORKT1COINSURANCEATTR_VAL = 21;
	private static final int NETWORKT1COINSURANCEVAL_VAL = 22;

	private static final int NETWORKT2COPAYATTR_VAL = 23;
	private static final int NETWORKT2COPAYVAL_VAL = 24;
	private static final int NETWORKT2COINSURANCEATTR_VAL = 25;
	private static final int NETWORKT2COINSURANCEVAL_VAL = 26;

	private static final int OUTNETWORKCOPAYATTR_VAL = 27;
	private static final int OUTNETWORKCOPAYVAL_VAL = 28;
	private static final int OUTNETWORKCOINSURANCEATTR_VAL = 29;
	private static final int OUTNETWORKCOINSURANCEVAL_VAL = 30;
	/* New Fields Constants value end here */

	static {
		columnKeys = new HashMap<String, Integer>();

		columnKeys.put(BENEFIT_NAME, BENEFIT_NAME_VAL);

		/* Renamed Attribute chagnes */
		columnKeys.put(LIMITATION, LIMITATION_VAL);
		columnKeys.put(LIMITATIONATTRIBUTE, LIMITATIONATTRIBUTE_VAL);
		columnKeys.put(EXCEPTIONS, EXCEPTIONS_VAL);

		columnKeys.put(SUBJECTTOINNETWORKDEDUCTIBLE,
				SUBJECTTOINNETWORKDEDUCTIBLE_VAL);
		columnKeys.put(EXCLUDEFROMINNETWORKMOOP, EXCLUDEFROMINNETWORKMOOP_VAL);
		columnKeys.put(SUBJECTTOOUTOFNETWORKDEDUCTIBLE,
				SUBJECTTOOUTOFNETWORKDEDUCTIBLE_VAL);
		columnKeys.put(EXCLUDEDFROMOUTOFNETWORKMOOP,
				EXCLUDEDFROMOUTOFNETWORKMOOP_VAL);
		columnKeys
				.put(COMBINEDINANDOUTOFNETWORK, COMBINEDINANDOUTOFNETWORK_VAL);
		columnKeys.put(COMBINEDINANDOUTOFNETWORKATTRIBUTE,
				COMBINEDINANDOUTOFNETWORKATTRIBUTE_VAL);
		columnKeys.put(INNETWORKINDIVIDUAL, INNETWORKINDIVIDUAL_VAL);
		columnKeys.put(INNETWORKFAMILY, INNETWORKFAMILY_VAL);
		columnKeys.put(INNETWORKTIERTWOKINDIVIDUAL,
				INNETWORKTIERTWOKINDIVIDUAL_VAL);
		columnKeys.put(INNETWORKTIERTWOKFAMILY, INNETWORKTIERTWOKFAMILY_VAL);
		columnKeys.put(OUTNETWORKINDIVIDUAL, OUTNETWORKINDIVIDUAL_VAL);
		columnKeys.put(OUTNETWORKFAMILY, OUTNETWORKFAMILY_VAL);
		columnKeys.put(COMBINEDINANDOUTOFNETWORKINDIVIDUAL,
				COMBINEDINANDOUTOFNETWORKINDIVIDUAL_VAL);
		columnKeys.put(COMBINEDINANDOUTOFNETWORKFAMILY,
				COMBINEDINANDOUTOFNETWORKFAMILY_VAL);

		/* New Benefit Attribute Start here */
		columnKeys.put(NETWORKT1COPAYATTR, NETWORKT1COPAYATTR_VAL);
		columnKeys.put(NETWORKT1COPAYVAL, NETWORKT1COPAYVAL_VAL);
		columnKeys.put(NETWORKT1COINSURANCEATTR, NETWORKT1COINSURANCEATTR_VAL);
		columnKeys.put(NETWORKT1COINSURANCEVAL, NETWORKT1COINSURANCEVAL_VAL);

		columnKeys.put(NETWORKT2COPAYATTR, NETWORKT2COPAYATTR_VAL);
		columnKeys.put(NETWORKT2COPAYVAL, NETWORKT2COPAYVAL_VAL);
		columnKeys.put(NETWORKT2COINSURANCEATTR, NETWORKT2COINSURANCEATTR_VAL);
		columnKeys.put(NETWORKT2COINSURANCEVAL, NETWORKT2COINSURANCEVAL_VAL);

		columnKeys.put(OUTNETWORKCOPAYATTR, OUTNETWORKCOPAYATTR_VAL);
		columnKeys.put(OUTNETWORKCOPAYVAL, OUTNETWORKCOPAYVAL_VAL);
		columnKeys
				.put(OUTNETWORKCOINSURANCEATTR, OUTNETWORKCOINSURANCEATTR_VAL);
		columnKeys.put(OUTNETWORKCOINSURANCEVAL, OUTNETWORKCOINSURANCEVAL_VAL);
		/* New Benefit Attribute End here */
	}

	public PlanHealth mapData(List<List<String>> data, final List<String> columns,Map<String,String> benefitMap){
		PlanHealth planHealth = new PlanHealth();
		try {
			long startTime = TimeShifterUtil.currentTimeMillis();
			//nameKeys = new HashMap<String, String>();

			/*for (HierarchicalConfiguration sub : benefitList) {
				String BenefitName = sub.getString("Name");
				String BenefitConst = sub.getString("ConstName");
				nameKeys.put(BenefitName, BenefitConst);
			}*/

			LOGGER.info("Started mapping the data file for Plan benefits.");
			String colName = null;
			String key = null;
			PlanHealthBenefit planHealthBenefit = new PlanHealthBenefit();
			;
			int keyIndex = columns.indexOf(BENEFIT_NAME);
			HashMap<String, PlanHealthBenefit> benefitsObjs1 = new HashMap<String, PlanHealthBenefit>();
			planHealth.setPlanHealthBenefits(benefitsObjs1);
			LOGGER.info("PlanHealth mapdata");
			for (List<String> line : data) {
				key = line.get(keyIndex);
				LOGGER.debug("Key Value from Mapper: " + SecurityUtil.sanitizeForLogging(String.valueOf(key)));
				if (key != null && key.length() > 0) {
					planHealthBenefit = new PlanHealthBenefit();
					if (benefitMap.containsKey(key)) {
						String keyConst = benefitMap.get(key);
						planHealthBenefit.setName(keyConst);
						benefitsObjs1.put(keyConst, planHealthBenefit);
					} else {
						planHealthBenefit.setName(key);
						benefitsObjs1.put(key, planHealthBenefit);
					}
					planHealthBenefit.setPlanHealth(planHealth);
				}
				int colIdx = 0;
				for (String columnData : line) {
					colName = columns.get(colIdx);
					LOGGER.debug("Mapping data for " + SecurityUtil.sanitizeForLogging(String.valueOf(key)));
					mapData(columnData, colName, planHealthBenefit);
					colIdx++;
				}
				key = null;
				planHealthBenefit = null;
			}
			long endTime = TimeShifterUtil.currentTimeMillis();
			LOGGER.debug("Mapping the data file for Plan benefits completed in." + SecurityUtil.sanitizeForLogging(String.valueOf((endTime - startTime))) + " seconds.");

		} catch (Exception e) {
			LOGGER.error("Error occured while mapping to constant",
					e.getMessage(), e);
		}
		return planHealth;
	}

	private void mapData(String data, String column,
			PlanHealthBenefit planHealthBenefit) {

		Integer colVal = columnKeys.get(column);
		if (colVal == null) {
			colVal = 0;
		}
		switch (colVal) {

		/* Renamed Benefit Attribute Start here */
		case LIMITATION_VAL:
			planHealthBenefit.setNetworkLimitation(data);
			break;
		case LIMITATIONATTRIBUTE_VAL:
			planHealthBenefit.setNetworkLimitationAttribute(data);
			break;
		case EXCEPTIONS_VAL:
			planHealthBenefit.setNetworkExceptions(data);
			break;
		/* Renamed Benefit Attribute End here */

		case SUBJECTTOINNETWORKDEDUCTIBLE_VAL:
			planHealthBenefit.setSubjectToInNetworkDuductible(data);
			break;
		case EXCLUDEFROMINNETWORKMOOP_VAL:
			planHealthBenefit.setExcludedFromInNetworkMoop(data);
			break;
		case SUBJECTTOOUTOFNETWORKDEDUCTIBLE_VAL:
			planHealthBenefit.setSubjectToOutNetworkDeductible(data);
			break;
		case EXCLUDEDFROMOUTOFNETWORKMOOP_VAL:
			planHealthBenefit.setExcludedFromOutOfNetworkMoop(data);
			break;
		case COMBINEDINANDOUTOFNETWORK_VAL:
			planHealthBenefit.setCombinedInAndOutOfNetwork(data);
			break;
		case COMBINEDINANDOUTOFNETWORKATTRIBUTE_VAL:
			planHealthBenefit.setCombinedInAndOutNetworkAttribute(data);
			break;
		case INNETWORKINDIVIDUAL_VAL:
			try {
				planHealthBenefit.setInNetworkIndividual(Double
						.parseDouble(data));
			} catch (Exception e) {
				planHealthBenefit.setInNetworkIndividual(0.0);
			}
			break;
		case INNETWORKFAMILY_VAL:
			try {
				planHealthBenefit.setInNetworkFamily(Double.parseDouble(data));
			} catch (Exception e) {
				planHealthBenefit.setInNetworkFamily(0.0);
			}
			break;
		case INNETWORKTIERTWOKINDIVIDUAL_VAL:
			try {
				planHealthBenefit.setInNetworkTierTwoIndividual(Double
						.parseDouble(data));
			} catch (Exception e) {
				planHealthBenefit.setInNetworkTierTwoIndividual(0.0);
			}
			break;
		case INNETWORKTIERTWOKFAMILY_VAL:
			try {
				planHealthBenefit.setInNetworkTierTwoFamily(Double
						.parseDouble(data));
			} catch (Exception e) {
				planHealthBenefit.setInNetworkTierTwoFamily(0.0);
			}
			break;
		case COMBINEDINANDOUTOFNETWORKINDIVIDUAL_VAL:
			try {
				planHealthBenefit.setCombinedInOutNetworkIndividual(Double
						.parseDouble(data));
			} catch (Exception e) {
				planHealthBenefit.setCombinedInOutNetworkIndividual(0.0);
			}
			break;
		case COMBINEDINANDOUTOFNETWORKFAMILY_VAL:
			try {
				planHealthBenefit.setCombinedInOutNetworkFamily(Double
						.parseDouble(data));
			} catch (Exception e) {
				planHealthBenefit.setCombinedInOutNetworkFamily(0.0);
			}
			break;

		/* New Benefit Attribute Start here */
		case NETWORKT1COPAYATTR_VAL:
			planHealthBenefit.setNetworkT1CopayAttr(data);
			break;
		case NETWORKT1COPAYVAL_VAL:
			planHealthBenefit.setNetworkT1CopayVal(data);
			break;
		case NETWORKT1COINSURANCEATTR_VAL:
			planHealthBenefit.setNetworkT1CoinsurAttr(data);
			break;
		case NETWORKT1COINSURANCEVAL_VAL:
			planHealthBenefit.setNetworkT1CoinsurVal(data);
			break;

		case NETWORKT2COPAYATTR_VAL:
			planHealthBenefit.setNetworkT2CopayAttr(data);
			break;
		case NETWORKT2COPAYVAL_VAL:
			planHealthBenefit.setNetworkT2CopayVal(data);
			break;
		case NETWORKT2COINSURANCEATTR_VAL:
			planHealthBenefit.setNetworkT2CoinsurAttr(data);
			break;
		case NETWORKT2COINSURANCEVAL_VAL:
			planHealthBenefit.setNetworkT2CoinsurVal(data);
			break;

		case OUTNETWORKCOPAYATTR_VAL:
			planHealthBenefit.setOutOfNetworkCopayAttr(data);
			break;
		case OUTNETWORKCOPAYVAL_VAL:
			planHealthBenefit.setOutOfNetworkCopayVal(data);
			break;
		case OUTNETWORKCOINSURANCEATTR_VAL:
			planHealthBenefit.setOutOfNetworkCoinsurAttr(data);
			break;
		case OUTNETWORKCOINSURANCEVAL_VAL:
			planHealthBenefit.setOutOfNetworkCoinsurVal(data);
			break;

		/* New Benefit Attribute End here */
		default:
			LOGGER.debug("Invalid option : " + SecurityUtil.sanitizeForLogging(String.valueOf(colVal)));
			break;
		}
	}

	public static List<String[]> convertObjectListToStringList(
			List<PlanHealthBenefit> healthBenefit,
			List<HierarchicalConfiguration> benefitList) {

		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders = new String[20];
		columnHeaders[0] = BENEFIT_NAME;

		/* Renamed Benefit Attributes */

		columnHeaders[1] = NETWORKT1COPAYVAL;
		columnHeaders[2] = NETWORKT1COPAYATTR;
		columnHeaders[3] = NETWORKT1COINSURANCEVAL;
		columnHeaders[4] = NETWORKT1COINSURANCEATTR;

		columnHeaders[5] = NETWORKT2COPAYVAL;
		columnHeaders[6] = NETWORKT2COPAYATTR;
		columnHeaders[7] = NETWORKT2COINSURANCEVAL;
		columnHeaders[8] = NETWORKT2COINSURANCEATTR;

		columnHeaders[9] = OUTNETWORKCOPAYVAL;
		columnHeaders[10] = OUTNETWORKCOPAYATTR;
		columnHeaders[11] = OUTNETWORKCOINSURANCEVAL;
		columnHeaders[12] = OUTNETWORKCOINSURANCEATTR;

		columnHeaders[13] = LIMITATION;
		columnHeaders[14] = LIMITATIONATTRIBUTE;
		columnHeaders[15] = EXCEPTIONS;

		columnHeaders[16] = SUBJECTTOINNETWORKDEDUCTIBLE;
		columnHeaders[17] = SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		columnHeaders[18] = EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[19] = EXCLUDEDFROMOUTOFNETWORKMOOP;

		/* New Benefit Attributes */

		dataList.add(columnHeaders);

		for (int i = 0; i < healthBenefit.size(); i++) {
			String[] benefits = new String[20];

			benefits[0] = getBenefitName(healthBenefit.get(i).getName(),
					benefitList);
			benefits[1] = healthBenefit.get(i).getNetworkT1CopayVal();
			benefits[2] = healthBenefit.get(i).getNetworkT1CopayAttr();
			benefits[3] = healthBenefit.get(i).getNetworkT1CoinsurVal();
			benefits[4] = healthBenefit.get(i).getNetworkT1CoinsurAttr();

			benefits[5] = healthBenefit.get(i).getNetworkT2CopayVal();
			benefits[6] = healthBenefit.get(i).getNetworkT2CopayAttr();
			benefits[7] = healthBenefit.get(i).getNetworkT2CoinsurVal();
			benefits[8] = healthBenefit.get(i).getNetworkT2CoinsurAttr();

			benefits[9] = healthBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[10] = healthBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[11] = healthBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[12] = healthBenefit.get(i).getOutOfNetworkCoinsurAttr();

			benefits[13] = healthBenefit.get(i).getNetworkLimitation();
			benefits[14] = healthBenefit.get(i).getNetworkLimitationAttribute();
			benefits[15] = healthBenefit.get(i).getNetworkExceptions();

			benefits[16] = healthBenefit.get(i)
					.getSubjectToInNetworkDuductible();
			benefits[17] = healthBenefit.get(i)
					.getSubjectToOutNetworkDeductible();
			benefits[18] = healthBenefit.get(i).getExcludedFromInNetworkMoop();
			benefits[19] = healthBenefit.get(i)
					.getExcludedFromOutOfNetworkMoop();

			dataList.add(benefits);
		}
		return dataList;
	}

	// for dynamic configuration
public static List<String[]> convertObjectListToStringListFromMap(List<PlanHealthBenefit> healthBenefit,Map<String, String> healthBenefitMap,  Plan plan, List<PlanHealthCost> planHealthCostData){
		
		
		
		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders1 = new String[3];
		String[] columnHeaders2 = new String[2];
		String[] columnHeaders3 = new String[2];
		String[] columnHeaders4 = new String[5];
		String[] columnHeaders5 = new String[11];
		String[] columnHeaders12 = new String[3];
		String[] columnHeaders13 = new String[3];
		String[] columnHeaders = new String[20];
		
		columnHeaders1[0]=PLANNAMEANDNUMBER;
		
		columnHeaders2[0]=PLANSTARTDATE;
		columnHeaders3[0]=PLANENDDATE;
		columnHeaders4[0] = EMPTYSTRING;
		columnHeaders4[1] = INNETWORKTIER1;
		columnHeaders4[2] = INNETWORKTIER2;
		columnHeaders4[3] = OUTOFNETWORK;
		columnHeaders4[4] = COMBINEDINOUTNETWORK;
		
		
		columnHeaders5[0] = DEDUCTIBLES;
		columnHeaders5[1] = INDIVIDUAL;
		columnHeaders5[2] = FAMILY;
		columnHeaders5[3] = DEFAULTCOINSURANCE;
		columnHeaders5[4] = INDIVIDUAL;
		columnHeaders5[5] = FAMILY;
		columnHeaders5[6] = DEFAULTCOINSURANCE;
		columnHeaders5[7] = INDIVIDUAL;
		columnHeaders5[8] = FAMILY;
		columnHeaders5[9] = INDIVIDUAL;
		columnHeaders5[10] = FAMILY;
		
		dataList.add(columnHeaders1);
		dataList.add(columnHeaders2);
		dataList.add(columnHeaders3);
		dataList.add(columnHeaders4);
		dataList.add(columnHeaders5);
		
		for(int i = 0; i < planHealthCostData.size(); i++){
			String[] planDentalCosts = new String[11];
			PlanHealthCost planHealthCost = (PlanHealthCost) planHealthCostData.get(i);
			
			planDentalCosts[0] = getBenefitNameFromMap(planHealthCost.getName(),healthBenefitMap);
			planDentalCosts[1] = (Double)planHealthCost.getInNetWorkInd()!= null?Double.toString(planHealthCost.getInNetWorkInd()):"";
			planDentalCosts[2] = (Double)planHealthCost.getInNetWorkFly()!= null?Double.toString(planHealthCost.getInNetWorkFly()):"";
			planDentalCosts[3] = (Double)planHealthCost.getCombDefCoinsNetworkTier1()!= null?Double.toString(planHealthCost.getCombDefCoinsNetworkTier1()):"";
			planDentalCosts[4] = (Double)planHealthCost.getInNetworkTier2Ind()!= null?Double.toString(planHealthCost.getInNetworkTier2Ind()):"";
			planDentalCosts[5] = (Double)planHealthCost.getInNetworkTier2Fly()!= null?Double.toString(planHealthCost.getInNetworkTier2Fly()):"";
			planDentalCosts[6] = (Double)planHealthCost.getCombDefCoinsNetworkTier2()!= null?Double.toString(planHealthCost.getCombDefCoinsNetworkTier2()):"";
			planDentalCosts[7] = (Double)planHealthCost.getOutNetworkInd()!= null?Double.toString(planHealthCost.getOutNetworkInd()):"";
			planDentalCosts[8] = (Double)planHealthCost.getOutNetworkFly()!= null?Double.toString(planHealthCost.getOutNetworkFly()):"";
			planDentalCosts[9] = (Double)planHealthCost.getCombinedInOutNetworkInd()!= null?Double.toString(planHealthCost.getCombinedInOutNetworkInd()):"";
			planDentalCosts[10] = (Double)planHealthCost.getCombinedInOutNetworkFly()!= null?Double.toString(planHealthCost.getCombinedInOutNetworkFly()):"";
			
			dataList.add(planDentalCosts);
		}
		columnHeaders[0]=BENEFIT_NAME;
		
		/*	Renamed Benefit Attributes*/
		
		columnHeaders[1]=NETWORKT1COPAYVAL;
		columnHeaders[2]=NETWORKT1COPAYATTR;
		columnHeaders[3]=NETWORKT1COINSURANCEVAL;
		columnHeaders[4]=NETWORKT1COINSURANCEATTR;
		columnHeaders[5]=SUBJECTTOINNETWORKDEDUCTIBLE;
		
		columnHeaders[6]=NETWORKT2COPAYVAL;
		columnHeaders[7]=NETWORKT2COPAYATTR;
		columnHeaders[8]=NETWORKT2COINSURANCEVAL;
		columnHeaders[9]=NETWORKT2COINSURANCEATTR;
		columnHeaders[10]=SUBJECTTOOUTOFNETWORKDEDUCTIBLE;

		columnHeaders[11]=EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[12]=OUTNETWORKCOPAYVAL;
		columnHeaders[13]=OUTNETWORKCOPAYATTR;
		columnHeaders[14]=OUTNETWORKCOINSURANCEVAL;
		columnHeaders[15]=OUTNETWORKCOINSURANCEATTR;
		columnHeaders[16]=EXCLUDEDFROMOUTOFNETWORKMOOP;

		columnHeaders[17]=LIMITATION;
		columnHeaders[18]=LIMITATIONATTRIBUTE;
		columnHeaders[19]=EXCEPTIONS;
		
		columnHeaders12[0] = EMPTYSTRING;
		columnHeaders12[1] = INNETWORK;
		columnHeaders12[2] = OUTOFNETWORK;
		
		columnHeaders13[0] = EMPTYSTRING;
		columnHeaders13[1] = TIER1;
		columnHeaders13[2] = TIER2;
		/*	Renamed Benefit Attributes*/
		
		
		
		
		/*	New Benefit Attributes	*/
		String[] plans = new String[2];
		columnHeaders1[1] = plan.getName();
		columnHeaders1[2] = plan.getIssuerPlanNumber();
		
		columnHeaders2[1] = getRequiredDateInFormat(String.valueOf(plan.getStartDate()));
		columnHeaders3[1] = getRequiredDateInFormat(String.valueOf(plan.getEndDate()));
		dataList.add(columnHeaders12);
		dataList.add(columnHeaders13);		
		dataList.add(columnHeaders);
		
		for (int i = 0; i < healthBenefit.size(); i++) {
			String[] benefits = new String[20];
						
			benefits[0] = getBenefitNameFromMap(healthBenefit.get(i).getName(),healthBenefitMap);
			benefits[1] = healthBenefit.get(i).getNetworkT1CopayVal();
			benefits[2] = healthBenefit.get(i).getNetworkT1CopayAttr();
			benefits[3] = healthBenefit.get(i).getNetworkT1CoinsurVal();			
			benefits[4] = healthBenefit.get(i).getNetworkT1CoinsurAttr();
			benefits[5] = healthBenefit.get(i).getSubjectToInNetworkDuductible();
			
			benefits[6] = healthBenefit.get(i).getNetworkT2CopayVal();
			benefits[7] = healthBenefit.get(i).getNetworkT2CopayAttr();
			benefits[8] = healthBenefit.get(i).getNetworkT2CoinsurVal();			
			benefits[9] = healthBenefit.get(i).getNetworkT2CoinsurAttr();
			benefits[10] = healthBenefit.get(i).getSubjectToOutNetworkDeductible();
			
			benefits[11] = healthBenefit.get(i).getExcludedFromInNetworkMoop();

			benefits[12] = healthBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[13] = healthBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[14] = healthBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[15] = healthBenefit.get(i).getOutOfNetworkCoinsurAttr();
			benefits[16] = healthBenefit.get(i).getExcludedFromOutOfNetworkMoop();
			
			benefits[17] = healthBenefit.get(i).getNetworkLimitation();
			benefits[18] = healthBenefit.get(i).getNetworkLimitationAttribute();
			benefits[19] = healthBenefit.get(i).getNetworkExceptions();
			
			/*benefits[16] = healthBenefit.get(i).getSubjectToInNetworkDuductible();5
			benefits[17] = healthBenefit.get(i).getSubjectToOutNetworkDeductible();10
			benefits[18] = healthBenefit.get(i).getExcludedFromInNetworkMoop();11
			benefits[19] = healthBenefit.get(i).getExcludedFromOutOfNetworkMoop();16*/
			
			
			/*columnHeaders[9]=OUTNETWORKCOPAYVAL;
			columnHeaders[10]=OUTNETWORKCOPAYATTR;
			columnHeaders[11]=OUTNETWORKCOINSURANCEVAL;
			columnHeaders[12]=OUTNETWORKCOINSURANCEATTR;*/

			/*columnHeaders[13]=LIMITATION;
			columnHeaders[14]=LIMITATIONATTRIBUTE;
			columnHeaders[15]=EXCEPTIONS;*/
			
			//columnHeaders[16]=SUBJECTTOINNETWORKDEDUCTIBLE;5
			//columnHeaders[17]=SUBJECTTOOUTOFNETWORKDEDUCTIBLE;10
			//columnHeaders[18]=EXCLUDEFROMINNETWORKMOOP;11
			//columnHeaders[19]=EXCLUDEDFROMOUTOFNETWORKMOOP;16
			dataList.add(benefits);
		}
		return dataList;
	}

	
	public static List<String[]> convertsQDPBenefit(
			List<PlanDentalBenefit> dentalBenefit,
			List<HierarchicalConfiguration> benefitList) {

		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders = new String[20];
		columnHeaders[0] = BENEFIT_NAME;

		/* Renamed Benefit Attributes */

		columnHeaders[1] = NETWORKT1COPAYVAL;
		columnHeaders[2] = NETWORKT1COPAYATTR;
		columnHeaders[3] = NETWORKT1COINSURANCEVAL;
		columnHeaders[4] = NETWORKT1COINSURANCEATTR;

		columnHeaders[5] = NETWORKT2COPAYVAL;
		columnHeaders[6] = NETWORKT2COPAYATTR;
		columnHeaders[7] = NETWORKT2COINSURANCEVAL;
		columnHeaders[8] = NETWORKT2COINSURANCEATTR;

		columnHeaders[9] = OUTNETWORKCOPAYVAL;
		columnHeaders[10] = OUTNETWORKCOPAYATTR;
		columnHeaders[11] = OUTNETWORKCOINSURANCEVAL;
		columnHeaders[12] = OUTNETWORKCOINSURANCEATTR;

		columnHeaders[13] = LIMITATION;
		columnHeaders[14] = LIMITATIONATTRIBUTE;
		columnHeaders[15] = EXCEPTIONS;

		columnHeaders[16] = SUBJECTTOINNETWORKDEDUCTIBLE;
		columnHeaders[17] = SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		columnHeaders[18] = EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[19] = EXCLUDEDFROMOUTOFNETWORKMOOP;

		/* New Benefit Attributes */

		dataList.add(columnHeaders);

		for (int i = 0; i < dentalBenefit.size(); i++) {
			String[] benefits = new String[20];

			benefits[0] = getBenefitName(dentalBenefit.get(i).getName(),
					benefitList);
			benefits[1] = dentalBenefit.get(i).getNetworkT1CopayVal();
			benefits[2] = dentalBenefit.get(i).getNetworkT1CopayAttr();
			benefits[3] = dentalBenefit.get(i).getNetworkT1CoinsurVal();
			benefits[4] = dentalBenefit.get(i).getNetworkT1CoinsurAttr();

			benefits[5] = dentalBenefit.get(i).getNetworkT2CopayVal();
			benefits[6] = dentalBenefit.get(i).getNetworkT2CopayAttr();
			benefits[7] = dentalBenefit.get(i).getNetworkT2CoinsurVal();
			benefits[8] = dentalBenefit.get(i).getNetworkT2CoinsurAttr();

			benefits[9] = dentalBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[10] = dentalBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[11] = dentalBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[12] = dentalBenefit.get(i).getOutOfNetworkCoinsurAttr();

			benefits[13] = dentalBenefit.get(i).getNetworkLimitation();
			benefits[14] = dentalBenefit.get(i).getNetworkLimitationAttribute();
			benefits[15] = dentalBenefit.get(i).getNetworkExceptions();

			benefits[16] = dentalBenefit.get(i)
					.getSubjectToInNetworkDuductible();
			benefits[17] = dentalBenefit.get(i)
					.getSubjectToOutNetworkDeductible();
			benefits[18] = dentalBenefit.get(i).getExcludedFromInNetworkMoop();
			benefits[19] = dentalBenefit.get(i)
					.getExcludedFromOutOfNetworkMoop();

			dataList.add(benefits);
		}
		return dataList;
	}

	// for dynamic configuration
public static List<String[]> convertsQDPBenefitFromMap(List<PlanDentalBenefit> dentalBenefit,Map<String, String> dentalBenefitMap, Plan plan, List<PlanDentalCost> planDentalCostData){
		
		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders1 = new String[3];
		String[] columnHeaders2 = new String[2];
		String[] columnHeaders3 = new String[2];
		String[] columnHeaders4 = new String[5];
		String[] columnHeaders5 = new String[11];
		String[] columnHeaders12 = new String[3];
		String[] columnHeaders13 = new String[3];
		String[] columnHeaders = new String[20];
		
		
		columnHeaders1[0]=PLANNAMEANDNUMBER;
		
		columnHeaders2[0]=PLANSTARTDATE;
		columnHeaders3[0]=PLANENDDATE;
		columnHeaders4[0] = EMPTYSTRING;
		columnHeaders4[1] = INNETWORKTIER1;
		columnHeaders4[2] = INNETWORKTIER2;
		columnHeaders4[3] = OUTOFNETWORK;
		columnHeaders4[4] = COMBINEDINOUTNETWORK;
		
		
		columnHeaders5[0] = DEDUCTIBLES;
		columnHeaders5[1] = INDIVIDUAL;
		columnHeaders5[2] = FAMILY;
		columnHeaders5[3] = DEFAULTCOINSURANCE;
		columnHeaders5[4] = INDIVIDUAL;
		columnHeaders5[5] = FAMILY;
		columnHeaders5[6] = DEFAULTCOINSURANCE;
		columnHeaders5[7] = INDIVIDUAL;
		columnHeaders5[8] = FAMILY;
		columnHeaders5[9] = INDIVIDUAL;
		columnHeaders5[10] = FAMILY;
		
		dataList.add(columnHeaders1);
		dataList.add(columnHeaders2);
		dataList.add(columnHeaders3);
		dataList.add(columnHeaders4);
		dataList.add(columnHeaders5);
		
		for(int i = 0; i < planDentalCostData.size(); i++){
			String[] planDentalCosts = new String[11];
			PlanDentalCost planDentalCost = (PlanDentalCost) planDentalCostData.get(i);
			
			planDentalCosts[0] = getBenefitNameFromMap(planDentalCost.getName(),dentalBenefitMap);
			planDentalCosts[1] = (Double)planDentalCost.getInNetWorkInd()!= null?Double.toString(planDentalCost.getInNetWorkInd()):"";
			planDentalCosts[2] = (Double)planDentalCost.getInNetWorkFly()!= null?Double.toString(planDentalCost.getInNetWorkFly()):"";
			planDentalCosts[3] = (Double)planDentalCost.getCombDefCoinsNetworkTier1()!= null?Double.toString(planDentalCost.getCombDefCoinsNetworkTier1()):"";
			planDentalCosts[4] = (Double)planDentalCost.getInNetworkTier2Ind()!= null?Double.toString(planDentalCost.getInNetworkTier2Ind()):"";
			planDentalCosts[5] = (Double)planDentalCost.getInNetworkTier2Fly()!= null?Double.toString(planDentalCost.getInNetworkTier2Fly()):"";
			planDentalCosts[6] = (Double)planDentalCost.getCombDefCoinsNetworkTier2()!= null?Double.toString(planDentalCost.getCombDefCoinsNetworkTier2()):"";
			planDentalCosts[7] = (Double)planDentalCost.getOutNetworkInd()!= null?Double.toString(planDentalCost.getOutNetworkInd()):"";
			planDentalCosts[8] = (Double)planDentalCost.getOutNetworkFly()!= null?Double.toString(planDentalCost.getOutNetworkFly()):"";
			planDentalCosts[9] = (Double)planDentalCost.getCombinedInOutNetworkInd()!= null?Double.toString(planDentalCost.getCombinedInOutNetworkInd()):"";
			planDentalCosts[10] = (Double)planDentalCost.getCombinedInOutNetworkFly()!= null?Double.toString(planDentalCost.getCombinedInOutNetworkFly()):"";
			
			
			dataList.add(planDentalCosts);
		}
		columnHeaders12[0] = EMPTYSTRING;
		columnHeaders12[1] = INNETWORK;
		columnHeaders12[2] = OUTOFNETWORK;
		
		columnHeaders[0]=BENEFIT_NAME;
		
		columnHeaders13[0] = EMPTYSTRING;
		columnHeaders13[1] = TIER1;
		columnHeaders13[2] = TIER2;
		/*	Renamed Benefit Attributes*/
		
		columnHeaders[1]=NETWORKT1COPAYVAL;
		columnHeaders[2]=NETWORKT1COPAYATTR;
		columnHeaders[3]=NETWORKT1COINSURANCEVAL;
		columnHeaders[4]=NETWORKT1COINSURANCEATTR;
		columnHeaders[5]=SUBJECTTOINNETWORKDEDUCTIBLE;

		columnHeaders[6]=NETWORKT2COPAYVAL;
		columnHeaders[7]=NETWORKT2COPAYATTR;
		columnHeaders[8]=NETWORKT2COINSURANCEVAL;
		columnHeaders[9]=NETWORKT2COINSURANCEATTR;
		columnHeaders[10]=SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		
		columnHeaders[11]=EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[12]=OUTNETWORKCOPAYVAL;
		columnHeaders[13]=OUTNETWORKCOPAYATTR;
		columnHeaders[14]=OUTNETWORKCOINSURANCEVAL;
		columnHeaders[15]=OUTNETWORKCOINSURANCEATTR;
		columnHeaders[16]=EXCLUDEDFROMOUTOFNETWORKMOOP;

		columnHeaders[17]=LIMITATION;
		columnHeaders[18]=LIMITATIONATTRIBUTE;
		columnHeaders[19]=EXCEPTIONS;
		
		/*columnHeaders[9]=OUTNETWORKCOPAYVAL;
		columnHeaders[10]=OUTNETWORKCOPAYATTR;
		columnHeaders[11]=OUTNETWORKCOINSURANCEVAL;
		columnHeaders[12]=OUTNETWORKCOINSURANCEATTR;*/

		/*columnHeaders[13]=LIMITATION;
		columnHeaders[14]=LIMITATIONATTRIBUTE;
		columnHeaders[15]=EXCEPTIONS;*/
		
		//columnHeaders[16]=SUBJECTTOINNETWORKDEDUCTIBLE;5
		//columnHeaders[17]=SUBJECTTOOUTOFNETWORKDEDUCTIBLE;10
		//columnHeaders[18]=EXCLUDEFROMINNETWORKMOOP;11
		//columnHeaders[19]=EXCLUDEDFROMOUTOFNETWORKMOOP;16
		
		/*	New Benefit Attributes	*/
		
		String[] plans = new String[2];
		columnHeaders1[1] = plan.getName();
		columnHeaders1[2] = plan.getIssuerPlanNumber();
		
		columnHeaders2[1] = getRequiredDateInFormat(String.valueOf(plan.getStartDate()));
		columnHeaders3[1] = getRequiredDateInFormat(String.valueOf(plan.getEndDate()));
		dataList.add(columnHeaders12);
		dataList.add(columnHeaders13);
		dataList.add(columnHeaders);
		
		for (int i = 0; i < dentalBenefit.size(); i++) {
			String[] benefits = new String[20];
						
			
			benefits[0] = getBenefitNameFromMap(dentalBenefit.get(i).getName(),dentalBenefitMap);
			benefits[1] = dentalBenefit.get(i).getNetworkT1CopayVal();
			benefits[2] = dentalBenefit.get(i).getNetworkT1CopayAttr();
			benefits[3] = dentalBenefit.get(i).getNetworkT1CoinsurVal();			
			benefits[4] = dentalBenefit.get(i).getNetworkT1CoinsurAttr();
			benefits[5] = dentalBenefit.get(i).getSubjectToInNetworkDuductible();
			
			benefits[6] = dentalBenefit.get(i).getNetworkT2CopayVal();
			benefits[7] = dentalBenefit.get(i).getNetworkT2CopayAttr();
			benefits[8] = dentalBenefit.get(i).getNetworkT2CoinsurVal();			
			benefits[9] = dentalBenefit.get(i).getNetworkT2CoinsurAttr();
			benefits[10] = dentalBenefit.get(i).getSubjectToOutNetworkDeductible();
			benefits[11] = dentalBenefit.get(i).getExcludedFromInNetworkMoop();

			benefits[12] = dentalBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[13] = dentalBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[14] = dentalBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[15] = dentalBenefit.get(i).getOutOfNetworkCoinsurAttr();
			benefits[16] = dentalBenefit.get(i).getExcludedFromOutOfNetworkMoop();
			
			benefits[17] = dentalBenefit.get(i).getNetworkLimitation();
			benefits[18] = dentalBenefit.get(i).getNetworkLimitationAttribute();
			benefits[19] = dentalBenefit.get(i).getNetworkExceptions();
			/*benefits[9] = dentalBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[10] = dentalBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[11] = dentalBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[12] = dentalBenefit.get(i).getOutOfNetworkCoinsurAttr();*/
			
			/*benefits[13] = dentalBenefit.get(i).getNetworkLimitation();
			benefits[14] = dentalBenefit.get(i).getNetworkLimitationAttribute();
			benefits[15] = dentalBenefit.get(i).getNetworkExceptions();*/
			
			//benefits[5] = dentalBenefit.get(i).getSubjectToInNetworkDuductible();
			//benefits[17] = dentalBenefit.get(i).getSubjectToOutNetworkDeductible();
			//benefits[18] = dentalBenefit.get(i).getExcludedFromInNetworkMoop();
			//benefits[19] = dentalBenefit.get(i).getExcludedFromOutOfNetworkMoop();
			
			dataList.add(benefits);
		}
		return dataList;
	}

	private static String getBenefitNameFromMap(String benefitDBConstant,
			Map<String, String> benefits) {

		String name = benefits.get(benefitDBConstant);

		if (name != null) {
			return name;
		} else {

			return benefitDBConstant;
		}
	}

	private static String getBenefitName(String benefitDBConstant,
			List<HierarchicalConfiguration> benefitList) {
		try {
			for (HierarchicalConfiguration sub : benefitList) {
				String ConstName = sub.getString("ConstName");
				if (ConstName.equalsIgnoreCase(benefitDBConstant)) {
					return sub.getString("Name");
				}
			}
		} catch (Exception e) {
			LOGGER.error("Benefit name not found ", e);
		}
		return benefitDBConstant;
	}

	public static List<String[]> getFileHeader() {
		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders = new String[20];

		columnHeaders[0] = BENEFIT_NAME;
		columnHeaders[1] = NETWORKT1COPAYVAL;
		columnHeaders[2] = NETWORKT1COPAYATTR;
		columnHeaders[3] = NETWORKT1COINSURANCEVAL;
		columnHeaders[4] = NETWORKT1COINSURANCEATTR;

		columnHeaders[5] = NETWORKT2COPAYVAL;
		columnHeaders[6] = NETWORKT2COPAYATTR;
		columnHeaders[7] = NETWORKT2COINSURANCEVAL;
		columnHeaders[8] = NETWORKT2COINSURANCEATTR;

		columnHeaders[9] = OUTNETWORKCOPAYVAL;
		columnHeaders[10] = OUTNETWORKCOPAYATTR;
		columnHeaders[11] = OUTNETWORKCOINSURANCEVAL;
		columnHeaders[12] = OUTNETWORKCOINSURANCEATTR;

		columnHeaders[13] = LIMITATION;
		columnHeaders[14] = LIMITATIONATTRIBUTE;
		columnHeaders[15] = EXCEPTIONS;

		columnHeaders[16] = SUBJECTTOINNETWORKDEDUCTIBLE;
		columnHeaders[17] = SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		columnHeaders[18] = EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[19] = EXCLUDEDFROMOUTOFNETWORKMOOP;

		dataList.add(columnHeaders);

		return dataList;
	}

	private static String getRequiredDateInFormat(String strDate){
		String requiredDate = null;
		if(strDate != null){
			requiredDate = DateUtil.changeFormat(strDate, DB_DATE_FORMAT, REQUIRED_DATE_FORMAT);
		}else{
			requiredDate = StringUtils.EMPTY;
		}
		return requiredDate;
	}
}
