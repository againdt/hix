(function() {
	"use strict";
	angular.module("communicationPreferencesCtrlApp",[])
	.config(CommunicationPreferencesConfig)
	.controller('CommunicationPreferencesCtrl', CommunicationPreferencesCtrl);
	
	CommunicationPreferencesConfig.$inject = ['$httpProvider'];
	CommunicationPreferencesCtrl.$inject = ['$window', '$http', 'isEditMode', 'statesService'];
	
	function CommunicationPreferencesConfig($httpProvider){
		//initialize get if not there
	    if (!$httpProvider.defaults.headers.get) {
	        $httpProvider.defaults.headers.get = {};    
	    }

	    //disable IE ajax request caching
	    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
	    // extra
	    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
	    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
	}
	
	function CommunicationPreferencesCtrl($window, $http, isEditMode, statesService){
		var vm = this;
		var prevPrefCommunication = null;
		
		vm.loading = true;
		
		vm.states = statesService;
		
		vm.getCounty = getCounty;
		vm.saveData = saveData;
		
		vm.isEditMode = isEditMode;
		vm.goToDashboard = goToDashboard;
		vm.countyList = [];
		vm.reportChange = reportChange;
		vm.showErStatusModal = showErStatusModal;
		vm.showOptOutNotificationsModal = showOptOutNotificationsModal;
		vm.showTextModal = showTextModal;
		
		_init();
		
		
		function _init(){
			var promise = $http.get('/hix/indportal/preferences?editMode=' + vm.isEditMode);
			
			promise.success(function(response) {
					
				vm.formData = response;		
				prevPrefCommunication = response.prefCommunication;
		
				if(vm.formData.phoneNumber !== null){
					vm.formData.phone1 = vm.formData.phoneNumber.substring(0, 3);
					vm.formData.phone2 = vm.formData.phoneNumber.substring(3, 6);
					vm.formData.phone3 = vm.formData.phoneNumber.substring(6, 10);
				}
				
				if(vm.formData.prefSpokenLang){
					if(vm.formData.prefSpokenLang.toUpperCase() === 'SPANISH'){
						vm.formData.prefSpokenLang = 'SP-SPA';
					}else if(vm.formData.prefSpokenLang.toUpperCase() === 'ENGLISH'){
						vm.formData.prefSpokenLang = 'SP-ENG';
					}
				}else{
					vm.formData.prefSpokenLang = 'SP-ENG';
				}
				
				
				if(vm.formData.prefWrittenLang){
					if(vm.formData.prefWrittenLang.toUpperCase() === 'SPANISH'){
						vm.formData.prefWrittenLang = 'SP-SPA';
					}else if(vm.formData.prefWrittenLang.toUpperCase() === 'ENGLISH'){
						vm.formData.prefWrittenLang = 'SP-ENG';
					}
				}else{
					vm.formData.prefWrittenLang = 'SP-ENG';
				}
				
				if(vm.formData.prefCommunication === null){
					vm.formData.prefCommunication = 'Email';
				}
				
				if(vm.formData.locationDto === null){
					vm.formData.locationDto = {
						state: undefined
					}
				}
				
				getCounty();
				
			}).then(function(){
				if((vm.mailingAddressUpdated === true && vm.contactPreferencesUpdated === false) || (vm.mailingAddressUpdated === false && vm.contactPreferencesUpdated === true)){
					$('#communicationPreferencesUpdateSuccessModal').modal();
				}
			});
			
			promise.error(function(response) {
				vm.loading = false;
			});
		}
		
		
		
		function getCounty(changedField){
			if(vm.formData.locationDto === null ){
				vm.loading = false;
				return;
			}else if(vm.formData.locationDto.zipcode === undefined || vm.formData.locationDto.zipcode.length !== 5 || !vm.formData.locationDto.state){
				vm.loading = false;
				vm.formData.locationDto.countyName = '';
				
				if(!vm.formData.locationDto.state && vm.formData.locationDto.state !== undefined){
					vm.addressValidationForm.state.$setValidity("validState", true);
				}
				return;
			}
			
			//var zipcode = 'zip=' + vm.formData.locationDto.zipcode;
			var data = 'zip=' + vm.formData.locationDto.zipcode + "&stateCode=" + vm.formData.locationDto.state 
			
			var config = {
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
			};

			var promise = $http.post('/hix/iex/validateZip', data, config);
			
			promise.success(function(response) {
				vm.loading = false;
				
				vm.countyList = response;
				
				if(vm.countyList === ''){
					if(changedField === 'zipCode'){
						vm.addressValidationForm.zipcode.$setValidity("validZipCode", false);
					}else{
						vm.addressValidationForm.state.$setValidity("validState", false);
					}
					
				}else{
					
					if(vm.addressValidationForm && vm.addressValidationForm.zipcode){
						vm.addressValidationForm.zipcode.$setValidity("validZipCode", true);
						vm.addressValidationForm.state.$setValidity("validState", true);
					}

					
					if(Object.keys(vm.countyList).length === 1){
						vm.formData.locationDto.countyName = Object.keys(vm.countyList)[0];
					}
				}

			});
			
			promise.error(function(data) {
				vm.loading = false;
				
				alert('Unable to get county');
			});
		}
		
		
		function saveData() {
			
			vm.loading = true;

			var data = {
				locationDto: vm.formData.locationDto,
				dob: vm.formData.dob,
				phoneNumber: "" + vm.formData.phone1 + vm.formData.phone2 + vm.formData.phone3,
				prefCommunication: vm.formData.prefCommunication,
				prefSpokenLang: vm.formData.prefSpokenLang,
				prefWrittenLang: vm.formData.prefWrittenLang,
				hasActiveEnrollment: vm.formData.hasActiveEnrollment,
				mailingAddressUpdated: vm.mailingAddressUpdated,
				contactPreferencesUpdated: vm.contactPreferencesUpdated,
				prevPrefCommunication: vm.isEditMode && prevPrefCommunication !== vm.formData.prefCommunication? prevPrefCommunication: null,
				editMode: vm.isEditMode,
				textMe: vm.formData.textMe,
				optInPaperless1095: vm.formData.optInPaperless1095
			};
			
			angular.forEach(vm.countyList, function(value, key){
				if(key === data.locationDto.countyName){
					data.locationDto.countyCode = value.countyCode;
				}
			});
			

			var promise = $http.post('/hix/indportal/preferences', angular.toJson(data));
			
			promise.success(function(response) {
				if (response.toUpperCase() === 'SUCCESS') {
					if(!vm.isEditMode){
						//don't need to set loading to false
						goToDashboard();
					}else{
						_init();				
					}
				} else if (response.toUpperCase() === 'FAILURE') {
					vm.loading = false;
					alert('Unable to save Communication Preferences');
				}

			});
			
			promise.error(function(data) {
				vm.loading = false;
				alert('Unable to save Communication Preferences');
			});
		};
		
		
		function goToDashboard(){
			location.href = "/hix/indportal";
		}
		
		
		function reportChange(){
			if ($("#stateCode").val() === 'MN') {
				$window.open('https://www.mnsure.org/current-customers/manage-account/report-change/private.jsp', '_blank')
			} else if(vm.formData.nonFinancial === true){
				location.href='./iex/lce/reportyourchange?coverageYear=' + vm.formData.activeEnrollmentYear + '&mau=Y';
			}else{
				$('#reportAChangeModal').modal();
			}
		}
		
		function showErStatusModal(){
			$('#erStatusModal').modal();
		}
		
		
		function showOptOutNotificationsModal(){
			$('#optOutNotificationsModal').modal();
		}
		
		function showTextModal() {
			if(vm.formData.textMe) {
				$('#textMeModal').modal();
			}
		}
	}
})();