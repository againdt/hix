package com.getinsured.hix.provider.search;

import java.util.List;

import com.getinsured.hix.provider.service.ManageFacilitiesDTO;
import com.getinsured.hix.provider.service.ManageProvidersDTO;
import com.getinsured.hix.provider.service.zipcode.ZipCode;

public interface SolrProxy {

	String findProviders(String searchKey, ZipCode userZip, int pageSize, int start, String lanugage, String gender, float distance,String providerType, String searchType, String providerSpecialty, String networkId);
	
	String findProvidersByCity(String searchKey, String userCity, int pageSize, int start, String lanugage, String gender,String providerType, String searchType, String providerSpecialty);

	String findProvidersDetails(String groupKey, ZipCode userLocation);

	long getProviderDataCount(boolean hasProviderType, String nameFilter, String facilityFilter, String specialityFilter, String networkIdFilter);

	List<ManageProvidersDTO> getProviderDataList(String nameFilter, String networkIdFilter, String specialityFilter, int page, int size);

	List<ManageFacilitiesDTO> getFacilityDataList(String facilityFilter, String specialityFilter, String networkIdFilter, int page, int size);
}
