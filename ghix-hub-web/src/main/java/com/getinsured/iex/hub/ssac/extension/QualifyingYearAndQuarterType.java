
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * Qualifying Year and Quarter
 *             
 * 
 * <p>Java class for QualifyingYearAndQuarterType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QualifyingYearAndQuarterType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}QualifyingYear"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}QualifyingQuarter"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QualifyingYearAndQuarterType", propOrder = {
    "qualifyingYear",
    "qualifyingQuarter"
})
public class QualifyingYearAndQuarterType
    extends ComplexObjectType
{

    @XmlElement(name = "QualifyingYear", required = true)
    protected QuarterYearType qualifyingYear;
    @XmlElement(name = "QualifyingQuarter")
    protected int qualifyingQuarter;

    /**
     * Gets the value of the qualifyingYear property.
     * 
     * @return
     *     possible object is
     *     {@link QuarterYearType }
     *     
     */
    public QuarterYearType getQualifyingYear() {
        return qualifyingYear;
    }

    /**
     * Sets the value of the qualifyingYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuarterYearType }
     *     
     */
    public void setQualifyingYear(QuarterYearType value) {
        this.qualifyingYear = value;
    }

    /**
     * Gets the value of the qualifyingQuarter property.
     * 
     */
    public int getQualifyingQuarter() {
        return qualifyingQuarter;
    }

    /**
     * Sets the value of the qualifyingQuarter property.
     * 
     */
    public void setQualifyingQuarter(int value) {
        this.qualifyingQuarter = value;
    }

}
