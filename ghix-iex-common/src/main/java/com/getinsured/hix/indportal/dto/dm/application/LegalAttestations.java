package com.getinsured.hix.indportal.dto.dm.application;

public class LegalAttestations {
	 private Boolean absentParentAgreementIndicator;
	 private Boolean changeInformationAgreementIndicator;
	 private Boolean medicaidRequirementAgreementIndicator;
	 private Integer renewEligibilityYearQuantity;
	 private Boolean nonIncarcerationAgreementIndicator;
	 private Boolean penaltyOfPerjuryAgreementIndicator;
	 private Boolean renewalAgreementIndicator;
	 private Boolean terminateCoverageOtherMecFoundAgreementIndicator;


	 // Getter Methods 

	 public Boolean getAbsentParentAgreementIndicator() {
	  return absentParentAgreementIndicator;
	 }

	 public Boolean getChangeInformationAgreementIndicator() {
	  return changeInformationAgreementIndicator;
	 }

	 public Boolean getMedicaidRequirementAgreementIndicator() {
	  return medicaidRequirementAgreementIndicator;
	 }

	 public Integer getRenewEligibilityYearQuantity() {
	  return renewEligibilityYearQuantity;
	 }

	 public Boolean getNonIncarcerationAgreementIndicator() {
	  return nonIncarcerationAgreementIndicator;
	 }

	 public Boolean getPenaltyOfPerjuryAgreementIndicator() {
	  return penaltyOfPerjuryAgreementIndicator;
	 }

	 public Boolean getRenewalAgreementIndicator() {
	  return renewalAgreementIndicator;
	 }

	 public Boolean getTerminateCoverageOtherMecFoundAgreementIndicator() {
	  return terminateCoverageOtherMecFoundAgreementIndicator;
	 }

	 // Setter Methods 

	 public void setAbsentParentAgreementIndicator(Boolean absentParentAgreementIndicator) {
	  this.absentParentAgreementIndicator = absentParentAgreementIndicator;
	 }

	 public void setChangeInformationAgreementIndicator(Boolean changeInformationAgreementIndicator) {
	  this.changeInformationAgreementIndicator = changeInformationAgreementIndicator;
	 }

	 public void setMedicaidRequirementAgreementIndicator(Boolean medicaidRequirementAgreementIndicator) {
	  this.medicaidRequirementAgreementIndicator = medicaidRequirementAgreementIndicator;
	 }

	 public void setRenewEligibilityYearQuantity(Integer renewEligibilityYearQuantity) {
	  this.renewEligibilityYearQuantity = renewEligibilityYearQuantity;
	 }

	 public void setNonIncarcerationAgreementIndicator(Boolean nonIncarcerationAgreementIndicator) {
	  this.nonIncarcerationAgreementIndicator = nonIncarcerationAgreementIndicator;
	 }

	 public void setPenaltyOfPerjuryAgreementIndicator(Boolean penaltyOfPerjuryAgreementIndicator) {
	  this.penaltyOfPerjuryAgreementIndicator = penaltyOfPerjuryAgreementIndicator;
	 }

	 public void setRenewalAgreementIndicator(Boolean renewalAgreementIndicator) {
	  this.renewalAgreementIndicator = renewalAgreementIndicator;
	 }

	 public void setTerminateCoverageOtherMecFoundAgreementIndicator(Boolean terminateCoverageOtherMecFoundAgreementIndicator) {
	  this.terminateCoverageOtherMecFoundAgreementIndicator = terminateCoverageOtherMecFoundAgreementIndicator;
	 }
	}
