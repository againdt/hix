import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import INPUT_TYPES from '../../constants/inputTypes';



const Page2_25 = props => {

    const {
        data,
        isFirstVisit,
        householdMembers
    } = props;

    const [ noneOfAbove, toggleNoneOfAbove ] = useState(isFirstVisit ?
        null :
        householdMembers.every(hhm =>
            !hhm
                .citizenshipImmigrationStatus
                .honorablyDischargedOrActiveDutyMilitaryMemberIndicator));

    const onFieldChange = (...args) => {

        const [ , indexInArray, , [  name, value ]  ] = args;

        const newState = { ...data };

        switch(true) {
        case name.startsWith('hhm'):
            newState.singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[indexInArray]
                .citizenshipImmigrationStatus
                .honorablyDischargedOrActiveDutyMilitaryMemberIndicator = value;
            if(value) {
                toggleNoneOfAbove(false);
            }
            break;

        case name === 'noneOfTheAbove':
            if(value) {
                newState.singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember = householdMembers.map(hhm => ({
                        ...hhm,
                        citizenshipImmigrationStatus: {
                            ...hhm.citizenshipImmigrationStatus,
                            honorablyDischargedOrActiveDutyMilitaryMemberIndicator: false
                        }
                    }));
                toggleNoneOfAbove(true);
            }
            break;
        }

        props.actions.changeData(newState);
    };

    const getContent = () => {

        const hhmsInputs = householdMembers.map((hhm, idx) => {
            const {
                citizenshipImmigrationStatus: {
                    honorablyDischargedOrActiveDutyMilitaryMemberIndicator: isMilitary
                },
                name,
                personId,
            } = hhm;

            return (
                <UniversalInput
                    key={personId}
                    label={{
                        text: normalizeNameOfPerson(name),
                        isLabelRequired: false
                    }}
                    inputData={{
                        isInputRequired: false,
                        type: INPUT_TYPES.CHECKBOX,
                        name: `hhm_${idx}`,
                        checked: isMilitary,
                        value: isMilitary,
                        currentData: {},
                        fields: [],
                    }}
                    inputActions={{
                        onFieldChange
                    }}
                    indexInArray={idx}
                />
            );
        });

        const noneOfAboveInput = (
            <UniversalInput
                key={'None_of_above'}
                label={{
                    text: 'None of the Above',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: 'noneOfTheAbove',
                    checked: noneOfAbove,
                    value: noneOfAbove,
                    currentData: {},
                    fields: [],
                }}
                inputActions={{
                    onFieldChange
                }}


            />
        );
        return [
            ...hhmsInputs,
            noneOfAboveInput
        ];
    };

    return (
        <div id="fhMilitaryDuty" className="fade-in">
            <div className="subsection">
                <p>
                    Are any of these people honorably discharged veteran or active duty member of the military?
                </p>
                <Form>
                    { getContent() }
                </Form>
            </div>
        </div>
    );
};


const mapStateToProps = state => ({

    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember

});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_25.defaultProps = {
    name: 'Page2_25'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_25);
