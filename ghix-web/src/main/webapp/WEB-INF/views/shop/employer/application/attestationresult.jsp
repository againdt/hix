<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>



<script type="text/javascript">
 
function validateForm(){
	if(isNaN($("#ein").val())){
		alert("<spring:message code='label.validateeindigit' javaScriptEscape='true'/>");
		return false;
	}
	if($("#ein").val().length != 9){
		alert("<spring:message code='label.validateeindigit' javaScriptEscape='true'/>");
		return false;
	}
	
	$("#frmEmpAttestRslt").submit();
     return true;
}

$(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
});

</script>

<div class="row-fluid">
		<div class="gutter10">
			<!--<ul class="page-breadcrumb">
            <li><a href="#">&lt; Back</a></li>
            <li><a href="#">Account</a></li>
            <li><a href="#">Manage Account Information</a></li>
            </ul>
        page-breadcrumb ends-->
			<h1>${employer.name}</h1>
			<div class="nmhide">
				<p style="color: red; float:right;" ><c:out value="${generateactivationlinkInfo}"></c:out><c:out value="${empEligibilityNotificationinfo}"></c:out><p/>
			</div>
		</div>
	</div> 
		<div class="row-fluid">
			<!-- add id for skip side bar -->
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="label.emprattesttxt1"/></h4>
                </div>
                 <ul class="nav nav-list">
                        <li ><a href="<c:url value="${companyInfoPage}" />"><spring:message  code="label.emprattesttxt2"/> </a></li>
                        <li class="active"><a href="#"><spring:message  code="label.emprattesttxt3"/></a></li>
                </ul>
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
				<div class="mshide">
					<p style="color: red;" ><c:out value="${generateactivationlinkInfo}"></c:out><c:out value="${empEligibilityNotificationinfo}"></c:out><p/>
				</div>
            	<div class="header">
                    <h4><spring:message  code="label.eligibilityresults"/>
                     <c:if test="${eStatus == 'CONDITIONALLY_APPROVED'}">
	                     <input type="button" name="save" id="save" value="<spring:message  code="label.save"/>" onClick="javascript:validateForm();" class="btn btn-primary btn-small pull-right marginL5 margin5-r">
	                     <input type="button" name="cancel" id="cancel" value="<spring:message  code="label.cancel"/>" onClick="javascript:window.history.back();" class="btn btn-small pull-right">
                     </c:if>
    		        </h4>
                     
                </div>
              <c:if test="${eStatus == 'CONDITIONALLY_APPROVED'}">
			  	<form class="pull-right paddingTB6" id="frmEmpAttestRslt" name="frmEmpAttestRslt" action="attestationresult" method="POST">
				<df:csrfToken/>
			  </c:if>	
				<div class="gutter10">

					<div class="profile">
                        <div class="header"><h4 class="pull-left"><spring:message code="label.results"/></div>
                        	<c:if test="${eStatus == 'EXPIRED' }">
		                        <table class="table table-border-none">
		                            <tr>
		                                <td class="txt-right span4"><spring:message  code="label.eligibilitystatus"/></td>
		                                <td><strong><spring:message  code="label.expired"/> ${statusDate}</strong></td>
		                            </tr>
								</table>
							</c:if>
	                        <c:if test="${eStatus == 'DENIED' }">
	                        <table class="table table-border-none">
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.eligibilitystatus"/></td>
	                                <td><strong><spring:message  code="label.denied"/> ${statusDate}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.eligibilitydecisionfactors"/></td>
	                                <td><strong>${statusNote}</strong></td>
	                            </tr>
	                            <c:if test="${isAppealPresent == false}">
		                              <tr>
		                               <td class="txt-right span4">&nbsp;</td>
		                              	  <td><a class="btn btn-small" href="<c:url value='/shop/employer/appeal/create?no_of_appeals=${totalNoOfAppeals}'  />"  ><spring:message  code="label.appeal"/></a> </td>
		                              </tr>    
	                              </c:if>
							</table>
							</c:if>
						<c:if test="${eStatus == 'APPROVED'}">
                        <table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.eligibilitystatus"/></td>
                                <td><strong><spring:message  code="label.approved"/> ${statusDate}</strong></td>
                            </tr>
						</table>
						</c:if>
						 <c:if test="${eStatus == 'CONDITIONALLY_APPROVED'}">
                        <table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.eligibilitystatus"/></td>
                                <td><strong>${statusNote}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right span4">EIN <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></td>
                                <td>
                                	<input type="text" name="ein" id="ein" size="10"  maxlength="9"/>
                                	<a target="_blank" href="http://www.irs.gov/Businesses/Small-Businesses-&-Self-Employed/Apply-for-an-Employer-Identification-Number-(EIN)-Online" ><spring:message  code="label.getnewein"/></a>
                                </td>
                            </tr>
                                 
						</table>
						</c:if>
					</div>
					
					
					<br />	
					
					<div class="profile">
                        <div class="header">
                            <h4 class="pull-left"><spring:message  code="label.emprattesttxt4"/></h4>
                        </div>
                        <div class="gutter10">
                        <table class="table table-border-none">
							<tr>
								<td class="txt-right span4"><spring:message  code="label.businessname"/></td>
								<td><strong>${employer.name}</strong></td>
							</tr>
							<tr>
								<td class="txt-right">EIN</td>
								<td><strong>${employer.federalEIN}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.emprattestcorptype"/></td>
								<td><strong>${orgType}</strong>
								</td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.emprfultimeemp"/></td>
								<td><strong>${employer.fullTimeEmp}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.emprpriworksiteaddr"/></td>
								<td><strong>${primaryWkSite}</strong></td>
							</tr>
							<c:forEach items="${additionalWkSites}" var="additionalWkSite" varStatus="theCount">
								<tr>
									<td class="txt-right"><spring:message  code="label.employeradditionalworksite"/> ${theCount.count}</td>
									<td><strong>${additionalWkSite}</strong></td>
								</tr>
							</c:forEach>
						</table>
						</div>
					</div>	
					
					<div class="profile">
						
                        <div class="header">
                            <h4 class="pull-left"><spring:message  code="label.rightnrespons"/></h4>
                        </div>
                        <div class="gutter10">
                        <p>
                        <c:choose>
                        <c:when test="${eStatus == 'DENIED' || eStatus == 'CONDITIONALLY_APPROVED' || eStatus == 'APPROVED'}">
                        <ul>
							<li><spring:message  code="label.righttoappeal1"/></li>
							<li><spring:message  code="label.righttoappeal2"/> ${exchangeName}  <spring:message  code="label.righttoappeal3"/></li>
							<li><spring:message  code="label.righttoappeal4"/>&nbsp;${maxAppealDays}&nbsp;<spring:message  code="label.righttoappealDays"/></li>
							<li><spring:message  code="label.righttoappeal5"/></li>
						</ul>
						</c:when>
						<c:otherwise>
						<spring:message  code="label.righttoappeal"/>
						</c:otherwise>
						</c:choose>
                        </p>
					</div>
					</div>	
					</div>
				<c:if test="${eStatus == 'CONDITIONALLY_APPROVED'}">	
					</form>
				</c:if>
				<c:if test="${eStatus == 'DENIED' || eStatus == 'CONDITIONALLY_APPROVED' ||eStatus == 'APPROVED' ||  eStatus == 'EXPIRED'}">
				<div align="center">
					<input type="button" name="Done" id="Done" value="<spring:message  code="label.donebtn"/>" onClick="javascript:location.href='<c:url value="${doneBtnUrl}"/>'" class="btn btn-primary marginL5">
				</div>
				</c:if>
				
			</div>
		</div><!--  end of span9 -->
