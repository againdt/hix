package com.getinsured.hix.batch.enrollment.external.nv.writer;

import java.io.File;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.eligibility.ssap.integration.at.client.service.AccountTransferInboundSoapWebClient;
import com.getinsured.hix.indportal.dto.dm.application.result.ApplicationData;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;

public class ApplicationItemWriter implements ItemWriter<ApplicationData>, ApplicationContextAware  {
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationItemWriter.class);

	//private StepContext stepContext;
	private String fileName;

	private String jobId;
	private static ApplicationContext applicationContext;
	private static JAXBContext jc = null;
	private static Marshaller marshaller = null;
	static {
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.erp.gov.cms.dsh.at.extension._1");
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);

		} catch (JAXBException e) {
			LOGGER.error("Error while initializing JAXBContext", e);
		}

	}
	private AccountTransferInboundSoapWebClient accountTransferSoapWebClient;
	private static final String ATSoapClientBeanName = "accountTransferInboundSoapWebClient";
	
    public void write(List<? extends ApplicationData> items) throws Exception { 
    	LOGGER.debug("ApplicationItemWriter starts"); 
        for(ApplicationData data : items)
		{
        	if(StringUtils.isEmpty(data.getErrorMessage()))
        	{
				StringWriter responseString = new StringWriter();
				AccountTransferResponsePayloadType atResponse = null;
				StringBuilder errorResponse = new StringBuilder();
				accountTransferSoapWebClient = (AccountTransferInboundSoapWebClient) applicationContext.getBean(ATSoapClientBeanName);
				String status = "";
				try {
					if(data.getRequestObj() != null) {
					atResponse = accountTransferSoapWebClient.send(data.getRequestObj());
						if (atResponse != null) {
							marshaller.marshal(atResponse, responseString);
						}
						if("HS000000".equalsIgnoreCase(atResponse.getResponseMetadata().getResponseCode().getValue()))
						{
							status = "SUCCESS";
							data.setErrorMessage("SUCCESS");
						}
						else	
						{
							data.setResponseXML(responseString.toString().replaceAll(System.lineSeparator(), " "));
						}
					}
					updateErrorFiles(data,status);
				} catch (WebServiceIOException wsIOE) {
					// lOGGER.error(wsIOE.getMessage());
					errorResponse.append("Error calling Account Transfer Endpoint:").append(wsIOE.getMessage());
					// return this.formResponse(AccountTransferConstants.ERROR_CODE_ONE,
					// errorResponse.toString());
				} catch (SoapFaultClientException sfce) {
					// lOGGER.error(sfce.getMessage());
					errorResponse.append("Error calling Account Transfer Endpoint:").append(sfce.getMessage());
					// return this.formResponse(AccountTransferConstants.ERROR_CODE_TWO,
					// errorResponse.toString());
				}
				if(!StringUtils.isEmpty(errorResponse.toString()))
				{
					data.setErrorMessage(data.getErrorMessage()+errorResponse.toString());
					updateErrorFiles(data,"FAILED");
				}
        	}
        	else {
        		updateErrorFiles(data,"FAILED");
        	}
		}
        LOGGER.debug("ExternalEnrollmeApplicationItemWriterntItemWriter ends"); 
    }

    private void updateErrorFiles(ApplicationData data,String status) {
		if(!"SUCCESS".equalsIgnoreCase(status))
		{
			updateFailedJson(data.getJsonSource());
			updateRequestXML(data);
		}
		String requestId = "";
		
		if(data.getRequestObj() != null) {
			requestId = data.getRequestObj().getInsuranceApplication().getApplicationIdentification().get(0).getIdentificationID().getValue();
		}
		if(!StringUtils.isEmpty(data.getErrorMessage()))
		{
			updateErrorDetails(requestId+","+data.getErrorMessage());
		}
		else
		{
			updateErrorDetails(requestId+","+data.getResponseXML());
		}
		
	}
    private void updateErrorDetails(String errorMessage) {
    	try {
    		
		    Path path = Paths.get(fileName);
		    String absolutePath = path.getParent().toString()+File.separator+jobId+"_failedRecords_descr.csv";
		    path = Paths.get(absolutePath);
		    Files.write(path, (errorMessage + System.lineSeparator()).getBytes(StandardCharsets.UTF_8), Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		    //ite(path, errorList, StandardCharsets.UTF_8,Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final Exception e) {
			LOGGER.error("Error while writing the data for errorDescrFile : ",e);
			LOGGER.error("--- Errorlist print starts ---");
			LOGGER.error(errorMessage);
			LOGGER.error("--- Errorlist print ends ---");
		}
	}

	private void updateRequestXML(ApplicationData data) {
		try {
			StringWriter requestXMLStr = new StringWriter();
			if (data.getRequestObj() != null) {
				marshaller.marshal(data.getRequestObj(), requestXMLStr);
			}
		    Path path = Paths.get(fileName);
		    String absolutePath = path.getParent().toString()+File.separator+jobId+"_failedRequest.txt";
		    path = Paths.get(absolutePath);
		    Files.write(path, (requestXMLStr.toString()+ System.lineSeparator()).getBytes(StandardCharsets.UTF_8), Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		    //ite(path, errorList, StandardCharsets.UTF_8,Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final Exception e) {
			LOGGER.error("Error while writing the data for errorDescrFile : ",e);
		}
	}

	private void updateFailedJson(String errorList ) {
		try {
		    Path path = Paths.get(fileName);
		    String absolutePath = path.getParent().toString()+File.separator+jobId+"_failedJson.txt";
		    path = Paths.get(absolutePath);
		    Files.write(path, (errorList+ System.lineSeparator()).getBytes(StandardCharsets.UTF_8), Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		    //ite(path, errorList, StandardCharsets.UTF_8,Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final Exception e) {
			LOGGER.error("Error while writing the data for errorDescrFile : ",e);
			LOGGER.error("--- Errorlist print starts ---");
			LOGGER.error(errorList);
			LOGGER.error("--- Errorlist print ends ---");
		}
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		applicationContext = context;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
}
