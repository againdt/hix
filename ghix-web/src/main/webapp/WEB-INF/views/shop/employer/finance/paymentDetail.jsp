<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/datepicker.css"/>" />

<div class="gutter10">

<div class="row-fluid">
<ul class="breadcrumb txt-left">
	<li><a href="<c:url value="/shop/employer/" />">Employer Portal </a> <span class="divider">/</span></li>
	<li><a href="<c:url value="/shop/employer/viewpayment" />">Payments </a> <span class="divider">/</span></li>


	<li class="active">Add Payment Method</li>
</ul>
			<h1>Add Payment Method <a tabindex="-1" href="#" rel="tooltip" data-placement="top"
			data-original-title="You can add a new method of payment based on the different options supported 
			by the system. Based on your selection of preferred payment method, you would need to specify 
			additional details like bank account, credit/debit card or billing address information." 
			class="info"><i class="icon-question-sign"></i></a></h1>

</div>
<!--titlebar-->

<div class="row-fluid">
		<div id="sidebar" class="span3">
	<h4 class="graydrkbg"> Steps</h4>
			<ul class="nav nav-list">
				<li><a href="#">Preferred Payment Method</a></li>
				<li  class="active"><a href="#">Payment Method Details</a></li>
		  </ul>
	</div>
	<!--sidebar-->

	<div id="rightpanel" class="span9 pull-right">
	<h4 class="graydrkbg gutter10">Payment Method Details</h4>

		<div class="gutter10">

			<!-- start: this only shows when you come from 'edit payment' -->
			<!--<form class="form-horizontal">
				<df:csrfToken/>
			    <div class="control-group">
					<label class="control-label" for="">Payment Method Name</label>
					<div class="controls">
						<input type="text" id="paymentmethod" placeholder="Chase Visa card">
					</div>
				</div>
			</form>-->
			<!-- end: this only shows when you come from 'edit payment' with respective field of which account the user is in -->

			<form class="form-horizontal gutter10" id="fromaddpayment"
				name="fromaddpayment" action="savepayment" method="POST">
				<df:csrfToken/>
				<input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="moduleId"
				id="employerid" />
			
				<input type="hidden" name="moduleName" id="moduleName"
				<c:if test="${paymentdetail.moduleName != null}">
				value = "<c:out value="${paymentdetail.moduleName}"/>"</c:if>
				<c:if test="${moduleName != null}">
				value = "<c:out value="${moduleName}"/>"</c:if>>
							
				<input type="hidden" value="<encryptor:enc value = "${paymentdetail.id}"/>"
				name="id" id="paymentid" /> <input type="hidden"
				value="${paymentdetail.paymentType}" name="existingpaymenttype"
				id="existingpaymenttype" />
				 <input type="hidden"
				value="${paymenttype}" name="paymentType"
				id="paymentType" />
				<input type="hidden" value="<encryptor:enc value = "${paymentdetail.financialInfo.id}"/>" name="financialInfo.id" />
				<input type="hidden" value="${paymentdetail.financialInfo.created}" name="financeInfoCreated" />
				<input type="hidden" value="${paymentdetail.createdOn}" name="paymentMethodCreated" />
				<div class="control-group">
					<label class="control-label required" for="paymentMethodName">Payment Method Name <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" class="input-xlarge" id="paymentMethodName"  name="paymentMethodName"
						 value="${paymentdetail.paymentMethodName}" placeholder="Payment Method Name">
						<div id="paymentMethodName_error"></div>
					</div>
				</div>
				<div class="" id="directBank" style="display: none">
					<h4 class="lightgray">Bank Account Detail for Direct Bank Withdrawal</h4>
					<div class="control-group">
					</div>
					<div class="control-group">
						<label for="routingNumber" class="control-label required">Routing
							Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> 
							<a tabindex="-1" class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover"  href="#"> <i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="text" name="financialInfo.bankInfo.routingNumber"
								value="${paymentdetail.financialInfo.bankInfo.routingNumber}"
								id="routingNumber" class="input-xlarge" maxlength="9" size="30" placeholder="9-digit ABA routing number">
							<div id="routingNumber_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="accountNumber" class="control-label required">Account
							Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> 
							<a tabindex="-1" class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover"  href="#"> <i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="text" name="financialInfo.bankInfo.accountNumber"
								value="${paymentdetail.financialInfo.bankInfo.accountNumber}"
								id="accountNumber" class="input-xlarge" maxlength="18" size="30" >
								<c:if test="${flow != null && flow == 'edit'}">
								<input type="button" class="btn btn-primary" name="editAccountNo" value="Click To Edit" onclick="javascript:clearAccountNoField();"/>
								</c:if>
							<div id="accountNumber_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="nameOnAccount" class="control-label required">Name
							On Account <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.bankInfo.nameOnAccount"
								value="${paymentdetail.financialInfo.bankInfo.nameOnAccount}"
								id="nameOnAccount" maxlength="20" class="input-xlarge" size="30">
							<div id="nameOnAccount_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="accountType" class="control-label required">Account
							Type <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select id="accountType"
								name="financialInfo.bankInfo.accountType">
								<option value="">Select</option>
								<option
									<c:if test="${'C' == paymentdetail.financialInfo.bankInfo.accountType}"> SELECTED </c:if>
									value="C">Checking</option>
								<option
									<c:if test="${'S' == paymentdetail.financialInfo.bankInfo.accountType}"> SELECTED </c:if>
									value="S">Saving</option>
							</select>
							<div id="accountType_error"></div>
						</div>
						<!-- end of controls-->
					</div>
				</div>
				<!-- profile -->
				<div class="" id="debitCredit" style="display: none">
				<h4 class="lightgray">Credit/Debit Card Detail</h4>
					<div class="control-group">
						
					</div>
					<div class="control-group">
						<label class="control-label required">Card Type <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select id="cardType"
								name="financialInfo.creditCardInfo.cardType">
								<option value="">Select</option>
								<option
									<c:if test="${'mastercard' == paymentdetail.financialInfo.creditCardInfo.cardType}"> SELECTED </c:if>
									value="mastercard">Master Card</option>
								<option
									<c:if test="${'visa' == paymentdetail.financialInfo.creditCardInfo.cardType}"> SELECTED </c:if>
									value="visa">Visa</option>
								<option
									<c:if test="${'americanexpress' == paymentdetail.financialInfo.creditCardInfo.cardType}"> SELECTED </c:if>
									value="americanexpress">American Express</option>
							</select>
							<div id="cardType_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="cardNumber" class="control-label required">Card
							Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.creditCardInfo.cardNumber"
								value="${paymentdetail.financialInfo.creditCardInfo.cardNumber}"
								id="cardNumber" class="input-large" maxlength="16" size="" placeholder=""  pattern="\d*">
								<c:if test="${flow != null && flow == 'edit'}">
								<input type="button" class="btn btn-primary" name="editCardNo" value="Click To Edit" onclick="javascript:clearCardNoField();"/>
								</c:if>
							<div id="cardNumber_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="nameOnCard" class="control-label required">Name
							On Card <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.creditCardInfo.nameOnCard"
								value="${paymentdetail.financialInfo.creditCardInfo.nameOnCard}"
								id="nameOnCard" maxlength="20" class="input-large" size="30" placeholder="">
							<div id="nameOnCard_error"></div>
						</div>
					</div>

					<div class="control-group">
						<label for="expirationDate" class="control-label">Expiration
							Date <a tabindex="-1" class="info" data-placement="top" rel="tooltip" href="#" data-original-title="Date your card will expire (month and year)"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<span> <select class="span2"
								name="financialInfo.creditCardInfo.expirationMonth"
								id="expirationMonth">
									<option value="">MM</option>
									<option
										<c:if test="${'01' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="01">Jan</option>
									<option
										<c:if test="${'02' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="02">Feb</option>
									<option
										<c:if test="${'03' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="03">Mar</option>
									<option
										<c:if test="${'04' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="04">Apr</option>
									<option
										<c:if test="${'05' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="05">May</option>
									<option
										<c:if test="${'06' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="06">Jun</option>
									<option
										<c:if test="${'07' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="07">Jul</option>
									<option
										<c:if test="${'08' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="08">Aug</option>
									<option
										<c:if test="${'09' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="09">Sep</option>
									<option
										<c:if test="${'10' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="10">Oct</option>
									<option
										<c:if test="${'11' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="11">Nov</option>
									<option
										<c:if test="${'12' == paymentdetail.financialInfo.creditCardInfo.expirationMonth}"> SELECTED </c:if>
										value="12">Dec</option>
							</select>
							</span> <span class="input-mini"> <select class="span3"
								id="expirationYear"
								name="financialInfo.creditCardInfo.expirationYear">
									<option value="">YYYY</option>
									<option
										<c:if test="${'2012' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2012">2012</option>
									<option
										<c:if test="${'2013' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2013">2013</option>
									<option
										<c:if test="${'2014' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2014">2014</option>
									<option
										<c:if test="${'2015' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2015">2015</option>
									<option
										<c:if test="${'2016' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2016">2016</option>
									<option
										<c:if test="${'2017' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2017">2017</option>
									<option
										<c:if test="${'2018' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2018">2018</option>
									<option
										<c:if test="${'2019' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2019">2019</option>
									<option
										<c:if test="${'2020' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2020">2020</option>
									<option
										<c:if test="${'2021' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2021">2021</option>
									<option
										<c:if test="${'2022' == paymentdetail.financialInfo.creditCardInfo.expirationYear}"> SELECTED </c:if>
										value="2022">2022</option>
							</select>
							</span>
							<div id="expirationYear_error"></div>
							<div id="creditInfoAuth_error"></div>
						</div>
					</div>
					<!-- end of controls-->
					<%-- <div class="control-group">
						<label for="securityCode" class="control-label required">Security
							Code <a class="info payment" data-placement="top" rel="tooltip" href="#" data-original-title="4-digit code in back of credit card (On the front of American Express cards)"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<input type="text"
								name="financialInfo.creditCardInfo.secureToken"
								value="${paymentdetail.financialInfo.creditCardInfo.secureToken}"
								id="secureToken" maxlength="4" class="input-mini" size="30">
							<div id="secureToken_error"></div>
						</div>
					</div> --%>
				</div>

				<div class="profile" id="manual" style="display: none">
					<h4 class="lightgray">Billing Address Details</h4>
					<p><strong>Please provide the billing address to be used to mail your invoices for premiums.</strong></p>
					<div class="control-group">
						<label for="streetAddress1" class="control-label required">Street
							Address1 <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.address1" id="address1"
							<c:if test="${flow != null && flow == 'add'}">
							value="<c:out value="${employerLocation.location.address1}"/>"</c:if>
							<c:if test="${flow != null && flow == 'edit'}">
							value = "<c:out value="${paymentdetail.financialInfo.address1}"/>"</c:if>
							class="input-xlarge" size="30">
							<div id="address1_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="streetAddress2" class="control-label">Street
							Address2 <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.address2"
								<c:if test="${flow != null && flow == 'add'}">
								value="<c:out value="${employerLocation.location.address2}"/>"</c:if>
								<c:if test="${flow != null && flow == 'edit'}">
								value = "<c:out value="${paymentdetail.financialInfo.address2}"/>"</c:if>
								 id="address2" class="input-xlarge" maxlength="20" size="30" placeholder="optional"> 

							<div id="address2_error"></div>
						</div>
					</div>

					<div class="control-group">
						<label for="city" class="control-label required">City <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.city"
							<c:if test="${flow != null && flow == 'add'}">
								value="<c:out value="${employerLocation.location.city}"/>"</c:if>
							<c:if test="${flow != null && flow == 'edit'}">
								value = "<c:out value="${paymentdetail.financialInfo.city}"/>"</c:if>
								id="city" maxlength="20" class="input-medium" size="30">
							<div id="city_error"></div>
						</div>
					</div>
					<div class="control-group">
						<label for="state" class="control-label required"> State <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select class="input-medium" id="state"
								name="financialInfo.state" path="statelist">
								<option value="">Select</option>
								<c:forEach var="state" items="${statelist}">
									<option
										<c:if test="${state.code == paymentdetail.financialInfo.state}"> SELECTED </c:if>
										<c:if test="${state.code == employerLocation.location.state}"> SELECTED </c:if>
										value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<div id="state_error"></div>
						</div>
						<!-- end of controls-->
					</div>
					<div class="control-group">
						<label for="zip" class="control-label required">Zip Code <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.zipcode"
							<c:if test="${flow != null && flow == 'add'}">
								value="<c:out value="${employerLocation.location.zip}"/>"</c:if>
							<c:if test="${flow != null && flow == 'edit'}">
								value = "<c:out value="${paymentdetail.financialInfo.zipcode}"/>"</c:if>
								id="zipcode" maxlength="20" class="input-small" size="30">
							<div id="zipcode_error"></div>
						</div>
					</div>
				</div>
				<!-- profile -->
				<!-- start: this only shows when you come from 'edit payment' -->
					<div class="controls" id="autoPayCheckBox">
						<label class="checkbox"> <input type="checkbox" name="chkboxAutoPay" id="chkboxAutoPay"
						<c:if test="${(paymentdetail.isDefault != null &&  paymentdetail.isDefault =='Y') || isDefaultPaymentMethodExist == false}">checked="checked"</c:if> onclick ="return readOnlyCheckBox()">
							Check here for monthly automatic payment with this account
						</label>
					</div>
					<div class="control">
					<br>
					<p><spring:message code="employer.editautopay.message"/></p>
					</div>
			<!-- end: this only shows when you come from 'edit payment' -->
			
				<div class="form-actions">
					<a class="btn" role="button"
						href="<c:url value="/shop/employer/paymentmethods"/>">Cancel</a>
					<!-- <input type="button" value="Cancel" class="btn btn-primary">-->
					<input type="button" name="submitBtn" id="submitBtn"
						onClick="javascript:validateForm();" Value="Save & Continue"
						class="btn btn-primary"><br /> <br />
				</div>
			</form>
		</div>
	</div>
</div>
</div>

<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap-datepicker.js" />">
	
</script>


<script type="text/javascript">
	$('#enrollmentdate').datepicker();
</script>

<script type="text/javascript">
$('.info').tooltip()
</script>

<script type="text/javascript">
$('.code').popover({ html: true,
	  trigger: 'hover',
	  placement: 'top',
	  content: function(){return '<img src="'+$(this).data('img') + '" />';}
});
	
	$(document).ready(function() {

		// only in case of edit radio button div has to be hidden
		if ('${flow}' == 'edit') {
			if ('${paymentdetail.paymentType}' == 'BANK') {
				$('#directBank').attr('style', 'display:block');
				$('#autoPayCheckBox').attr('style', 'display:block');

				if('${isPaymentGateWay}' == 'TRUE'){
					$('#accountNumber').attr("disabled", true); 
				}else{
					$('#accountNumber').attr('readonly', 'readonly'); 
				}  
				
				//$('#paymentmethod').attr('placeholder', 'Chase Bank Checking');
			} else if ('${paymentdetail.paymentType}' == 'CREDITCARD') {
				$('#debitCredit').attr('style', 'display:block');
				$('#autoPayCheckBox').attr('style', 'display:block');

                 if('${isPaymentGateWay}' == 'TRUE'){
					$('#cardNumber').attr("disabled", true); 
				}else{
					$('#cardNumber').attr('readonly', 'readonly'); 
				}   
			//	$('#cardNumber').attr("disabled", true);
				//$('#paymentmethod').attr('placeholder', 'Chase Visa Checking');
			} else {
				$('#manual').attr('style', 'display:block');
				$('#autoPayCheckBox').attr('style', 'display:none');
				//$('#paymentmethod').attr('placeholder', 'Manual (Check)');
			}
		} else {//first time in case of add payment method, direct bank option should be visible
			if ('${paymenttype}' == 'BANK') {
				$('#directBank').attr('style', 'display:block');
				$('#debitCredit').attr('style', 'display:none');
				$('#manual').attr('style', 'display:none');
				$('#autoPayCheckBox').attr('style', 'display:block');
				//$('#paymentmethod').attr('placeholder', 'Chase Bank Checking');
			}  else if ('${paymenttype}' == 'CREDITCARD') {
				$('#directBank').attr('style', 'display:none');
				$('#debitCredit').attr('style', 'display:block');
				$('#manual').attr('style', 'display:none');
				$('#autoPayCheckBox').attr('style', 'display:block');
				//$('#paymentmethod').attr('placeholder', 'Chase Visa Checking');
			} else if ('${paymenttype}' == 'MANUAL') {
				$('#directBank').attr('style', 'display:none');
				$('#debitCredit').attr('style', 'display:none');
				$('#manual').attr('style', 'display:block');
				$('#autoPayCheckBox').attr('style', 'display:none');
				//$('#paymentmethod').attr('placeholder', 'Manual (Check)');
			}
		}

	});

	var validator = $("#fromaddpayment")
			.validate(
					{
						rules : {
							'paymentMethodName' : {
								required : true
							},
							'financialInfo.bankInfo.accountType' : {
								required : true
							},
							'financialInfo.bankInfo.nameOnAccount' : {
								required : true
							},
							'financialInfo.bankInfo.routingNumber' : {
								required : true,
								number : true,
								minlength : 9
							},
							'financialInfo.bankInfo.accountNumber' : {
								required : true,
								number : true,
								minlength : 4,
								maxlength : 18
							},
							'financialInfo.creditCardInfo.cardType' : {
								required : true
							},
							'financialInfo.creditCardInfo.cardNumber' : {
								required : true,
								number : true,
								minlength : 15
							},
							'financialInfo.creditCardInfo.expirationYear' : {
								expirationDateCheck : true
							},
							'financialInfo.creditCardInfo.nameOnCard' : {
								required : true
							},
							/* 'financialInfo.creditCardInfo.secureToken' : {
								required : true,
								minlength : 4,
								maxlength : 4
							}, */
							'financialInfo.address1' : {
								required : true
							},
							'financialInfo.city' : {
								required : true
							},
							'financialInfo.state' : {
								required : true
							},
							'financialInfo.zipcode' : {
								required : true,
								minlength : 5,
								number : true
							}
						},
						messages : {
							'paymentMethodName' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validatePaymentMethodName' javaScriptEscape='true'/></span>"
							},
							'financialInfo.bankInfo.accountType' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountType' javaScriptEscape='true'/></span>"
							},
							'financialInfo.bankInfo.nameOnAccount' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateNameOnAccount' javaScriptEscape='true'/></span>"
							},
							'financialInfo.bankInfo.routingNumber' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumber' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumberFormat' javaScriptEscape='true'/></span>",
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateRoutingNumberLength' javaScriptEscape='true'/></span>"
							},
							'financialInfo.bankInfo.accountNumber' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumber' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumberFormat' javaScriptEscape='true'/></span>",
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumberLength' javaScriptEscape='true'/></span>",
								maxlength : "<span> <em class='excl'>!</em><spring:message code='label.validateAccountNumberLength' javaScriptEscape='true'/></span>"
							},
							'financialInfo.creditCardInfo.cardType' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateCardType' javaScriptEscape='true'/></span>"
							},
							'financialInfo.creditCardInfo.cardNumber' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumber' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumberFormat' javaScriptEscape='true'/></span>",
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateCardNumberLength' javaScriptEscape='true'/></span>"
							},
							'financialInfo.creditCardInfo.expirationYear' : {
								expirationDateCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateExpirationDate' javaScriptEscape='true'/></span>"
							},
							'financialInfo.creditCardInfo.nameOnCard' : {
								required : "<span> <em class='excl'>!</em>Please enter name on card.</span>"
							},
							/* 'financialInfo.creditCardInfo.secureToken' : {
								required : "<span> <em class='excl'>!</em>Please enter security code.</span>"
							}, */
							'financialInfo.address1' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress1' javaScriptEscape='true'/></span>"
							},
							'financialInfo.city' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"
							},
							'financialInfo.state' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"
							},
							'financialInfo.zipcode' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateZipNumber' javaScriptEscape='true'/></span>",
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateZipLength' javaScriptEscape='true'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							if($("#" + elementId + "_error").html() != null)
							{
								$("#" + elementId + "_error").html('');
							}
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error")
									.attr('class', 'error');
						}
					});

	jQuery.validator.addMethod("expirationDateCheck", function(value, element,
			param) {
		expirationMonth = $("#expirationMonth").val();
		expirationYear = $("#expirationYear").val();

		if (expirationMonth == "" || expirationYear == "") {
			return false;
		}
		return true;
	});

	function validateForm() {

		if ($("#fromaddpayment").validate().form()) {
			if('${flow}' == 'edit'){
				$("#paymentType").val('${paymentdetail.paymentType}');
			}

			if ('${paymenttype}' == 'CREDITCARD') {
				if(validateDate())
				{					
					validateCreditInfo();
				}
			} 
			/* else if(('${flow}' == 'add' && '${paymenttype}' == 'BANK') || ('${flow}' == 'edit' && '${paymentdetail.paymentType}' == 'BANK'))
				{
				validateBankAccInfo();
				} */

			else {
				document.getElementById("submitBtn").disabled = true; 
				$("#fromaddpayment").submit();
			}
		}
	}

	function validateCreditInfo() {
		var validateUrl = '<c:url value="/shop/employer/validatecreditinfo"> <c:param name="${df:csrfTokenParameter()}">
							<df:csrfToken plainToken="true" /> 
							</c:param>
							</c:url>';

		$
				.ajax({
					url : validateUrl,
					type : "POST",
					data : {
						cardType : $("#cardType").val(),
						cardNumber : $("#cardNumber").val(),
						expYear : $("#expirationYear").val(),
						expMonth : $("#expirationMonth").val()
					},
					success : function(response, xhr) {
						if(isInvalidCSRFToken(xhr))	    				
							return;	    				
						if (response == 'REJECT') {
							error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please provide valid creditcard Information.</span></label>";
							$('#cardNumber_error').html(error);
							return false;
						} else {
							$("#fromaddpayment").submit();
						}
					},
				});
	}
	
	function isInvalidCSRFToken(xhr){
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
			alert($('Session is invalid').text());
			rv = true;
		}
		return rv;
	}
	
	/* function validateBankAccInfo()
	{
		var validateUrl = 'validatebankinfo';
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				routingNumber : $("#routingNumber").val()
			},
			success: function(response)
			{
				if(response == 'INVALID')
				{
					$("#routingNumber_error").html("<label class='error'><span> <em class='excl'>!</em>Invalid, Please provide valid Routing Number.</span></label>");
					$("#routingNumber_error").attr('class','error help-inline');
					return false;
				}
				else
				{
					$("#fromaddpayment").submit();
				}

			
		});
	} */
	
	function validateDate()
	{
	    var expDate = new Date();
	    var today = new Date();
	    
	    expDate.setFullYear($("#expirationYear").val(), $("#expirationMonth").val(), 1);
	    if (expDate<today)
	    {
	    	error = "<label class='error' generated='true'><span><em class='excl'>!</em>Invalid Expiry date, Please select valid date.</span></label>";
			$('#expirationYear_error').html(error);
	      	return false;
	    }
	    else
	    {
	    	return true;
	    }
	}

	function clearAccountNoField()
	{
		document.getElementById('accountNumber').value = "";
		if('${isPaymentGateWay}' == 'TRUE'){
			$('#accountNumber').attr("disabled", false); 
		}else{
			$('#accountNumber').removeAttr('readonly'); 
		} 
	}
	
	function clearCardNoField()
	{
		document.getElementById('cardNumber').value = "";
		if('${isPaymentGateWay}' == 'TRUE'){
			$('#cardNumber').attr("disabled", false); 
		}else{
			$('#cardNumber').removeAttr('readonly'); 
		} 
	}
	
	function readOnlyCheckBox()
	{
		if(('${isDefaultPaymentMethodExist}' == 'false') || (${paymentdetail.isDefault != null &&  paymentdetail.isDefault =='Y'}))
			{
			  return false;
			}
		else
			{
			  return true;
			}	
	}
</script>
