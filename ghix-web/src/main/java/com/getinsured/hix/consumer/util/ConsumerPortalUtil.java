package com.getinsured.hix.consumer.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.consumer.model.MemberView;
import com.getinsured.hix.consumer.repository.IHouseholdRepository;
import com.getinsured.hix.crm.util.ConsumerEventsUtil;
import com.getinsured.hix.dto.cap.CapConsumerDto;
import com.getinsured.hix.dto.consumer.HouseholdRequestDTO;
import com.getinsured.hix.dto.consumer.HouseholdResponseDTO;
import com.getinsured.hix.dto.externalassister.DesignateAssisterDTO;
import com.getinsured.hix.dto.externalassister.DesignateAssisterRequest;
import com.getinsured.hix.dto.externalassister.DesignateAssisterResponse;
import com.getinsured.hix.eligibility.prescreen.util.PrescreenConstants;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.consumer.Member;
import com.getinsured.hix.model.consumer.Member.EligibilityProgramValues;
import com.getinsured.hix.model.consumer.Member.EligibilityStateValues;
import com.getinsured.hix.model.consumer.Member.Gender;
import com.getinsured.hix.model.consumer.Member.MemberBooleanFlag;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.RestfulResponseException;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.thoughtworks.xstream.XStream;

/**
 * Utility class for PHIX Eligibility
 * 
 * @author Nikhil Talreja
 * @since 20 August, 2013
 * 
 */
@Component
public class ConsumerPortalUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ConsumerPortalUtil.class);
	private static final String PREMIUM_TAX_CREDIT_PER_MONTH = "premiumTaxCreditPerMonth";
	private static final String PREMIUM_TAX_CREDIT_PER_YEAR = "premiumTaxCreditPerYear";
	private static final String ELIGIBILITY_PERIOD_END = "eligibilityPeriodEnd";
	private static final String ELIGIBILITY_PERIOD_START = "eligibilityPeriodStart";
	private static final String ELIGIBILITY_STATUS = "eligibilityStatus";
	private static final String PROGRAM = "program";
	public static final String MEMBERS = "members";
	public static final String HOUSEHOLD_ID = "cmrHouseholdId";
	@Autowired private ConsumerEventsUtil consumerEventsUtil;
	@Autowired
	private AppEventService appEventService;

	public static final String CSR = "csr";
	public static final int STEP_THRESHOLD_TO_SHOW_LEFT_NAV = 3;
	private static final String APPEVENT_DEMOGRAPHIC_UPDATES = "APPEVENT_DEMOGRAPHIC_UPDATES";
	private static final String APPEVENT_CSR_OVERRIDES = "APPEVENT_CSR_OVERRIDES";
	private static final String NAME_CHANGED = "NAME_CHANGED";
	private static final String EMAIL_CHANGED = "EMAIL_CHANGED";
	private static final String PHONE_CHANGED = "PHONE_CHANGED";
	public static final String SSN_OVERRIDE = "SSN_OVERRIDE";
	private static final String DOB_OVERRIDE = "DOB_CHANGED";
	public static final String CAP_MEMBER_RELINKALLOWED = "CAP_MEMBER_RELINKALLOWED";
    public static final String ENROLLMENT_DATA_EDIT = "ENROLLMENT_DATA_EDIT";
    public static final String ENROLLMENT_DATA_READ_ONLY="ENROLLMENT_DATA_READ_ONLY";
    public static final String CAP_ALLOW_RESEND834 = "CAP_ALLOW_RESEND834";
    
    public static final String TARGET_PAGE ="targetPage";
    public static final String TARGET_CONSUMER_ID ="targetConsumerId";
    public static final String ENROLLMENT_HISTORY ="view_enrollment";
    
	
	public static Set<String> OVERRIDE_PERMS = null;
	static {
		OVERRIDE_PERMS = new HashSet<>(4);
		OVERRIDE_PERMS.add(CAP_MEMBER_RELINKALLOWED);
		OVERRIDE_PERMS.add(ENROLLMENT_DATA_EDIT);
		OVERRIDE_PERMS.add(ENROLLMENT_DATA_READ_ONLY);
		OVERRIDE_PERMS.add(CAP_ALLOW_RESEND834);
		OVERRIDE_PERMS = Collections.unmodifiableSet(OVERRIDE_PERMS);
	}
	
	public enum SsnReasons {
		RELIGIOUS_EXCEPTION("Religious Exception"),
		JUST_APPLIED("Just Applied"),
		CITIZEN_EXCEPTION("Citizen Exception"),
		OTHER("Other");

	    private String reason;

	    private SsnReasons(String reason)
	    {
	        this.reason = reason;
	    }
	    
	    public static String getReason(String reason) {
	    	for(SsnReasons ssnReason : SsnReasons.values())
	        {
	           if(ssnReason.name().equals(reason))
	               return ssnReason.getReasons();
	        }
	    	return null;
	    }

	    public String getReasons()
	    {
	        return this.reason;
	    }
	}
	
    private String tokenValidationKey = null;
	
	@Value("#{configProp['saml.token.validation_key'] != null ? configProp['saml.token.validation_key'] : 'NOT AVAILABLE'}")
	public void setTokenValidationKey(String key){
		tokenValidationKey = key.trim();
	}
	@Autowired
	private HIXHTTPClient hIXHTTPClient;
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	public static final String CONTENT_TYPE = "application/json";

	@Autowired
	private IHouseholdRepository iHouseholdRepository;
	
	@Autowired private UserService userService;
	@Autowired private LookupService lookupService;
	/**
	 * Method to get the household record from the household table given the id
	 * 
	 * @author Suhasini Goli
	 * @since 9/19/2013
	 * 
	 * @param int householdId - household id for which the records needs to be
	 *        retrieved
	 * @return Household - Household object
	 */
	public Household getHouseholdRecord(int householdId) {
		try {
			LOGGER.info("Retrieving household for "+SecurityUtil.sanitizeForLogging(""+householdId));
			
			String postResp = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, String.valueOf(householdId), String.class);
			
			XStream xstream = GhixUtils.getXStreamStaxObject();
			ConsumerResponse consumerResponse = (ConsumerResponse) xstream.fromXML(postResp);
			if(null == consumerResponse){
				return null;
			}

			return consumerResponse.getHousehold();

		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Unable to get household record.",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
			return null;
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while fetching record from CMR_HOUSEHOLD table",e);
			return null;
		}
	}
	/**
	 * Method to get the household record from the household table given the id
	 * 
	 * @author Suhasini Goli
	 * @since 9/19/2013
	 * 
	 * @param int householdId - household id for which the records needs to be
	 *        retrieved
	 * @return Household - Household object
	 */
	public Household getHouseholdRecordByUserId(int userId) {

		Household household = null;
		try {
			LOGGER.info("Calling the RestAPI for getting the household by UserID");
			String postParams = "\"" + userId + "\"";

			XStream xstream = GhixUtils.getXStreamStaxObject();
			String postResp = restTemplate
					.postForObject(
							GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_USER_ID,
							postParams, String.class);

			household = (Household) xstream.fromXML(postResp);

			return household;

		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Exception occured while fetching record from CMR_HOUSEHOLD table by user_id.",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
			return null;
		}
		catch (Exception e) {
			LOGGER.error(
					"Exception occured while fetching record from CMR_HOUSEHOLD table by user_id",
					e);
			return null;
		}
	}

	public Household getHouseholdRecordByEligLeadId(int eligLeadId) {
		Household household = null;
		try {
			LOGGER.info("Calling the RestAPI for getting the household by EligLeadID");
			String postParams = "\"" + eligLeadId + "\"";

			XStream xstream = GhixUtils.getXStreamStaxObject();
			String postResp = restTemplate
					.postForObject(
							GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ELIG_LEAD_ID,
							postParams, String.class);
			household = (Household) xstream.fromXML(postResp);
			return household;
		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Unable to get household record by elig lead id.",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
			return null;
		}
		catch (Exception e) {
			LOGGER.error(
					"Exception occured while fetching record from CMR_HOUSEHOLD table by Elig_Lead_id",
					e);
			return null;
		}
	}

	public List<Member> findMembersByHousehold(Household household) {
		List<Member> memberList = new ArrayList<Member>();
		try {
			XStream xstream = GhixUtils.getXStreamStaxObject();
			String requestXML = GhixUtils.getXStreamStaxObject().toXML(
					household);
			String restResponse = ghixRestTemplate
					.postForObject(
							GhixEndPoints.ConsumerServiceEndPoints.GET_MEMBERS_BY_HOUSEHOLD,
							requestXML, String.class);
			ConsumerResponse response = (ConsumerResponse) xstream
					.fromXML(restResponse);

			if (response != null)
				return response.getMembers();

			return memberList;
		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Exception occured while fetching record from CMR_HOUSEHOLD table by user_id.",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
			return memberList;
		}
		catch (Exception ex) {
			LOGGER.error(
					"Exception occured while fetching record from CMR_HOUSEHOLD table by user_id",
					ex);
			return memberList;
		}
	}


	public Household createSampleHouseholdRecord() {
		try {
			Household household = new Household();
			household.setAffiliate("SG Affiliate");
			household.setFfmHouseholdID("123abc123");
			household.setAptc("400");
			household.setEmail("sgtest02@mailinator.com");
			household.setFamilySize(4);
			household.setHouseHoldIncome(new Double(30000));

			LOGGER.info("Calling the RestAPI");

			ObjectMapper mapper = new ObjectMapper();
			String response = hIXHTTPClient
					.getPOSTData(
							GhixEndPoints.ConsumerServiceEndPoints.SAVE_HOUSEHOLD_RECORD,
							mapper.writeValueAsString(household), CONTENT_TYPE);
			return mapper.readValue(response, Household.class);
		} 
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Exception occured while saving the Houshold record",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
			return null;
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while saving the Houshold record ",
					e);
			return null;
		}

	}

	/**
	 * Create a household record
	 * 
	 * @return
	 */
	public Household createHouseholdRecord(Household houseHold) {
		if (houseHold == null) {
			houseHold = new Household();
		}

		// Add Demographic Updates events to Customer History
		if(houseHold.getId() > 0) {
			addDemographicUpdateEvents(houseHold);
		}
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			String response = hIXHTTPClient
					.getPOSTData(
							GhixEndPoints.ConsumerServiceEndPoints.SAVE_HOUSEHOLD_RECORD,
							mapper.writeValueAsString(houseHold), CONTENT_TYPE);
			houseHold = mapper.readValue(response, Household.class);

		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Exception creating empty Household record",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
			return null;
		}
		catch (Exception ex) {
			LOGGER.error("Exception creating empty Household record"
					+ ex.getMessage());
		}

		return houseHold;
	}
/*
	public Household saveHouseholdRecord(Household houseHold) {
		if (houseHold == null) {
			return null;
		}
		String requestPayLoad;
		ObjectMapper mapper;
		try {
			mapper = new ObjectMapper();
			requestPayLoad = mapper.writeValueAsString(houseHold);
			String response = hIXHTTPClient
					.getPOSTData(
							GhixEndPoints.ConsumerServiceEndPoints.SAVE_HOUSEHOLD_RECORD,
							requestPayLoad, CONTENT_TYPE);
			houseHold = mapper.readValue(response, Household.class);

		} catch (Exception ex) {
			LOGGER.error("Exception saving Household record" + ex.getMessage());
			return null;
		}

		return houseHold;
	}*/
	public Household saveHouseholdRecord(Household houseHold)
			throws ClientProtocolException, IOException, URISyntaxException {
		if (houseHold == null) {
			return null;
		}
		Household responseHousehold = null;
		boolean householdCreateflag = false; //flag to check if household is already created so to not to trigger Household created event for the member.
		/**
		 * Since id is all we care about. to remove infinite recursion issue
		 * here
		 */
		if (houseHold.getUser() != null) {
			AccountUser user = new AccountUser();
			user.setId(houseHold.getUser().getId());
			houseHold.setUser(user);
		}

		try {
			if (houseHold.getId() == 0) {
				if(userService.getLoggedInUser() != null) {
					houseHold.setCreatedBy(userService.getLoggedInUser().getId());
				}else {
					houseHold.setCreatedBy(houseHold.getUser().getId());
				}
				householdCreateflag=true;
			}
			if(userService.getLoggedInUser() != null) {
				houseHold.setUpdatedBy(userService.getLoggedInUser().getId());
			}else {
				houseHold.setUpdatedBy(houseHold.getUser().getId());
			}
			
		} catch (InvalidUserException e) {

			LOGGER.error("InvalidUserException" + e);
			throw new RuntimeException(e);
		}
		responseHousehold = createHouseholdRecord(houseHold);
		if (null != responseHousehold && householdCreateflag) {
			consumerEventsUtil.generateHouseholdCeateEvent(responseHousehold);
		}
		return responseHousehold;
	}

	public boolean createMemberRecords(int eligLeadId) {

		Calendar cal = TSCalendar.getInstance();
		// final void set(int year, int month, int dayOfMonth)
		cal.set(1960, 9, 01);
		Member member1 = new Member();
		Household household = getHouseholdRecordByEligLeadId(eligLeadId);
		if (household == null) {
			return false;
		}
		// dummy household data
		household.setNumberOfApplicants(4);
		household.setPremium("2000");
		household.setStatus("Eligible");
		household.setAptc("Aptc");
		household.setCsr("5");

		Calendar calstart = TSCalendar.getInstance();
		calstart.set(Calendar.YEAR, 2012);
		calstart.set(Calendar.MONTH, 3);
		calstart.set(Calendar.DAY_OF_MONTH, 5);

		Calendar calend = TSCalendar.getInstance();
		calend.set(Calendar.YEAR, 2013);
		calend.set(Calendar.MONTH, 4);
		calend.set(Calendar.DAY_OF_MONTH, 7);
		try{
		saveHouseholdRecord(household);
		}
		catch(Exception ex){
			
		}

		Household hh = new Household();
		hh.setId(household.getId());
		member1.setHousehold(hh);
		member1.setEmail("Kevin.channel@mailinator.com");
		member1.setFirstName("Kevin");
		member1.setLastName("Channel");
		member1.setBirthDate(TSDate.getNoOffsetTSDate(cal.getTimeInMillis()));
		member1.setGender(Gender.M);
		member1.setIsHouseHoldContact(MemberBooleanFlag.YES);
		member1.setIsSeekingCoverage(MemberBooleanFlag.YES);
		member1.setPhoneNumber("408-444-8884");
		member1.setSmoker(MemberBooleanFlag.YES);
		member1.setSsn("456-89-5678");
		member1.setEligibilityStartDate(TSDate.getNoOffsetTSDate(calstart.getTimeInMillis()));
		member1.setEligibilityEndDate(TSDate.getNoOffsetTSDate(calend.getTimeInMillis()));
		member1.setEligibilityProgram(EligibilityProgramValues.MEDICAID);

		Location location = new Location();
		location.setAddress1("Patina Court");
		location.setCity("Los Angeles");
		location.setState("CA");
		location.setZip("12346");

		member1.setContactLocation(location);

		member1.setAptcEligibilityIndicator(MemberBooleanFlag.YES);

		cal.set(2013, 10, 01);
		member1.setAptcEligibilityStartDate(TSDate.getNoOffsetTSDate(cal.getTimeInMillis()));
		cal.set(2013, 12, 01);
		member1.setAptcEligibilityEndDate(TSDate.getNoOffsetTSDate(cal.getTimeInMillis()));

		member1.setCsrEligibilityIndicator(MemberBooleanFlag.YES);

		cal.set(2013, 10, 01);
		member1.setCsrEligibilityStartDate(TSDate.getNoOffsetTSDate(cal.getTimeInMillis()));
		cal.set(2013, 12, 01);
		member1.setCsrEligibilityStartDate(TSDate.getNoOffsetTSDate(cal.getTimeInMillis()));

		member1.setCsrLevel("03");
		//member1.setFfeApplicantId(1234567);
		member1.setMaritalStatus(MemberBooleanFlag.YES);
		member1.setMonthlyAptcAmount(400);
		// ========================================================================================================

		cal.set(1965, 10, 01);
		Member member2 = new Member();
		member2.setHousehold(household);
		// member2.setHousehold(hh);
		member2.setEmail("lorie.channel@mailinator.com");
		member2.setFirstName("Lorie");
		member2.setLastName("Channel");
		member2.setBirthDate(TSDate.getNoOffsetTSDate(cal.getTimeInMillis()));
		member2.setGender(Gender.F);
		member2.setIsHouseHoldContact(MemberBooleanFlag.NO);
		member2.setIsSeekingCoverage(MemberBooleanFlag.YES);
		member2.setPhoneNumber("408-532-8884");
		member2.setSmoker(MemberBooleanFlag.NO);
		member2.setSsn("456-99-5678");
		member2.setEligibilityStartDate(TSDate.getNoOffsetTSDate(calstart.getTimeInMillis()));
		member2.setEligibilityEndDate(TSDate.getNoOffsetTSDate(calend.getTimeInMillis()));
		member2.setEligibilityStatus(EligibilityStateValues.FINAL_ELIGIBILITY_COMPLETE);
		member2.setEligibilityProgram(EligibilityProgramValues.APTC);

		Location location2 = new Location();
		location2.setAddress1("Bel Air Drive");
		location2.setCity("San Jose");
		location2.setState("CA");
		location2.setZip("95121");

		member2.setContactLocation(location2);

		// ==============================================================================

		cal.set(1998, 01, 01);
		Member member3 = new Member();
		// member3.setHousehold(household);
		member3.setHousehold(hh);
		member3.setEmail("john.channel@mailinator.com");
		member3.setFirstName("john");
		member3.setLastName("Channel");
		member3.setBirthDate(TSDate.getNoOffsetTSDate(cal.getTimeInMillis()));
		member3.setGender(Gender.M);
		member3.setIsHouseHoldContact(MemberBooleanFlag.YES);
		member3.setIsSeekingCoverage(MemberBooleanFlag.YES);
		member3.setPhoneNumber("408-444-8884");
		member3.setSmoker(MemberBooleanFlag.YES);
		member3.setSsn("456-89-5678");

		member3.setContactLocation(location2);
		// =====================================================================================
		cal.set(2000, 01, 01);
		Member member4 = new Member();
		member4.setHousehold(household);
		// member4.setHousehold(hh);
		member4.setEmail("bob.channel@mailinator.com");
		member4.setFirstName("Robert");
		member4.setLastName("Channel");
		member4.setBirthDate(TSDate.getNoOffsetTSDate(cal.getTimeInMillis()));
		member4.setGender(Gender.M);
		member4.setIsHouseHoldContact(MemberBooleanFlag.YES);
		member4.setIsSeekingCoverage(MemberBooleanFlag.YES);
		member4.setPhoneNumber("408-444-8884");
		member4.setSmoker(MemberBooleanFlag.YES);
		member4.setSsn("456-89-5678");

		member4.setContactLocation(location2);

		LOGGER.info("Calling the RestAPI");

		ObjectMapper mapper = new ObjectMapper();
		try {

			String response1 = hIXHTTPClient.getPOSTData(
					GhixEndPoints.ConsumerServiceEndPoints.SAVE_MEMBER_RECORD,
					mapper.writeValueAsString(member1), CONTENT_TYPE);
			Member savedMember1 = mapper.readValue(response1, Member.class);

			String response2 = hIXHTTPClient.getPOSTData(
					GhixEndPoints.ConsumerServiceEndPoints.SAVE_MEMBER_RECORD,
					mapper.writeValueAsString(member2), CONTENT_TYPE);
			Member savedMember2 = mapper.readValue(response2, Member.class);

			// String response3 =
			// hIXHTTPClient.getPOSTData(GhixEndPoints.ConsumerServiceEndPoints.SAVE_MEMBER_RECORD,
			// mapper.writeValueAsString(member3), CONTENT_TYPE);
			// Member savedMember3 = mapper.readValue(response3, Member.class);

		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Exception occured while saving the member record",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
			return false;
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while saving the member record ", e);
			return false;

		}

		return true;

	}

	/**
	 * This is default data when no response from FFM is available
	 * 
	 * @param model
	 */
	public static void defaultDataForMyEligibility(Model model) {
		List<MemberView> members = new ArrayList<MemberView>();
		model.addAttribute(PROGRAM, "Qualified Health Pl$n");
		model.addAttribute(ELIGIBILITY_STATUS, "Eligible1");
		model.addAttribute(ELIGIBILITY_PERIOD_START, "MM/DD/YYY0");
		model.addAttribute(ELIGIBILITY_PERIOD_END, "MM/DD/YYY0");
		model.addAttribute(PREMIUM_TAX_CREDIT_PER_YEAR, "$1965");
		model.addAttribute(PREMIUM_TAX_CREDIT_PER_MONTH, "$162");
		model.addAttribute(CSR, "Level 2");

		MemberView member = new MemberView("Mary", "ProgramX", "StatusX",
				"MM/DD/YYYY", "MM/DD/YYYY");
		members.add(member);
		member = new MemberView("Billy", "Program1X", "Status1X", "MM/DD/YYYY",
				"MM/DD/YYYY");
		members.add(member);
		model.addAttribute(MEMBERS, members);

	}

	public void setDataForMyEligibilityPage(Model model, Household household,
			List<Member> members) {
		 DateFormat dateFormatMMDDYYY = new SimpleDateFormat("MM/dd/yyyy");
		if (model != null && household != null) {
			model.addAttribute(PROGRAM, "What??");
			model.addAttribute(ELIGIBILITY_STATUS, household.getStatus());

			model.addAttribute(PREMIUM_TAX_CREDIT_PER_YEAR,
					household.getPremium());
			model.addAttribute(PREMIUM_TAX_CREDIT_PER_MONTH,
					Long.valueOf(household.getPremium()) / 12);
			model.addAttribute(CSR, "Level " + household.getCsr());

			List<MemberView> viewMembers = new ArrayList<MemberView>();
			MemberView view;
			for (Member member : members) {
				view = new MemberView();
				if (member.getEligibilityStartDate() != null) {

					view.setEligibilityPeriodStart(dateFormatMMDDYYY
							.format(member.getEligibilityStartDate()));
				}

				if (member.getEligibilityEndDate() != null) {
					view.setEligibilityPeriodEnd(dateFormatMMDDYYY
							.format(member.getEligibilityEndDate()));
				}

				if (member.getEligibilityStatus() != null) {
					view.setEligibilityStatus(member.getEligibilityStatus()
							.name());
				}
				if (member.getEligibilityProgram() != null) {
					view.setProgram(member.getEligibilityProgram().name());
				}
				// member.getFirstName(), member.getEligibilityProgram().name(),
				// member.getEligibilityStatus().name(),
				// member.getEligibilityStartDate().toString(),
				// member.getEligibilityEndDate().toString());
				viewMembers.add(view);
			}
			model.addAttribute(MEMBERS, viewMembers);

		}
	}

	public void setUserDetails(Model model, AccountUser user) {
		model.addAttribute("memberName", user.getUserName());
	}

	/**
	 * Fetch EligLeadRecord for given id
	 * 
	 * @param leadId
	 * @return
	 */
	public EligLead fetchEligLead(long leadId) {

		try {
			ObjectMapper mapper = new ObjectMapper();
			String response = hIXHTTPClient.getPOSTData(
					GhixEndPoints.PhixEndPoints.GET_LEAD, "\"" + leadId + "\"",
					PrescreenConstants.CONTENT_TYPE);
			return mapper.readValue(response, EligLead.class);
		} catch (Exception e) {
			LOGGER.error(
					"Exception occured while fetching record from ELIG_LEAD table",
					e);
			return null;
		}
	}

	public EligLead createEligLeadRecord(EligLead record) {
		if (record == null) {
			record = new EligLead();
		}
		ObjectMapper mapper;
		String requestPayLoad;
		try {
			mapper = new ObjectMapper();
			requestPayLoad = mapper.writeValueAsString(record);
			String response = hIXHTTPClient.getPOSTData(
					GhixEndPoints.PhixEndPoints.SAVE_LEAD, requestPayLoad,
					PrescreenConstants.CONTENT_TYPE);
			return mapper.readValue(response, EligLead.class);
		} catch (Exception e) {
			LOGGER.error(
					"Exception occured while saving record to ELIG_LEAD table",
					e);
			return null;
		}
	}

	/**
	 * Disable/enable left nav links on memberportal
	 * 
	 * @param step
	 * @return
	 */
	public boolean showLeftNavLinks(int step) {
		return (step > STEP_THRESHOLD_TO_SHOW_LEFT_NAV) ? true : false;
	}

	public static void main(String[] args) {
		 DateFormat dateFormatMMDDYYY = new SimpleDateFormat("MM/dd/yyyy");
		Calendar calend = TSCalendar.getInstance();
		calend.set(Calendar.YEAR, 2013);
		calend.set(Calendar.MONTH, 4);
		calend.set(Calendar.DAY_OF_MONTH, 7);

		Date date = calend.getTime();
		//System.out.println(dateFormatMMDDYYY.format(date));
		LOGGER.debug(dateFormatMMDDYYY.format(date));

	}
	
	public void getHouseholdList() {
		
		String requestPayLoad;
		ObjectMapper mapper;
		List<Integer> hhIdList = new ArrayList<Integer>();
		HouseholdResponseDTO hhResponseDTO;
		
		hhIdList.add(100);
		hhIdList.add(101);
		
		HouseholdRequestDTO hhRequestDTO = new HouseholdRequestDTO();
		hhRequestDTO.setHouseholdIdList(hhIdList);
		
		try {
			mapper = new ObjectMapper();
			requestPayLoad = mapper.writeValueAsString(hhRequestDTO);
			String response = hIXHTTPClient.getPOSTData(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_LIST,	requestPayLoad, CONTENT_TYPE);
			hhResponseDTO = mapper.readValue(response, HouseholdResponseDTO.class);

		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Exception getting a list of households",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
			return ;
		}
		catch (Exception ex) {
			LOGGER.error("Exception getting a list of households" + ex.getMessage());
			return;
		}

		return;
	}
	
	public void addDemographicUpdateEvents(Household household) {
		 DateFormat dateFormatMMDDYYY = new SimpleDateFormat("MM/dd/yyyy");
		// Return if new household creation request or null
		if (household == null || household.getId() < 1) {
			return;
		}
		
		Household existingHousehold = iHouseholdRepository.findById(household.getId());
		String name1 = household.getFirstName() + " " + household.getLastName();
		String name2 = existingHousehold.getFirstName() + " " + existingHousehold.getLastName();
		
		// Name Changed event
		if(StringUtils.isNotBlank(household.getFirstName()) && StringUtils.isNotBlank(household.getLastName()) && !name1.equalsIgnoreCase(name2)) {
			logDemographicUpdateEvents(household.getId(), NAME_CHANGED, name2, name1);
		}
		
		// Email Address Changed event
		if(StringUtils.isNotBlank(household.getEmail()) && !(household.getEmail()).equalsIgnoreCase(existingHousehold.getEmail())) {
			logDemographicUpdateEvents(household.getId(), EMAIL_CHANGED, existingHousehold.getEmail(), household.getEmail());
		}
		
		// Phone Number Changed event
		if(StringUtils.isNotBlank(household.getPhoneNumber()) && !(household.getPhoneNumber()).equalsIgnoreCase(existingHousehold.getPhoneNumber())) {
			logDemographicUpdateEvents(household.getId(), PHONE_CHANGED, existingHousehold.getPhoneNumber(), household.getPhoneNumber());
		}
		
		// SSN Changed event
		if(null == household.getSsnNotProvidedReason()) {
			household.setSsnNotProvidedReason("");
		}
		if(null == existingHousehold.getSsnNotProvidedReason()) {
			existingHousehold.setSsnNotProvidedReason("");
		}
		if(!household.getSsnNotProvidedReason().equalsIgnoreCase(existingHousehold.getSsnNotProvidedReason())){
			logCsrOverridesEvents(household.getId(), SSN_OVERRIDE, getSsnLogValueFromHousehold(existingHousehold), getSsnLogValueFromHousehold(household));
		}
		
		String oldBirthDate = existingHousehold.getBirthDate() != null ? dateFormatMMDDYYY.format(existingHousehold.getBirthDate()) : "";
		String newBirthDate = household.getBirthDate() != null ? dateFormatMMDDYYY.format(household.getBirthDate()) : "";
		
		if((household.getBirthDate() != null && existingHousehold.getBirthDate() != null 
				&& !com.getinsured.hix.platform.util.DateUtil.equateDate(household.getBirthDate(),existingHousehold.getBirthDate())) || !oldBirthDate.equalsIgnoreCase(newBirthDate)){
			logCsrOverridesEvents(household.getId(), DOB_OVERRIDE, oldBirthDate, newBirthDate);
		}
		
	}
	
	public String getSsnLogValueFromHousehold(Household hh) {
		String currentSsnValue;
		if(StringUtils.isNotBlank(hh.getSsnNotProvidedReason())) {
			currentSsnValue = "SSN Not Required ("+SsnReasons.getReason(hh.getSsnNotProvidedReason())+")";
		}else {
			currentSsnValue = "SSN Required";
		}
		return currentSsnValue;
	}
	
	/**
	 * @author Kunal Dav
	 * @param householdId
	 * @param eventType
	 * @param previousValue
	 * @param currentValue
	 * @return
	 * @description Method to log Demographic Updates events
	 */
	public void logDemographicUpdateEvents(int householdId, String eventType, String previousValue, String currentValue)  {
		
		Map<String, String> mapEventParam = new HashMap<>();
		mapEventParam.put("Source", "CAP Individual Profile Page");
		mapEventParam.put("Previous Value", (StringUtils.isBlank(previousValue)? "NA" : previousValue));
		mapEventParam.put("Current Value", (StringUtils.isBlank(currentValue)? "NA" : currentValue));
		
		//event type and category
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APPEVENT_DEMOGRAPHIC_UPDATES, eventType);
		EventInfoDto eventInfoDto = new EventInfoDto();
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleId(householdId);
		eventInfoDto.setModuleName("INDIVIDUAL");
		
		appEventService.record(eventInfoDto, mapEventParam);
	}
	
	/**
	 * @author Swapnil Bhattad
	 * @param householdId
	 * @param eventType
	 * @param previousValue
	 * @param currentValue
	 * @return
	 * @description Method to log Demographic Updates events
	 */
	public void logCsrOverridesEvents(int householdId, String eventType, String previousValue, String currentValue)  {
		
		Map<String, String> mapEventParam = new HashMap<>();
		mapEventParam.put("Source", "CAP Individual Profile Page");
		mapEventParam.put("Previous Value", (StringUtils.isBlank(previousValue)? "NA" : previousValue));
		mapEventParam.put("Current Value", (StringUtils.isBlank(currentValue)? "NA" : currentValue));
		
		//event type and category
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APPEVENT_CSR_OVERRIDES, eventType);
		EventInfoDto eventInfoDto = new EventInfoDto();
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleId(householdId);
		eventInfoDto.setModuleName("INDIVIDUAL");
		
		appEventService.record(eventInfoDto, mapEventParam);
	}
	
	public Household updateEditMemberDetails(Household household) throws InvalidUserException {
		
		// Add Demographic Updates events to Customer History
		if(household.getId() > 0) {
			addDemographicUpdateEvents(household);
		}
		
		CapConsumerDto capConsumerDto = new CapConsumerDto();
		capConsumerDto.setIdEnc(String.valueOf(household.getId()));
		capConsumerDto.setFirstName(household.getFirstName());
		capConsumerDto.setLastName(household.getLastName());
		capConsumerDto.setEmail(household.getEmail());
		capConsumerDto.setContactNumber(household.getPhoneNumber());
		capConsumerDto.setLastUpdated(String.valueOf(userService.getLoggedInUser().getId()));
		capConsumerDto.setSsn(household.getSsn());
		capConsumerDto.setSsnNotProvidedReason(household.getSsnNotProvidedReason());
		capConsumerDto.setDob(household.getBirthDate());
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			String capConsumerDtoStr = mapper.writeValueAsString(capConsumerDto);
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.UPDATE_EDIT_MEMBER_DETAILS, capConsumerDtoStr, String.class);
			household = mapper.readValue(response, Household.class);

		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Unable to get household record.",ex);	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex);
				break;
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exception creating empty Household record"
					+ ex.getMessage());
		}

		return household;
	}
	
	public void handleExternalAssister(AccountUser user, Map<String, String> parameters) {

		String targetUserMnSureId = null;

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info(" Inside Handle External Assister  for user {}", userService.getDefaultRole(user).getName());
		}
		/* Consider Target Consumer ID from Param Map if available */
		if (parameters != null && parameters.get(TARGET_CONSUMER_ID) != null) {
			targetUserMnSureId = (String) parameters.get(TARGET_CONSUMER_ID);
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info(" Using the Consumer Id from the Param's {}", targetUserMnSureId);
			}

		} else {
			if (user.getLoginConext() != null && user.getLoginConext().get(TARGET_CONSUMER_ID) != null)
				targetUserMnSureId = (String) user.getLoginConext().get(TARGET_CONSUMER_ID); // Pre defined param from
																								// SAML redirect
		}

		if (StringUtils.isNotBlank(targetUserMnSureId)) {

			String assisterMnSureId = user.getExtnAppUserId();
			DesignateAssisterRequest assisterRequest = new DesignateAssisterRequest();
			assisterRequest.setExternalAssisterId(assisterMnSureId);
			assisterRequest.setExternalUserId(targetUserMnSureId);
			assisterRequest.setAssisterType(DesignateAssisterRequest.ASSISTER_TYPE.BROKER);
			DesignateAssisterDTO response = ghixRestTemplate.postForObject(
					GhixEndPoints.ExternalAssisterEndPoints.HOUSEHOLDS_FOR_ASSISTER, assisterRequest,
					DesignateAssisterDTO.class);

			if (response != null && response.getStatus().equalsIgnoreCase("SUCCESS")) {
				List<Integer> linkedModuleIdsForAssister = response.getDesignateAssisterResponse().stream()
						.map(DesignateAssisterResponse::getHouseholdId).collect(Collectors.toList());

				user.setLinkedModuleIds(linkedModuleIdsForAssister);
				if (linkedModuleIdsForAssister != null) {
					int size = linkedModuleIdsForAssister.size();
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info(" Assister {} is linked with {} households for user  {}", assisterMnSureId,
								linkedModuleIdsForAssister, targetUserMnSureId);
					}
					switch (size) {
					case 0: {
						// No households associated
						user.setActiveModuleId(-1);
						break;
					}
					case 1: {
						// Exactly one househild
						user.setActiveModuleId(linkedModuleIdsForAssister.get(0));
						break;
					}
					default: {
						// Multiple, let the user choose from the portal
						user.setActiveModuleId(0);
					}
					}
					user.setActiveModuleName("INDIVIDUAL".intern());
					user.setPersonatedRoleName("INDIVIDUAL".intern());
				}
			} else {
				// Call failed
				user.setActiveModuleId(-1);
				user.setActiveModuleName("INDIVIDUAL".intern());
				user.setPersonatedRoleName("INDIVIDUAL".intern());

			}
		} else {

			LOGGER.info(" Target Consumer Id not found in Params / User Context ", targetUserMnSureId);
			user.setActiveModuleId(0);
		}

	}
	
	
//	/**
//	 * Allowed roles for relink should be "ADMIN","OPERATIONS","L1_CSR", "L2_CSR","SUPERVISOR" - Manage member page
//	 * @param request
//	 * @return
//	 */
//	public String isRelinkAllowed(HttpServletRequest request) {
//		return this.perEvaluator.hasPermission("CAP_MEMBER_RELINKALLOWED") ? "Y": "N";
//	}
//	/**
//	 * Allows the role to edit the enrollment - Enrollment details page
//	 * @param request
//	 * @return
//	 */
//	public String isEnrollmentEditAllowed(HttpServletRequest request) {
//		return this.perEvaluator.hasPermission("ENROLLMENT_DATA_EDIT") ? "Y": "N";
//	}
//	
//	/**
//	 * Allows the role to read the enrollment details - Enrollment details page
//	 * @param request
//	 * @return
//	 */
//	public String isEnrollmentReadOnlyAllowed(HttpServletRequest request) {
//		return this.perEvaluator.hasPermission("ENROLLMENT_DATA_READ_ONLY") ? "Y": "N";
//	}
//	
//
//	public String isResendAllowed(HttpServletRequest request) {
//		return this.perEvaluator.hasPermission("CAP_ALLOW_RESEND834") ? "Y": "N";
//	}

}
