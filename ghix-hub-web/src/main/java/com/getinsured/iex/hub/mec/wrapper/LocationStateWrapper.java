package com.getinsured.iex.hub.mec.wrapper;

import com.getinsured.iex.hub.mec.niem.niem.usps_states._2.USStateCodeSimpleType;
import com.getinsured.iex.hub.mec.niem.niem.usps_states._2.USStateCodeType;

/**
 * @author Vishaka Tharani
 * @since February 21, 2014
 *
 */
public class LocationStateWrapper {

	private static com.getinsured.iex.hub.mec.niem.niem.usps_states._2.ObjectFactory stateFactory;
	
	private USStateCodeType usStateCodeType = null;
	private String locationStateUSPostalServiceCode;

	public LocationStateWrapper() {
		stateFactory = new com.getinsured.iex.hub.mec.niem.niem.usps_states._2.ObjectFactory();
	}

	public USStateCodeType getSystemRepresentation() {
		return this.usStateCodeType;
	}

	public void setLocationStateUSPostalServiceCode(String locationStateUSPostalServiceCode){
		if(locationStateUSPostalServiceCode != null && locationStateUSPostalServiceCode.trim().length() > 0){
			if(usStateCodeType == null)
			{
				usStateCodeType = stateFactory.createUSStateCodeType();
			}
			usStateCodeType.setValue(USStateCodeSimpleType.valueOf(locationStateUSPostalServiceCode));
			this.locationStateUSPostalServiceCode = locationStateUSPostalServiceCode;
		}
	}
	
	public String getLocationStateUSPostalServiceCode() {
		return this.locationStateUSPostalServiceCode;
	}
}
