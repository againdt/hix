package com.getinsured.hix.batch.migration.couchbase.ecm;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.migration.couchbase.ecm.DataConfig.MigrationStepEnum;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public abstract class BaseEcmCouchMigrationClass extends StepExecutionListenerSupport implements Tasklet {
	private static final String SKIP_THE_CURRENT_STEP = "Skip the current step - ";
	private static final String EXECUTING_STEP = "Executing step - ";
	private static final String STEP_EXECUTION_PRE_CHECK_STARTS = "Step execution pre check starts for - ";
	private MigrationStepEnum currentStep;

	private String jobStep;

	public BaseEcmCouchMigrationClass(MigrationStepEnum currentStep) {
		this.currentStep = currentStep;
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseEcmCouchMigrationClass.class);

	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.info(STEP_EXECUTION_PRE_CHECK_STARTS + currentStep);
		JobParameters jobParameters = stepExecution.getJobParameters();
		String stepNumber = jobParameters.getString("step");
		if (stepNumber == null || MigrationStepEnum.fromValue(stepNumber) == null) {
			throw new GIRuntimeException("Step Number is not valid.");
		}
		this.jobStep = stepNumber;
	}

	int extractMaximumPrimaryRecordId(Connection con, String tableName, String columnName,
			DataConfig.MigrationStatusEnum status) throws SQLException {
		int iReturn = 0;
		StringBuilder st = new StringBuilder(50);
		st.append("select max(PRIMARY_ID) from ").append(DataConfig.COUCH_MIGRATION).append(" where TABLE_NAME = '")
				.append(tableName).append("' and COLUMN_NAME = '").append(columnName)
				.append("' and MIGRATION_STATUS in ('").append(status.name()).append("')");
		Map<String, String> data = JDBCUtil.recordByColumnId(con, st.toString(), 1);
		if (CouchMigrationUtil.mapSize(data) != 0) {
			iReturn = CouchMigrationUtil.convertToInt(data.get("1"));
		}
		return iReturn;
	}

	private boolean validStep() {
		return currentStep.name().equalsIgnoreCase(jobStep);
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		if (validStep()) {
			LOGGER.info(EXECUTING_STEP + currentStep);
			startExecution();
		} else {
			LOGGER.info(SKIP_THE_CURRENT_STEP + currentStep);
		}
		return RepeatStatus.FINISHED;
	}

	abstract void startExecution() throws Exception;
}
