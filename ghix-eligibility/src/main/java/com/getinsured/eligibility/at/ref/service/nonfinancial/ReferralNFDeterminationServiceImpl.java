package com.getinsured.eligibility.at.ref.service.nonfinancial;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.getinsured.hix.model.NoticeQueued;
import com.getinsured.hix.platform.util.DateUtil;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerBaseService;
import com.getinsured.eligibility.at.ref.service.ReferralActivationNotificationService;
import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.at.ref.service.ReferralProcessingService;
import com.getinsured.eligibility.at.ref.service.ReferralSsapCmrLinkService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TSCalendar;

/**
 * @author chopra_s
 * 
 */
@Component("referralNFDeterminationService")
@Scope("singleton")
public class ReferralNFDeterminationServiceImpl extends LceProcessHandlerBaseService implements ReferralNonFinancialService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralNFDeterminationServiceImpl.class);

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("referralActivationNotificationService")
	private ReferralActivationNotificationService referralActivationNotificationService;

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;

	@Autowired
	@Qualifier("referralNFCompareService")
	private ReferralNFCompareService referralNFCompareService;

	@Autowired
	@Qualifier("referralSsapCmrLinkService")
	private ReferralSsapCmrLinkService referralSsapCmrLinkService;
	
	@Autowired
	private NoticeService noticeService;
	
	@Autowired
	ReferralProcessingService referralProcessingService;

	private static final Set<String> APPLICATION_STATUS_CHECK = new HashSet<String>(Arrays.asList(ApplicationStatus.OPEN.getApplicationStatusCode(), ApplicationStatus.SIGNED.getApplicationStatusCode(),
	        ApplicationStatus.SUBMITTED.getApplicationStatusCode(), ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(), ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode()));

	private static final Set<String> APPLICATION_STATUS_ALLOWED = new HashSet<String>(Arrays.asList(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode()));

	@Override
	@ReferralTransactionAnno
	public boolean execute(NFProcessDTO nfProcessRequestDTO) {
		LOGGER.info("ReferralNFDeterminationServiceImpl starts for - " + nfProcessRequestDTO);
		boolean blnComplete = false;
		final SsapApplication application = ssapApplicationRepository.findAndLoadApplicantsByAppId(nfProcessRequestDTO.getCurrentApplicationId());

		if (application == null) {
			LOGGER.error(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getCurrentApplicationId());
			throw new GIRuntimeException(ReferralConstants.NO_SSAP_FOUND + nfProcessRequestDTO.getCurrentApplicationId());
		}

		if (nfProcessRequestDTO.isMultipleCmr()) {
			LOGGER.info("Multiple Household exists for application primary applicantid");
			initiateActivation(nfProcessRequestDTO, application);
			return true;
		}

		if(!nfProcessRequestDTO.isAutoLink()) {
			//Second parameter is householdCaseId in case of CA AT
			if(application.getCmrHouseoldId() == null)
			{
				referralProcessingService.checkHouseholdExistsAndThenCreate(application.getId(),nfProcessRequestDTO.getCaseNumber());
			}
			String deferEmailSending = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_DEFER_ACCOUNT_ACTIVATION_EMAIL);
			if("true".equalsIgnoreCase(deferEmailSending)){
				Date todayDate = TSCalendar.getInstance().getTime();
				todayDate = DateUtil.removeTime(todayDate);
				noticeService.scheduleQueuedNotice("AccountMigrationEmail", todayDate, NoticeQueued.TableNames.SSAP_APPLICATIONS, "ID", application.getId());
			} else if(!application.getExchangeEligibilityStatus().equals(ExchangeEligibilityStatus.MEDICAID)) {
				initiateActivation(nfProcessRequestDTO, application);
			} 
			blnComplete = true;
		}
		
		/*if (nfProcessRequestDTO.isNonFinancialCmr()) {
			boolean blnNFCmr = determineLogicForMatchingNFCmr(nfProcessRequestDTO, application);
			if (!blnNFCmr) {
				LOGGER.info("No Matching NF application household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId());
				referralProcessingService.checkHouseholdExistsAndThenCreate(application.getId());
				initiateActivation(nfProcessRequestDTO, application);
				return true;
			}
		} else {
			boolean blnNFCmr = determineLogicForMatchingCmr(nfProcessRequestDTO, application);
			if (!blnNFCmr) {
				LOGGER.info("No Single Matching household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId());
				referralProcessingService.checkHouseholdExistsAndThenCreate(application.getId());
				initiateActivation(nfProcessRequestDTO, application);
				return true;
			}
		}*/
		
		

		/*blnComplete = handleSingleConsumerFound(nfProcessRequestDTO, application);
		if (blnComplete) {
			referralProcessingService.checkHouseholdExistsAndThenCreate(application.getId());
			initiateActivation(nfProcessRequestDTO, application);
		} else {
			linkApplicationToHousehold(application, nfProcessRequestDTO.getMatchingCmrId());
		}

		LOGGER.info("ReferralNFDeterminationServiceImpl ends for - " + nfProcessRequestDTO);*/

		return blnComplete;
	}

	private void linkApplicationToHousehold(SsapApplication application, int cmrId) {
		referralSsapCmrLinkService.executeLinkingWithNFCmr(application.getId(), cmrId);
	}

	private boolean handleSingleConsumerFound(NFProcessDTO nfProcessRequestDTO, SsapApplication application) {
		LOGGER.info("Single Matching household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId() + ", CMR ID " + nfProcessRequestDTO.getMatchingCmrId());
		final List<SsapApplication> cmrApplications = ssapApplicationRepository.findByCmrHouseoldId(new BigDecimal(nfProcessRequestDTO.getMatchingCmrId()));
		LOGGER.info(ReferralUtil.listSize(cmrApplications) + " - Applications found for cmr - " + nfProcessRequestDTO.getMatchingCmrId());

		if (ReferralUtil.listSize(cmrApplications) == ReferralConstants.NONE) {
			LOGGER.info("No Applications found for cmr - " + nfProcessRequestDTO.getMatchingCmrId());
			return true;
		}

		final long nfApplicationId = fetchApplicationId(cmrApplications, application);

		if (nfApplicationId == ReferralConstants.NONE) {
			LOGGER.info("No Matching Non Financial Application found for application - " + nfProcessRequestDTO.getCurrentApplicationId());
			return true;
		}

		nfProcessRequestDTO.setNfApplicationId(nfApplicationId);

		final boolean blnCompare = referralNFCompareService.memberMatching(nfProcessRequestDTO);
		if (!blnCompare) {
			return true;
		}

		return false;
	}

	private long fetchApplicationId(List<SsapApplication> cmrApplications, SsapApplication application) {
		boolean allowedFound = false;
		long applicationId = 0l;
		for (SsapApplication ssapApplication : cmrApplications) {
			if ((application.getCoverageYear() == ssapApplication.getCoverageYear()) && APPLICATION_STATUS_CHECK.contains(ssapApplication.getApplicationStatus())) {
				if (ReferralConstants.Y.equals(ssapApplication.getFinancialAssistanceFlag())) {
					return 0;
				}
				if (APPLICATION_STATUS_ALLOWED.contains(ssapApplication.getApplicationStatus())) {
					if (allowedFound) {
						return 0;
					}
					applicationId = ssapApplication.getId();
					allowedFound = true;
				} else {
					return 0;
				}
			}
		}
		return applicationId;
	}

	private boolean determineLogicForMatchingNFCmr(NFProcessDTO nfProcessRequestDTO, SsapApplication application) {
		final SsapApplicant primaryApplicant = ReferralUtil.retreiveApplicant(application, ReferralConstants.PRIMARY);
		BigDecimal matchingCmr = referralSsapCmrLinkService.findMatchingCmrHousehold(primaryApplicant,new BigDecimal(nfProcessRequestDTO.getNfCmrId()));
		if(matchingCmr != null) {
			nfProcessRequestDTO.setMatchingCmrId(nfProcessRequestDTO.getNfCmrId());
			return true;
		}
		return false;	
	}

	private boolean determineLogicForMatchingCmr(NFProcessDTO nfProcessRequestDTO, SsapApplication application) {
		final SsapApplicant primaryApplicant = ReferralUtil.retreiveApplicant(application, ReferralConstants.PRIMARY);
		final List<Integer> matchingCmr = cmrHouseholdRepository.findMatchingCmrByProfile(ReferralUtil.capitalizeFirstLetter(primaryApplicant.getFirstName()), ReferralUtil.capitalizeFirstLetter(primaryApplicant.getLastName()), primaryApplicant.getBirthDate(), primaryApplicant.getSsn());
		LOGGER.info("Number of Matching household found for primary applicant for application - " + nfProcessRequestDTO.getCurrentApplicationId() + " : " + ReferralUtil.listSize(matchingCmr));
		if (ReferralUtil.listSize(matchingCmr) == ReferralConstants.ONE) {
			nfProcessRequestDTO.setMatchingCmrId(matchingCmr.get(0));
			return true;
		}
		return false;
	}

	private void initiateActivation(NFProcessDTO nfProcessRequestDTO, SsapApplication application) {
		if (AccountTransferCategoryEnum.OE.value().equals(nfProcessRequestDTO.getAccountTransferCategory())) {
			triggerOEEmail(application);
		} else if (AccountTransferCategoryEnum.QE.value().equals(nfProcessRequestDTO.getAccountTransferCategory())) {
			triggerQEEmail(application);
		}

	}
	
	public void initiateActivation(Long applicationId) {
		SsapApplication application = ssapApplicationRepository.findOne(applicationId);
		if ("OE".equalsIgnoreCase(application.getApplicationType())) {
			triggerOEEmail(application);
		} else if ("QEP".equalsIgnoreCase(application.getApplicationType())) {
			triggerQEEmail(application);
		}
	}

	private void triggerOEEmail(SsapApplication application) {
		try {
			LOGGER.info("trigger OE Without AutoLinking AccountActivation for " + application.getId());
			referralActivationNotificationService.triggerOEAccountActivation(application);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append("- Error sending OE Activation/Auto Linking email ").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	private void triggerQEEmail(SsapApplication application) {
		try {
			LOGGER.info("trigger QE Without AutoLinking AccountActivation for " + application.getId());
			referralActivationNotificationService.triggerQEWithoutAutoLinkingAccountActivation(application);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append("- Error sending QE Activation/Auto Linking email ").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

}