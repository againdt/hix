<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="encHouseholdId"><encryptor:enc value='${householdId}'/></c:set>
<c:set var="encHouseholdEmail"><encryptor:enc value='${household.email}'/></c:set>
<c:set var="encHouseholdPhone"><encryptor:enc value='${household.phoneNumber}'/></c:set>

<jsp:include page="sendConsumerActivationLink.jsp">
	<jsp:param value="${encHouseholdId}" name="householdId"/>
	<jsp:param value="${encHouseholdEmail}"  name="householdEmail"/>
	<jsp:param value="${encHouseholdPhone}" name="householdPhone"/>
</jsp:include>
<div class="gutter10-lr">
    <div class="l-page-breadcrumb" style="margin-top:10px;">
        <!--start page-breadcrumb -->
        <div class="row-fluid">
            <ul class="page-breadcrumb">
                <li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/crm/member/managemembers"/>">Members</a></li>
			<li><spring:message code="label.basicInformation"/></li>
            </ul>
            <div style="font-size: 14px; color: red">
                <c:if test="${errorMsg != ''}">
                    <p>
                        <c:out value="${errorMsg}"></c:out>
                    <p />
                </c:if>
                <br>
            </div>
		<h1 class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})</h1> 
        </div>
        <!--  end of row-fluid -->
    </div>
    <!--  end l-page-breadcrumb -->
    
    <div class="row-fluid">
        <div id="sidebar" class="span3">
		           <jsp:include page="../cap/consumerSummaryNavigation.jsp"></jsp:include>
		</div>
        <div id="rightpanel" class="span9" ng-app="resetPasswordApp" ng-cloak>
            <div ui-view></div>
        </div>
    </div>
</div>


<input type="hidden" name="responseJson" id="responseJson" value='${responseJson}'>
<input type="hidden" name="encHouseHoldId" id="encHouseHoldId" value="${encHouseHoldId}">
<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>




<form name="dialogForm" id="dialogForm" method="POST"  action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Member Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body clearfix gutter10-lr">
    <div class="control-group">	
				<div class="controls">
                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
                Through this portal you will be able to take actions on behalf of the member.<br/>
                Proceed to Member view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Consumer View</button>
        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="individual"/>" />
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${householdId}"/>" />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${household.eligLead.name}" />
        <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
    </div>
</div>
</form>
<script>
$(document).ready(function() {
	$("#resetPasswordMenu").addClass("active navmenu");
});
</script>
<script src="<c:url value="/resources/angular/mask.js" />"></script>
<script src="<c:url value="/resources/js/moment.min.js" />"></script>
<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js" />"></script>
<script src="<c:url value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js" />"></script>

<script src="<c:url value="/resources/js/cap/resetPassword/resetPassword.app.js" />"></script>
<script src="<c:url value="/resources/js/cap/resetPassword/resetPassword.router.js" />"></script>
<script src="<c:url value="/resources/js/cap/resetPassword/resetPassword.directive.js" />"></script>
<script src="<c:url value="/resources/js/cap/resetPassword/resetPassword.filter.js" />"></script>
<script src="<c:url value="/resources/js/cap/resetPassword/resetPassword.controller.js" />"></script>

