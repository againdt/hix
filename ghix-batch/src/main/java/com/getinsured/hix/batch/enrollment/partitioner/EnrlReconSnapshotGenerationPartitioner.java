package com.getinsured.hix.batch.enrollment.partitioner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.skip.EnrlReconSnapshotGenerationData;
import com.getinsured.hix.enrollment.repository.IEnrlReconDataRepository;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;



@Component("enrlReconSnapshotGenerationPartitioner")
@Scope("step")
public class EnrlReconSnapshotGenerationPartitioner implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconSnapshotGenerationPartitioner.class);
	
	private String fileIdList;
	private JobService jobService;
	private String fileLevelPartition;
	long jobExecutionId = -1;
	private StepExecution stepExecution;
	private EnrlReconSnapshotGenerationData enrlReconSnapshotGenerationData;
	private IEnrlReconDataRepository enrlReconDataRepository;
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		// TODO Auto-generated method stub
		Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
		try{
		if(EnrollmentUtils.isNotNullAndEmpty(fileIdList)){
			List<Integer> fileList= getFileIdListList(fileIdList);
			Map<String,List<Long>> issuerPolicyIdMap= new HashMap<>();
			boolean perFilePartition= false;
			if(fileLevelPartition!=null){
				perFilePartition=EnrollmentUtils.getBooleanFromYesOrYFlag(fileLevelPartition);
			}
			int partition=0;
			if(perFilePartition){
				
				for (Integer fileId: fileList){
					
					List<Long> enrollmentIds= enrlReconDataRepository.getEnrollmentIdsByFileId(fileId);
					if(enrollmentIds!=null && enrollmentIds.size()>0){
						issuerPolicyIdMap.put(partition+"", enrollmentIds);
						ExecutionContext value = new ExecutionContext();
						value.put("partition", partition);
						partitionMap.put("partition - "+fileId+partition, value);
						partition++;
					}
					
				}
			}else{
				
				List<Long> enrollmentIds= enrlReconDataRepository.getEnrollmentIdsByFileIds(fileList);
				if(enrollmentIds!=null && enrollmentIds.size()>0){
					issuerPolicyIdMap.put(partition+"", enrollmentIds);
					ExecutionContext value = new ExecutionContext();
					value.put("partition", partition);
					partitionMap.put("partition - "+partition, value);
				}
			}
			
			if(enrlReconSnapshotGenerationData!=null){
				enrlReconSnapshotGenerationData.clearEnrollmentIdMap();
				if(issuerPolicyIdMap!=null){
					enrlReconSnapshotGenerationData.putAllToEnrollmentIdMap(issuerPolicyIdMap);
				}
			}
		}else{
			throw new Exception("File Id List is empty");
		}
		
		}catch(Exception e){
			LOGGER.info("Partitioner failed to execute : "+ e.getMessage());
			
			if(stepExecution != null){
				ExecutionContext executionContext = stepExecution.getJobExecution().getExecutionContext();
				executionContext.put("jobExecutionStatus", "failed");
				executionContext.put("errorMessage", e.getMessage());
			}
		}
		
		return partitionMap;
	}


	public JobService getJobService() {
		return jobService;
	}


	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}


	public StepExecution getStepExecution() {
		return stepExecution;
	}


	public void setStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}


	public String getFileIdList() {
		return fileIdList;
	}


	public void setFileIdList(String fileIdList) {
		this.fileIdList = fileIdList;
	}


	public String getFileLevelPartition() {
		return fileLevelPartition;
	}


	public void setFileLevelPartition(String fileLevelPartition) {
		this.fileLevelPartition = fileLevelPartition;
	}



	private List<Integer> getFileIdListList(String fileIds) {
		StringTokenizer stringTok = new StringTokenizer(fileIds,"|");
		List<Integer> fileIdList = new ArrayList<Integer>();
		
		while (stringTok.hasMoreElements()) {
			fileIdList.add( Integer.parseInt(stringTok.nextElement()+""));
		}
		
		return fileIdList;
		
	}


	public IEnrlReconDataRepository getEnrlReconDataRepository() {
		return enrlReconDataRepository;
	}


	public void setEnrlReconDataRepository(IEnrlReconDataRepository enrlReconDataRepository) {
		this.enrlReconDataRepository = enrlReconDataRepository;
	}


	public EnrlReconSnapshotGenerationData getEnrlReconSnapshotGenerationData() {
		return enrlReconSnapshotGenerationData;
	}


	public void setEnrlReconSnapshotGenerationData(EnrlReconSnapshotGenerationData enrlReconSnapshotGenerationData) {
		this.enrlReconSnapshotGenerationData = enrlReconSnapshotGenerationData;
	}
	
	
	
}
