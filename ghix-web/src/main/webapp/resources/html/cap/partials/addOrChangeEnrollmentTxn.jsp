<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil" %>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration" %>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum" %>

<%
    String applyAptcToDental = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.APPLY_APTC_TO_DENTAL);
%>
<div spinning-loader="loader"></div>
	<form novalidate name="addOrChangeEnrollmentTxnForm">
		<div class="header margin20-b">
			<h4>
				<span ng-if="vm.isEditMode === false">
				Enrollment - Add
				</span>
				 <span ng-if="vm.isEditMode === true">
				Enrollment - Change
				</span>
				<span class="pull-right" style="margin-top: -5px;">
					<button ng-click="vm.backToPremiumHistory('back')" class="btn" ng-if="vm.isEditMode === true">Back</button>
					<button ng-click="vm.backToPremiumHistory('cancel')" class="btn" ng-if="vm.isEditMode === false">Back</button>
				</span>
		</div>
		</h4>
		<div class="gutter10 enrollment-history">
			<div class="row-fluid">
				<table class="table table-condensed span6" ng-if="vm.isEditMode === false">
					<tr>
						<th>Enrollment Start Date<img width="10" height="10" alt="Required!"
							src="/hix/resources/images/requiredAsterix.png"
							aria-hidden="true" /></th>
						<td><input type="text" date-validation="date_sameYear"
							class="input-small" name="enrollmentBenefitEffectiveStartDateUI"
							ng-model="vm.enrollment.enrollmentBenefitEffectiveStartDateUI"
							ng-change="vm.updateEnrolleeStartDate()" ui-mask="99/99/9999"
							model-view-value="true" required
							placeholder="MM/DD/YYYY"></td>
					</tr>
					<tr>
						<td colspan="2">
							<span class="error-message" ng-if="addOrChangeEnrollmentTxnForm.enrollmentBenefitEffectiveStartDateUI.$error.required && addOrChangeEnrollmentTxnForm.enrollmentBenefitEffectiveStartDateUI.$dirty">
								Please enter enrollment start date. 
							</span>
							<span ng-if="!addOrChangeEnrollmentTxnForm.enrollmentBenefitEffectiveStartDateUI.$error.required">
								<span class="error-message" ng-if="addOrChangeEnrollmentTxnForm.enrollmentBenefitEffectiveStartDateUI.$error.date">
									The entered value is not a valid date. Please enter the date in
									MM/DD/YYYY format.
								</span>
								<span class="error-message" ng-if="addOrChangeEnrollmentTxnForm.enrollmentBenefitEffectiveStartDateUI.$error.sameYear" > 
									Please enter valid coverage year.
								</span>
							</span>
							
						</td>
					</tr>
				</table>

				<table class="table table-condensed span6" ng-if="vm.isEditMode === false">
					<tr>
						<th>APTC Amount</th>
						<td>$ <input type="text" class="input-small"
							name="aptcAmount"
							ng-model="vm.enrollment.aptcAmount"
                            ng-disabled="<%=applyAptcToDental%> === false && vm.enrollment.planType === 'Dental'"
							maxlength="10" ng-pattern="/^[0-9]+(\.[0-9]{1,4})?$/">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<span class="error-message"
							ng-if="addOrChangeEnrollmentTxnForm.aptcAmount.$invalid">
								Please enter valid APTC amount. </span></td>
					</tr>
				</table>
				
				<table class="table table-condensed span6" ng-if="vm.isEditMode === true">
					<tr>
						<th>Enrollment Transaction Type</th>
						<td>
							<select name="enrollmentLevelTxnType" id="enrollmentLevelTxnType" class="input-medium" ng-model="vm.enrollmentLevelTxnType" ng-change="vm.updateSubscriberTxnType()">
   									<option value="CHANGE">Change</option>
   									<option value="TERM">Term</option>
   							</select>
   						</td>
					</tr>
				</table>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
						<th class="thBorder">&nbsp;</th>
						<th style="white-space: nowrap;" ng-if="vm.isEditMode === true" class="text-center">Reason</th>
						<th style="white-space: nowrap;">Name</th>
						<th style="white-space: nowrap;">Type</th>
						<th>Benefit Effective Date</th>
						<th>Benefit End Date</th>
						<th style="white-space: nowrap;">Member ID</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="enrollee in vm.enrollment.enrolleeDataDtoList | orderBy:[vm.relationshipOrderBy, 'memberId']" ng-if="vm.isEditMode === false || (vm.enrollmentLevelTxnType === 'CHANGE' || enrollee.relationshipToSubscriber === 'Self')">
						<td>
						<label class="checkbox inline"> 
							<span ng-if="vm.isEditMode === false">
								<input type="checkbox" ng-model="enrollee.txnType" ng-checked="true"
									ng-true-value="true" ng-false-value="false"
									ng-init='enrollee.txnType="true"' ng-disabled="enrollee.relationshipToSubscriber === 'Self'">
							</span>
							<span ng-if="vm.isEditMode === true">
								<select name="enrollee.txnType" ng-model="enrollee.txnType" class="input-medium" ng-disabled="enrollee.relationshipToSubscriber === 'Self'" ng-change="vm.setTxnReason(enrollee)">
   									<option value="CHANGE">Change</option>
   									<option value="TERM">Term</option>
   									
   									<option value="ADD" ng-if="enrollee.enrolleeEffectiveEndDateUI.substring(0, 5) === '12/31'">Add</option>
   								</select>
   							</span>
						</label>
						</td>
						<td ng-if="vm.isEditMode === true">
							<label class="checkbox inline"> 
								<select name="enrollee.txnReason" ng-model = "enrollee.txnReason" class="input-medium"  ng-change="vm.setIsTermReasonNonPaymentFlag()" >
	  									<option value="EC" ng-if="enrollee.txnType == 'ADD'">Member Benefit Selection</option>
										<option value="29" ng-if="enrollee.txnType == 'CHANGE'">Benefit Selection</option>
										<option value="AI" ng-if="enrollee.txnType != 'ADD'">No Reason Given</option>
	  									<option value="59" ng-if="enrollee.txnType != 'CHANGE' && enrollee.txnType != 'ADD' && enrollee.relationshipToSubscriber === 'Self'">Non Payment</option>
	  									<option value="14" ng-if="enrollee.txnType != 'CHANGE' && enrollee.txnType != 'ADD'">Voluntary Withdrawal</option>
	  							</select>
							</label>
						</td>		
						<td>{{enrollee.firstName}} {{enrollee.middleName}}
							{{enrollee.lastName}}</td>
						<td>{{enrollee.relationshipToSubscriber}}</td>
						<td>{{enrollee.enrolleeEffectiveStartDateUI}}</td>
						<td>{{enrollee.enrolleeEffectiveEndDateUI}}</td>
						<td>{{enrollee.memberId}}</td>
					</tr>
				</tbody>
			</table>
			
			<p class="alert alert-info" ng-if="vm.isEditMode === true && vm.enrollmentLevelTxnType === 'TERM'">
				<strong>Note:</strong> Only Subscriber record is sent in 834 for TERM transactions.				
			</p>
			<div id = "premiumPaidThroughDate" class = "row-fluid" ng-if="vm.isTermReasonNonPayment === true" >
				<table class="table table-condensed span6">
					<tr>
						<th>Enter the last premium paid through date :<img width="10" height="10" aria-hidden="true" /></th>
						<td><input type="text" 
							class="input-small" name="enrollmentLastPremPaidThroughDateUI"
							ng-model="vm.enrollmentLastPremPaidThroughDateUI"
							ng-change="vm.validateLastPremPaidThroughDate()" ui-mask="99/99/9999"
							model-view-value="true" 
							placeholder="MM/DD/YYYY"
							date-validation="date"></td>
					</tr>
					<tr>
						<td colspan="2">
							<span class="error-message" ng-if="addOrChangeEnrollmentTxnForm.enrollmentLastPremPaidThroughDateUI.$error.required && addOrChangeEnrollmentTxnForm.enrollmentLastPremPaidThroughDateUI.$dirty">
								Please enter the last premium paid through date.
							</span>
							<span ng-if="!addOrChangeEnrollmentTxnForm.enrollmentLastPremPaidThroughDateUI.$error.required">
								<span class="error-message" ng-if="addOrChangeEnrollmentTxnForm.enrollmentLastPremPaidThroughDateUI.$error.date">
									The entered value is not a valid date. Please enter the date in
									MM/DD/YYYY format.
								</span>
								<span class="error-message" ng-if="addOrChangeEnrollmentTxnForm.enrollmentLastPremPaidThroughDateUI.$error.lastPremDateWithinCoverage" > 
									Please enter a valid date within the enrollment coverage period.
								</span>
							</span>
							
						</td>
					</tr>
				</table>
			</div>
		
			<div class="addOrChangeTxnDiv">
				<img width="10" height="10" alt="Required!"
							src="/hix/resources/images/requiredAsterix.png"
							aria-hidden="true" /></th>
				<textarea class="overrideTextarea margin15-t"
					ng-model="vm.addOrChangeComment"
					placeholder="Please explain the change you are making and why"
					maxlength="4000" required></textarea>
				<span>{{4000 - vm.addOrChangeComment.length}} left</span>
			</div>
			<div>
			<p class="alert alert-info margin10-t" ng-if="vm.isEditMode === false">
				<strong>Note:</strong> 
				The APTC amount(s) are not validated for accuracy. Before updating the APTC amounts, 
				make sure the entered APTC amount and the APTC already allocated on associated Dental Enrollment 
				for the same month does not exceed the maximum APTC amount that the consumer is eligible for.
			</p>
				<div class="margin15-t margin20-b txt-right">
					<label class="checkbox inline"> 
						<input type="checkbox" ng-model="vm.addOrChangeSendIn834" ng-checked="vm.isTermReasonNonPayment === false" ng-true-value="true"
						ng-false-value="false" ng-init='vm.addOrChangeSendIn834=true' ng-disabled = "vm.isTermReasonNonPayment === true"> 
						<span>Send In 834</span>
					</label>
					<button ng-click="vm.validateAndSaveAddOrChangeTxn()" class="btn btn-primary margin20-l"
						ng-disabled="addOrChangeEnrollmentTxnForm.$invalid">Submit</button>
					<button ng-click="vm.backToPremiumHistory('cancel')" class="btn margin10-l">Cancel</button>
				</div>
			<p class="alert alert-info margin10-t" ng-if="vm.isTermReasonNonPayment === true">
				<strong>Note:</strong> 
				"Send in 834" is disabled when Non-Payment is the reason for termination.
			</p>
			<p class="alert alert-info margin10-t" ng-if="vm.isTermReasonNonPayment === true">
				While not mandatory, it is strongly advisable to add the premium last paid to date end in the field.
			</p>

			</div>
		</div>
	</form>
<div id="addOrChangeResultModal" class="modal hide fade" tabindex="-1" role="dialog" 
	aria-labelledby="addOrchangeResultModalHeader" aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="vm.backToPremiumHistory()">x</button>
		<h3 id="addOrchangeResultModalHeader">
			<span ng-if="vm.isUpdatedSuccess === false">Error Occured</span> <span
				ng-if="vm.isUpdatedSuccess === true">Success</span>
		</h3>
	</div>
	<div class="modal-body">{{vm.addOrChangeResultMessage}}</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" ng-click="vm.backToPremiumHistory(vm.isUpdatedSuccess)">Close</button>
	</div>
</div>
