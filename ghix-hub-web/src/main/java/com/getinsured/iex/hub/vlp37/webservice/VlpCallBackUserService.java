package com.getinsured.iex.hub.vlp37.webservice;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.getinsured.iex.hub.platform.FileBasedConfiguration;

@Component
@Qualifier("vlpCallBackUserService")
public class VlpCallBackUserService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		FileBasedConfiguration config = FileBasedConfiguration.getConfiguration();
		String userName = config.getProperty("fdh.inbound.username");
		String password = config.getProperty("fdh.inbound.password");
		VLPCallbackUser user = new VLPCallbackUser(userName, password);
		return user;
	}
}
