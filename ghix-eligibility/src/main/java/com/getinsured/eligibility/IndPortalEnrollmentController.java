package com.getinsured.eligibility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.indportal.enrollment.api.IndPortalEnrollmentUtility;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.dto.Ind57Mapping;
import com.getinsured.iex.indportal.dto.IndDisenroll;

/**
 * 
 * @author Gajulapalli_K
 *
 */

@Controller
@RequestMapping("/indportal/enrollment")
public class IndPortalEnrollmentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(IndPortalEnrollmentController.class);

	@Autowired
	private IndPortalEnrollmentUtility indPortalEnrollmentUtility;
			
	@RequestMapping(value = "/disenrollment", method = RequestMethod.POST)
	@ResponseBody	
	public String individualDisenrollment(@RequestBody IndDisenroll disEnReq) throws GIException {		
		String responseVal = "failure";
		LOGGER.debug("Invoking individualDisenrollment method in IndPortalEnrollmentController");
		try {			
			responseVal = indPortalEnrollmentUtility.invokeIndDisenrollment(disEnReq);
		}catch(Exception e) {			
			LOGGER.error("Error during disenrollment ", e);
			responseVal = "failure";
		}		
		return responseVal;
	}

	@RequestMapping(value = "/disenrollmentforsep", method = RequestMethod.POST)
	@ResponseBody	
	public String individualDisenrollmentForSep(@RequestBody IndDisenroll indDisenroll) throws GIException {		
		String responseVal = "failure";
		LOGGER.debug("Invoking individualDisenrollmentForSep method in IndPortalEnrollmentController");
		try {			
			responseVal = indPortalEnrollmentUtility.invokeIndDisenrollmentForSep(indDisenroll);
		}catch(Exception e) {
			LOGGER.error("Error during sep disenrollment ", e);
			responseVal = "failure";
		}		
		return responseVal;
	}

	@RequestMapping(value = "/adminupdateenrollment", method = RequestMethod.POST)
	@ResponseBody
	public String adminUpdateEnrollment(@RequestBody Ind57Mapping ind57Mapping) throws GIException {
		String responseVal = "failure";
		try {
			responseVal =  indPortalEnrollmentUtility.invokeAdminUpdateEnrollment(ind57Mapping);
		}catch(Exception e) {		
			responseVal = "failure";
		}
		return responseVal;
	}

}
