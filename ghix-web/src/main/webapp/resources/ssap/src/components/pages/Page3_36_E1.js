import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import IncomeItem from '../summary/IncomeItem';
import PageHeader from '../headers/PageHeader';
import SummaryItem from '../summary/SummaryItem';
import currencyTools from '../../utils/currencyTools';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import transformDropdownOptionsToObject from '../../utils/transformDropdownOptionsToObject';
import incomeCalculator from '../../utils/incomeCalculator';
import {getCurrentHouseholdMember} from '../../utils/getCurrentHouseholdMember';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import {FREQUENCY_TYPES} from '../../constants/frequencyTypes';

// Components

// Utils

// Constants


const Page3_36_E1 = props => {

    const {
        adjustedFlow,
        data: {
            singleStreamlinedApplication: {
                coverageYear
            }
        },
        householdMembers,
        page
    } = props;

    const {
        hhmIndex,
        currentHhm
    } = getCurrentHouseholdMember(adjustedFlow, page, householdMembers);

    const {
        annualTaxIncome: {
            projectedIncome,
            reportedIncome
        },
        incomes,
        expenses,
        name,
    } = currentHhm;

    const {
        buttonContainer: ButtonContainer
    } = props;

    const INCOME_SOURCES_LIST = transformDropdownOptionsToObject(DROPDOWN_OPTIONS.INCOME_SOURCES);
    const DEDUCTION_SOURCES_LIST = transformDropdownOptionsToObject(DROPDOWN_OPTIONS.DEDUCTION_SOURCES);
    const FREQUENCIES_LIST = transformDropdownOptionsToObject(DROPDOWN_OPTIONS.FREQUENCY);

    const getIncomeValueForRender = () => {
        const valueForRender = projectedIncome ?
            projectedIncome :
            reportedIncome;

        return valueForRender ? currencyTools.getMoneyFormat(valueForRender, true) : '$0.00';
    };

    const monthlyIncome = incomeCalculator.getIncomeOnMonthBase(incomes, expenses);

    return (
        <Fragment>
            <div className="subsection">
                <PageHeader
                    text={
                        <span>
                            {`${normalizeNameOfPerson(name)}'s income summary`}
                        </span>
                    }
                    headerClassName={'usa-heading-alt'}
                    additionalElement={(
                        <ButtonContainer
                            type="editButton_secondary"
                            args={[hhmIndex]}
                        />
                    )}
                    additionalElementClassName={'header-btn'}
                />
                <SummaryItem
                    category={`${normalizeNameOfPerson(name)}'s total income in ${coverageYear}`}
                    value={getIncomeValueForRender()}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label',
                        divClass2: 'summary__value'
                    }}

                />


                {
                    incomes.length > 0 &&

                    <Fragment>
                        <PageHeader
                            text="Current monthly income"
                            headerClassName={'usa-heading-alt summary__header--no-border'}
                        />
                        <IncomeItem
                            type={'Income Source'}
                            amount={'How much'}
                            frequency={'How often'}
                            design='header'
                        />
                        {
                            incomes.map((income, idx) => {
                                const {
                                    type,
                                    sourceName,
                                    amount,
                                    frequency
                                } = income;

                                return (
                                    <IncomeItem
                                        key={`${type}_${idx}`}
                                        type={INCOME_SOURCES_LIST[type]}
                                        sourceName={sourceName}
                                        amount={currencyTools.getMoneyFormat(incomeCalculator.adjustAmountByFrequency(amount, frequency), true)}
                                        frequency={FREQUENCIES_LIST[FREQUENCY_TYPES.YEARLY]}
                                    />
                                );
                            })
                        }
                    </Fragment>
                }

                {
                    expenses.length > 0 &&

                    <Fragment>
                        <PageHeader
                            text="Income deductions"
                            headerClassName={'usa-heading-alt summary__header--no-border'}
                        />
                        <IncomeItem
                            type={'Deduction Source'}
                            amount={'How much'}
                            frequency={'How often'}
                            design='header'
                        />
                        {
                            expenses.length > 0 &&

                            expenses.map((expense, idx) => {
                                const {
                                    type,
                                    amount,
                                    frequency
                                } = expense;

                                return (
                                    <IncomeItem
                                        key={`${type}_${idx}`}
                                        type={DEDUCTION_SOURCES_LIST[type]}
                                        amount={currencyTools.getMoneyFormat(incomeCalculator.adjustAmountByFrequency(amount, frequency), true)}
                                        frequency={FREQUENCIES_LIST[FREQUENCY_TYPES.YEARLY]}
                                    />
                                );
                            })
                        }
                    </Fragment>
                }

                {
                    incomes.find(i => i.tribalAmount > 0) &&

                    <Fragment>
                        <PageHeader
                            text="Amounts from Tribal Sources"
                            headerClassName={'usa-heading-alt summary__header--no-border'}
                        />
                        <IncomeItem
                            type={'Tribal Income Source'}
                            amount={'How much'}
                            frequency={'How often'}
                            design='header'
                        />
                        {
                            incomes.map((income, idx) => {
                                const {
                                    type,
                                    sourceName,
                                    tribalAmount,
                                    frequency
                                } = income;

                                if(tribalAmount !== 0) {
                                    return (
                                        <IncomeItem
                                            key={`${type}_${idx}`}
                                            type={INCOME_SOURCES_LIST[type]}
                                            sourceName={sourceName}
                                            amount={currencyTools.getMoneyFormat(incomeCalculator.adjustAmountByFrequency(tribalAmount, frequency), true)}
                                            frequency={FREQUENCIES_LIST[FREQUENCY_TYPES.YEARLY]}
                                        />
                                    );
                                }
                            })
                        }
                    </Fragment>
                }
            </div>

            {
                monthlyIncome !== 0 &&

                <div className="usa-alert usa-alert-info alert--info">
                    <div className="usa-alert-body" >
                        <p className="usa-alert-text">
                            Current monthly income <br/>
                            { currencyTools.getMoneyFormat(monthlyIncome, true) } <br/>
                            This is based on your income sources above. We add them together for a year based
                            on how often you get each type, and then divided by 12 for a monthly amount
                        </p>
                    </div>
                </div>
            }

        </Fragment>
    );
};

const mapStateToProps = state => ({
    adjustedFlow: state.adjustedFlow,
    data: state.data,
    exception: state.exception,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    page: state.page
});

Page3_36_E1.defaultProps = {
    name: 'Page3_36_E1'
};

export default connect(mapStateToProps)(Page3_36_E1);
