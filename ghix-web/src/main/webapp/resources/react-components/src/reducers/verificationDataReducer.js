import update from 'immutability-helper';
import { ActionTypes } from '../constants/constants';

const INITIAL_STATE = {
    selectedApplicantIndex: 0
};

const verificationDataReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case (ActionTypes.RECEIVED_VERIFICATION_DATA_SUCCESS):
            return {
                ...state,
                ...payload
            };
        case (ActionTypes.SELECT_APPLICANT_INDEX):
            return {
                ...state,
                selectedApplicantIndex: payload
            };

        case (ActionTypes.UPDATED_VERIFICATION_DATA):
            const { inputType, value } = payload;
            const selectedApplicantIndex = state.selectedApplicantIndex;

            return update(state, {
                eventVerificationDetailsDTO: {
                    [selectedApplicantIndex]: {
                        [inputType]: { $set: value }
                    }
                }
            });

        default:
            return state;
    }
};

export default verificationDataReducer;