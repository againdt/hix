package com.getinsured.hix.cap.consumer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.cap.consumer.dto.ConsumerDto;
import com.getinsured.hix.cap.util.AddConsumerAjaxResponse;
import com.getinsured.hix.cap.util.AjaxResponseBase;
import com.getinsured.hix.cap.util.CapRequestValidatorUtil;
import com.getinsured.hix.cap.util.UIUtilities;
import com.getinsured.hix.cap.util.ValidationUtility;
import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.crm.util.ConsumerEventsUtil;
import com.getinsured.hix.dto.broker.BrokerRequestDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.BrokerResponse;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.RestfulResponseException;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.screener.util.ScreenerUtil;
import com.getinsured.timeshift.TimeShifterUtil;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@Api(value = "ConsumerController operations", basePath = "", description = "All operations for Consumer")
@RequestMapping(value="cap/consumer")
public class ConsumerCRUDController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerCRUDController.class);
	private static final String FLOW = "flow";
	private static final String HOUSEHOLD = "household";
	private static final int THREE = 3;
	private static final int SIX = 6;
	private static final int TEN = 10;
	 private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	    private static final String EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER = "Exception occurred while getting logged in user";
	
	//TODO: autowire it.
	static ObjectMapper sMapper = new ObjectMapper();
	@Autowired	private ConsumerPortalUtil consumerPortalUtil;
	@Autowired	@Qualifier("screenerUtil") private ScreenerUtil phixUtil;
	@Autowired	private UserService userService;
	@Autowired	private GhixRestTemplate ghixRestTemplate;
	@Autowired  private AccountActivationService accountActivationService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private RestTemplate restTemplate;
	@Autowired private BrokerService brokerService;
	@Autowired private BrokerMgmtUtils brokerMgmtUtils;
	@Autowired private EntityUtils entityUtils;
	@Autowired private ConsumerEventsUtil consumerEventsUtil;
	@Autowired private CapRequestValidatorUtil capRequestValidatorUtil;
	@Autowired
	private RestTemplate mvcRestTemplate;
	
//TODO: Ensure permission - only CSR and up should be able to do this..
	@GiAudit(transactionName = "Create New Consumer", eventType = EventTypeEnum.CAP_MANAGE_HOUSEHOLD, eventName = EventNameEnum.PII_READ)
	@ApiOperation(value = "Create new Consumer", notes = "This method will create a new consumer household.")
	@RequestMapping(value = "/addconsumer", method = {RequestMethod.GET, RequestMethod.POST})
	@PreAuthorize("hasPermission(#model, 'ADD_NEW_CONSUMER')")
	public String addConsumer(Model model,
			HttpServletRequest request) {
		
		//HIX-113429 Redirect Brokers/CSR to static page from add new consumer
		String preventAddConsumer = DynamicPropertiesUtil.getPropertyValue("global.preventAddConsumer");
		if(StringUtils.equalsIgnoreCase(preventAddConsumer, "true")) {
			return "account/nosignup";
		}
		try {
			boolean isCertified = isUserCertified(request);
			if (! isCertified) {
				throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
			}

		} catch(AccessDeniedException accessDeniedException) {
			 LOGGER.error("Exception occured while accepting designation request for Employer / Individual : ", accessDeniedException);
			 throw accessDeniedException;
		} catch (InvalidUserException ex) {
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_GETTING_LOGGED_IN_USER, ex);
		} catch(Exception e) {
			LOGGER.error("Error in newConsumer()"+e.getMessage());
		}
		
		
		String userActiveRoleName = request.getSession().getAttribute("userActiveRoleName").toString();
		if(userActiveRoleName.equalsIgnoreCase("CSR")){
			model.addAttribute("showPermissionToCall","Y");
		}else{
			model.addAttribute("showPermissionToCall","N");
		}

		
		
		
		
		 //Check if the flow is referral
		String applicationJson = request.getParameter("applicationJson");
		model.addAttribute("applicationJson", (applicationJson != null) ? applicationJson : StringUtils.EMPTY);
		model.addAttribute("csrReferralFlow", (applicationJson != null) ? "csrReferralFlow" : StringUtils.EMPTY);
		GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
		return "cap/consumer/addconsumer";
	}

	// TODO: Ensure permission - only CSR and up should be able to do this..
	@GiAudit(transactionName = "Create New Consumer", eventType = EventTypeEnum.CAP_MANAGE_HOUSEHOLD, eventName = EventNameEnum.PII_WRITE)
	@ApiOperation(value = "New Consumer Submit", notes = "This method will create a new consumer household.")
	@RequestMapping(value = "/addconsumersubmit", method = { RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ADD_NEW_CONSUMER')")
	@ResponseBody
	// @PreAuthorize("hasPermission(#model, 'CSR_ROLE')")
	//NOTE: Affiliates and flows are not part of this scenario.
	public AjaxResponseBase addConsumerSubmit(@RequestBody String consumerJson) {
		long startTime = TimeShifterUtil.currentTimeMillis();
		
		try {
			LOGGER.debug("Start ConsumerCRUDController::addConsumerSubmit");
			ConsumerDto consumer = sMapper.readValue(consumerJson,ConsumerDto.class);
			
			if (!capRequestValidatorUtil.validateAddConsumerSubmit(consumer)) {
				return generateAddConsumerResponse(false, "Validation errors. Please fix before proceeding.", null);
			}
			
			boolean isInvalid = false;
			String message = StringUtils.EMPTY;
			if(checkExactHouseholdExists(consumer)) {
				isInvalid = true;
				message += "INVALID_CONSUMER";
			}
			if(!ValidationUtility.isNullOrEmpty(consumer.getEmailAddress())){
				if(checkEmail(consumer.getEmailAddress())) {
					isInvalid = true;
					message += "INVALID_EMAIL";
				}	
			}
			
			if(isInvalid) {
				return generateAddConsumerResponse(false, message, null);
			}
			
			// TODO: Break the Household-EligLead FK relationship? 'Coz in the case of States, we do not have any ELIG_LEAD entry in certain cases..
			EligLead dummyEligLeadRecord = persistEligLead(consumer);
			//HIX-116941 : store zip code while creating member individual
			Location location = new Location();
			location.setZip(consumer.getZipcode());
			Household dbHousehold = persistHousehold(consumer,dummyEligLeadRecord,location);
			
			if(!ValidationUtility.isNullOrEmpty(consumer.getEmailAddress()))
			{
			 generateActivationLink(dbHousehold);	
			}
			triggerDesignation(dbHousehold);
			consumerPortalUtil.logCsrOverridesEvents(dbHousehold.getId(), ConsumerPortalUtil.SSN_OVERRIDE, "NA", consumerPortalUtil.getSsnLogValueFromHousehold(dbHousehold));
			
			GiAuditParameterUtil.add(new GiAuditParameter("HouseholdID", dbHousehold.getId()),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			return generateAddConsumerResponse(true, "Success", dbHousehold);
		} catch (Exception e) {
			LOGGER.error("Error while adding consumer:" + e.getStackTrace());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while adding consumer"));
			return generateAddConsumerResponse(false, e.getMessage(), null);
		}
		finally
		{
			UIUtilities.printPerf("addConsumerSubmit",startTime);
	//		LOGGER.info( String.format("[PERF] ConsumerCRUDController:addConsumerSubmit: %s ms", UIUtilities.getElapsedTimeInMillis(startTime)));
		}
	}
	
	
	
	private boolean checkEmail(String Email) {
		LOGGER.info("checkEmail : START");

		// AccountUser userNameObj = null;
		boolean ifEmailExists = false;

		try {

			ResponseEntity<Boolean> appDetailsEntity = mvcRestTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.CHECKEMAILIFEXIST,Email, Boolean.class);
			ifEmailExists = appDetailsEntity.getBody();

		} 
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Unable to check if email exists.email="+Email,ex.getMessage());	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. input = "+Email, ex.getMessage());
				break;
			}
		}

		return ifEmailExists;
	}
	
	
	@RequestMapping(value = "/sendActivationLink", method = { RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ADD_NEW_CONSUMER')")
	@ResponseBody public String sendActivationLink(@RequestParam String householdId, @RequestParam String email, @RequestParam String phone){
		String errorMsg = StringUtils.EMPTY;
		
		if(StringUtils.isEmpty(householdId) || StringUtils.isEmpty(email) || StringUtils.isEmpty(phone)) {
			errorMsg = "Invalid Parameters.";
		} else {
			try {
				Household household = consumerPortalUtil.getHouseholdRecord(Integer.parseInt(householdId));
				
				boolean update = false;
				if(StringUtils.isEmpty(household.getEmail()) && StringUtils.isNotEmpty(email)) {
					household.setEmail(email);
					update = true;
				}
				if(StringUtils.isEmpty(household.getPhoneNumber()) && StringUtils.isNotEmpty(phone)) {
					household.setPhoneNumber(phone);
					update = true;
				}
				if(update) {
					household = consumerPortalUtil.saveHouseholdRecord(household);
				}
				
				generateActivationLink(household);
			} catch (Exception e) {
				LOGGER.error("Unable to send the activation link: ", e);
				errorMsg = "Unable to send activation link to consumer";
			}
		}
		return errorMsg;
	}
	
	private  void generateActivationLink(Household household) throws InvalidUserException, NumberFormatException, GIException {
		 
			 		AccountUser user = userService.getLoggedInUser();
					
					Map<String,String> consumerActivationDetails = new HashMap<String, String>();
					consumerActivationDetails.put("consumerName", household.getFirstName()+" " +household.getLastName());
					consumerActivationDetails.put("exchangeName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					consumerActivationDetails.put("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
					consumerActivationDetails.put("exchangeURL",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
					consumerActivationDetails.put("emailType","consumerAccountActivationEmail");
					consumerActivationDetails.put("expirationDays",String.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER)));

					CreatedObject createdObject = new CreatedObject();
					createdObject.setObjectId(household.getId());
					createdObject.setEmailId(household.getEmail());
					createdObject.setRoleName(GhixRole.INDIVIDUAL.toString());
					createdObject.setPhoneNumbers(Arrays.asList(household.getPhoneNumber()));
					createdObject.setFullName(household.getFirstName() + " " + household.getLastName());
					createdObject.setFirstName(household.getFirstName());
					createdObject.setLastName(household.getLastName());				
					createdObject.setCustomeFields(consumerActivationDetails);
					CreatorObject creatorObject = new CreatorObject();
					creatorObject.setObjectId(user.getId());
					creatorObject.setFullName(user.getFullName());
					creatorObject.setRoleName(GhixRole.ADMIN.toString());

					accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject, Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER)));
					
					LOGGER.info("Created activation record for:[UserId,HouseHoldId]:["+SecurityUtil.sanitizeForLogging(""+user.getId()) +","+ SecurityUtil.sanitizeForLogging(""+household.getId())+"]");
			}

	//Trigger Designation.. )
	//TODO: Can we handle the InvalidUserException gracefully?
	private void triggerDesignation(Household individual)
			throws InvalidUserException {

		try {
			AccountUser loggedInUser = userService.getLoggedInUser();
			boolean isAssister = userService.hasUserRole(loggedInUser,
					RoleService.ASSISTER_ROLE);
			boolean isBroker = userService.hasUserRole(loggedInUser,
					RoleService.BROKER_ROLE);
			boolean isEntityAdmin = userService.hasUserRole(loggedInUser,
					RoleService.ENTITY_ADMIN_ROLE);
			boolean isBrokerAdmin = userService.hasUserRole(loggedInUser,
					RoleService.BROKER_ADMIN_ROLE);
			boolean isExchangAdmin = userService.hasUserRole(loggedInUser,
					RoleService.ADMIN_ROLE);


			if (loggedInUser.getActiveModuleName().equalsIgnoreCase(RoleService.ASSISTER_ROLE) && (isAssister || isEntityAdmin || isExchangAdmin)) { // We have separate module deployed for Broker and // Assister so we need to have this if/else
				AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
				assisterRequestDTO.setIndividualId(individual.getId());
				assisterRequestDTO.setUserId(loggedInUser.getId());
				assisterRequestDTO.setEsignName(loggedInUser.getFullName());
				assisterRequestDTO.setId(loggedInUser.getActiveModuleId());
				assisterRequestDTO
						.setDesigStatus(DesignateAssister.Status.Active);
				String requestXML = GhixUtils.getXStreamStaxObject()
						.toXML(assisterRequestDTO);
				// This goes to ghix-entity
						String response = ghixRestTemplate
						.postForObject(
								GhixEndPoints.EnrollmentEntityEndPoints.DESIGNATE_ASSISTER,
								requestXML, String.class); // Rest call for
															// designation
				AssisterResponseDTO assisterResponseDTO = (AssisterResponseDTO) GhixUtils
						.getXStreamStaxObject().fromXML(response);
				LOGGER.info("Result of Assister-Designation for user:"
						+ SecurityUtil.sanitizeForLogging(""+loggedInUser.getId()) + " is:"
						+ SecurityUtil.sanitizeForLogging(""+assisterResponseDTO.getResponseCode()) + ", Desc:"
						+ SecurityUtil.sanitizeForLogging(assisterResponseDTO.getResponseDescription()));

			} else if (loggedInUser.getActiveModuleName().equalsIgnoreCase(RoleService.BROKER_ROLE) && (isBroker || isBrokerAdmin || isExchangAdmin)) { 
				// Simillar code here that will go to ghix-broker
				BrokerRequestDTO brokerRequestDTO = new BrokerRequestDTO();
				brokerRequestDTO.setIndividualId(individual.getId());
				brokerRequestDTO.setUserId(userService.getLoggedInUser()
						.getId());
				brokerRequestDTO.setEsignName(userService.getLoggedInUser()
						.getFullName());
				brokerRequestDTO.setId(loggedInUser.getActiveModuleId());
				brokerRequestDTO.setDesigStatus(DesignateBroker.Status.Active);
				String requestXML = GhixUtils.getXStreamStaxObject()
						.toXML(brokerRequestDTO);
				String response = ghixRestTemplate.postForObject(GhixEndPoints.BrokerServiceEndPoints.BROKER_DESIGNATE_INDIVIDUAL, requestXML, String.class);
				BrokerResponse brokerResponseDTO = (BrokerResponse) GhixUtils
						.getXStreamStaxObject().fromXML(response);

				LOGGER.info("Result of Broker-Designation for user:"
						+ SecurityUtil.sanitizeForLogging(""+loggedInUser.getId()) + " is:"
						+ SecurityUtil.sanitizeForLogging(""+brokerResponseDTO.getResponseCode()) + ", Desc:"
						+ SecurityUtil.sanitizeForLogging(brokerResponseDTO.getResponseDescription()));
			} else {
				LOGGER.info("Not triggering designation flow for User :"
						+ SecurityUtil.sanitizeForLogging(""+loggedInUser.getId())
						+ " since does not have appropriate role.");
			}
		} catch (Exception ex) {
			LOGGER.error("Ignoring error during triggerDesignation. Error:"
					+ ex.getMessage());
			LOGGER.error(ex.getStackTrace().toString());
		}
	}

	private EligLead persistEligLead(ConsumerDto consumer ) throws InvalidUserException {
		EligLead emptyEligLeadRecord = new EligLead();
		emptyEligLeadRecord.setEmailAddress(consumer.getEmailAddress());
		emptyEligLeadRecord.setCreatedBy(userService.getLoggedInUser().getId());
		emptyEligLeadRecord.setName(consumer.getFullName());
		emptyEligLeadRecord.setStage(EligLead.STAGE.WELCOME);
		emptyEligLeadRecord = phixUtil.createEligLeadRecord(emptyEligLeadRecord);
		if (emptyEligLeadRecord == null) {
		 	throw new RuntimeException("[100] Could not persist to database. Please contact admin.");
		}
		LOGGER.debug("Created & persisted empty EligLead record. EligLeadId="+SecurityUtil.sanitizeForLogging(""+emptyEligLeadRecord.getId()));
		return emptyEligLeadRecord;
	}

	private Household persistHousehold(ConsumerDto consumer,
			EligLead dummyEligLeadRecord, Location location) throws ClientProtocolException,
			IOException, URISyntaxException, ParseException {
		Household houseHoldRecord = mapConsumerDto2HouseHold(consumer);
		houseHoldRecord.setEligLead(dummyEligLeadRecord);
		houseHoldRecord.setPrefContactLocation(location);
		Household dbHousehold = consumerPortalUtil.saveHouseholdRecord(houseHoldRecord);
		LOGGER.debug("[101] Created & persisted Household record. HouseHoldId="+SecurityUtil.sanitizeForLogging(""+dbHousehold.getId()));
		return dbHousehold;
	}
private AddConsumerAjaxResponse generateAddConsumerResponse(boolean isSuccess, String message, Household dbHousehold)
{
	AddConsumerAjaxResponse retVal  = new AddConsumerAjaxResponse(isSuccess,message);
	if(isSuccess)//TODO: ugly- figure out if we can handle boolean in angular
	{
		String encryptCmrId =  ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(dbHousehold.getId()));
		retVal.setHouseholdId(encryptCmrId);
		retVal.setFirstName(dbHousehold.getFirstName());
		retVal.setLastName(dbHousehold.getLastName());
	}
	
	LOGGER.info("End ConsumerCRUDController:addConsumerSubmit -  Response Message:"+SecurityUtil.sanitizeForLogging(retVal.toString()));
	return retVal;
}
	private Household mapConsumerDto2HouseHold(ConsumerDto consumer) throws ParseException {
		 Household retVal = new Household();
 		 retVal.setConsentToCallback(getHouseholdFlag(consumer.getCallPermission()));
		 retVal.setFirstName(consumer.getFirstName());
		 retVal.setLastName(consumer.getLastName());
		 retVal.setBirthDate(consumer.getDobAsDate());
		 retVal.setZipCode(consumer.getZipcode());
		 retVal.setEmail(consumer.getEmailAddress());
		 retVal.setPhoneNumber(consumer.getPhoneNumber());
		 retVal.setSsnNotProvidedReason("NO".equalsIgnoreCase(consumer.getSsnRequired()) ? consumer.getSsnNotReqdReasons() : "");// store reason only in case SSN has not been provided.
		 
		return retVal;
	}

	private Household.YESNOFLAG getHouseholdFlag(String callPermission)
	{
		ValidationUtility.throwIfEmpty(callPermission, "Enter call permission ( Yes / No )");
		return callPermission.toLowerCase().equals("yes")?Household.YESNOFLAG.YES:Household.YESNOFLAG.NO;
	}
	
	/**
	 * Method to check permission to Create New Individual for Agent (Broker) and Counselor (Assister)
	 * 
	 * @author Kunal Dav
	 * @since 06/10/2015
	 * 
	 * @param HttpServletRequest request
	 * @return boolean isCertified
	 */
	private boolean isUserCertified(HttpServletRequest request) throws InvalidUserException {
		boolean isCertified = true;
		
		String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
		boolean isUserSwitched =  ((isUserSwitchToOtherView != null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)) == true) ? true : false;
		
		int userId = 0;
		String roleName = StringUtils.EMPTY;
		AccountUser user = userService.getLoggedInUser();
		if(user == null) {
			throw new InvalidUserException();
		}
		
		if(isUserSwitched) {
			userId = user.getActiveModuleId();
			roleName = user.getActiveModuleName() != null ? user.getActiveModuleName() : "";
		} else {
			userId = user.getId();
			roleName = userService.getDefaultRole(user).getName();
		}
		
		switch(roleName.toUpperCase()) {
			case RoleService.BROKER_ROLE : 
				isCertified = brokerMgmtUtils.isBrokerCertified(userId, isUserSwitched);
				break;
			case RoleService.ASSISTER_ROLE : 
				isCertified = entityUtils.isAssisterCertified(userId);
				break;
			default : 
		}
		
		
		return isCertified;
	}
	
	private boolean checkExactHouseholdExists(ConsumerDto consumer) {
		LOGGER.info("checkExactHouseholdExists : START");

		boolean ifExactHouseholdExists = false;

		try {
			
			Household household = new Household();
			household.setFirstName(consumer.getFirstName());
			household.setLastName(consumer.getLastName());
			household.setZipCode(consumer.getZipcode());
			household.setBirthDate(consumer.getDobAsDate());

			ResponseEntity<Boolean> appDetailsEntity = mvcRestTemplate.postForEntity(GhixEndPoints.ConsumerServiceEndPoints.CHECK_EXACT_HOUSEHOLD_EXISTS, household, Boolean.class);
			ifExactHouseholdExists = appDetailsEntity.getBody();

		}
		catch(RestfulResponseException ex){
			switch(ex.getHttpStatus()){
			case INTERNAL_SERVER_ERROR:
				LOGGER.error("Unable to check if exact household exists",ex.getMessage());	
				break;
			
			default:
				LOGGER.error("Unknown error occurred. ", ex.getMessage());
				break;
			}
		} catch (ParseException e) {
			LOGGER.error("Unable to parse dob",e.getMessage());	
		}

		return ifExactHouseholdExists;
	}
}
