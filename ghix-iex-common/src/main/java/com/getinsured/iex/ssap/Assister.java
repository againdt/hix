package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class Assister {
	@Expose
	private String assisterName;
	@Expose
	private String assisterFirstName;
	@Expose
	private String assisterLastName;
	@Expose
	private String assisterID;	
	@Expose
	private Integer internalAssisterId;

	public String getAssisterName() {
		return assisterName;
	}

	public void setAssisterName(String assisterName) {
		this.assisterName = assisterName;
	}

	public String getAssisterFirstName() {
		return assisterFirstName;
	}

	public void setAssisterFirstName(String assisterFirstName) {
		this.assisterFirstName = assisterFirstName;
	}

	public String getAssisterLastName() {
		return assisterLastName;
	}

	public void setAssisterLastName(String assisterLastName) {
		this.assisterLastName = assisterLastName;
	}

	public String getAssisterID() {
		return assisterID;
	}

	public void setAssisterID(String assisterID) {
		this.assisterID = assisterID;
	}

	public Integer getInternalAssisterId() {
		return internalAssisterId;
	}

	public void setInternalAssisterId(Integer internalAssisterId) {
		this.internalAssisterId = internalAssisterId;
	}
}
