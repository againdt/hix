package com.getinsured.eligibility.indportal.customGrouping;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.indportal.CustomGroupingUtil;
import com.getinsured.eligibility.indportal.dto.CustomGroupingRequestDTO;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.redetermination.service.RenewalApplicationService;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.hix.dto.enrollment.EnrollmentMemberDataDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPremiumDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.indportal.dto.AppGroupRequest;
import com.getinsured.hix.indportal.dto.AppGroupResponse;
import com.getinsured.hix.indportal.dto.AppGroupResponse.GroupType;
import com.getinsured.hix.indportal.dto.AppGroupResponse.ProgramType;
import com.getinsured.hix.indportal.dto.AptcRatioRequest;
import com.getinsured.hix.indportal.dto.AptcRatioResponse;
import com.getinsured.hix.indportal.dto.Group;
import com.getinsured.hix.indportal.dto.Member;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.iex.household.service.RelationshipTypeService;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.IndividualPortalUtil;
import com.getinsured.timeshift.TSCalendar;
import com.google.gson.Gson;

@Controller
@RequestMapping("/eligibility")
public class CustomGroupingController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomGroupingController.class);
	

	private static final String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	private static final String SUCCESS = "SUCCCESS";
	private static final String FAILURE = "FAILURE";

	@Autowired private Gson platformGson;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private IEligibilityProgram eligibilityProgramRepository;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private CustomGroupingUtil customGroupingUtil;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IndividualPortalUtil individualPortalUtil;
	@Autowired private RenewalApplicationService renewalApplicationService;
	@Autowired private RelationshipTypeService relationshipTypeService;
	@Autowired private IntegrationLogService integrationLogService;
	
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	
	public enum EligibilityType{
		EXCHANGE_ELIGIBILITY_TYPE("ExchangeEligibilityType"),
		APTC_ELIGIBILITY_TYPE("APTCEligibilityType"),
		STATE_SUBSIDY_ELIGIBILITY_TYPE("StateSubsidyEligibilityType"),
		CSR_ELIGIBILITY_TYPE("CSREligibilityType"),
		CHIP_ELIGIBILITY_TYPE("CHIPEligibilityType"),
		ASSESSED_CHIP_ELIGIBILITY_TYPE("AssessedCHIPEligibilityType"),
		MEDICAID_MAGI_ELIGIBILITY_TYPE("MedicaidMAGIEligibilityType"),
		MEDICAID_NON_MAGI_ELIGIBILITY_TYPE("MedicaidNonMAGIEligibilityType"),
		ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE("AssessedMedicaidMAGIEligibilityType"),
		ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE("AssessedMedicaidNonMAGIEligibilityType");
		private final String eligibilityType;
		EligibilityType(final String eligibilityType)
		{
			this.eligibilityType = eligibilityType;
		}
		public String toString()
		{
			return eligibilityType;
		}
	}


	@RequestMapping(value = "/api/getAptcRatio", method = RequestMethod.POST)
	public @ResponseBody String getAptcRatio(@RequestBody String aptcRatioRequestStr, HttpServletResponse response){
		AptcRatioResponse aptcRatioResponse = new AptcRatioResponse();
		AptcRatioRequest aptcRatioRequest = null;
		try{
			aptcRatioRequest = platformGson.fromJson(aptcRatioRequestStr, AptcRatioRequest.class);
		}catch(Exception e){
			response.setStatus(400);
			return "Invalid Request";
		}
		
		Object responseObj = customGroupingUtil.getAptcDistribution(aptcRatioRequest);
		
		String responseStr = null;
		if(responseObj instanceof String) {
			response.setStatus(400);
			String errorMessage = (String) responseObj;
			return errorMessage;
		} else if(responseObj instanceof Exception){
			response.setStatus(500);
			Exception ex = (Exception) responseObj;
			return ex.getMessage();
		} else {
			aptcRatioResponse = (AptcRatioResponse) responseObj;
			responseStr = platformGson.toJson(aptcRatioResponse);
		}
		
		return responseStr;
	}
	
	
	/*
	 *get the eligibility of member for group formation
	 *get APTC eligibility for splitting the APTC
	 *
	 *verify if the groups has 
	 * 
	 */
	
	
	@RequestMapping(value = "/api/getAppGroups", method = RequestMethod.POST)
	public @ResponseBody String getAppGroups(@RequestBody String appGroupRequestStr, HttpServletResponse response){
		AppGroupResponse appGroupResponse = new AppGroupResponse();
		AppGroupRequest appGroupRequest = null;
		boolean isRenewal = false;
		boolean isHealthRenewal = false;
		boolean isDentalRenewal = false;
		boolean isAutoRenewed = false;
		try{
			appGroupRequest = platformGson.fromJson(appGroupRequestStr, AppGroupRequest.class);
		}catch(Exception e){
			//response.setStatus(400);
			appGroupResponse.setStatus(400);
			appGroupResponse.setMessage("Invalid Request");
			return platformGson.toJson(appGroupResponse);
		}
		if(appGroupRequest != null) {
			try {
				String nonStandardConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_GROUPING_KEEP_NON_STANDARD_RELATIONSHIP_SEPARATE);
				String message = validateAppGroupRequest(appGroupRequest);
				if("SUCCESS".equalsIgnoreCase(message)) {
					CustomGroupingRequestDTO customGroupingRequestDTO = new CustomGroupingRequestDTO();
					// Filter applicant list to remove member NOT seeking coverage
					appGroupResponse.setApplicationId(appGroupRequest.getApplicationId());
					
					List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplicationId(appGroupRequest.getApplicationId());
					if(ssapApplicants == null || ssapApplicants.size() <= 0) {
						//response.setStatus(400);
						appGroupResponse.setStatus(400);
						appGroupResponse.setMessage("No Applicants found");
						return platformGson.toJson(appGroupResponse);
					}
					List<SsapApplicant> filteredSsapApplicants = new ArrayList<SsapApplicant>();
					for(SsapApplicant ssapApplicant : ssapApplicants){
						if("Y".equals(ssapApplicant.getApplyingForCoverage())){
							filteredSsapApplicants.add(ssapApplicant);
 						}
					}
					
					if(filteredSsapApplicants.size() <= 0) {
						//response.setStatus(400);
						appGroupResponse.setStatus(400);
						appGroupResponse.setMessage("None of Members are Applying for coverage");
						return platformGson.toJson(appGroupResponse);
					}
					
					Map<String, List<EligibilityProgram>> programEligibilityMap = new HashMap<String, List<EligibilityProgram>>();
					for(SsapApplicant ssapApplicant : filteredSsapApplicants){
						programEligibilityMap.put(ssapApplicant.getApplicantGuid(), getProgramEligibility(ssapApplicant));
					}
					
					for(SsapApplicant ssapApplicant : ssapApplicants){
						if(!checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()), EligibilityType.EXCHANGE_ELIGIBILITY_TYPE)){
							filteredSsapApplicants.remove(ssapApplicant);
						}
					}
					
					SsapApplication ssapApplication = ssapApplicationRepository.findOne(appGroupRequest.getApplicationId());
					SingleStreamlinedApplication singleStreamlinedApplication =  ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
					TaxHousehold taxHousehold = singleStreamlinedApplication.getTaxHousehold().get(0);
					
					customGroupingRequestDTO.setApplicationMaxAptc(ssapApplication.getMaximumAPTC());
					customGroupingRequestDTO.setMaximumStateSubsidy(ssapApplication.getMaximumStateSubsidy());
					customGroupingRequestDTO.setCoverageStartDate(appGroupRequest.getCoverageStartDate());
					
					if(null!=taxHousehold.getHouseholdMember() && taxHousehold.getHouseholdMember().size()>0) {
						
						Set<String> guidsToRetain = new HashSet<String>();
						taxHousehold.getHouseholdMember().stream().filter(m->m.isSeeksQhp()).forEach(h->guidsToRetain.add(h.getApplicantGuid()));
						filteredSsapApplicants = filteredSsapApplicants.stream().filter(s->guidsToRetain.contains(s.getApplicantGuid())).collect(Collectors.toList());
						
					}
					
					
					if(filteredSsapApplicants.size() <= 0) {
						//response.setStatus(400);
						appGroupResponse.setStatus(400);
						appGroupResponse.setMessage("No exchange eligible member");
						return platformGson.toJson(appGroupResponse);
					}
					
					// Set csrLevel to NULL for applicants who are not csr eligible
					for(SsapApplicant ssapApplicant : ssapApplicants){
						if(!checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()), EligibilityType.CSR_ELIGIBILITY_TYPE)){
							ssapApplicant.setCsrLevel(null);
						}
					}
					
					// --- 1. Create groups based on enrollments ---
					String coverageYear = appGroupRequest.getCoverageStartDate().substring(6, appGroupRequest.getCoverageStartDate().length());
					String coverageMonth = appGroupRequest.getCoverageStartDate().substring(0, 2);
					
					Map<String, List<EnrollmentMemberDataDTO>> enrolleeData = null;
					
					Boolean isOE = individualPortalUtil.isInsideOEEnrollmentWindow((int)ssapApplication.getCoverageYear());			
					appGroupResponse.setInsideOEEnrollmentWindow(isOE);
					appGroupResponse.setCoverageYear((int)ssapApplication.getCoverageYear());
					RenewalStatus healthRenewalStatus = null;
					RenewalStatus dentalRenewalStatus = null;
					String status = ssapApplication.getApplicationStatus();
					String dentalStatus = ssapApplication.getApplicationDentalStatus();
					appGroupResponse.setRenewalApplication(false);
					if(isOE && "CA".equalsIgnoreCase(stateCode) && "OE".equals(ssapApplication.getApplicationType())) {
					try {
						//CA specific active renewal - check if the application renewal status is to_consider 
						Map<String,Object> renewalApplicationStatus = null;
							renewalApplicationStatus =  renewalApplicationService.getRenewalApplicationStatus(appGroupRequest.getApplicationId());	
						
						healthRenewalStatus  = (RenewalStatus)renewalApplicationStatus.get("healthRenewalStatus");
						dentalRenewalStatus = (RenewalStatus)renewalApplicationStatus.get("dentalRenewalStatus");
					}
					catch(Exception e) {
						LOGGER.error("----Error while getting renewal status----", e);
					}
					if(RenewalStatus.TO_CONSIDER.equals(healthRenewalStatus) || RenewalStatus.TO_CONSIDER.equals(dentalRenewalStatus) ){
 						isRenewal = true;
						if(RenewalStatus.TO_CONSIDER.equals(healthRenewalStatus) && ("ER".equalsIgnoreCase(status) || "PN".equalsIgnoreCase(status)) ) {
								appGroupResponse.setRenewalApplication(true);	
								isHealthRenewal = true;
						}
						if(RenewalStatus.TO_CONSIDER.equals(dentalRenewalStatus) && dentalStatus == null ) {
								isDentalRenewal = true;
						}
 					  }	
					if(RenewalStatus.RENEWED.equals(healthRenewalStatus)){
						isAutoRenewed = true;
					}

					}	
						
					Map<String,BigDecimal> allMembersAPTC = null;
					Map<String,BigDecimal> allDentalMembersAPTC = null;
					Map<String,BigDecimal> allMembersStateSubsidy = null;
					Map<String,String> membersEnrollmentData = new HashMap<String,String>();
					Map<String,BigDecimal> allDentalMembersStateSubsidy = null;
					boolean isSEP = false;
					
					String enrollmentResponseStr = getEnrollmentsByMember(ssapApplicants, ssapApplication.getCoverageYear()+"");
					//enrollmentMemberDataDTO.getExchgIndivIdentifier()
					
					
					if(StringUtils.isNotBlank(enrollmentResponseStr)) {
						EnrollmentResponse enrollmentResponse = platformGson.fromJson(enrollmentResponseStr, EnrollmentResponse.class);
						if(enrollmentResponse != null) {
							enrolleeData = enrollmentResponse.getEnrollmentMemberDataDTOMap();							
							ignoreTermHealthEnrollment(enrolleeData, filteredSsapApplicants);
						}
					}
					

					if(isRenewal) {
						
						String enrollmentRenew = getEnrollmentsByMember(ssapApplicants, ssapApplication.getCoverageYear()-1+"");
						Map<String, List<EnrollmentMemberDataDTO>> enrolleeDataPN = null;
						if(StringUtils.isNotBlank(enrollmentRenew)) {
							EnrollmentResponse enrollmentResponse = platformGson.fromJson(enrollmentRenew, EnrollmentResponse.class);
							if(enrollmentResponse != null) {
								enrolleeDataPN = enrollmentResponse.getEnrollmentMemberDataDTOMap();							
								ignoreTermPrevEnrollment(enrolleeDataPN, filteredSsapApplicants);
								List<SsapApplicant> applicantsToRemove = null;
								//if healthstatus is not TO_CONSIDER - remove those app from prev year, same for dental
								if(!isHealthRenewal) {
									ignorePrevEnrollments(enrolleeDataPN,"health",ssapApplicants);
								}
																
								if(!isDentalRenewal) {
									ignorePrevEnrollments(enrolleeDataPN,"dental",ssapApplicants);
								}
								//if status PN, remove previous enrollments only for health enrolled members.
								if(isHealthRenewal && "PN".equalsIgnoreCase(status)) {
									applicantsToRemove = getApplicantstoRemove(ssapApplicants,enrolleeData);
									ignorePrevEnrollments(enrolleeDataPN,"health",applicantsToRemove);
								}
															
																 		 						
								 	
							}
						}
						
						if(enrolleeDataPN != null && enrolleeDataPN.size() > 0) {
							if(enrolleeData == null) {
								enrolleeData = new HashMap<String, List<EnrollmentMemberDataDTO>>();
								enrolleeData.putAll(enrolleeDataPN);
							}
							else {
								mergeEnrollments(enrolleeData,enrolleeDataPN);
							}
							
							
						}
						
					}
									    
					
					if("SEP".equals(ssapApplication.getApplicationType()) || "LCE".equals(ssapApplication.getApplicationType()) || (appGroupRequest.getPostAptcRedistributionFlag() != null && appGroupRequest.getPostAptcRedistributionFlag()))
					{
						Map<String,BigDecimal> remainingAptc = new HashMap<String,BigDecimal>();
						allMembersAPTC = getAptcForMembers(filteredSsapApplicants, appGroupRequest, ssapApplication, programEligibilityMap, enrolleeData, remainingAptc, "Health");
						allMembersStateSubsidy = getStateSubsidyForMembers(filteredSsapApplicants, appGroupRequest, ssapApplication, programEligibilityMap, enrolleeData, remainingAptc, "Health");
						if(!"CA".equalsIgnoreCase(stateCode) && allMembersAPTC.isEmpty()){
							allDentalMembersAPTC = getAptcForMembers(filteredSsapApplicants, appGroupRequest, ssapApplication, programEligibilityMap, enrolleeData, remainingAptc, "Dental");
							allDentalMembersStateSubsidy = getStateSubsidyForMembers(filteredSsapApplicants, appGroupRequest, ssapApplication, programEligibilityMap, enrolleeData, remainingAptc, "Dental");
							
						}
						isSEP = true;
					}
					//Extra step to reset aptc to actual current value for last year enrollment group. Sum memebr level aptc & set memebr level aptc to null.
					List<Group> groupList = createGroupFromExistingEnrl(filteredSsapApplicants, coverageYear, coverageMonth, programEligibilityMap,ssapApplication,allMembersAPTC, allMembersStateSubsidy, isSEP, appGroupRequest,membersEnrollmentData, enrolleeData,isHealthRenewal,isDentalRenewal, allDentalMembersAPTC, allDentalMembersStateSubsidy);
					 
					//Update existing enrollments for groups containg only non-standard members 
					 if(!nonStandardConfig.isEmpty() && nonStandardConfig != null) {
				        	if("true".equalsIgnoreCase(nonStandardConfig)) {  
				        		groupList = updateNonstandardStatus(groupList);
				        	}
					 }
					 
					 customGroupingRequestDTO.setGroupList(groupList);
					
					// --- 2. Calculate Remaining APTC after allocate to the enrollment groups ---
					BigDecimal maxAptc = BigDecimal.ZERO;
					BigDecimal remainingAPTC = BigDecimal.ZERO;
					if(ssapApplication.getMaximumAPTC() != null && ssapApplication.getMaximumAPTC().compareTo(BigDecimal.ZERO) > 0 ) {
						maxAptc = ssapApplication.getMaximumAPTC();
						LOGGER.warn("maxAptc {}", maxAptc);
						remainingAPTC = calculateRemainingAPTC(groupList, maxAptc);
						customGroupingRequestDTO.setRemainingAPTC(remainingAPTC);
						LOGGER.warn("remainingAPTC {}", remainingAPTC);
						if(remainingAPTC.compareTo(BigDecimal.ZERO)<0 ){
							if(appGroupRequest.getPostAptcRedistributionFlag() == null || !appGroupRequest.getPostAptcRedistributionFlag()){
								LOGGER.error("APTC = "+ maxAptc.subtract(remainingAPTC) +" distributed in enrolled groups can not be more than Max APTC "+maxAptc+" of application "+ssapApplication.getId());
								appGroupRequest.setPostAptcRedistributionFlag(Boolean.TRUE);
								saveIntegrationLog(customGroupingRequestDTO, ssapApplication.getId(), SUCCESS);
								String postAppRequestStr = platformGson.toJson(appGroupRequest);
								String postAppGroupResponse = getAppGroups(postAppRequestStr, response);
								return postAppGroupResponse;
							}else{
								saveIntegrationLog(customGroupingRequestDTO, ssapApplication.getId(), SUCCESS);
								appGroupResponse.setStatus(400);
								appGroupResponse.setMessage("APTC = "+ maxAptc.subtract(remainingAPTC) +" distributed in enrolled groups can not be more than Max APTC "+maxAptc+" of application");
								return platformGson.toJson(appGroupResponse);
							}							
						}
					}

					BigDecimal maxStateSubsidy = BigDecimal.ZERO;
					BigDecimal remainingStateSubsidy = BigDecimal.ZERO;
					if(ssapApplication.getMaximumStateSubsidy() != null && ssapApplication.getMaximumStateSubsidy().compareTo(BigDecimal.ZERO) > 0 ) {
						maxStateSubsidy = ssapApplication.getMaximumStateSubsidy();
						LOGGER.warn("maxStateSubsidy {}", maxStateSubsidy);
						remainingStateSubsidy = calculateRemainingStateSubsidy(groupList, maxStateSubsidy);
						LOGGER.warn("remainingStateSubsidy {}", remainingStateSubsidy);
						customGroupingRequestDTO.setRemainingStateSubsidy(remainingStateSubsidy);
						
						if(remainingStateSubsidy.compareTo(BigDecimal.ZERO)<0 ){
							if(appGroupRequest.getPostAptcRedistributionFlag() == null || !appGroupRequest.getPostAptcRedistributionFlag()){
								LOGGER.error("State Subsidy = "+ maxStateSubsidy.subtract(remainingStateSubsidy) +" distributed in enrolled groups can not be more than Max State Subsidy "+maxStateSubsidy+" of application " + ssapApplication.getId());
								appGroupRequest.setPostAptcRedistributionFlag(Boolean.TRUE);
								saveIntegrationLog(customGroupingRequestDTO, ssapApplication.getId(), SUCCESS);
								String postAppRequestStr = platformGson.toJson(appGroupRequest);
								String postAppGroupResponse = getAppGroups(postAppRequestStr, response);
								return postAppGroupResponse;
							}else{
								saveIntegrationLog(customGroupingRequestDTO, ssapApplication.getId(), SUCCESS);
								appGroupResponse.setStatus(400);
								appGroupResponse.setMessage("State Subsidy = "+ maxStateSubsidy.subtract(remainingStateSubsidy) +" distributed in enrolled groups can not be more than Max State Subsidy "+maxStateSubsidy+" of application");
								return platformGson.toJson(appGroupResponse);
							}							
						}
					}
					
					
					// --- 3. Add newly added members ---
					groupList = addRemainingMembers(filteredSsapApplicants, appGroupRequest.getCoverageStartDate(), groupList, remainingAPTC, remainingStateSubsidy,"Health", programEligibilityMap, ssapApplication.getApplicationType(),isHealthRenewal,isAutoRenewed,nonStandardConfig);
					customGroupingRequestDTO.setGroupList(groupList);
					
					
					// --- 4. Calculate remaining aptc after allocating to enrolled plans ---
					BigDecimal finalRemainingAPTC = BigDecimal.ZERO;
					if(maxAptc.compareTo(BigDecimal.ZERO) > 0) {
						boolean allMembersAddedToHealth = allGroupsHasHealthEnrollment(groupList);
						if(allMembersAddedToHealth  && !"CA".equalsIgnoreCase(stateCode)) {
							finalRemainingAPTC = calculateRemainingAPTC(groupList, maxAptc);
							customGroupingRequestDTO.setFinalRemainingAPTC(finalRemainingAPTC);
							
							if(finalRemainingAPTC.compareTo(BigDecimal.ZERO)<0){
								saveIntegrationLog(customGroupingRequestDTO, ssapApplication.getId(), FAILURE);
								appGroupResponse.setStatus(400);
								appGroupResponse.setMessage("APTC distributed in both enrollment and non enrolled groups can not be more than Max APTC of application");
								return platformGson.toJson(appGroupResponse);
							}
						}
						else
						{
							finalRemainingAPTC = null;
						}
					}

					BigDecimal finalRemainingStateSubsidy = BigDecimal.ZERO;
					if(maxStateSubsidy.compareTo(BigDecimal.ZERO) > 0) {
						boolean allMembersAddedToHealth = allGroupsHasHealthEnrollment(groupList);
						if(allMembersAddedToHealth  && !"CA".equalsIgnoreCase(stateCode)) {
							finalRemainingStateSubsidy = calculateRemainingStateSubsidy(groupList, maxStateSubsidy);
							customGroupingRequestDTO.setFinalRemainingStateSubsidy(finalRemainingStateSubsidy);
							
							if(finalRemainingStateSubsidy.compareTo(BigDecimal.ZERO)<0){
								saveIntegrationLog(customGroupingRequestDTO, ssapApplication.getId(), FAILURE);
								appGroupResponse.setStatus(400);
								appGroupResponse.setMessage("State Subsidy distributed in both enrollment and non enrolled groups can not be more than Max State Subsidy of application");
								return platformGson.toJson(appGroupResponse);
							}
						}
						else
						{
							finalRemainingStateSubsidy = null;
						}
					}
					
					// -- 5. now start forming group for Dental assign the APTC
					finalRemainingStateSubsidy = "CA".equalsIgnoreCase(stateCode)? null : finalRemainingStateSubsidy;
					finalRemainingAPTC = "CA".equalsIgnoreCase(stateCode)? null : finalRemainingAPTC;
					groupList = addRemainingMembers(filteredSsapApplicants, appGroupRequest.getCoverageStartDate(), groupList, finalRemainingAPTC, finalRemainingStateSubsidy, "Dental", programEligibilityMap, ssapApplication.getApplicationType(),isDentalRenewal,isAutoRenewed,nonStandardConfig);
					
					//---  Update enrollmentIds in members as response ---
					for(Group group : groupList)
					{
						for(Member member : group.getMemberList())
						{
							member.setHealthEnrollmentId(membersEnrollmentData.get("H"+member.getId()));
							member.setDentalEnrollmentId(membersEnrollmentData.get("D"+member.getId()));
						}
					}
					
					// -- 6. Determine program type. Program type for health should be A if there is only 1 health group and all the members from Dental group are present in Health group
					groupList = setProgramTypeForGroup(groupList);
					appGroupResponse.setGroupList(groupList);
					appGroupResponse.setApplicationType(ssapApplication.getApplicationType());
				}else {
					//response.setStatus(400);
					appGroupResponse.setStatus(400);
					appGroupResponse.setMessage(message);
					return platformGson.toJson(appGroupResponse);
				}
			}catch(Exception e){
				//response.setStatus(500);
				LOGGER.error("----Error while creating groups from application----", e);
				appGroupResponse.setStatus(500);
				appGroupResponse.setMessage(e.getMessage());
				return platformGson.toJson(appGroupResponse);
			}
		}
			
		String responseStr = platformGson.toJson(appGroupResponse);
		
		return responseStr;
	}
	
	private void mergeEnrollments(Map<String, List<EnrollmentMemberDataDTO>> enrolleeData,Map<String, List<EnrollmentMemberDataDTO>> enrolleeDataPrev){		

		enrolleeDataPrev.forEach((key , value) -> {
	        List<EnrollmentMemberDataDTO> list = value;
	        if (list != null && !list.isEmpty()) {
	        	if(!enrolleeData.containsKey(key))
	        		enrolleeData.put(key,list);
	        	else {
	        		List<EnrollmentMemberDataDTO> enrolleeList = enrolleeData.get(key);
	        		enrolleeList.addAll(list);
	        		enrolleeData.put(key,enrolleeList);
	        	}
	        }
	       
	    });

	}
	
	private void ignorePrevEnrollments(Map<String, List<EnrollmentMemberDataDTO>> enrolleeData, String type,List<SsapApplicant> ssapApplicants){

			if(enrolleeData != null && ssapApplicants != null) {
				for(SsapApplicant ssapApplicant : ssapApplicants) {
					List<EnrollmentMemberDataDTO> enrollmentDTOList = enrolleeData.get(ssapApplicant.getApplicantGuid());
					if(enrollmentDTOList != null && !enrollmentDTOList.isEmpty()) {					
						Iterator<EnrollmentMemberDataDTO> itrEnrlMemberDtoList = enrollmentDTOList.iterator();
						while(itrEnrlMemberDtoList.hasNext()) {
							EnrollmentMemberDataDTO enrollmentMemberDTO = itrEnrlMemberDtoList.next();
							if(("HEALTH".equalsIgnoreCase(enrollmentMemberDTO.getInsuranceType()) && "health".equalsIgnoreCase(type)) || ("DENTAL".equalsIgnoreCase(enrollmentMemberDTO.getInsuranceType()) && "dental".equalsIgnoreCase(type))){
								itrEnrlMemberDtoList.remove();
							}
						}	
						
					}
				}
			}
		 		
		}
	
	//Active Renewal, getting applicants who have current health enrollments 
	private List<SsapApplicant> getApplicantstoRemove(List<SsapApplicant> ssapApplicants,Map<String, List<EnrollmentMemberDataDTO>> enrolleeData){
 		List<SsapApplicant> remainingApplicants = new ArrayList<SsapApplicant>();
 	if(enrolleeData != null){		
 		for(SsapApplicant ssapApplicant: ssapApplicants) {	
			if(enrolleeData.containsKey(ssapApplicant.getApplicantGuid())) {
				List<EnrollmentMemberDataDTO> enrollmentDTOList = enrolleeData.get(ssapApplicant.getApplicantGuid());
				if(enrollmentDTOList != null && !enrollmentDTOList.isEmpty()) {					
					for(EnrollmentMemberDataDTO enrollmentMemberDTO:enrollmentDTOList ) {
						if("HEALTH".equalsIgnoreCase(enrollmentMemberDTO.getInsuranceType())){
							remainingApplicants.add(ssapApplicant);
						}
					}	
					
				}	
 			}
 		 }
	  }
 		
		return remainingApplicants;
	}
	
	private List<Group> updateAptcForPrevEnrollments(List<Group> groupList,AptcRatioResponse aptcRatioResponse,String type){		
		for(Group group : groupList) {
			BigDecimal aptc = BigDecimal.ZERO;
			
			if(group.getIsRenewal() && type.equalsIgnoreCase(group.getGroupType().toString())) {
				for(Member member : group.getMemberList()) {					
					
					if(group.getMaxAptc() == null) {
						BigDecimal memberAptc = getAptcOfMember(member.getId(), aptcRatioResponse);
						if(memberAptc != null) {
							aptc = aptc.add(memberAptc);
							member.setAptc(null);
						}
					}				
					
				}
				if(aptc.compareTo(BigDecimal.ZERO) != 0) {
				group.setMaxAptc(aptc);
				group.setElectedAptc(aptc);
				}
			
			}
		}
		return groupList;
	}
	
	private List<Group> updateSubsidyForPrevEnrollments(List<Group> groupList,AptcRatioResponse stateSubsidyRatioResponse,String type){		
		for(Group group : groupList) {
			BigDecimal stateSubsidy = BigDecimal.ZERO;
			if(group.getIsRenewal() && type.equalsIgnoreCase(group.getGroupType().toString())) {
				for(Member member : group.getMemberList()) {

					if(group.getMaxStateSubsidy() == null){
						BigDecimal memberSubsidy = getAptcOfMember(member.getId(), stateSubsidyRatioResponse);
						if(memberSubsidy != null) {
							stateSubsidy =  stateSubsidy.add(memberSubsidy);
							member.setStateSubsidy(null);
						}
					}
					
				}	
				if(stateSubsidy.compareTo(BigDecimal.ZERO) != 0) {
				group.setMaxStateSubsidy(stateSubsidy);
				group.setElectedStateSubsidy(stateSubsidy);
				}
			
			}
		}
		return groupList;
	}

	
	private List<Group> setProgramTypeForGroup(List<Group> groupList) {
		int healthGroupCount = 0;
		
		for(Group group : groupList) {
			if(GroupType.HEALTH.equals(group.getGroupType())) {
				healthGroupCount++;
			}
		}
		
		if(healthGroupCount == 1) {
			List<String> healthGroupMembers = new ArrayList<String>();
			List<String> dentalGroupMembers = new ArrayList<String>();
			for(Group group : groupList) {
				if(GroupType.HEALTH.equals(group.getGroupType())) {
					for(Member member: group.getMemberList()) {
						healthGroupMembers.add(member.getId());
					}
				}
				if(GroupType.DENTAL.equals(group.getGroupType())) {
					for(Member member: group.getMemberList()) {
						dentalGroupMembers.add(member.getId());
					}
				}
			}
			if(healthGroupMembers.size() > 0 && healthGroupMembers.containsAll(dentalGroupMembers)) {
				for(Group group : groupList) {
					if(GroupType.HEALTH.equals(group.getGroupType())) {
						group.setProgramType(ProgramType.A);
					}
				}
			}
		}
		return groupList;
	}


	private List<EligibilityProgram> getProgramEligibility(SsapApplicant ssapApplicant) {
		return eligibilityProgramRepository.getApplicantEligibilities(ssapApplicant.getId());
	}


	private boolean allGroupsHasHealthEnrollment(List<Group> groupList) {
		for(Group group : groupList){
			if (GroupType.HEALTH.equals(group.getGroupType())){
				for(Member member : group.getMemberList()){
					if(StringUtils.isNotEmpty(member.getHealthEnrollmentId())){
						return false;
					}
				}
			}
		}
		return true;
	}


	private BigDecimal calculateRemainingAPTC(List<Group> groupList, BigDecimal maxAptc) {
		BigDecimal usedAptc = BigDecimal.ZERO;
		for(Group group : groupList)
		{
			if(group.getMaxAptc() != null)
			{
				usedAptc = usedAptc.add(group.getMaxAptc());
				LOGGER.warn("Group Enrollment Id {}", group.getGroupEnrollmentId());
				LOGGER.warn("Group Max Aptc {}", group.getMaxAptc());
			}
		}
		BigDecimal remainingAptc = maxAptc.subtract(usedAptc);
		return remainingAptc;
	}

	private BigDecimal calculateRemainingStateSubsidy(List<Group> groupList, BigDecimal maxStateSubsidy) {
		BigDecimal usedStateSubsidy = BigDecimal.ZERO;
		for(Group group : groupList)
		{
			if(group.getMaxStateSubsidy() != null)
			{
				usedStateSubsidy = usedStateSubsidy.add(group.getMaxStateSubsidy());
				LOGGER.warn("Group Enrollment Id {}", group.getGroupEnrollmentId());
				LOGGER.warn("Group Max State Subsidy {}", group.getMaxStateSubsidy());
			}
		}
		BigDecimal remainingStateSubsidy = maxStateSubsidy.subtract(usedStateSubsidy);
		return remainingStateSubsidy;
	}


	private boolean checkEligibility(List<EligibilityProgram> programs,EligibilityType eligibilityType) 
	{
		if(programs != null && programs.size() > 0) {
			for(EligibilityProgram eligibilityProgram : programs){
				if (eligibilityType.toString().equals(eligibilityProgram.getEligibilityType()) && "TRUE".equals(eligibilityProgram.getEligibilityIndicator())){	
					return true;
				}
			}
		}
		return false;
	}

	private String validateAppGroupRequest(AppGroupRequest appGroupRequest) {
		if(appGroupRequest.getApplicationId() == null) {
			return "Invalid Application Id";
		}
		if(!DateUtil.isValidDate(appGroupRequest.getCoverageStartDate(), "MM/dd/YYYY")) {
			return "Invalid Coverage Start Date";
		}
		return "SUCCESS";
	}
	
	private List<Group> createGroupFromExistingEnrl(List<SsapApplicant> ssapApplicants, String coverageYear, String coverageMonth, Map<String, List<EligibilityProgram>> programEligibilityMap, SsapApplication ssapApplication, Map<String, BigDecimal> allMembersAPTC, Map<String, BigDecimal> allMembersStateSubsidy, boolean isSEP, AppGroupRequest appGroupRequest, Map<String, String> membersEnrollmentData, Map<String, List<EnrollmentMemberDataDTO>> enrolleeData, Boolean isHealthRenewal, Boolean isDentalRenewal,  Map<String, BigDecimal> allDentalMembersAPTC, Map<String, BigDecimal> allDentalMembersStateSubsidy) {
		//Get existing enrollment and put members from same enrollment into one group
		AppGroupResponse appGroupResponse = new AppGroupResponse();
		List<Group> groupList = new ArrayList<Group>();
		boolean hasSameEnrollmentId = false;
		if(enrolleeData != null) {
			for(SsapApplicant ssapApplicant : ssapApplicants) {
				List<EnrollmentMemberDataDTO> enrollmentDTOList = enrolleeData.get(ssapApplicant.getApplicantGuid());
				if(enrollmentDTOList != null && !enrollmentDTOList.isEmpty()) {
					for(EnrollmentMemberDataDTO enrollmentDTO : enrollmentDTOList) {
						Boolean isPrevYearEnrollment = isPrevYearEnrollment(enrollmentDTO,coverageYear);
						if(customGroupingUtil.isEnrollmentActive(enrollmentDTO) || (isHealthRenewal && isPrevYearEnrollment) || (isDentalRenewal && isPrevYearEnrollment)){
							BigDecimal enrollmentAPTC = getEnrollmentAptc(enrollmentDTO);
							BigDecimal electedAPTC = getElectedAPTC(enrollmentDTO);

							

							BigDecimal enrollmentStateSubsidy = getEnrollmentStateSubsidy(enrollmentDTO);
							BigDecimal electedStateSubsidy = getElectedStateSubsidy(enrollmentDTO);
							
							if(isPrevYearEnrollment && (("Health".equalsIgnoreCase(enrollmentDTO.getInsuranceType()) && isHealthRenewal) || ("Dental".equalsIgnoreCase(enrollmentDTO.getInsuranceType()) && isDentalRenewal) )) {
								enrollmentAPTC = null;
								electedAPTC = null;
								enrollmentStateSubsidy = null;
								electedStateSubsidy = null;
							}
							
							BigDecimal memberAPTC = null;
							BigDecimal memberStateSubsidy = null;

							hasSameEnrollmentId = enrollmentDTO.getPriorSsapApplicationid() != null && ssapApplication.getId() == enrollmentDTO.getPriorSsapApplicationid().longValue() ? true : false;
							if ((isSEP && !hasSameEnrollmentId) || (appGroupRequest.getPostAptcRedistributionFlag() != null && appGroupRequest.getPostAptcRedistributionFlag())) {
								if("Health".equalsIgnoreCase(enrollmentDTO.getInsuranceType())) {
									memberAPTC = allMembersAPTC.get(ssapApplicant.getApplicantGuid());
									memberStateSubsidy = allMembersStateSubsidy.get(ssapApplicant.getApplicantGuid());
								}else if("Dental".equalsIgnoreCase(enrollmentDTO.getInsuranceType())) {
									memberAPTC = allDentalMembersAPTC != null ? allDentalMembersAPTC.get(ssapApplicant.getApplicantGuid()) : null;
									memberStateSubsidy = allDentalMembersStateSubsidy != null ? allDentalMembersStateSubsidy.get(ssapApplicant.getApplicantGuid()) : null;
								}
							}
							
							
							if ("Health".equalsIgnoreCase(enrollmentDTO.getInsuranceType())) {
								membersEnrollmentData.put("H"+ssapApplicant.getApplicantGuid(), enrollmentDTO.getEnrollmentID().toString());
							} else if ("Dental".equalsIgnoreCase(enrollmentDTO.getInsuranceType())) {
								membersEnrollmentData.put("D"+ssapApplicant.getApplicantGuid(), enrollmentDTO.getEnrollmentID().toString());
							}
							createOrUpdateGroup(appGroupResponse, groupList, ssapApplicant, enrollmentDTO,enrollmentDTO.getInsuranceType(), enrollmentAPTC, enrollmentStateSubsidy, programEligibilityMap, memberAPTC, memberStateSubsidy, isSEP, appGroupRequest, hasSameEnrollmentId, electedAPTC, electedStateSubsidy, isHealthRenewal, isDentalRenewal, isPrevYearEnrollment);
						
						}
					}
				}
			}
		}
		return groupList;
	}

	private List<Group> updateNonstandardStatus(List<Group> groupList){		
		for(Group group : groupList) {
			boolean isStandardRelation = false;
		 if(group.getGroupType() == GroupType.HEALTH) {
			 for(Member member : group.getMemberList()) {					
				 if(!checkIsNonStandardRelation(member.getRelation())) {
					 isStandardRelation = true;
					 break;
				 }					
			 }
			 if(!isStandardRelation) {
				 group.setIsNonStandardRelationGroup(true);
			 }	
		   }
		}
		return groupList;
	}

	//TODO: When future state specific requirement is given, pass state code alomg with relation 
	private boolean checkIsNonStandardRelation(String relation) {	
		try {
			if(relation != null)
				return (!relationshipTypeService.isRelationshipStandardForCode(relation));
			else
				return true;
			}
		catch(Exception e) {
			LOGGER.error("CustomGroupingController-Error fetching relationship status:",e);
		}
		return false;
	}
	
	private Boolean isPrevYearEnrollment(EnrollmentMemberDataDTO enrollmentDTO,String coverageYear) {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(enrollmentDTO.getBenefitEffectiveDate());
		int enrollmentYear = cal.get(Calendar.YEAR);
		int year = Integer.parseInt(coverageYear);
		if(enrollmentYear < year)
			return true;
		else
			return false;
	}
	
	private BigDecimal getEnrollmentAptc(EnrollmentMemberDataDTO enrollmentDTO) {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(enrollmentDTO.getBenefitEffectiveDate());
		int year = cal.get(Calendar.YEAR);
		int month = 12;//cal.get(Calendar.MONTH) + 1;
		BigDecimal maxAPTC = null;
		List<EnrollmentPremiumDTO> monthlyPrmiums = enrollmentDTO.getEnrollmentMonthlyPremium();
		if(monthlyPrmiums != null && !monthlyPrmiums.isEmpty()) {
			for(EnrollmentPremiumDTO premiumDTO : monthlyPrmiums)
			{
				if(premiumDTO.getMonth().equals(month) && premiumDTO.getYear().equals(year) && premiumDTO.getMaxAptc() != null)	
				{
					maxAPTC = new BigDecimal(Float.toString(premiumDTO.getMaxAptc())).setScale(2, BigDecimal.ROUND_HALF_UP);
					break;
				}
			}
		}
		return maxAPTC;
	}

	private BigDecimal getEnrollmentStateSubsidy(EnrollmentMemberDataDTO enrollmentDTO) {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(enrollmentDTO.getBenefitEffectiveDate());
		int year = cal.get(Calendar.YEAR);
		int month = 12;//cal.get(Calendar.MONTH) + 1;
		BigDecimal maxStateSubsidy = null;
		List<EnrollmentPremiumDTO> monthlyPrmiums = enrollmentDTO.getEnrollmentMonthlyPremium();
		if(monthlyPrmiums != null && !monthlyPrmiums.isEmpty()) {
			for(EnrollmentPremiumDTO premiumDTO : monthlyPrmiums)
			{
				if(premiumDTO.getMonth().equals(month) && premiumDTO.getYear().equals(year) && premiumDTO.getMaxStateSubsidy() != null)
				{
					maxStateSubsidy = premiumDTO.getMaxStateSubsidy().setScale(2, BigDecimal.ROUND_HALF_UP);
					break;
				}
			}
		}
		return maxStateSubsidy;
	}
	
	private BigDecimal getElectedAPTC(EnrollmentMemberDataDTO enrollmentDTO) {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(enrollmentDTO.getBenefitEffectiveDate());
		int year = cal.get(Calendar.YEAR);
		int month = 12;//cal.get(Calendar.MONTH) + 1;
		BigDecimal maxAPTC = null;
		List<EnrollmentPremiumDTO> monthlyPrmiums = enrollmentDTO.getEnrollmentMonthlyPremium();
		if(monthlyPrmiums != null && !monthlyPrmiums.isEmpty()) {
			for(EnrollmentPremiumDTO premiumDTO : monthlyPrmiums)
			{
				if(premiumDTO.getMonth().equals(month) && premiumDTO.getYear().equals(year) && premiumDTO.getActualAptcAmount() != null)	
				{
					maxAPTC = new BigDecimal(Float.toString(premiumDTO.getActualAptcAmount())).setScale(2, BigDecimal.ROUND_HALF_UP);
					break;
				}
			}
		}
		return maxAPTC;
	}

	private BigDecimal getElectedStateSubsidy(EnrollmentMemberDataDTO enrollmentDTO) {
		Calendar cal = TSCalendar.getInstance();
		cal.setTime(enrollmentDTO.getBenefitEffectiveDate());
		int year = cal.get(Calendar.YEAR);
		int month = 12;//cal.get(Calendar.MONTH) + 1;
		BigDecimal maxStateSubsidy = null;
		List<EnrollmentPremiumDTO> monthlyPrmiums = enrollmentDTO.getEnrollmentMonthlyPremium();
		if(monthlyPrmiums != null && !monthlyPrmiums.isEmpty()) {
			for(EnrollmentPremiumDTO premiumDTO : monthlyPrmiums)
			{
				if(premiumDTO.getMonth().equals(month) && premiumDTO.getYear().equals(year) && premiumDTO.getActualStateSubsidyAmount() != null)
				{
					maxStateSubsidy = premiumDTO.getActualStateSubsidyAmount().setScale(2, BigDecimal.ROUND_HALF_UP);
					break;
				}
			}
		}
		return maxStateSubsidy;
	}


	private void createOrUpdateGroup(AppGroupResponse appGroupResponse, List<Group> groupList,
			SsapApplicant ssapApplicant, EnrollmentMemberDataDTO enrollmentDTO, String insuranceType, BigDecimal enrollmentAPTC, BigDecimal enrollmentStateSubsidy, Map<String, List<EligibilityProgram>> programEligibilityMap, BigDecimal memberAptc, BigDecimal memberStateSubsidy,boolean isSEP, AppGroupRequest appGroupRequest, boolean hasSameEnrollmentId, BigDecimal electedAPTC, BigDecimal electedStateSubsidy, Boolean isHealthRenewal, Boolean isDentalRenewal, Boolean isPrevYearEnrollment) {
		Member newMember = createMember(ssapApplicant, memberAptc, memberStateSubsidy, appGroupRequest.getCoverageStartDate());

		boolean newMemberAdded = false;
		boolean memberAPTCEligible = checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()),EligibilityType.APTC_ELIGIBILITY_TYPE);
		boolean memberStateSubsidyEligible = checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()),EligibilityType.STATE_SUBSIDY_ELIGIBILITY_TYPE);

		for (Group existingGroup : groupList) {
			if (StringUtils.isNotBlank(existingGroup.getGroupEnrollmentId())
					&& existingGroup.getGroupEnrollmentId().equalsIgnoreCase(enrollmentDTO.getEnrollmentID().toString())) {
			  if(!existingGroup.getMemberList().contains(newMember)) {
				existingGroup.getMemberList().add(newMember);
				String groupCsr = getMinMemberCsr(existingGroup.getMemberList());
				existingGroup.setGroupCsr(groupCsr);
				BigDecimal aptcTobeUsed = enrollmentAPTC;
				BigDecimal stateSubsidyTobeUsed = enrollmentStateSubsidy;
				if ((isSEP  && !hasSameEnrollmentId) || (appGroupRequest.getPostAptcRedistributionFlag() != null && appGroupRequest.getPostAptcRedistributionFlag())){
					if(memberAptc != null) {
						aptcTobeUsed = memberAptc;
						if (existingGroup.getMaxAptc() == null){
							existingGroup.setMaxAptc(aptcTobeUsed);
						}else{//In case of SEP we re-distribute aptc and put it at member level hence we need to add them to get group level APTC
							existingGroup.setMaxAptc(existingGroup.getMaxAptc().add(aptcTobeUsed));
						}
					}

						if (memberStateSubsidy != null) {
							stateSubsidyTobeUsed = memberStateSubsidy;
							if (existingGroup.getMaxStateSubsidy() == null) {
								existingGroup.setMaxStateSubsidy(stateSubsidyTobeUsed);
							} else {
								existingGroup.setMaxStateSubsidy(existingGroup.getMaxStateSubsidy().add(stateSubsidyTobeUsed));
							}
						}
				}else{
					//In case of non SEP we get APTC at enrollment level from Enrollment premium hence we can set it directly
					if(memberAPTCEligible){
						existingGroup.setMaxAptc(aptcTobeUsed);
					}
					if (memberStateSubsidyEligible) {
						existingGroup.setMaxStateSubsidy(stateSubsidyTobeUsed);
					}
				}
			  }
				newMemberAdded = true;
				break;
			}
		}

		if (!newMemberAdded) {
			Group newGroup = new Group();
			newGroup.setId(groupList.size() + 1);
			if ("Health".equalsIgnoreCase(insuranceType)) {
				newGroup.setGroupType(GroupType.HEALTH);
				if(StringUtils.isNotBlank(ssapApplicant.getCsrLevel())) {
					newGroup.setGroupCsr(ssapApplicant.getCsrLevel());
				}else {
					newGroup.setGroupCsr("CS1");
				}
				newGroup.setProgramType(ProgramType.H);
			} else if ("Dental".equalsIgnoreCase(insuranceType)) {
				newGroup.setGroupType(GroupType.DENTAL);
				newGroup.setProgramType(ProgramType.D);
			}
			newGroup.setHasAPTCEligibleMembers(memberAPTCEligible);
			if(memberStateSubsidyEligible) {
				newGroup.setHasStateSubsidyEligibleMembers(memberStateSubsidyEligible);
			}
			newGroup.setGroupEnrollmentId(enrollmentDTO.getEnrollmentID().toString());
			newGroup.setEncryptedGroupEnrollmentId(ghixJasyptEncrytorUtil.encryptStringByJasypt(enrollmentDTO.getEnrollmentID().toString()));
			if ((isSEP && !hasSameEnrollmentId) || (appGroupRequest.getPostAptcRedistributionFlag() != null && appGroupRequest.getPostAptcRedistributionFlag())){
				BigDecimal groupMaxAptc = newGroup.getMaxAptc();
				if(memberAptc != null){
					if (groupMaxAptc == null){
						newGroup.setMaxAptc(memberAptc);
					} else {
						newGroup.setMaxAptc(newGroup.getMaxAptc().add(memberAptc));
					}
				}
					BigDecimal groupMaxStateSubsidy = newGroup.getMaxStateSubsidy();
					if (memberStateSubsidy != null) {
						if (groupMaxStateSubsidy == null) {
							newGroup.setMaxStateSubsidy(memberStateSubsidy);
						} else {
							newGroup.setMaxStateSubsidy(newGroup.getMaxStateSubsidy().add(memberStateSubsidy));
						}
					}
			} else {
				if(memberAPTCEligible) {
					newGroup.setMaxAptc(enrollmentAPTC);
				}
				if (memberStateSubsidyEligible) {
					newGroup.setMaxStateSubsidy(enrollmentStateSubsidy);
				}
			}
			List<Member> memberList = new ArrayList<Member>();
			memberList.add(newMember);
           
			if((isHealthRenewal && isPrevYearEnrollment && "Health".equalsIgnoreCase(insuranceType)) || (isDentalRenewal && isPrevYearEnrollment && "Dental".equalsIgnoreCase(insuranceType))) {
				newGroup.setIsRenewal(true);
			}
			else {
				newGroup.setIsRenewal(false);
			}
			newGroup.setMemberList(memberList);
			newGroup.setCoverageDate(DateUtil.dateToString(enrollmentDTO.getBenefitEffectiveDate(), "MM/dd/yyyy"));
			newGroup.setHiosIssuerId(enrollmentDTO.getHiosIssuerId());
			newGroup.setIssuerName(enrollmentDTO.getIssuerName());
			newGroup.setPlanName(enrollmentDTO.getPlanName());
			newGroup.setEnrollmentApplicationId(enrollmentDTO.getSsapApplicationID());
			newGroup.setEnrollmentPriorApplicationId(enrollmentDTO.getPriorSsapApplicationid());
			newGroup.setElectedAptc(electedAPTC);
			newGroup.setElectedStateSubsidy(electedStateSubsidy);
			newGroup.setAllowChangePlan(enrollmentDTO.getAllowChangePlan());
			if(enrollmentDTO.getEnrollmentCreationDate() != null) {
				newGroup.setEnrollmentCreationDate(DateUtil.dateToString(enrollmentDTO.getEnrollmentCreationDate(), "MM/dd/yyyy"));
			}
			newGroup.setEnrollmentEndDate(DateUtil.dateToString(enrollmentDTO.getBenefitEndDate(), "MM/dd/yyyy"));
			newGroup.setNetPremium(enrollmentDTO.getNetPremium());
			newGroup.setEnrollmentStatus(enrollmentDTO.getEnrollmentStatus());

			boolean allowDisenroll =  getDisenrollStatus(newGroup);
			newGroup.setAllowDisenroll(allowDisenroll);
			
			groupList.add(newGroup);
		}
	}
	
	
	private String getMinMemberCsr(List<com.getinsured.hix.indportal.dto.Member> memberList) {
		String groupCsr = null;
		if(memberList != null){
			for(com.getinsured.hix.indportal.dto.Member member : memberList){
				if(member != null){
					String memberCsr = StringUtils.isNotBlank(member.getCsr()) ? member.getCsr() : "CS1";
					if("CS1".equalsIgnoreCase(memberCsr)) {
						return memberCsr;
					}
					if(groupCsr == null){
						groupCsr = memberCsr;
					}
					if(!"CS2".equalsIgnoreCase(memberCsr) && !"CS3".equalsIgnoreCase(memberCsr)) {
						groupCsr = memberCsr;
					}
				}
			}					
		}
		return groupCsr;
   }
	
	private String getEnrollmentsByMember(List<SsapApplicant> ssapApplicants, String coverageYear) {
		List<String> applicantGuidList = new ArrayList<String>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidList.add(ssapApplicant.getApplicantGuid());
		}
		
		List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
		enrollmentStatusList.add(EnrollmentStatus.PENDING);
		enrollmentStatusList.add(EnrollmentStatus.CONFIRM);
		enrollmentStatusList.add(EnrollmentStatus.TERM);
		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setMemberIdList(applicantGuidList);
		enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
		
		List<String> coverageYearList = new ArrayList<String>();
		coverageYearList.add(coverageYear);
		enrollmentRequest.setCoverageYearList(coverageYearList);
		String enrollmentResponseStr = ghixRestTemplate.exchange(EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_MEMBERID_LIST, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
	
		return enrollmentResponseStr;
	}
	
	private Member createMember(SsapApplicant ssapApplicant, BigDecimal memberAptc, BigDecimal memberStateSubsidy, String coverageStartDate) {

		Member newMember = new Member();
		newMember.setSuffix(ssapApplicant.getNameSuffix());
		newMember.setId(ssapApplicant.getApplicantGuid());
		newMember.setFirstName(ssapApplicant.getFirstName());
		newMember.setMiddleName(ssapApplicant.getMiddleName());
		newMember.setLastName(ssapApplicant.getLastName());
		newMember.setDob(DateUtil.dateToString(ssapApplicant.getBirthDate(), "MM/dd/yyyy"));
		newMember.setRelation(ssapApplicant.getRelationship());
		if(StringUtils.isNotBlank(ssapApplicant.getCsrLevel())) {
			newMember.setCsr(ssapApplicant.getCsrLevel());
		}else {
			newMember.setCsr("CS1");
		}
		newMember.setIsNativeAmerican(ssapApplicant.getNativeAmericanFlag());
		
		if(memberAptc != null)
		{
			newMember.setAptc(memberAptc);
		}

		if(memberStateSubsidy != null){
			newMember.setStateSubsidy(memberStateSubsidy);
		}
		
		Integer age = customGroupingUtil.getAgeFromCoverageDate(DateUtil.StringToDate(coverageStartDate, "MM/dd/yyyy"), ssapApplicant.getBirthDate());
		newMember.setAgeAtCoverageDate(age);
		
		return newMember;
	}
	
	private List<Group> addRemainingMembers(List<SsapApplicant> ssapApplicants, String coverageStartDate, List<Group> groupList, BigDecimal maxAptc, BigDecimal maxStateSubsidy, String type, Map<String, List<EligibilityProgram>> programEligibilityMap, String applicationType, Boolean isRenewal, Boolean isRenewed, String nonStandardConfig) {

		AptcRatioResponse aptcRatioResponse = null;
		if(maxAptc != null) {
			aptcRatioResponse = getAptcForRemainingMembers(ssapApplicants, coverageStartDate, groupList, maxAptc, programEligibilityMap, type, EligibilityType.APTC_ELIGIBILITY_TYPE,isRenewal);
			if(isRenewal) {
				groupList = updateAptcForPrevEnrollments(groupList,aptcRatioResponse,type);
			
			}
		}
       
		
		AptcRatioResponse stateSubsidyRatioResponse = null;
		if(maxStateSubsidy != null && maxStateSubsidy.compareTo(BigDecimal.ZERO) > 0 ) {
			stateSubsidyRatioResponse = getAptcForRemainingMembers(ssapApplicants, coverageStartDate, groupList, maxStateSubsidy, programEligibilityMap, type, EligibilityType.STATE_SUBSIDY_ELIGIBILITY_TYPE,isRenewal);
			if(isRenewal) {
				groupList = updateSubsidyForPrevEnrollments(groupList,stateSubsidyRatioResponse,type);
			
			}
		}
          
				
		
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			if(!isMemberPresentInExistingGroup(ssapApplicant.getApplicantGuid(), groupList, type)) {
				// Add check for health
				BigDecimal memberAptc = getAptcOfMember(ssapApplicant.getApplicantGuid(), aptcRatioResponse);
				BigDecimal memberStateSubsidy = getAptcOfMember(ssapApplicant.getApplicantGuid(), stateSubsidyRatioResponse);

				Member newMember = createMember(ssapApplicant, memberAptc, memberStateSubsidy, coverageStartDate);
				// HEALTH: Check if member can be added in existing NON enrolled group. If member can go into enrolled group, 
				//			then DO NOT ADD that member into enrolled group, instead create a new group according to CS level 
				//			and mention possible enrolled group where the member can merge
				// DENTAL: Check if there is existing dental group. If present add the member to that group else create new group
				//newMember.setEnrolledInHealth(false);
				//newMember.setEnrolledInDental(false);
				boolean memberAPTCEligible = false;
				boolean memberStateSubsidyEligible = false;
				boolean isNonStandardRelation = false;

		        if(!nonStandardConfig.isEmpty() && nonStandardConfig != null) {
		        	if("true".equalsIgnoreCase(nonStandardConfig)) {
		        		isNonStandardRelation = checkIsNonStandardRelation(newMember.getRelation());
		        	}
				}			

				if(programEligibilityMap != null)
				{
					memberAPTCEligible = checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()),EligibilityType.APTC_ELIGIBILITY_TYPE);
					memberStateSubsidyEligible = checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()),EligibilityType.STATE_SUBSIDY_ELIGIBILITY_TYPE);
				}
				boolean newMemberAdded = false;
				for(Group existingGroup : groupList) {
					if("Health".equals(type) && GroupType.HEALTH.equals(existingGroup.getGroupType())) {
						if((StringUtils.isBlank(existingGroup.getGroupCsr()) && StringUtils.isBlank(newMember.getCsr())) || (StringUtils.isNotBlank(existingGroup.getGroupCsr()) && existingGroup.getGroupCsr().equalsIgnoreCase(newMember.getCsr())) && !isNonStandardRelation && !existingGroup.getIsNonStandardRelationGroup()) {							
							if("MN".equalsIgnoreCase(stateCode) || ((existingGroup.isHasAPTCEligibleMembers() == (memberAPTCEligible )) && (existingGroup.isHasStateSubsidyEligibleMembers() == memberStateSubsidyEligible))){
								if(StringUtils.isNotBlank(existingGroup.getGroupEnrollmentId())) {
									if("SEP".equals(applicationType) || "LCE".equals(applicationType) || isRenewal || isRenewed) {
										List<String> canBeAddedInEnrolledGroup = newMember.getCanBeAddedInEnrolledGroup();
										if(CollectionUtils.isEmpty(canBeAddedInEnrolledGroup))
										{
											canBeAddedInEnrolledGroup = new ArrayList<String>();
											newMember.setCanBeAddedInEnrolledGroup(canBeAddedInEnrolledGroup);
										}
										canBeAddedInEnrolledGroup.add(existingGroup.getGroupEnrollmentId());
									}
								}else{
									existingGroup.getMemberList().add(newMember);
									if(newMember.getAptc() != null) {
										if(existingGroup.getMaxAptc() != null) {
											existingGroup.setMaxAptc(newMember.getAptc().add(existingGroup.getMaxAptc()));
										}else {
											existingGroup.setMaxAptc(newMember.getAptc());
										}
									}
									if(newMember.getStateSubsidy() != null) {
										if(existingGroup.getMaxStateSubsidy() != null) {
											existingGroup.setMaxStateSubsidy(newMember.getStateSubsidy().add(existingGroup.getMaxStateSubsidy()));
										}else {
											existingGroup.setMaxStateSubsidy(newMember.getStateSubsidy());
										}
									}
									newMemberAdded = true;
									break;
								}
							}
						}						
					}
					if("Dental".equals(type) && GroupType.DENTAL.equals(existingGroup.getGroupType())) {
						existingGroup.getMemberList().add(newMember);
						if(StringUtils.isBlank(existingGroup.getGroupEnrollmentId())){
							if(newMember.getAptc() != null) {
								if(existingGroup.getMaxAptc() != null) {
									existingGroup.setMaxAptc(newMember.getAptc().add(existingGroup.getMaxAptc()));
								}else {
									existingGroup.setMaxAptc(newMember.getAptc());
								}
							}
							if(newMember.getStateSubsidy() != null) {
								if(existingGroup.getMaxStateSubsidy() != null) {
									existingGroup.setMaxStateSubsidy(newMember.getStateSubsidy().add(existingGroup.getMaxStateSubsidy()));
								}else {
									existingGroup.setMaxStateSubsidy(newMember.getStateSubsidy());
								}
							}
						}						
						newMemberAdded = true;
						break;
					}	
				}
				
					
				if(!newMemberAdded) {
					Group newGroup = new Group();
					newGroup.setId(groupList.size() + 1);
					if("Health".equalsIgnoreCase(type)){
						if(StringUtils.isNotBlank(ssapApplicant.getCsrLevel())) {
							newGroup.setGroupCsr(ssapApplicant.getCsrLevel());
						}else {
							newGroup.setGroupCsr("CS1");
						}
						newGroup.setGroupType(GroupType.HEALTH);
						newGroup.setProgramType(ProgramType.H);
					}
					else if("Dental".equals(type)){
						newGroup.setGroupType(GroupType.DENTAL);
						newGroup.setProgramType(ProgramType.D);
					}
					newGroup.setHasAPTCEligibleMembers(memberAPTCEligible);
					newGroup.setMaxAptc(newMember.getAptc());
					if(memberStateSubsidyEligible) {
						newGroup.setHasStateSubsidyEligibleMembers(memberStateSubsidyEligible);
						newGroup.setMaxStateSubsidy(newMember.getStateSubsidy());
					}
					
					if(isNonStandardRelation) {
						newGroup.setIsNonStandardRelationGroup(true);
					}	
					List<Member> memberList = new ArrayList<Member>();
					memberList.add(newMember);
					
					newGroup.setMemberList(memberList);
					newGroup.setCoverageDate(coverageStartDate);
					//--- copy similar things and create two groups 1 for health and one for dental ---
					groupList.add(newGroup);
				}
			}
		}
		
		return groupList;
	}
	
	private boolean isMemberPresentInExistingGroup(String applicantGuid, List<Group> groupList, String type) {
		if(groupList != null && !groupList.isEmpty()) {
			for(Group existingGroup : groupList) {
				if(type.equalsIgnoreCase(existingGroup.getGroupType().toString())) {
					for(Member existingMember : existingGroup.getMemberList()) {
						if(applicantGuid.equalsIgnoreCase(existingMember.getId())) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	private boolean isMemberPresentInLastYearGroups(String applicantGuid, List<Group> groupList, String type) {
		if(groupList != null && !groupList.isEmpty()) {
			for(Group existingGroup : groupList) {
				if(type.equalsIgnoreCase(existingGroup.getGroupType().toString()) && existingGroup.getIsRenewal()) {
					for(Member existingMember : existingGroup.getMemberList()) {
						if(applicantGuid.equalsIgnoreCase(existingMember.getId())) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	private BigDecimal getAptcOfMember(String applicantGuid, AptcRatioResponse aptcRatioResponse) {
		BigDecimal aptc = null;
		if(aptcRatioResponse != null && aptcRatioResponse.getMemberList() != null && !aptcRatioResponse.getMemberList().isEmpty()) {
			for(AptcRatioResponse.Member aptcMemberObj : aptcRatioResponse.getMemberList()) {
				if(applicantGuid.equalsIgnoreCase(aptcMemberObj.getId())) {
					return aptcMemberObj.getAptc();
				}
			}
		}
		return aptc;
	}
	
	private AptcRatioResponse getAptcForRemainingMembers(List<SsapApplicant> ssapApplicants, String coverageStartDate, List<Group> groupList, BigDecimal remainingAptc, Map<String, List<EligibilityProgram>> programEligibilityMap, String type, EligibilityType elibilityType,Boolean isRenewal) {
		AptcRatioResponse aptcRatioResponse = new AptcRatioResponse();

		String coverageYear = coverageStartDate.substring(6, coverageStartDate.length());
		Date coverageDate = DateUtil.StringToDate(coverageStartDate, "MM/dd/yyyy");
		
		AptcRatioRequest aptcRatioRequest = new AptcRatioRequest();
		aptcRatioRequest.setCoverageYear(Integer.parseInt(coverageYear));
		aptcRatioRequest.setMaxAptc(remainingAptc);
		
		List<AptcRatioRequest.Member> memberList= new ArrayList<AptcRatioRequest.Member>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			if(!isMemberPresentInExistingGroup(ssapApplicant.getApplicantGuid(), groupList, type) || (isRenewal && isMemberPresentInLastYearGroups(ssapApplicant.getApplicantGuid(), groupList, type))) {
				// Check if member is eligible for APTC
				if(checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()), elibilityType))
				{
					AptcRatioRequest.Member newMember = aptcRatioRequest.new Member();
					newMember.setId(ssapApplicant.getApplicantGuid());
					
					LocalDate personBirthdate = new LocalDate(ssapApplicant.getBirthDate());
			    		LocalDate todaysDate = new LocalDate(coverageDate);
			    		Period period = new Period(personBirthdate, todaysDate, PeriodType.yearMonthDay());
			    		Integer age = period!=null ? period.getYears():0;
			    
					newMember.setAge(age);
					memberList.add(newMember);
				}
			}
		}
		if(memberList.size() > 0) {
			aptcRatioRequest.setMemberList(memberList);
			
			Object responseObj = customGroupingUtil.getAptcDistribution(aptcRatioRequest);
			if(responseObj instanceof AptcRatioResponse) {
				aptcRatioResponse = (AptcRatioResponse) responseObj;
			}
		}
		return aptcRatioResponse;
	}
	
	private Map<String,BigDecimal> getAptcForMembers(List<SsapApplicant> ssapApplicants, AppGroupRequest appGroupRequest, SsapApplication ssapApplication, Map<String, List<EligibilityProgram>> programEligibilityMap, Map<String, List<EnrollmentMemberDataDTO>> enrolleeData, Map<String,BigDecimal> remainingDentalAptc, String insuranceType) {
		AptcRatioResponse aptcRatioResponse = new AptcRatioResponse();
		Map<String,BigDecimal> allMembersAPTC = new HashMap<String,BigDecimal>();
		Date coverageDate = DateUtil.StringToDate(appGroupRequest.getCoverageStartDate(), "MM/dd/yyyy");
		
		AptcRatioRequest aptcRatioRequest = new AptcRatioRequest();
		aptcRatioRequest.setCoverageYear((int) ssapApplication.getCoverageYear());
		BigDecimal remainingAptc = ssapApplication.getMaximumAPTC();
		if("Dental".equals(insuranceType) && !remainingDentalAptc.isEmpty()){
			remainingAptc = remainingDentalAptc.get("dentalAptc");
		}
		if(remainingAptc != null){
			aptcRatioRequest.setMaxAptc(remainingAptc);
		}
		
		List<AptcRatioRequest.Member> memberList= new ArrayList<AptcRatioRequest.Member>();
		List<Integer> enrollmentsAptcSubtracted = new ArrayList<Integer>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			boolean memberNeedAPTCDistribution = true;
			// Check if member is eligible for APTC
			if(checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()),EligibilityType.APTC_ELIGIBILITY_TYPE)){
				if(enrolleeData != null && enrolleeData.get(ssapApplicant.getApplicantGuid()) != null) {
					List<EnrollmentMemberDataDTO> enrolledMemberData = enrolleeData.get(ssapApplicant.getApplicantGuid());
					if(CollectionUtils.isNotEmpty(enrolledMemberData)) {
						for(EnrollmentMemberDataDTO enrollmentMemberDataDTO : enrolledMemberData) {
							if(customGroupingUtil.isEnrollmentActive(enrollmentMemberDataDTO)){
								if(("HEALTH".equalsIgnoreCase(enrollmentMemberDataDTO.getInsuranceType()) && "Health".equals(insuranceType)) 
										|| ("DENTAL".equalsIgnoreCase(enrollmentMemberDataDTO.getInsuranceType()) && "Dental".equals(insuranceType))) {
									if(enrollmentMemberDataDTO.getPriorSsapApplicationid() != null && enrollmentMemberDataDTO.getPriorSsapApplicationid().longValue() == ssapApplication.getId() 
											&& (appGroupRequest.getPostAptcRedistributionFlag() == null || !appGroupRequest.getPostAptcRedistributionFlag())) {
										memberNeedAPTCDistribution = false;
										if(!enrollmentsAptcSubtracted.contains(enrollmentMemberDataDTO.getEnrollmentID())) {
											enrollmentsAptcSubtracted.add(enrollmentMemberDataDTO.getEnrollmentID());
											BigDecimal enrollmentAPTC = getEnrollmentAptc(enrollmentMemberDataDTO);
											if(aptcRatioRequest.getMaxAptc() != null && enrollmentAPTC != null){
												aptcRatioRequest.setMaxAptc(aptcRatioRequest.getMaxAptc().subtract(enrollmentAPTC));
												remainingDentalAptc.put("dentalAptc", aptcRatioRequest.getMaxAptc()) ; 
											}											
										}
									} else if(enrollmentMemberDataDTO.getBenefitEffectiveDate() != null) {
										coverageDate = enrollmentMemberDataDTO.getBenefitEffectiveDate();
									}
								}
							}							
						}
					}
					if(memberNeedAPTCDistribution) {
						LocalDate todaysDate = new LocalDate(coverageDate);
						LocalDate personBirthdate = new LocalDate(ssapApplicant.getBirthDate());
						Period period = new Period(personBirthdate, todaysDate, PeriodType.yearMonthDay());
						Integer age = period!=null ? period.getYears():0;
						AptcRatioRequest.Member newMember = aptcRatioRequest.new Member();
						newMember.setId(ssapApplicant.getApplicantGuid());
						newMember.setAge(age);
						memberList.add(newMember);
					}
				}
				else
				{
					LocalDate todaysDate = new LocalDate(coverageDate);
					LocalDate personBirthdate = new LocalDate(ssapApplicant.getBirthDate());
					Period period = new Period(personBirthdate, todaysDate, PeriodType.yearMonthDay());
					Integer age = period!=null ? period.getYears():0;
					AptcRatioRequest.Member newMember = aptcRatioRequest.new Member();
					newMember.setId(ssapApplicant.getApplicantGuid());
					newMember.setAge(age);
					memberList.add(newMember);
				}
			}
		}
		
		if(memberList.size() > 0) {
			aptcRatioRequest.setMemberList(memberList);
			
			Object responseObj = customGroupingUtil.getAptcDistribution(aptcRatioRequest);
			if(responseObj instanceof AptcRatioResponse) {
				aptcRatioResponse = (AptcRatioResponse) responseObj;
			}
		}
		if(aptcRatioResponse != null && aptcRatioResponse.getMemberList()!=null && !aptcRatioResponse.getMemberList().isEmpty()){
			for(AptcRatioResponse.Member member :aptcRatioResponse.getMemberList() )
			{
				allMembersAPTC.put(member.getId(),member.getAptc());
			}
		}
		
		return allMembersAPTC;
	}

	private Map<String,BigDecimal> getStateSubsidyForMembers(List<SsapApplicant> ssapApplicants, AppGroupRequest appGroupRequest, SsapApplication ssapApplication, Map<String, List<EligibilityProgram>> programEligibilityMap, Map<String, List<EnrollmentMemberDataDTO>> enrolleeData, Map<String,BigDecimal> remainingDentalSubsidy, String insuranceType) {
		AptcRatioResponse aptcRatioResponse = new AptcRatioResponse();
		Map<String,BigDecimal> allMembersStateSubsidy = new HashMap<String,BigDecimal>();
		Date coverageDate = DateUtil.StringToDate(appGroupRequest.getCoverageStartDate(), "MM/dd/yyyy");

		AptcRatioRequest aptcRatioRequest = new AptcRatioRequest();
		aptcRatioRequest.setCoverageYear((int) ssapApplication.getCoverageYear());
		BigDecimal remainingStateSubsidy = ssapApplication.getMaximumStateSubsidy();
		if("Dental".equals(insuranceType) && !remainingDentalSubsidy.isEmpty()){
			remainingStateSubsidy = remainingDentalSubsidy.get("dentalAptc");
		}
		if(remainingStateSubsidy != null){
			aptcRatioRequest.setMaxAptc(remainingStateSubsidy);
		}

		List<AptcRatioRequest.Member> memberList= new ArrayList<AptcRatioRequest.Member>();
		List<Integer> enrollmentsStateSubsidySubtracted = new ArrayList<Integer>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			boolean memberNeedStateSubsidyDistribution = true;
			// Check if member is eligible for APTC
			if(checkEligibility(programEligibilityMap.get(ssapApplicant.getApplicantGuid()),EligibilityType.STATE_SUBSIDY_ELIGIBILITY_TYPE)){
				if(enrolleeData != null && enrolleeData.get(ssapApplicant.getApplicantGuid()) != null) {
					List<EnrollmentMemberDataDTO> enrolledMemberData = enrolleeData.get(ssapApplicant.getApplicantGuid());
					if(CollectionUtils.isNotEmpty(enrolledMemberData)) {
						for(EnrollmentMemberDataDTO enrollmentMemberDataDTO : enrolledMemberData) {
							if(customGroupingUtil.isEnrollmentActive(enrollmentMemberDataDTO)){
								if(("HEALTH".equalsIgnoreCase(enrollmentMemberDataDTO.getInsuranceType()) && "Health".equals(insuranceType)) 
										|| ("DENTAL".equalsIgnoreCase(enrollmentMemberDataDTO.getInsuranceType()) && "Dental".equals(insuranceType))) {
									if(enrollmentMemberDataDTO.getPriorSsapApplicationid() != null && enrollmentMemberDataDTO.getPriorSsapApplicationid().longValue() == ssapApplication.getId() 
											&& (appGroupRequest.getPostAptcRedistributionFlag() == null || !appGroupRequest.getPostAptcRedistributionFlag())) {
										memberNeedStateSubsidyDistribution = false;
										if(!enrollmentsStateSubsidySubtracted.contains(enrollmentMemberDataDTO.getEnrollmentID())) {
											enrollmentsStateSubsidySubtracted.add(enrollmentMemberDataDTO.getEnrollmentID());
											BigDecimal enrollmentStateSubsidy = getEnrollmentStateSubsidy(enrollmentMemberDataDTO);
											if(aptcRatioRequest.getMaxAptc() != null && enrollmentStateSubsidy != null){
												aptcRatioRequest.setMaxAptc(aptcRatioRequest.getMaxAptc().subtract(enrollmentStateSubsidy));
												remainingDentalSubsidy.put("dentalAptc", aptcRatioRequest.getMaxAptc()) ;
											}
										}
									} else if(enrollmentMemberDataDTO.getBenefitEffectiveDate() != null) {
										coverageDate = enrollmentMemberDataDTO.getBenefitEffectiveDate();
									}
								}
							}
						}
					}
					if(memberNeedStateSubsidyDistribution) {
						LocalDate todaysDate = new LocalDate(coverageDate);
						LocalDate personBirthdate = new LocalDate(ssapApplicant.getBirthDate());
						Period period = new Period(personBirthdate, todaysDate, PeriodType.yearMonthDay());
						Integer age = period!=null ? period.getYears():0;
						AptcRatioRequest.Member newMember = aptcRatioRequest.new Member();
						newMember.setId(ssapApplicant.getApplicantGuid());
						newMember.setAge(age);
						memberList.add(newMember);
					}
				}
				else
				{
					LocalDate todaysDate = new LocalDate(coverageDate);
					LocalDate personBirthdate = new LocalDate(ssapApplicant.getBirthDate());
					Period period = new Period(personBirthdate, todaysDate, PeriodType.yearMonthDay());
					Integer age = period!=null ? period.getYears():0;
					AptcRatioRequest.Member newMember = aptcRatioRequest.new Member();
					newMember.setId(ssapApplicant.getApplicantGuid());
					newMember.setAge(age);
					memberList.add(newMember);
				}
			}
		}

		if(memberList.size() > 0) {
			aptcRatioRequest.setMemberList(memberList);

			Object responseObj = customGroupingUtil.getAptcDistribution(aptcRatioRequest);
			if(responseObj instanceof AptcRatioResponse) {
				aptcRatioResponse = (AptcRatioResponse) responseObj;
			}
		}
		if(aptcRatioResponse != null && aptcRatioResponse.getMemberList()!=null && !aptcRatioResponse.getMemberList().isEmpty()){
			for(AptcRatioResponse.Member member :aptcRatioResponse.getMemberList() )
			{
				allMembersStateSubsidy.put(member.getId(),member.getAptc());
			}
		}

		return allMembersStateSubsidy;
	}
	
	private void ignoreTermHealthEnrollment(Map<String, List<EnrollmentMemberDataDTO>> enrolleeData, List<SsapApplicant> ssapApplicants) {
		if(enrolleeData != null) {
			for(SsapApplicant ssapApplicant : ssapApplicants) {
				List<EnrollmentMemberDataDTO> enrollmentMemberDTOList = enrolleeData.get(ssapApplicant.getApplicantGuid());
				int healthEnrlCount = 0;
				if(CollectionUtils.isNotEmpty(enrollmentMemberDTOList)){
					for(EnrollmentMemberDataDTO enrollmentMemberDTO : enrollmentMemberDTOList){
						if("HEALTH".equalsIgnoreCase(enrollmentMemberDTO.getInsuranceType())){
							healthEnrlCount++;
						}
					}
					if(healthEnrlCount > 1){
						Iterator<EnrollmentMemberDataDTO> itrEnrlMemberDtoList = enrollmentMemberDTOList.iterator();
						while(itrEnrlMemberDtoList.hasNext()) {
							EnrollmentMemberDataDTO enrollmentMemberDTO = itrEnrlMemberDtoList.next();
							if("HEALTH".equalsIgnoreCase(enrollmentMemberDTO.getInsuranceType()) && "TERM".equalsIgnoreCase(enrollmentMemberDTO.getEnrollmentStatus())){
								itrEnrlMemberDtoList.remove();
							}
						}						
					}									
				}
			}
		}
	}
	
	private void ignoreTermPrevEnrollment(Map<String, List<EnrollmentMemberDataDTO>> enrolleeData, List<SsapApplicant> ssapApplicants) {
		if(enrolleeData != null && ssapApplicants != null) {
			for(SsapApplicant ssapApplicant : ssapApplicants) {
				List<EnrollmentMemberDataDTO> enrollmentDTOList = enrolleeData.get(ssapApplicant.getApplicantGuid());
				if(enrollmentDTOList != null && !enrollmentDTOList.isEmpty()) {					
					Iterator<EnrollmentMemberDataDTO> itrEnrlMemberDtoList = enrollmentDTOList.iterator();
					while(itrEnrlMemberDtoList.hasNext()) {
						EnrollmentMemberDataDTO enrollmentMemberDTO = itrEnrlMemberDtoList.next();
						if("HEALTH".equalsIgnoreCase(enrollmentMemberDTO.getInsuranceType()) && ("TERM".equalsIgnoreCase(enrollmentMemberDTO.getEnrollmentStatus()) 
								|| !customGroupingUtil.isEnrolleeActive(enrollmentMemberDTO))){
							itrEnrlMemberDtoList.remove();
						}
					}	
					
				}
			}
		}
	}

	
	private boolean getDisenrollStatus(Group group) {
		if (group.getEnrollmentStatus() != null && group.getEnrollmentStatus().equalsIgnoreCase("TERM")
				|| group.getEnrollmentStatus().equalsIgnoreCase("CANCEL")
				|| group.getEnrollmentStatus().equalsIgnoreCase("ABORTED")) {
			return false;
		} else if (group.getEnrollmentStatus() == null) {
			return false;
		}
		return true;
	}	
	
	private void saveIntegrationLog(CustomGroupingRequestDTO customGroupingRequestDTO, Long ssapId, String status) {
		try{
			String payload = platformGson.toJson(customGroupingRequestDTO);
			integrationLogService.save(payload, "CUSTOM_GROUPING", status, ssapId, null);
		}catch(Exception e){
			LOGGER.error("----Error while saving in Integration Log table ----", e);			
		}
		
	}
	
}
