(function (views) {
	views.Tiers = Backbone.View.extend({
		events : {
			'click a.selectTier': 'selectTier',
			'click a.removeTier': 'removeTier',
			'click a.getSelectedTier': 'getSelectedTier'
					
		},
		el : '#summary',
		initialize : function (options) {
			_.bindAll (this, 'render', 'addAll', 'loadAll', 'addOne','selectTier','removeTier','getSelectedTier');
			var self = this;
			
			
			self.collection.fetch({
				success : function () {
					
					if(self.collection.length==0){
						$('#summary').empty();
						$('#summary').hide();
						$('#summary_err').show();
					}
						
					self.collection.pager();
					self.loadAll();
				},
				silent:true
			});
			
			self.tierModel = options.tierModel;
			$('.selectTier').bind('click', this.selectTier);
			$('.getSelectedTier').bind('click', this.getSelectedTier);
			self.collection.bind('reset', self.addAll);
			
			
		},
		addAll : function () {
			var self = this;
			$('#summary').empty();
			$('#employerCost').empty();
			$('#employeeCost').empty();
			var maxEmployerCost=0;
			var maxEmployeeCost=0;
			self.collection.each (function(model){
				if(model.get("maxMonthlyEmployerCost") > maxEmployerCost){
					maxEmployerCost = Math.round(model.get("maxMonthlyEmployerCost"));
				}
				if(model.get("maxMonthlyEmployeeCost") > maxEmployeeCost){
					maxEmployeeCost = Math.round(model.get("maxMonthlyEmployeeCost"));
				}
			});
			
			$('#maxEmployerCost').val(maxEmployerCost+10);
			$('#maxEmployeeCost').val(maxEmployeeCost+10);
			
			self.collection.each (self.addOne);
		},
		
		loadAll : function () {
			var self = this;
			self.collection.each (function(model){
				
				if($('#tierType').val() == model.get("tierName") && $('#tierType').val() !=''){
					$('#tier_'+model.get("tierName")).hide();
					$('#removetier_'+ model.get("tierName")).show();
					self.collection.selectedTier =$('#tierType').val();
				}else{
					$('#tier_'+model.get("tierName")).show();
					$('#removetier_'+ model.get("tierName")).hide();
				}
			});
			
		},
		
		addOne : function (model) {
			var viewSummary = new Summary({model:model}); 
			$('#summary').append(viewSummary.render().el);
			$('#employerCost').append(viewSummary.getEmployerCost());
			$('#employeeCost').append(viewSummary.getEmployeeCost());
			$('#acturialValue').append(viewSummary.getActurials());
			
			var employerCostArray = new Array(model.get("minMonthlyEmployerCost"),model.get("maxMonthlyEmployerCost"));
			var employeeCostArray = new Array(model.get("minMonthlyEmployeeCost"),model.get("maxMonthlyEmployeeCost"));
			
			drawbar('Employer_'+model.get("tierName"), model.get("tierName"), employerCostArray,'#67c2c2','#009999',$('#maxEmployerCost').val());
			drawbar('Employee_'+model.get("tierName"), model.get("tierName"), employeeCostArray,'#999999','#666666',$('#maxEmployeeCost').val());
			
			if(model.get("tierName") == 'BRONZE'){
			drawpie('pie_'+model.get("tierName"), model.get("tierName"),40 ,60 );
			}else if(model.get("tierName") == 'SILVER'){
				drawpie('pie_'+model.get("tierName"), model.get("tierName"),30 ,70 );
			}else if(model.get("tierName") == 'GOLD'){
				drawpie('pie_'+model.get("tierName"), model.get("tierName"),20 ,80 );
			}else if(model.get("tierName") == 'PLATINUM'){
				drawpie('pie_'+model.get("tierName"), model.get("tierName"),10 ,90 );
			}
			
			var vMinPremium = model.get("minPremium");
			model.set({ minPremium : formatCurrency(vMinPremium)});
			var vMaxPremium = model.get("maxPremium");
			model.set({ maxPremium : formatCurrency(vMaxPremium)});
			
			model.set({tier : capitalizeString(model.get("tierName"))});
		},
		selectTier: function (e) {
			var self=this;
			var allEmployeeTiers="";
			// if tier plans are not available for selected tier
			self.collection.each (function(model){
				
				if(model.get("tierName") == e.target.id.replace("tier_",""))
				{
					if(model.get("notAffordableEmployees") !="")
					{
						$('#planAvailability').modal("show");
						$('#tierName').html(model.get("tierName"));
						$('#notAvailableEmployees').html(model.get("notAffordableEmployees"));
					}	
				}
				
				if(model.get("notAffordableEmployees") =="")
				{
					allEmployeeTiers += model.get("tierName") + " ";
				}
				$('#availableTiers').html(allEmployeeTiers);

			});
			
			
			if(self.collection.selectedTier !=""){
				$('#'+'tier_'+ self.collection.selectedTier).show();
				$('#'+'removetier_'+ self.collection.selectedTier).hide();
				self.collection.selectedTier = e.target.id.replace("tier_","");
				$('#'+'tier_'+ self.collection.selectedTier).hide();
				$('#'+'removetier_'+ self.collection.selectedTier).show();
			}
			else{
				self.collection.selectedTier = e.target.id.replace("tier_","");
				$('#tier_'+self.collection.selectedTier).hide();
				$('#removetier_'+ self.collection.selectedTier).show();
			}
		},
		removeTier: function (e) {
			var self=this;
			self.collection.selectedTier = "";;
			$('#tier_'+e.target.id.replace("removetier_","")).show();
			$('#removetier_'+ e.target.id.replace("removetier_","")).hide();
		},
		getSelectedTier: function (e) {
			var self=this;
			if(self.collection.selectedTier !="" ){
				$('#missinginfo').modal('show');
				self.tierModel.save({id: self.collection.selectedTier},{success:function(){
					$('#missinginfo').modal('hide');
					location.href="benchmarkinfo";	
				}});
			}
			else{
				alert('Please select a plan level.');
			}
			
		}
	});
	function formatCurrency(num) {
			num = parseFloat(num).toFixed(0);
			num = num.toString().replace(/\$|\,/g,'');
			if(isNaN(num))
			num = "0";
			sign = (num == (num = Math.abs(num)));
			num = Math.floor(num*100+0.50000000001);
			cents = num%100;
			num = Math.floor(num/100).toString();
			if(cents<10)
			cents = "0" + cents;
			for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
			num = num.substring(0,num.length-(4*i+3))+','+
			num.substring(num.length-(4*i+3));
			return (((sign)?'':'-') + '$' + num);
		}
	var Summary = Backbone.View.extend({
		template: _.template($('#resultItemTemplateSummary').html()),
		template_employer_cost: _.template($('#employerCostSummary').html()),
		template_employee_cost: _.template($('#employeeCostSummary').html()),
		template_acturials: _.template($('#acturialSummary').html()),

		initialize: function() {
			this.model.bind('change', this.render, this);
			this.model.bind('destroy', this.remove, this);
		},
		
		render : function () {
		
			$(this.el).html(this.template(this.model.toJSON()));
			return this;
		},
		
		getEmployerCost : function () {
			return this.template_employer_cost(this.model.toJSON());
		},
		getEmployeeCost : function () {
			return this.template_employee_cost(this.model.toJSON());
		},
		getActurials : function () {
			return this.template_acturials(this.model.toJSON());
		}
	});
})(App.views);