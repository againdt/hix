<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.timeshift.util.TSDate"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<%
    Date tsDate = new TSDate();
    String serverDate = null;

    if(tsDate != null){
        java.text.DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        formatter.setLenient(false);
        serverDate = formatter.format(tsDate);
    }

%>
    <c:set var="serverDate" value="<%=serverDate%>" />

<style>
      #menu {
        display: none;
      }
      #contactRequestAckSuccess, #contactRequestAckFailure {
        width: 75%;
        text-align: center;
        padding: 10px 0 30px;
        margin: 0 auto;
      }
      #contactRequestAckSuccess p, #contactRequestAckFailure p {
        margin-bottom: 20px;
      }
</style>

<script>
 function sayHello(message){
	alert(message);
}
 
 window.reactApp = {
		    user: {
		      name: 'BTM',
		      email: 'example@example.com'
		    }
		  }
 
</script>

  <div id="root"></div>
	<script type="text/babel">

		class WhoAre extends React.Component {
		render() {

        	return (<p> Who are you ? </p>);
		}
	}


		class Greeting extends React.Component {
		    constructor(props){
				super(props);
				this.state = {
					showEnrollButton:'N',
					firstName: '',
					lastName: '',
				};
			}

			componentDidMount(){
        		this.setState({
            		showEnrollButton: document.getElementById("showEnrollButton").value,
					firstName : document.getElementById("firstName").value,
					lastName : document.getElementById("lastName").value,
        		});
  			}
		
    		render() {
				let btnClass = classNames({
      				foo: true,
      			});
        		return (
					<div>
						<p className={btnClass}> Hello world</p>
						<WhoAre />
					</div>
					
				);
    		}
		}
		ReactDOM.render(
    		<Greeting/>,
    		document.getElementById('root')
		);

	</script>
<div class="gutter10" id="individual-portal" ng-app="indPortalApp">
<%
 String dashboardVersion = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_DASHBOARD_VERSION);
%>
<c:set var="dashboardVersion" value="<%=dashboardVersion%>" />

<c:choose>
  <c:when test="${popup == true}">
	<%@ include file="popup.jsp" %>
  </c:when>
  <c:otherwise>
    <c:choose>
      <c:when test="${currentPageContext == 'multipleHousehold'}">
        <script>window.location.href = "${pageContext.request.contextPath}" + "/indportal#/multiHousehold";</script>
        <%--<%@ include file="multipleHousehold.jsp" %>--%>
      </c:when>
      <c:when test="${dashboardVersion == '2.0'}">
         <%@ include file="portalHome_CA.jsp" %>
         <%--<%@ include file="multipleHousehold.jsp" %>--%>
      </c:when>
      <c:otherwise>
         <jsp:include page="portalHome.jsp" flush="true"/>
      </c:otherwise>
    </c:choose>
  </c:otherwise>
</c:choose>

  <jsp:include page="multipleHousehold.jsp" flush="true"/>
  <jsp:include page="myApplications.jsp" flush="true"/>
  <jsp:include page="eligibilityResults.jsp" flush="true" />
  <jsp:include page="appeals.jsp" flush="true" />
  <jsp:include page="tobacco.jsp" flush="true" />
  <jsp:include page="plansummary.jsp" flush="true" />
  <jsp:include page="overrideHist.jsp" flush="true" />
  <jsp:include page="accordionTemplate.jsp" flush="true" />
  <jsp:include page="appealshistory.jsp" flush="true" />
  <jsp:include page="disenrollsep.jsp" flush="true" />
  <jsp:include page="qephome.jsp" flush="true" />
  <jsp:include page="qepeligible.jsp" flush="true" />
  <jsp:include page="qepnoteligible.jsp" flush="true" />
  <jsp:include page="customGrouping.jsp" flush="true" />
  <jsp:include page="enrollmenthistory.jsp" flush="true" />
  <jsp:include page="mypreferences.jsp" flush="true"/>

 	<div class="row-fluid">
	<!-- <div class="row-fluid" id="titlebar"> -->
    <h1 id="skip" class="indDashboard-welcome"><spring:message code="indportal.portalhome.welcome" javaScriptEscape="true"/>, ${firstName}&nbsp;${lastName}&nbsp;${suffix}</h1>
  </div>
  <sec:accesscontrollist hasPermission="IND_PORTAL_VIEW_AND_PRINT" domainObject="user">
	<div style="display:none" class="announcements alert alert-block">
		
	</div>
  </sec:accesscontrollist>
  <div class="row-fluid">
    <jsp:include page="leftNav.jsp" flush="true" />
      <div class="span9 dashboard-rightpanel margin30-b" id="rightpanel" ng-view></div>
    </div>

    <input id="firstName" name="firstName" type="hidden" value="${firstName}"/>
    <input id="lastName" name="lastName" type="hidden" value="${lastName}"/>

  <input id="multiHouseholdContext" name="multiHouseholdContext" type="hidden" value="${multiHouseholdContext}"/>
  <input id="aptc" name="aptc" type="hidden" value="${aptc}"/>
  <input id="csr" name="csr" type="hidden" value="${csr}"/>
  <input id="stage" name="stage" type="hidden" value="${stage}"/>
  <input id="issuerLogo" name="issuerLogo" type="hidden" value="${issuerLogo}"/>
  <input id="planName" name="planName" type="hidden" value="${planName}"/>
  <input id="permonth" name="permonth" type="hidden" value="${permonth}"/>
  <input id="monthlyPremium" name="monthlyPremium" type="hidden" value="${monthlyPremium}"/>
  <input id="favoritePlanSelected" name="favoritePlanSelected" type="hidden" value="${favoritePlanSelected}"/>
  <input id="planType" name="planType" type="hidden" value="${planType}"/>
  <input id="appliedCredit" name="appliedCredit" type="hidden" value="${appliedCredit}"/>
  <input id="officeVisit" name="officeVisit" type="hidden" value="${officeVisit}"/>
  <input id="genericMedication" name="genericMedication" type="hidden" value="${genericMedication}"/>
  <input id="deductible" name="deductible" type="hidden" value="${deductible}"/>
  <input id="outOfPocket" name="outOfPocket" type="hidden" value="${outOfPocket}"/>
  <input id="applicationStartLabel" name="applicationStartLabel" type="hidden" value="${applicationStartLabel}"/>
  <input id="enableEnrollment" name="enableEnrollment" type="hidden" value="${enableEnrollment}"/>
  <input id="enrollCaseNumber" name="enrollCaseNumber" type="hidden" value="${enrollCaseNumber}"/>
  <input id="isInsideOe" name="isInsideOe" type="hidden" value="${isInsideOe}"/>
  <input id="isOeApproaching" name="isOeApproaching" type="hidden" value="${isOeApproaching}"/>
  <input id="daysTostartOe" name="daysTostartOe" type="hidden" value="${daysTostartOe}"/>
  <input id="daysEndOe" name="daysEndOe" type="hidden" value="${daysEndOe}"/>
  <input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
  <input id="sCode" name="sCode" type="hidden" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>"/>
  <input id="enableEnrollOnOE" name="enableEnrollOnOE" type="hidden" value="${enableEnrollOnOE}"/>	
  <input id="isRecTribe" name="isRecTribe" type="hidden" value="${isRecTribe}"/>
  <input id="showEnrollButton" name="showEnrollButton" type="hidden" value="${showEnrollButton}"/>
  <input id="isPlanEffective" name="isPlanEffective" type="hidden" value="${isPlanEffective}"/>
  <input id="coverageStartDate" name="coverageStartDate" type="hidden" value="${coverageStartDate}"/>
  <input id="coverageEndDate" name="coverageEndDate" type="hidden" value="${coverageEndDate}"/>
  <input id="applicationStatus" name="applicationStatus" type="hidden" value="${applicationStatus}"/>
  <input id="qepFlag" name="qepFlag" type="hidden" value="${qepFlag}"/>
  <input id="allowChangePlan" name="allowChangePlan" type="hidden" value="${allowChangePlan}"/>
  <input id="changePlanInSEP" name="changePlanInSEP" type="hidden" value="${changePlanInSEP}"/>
  <input id="coverageYear" name="coverageYear" type="hidden" value="${coverageYear}"/>
  <input id="showChangePlanInOE" name="showChangePlanInOE" type="hidden" value="${showChangePlanInOE}"/>
  <input id="renewed" name="renewed" type="hidden" value="${markedOtr}"/>
  <input id="applicationValidationStatus" name="applicationValidationStatus" type="hidden" value="${applicationValidationStatus}"/>
  <input id="serverDate" name="serverDate" type="hidden" value="${serverDate}"/>
  <input id="ssapFlowVersion" name="ssapFlowVersion" type="hidden" value="${ssapFlowVersion}" />

  </div>

<script>
 $(document).ready(function(){

    // Data layer variable to be pushed at the end of shell.jsp
    dl_pageCategory = 'Dashboard';

    $('body').tooltip({
        selector: '[rel=tooltip]',
        placement: 'right',
    });
    
    $.ajax({
    	  method: "GET",
    	  url: "indportal/announcements",
    	  cache: false
    	}).done(function( msg ) { 
    	    if(msg.length>0){
				var announcement = "";
				var announcementObject = new Object();
				for (var i = 0; i < msg.length; i++) {
					announcementObject = msg[i];
					announcement += announcementObject.announcementText;
				}
				$( "div.announcements" ).html(announcement);
				$( "div.announcements" ).css("display", "block");
			}
    	  });
  });
 </script>
<script src="<gi:cdnurl value="/resources/angular/angular-file-upload-shim.min.js"/>"></script>
<script src="<gi:cdnurl value="/resources/angular/angular-file-upload.min.js"/>"></script>
<script src="<gi:cdnurl value="/resources/angular/mask.js"/>"></script>
<script src="<gi:cdnurl value="/resources/angular/ui-bootstrap-tpls-0.9.0.min.js"/>"></script>
<script src="<gi:cdnurl value="/resources/js/indportal/individual_portal.js"/>?v=3.0" type="text/javascript"></script>
<script src="<gi:cdnurl value="/resources/js/angular-sanitize.min.js"/>"></script>
<script type="text/javascript" src="/hix/resources/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/hix/resources/js/spring-security-csrf-token-interceptor.js"></script>


<script src="<c:url value="/resources/js/moment.min.js" />"></script> 
<script src='<c:url value="/resources/js/preferences/communicationPreferences.controller.js" />'></script>
<script src='<c:url value="/resources/js/preferences/communicationPreferences.directive.js" />'></script>
<script src='<c:url value="/resources/js/preferences/communicationPreferences.service.js" />'></script>
<script src='<c:url value="/resources/js/addressValidation.module.js" />'></script>


