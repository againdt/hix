//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.30 at 02:39:14 PM IST 
//


package com.getinsured.ahbx.broker.individualbookofbusinessdetails;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcceptanceStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AcceptanceStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="D"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AcceptanceStatusType", namespace = "http://ahbx.accenture.com/AHBXValidation")
@XmlEnum
public enum AcceptanceStatusType {


    /**
     * It refers to status as "Accepted"
     * 					
     * 
     */
    C,

    /**
     * It refers to status as "Declined"
     * 					
     * 
     */
    D;

    public String value() {
        return name();
    }

    public static AcceptanceStatusType fromValue(String v) {
        return valueOf(v);
    }

}
