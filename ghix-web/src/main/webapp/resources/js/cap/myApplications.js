var indPortalApp = angular.module('indPortalApp', ['communicationPreferencesCtrlApp', 'communicationPreferencesDirectiveApp', 'addressValidationModule','ngSanitize', 'ngRoute', 'ui.mask', 'ngResource', 'angularFileUpload', 'ui.bootstrap', 'ng-bootstrap-datepicker','ui.mask','ngCookies','spring-security-csrf-token-interceptor']);
var hhdetails ="";
var mmdetails ="";
var addDetails ="";
var oComments="";
var healthplan="";
var dentalplan="";
var tempCoverageYear = null;
var planSummary="";

indPortalApp.run(function($rootScope, $window){
	$rootScope.goDash= function (){
		$window.location.href = "/hix/indportal";
		};
		
	$rootScope.openLink = function(address){
		$window.open(address);
	};
});

indPortalApp.value('isEditMode', true);


indPortalApp.controller('indPortalAppCtrl', ['$scope', '$http', '$location', '$window', '$sce', '$timeout', 'validateDate','$cookies','$cookieStore', '$q',function($scope, $http, $location, $window, $sce, $timeout, validateDate,$cookies, $cookieStore, $q) {

	$scope.showAgentPromo = function(){
		var dismissAgentPromo = $cookieStore.get('dismissAgentPromo');
		if(!dismissAgentPromo){
			$('#agentPromotion').modal('show');
		}
	};

	$scope.nextSteps = false;
	$scope.enableOE = $('#enableEnrollOnOE').val();
	$scope.coveragePeriod ={
	    start: "starting",
	    end: "ending"
	};


	//dummy data for sepDenial.jsp
	$scope.eventDetails = [
//	    {applicant:'primary', eventType:'income', eventName:"1", eventDate:"03/03/2015"},
	    {applicant:'primary', eventType:'address', eventName:"2", eventDate:"11/03/2015"},
	    {applicant:'primary', eventType:'other', eventName:"3: a very event with a very long name", eventDate:"03/03/2015"},
//	    {applicant:'2nd', eventType:'income', eventName:"4", eventDate:"03/03/2015"},
//	    {applicant:'3rd', eventType:'remove', eventName:"5", eventDate:"03/03/2015"},
//	    {applicant:'4th', eventType:'income', eventName:"6", eventDate:"03/03/2015"}
	];

	//Duplicate functions
	$scope.reload = function(){
		$window.location.reload(true);
	};

	$scope.refresh = function(){
		$window.location.reload(true);
	};
	//

	$scope.disenrollsepapp = function(sepEventsNumber){
		$scope.loader = true;
		var url = '/hix/indportal/disenrollforsep';
		var data = {'caseNumber': sepEventsNumber};
		var params = {
				"coverageYear" : $('#coverageYear').val()
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		var response = $http.post(url, data, config);

		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			if(data === "success"){
				$scope.disenrollresult = "success";
			}
			else{
				$scope.disenrollresult = "failure";
			}
		});
		response.error(function() {
			$scope.disenrollresult = "failure";
			$scope.loader = false;
		});
	};

	$scope.csractions = function(index) {
		if($('#csr_wrapper_'+index).hasClass('margin40-b')){
		$('#csr_wrapper_'+index).toggleClass('margin40-b margin0-b');
		$('#csrMenu_'+index).slideToggle();
		}
		else{
		$('#csrMenu_'+index).slideToggle(function(){
				$('#csr_wrapper_'+index).toggleClass('margin40-b margin0-b');
		});
		}	
	};
	
	$scope.csrPastActions = function(index) {
		if($('#csr_pwrapper_'+index).hasClass('margin40-b')){
		$('#csr_pwrapper_'+index).toggleClass('margin40-b margin0-b');
		$('#csrPMenu_'+index).slideToggle();
		}
		else{
		$('#csrPMenu_'+index).slideToggle(function(){
				$('#csr_pwrapper_'+index).toggleClass('margin40-b margin0-b');
		});
		}	
	};


	$scope.modalForm = {};
	$scope.cancelApplicationAlert = function(params) {
		$scope.caseNumber = params;
		$scope.modalForm.openDialog = true;
	};

//HIX-87479 - HIX-86989 As an exchange system, allow consumers to change their termination date for an enrollment that has been terminated with a future date

$scope.cendDateOptions = {
	currentMode : false,
	openSuccessCTDModal: false,
	openFailCTDModal: false
  };
function convertDate(date) {
	var parts = date.toString().split(" ");
	var months = {
	  Jan: "01",
	  Feb: "02",
	  Mar: "03",
	  Apr: "04",
	  May: "05",
	  Jun: "06",
	  Jul: "07",
	  Aug: "08",
	  Sep: "09",
	  Oct: "10",
	  Nov: "11",
	  Dec: "12"
	};
	return months[parts[1]]+"/"+parts[2]+"/"+parts[3];
};

$scope.terminationDates = [];

$scope.editTerminationDate = function(myEnrollment)	{
	$scope.terminationDates = [];
	$scope.cendDateOptions.currentMode = true;
	$scope.currentCoverageEndDate = myEnrollment.coverageEndDate;
	$scope.myEnrollmentEdit = myEnrollment;
	var startDate = new Date() > new Date(myEnrollment.coverageStartDate)? new Date() : new Date(myEnrollment.coverageStartDate);
	var endDate = new Date(myEnrollment.coverageEndDate);

	var startMonth = startDate.getMonth();
	var endMonth = endDate.getMonth();

	var year = endDate.getFullYear();

	//var pickMonths = endMonth - startMonth;
	for (var i=startMonth; i < endMonth; i++){
		var lastDay = new Date(year, i + 1, 0);
		var formatedDated = convertDate(lastDay);
		$scope.terminationDates.push(formatedDated);
	}
};

$scope.updateTerminationDate = function(){
	$scope.loader = true;
	$scope.cendDateOptions.currentMode = false;

	var url = '/hix/indportal/enrollments/terminationDate';
	var data = {
		'healthEnrollmentId' : $scope.myEnrollmentEdit.enrollmentId,
		'caseNumber': $scope.myEnrollmentEdit.caseNumber,
		'reasonCode': 'OTHER',
		'coverageStartDate':  $scope.myEnrollmentEdit.coverageStartDate,
		'terminationDate': $scope.newCoverageEndDate
	};

	var promise = $http.post(url, data);

	promise.success(function(response) {
		$scope.loader = false;
		$scope.newCoverageEndDate = '';
		if(response === 'success'){
			$scope.cendDateOptions.openSuccessCTDModal = true;
			$scope.getEnrollmentsHistory();
		}else{
			$scope.cendDateOptions.openFailCTDModal = true;
		}
	});

	promise.error(function() {
		$scope.loader = false;
		$scope.cendDateOptions.openFailCTDModal = true;
		$scope.newCoverageEndDate = '';
	});

};

	//HIX-69292 - Impacts to change reporting for auto renewal
	$scope.checkForAutoRenewal = function(isNonFinancial,applicationStatus,caseNumber,event,typeOfCheck,covYear) {

		$scope.loader = true;
		var url = '/hix/indportal/activeApplicationCheck';


		var params = {
				"coverageYear": covYear
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		var response = $http.post(url, null, config);

		response.success(function(data, status, headers, config) {

			$scope.loader = false;

			if(data == 'true'){
				$scope.openAutoRenewalLceDialog = true;
				if(typeOfCheck == 'reportAChange'){
					$('#autoRenewalButton').click(function(){
						$scope.reportaChange(isNonFinancial,applicationStatus,caseNumber,covYear);
					});
				}
				if(typeOfCheck == 'enroll'){
					$('#autoRenewalButton').click(function(){
						$scope.preEnroll(event,covYear);
					});
				}
				if(typeOfCheck == 'changePlan'){
					$('#autoRenewalButton').click(function(){
						$scope.changePlanInSEP(event);
					});
				}
				if(typeOfCheck == 'changePlanTribe'){
					$('#autoRenewalButton').click(function(){
						$scope.changePlan(event);
					});
				}
			}
			else{
				if(typeOfCheck == 'reportAChange'){
					$scope.reportaChange(isNonFinancial,applicationStatus,caseNumber,covYear);
				}
				if(typeOfCheck == 'enroll'){
					$scope.preEnroll(event,covYear);
				}
				if(typeOfCheck == 'changePlan'){
					$scope.changePlanInSEP(event);
				}
				if(typeOfCheck == 'changePlanTribe'){
					$scope.changePlan(event);
				}
			}

		});
		response.error(function() {
			$scope.loader = false;
		});

	};

	//Function leads to report a change page (under different angular app)
	$scope.reportaChange = function(isNonFinancial,applicationStatus,caseNumber,coverageYear) {

		if(isNonFinancial == true && applicationStatus == 'Enrolled (Or Active)'){

			$scope.loader = true;
			var url = '/hix/indportal/sepcheck';

			var params = {
					'caseNumber': caseNumber
			};
			var config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'},
				params:params
			};

			var response = $http.post(url, null, config);

			response.success(function(data, status, headers, config) {

				if(data == 'true'){
					location.href='./iex/lce/reportyourchange?coverageYear='+coverageYear;
				}
				else{
					$location.path('/qephome');
				}

			});
			response.error(function() {
				$scope.loader = false;
			});


		}else{
			$scope.openDialoglce = true;
		}
	};

	$scope.isRecTribe = $('#isRecTribe').val();

	$scope.disenrollAlert = function(caseNumber, typeOfPlan, covStartDate) {
		$scope.disenrollcasenumber= caseNumber;
		$scope.typeOfPlan = typeOfPlan;

		/*
		 * HIX-59752 Prevent Disenroll Action on an enrolled application
		 * if there is any another application in Eligibility Received status
		 */
		if(typeOfPlan == "Health" || typeOfPlan == "both"){
		$scope.loader = true;

		var url = '/hix/indportal/checkForPendingApplication';

		var params = {
					"coverageYear":covStartDate.slice(-4)
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		var response = $http.post(url, null, config);

		response.then(function(responseData, status, headers, config){
			if(responseData.data=="true"){
				$scope.loader = false;
				$scope.pendingAppDialog = true;
			}else{
				$scope.loader = false;
				$scope.disenrollDialog = true;
			}
		}, function(responseData, status, headers, config){
			$scope.loader = false;
			$scope.disenrollDialog = true;
		});
		}
		else{
			$scope.loader = false;
			$scope.disenrollDialog = true;
		}
	};

	$scope.continueDisenrollment = function (){
		$scope.comingStartDayDialog = false;
		$scope.disenrollDialog = true;
	};

	$scope.disenrollmentReason = function() {
		$scope.disenrollDialog = false;
		//if ($scope.typeOfPlan === "Health" || $scope.typeOfPlan == "both"){
			$scope.areYourSureDialog = true;
		//} else {
			//$scope.goToDisenrollDateModal();
		//}
	};

	$scope.goToDisenrollDateModal = function(){

		var isPlanEffectuated = "N";
		var isInsideOe = $("#isInsideOe").val();
		var typeOfPlan = $scope.typeOfPlan;

		if (typeOfPlan === "Health" || $scope.typeOfPlan == "both"){
			isPlanEffectuated =	$scope.healthdetails.isPlanEffectuated;
		} else if (typeOfPlan === "Dental"){
			isPlanEffectuated =	$scope.dentaldetails.isPlanEffectuated;
		};

		if(isPlanEffectuated == 'Y' && isInsideOe == 'false'){

			$scope.areYourSureDialog = false;
			$scope.disenrollDateDialog = {step: 1};
			$scope.disenrollmentDates = [];

			var actualDates = [];

			if (typeOfPlan === "Health" || $scope.typeOfPlan == "both"){
				actualDates = [$scope.healthdetails.endOfCurrentMonth, $scope.healthdetails.endOfNextMonth, $scope.healthdetails.endOfMonthAfterNext];
			}
			else if (typeOfPlan === "Dental"){
				actualDates = [$scope.dentaldetails.endOfCurrentMonth, $scope.dentaldetails.endOfNextMonth, $scope.dentaldetails.endOfMonthAfterNext];
			}

			var dateValue = ["CURRENT_MONTH", "NEXT_MONTH", "MONTH_AFTER_NEXT_MONTH"];
			var dateDisplay = ["Last day of the current month", "Last day of next month", "Last day of the month after next"];
			for(var i = 0; i < 3; i++){
				if(actualDates[i]){
					var dateObj = {
							actual: actualDates[i],
							value: dateValue[i],
							display: dateDisplay[i]
					};
					$scope.disenrollmentDates.push(dateObj);
				}
			}
		}
		else{
			$scope.submitDisenrollment();
		}
	};

	$scope.cancelDisenroll = function(){
		$scope.checked = null;
		$scope.otherReason = "";
	};

	$scope.submitDisenrollment = function() {
		$scope.areYourSureDialog = false;
		$scope.loader = true;
		var url = null;
		var data = null;

		if (!$scope.checked) {
			$scope.checked = "NO_OPTION_SELECTED";
		};

		//need to update reasonCode
		if ($scope.typeOfPlan === "both"){
			url = '/hix/indportal/terminatePlan';
			data = {'applicationId':$scope.healthdetails.ssapApplicationId,'caseNumber': $scope.disenrollcasenumber, 'reasonCode':$scope.checked, 'otherReason':$scope.otherReason,'coverageStartDate':$scope.healthdetails.coverageStartDate,'isRecTribe':$scope.isRecTribe};
			if($scope.dentaldetails && $scope.dentaldetails.disEnroll){
				data['dentalEnrollmentId'] = $scope.dentaldetails.enrollmentId;
			}
		}
		else if ($scope.typeOfPlan === "Health"){
			url = '/hix/indportal/terminateHealthPlan';
			data = {'healthEnrollmentId':$scope.healthdetails.enrollmentId,'caseNumber': $scope.disenrollcasenumber, 'reasonCode':$scope.checked, 'otherReason':$scope.otherReason, 'coverageStartDate':$scope.healthdetails.coverageStartDate,'isRecTribe':$scope.isRecTribe};
			if($scope.dentaldetails && $scope.dentaldetails.disEnroll){
				data['dentalActive'] = true;
				data['dentalAptc'] = $scope.dentaldetails.electedAptc;
				data['dentalEnrollmentId'] = $scope.dentaldetails.enrollmentId;
			}
		} else if ($scope.typeOfPlan === "Dental"){
			url = '/hix/indportal/terminateDentalPlan';
			data = {'dentalEnrollmentId': $scope.disenrollcasenumber, 'caseNumber': $scope.dentaldetails.caseNumber,'reasonCode':$scope.checked, 'otherReason':$scope.otherReason, 'coverageStartDate':$scope.dentaldetails.coverageStartDate};
			if($scope.healthdetails && $scope.healthdetails.disEnroll){
				data['healthActive'] = true;
			}
		};

		if($scope.disenrollDateDialog.date){
			data['terminationDateChoice'] = $scope.disenrollDateDialog.date.value;
		}

		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'}
		};

		var response = $http.post(url, data, config);
		response.then(function(responseData, status, headers, config){
			if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
				$scope.loader = false;
				return;
			}

			if(responseData.data=="success"){
				$scope.loader = false;
				$scope.submitDisenrollmentDialog = true;
			}else{
				$scope.loader = false;
				$scope.submitDisenrollmentFailureDialog = true;
			}
		}, function(responseData, status, headers, config){
			$scope.loader = false;
			$scope.submitDisenrollmentFailureDialog = true;
		});
	};

	$scope.refreshEnrollmentHistory = function(){
		$scope.modalForm.overrideEnrollmentSuccessful = false;
		$location.path('/enrollmenthistory');
		$window.location.reload(true);
	};

	$scope.goTo = function(dest){
		$location.path(dest);
	};

	$scope.goHome = function(){
		$scope.submitDisenrollmentDialog= false;
		$location.path('/');
		$window.location.reload(true);
	};



	$scope.modelattrs =
	                 {'aptc': $('#aptc').val(),
	                  'csr': $('#csr').val(),
	                  'sCode':$('#sCode').val(),
	                  'caseNumber':$('#enrollCaseNumber').val(),
	                  'enCoverageEndDate':$('#coverageEndDate').val()};

	$scope.coverageStartDate = $('#coverageStartDate').val();

	$scope.showEligibilityDetailsFromHome= function(event){
		$scope.showEligibilityResults(event.target.id);
	};

	$scope.preEnroll= function(event,covYear){
		$scope.loader = true;
		var postData = 'caseNumber=' + event.target.id;
		var params = {
				"coverageYear": covYear
		};
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params
		};
		var response = $http.post('indportal/getApplicantsForAddInfo', postData, config);
		response.success(function(data, status, headers, config) {
			if (data === "" || data === null){
				$scope.loader = false;
				$scope.openInitPreEnroll = true;
				return;
			}
			else if (data != null && data.errCode != "" && data.errCode != null){
				$scope.loader = false;
				$scope.openPreEnrollESD = true;
				return;
			}
			coverageStartDateAddInfo = data.coverageStartDate;
			coverageYearOver = data.coverageYearOver;
			addDetails = data.applicantDetails["eligiblitydetails"];
			addInElgDetails = data.applicantDetails["inEligibilitydetails"];
			addFlag = data["householdExempt"];
			isSepFlag = data["isSep"];
			isChangePlanFlowFlag = false;
			isSEPPlanFlow = false;
			tempCoverageYear = covYear;
			$location.path('/additionalinfo');
		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitPreEnroll = true;
		});
	};

	//Change plan option
	$scope.changePlan = function(event){
		$scope.loader = true;

		var allowChangePlan = $("#allowChangePlan").val();

		if(allowChangePlan == 'Y'){
		var postData = 'caseNumber=' + event.target.id + '&isChangePlanFlowFlag=true';
		var params = {
				"coverageYear": $('#coverageYear').val()
		};
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params
		};
		var response = $http.post('/hix/indportal/getApplicantsForAddInfo', postData, config);
		response.success(function(data, status, headers, config) {
			if (data === "" || data === null){
				$scope.loader = false;
				$scope.openInitPreEnroll = true;
				return;
			}
			coverageStartDateAddInfo = data.coverageStartDate;
			coverageYearOver = data.coverageYearOver;
			addDetails = data.applicantDetails["eligiblitydetails"];
			addInElgDetails = data.applicantDetails["inEligibilitydetails"];
			addFlag = data["householdExempt"];
			isSepFlag = data["isSep"];
			isChangePlanFlowFlag = true;
			isSEPPlanFlow = false;
			$location.path('/additionalinfo');
		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitPreEnroll = true;
		});
		}
		else{
			$scope.loader = false;
			$scope.disAlloweChangePlan = true;
		}
	};

	//Change plan in SEP
	$scope.changePlanInSEP = function(event){
		$scope.loader = true;

		var postData = 'caseNumber=' + event.target.id + '&changeSEPPlanFlow=true';
		var params = {
				"coverageYear": $('#coverageYear').val()
		};
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params
		};
		var response = $http.post('/hix/indportal/getApplicantsForAddInfo', postData, config);
		response.success(function(data, status, headers, config) {
			if (data === "" || data === null){
				$scope.loader = false;
				$scope.openInitPreEnroll = true;
				return;
			}
			coverageStartDateAddInfo = data.coverageStartDate;
			coverageYearOver = data.coverageYearOver;
			addDetails = data.applicantDetails["eligiblitydetails"];
			addInElgDetails = data.applicantDetails["inEligibilitydetails"];
			addFlag = data["householdExempt"];
			isSepFlag = data["isSep"];
			isChangePlanFlowFlag = false;
			isSEPPlanFlow = true;
			$location.path('/additionalinfo');
		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitPreEnroll = true;
		});

	};

	$scope.showApplicantDetails = function(){
		if (addDetails){
			$scope.additionalDetails = addDetails;
			$scope.additionalInElgDetails = addInElgDetails;
			$scope.hardshipExemption = addFlag;
			$scope.isSep = isSepFlag;
			$scope.isChangePlanFlow = isChangePlanFlowFlag;
			$scope.isSEPPlanFlow = isSEPPlanFlow;
			$scope.coverageStartDateAddInfo = coverageStartDateAddInfo;
			$scope.coverageYearOver = coverageYearOver;
		} else {
			$location.path('/');
		}

	};

	$scope.enrollAfterTobacoo = function (){
		var eliToSend = [];

		if ($scope.hardshipExemption){
			angular.forEach($scope.additionalDetails, function(val, key){
				if (!val.exemptionNumber){
					$scope.fillExemptNum = true;
					return;
				}
			});
			 if ($scope.fillExemptNum){
				 return;
			 };
			eliToSend = $scope.additionalDetails;

		} else {
			for (var i = 0; i < $scope.additionalDetails.length; i++){
				eliToSend[i] = {};
				for (var prop in $scope.additionalDetails[i]){
					if (prop !== "exemptionNumber"){
						eliToSend[i][prop] = $scope.additionalDetails[i][prop];
					}
				}
			};
		};
		$scope.loader = true;
		$scope.addDetailsToSend = {
			applicantDetails : {
				eligiblitydetails: eliToSend,
				inEligibilitydetails: $scope.additionalInElgDetails
			},
			householdExempt: $scope.hardshipExemption,
			isChangePlanFlow : $scope.isChangePlanFlow,
			isSEPPlanFlow : $scope.isSEPPlanFlow
		};

		var postdata = angular.toJson($scope.addDetailsToSend);
		var params = {
				"coverageYear": tempCoverageYear==null?$('#coverageYear').val() : tempCoverageYear
		};
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'},
			params:params
		};

		$http.post('indportal/saveAdditionalInfo', postdata, config)
			.success(function(data, status, headers, config){
				if(typeof(data)!=="string" && data !== "" && data.status != null){
					$scope.loader = false;
					$scope.modalForm.techIssue = 'true';
				}else if(data.startsWith('failure')){
					$scope.loader = false;
					$scope.modalForm.techIssue = 'true';
				}else{
					location.href='./private/setHousehold/'+data;
				}
			}).error(function(data, status, headers, config){
				$scope.loader = false;
				$scope.modalForm.techIssue = 'true';
			});
	};

	$scope.enroll= function(event){
		var postData = 'caseNumber=' + event.target.id;
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		};
		var response = $http.post('/hix/indportal/enroll', postData, config);
		response.success(function(data, status, headers, config) {
			if(typeof(data)!=="string" && data !== "" && data.status != null){
				$scope.loader = false;
				$scope.openInitFail = true;
			}else{
				location.href='./private/setHousehold/'+data;
			}			
		});
		response.error(function() {
			$scope.openInitFail = true;
		});
	};
	$scope.initPortal = function(){
		var response = $http.get('indportal/init');
		response.success(function(data, status, headers, config) {
			if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
				return;
			}
			$scope.portalHome = data;
		});
	};
	//need error block

	$scope.init = function(){
		$scope.loader = true;
		
		$scope.yesno = [{name:'Yes',id:'Yes'},
						{name:'No',id:'No'}];
		
		$scope.csrvaluesIfNativeAmerican = [{name:'CS Level 2',id:'CS2'},
											{name:'CS Level 3',id:'CS3'},
											{name:'CS Level 4',id:'CS4'},
											{name:'CS Level 5',id:'CS5'},
											{name:'CS Level 6',id:'CS6'},
											{name:'No',id:'No'}
										  ];
		
		$scope.csrvaluesIfNotNativeAmerican =[{name:'CS Level 4',id:'CS4'},
											  {name:'CS Level 5',id:'CS5'},
											  {name:'CS Level 6',id:'CS6'},
											  {name:'No',id:'No'}];
		
		//###############multi-year starts############################################
		var maCurrentCoverageYear = parseInt($('#maCurrentCoverageYear').val());
		var exchangeStartYear = parseInt($('#gExchangeStartYear').val());
		var minStartYear = Math.min(maCurrentCoverageYear, exchangeStartYear);
		$scope.maCurrentCoverageYearArray = [];
		$scope.userPermissionsList = [];

		for(var i = maCurrentCoverageYear; i >= minStartYear; i--  ){
			$scope.maCurrentCoverageYearArray.push(i);
		}

		if($scope.maCurrentCoverageYear === undefined){
			$scope.maCurrentCoverageYear = maCurrentCoverageYear;
		}

		$scope.isUserAllowedForActions = function(userPermissionsList, application) {
			var isUserAllowed = false;
			angular.forEach(userPermissionsList, function(permission) {
				if (permission === 'IND_PORTAL_EDIT_SPCL_ENRLMNT_PERIOD' ||
					(permission === 'MYAPP_CHANGE_COVRAGE_STARTDATE' && application.applicationStatus != 'Closed') ||
					permission === 'MYAPP_VIEW_OVERRIDE_HIST' ||
					(permission === 'MYAPP_EDIT_NONFIN_APP' && (application.applicationStatus === 'Eligibility Received' || application.applicationStatus === 'Partially Enrolled' || application.applicationStatus === 'Enrolled (Or Active)' || application.applicationStatus === 'Signed')) ||
					(permission === 'MYAPP_INIT_NONFIN_VERIFICATION' && application.isNonFinancial) ||
					(permission === 'MYAPP_RTN_NONFIN_ELIGIBILITY' && application.isNonFinancial) ||
					(permission === 'MYAPP_CANCEL_TERMPLAN' && (application.applicationStatus === 'Partially Enrolled' || application.applicationStatus === 'Enrolled (Or Active)')) ||
					permission === 'MYAPP_SEND_DEMOGRPHCUPDT_CARRIER' ||
					(permission === 'MYAPP_OVERRD_SPCL_ENROLLMENT' && application.applicationStatus === 'Eligibility Received' && (application.isQep || application.isSepApp) && !(application.eligibilityStatus === 'DE' && application.isNonFinancial)) ||
					(permission === 'MYAPP_OVERRD_APTCELGBLT_STARTDT' && application.isSepApp && application.applicationStatus === 'Eligibility Received' && !application.isNonFinancial)) {
					isUserAllowed = true;
				}
			});

			return isUserAllowed;
		};

		//###############multi-year ends############################################
		//encHouseholdId = document.getElementById('encHouseholdId').value;
		var response = $http.get('/hix/crm/getApplications?coverageYear=' + $scope.maCurrentCoverageYear + '&tmstp='+new Date().getTime() + '&encHouseholdId='+encHouseholdId);
		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
				return;
			}
			$scope.currentApplications = data["currentApplications"];
			$scope.pastApplications = data["pastApplications"];
			$scope.userPermissionsList = data["userPermissionList"];


			
			$scope.queuedApplications = data["queuedApplications"];
			console.log("queuedApplications");
			console.log(data["queuedApplications"]);

			$scope.myAppsInsideOE = data.insideOE;
			$scope.myAppsInsideQEP = data.insideQEP;
			
			$scope.getOutBoundButtonStatus();
		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitFail = true;
		});

		$timeout(function() {
			$scope.bindDatepicker();
		},0);
};

// $scope.$on('$viewContentLoaded', function(event) {
//     $timeout(function() {
//     	$scope.bindDatepicker();
//     },0);
// });

$scope.getOutBoundButtonStatus = function(){
	if(typeof($scope.currentApplications[0]) != "undefined") {
		var caseNumberValue = $scope.currentApplications[0].caseNumber;
		var response = $http.get('/hix/cap/getOutBoundButtonStatus?caseNumber=' + caseNumberValue);
		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
				return;
			}
			$scope.outBoundButtonStatus = data.result;
		});
	} else {
		//there are no applications for selected year in dropdown
	}
}

$scope.bindDatepicker = function(){

	/*Bind Datepicker to element
	 TODO:  currently we are using jquery for binding, we have to replace it in angular */
	$('.newSEPEndDate').each(function() {
		$(this).datepicker({
			autoclose : true,
			dateFormat : 'MM/DD/YYYY'
		}).on('changeDate', function(){
			$(this).datepicker('hide');
			$('input[name=newSEPEndDate]').trigger('blur');
		});
	}).on('hide', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});

	$(".datePicker").each(function(){
		$(this).datepicker({
			autoclose: true
		});
	}).on('hide', function(event) {
		event.preventDefault();
		event.stopPropagation();
	});

	/*Setting z-index for date-picker when open in modal dialog */
	$('.js-dateTrigger').on('click', function(){
		setTimeout(function(){
			$('.datepicker').css({zIndex: 1070});
		},80);
	});
};

$scope.showVerificationResults = function(caseNumber){
	window.location= 'indportal/viewVerificationResults?caseNumber='+caseNumber;
};

$scope.showEligibilityDetails = function(){
	$scope.householdEligibilityDetails = hhdetails;
	$scope.memberEligibilityDetails = mmdetails;
};

$scope.showEligibilityResults = function(caseNumber){
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('/hix/indportal/geteligibilitydetails', postData, config);
	response.success(function(data, status, headers, config){
		/*if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
			return;
		}*/
		hhdetails = data["householdEligibilityDetails"];
		mmdetails = data["membersEligibilityDetails"];
		if(hhdetails=="" && mmdetails==""){
			$scope.eligResultDialog = true;
		}else{
			window.location = './indportal#/eligibilityresults?caseNumber=' + caseNumber;
		}
	});
	//need error block

};


$scope.showAppealsDetails = function(){
	$scope.appealsDetails = appeals;
};

$scope.showAppeals = function(){
	var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8'}
	};
	var response = $http.post('indportal/getAppeals', config);
	response.success(function(data, status, headers, config){
		if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
			return;
		}
		if(data === null){
			$scope.retrievalFail = true;
		} else {
			$scope.appealsDetails = data["appeals"];
		}
	});
	response.error(function(){
		$scope.retrievalFail = true;
	});

};

$scope.retrieveReferrals = function(){
	var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8'},
	};
	var response = $http.post('indportal/pendingReferrals', config);
	response.success(function(data, status, headers, config){
		if(typeof(data)==="string"){
			location.href='./account/user/login';
			return;
		}
		if(data === null){
			$scope.retrievalFail = true;
		} else {
			$scope.pendingReferrals = data['referrals'];
		}
	});
	response.error(function(){
		$scope.retrievalFail = true;
	});

};

$scope.showPlanSummary = function(caseNumber){
	$scope.loader = true;
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('/hix/indportal/getplandetails', postData, config);
	response.success(function(data, status, headers, config){
		/*if(typeof(data)==="string"){
			return;
		}*/
		$scope.loader = false;
		/*healthplan = data["health"];
		dentalplan = data["dental"];*/
		planSummary = data;
		$location.path('/plansummary');
	});
	//need error block

};

//Called after APTC is updated
$scope.refreshPlanSummary = function(caseNumber){
	$scope.loader = true;
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('/hix/indportal/getplandetails', postData, config);
	response.success(function(data, status, headers, config){
		/*if(typeof(data)==="string"){
			return;
		}*/
		$scope.loader = false;
		planSummary = data;
		$location.path('/plansummary');
		if(planSummary!=undefined && planSummary!=""){
			$scope.planDetails = planSummary;
		}
		/*healthplan = data["health"];
		dentalplan = data["dental"];
		$location.path('/plansummary');
		if(healthplan!=undefined && healthplan!=""){
			$scope.healthdetails = healthplan;
		}
		if(dentalplan!=undefined && dentalplan!=""){
			$scope.dentaldetails = dentalplan;
			if ($scope.dentaldetails.officeVisit === "$null"){
				$scope.dentaldetails.officeVisit = "N/A";
			}
		}*/
	});
	//need error block

};

$scope.initplansummary = function(){
	if(planSummary == ""){
		$location.path('/indportal');
	}
	if(planSummary!=undefined && planSummary!=""){
		$scope.planDetails = planSummary;
		$scope.showhealthplan = true;
		$scope.showdentalplan=true;
	}
	/*if(healthplan=="" && dentalplan==""){
		$location.path('/indportal');
	}
	if(healthplan!=undefined && healthplan!=""){
		$scope.healthdetails = healthplan;
		$scope.showhealthplan = true;

		$scope.electedAptc = $scope.healthdetails.electedAptc;
		$scope.newAptc = $scope.healthdetails.electedAptc;

	}
	if(dentalplan!=undefined && dentalplan!=""){
		$scope.dentaldetails = dentalplan;
		if ($scope.dentaldetails.officeVisit === "$null"){
			$scope.dentaldetails.officeVisit = "N/A";
		}
		$scope.showdentalplan=true;
	}*/
};

$scope.showOverrideComments = function(){
	if(oComments==""){
		$location.path('/applications');
	}
	$scope.overrideComments = oComments;
};
$scope.getComments = function(caseNumber){
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('/hix/indportal/getOverrideComments', postData, config);
	response.success(function(data, status, headers, config){
		/*if(typeof(data)==="string"){
			return;
		}*/
		$scope.modalForm.oComments = data["comments"];
		$scope.modalForm.ORHist = true;
	});

	response.error(function(data, status, headers, config){
		$scope.modalForm.subResult = 'failure';
	});


};

$scope.showApplication = function(caseNumber){
	var ssapFlowVersion = $("#ssapFlowVersion").val();
	if(ssapFlowVersion && ssapFlowVersion === "2.0") {
		$window.location = '/hix/newssap/start?caseNumber=' + caseNumber;
	} else {
		$window.location= '/hix/cap/gotossap?caseNumber=' + caseNumber;
	}
};
$scope.startApplication = function(caseNumber){
	$scope.showDialog = false;
	$(".modal-backdrop").remove();
	$location.path('/qepeligible');
};

$scope.editApplication = function(caseNumber){
    var switchToModuleName = $('#switchToModuleName').val();
    var switchToModuleId = $('#switchToModuleId').val();
    var switchToResourceName = $('#switchToResourceName').val();
    var ssapUrl = '/indportal/editapplication?caseNumber='+caseNumber;
    $window.location = '/hix/account/user/switchUserRole?switchToModuleName='+switchToModuleName
        +'&switchToModuleId='+switchToModuleId
        +'&switchToResourceName='+switchToResourceName
        +'&switchToNonDefaultURL='+ ssapUrl;
};

$scope.enrollApplication= function(caseNumber){
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('/hix/indportal/enroll', postData, config);
	response.success(function(data, status, headers, config) {
		if(typeof(data)!=="string" && data !== "" && data.status != null){
			$scope.loader = false;
			$scope.openInitFail = true;
		}else{
			location.href='./private/setHousehold/'+data;
		}	

	});
	response.error(function() {
		$scope.openInitFail = true;
	});
};

$scope.cancelApplication= function(caseNumber){
	$scope.loader = true;
	var postData = 'caseNumber=' + caseNumber;
	var config = {
		headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	};
	var response = $http.post('/hix/indportal/cancel', postData, config);
	response.success(function(data, status, headers, config) {
		$scope.loader = false;
		$scope.modalForm.openDialog = false;
		if(data==="success"){
			$scope.refresh();
		}
		else{
			//$scope.currentApplications = data["currentApplications"];
			//$scope.pastApplications = data["pastApplications"];
			$scope.openInitFail = true;
		}
	});
	response.error(function() {
		$scope.loader = false;
		$scope.modalForm.openDialog = false;
		$scope.openInitFail = true;
	});
};

//Cancelling an enrolled application
$scope.cancelEnrolledApplication= function(caseNumber,covYear){
	$scope.loader = true;

	//1. Check for pending application
	var url = '/hix/indportal/checkForPendingApplication';

	var params = {
				"coverageYear":covYear
	};
	var config = {
		headers: { 'Content-Type': 'application/json; charset=UTF-8'},
		params:params
	};

	var response = $http.post(url, null, config);

	response.then(function(responseData, status, headers, config){
		if(responseData.data=="true"){
			$scope.loader = false;
			$scope.pendingAppDialog = true;
		}else{
			//2. Check for active enrollment
			url = '/hix/indportal/checkEnrollmentsForCancellation';

			params = {
						"caseNumber":caseNumber
			};
			config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'},
				params:params
			};

			response = $http.post(url, null, config);

			response.then(function(responseData, status, headers, config){
				if(responseData.data=="true"){
					$scope.loader = false;
					$scope.activeEnrollmentDialog = true;
				}else{
					$scope.loader = false;
					$scope.caseNumber = caseNumber;
					$scope.modalForm.openDialog = true;
				}
			});

		}
	}, function(responseData, status, headers, config){
		$scope.loader = false;
		$scope.caseNumber = caseNumber;
		$scope.modalForm.openDialog = true;
	});
};

//HIX-75520
$scope.showEventSummary = function(caseNumber){

	$scope.loader = true;

	var url = 'iex/lce/geteventnames/'+caseNumber;

	config = {
		headers: { 'Content-Type': 'application/json; charset=UTF-8'},
	};

	response = $http.get(url, null, config);

	$scope.applicantEvents = [];

	response.success(function(response, status, headers, config){
		if(response && response.length > 0){
			angular.forEach(response, function(val, key){
				$scope.applicantEvents.push(val);
			});
		}
		$scope.loader = false;
		$scope.eventSummaryDialog = true;
	});

	response.error(function (){
		$scope.loader = false;
		$scope.openInitFail = true;
	});

};

$scope.payForHealth= function(caseNumber,planType,enrollmentId){
	var Data ={ 'caseNumber':caseNumber,'typeOfPlan': planType,'enrollmentId': enrollmentId};
	var config = {
		headers: {   'Content-Type': 'application/json; charset=UTF-8'}
	};

	$scope.loader = true;
	var response = $http.post('/hix/indportal/payforhealth', Data, config);
	response.success(function(data, status, headers, config) {
		if(data != null){
			$scope.payForHealthUrl(data);
		} else {
			$scope.loader = false;
			$scope.payFHFail = true;
		}
	});
	response.error(function() {
		$scope.loader = false;
		$scope.payFHFail = true;
	});
};

$scope.payForHealthUrl =function(data){
	$('#finEnrlDataKey').attr('value', data);
	$scope.loader = false;
	$scope.paySuccess = true;
	/*var params = {
			"csrftoken": $('#tokid').val(),
			"finEnrlDataKey":data
	};
	var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params

	};*/
	/*var response = $http.post('finance/paymentRedirect?',data, config);
	response.success(function(data, status, headers, config) {
		if(data != null && data.STATUS == 'SUCCESS'){
			var actionUrl = data.ffmEndpointURL;
			$('#postform').attr('action',actionUrl);
			$('#SAMLResponse').attr('value', data.SAMLResponse);
			$scope.loader = false;
			$scope.paySuccess = true;
		} else {
			$scope.loader = false;
			$scope.payFHFail = true;
		}
	});
	response.error(function() {
		$scope.loader = false;
		$scope.payFHFail = true;
	});*/
};

$scope.payHealthExternal= function(){
	document.postform.submit();
	$scope.paySuccess = false;
	$scope.loader = false;
};


$scope.financialHelpPath = function(){
	window.open('https://idalink.idaho.gov/');
};

$scope.goToNewSSAP = function(applicationType) {
	var ssapFlowVersion = $("#ssapFlowVersion").val();
	var coverageYear = $("#coverageYear").val();
	if (ssapFlowVersion && ssapFlowVersion === "2.0") {
		$window.location = '/hix/newssap/start?applicationType=' + applicationType + '&coverageYear=' + coverageYear;
	} else {
		applicationType === "OE" ? $scope.goToSsapOE() : $scope.goToSsap();
	}
};

$scope.goToSsap = function(){
	//location.href='./ssap';
	$window.location= '/hix/indportal/startQepApp?coverageYear='+$('#coverageYear').val();
};

$scope.goToSsapOE = function(){
	//location.href='./ssap';
	$window.location= '/hix/indportal/startOEApp?coverageYear='+$('#coverageYear').val();
};

$scope.getIndividualPortalPage = function(coverageYear){
	$window.location= '/hix/indportal?coverageYear='+coverageYear;
};

$scope.goQephome = function(){
	$location.path('/qephome');
};

//
	$scope.parseDate = function (str){
		var ans = str;
		if (str && str.length === 8){
			ans = str.substr(0,2) + "/" + str.substr(2,2) + "/" + str.substr(4, 4);
		}
		return ans;
	};

	///////////////////////////////////////
	///CSR related functions & variables///
	///////////////////////////////////////
	$scope.getSubsq = function(event){
		$scope.loader = true;
		var url;
		if($scope.currentCsr.route.startsWith("cap/")){
			url = '/hix/'+ $scope.currentCsr.route;
		}
		else {	
			url = '/hix/indportal/' + $scope.currentCsr.route;
		}
		var data;
		var config;
		if ($scope.currentCsr.title === 'Cancel or Terminate Plan'){
			$scope.termDateErr = null;
			var termDate = $scope.modalForm.cancelDate.substring(4) + "-" + $scope.modalForm.cancelDate.substring(0, 2) + "-" + $scope.modalForm.cancelDate.substring(2, 4);
			data = {'caseNumber': $scope.csrModalCaseNum,'coverageStartDate':$scope.coverageStartDate, 'terminationDate': termDate, 'reasonCode':'OTHER'};
			config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'}
			};

		} else if ($scope.currentCsr.title === 'Special Enrollment' || $scope.currentCsr.title === 'Change Coverage Start Date'){
			$scope.SEPData.sepStartDate = $scope.parseDate($scope.SEPData.sepStartDate);
			$scope.SEPData.sepEndDate = $scope.parseDate($scope.SEPData.sepEndDate);
			$scope.SEPData.covStartDate = $scope.parseDate($scope.SEPData.covStartDate);
			if ($scope.currentCsr.title === 'Special Enrollment'){
				$scope.SEPData.editSep = true;
				$scope.SEPStartErr = null;
				$scope.SEPEndtErr = null;
			} else {
				$scope.SEPData.editSep = false;
				$scope.SEPCovErr = null;
			}

			data = $scope.SEPData;

			config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'}
			};

		} else if($scope.currentCsr.title === 'Reinstate Enrollment') {
			$scope.ReinstateenrollmentData.caseNumber = $scope.csrModalCaseNum;
			delete $scope.ReinstateenrollmentData.showReinstateEnrollmentOptions;
			data = $scope.ReinstateenrollmentData;
			config = {
					headers: { 'Content-Type': 'application/json; charset=UTF-8'}
				};
		} else if($scope.currentCsr.title === 'Override SEP Denial') {
			data = {"caseNumber":$scope.csrModalCaseNum,"enrollmentEndDate":$scope.parseDate($scope.modalForm.newSEPEndDate)};
			config = {
					headers: { 'Content-Type': 'application/json; charset=UTF-8'}
				};
		} else {
			data = 'caseNumber=' + $scope.csrModalCaseNum;
			config = {
				headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
			};
		}
		var response = $http.post(url, data, config);
		response.success(function(responseData, status, headers, config){
			if(typeof(responseData)==="string"){
				responseData = responseData.toLowerCase();
			}
			if(typeof(responseData)==="string" && (responseData !== "success" && responseData !== "failure")){
				$scope.loader = false;
				return;
			}
			if(responseData=="success"){
				$scope.loader = false;
				$scope.modalForm.subResult = 'success';
			}else{
				$scope.loader = false;
				$scope.modalForm.subResult = 'failure';
			}
		})
		.error(function(responseData, status, headers, config){
			$scope.loader = false;
			$scope.modalForm.subResult = 'failure';
		});
	};

	$scope.csrContainer = {
		edit:{title: 'Edit Application', route: '', func: $scope.editApplication, header: csrStorage.edit.header, content: csrStorage.edit.content},
		initiate:{title: 'Initiate Verifications', route: 'initVerifications', func: $scope.initVerifications, header: csrStorage.initiate.header, content: csrStorage.initiate.content},
		rerun:{title: 'Re-run Eligibility', route: 'rerunEligibility', func: $scope.rerunEligibility,header: csrStorage.rerun.header, content: csrStorage.rerun.content},
		cancelTerm:{title: 'Cancel or Terminate Plan', route: 'terminatePlan', func: $scope.terminatePlan, header: csrStorage.cancelTerm.header, content: csrStorage.cancelTerm.content},
		update:{title: 'Update Carrier', route: 'updateCarrier', func: $scope.updateCarrier, header: csrStorage.update.header, content: csrStorage.update.content},
		view:{title: 'View Override History', route: 'getOverideHistory', func: $scope.getOverideHistory, header: csrStorage.view.header, content: csrStorage.view.content},
		specialEnroll:{title: 'Special Enrollment', route: 'saveSepDetails', func: $scope.getOverideHistory, header: csrStorage.specialEnroll.header, content: csrStorage.specialEnroll.content},
		coverageDate:{title: 'Change Coverage Start Date', route: 'saveSepDetails', func: $scope.getOverideHistory, header: csrStorage.coverageDate.header, content: csrStorage.coverageDate.content},
		reinstate:{title: 'Reinstate Enrollment', route: 'reinstateenrollment', func: $scope.getOverideHistory, header: csrStorage.reinstate.header, content: csrStorage.reinstate.content},
		overrideSEPDenial:{title: 'Override SEP Denial', route: 'seps/reversedenial/', func: $scope.getOverideHistory, header: csrStorage.overrideSEPDenial.header, content: csrStorage.overrideSEPDenial.content},
		overrideEnrollment:{title:'Override Enrollment', route: '', func:'',header:"Override Enrollment Status", content:"Please specify the reason for overriding enrollment status for this customer. This will help keep track of updates to the customer's record."},
		sendObAt:{title: 'Send Outbound Account Transfer', route: 'cap/sendobat', func: $scope.getOverideHistory, header: 'Send Outbound Account Transfer', content: 'Please specify the reason for sending the outbound account transfer. This will help keep track of updates to the customer’s record.'},
		overrideProgElig:{title: 'Override Program Eligibility', route: '', func: $scope.getOverideHistory, header: 'Override Program Eligibility', content: 'Override Program Eligibility Content'}
		
	};

	$scope.openCSR = function(caseNum, type){
		$scope.modalForm.csr = true;

		$scope.modalForm.overrideComment = "";
		$scope.csrModalCaseNum = caseNum;
		$scope.currentCsr = $scope.csrContainer[type];
	};

	$scope.openOverrideEnrollStatus = function(caseNum, type, overrideEnrollStatus){
		$scope.modalForm.csr = true;
		$scope.csrModalCaseNum = caseNum;
		$scope.currentCsr = $scope.csrContainer[type];
		if(type === 'overrideEnrollment'){
			$scope.modalForm.overrideEnrollmentDetails = true;
			$scope.overrideEnrollmentData = {
					"enrollmentId":overrideEnrollStatus.enrollmentId,
					"caseNumber":caseNum,
					"familyMembers":overrideEnrollStatus.enrolleeHistoryEntries
			};
		}
	};

	$scope.openOverrideSEPCSR = function(caseNum, type, appCoverageDate){
		$scope.modalForm.csr = true;
		$scope.modalForm.overrideComment = "";
		$scope.csrModalCaseNum = caseNum;
		$scope.currentCsr = $scope.csrContainer[type];
		$scope.appCoverageDate = appCoverageDate;
	};

	$scope.openOverrideSEPDenial = function(caseNum, appCoverageDate){
		$scope.loader = true;
		var url = '/hix/indportal/applications/'+appCoverageDate+'/checkactive';
		var params = {
				'caseNumber': caseNum
			};
			var config = {
				headers: { 'Content-Type': 'application/json; charset=UTF-8'}
			};

		var response = $http.post(url, null, config);
		response.success(function(data, status, headers, config) {
			$scope.loader = false;
			if(data === "success"){
				$scope.activefailuremsg = true;
				$scope.loader = false;
			}
			else{
				$scope.loader = false;
  				$scope.overrideNewSEPDate = true;
			}
		});
		response.error(function() {
			$scope.loader = false;
			$scope.checkactivefailure = true;
		});
	};

	$scope.getFormattedDate = function(eDate, dateFormat){

		//Check eDate is date, if not return it back
		if(!((null != eDate) && !isNaN(eDate) && ("undefined" !== typeof eDate.getDate))){
			return eDate;
		}

		if (typeof(dateFormat)==='undefined' || dateFormat === "") dateFormat = "MM/DD/YYYY"; //default format

		var monthLongNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var _month = eDate.getMonth();
		var _day = eDate.getDate();
		var _year = eDate.getFullYear();
		var formattedDate = "";

		switch (dateFormat) {
		    case "MMMM DD, YYYY": //January 05, 2015
		    	formattedDate = monthLongNames[_month] + " " + ("0" + _day).slice(-2) + ", " + _year ;
		        break;
		    case "MM/DD/YYYY":  //01/05/2015
		    	formattedDate =  ("0" + (parseInt(_month)+1)).slice(-2) + "/" + ("0" + _day).slice(-2) + "/" + _year ;
		        break;
		    case "MMM DD, YYYY": //Jan 05, 2015
		    	formattedDate = monthShortNames[_month] + " " + ("0" + _day).slice(-2) + ", " + _year ;
		        break;
		}  // add cases as the date format require
		return formattedDate;
	};

	$scope.validateSepEndDate = function($event, condition){
		var dateStr = $event.target.value;
		if (dateStr.length === 8){
			dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
		};
		if (validateDate.check(dateStr)){
			var dateArr = dateStr.split("/");
			var eDate = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
			var today = new Date($('#maServerDateTime').val());
			if (eDate < today){
				$scope.newSEPEndDateErrMsg = "Past date is not allowed";
				$scope[condition] = true;
			} else {
				dateStr = $scope.parseDate($scope.calculateOverrideSEPEnd($("#sepDenialGracePeriod").val()));
				dateArr = dateStr.split("/");
				var lastDate = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
				if (eDate > lastDate){
					$scope.newSEPEndDateErrMsg = "The special enrollment period date cannot be after " + $scope.getFormattedDate(lastDate, 'MMMM DD, YYYY');
					$scope[condition] = true;
				}else{
					delete $scope.newSEPEndDateErrMsg;
					$scope[condition] = false;
				}
			}
		} else {
			$scope.newSEPEndDateErrMsg = "Please enter valid date";
			$scope[condition] = true;
		}
	};

	$scope.calculateOverrideSEPEnd = function(GracePeriod){
		if (typeof(GracePeriod)==='undefined') GracePeriod = 0;
		var endDate = new Date($('#maServerDateTime').val());
		var timeSpan = 60+parseInt(GracePeriod);
		endDate.setDate(endDate.getDate() + timeSpan);
		function modifySingleDigit (date){
			if (date < 10){
				date = "0" + date;
			} else {
				date = "" + date;
			};
			return date;
		}

		var mm = modifySingleDigit(endDate.getMonth() + 1);
		var dd = modifySingleDigit(endDate.getDate());

		var result = ""+ mm + "/" + dd + "/" + endDate.getFullYear();
		return result;
	};

	$scope.modalForm.newSEPEndDate = $scope.parseDate($scope.calculateOverrideSEPEnd($("#sepDenialGracePeriod").val()));

	//The max # of characters allowed in the override reason textarea
	$scope.maxLength = 4000;

	$scope.cancelCsrAction = function(){
		$scope.SPEopened = false;

		if($scope.ReinstateenrollmentData !== undefined){
			$scope.ReinstateenrollmentData.showReinstateEnrollmentOptions = false;
		}

		$scope.csrInputTermDate = false;
		$scope.csrInputOverride = false;
		$scope.termDate = null;
		$scope.changeCovStart = false;
		$scope.SEPStartErr = null;
		$scope.SEPEndErr = null;
		$scope.SEPCovErr = null;
		$scope.termDateErr = null;

		$scope.modalForm.overrideComment = "";
		$scope.modalForm.subResult = null;
		$scope.modalForm.csr = false;
		$scope.modalForm.cancelDate = null;

		$scope.modalForm.sepStartDate = null;
		$scope.modalForm.sepEndDate = null;
		$scope.modalForm.coverageStartDate = null;
		$scope.activefailuremsg = false;
		$scope.overrideNewSEPDate = false;
		$scope.newSEPEndDateErr = false;
		$scope.modalForm.newSEPEndDate = $scope.parseDate($scope.calculateOverrideSEPEnd($("#sepDenialGracePeriod").val())); // reset date

		$scope.modalForm.overrideEnrollmentDetails = true;
		$scope.modalForm.overrideEnrollmentSuccessful = false;

		if($scope.overrideEnrollmentData){
			angular.forEach($scope.overrideEnrollmentData.familyMembers, function(value, key){
				value.issuerAssignedMemberId = '';
			});
		}

		$scope.newCoverageEndDate = '';
		$scope.refresh();
		
	};

	$scope.submitOverrideComment = function(info){
		$scope.loader = true;

		var postData = {'caseNumber': $scope.csrModalCaseNum, 'overrideComment': info};
		var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8' }
		};
		var url = '/hix/indportal/addOverrideComment'+'?encHouseholdid='+encHouseholdId;

		var response = $http.post(url, postData, config);

		response.success(function(aresponse){

			if(typeof(aresponse)==="string" && (aresponse !== "success" && aresponse !== "failure")){
				$scope.loader = false;
				return;
			};

	  		if(aresponse=="success"){
	  			if ($scope.currentCsr.title === "Cancel or Terminate Plan"){
	  				$scope.loader = false;
	  				$scope.csrInputTermDate = true;
	  			} else if ($scope.currentCsr.title === 'Edit Application'){
	  				var postData = 'caseNumber=' + $scope.csrModalCaseNum;
	  				var config = {
	  					headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
	  				};
	  				var response = $http.post('/hix/indportal/cloneApplicationByCaseNumber/'+ $scope.csrModalCaseNum, postData, config);
	  				response.success(function(data, status, headers, config) {
	  					if(typeof(data)==="string" && data !== "" && data != null){
	  						$scope.csrModalCaseNum = data;
	  		  				$scope.editApplication($scope.csrModalCaseNum);
	  					}			
	  				});	  				
	  			} else if ($scope.currentCsr.title === 'Special Enrollment' || $scope.currentCsr.title === 'Change Coverage Start Date'){
	  				$scope.getDTO();
	  			} else if ($scope.currentCsr.title === "Reinstate Enrollment") {
	  				$scope.getApplicationEnrollmentDetails();
	  			} else if ($scope.currentCsr.title === "Override SEP Denial"){
	  				$scope.openOverrideSEPDenial($scope.csrModalCaseNum, $scope.appCoverageDate);
	  				//$scope.loader = false;
	  				//$scope.overrideNewSEPDate = true;
	  			}else if ($scope.currentCsr.title === "Override Program Eligibility"){
	  				return;
	  			}else {
	  				$scope.getSubsq();
	  			}
	  		}else{
	  			$scope.loader = false;
	  			$scope.modalForm.subResult = 'failure';
	  		}

		}).error(function(adata){
			$scope.loader = false;
			$scope.modalForm.subResult = 'failure';

		});
	};
	
	
	$scope.getOverrideEligibilityStartDate = function(application){
		$scope.loader = true;
		$scope.overrideEligibilityStartDatecaseNumber = application.caseNumber
		var url = '/hix/cap/getEligibilityDateDetails?caseNumber=' + $scope.overrideEligibilityStartDatecaseNumber+'&coverageYear='+application.coverageYear+'&encHouseholdid='+encHouseholdId;
		
		var promise = $http.get(url);
		
		promise.success(function(response){
			$scope.loader = false;
			
			if(response) {
	  			$scope.overrideEligibilityStartDateData = response;
	  			$('#overrideStartDateModal').modal();
	  			
	  		} else {
	  			$('#errorModal').modal();
	  		}
			
		})
		
		promise.error(function(response){
			$scope.loader = false;
			$('#errorModal').modal();
		})
	}
	

	$scope.saveOverrideEligibilityDate = function(){
		$scope.loader = true;
		var url = '/hix/indportal/saveOverrideEligibilityDate?caseNumber=' + $scope.overrideEligibilityStartDatecaseNumber;		
		
//		var enrollmentStartDate = moment($scope.overrideEligibilityStartDateData.enrollmentStartDate, 'MM/DD/YYYY');
//		var financialEffectiveDate = moment($scope.overrideEligibilityStartDateData.financialEffectiveDate, 'MM/DD/YYYY');
//		
//		if(enrollmentStartDate.isAfter(financialEffectiveDate)){
//			$scope.overrideEligibilityStartDateData.financialEffectiveDate = $scope.overrideEligibilityStartDateData.enrollmentStartDate
//		}
		
		
		var promise = $http.post(url, $scope.overrideEligibilityStartDateData);
		
		promise.success(function(response){
			$scope.loader = false;
			if(response === 'success') {
				$scope.successModalTitle = "Override APTC Eligibility Start Date";
				$('#successModal').modal();
	  			
	  		} else {
	  			$('#errorModal').modal();
	  		}
			
		})
		
		promise.error(function(response){
			$scope.loader = false;
			$('#errorModal').modal();
		})
	}
	
	$scope.eligibilityStartDateValidation = function(application, progELigForm){
		progELigForm.eligMaxAPTC.$setValidity('required', true);
		progELigForm.eligStartDate.$setValidity('sameYear', true);
		if(application.eligibilityStartDate.substring(application.eligibilityStartDate.length - 4) != $scope.maCurrentCoverageYear){
			progELigForm.eligStartDate.$setValidity('sameYear', false);
		}
	}
	
	$scope.getProgrmeEligibility = function(application){
		$scope.loader = true;
		$scope.caseNumber = application.caseNumber;
		var url = '/hix/cap/getProgrmeEligibility?caseNumber=' + $scope.caseNumber;		
		var promise = $http.get(url);		
		promise.success(function(response){
			$scope.loader = false;			
			if(response) {
	  			$scope.programEligibility = response;
	  			$('#overrideProgEligModal').modal();	  			
	  		} else {
	  			$('#errorModal').modal();
	  		}		
		})
		
		promise.error(function(response){
			$scope.loader = false;
			$('#errorModal').modal();
		})
	}
	
	$scope.confirmOverrideProgElig = function(application){
		$scope.modalForm.overrideComment = "";
		$scope.cnfOvrProgEligModal = true;
	}
	
	$scope.backToOverrideProgElig = function(application,comment){
		$scope.cnfOvrProgEligModal = false;
	}
	
	$scope.submitOverrideProgElig = function(application, comment){
		$scope.csrModalCaseNum = $scope.caseNumber;
		$scope.submitOverrideProgramEligibilityComment(comment);
		var postData = application;
		var currentCoverageYear = parseInt($('#maCurrentCoverageYear').val());
		var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8' }
		};
		var url = '/hix/cap/overrideProgramElig'+'?caseNumber='+$scope.caseNumber+'&coverageYear='+ currentCoverageYear;
		var promise = $http.post(url, postData, config);
		promise.success(function(response){
			$scope.loader = false;
			if(response === 'success') {
				$scope.successModalTitle = "Override Program Eligibility";
				$('#successModal').modal();
	  		} else {
	  			$('#errorModal').modal();
	  		}
		})
		
		promise.error(function(response){
			console.log('got the prog elig error ',response);
			$scope.loader = false;
			$('#errorModal').modal();
		})
	}
	
	$scope.submitOverrideProgramEligibilityComment = function(info){
		$scope.loader = true;

		var postData = {'caseNumber': $scope.csrModalCaseNum, 'overrideComment': info};
		var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8' }
		};
		var url = '/hix/indportal/addOverrideComment'+'?encHouseholdid='+encHouseholdId;

		var response = $http.post(url, postData, config);

		response.success(function(aresponse){

			if(typeof(aresponse)==="string" && (aresponse !== "success" && aresponse !== "failure")){
				$scope.loader = false;
				return;
			};

	  		if(aresponse=="success"){
				return;
	  		}else{
	  			$scope.loader = false;
	  			$scope.modalForm.subResult = 'failure';
	  		}

		}).error(function(adata){
			$scope.loader = false;
			$scope.modalForm.subResult = 'failure';

		});
	};	
	
	$scope.getApplicationEnrollmentDetails = function() {
		var postData = 'caseNumber=' + $scope.csrModalCaseNum;
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }
		};
		var url = '/hix/indportal/getApplicationEnrollmentDetails';

		$http.post(url, postData, config).success(function(resp) {
			$scope.loader = false;

	  		if(resp != null) {
	  			$scope.ReinstateenrollmentData = {};
	  			$scope.ReinstateenrollmentData.showReinstateEnrollmentOptions = true;

	  			if(resp['Health'] != null) {
	  				$scope.ReinstateenrollmentData.healthEnrollmentId = resp['Health'];
	  			}
	  			if(resp['Dental'] != null) {
	  				$scope.ReinstateenrollmentData.dentalEnrollmentId = resp['Dental'];
	  			}
	  			if(resp['Dental'] == null && resp['Health'] == null){
	  				$scope.ReinstateenrollmentData.noenrollmentsPresent = true;
	  			}
	  		} else {
	  			$scope.modalForm.subResult = 'failure';
	  		}
		}).error(function() {
			$scope.loader = false;
			$scope.modalForm.subResult = 'failure';
		});
	};

	$scope.triggerReinstateEnrollment = function() {

	};

	$scope.getDTO = function(){
		var postData = 'caseNumber=' + $scope.csrModalCaseNum;
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		};
		var url = '/hix/indportal/getSepDetails';

		$http.post(url, postData, config).success(function(resp){
			$scope.loader = false;
			if(typeof(resp)==="string" && (resp !== "success" && resp !== "failure")){
				return;
			};

	  		if(resp!=null){
	  			if ($scope.currentCsr.title === 'Special Enrollment'){
	  				$scope.SPEopened = true;
	  			} else if ($scope.currentCsr.title === 'Change Coverage Start Date'){
	  				$scope.changeCovStart = true;
	  			}
  				$scope.SEPData = resp;
	  		}else{
	  			$scope.modalForm.subResult = 'failure';
	  		}
		}).error(function(){
			  $scope.modalForm.subResult = 'failure';
		});
	};

	$scope.initOtr = function(enableOtr, applicationStatus){

		$scope.enableOtr = enableOtr;
		$scope.applicationStatus = applicationStatus;

		if($scope.enableOtr === true && $scope.applicationStatus === 'Enrolled (Or Active)'){
			var renewed = $('#renewed').val() === 'true';

			$scope.isOtrChecked = false;

			$scope.otr = {
				renewed : renewed
			};
		}
	};


	$scope.checkOtr = function(){
		$scope.loader = true;

		var url = '/hix/indportal/otr/' + $('#coverageYear').val();
		var promise = $http.get(url);

		promise.success(function(response){
			$scope.loader = false;

			$scope.otr = response;

			if($scope.otr.showOTR === true){
				$scope.showBootstrapModal('otrOverrideCommentModal');
			}

		});

		promise.error(function(response){
			$scope.loader = false;
			$scope.showBootstrapModal('otrFailModal');
		});
	};


	$scope.saveOtrOverrideComment = function(){
		$scope.loader = true;

		var url = '/hix/indportal/addOverrideComment';

		var data = {
			caseNumber: $scope.otr.caseNumber,
			overrideComment: $scope.overrideComment
		};

		var promise = $http.post(url, data);

		promise.success(function(response){
			$scope.isOtrChecked = true;

			if(response.toUpperCase() === 'SUCCESS'){
				saveOtr();
			}else{
				$scope.loader = false;

				$scope.showBootstrapModal('otrFailModal');
			}

		});

		promise.error(function(response){
			$scope.loader = false;
			$scope.showBootstrapModal('otrFailModal');
		});
	};

	$scope.resetOtr = function(){
		$scope.overrideComment = '';
		$scope.saveOtrResult = null;
	};


	function saveOtr(){
		var url = '/hix/indportal/otr';

		var data = {
			caseNumber: $scope.otr.caseNumber,
			coverageYear: $scope.otr.coverageYear
		};

		var promise = $http.post(url, data);

		promise.success(function(response){
			$scope.loader = false;

			if(response.toUpperCase() === 'SUCCESS'){
				$scope.otr = {
					renewed : true
				}
			}else{
				$scope.showBootstrapModal('otrFailModal');
			}
		});

		promise.error(function(response){
			$scope.loader = false;
			$scope.showBootstrapModal('otrFailModal');
		});

	};

	$scope.showBootstrapModal = function(modalId){
		$('#' + modalId).modal({
			backdrop: 'static',
			keyboard: false
		});
	};


	$scope.getOverrideHistory = function(caseNumber,enrollmentId){
		$scope.loader = true;
		var postData = 'caseNumber=' + caseNumber;
		var params = {
				"enrollmentId" : enrollmentId
			};
		var config = {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
			params:params
		};
		var response = $http.post('indportal/getOverrideCommentsOfEnrollmentHistory', postData, config);
		response.success(function(data, status, headers, config){
			$scope.loader = false;
			if(typeof(data)==="string" && data==="failure"){
				return;
			}
			$scope.modalForm.csrOverrideHistory = true;
			$scope.modalForm.subResult = true;
	  		$scope.overrideHistoryData = data["comments"];
		});

		response.error(function(data, status, headers, config){
			$scope.modalForm.subResult = 'failure';
		});
	}

	$scope.validateLastPremiumPaidDate = function($event, condition){//
		var dateStr = $event.target.value;
		if (dateStr.length === 8){
			dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
		};
		if (!validateDate.check(dateStr)){
			$scope.LastPremiumPaidDateErrMsg = "Please enter valid date";
			$scope[condition] = true;
		} else {
			delete $scope.LastPremiumPaidDateErrMsg;
			$scope[condition] = false;
		}
	};

	$scope.getOverrideEnrollmentResult = function(overrideCommentText, overrideEnrollmentData){
		$scope.loader = true;
		var postData = overrideEnrollmentData;
		postData['overrideCommentText'] = overrideCommentText;
		var config = {
			headers: { 'Content-Type': 'application/json; charset=UTF-8'}
		};
		var url = '/hix/indportal/updateEnrollmentStatus';

		$http.post(url, postData, config).success(function(resp){
			$scope.loader = false;
			if(typeof(resp)==="string" && (resp !== "success" && resp !== "failure")){
				return;
			};
			//hide member details modal body
  			$scope.modalForm.overrideEnrollmentDetails = false;

	  		if(resp!=null && resp === "success"){
	  			//show successful modal body
	  			$scope.modalForm.overrideEnrollmentSuccessful = true;
	  		}else{
	  			$scope.modalForm.subResult = 'failure';
	  		}
		}).error(function(){
			  $scope.modalForm.subResult = 'failure';
		});
	};


	$scope.dateCheck = function($event, condition){
		if (validateDate.check($event.target.value)){
			$scope[condition] = false;
		} else {
			$scope[condition] = true;
		}
	};
	
	$scope.covStartdateCheck = function($event,condition){
		$scope.dateCheck($event,condition);
		$scope.chkCurrYear(condition);
	}
	
	$scope.chkCurrYear = function(condition){
		let covStDt;
		if($scope.SEPData.covStartDate) {
			covStDt = new Date(Date.parse($scope.SEPData.covStartDate.replace(/^(\d\d)(\d\d)(\d\d\d\d)$/,"$1 $2 $3") ) );			
		}

		if(covStDt.getYear()!=new Date($('#maServerDateTime').val()).getYear()){
			$scope[condition] = true;
		}else{
			$scope[condition] = false;
		}
	};

	$scope.calculateSEPEnd = function(){
		var endDate = new Date();
		var timeSpan = 70;
		endDate.setDate(endDate.getDate() + timeSpan);
		function modifySingleDigit (date){
			if (date < 10){
				date = "0" + date;
			} else {
				date = "" + date;
			};
			return date;
		}

		var mm = modifySingleDigit(endDate.getMonth() + 1);
		var dd = modifySingleDigit(endDate.getDate());

		var result = ""+ mm + "/" + dd + "/" + endDate.getFullYear();
		return result;
	};

	 $scope.sepEndDate = $scope.calculateSEPEnd();

	$scope.eventDetailsErrorCount = 0;
	$scope.validateEDate = function(dateStr, index){
		if (dateStr.length === 8){
			dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
		};
		if (validateDate.check(dateStr)){
			var dateArr = dateStr.split("/");
			var eDate = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
			var today = new Date();
			if (eDate > today){
				$scope.eventDetails[index].dateError = true;
				$scope.eventDetails[index].over70Days = false;
				$scope.eventDetails[index].dateErrorMsg || $scope.eventDetailsErrorCount++;
				$scope.eventDetails[index].dateErrorMsg = "Event date(s) cannot be in the future.";
			} else if ((today - eDate)/(1000*60*60*24) > 70 ){
				$scope.eventDetails[index].over70Days = true;
				(($scope.eventDetailsErrorCount > 0 && $scope.eventDetails[index].dateErrorMsg) && $scope.eventDetailsErrorCount--);
			} else {
				$scope.eventDetails[index].dateError = false;
				$scope.eventDetails[index].over70Days = false;
				(($scope.eventDetailsErrorCount > 0 && $scope.eventDetails[index].dateErrorMsg) && $scope.eventDetailsErrorCount--);
				delete $scope.eventDetails[index].dateErrorMsg;
			}
		} else {

			$scope.eventDetails[index].dateError = true;
			$scope.eventDetails[index].over70Days = false;
			$scope.eventDetails[index].dateErrorMsg || $scope.eventDetailsErrorCount++;
			$scope.eventDetails[index].dateErrorMsg = "Please use a valid date format.";
		}
	};

	angular.forEach($scope.eventDetails, function(val, key){
		$scope.validateEDate(val.eventDate, key);
	});

	$scope.validateSepDate = function(dateStr){
		if (dateStr.length === 8){
			dateStr = dateStr.slice(0, 2) + "/" + dateStr.slice(2, 4) + "/" + dateStr.slice(4,8);
		};

		if (validateDate.check(dateStr)){
			var dateArr = dateStr.split("/");
			var eDate = new Date(dateArr[2], dateArr[0] - 1, dateArr[1]);
			var today = new Date();
			if (eDate > today){
				$scope.SepDateErrorMsg = "Future date is not allowed";
			} else {
				delete $scope.SepDateErrorMsg;
			}
		} else {
			$scope.SepDateErrorMsg = "Please enter valid date";
		}
	}
	//APTC slider

	$scope.showAdjustButton = true;

	$scope.changeAptc = function(){

		$scope.loader = true;
		$scope.isAptcGetUpdated = undefined;
		var url = '/hix/indportal/aptcPremiumEffDate';
		var data = {
			'caseNumber': $scope.healthdetails.caseNumber,
			'healthEnrollmentId': $scope.healthdetails.enrollmentId,
			'aptcPremiumEffectiveDate': $scope.sliderEffectiveDate
		};

		var promise = $http.post(url, data);


		promise.success(function(res){
			$scope.loader = false;

			if(typeof(res) === 'object' && res.response !== null && res.response !== 'failure'){
				$scope.showAptcSliderError = false;

				$scope.showItem1 = true;
				$scope.showItem2 = false;
				$scope.showAptcSlider = true;
				$scope.showConfirmButton = false;
				$scope.showAdjustButton = true;

				$scope.sliderEffectiveDate = res.aptcPremiumEffectiveDate;
				$scope.newAptc = res.aptcAmt;
			}else{
				$scope.showAptcSliderError = true;
			}
		});


		promise.error(function(res){
			$scope.loader = false;

			$scope.showAptcSliderError = true;
		});

	};

	$scope.showConfirmButton = false;

	$scope.showSlide2 = function(){
		$scope.showItem1 = false;
		$scope.showItem2 = true;
		$scope.showConfirmButton = true;
		$scope.showAdjustButton = false;

		$scope.newAptc = $scope.healthdetails.electedAptc;

		$scope.formatAptcCredit();
		$scope.electedAptc = $scope.newAptc;
		$scope.getSliderWidth();
		$scope.aptcOverMax = false;
	};

	$scope.saveAptc = function(){
		if($scope.aptcOverMax){
			return;
		};

		$scope.loader = true;
		var postData = {'caseNumber': $scope.healthdetails.caseNumber,
				'healthEnrollmentId': $scope.healthdetails.enrollmentId,
				'aptcAmt':$scope.newAptc,
				'aptcPremiumEffectiveDate': $scope.sliderEffectiveDate};

		if ($scope.dentaldetails){
			postData['dentalEnrollmentId'] = $scope.dentaldetails.enrollmentId;
		}

		var config = {
			headers: {  'Content-Type': 'application/json; charset=UTF-8' }
		};
		var url = '/hix/indportal/updateaptc';

		var response = $http.post(url, postData, config);

		response.success(function(responseData, status, headers, config){
			if(typeof(responseData)==="string" && (responseData !== "success" && responseData !== "failure")){
				$scope.loader = false;
				return;
			}
			if(responseData=="success"){
				$scope.showAptcSlider = false;
				$scope.loader = false;
				$scope.refreshPlanSummary ($scope.healthdetails.caseNumber);
			}else{
				$scope.showAptcSlider = false;
				$scope.loader = false;
				$scope.payFHFail = true;
			}
		})
		.error(function(responseData, status, headers, config){
			$scope.showAptcSlider = false;
			$scope.loader = false;
			$scope.payFHFail = true;
		});
	};

	$scope.getSliderWidth = function (){
		$scope.isAptcGetUpdated === undefined? $scope.isAptcGetUpdated = false : $scope.isAptcGetUpdated = true;
		if (parseFloat($scope.newAptc) <= parseFloat($scope.healthdetails.maxAptc)){
			$scope.width = ($scope.newAptc /$scope.healthdetails.maxAptc)*96.5 +"%";
		} else {
			$scope.width = "96.5%";
		}

	};

	$scope.formatAptcCredit = function(){
		var decimal = $scope.newAptc.indexOf(".");
		var strLen = $scope.newAptc.length;
		if (decimal === 0) {
			$scope.newAptc = "0" + $scope.newAptc;
		};

		if (decimal > -1){
			if (decimal === strLen - 1){
				$scope.newAptc = $scope.newAptc + "00";
			} else if (decimal === strLen - 2){
				$scope.newAptc = $scope.newAptc + "0";
			} else if (decimal < strLen - 3){
				$scope.newAptc = $scope.newAptc.substring(0, decimal + 3);
			}
		} else {
			$scope.newAptc = $scope.newAptc + ".00";
		};
		$scope.newAptc = parseFloat($scope.newAptc);
	};

	$scope.changeSelectedAptc = function(option){
		if (option === "max"){
			$scope.newAptc = $scope.healthdetails.maxAptc;
		} else {
			$scope.newAptc = $scope.electedAptc;
		};
		$scope.formatAptcCredit();
		$scope.getSliderWidth();
		$scope.aptcOverMax = false;
	};

	//enrollments history
	$scope.getEnrollmentsHistory = function(){

		var ehCurrentCoverageYear = parseInt($('#ehCurrentCoverageYear').val())
		$scope.ehCoverageYearArray = [];

		for(var i = ehCurrentCoverageYear; i >= 2015; i--  ){
			$scope.ehCoverageYearArray.push(i);
		}

		if($scope.activeEhCoverageYear === undefined){
			$scope.activeEhCoverageYear = ehCurrentCoverageYear;
		}



		$scope.loader = true;
		var response = $http.get('indportal/enrollmenthistory?coverageYear=' + $scope.activeEhCoverageYear+'&tmstp='+new Date().getTime());
		response.success(function(data, status, headers, config) {

			if(typeof(data)==="string" && (data !== "success" && data !== "failure")){
				return;
			}

			$scope.loader = false;
			$scope.myEnrollments = data; /*activeHealthEnrollments, data.activeDentalEnrollments, data.inactiveHealthEnrollments, data.inactiveDentalEnrollments];*/
			$scope.healthdetails = data.activeHealthEnrollments[data.activeHealthEnrollments.length-1];
			$scope.dentaldetails = data.activeDentalEnrollments[data.activeDentalEnrollments.length-1];
			if($scope.healthdetails){
				$scope.healthdetails['disEnroll'] = $scope.healthdetails.allowDisenroll;
			}
			if($scope.dentaldetails){
				$scope.dentaldetails['disEnroll'] = $scope.dentaldetails.allowDisenroll;
			}
		});
		response.error(function() {
			$scope.loader = false;
			$scope.openInitFail = true;
		});
	};

	$scope.enrollmentCsrAction = function($event){
		$($event.target).closest('.enrollmentCsrActionDiv').find('.csrButtonDiv').slideToggle();
	};
	
	
	$scope.getSepEventDetails = function(application){
		var url = '/hix/cap/applicant/events/' + application.caseNumber + '/all/'+encHouseholdId;
		var promise = $http.get(url);
		
		$scope.loader = true;
		
		//remove previous error message
		$scope.sameEventIsSelected = false;
		
		promise.success(function(response){
			if(response.status === '200'){
				$scope.sepDetails = response;

				for(var i=0; i<$scope.sepDetails.applicants.length; i++){
					for(var j=0; j<$scope.sepDetails.applicants[i].events.length; j++){
						$scope.sepDetails.applicants[i].events[j].previousEventId = $scope.sepDetails.applicants[i].events[j].eventId;
						$scope.sepDetails.applicants[i].events[j].previousEventDate = $scope.sepDetails.applicants[i].events[j].eventDate;
						$scope.sepDetails.applicants[i].events[j].previousEventNameLabel = $scope.sepDetails.applicants[i].events[j].eventLabel;
						
					}
				}
				
				
				$scope.sepDetails.caseNumber = application.caseNumber;
				$scope.sepDetails.isFinancial = application.isNonFinancial? false : true;
				
				//button only show for QLE and SEP application, no OE application
				$scope.sepDetails.applicationType = application.isQep? 'QEP' : 'SEP';
				
			}else{
				$('#errorModal').modal();
			}
			
		}).then(getSepEventDetails);
		
		promise.error(function(response){
			$scope.loader = false;
			$('#errorModal').modal();
		});
	}
	
	function getSepEventDetails(){
		var promises = [];
		
		for(var i=0; i<$scope.sepDetails.applicants.length; i++){
			for(var j=0; j<$scope.sepDetails.applicants[i].events.length; j++){
				promises.push(getSepEventDetailsAPICall(i, j));
			}
		}
		
		
		$q.all(promises).then(function(){
			$scope.loader = false;
			openOverrideSepModal();
		}, function(){
			$scope.loader = false;
			$('#errorModal').modal();
		});
		
	}
	
	function getSepEventDetailsAPICall(aplicantIndex, eventIndex){
		var url = '/hix/indportal/getSEPEventsByApplicationType';
		var defer = $q.defer();
		
		var data = {
			isFinancial: $scope.sepDetails.isFinancial? 'Y' : 'N',
			displayOnUI: 'Y',
			changeType: $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].changeType
		}
		
		var promise = $http.post(url, data);
		
		promise.success(function(response){
			var eventList = [];
			var currentEventId = $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventId;
			var isCurrentEventContained = false;
			
			response.sepEventss.forEach(function(item){
				var eventId = item._links.self.href.split('/').pop();
				
				eventList.push({
					eventNameLabel: item.label,
					eventId: eventId,
					type: item.type,
					allowFuture:item.allowFutureEvent
				});
						
				if(eventId == currentEventId){
					isCurrentEventContained = true
				}
			})
			
			$scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventList = eventList;		
					
			if(!isCurrentEventContained){
				$scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventList.push({
					eventNameLabel: $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventLabel,
					eventId: $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].eventId,
					type: $scope.sepDetails.applicants[aplicantIndex].events[eventIndex].type
				});
			}
			
			defer.resolve();
		});
		
		promise.error(function(response){
			defer.reject();
		});
		
		return defer.promise;
	}
	
	
	function openOverrideSepModal(){
		
		$scope.isSepEventDetailsForm= true;
		$scope.isSepDateDetailsForm = false;
		$scope.sepDetails.overrideCommentText= '';
		
		//2 calls: 1-get events and dates; 2-get dropdown list
		$('#overrideSepModal').modal({
			keyboard: false,
			backdrop: 'static'
		});
	}

	$scope.getSepDateDetails = function(){
		//check whether there is changed
		$scope.applicantUpdatedSepEvents = getApplicantEvents();
		
		//remove previous error message
		$scope.sepDateDetailsForm.sepEndDate.$setValidity("startBeforeEnd", true);
		
		//console.log($scope.applicantUpdatedSepEvents.length);
		if($scope.applicantUpdatedSepEvents.length === 0){
		
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("isUpdated", false);
			
			$scope.loader = true;
			var url = '/hix/indportal/getSEPPeriodDates?caseNumber=' + $scope.sepDetails.caseNumber;

			var promise = $http.get(url);
			
			promise.success(function(response){
				$scope.loader = false;
				
				if(response !== ''){
					$scope.sepDetails.sepStartDate = response.sepStartDate;
					$scope.sepDetails.sepEndDate = response.sepEndDate;	
					
					updateSepDatesAndPages();
				}else{
					$('#overrideSepModal').modal('hide');
					$('#errorModal').modal();
				}
			});
			
			promise.error(function(response){
				$scope.loader = false;
				$('#overrideSepModal').modal('hide');
				$('#errorModal').modal();
			});

			
		}else{
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("isUpdated", true);
			
			$scope.sepDetails.sepStartDate = getSepDate('MIN');
			$scope.sepDetails.sepEndDate = getSepDate('MAX');
			
			updateSepDatesAndPages();

		}

		}
		
	
	function updateSepDatesAndPages(){
		$scope.sepDetails.previousSepStartDate = $scope.sepDetails.sepStartDate;
		$scope.sepDetails.previousSepEndDate = $scope.sepDetails.sepEndDate;
		
		$scope.isSepEventDetailsForm = false;
		$scope.isSepDateDetailsForm = true;
	}
	
	$scope.validateEventAndDate = function(eventForm, event, applicant){		
		hasSameEvent(eventForm);
		$scope.eventDateValidation(eventForm, event, applicant);
	}
	
	
	function hasSameEvent(eventForm){
		
		$scope.sameEventIsSelected = false;

		for(var i=0; i<$scope.sepDetails.applicants.length; i++){
			var events = $scope.sepDetails.applicants[i].events.map(function(eventObj){
				return eventObj.eventId;
			})
			
			$scope.sameEventIsSelected = events.some(function(event, index){ 
			    return events.indexOf(event) != index 
			});
			
			if($scope.sameEventIsSelected){
				break;
			}

		}
		
	}
	
	
	
	
	$scope.eventDateValidation = function(eventForm, event, applicant){
				eventForm.eventDate.$setValidity('withinFuture60Days', true);
				eventForm.eventDate.$setValidity('withinPast60Days', true);
				eventForm.eventDate.$setValidity('within60Days', true);				
		//eventForm.eventDate.$setValidity('sameYear', true);
		
		/*if(event.eventDate.substring(event.eventDate.length - 4) != $scope.maCurrentCoverageYear){
			eventForm.eventDate.$setValidity('sameYear', false);
		}else{*/
			for(var i = 0; i < $scope.sepDetails.applicants.length; i++){
				
				//get the right person
				if($scope.sepDetails.applicants[i].personId === applicant.personId){
				for(var j = 0; j < $scope.sepDetails.applicants[i].events.length; j++){
				
				var eventType;
				var allowFutureEvent;
				for(var k = 0; k < $scope.sepDetails.applicants[i].events[j].eventList.length; k++){
					if(event.eventId === $scope.sepDetails.applicants[i].events[j].eventList[k].eventId){
						eventType = $scope.sepDetails.applicants[i].events[j].eventList[k].type;
						allowFutureEvent = $scope.sepDetails.applicants[i].events[j].eventList[k].allowFuture;
					}
				}
						
						if(eventType !== undefined){
				if(allowFutureEvent === 'Y'){
					//within past or future 60 days
					eventForm.eventDate.$setValidity('within60Days', uiDateValidation(event.eventDate, 'within60Days'));
				}else{
					//within past 60 days
					eventForm.eventDate.$setValidity('withinPast60Days', uiDateValidation(event.eventDate, 'withinPast60Days'));
				}
			}
					}
				}

		}
//	}
	
	}
	
	function uiDateValidation(eventDate, type){
		if(eventDate !== undefined){
			
			var date;
			if(eventDate.length === 10){
				date = eventDate;
			}else{
				date = eventDate.substring(0,2) + '/' + eventDate.substring(2,4) + '/' + eventDate.substring(4,8);
			}

			if(type === 'withinFuture60Days'){
				
				return moment().isSame(date, 'day') || moment().diff(date, 'day') > -59 && moment().diff(date, 'day') < 0;
				
			}else if(type === 'withinPast60Days'){
				
				return moment().isSame(date, 'day') || moment().diff(date, 'day') < 60 && moment().diff(date, 'day') > 0;
				
			}else if(type === 'within60Days'){
				
				return moment().isSame(date, 'day') || moment().diff(date, 'day') < 60 && moment().diff(date, 'day') > 0 || moment().diff(date, 'day') > -59 && moment().diff(date, 'day') < 0;
				
			}
			
		}else{
			return true;
		}

		
	}
	
	
	$scope.validateStartBeforeEnd = function(){
		var startDate = $scope.sepDetails.sepStartDate;
		var endDate = $scope.sepDetails.sepEndDate;

		//if(startDate && endDate && moment(startDate,'MM/DD/YYYY').isValid() && moment(endDate,'MM/DD/YYYY').isValid()){
		//if(startDate && endDate && $scope.sepDateDetailsForm.sepStartDate.$valid && $scope.sepDateDetailsForm.sepEndDate.$valid){
		if(startDate && endDate){
			startDate = startDate.indexOf('/') > -1? startDate : startDate.substring(0,2) + '/' + startDate.substring(2,4)+ '/' + startDate.substring(4,8);
			endDate = endDate.indexOf('/') > -1? endDate : endDate.substring(0,2) + '/' + endDate.substring(2,4)+ '/' + endDate.substring(4,8);
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("startBeforeEnd", moment(endDate).isAfter(startDate, 'day') || moment(endDate).isSame(startDate, 'day') );
			
		}else{
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("startBeforeEnd", true);
		}
		
		
		if($scope.applicantUpdatedSepEvents.length === 0 && startDate === $scope.sepDetails.previousSepStartDate && endDate === $scope.sepDetails.previousEndDate){
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("isUpdated", false);
		}else{
			$scope.sepDateDetailsForm.sepEndDate.$setValidity("isUpdated", true);
		}

		
	}
	
	
	
	function getUiDateFormat(dateToFormat){
		var date = dateToFormat? new Date(dateToFormat) : new Date()

		var dd = date.getDate();
		var mm = date.getMonth()+1; 

		var yyyy = date.getFullYear();
		if(mm<10){
		    mm='0'+mm;
		}
		
		if(dd<10){
		    dd='0'+dd;
		}
		return mm+'/'+dd+'/'+yyyy;
		
	}
	
	
	function getSepDate(type){
		var dates = [];
		for(var i = 0; i < $scope.sepDetails.applicants.length; i++){
			if($scope.sepDetails.applicationType === 'SEP' || ($scope.sepDetails.applicationType === 'QEP' && $scope.sepDetails.applicants[i].personId === 1)){
			for(var j = 0; j < $scope.sepDetails.applicants[i].events.length; j++){
				dates.push(new Date($scope.sepDetails.applicants[i].events[j].eventDate));
			}
		}
		}
		
		var dateToReturn;
		var oneDay = 24*60*60*1000;
		if(type === 'MAX'){
			var sepGracePeriod = parseInt($('#gracePeriod').val());
			dateToReturn = Math.max.apply(null,dates);
			
			dateToReturn += oneDay*(60 + sepGracePeriod);	
		}else{
			dateToReturn = Math.min.apply(null,dates);
		}
		return getUiDateFormat(dateToReturn);
	}
	
	$scope.backToSepDetails = function(){
		$scope.isSepEventDetailsForm= true;
		$scope.isSepDateDetailsForm= false;
	}
	
	$scope.saveSepOverride = function(){
		$scope.loader = true;
		var url = '/hix/indportal/overrideSEPDetails?encHouseholdid='+encHouseholdId;
		
		var data = {
			sepStartDate: $scope.sepDetails.sepStartDate,
			sepEndDate: $scope.sepDetails.sepEndDate,
			caseNumber: $scope.sepDetails.caseNumber,
			applicantEvents: $scope.applicantUpdatedSepEvents,
			overrideCommentText: $scope.sepDetails.overrideCommentText,
			coverageYear: $scope.maCurrentCoverageYear,
			applicationType: $scope.sepDetails.applicationType,
			isFinancial: $scope.sepDetails.isFinancial
		};
		
		var promise = $http.post(url, data);
		
		promise.success(function(response){
			$scope.loader = false;
			
			if(response !== 'success'){
				$('#errorModal').modal();
			}else{
				$scope.successModalTitle = "Override Special Enrollment";
				$('#successModal').modal();
			}
		});
		
		promise.error(function(response){
			$scope.loader = false;
			
			$('#errorModal').modal();
		});
		
		
	}
	
	function getApplicantEvents(){
		var applicantEvents = [];
		
		for(var i=0; i< $scope.sepDetails.applicants.length; i++){
			for(var j=0; j< $scope.sepDetails.applicants[i].events.length; j++){
				
				var eventObj = $scope.sepDetails.applicants[i].events[j];
				
				if(eventObj.eventId.toString() !== eventObj.previousEventId.toString() || eventObj.eventDate !== eventObj.previousEventDate){
					
					var eventNameLabel = '';
					
					eventObj.eventList.forEach(function(item, index){
						if(item.eventId.toString() === eventObj.eventId.toString()){
							eventNameLabel = item.eventNameLabel
						}
					})
					
					applicantEvents.push({
						firstName: $scope.sepDetails.applicants[i].firstName,
						middleName: $scope.sepDetails.applicants[i].middleName,
						lastName: $scope.sepDetails.applicants[i].lastName,
						eventId: eventObj.eventId,
						eventDate: eventObj.eventDate,
						applicantEventId: eventObj.applicantEventId,
						eventNameLabel: eventNameLabel,
						previousEventId: eventObj.previousEventId,
						previousEventNameLabel: eventObj.previousEventNameLabel
					})
				}
			}
		}
		
		//if QEP and there is change, that change must be for primary contact
		if($scope.sepDetails.applicationType === 'QEP' && applicantEvents.length !== 0){
			
			var eventId = applicantEvents[0].eventId;
			var	eventDate = applicantEvents[0].eventDate;
			var	eventNameLabel = applicantEvents[0].eventNameLabel;
			
			for(var i=0; i< $scope.sepDetails.applicants.length; i++){					
				var applicantObj = $scope.sepDetails.applicants[i];
				if(applicantObj.personId !== 1){
					applicantEvents.push({
						firstName: applicantObj.firstName,
						middleName: applicantObj.middleName,
						lastName: applicantObj.lastName,
						applicantEventId: applicantObj.events[0].applicantEventId,
						previousEventId: applicantObj.events[0].eventId,
					    previousEventNameLabel: applicantObj.events[0].eventLabel,
					    eventId: eventId,
						eventDate: eventDate,
						eventNameLabel: eventNameLabel
					})
				}
			}
		}
		
		
		return applicantEvents;
	}

}]);
//End indPortalAppCtrl controller

//This controller is for contactUs.jsp and appeal.jsp
indPortalApp.controller('contactUsCtrl', ['$scope', '$http', '$upload', '$q', '$timeout', function($scope, $http, $upload, $q, $timeout){
  $scope.filesSelected = [];
  $scope.filesUploaded = [];
  var counter = 1;

  $scope.formData = {};
  $scope.appealForm = {};


  $scope.clearReason = function (type){
	  if (type != false) {
		  $scope.formData.reason = null;
	  };
  };


  $scope.selectAppealFile =  function($files, event){
	  var name = $files[0].name.split(".");
	  if (!$scope.checkFileType($files)){
		  event.target.parentElement.children[2].value ="";
		  $scope.wrongFileTypeDialog = true;
		  return
	  };
	  $scope.appealfileSelected= $files[0];
  };

  $scope.cancelAppealfile =  function(){
	  $scope.appealfileSelected= null;
	  event.target.parentElement.children[2].value ="";
  };

  $scope.onFileSelect= function($files, index, event){
      index = index || 0;
	  var name = $files[0].name.split(".");
	  if (!$scope.checkFileType($files)){
		  event.target.parentElement.children[1].value = "";
		  $scope.wrongFileTypeDialog = true;
		  return
	  };
      $scope.filesSelected[index] = $files[0];
    };

  $scope.addFile = function(){
    $scope.filesSelected.push(counter);
    counter++;
  };

  $scope.cancelFile = function(index){
    var toDel = $scope.filesSelected[index];

    for (var i = 0; i < $scope.filesUploaded.length; i++) {
      if ($scope.filesUploaded[i].name === toDel.name){
        $scope.filesUploaded.splice(i, 1);
      }
    };
    $scope.filesSelected.splice(index, 1);

    if (index == 0){
    	event.target.parentElement.children[1].value = "";
    };
   };

  $scope.uploadFile = function(index){
    var target = $scope.filesSelected[index];
    if ($scope.filesUploaded.length === 0){
    	return $scope.uploadOneFile(index);
    } else {
      for (var i = 0; i < $scope.filesUploaded.length; i++) {
        if ($scope.filesUploaded[i].name === target.name){
        	return $scope.uploadOneFile(index);
        }
      };
    }
   	return $scope.uploadOneFile(index);
    };


  var uploadURL = "/hix/indportal/upload";
  var submitURL = "/hix/indportal/submitcontactus";
  var uploadAppealURL = "/hix/indportal/upload";

  $scope.checkFileType = function(files){
	  var supportedFileType = {
	     "txt": true,
	     "css": true,
	     "xml": true,
	     "csv": true,
	     "png": true,
	     "jpg": true,
	     "jpeg": true,
	     "bmp": true,
	     "gif": true,
	     "doc": true,
	     "docx": true,
	     "dot": true,
	     "dotx": true,
	     "xls": true,
	     "xlsx": true,
	     "ppt": true,
	     "pptx": true,
	     "pdf": true,
	     "zip": true,
	     "tar": true,
	     "gz": true,
	     "js": true
	  };

	  var name = files[0].name.split(".");

	 if (supportedFileType[name[name.length - 1]]){
		 return true;
	 } else {
		 $scope.wrongFileName = files[0].name;
		 $scope.wrongFileType = name[name.length - 1];
		 return false;
	 }
  };

//fileToUpload {file:}
  $scope.uploadOneFile = function(index, type){

	  var file;
	  var url;
	  var deferred = $q.defer();
	if (type === "appeal"){
		file = $scope.appealfileSelected;
		url = uploadAppealURL;
	} else {
		file = $scope.filesSelected[index];
		url = uploadURL;
		$scope.typeUpload = 'contact';
	};

	if (file.size> 5242880){
		$scope.loader=false;
		$scope.oversizedFileD = true;
		deferred.reject();
		return deferred.promise;
	}

	var fileName = 'fileToUpload';
    var fd = new FormData();

    fd.append(fileName, file),
    fd.append("csrftoken",$('#tokid').val());

   $http.post(url,fd, {
	   transformRequest: angular.identity,
	   headers: {'Content-Type':undefined}
    }).success(function(data){
    	if (data == 'failure'){
    		$scope.loader = false;
    		$scope.openDialog = true;
    		deferred.reject();
    	} else {
    		var temp = {};
    		temp.file = data;
    		temp.name = file.name;
    		if ($scope.typeUpload === 'contact'){
        		$scope.filesUploaded.push(temp);
    		} else {
    			$scope.appealFileUploaded = temp;
    		}
   			file.uploaded = true;
        deferred.resolve();
    	}
    }).error(function(){
    	$scope.loader = false;
    	$scope.openDialog = true;

    	deferred.reject();
    });
    return deferred.promise;
};

$scope.openD= function(){
	$scope.openDialog = true;
	return true;
};

//need to add function to check the filesSelected
  $scope.submitForm = function(){
	  $scope.loader = true;
	  var starting = new Date();
	  var sendToServer = function (){

    	$scope.formData.fileList = [];
    	angular.forEach($scope.filesUploaded, function(val, key){
    		$scope.formData.fileList.push(val.file);
    	});

    	$scope.postResult="";
    	var params = {
    			"csrftoken": $('#tokid').val()
    	};

          $http({
	      method: 'POST',
	      url: submitURL,
	      data: JSON.stringify($scope.formData),
	      headers: { 'Content-Type': 'application/json; charset=UTF-8' },
	      params: params

	    }).success(function(data){
	    	$scope.loader = false;
	    	if (data == "success"){
	   	     	$scope.postResult = 'postsuccess';
	    	} else {
	    		$scope.postResult = 'postfailure';
	    	};

	    }).error(function(data){
	    	$scope.postResult = 'postfailure';
	    	$scope.loader = false;
	    });
	  };

    if ($scope.filesSelected.length !== $scope.filesUploaded.length){
      var promiseList = [];
      for (var i = 0; i < $scope.filesSelected.length; i++) {
    	if (typeof $scope.filesSelected[i] === 'object'){
        	var deferred = $q.defer();
        	promiseList.push(deferred.promise);
        	(function(){
        		var data = $scope.uploadFile(i);
        		deferred.resolve(data);
        	}());
    	}
      };
      var promiseAll = $q.all(promiseList);

       promiseAll.then(function(){
    	   sendToServer();
       });

    } else {
    	sendToServer();
    }

  };

  $scope.submitAppeal= function(){
    var appealUrl = "/hix/indportal/submitappeal";

    $scope.loader = true;

    $scope.appealPostResult='';
	  var sendToServer = function (){
		  if ( $scope.appealFileUploaded){
	   		$scope.appealForm.fileId = $scope.appealFileUploaded.file;
		  };

		  if ($scope.appealForm.appealReason == null){
			  $scope.appealForm.appealReason = "Qhp";
		  }
    	$scope.postResult='';
          $http({
	      method: 'POST',
	      url: appealUrl,
	      data: JSON.stringify($scope.appealForm),
	      headers: { 'Content-Type': 'application/json; charset=UTF-8' }
	    }).success(function(data){
	    	if (data == "success"){
	   	     	$scope.appealPostResult = 'postsuccess';
	    	} else {
	    		$scope.appealPostResult = 'postfailure';
	    	};

	      $scope.loader = false;
	    }).error(function(data){
	    	$scope.appealPostResult = 'postfailure';
	    	$scope.loader = false;
	    	$scope.openDialog='true';
	    });
	  };

	  if ($scope.appealfileSelected) {
		  var promise = $scope.uploadOneFile(0, 'appeal');
		  promise.then(function(){
			  sendToServer();
		   }, function(){
		     $scope.loader = false;
		     $scope.openDialog='true';
		   });
	  } else {
			  sendToServer();
  };
  };
}]);

indPortalApp.factory('storageFty', function(){
	var storage = {};
	return {
		setVal: function(key, value){
			storage.key = value;
		},

		getVal: function(key){
			return storage.key;
		}

	};
});



(function(){
	var validateDate = function () {
		return {
		  check: function(date){
			if (date === ""){
				return false;
			}


			if (/[dmy]/.test(date)){
				return false;
			}
			var dateArr = date.split("/");

			var daysInMo;

    		switch(dateArr[0]){
    		  case "02":
    		    if (dateArr[2] % 4 === 0){
    		      daysInMo = 29;
    		    } else {
    		      daysInMo = 28;
    		    };
    		    break;
    		  case "04" : case "06" : case "09" : case "11":
    		    daysInMo = 30;
    		    break;
    		  default:
    		    daysInMo = 31;
    		}

    		if (dateArr[0] > 12 || dateArr[1] > daysInMo || dateArr[0] < 1 || dateArr[1] < 1){
    			return false;
    		} else {
    			return true;
    		}
		  }
		};
	};

	angular.module('indPortalApp')
	.factory('validateDate', validateDate);
})();

indPortalApp.filter('removeForwardSlashes', function () {
	return function (input) {
	    if(input){
	    	return input.replace(/\//g, "");
	    }
	    return input;
	};
});

indPortalApp.filter('formatDate', function () {
	return function (input) {
	    if(input){
	    	var inputArr = input.split("/");
	    	var formattedDate= new Date(inputArr[2], inputArr[0] - 1, inputArr[1]);
	    	return formattedDate;
	    }
	    return input;
	};
});

indPortalApp.directive('triggerAddFile', function(){
return {
  link: function(scope, element, attrs){
    element.bind('click', function(){
    	var inputBtn = element.parent().find('input');
        inputBtn.click();
      });
    }
  };
});

indPortalApp.directive('ngFocus', [function() {
	var FOCUS_CLASS = "ng-focused";
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$focused = false;
			element.bind('focus', function(evt) {
				element.addClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = true;
				});
			}).bind('blur', function(evt) {
				element.removeClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = false;
				});
			});
		}
	};
}]);

indPortalApp.directive("modalShow", function ($parse) {
return {
    restrict: "A",
    link: function (scope, element, attrs) {

        //Hide or show the modal
        scope.showModal = function (visible, elem) {
            if (!elem)
                elem = element;

            if (visible)
                $(elem).modal("show");
            else
                $(elem).modal("hide");
        };

        //Watch for changes to the modal-visible attribute
        scope.$watch(attrs.modalShow, function (newValue, oldValue) {
          scope.showModal(newValue, attrs.$$element);
        }, true);

        //Update the visible value when the dialog is closed through UI actions (Ok, cancel, etc.)
        $(element).bind("hide.bs.modal", function () {
          $parse(attrs.modalShow).assign(scope, false);
          if (!scope.$$phase && !scope.$root.$$phase)
              scope.$apply();
        });
    }

};
});
//Plan Summary Accordian: End
indPortalApp.directive("btstAccordion", function () {
    return {
        restrict: "E",
        transclude: true,
        replace: true,
        scope: {},
        templateUrl:"accordionTemplate2.html",
        link: function (scope, element, attrs) {

            // give this element a unique id
            var id = element.attr("id");
            if (!id) {
                id = "btst-acc" + scope.$id;
                element.attr("id", id);
            }

            // set data-parent on accordion-toggle elements
            var arr = element.find(".accordion-toggle");
            for (var i = 0; i < arr.length; i++) {
                $(arr[i]).attr("data-parent", "#" + id);
                $(arr[i]).attr("href", "#" + id + "collapse" + i);
            }
            arr = element.find(".accordion-body");
            $(arr[0]).addClass("in"); // expand first pane
            for (var i = 0; i < arr.length; i++) {
                $(arr[i]).attr("id", id + "collapse" + i);
            }
        },
        controller: function () {}
    };
});

indPortalApp.directive('btstPane', function () {
    return {
        require: "^btstAccordion",
        restrict: "E",
        transclude: true,
        replace: true,
        scope: {
            ngtitle: "@",
            category: "=",
            order: "="
        },
        templateUrl:
            "accordionTemplate.html",
        link: function (scope, element, attrs) {
            scope.$watch("title", function () {
                // NOTE: this requires jQuery (jQLite won't do html)
                var hdr = element.find(".accordion-toggle");
                hdr.html(scope.ngtitle);
            });
        }
    };
});
// Plan Summary Accordian: End



//enrollments collapse
indPortalApp.directive("collapseIconUpdate", function () {
    return {
        restrict: "A",
        link : function(scope, element, attrs) {

        	element.bind('click', function(){
        		if(element.hasClass("icon-chevron-sign-down")){
            		element.removeClass("icon-chevron-sign-down").addClass("icon-chevron-sign-up");
            	}else if(element.hasClass("icon-chevron-sign-up")){
            		element.removeClass("icon-chevron-sign-up").addClass("icon-chevron-sign-down");
            	}
        	});
		}
    };
});

//enrollments collapse end

(function(){
	var spinningLoader = function($timeout){
		return {
			restrict: "EA",
			template: '<div id="loaderDiv" ng-show="loadSpinner"><img alt="loading" src="resources/images/loader_gray.gif" class="ajax-loader"/></div>',
		    link: function (scope, elem, attrs, ctrl) {
	          var starting;
	          scope.$watch(attrs.spinningLoader, function(nV, oV){
	            if (nV === true){
	              starting = new Date();
	              scope.loadSpinner = true;
	            } else {
	              var ending = new Date();
	              var timeDiff = ending - starting;
	              if (timeDiff >= 1000){
	                scope.loadSpinner = false;
	              } else {
	                $timeout(function(){
	                  scope.loadSpinner = false;
	                }, (1000 - timeDiff));
	              };
	            };
	         });
	      }
		};
	};


	angular.module('indPortalApp')
	.directive("spinningLoader", spinningLoader);
})();


(function() {
	var numOnly = function() {
		return {
			priority: "200",
			restrict : "A",
			require : "ngModel",
			link : function(scope, ele, attrs, ngM) {
				ele.bind("keydown", function(event) {
					if(event.which === 110 || event.which === 190){
						if(!event.currentTarget.id === "aptcText" || !(event.currentTarget.value[event.currentTarget.value.length -1] !== ".") || (event.currentTarget.value % 1 != 0) ){
							event.preventDefault();
						} else {
							return;
						}
					}

					if (event.which === 96 || event.which === 48) {
						if (event.target.value === "0"){
							event.preventDefault();
						}
					};

					if(event.ctrlKey || event.metaKey) {
						if (event.which === 86 || event.which === 67){
							return;
						}
					}

					if (event.which < 8 || event.which > 57) {
						if (event.which >= 96 && event.which <=105){
							return;
						}
						event.preventDefault();
					} else {
						if (event.shiftKey){
							event.preventDefault();
						}
					}
				});
		  }
		};
    };

	angular.module('indPortalApp')
		.directive("numOnly", numOnly)
})();

(function(){
	var checkMax = function(){
		return {
			restrict : "A",
			require : "ngModel",
			link : function(scope, ele, attrs, ngM) {
				scope.$watch("newAptc", function(nv, ov, scope){
					if (parseFloat(nv) > parseFloat(scope.healthdetails.maxAptc)){
						scope.aptcOverMax = false;
					}
				})
			}
		}
	};

	angular.module('indPortalApp')
		.directive("checkMax", checkMax)
})();



(function(){
	function sepDateValidation(){
		var directive = {
			restrict : 'AE',
			require : 'ngModel',
			link : function(scope, element, attrs, ngModel) {
				var vaidationTypeArr = attrs.sepDateValidation.split('_');
				
				function customDateValidation(value){
					//remove placeholder
					var date = value.replace(/[A-Za-z]/g, '');
					var myScope = scope;
					//remove extra-last-character
					if(date.length === 11){
						date = date.substring(0, date.length - 1);
					}

					if(date.length === 10){
						resetValidation();
						
						var isValid = true;
						
						for(var i = 0; i < vaidationTypeArr.length; i++){
							if(vaidationTypeArr[i] === 'date' && !moment(date,'MM/DD/YYYY').isValid()){
								isValid = 'date';
								break;
							}else if(vaidationTypeArr[i] === 'future' && moment().diff(date, 'day') > 0){
								isValid = 'future';
								break;
							}else if(vaidationTypeArr[i] === 'withinPast60' && !(moment().isSame(date, 'day') || moment().diff(date, 'day') < 60 && moment().diff(date, 'day') > 0)){
								isValid = 'withinPast60';
								break;
							}else if(vaidationTypeArr[i] === 'sameCoverageYear' && parseInt(date.substring(6,10)) !== scope.maCurrentCoverageYear){
								isValid = 'sameCoverageYear';
								break;
							}else if(vaidationTypeArr[i] === 'eligibilityStartDate' && moment(date, "MM/DD/YYYY").isAfter(moment(scope.overrideEligibilityStartDateData.enrollmentEndDate, "MM/DD/YYYY"))){
								isValid = 'eligibilityStartDate';
								break;
							}else if(vaidationTypeArr[i] === 'firstDate' && date.substring(3,5) !== '01'){
								isValid = 'firstDate';
								break;
							}
						}
							
						return isValid;
					}else{
						return true;
					}
					
				}
				
				function resetValidation(){
					vaidationTypeArr.forEach(function(item, index){
						ngModel.$setValidity(item, true);	
					});
				}
				
				ngModel.$parsers.unshift(function(value){
					if(value === undefined || value === null){
						return;
					}
					var dateValidationType = customDateValidation(value);
					if(dateValidationType !== true){
						ngModel.$setValidity(dateValidationType, false);
					}
					
					return value;	
				});
				
				 ngModel.$formatters.unshift(function(value) {
					if(value === undefined || value === null){
						return;
					}
					
					var dateValidationType = customDateValidation(value);
					if(dateValidationType !== true){
						ngModel.$setValidity(dateValidationType, false);
					}
					 
					return value;
	             });
				
				
			}
		};
	
		return directive;
	}
	
	angular.module('indPortalApp').directive("sepDateValidation", sepDateValidation)
})();


(function(){	
	angular.module('indPortalApp')
	.directive("datePicker", datePicker);

	function datePicker(){
		var directive = {
			restrict : 'AE',
			link : function(scope, element, attrs) {						
				element.find('.icon-calendar').click(function(){
					element.datepicker({
						autoclose: true,
						orientation: "bottom"
					});
					element.find('input').off('click focus');
				});
			}
		};
		
		return directive;
	}
})();
