<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<sql:query dataSource="jdbc/ghixDS" var="result1">
select date1, count(date1) AS total
from(
select 
to_char(TRUNC ( cast(CREATION_TIMESTAMP as date)) + ( ROUND ( (cast(CREATION_TIMESTAMP as date) - TRUNC (cast(CREATION_TIMESTAMP as date))) * 288 ) / 288 ), 'DD HH24:MI') as date1
FROM ELIG_LEAD
where CREATION_TIMESTAMP > sysdate - 90 and AFFILIATE_ID = (select max(id) from aff_affiliate where upper(COMPANY_NAME) = 'BANK RATE')
order by CREATION_TIMESTAMP
)
GROUP BY DATE1
order by date1 
</sql:query> 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Bank Rate - Traffic - Last 90 Days</title>
  
  <style>
  table#table1 {
  	border: 1px solid #666;
  	width: 80%;
  	margin: 20px 0 20px 0 !important;
  	font-size: 14px;
  }
  
  th,td {
  	padding: 2px 4px 2px 4px !important;
  	text-align: left;
  	vertical-align: top;	
  }
  
  thead tr {
  	background-color: #fc0;
  }
  
  th.sorted {
  	background-color: orange;
  }
  
  th a,th a:visited {
  	color: black;
  }
  
  th a:hover {
  	text-decoration: underline;
  	color: black;
  }
  
  th.sorted a,th.sortable a {
  	background-position: right;
  	display: block;
  	width: 100%;
  }
  
  tr.odd {
  	background-color: #fff
  }
  
  tr.tableRowEven,tr.even {
  	background-color: #fea
  }
  
  div.exportlinks {
  	background-color: #eee;
  	border: 1px dotted #999;
  	padding: 2px 4px 2px 4px;
  	margin: 2px 0 10px 0;
  	width: 79%;
  }
  
  span.export {
  	padding: 0 4px 1px 20px;
  	display: inline;
  	display: inline-block;
  	cursor: pointer;
  }
  
  span.pagebanner {
  	background-color: #eee;
  	border: 1px dotted #999;
  	padding: 2px 4px 2px 4px;
  	width: 79%;
  	margin-top: 10px;
  	display: block;
  	border-bottom: none;
  }
  
  span.pagelinks {
  	background-color: #eee;
  	border: 1px dotted #999;
  	padding: 2px 4px 2px 4px;
  	width: 79%;
  	display: block;
  	border-top: none;
  	margin-bottom: -5px;
  }
  
  
  .group-1 {
      font-weight:bold;
      padding-bottom:10px;
      border-top:1px solid black;
  }
  .group-2 {
      font-style:italic;
      border-top: 1px solid black;
  
  }
  .subtotal-sum, .grandtotal-sum {
      font-weight:bold;
      text-align:right;
  }
  .subtotal-header {
      padding-bottom: 0px;
      border-top: 1px solid white;
  }
  .subtotal-label, .grandtotal-label {
      border-top: 1px solid white;
      font-weight: bold;
  }
  .grouped-table tr.even {
      background-color: #fff;
  }
  .grouped-table tr.odd {
      background-color: #fff;
  }
  .grandtotal-row {
      border-top: 2px solid black;
  }
  
  </style>

  
  <script type="text/javascript" src="http://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load('visualization', '1', {packages: ['corechart']});

    
          function drawVisualization() {
            // Create and populate the data table.
            var data = google.visualization.arrayToDataTable([
              ['State',   'Requests'],        
		<c:forEach var="row" items="${result1.rows}">
		['<c:out value="${row.date1}"/>',<c:out value="${row.total}"/>],
		</c:forEach>
            ]);
          
            // Create and draw the visualization.
            new google.visualization.LineChart(document.getElementById('visualization')).
                draw(data, {curveType: "function",
                            width: 1200, height: 400,
                            vAxis: {maxValue: 10}}
                    );
      }
    

    google.setOnLoadCallback(drawVisualization);
  </script>
</head>
<body style="font-family: Arial;border: 0 none;">
<b>Daily Traffic (Last 90 Days)</b>
<div id="visualization"></div>
<br><br>
<b>Daily Traffic (Last 90 Days)</b>
<br>

<display:table name="${result1.rows}"  requestURI="showDataTop" id="table1" export="true" >
	<display:column property="date1" title="Date" sortable="false" />
	<display:column property="total" title="Total" sortable="false" />
</display:table>

</body>
</html>

