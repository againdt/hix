package com.getinsured.hix.indportal.dto;

/**
 * @author Sunil Desu
 * @see HIX-40418
 * 
 */
public class IndPortalApplicantDetails {

	private long personId;
	private String name;
	private String applicantGuid;
	private Boolean applyingForCoverage;
	private Boolean tobaccoUser;
	private Boolean hardshipExemption;
	private Boolean enrolledInCessProgram;
	private String caseNumber;
	private String exemptionNumber;
	private boolean enableTobacco;
	private boolean nativeAmericanFlag;

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApplicantGuid() {
		return applicantGuid;
	}

	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}

	public Boolean getApplyingForCoverage() {
		return applyingForCoverage;
	}

	public void setApplyingForCoverage(Boolean applyingForCoverage) {
		this.applyingForCoverage = applyingForCoverage;
	}

	public Boolean getTobaccoUser() {
		return tobaccoUser;
	}

	public void setTobaccoUser(Boolean tobaccoUser) {
		this.tobaccoUser = tobaccoUser;
	}

	public Boolean getHardshipExemption() {
		return hardshipExemption;
	}

	public void setHardshipExemption(Boolean hardshipExemption) {
		this.hardshipExemption = hardshipExemption;
	}

	public Boolean getEnrolledInCessProgram() {
		return enrolledInCessProgram;
	}

	public void setEnrolledInCessProgram(Boolean enrolledInCessProgram) {
		this.enrolledInCessProgram = enrolledInCessProgram;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getExemptionNumber() {
		return exemptionNumber;
	}

	public void setExemptionNumber(String exemptionNumber) {
		this.exemptionNumber = exemptionNumber;
	}
	
	

	public boolean isEnableTobacco() {
		return enableTobacco;
	}

	public void setEnableTobacco(boolean enableTobacco) {
		this.enableTobacco = enableTobacco;
	}

	public boolean isNativeAmericanFlag() {
		return nativeAmericanFlag;
	}

	public void setNativeAmericanFlag(boolean nativeAmericanFlag) {
		this.nativeAmericanFlag = nativeAmericanFlag;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IndPortalApplicantDetails [personId=");
		builder.append(personId);
		builder.append(", name=");
		builder.append(name);
		builder.append(", applicantGuid=");
		builder.append(applicantGuid);
		builder.append(", applyingForCoverage=");
		builder.append(applyingForCoverage);
		builder.append(", tobaccoUser=");
		builder.append(tobaccoUser);
		builder.append(", hardshipExemption=");
		builder.append(hardshipExemption);
		builder.append(", enrolledInCessProgram=");
		builder.append(enrolledInCessProgram);
		builder.append(", caseNumber=");
		builder.append(caseNumber);
		builder.append(", exemptionNumber=");
		builder.append(exemptionNumber);
		builder.append("]");
		return builder.toString();
	}
}
