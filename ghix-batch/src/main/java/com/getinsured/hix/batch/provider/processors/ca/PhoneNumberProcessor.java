package com.getinsured.hix.batch.provider.processors.ca;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class PhoneNumberProcessor implements ProviderDataFieldProcessor {

	private ValidationContext context;
	private int index;

	@Override
	public Object process(Object objToBeValidated)
			throws ProviderValidationException, InvalidOperationException {
		String phoneNumberData = objToBeValidated.toString();
		if(StringUtils.isNotBlank(phoneNumberData)){
			phoneNumberData = phoneNumberData.replaceAll("\\|", ",");
			phoneNumberData = phoneNumberData.replaceAll("~", ",");

		}
		String[] phoneNumberArray = phoneNumberData.split(",");
		List<String> providerPhoneNumber = new ArrayList<String>(Arrays.asList(phoneNumberArray));
		Object tmp = this.context.getContextField("output_field");
		String outputField = (tmp == null)? null: (String)tmp;;
		if(outputField == null){
			//Not available in metadata file, create a default one
			this.context.addContextInfo("output_field", "phoneNumber");
			outputField = "phoneNumber";
		}
		if(providerPhoneNumber.size() > 0){
			context.addContextInfo(outputField, providerPhoneNumber);
		}
		return objToBeValidated;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
