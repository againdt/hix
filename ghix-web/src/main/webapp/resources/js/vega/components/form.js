﻿define(function (required) {
    var form = new baseComponent();
    form.type = "form";
    form.getHTML = function (cgfObj) {
        var defCfg = {
            editable: true
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;
        var $form = $("<form novalidate></form>");
        $form.attr("method", compData.method);
        $form.attr("name", compData.name);
        $form.attr("id", compData.name);
        $form.attr("action", compData.url);
        if (compData.onSubmit) {
            $form.attr("ng-submit", compData.onSubmit);
        };
        $form.addClass(compData.css).addClass("k-content");
        if (compData.colRatio != undefined) {
            var rationArray = compData.colRatio.split(":");
            var totoRatio = 0;
            for (var r = 0; r < rationArray.length; r++) {
                rationArray[r] = Number(rationArray[r]);
                totoRatio += rationArray[r];
            };
            var minSize = Math.ceil(12 / totoRatio);
        };
        for (var i = 0; i < compData.fields.length; i++) {
            var comp = compData.fields[i];
            if ($.isArray(comp)) {
                var $retobj = $("<div class=\"row-fluid\"></div>");
                var spanDiv = Math.round(12 / (comp.length));                
                
                for (var a = 0; a < comp.length; a++) {
                    if (compData.colRatio != undefined) {

                        spanDiv = Math.ceil(minSize * Number(rationArray[a]));
                    };
                    var thisComp = $.extend({}, comp[a]);
                    if (spanDiv < 1) { spanDiv = 1 };
                    var $span = $("<div class=\"span" + spanDiv + "\"></div>");
                    $retobj.append($span);
                    thisComp.editable = compData.editable;
                    thisComp.colRation = compData.colRation;
                    var $newField = this.context._getRawHTML(thisComp);
                    $newField = this.replaceValues(compData, $newField);
                    $span.append($newField);

                    $form.append($retobj);
                };
            } else {
                compData.fields[i].editable = compData.editable;
                compData.fields[i].colRation = compData.colRation;
                var $newField = this.context._getRawHTML(compData.fields[i]);
                $newField = this.replaceValues(compData, $newField);
                $form.append($newField);
            };            
        };
        var frmStr = $form[0].outerHTML;
        return frmStr
    };
    form.replaceValues = function (compData, str) {
        str = String(str).replace(/##formName##/ig, compData.name);
        return str;
    };
    return form;
});