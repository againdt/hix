package com.getinsured.eligibility.ssap.integration.income.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;

@Component(value = "ifsvRuleServiceActivator")
public class IncomeRuleServiceActivator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IncomeRuleServiceActivator.class);
	
	@ServiceActivator
	public Message<Map<String, String>> receive(Message<String> message) {
		String ifsvResponse = message.getPayload();
		LOGGER.debug(" ifsvRuleServiceActivator payload: " + message.getPayload());
		String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.SSAP_JSON_HEADER).toString();
		Map<String, String> incomeRulePayload = new HashMap<String, String>();
		incomeRulePayload.put("ssapJson", ssapJSON);
		if(null != ifsvResponse && ifsvResponse.contains("ApplicantVerification")) {
			incomeRulePayload.put("hubResponse", ifsvResponse);
		} else {
			LOGGER.debug(" Since IFSV is not invoked , setting hubResponse to null in the incomeRulePayload" );
			incomeRulePayload.put("hubResponse", null);
		}
		Message<Map<String, String>> ruleMessage = MessageBuilder.withPayload(incomeRulePayload).setHeader("Content-Type", "application/x-www-form-urlencoded").build();
		return ruleMessage;
	}

}
