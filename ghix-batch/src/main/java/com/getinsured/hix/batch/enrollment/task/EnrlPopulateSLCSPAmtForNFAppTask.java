package com.getinsured.hix.batch.enrollment.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrlPopulateSLCSPAmtForNFAppService;

public class EnrlPopulateSLCSPAmtForNFAppTask implements Tasklet {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlPopulateSLCSPAmtForNFAppTask.class);
	
	private String ssapApplicationIds;
	private EnrlPopulateSLCSPAmtForNFAppService enrlPopulateSLCSPAmtForNFAppService;
	private String coverageYear;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		try {
			if (coverageYear != null) {
				if (null != ssapApplicationIds && !ssapApplicationIds.isEmpty()) {
					List<Long> ssapApplicationIdList = new ArrayList<Long>();
					if (ssapApplicationIds.split("\\|").length > 0 && ssapApplicationIds.matches("[0-9|]*")) {
						String[] ssapApplicationIdsArray = ssapApplicationIds.split("\\|");
						for (String ssapApplicationId : ssapApplicationIdsArray) {
							if (NumberUtils.isDigits(ssapApplicationId.trim())) {
								ssapApplicationIdList.add(Long.valueOf(ssapApplicationId.trim()));
							}
						}
						enrlPopulateSLCSPAmtForNFAppService.processSLCSPAmountForNFApp(ssapApplicationIdList, coverageYear);
					}
				} else {
					LOGGER.info("No ssapApplicationId's been passed as input parameter");
					enrlPopulateSLCSPAmtForNFAppService.processSLCSPAmountForNFApp(coverageYear);
				}
			} else {
				throw new Exception("CoverageYear Not Been Passes in the input: ");
			}
		} catch (Exception e) {
			LOGGER.error("Error occured in processing SLCSP Amount for NonFinancial Applications : " + e.getMessage());
			throw new Exception("Error occured in EnrlPopulateSLSCPAmtJob : " + e.getMessage());
		}
		return RepeatStatus.FINISHED;
	}

	public String getSsapApplicationIds() {
		return ssapApplicationIds;
	}

	public void setSsapApplicationIds(String ssapApplicationIds) {
		this.ssapApplicationIds = ssapApplicationIds;
	}

	public EnrlPopulateSLCSPAmtForNFAppService getEnrlPopulateSLCSPAmtForNFAppService() {
		return enrlPopulateSLCSPAmtForNFAppService;
	}

	public void setEnrlPopulateSLCSPAmtForNFAppService(
			EnrlPopulateSLCSPAmtForNFAppService enrlPopulateSLCSPAmtForNFAppService) {
		this.enrlPopulateSLCSPAmtForNFAppService = enrlPopulateSLCSPAmtForNFAppService;
	}

	public String getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}

}
