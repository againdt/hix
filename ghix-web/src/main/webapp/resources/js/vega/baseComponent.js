﻿var baseComponent = function () {
    this._events = [];
};
baseComponent.prototype = {
    type: null,
    _events: [],
    events: {},
    context: null,
    defaultJSON: {
        editable:true
    },
    preInit:function()
    {

    },
    init:function()
    {

    },
    getHTML:function(obj)
    {

    },
    on:function(event,data,callBack)
    {
        this._events.push({
            event: event,
            data: data,
            callBack: callBack
        });
    },
    off:function(event,callBack)
    {
        var eveObj = {
            event: event,
            callBack: callBack
        };
        eveObj.callBack = String(eveObj.callBack);
        eveObj = JSON.stringify(eveObj);
        for (var i = 0; i < this._events.length ; i++) {
            var aObj = {
                event: this._events[i].event,
                callBack: this._events[i].callBack
            };
            aObj.callBack = String(aObj.callBack);
            aObj = JSON.stringify(aObj);
            if (eveObj == aObj) {
                this._events.splice(i, 1);
            }
        }
    },
    dispach:function(event,retObj)
    {
        for (var e = 0; e < this._events.length; e++) {
            var eveObj = this._events[e];
            if (eveObj.event == event) {
                retObj.data = eveObj.data;
                eveObj.callBack(retObj);
            };
        };
    },
    check: function (event,evData) {
        var ret = true;
        for (var e = 0; e < this._events.length; e++) {
            var eveObj = this._events[e];
            if (eveObj.event == event) {
                var retObj = {};
                retObj.data = eveObj.data;
                retObj.evData = evData;
                var cRet = eveObj.callBack(retObj);
                if (cRet == false) {
                    ret = false;
                };
            };
        };
        return ret;
    },
    registerEvent: function (eventType) {
        if (String(eventType) in this.events) {
            log("event '" + eventType + "' already exist...");
            return;
        } else {
            this.events[String(eventType)] = String(eventType);
        }

    }
};