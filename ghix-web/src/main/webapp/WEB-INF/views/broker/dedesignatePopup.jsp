<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<html>
	<body>
	<c:set var="encryptedId" ><encryptor:enc value="${brokerId}" isurl="true"/> </c:set>
	<c:url value="/broker/dedesignate/${encryptedId}" var="theUrl">  
	 
	</c:url>
		<div id="header" class="gray"></div>
		<div id="dedesignateBox">
			<form class="form-vertical" id="dedesignateForm" name="dedesignateForm" method="POST" target="_parent">
	
				<input type="hidden" name="brokerId" id="brokerId" value="<encryptor:enc value="${brokerId}"/>" />
				<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}" />				
				<div class="profile">
					<div id="confirmationMessage">
						<h4 class="hide jsConfirmHeading">
						<spring:message  code="label.brkconfirmmessage1"/> ${brokerName} <spring:message  code="label.brkconfirmmessage2"/>
						</h4><br/>
						<p>
						<spring:message  code="label.brkconfirm1"/> ${brokerName} <%-- <spring:message  code="label.brkconfirm2"/> --%>
						</p><br/>
					</div>
						<input name="submitFromLightbox" id="submitFromLightbox" type="button" onClick="dedesignateBroker();" value="<spring:message  code='label.brkConfirm'/>" title="<spring:message  code='label.brkConfirm'/>" class="btn btn-primary pull-right" />					
						<input type="button"  class="btn pull-left" data-dismiss="modal" onClick="window.parent.closeLightboxOnCancel();" value="<spring:message  code='label.brkCancel'/>" title="<spring:message  code='label.brkCancel'/>" />	

				</div>
			</form>	
		</div>
	</body>
</html>


<script type="text/javascript">	

$(function (){
	var confirmHeading = $(".jsConfirmHeading").text();
	  var headerMessage = '<h4>' + confirmHeading + '<button type="button" class="modalClose close" aria-hidden="true">x</button></h4>';
	 window.parent.$(".jsValidateDedesignateModal").find(".modal-header").html(headerMessage);
	});
	
	
var href = '<c:out value="${theUrl}"/>';
	function dedesignateBroker() 
	{	
		var dataObj = { 'csrftoken': $('#tokid').val() }; 
		var xhr=new XMLHttpRequest();
		$.ajax({
	        type: "POST",
	        data : dataObj ,
	        url: href,
	        success: function(response){
	        	if(isInvalidCSRFToken(xhr))                                  
                    return; 
	        	if(response == 'success')
        		{       	
	        		window.parent.parent.location.reload();     
        		}	        	
	        },	        
		});
	}		

	function isInvalidCSRFToken(xhr) {
	       var rv = false;
	       if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
	              alert($('Session is invalid').text());
	              rv = true;
	       }
	       return rv;
	}
</script>
