package com.getinsured.ssap.model.hub.vlp.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model for case status response object from the HUB.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-04
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CaseStatusResponse {
  /*
  TODO: Follow up with Abhai about invalid JSON response and misspelled fields.
{
"Crteated":2019-05-28 18:30:29.703,
"CaseStatus":"RESUBMIT_SAVIS",
"CaseNumber":"6000060015509OC",
"LastUpdated":"2019-05-28 18:30:29.703",
"PartnerName":"SSHIX_NV2DEV",
"EscCode":32,
"Resolution":null
}
 */

  @JsonProperty("CaseStatus")
  private String status;

  @JsonProperty("CaseNumber")
  private String caseNumber;

  @JsonProperty("Crteated")
  private Date createdDate;

  @JsonProperty("LastUpdated")
  private Date lastUpdatedDate;

  @JsonProperty("PartnerName")
  private String partnerName;

  @JsonProperty("EscCode")
  private int escCode;

  @JsonProperty("Resolution")
  private String resolution;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getCaseNumber() {
    return caseNumber;
  }

  public void setCaseNumber(String caseNumber) {
    this.caseNumber = caseNumber;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setLastUpdatedDate(Date lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getPartnerName() {
    return partnerName;
  }

  public void setPartnerName(String partnerName) {
    this.partnerName = partnerName;
  }

  public int getEscCode() {
    return escCode;
  }

  public void setEscCode(int escCode) {
    this.escCode = escCode;
  }

  public String getResolution() {
    return resolution;
  }

  public void setResolution(String resolution) {
    this.resolution = resolution;
  }

  @Override
  public String toString() {
    return "CaseStatusResponse{" +
        "status='" + status + '\'' +
        ", caseNumber='" + caseNumber + '\'' +
        ", createdDate=" + createdDate +
        ", lastUpdatedDate=" + lastUpdatedDate +
        ", partnerName='" + partnerName + '\'' +
        ", escCode=" + escCode +
        ", resolution='" + resolution + '\'' +
        '}';
  }
}
