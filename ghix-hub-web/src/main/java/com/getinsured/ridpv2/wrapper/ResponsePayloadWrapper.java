package com.getinsured.ridpv2.wrapper;


import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ResponseCodeType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ResponsePayloadType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.VerificationResponseType;
import com.getinsured.ridpv2.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.TextType;

public class ResponsePayloadWrapper implements JSONAware {
	
	private String responseDescription;
	private String responseCode;
	private String sessionIdentifier = null;
	private VerificationResponseWrapper vrWrapper;

	public ResponsePayloadWrapper(ResponsePayloadType response){
		ResponseMetadataType metadata = response.getResponseMetadata();
		if(metadata != null){
			ResponseCodeType responseCodeType = metadata.getResponseCode();
			if(responseCodeType != null){
				this.setResponseCode(responseCodeType.getValue());
			}
			TextType responseDesc = metadata.getResponseDescriptionText();
			if(responseDesc != null){
				this.setResponseDescription(responseDesc.getValue());
			}
		}
		VerificationResponseType vr = response.getVerificationResponse();
		if(vr != null){
			this.vrWrapper = new VerificationResponseWrapper(vr);
			this.sessionIdentifier = vrWrapper.getSessionIdentification();
		}
	}

	private void setResponseDescription(String value) {
		this.responseDescription = value;
		
	}

	private void setResponseCode(String value) {
		this.responseCode = value;
		
	}

	public String getResponseDescription() {
		return responseDescription;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public String getSessionIdentifier() {
		return sessionIdentifier;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("responseCode", this.responseCode);
		obj.put("responseDescription", this.responseDescription);
		obj.put("verificationResponse", this.vrWrapper);
		return obj.toJSONString();
	}

}
