$(document).ready(function() {

	$("#ms").removeAttr("checked");
	$("#nod").removeAttr("checked");
	$("#lomec").removeAttr("checked");
	$("#incar").removeAttr("checked");
	$("#fifth").removeAttr("checked");
	$("#sixth").removeAttr("checked");
	$("#income").removeAttr("checked");
	$("#eighth").removeAttr("checked");
	$("#other").removeAttr("checked");
	$("#mpAccount").removeAttr("checked");
	$("#incarcerated").removeAttr("checked");
	$("#noLongerIncarcerated").removeAttr("checked");
	
  $("#ms").click(function(){
	
	  if (this.checked == true){
	     $('#msDiv').show();  
	    } else {
	    	$('#msDiv').hide();
	    }
	  
	  if(this.checked == true || $('#nod').checked == true){
		  $('#DepInfoTable').show();
		  $('#ditP').show();
	  }
	  else{
		  $('#DepInfoTable').hide();
		  $('#ditP').hide();
	  }
    
  });
  
  $("#nod").click(function(){
	  if (this.checked == true){
	     $('#nodDiv').show();
	    } else {
	    	$('#nodDiv').hide();
	    }
	  
	  if(this.checked == true || $('#ms').checked == true){
		  $('#DepInfoTable').show();
		  $('#ditP').show();
	  }
	  else{
		  $('#DepInfoTable').hide();
		  $('#ditP').hide();
	  }
    
    
  });
  
  $("#lomec").click(function(){
	  if (this.checked == true){
	     $('#lomecDiv').show();
	    } else {
	    	$('#lomecDiv').hide();
	    }
    
  });
  
  $("#incar").click(function(){
	  if (this.checked == true){
	     $('#incarDiv').show();
	    } else {
	    	$('#incarDiv').hide();
	    }
    
  });
  
  $("#fifth").click(function(){
	  if (this.checked == true){
	     $('#lpDiv').show();
	     $('#worksheetP').show();
	 	$('#DepInfoTable3').show();
	     
	    } else {
	    	$('#lpDiv').hide();
	    	$('#worksheetP').hide();
		 	$('#DepInfoTable3').hide();
	    }
    
  });
  
  $("#sixth").click(function(){
	  if (this.checked == true){
	     $('#cirmaDiv').show();
	    } else {
	    	$('#cirmaDiv').hide();
	    }
    
  });
  
  $("#income").click(function(){
	  if (this.checked == true){
	     $('#incomeDiv').show();
	     $('#DepInfoTable4').show();
	     $('#incomeP').show();
	    } else {
	    	$('#incomeDiv').hide();
	    	$('#DepInfoTable4').hide();
		     $('#incomeP').hide();
	    }
    
  });
  
  $("#eighth").click(function(){
	  if (this.checked == true){
	     $('#gomecDiv').show();
	     $('#hcP').show();
	     $('#DepInfoTable2').show();
	    } else {
	    	$('#gomecDiv').hide();
	    	$('#hcP').hide();
	    	$('#DepInfoTable2').hide();
	    }
    
  });
  
  $("#other").click(function(){
	  if (this.checked == true){
	     $('#otherDiv').show();
	    } else {
	    	$('#otherDiv').hide();
	    }
    
  });
  
});

function viewLifeChangeEvent(){
	
	$('#loginAndReg').hide();
	$('#'+currentPage).hide();
	$('#saveBtn').hide();
	$('#contBtn').hide();
	$('#prevBtn').hide();
	$('#ssapHeader').hide();
	$('#lifeChangeEventHeader').show();
	$('#lifeChangeEventMainDIV').show();
	
}



window.ParsleyConfig = $.extend( true, {}, window.ParsleyConfig, { 
    errors: {
        container: function ( elem ) {
        	var $container = (elem).parents('.controls').find('.errorMessage'),
        		$errorForMultipleDivHideShow = (elem).closest('.controls').find('.errorMessage'),
        		$containerGroup= (elem).parents('.control-group').find('.errorMessage'),
        		$errorForMultipleRadioDivHideShow = (elem).parents('.control-group').find('div.errorMessage');
        		
        	if($(elem).parent('div').hasClass('date-picker')){        		
        		//for date picker validation
        		return $container;
        	}else if($(elem).parents('div').hasClass('controls')){
        		if($(elem).closest('div').attr('data-rel') != "multipleInputs"){        			
            		return $container;
        		}else{        			
        			/*for alimony page*/
        			return $errorForMultipleDivHideShow;
        		}
        		
        	}else{
        		if($(elem).parents('.control-group').find('div.errorMessage').is(':visible')){        			
            		return $errorForMultipleRadioDivHideShow;
        		}else{        			
        			return $containerGroup;
        		}
        		
        	}
        }
    }
});




window.ParsleyConfig = window.ParsleyConfig || {};

(function ($) {
	window.ParsleyConfig = $.extend( true, {}, window.ParsleyConfig, {
	    validators: {
	    	americandate: function () {
		        return {
		          validate: function ( val, elem, self) {
		            if ( !/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/.test( val ) ) {
		              return false;
		            }
		            var parts = val.split(/[.\/-]+/);
		            var day = parseInt(parts[1], 10);
		            var month = parseInt(parts[0], 10);
		            var year = parseInt(parts[2], 10);
		            if ( year == 0 || month == 0 || month > 12 ) {
		              return false;
		            }
		            var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		            if ( year % 400 == 0 || ( year % 100 != 0 && year % 4 == 0 ) ) {
		              monthLength[1] = 29;
		            }
		            return day > 0 && day <= monthLength[month - 1];
		          }
		          , priority: 32
		        }
	     	}
	    }
  	});
}(window.jQuery || window.Zepto));