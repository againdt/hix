package com.getinsured.hix.indportal.dto.dm.application;

public class Name {
	 private String firstName;
	 private String middleName;
	 private String lastName;
	 private String suffix;


	 // Getter Methods 

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getFirstName() {
	  return firstName;
	 }

	 public String getLastName() {
	  return lastName;
	 }

	 // Setter Methods 

	 public void setFirstName(String firstName) {
	  this.firstName = firstName;
	 }

	 public void setLastName(String lastName) {
	  this.lastName = lastName;
	 }
	}
