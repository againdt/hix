package com.getinsured.hix.finance.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.getinsured.hix.dto.enrollment.EnrolleePaymentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;
import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FileBasedConfiguration;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.saml.SamlPaymentProviderService;
import com.getinsured.hix.serff.repository.IIssuerPaymentInfoRepo;
import com.getinsured.hix.util.FinanceRestApiUtils;
import com.getinsured.saml.SAMLResponseBuilder;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;

import gov.ca.calheers.carrierpayment.beans.CarrierPayment;
import gov.ca.calheers.carrierpayment.beans.MemberInformation;
import gov.ca.calheers.carrierpayment.beans.ResponsibleParty;


/**
 * Handles requests for SAML payload request
 * @author Sahoo_s
 * @since 30 April 2014
 */

@Controller
public class PaymentRedirectController {

	private  final Logger LOGGER = LoggerFactory.getLogger(PaymentRedirectController.class);

	private static final int BYTES_DOWNLOAD = 1024;
	private static final String FFM_ENDPOINT_URL = "ffmEndpointURL";
	private static final String RELAY_STATE_URLL = "relayStateUrl";
	private static final String PAYREDIRECT_ERROR_MSG = "payRedirectErrorMsg";
	private String issuer_mock_url;
	private boolean mockEnabled;
	static JAXBContext contextObj = null;
	@Autowired IssuerService issuerService;
	@Autowired IIssuerPaymentInfoRepo paymentInfoRepo;
	
	
	@Autowired private FinanceRestApiUtils financeRestApiUtils;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private Gson platformGson;
	@Autowired private List<SamlPaymentProviderService> issuerPaymentProviders;
	@Autowired private SAMLResponseBuilder samlResponseBuilder;
	@Autowired private IssuerPaymentConfigurationService issuerPaymentConfig;

	private boolean useDefaultKeystoreCreds;
	/**
	 * Create SAML2.0 Assertion request 
	 * 			   for Carrier Premium Redirection
	 * @param finEnrlDataKey
	 *            {@link String}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation
	 */

	@Value("#{configProp['issuer.payment_redirect.useDefaultKeystore'] != null ? configProp['issuer.payment_redirect.useDefaultKeystore'] : 'true'}")
	public void setDefaultCredentials(String defaultCreds) {
		this.useDefaultKeystoreCreds = defaultCreds.trim().equalsIgnoreCase("true");
	}

	@Value("#{configProp['issuer.payment_redirect.mockUrl'] != null ? configProp['issuer.payment_redirect.mockUrl'] : 'http://localhost:8080/mock/paymentRedirect'}")
	public void setMockUrl(String mockUrl) {
		this.issuer_mock_url = mockUrl.trim();
	}

	@Value("#{configProp['issuer.payment_redirect.mockEnabled'] != null ? configProp['issuer.payment_redirect.mockEnabled'] : 'false'}")
	public void setMockEnabled(String mockEnabled) {
		this.mockEnabled = mockEnabled.trim().equalsIgnoreCase("true");
	}
	
	private SamlPaymentProviderService getProviderService(){
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		SamlPaymentProviderService requiredServioce = null;
		for(SamlPaymentProviderService service: issuerPaymentProviders){
			if(service.isApplicable(stateCode, null)){
				requiredServioce = service;
			}
		}
		return requiredServioce;
	}

	@RequestMapping(value = "/finance/paymentRedirect")
	public String paymentRedirectInfo(@RequestParam String finEnrlDataKey, Model model, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("PaymentRedirectController :: Inside getPaymentRedirectInfo method :: Started");
		EnrollmentPaymentDTO  paymentDTO = (EnrollmentPaymentDTO)request.getSession().getAttribute(finEnrlDataKey);
		Issuer issuer = null;
		IssuerPaymentConfiguration issuerConfig = null;
		String  samlResponse = null;
		String issuerAuthUrl = null;
		try {
			if (paymentDTO !=null) {
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				
				if("CA".equals(stateCode)) {
					LOGGER.info("PaymentRedirectController :: calling paymentRedirectCA");
					samlResponse = paymentRedirectCA(paymentDTO);
					model.addAttribute(FFM_ENDPOINT_URL, paymentDTO.getPaymentUrl());
				}else {
					String hiosid = paymentDTO.getHiosIssuerId();
					issuer = this.issuerService.getIssuerByHiosID(hiosid);
					issuerConfig = this.issuerPaymentConfig.getIssuerPaymentInfo(hiosid);
					if(issuerConfig.isSamlSupported()) {
						samlResponse = paymentRedirect(paymentDTO, issuer, issuerConfig);
						String relayStateOrDirectUrl = paymentDTO.getPaymentUrl();
						if(issuerConfig.useRelayState() && issuerConfig.metadataEnabled()) {
							String partnerEntityId = issuerConfig.getEntityId();
							String assertionConsumerUrl = this.samlResponseBuilder.getPaymentAuthURL(partnerEntityId);
							LOGGER.info("Metadata enabled for {} using Relay state {} and redirecting the user to {} for entity id {}", 
									hiosid,
									relayStateOrDirectUrl, 
									assertionConsumerUrl,
									partnerEntityId);
							model.addAttribute(RELAY_STATE_URLL,relayStateOrDirectUrl);
							model.addAttribute(FFM_ENDPOINT_URL, assertionConsumerUrl);
						}else {
							model.addAttribute(FFM_ENDPOINT_URL, relayStateOrDirectUrl);
						}
					}else {
						issuerAuthUrl = issuerConfig.getIssuerAuthURL();
					}
				}
				if(samlResponse != null && samlResponse.length() > 0) {
					model.addAttribute("SAMLResponse", samlResponse);
				}else {
					LOGGER.info("Issuer does not support SAML processing, redirecting the user to {}",issuerAuthUrl);
					response.sendRedirect(issuerAuthUrl);
				}
			}else {
				model.addAttribute(PAYREDIRECT_ERROR_MSG, "IMPORTANT:Unable to process payment request. Please contact Technical Support for assistance.");
			}
		} catch (Exception e) {
			model.addAttribute(PAYREDIRECT_ERROR_MSG, "IMPORTANT:Unable to process payment request. Please contact Technical Support for assistance.");
			LOGGER.info("PaymentRedirectController :: Inside getPaymentRedirectInfo method :: Ended",e);
		}

		return "finance/paymentRedirectUser";
	}
	
	public String paymentRedirect(EnrollmentPaymentDTO  paymentDTO,Issuer issuer, IssuerPaymentConfiguration issuerConfig) throws Exception {
		String  samlResponse = null;
			boolean encryptAssertions = issuerConfig.wantsEncryptedAssertions();
			boolean usingMetadata = issuerConfig.metadataEnabled();
			String partnerEntityId = issuerConfig.getEntityId();
			String exchangeEntityId = FileBasedConfiguration.getConfiguration().getProperty("saml.alias");
			String signatureAlgo = issuerConfig.getSignatureAlgoId();
			String digestAlgo = issuerConfig.getDigestAlgoId();
			LOGGER.info("Encrypt assertions: {}\n, Use Metadata: {}\n, Partner Entity Id: {}\n, Exchange Entity Id: {}\n, Signature Algo {}\n, digest Algo {}",
					encryptAssertions,
					usingMetadata,
					partnerEntityId,
					exchangeEntityId,
					signatureAlgo,
					digestAlgo
					);
			if(usingMetadata) {
				samlResponse = this.samlResponseBuilder.buildSAMLResponseFromSPMetadata(
						encryptAssertions, 
						issuerConfig.getUseHostAsDestination(),
						issuerConfig.getIssuerAuthAddress(),
						issuerConfig.getIssuerAuthDNS(), 
						partnerEntityId, 
						exchangeEntityId, 
						GhixPlatformConstants.PLATFORM_KEY_ALIAS, 
						issuerConfig.getIssuerSupportedClaims(this.issuerPaymentConfig.getPaymentData(paymentDTO)),
						signatureAlgo,
						digestAlgo);
				
			}else {
				samlResponse = this.samlResponseBuilder.buildSAMLResponse(
						encryptAssertions,
						issuerConfig.getIssuerAuthAddress(),
						issuerConfig.getIssuerAuthDNS(), 
						partnerEntityId, 
						issuerConfig.getIssuerAuthURL(), 
						exchangeEntityId, 
						GhixPlatformConstants.PLATFORM_KEY_ALIAS, 
						issuerConfig.getIssuerSupportedClaims(this.issuerPaymentConfig.getPaymentData(paymentDTO)),
						signatureAlgo,
						digestAlgo);
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("******************************* Begin SAML Payload (Not Encoded) *************************************");
				LOGGER.debug(samlResponse);
				LOGGER.debug("*******************************       End SAML Payload           *************************************");
			}
		return Base64.getEncoder().encodeToString(samlResponse.getBytes());
	}
	
	
	public String paymentRedirectCA(EnrollmentPaymentDTO  paymentDTO ) throws Exception {
		String  samlResponse = null;
		SamlPaymentProviderService paymentService = getProviderService();
		if(paymentService == null){
			throw new RuntimeException("Failed to find the payment sertvice");
		}
		CarrierPayment  carrierPayment = getCarrierPaymentForCA(paymentDTO);
		IssuerRequest issuerRequest = new IssuerRequest();
		issuerRequest.setId(paymentDTO.getHiosIssuerId());
		ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_PAYMENT_INFO_BY_HIOSID, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, issuerRequest);
		if(response != null){
			IssuerPaymentInfoResponse issuerPaymentInfoResponse = platformGson.fromJson(response.getBody(), IssuerPaymentInfoResponse.class);
			if(this.mockEnabled) {
				paymentDTO.setPaymentUrl(this.issuer_mock_url);
				issuerPaymentInfoResponse.setIssuerAuthURL(this.issuer_mock_url);
			}
			samlResponse = paymentService.processPaymentRequest(carrierPayment,issuerPaymentInfoResponse);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("******************************* Begin SAML Payload (Not Encoded) *************************************");
				LOGGER.debug(samlResponse);
				LOGGER.debug("*******************************       End SAML Payload           *************************************");
			}
		}
		return samlResponse;
	}

	
	private CarrierPayment getCarrierPaymentForCA(EnrollmentPaymentDTO inputData){
		CarrierPayment carrierPayment = new CarrierPayment();
		carrierPayment.setAPTCAmount( new BigDecimal(inputData.getAptcAmt()).setScale(2, BigDecimal.ROUND_HALF_UP));
		String lang = inputData.getPreferredLanguage();
		String payNowLang = null;
		if(lang == null) {
			lang = "en";
		}
		lang = lang.toLowerCase();
		switch (lang) {
			case "en":
				payNowLang = "en";
				break;
			case "es":
				payNowLang = "es";
				break;
			default:
				payNowLang = "en";
		}
		carrierPayment.setLanguage(payNowLang);
		carrierPayment.setCaseID(inputData.getCaseId());
		carrierPayment.setPaymentTransactionId(inputData.getPaymentTxnId());
		carrierPayment.setPreviousPaymentTransactionId(inputData.getPreviousPaymentTxnId());
		carrierPayment.setProposedCoverageEffectiveDate(stringToXMLGregorianCalendar(DateUtil.dateToString(inputData.getBenefitEffectiveDate(),"MM/dd/yyyy")));
		carrierPayment.setQHPPlanId(inputData.getCmsPlanId());
		carrierPayment.setResidenceCountyCode(inputData.getCountyCode());
		carrierPayment.setResidenceZipCode(inputData.getZip());
		carrierPayment.setSubscriberID(inputData.getExchgSubscriberIdentifier());
		carrierPayment.setTotalPremiumAmount(new BigDecimal(inputData.getGrossPremiumAmt()).setScale(2, BigDecimal.ROUND_HALF_UP));
		carrierPayment.setTotalPremiumOwed(new BigDecimal(inputData.getNetPremiumAmt()).setScale(2, BigDecimal.ROUND_HALF_UP));

		ResponsibleParty responsibleParty = new ResponsibleParty();
		// Name 
		responsibleParty.setFirstName(inputData.getFirstName());
		responsibleParty.setLastName(inputData.getLastName());
		responsibleParty.setMiddleName(inputData.getMiddleName());

		// Email
		responsibleParty.setResponsiblePartyEmailAddress(inputData.getPrefferedEmail());

		// Location
		responsibleParty.setStreetName1(inputData.getAddressLine1());
		responsibleParty.setStreetName2(inputData.getAddressLine2());
		responsibleParty.setCityName(inputData.getCity());
		responsibleParty.setState(inputData.getState());
		responsibleParty.setZipCode(inputData.getZip());

		carrierPayment.setResponsibleParty(responsibleParty);

		// Member Information
		if(!inputData.getEnrolleePaymentDtoList().isEmpty() && inputData.getEnrolleePaymentDtoList().size() > 0){
			List<EnrolleePaymentDTO> enrolleePaymentList = inputData.getEnrolleePaymentDtoList();
			for (EnrolleePaymentDTO enrolleePaymentDTO : enrolleePaymentList) {
				if(enrolleePaymentDTO != null){
					MemberInformation memberInfo = new MemberInformation();
					memberInfo.setFirstName(enrolleePaymentDTO.getFirstName());
					memberInfo.setLastName(enrolleePaymentDTO.getLastName());
					memberInfo.setMiddleName(enrolleePaymentDTO.getMiddleName());
					memberInfo.setRelationshipCode(enrolleePaymentDTO.getRelationshipCode());
					memberInfo.setAge(String.valueOf(enrolleePaymentDTO.getAge()));
					memberInfo.setMemberID(enrolleePaymentDTO.getMemberId());
					memberInfo.setTotalMemberPremiumAmount(getBigDecimalObj(enrolleePaymentDTO.getTotalIndvResponsibilityAmt()));
					carrierPayment.getMemberInformation().add(memberInfo);
				}
			}
		}

		return carrierPayment;
	}
	
	public BigDecimal getBigDecimalObj(Float totalIndvResponsibilityAmt) {
		if(totalIndvResponsibilityAmt == null) {
			return BigDecimal.ZERO;
		}
		return new BigDecimal(totalIndvResponsibilityAmt.toString()).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	private static XMLGregorianCalendar stringToXMLGregorianCalendar(String dateString) {
		XMLGregorianCalendar gregCal=null;
		if (StringUtils.isBlank(dateString)) { 
			return null;
		}else{
			try{
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Date date = simpleDateFormat.parse(dateString);
				GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
				gregorianCalendar.setTime(date);
				gregCal = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gregorianCalendar.get(Calendar.YEAR), gregorianCalendar.get(Calendar.MONTH)+1, gregorianCalendar.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
				return gregCal;
			}catch(ParseException | DatatypeConfigurationException e){
				return null;
			}
		}
	}
	
	/**
	 * Decode the SAML2.0 Assertion response  
	 * and redirect to a dummy carrier page for testing
	 * @param samlResponse
	 *            {@link String}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/finance/ffmEndpointURL")
	public String decodeSAMLOutput(@RequestParam String samlResponse, Model model, HttpServletRequest request) {
		LOGGER.info("PaymentRedirectController :: Inside decodeSAMLOutput method :: Started");
		try {
			if (samlResponse != null && !samlResponse.isEmpty()) {
				String encodedSAMLResponse = samlResponse;
				FinanceResponse financeResponse = financeRestApiUtils.decodeSamlResponse(encodedSAMLResponse);
				EnrollmentPaymentDTO enrollmentPaymentDTO = (EnrollmentPaymentDTO) financeResponse.getDataObject().get(GhixConstants.SAML_RESPONSE);
				model.addAttribute("enrollmentPaymentDTO", enrollmentPaymentDTO);
				String decodedSAMLXml = (String) financeResponse.getDataObject().get(GhixConstants.SAML_RESPONSE_XML);
				request.getSession().setAttribute(GhixConstants.SAML_RESPONSE_XML, decodedSAMLXml);
				LOGGER.info("PaymentRedirectController :: Inside decodeSAMLOutput method :: Ended");
			}
		} catch (Exception e) {
			LOGGER.info("PaymentRedirectController :: Inside decodeSAMLOutput method :: Ended" +e);
		}
		return "finance/dummyCarrierEndpointURL";
	}


	/**
	 * Download the SAML2.0 Assertion request 
	 * after decode the response
	 * @param decodedSAMLXml
	 *            {@link String}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 */
	@RequestMapping(value = "/finance/downloadSAMLPayloadXml")
	public void downloadSAMLPayloadXml(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("PaymentRedirectController :: Inside downloadSAMLPayloadXml method :: Started");
		try {
			String decodedSAMLXml = (String) request.getSession().getAttribute(GhixConstants.SAML_RESPONSE_XML);
			request.getSession().removeAttribute(GhixConstants.SAML_RESPONSE_XML);
			if (decodedSAMLXml != null && !decodedSAMLXml.isEmpty()) {
				//Logic to download the SAML2.0 request XML to client requested machine
				response.setContentType("text/xml");
				response.setHeader("Content-Disposition", "attachment;filename=SamlPayloadRequest.xml");

				InputStream input = new ByteArrayInputStream(decodedSAMLXml.getBytes("UTF8"));

				int read = 0;
				byte[] bytes = new byte[BYTES_DOWNLOAD];
				OutputStream os = response.getOutputStream();

				while ((read = input.read(bytes)) != -1) {
					os.write(bytes, 0, read);
				}
				os.flush();
				os.close();
				LOGGER.info("PaymentRedirectController :: Inside downloadSAMLPayloadXml method :: Ended");
			}
		} catch (Exception e) {
			LOGGER.info("PaymentRedirectController :: Inside downloadSAMLPayloadXml method :: Ended"+e);
		}
	}


	//This block of code written for the Testing purpose
	private static final String FINANCE_ENROLLMENT_HEALTH_KEY = "finEnrlHltDataKey";
	private static final String FINANCE_ENROLLMENT_DENTAL_KEY = "finEnrlDntDataKey";
	@RequestMapping(value = "/finance/loadPayment")
	public String loadPayment(Model model, HttpServletRequest request, HttpServletResponse response) {

		EnrollmentPaymentDTO healthPaymentDTO = new EnrollmentPaymentDTO();
		healthPaymentDTO.setEnrollmentId(2786);
		healthPaymentDTO.setExchgSubscriberIdentifier("1000001637");
		healthPaymentDTO.setCmsPlanId("60597ID016000201");
		healthPaymentDTO.setPaymentTxnId("2786ID12345");
		healthPaymentDTO.setNetPremiumAmt(Float.valueOf("650.0"));
		healthPaymentDTO.setGrossPremiumAmt(Float.valueOf("437.15"));
		healthPaymentDTO.setAptcAmt(Float.valueOf("212.85"));
		healthPaymentDTO.setBenefitEffectiveDate(new TSDate());
		healthPaymentDTO.setFirstName("DRPJHAPPC");
		healthPaymentDTO.setLastName("JEEP");
		healthPaymentDTO.setAddressLine1("450 W State St");
		healthPaymentDTO.setState("NM");
		healthPaymentDTO.setCity("BOISE");
		healthPaymentDTO.setZip("83702");
		healthPaymentDTO.setContactEmail("harry.gilli@mailcatch.com");
		healthPaymentDTO.setEnrolleeNames("Harry,Gilbert;Joe,Stefhen");
		healthPaymentDTO.setPaymentUrl("http://selecthealth.org/ffmpaymenttest/Medical.aspx");
		healthPaymentDTO.setHiosIssuerId("12345");

		EnrollmentPaymentDTO dentalPaymentDTO = new EnrollmentPaymentDTO();
		dentalPaymentDTO.setEnrollmentId(3208);
		dentalPaymentDTO.setExchgSubscriberIdentifier("100012989");
		dentalPaymentDTO.setCmsPlanId("93091NM005000101");
		healthPaymentDTO.setPaymentTxnId("3208ID12345");
		dentalPaymentDTO.setNetPremiumAmt(Float.valueOf("132.34"));
		dentalPaymentDTO.setGrossPremiumAmt(Float.valueOf("200.34"));
		dentalPaymentDTO.setAptcAmt(Float.valueOf("100"));
		dentalPaymentDTO.setBenefitEffectiveDate(new TSDate());
		dentalPaymentDTO.setFirstName("GetInsured");
		dentalPaymentDTO.setLastName("Vimo");
		dentalPaymentDTO.setAddressLine1("Albuquerque");
		dentalPaymentDTO.setState("NM");
		dentalPaymentDTO.setCity("Mountain View");
		dentalPaymentDTO.setZip("123456");
		dentalPaymentDTO.setContactEmail("harry.gilli@mailcatch.com");
		dentalPaymentDTO.setEnrolleeNames("Harry,Gilbert;Joe,Stefhen");
		dentalPaymentDTO.setPaymentUrl("https://prod_saml_sp_ffm.bcidaho.com/bci/saml/sp");
		dentalPaymentDTO.setHiosIssuerId("12345");

		HttpSession session = request.getSession();
		session.setAttribute(FINANCE_ENROLLMENT_HEALTH_KEY, healthPaymentDTO);
		session.setAttribute(FINANCE_ENROLLMENT_DENTAL_KEY, dentalPaymentDTO);

		model.addAttribute(FINANCE_ENROLLMENT_HEALTH_KEY,FINANCE_ENROLLMENT_HEALTH_KEY);
		model.addAttribute(FINANCE_ENROLLMENT_DENTAL_KEY,FINANCE_ENROLLMENT_DENTAL_KEY);

		return "finance/testPaymentRedirectUser";
	}
	
	@RequestMapping(value = "/finance/testPaymentRedirect/{hiosId}")
	@PreAuthorize("hasPermission(#model, 'MANAGE_FINANCIAL_INFORMATION')")
	public String paymentRedirectTest(@PathVariable(value="hiosId") String hiosId, Model model, HttpServletRequest request, HttpServletResponse response) {
		Boolean paymentTestEnabled = FileBasedConfiguration.getConfiguration().getBoolean("issuer.payment.testEnabled", false);
		Issuer issuer = null;
		try {
			if(!paymentTestEnabled) {
				response.sendError(HttpStatus.SC_UNAUTHORIZED);
			}else {
				issuer = this.issuerService.getIssuerByHiosID(hiosId);
				if(issuer == null) {
					model.addAttribute("Message", "No issuer available for  HIOS ID"+hiosId);
					response.sendError(HttpStatus.SC_UNAUTHORIZED);
				}
			}
		} catch (IOException e) {
			LOGGER.error("Error sending error when payment test is not enabled",e);
		}
		LOGGER.info("PaymentRedirectController :: Inside getPaymentRedirectInfo method :: Started");
		EnrollmentPaymentDTO  paymentDTO = this.getEnrollmentDTO(issuer);
		IssuerPaymentConfiguration issuerConfig = null;
		String  samlResponse = null;
		String issuerAuthUrl = null;
		try {
			if (paymentDTO !=null) {
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				
				if("CA".equals(stateCode)) {
					LOGGER.info("PaymentRedirectController :: calling paymentRedirectCA");
					samlResponse = paymentRedirectCA(paymentDTO);
					model.addAttribute(FFM_ENDPOINT_URL, paymentDTO.getPaymentUrl());
				}else {
					String hiosid = paymentDTO.getHiosIssuerId();
					issuerConfig = this.issuerPaymentConfig.getIssuerPaymentInfo(hiosid);
					if(issuerConfig.isSamlSupported()) {
						samlResponse = paymentRedirect(paymentDTO, issuer, issuerConfig);
						String relayStateOrDirectUrl = paymentDTO.getPaymentUrl();
						if(issuerConfig.useRelayState() && issuerConfig.metadataEnabled()) {
							String partnerEntityId = issuerConfig.getEntityId();
							String assertionConsumerUrl = this.samlResponseBuilder.getPaymentAuthURL(partnerEntityId);
							LOGGER.info("Metadata enabled for {} using Relay state {} and redirecting the user to {} for entity id {}", 
									hiosid,
									relayStateOrDirectUrl, 
									assertionConsumerUrl,
									partnerEntityId);
							model.addAttribute(RELAY_STATE_URLL,relayStateOrDirectUrl);
							model.addAttribute(FFM_ENDPOINT_URL, assertionConsumerUrl);
						}else {
							model.addAttribute(FFM_ENDPOINT_URL, relayStateOrDirectUrl);
						}
					}else {
						issuerAuthUrl = issuerConfig.getIssuerAuthURL();
					}
				}
				if(samlResponse != null && samlResponse.length() > 0) {
					model.addAttribute("SAMLResponse", samlResponse);
				}else {
					LOGGER.info("Issuer does not support SAML processing, redirecting the user to {}",issuerAuthUrl);
					response.sendRedirect(issuerAuthUrl);
				}
			}else {
				model.addAttribute(PAYREDIRECT_ERROR_MSG, "IMPORTANT:	. Please contact Technical Support for assistance.");
			}
		} catch (Exception e) {
			model.addAttribute(PAYREDIRECT_ERROR_MSG, "IMPORTANT:Unable to process payment request. Please contact Technical Support for assistance.");
			LOGGER.info("PaymentRedirectController :: Inside getPaymentRedirectInfo method :: Ended",e);
		}

		return "finance/paymentRedirectUser";
	}
	
	private EnrollmentPaymentDTO getEnrollmentDTO(Issuer issuer) {
		EnrollmentPaymentDTO healthPaymentDTO = new EnrollmentPaymentDTO();
		healthPaymentDTO.setEnrollmentId(2786);
		healthPaymentDTO.setExchgSubscriberIdentifier("1000001637");
		healthPaymentDTO.setCmsPlanId("60597ID016000201");
		healthPaymentDTO.setPaymentTxnId("2786ID12345");
		healthPaymentDTO.setNetPremiumAmt(Float.valueOf("650.0"));
		healthPaymentDTO.setGrossPremiumAmt(Float.valueOf("437.15"));
		healthPaymentDTO.setAptcAmt(Float.valueOf("212.85"));
		healthPaymentDTO.setBenefitEffectiveDate(new TSDate());
		healthPaymentDTO.setFirstName("DRPJHAPPC");
		healthPaymentDTO.setLastName("JEEP");
		healthPaymentDTO.setAddressLine1("450 W State St");
		healthPaymentDTO.setState("NM");
		healthPaymentDTO.setCity("BOISE");
		healthPaymentDTO.setZip("83702");
		healthPaymentDTO.setContactEmail("harry.gilli@mailcatch.com");
		healthPaymentDTO.setEnrolleeNames("Harry,Gilbert;Joe,Stefhen");
		healthPaymentDTO.setPaymentUrl(issuer.getPaymentUrl());
		healthPaymentDTO.setHiosIssuerId(issuer.getHiosIssuerId());
		return healthPaymentDTO;
	}

}
