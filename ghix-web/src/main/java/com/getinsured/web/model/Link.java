package com.getinsured.web.model;

/**
 * Describes link for the environment.
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
public class Link
{
  private LinkType type;
  private String url;

  public LinkType getType()
  {
    return type;
  }

  public void setType(final LinkType type)
  {
    this.type = type;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUrl(final String url)
  {
    this.url = url;
  }
}
