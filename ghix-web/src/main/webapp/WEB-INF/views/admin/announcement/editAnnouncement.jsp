<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>


<script type="text/javascript">

 function getCurrentDate(){
	var currentDate = new Date();
    var year = currentDate.getFullYear();
    var currMonth = currentDate.getMonth()+1;
    var month = (currMonth > 9 ) ? currMonth : '0' + currMonth; 
    var day = currentDate.getDate();

    var today = month + '/' + day + '/' + year ;
    
    return today;
 }
 
jQuery.validator.addMethod("effectiveStartDateCheck", function (value, element, param) {
    var effectiveStartDate = $("#effectiveStartDate").val();
    var today = getCurrentDate() ;
    if (new Date(effectiveStartDate) < new Date(today)) {
        return false;
    }
    return true;
});


jQuery.validator.addMethod("effectiveEndDateCheck", function (value, element, param) {
	var effectiveEndDate = $("#effectiveEndDate").val();
    var today = getCurrentDate();
    if (new Date(effectiveEndDate) <= new Date(today)) {
        return false;
    }
    return true;
});

jQuery.validator.addMethod("checkDuration", function (value, element, param) {
	 var effectiveStartDate = new Date($("#effectiveStartDate").val());
	 var effectiveEndDate = new Date($("#effectiveEndDate").val());
	 if(new Date(effectiveStartDate) >= new Date(effectiveEndDate)){
		 return false;
	 }return true;
});

function validateForm(){
	 if( $('#effectiveStartDate').prop('defaultValue') == $('#effectiveStartDate').val() ){
		 $('#effectiveStartDate').addClass("ignore");
	 }else{
		 $('#effectiveStartDate').removeClass("ignore");
	 }
	 
	 if( $('#effectiveEndDate').prop('defaultValue') == $('#effectiveEndDate').val() ){
		 $('#effectiveEndDate').addClass("ignore");
	 }else{
		 $('#effectiveStartDate').removeClass("ignore");
	 }
	 
	 $('#frmeditannouncement').valid();
	 
	 var rolesSelected = false;
	 $(".role").each(function() { 
			var $this = $(this); 
			if($this.is(':checked')){
				rolesSelected=true;
				return false;
			}
	  }); 
	 if (rolesSelected == false){
		 var error = "<label class='error' generated='true'><span><em class='excl'>!</em>Please Select Role.</span></label>";
		 $('#role_error').html(error);
		 return false;
	 }else{
		 $('#role_error').html();
		 return true;
	 }
}


</script>

<div class="gutter10">
		<div class="row-fluid">
            <h1><a name="skip"></a>Edit Announcement</h1>
        </div>

<div class="row-fluid">	
		<div class="span3" id="sidebar">
			<div class="gutter10">
				<h3></h3>
				<p></p>
			</div>
		</div>
		
		<div class="span9" id="rightpanel">
				<div class="header">
					<h4>Edit Announcement</h4>
				</div>	
		 <form class="form-horizontal gutter10" id="frmeditannouncement" name="frmeditannouncement" action="../edit" method="POST">
		 		<input type="hidden" name="id" id="id" value="${id}">
				<div class="control-group">
					<label class="control-label required" for="name" ><spring:message  code="label.announcementName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input class="input-large" type="text"  name="name" id="name" readonly="readonly" value="${announcementObj.name}" size="35">
						<div id="name_error"></div>	
					</div>
				</div>
				<div class="control-group">
					<label for="LastName" class="control-label required"><spring:message  code="label.text"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<textarea name="announcementText" id="announcementText" class="xlarge" rows="2" cols="20" size="5">${announcementObj.announcementText}</textarea>
						<div id="announcementText_error"  class="help-inline"></div>
					</div>
				</div>
				<div class="control-group">
					<fmt:formatDate pattern="MM/dd/yyyy" value="${announcementObj.effectiveStartDate}" var="effectiveStartDate"/>
					<label for="effectiveStartDate" class="required control-label" for=""><spring:message  code="label.effectiveStartDate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input title="MM/dd/yyyy" class="datepick input-small"  name="effectiveStartDate" id="effectiveStartDate" value="<c:out value="${effectiveStartDate}"/>" />
							<div id="effectiveStartDate_error" class="help-inline"></div>
						</div> <!-- end of controls-->
				</div>
				<div class="control-group">
					<fmt:formatDate pattern="MM/dd/yyyy" value="${announcementObj.effectiveEndDate}" var="effectiveEndDate"/>
					<label for="effectiveEndDate" class="required control-label" for=""><spring:message  code="label.effectiveEndDate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input title="MM/dd/yyyy" class="datepick input-small" name="effectiveEndDate" id="effectiveEndDate" value="<c:out value="${effectiveEndDate}"/>" />
							<div id="effectiveEndDate_error" class="help-inline"></div>
						</div> <!-- end of controls-->
				</div>
				<div class="control-group">
					<label for="status" class="control-label required"><spring:message  code="label.status"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label> 
					<div class="controls">
						<select  name="status" id="status" class="input-medium">
						    <option ${announcementObj.status == 'PENDING' ? 'selected="selected"' : '' } value="PENDING">PENDING</option>
						    <option ${announcementObj.status == 'APPROVED' ? 'selected="selected"' : '' } value="APPROVED">APPROVED</option>
							<option ${announcementObj.status == 'DELETED' ? 'selected="selected"' : '' } value="DELETED">DELETED</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label for="status" class="control-label required"><spring:message  code="label.roles"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
					<c:set var="counter" value="0" />  
					<c:forEach var="role" items="${roles}"> 
						<input type="hidden" name="announcementRole[${counter}].announcement.id" id="hiddenAnnouncementRole" value="${id}" />
						<label class="checkbox inline">
						<input type="checkbox" class="role"  ${fn:contains(rolelist, role.id) ? 'checked="checked"' : '' } name="announcementRole[${counter}].role.id" id="role" value="${role.id}">&nbsp;${role.name}&nbsp;&nbsp;&nbsp;
						</label>
						<c:set var="counter" value="${counter+1}" />  
					</c:forEach>
					<div id="role_error"></div>
					</div>
				</div>
				<div class="form-actions">
					<input type="submit" name="submitButton" id="submitButton" onclick="return validateForm();" value="<spring:message  code="label.submit"/>" class="btn btn-primary" title="<spring:message  code="label.submit"/>"/>			
				</div>
		</form>
</div>
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
	
					<p>The prototype showcases three scenarios (A, B and C) dependant on
						a particular Employer's eligibility to use the SHOP Exchange.</p>
				</div>
			</div>
		</div>

	</div> <!-- row-fluid -->
</div>
<script type="text/javascript">
$(function() {
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			minDate: '0',
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
});


var validator = $("#frmeditannouncement").validate({ 
	ignore: ".ignore",
	onfocusout: function(){},
	onkeyup: false,
	rules : { 'name':{required : true},
			  'announcementText':{required : true},
			  'effectiveStartDate':{required : true, date: true, checkDuration : true, effectiveStartDateCheck : true},
			  'effectiveEndDate':{required : true, date: true, checkDuration : true, effectiveEndDateCheck : true}
	},
	messages : {
			'name' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateName' javaScriptEscape='true'/></span>"},
			'announcementText' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAnnouncementText' javaScriptEscape='true'/></span>"},
			'effectiveStartDate' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEffectiveStartDate' javaScriptEscape='true'/></span>",
				effectiveStartDateCheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateStartDateVal' javaScriptEscape='true'/></span>" ,
				checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.validateDurationStart' javaScriptEscape='true'/></span>",
				date : "<span> <em class='excl'>!</em><spring:message code='label.validateDateFormat' javaScriptEscape='true'/></span>"				
			},
			'effectiveEndDate' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEffectiveEndDate' javaScriptEscape='true'/></span>",
				effectiveEndDateCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEndDateVal' javaScriptEscape='true'/></span>", 
				checkDuration : "<span> <em class='excl'>!</em><spring:message code='label.validateDurationEnd' javaScriptEscape='true'/></span>",
				date : "<span><em class='excl'>!</em><spring:message code='label.validateDateFormat' javaScriptEscape='true'/></span>",
		 	}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});
</script>