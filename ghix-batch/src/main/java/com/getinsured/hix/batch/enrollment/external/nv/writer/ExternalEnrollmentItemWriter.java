package com.getinsured.hix.batch.enrollment.external.nv.writer;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.external.nv.dto.HouseholdEnrollments;
import com.getinsured.hix.batch.enrollment.service.EnrlPopulateExtEnrollmentService;

public class ExternalEnrollmentItemWriter implements ItemWriter<HouseholdEnrollments> {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalEnrollmentItemWriter.class);

	private EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService;
	//private StepContext stepContext;
	private String fileName;

	private String fileId;
	
    public void write(List<? extends HouseholdEnrollments> items) throws Exception { 
    	LOGGER.debug("ExternalEnrollmentItemWriter starts"); 
        List<String> errorList = new ArrayList<>();
        Integer fileIdInt = Integer.parseInt(fileId);
        for (HouseholdEnrollments item : items) { 
        	if(item.getEnrollments() != null && item.getEnrollments().size()>0)
        	{
        		enrlPopulateExtEnrollmentService.loadAndSaveExternalEnrollment(item.getEnrollments(), fileIdInt, errorList);
        	}
    		if (item.getHasErrors() || errorList.size()>0)
    		{
    			updateErrorFile(Arrays.asList(item.getJsonLine()));
    			if(item.getParsingErrors().size()>0)
    			{
    				updateErrorDescFile(item.getParsingErrors());
    			}
    			if(errorList.size()>0)
    			{
    				updateErrorDescFile(errorList);
    			}
    			errorList.clear();
    		}
        } 
        LOGGER.debug("ExternalEnrollmentItemWriter ends"); 
    }

    private void updateErrorDescFile(List<String> errorList ) {
		try {
		    Path path = Paths.get(fileName);
		    String absolutePath = path.getParent().toString()+File.separator+fileId+"_failedRecords_descr.csv";
		    path = Paths.get(absolutePath);
		    Files.write(path, errorList, StandardCharsets.UTF_8,
		        Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final Exception e) {
			LOGGER.error("Error while writing the data for errorDescrFile : ",e);
			LOGGER.error("--- Errorlist print starts ---");
			for(String error : errorList)
			{
				LOGGER.error(error);
			}
			LOGGER.error("--- Errorlist print ends ---");
		}
	}
    
	private void updateErrorFile(List<String> errorList ) {
		try {
		    Path path = Paths.get(fileName);
		    String absolutePath = path.getParent().toString()+File.separator+fileId+"_failedRecords.json";
		    path = Paths.get(absolutePath);
		    Files.write(path, errorList, StandardCharsets.UTF_8,
		        Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (final Exception e) {
			LOGGER.error("Error while writing the data for errorFile : ",e);
			LOGGER.error("--- Errorlist print starts ---");
			for(String error : errorList)
			{
				LOGGER.error(error);
			}
			LOGGER.error("--- Errorlist print ends ---");
		}
	} 


	public void setEnrlPopulateExtEnrollmentService(EnrlPopulateExtEnrollmentService enrlPopulateExtEnrollmentService) {
		this.enrlPopulateExtEnrollmentService = enrlPopulateExtEnrollmentService;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
