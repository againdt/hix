<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<%
	String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
%>
<c:set var="stateExchangeType" value="<%=stateExchangeType%>" />


<div class="row-fluid" role="main">
	<div class="landing-signup">
		<h1><spring:message code="label.homePage.headerCopy"/></h1>
		<div class="row-fluid" role="main">
			<div class="span8">
				
				<c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'individual'.equalsIgnoreCase(stateExchangeType)}">
					<div class="register-group">
						<c:set var="individualRoleName" ><encryptor:enc value="individual" isurl="true"/> </c:set>
						<div class="icons-family"></div>
						<p><span><spring:message code="label.homePage.individual"/></span> <spring:message code="label.homePage.individualCopy"/></p>
						<a class="btn pull-right" href="<c:url value='/preeligibility' />"><i class="icon-large icon-arrow-right"></i></a>
					</div>
				</c:if>
				
				<c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'shop'.equalsIgnoreCase(stateExchangeType)}">
					<div class="register-group"> 
						<c:set var="employerRoleName" ><encryptor:enc value="employer" isurl="true"/> </c:set>
						<div class="icons-employer"></div>
						<p><span><spring:message code="label.homePage.employer"/></span> <spring:message code="label.homePage.employerCopy"/></p>
						<a class="btn pull-right" href="<c:url value="/account/signup/${employerRoleName}" />"><i class="icon-large icon-arrow-right"></i></a>
					</div>
					
					<div class="register-group">
						<c:set var="brokerRoleName" ><encryptor:enc value="broker" isurl="true"/> </c:set>
						<div class="icons-agent"></div>
						<p><span><spring:message code="label.homePage.agent"/></span> <spring:message code="label.homePage.agentCopy"/></p>
						<a class="btn pull-right" href="<c:url value="/account/signup/${brokerRoleName}" />"><i class="icon-large icon-arrow-right"></i></a>
					</div>
				</c:if>
				
				<c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'individual'.equalsIgnoreCase(stateExchangeType)}">
					<div class="register-group">
						<c:set var="assisterRoleName" ><encryptor:enc value="assisterenrollmententity" isurl="true"/> </c:set>
						<div class="icons-entity"></div>
						<p><span><spring:message code="label.homePage.entity"/></span> <spring:message code="label.homePage.entityCopy"/></p>
						<a class="btn pull-right" href="<c:url value="/account/signup/${assisterRoleName}"/>"><i class="icon-large icon-arrow-right"></i></a>
					</div>
				</c:if>
				
			</div>
	 	 	<div class="span4">
	 	 		<div class="register-signin">
	 	 			<p><i class="icon-lock"></i> <spring:message code="label.homePage.haveAccountQuestion"/> <a href="<c:url value='/account/user/login'/>"><spring:message code="label.homePage.haveAccountAction"/></a></p>
	 	 		</div>
	 	 	</div>
		</div>		
	</div>				
</div>
