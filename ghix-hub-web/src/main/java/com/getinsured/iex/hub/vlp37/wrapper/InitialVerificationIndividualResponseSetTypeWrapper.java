package com.getinsured.iex.hub.vlp37.wrapper;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifIndividualResponseSetType;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationIndividualResponseSetType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpsda.ArrayOfSponsorshipDataType;

/**
 * Wrapper class for gov.hhs.cms.dsh.sim.ee.vlp.DHSResponseFields
 * 
 * @author Nikhil Talreja
 * @since 10-Feb-2014
 * 
 */
public class InitialVerificationIndividualResponseSetTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InitialVerificationIndividualResponseSetTypeWrapper.class);
	
	private String caseNumber;
	private String nonCitLastName;
	private String nonCitFirstName;
	private String nonCitMiddleName;
	private String nonCitBirthDate;
	private String nonCitEntryDate;
	private String nonCitAdmittedToDate;
	private String nonCitAdmittedToText;
	private String nonCitCountryBirthCd;
	private String nonCitCountryCitCd;
	private String nonCitCoaCode;
	private String nonCitEadsExpireDate;
	private int eligStatementCd;
	private String eligStatementTxt;
	private String iavTypeCode;
	private String iavTypeTxt;
	private String webServSftwrVer;
	private String grantDate;
	private String grantDateReasonCd;
	private boolean sponsorDataFoundIndicator;
	private ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData;
	private String sponsorshipReasonCd;
	private String fiveYearBarApplyCode;
	private String qualifiedNonCitizenCode;
	private String fiveYearBarMetCode;
	private String usCitizenCode;
	
    private String admittedToDate;
    private String admittedToText;
    private String nonCitProvOfLaw;
    private String agencyAction;

    
    public InitialVerificationIndividualResponseSetTypeWrapper(InitialVerificationIndividualResponseSetType initVerifIndividualResponseSetType){
    	
    	if (initVerifIndividualResponseSetType == null){
    		return;
    	}
    	
    	this.caseNumber = initVerifIndividualResponseSetType.getCaseNumber();
    	this.nonCitLastName = initVerifIndividualResponseSetType.getNonCitLastName();
    	this.nonCitFirstName = initVerifIndividualResponseSetType.getNonCitFirstName();
    	this.nonCitMiddleName = initVerifIndividualResponseSetType.getNonCitLastName();
    	try{
	    	if(initVerifIndividualResponseSetType.getNonCitBirthDate() != null){
	    		this.nonCitBirthDate = VLPServiceUtil.xmlDateToString(initVerifIndividualResponseSetType.getNonCitBirthDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(initVerifIndividualResponseSetType.getNonCitEntryDate() != null){
	    		this.nonCitEntryDate = VLPServiceUtil.xmlDateToString(initVerifIndividualResponseSetType.getNonCitEntryDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(initVerifIndividualResponseSetType.getAdmittedToDate() != null){
	    		this.admittedToDate = VLPServiceUtil.xmlDateToString(initVerifIndividualResponseSetType.getAdmittedToDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(initVerifIndividualResponseSetType.getNonCitEadsExpireDate() != null){
	    		this.nonCitEadsExpireDate = VLPServiceUtil.xmlDateToString(initVerifIndividualResponseSetType.getNonCitEadsExpireDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(initVerifIndividualResponseSetType.getGrantDate() != null){
	    		this.grantDate = VLPServiceUtil.xmlDateToString(initVerifIndividualResponseSetType.getGrantDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
    	}
    	catch(Exception e){
    		LOGGER.error("Exception occured while setting date",e);
    	}


    	this.admittedToText = initVerifIndividualResponseSetType.getAdmittedToText();
    	this.nonCitCountryBirthCd = initVerifIndividualResponseSetType.getNonCitCountryBirthCd();
    	this.nonCitCountryCitCd = initVerifIndividualResponseSetType.getNonCitCountryCitCd();
    	this.nonCitCoaCode = initVerifIndividualResponseSetType.getNonCitCoaCode();
    	this.eligStatementCd = initVerifIndividualResponseSetType.getEligStatementCd().intValue();
    	this.eligStatementTxt = initVerifIndividualResponseSetType.getEligStatementTxt();
    	this.iavTypeCode = initVerifIndividualResponseSetType.getIAVTypeCode();
    	this.iavTypeTxt = initVerifIndividualResponseSetType.getIAVTypeTxt();
    	this.webServSftwrVer = initVerifIndividualResponseSetType.getWebServSftwrVer();
    	this.grantDateReasonCd = initVerifIndividualResponseSetType.getGrantDateReasonCd();
    	this.sponsorDataFoundIndicator = initVerifIndividualResponseSetType.isSponsorDataFoundIndicator() == null ? false : initVerifIndividualResponseSetType.isSponsorDataFoundIndicator();
    	
    	this.arrayOfSponsorshipData = new ArrayOfSponsorshipDataTypeWrapper(initVerifIndividualResponseSetType.getArrayOfSponsorshipData());
    	
    	this.sponsorshipReasonCd = initVerifIndividualResponseSetType.getSponsorshipReasonCd();
    	this.nonCitProvOfLaw = initVerifIndividualResponseSetType.getNonCitProvOfLaw();
    	this.agencyAction = initVerifIndividualResponseSetType.getAgencyAction();
    	this.fiveYearBarApplyCode = initVerifIndividualResponseSetType.getFiveYearBarApplyCode();
    	this.qualifiedNonCitizenCode = initVerifIndividualResponseSetType.getQualifiedNonCitizenCode();
    	this.fiveYearBarMetCode = initVerifIndividualResponseSetType.getFiveYearBarMetCode();
    	this.usCitizenCode = initVerifIndividualResponseSetType.getUSCitizenCode();
    }
    
	
    
	public String getCaseNumber() {
		return caseNumber;
	}



	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}



	public String getNonCitLastName() {
		return nonCitLastName;
	}



	public void setNonCitLastName(String nonCitLastName) {
		this.nonCitLastName = nonCitLastName;
	}



	public String getNonCitFirstName() {
		return nonCitFirstName;
	}



	public void setNonCitFirstName(String nonCitFirstName) {
		this.nonCitFirstName = nonCitFirstName;
	}



	public String getNonCitMiddleName() {
		return nonCitMiddleName;
	}



	public void setNonCitMiddleName(String nonCitMiddleName) {
		this.nonCitMiddleName = nonCitMiddleName;
	}



	public String getNonCitBirthDate() {
		return nonCitBirthDate;
	}



	public void setNonCitBirthDate(String nonCitBirthDate) {
		this.nonCitBirthDate = nonCitBirthDate;
	}



	public String getNonCitEntryDate() {
		return nonCitEntryDate;
	}



	public void setNonCitEntryDate(String nonCitEntryDate) {
		this.nonCitEntryDate = nonCitEntryDate;
	}



	public String getNonCitAdmittedToDate() {
		return nonCitAdmittedToDate;
	}



	public void setNonCitAdmittedToDate(String nonCitAdmittedToDate) {
		this.nonCitAdmittedToDate = nonCitAdmittedToDate;
	}



	public String getNonCitAdmittedToText() {
		return nonCitAdmittedToText;
	}



	public void setNonCitAdmittedToText(String nonCitAdmittedToText) {
		this.nonCitAdmittedToText = nonCitAdmittedToText;
	}



	public String getNonCitCountryBirthCd() {
		return nonCitCountryBirthCd;
	}



	public void setNonCitCountryBirthCd(String nonCitCountryBirthCd) {
		this.nonCitCountryBirthCd = nonCitCountryBirthCd;
	}



	public String getNonCitCountryCitCd() {
		return nonCitCountryCitCd;
	}



	public void setNonCitCountryCitCd(String nonCitCountryCitCd) {
		this.nonCitCountryCitCd = nonCitCountryCitCd;
	}



	public String getNonCitCoaCode() {
		return nonCitCoaCode;
	}



	public void setNonCitCoaCode(String nonCitCoaCode) {
		this.nonCitCoaCode = nonCitCoaCode;
	}



	public String getNonCitEadsExpireDate() {
		return nonCitEadsExpireDate;
	}



	public void setNonCitEadsExpireDate(String nonCitEadsExpireDate) {
		this.nonCitEadsExpireDate = nonCitEadsExpireDate;
	}



	public int getEligStatementCd() {
		return eligStatementCd;
	}



	public void setEligStatementCd(int eligStatementCd) {
		this.eligStatementCd = eligStatementCd;
	}



	public String getEligStatementTxt() {
		return eligStatementTxt;
	}



	public void setEligStatementTxt(String eligStatementTxt) {
		this.eligStatementTxt = eligStatementTxt;
	}



	public String getIavTypeCode() {
		return iavTypeCode;
	}



	public void setIavTypeCode(String iavTypeCode) {
		this.iavTypeCode = iavTypeCode;
	}



	public String getIavTypeTxt() {
		return iavTypeTxt;
	}



	public void setIavTypeTxt(String iavTypeTxt) {
		this.iavTypeTxt = iavTypeTxt;
	}



	public String getWebServSftwrVer() {
		return webServSftwrVer;
	}



	public void setWebServSftwrVer(String webServSftwrVer) {
		this.webServSftwrVer = webServSftwrVer;
	}



	public String getGrantDate() {
		return grantDate;
	}



	public void setGrantDate(String grantDate) {
		this.grantDate = grantDate;
	}



	public String getGrantDateReasonCd() {
		return grantDateReasonCd;
	}



	public void setGrantDateReasonCd(String grantDateReasonCd) {
		this.grantDateReasonCd = grantDateReasonCd;
	}



	public boolean isSponsorDataFoundIndicator() {
		return sponsorDataFoundIndicator;
	}



	public void setSponsorDataFoundIndicator(boolean sponsorDataFoundIndicator) {
		this.sponsorDataFoundIndicator = sponsorDataFoundIndicator;
	}



	public ArrayOfSponsorshipDataTypeWrapper getArrayOfSponsorshipData() {
		return arrayOfSponsorshipData;
	}



	public void setArrayOfSponsorshipData(
			ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData) {
		this.arrayOfSponsorshipData = arrayOfSponsorshipData;
	}



	public String getSponsorshipReasonCd() {
		return sponsorshipReasonCd;
	}



	public void setSponsorshipReasonCd(String sponsorshipReasonCd) {
		this.sponsorshipReasonCd = sponsorshipReasonCd;
	}


	public String getFiveYearBarApplyCode() {
		return fiveYearBarApplyCode;
	}



	public void setFiveYearBarApplyCode(String fiveYearBarApplyCode) {
		this.fiveYearBarApplyCode = fiveYearBarApplyCode;
	}



	public String getQualifiedNonCitizenCode() {
		return qualifiedNonCitizenCode;
	}



	public void setQualifiedNonCitizenCode(String qualifiedNonCitizenCode) {
		this.qualifiedNonCitizenCode = qualifiedNonCitizenCode;
	}



	public String getFiveYearBarMetCode() {
		return fiveYearBarMetCode;
	}



	public void setFiveYearBarMetCode(String fiveYearBarMetCode) {
		this.fiveYearBarMetCode = fiveYearBarMetCode;
	}



	public String getUsCitizenCode() {
		return usCitizenCode;
	}



	public void setUsCitizenCode(String usCitizenCode) {
		this.usCitizenCode = usCitizenCode;
	}



	public String getAdmittedToDate() {
		return admittedToDate;
	}



	public void setAdmittedToDate(String admittedToDate) {
		this.admittedToDate = admittedToDate;
	}



	public String getAdmittedToText() {
		return admittedToText;
	}



	public void setAdmittedToText(String admittedToText) {
		this.admittedToText = admittedToText;
	}



	public String getNonCitProvOfLaw() {
		return nonCitProvOfLaw;
	}



	public void setNonCitProvOfLaw(String nonCitProvOfLaw) {
		this.nonCitProvOfLaw = nonCitProvOfLaw;
	}



	public String getAgencyAction() {
		return agencyAction;
	}



	public void setAgencyAction(String agencyAction) {
		this.agencyAction = agencyAction;
	}



	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.caseNumber != null){
			obj.put("CaseNumber",this.caseNumber);
		}
		
		if(this.nonCitLastName != null){
			obj.put("NonCitLastName",this.nonCitLastName);
		}
		
		if(this.nonCitFirstName != null){
			obj.put("NonCitFirstName",this.nonCitFirstName);
		}
		
		if(this.nonCitMiddleName != null){
			obj.put("NonCitMiddleName",this.nonCitMiddleName);
		}
		
		if(this.nonCitBirthDate != null){
			obj.put("NonCitBirthDate",this.nonCitBirthDate);
		}
		
		if(this.nonCitEntryDate != null){
			obj.put("NonCitEntryDate",this.nonCitEntryDate);
		}
		
		if(this.nonCitAdmittedToDate != null){
			obj.put("NonCitAdmittedToDate",this.nonCitAdmittedToDate);
		}
		
		if(this.nonCitAdmittedToText != null){
			obj.put("NonCitAdmittedToText",this.nonCitAdmittedToText);
		}
		
		if(this.nonCitCountryBirthCd != null){
			obj.put("NonCitCountryBirthCd",this.nonCitCountryBirthCd);
		}
		
		if(this.nonCitCountryCitCd != null){
			obj.put("NonCitCountryCitCd",this.nonCitCountryCitCd);
		}
		
		if(this.nonCitCoaCode != null){
			obj.put("NonCitCoaCode",this.nonCitCoaCode);
		}
		
		if(this.nonCitEadsExpireDate != null){
			obj.put("NonCitEadsExpireDate",this.nonCitEadsExpireDate);
		}
		
		obj.put("EligStatementCd",this.eligStatementCd);
		
		if(this.eligStatementTxt != null){
			obj.put("EligStatementTxt",this.eligStatementTxt);
		}
		
		if(this.iavTypeCode != null){
			obj.put("IAVTypeCode",this.iavTypeCode);
		}
		
		if(this.iavTypeTxt != null){
			obj.put("IAVTypeTxt",this.iavTypeTxt);
		}
		
		if(this.webServSftwrVer != null){
			obj.put("WebServSftwrVer",this.webServSftwrVer);
		}
		
		if(this.grantDate != null){
			obj.put("GrantDate",this.grantDate);
		}
		
		if(this.grantDateReasonCd != null){
			obj.put("GrantDateReasonCd",this.grantDateReasonCd);
		}
		
		obj.put("SponsorDataFoundIndicator",this.sponsorDataFoundIndicator);
		
		if(this.arrayOfSponsorshipData != null){
			obj.put("ArrayOfSponsorshipData",this.arrayOfSponsorshipData);
		}
		
		if(this.sponsorshipReasonCd != null){
			obj.put("SponsorshipReasonCd",this.sponsorshipReasonCd);
		}
		
		if(this.fiveYearBarApplyCode != null){
			obj.put("FiveYearBarApplyCode",this.fiveYearBarApplyCode);
		}
		
		if(this.qualifiedNonCitizenCode != null){
			obj.put("QualifiedNonCitizenCode",this.qualifiedNonCitizenCode);
		}
		
		if(this.fiveYearBarMetCode != null){
			obj.put("FiveYearBarMetCode",this.fiveYearBarMetCode);
		}
		
		if(this.usCitizenCode != null){
			obj.put("USCitizenCode",this.usCitizenCode);
		}
		
		if(admittedToDate != null){
			obj.put("AdmittedToDate", admittedToDate);
		}
		
		if(this.admittedToText != null){
			obj.put("AdmittedToText", this.admittedToText);
		}
		
		if(this.nonCitProvOfLaw != null){
			obj.put("NonCitProvOfLaw", this.nonCitProvOfLaw);
		}
		
		if(this.agencyAction != null){
			obj.put("AgencyAction", this.agencyAction);
		}

		return obj.toJSONString();
	}

}
