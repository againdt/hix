export const defaultValuesObject = {
  MondayStart: 'select',
  MondayEnd: 'select',
  closed_Monday: false,
  TuesdayStart: 'select',
  TuesdayEnd: 'select',
  closed_Tuesday: false,
  WednesdayStart: 'select',
  WednesdayEnd: 'select',
  closed_Wednesday: false,
  ThursdayStart: 'select',
  ThursdayEnd: 'select',
  closed_Thursday: false,
  FridayStart: 'select',
  FridayEnd: 'select',
  closed_Friday: false,
  SaturdayStart: 'select',
  SaturdayEnd: 'select',
  closed_Saturday: false,
  SundayStart: 'select',
  SundayEnd: 'select',
  closed_Sunday: false,
  apply_to_all: false
};

export const weekdays = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday'
];

export const days = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday'
];

export const TERM_AND_CONDITIONS_LINK =
  'https://www.nevadahealthlink.com/brokerconnect';
