<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<script src="//maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<style type="text/css">
#detail {
    padding-top: 0 !important;
	background: #fff;
}
.border-b {
	border-bottom: 1px solid #E5E5E5;
}
.border-t {
	border-top: 1px solid #E5E5E5;
}
.container {
	margin-top: -40px;
	padding: 0 13px;
	width: 97%;
}
#profile-info h1 {margin-top: 0;}
#profile-info dl {margin: 10px 0 0;}
#profile-info dt, #profile-info dd {line-height: 24px;}
	
#profile-info dl dt{margin-right: 3px; width: auto; font-weight: normal;}
#profile-info dd {font-weight: bold;}
#profile-info .dl-horizontal dd {margin-left: 15px;}
address {margin-bottom: 0;}
</style>

<script type="text/javascript">
function insertData(){
	var idvalue ='${physician.id}';
	var addId= '${physicianAddressObj.id}';
	var name = '${physician.first_name}${physician.last_name}';
	var qual = '${physician.education}';
	var speca = '${specialties}';
	var line1 = '${physicianAddressObj.address1}';
	var city = '${physicianAddressObj.city}';
	var state = '${physicianAddressObj.state}';
	var zipcode = '${physicianAddressObj.zip}';
	var contactNum ='${physicianAddressObj.phoneNumber}';
	var email = '${physician.emailAddress}';
	var physicianTypeVal ='${physicianType}';
	var jsondata = {id: idvalue ,name: name,qualification: qual,specialties: speca ,contactNumber: contactNum ,email: email ,address:[{id:addId,line1:line1,zip:zipcode,city:city,state:state}]};

	if(physicianTypeVal == 'DOCTOR'){
		
		var existingData = []; 
		if(parent.document.getElementById("doctors").value!=""){
			existingData = JSON.parse(parent.document.getElementById("doctors").value);
		}
		existingData.push(jsondata);
		parent.document.getElementById("doctors").value = JSON.stringify(existingData); 
		window.parent.displayProviders("doctors");
	
	}else if (physicianTypeVal == 'DENTIST') {
		
		var existingData = []; 
		if(parent.document.getElementById("dentists").value!=""){
			existingData = JSON.parse(parent.document.getElementById("dentists").value);
		}
		existingData.push(jsondata);
		parent.document.getElementById("dentists").value = JSON.stringify(existingData); 
		window.parent.displayProviders("dentists");
	}
	
	$('div').remove('.modal bigModal in');
	window.parent.closeModal();
 }
</script>
<script type="text/javascript">

 $(document).ready(function(){
    var locations = ${mapString};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(locations[0][0],locations[0][1]),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker;
	for (i = 0; i <= locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][0],locations[i][1]),
        map: map    
      });
      marker.set("type", "point");
      marker.set("id", 1);
    };
}); 

</script>
<body id="detail">
    <div class="gutter10">
    	<div class="row-fluid">
    		<a class="btn" href="javascript:history.go(-1)"><spring:message code='label.back'/></a>
    		<a class="btn btn-primary" href="/hix/provider/searchproviders?domElement=${physicianType}&zipcode=${defaultZip}"><spring:message code='label.searchAgain'/></a>
        </div>
    </div>
    <div class="gutter10-lr">
    	<form class="form-horizontal" id="frmdetail" name="frmdetail" action="/plandisplay/preferences"  method="POST">
        	<div class="row-fluid border-t">
        	<div class="gutter10 span6">
            	<div id="profile-info" class="row-fluid">
                	<h1>${physician.first_name} ${physician.last_name }</h1>
    				<div class="doc-info">
    					
    					<address>
    					<c:forEach var="physicianAddresses" items="${physician.physicianAddress}">
    						${physicianAddresses.address1}, ${physicianAddresses.state} ${physicianAddresses.zip}<br>
    					<abbr title="Phone">P:</abbr> ${physicianAddresses.phoneNumber}
    					</c:forEach>
    					<br><a href="mailto:#">${physician.emailAddress}</a></address>
    				</div>
    				<dl class="dl-horizontal">
    					<dt><spring:message code='label.specialities'/></dt>
    						<c:forEach var="physicianSpeciality" items="${physician.physicianSpecialRelation}">
    						<dd>${physicianSpeciality.specialty.name}</dd>
    						</c:forEach>
    					<dt><spring:message code='label.boardCertified'/></dt>
    					<dd>${physician.boardCertification}</dd>
                    </dl>
                    <dl class="dl-horizontal">
    					<dt><spring:message code='label.languagesWithColan'/></dt>
    					<dd>${physician.languages}</dd>
    					<dt><spring:message code='label.medicalSchool'/> </dt>
    					<dd>${physician.education}</dd>
    				</dl>
                </div><!--end profile-info-->
            </div> 
            <div class="span5 gutter10-tb">
    			<div id="map" style="width: 220px; height: 205px;"></div>
    		</div>
           	</div><!--end row-fluid-->
            <div class="row-fluid gutter10-tb border-t">
            	<a class="btn btn-primary" href="#" onclick="insertData();"><spring:message code='label.addToList'/></a>
            </div>
        </form>
    </div>
    <!--footer-->
</body>