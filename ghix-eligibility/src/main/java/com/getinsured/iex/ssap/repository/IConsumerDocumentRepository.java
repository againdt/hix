package com.getinsured.iex.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import com.getinsured.hix.model.ConsumerDocument;

@RestResource(path="cmrDocument")
public interface IConsumerDocumentRepository extends JpaRepository<ConsumerDocument, Long> {

	List<ConsumerDocument> findByTargetNameAndTargetId(@Param("targetName") String targetName,@Param("targetId") Long targetId);

}
