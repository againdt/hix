<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <title>API Docs</title>
  <!-- needed for mobile devices -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
   <% 
     String yamlFile = (String)request.getParameter("yaml");
   %>
    
    
  <c:set scope="request" var="yamlFile" value="<%=yamlFile%>" />
    <c:choose>
	    <c:when test="${not empty yamlFile}">
  <redoc spec-url="../../ghix-tkm/resources/docs/${yamlFile}"></redoc>
  <script src="https://rebilly.github.io/ReDoc/releases/latest/redoc.min.js"></script>
	    </c:when>
	    <c:otherwise>
	    	<div style="margin: 20px">
	    		<h1> Ticket Management API Documentation </h1><br>
		    	<a class="btn btn-primary btn-lg" href="docs?yaml=tkm_qle.yaml">Ticket Management APIs</a>
			</div>
  		</c:otherwise>
    </c:choose>
    
    
	
	
	
</body>
</html>
