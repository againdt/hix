 <%@ include file="../include.jsp" %>
  <div id="plans" style="display:none">
<div style="margin:2%">
<!--Start Tabmenu-->
  <div class="Tabmenu">
    <ul>
       <li><a href="#">Carriers List <img src="images/plansImages/tabmenu_arrow.jpg"></a>
       	<ul id="carrierList">
		       	
		</ul>     
       </li>
       <li><a href="#">Plan Type <img src="images/plansImages/tabmenu_arrow.jpg"></a>
            <ul id="planType">
		       	
			</ul>
       </li>
       <li><a href="#">Monthly Price<img src="images/plansImages/tabmenu_arrow.jpg"></a>
       	<ul id="monthlyPricing">
		       	<li onclick="sendPlanRequest('Not Sure', 'monthly_price')">Not Sure</li>
				<li onclick="sendPlanRequest('Under-$300', 'monthly_price')">Under-$300</li>
				<li onclick="sendPlanRequest('$300-$600', 'monthly_price')">$300-$600</li>
				<li onclick="sendPlanRequest('$600-$900', 'monthly_price')">$600-$900</li>
				<li onclick="sendPlanRequest('$900-$1200', 'monthly_price')">$900-$1200</li>
				<li onclick="sendPlanRequest('$1200-$1500', 'monthly_price')">$1200-$1500</li>
				<li onclick="sendPlanRequest('$1500-$1800', 'monthly_price')">$1500-$1800</li>
				<li onclick="sendPlanRequest('$1800-$2100', 'monthly_price')">$1800-$2100</li>
				<li onclick="sendPlanRequest('$2100-$2400', 'monthly_price')">$2100-$2400</li>
				<li onclick="sendPlanRequest('$2400-$2700', 'monthly_price')">$2400-$2700</li>
				<li onclick="sendPlanRequest('$2700-$3000', 'monthly_price')">$2700-$3000</li>
			</ul>   
       </li>
       <li><a href="#">Metal Tier<img src="images/plansImages/tabmenu_arrow.jpg"></a>
       		<ul id="metalLier">
		       	
			</ul>
       
       </li>
       <li style="background:none"><a href="#">Sort Plan<img src="images/plansImages/tabmenu_arrow.jpg"></a>
       <ul id="sort">
		       	<li onclick="sendPlanRequest('Low-High', 'sort')">Low-High</li>
				<li onclick="sendPlanRequest('High-Low', 'sort')">High-Low</li>
			</ul>
       </li>
    </ul>
  </div>
 <!--End Tabmenu--> 
 <div id="pageNumberDiv" style=" text-align: center; "></div>
 <!--Start Carrier-Details-->
 <div class="Carrier-Details">
  <ul>
    <li>Monthly Premium</li>
    <li>Carrier Details</li>       
    <li>Plan Details</li>                   
    <li>Annual Deductibles</li>           
    <li>Est.Out of Pocket Costs</li>
    <div class="right">
      <img src="images/plansImages/left_arrow.jpg" onclick="javascript:previosResults()">
      <img src="images/plansImages/right_arrow.jpg" onclick="javascript:nextResults()">
      </div>
  </ul>
 </div>
 <!--end Carrier-Details-->
 <div id="plansResultDiv">
 
 </div>
 
 <div id="fullDetailDiv" style="display:none;">
 
 </div>
 
</div>

<div id="waitingPlan" class="waitingPlan" style='display:none;'>
	<div class="waitingPlan-inner">
				<br>
				<span id="messageDiv87"> Processing please wait &nbsp; <img src="img/b.gif" /></span>
			</div>
	</div>

</div>
