<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>    
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
        	<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message code="label.plans"/></a></li>
            <li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message code="label.manageQHPS"/></a></li>
            <li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message code="label.linkPlanDetails"/></a></li>
            <li><spring:message code="label.titlePlanBenefits"/></li>
        </ul><!--page-breadcrumb ends -->
			<h1><a name="skip"></a><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/>${plan.name} 
			<%-- <c:if test="${STATE_CODE != null && STATE_CODE != 'MN'}">
				<a class="btn btn-primary btn-small pull-right" target="_blank" href="<c:url value="/private/planinfo/${planDisplayDate}/${plan.id}" />" title="<spring:message code="label.displayBenefit"/>" ><spring:message code="label.displayBenefit"/></a>
			</c:if> --%>
			</h1>
			<div class="gutter10 pull-right">
				<c:if test="${displayEditButton == 'YES'}"><a href="<c:url value="/admin/planmgmt/editqhpbenefits/${encPlanId}" />" class="pull-right marginTop10"><i class="icon icon-user"></i><spring:message code="button.label"/></a></c:if>
				<%-- <c:if test="${IS_CA_CALL == 'true' and fn:length(data) gt 0}"><a href="<c:url value="/admin/planmgmt/dowloadbenefits/${plan.planHealth.id}"/>" class="btn btn-primary span11" id="downloadQHPBenefitFile"><spring:message code="label.download"/></a></c:if> --%>	
			</div>
		</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.aboutPlan"/></h4>
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li class="active"><a href="<c:url value="/admin/planmgmt/viewqhpbenefits/${encPlanId}"/>"><spring:message  code='label.linkPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhprates/${encPlanId}" />"><spring:message  code='label.linkPlanRates'/></a></li>
                    <c:if test="${displayProviderNetwork == 'YES'}">
                    	<li><a href="<c:url value="/admin/planmgmt/viewqhpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkProviderNetwork'/></a></li>
					</c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <c:if test="${exchangeType != 'PHIX'}">
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>
				<br /><br />
				<c:if test="${displayAction == 'YES'}">
					<h4 class="margin0"><spring:message code='label.action'/></h4>
					<ul class="nav nav-list graybg">
						<li><a href="<c:url value="/admin/planmgmt/editqhpdetail/${encPlanId}" />"><!--<i class="icon icon-pencil" style=""></i>--> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpbenefits/${encPlanId}" />"><!--<i class="icon icon-arrow-up"></i>--> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhprates/${encPlanId}" />"><!--<i class="icon icon-arrow-up"></i>--> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpprovidernetwork/${encPlanId}" />"><!--<i class="icon icon-folder-open"></i>--> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpenrollmentavail/${encPlanId}" />"><!--<i class="icon icon-folder-open"></i>--> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpcertification/${encPlanId}" />"><!--<i class="icon icon-folder-open"></i>--> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>
			
        </div><!-- end of span3 -->
        
		<div class="span9" id="rightpanel">
			<display:table name="data" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-border-none table-striped"  defaultsort="1" defaultorder="descending" >
			           <display:column property="lastUpdateTimestamp" titleKey="label.date" format="{0,date,MMM dd, yyyy}" sortable="true"/>
			           <display:column property="displayField" titleKey="label.fieldUpdated" sortable="true" />
			           <display:column property="effectiveDateCombined" titleKey="label.effectiveDate" sortable="true" />
			           <%-- <display:column property="userName" titleKey="label.userName" sortable="false" />
						<c:if test="${exchangeType != 'PHIX'}">
							<display:column property="commentId" titleKey="label.comment" sortable="false"  />
						</c:if> --%>
						 <display:column property="displayVal" titleKey="label.download" sortable="false" />
						<display:setProperty name="paging.banner.placement" value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
			           <display:setProperty name="paging.banner.last" value=''/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
			           <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.one_item_found" value=''/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">			           
			           <div class="pagination center">
						<ul>
							<li>{0}</li>
							<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
						</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
						<div class="pagination center">
							<ul>
								<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
								<li>{0}</li>
							</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.full" value='
						<div class="pagination center">
							<ul>
								<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
								<li>{0}</li>
								<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
							</ul>
						</div>
						'/>
			</display:table>
	</div><!-- end of .span9 -->
</div>
</div>
<div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel"><spring:message  code='label.viewComment'/></h3>
	</div>
	<div id="commentDet" class="modal-body">
		<p></p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message  code='label.btnClose'/></a>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	});
});

function getComment(commentId)
{
	$('#commentDet').html("<p> <spring:message  code='label.loadingComment'/> </p>");
	$.ajax({
		  type: 'GET',
		  url: "../getComment",
		  data:{"commentId":commentId},
		  success: function(data) {
			  $('#modal-body').html("<p> "+ data + "</p>");
          }	  
		});
	
}
function showdetail(documentUrl) {
	 var rv = -1; // Return value assumes failure.
	 if (navigator.appName == 'Microsoft Internet Explorer'){
	    var ua = navigator.userAgent;
	    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	    if (re.exec(ua) != null){
	       rv = parseFloat( RegExp.$1 );
	    }
	 }
	 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
		 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="graydrkaction"><h4 class="margin0 pull-left"><spring:message  code='label.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:"static",keyboard:"false"});
	 } else {
		 location.href = documentUrl;
	 }   
}
</script>
