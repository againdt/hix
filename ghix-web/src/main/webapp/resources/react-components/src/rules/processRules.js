/**
 * Created by sellathurai_s on 7/18/18.
 */
import React from "react";
import { FormattedMessage } from 'react-intl';
import {Engine} from 'json-rules-engine';
import rulesData from './json/futherActionRules.json';
import RulesUtil from './rulesUtil';



class ProcessRules {

    constructor(facts){
        // console.log("Inside Process Rules.");
        this.engine = new Engine();
        this.facts = facts;
        this.rulesData = rulesData;
        this.rulesUtil = new RulesUtil();
    }

    //TODO: do this for all other application status..
    getDefaultOutput(applicationStatus) {
        switch (applicationStatus){
            case '':
                return {
                    "healthCoverageStatus": <FormattedMessage id="further_action.coverage_status.application_not_started"/>,
                    "paragraphHeader": <FormattedMessage id="further_action.paragraph_header"/>,
                    "paragraphText": <FormattedMessage id="further_action.further_text.default.paragraphText_26"/>,
                    "ctaButton": <FormattedMessage id="further_action.cta.btn_start_new_application"/>,
                    "linkDestination": "START_NEW_APPLICATION"
                };
                break;
            case 'OP':
                return {
                    "healthCoverageStatus": <FormattedMessage id="further_action.coverage_status.in_progress"/>,
                    "paragraphHeader": <FormattedMessage id="further_action.paragraph_header"/>,
                    "paragraphText": <FormattedMessage id="further_action.further_text.default.paragraphText_27"/>,
                    "ctaButton": <FormattedMessage id="further_action.cta.btn_resume_application"/>,
                    "linkDestination": "RESUME_APPLICATION"
                };
                break;
            case 'ER':
                return {
                    "healthCoverageStatus": <FormattedMessage id="further_action.coverage_status.in_progress"/>,
                    "paragraphHeader": <FormattedMessage id="further_action.paragraph_header"/>,
                    "paragraphText": <FormattedMessage id="further_action.further_text.default.paragraphText_28"/>,
                    "ctaButton": <FormattedMessage id="further_action.cta.btn_shop_for_plans"/>,
                    "linkDestination": "GROUPING_SCREEN"
                };
                break;
            case 'PN':
                return {
                    "healthCoverageStatus": <FormattedMessage id="further_action.coverage_status.in_progress"/>,
                    "paragraphHeader": <FormattedMessage id="further_action.paragraph_header"/>,
                    "paragraphText": <FormattedMessage id="further_action.further_text.default.paragraphText_29"/>,
                    "ctaButton": <FormattedMessage id="further_action.cta.btn_continue_shopping"/>,
                    "linkDestination": "GROUPING_SCREEN"
                };
                break;
            case 'EN':
                return {
                    "healthCoverageStatus": <FormattedMessage id="further_action.coverage_status.enrolled"/>,
                    "paragraphHeader": <FormattedMessage id="further_action.paragraph_header"/>,
                    "paragraphText": <FormattedMessage id="further_action.further_text.default.paragraphText_30"/>,
                    "ctaButton": <FormattedMessage id="further_action.cta.btn_change_plans"/>,
                    "linkDestination": "GROUPING_SCREEN"
                };
                break;


            default:
                break;
        }
        return {
            "healthCoverageStatus": <FormattedMessage id="further_action.coverage_status.application_not_started"/>,
            "paragraphHeader": <FormattedMessage id="further_action.paragraph_header"/>,
            "paragraphText": <FormattedMessage id="further_action.further_text.default.paragraphText_26"/>,
            "ctaButton": <FormattedMessage id="further_action.cta.btn_start_new_application"/>,
            "linkDestination": "START_NEW_APPLICATION"
        };
    }

    executeRules() {
        return new Promise((resolve, reject) => {

            // console.log("Inside executeRules.");

            for (let i = 0; i < this.rulesData.length; i++) {
                this.engine.addRule({
                    conditions: {
                        any: [{
                            all: this.rulesUtil.createRule(this.rulesData[i].rules)
                        }]
                    },
                    event: this.rulesUtil.createEvent(this.rulesData[i].result)
                });
            }

            let displayData = [];
            this.engine
                .run(this.facts)
                .then(events => { // run() returns events with truthy conditions
                    if (events && events.length > 0) {
                        let displayData = events.map( function (data) {
                                console.log("Further Action Matched Row id=" + (data.params ? data.params.id : "Not found"));
                                return data.params
                            }
                        );
                        resolve(displayData);
                    } else {
                        console.log("Further Action Matched Row id=Not found");
                        // console.log("Default State: " + JSON.stringify(this.getDefaultOutput("ER")));
                        displayData.push(this.getDefaultOutput(this.facts.applicationStatus));
                        // console.log("No matching records");
                        resolve(displayData);
                    }
                });

        });
    }
};

export { ProcessRules as default};