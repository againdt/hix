import maskSSN from './maskSSN';
import {REASONABLE_EXPLANATIONS_LIST} from '../constants/dropdownOptions';
import moment from 'moment';
import currencyTools from './currencyTools';
import React from 'react';


export default {
    getValueForSSN : (socialSecurityNumber, reasonableExplanationForNoSSN) => {
        return socialSecurityNumber ?
            maskSSN(socialSecurityNumber) :
            (reasonableExplanationForNoSSN? `--NA-- (Reason : ${REASONABLE_EXPLANATIONS_LIST[reasonableExplanationForNoSSN]})` : '--NA--');
    },
    calculateAge : (dateOfBirth) => {
        return moment().diff(moment(dateOfBirth, 'YYYYMMDD'), 'years');
    },
    getCoverageType : (programTypes) => programTypes.map(program => {
        if (program.eligible) return <div className='summary__value' key={program.name}>{program.name}</div>;
        else return '';
    }),
    // getOtherCoverageType : (programTypes) => programTypes.map(program => {
    //     if (program.eligible && program.type === 'OTHER_COVERAGE') return <div key={program.name}>{program.name}</div>;
    // }),

    getOtherCoverageInsuranceName: (otherInsurance) => {
        const {
            insuranceName,
            policyNumber,
            limitedBenefit
        } = otherInsurance;
        if(insuranceName || policyNumber || limitedBenefit) {
            return (
                <dl className='usa-offset-one-twelfth'>
                    <dt className='summary__label'>Insurance Name</dt>
                    <dd className='summary__value'>{insuranceName}</dd>
                    <dt className='summary__label'>Policy Number</dt>
                    <dd className='summary__value'>{policyNumber}</dd>
                    <dt className='summary__label'>Is this a limited benefit coverage?</dt>
                    <dd className='summary__value'>{limitedBenefit ? 'Yes' : 'No'}</dd>
                </dl>
            );
        }
        else return '';
    },

    getEmployerSponsoredCoverageDetails : (employerSponsoredCoverage) => {
        return employerSponsoredCoverage.map((coverageDetails, indx)=> {
            // const isMinValPlan = () => {
            //     if (coverageDetails.minimumValuePlan && coverageDetails.employerPremium > 0 && coverageDetails.employerPremiumFrequency !== 'UNSPECIFIED') {
            //         <span className='usa-width-one-half grid-offset-1'>Premium amount for the lowest-cost plan</span> :
            //         <span className='usa-width-one-fourth'>{coverageDetails.employerPremium}{coverageDetails.employerPremiumFrequency}</span>
            //     }
            // }
            return (
                <div className='usa-width-one-whole review-document bordered-t' key={indx}>
                    <span className='usa-width-one-whole usa-offset-one-twelfth summary__label'>Employer Name</span>
                    <span className='usa-width-one-whole usa-offset-one-twelfth summary__value'>{coverageDetails.companyName}</span>
                    <span className='usa-width-one-whole usa-offset-one-twelfth summary__label'>Contact Number</span>
                    <span className='usa-width-one-whole usa-offset-one-twelfth summary__value'>{coverageDetails.phone}</span>
                    <span className='usa-width-one-whole usa-offset-one-twelfth summary__label'>Health plan meets the minimum value standard?</span>
                    <span className='usa-width-one-whole usa-offset-one-twelfth summary__value'>{coverageDetails.minimumValuePlan ? 'Yes' : 'No'}</span>
                    {(coverageDetails.minimumValuePlan && coverageDetails.employerPremium > 0 && coverageDetails.employerPremiumFrequency !== 'UNSPECIFIED') ?
                        <span className='usa-width-one-whole usa-offset-one-twelfth'><span className='usa-width-one-whole summary__label'>Premium amount for the lowest-cost plan</span>
                            <span className='usa-width-one-whole summary__value'>${coverageDetails.employerPremium / 100} / {coverageDetails.employerPremiumFrequency}</span></span>
                        : ''
                    }
                </div>
            );
        });
    },
    getIncomeValueForRender : (projectedIncome, reportedIncome) => {
        const valueForRender = projectedIncome ?
            projectedIncome :
            reportedIncome;

        return valueForRender ? currencyTools.getMoneyFormat(valueForRender, true) : '$0.00';
    }
};
