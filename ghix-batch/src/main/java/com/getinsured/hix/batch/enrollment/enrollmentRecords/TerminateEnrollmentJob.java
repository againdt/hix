package com.getinsured.hix.batch.enrollment.enrollmentRecords;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.skip.Enrollment834Out;
import com.getinsured.hix.batch.util.EnrollmentEmailUtils;
import com.getinsured.hix.batch.util.GhixBatchConstants;
//import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.common.io.Files;


public class TerminateEnrollmentJob  implements Tasklet{
	private IssuerFileList issuerFileList;
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollmentJob.class);
	private EnrollmentEmailUtils enrollmentEmailUtils;
	private String batchJobStatus;
	private Enrollment834Out enrollment834Out;
	private JobService jobService;
	public IssuerFileList getIssuerFileList() {
		return issuerFileList;
	}

	public void setIssuerFileList(IssuerFileList issuerFileList) {
		this.issuerFileList = issuerFileList;
	}
	
	public EnrollmentEmailUtils getEnrollmentEmailUtils() {
		return enrollmentEmailUtils;
	}

	public void setEnrollmentEmailUtils(EnrollmentEmailUtils enrollmentEmailUtils) {
		this.enrollmentEmailUtils = enrollmentEmailUtils;
	}
	

	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		try{
		ExecutionContext je = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		Long jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		if(jobExecutionId!=null && jobService!=null){
			batchJobStatus= jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		Object jobExecutionStatus = je.get("jobExecutionStatus");
		String jobStatus = null;

		if(jobExecutionStatus != null){
			jobStatus = (String)jobExecutionStatus;
		}
		
		EnrollmentBatchEmailDTO enrollmentBatchEmailDTO = new EnrollmentBatchEmailDTO();
		enrollmentBatchEmailDTO.setFileLocation(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.XMLEXTRACTPATH));
		enrollmentBatchEmailDTO.setAppServer(GhixBatchConstants.appserver);

		if((jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0) ){
			if(issuerFileList != null){
				LOGGER.info("Job execution failed, hence removing all temp files created by this job execution from outbound folder");
				enrollmentBatchEmailDTO.setErrorCode(201);

				if (je.get("errorMessage") != null) {
					enrollmentBatchEmailDTO.setErrorMessage((String)je.get("errorMessage"));
				}else{
					enrollmentBatchEmailDTO.setErrorMessage("UNKNOWN ERROR MESSAGE");
				}

				if((jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0) && enrollmentEmailUtils != null){
					enrollmentEmailUtils.enrollmentBatchFailureEmailNotification(enrollmentBatchEmailDTO, chunkContext);
					//Failing the job
					throw new GIException("Enrollment Extraction Job Execution Failed");
				}

			}
		}else if( (batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED)))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		else{
			if(issuerFileList != null){
				LOGGER.info("Job execution succeeded, hence renaming all temp files in outbound folder");
				
				mergeFiles(issuerFileList.issuerXmlFiles);
			}
		}
		Boolean datesProvided = (Boolean) je.get("datesProvided");
		if(datesProvided != null && datesProvided){
			Date lastRunDate = (Date) je.get("lastRunDate");
			chunkContext.getStepContext().getStepExecution().getJobExecution().setStartTime(lastRunDate);
		}
		}finally{
			if(enrollment834Out!=null){
				enrollment834Out.clearIssuerEnrollmentIdMap();
			}
			if(issuerFileList != null){
				issuerFileList.deleteAllFiles();
				issuerFileList.clearFiles();
			}
		}
		return RepeatStatus.FINISHED;
	}

	public Enrollment834Out getEnrollment834Out() {
		return enrollment834Out;
	}

	public void setEnrollment834Out(Enrollment834Out enrollment834Out) {
		this.enrollment834Out = enrollment834Out;
	}

	private void mergeFiles(List<String> fileList) throws Exception{
		try{
			Map<String,Map<Integer,String>> issuerFileMap=createIssuerFileMap(fileList);

			if(issuerFileMap!=null && !issuerFileMap.isEmpty()){
				for(Map.Entry<String,Map<Integer,String>> entry : issuerFileMap.entrySet()){
					LOGGER.info("File Merging and Renaming starts for issuer :: "+entry.getKey());
					Map<Integer,String> partFileMap=sortByKey(entry.getValue());
					List<String> issuerFileList = new ArrayList<String>(partFileMap.values());
					LOGGER.info("Files  for issuer :: "+entry.getKey() +" in list "+ issuerFileList);
					if(issuerFileList!=null &&! issuerFileList.isEmpty()){

						if(issuerFileList.size()==1){
							//rename
							File tempFile = new File(issuerFileList.get(0));
							if(tempFile != null){
								// Rename back from .tmp to .xml So that files get picked up for EDI.
								String folderPath=issuerFileList.get(0).substring(0,issuerFileList.get(0).lastIndexOf(File.separator));
								String oldFileName=issuerFileList.get(0).substring(issuerFileList.get(0).lastIndexOf(File.separator)+1);
								//String newXmlFileName = oldFileName.replaceAll(".tmp", ".xml").replaceAll(oldFileName.substring(StringUtils.ordinalIndexOf(oldFileName, "_", 2),StringUtils.ordinalIndexOf(oldFileName,"_", 3)), "");
								String newXmlFileName = new StringBuilder(oldFileName).delete(StringUtils.ordinalIndexOf(oldFileName, "_", 2), StringUtils.ordinalIndexOf(oldFileName,"_", 3)).toString();
								newXmlFileName=newXmlFileName.replaceAll(".tmp", ".xml");
								File newXmlFile = new File(folderPath+File.separator+newXmlFileName);
								LOGGER.info("New File name for issuer :: "+entry.getKey()+ " = " +newXmlFileName);
								
								boolean	isRenamedSuccessfully = renameFileByGuavaMoveFiles(tempFile, newXmlFile);
								LOGGER.info("File rename from "+oldFileName+ " to " +newXmlFileName+ " status "+isRenamedSuccessfully);
							}
						}
						else{
							String folderPath=issuerFileList.get(0).substring(0,issuerFileList.get(0).lastIndexOf(File.separator));
							String oldName=issuerFileList.get(0).substring(issuerFileList.get(0).lastIndexOf(File.separator)+1);
							//String newXMLFileName=oldName.replaceAll(oldName.substring(StringUtils.ordinalIndexOf(oldName, "_", 2),StringUtils.ordinalIndexOf(oldName,"_", 3)), "");
							String newXMLFileName = new StringBuilder(oldName).delete(StringUtils.ordinalIndexOf(oldName, "_", 2), StringUtils.ordinalIndexOf(oldName,"_", 3)).toString();
							joinFiles(new File(folderPath+File.separator+newXMLFileName),issuerFileList);
							
							
							//Code to rename new tmp file
							File tempFile=new File(folderPath+File.separator+newXMLFileName);
							if(tempFile!=null){
								newXMLFileName=newXMLFileName.replaceAll(".tmp", ".xml");
								File newXmlFile = new File(folderPath+File.separator+newXMLFileName);
								//tempFile.renameTo(newXmlFile);
								LOGGER.info("New File name for issuer :: "+entry.getKey()+ " = " +newXMLFileName);
								
								boolean isRenamedSuccessfully  = renameFileByGuavaMoveFiles(tempFile, newXmlFile);
								
								LOGGER.info("File rename from "+oldName+ " to " +newXMLFileName+ " status "+isRenamedSuccessfully);
							}
						}

					}
					LOGGER.info("File Merging and Renaming ends for issuer :: "+entry.getKey());

				}

			}

		}catch(Exception e){
			LOGGER.error("File Merging Failed");
			throw new Exception("File Merging Failed "+e.getMessage() );
		}
	}
	
	private boolean renameFileByGuavaMoveFiles(final File sourceFile, File targetFile) {
		boolean isFileRenamedSuccess = true;
		try {
			LOGGER.info("Moving File :: from  "+sourceFile.getAbsolutePath()+ " to " +targetFile.getAbsolutePath());
			Files.move(sourceFile, targetFile);
		} catch (IOException ioe) {
			LOGGER.error("Exception occurred while renamimg file: "+sourceFile.getName() + ioe.getMessage(),ioe);
			isFileRenamedSuccess = false;
		}
		return isFileRenamedSuccess;

	}

	private Map<String,Map<Integer,String>> createIssuerFileMap(List<String> issuerFileList){
		Map<String,Map<Integer,String>> issuerFileMap=null;
		if(issuerFileList!=null && issuerFileList.size()>0){
			issuerFileMap= new HashMap<String,Map<Integer,String>>();
			for(String fullFileName: issuerFileList){
				String fileName= fullFileName.substring(fullFileName.lastIndexOf(File.separator)+1);
				//sample fileName  = to_26002_0_ID_834_INDV_20161121154946.tmp
				if(StringUtils.isNotBlank(fileName)){
					String[] splitted= fileName.split("_");
					if(fileName.contains("RENTERM")){
						//Group 1 Invidual RENTERM XML files
						String splitName = splitted[1]+"_RENTERM";
						if(issuerFileMap.get(splitName)!=null){
							Map<Integer,String> partFileMap=issuerFileMap.get(splitName);
							partFileMap.put(Integer.valueOf(splitted[2]), fullFileName);
						}else{
							Map<Integer,String> partFileMap=new HashMap<Integer,String>();
							partFileMap.put(Integer.valueOf(splitted[2]), fullFileName);
							issuerFileMap.put(splitName, partFileMap);
						}
					}
					else{
						//Group 2 Invidual XML files
						if(issuerFileMap.get(splitted[1])!=null){
							Map<Integer,String> partFileMap=issuerFileMap.get(splitted[1]);
							partFileMap.put(Integer.valueOf(splitted[2]), fullFileName);
						}else{
							Map<Integer,String> partFileMap=new HashMap<Integer,String>();
							partFileMap.put(Integer.valueOf(splitted[2]), fullFileName);
							issuerFileMap.put(splitted[1], partFileMap);
						}
					}
				}
			}
		}
		return issuerFileMap;
	}
	
	private  void joinFiles(File destination, List<String> sources)
	        throws IOException {
	    OutputStream output = null;
	    try {
	    	if(destination!=null){
	    		String endString=null;
	        output = createAppendableStream(destination);
	        for (int i=0; i<sources.size();i++) {
	        	File source= new File(sources.get(i));
	        	InputStream input = null;
	            input = new BufferedInputStream(new FileInputStream(source));
	           
	           StringBuffer str=new StringBuffer( IOUtils.toString(input));
	            if(i==0){
	            	input= new BufferedInputStream(new ByteArrayInputStream(str.substring(0,  str.lastIndexOf("</enrollment>")+13).concat("\n").getBytes()));
	            	endString=str.substring(str.lastIndexOf("</enrollment>")+14);
	            }else{
	            	input= new BufferedInputStream(new ByteArrayInputStream(str.substring(str.indexOf("<enrollment>"),  str.lastIndexOf("</enrollment>")+13).concat("\n").getBytes()));
	            }
	        appendFile(output, input);
	        
	    	
	        }
	        if(endString!=null && output!=null){
	        appendFile(output, new BufferedInputStream(new ByteArrayInputStream(endString.getBytes())));
	        }
	}
	    }finally {
	        IOUtils.closeQuietly(output);
	    }
	}

	public static BufferedOutputStream createAppendableStream(File destination)
	        throws FileNotFoundException {
	    return new BufferedOutputStream(new FileOutputStream(destination, true));
	}

	private static void appendFile(OutputStream output, InputStream input)
	        throws IOException {
	   // InputStream input = null;
	    try {
	        IOUtils.copy(input, output);
	    } finally {
	        IOUtils.closeQuietly(input);
	    }
	}


	private Map<Integer, String> sortByKey(Map<Integer,String> partFileMap){
		if(partFileMap!=null && !partFileMap.isEmpty()){
			Map<Integer, String> treeMap= new TreeMap<Integer, String>(
					new Comparator<Integer>() {

						@Override
						public int compare(Integer o1, Integer o2) {
							return o1.compareTo(o2);
						}

					});
			treeMap.putAll(partFileMap);
			return treeMap;
		}
		return null;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
}
