<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<div class="gutter10"  ng-app="enrollmentApp" ng-controller="enrollmentAppCtrl">
	<form class="form-horizontal" id="frmConsuerDetail" name="frmConsuerDetail" action="" method="POST">		
		<div id="rightpanel" class="span9-remove">
			<div class="">
			<h1>
				1095-A Records &nbsp;				
			</h1>
		</div>
			<div class="header">
                 <h4 class="pull-left">Recipient Information
                 </h4>
                 <div ng-hide = "editDetailMod">
                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="editDetail()">Edit</a>
                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="resendDetail()">Resend</a>
                 </div>
                 <div ng-show = "editDetailMod">
                 <a class="btn btn-small pull-right" title="Edit"  href="javascript:void(0)" ng-click="cancelEditl()">Cancel</a>
                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="saveDetail()">Save</a>                                 
                 </div>
            </div>
            <div class="gutter10">
			
				<table class="table-border-none table">
					<tbody>
						<tr>
							<td class="txt-right span4"> Recipient First Name <!-- <spring:message  code='label.columnHeaderName'/>-->:</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.firstName}} </strong></div>
							<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="<spring:message code="label.firstName" />" name="firstName" id="firstName" ng-model="recipent.firstName"  maxlength="40" />
							</div>
							</td>
						</tr>						
						<tr>
							<td class="txt-right span4"> Recipient Last Name <!-- <spring:message  code='label.columnHeaderName'/>-->:</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.lastName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Recipient Last Name" name="recipientLastName" id="recipientLastName" ng-model="recipent.lastName"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Recipient SSN :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.ssn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Recipient SSN" name="recipientSSN" id="recipientSSN" ng-model="recipent.ssn"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Recipient Date Of Birth :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.birthDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Recipient Date Of Birth" name="recipientDateOfBirth" id="recipientDateOfBirth" ng-model="recipent.birthDate"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr ng-show = "spouse.firstName">
							<td class="txt-right span4"> Spouse First Name :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.firstName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Spouse First Name" name="spouseFirstName" id="spouseFirstName" ng-model="spouse.firstName"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr ng-show = "spouse.firstName">
							<td class="txt-right span4"> Spouse Last Name :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.lastName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Spouse Last Name" name="spouseLastName" id="spouseLastName" ng-model="spouse.lastName"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr ng-show = "spouse.firstName">
							<td class="txt-right span4"> Spouse SSN :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.ssn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Spouse SSN" name="spouseSSN" id="spouseSSN" ng-model="spouse.ssn"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr ng-show = "spouse.firstName">
							<td class="txt-right span4"> Spouse Date Of Birth :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{spouse.birthDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Spouse Date Of Birth" name="spouseDateOfBirth" id="spouseDateOfBirth" ng-model="spouse.birthDate"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Stree Address :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.address1}} {{recipent.address2}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Recipient Address1" name="recipientaddress1" id="recipientaddress1" ng-model="recipent.address1"  maxlength="40" /></br>
								<input class="filterField" type="text" placeholder="Recipient Address2" name="recipientaddress2" id="recipientaddress2" ng-model="recipent.address2"  maxlength="40" />
							</div>
							</td>
						</tr>						
						<tr>
							<td class="txt-right span4"> City :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.city}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="City" name="recipientCity" id="recipientCity" ng-model="recipent.city"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> State :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.state}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="State" name="recipientState" id="recipientState" ng-model="recipent.state"  maxlength="40" />
							</div>
							</td>
						</tr>						
						<tr>
							<td class="txt-right span4"> Zip :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.zip}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Zip" name="recipientZip" id="recipientZip" ng-model="recipent.zip"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Country :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{recipent.county}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Country" name="recipientCountry" id="recipientCountry" ng-model="recipent.county"  maxlength="40" />
							</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="header">
                 <h4 class="pull-left">Policy Information
                 </h4>                 
            </div>
            <div class="gutter10">
			
				<table class="table-border-none table">
					<tbody>
						<tr>
							<td class="txt-right span4"> Marketplace assigned Policy Number :</td>
							<td><div><strong>{{enrollment1095Details.policyNumber}} </strong></div>								
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Policy Issuer's Name :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.policyIssuerName}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Policy Issuer's Name" name="policyIssuerName" id="policyIssuerName" ng-model="enrollment1095Details.policyIssuerName"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Policy Start Date :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.policyStartDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Policy Start Date" name="policyStartDate" id="policyStartDate" ng-model="enrollment1095Details.policyStartDate"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Policy End Date :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.policyEndDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Policy End Date" name="policyEndDate" id="policyEndDate" ng-model="enrollment1095Details.policyEndDate"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Corrected Indicator :</td>
							<td><div><strong>{{enrollment1095Details.correctedCheckBoxIndicator}} </strong></div>
								
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Void Indicator :</td>
							<td><div><strong>{{enrollment1095Details.voidCheckboxindicator}} </strong></div>								
							</td>
						</tr>											
					</tbody>
				</table>
			</div>
			
			<div class="header">
                 <h4 class="pull-left">Covered Individuals
                 </h4>                
            </div>
            <div class="gutter10">
			
				<table class="table table-striped search-applicant-results">
					<thead>
						<tr class="graydrkbg ">
							<th >Covered Individual </th>
							<th >SSN</th>
							<th >Date Of Birth</th>	
							<th >Policy Start Date</th>
							<th >Policy End Date</th>																			
						</tr>
					</thead>
						<tr ng-repeat="member in members">
							<td><div ng-hide = "editDetailMod"><strong>{{member.firstName}}&nbsp;{{member.lastName}}</strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="First Name" name="memberFirstName" id="memberFirstName" ng-model="member.firstName"   style="width: 100px;" /></br>
								<input class="filterField" type="text" placeholder="Last Name" name="memberLastName" id="memberLastName" ng-model="member.lastName"  style="width: 100px;" />
							</div>
							</td>							
							<td><div ng-hide = "editDetailMod"><strong>{{member.ssn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="SSN" name="memberSSN" id="memberSSN" ng-model="member.ssn" style="width: 100px;" />
							</div>
							</td>
							<td><div ng-hide = "editDetailMod"><strong>{{member.birthDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Date Of Birth" name="memberBirthDate" id="memberBirthDate" ng-model="member.birthDate"  style="width: 100px;" />
							</div>
							</td>
							<td><div ng-hide = "editDetailMod"><strong>{{member.coverageStartDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Policy Start Date" name="memberBirthDate" id="memberBirthDate" ng-model="member.coverageStartDate"  style="width: 100px;" />
							</div>
							</td>							
							<td><div ng-hide = "editDetailMod"><strong>{{member.coverageEndDate}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Policy End Date" name="memberBirthDate" id="memberBirthDate" ng-model="member.coverageEndDate"  style="width: 100px;" />
							</div>
							</td>							
						</tr>						
				</table>
			</div>	
			
			<div class="header">
                 <h4 class="pull-left">Premium Information
                 </h4>                 
            </div>
            <div class="gutter10">
			
				<table class="table table-striped search-applicant-results">
					<thead>
						<tr class="graydrkbg ">
							<th style="width: 80px;">Month </th>
							<th style="width: 80px;">Total </br>Gross Premium </br>(X)</th>
							<th style="width: 30px;">EHB%</br>(Y)</th>	
							<th style="width: 100px;" >Associated </br>Dental Peadiatric </br>EHB Amount</br>(Z)</th>
							<th style="width: 100px;" >Accountable Member </br>For Dental</th>
							<th style="width: 100px;">B. Monthly Premium</br>Amount of Second</br>Lowest Cost Silver</br>Plan (SLCSP)</th>
							<th style="width: 100px;">C. Monthly Advance </br>Payment of Premium</br>Tax Creadit</th>																										
						</tr>
					</thead>
						<tr ng-repeat="premium in enrollment1095Details.enrollmentPremiums">
							<td><div>{{premium.monthName}}</div></td>							
							<td><div ng-hide = "editDetailMod"><strong>{{premium.grossPremium}}</strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Gross Premium" name="grossPremium" id="grossPremium" ng-model="premium.grossPremium"  style="width: 50px;" />
								</div>
							</td>
							<td><div>{{premium.ehbPercent}}</div></td>
							<td><div>{{premium.pediatricEhbAmt}}</div></td>
							<td><div>{{premium.accountableMemberDental}}</div></td>
							
							<td><div ng-hide = "editDetailMod">{{premium.slcspAmount}}</div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="slcsp Amount" name="slcspAmount" id="slcspAmount" ng-model="premium.slcspAmount"  style="width: 50px;" />
								</div>
							</td>
							<td><div ng-hide = "editDetailMod">{{premium.aptcAmount}}</div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="aptc Amount" name="aptcAmount" id="aptcAmount" ng-model="premium.aptcAmount"  style="width: 50px;" />
								</div>
							</td>
														
						</tr>						
				</table>
			</div>	
			
			
			<div class="header">
                 <h4 class="pull-left">Other
                 </h4>                
            </div>
            <div class="gutter10">
			
				<table class="table-border-none table">
					<tbody>
						<tr>
							<td class="txt-right span4"> Marketplace Identifier :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.correctedCheckBoxIndicator}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Marketplace Identifier" name="marketplaceIdentifier" id="marketplaceIdentifier" ng-model="enrollment1095Details.correctedCheckBoxIndicator"  maxlength="40" />
							</div>
							</td>
						</tr>						
						<tr>
							<td class="txt-right span4"> Last Updated :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.updatedOn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder=" Last Updated" name="lastUpdated" id="lastUpdated" ng-model="enrollment1095Details.updatedOn"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> PDF Generated on :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.pdfGeneratedOn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="PDF Generated on" name="pdfGeneratedOn" id="pdfGeneratedOn" ng-model="enrollment1095Details.pdfGeneratedOn"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Last Sent to Consumer :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.pdfGeneratedOn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Last Sent to Consumer" name="lastSenttoConsumer" id="lastSenttoConsumer" ng-model="enrollment1095Details.pdfGeneratedOn"  maxlength="40" />
							</div>
							</td>
						</tr>
						<tr>
							<td class="txt-right span4"> Last Sent to CMS :</td>
							<td><div ng-hide = "editDetailMod"><strong>{{enrollment1095Details.cmsXmlGeneratedOn}} </strong></div>
								<div ng-show = "editDetailMod">
								<input class="filterField" type="text" placeholder="Last Sent to CMS" name="lastSenttoCMS" id="lastSenttoCMS" ng-model="enrollment1095Details.cmsXmlGeneratedOn"  maxlength="40" />
							</div>
							</td>
						</tr>									
					</tbody>
				</table>
			</div>
			<div style="border-bottom: 1px solid #e7e7e7;">&nbsp; </div>
			<div></br>
                 <div class="pull-left"><a class="btn btn-small pull-right" title="Edit" href="/hix/enrollment/1095A/searchmember">Back to Search</a>
                 </div>
                 <div ng-hide = "editDetailMod">
                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="editDetail()">Edit</a>
                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="resendDetail()">Resend</a>
                 </div>
                 <div ng-show = "editDetailMod">
                 <a class="btn btn-small pull-right" title="Edit"  href="javascript:void(0)" ng-click="cancelEditl()">Cancel</a>
                 <a class="btn btn-small pull-right" title="Edit" href="javascript:void(0)" ng-click="saveDetail()">Save</a>                 
                 </div>
            </div>			
		</div>
		<input type="hidden" value='${enrollment1095Details}' id="consumerDetailJson" name="consumerDetailJson">
		<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
		</form>	
	<!-- row-fluid -->
	<div ng-show="modalForm.edit1095">
     <div id="ssapSubmitModal" modal-show="modalForm.edit1095" class="modal hide fade" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">
            
            <div class="modal-header" ng-if = "modalForm.subResult != 'loader'">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelCsrAction()">&times;</button>
              <h3 id="myModalLabel">
				<span>1095-A Edit</span>
			  </h3>
            </div>

		  <div ng-if = "modalForm.subResult == 'reason'">
            <div class="modal-body">
				<p ng-class="{fadeFont: fadeFont}">Please specify the reason for changing 1095-A infomation for this customer.</br>
					This will help keep track of updates to the customer's record.
				</p>
            </div>

            <div class="modal-body" >
                <span class="oRReason">Specify the reason for override. <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
                <br>
                <textarea class="overrideText" ng-model="modalForm.overrideComment" maxlength="{{maxLength}}" placeholder="Please explain the change you are making and why"></textarea>
                <br>
                <div>
                  <a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()">Cancel</a>
                  <a class="btn btn-primary pull-right" ng-click="saveContinue()" class="" ng-disabled = "modalForm.overrideComment.length < 1">Continue</a>
                  <span class="pull-left">{{maxLength - modalForm.overrideComment.length}} max character</span>
                </div>
            </div>
            </div>
            <div ng-if = "modalForm.subResult == 'loader'">
					  <div class="modal-body">
			    		<p align ="center">Processing please wait </p>
  		<img  src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/>
			    	  </div>
			
					</div>

					<div ng-if = "modalForm.subResult == 'success'">
					  <div class="modal-body">
			    		<h3>Submission Successful!</h3>			        
			    		<p>Press OK button to go back to dashboard.</p>
			    	  </div>
			
			       	  <div class="modal-footer">
			          	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()">OK</a>
			          </div>
					</div>
			
					<div ng-if = "modalForm.subResult == 'failure'">
					  <div class="modal-body">
			    		<h3> Failure!</h3>
			    		<p> While submitting error occured, please try later.</p>
						
			    	  </div>
			
			       	  <div class="modal-footer">
			          	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()">OK</a>
			          </div>
					</div>

        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
    </div>
  </div> 
	<div ng-show="modalForm.processing">
	<div id="processingModal" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
  		<div class="modal-body">
  		<p align ="center">Processing please wait </p>
  		<img  src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/>
    		
  		</div>
	</div>
	</div>
	<!-- CSR Modal-->
  
	
</div>

<script>var baseURL = "${pageContext.request.contextPath}";</script>
<script type="text/javascript" src="/hix/resources/js/enrollment1095/enrollment1095.js"></script>