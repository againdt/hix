package com.getinsured.eligibility.verification.util;

import java.io.*;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.iex.dto.SsapApplicant;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.org.nmhix.ssa.person.HouseholdMember;
import com.getinsured.iex.org.nmhix.ssa.person.SingleStreamlinedApplication;
import com.getinsured.iex.org.nmhix.ssa.person.TaxHousehold;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.*;

/**
 * SSAP Utility Class.
 * TODO: !!! Refactor this to use Jackson JSON Parser to map to actual objects we have instead of this bad JsonObject/JsonArray type of programming!!!
 */
@Component
public class SSAPUtils {

  private static final Logger LOGGER = LoggerFactory.getLogger(SSAPUtils.class);

  public static final String KEY_ROOT_KEY = "IntialVerificationResponseTypeResponseSet";
  public static final String KEY_INITIAL_VERIFICATION_INDIVIDUAL_RESPONSE = "InitVerifIndividualResponse";
  public static final String KEY_RESPONSE_METADATA = "ResponseMetadata";
  public static final String KEY_LAWFULPRESENCE_VERIFIED_CODE = "LawfulPresenceVerifiedCode";
  public static final String KEY_INDIVIDUAL_DETAILS = "InitialVerificationIndividualResponseSet";
  private static Gson gson = new Gson();

  @Autowired
  private GIWSPayloadRepository giWSPayloadRepository;

  /**
   * @param value the string to check
   * @return true or false
   * @see SSAPUtils#isEmpty(String)
   * <p>
   * Checks for null or blank String objects
   */
  public static boolean isEmpty(String value) {
    return (value != null && !"".equals(value.trim())) ? false : true;
  }

  /**
   * @param ssn the ssn to be formatted
   * @return formatted ssn
   * @see SSAPUtils#formatSSN(String)
   * <p>
   * removes all '-' from SSN
   */
  public static String formatSSN(String ssn) {
    return ssn.replaceAll("[- ]+", "");
  }

  /**
   * @param date the date to check
   * @return true or false
   * @see SSAPUtils#isDateGreaterThanCurrentDate(String)
   * <p>
   * Checks if the given date is greater than current date
   */
  public static boolean isDateGreaterThanCurrentDate(String date) throws Exception {
    boolean status = false;

    try {
      SimpleDateFormat sdf = new SimpleDateFormat(SSAPConstants.DATE_FORMAT);
      Date inputDate = sdf.parse(date);
      Date currentDate = sdf.parse(sdf.format(new TSDate()));

      if (inputDate.compareTo(currentDate) > 0) {
        status = true;
      }
    } catch (ParseException exception) {
      LOGGER.error("Exception occurred while parsing date : ", exception);
      throw exception;
    }

    return status;
  }

  /**
   * @param date the date to check
   * @return true or false
   * @see SSAPUtils#isDateGreaterThanCurrentDateInMillis(String)
   * <p>
   * Checks if the given date is greater than current date
   */
  public static boolean isDateGreaterThanCurrentDateInMillis(String date) throws Exception {
    boolean status = false;

    try {
      if (!isEmpty(date)) {
        if (Long.valueOf(date) > TimeShifterUtil.currentTimeMillis()) {
          status = true;
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while comparing dates in Millis : ", exception);
      throw exception;
    }

    return status;
  }

  /**
   * @param throwable
   * @param json
   * @return payload
   * @see SSAPUtils#createGIWSPayload(Throwable, String)
   * <p>
   * creates GIWSPayload record
   */
  public static GIWSPayload createGIWSPayload(Exception exception, String json, String rule) {
    GIWSPayload payload = new GIWSPayload();
    payload.setExceptionMessage(exceptionToJson(exception));
    payload.setStatus(SSAPConstants.FAILURE);
    payload.setCreatedTimestamp(new TSDate());
    payload.setEndpointFunction(SSAPConstants.VERIFICATION_RULE);
    payload.setRequestPayload(json);
    payload.setEndpointOperationName(rule);

    return payload;
  }

  /**
   * @param ssapJson
   * @param hubJson
   * @param vlpJson
   * @param esiJson
   * @param nonEsiJson
   * @param rule
   * @return String
   * @see SSAPUtils#createRequestPayload(String, String, String, String, String, String)
   * <p>
   * creates Request object to save in GI_WS_PAYLOAD table
   */
  public static String createRequestPayload(String ssapJson, String hubJson, String vlpJson, String esiJson, String nonEsiJson, String rule) {
    StringBuffer buffer = new StringBuffer();

    if (!SSAPUtils.isEmpty(rule)) {
      if (rule.equals(SSAPConstants.RULES.MEC_VERIFICATION_RULE.toString())) {
        buffer.append("ssapJson=" + ssapJson);
        buffer.append("\n").append("esiJson=" + esiJson);
        buffer.append("\n").append("nonEsiJson=" + nonEsiJson);
      } else if (rule.equals(SSAPConstants.RULES.VLP_VERIFICATION_RULE.toString())) {
        buffer.append("ssapJson=" + ssapJson);
        buffer.append("\n").append("vlpJson=" + vlpJson);
      } else {
        buffer.append("ssapJson=" + ssapJson);
        buffer.append("\n").append("hubResponse=" + hubJson);
      }
    }

    return buffer.toString();
  }

  public static JsonArray getHouseholdMembers(JsonObject singleStreamlinedApplication) throws Exception {

    JsonArray taxHousehold;
    JsonArray householdMembers;

    try {
      taxHousehold = singleStreamlinedApplication.getAsJsonArray(SSAPConstants.TAX_HOUSEHOLD);

      householdMembers = taxHousehold.get(0).getAsJsonObject().get(SSAPConstants.HOUSEHOLD_MEMBER).getAsJsonArray();
    } catch (Exception exception) {
      throw exception;
    }

    return householdMembers;
  }

	/**
	 * Filters household members for verifications. We filter out if pre-eligibility
	 * determined that household member is CHIP or Medicare eligible AND not specifically
	 * selected to shop for QHP plans.
	 *
	 * @param singleStreamlinedApplication single streamlined application as {@link JsonObject}.
	 * @return {@link JsonArray} of filtered household members {@link HouseholdMember}.
	 */
	public static JsonArray getFilteredHouseholdMembers(JsonObject singleStreamlinedApplication) {
		JsonArray householdMembers = null;
		try {
			JsonArray taxHousehold = singleStreamlinedApplication.getAsJsonArray(SSAPConstants.TAX_HOUSEHOLD);
			com.getinsured.iex.ssap.TaxHousehold taxHousehold1 = gson.fromJson(taxHousehold.get(0), com.getinsured.iex.ssap.TaxHousehold.class);
			List<com.getinsured.iex.ssap.HouseholdMember> filtered = filterHouseholdMembersForVerifications(taxHousehold1);
			householdMembers = gson.toJsonTree(filtered).getAsJsonArray();
		}
		catch(Exception e) {
			throw e;
		}

		return householdMembers;
	}

	private static List<com.getinsured.iex.ssap.HouseholdMember> filterHouseholdMembersForVerifications(final com.getinsured.iex.ssap.TaxHousehold taxHousehold) {
    return taxHousehold.getHouseholdMember().stream().filter(
        m -> m.getApplyingForCoverageIndicator() != null && m.getApplyingForCoverageIndicator() == Boolean.TRUE && m.isSeeksQhp()
    ).collect(Collectors.toList());
	}
  /**
   * @param jsonString the JSON string to be parsed
   * @return Object representation of JSON
   * @throws Exception
   * @see SSAPUtils#parseJSON(String)
   * <p>
   * parsing the JSON with given String
   */
  public static SingleStreamlinedApplication parseJSON(String jsonString) throws Exception {
    LOGGER.info("Parse JSON : Starts");
    //Gson gson = new GsonBuilder().setDateFormat(SSAPConstants.DATE_FORMAT).create();
    Gson gson = new Gson();
    JsonParser jsonParser = new JsonParser();
    SingleStreamlinedApplication singleStreamlinedApplication = null;

    try {
      JsonObject jsonObject = jsonParser.parse(jsonString).getAsJsonObject();
      singleStreamlinedApplication = gson.fromJson(jsonObject.get(SSAPConstants.SINGLE_STREAMLINED_APPLICATION),
          SingleStreamlinedApplication.class);
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while parsing json : ", exception);
      throw exception;
    }

    LOGGER.info("Parse JSON : Ends");
    return singleStreamlinedApplication;
  }

  public static JsonObject parseVLPJSON(String jsonString) throws Exception {
    LOGGER.info("Parse JSON : Starts");
    //Gson gson = new GsonBuilder().setDateFormat(SSAPConstants.DATE_FORMAT).create();
    JsonParser jsonParser = new JsonParser();
    JsonObject jsonObject = null;

    try {
      jsonObject = jsonParser.parse(jsonString).getAsJsonObject();
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while parsing json : ", exception);
      throw exception;
    }

    LOGGER.info("Parse JSON : Ends");
    return jsonObject;
  }


  /**
   * @param jsonString the JSON string to be parsed
   * @return list of Household Members
   * @throws Exception
   * @see SSAPUtils#getListofHouseholdMembers(String)
   * <p>
   * Get list of Household Members from ssap json
   */
  public static List<HouseholdMember> getListofHouseholdMembers(String jsonString) throws Exception {
    LOGGER.info("Fetching list of Household Members from SSAP Json : Starts");
    List<HouseholdMember> householdMembers = null;
    JsonParser parser = new JsonParser();
    //Gson gson = new GsonBuilder().setDateFormat(SSAPConstants.DATE_FORMAT).create();
    Gson gson = new Gson();

    try {
      Object obj = parser.parse(jsonString);
      JsonObject jsonObject = (JsonObject) obj;

      JsonArray arrayOfHouseHoldMembers = (JsonArray) jsonObject.get(SSAPConstants.HOUSEHOLD_MEMBER);
      if (arrayOfHouseHoldMembers != null) {
        householdMembers = new ArrayList<HouseholdMember>();
        Iterator<JsonElement> iterator = arrayOfHouseHoldMembers.iterator();
        while (iterator.hasNext()) {
          householdMembers.add(gson.fromJson(iterator.next().getAsJsonObject(), HouseholdMember.class));
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching list of Household Members from SSAP Json : ", exception);
      throw exception;
    }

    LOGGER.info("Fetching list of Household Members from SSAP Json : Starts");
    return householdMembers;
  }

  /**
   * @param jsonString the JSON string to be parsed
   * @return SsapApplicant
   * @throws Exception
   * @see SSAPUtils#parseSsapApplicantJSON(String)
   * <p>
   * Get SsapApplicant DTO from Json
   */
  public static SsapApplicant parseSsapApplicantJSON(String jsonString) throws Exception {
    LOGGER.info("Parse SSAP Applicant Json : Starts");
    Gson gson = new Gson();
    JsonParser jsonParser = new JsonParser();
    SsapApplicant ssapApplicant = null;

    try {
      JsonObject jsonObject = jsonParser.parse(jsonString).getAsJsonObject();
      ssapApplicant = gson.fromJson(jsonObject.get(SSAPConstants.SSAP_APPLICANT),
          SsapApplicant.class);
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while parsing SSAP Applicant Json : ", exception);
      throw exception;
    }

    LOGGER.info("Parse SSAP Applicant Json : Ends : ");
    return ssapApplicant;
  }

  /**
   * @param ssn
   * @param node
   * @param jsonString
   * @return String
   * @throws Exception
   * @see SSAPUtils#searchHubResponseJson(String, String, String)
   * <p>
   * search Hub Response Json for a particular member/individual for a given value
   */
  public static String searchHubResponseJson(String node, String jsonString) throws Exception {
    LOGGER.info("Search Hub Json : Starts");

    ObjectMapper mapper = new ObjectMapper();
    String childNode = null;
    String value = null;
    JsonParser parser = new JsonParser();
    String individualResponse;

    try {
      if (!isEmpty(node) && !isEmpty(jsonString)) {

        Object obj = parser.parse(jsonString);
        JsonObject jsonObject = (JsonObject) obj;

        //String individualResponse = getIndividualResponse(ssn, jsonString);
        individualResponse = jsonObject.get(SSAPConstants.SSA_COMPOSITE_INDIVIDUAL_RESPONSE).getAsJsonObject().toString();

        if (!isEmpty(individualResponse)) {
          JsonNode root = mapper.readTree(individualResponse);
          String[] nodes = node.split(SSAPConstants.SLASH);
          JsonNode rootNode = root.get(nodes[0]);

          if (nodes.length > 1) {
            for (int i = 1; i < nodes.length; i++) {
              childNode = nodes[i];
              if (rootNode != null && rootNode.has(childNode)) {
                if (i == (nodes.length - 1)) {
                  value = rootNode.get(childNode).asText();
                } else {
                  rootNode = rootNode.get(childNode);
                }
              }
            }
          } else {
            value = rootNode.toString();
          }
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while searching Hub Response Json by Node : ", exception);
      throw exception;
    }

    LOGGER.info("Search Hub Json : Ends");
    return value;
  }

  /**
   * @param ssn
   * @param indicator
   * @param json
   * @return
   * @throws Exception
   * @see SSAPUtils#getESIIndicatorValue(String, String, String)
   * <p>
   * Search in the ESI json by given indicator and ssn
   */
  public static String getESIIndicatorValue(String indicator, String json) throws Exception {
    LOGGER.info("Get ESI Indicator by SSN : Starts ");

    JsonParser parser = new JsonParser();
    String indicatorValue = null;
    JsonObject mecInfo = null;
    JsonObject applicantResponse = null;

    try {
      if (!SSAPUtils.isEmpty(json)) {
        Object obj = parser.parse(json);
        JsonObject jsonObject = (JsonObject) obj;

        mecInfo = jsonObject.get(SSAPConstants.MEC_INFO).getAsJsonObject();
        applicantResponse = mecInfo.get(SSAPConstants.APPLICANT_RESPONSE).getAsJsonObject();
        indicatorValue = applicantResponse.get(indicator).getAsString();
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching ESI Indicator : ", exception);
      throw exception;
    }

    LOGGER.info("Get ESI Indicator by SSN : Ends");
    return indicatorValue;
  }

  /**
   * @param json
   * @return
   * @see SSAPUtils#checkMECVerificationCode(String, String)
   * <p>
   * Checks MEC Verification codes for given SSN in NON-ESI json
   */
  public static boolean checkMecVerificationCode(String json) throws Exception {
    LOGGER.info("Check MEC Verification Code : Starts");

    JsonParser parser = new JsonParser();
    JsonObject mecCoverage = null;
    String mecVerificationCode = null;
    String orgCode = null;

    try {
      if (!SSAPUtils.isEmpty(json)) {
        Object obj = parser.parse(json);
        JsonObject jsonObject = (JsonObject) obj;
        JsonArray response = jsonObject.get(SSAPConstants.INDIVIDUAL_RESPONSE_TYPE).getAsJsonArray();

        JsonArray otherCoverages = (JsonArray) response.get(0).getAsJsonObject().get(SSAPConstants.OTHER_COVERAGES);
        Iterator<JsonElement> itr = otherCoverages.iterator();
        while (itr.hasNext()) {
          JsonObject res = itr.next().getAsJsonObject();
          orgCode = res.get(SSAPConstants.ORGANIZATION_CODE).getAsString();

          if (!SSAPUtils.isEmpty(orgCode) && checkOrganizationCode(orgCode)) {
            mecCoverage = res.get(SSAPConstants.MEC_COVERAGE_TYPE).getAsJsonObject();

            mecVerificationCode = mecCoverage.get(SSAPConstants.MEC_VERIFICATION_CODE).getAsString();

            if (!SSAPUtils.isEmpty(mecVerificationCode) && ("Y".equals(mecVerificationCode) || "P".equals(mecVerificationCode))) {
              return true;
            }
          }
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while checking MEC Verification Code : ", exception);
      throw exception;
    }
    LOGGER.info("Check MEC Verification Code : Ends");

    return false;
  }

  /**
   * @param personId
   * @param json
   * @return
   * @throws Exception
   * @see SSAPUtils#getLawfulPresenceVerifiedCode(String, String)
   * <p>
   * Search for LawfulPresenceVerifiedCode in VLP json by personId
   */
  public static String getLawfulPresenceVerifiedCode(String personId, String json) throws Exception {
    LOGGER.info("Get Lawful Presence Verified Code : Starts.");

    JsonParser parser = new JsonParser();
    JsonObject agency3InitVerifResponseSet = null;
    String lawfulPresenceVerifiedCode = null;

    try {
      Object obj = parser.parse(json);
      JsonObject jsonObject = (JsonObject) obj;

      if (jsonObject.has(SSAPConstants.AGENCY3_INIT_VERIF_RESPONSE_SET)) {
        agency3InitVerifResponseSet = jsonObject.get(SSAPConstants.AGENCY3_INIT_VERIF_RESPONSE_SET).getAsJsonObject();

        JsonArray arrayOfIndividualResponse = (JsonArray) agency3InitVerifResponseSet.get(SSAPConstants.AGENCY3_INIT_VERIF_INDIVIDUAL_RESPONSE);
        Iterator<JsonElement> iterator = arrayOfIndividualResponse.iterator();
        while (iterator.hasNext()) {
          JsonObject response = iterator.next().getAsJsonObject();
          //applicationAndPersonId = response.get(SSAPConstants.APPLICATION_PERSON_ID).getAsString();
          //personID = applicationAndPersonId.substring(applicationAndPersonId.indexOf(SSAPConstants.UNDERSCORE)+1);

          //if(personId.equals(personID)){
          lawfulPresenceVerifiedCode = response.get(SSAPConstants.LAWFUL_PRESENCE_VERIFIED_CODE).getAsString();
          break;
          //}
        }
      } else if (jsonObject.has(KEY_ROOT_KEY)) // New JSON response from HUB
      {
        JsonElement individualResponseSet = jsonObject.get(KEY_ROOT_KEY);

        if (individualResponseSet.isJsonObject() &&
            individualResponseSet.getAsJsonObject().has(KEY_INITIAL_VERIFICATION_INDIVIDUAL_RESPONSE)) {
          JsonElement individualResponse = individualResponseSet.getAsJsonObject().get(KEY_INITIAL_VERIFICATION_INDIVIDUAL_RESPONSE);

          if (individualResponse.isJsonArray()) {
            JsonArray individualElements = individualResponse.getAsJsonArray();
            if (individualElements.size() > 0) {
              JsonElement individualElement = individualElements.get(0);
              if (individualElement.isJsonObject()) {
                JsonObject individual = individualElement.getAsJsonObject();
                if (individual.has(KEY_LAWFULPRESENCE_VERIFIED_CODE)) {
                  lawfulPresenceVerifiedCode = individual.get(KEY_LAWFULPRESENCE_VERIFIED_CODE).getAsString();
                }
              }
            }
          }
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching Lawful Presence Verified Code : ", exception);
      throw exception;
    }

    LOGGER.info("Get Lawful Presence Verified Code : Ends ");
    return lawfulPresenceVerifiedCode;
  }

  /**
   * @param personId
   * @param json
   * @return
   * @throws Exception
   * @see SSAPUtils#checkPersonInVLPResponse(String, String)
   * <p>
   * Checks if a given person Id is present in VLP json
   */
  public static boolean checkPersonInVLPResponse(String personId, String json) throws Exception {
    LOGGER.info("Check Person ID in VLP Response : Starts.");

    JsonParser parser = new JsonParser();
    JsonObject agency3InitVerifResponseSet = null;
    String applicationAndPersonId = null;
    String personID = null;
    boolean isPresent = false;

    try {
      Object obj = parser.parse(json);
      JsonObject jsonObject = (JsonObject) obj;

      agency3InitVerifResponseSet = jsonObject.get(SSAPConstants.AGENCY3_INIT_VERIF_RESPONSE_SET).getAsJsonObject();

      JsonArray arrayOfIndividualResponse = (JsonArray) agency3InitVerifResponseSet.get(SSAPConstants.AGENCY3_INIT_VERIF_INDIVIDUAL_RESPONSE);
      Iterator<JsonElement> iterator = arrayOfIndividualResponse.iterator();
      while (iterator.hasNext()) {
        JsonObject response = iterator.next().getAsJsonObject();
        applicationAndPersonId = response.get(SSAPConstants.APPLICATION_PERSON_ID).getAsString();
        personID = applicationAndPersonId.substring(applicationAndPersonId.indexOf(SSAPConstants.UNDERSCORE) + 1);

        if (personId.equals(personID)) {
          isPresent = true;
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while checking Person ID in VLP Response : ", exception);
      throw exception;
    }

    LOGGER.info("Check Person ID in VLP Response : Ends");
    return isPresent;
  }

  /**
   * @param personId HouseHoldMember/Person ID
   * @param json     SSAP JSON
   * @return HouseholdMember
   * @throws Exception
   * @see SSAPUtils#getHouseHoldMemberById(int, String)
   * <p>
   * Fetch HouseHoldMember/Person from SSAP Application JSON
   */
  public static HouseholdMember getHouseHoldMemberById(int personId, String json) throws Exception {
    LOGGER.info("Get Household Member by Person ID : Starts");
    SingleStreamlinedApplication singleStreamlinedApplication = null;
    HouseholdMember member = null;

    try {
      singleStreamlinedApplication = parseJSON(json);

      if (singleStreamlinedApplication != null) {
        for (TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {
          for (HouseholdMember householdMember : taxHousehold.getHouseholdMember()) {
            if (personId == householdMember.getPersonId()) {
              member = householdMember;
              break;
            }
          }
          break;
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching Household Member by Person ID : ", exception);
      throw exception;
    }

    LOGGER.info("Get Household Member by Person ID : Ends : ");
    return member;
  }

  /**
   * @param singleStreamlinedApplication
   * @return List<Integer>
   * @throws Exception
   * @see SSAPUtils#getListOfTaxFilerDependants(SingleStreamlinedApplication)
   * <p>
   * Fetch Tax Filer Dependants
   */
  public static List<String> getListOfTaxFilerDependants(JsonObject member, String primaryTaxFilerPersonId) throws Exception {
    LOGGER.info("Get Tax Filer Dependants : Starts");
    JsonObject taxFiler = null;
    List<String> dependants = new ArrayList<String>();
    JsonArray taxFilerDependants;
    JsonObject dependantHouseholdMemeberId;

    try {
      if (member != null) {
        taxFiler = member.get(SSAPConstants.TAX_FILER).getAsJsonObject();
        taxFilerDependants = taxFiler.get(SSAPConstants.TAX_FILER_DEPENDENTS).getAsJsonArray();

        Iterator<JsonElement> iterator = taxFilerDependants.iterator();
        while (iterator.hasNext()) {
          dependantHouseholdMemeberId = iterator.next().getAsJsonObject();
          dependants.add(dependantHouseholdMemeberId.get(SSAPConstants.DEPENDANT_HOUSEHOLD_MEMBER_ID).getAsString());
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching Tax Filer Dependants : ", exception);
      throw exception;
    }

    LOGGER.info("Get Tax Filer Dependants : Ends");
    return dependants;
  }
	
	/*public static List<Integer> getListOfTaxFilerDependants(SingleStreamlinedApplication singleStreamlinedApplication) throws Exception{
		LOGGER.info("Get Tax Filer Dependants : Starts");
		HouseholdMember taxFiler = null;
		List<TaxFilerDependants> listOfTaxFilerDependants = new ArrayList<TaxFilerDependants>();
		List<Integer> dependants = new ArrayList<Integer>();
		
		try{
			taxFiler = getTaxFiler(singleStreamlinedApplication);
			
			if(taxFiler!=null){
				listOfTaxFilerDependants = taxFiler.getTaxFiler().getTaxFilerDependants();
				
				for(TaxFilerDependants dependant: listOfTaxFilerDependants){
					dependants.add(Integer.valueOf(dependant.getDependantHouseholdMemeberId()));
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while fetching Tax Filer Dependants : " , exception);
			throw exception;
		}
		
		LOGGER.info("Get Tax Filer Dependants : Ends");
		return dependants;
	}*/


  /**
   * @param node           given String/Node to be searched against
   * @param hubResponseXML Hub Response XML
   * @return node value as String
   * @throws Exception
   * @see SSAPUtils#searchHUBResponse(String, String)
   * <p>
   * Search value of given node in Hub Response XML
   */
  public static String searchHUBResponseXML(String node, String hubResponseXML) throws Exception {
    LOGGER.info("Search Hub Response by given Node : Starts");
    String nodeValue = null;

    try {
      XPathFactory xPathFactory = XPathFactory.newInstance();
      XPath xpath = xPathFactory.newXPath();
      nodeValue = xpath.evaluate(node, convertStringToDocument(hubResponseXML));
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while searching Hub Response : ", exception);
      throw exception;
    }

    LOGGER.info("Search Hub Response by given Node : Ends");
    return nodeValue;
  }

  /**
   * @param json the JSON string to be parsed
   * @return SsapApplicant
   * @throws Exception
   * @see SSAPUtils#getApplicant(String)
   * <p>
   * Get SsapApplicant DTO from Json
   */
  public static SsapApplicant getApplicant(String json) throws Exception {
    LOGGER.info("Get SSAP Applicant : Starts");
    SsapApplicant ssapApplicant = null;
    JsonParser parser = new JsonParser();
    Gson gson = new Gson();
    JsonArray arrayOfApplicants = null;

    try {
      Object obj = parser.parse(json);
      JsonObject jsonObject = (JsonObject) obj;
      JsonObject embeddedObject = jsonObject.get(SSAPConstants.EMBEDDED).getAsJsonObject();

      if (embeddedObject != null) {
        arrayOfApplicants = (JsonArray) embeddedObject.get(SSAPConstants.SSAP_APPLICANT);
        Iterator<JsonElement> iterator = arrayOfApplicants.iterator();
        while (iterator.hasNext()) {
          JsonObject ssapApplicantJson = iterator.next().getAsJsonObject();
          ssapApplicant = gson.fromJson(ssapApplicantJson, SsapApplicant.class);
          ssapApplicant.setId(getIdFromJson(ssapApplicantJson));
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching SsapApplicant from Json : ", exception);
      throw exception;
    }

    LOGGER.info("Get SSAP Applicant : Ends : ");
    return ssapApplicant;
  }

  /**
   * Convert exception trace to Json
   *
   * @param e
   * @return
   */
  @SuppressWarnings("unchecked")
  private static String exceptionToJson(Exception exception) {
    if (exception == null) {
      return "None";
    }
    JSONObject obj = new JSONObject();
    JSONObject error = new JSONObject();
    error.put("Exception Type", exception.getClass().getName());
    error.put("ExceptionMessage", exception.getMessage());
    error.put("ExceptionCause", exception.getCause());
    obj.put("Exception", error);

    String response = obj.toJSONString();
    response = response.replaceAll("\\\\", "");
    response = response.replaceAll("(\"\\{)", "\\{");
    response = response.replaceAll("(\\}\")", "\\}");

    return response;
  }

  /**
   * Fetch Individual Response from Composite Response by SSN
   *
   * @param ssn
   * @param json
   * @return
   * @throws Exception
   */
  public static boolean checkPersonSSN(String ssn, String json) throws Exception {
    LOGGER.info("Get Individual Response from Hub Json by SSN : Starts ");
    JsonParser parser = new JsonParser();
    boolean ssnMatch = false;

    try {
      Object obj = parser.parse(json);
      JsonObject jsonObject = (JsonObject) obj;
      JsonObject individualResponse = jsonObject.get(SSAPConstants.SSA_COMPOSITE_INDIVIDUAL_RESPONSE).getAsJsonObject();
      String personSSN = individualResponse.get(SSAPConstants.PERSON_SSN_IDENTIFICATION).getAsString();

      if (ssn.equals(personSSN)) {
        ssnMatch = true;
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching Individual Response : ", exception);
      throw exception;
    }

    LOGGER.info("Get Household Member by Person ID : Ends");
    return ssnMatch;
  }

  /**
   * Fetch Individual Response from Composite Response by SSN
   *
   * @param ssn
   * @param json
   * @return
   * @throws Exception
   */
  public static boolean checkPersonSSNInMEC(String ssn, String json) throws Exception {
    LOGGER.info("Get Individual Response from Hub Json by SSN : Starts");
    JsonParser parser = new JsonParser();
    boolean ssnMatch = false;

    try {
      Object obj = parser.parse(json);
      JsonObject jsonObject = (JsonObject) obj;
      JsonArray individualResponse = jsonObject.get(SSAPConstants.INDIVIDUAL_RESPONSE_TYPE).getAsJsonArray();
      JsonObject applicant = individualResponse.get(0).getAsJsonObject().get(SSAPConstants.APPLICANT_TYPE).getAsJsonObject();
      String personSSN = applicant.get(SSAPConstants.PERSON_SSN_IDENTIFICATION).getAsString();

      if (ssn.equals(personSSN)) {
        ssnMatch = true;
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching Individual Response : ", exception);
      throw exception;
    }

    LOGGER.info("Get Household Member by Person ID : Ends");
    return ssnMatch;
  }

  /**
   * Fetch ESI json for a particular person
   *
   * @param ssn
   * @param json
   * @return
   * @throws Exception
   */
  private static String getESIResponseBySSN(String ssn, String json) throws Exception {
    LOGGER.info("Get ESI Response by SSN : Starts");
    String value = null;
    JsonParser parser = new JsonParser();

    try {
      Object obj = parser.parse(json);
      JsonObject jsonObject = (JsonObject) obj;
      String personSsnIdentification = jsonObject.get(SSAPConstants.PERSON).getAsJsonObject().get(SSAPConstants.SSN.toUpperCase()).getAsString();
      if (ssn.equals(personSsnIdentification)) {
        value = jsonObject.toString();
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching ESI Response : ", exception);
      throw exception;
    }

    LOGGER.info("Get ESI Response by SSN : Ends");
    return value;
  }

  /**
   * Fetch MEC Verification Code from non-esi json
   *
   * @param ssn
   * @param organizationCode
   * @param json
   * @return
   * @throws Exception
   */
  private static String getMecVerificationCode(String ssn, String organizationCode, String json) throws Exception {
    LOGGER.info("Get MEC Verification Code by SSN : Starts ");

    JsonParser parser = new JsonParser();
    JsonObject mecCoverage = null;
    JsonObject otherCoverage = null;
    JsonObject individualResponseSet = null;
    JsonObject individualResponse = null;
    String orgCode = null;

    try {
      if (!SSAPUtils.isEmpty(json)) {
        Object obj = parser.parse(json);
        JsonObject jsonObject = (JsonObject) obj;

        individualResponseSet = jsonObject.get(SSAPConstants.INDIVIDUAL_RESPONSE_SET).getAsJsonObject();
        individualResponse = individualResponseSet.get(SSAPConstants.INDIVIDUAL_RESPONSE).getAsJsonObject();
        otherCoverage = individualResponse.get(SSAPConstants.OTHER_COVERAGE).getAsJsonObject();
        orgCode = otherCoverage.get(SSAPConstants.ORGANIZATION_CODE).getAsString();

        if (!SSAPUtils.isEmpty(orgCode) && organizationCode.equals(orgCode)) {
          mecCoverage = otherCoverage.get(SSAPConstants.MEC_COVERAGE_TYPE).getAsJsonObject();

          return mecCoverage.get(SSAPConstants.MEC_VERIFICATION_CODE).getAsString();
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching MEC Verification Code Response : ", exception);
      throw exception;
    }

    LOGGER.info("Get MEC Verification Code by SSN : Ends");
    return null;
  }

  /**
   * @param singleStreamlinedApplication
   * @return HouseholdMember
   * @throws Exception
   * @see SSAPUtils#getTaxFiler(SingleStreamlinedApplication)
   * <p>
   * Fetch Tax Filer
   */
  public static JsonObject getTaxFiler(JsonArray householdMembers, String primaryTaxFilerPersonId) throws Exception {
    LOGGER.info("Get Tax Filer : Starts");
    JsonObject householdMember;
    String personId;

    try {
      Iterator<JsonElement> iterator = householdMembers.iterator();
      while (iterator.hasNext()) {
        householdMember = iterator.next().getAsJsonObject();
        personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
        if (primaryTaxFilerPersonId.equals(personId)) {
          return householdMember;
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching Household Member by Person ID : ", exception);
      throw exception;
    }

    LOGGER.info("Get Household Member by Person ID : Ends ");
    return null;
  }

  /**
   * Converts XML string to XML document
   *
   * @param xmlStr
   * @return
   * @throws Exception
   */
  private static Document convertStringToDocument(String xmlStr) throws Exception {
    LOGGER.info("Convert XML string to Document : Starts");
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;
    Document doc = null;

    try {
      builder = factory.newDocumentBuilder();
      doc = builder.parse(new InputSource(new StringReader(xmlStr)));
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while converting xml String to Document : ", exception);
      throw exception;
    }

    LOGGER.info("Convert XML string to Document : Ends");
    return doc;
  }

  /**
   * Fetch the application id from SSAPApplicant json
   *
   * @param jsonObject
   * @return
   * @throws Exception
   */
  private static long getIdFromJson(JsonObject jsonObject) throws Exception {
    LOGGER.info("Get SSAP Applicant ID : Starts");
    long ssapApplicantRowId = 0;
    String url;

    try {
      url = jsonObject.get(SSAPConstants.LINKS).getAsJsonObject().get(SSAPConstants.SELF).getAsJsonObject().get(SSAPConstants.HREF).getAsString();

      if (!isEmpty(url)) {
        ssapApplicantRowId = Long.valueOf(url.substring(url.lastIndexOf(SSAPConstants.SLASH) + 1));
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching SsapApplicant ID from Json : ", exception);
      throw exception;
    }

    LOGGER.info("Get SSAP Applicant ID : Ends : ");
    return ssapApplicantRowId;
  }

  private static boolean checkOrganizationCode(String orgCode) {
    boolean checkOrgCode = false;

    try {
      List<String> organizationCodes = new ArrayList<String>();
      organizationCodes.add("MEDI");
      organizationCodes.add("TRIC");
      organizationCodes.add("PECO");
      organizationCodes.add("BHPC");
      organizationCodes.add("VHAC");

      if (organizationCodes.contains(orgCode)) {
        checkOrgCode = true;
      }
    } catch (Exception exception) {
      LOGGER.error("Exception in method checkOrganizationCode : ", exception);
    }

    return checkOrgCode;
  }

  // Will be used for testing purpose
  public static String convertDocumentToString() {
    TransformerFactory tf = TransformerFactory.newInstance();
    Transformer transformer;
    StringWriter writer = null;
    try {
      transformer = tf.newTransformer();
      writer = new StringWriter();
      transformer.transform(new DOMSource(loadXML()), new StreamResult(writer));
      String output = writer.getBuffer().toString();

      return output;
    } catch (TransformerException e) {
      LOGGER.error("Exception occurred while converting doc to string : ", e);
    } finally {
      IOUtils.closeQuietly(writer);
    }
    return null;
  }


  // Will be used for testing purpose
  public static Document loadXML() {
    String xmlFilePath = "E:\\STATE_MAIN_IEX\\NON_ESI.xml";
    // String xmlFilePath = "/SSACompositeMaximumResponse.xml";
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder;
    Document doc = null;
    try {
      documentBuilder = documentBuilderFactory.newDocumentBuilder();
      doc = documentBuilder.parse(new File(xmlFilePath));
    } catch (ParserConfigurationException e) {
      LOGGER.error("Exception occurred while loading xml : ", e);
    } catch (SAXException e) {
      LOGGER.error("Exception occurred while loading xml : ", e);
    } catch (IOException e) {
      LOGGER.error("Exception occurred while loading xml : ", e);
    }

    return doc;
  }

  public static JsonObject getHouseholdMember(String jsonString) throws Exception {

    JsonParser parser = new JsonParser();
    JsonObject singleStreamlinedApplication;
    JsonArray taxHousehold;
    JsonArray householdMembers;

    try {
      Object obj = parser.parse(jsonString);
      JsonObject jsonObject = (JsonObject) obj;

      singleStreamlinedApplication = jsonObject.get(SSAPConstants.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();

      taxHousehold = singleStreamlinedApplication.getAsJsonArray(SSAPConstants.TAX_HOUSEHOLD);

      householdMembers = taxHousehold.get(0).getAsJsonObject().get(SSAPConstants.HOUSEHOLD_MEMBER).getAsJsonArray();
    } catch (Exception exception) {
      throw exception;
    }

    return householdMembers.get(0).getAsJsonObject();
  }

  public static String getHouseholdIncome(JsonObject singleStreamlinedApplication) throws Exception {

    String houseHoldIncome;
    JsonArray taxHousehold;

    try {
      taxHousehold = singleStreamlinedApplication.getAsJsonArray(SSAPConstants.TAX_HOUSEHOLD);

      houseHoldIncome = taxHousehold.get(0).getAsJsonObject().get(SSAPConstants.HOUSEHOLD_INCOME).getAsString();
    } catch (Exception exception) {
      throw exception;
    }

    return houseHoldIncome;
  }

  public static String getApplicationIdFromSsapJson(JsonObject singleStreamlinedApplication) {
    String ssapApplicationId;

    try {
      ssapApplicationId = singleStreamlinedApplication.get(SSAPConstants.SSAP_APPLICATION_ID).getAsString();
    } catch (Exception exception) {
      throw exception;
    }

    return ssapApplicationId;
  }

  public static String getTaxReturnMAGIAmount(String hubResponse) {
    JsonParser parser = new JsonParser();
    JsonObject household;
    JsonArray applicantVerification;
    JsonObject taxReturn;
    String taxReturnMAGIAmount = null;

    try {
      Object obj = parser.parse(hubResponse);
      JsonObject jsonObject = (JsonObject) obj;

      household = jsonObject.get(SSAPConstants.HOUSEHOLD).getAsJsonObject();
      applicantVerification = household.get(SSAPConstants.APPLICANT_VERIFICATION).getAsJsonArray();

      Iterator<JsonElement> iterator = applicantVerification.iterator();
      while (iterator.hasNext()) {
        taxReturn = iterator.next().getAsJsonObject().get(SSAPConstants.TAX_RETURN).getAsJsonObject();
        taxReturnMAGIAmount = taxReturn.get(SSAPConstants.TAX_RETURN_MAGI_AMOUNT).getAsString();
        break;
      }

    } catch (Exception exception) {
      throw exception;
    }

    return taxReturnMAGIAmount;
  }

  public static String getIRSIncome(String hubResponse) {
    JsonParser parser = new JsonParser();
    JsonObject household;
    String irsIncome = null;

    try {
      Object obj = parser.parse(hubResponse);
      JsonObject jsonObject = (JsonObject) obj;

      household = jsonObject.get(SSAPConstants.HOUSEHOLD).getAsJsonObject();
      irsIncome = household.get(SSAPConstants.IRS_INCOME).getAsString();
    } catch (Exception exception) {
      throw exception;
    }

    return irsIncome;
  }

  public static boolean checkIRSCompatibilityRange(float houseHoldIncomeAmount, float iRSIncome) {
    float difference;
    int range = SSAPConstants.IRS_INCOME_COMPATIBILITY_RANGE;
    boolean flag = false;

    try {
      difference = iRSIncome - houseHoldIncomeAmount;
      if (difference <= (houseHoldIncomeAmount * range / 100)) {
        flag = true;
      }
    } catch (Exception exception) {
      throw exception;
    }

    return flag;
  }

  public static boolean checkEquifaxCompatibilityRange(float houseHoldIncomeAmount, float aggregatedCompensation) {
    float difference;
    int range = SSAPConstants.EQUIFAX_COMPATIBILITY_RANGE;
    boolean flag = false;

    try {
      difference = aggregatedCompensation - houseHoldIncomeAmount;
      if (difference <= (houseHoldIncomeAmount * range / 100)) {
        flag = true;
      }
    } catch (Exception exception) {
      throw exception;
    }

    return flag;
  }

  public static JsonObject getSingleStreamlinedApplication(String ssapJson) {
    JsonParser parser = new JsonParser();
    JsonObject singleStreamlinedApplication;

    try {
      Object obj = parser.parse(ssapJson);
      JsonObject jsonObject = (JsonObject) obj;

      singleStreamlinedApplication = jsonObject.get(SSAPConstants.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
    } catch (Exception exception) {
      throw exception;
    }

    return singleStreamlinedApplication;
  }

  public static String getLink(SsapApplicantResource ssapApplicantResource) throws Exception {
    String link = null;
    try {
      Object links = ssapApplicantResource.get_links();

      if (links != null) {
        JSONObject jsonObjectLinks = new JSONObject((Map<String, String>) links);
        Map<String, String> self = (Map<String, String>) jsonObjectLinks.get("self");
        String selfHref = (self.get("href") == null) ? "" : self.get("href").toString();
        URI myUri = null;
        if ("".equals(selfHref)) {
          throw new Exception("The end point to update the applicant is incorrect or not defined");
        } else {
          myUri = URI.create(selfHref);
          link = myUri.toString();
        }
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching SsapApplicant ID from Json : ", exception);
      throw exception;
    }

    return link;
  }

  public static long getIdFromLink(String link) throws Exception {
    LOGGER.info("Get SSAP Applicant ID by parsing given link : Starts");
    long ssapApplicantRowId = 0;

    try {

      if (!isEmpty(link)) {
        ssapApplicantRowId = Long.valueOf(link.substring(link.lastIndexOf(SSAPConstants.SLASH) + 1));
      }
    } catch (Exception exception) {
      LOGGER.error("Exception occurred while fetching ID from Link : ", exception);
      throw exception;
    }

    LOGGER.info("Get SSAP Applicant ID by parsing given link : Ends : ");
    return ssapApplicantRowId;
  }

  public static String getStateCode() {
    return DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
  }

  // Will be used for testing purpose
  public static String loadSSAPJSON() {
    Gson gson = new Gson();
    JsonParser jsonParser = new JsonParser();
    String json = null;
    try {
      String jsonPath = "C:\\Users\\pradhan_b\\Desktop\\Equifax Rule\\ssap.json";
      JsonObject jsonObject = jsonParser.parse(new FileReader(jsonPath)).getAsJsonObject();
      json = gson.toJson(jsonObject);
    } catch (Exception e) {
      LOGGER.error("Exception occurred while loading SSAP JSON data : ", e);
    }
    return json;
  }

  // Will be used for testing purpose
  public static String loadHUBJSON() {
    Gson gson = new Gson();
    JsonParser jsonParser = new JsonParser();
    String json = null;
    try {
      String jsonPath = "C:\\Users\\pradhan_b\\Desktop\\Equifax Rule\\hub_response.json";
      JsonObject jsonObject = jsonParser.parse(new FileReader(jsonPath)).getAsJsonObject();
      json = gson.toJson(jsonObject);
    } catch (Exception e) {
      LOGGER.error("Exception occurred while loading HUB JSON data : ", e);
    }
    return json;
  }

  // Will be used for testing purpose
  public static String loadESIJSON() {
    Gson gson = new Gson();
    JsonParser jsonParser = new JsonParser();
    String json = null;
    try {
      String jsonPath = "E:\\STATE_MAIN_IEX\\Testing\\new MEC Json\\MEC_Response.json";
      JsonObject jsonObject = jsonParser.parse(new FileReader(jsonPath)).getAsJsonObject();
      json = gson.toJson(jsonObject);
    } catch (Exception e) {
      LOGGER.error("Exception occurred while loading ESI JSON data : ", e);
    }
    return json;
  }

  // Will be used for testing purpose
  public static String loadNonESIJSON() {
    Gson gson = new Gson();
    JsonParser jsonParser = new JsonParser();
    String json = null;
    FileReader fr = null;
    try {
      String jsonPath = "E:\\STATE_MAIN_IEX\\Testing\\new MEC Json\\NMEC_Response.json";
      fr = new FileReader(jsonPath);
      //JsonObject jsonObject = jsonParser.parse(new FileReader(jsonPath)).getAsJsonObject();
      JsonObject jsonObject = jsonParser.parse(fr).getAsJsonObject();
      json = gson.toJson(jsonObject);
    } catch (Exception e) {
      LOGGER.error("Exception occurred while loading Non ESI JSON data : ", e);
    } finally {
      IOUtils.closeQuietly(fr);
    }
    return json;
  }

  // Will be used for testing purpose
  public static String loadVLPJSON() {
    Gson gson = new Gson();
    JsonParser jsonParser = new JsonParser();
    String json = null;
    FileReader fr = null;
    try {
      String jsonPath = "E:\\STATE_MAIN_IEX\\Testing\\new MEC Json\\vlp_json.json";
      fr = new FileReader(jsonPath);
      //JsonObject jsonObject = jsonParser.parse(new FileReader(jsonPath)).getAsJsonObject();
      JsonObject jsonObject = jsonParser.parse(fr).getAsJsonObject();
      json = gson.toJson(jsonObject);
    } catch (Exception e) {
      LOGGER.error("Exception occurred while loading VLP JSON data : ", e);
    } finally {
      IOUtils.closeQuietly(fr);
    }
    return json;
  }
}
