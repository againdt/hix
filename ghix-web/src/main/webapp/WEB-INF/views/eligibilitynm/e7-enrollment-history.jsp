
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View account</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link type="text/css" rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">


<div class="row-fluid" id="main">
    <div class="row-fluid" id="titlebar">
      <div class="span3">      
         <a class="btn btn-link" href="e1-1-start-app">Back </a>
      </div>
      <div class="span9 pull-left">
        <h1>Your Account Dashboard</h1>
      </div>
    </div>
    
    <!--titlebar-->
    <div class="row-fluid">
      <div class="span3 gutter10" id="sidebar">
      <div class="gutter10">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li><a href="e7-view-account">Overview <i class="icon-chevron-right pull-right"></i></a></li>
          <li class="active"><a href="e7-enrollment-history">Enrollment History <i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="#">Household Profile<i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="e8-changes-in-circumstance">Change in Circumstance <i class="icon-chevron-right"></i></a></li>
          <li><a href="#">Inbox <i class="icon-chevron-right pull-right"></i></a></li>
          <li><a href="#">Account Settings <i class="icon-chevron-right pull-right"></i></a></li>
        </ul>
        </div><!-- gutter10 -->
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
        <div class="gutter10">
        <h3>2014 Enrollment Progress</h3>

        <p>Your application is complete. Janet Jacobs and Joe Jacobs have been determined eligible for health coverage and help paying for health coverage. However, coverage can't begin until they have selected a health plan.</p>

        <img src="../resources/eligibilitynm/img/progress.png" alt="Progress bar">
        <form class="form-vertical">
          <div class="well-small">
            Your Current Eligibility Determination <a href="e9-confirmation" class="btn"><i class="icon-file"></i></a>
            <a href="e9-confirmation" class="btn offset3">Complete Enrollment</a>
          </div>
        </form>

    <!--gutter10-->
  </div>
  <!--rightpanel-->
</div>
<!--row-fluid-->
</div>
<!--main-->
</div>


<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script>
<script src="../resources/eligibilitynm/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/jquery.multiselect.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-modal.js"></script>
<script>
$(document).ready(function(){
$('.qualify').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});
$('.date').datepicker();
});
</script>
</body>
</html>
