(function(){
	'use strict';
	
	angular.module('dashboardAnnouncementApp')
	.filter("endWith", endWith);

	function endWith(){
		return function(items, end) {
    		end = +end; //parse to int
            return items.slice(end-5, end);
	    }
	}
})();