package com.getinsured.hix.batch.notification.ssap;

import com.getinsured.hix.model.NoticeQueued;

public interface QueuedNoticeProcessor {
    boolean verify(NoticeQueued noticeQueued);
    boolean process(NoticeQueued noticeQueued);
}
