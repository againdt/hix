package com.getinsured.ssap.model.eligibility;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Describes Eligibility Response.
 * @author Yevgen Golubenko
 * @since 4/4/19
 */
public class EligibilityResponse
{
  private String requestId;
  private long applicationId;
  private ResponseStatus  status;
  private boolean aptcEligible;
  private long aptcAmount;
  private long slspBenchmarkPremium;
  private Map<String, String> errors;
  private String householdDecisionPath;
  private Date aptcEligibilityStartDate;
  private Date aptcEligibilityEndDate;
  private List<HouseholdMemberResponse> members = null;

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(long applicationId) {
    this.applicationId = applicationId;
  }

  public ResponseStatus getStatus() {
    return status;
  }

  public void setStatus(ResponseStatus status) {
    this.status = status;
  }

  public boolean isAptcEligible() {
    return aptcEligible;
  }

  public void setAptcEligible(boolean aptcEligible) {
    this.aptcEligible = aptcEligible;
  }

  public long getAptcAmount() {
    return aptcAmount;
  }

  public void setAptcAmount(long aptcAmount) {
    this.aptcAmount = aptcAmount;
  }

  public Map<String, String> getErrors() {
    return errors;
  }

  public void setErrors(Map<String, String> errors) {
    this.errors = errors;
  }

  public String getHouseholdDecisionPath() {
    return householdDecisionPath;
  }

  public void setHouseholdDecisionPath(String householdDecisionPath) {
    this.householdDecisionPath = householdDecisionPath;
  }

  public Date getAptcEligibilityStartDate() {
    return aptcEligibilityStartDate;
  }

  public void setAptcEligibilityStartDate(Date aptcEligibilityStartDate) {
    this.aptcEligibilityStartDate = aptcEligibilityStartDate;
  }

  public Date getAptcEligibilityEndDate() {
    return aptcEligibilityEndDate;
  }

  public void setAptcEligibilityEndDate(Date aptcEligibilityEndDate) {
    this.aptcEligibilityEndDate = aptcEligibilityEndDate;
  }

  public List<HouseholdMemberResponse> getMembers() {
    return members;
  }

  public void setMembers(List<HouseholdMemberResponse> members) {
    this.members = members;
  }
  
  public long getSlspBenchmarkPremium() {
	return slspBenchmarkPremium;
  }
	
  public void setSlspBenchmarkPremium(long slspBenchmarkPremium) {
	this.slspBenchmarkPremium = slspBenchmarkPremium;
  }

  @Override
  public String toString() {
    return "EligibilityResponse{" +
        "requestId='" + requestId + '\'' +
        ", applicationId=" + applicationId +
        ", status=" + status +
        ", aptcEligible=" + aptcEligible +
        ", aptcAmount=" + aptcAmount +
        ", errors=" + errors +
        ", householdDecisionPath='" + householdDecisionPath + '\'' +
        ", aptcEligibilityStartDate=" + aptcEligibilityStartDate +
        ", aptcEligibilityEndDate=" + aptcEligibilityEndDate +
        ", members=" + members +
        '}';
  }
}
