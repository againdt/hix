package com.getinsured.referraltask;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.indportal.enrollment.api.IndPortalEnrollmentUtility;
import com.getinsured.iex.dto.Ind57Mapping;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("referralAdminUpdateTask")
public class ReferralAdminUpdateTask {
	private static final String ADMIN_UPDATE_REFERRAL = "ADMIN_UPDATE_REFERRAL";
	private static final String FAILURE = "FAILURE";
	private static final String SUCCESS = "SUCCESS";
	
	private static final Logger LOGGER = Logger.getLogger(ReferralAdminUpdateTask.class);

	@Autowired
	private IndPortalEnrollmentUtility indPortalEnrollmentUtility;
	
	@Autowired private IntegrationLogService integrationLogService;
	

	public void triggerAdminUpdate(String caseNumber, long ssapApplicationId) {
		String status = SUCCESS;
		String payload = SUCCESS; 
		try {
			LOGGER.info("triggerAdminUpdate Starts for caseNumber " + caseNumber);
			final Ind57Mapping ind57Mapping = new Ind57Mapping();
			ind57Mapping.setCaseNumber(caseNumber);
			ind57Mapping.setUserName(ReferralConstants.EXADMIN_USERNAME);
			indPortalEnrollmentUtility.invokeAdminUpdateEnrollment(ind57Mapping);
			LOGGER.info("triggerAdminUpdate Ends for caseNumber " + caseNumber);
		} catch (Exception e) {
			status = FAILURE;
			payload = "Error while invoking Admin update for case number " + caseNumber + " " + e.getMessage();
			LOGGER.error(payload);
		} finally {
			integrationLogService.save(payload, ADMIN_UPDATE_REFERRAL, status, ssapApplicationId, null);
		}
	}
}
