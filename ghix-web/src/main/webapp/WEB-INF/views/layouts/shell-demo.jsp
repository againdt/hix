<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%
       String themeName = (String) request.getSession().getAttribute(
                     "switchToThemeName");
       if (StringUtils.isBlank(themeName)) {
              themeName = "default";
       }
       String bodyId = "body-" + themeName;
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<META HTTP-EQUIV="expires" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
<meta http-equiv="pragma" content="no-cache" />
<%
       String defaultTitle ="Covered California";
       String pageTitle ="";
       
       if (themeName == "employer"){
                   pageTitle = defaultTitle + "- Employer Portal";
       }
       else if (themeName == "employee"){
                   pageTitle = defaultTitle + "- Employee Portal";
       }
       else if (themeName == "issuer"){
                   pageTitle = defaultTitle + "- Issuer Portal";
       }
       if (themeName == "agent"){
                   pageTitle = defaultTitle + "- Agent Portal";
       }
       else{
                   pageTitle = defaultTitle;
       }       
%>
<title>Your Health Idaho</title>

<meta name="description" content="" />
<meta name="author" content="" />

<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<c:set var="quick_fixes" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.THEMEQUICKFIXES_FILE)%>"></c:set>
<c:set var="ghix_customjs" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.THEMEJS_FILE)%>"></c:set>
 <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/resources/images/favicon.ico" />"/>
 
<%--  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" media="screen,print" /> --%>
 <link href="<c:url value="/resources/css/bootstrap-responsive.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<c:url value="/resources/css/formvalidation.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<gi:cdnurl value="/resources/css/custom-theme/jquery-ui-1.10.4.custom.min.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<c:url value="/resources/css/jquery.ui.datepicker.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' media="screen,print" />    
 <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css' media="screen,print" />
 <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css' media="screen,print">

 <link href="<gi:cdnurl value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />
 <link href="<gi:cdnurl value="/resources/css/ghixcustom.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<gi:cdnurl value="/resources/css/${quick_fixes}" />" media="screen,print" rel="stylesheet" type="text/css" /> 

 
<%--  <c:if test="${(iframe == 'yes')}">
 <link href="<c:url value="/resources/css/iframe.css" />" media="screen,print" rel="stylesheet" type="text/css" />
 </c:if> --%>
 <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.8.2.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script> 
 <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/ghixcustom.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-touch-punch-0.2.2.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/accessibility.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/${ghix_customjs}" />"></script>
  
<%--   <c:if test="${(iframe == 'yes')}">
    <script type="text/javascript" src="<c:url value="/resources/js/gi.iframe.js" />"></script>
  </c:if> --%>
 <link rel="stylesheet" type="text/css" href="/hix/resources/css/datepicker.css" media="screen,print"/>
<script type="text/javascript" src="/hix/resources/js/bootstrap-datepicker.js"></script>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/easyXDM.debug.js" />"></script>  --%>
 <!--[if lt IE 9]>
	<script type="text/javascript" src="<c:url value='/resources/js/html5.js' />"></script>
	<link rel="stylesheet" type="text/css" href="/hix/resources/css/ie8fixes.css" media="screen,print">
	<link href="<c:url value="/resources/css/" /><%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.THEMEIE8_FILE)%>" media="screen,print" rel="stylesheet" type="text/css" />
 <![endif]-->
 
 
 
<div id="masthead" class="topbar"><tiles:insertAttribute name="masthead-demo" /></div>
    <div id="topnav" class="topbar"><tiles:insertAttribute name="topnav" /></div> 

<div id="container-wrap" class="row-fluid">
      <div class="container">
     
  <tiles:insertAttribute name="main" />

      </div>
    </div>


<div id="footer" class="container"><tiles:insertAttribute name="footer" /></div>