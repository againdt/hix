/**
 * 
 */
package com.getinsured.hix.batch.enrollment.task;

import java.util.Calendar;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentReconciliationService;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;

/**
 * @author panda_p
 *
 */
public class EnrlReconDataCleanupTask  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconDataCleanupTask.class);

	private String month;
	private String year;
	private EnrollmentReconciliationService enrollmentReconciliationService;
	
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		long jobExecutionId=-1;
		jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		if(EnrollmentUtils.isNotNullAndEmpty(month) && EnrollmentUtils.isNotNullAndEmpty(year)){
			try{
				if(!NumberUtils.isNumber(month)|| Integer.parseInt(month)<1 || Integer.parseInt(month)>12){
					LOGGER.error("Invalid:: Input Parameter Month : "+month);
					throw new Exception("Invalid:: Input Parameter Month : "+month);
			}
				if(!NumberUtils.isNumber(year)||  Integer.parseInt(year)<2014 || Integer.parseInt(year)>Calendar.getInstance().get(Calendar.YEAR) ){
					LOGGER.error("Invalid:: Input Parameter Year: "+year);
					throw new Exception("Invalid:: Input Parameter Year : "+ year);
			}
			String cutOffDate = month+"/01/"+year;
			enrollmentReconciliationService.deleteReconData(cutOffDate, jobExecutionId);
				
			}catch(Exception e){
				LOGGER.error("Error occured in data clean up : "+e.getMessage());
				throw new Exception("Error occured in data clean up : "+e.getMessage());
			}
		}else{
			LOGGER.error("Input Parameter Month or Year is null or empty");
			throw new Exception("Input Parameter Month or Year is null or empty");
		}
		
		return RepeatStatus.FINISHED;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
			this.month = month;
	}
	
	public EnrollmentReconciliationService getEnrollmentReconciliationService() {
		return enrollmentReconciliationService;
	}

	public void setEnrollmentReconciliationService(EnrollmentReconciliationService enrollmentReconciliationService) {
		this.enrollmentReconciliationService = enrollmentReconciliationService;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	
}
