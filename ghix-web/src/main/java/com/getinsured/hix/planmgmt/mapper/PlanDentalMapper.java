package com.getinsured.hix.planmgmt.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.timeshift.TimeShifterUtil;

public class PlanDentalMapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDentalMapper.class);

	private static Map<String, Integer> columnKeys = null;
	
	private static final  String NETWORKSUBJECTDEDUCTIBLE = "Subject to Deductible (Tier 1)";
	private static final  String NONNETWORKSUBJECTDEDUCTIBLE = "Subject to Deductible (Tier 2)";
	private static final  String NONNETWORKEXCLUDEMOOP = "Excluded from Out of Network MOOP";
	private static final  String NETWORKEXCLUDEMOOP = "Excluded from In Network MOOP";
	private static final  String NETWORKVALUE = "Network (Value)";
	private static final  String NETWORKATTRIBUTE = "Network Attribute";
	private static final  String NETWORKLIMITATION = "Network Limitation (Numeric Value)";
	private static final  String NETWORKLIMITATIONATTRIBUTE = "Network Limitation Attribute";
	private static final  String NETWORKEXCEPTIONS = "Network Exceptions";
	private static final  String NONNETWORKVALUE = "Non-Network (Value)";
	private static final  String NONNETWORKATTRIBUTE = "Non-Network Attribute";
	private static final  String NONNETWORKLIMITATION = "Non-Network Limitation (Numeric Value)";
	private static final  String NONNETWORKLIMITATIONATTRIBUTE = "Non-Network Limitation Attribute";
	private static final  String NONNETWORKEXCEPTIONS = "Non-Network Exceptions";
	private static final  String BENEFIT_NAME = "Name";

	private static final  int NETWORKVALUE_VAL = 1;
	private static final  int NETWORKATTRIBUTE_VAL = 2;
	private static final  int NETWORKLIMITATION_VAL = 3;
	private static final  int NETWORKLIMITATIONATTRIBUTE_VAL = 4;
	private static final  int NETWORKEXCEPTIONS_VAL = 5;
	private static final  int NONNETWORKVALUE_VAL = 6;
	private static final  int NONNETWORKATTRIBUTE_VAL = 7;
	private static final  int NONNETWORKLIMITATION_VAL = 8;
	private static final  int NONNETWORKLIMITATIONATTRIBUTE_VAL = 9;
	private static final  int NONNETWORKEXCEPTIONS_VAL = 10;
	private static final  int BENEFIT_NAME_VAL = 11;
	private static final  int NETWORKSUBJECTDEDUCTIBLE_VAL = 12;
	private static final  int NONNETWORKSUBJECTDEDUCTIBLE_VAL = 13;
	private static final  int NONNETWORKEXCLUDEMOOP_VAL = 14;
	private static final  int NETWORKEXCLUDEMOOP_VAL = 15;
	
	static {
		columnKeys = new HashMap<String, Integer>();

		columnKeys.put(NETWORKSUBJECTDEDUCTIBLE, NETWORKSUBJECTDEDUCTIBLE_VAL);
		columnKeys.put(NONNETWORKSUBJECTDEDUCTIBLE, NONNETWORKSUBJECTDEDUCTIBLE_VAL);
		columnKeys.put(NONNETWORKEXCLUDEMOOP, NONNETWORKEXCLUDEMOOP_VAL);
		columnKeys.put(NETWORKEXCLUDEMOOP, NETWORKEXCLUDEMOOP_VAL);
		columnKeys.put(NETWORKVALUE, NETWORKVALUE_VAL);
		columnKeys.put(NETWORKATTRIBUTE, NETWORKATTRIBUTE_VAL);
		columnKeys.put(NETWORKLIMITATION, NETWORKLIMITATION_VAL);
		columnKeys.put(NETWORKLIMITATIONATTRIBUTE, NETWORKLIMITATIONATTRIBUTE_VAL);
		columnKeys.put(NETWORKEXCEPTIONS, NETWORKEXCEPTIONS_VAL);
		columnKeys.put(NONNETWORKVALUE, NONNETWORKVALUE_VAL);
		columnKeys.put(NONNETWORKATTRIBUTE, NONNETWORKATTRIBUTE_VAL);
		columnKeys.put(NONNETWORKLIMITATION, NONNETWORKLIMITATION_VAL);
		columnKeys.put(NONNETWORKLIMITATIONATTRIBUTE, NONNETWORKLIMITATIONATTRIBUTE_VAL);
		columnKeys.put(NONNETWORKEXCEPTIONS, NONNETWORKEXCEPTIONS_VAL);
		columnKeys.put(BENEFIT_NAME, BENEFIT_NAME_VAL);
		
	}

	public PlanDental mapData(List<List<String>> data, final List<String> columns,Map<String,String> benefitMap) {
		long startTime = TimeShifterUtil.currentTimeMillis();
		
		LOGGER.info("Started mapping the data file for Plan benefits.");
		PlanDental planDental = new PlanDental();
		String colName = null;
		String key = null;
		PlanDentalBenefit planDentalBenefit = new PlanDentalBenefit();
		int keyIndex = columns.indexOf(BENEFIT_NAME);
		HashMap<String, PlanDentalBenefit> benefitsObjs1 = new HashMap<String, PlanDentalBenefit>();
		planDental.setPlanDentalBenefits(benefitsObjs1);
		for (List<String> line : data) {
			key = line.get(keyIndex);
			if (key != null && key.length() > 0) {
				planDentalBenefit = new PlanDentalBenefit();
				if (benefitMap.containsKey(key)) {
					String keyConst = benefitMap.get(key);
					planDentalBenefit.setName(keyConst);
					benefitsObjs1.put(keyConst, planDentalBenefit);
				} else {
					planDentalBenefit.setName(key);
					benefitsObjs1.put(key, planDentalBenefit);
				}
				planDentalBenefit.setPlanDental(planDental);
			}
			int colIdx = 0;
			for (String columnData : line) {
				colName = columns.get(colIdx);
				LOGGER.debug("Mapping data for " + SecurityUtil.sanitizeForLogging(String.valueOf(key)));
				mapData(columnData, colName, planDentalBenefit);
				colIdx++;
			}
			key = null;
			planDentalBenefit = null;
		}
		long endTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.debug("Mapping the data file for Plan benefits completed in." + SecurityUtil.sanitizeForLogging(String.valueOf((endTime - startTime))) + " seconds.");
		return planDental;
	}

	private void mapData(String data, String column, PlanDentalBenefit planDentalBenefit) {
		Integer colVal = columnKeys.get(column);
		if (colVal == null) {
			colVal = 0;
		}
		switch (colVal) {
			case NETWORKVALUE_VAL:
				//planDentalBenefit.setNetworkValue(data);
				break;
			case NETWORKATTRIBUTE_VAL:
				//planDentalBenefit.setNetworkAttribute(data);
				break;
			case NETWORKLIMITATION_VAL:
				planDentalBenefit.setNetworkLimitation(data);
				break;
			case NONNETWORKVALUE_VAL:
				//planDentalBenefit.setNonnetworkValue(data);
				break;
			case NONNETWORKATTRIBUTE_VAL:
				//planDentalBenefit.setNonnetworkAttribute(data);
				break;
			case NONNETWORKLIMITATION_VAL:
				//planDentalBenefit.setNonnetworkLimitation(data);
				break;
			case NETWORKLIMITATIONATTRIBUTE_VAL:
				//planDentalBenefit.setNetworkLimitationAttribute(data);
				break;
			case NETWORKEXCEPTIONS_VAL:
				planDentalBenefit.setNetworkExceptions(data);
				break;
			case NONNETWORKLIMITATIONATTRIBUTE_VAL:
				//planDentalBenefit.setNonnetworkLimitationAttribute(data);
				break;
			case NONNETWORKEXCEPTIONS_VAL:
				//planDentalBenefit.setNonnetworkExceptions(data);
				break;
			case NETWORKSUBJECTDEDUCTIBLE_VAL:
				planDentalBenefit.setNetworkSubjDeduct(data);
				break;
			case NONNETWORKSUBJECTDEDUCTIBLE_VAL:
				planDentalBenefit.setNonnetworkSubjDeduct(data);
				break;
			case NONNETWORKEXCLUDEMOOP_VAL:
				planDentalBenefit.setNonnetworkExclFrmMoop(data);
				break;
			case NETWORKEXCLUDEMOOP_VAL:
				planDentalBenefit.setNetworkExclFrmMoop(data);
				break;
		}
	}

}
