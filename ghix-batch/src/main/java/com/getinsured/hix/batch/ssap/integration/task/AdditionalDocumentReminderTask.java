package com.getinsured.hix.batch.ssap.integration.task;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.web.client.RestTemplate;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.batch.repository.BatchNoticeRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

public class AdditionalDocumentReminderTask implements Tasklet {
	
	private static final Logger LOGGER = Logger.getLogger(AdditionalDocumentReminderTask.class);
	
	public static final String VERIFIED = "VERIFIED";

	private static final Integer BUFFER_DAYS_TO_RETRY = 5;
	
	private SsapApplicationRepository ssapApplicationRepository;
	
	private BatchNoticeRepository batchNoticeRepository;
	
    private SsapJsonBuilder ssapJsonBuilder;
	
    private RestTemplate restTemplate;
    
	private final List<String> openApplicationStatuses = Arrays.asList(
	        ApplicationStatus.SIGNED.getApplicationStatusCode(),
	        ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),
	        ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
	
	private List<Integer> reminderIntervalInDays =  Arrays.asList(60);

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        LOGGER.info("AdditionalDocumentReminderTask started on " + new Timestamp(System.currentTimeMillis()));
        	
        for (Integer interval : reminderIntervalInDays) {
        	try {
        		DateTime endDate = DateTime.now().minusDays(interval);
        		DateTime startDate = DateTime.now().minusDays(interval + BUFFER_DAYS_TO_RETRY);
        		
        		List<SsapApplication> applicationsToVerify = ssapApplicationRepository.findApplicationsWithEsignDateWithinRange(startDate.toDate(), 
        				endDate.toDate(), openApplicationStatuses);
        		
        		for(SsapApplication application : applicationsToVerify) {
        			boolean householdVerified = true;
        			for(SsapApplicant applicant : application.getSsapApplicants()) {
        				if(StringUtils.isNotBlank(applicant.getApplyingForCoverage()) && "Y".equals(applicant.getApplyingForCoverage())
        						&& "Y".equals(applicant.getOnApplication())) {
        					boolean incarcerationStatus = StringUtils.isNotBlank(applicant.getIncarcerationStatus()) && applicant.getIncarcerationStatus().equals(VERIFIED);
        					boolean citizenshipStatus = StringUtils.isNotBlank(applicant.getCitizenshipImmigrationStatus()) && applicant.getCitizenshipImmigrationStatus().equals(VERIFIED);
        					boolean ssnVerificationStatus = StringUtils.isNotBlank(applicant.getSsnVerificationStatus()) && applicant.getSsnVerificationStatus().equals(VERIFIED);
        					boolean deathVerificationStatus = StringUtils.isNotBlank(applicant.getDeathStatus()) && applicant.getDeathStatus().equals(VERIFIED);
        					boolean vlpVerificationStatus = StringUtils.isNotBlank(applicant.getVlpVerificationStatus()) && applicant.getVlpVerificationStatus().equals(VERIFIED);
        					boolean residencyVerificationStatus = StringUtils.isNotBlank(applicant.getResidencyStatus()) && applicant.getResidencyStatus().equals(VERIFIED);
        					
        					if(!checkIncarcerationStatus(applicant.getSsapApplication().getApplicationData(), applicant.getPersonId(), incarcerationStatus) || 
        							!ssnVerificationStatus || !deathVerificationStatus || !(vlpVerificationStatus || citizenshipStatus) || !residencyVerificationStatus) {
        						householdVerified = false;
        						break;
        					}
        				}
        			}
        			
        			if(!householdVerified && !isNoticeSent(application, interval)) {
        				// Call Eligibility notification service to send notifications.
        				try {
        					restTemplate.getForEntity(GhixEndPoints.EligibilityEndPoints.ELIGIBILITY_ADD_DOC_REQD_NOTIFICATION_URL + application.getCaseNumber(), String.class);
        				} catch (Exception exception) {
        					LOGGER.error("Error invoking additional doc required notification service", exception);
        				}
        			}
        		}
        	} catch (Exception e) {
        		LOGGER.error("Error in AdditionalDocumentReminderTask", e);
        		throw new GIRuntimeException("Error in AdditionalDocumentReminderTask:" + e.getMessage());
        	}
		}
        LOGGER.info("AdditionalDocumentReminderTask finished on " + new Timestamp(System.currentTimeMillis()));
        return RepeatStatus.FINISHED;
	}
	
	
    private boolean isNoticeSent(SsapApplication application, Integer interval) {
    	DateTime startDate = new DateTime(application.getEsignDate()).plusDays(interval);
    	DateTime endDate = new DateTime(application.getEsignDate()).plusDays(interval + BUFFER_DAYS_TO_RETRY);
    	List<Notice> notices = batchNoticeRepository.additionalInfoNoticesCreatedForModuleIdBetween(startDate.toDate(), 
    			endDate.toDate(), application.getCmrHouseoldId().intValue());
		return notices != null && CollectionUtils.size(notices) > 0;
	}


	private boolean checkIncarcerationStatus(String ssapJsonString, long personId, boolean incarcerationStatus) {
    	boolean determinedIncarcerationStatus = false;
    	if(incarcerationStatus) {
    		SingleStreamlinedApplication ssapJSON = ssapJsonBuilder.transformFromJson(ssapJsonString);
    		for (HouseholdMember member : ssapJSON.getTaxHousehold().get(0).getHouseholdMember()) {
				if(member.getPersonId() == personId) {
					determinedIncarcerationStatus = !member.getIncarcerationStatus().getIncarcerationStatusIndicator();
					break;
				}
			}
    	}
		return determinedIncarcerationStatus;
	}

    
	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}


	public void setSsapApplicationRepository(
			SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}


	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}


	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}


	public RestTemplate getRestTemplate() {
		return restTemplate;
	}


	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}


	public BatchNoticeRepository getBatchNoticeRepository() {
		return batchNoticeRepository;
	}


	public void setBatchNoticeRepository(BatchNoticeRepository batchNoticeRepository) {
		this.batchNoticeRepository = batchNoticeRepository;
	}
}
