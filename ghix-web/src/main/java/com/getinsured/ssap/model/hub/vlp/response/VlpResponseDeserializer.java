package com.getinsured.ssap.model.hub.vlp.response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * VLP Response deserializer.
 *
 * @author Yevgen Golubenko
 * @since 2/27/19
 */
public class VlpResponseDeserializer extends JsonDeserializer<VlpResponse>
{
  private static final Logger log = LoggerFactory.getLogger(VlpResponseDeserializer.class);

  public static final String KEY_ROOT_KEY = "IntialVerificationResponseTypeResponseSet";
  public static final String KEY_INITIAL_VERIFICATION_INDIVIDUAL_RESPONSE = "InitVerifIndividualResponse";
  public static final String KEY_RESPONSE_METADATA = "ResponseMetadata";
  public static final String KEY_LAWFULPRESENCE_VERIFIED_CODE = "LawfulPresenceVerifiedCode";
  public static final String KEY_INDIVIDUAL_DETAILS = "InitialVerificationIndividualResponseSet";

  private static ObjectMapper om = new ObjectMapper();

  @Override
  public VlpResponse deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException
  {
    VlpResponse vlpResponse = new VlpResponse();

    JsonNode node = jp.getCodec().readTree(jp);

    TreeSet<ResponseCode> responseCodeSet = new TreeSet<>();

    List<IndividualDetails> individualDetailsList = new ArrayList<>();

    if (node.isObject())
    {
      String rootKeyValue = null;
      JsonRootName rootKey = VlpResponse.class.getAnnotation(JsonRootName.class);

      if (rootKey != null)
      {
        rootKeyValue = rootKey.value();
      } else
      {
        rootKeyValue = KEY_ROOT_KEY;
      }

      if (node.has(rootKeyValue))
      {
        JsonNode rootNode = node.get(rootKeyValue);

        JsonNode initVerifIndividualResponseNode = rootNode.get(KEY_INITIAL_VERIFICATION_INDIVIDUAL_RESPONSE);

        /* If this is an array */
        if (initVerifIndividualResponseNode != null && initVerifIndividualResponseNode.isArray())
        {
          initVerifIndividualResponseNode.elements().forEachRemaining(respNode -> {
            IndividualDetails individualDetails = new IndividualDetails();

            if(respNode.has(KEY_INDIVIDUAL_DETAILS))
            {
              try
              {
                individualDetails = om.convertValue(respNode.get(KEY_INDIVIDUAL_DETAILS), IndividualDetails.class);
              }
              catch (Exception e)
              {
                log.error("Unable to parse {} key from JSON: {}",
                    KEY_INDIVIDUAL_DETAILS,
                    respNode.get(KEY_INDIVIDUAL_DETAILS).toString());
                log.error("Exception parsing IndividualDetails from JSON", e);
              }
            }

            getResponseMetadataFromNode(respNode, responseCodeSet, individualDetails);

            if (respNode.has(KEY_LAWFULPRESENCE_VERIFIED_CODE))
            {
              String code = respNode.get(KEY_LAWFULPRESENCE_VERIFIED_CODE).textValue();
              MultiCode multiCode = MultiCode.valueOf(code);
              individualDetails.setLawfulPresenceVerifiedCode(multiCode);
            }

            individualDetailsList.add(individualDetails);
          });
        }
        // Not array
        else {
          IndividualDetails individualDetails = om.convertValue(rootNode, IndividualDetails.class);

          getResponseMetadataFromNode(node, responseCodeSet, individualDetails);

          individualDetailsList.add(individualDetails);
        }
      }
      else if(node.has("EXCEPTION")) {
        JsonNode exceptionNode = node.get("EXCEPTION");
        if(exceptionNode.has("EXCEPTION_MESSAGE")) {
          JsonNode exceptionMessage = exceptionNode.get("EXCEPTION_MESSAGE");
          if(exceptionMessage.isTextual()) {
            ValidationErrorCode validationErrorCode = ValidationErrorCode.EXCEPTION;
            validationErrorCode.setExceptionDetails(exceptionMessage.asText());
            vlpResponse.setValidationErrorCode(validationErrorCode);
            vlpResponse.setResponseCode(ResponseCode.EXCEPTION);
          }
        }
      } else if(node.has("individualDetails")) {
        JsonNode individualDetailsNode = node.get("individualDetails");
        if(individualDetailsNode.isArray() && individualDetailsNode.get(0) != null) {
          IndividualDetails individualDetails = new IndividualDetails();

          try
          {
            individualDetails = om.convertValue(individualDetailsNode.get(0), IndividualDetails.class);
          }
          catch (Exception e)
          {
            log.error("Unable to parse {} key from JSON: {}", "individualDetails", individualDetailsNode.get(0).toString());
            log.error("Exception parsing IndividualDetails from JSON", e);
          }

          if (individualDetailsNode.get(0) != null && individualDetailsNode.get(0).has(KEY_LAWFULPRESENCE_VERIFIED_CODE))
          {
            String code =  individualDetailsNode.get(0).get(KEY_LAWFULPRESENCE_VERIFIED_CODE).textValue();
            MultiCode multiCode = MultiCode.fromValue(code);
            individualDetails.setLawfulPresenceVerifiedCode(multiCode);
          }

          individualDetailsList.add(individualDetails);
        }
      } else if(node.has("RetriveStep2ResponseTypeResponseSet")) { // Retrieve Step 2 Case Resolution has this as root key
        JsonNode stepTwoCaseResolution = node.get("RetriveStep2ResponseTypeResponseSet");

        if(stepTwoCaseResolution != null && stepTwoCaseResolution.isObject()) {
          try {
            Object o = om.convertValue(stepTwoCaseResolution, Object.class);
            vlpResponse.setStepTwoResponseType(o);

            if(node.has("ResponseMetadata")) {
              JsonNode responseMetadataNode = node.get("ResponseMetadata");
              ResponseMetadata responseMetadata = om.convertValue(responseMetadataNode, ResponseMetadata.class);
              vlpResponse.setResponseMetadata(responseMetadata);

              if(responseMetadataNode.has("ResponseCode")) {
                String responseMetadataResponseCode = om.convertValue(responseMetadataNode.get("ResponseCode"), String.class);
                ResponseCode responseCode = ResponseCode.fromJsonName(responseMetadataResponseCode);
                responseMetadata.setResponseCode(responseCode);
                vlpResponse.setResponseCode(responseCode);
              }
            }
          }
          catch(Exception e) {
            log.error("Unable to parse JSON response for step 2 case resolution: {}", e.getMessage());
            log.error("Exception details", e);
          }
        }
      }
    }

    if(!responseCodeSet.isEmpty()) {
      Optional<ResponseCode> nonSuccessCodeOptional = responseCodeSet.stream().filter(rc -> rc != ResponseCode.SUCCESS).findFirst();

      if (nonSuccessCodeOptional.isPresent()) {
        vlpResponse.setResponseCode(nonSuccessCodeOptional.get());
        vlpResponse.setValidationErrorCode(ValidationErrorCode.fromResponseCode(vlpResponse.getResponseCode()));
      } else {
        vlpResponse.setResponseCode(responseCodeSet.first());
      }
    }

    vlpResponse.setIndividualDetails(individualDetailsList);

    return vlpResponse;
  }

  private void getResponseMetadataFromNode(final JsonNode node, final TreeSet<ResponseCode> responseCodeSet, final IndividualDetails individualDetails)
  {
    if (node.has(KEY_RESPONSE_METADATA))
    {
      ResponseMetadata responseMetadata = null;

      try
      {
        responseMetadata = om.convertValue(node.get(KEY_RESPONSE_METADATA), ResponseMetadata.class);
        individualDetails.setResponseMetadata(responseMetadata);
        responseCodeSet.add(responseMetadata.getResponseCode());
      }
      catch (Exception e)
      {
        log.error("Unable to parse ResponseMetadata: {}", e.getMessage());
      }
    }
  }
}
