package com.getinsured.hix.batch.enrollment.email.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.batch.enrollment.email.EnrollmentBatchFailureEmailNotification;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
/**
 * 
 * @author raja
 * @since 07/09/2013
 */
@Service("enrollmentBatchFailureEmailNotificationService")
@Transactional
public class EnrollmentBatchFailureEmailNotificationServiceImpl implements EnrollmentBatchFailureEmailNotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentBatchFailureEmailNotificationServiceImpl.class);
	
	
	
	/** to send email */
	@Autowired private EnrollmentBatchFailureEmailNotification activationEmailType;
	
	@Override
	public void sendEnrollmentBatchFailureEMailNotification(EnrollmentBatchEmailDTO enrollmentBatchEmailDTO, ChunkContext chunkContext) throws NotificationTypeNotFound{
		
		if(chunkContext!=null){
			enrollmentBatchEmailDTO.setSubject("Failure in EDI Job: <"+chunkContext.getStepContext().getJobName()+"> - <"+chunkContext.getStepContext().getStepExecution().getJobExecution().getId().toString()+">");
			enrollmentBatchEmailDTO.setJobId(chunkContext.getStepContext().getStepExecution().getJobExecution().getId().toString());
			enrollmentBatchEmailDTO.setJobName(chunkContext.getStepContext().getJobName());
			enrollmentBatchEmailDTO.setStepId(chunkContext.getStepContext().getStepExecution().getId().toString());
			enrollmentBatchEmailDTO.setStepName(chunkContext.getStepContext().getStepName());
		}
		enrollmentBatchEmailDTO.setFailureDate(new Date());
		
		activationEmailType.setEnrollmentBatchEmailDTO(enrollmentBatchEmailDTO);
		Notice noticeObj = activationEmailType.generateEmail();
//		LOGGER.info(noticeObj.getEmailBody());
		activationEmailType.sendEmail(noticeObj);
	}
}
