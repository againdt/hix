package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AdminDocument;
import com.getinsured.hix.model.IssuerQualityRating;
import com.getinsured.hix.planmgmt.mapper.IssuerQualityRatingPojo;

public interface IssuerQualityRatingService {

	String RATINGS_FOLDER = "ratings";

	boolean saveQualityRating(IssuerQualityRating issuerRatingObj);

	Map<String, String> getIssuerQualityRatingByIssuerId(int issuerId);

	boolean saveDocument(AdminDocument adminDocument);

	List<AdminDocument> getAllAdminDocument(String documentType);

	/*	Check IssuerQualityRating Exist OR Not*/
	boolean getIssuerQualityRatingWithEffectiveStartandEndDate(IssuerQualityRating qualityRating);

	boolean getIssuersEffectiveEndDates(IssuerQualityRating qualityRating);

	boolean getIssuersEffectiveDateRange(IssuerQualityRating qualityRating);

	IssuerQualityRating getQualitRatingForIssuer(Integer issuerId);
	
	List<List<IssuerQualityRatingPojo>> processQualityRatingData(String filePath, String stateCode);
	
	boolean saveDocument(AccountUser user,int commentid, MultipartFile qualityRating,String documentName);
	
	boolean saveIssuerQualityRating(IssuerQualityRatingPojo issuerQualityRatingPojo, AccountUser user, IssuerQualityRating issuerQualityRatingObj, String stateCode);
}