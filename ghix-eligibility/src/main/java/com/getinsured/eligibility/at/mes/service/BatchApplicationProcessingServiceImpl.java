package com.getinsured.eligibility.at.mes.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.mes.repository.ATSpanProcessingRepository;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.resp.si.dto.AtAdditionalParamsDTO;
import com.getinsured.eligibility.at.service.AccountTransferStrategy;
import com.getinsured.eligibility.at.service.GenericAtStrategyImpl;
import com.getinsured.eligibility.enums.AccountTransferProcessStatus;
import com.getinsured.eligibility.enums.AccountTransferQueueStatus;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.model.AccountTransferSpanInfo;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

@Service("batchApplicationProcessingService")
public class BatchApplicationProcessingServiceImpl implements BatchApplicationProcessingService {

	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(BatchApplicationProcessingServiceImpl.class);
	
	@Autowired
	ATSpanProcessingRepository atSpanProcessingRepository;

	@Autowired
	GIWSPayloadRepository giwsPayloadRepository;
	
	@Autowired private Gson platformGson;
	
	@Autowired private AccountTransferStrategy accountTransferStrategy;
	
	@Autowired private GenericAtStrategyImpl genericAtStrategyImpl;
	
	@Override
	public void executeQueuedApplication() {
		
		try {
			accountTransferStrategy = genericAtStrategyImpl;
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("executeQueuedApplication Started");	
			}
			// get the application ids to be processed for the current date with queue status as QUEUED and process status as null
			Date currentDate = new TSDate();
			List<AccountTransferSpanInfo> queuedApplications = atSpanProcessingRepository
					.getQueuedApplicationsForCurrentDate(currentDate, AccountTransferQueueStatus.QUEUED);
			// get the giws payload from ssap_applications for each application
			for (AccountTransferSpanInfo accountTransferSpanInfo : queuedApplications) {
				String referralResponseMapStr = accountTransferSpanInfo.getReferralResponseMap();
				// get the GI_WS_PAYLOAD_ID from the map
				
				if(null != referralResponseMapStr) {
					// then process the application for batch otherwise continue with the loop
						Integer giwsPayloadId = accountTransferSpanInfo.getGiWsPayloadId();
						// call the gi_ws_payload table to get the xml
						GIWSPayload giwsPayload = giwsPayloadRepository.findById(giwsPayloadId.intValue());
						// convert this payload into AccountTransferRequestPayloadType
						AccountTransferRequestPayloadType accountTransferRequestPayloadType = null;
						
						if(!"NV".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)))
						{
							String requestBody = ReferralUtil.extractSoapBoady(giwsPayload.getRequestPayload());
							accountTransferRequestPayloadType = ReferralUtil.unmarshal(requestBody);
						}
						else
						{
							accountTransferRequestPayloadType = ReferralUtil.getAccountTransferRequestObject(giwsPayload.getRequestPayload());
						}
						//mark the span in progress
						accountTransferSpanInfo.setProcessStatus(AccountTransferProcessStatus.INPROGRESS);
						atSpanProcessingRepository.save(accountTransferSpanInfo);
						// call the processing for the AT				
						if(LOGGER.isInfoEnabled()){
							LOGGER.info("Call the process method : GenericAtStrategyImpl  with giwsPayloadId = {} , HH Id = {} , ssap application id = {} ",giwsPayloadId,accountTransferSpanInfo.getCmrHouseholdId(),accountTransferSpanInfo.getSsapApplicationId());	
						}
						AtAdditionalParamsDTO atAdditionalParams = new AtAdditionalParamsDTO();
						atAdditionalParams.setGiWsPayloadId(giwsPayloadId);
						atAdditionalParams.setRequester(ReferralProcessingConstants.REQUESTER_MES_BATCH);
						atAdditionalParams.setAdditionalFieldsMapXmlString(referralResponseMapStr);
						accountTransferStrategy.process(accountTransferRequestPayloadType,atAdditionalParams);	
					
					
				}
				
			}
		} catch (Exception e) {
			String errorMsg = "Exception occurred executeQueuedApplication "+ ExceptionUtils.getFullStackTrace(e);
			LOGGER.error(errorMsg);
		}

	}

}

