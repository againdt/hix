
package com.getinsured.eligibility.at.dto;

/**
 * Family Member Object for the Retrieving HouseholdComposition members info
 * @author Supreet Bhandari
 * @since 31 July, 2019
 *
 */
public class EligEngineHouseholdCompositionMember {

	private long applicantId;

	private int householdSizeMedicaid;

	private long householdIncomeMedicaid;

	public long getApplicantId() {
		return applicantId;
	}

	public void setApplicantId(long applicantId) {
		this.applicantId = applicantId;
	}

	public int getHouseholdSizeMedicaid() {
		return householdSizeMedicaid;
	}

	public void setHouseholdSizeMedicaid(int householdSizeMedicaid) {
		this.householdSizeMedicaid = householdSizeMedicaid;
	}

	public long getHouseholdIncomeMedicaid() {
		return householdIncomeMedicaid;
	}

	public void setHouseholdIncomeMedicaid(long householdIncomeMedicaid) {
		this.householdIncomeMedicaid = householdIncomeMedicaid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HouseholdCompositionMember [applicantId=");
		builder.append(applicantId);
		builder.append(", householdSizeMedicaid=");
		builder.append(householdSizeMedicaid);
		builder.append(", householdIncomeMedicaid=");
		builder.append(householdIncomeMedicaid);
		builder.append("]");
		return builder.toString();
	}

}
