import React, { Fragment, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Form from './Form';
import NamesInputs from './pageIncludes/common/names/NamesInputs';
import PageHeader from '../headers/PageHeader';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import {
    changeData,
    clearMethods,
    clearScrollToElement,
    scrollToElement,
    setMethods
} from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import cloneObject from '../../utils/cloneObject';

// Constants

import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import EMPTY_OBJECTS from '../../constants/emptyObjects';



/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'About Household' Page
 *
 */
const Page1_05 = props => {

    const {
        data,
        householdMembers,
        page,
        scroll
    } = props;

    const {
        buttonContainer: ButtonContainer
    } = props;

    // Here we scroll to the top of the page if we have not had element to be scrolled

    useEffect(() => {
        if(scroll === null) {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        }
    }, []);

    // Here we scroll to created household member

    useEffect(() => {
        if(scroll !== null) {
            const elementToBeScrolledTo = document.querySelector(`.${scroll}`);
            elementToBeScrolledTo.scrollIntoView({ behavior: 'smooth' });
            props.actions.clearScrollToElement();
        }
    }, [ scroll ]);


    // If we clicked 'Add another person' on Summary page for 1st section here we:
    // a) generate new One;
    // b) increase last household member index to scroll to him/her

    const handleAddingNewPerson = () => {

        const newHouseholdMember = cloneObject({
            ...EMPTY_OBJECTS.HOUSEHOLD_MEMBER,
            personId: householdMembers.length + 1,
            householdContact: {
                ...EMPTY_OBJECTS.HOUSEHOLD_MEMBER.householdContact,
                homeAddress: householdMembers[0].householdContact.homeAddress
            }
        });

        const newState = {
            ...data,
            singleStreamlinedApplication: {
                ...data.singleStreamlinedApplication,
                maxAchievedPage: page
            }
        };

        const updatedHouseholdMembers = [
            ...householdMembers,
            newHouseholdMember
        ];

        newState.singleStreamlinedApplication.taxHousehold[0].householdMember = updatedHouseholdMembers;
        props.actions.changeData(newState);
        props.actions.scrollToElement(`household-member-${updatedHouseholdMembers.length - 1}`);
    };

    const handleMemberDeletion = (householdMembers, memberIndex) => {

        // 0. Here we create Map with match [ <personId> before deletion, <personId> after deletion ]

        const personIdsMatch = householdMembers.reduce((res, hhm, idx) => {
            switch(true) {
            case memberIndex > idx:
                return res.set(idx + 1, idx + 1);
            case memberIndex === idx:
                return res.set(idx + 1, null);
            case memberIndex < idx:
                return res.set(idx + 1, idx);
            }
        }, new Map([]));

        // 1. Here we delete household member

        householdMembers.splice(memberIndex, 1);

        // 1.1. Here we normalize consequences because of gap after deletion:

        // 2. Here we adjust blood relationship between rest of household members
        // 2.1. Filtering we exclude all relations of deleted member
        // 2.2. Mapping we adjust household members' indexes to eliminate gap in number sequence

        householdMembers[0].bloodRelationship =
            [ ...householdMembers[0].bloodRelationship ]
                .filter(br => personIdsMatch.get(+br.individualPersonId) && personIdsMatch.get(+br.relatedPersonId))
                .map(br => ({
                    ...br,
                    individualPersonId: String(personIdsMatch.get(+br.individualPersonId)),
                    relatedPersonId: String(personIdsMatch.get(+br.relatedPersonId))
                }));

        // 3. Here we update JSON in Redux store

        const newState = {
            ...data,
            singleStreamlinedApplication: {
                ...data.singleStreamlinedApplication,
                maxAchievedPage: page
            }
        };

        newState.singleStreamlinedApplication.taxHousehold[0] = {
            ...newState.singleStreamlinedApplication.taxHousehold[0],
            primaryTaxFilerPersonId: 0,
            householdMember: householdMembers.map(hhm => {
                return {
                    ...hhm,
                    employerSponsoredCoverage: hhm.employerSponsoredCoverage
                        .filter(c => personIdsMatch.get(c.personId))
                        .map(c => ({ ...c, personId: personIdsMatch.get(c.personId) })),
                    householdId: 0,
                    personId: personIdsMatch.get(hhm.personId),
                    taxHouseholdComposition: {
                        ...EMPTY_OBJECTS.HOUSEHOLD_COMPOSITION
                    }
                };
            })
        };

        props.actions.changeData(newState);
    };

    // Updating some props in JSON

    const onFieldChange = (newData, indexInArray) => {
        const newState = { ...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = newData;
        props.actions.changeData(newState);
    };

    useEffect(() => {
        props.actions.setMethods('Page1_05', { handleAddingNewPerson, handleMemberDeletion });
        return () => {
            props.actions.clearMethods('Page1_05');
        };
    }, [ householdMembers.length ]);

    return (
        <Fragment>
            <div id="saPrimaryContact" className="">
                <Form>
                    {
                        householdMembers.map((member, i) => {
                            const {
                                applicantGuid,
                                dateOfBirth,
                                name,
                                name: {
                                    firstName,
                                    middleName,
                                    lastName,
                                    suffix
                                },
                                applyingForCoverageIndicator
                            } = member;

                            return (
                                <div
                                    className={`household-member-${i} subsection`}
                                    key={`${applicantGuid}_${i}`}
                                >
                                    <PageHeader
                                        text={firstName ? normalizeNameOfPerson(name) : `Applicant ${i + 1}`}
                                        headerClassName={'usa-heading-alt'}
                                        additionalElement={
                                            i !== 0 ?
                                                (<ButtonContainer
                                                    type="deleteButton"
                                                    args={[householdMembers, i]}
                                                />)
                                                : null
                                        }
                                        additionalElementClassName={'header-btn'}
                                    />

                                    <UniversalInput
                                        label={{
                                            text: 'Are you seeking coverage?'
                                        }}
                                        inputData={{
                                            isInputRequired: false,
                                            type: INPUT_TYPES.CHECKBOX,
                                            name: 'applyingForCoverageIndicator',
                                            classes:['layout--02'],
                                            checked: applyingForCoverageIndicator,
                                            value: applyingForCoverageIndicator,
                                            currentData: householdMembers[i],
                                            fields: []
                                        }}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                        indexInArray={i}
                                    />
                                    <NamesInputs
                                        {...{
                                            firstName,
                                            middleName,
                                            lastName,
                                            suffix,
                                            currentData: member,
                                            fields: ['name'],
                                            isRequired:true,
                                            inputActions: {
                                                onFieldChange
                                            },
                                            indexInArray: i
                                        }}
                                    />
                                    <UniversalInput
                                        label={{
                                            text: 'Date of Birth'
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.DATE_PICKER,
                                            name: 'dateOfBirth',
                                            value: dateOfBirth,
                                            placeholder: 'MM/DD/YYYY',
                                            validationType: VALIDATION_TYPES.DATE_NOT_GREATER_THAN_CURRENT,
                                            currentData: householdMembers[i],
                                            fields: []
                                        }}
                                        errorClasses={['usa-offset-one-third']}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                        indexInArray={i}
                                    />

                                </div>
                            );
                        })
                    }
                </Form>
                <div className="subsection">
                    <PageHeader
                        text="Need to include someone else?"
                        headerClassName={'usa-heading-alt'}
                        additionalElement={(
                            <ButtonContainer
                                type="addPersonButton"
                                args={[ householdMembers ]}
                            />
                        )}
                        additionalElementClassName={'header-btn'}
                    />
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    page: state.page,
    scroll: state.scroll
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        clearMethods,
        clearScrollToElement,
        scrollToElement,
        setMethods
    }, dispatch)
});

Page1_05.defaultProps = {
    name: 'Page1_05'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page1_05);
