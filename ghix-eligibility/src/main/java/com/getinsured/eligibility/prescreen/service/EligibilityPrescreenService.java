/**
 * 
 */
package com.getinsured.eligibility.prescreen.service;

import com.getinsured.hix.model.prescreen.PrescreenData;
import com.getinsured.hix.model.prescreen.calculator.FplRequest;
import com.getinsured.hix.model.prescreen.calculator.FplResponse;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenDTO;
import com.getinsured.hix.ui.model.eligibility.prescreen.PrescreenRequest;

/**
 * This is the service interface for Eligibility Prescreen
 * @author Nikhil Talreja
 * @since 19 April, 2013
 *
 */
public interface EligibilityPrescreenService {
	
	/**
	 * This method calculates APTC for a household
	 * 
	 * @author Sunil Desu
	 * @since April 17 2013
	 * @param aptcRequest
	 *            Houshehold for which APTC is to be calculated
	 * @return PrescreenRequest - with APTC, FPL, ApplicablePercentage values
	 */
	PrescreenRequest calculateAptc(PrescreenRequest aptcRequest);
	
	
	/**
	 * This method is exposed for services to invoke by passing the FPL
	 * percentage. The corresponding CSR value is returned.
	 * 
	 * @author Sunil Desu
	 * @since April 17 2013
	 * @param fpl
	 *            percentage for which the CSR has to be evaluated
	 * @return csr
	 */
	String computeCSR(double fpl);
	
	/**
	 * This is the implementation of the MAGI calculation for a given household
	 * and the corresponding tax deductions that are applicable.
	 * 
	 * @author Sunil Desu
	 * @since April 17 2013
	 * 
	 * @param prescreenHousehold
	 * @param prescreenDeductions
	 * @return magiValue which is the calculated MAGI income value
	 */
	double calculateMagi(PrescreenRequest prescreenRequest);
	
	/**
	 * This is the method that will be exposed for invocation for the
	 * calculation of FPL.
	 * 
	 * @author Sunil Desu
	 * @since April 11 2013
	 * 
	 * @param fplRequest
	 *            is the request object which contains the required parameters
	 *            for FPL Calculation. State, family size and family income are
	 *            required and cannot be {@code null}.
	 * 
	 * @return fplResponse is the response object which contains
	 *         <ol>
	 *         <li>the percentage value of FPL
	 *         <li>error message, which is populated if the processing results
	 *         in an error condition
	 *         </ol>
	 */
	FplResponse calculateFpl(FplRequest fplRequest);
	
	PrescreenData savePrescreenRequestRecord(PrescreenData record);
	
	PrescreenData getPrescreenRequestRecord(long id);
	
	PrescreenData getPrescreenRecordBySessionId(String sessionId);
	
	/**
	 * This method calculates APTC for a household - Product version
	 * 
	 * @author Sunil Desu
	 * @since June 06 2013
	 * @param aptcRequest
	 *            Houshehold for which APTC is to be calculated
	 * @return PrescreenDTO - with APTC, FPL, ApplicablePercentage values
	 */
	PrescreenDTO getAptc(PrescreenDTO aptcRequest);

}
