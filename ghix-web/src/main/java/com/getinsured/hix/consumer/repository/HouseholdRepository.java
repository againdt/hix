package com.getinsured.hix.consumer.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.accountactivation.repository.IGHIXCustomRepository;

@Repository
@Transactional(readOnly = true)
public class HouseholdRepository implements IGHIXCustomRepository<Household, Integer>{

	@Autowired
	private IHouseholdRepository iHouseholdRepository;

	@Override
	public boolean recordExists(Integer id) {
		return iHouseholdRepository.exists(id);
	}

	@Override
	@Transactional(readOnly=false)
	public void updateUser(AccountUser accountUser, Integer id) {
		/** do more stuff here..like updating some status of employee, etc */
		Household household = iHouseholdRepository.findOne(id);
		household.setUser(accountUser);
		iHouseholdRepository.save(household);
	}

}
