package com.getinsured.hix.entity.assister.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.entity.DesignateAssister;

/**
 * Spring-JPA repository that encapsulates methods that interact with the
 * DesignateAssister table
 */
public interface IAssisterDesignateRepository extends JpaRepository<DesignateAssister, Integer> {

	/**
	 * Fetches the Assister Designation record against the given Id
	 * 
	 * @param id
	 * @return the Assister Designation record against the Id
	 */
	@Query("from DesignateAssister da where da.id = :id")
	DesignateAssister findDesignateAssisterById(@Param("id") int id);

	/**
	 * Removes the Assister Designation record against the given Assister Id
	 * 
	 * @param assisterId
	 *            the assister Id
	 * @return deletion status
	 */
	@Query("delete from DesignateAssister da where da.assisterId = :assisterId")
	int deleteDesignateAssisterByAssisterId(@Param("assisterId") int assisterId);

	/**
	 * Fetches the Assister Designation record against the given Individual Id
	 * 
	 * @param individualId
	 *            the individual Id
	 * @return the Assister Designation record against the Individual Id
	 */
	@Query("FROM DesignateAssister da where da.individualId = :individualId")
	DesignateAssister findByIndividualId(@Param("individualId") Integer individualId);

	/**
	 * Retrieves DesignateAssister records against entityId and status for
	 * external individuals.
	 */
	@Query("FROM DesignateAssister as an where an.entityId = :entityId and an.status = :status and an.individualId != null")
	List<DesignateAssister> findDesignateAssisterForExternalIndbyEntityId(@Param("entityId") int entityId,
			@Param("status") DesignateAssister.Status status);

	/**
	 * Retrieves DesignateAssister records against assisterId and status for
	 * external individuals.
	 */
	@Query("FROM DesignateAssister as an where an.assisterId = :assisterId and an.status = :status and an.individualId != null")
	List<DesignateAssister> findDesignateAssisterForExternalIndbyAssisterId(@Param("assisterId") int assisterId,
			@Param("status") DesignateAssister.Status status);
	
	/**
	 * Retrieving list of designated individuals based on assister id 
	 * @param assisterId assister id
	 * @return {@link List} of {@link DesignateAssister}
	 */
	List<DesignateAssister> findByAssisterId(int assisterId); 
}
