<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<div class="row-fluid">
    <div class="gutter10">
    	<ul class="page-breadcrumb">
        	<li><a href="<c:url value="/planmgmt/issuer/financial/info"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.newIssuerApplication"/></a></li>
            <li><spring:message  code="pgheader.step"/> 4</li>
        </ul><!--page-breadcrumb ends-->
        <h1><issuerLogo:view issuerName="" /><spring:message  code="pgheader.newIssuerApplication"/></h1>
    </div>
</div>
		<div class="row-fluid">
        	<div class="gutter10">
                <div class="span3" id="sidebar">
                    <div class="header">
                        <h4 class="margin0"><spring:message  code="pgheader.steps"/></h4>
                    </div>
                    <!--  beginning of side bar -->
                    <ol class="nav nav-list">
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.addRepresentative"/></a></li>
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.issuerApplication"/></a></li>
                        <li><a href="#"><span class="completed"><spring:message  code="pgheader.completed"/></span><spring:message  code="pgheader.financialInformation"/></a></li>
                        <li class="active"><a href="#"><spring:message  code="pgheader.updateProfile"/></a></li>
                        <li class="list-style-none">
                            <ul class="sub-menu marginT-10">
                                <li><a href="#1" class="submenu-active "><spring:message  code="pgheader.update.companyProfile"/></a></li>
                                <li><a href="#2"><spring:message  code="pgheader.update.inidividualMarketProfile"/></a></li>
                                <li><a href="#3"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                            </ul>
                        </li>
                        <li><a href="#"><spring:message  code="pgheader.signApplication"/></a></li>
                    </ol>
                    <!-- end of side bar -->
                </div>
                <!-- end of span3 -->
                <div class="span9" id="rightpanel">
                    <div class="header">
                        <h4 class="span9"><spring:message  code="pgheader.step"/> 4: <spring:message  code="pgheader.update.companyProfile"/></h4>
                    </div>
                    <div class="gutter10">
            
                    <form class="form-horizontal" id="frmIssuerComProInfo" name="frmIssuerComProInfo" action="<c:url value="/planmgmt/issuer/profile/update/company/save" />" method="POST">
                        <df:csrfToken/>
                        <input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/>
                        <div class="control-group">
                            <label class="control-label" for="comp-legal-name"><spring:message  code="label.companyLegalName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="companyLegalName" id="companyLegalName" class="input-large" value="${issuerObj.companyLegalName}"/>
                                <div id="companyLegalName_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        <div class="control-group">
                            <label class="control-label" for="comp-logo"><spring:message  code="label.companyLogo"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a class="ttclass" rel="tooltip" href="#" data-original-title="Company logo"><i class="icon-question-sign"></i></a></label>
                            <div class="controls">
                                <input type="file" id="companyLogo" name="companyLogo"  class="input-file" />
                                &nbsp; <button type="submit" class="btn" id="btn_company_logo" name="btn_company_logo"><spring:message  code="label.upload"/></button>
                                 <div id="companyLogo_upload_display"></div>
                                 <c:if test="${not empty issuerObj.companyLogo}">
                                	<div id="companyLogo_display"><a href="<c:url value="/ecm/filedownloadbyid?uploadedFileId=${issuerObj.companyLogo}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a></div>
                                </c:if>
                                <div id="companyLogo_error" class=""></div>
                                <input type="hidden" id="hdnCompanyLogo" name="hdnCompanyLogo" value="${issuerObj.companyLogo}"/>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="state"><spring:message  code="label.stateOfDomicile"/><a class="ttclass" rel="tooltip" href="#" data-original-title="State of Domicile"><i class="icon-question-sign"></i></a></label>
                            <div class="controls">
                                <select class="input-medium" name="stateOfDomicile" id="stateOfDomicile">
                                     <c:forEach var="stateObj" items="${stateObj}">
						    			<option <c:if test="${stateObj.code == issuerObj.stateOfDomicile}"> SELECTED </c:if> value="${stateObj.code}">${stateObj.name}</option>
									</c:forEach>
                                </select>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="graylightaction margin0">
                            <h4 class="span10"><spring:message  code="pgheader.update.companyAddress"/></h4>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="bank-address-1"><spring:message  code="label.addressLine1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="addressLine1" id="addressLine1" class="input-large" value="${issuerObj.companyAddressLine1}"/>
                                <div id="addressLine1_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="bank-address-2"><spring:message  code="label.addressLine2"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="addressLine2" id="addressLine2" class="input-large" value="${issuerObj.companyAddressLine2}"/>
                                <div id="addressLine2_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="bank-city"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="city" id="city" class="input-large" value="${issuerObj.companyCity}"/>
                                <div id="city_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="bank-state"><spring:message  code="label.state"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="pull-left marginL10">
                                <select class="input-medium" name="state" id="state">
                                    <c:forEach var="stateObj" items="${stateObj}">
						    			<option <c:if test="${stateObj.code == issuerObj.companyState}"> SELECTED </c:if> value="${stateObj.code}">${stateObj.name}</option>
									</c:forEach>
                                </select>
                                <div id="bank-state-error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="zipcode"><spring:message  code="label.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="zipcode" id="zipcode" class="input-large" value="${issuerObj.companyZip}"/>
                                <div id="zipcode_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="issuer-website"><spring:message  code="label.issuerWebsite"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="issuerWebsite" id="issuerWebsite" class="input-large" value="${issuerObj.siteUrl}"/>
                                <div id="issuerWebsite_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="control-group">
                            <label class="control-label" for="facing-website"><spring:message  code="label.consumerFacingWebSiteURL"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="facingWebSite" id="facingWebSite" class="input-large" value="${issuerObj.companySiteUrl}"/>
                                <div id="facingWebSite_error" class=""></div>
                            </div>
                        </div><!-- end of control-group-->
                        
                        <div class="form-actions paddingLR20">
                            <a href='<c:url value="/planmgmt/issuer/financial/info"/>' class="btn"><spring:message  code="label.back"/></a>
                            <button type="submit" class="btn btn-primary"><spring:message  code="label.next"/></button>
                        </div>
                    </form>
                </div>
            </div><!--  end of span9 -->
        	</div>
		</div><!-- end row-fluid -->
  
  <script type="text/javascript">
  $(document).ready(function(){
  $("#btn_company_logo").click(function(){
		$('#frmIssuerComProInfo').ajaxSubmit({
			url: "<c:url value='/planmgmt/issuer/profile/update/company/logo/upload'/>",
			success: function(responseText){
				     var val = responseText.split("|");
				     $("#companyLogo_display").text(val[1]);
					 $("#hdnCompanyLogo").val(val[2]);
					 $("#companyLogo_upload_display").html(val[1]);
	       	}
		});
	  return false; 
	});	// button click close
  }) 
</script>
  
  <script type="text/javascript">
	$('.ttclass').tooltip();
	var validator = $("#frmIssuerComProInfo").validate({ 
		rules : {
			companyLegalName : {required : true},
			companyLogo : {required : false},
			addressLine1 : { required : true},
			//addressLine2 : { required : true},
			city : { required : true},
			zipcode : { required : true, number : true, minlength : 5, maxlength : 5}		
			
		},
		messages : {
			companyLegalName : { required : "<span><em class='excl'>!</em><spring:message  code='err.companyLegalName' javaScriptEscape='true'/></span>"},
			companyLogo: { required : "<span><em class='excl'>!</em><spring:message  code='err.companyLogo' javaScriptEscape='true'/></span>"},
			addressLine1 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
			addressLine2 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine2' javaScriptEscape='true'/></span>"},
			city: { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
			zipcode : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>",
				        number : "<span><em class='excl'>!</em><spring:message  code='err.issuervalidZip' javaScriptEscape='true'/></span>",
				        minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipLength' javaScriptEscape='true'/></span>",
				        maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipLength' javaScriptEscape='true'/></span>"
			          }		
			
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});
</script>
