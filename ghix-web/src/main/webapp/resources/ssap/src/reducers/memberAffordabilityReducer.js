import { memberAffordabilityTypes } from '../constants/actionTypes';
import CONFIG from '../../src/.env.js';

const initialState = {
    'year': CONFIG.ENV.DEVELOPMENT ? 2020 : 0,
    'percent' : CONFIG.ENV.DEVELOPMENT ? 9.78 : 0,
};

export default (state = initialState, action) => {
    switch (action.type) {
    case memberAffordabilityTypes.SET_MEMBER_AFFORDABILITY:
        return action.affordabilityData;
    case memberAffordabilityTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};
