/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.allowedContent =true;
	config.removePlugins = 'elementspath';
	config.fillEmptyBlocks = false;
	config.fullPage = true;
	config.toolbar = [
	              	{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
	              	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
	              	'/',
	              	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline' ] },
	              	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList',  'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',  ] },
	              	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	              	{ name: 'insert', items: [ 'Image',  'Table', 'PageBreak' ] },
	              	'/',
	              	{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	              	{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
	              	{ name: 'tools', items: [ 'ShowBlocks' ] },
	              	
	              	
	              ];
	
	config.height = '400px';
	  //config.width = '600px';

	//config.toolbar=false;
	//SWEFEARDFAEWFEWF
};
