package com.getinsured.hix.batch.referral.service;

import java.util.Date;
import java.util.List;

public interface PassiveEnrollmentNotice {

	String generateNoticeInInbox(PassiveEnrollmentNoticeDTO passiveEnrollmentNotice) throws Exception;

	List<PassiveEnrollmentNoticeDTO> getEnrollmentData(Date startDate,
			Date endDate, List<String> roleNameList, Long ssapAppId);

}
