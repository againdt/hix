package com.getinsured.hix.entity.assister.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.entity.admin.service.AssisterIndTriggerService;
import com.getinsured.hix.entity.assister.repository.IAssisterDesignateRepository;
import com.getinsured.hix.entity.assister.service.AssisterDesignateService;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.entity.DesignateAssister;

/**
 * Implements {@link AssisterDesignateService} to perform Assister Designation
 * related operations like designate,find etc.
 */
@Service("assisterDesignateService")
@Transactional
public class AssisterDesignateServiceImpl implements AssisterDesignateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterDesignateServiceImpl.class);

	@Autowired
	private EntityUtils entityUtils;
	@Autowired
	private IAssisterDesignateRepository assisterDesignateRepository;
	@Autowired
	private AssisterIndTriggerService assisterIndTriggerService;

	/**
	 * @see AssisterDesignateService#saveDesignateAssister(DesignateAssister)
	 */
	@Override
	@Transactional
	public EntityResponseDTO saveDesignateAssister(DesignateAssister designateAssister, int existingAssisterid) {
		
		LOGGER.info("saveDesignateAssister: START");

		EntityResponseDTO entityResponseDTO = null;
		DesignateAssister savedDesignateAssister;
		
		try {		
			if(EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE)){
				if(existingAssisterid!=0){
					DesignateAssister existingDesignateAssister = (DesignateAssister) designateAssister.clone();
					existingDesignateAssister.setAssisterId(existingAssisterid);
					existingDesignateAssister.setStatus(DesignateAssister.Status.InActive);
					EntityResponseDTO response = assisterIndTriggerService.triggerInd47(existingDesignateAssister);
				
					if(response!=null && !EntityUtils.isEmpty(response.getStatus()) && EntityConstants.RESPONSE_SUCCESS.equals(response.getStatus())){
						entityResponseDTO = assisterIndTriggerService.triggerInd47(designateAssister);
						if(entityResponseDTO!=null && !EntityUtils.isEmpty(entityResponseDTO.getStatus()) && EntityConstants.RESPONSE_SUCCESS.equals(entityResponseDTO.getStatus())){
							assisterDesignateRepository.save(designateAssister);
						}
					}
				} else {
				entityResponseDTO = assisterIndTriggerService.triggerInd47(designateAssister);
				if(entityResponseDTO!=null && !EntityUtils.isEmpty(entityResponseDTO.getStatus()) && EntityConstants.RESPONSE_SUCCESS.equals(entityResponseDTO.getStatus())){
					assisterDesignateRepository.save(designateAssister);
				}
				}
			} else {
				entityResponseDTO = new EntityResponseDTO();
				savedDesignateAssister = assisterDesignateRepository.save(designateAssister);
				if(null == savedDesignateAssister) {
					entityResponseDTO.setStatus(EntityConstants.RESPONSE_FAILURE);
				}
				else {			
					entityResponseDTO.setStatus(EntityConstants.RESPONSE_SUCCESS);
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Error while sending Assister information to AHBX : ",	exception);
		} finally {
			LOGGER.info("saveDesignateAssister: END");
		}
		
		return entityResponseDTO;
	}

	/**
	 * @see AssisterDesignateService#deDesignateAssister(int)
	 */
	@Override
	public int deDesignateAssister(int assisterId) {
		
		LOGGER.info("deDesignateAssister: START");
		LOGGER.info("deDesignateAssister: END");
		
		return assisterDesignateRepository.deleteDesignateAssisterByAssisterId(assisterId);
	}

	/**
	 * @see AssisterDesignateService#findAssisterByIndividualId(int)
	 */
	@Override
	public DesignateAssister findAssisterByIndividualId(int individualId) {
		
		LOGGER.info("findAssisterByIndividualId: START");
		LOGGER.info("findAssisterByIndividualId: END");
		
		return assisterDesignateRepository.findByIndividualId(individualId);
	}

	/**
	 * @see DesignateAssisterService#deDesignateIndividualForAssister(int, int)
	 */
	@Override
	@Transactional
	public int deDesignateIndividualsForAssister(int assisterId) {
		
		LOGGER.info("deDesignateIndividualsForAssister: START");
		
		LOGGER.info("AssisterDesignateServiceImpl : deDesignateIndividualForAssister : START");
		int count = 0;

		// retrieve designated individuals
		List<DesignateAssister> designatedIndividuals = getDesignateIndividualsByAssisterId(assisterId);

		// iterate over designated individuals list to inactive the record
		if (designatedIndividuals != null && !designatedIndividuals.isEmpty()) {
			for (DesignateAssister designateAssister : designatedIndividuals) {
				
				if(!DesignateAssister.Status.InActive.equals(designateAssister.getStatus())){
					designateAssister.setStatus(DesignateAssister.Status.InActive);
					int loggedInUserId = EntityUtils.getLoggedInUser();
					if(loggedInUserId != -1)
					{
						//designateAssister.setCreatedBy(String.valueOf(loggedInUserId));
						designateAssister.setLastUpdatedBy(Long.valueOf(loggedInUserId));
					}
					
					// Update Enrollment details if the admin De-certified counselor request, De-certified failure to renew, Decertified-misconduct	assister		
					entityUtils.updateEnrollmentDetails(EntityConstants.ENROLLMENT_TYPE_INDIVIDUAL,
							 new Long(designateAssister.getIndividualId()),designateAssister.getAssisterId(), EntityConstants.ASSISTER_ROLE,
							EntityConstants.REMOVEACTIONFLAG);

					designateAssister = assisterDesignateRepository.save(designateAssister);
					
					assisterIndTriggerService.triggerInd47(designateAssister);

					// counting number of updated records
					count++;
				}
				
			}
	
		}
		LOGGER.info("AssisterDesignateServiceImpl : deDesignateIndividualForAssister : END");
		
		LOGGER.info("deDesignateIndividualsForAssister: END");
		
		return count;
	}

	@Override
	public List<DesignateAssister> getDesignateIndividualsByAssisterId(int assisterId) {
		
		LOGGER.info("getDesignateIndividualsByAssisterId: START");
		LOGGER.info("getDesignateIndividualsByAssisterId: END");
		
		return assisterDesignateRepository.findByAssisterId(assisterId);
	}
}
