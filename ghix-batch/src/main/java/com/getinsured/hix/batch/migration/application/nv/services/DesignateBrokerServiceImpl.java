package com.getinsured.hix.batch.migration.application.nv.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.batch.agency.repository.IBrokerRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.household.repository.HouseholdRepository;
import com.getinsured.timeshift.util.TSDate;

/**
 * Implements {@link DesignateService} to designate / de-designate employers /
 * individuals
 * 
 * @author kanthi_v
 * 
 */
@Service("designateService")
@Transactional
public class DesignateBrokerServiceImpl implements DesignateService {

	private static final String EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT = "Exception occurred while designating Agent : ";

	private static final String DESIGNATE_INDIVIDUAL_FOR_BROKER_END = "DesignateIndividualForBroker: END";

	private static final String DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_START = "DesignateIndividualForBroker: START";
	
	private static final String DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_BY_CASEID_START = "DesignateIndividualForBrokerByCaseId: START";

	private static final Logger LOGGER = LoggerFactory.getLogger(DesignateBrokerServiceImpl.class);

	@Autowired
	private IDesignateBrokerRepository brokerDesignateRepository;
    @Autowired
    private IBrokerRepository brokerRepository;
    
    @Autowired
    private HouseholdRepository householdRepository; 
    
    @Autowired
    private UserService userService;

    @Override
	@Transactional
	public Integer findUserIdByName(String userName)  {	
    	AccountUser user =  userService.findByUserName(userName);
    	Integer id = null;
    	if(user != null)
    	{
    		id= user.getId();
    	}
    	return id;
    }
    
	/**
	 * @throws Exception 
	 * @see DesignateService#designateIndividualForBroker(int, int)
	 */
	@Override
	@Transactional
	public DesignateBroker designateIndividualForBroker(String brokerNpn, String emailId) throws GIException {
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_START);
		
		DesignateBroker dedesignatedBroker = null;
		DesignateBroker designateBroker = null;
		try{
			Broker broker = brokerRepository.findByNpn(brokerNpn);
			List<Household> households =  householdRepository.findByEmailIgnoreCaseOrderByIdDesc(emailId);
			if (broker != null && broker.getUser()!=null && !CollectionUtils.isEmpty(households)) {
				LOGGER.debug("De-designating external individual : " + households.get(0).getId());
				
				designateBroker = brokerDesignateRepository.findDesigBrokerByAgentAndIndividualId(broker.getId(), households.get(0).getId());
				
				if(designateBroker==null){
					designateBroker = new DesignateBroker();
				}
				designateBroker.setBrokerId(broker.getId());
				designateBroker.setIndividualId(households.get(0).getId());
				designateBroker.setStatus(Status.Active);
				designateBroker.setCreatedBy(broker.getUser().getId());
				designateBroker.setCreated(new TSDate());
				designateBroker.setEsignBy(broker.getUser().getFirstName() + ' ' + broker.getUser().getLastName());
				designateBroker.setEsignDate(new TSDate());
				designateBroker.setShow_switch_role_popup('Y');
				dedesignatedBroker = brokerDesignateRepository.save(designateBroker);
				
			}
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
			throw new GIException(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
		}

		LOGGER.info(DESIGNATE_INDIVIDUAL_FOR_BROKER_END);
		
		return dedesignatedBroker;
	}
	
	@Override
	@Transactional
	public DesignateBroker designateIndividualForBrokerByCaseId(String brokerNpn, String caseId, Integer userId, String firstName, String lastName) throws GIException {
		
		LOGGER.info(DE_DESIGNATE_INDIVIDUAL_FOR_BROKER_BY_CASEID_START);
		
		DesignateBroker dedesignatedBroker = null;
		DesignateBroker designateBroker = null;
		try{
			List<Integer> brokerIds = brokerRepository.getBrokerIdsByNpn(brokerNpn);
			List<Integer> households =  householdRepository.findByCaseIdOrderByIdDesc(caseId);
			if (!CollectionUtils.isEmpty(brokerIds) && !CollectionUtils.isEmpty(households)) {
				LOGGER.debug("De-designating external individual : " + households.get(0));
				List<DesignateBroker> designateBrokers = brokerDesignateRepository.findDesigBrokerByIndividualId(households.get(0));
				//designateBroker = brokerDesignateRepository.findDesigBrokerByIndividualId(households.get(0));
				boolean updateNeeded = true;
				if(CollectionUtils.isEmpty(designateBrokers)){
					designateBroker = new DesignateBroker();
				}
				else
				{	
					designateBroker = designateBrokers.get(0);
					if((brokerIds.get(0)).equals(designateBroker.getBrokerId())) {
						updateNeeded = false;
					}
					if (designateBrokers.size() > 1)
					{
						deleteDuplicateBrokers(designateBrokers);
					}
				}
				if(updateNeeded) {
					designateBroker.setBrokerId(brokerIds.get(0));
					designateBroker.setIndividualId(households.get(0));
					designateBroker.setStatus(Status.Active);
					designateBroker.setCreatedBy(userId);
					designateBroker.setCreated(new TSDate());
					designateBroker.setEsignBy(firstName+ ' ' + lastName);
					designateBroker.setEsignDate(new TSDate());
					designateBroker.setShow_switch_role_popup('Y');
					dedesignatedBroker = brokerDesignateRepository.save(designateBroker);
				}
			}
		} catch(Exception exception){
			LOGGER.error(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
			throw new GIException(EXCEPTION_OCCURRED_WHILE_DESIGNATING_AGENT, exception);
		}

		LOGGER.info(DESIGNATE_INDIVIDUAL_FOR_BROKER_END);
		
		return dedesignatedBroker;
	}
	
	private void deleteDuplicateBrokers(List<DesignateBroker> designateBrokers) {
		int i=1;
		for(i=0;i<designateBrokers.size();i++)
		{
			brokerDesignateRepository.delete(designateBrokers.get(i).getId());
		}
		
	}
	
	@Override
	@Transactional
	public void deleteDesignatedBrokerByCaseId(String householdCaseId)
	{
		List<Integer> householdIds = householdRepository.findByCaseIdOrderByIdDesc(householdCaseId);
		if(!CollectionUtils.isEmpty(householdIds))
		{
			brokerDesignateRepository.deleteDesignatedBrokerByCaseId(householdIds.get(0));
		}
	}
	
}

