<%--
  Created by IntelliJ IDEA.
  User: shellnut_b
  Date: 2019-03-01
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%
    Broker mybroker = (Broker)request.getAttribute("broker");
    long isProfileExists = 0;
    if(mybroker != null)
    {
        isProfileExists = mybroker.getId();

    }

%>
<script type="text/javascript">
    window.csrf_token = '<df:csrfToken plainToken="true" />';
</script>
<script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>
<div class="gutter10">
    <div class="l-page-breadcrumb">
        <!--start page-breadcrumb -->
        <div class="row-fluid">
            <ul class="page-breadcrumb">
                <li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
                <li><a href="<c:url value="/broker/viewcertificationinformation"/>"><spring:message code="label.account" /></a></li>
                <li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
            </ul>
        </div>
        <!--  end of row-fluid -->
    </div>
    <div class="row-fluid">
        <h1 id="skip">${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
    </div>
    <div class="row-fluid">
        <!-- #sidebar -->
        <jsp:include page="brokerLeftNavigationMenu.jsp" />
<%--<script type="text/ng-template" id="helpOnDemand">--%>
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
            <div id="brokerReactID"></div>
            <link rel="stylesheet"  href="<gi:cdnurl value="/resources/react/broker/build/static/css/main.css"/>">
            <script type="text/javascript" src="<gi:cdnurl value="/resources/react/broker/build/static/js/main.js"/>">
            </script>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        if(window.route_page === 'participation') {
            $("#brkHelpOnDemand").removeClass("link");
            $("#brkHelpOnDemand").addClass("active");
        } else if(window.route_page === 'connect') {
            $("#brkConnect").removeClass("link");
            $("#brkConnect").addClass("active");
        }

        $("a[title='My Information']").parent().addClass("active")
    });
</script>
