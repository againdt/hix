package com.getinsured.iex.hub.nmec.servicehandler;

import java.util.Iterator;

import javax.xml.bind.JAXBElement;

import org.json.simple.parser.ParseException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ObjectFactory;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.VerifyNonESIMECRequestType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.VerifyNonESIMECResponseType;
import com.getinsured.iex.hub.nmec.prefixmappers.NonMECNamespaceMapper;
import com.getinsured.iex.hub.nmec.wrapper.ApplicantTypeWrapper;
import com.getinsured.iex.hub.nmec.wrapper.IndividualRequestTypeWrapper;
import com.getinsured.iex.hub.nmec.wrapper.USStateCodeTypeWrapper;
import com.getinsured.iex.hub.nmec.wrapper.VerifyNonESIMECResponseTypeWrapper;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ssap.iex.HouseholdMember;
import com.getinsured.iex.hub.platform.ssap.iex.SSAP;
import com.getinsured.iex.hub.platform.ssap.iex.SSAPTaxHouseHold;

public class NonESIMECApplicantServiceHandler extends
		com.getinsured.iex.hub.platform.AbstractServiceHandler {
	
	private Jaxb2Marshaller marshaller;
	public Object getRequestpayLoad() throws HubServiceException {
		HubServiceException he = null;
		Object response = null;
		try {
			response = getPayLoadFromJSONInput();
		} catch (HubServiceException e) {
			he = e;
		} 
		if (he != null) {
			throw he;
		}
		return response;
	}


	public Object getPayLoadFromJSONInput()
			throws HubServiceException {

		SSAP ssap = null;
		try{
			ssap = new SSAP(getJsonInput());
		}catch(ParseException ex){
			listJSONKeys();
			throw new HubServiceException("Failed to process input data ["+ex.getMessage()+"]", ex);
		}
		
		ObjectFactory objectFactory = new ObjectFactory();
		VerifyNonESIMECRequestType request = objectFactory
				.createVerifyNonESIMECRequestType();
		this.setServiceName("NMEC");
		HouseholdMember houseHoldMember = null;
		SSAPTaxHouseHold taxHousehold = null;
		
		Iterator<SSAPTaxHouseHold> householdCursor = ssap.houseHoldIterator();
		while(householdCursor.hasNext()){
			taxHousehold = householdCursor.next();
			Iterator<HouseholdMember> memberCursor = taxHousehold.iterator();
			
			while(memberCursor.hasNext()){
					houseHoldMember =memberCursor.next();
					IndividualRequestTypeWrapper individualRequestTypeWrapper = new IndividualRequestTypeWrapper();
					try {
						this.extractSSAPData(houseHoldMember, individualRequestTypeWrapper);
					
						ApplicantTypeWrapper applicantWrapper = new ApplicantTypeWrapper();
						this.extractSSAPData(houseHoldMember, applicantWrapper);
						this.extractSSAPData(houseHoldMember.getSsnInfo(), applicantWrapper);
						USStateCodeTypeWrapper usStateCodeTypeWrapper = new USStateCodeTypeWrapper();
						this.extractSSAPData(houseHoldMember.getHomeAddress(), usStateCodeTypeWrapper);
						
						individualRequestTypeWrapper
								.setApplicantTypeWrapper(applicantWrapper);
						individualRequestTypeWrapper.setUSStateCodeTypeWrapper(usStateCodeTypeWrapper);
		
						request.getIndividualRequest().add(
								individualRequestTypeWrapper.getSystemRepresentation());
					} catch (HubMappingException e) {
						throw new HubServiceException(e.getMessage(), e);
					}
			}
		}
		return objectFactory.createVerifyNonESIMECRequest(request);
	}

	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		JAXBElement<VerifyNonESIMECResponseType> resp = (JAXBElement<VerifyNonESIMECResponseType>) responseObject;
		VerifyNonESIMECResponseType payload = resp.getValue();
		VerifyNonESIMECResponseTypeWrapper payloadWrapper = new VerifyNonESIMECResponseTypeWrapper(
				payload);
		this.setResponseCode(payloadWrapper.getResponseCode());
		this.requestStatus = RequestStatus.SUCCESS;
		return payloadWrapper.toJSONString();

	}


	@Override
	public RequestStatus getRequestStatus() {
	return this.requestStatus;
	}
	

	@Override
	public String getServiceIdentifier() {
		return "H31";
	}
	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem",new NonMECNamespaceMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem",new NonMECNamespaceMapper());
		}
		return this.marshaller;
	}

}
