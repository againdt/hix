<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:formatDate pattern="MM/dd/yyyy" value="${enrollmentStartDt}" var="enollStartDate" />
<c:choose>
	<c:when test="${enrollmentStartDt == ''}">
		<p><spring:message  code="label.noactiveorder"/></p>
	</c:when>
	<c:otherwise>
		<p><spring:message  code="label.openenrollment"/></p>
  		<h4><spring:message  code="label.openenrollbegin"/> ${enollStartDate}</h4>
	</c:otherwise>
</c:choose>
        		  
       		