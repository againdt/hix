<%@page import="java.util.HashMap"%>
<%@ taglib prefix="surl" uri="/WEB-INF/tld/secure-url.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<c:set var="encHouseHoldId" ><encryptor:enc value="${householdId}" isurl="true"/> </c:set>
<link href="<c:url value="/resources/css/daterangepicker-bs2.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/css/bootstrap-datetimepicker.min.css"/>" rel="stylesheet" type="text/css">
<script src="<c:url value="/resources/js/cap/moment.min.js"/>"></script>
<script src="<c:url value="/resources/js/cap/daterangepicker.js"/>"></script>
<script src="<c:url value="/resources/js/highcharts.js"/>"></script>
<script src="<c:url value="/resources/js/cap/bootstrap-datetimepicker.min.js"/>"></script>

<c:set var="encHouseholdId"><encryptor:enc value='${householdId}'/></c:set>
<c:set var="encHouseholdEmail"><encryptor:enc value='${household.email}'/></c:set>
<c:set var="encHouseholdPhone"><encryptor:enc value='${household.phoneNumber}'/></c:set>


<jsp:include page="../crm/sendConsumerActivationLink.jsp">
	<jsp:param value="${encHouseholdId}" name="householdId"/>
	<jsp:param value="${encHouseholdEmail}"  name="householdEmail"/>
	<jsp:param value="${encHouseholdPhone}" name="householdPhone"/>
</jsp:include>

<script type="text/javascript">
$(document).ready(function() {
	$("#historyMenu").addClass("active navmenu");
});
var csrfToken = {
        getParam: '${df:csrfTokenParameter()}=<df:csrfToken plainToken="true"/>',
        paramName: '${df:csrfTokenParameter()}',
        paramValue: '<df:csrfToken plainToken="true"/>'
    };

var historyServiceUrl='<c:url value="/crm/consumer/searchAppEvents" />';
var callLog='<c:url value="/cap/history/calllog" />';
var commentUrl='<c:url value="/crm/member/appeventcomments" />';
var moduleId="${encHouseHoldId}";

var Categories={};
var roleList=[];
var houseHoldId='${householdId}';

var logCommentsVar = ${permissionMap.memberAbtLogcall};
var showCommentsVar = ${permissionMap.memberAbtShowcomments};


Categories=${typeAndNames};
roleList=${roleList};



var Const={CATEGORIES:Categories};

var dateRangePickerOptions={
        locale:'MM/DD/YYYY',
        startDate:moment().subtract(1, 'year'),
        linkedCalendars:true,
        endDate:moment(),
        customRange:[moment().subtract(1, 'month'),moment()],
        ranges: {
           'All': [moment().subtract(1,'year'), moment()],
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    };
    
</script>
<div ng-app="customerHistoryApp">
 	<div ng-controller="mainController">
	
		<div class="gutter10"  id="consumerHistory">
		    <div class="row-fluid">
		        <h1 id="skip" class="householdInfo">${household.firstName} ${household.lastName} (ID: ${householdId})</h1>
		    </div>
		    <div class="row-fluid">
		        <div id="sidebar" class="span3">
		           <jsp:include page="consumerSummaryNavigation.jsp"></jsp:include>
				</div>
				<div class="span9" id="rightpanel">
			    	<div class="row-fluid">
			            <div class="header margin20-b">
			                <h4>Consumer History</h4>
			            </div>
	            		<div class="gutter10">
			            	<div ng-include src="'<c:url value="/resources/html/cap/partials/customerHistoryV2.html "/>'"> </div>
			            </div>
			        </div>
			   	</div>
			</div>
		</div>
 	</div>
 </div>

<!-- gutter10 -->
<!--  Latest UI -->
<!-- Modal 
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employer/home/${employerId}" />" novalidate="novalidate">
<form name="dialogForm" id="dialogForm" action="<c:url value="/account/user/switchUserRole/employer/${employerId}" />" novalidate="novalidate">-->
<form name="dialogForm" id="dialogForm" method="POST"  action="<c:url value="/crm/consumer/dashboard" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Member Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body clearfix gutter10-lr">
    <div class="control-group">	
				<div class="controls">
                Clicking "Member View" will take you to the Member's portal for ${household.firstName} ${household.lastName}.<br/>
                Through this portal you will be able to take actions on behalf of the member.<br/>
                Proceed to Member view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
        <input class="pull-left"  type="checkbox" id="checkConsumerView" name="checkConsumerView"  > 
        <div class="pull-left">&nbsp; Don't show this message again.</div>
        <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-primary" type="submit">Member View</button>
        <input type="hidden" name="switchToModuleName" id="switchToModuleName" value='<encryptor:enc value="individual"/>' />
        <input type="hidden" name="switchToModuleId" id="switchToModuleId" value='<encryptor:enc value="${householdId}"/>'  />
        <input type="hidden" name="switchToResourceName" id="switchToResourceName" value='${household.firstName} ${household.lastName}'  />
    <!--a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
  		<input type="hidden" name="householdId" id="householdId" value='<encryptor:enc value="${householdId}"/>'  />

<script src="<c:url value="/resources/js/cap/customerHistoryV2.js"/>"></script> 
<script src="<c:url value="/resources/js/parsley.2.0.js" />"></script>