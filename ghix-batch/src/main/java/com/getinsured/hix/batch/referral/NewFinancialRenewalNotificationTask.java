/**
 *
 */
package com.getinsured.hix.batch.referral;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.eligibility.enums.ApplicationSource;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.batch.referral.service.NewFinancialRenewalService;

/**
 * @author Biswakesh
 *
 */
public class NewFinancialRenewalNotificationTask extends BaseLCEReminderNotificationTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(NewFinancialRenewalNotificationTask.class);

	private static final long coverageYear = 2020;

	private NewFinancialRenewalService newFinancialRenewalService;

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution, org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		List<Long> ssapIds = null;

		try {
			ssapIds = newFinancialRenewalService.getSsapApplicationIds(RenewalStatus.SEND_ACTIVATION_LINK, ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode(), coverageYear, EligibilityStatus.AE, ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
			if(null != ssapIds && !ssapIds.isEmpty()) {
				newFinancialRenewalService.processSsapIds(ssapIds);
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE SENDING FINANCIAL RENEWAL NOTICE: ",ex);
		}

		return RepeatStatus.FINISHED;
	}

	public NewFinancialRenewalService getNewFinancialRenewalService() {
		return newFinancialRenewalService;
	}

	public void setNewFinancialRenewalService(
			NewFinancialRenewalService newFinancialRenewalService) {
		this.newFinancialRenewalService = newFinancialRenewalService;
	}



}
