package com.getinsured.eligibility.prescreen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.prescreen.MarketPlacePremium;

/**
 * 
 * @author bhatia_s
 *
 */
@Repository
public interface IPrescreenPremiumRepository  extends JpaRepository<MarketPlacePremium, Long>{

	@Query("SELECT mktp FROM MarketPlacePremium mktp, RatingArea ra, ZipCountyRatingArea zipcra WHERE zipcra.ratingArea = ra.id AND ra.id = mktp.ratingArea AND ((zipcra.countyFips=:strCountyFips AND zipcra.zip is null) or (zipcra.countyFips=:strCountyFips AND zipcra.zip =:strZip))")	
	MarketPlacePremium getRatingAreaByZipAndCounty(@Param("strZip") String strZip, @Param("strCountyFips") String strCountyFips);
	
}
