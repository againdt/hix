<%@ page import="com.getinsured.hix.model.GroupList,java.util.List,java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<style>
	.view-broker {
		top: 50px!important;
	}
	
.tile:before {
    background: none repeat scroll 0 0 rgba(190, 217, 219, 0.6);
    content: "";
    font-size: 24px;
    font-weight: bold;
    height: 100%;
    position: absolute;
    width: 100%;
    z-index: 1;
}
.tile:after {
    color: #666666;
    content: "<spring:message code="pd.label.commontext.notAvailable"/>";
    font-size: 28px;
    font-weight: bold;
    position: absolute;
    right: 29px;
    top: 75px;
    transform: rotate(322deg);
    z-index: 10;
}
</style>

<link rel="stylesheet" href="<c:url value="/resources/css/tiles.css" />" type="text/css">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui-1.css"/>">

<div class="row-fluid top-nav">
 <h1 class="span10 choose-plan txt-center"><spring:message code="pd.label.renewal.title.2"/></h1>
</div>

<div class="row-fluid">
  <div class="span3"></div><!--sidebar-->  
  <div id="rightpanel" class="span9"> 
  <div>
  <c:choose>
  	<c:when test="${enrollmentType == 'A'}">
		<p><b><spring:message code="pd.label.showcart.text.update"/>:</b> <spring:message code="pd.label.showcart.text.existingPlansUnavailable"/></p>
    </c:when>
    <c:otherwise>
    	<p><b><spring:message code="pd.label.showcart.text.update"/>:</b> <spring:message code="pd.label.title.message27"/></p>
    </c:otherwise>
  </c:choose>
  </div>
  
</div>
</div>

  
<div class="row-fluid">
  <div class="span3"></div><!--sidebar-->  
  <div id="rightpanel" class="span9">
    <c:forEach var="individualPlan" varStatus="status" items="${unAvailablePlans}"> 
      <div class="tile">			
        <div class="tile-header">
          <div class="planimg"><img class="carrierlogo hide" src='${individualPlan.issuerLogo}' alt="Logo ${individualPlan.issuer}" onload="resizeimage(this)" /></div>
          <div class="payment">$${individualPlan.premiumBeforeCredit} </div>
          <div class="plan-options">
          <c:choose>
        	<c:when test="${individualPlan.planType == 'HEALTH'}">
			  <table class="table">
				<tr>
				  <th class="span1"><spring:message code="pd.label.title.officeVisits"/></th>
				  <td><p><small>${individualPlan.planTier1.PRIMARY_VISIT.tileDisplayVal}</small></p></td>
				</tr>
				<tr>
				  <th><spring:message code="pd.label.title.genericDrugs"/></th>
				  <td><p><small>${individualPlan.planTier1.GENERIC.tileDisplayVal}</small></p></td>
				</tr>
				<tr>
			      <th><spring:message code="pd.label.commontext.deductible"/></th>
			      <td><p><small>
			      	<c:choose>
                  	  <c:when test="${(individualPlan.intgMediDrugDeductible != null)}">
                  	  	$<fmt:formatNumber value="${individualPlan.intgMediDrugDeductible}" pattern = "0"/>
                	  </c:when>
                	  <c:when test="${individualPlan.medicalDeductible != null && individualPlan.drugDeductible != null}">
                  	  	$${individualPlan.medicalDeductible} / $${individualPlan.drugDeductible}
                	  </c:when>
                	  <c:when test="${(individualPlan.medicalDeductible != null)}">
	                  	$<fmt:formatNumber value="${individualPlan.medicalDeductible}" pattern = "0"/>
                	  </c:when>
                	  <c:when test="${(individualPlan.drugDeductible != null)}">
                 	  	$<fmt:formatNumber value="${individualPlan.drugDeductible}" pattern = "0"/>
                	  </c:when>
                	  <c:otherwise><spring:message code="pd.label.commontext.notApp" htmlEscape="true"/></c:otherwise>
              	  	</c:choose>
			  	  </small></p></td>
				</tr>	
				<c:if test="${showOopOnOff == 'ON'}">
			  	  <tr>
			    	<th><spring:message code="pd.label.oopmax" htmlEscape="true"/>></th>
			    	<td><p><small>
			      	  <c:choose>
                		<c:when test="${individualPlan.intgMediDrugOopMax != null}">
                  	  	  $<fmt:formatNumber value="${individualPlan.intgMediDrugOopMax}" pattern = "0"/>
                    	</c:when>
                    	<c:when test="${individualPlan.medicalOopMax != null && individualPlan.drugOopMax != null}">
                  	  	  $${individualPlan.medicalOopMax} / $${individualPlan.drugOopMax}
                		</c:when>
                		<c:when test="${individualPlan.medicalOopMax != null}">
                 	  	  $<fmt:formatNumber value="${individualPlan.medicalOopMax}" pattern = "0"/>
                		</c:when>
                		<c:when test="${individualPlan.drugOopMax != null}">
                 	  	  $<fmt:formatNumber value="${individualPlan.drugOopMax}" pattern = "0"/>
                		</c:when>
                		<c:otherwise><spring:message code="pd.label.commontext.notApp" htmlEscape="true"/></c:otherwise>
              	  	  </c:choose>
			  		</small></p></td>
			  	  </tr>
				</c:if>
			  </table>
			</c:when>		
			<c:when test="${individualPlan.planType == 'DENTAL'}">
			  <table class="table">
				<tr>
        		  <th class="span1"><spring:message code="pd.label.title.routineDentalAdult" htmlEscape="true"/></th>
        		  <td><p><small>
        		  	<c:choose>
        			  <c:when test="${individualPlan.planTier1.RTN_DENTAL_ADULT.displayVal != null && planTier1.RTN_DENTAL_ADULT.displayVal != ''}">
        				${individualPlan.planTier1.RTN_DENTAL_ADULT.displayVal}
        		      </c:when>
        		      <c:otherwise>
        		    	<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
        		      </c:otherwise>
        		    </c:choose>
          		  </small></p></td>
      			</tr>
      			<tr>
        		  <th><spring:message code="pd.label.title.dentalCheckupChild" htmlEscape="true"/></th>
        		  <td><p><small>
        		  	<c:choose>
        			  <c:when test="${individualPlan.planTier1.DENTAL_CHECKUP_CHILDREN.displayVal != null && planTier1.DENTAL_CHECKUP_CHILDREN.displayVal != ''}">
        				${individualPlan.planTier1.DENTAL_CHECKUP_CHILDREN.displayVal}
        		      </c:when>
        		      <c:otherwise>
        		    	<spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
        		      </c:otherwise>
        		    </c:choose>
          		  </small></p></td>
      			</tr>
      			<tr>
        		  <th><a href="#" rel="tooltip" data-placement="bottom" data-html="true" data-original-title="<strong><spring:message code="pd.label.stm.deductible.deductible"/></strong><br><spring:message code="pd.label.tooltip.deductible"/>"><spring:message code="pd.label.commontext.deductible" htmlEscape='true'/></a></th>
        		  <td><p><small>
        		    <c:choose>
        		  	  <c:when test="${personCount != '' && personCount != 0 && personCount > 1}">
        		  	    <c:choose>
        				  <c:when test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL != null  && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != ''}">
        				    $${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly}
        			      </c:when>
        		    	  <c:otherwise>
        		    	    <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
        		    	  </c:otherwise>
        		  	    </c:choose>
        		  	  </c:when>
        		  	  <c:when test="${individualPlan.planCosts.DEDUCTIBLE_MEDICAL != null  && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != ''}">
        			    $${individualPlan.planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd}
        			  </c:when>
        		  	  <c:otherwise>
        		  		<p><spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/></p>
        		  	  </c:otherwise>
        		    </c:choose>
          		  </small></p></td>
        		</tr>
        		<c:if test="${showOopOnOff == 'ON'}">
        		  <tr>  
		   		    <th><spring:message code="pd.label.oopmax"/></th>
		            <td><small>
		          	  <c:choose>
        			    <c:when test="${individualPlan.oopMax != null}">
          				  ${individualPlan.oopMax}
        		        </c:when>
        		        <c:otherwise>
          		    	  <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
        		        </c:otherwise>
        		      </c:choose>
        		    </small></td>
        		  </tr>
        		</c:if>
			  </table>
			</c:when>
		  </c:choose>
		</div>
      </div>
    </div>              	    
  </c:forEach>
  <div class="margin-top50 form-horizontal">
 <c:choose>
  <c:when test="${productType !='D'}">
     <a href="<c:url value='/private/setSpecialEnrollmentType?insuranceType=HEALTH'/>" class="btn btn-primary pull-right">
       <spring:message code="pd.label.showcart.text.shopForNewPlan"/><i class="icon-caret-right"></i>
     </a>
  </c:when>
  <c:when test="${productType =='D'}">
     <a href="<c:url value='/private/setSpecialEnrollmentType?insuranceType=DENTAL'/>" class="btn btn-primary pull-right">
       <spring:message code="pd.label.showcart.text.shopForNewPlan"/><i class="icon-caret-right"></i>
     </a>
     
  </c:when>
 </c:choose>
  </div>
</div> <!--#rightpanel  -->
</div>
 
 
 
 
 <script type="text/javascript">

/* var progressbarwrapPosition = $('.navbar').height(); */

/* $(window).load(function() {
	$('html, body').animate({scrollTop: progressbarwrapPosition}, 800);	
	$('.navbar-fixed-top').css({top : '-20px',  position : 'absolute'});
}); */

var posWas;

$(window).bind('scroll', function(){ 
	var pos = $(window).scrollTop(); //position of the scrollbar
	
	if(pos > posWas){ //if the user is scrolling down...
		$('#progressbarwrap').css({top : '0px'});
	}
	if(pos < posWas){ //if the user is scrolling up...
		 $('#progressbarwrap').css({top : '0px'});
		if(pos == 0){
			$('#progressbarwrap').animate({top : progressbarwrapPosition}, 300);
		}
	}
	
	posWas = pos; //save the current position for the next pass
	
});

function resizeimage(imageObj){
	  var maxWidth = 140;
	  var maxHeight = 40;
      var width = imageObj.width;
      var height = imageObj.height;

      // Check if the current width is larger than the max
      if(width > maxWidth){
        ratio = maxWidth / width; // get ratio for scaling image
        imageObj.style.width =  maxWidth+'px'; // Set new width
        imageObj.style.height = (height * ratio)+'px'; // Scale height based on ratio
        height = height * ratio; // Reset height to match scaled image
        width = width * ratio; // Reset width to match scaled image
      }
      // Check if current height is larger than max
      if(height > maxHeight){                                     
        ratio = maxHeight / height; // get ratio for scaling image
        imageObj.style.height= maxHeight+'px'; // Set new height
        imageObj.style.width = (width * ratio) +'px'; // Scale width based on ratio                                        
      }
      imageObj.style.display = "block";
 }

</script>
    	