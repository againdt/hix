package com.getinsured.hix.broker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.BrokerNumber;

public interface IBrokerNumberRepository extends RevisionRepository<BrokerNumber, Integer, Integer>,
		JpaRepository<BrokerNumber, Integer> {

	BrokerNumber findByBrokerId(int brokerId);

	BrokerNumber findById(long brokerNumber);

}