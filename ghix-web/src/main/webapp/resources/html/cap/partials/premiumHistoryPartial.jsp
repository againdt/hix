<div spinning-loader="loader"></div>
<form novalidate name="premiumHistoryForm">
<%@ page import ="java.util.*" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<style type="text/css">
.fitwidth {
 width: 100px;
 white-space: nowrap;
}
</style>

<input type="hidden" id="permissionMap" value="${permissionMap}" />

<span style="display:none" id="redirectPremiumHistory">${redirectPremiumHistory}</span>

	<div class="header margin20-b">
		<h4>
			Enrollment - Premium History
			<span ng-if="vm.isEditMode === false" class="pull-right" style="margin-top: -5px;">
				<!-- <button ng-if="isEnrollmentEditAllowed === 'Y'" ng-click="vm.editPremium()" class="btn btn-primary">Edit</button> -->
				<button type="button" ng-click="vm.goToEnrollmentHistoryPage()" class="btn">Back</button>
			</span>
			
			<span ng-if="vm.isEditMode === true" class="pull-right" style="margin-top: -5px;">
				<button ng-click="vm.validateAndSaveEnrollmentData()" class="btn btn-primary" ng-disabled="(vm.fromPage !== 'addOrChangeEnrollmentTxn' && (premiumHistoryForm.$pristine || premiumHistoryForm.$invalid)) || (vm.fromPage === 'addOrChangeEnrollmentTxn' && premiumHistoryForm.$invalid)">Continue</button>
				<button ng-click="vm.cancelPremiumUpdate()" class="btn">Cancel</button>
			</span>
		</h4>
	</div>
	
	<div class="gutter10 enrollment-history">
		<div class="row-fluid" ng-show="vm.isEditMode === false || {{memberEnrlmntStrtenddt}} === true || {{memberEnrlmntStrtenddtEditable}} === true">
			<table class="table table-condensed span6">
				<tr>
					<th>Enrollment Start Date<img ng-if="vm.isEditMode === true" width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></th>
					<td ng-if="vm.isEditMode === false">{{vm.enrollment.enrollmentBenefitEffectiveStartDateUI}}</td>
					<td ng-if="vm.isEditMode === true">
						<input type="text" ng-if="memberEnrlmntStrtenddt || memberEnrlmntStrtenddtEditable" date-validation="date" class="input-small" name="enrollmentBenefitEffectiveStartDateUI" ng-model="vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI" 
						ng-change="vm.updateEnrollmentDate('start')" ui-mask="99/99/9999" model-view-value="true" required ng-minlength="10" placeholder="MM/DD/YYYY" ng-disabled="!memberEnrlmntStrtenddtEditable">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="error-message" ng-if="premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$error.date && premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$dirty"> 
							The entered value is not a valid date. Please enter the date in MM/DD/YYYY format.
						</span>
						<span class="error-message" ng-if="premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$error.required && premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$dirty"> 
							Please enter enrollment start date.
						</span>
						<span class="error-message" ng-if="premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$error.cancelNotAllowed"> 
							Enrollment/Subscriber level cancellation is not allowed from request type "Change", Use "Cancel Policy" request.
						</span>
						<span class="error-message" ng-if="premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$error.birthDateBeforStartDate"> 
							Enrollment coverage dates cannot be earlier than the subscriber's birthdate.
						</span>
					</td>
				</tr>
		
			</table>
			
			<table class="table table-condensed span6">
				<tr>
					<th>Enrollment End Date<img ng-if="vm.isEditMode === true" width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></th>
					<td ng-if="vm.isEditMode === false">{{vm.enrollment.enrollmentBenefitEffectiveEndDateUI}}</td>
					<td ng-if="vm.isEditMode === true">
						<input type="text" ng-if="memberEnrlmntStrtenddt || memberEnrlmntStrtenddtEditable" date-validation="date" class="input-small" name="enrollmentBenefitEffectiveEndDateUI" ng-model="vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI" ng-change="vm.updateEnrollmentDate('end')" ui-mask="99/99/9999" model-view-value="true" required ng-minlength="10" placeholder="MM/DD/YYYY" ng-disabled="!memberEnrlmntStrtenddtEditable">	
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="error-message" ng-if="premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$error.date && premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$dirty"> 
							The entered value is not a valid date. Please enter the date in MM/DD/YYYY format.
						</span>
						<span class="error-message" ng-if="premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$error.required && premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$dirty"> 
							Please enter enrollment end date.
						</span>
						<span class="error-message" ng-if="premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$error.cancelNotAllowed"> 
							Enrollment/Subscriber level cancellation is not allowed from request type "Change", Use "Cancel Policy" request.
						</span>
					</td>
				</tr>
			</table>
		</div>
		
		<table class="table table-condensed" ng-if="vm.isEditMode === false">
			<thead>
				<tr>
					<th>Type</th>
					<th>Name</th>
					<th>Gender</th>
					<th>SSN</th>
					<th>Benefit effective Date</th>
					<th>Member ID</th>
				</tr>
			</thead>
	
			<tbody>
				<tr ng-repeat="enrollee in vm.enrollment.enrolleeDataDtoList | orderBy:[vm.relationshipOrderBy, 'memberId']">
					<td class="txt-center">{{enrollee.relationshipToSubscriber}}</td>
					<td class="txt-center">{{enrollee.firstName}} {{enrollee.middleName}}
						{{enrollee.lastName}}</td>
					<td class="txt-center">{{enrollee.genderLookupLabel}}</td>
					<td class="txt-center">{{enrollee.taxIdentificationNumber | ssnFormat}}</td>
					<td class="txt-center">{{enrollee.enrolleeEffectiveStartDateUI}} - {{enrollee.enrolleeEffectiveEndDateUI}}</td>
					<td class="txt-center">{{enrollee.memberId}}</td>	
				</tr>
			</tbody>
		</table>
		
		
		
		
		
		<div ng-if="vm.isEditMode === true" ng-repeat="enrollee in vm.enrollmentCopy.enrolleeDataDtoList | orderBy:[vm.relationshipOrderBy, 'memberId'] " class="enrollee-editable-repeat" ng-form="enrolleeSubForm" ng-class="$index !== 0? 'enrollee-editable-repeat-not-first' : ''">
			<div class="relationship">{{enrollee.relationshipToSubscriber}} - {{enrollee.memberId}}</div> 
			<div class="enrollee-editable-div" ng-class="enrollee.relationshipToSubscriber.toUpperCase() === 'SELF'? 'self': ''">
				<div class="row-fluid" ng-if="(memberEnrlmntMembrName &&  enrollee.relationshipToSubscriber.toUpperCase() === 'SELF') || (memberEnrlmntMembrDpndntNm && enrollee.relationshipToSubscriber.toUpperCase() !== 'SELF')">
					<div class="span3">
						<label>First Name<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></label> 
						<input type="text" ng-disabled="(!memberEnrlmntMembrNmEditable && enrollee.relationshipToSubscriber.toUpperCase() === 'SELF') || (!memberEnrlmntMembrDpndntNmEditable && enrollee.relationshipToSubscriber.toUpperCase() !== 'SELF')" ng-model="enrollee.firstName" class="name-input" name="firstName" required ng-pattern="/^[A-Za-z0-9'-.\s]*$/" ng-disabled="enrollee.enrolleeEffectiveStartDateUI === enrollee.enrolleeEffectiveEndDateUI || vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI === vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI">
					</div>
					<div class="span3">
						<label>Middle Name</label>
						<input type="text" ng-disabled="(!memberEnrlmntMembrNmEditable && enrollee.relationshipToSubscriber.toUpperCase() === 'SELF') || (!memberEnrlmntMembrDpndntNmEditable && enrollee.relationshipToSubscriber.toUpperCase() !== 'SELF')" ng-model="enrollee.middleName" class="name-input" name="middleName" ng-pattern="/^[A-Za-z0-9'-.\s]*$/" ng-disabled="enrollee.enrolleeEffectiveStartDateUI === enrollee.enrolleeEffectiveEndDateUI || vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI === vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI">
					</div>
					<div class="span3">
						<label>Last Name<img  width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
						<input type="text" ng-disabled="(!memberEnrlmntMembrNmEditable && enrollee.relationshipToSubscriber.toUpperCase() === 'SELF') || (!memberEnrlmntMembrDpndntNmEditable && enrollee.relationshipToSubscriber.toUpperCase() !== 'SELF')" ng-model="enrollee.lastName" class="name-input" name="lastName" required ng-pattern="/^[A-Za-z0-9'-.\s]*$/" ng-maxlength="50" ng-disabled="enrollee.enrolleeEffectiveStartDateUI === enrollee.enrolleeEffectiveEndDateUI || vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI === vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI">
					</div>
					
					<p>
						<span class="error-message" ng-if="enrolleeSubForm.firstName.$error.pattern && enrolleeSubForm.firstName.$dirty" > 
							Please enter a valid first name.
						</span>
					</p>
					<p>
						<span class="error-message" ng-if="enrolleeSubForm.firstName.$error.required && enrolleeSubForm.firstName.$dirty" > 
							Please enter first name.
						</span>
					</p>
					<p>
						<span class="error-message" ng-if="enrolleeSubForm.middleName.$error.pattern && enrolleeSubForm.middleName.$dirty" > 
							Please enter a valid middle name.
						</span>
					</p>
					<p>
						<span class="error-message" ng-if="(enrolleeSubForm.lastName.$error.pattern || enrolleeSubForm.lastName.$error.maxlength) && enrolleeSubForm.lastName.$dirty" > 
							Please enter a valid last name.
						</span>
					</p>
					<p>
						<span class="error-message" ng-if="enrolleeSubForm.lastName.$error.required && enrolleeSubForm.lastName.$dirty" > 
							Please enter last name.
						</span>
					</p>
					
				</div>
				
				<div class="row-fluid margin20-t">
					<div class="span3" ng-if="(memberEnrlmntMembrGender &&  enrollee.relationshipToSubscriber.toUpperCase() === 'SELF') || (memberEnrlmntMembrDpndntGndr && enrollee.relationshipToSubscriber.toUpperCase() !== 'SELF')">
						<label>Gender<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></label>
						<select ng-model="enrollee.genderLookupLabel" style="width: 135px;" required ng-disabled="(!memberEnrlmntMembrGndrEditable && enrollee.relationshipToSubscriber.toUpperCase() === 'SELF')  || (!memberEnrlmntMembrDpndntGndrEditable && enrollee.relationshipToSubscriber.toUpperCase() !== 'SELF') || enrollee.enrolleeEffectiveStartDateUI === enrollee.enrolleeEffectiveEndDateUI || vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI === vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI">
							<option value="Male">Male</option>
							<option value="Female">Female</option>
							<option value="Unknown">Unknown</option>
						</select>
					</div>
					<div class="span3" ng-if="(memberEnrlmntMembrSsn && enrollee.relationshipToSubscriber.toUpperCase() === 'SELF') || (memberEnrlmntMembrDpndntSsn && enrollee.relationshipToSubscriber.toUpperCase() !== 'SELF')">
						<label>SSN</label>
						<input type="text" ng-disabled="(!memberEnrlmntMembrSsnEditable && enrollee.relationshipToSubscriber.toUpperCase() === 'SELF') || (!memberEnrlmntMembrDpndntSsnEditable && enrollee.relationshipToSubscriber.toUpperCase() !== 'SELF')" ng-model="enrollee.taxIdentificationNumber" class="enrollee-input" ui-mask="999-99-9999" ng-disabled="enrollee.enrolleeEffectiveStartDateUI === enrollee.enrolleeEffectiveEndDateUI || vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI === vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI">
					</div>
					<div class="span3" ng-show="memberEnrlmntDpndtStrtEndDt || memberEnrlmntDpndtStrtEndDtEditable">
						<label>Benefit Start Date<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></label>
						<input type="text" ng-model="enrollee.enrolleeEffectiveStartDateUI" class="enrollee-input" placeholder="MM/DD/YYYY" date-validation="date" name="enrolleeEffectiveStartDateUI" ui-mask="99/99/9999" model-view-value="true" ng-minlength="10" required ng-disabled="!memberEnrlmntDpndtStrtEndDtEditable || enrollee.relationshipToSubscriber.toUpperCase() === 'SELF'">
					</div>
					<div class="span3" ng-show="memberEnrlmntDpndtStrtEndDt || memberEnrlmntDpndtStrtEndDtEditable">
						<label>Benefit End Date<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></label>
						<input type="text" placeholder="MM/DD/YYYY" class="enrollee-input" ng-model="enrollee.enrolleeEffectiveEndDateUI" date-validation="date" name="enrolleeEffectiveEndDateUI" required ui-mask="99/99/9999" model-view-value="true" ng-minlength="10" ng-disabled="!memberEnrlmntDpndtStrtEndDtEditable  || enrollee.relationshipToSubscriber.toUpperCase() === 'SELF'">
					</div>
					
					<p>
						<span class="error-message" ng-if="enrolleeSubForm.enrolleeEffectiveStartDateUI.$error.required && enrolleeSubForm.enrolleeEffectiveStartDateUI.$dirty" > 
							Please enter benefit effective date.
						</span>
					</p>
					<p>
						<span class="error-message" ng-if="enrolleeSubForm.enrolleeEffectiveStartDateUI.$error.date  && enrolleeSubForm.enrolleeEffectiveStartDateUI.$dirty" > 
							The entered value is not a valid date. Please enter the date in MM/DD/YYYY format.
						</span>
					</p>
					<p>
						<span class="error-message" ng-if="enrolleeSubForm.enrolleeEffectiveEndDateUI.$error.required && enrolleeSubForm.enrolleeEffectiveEndDateUI.$dirty" > 
							Please enter benefit effective end date.
						</span>
					</p>
					<p>
						<span class="error-message" ng-if="enrolleeSubForm.enrolleeEffectiveEndDateUI.$error.date && enrolleeSubForm.enrolleeEffectiveEndDateUI.$dirty" > 
							The entered value is not a valid date. Please enter the date in MM/DD/YYYY format.
						</span>
					</p>
				</div>

			</div>
		</div>
		

		
		<table class="table table-striped table-condensed premium-editable-table">
				<thead>
					<tr>
						<th class="txt-center fitwidth"><spring:message code="lbl.enrollment.premiumhistory.month"/></th>
						<th class="txt-center fitwidth"><spring:message code="lbl.enrollment.premiumhistory.grossPremium"/></th>
						<th class="txt-center fitwidth" ng-if="(memberEnrlmntGroupMaxAptc || memberEnrlmntGroupMaxAptcEditable)"><spring:message code="lbl.enrollment.premiumhistory.groupMaxAPTC"/></th>
 						<th class="txt-center fitwidth" ng-if="(memberEnrlmntAptc || memberEnrlmntAptcEditable)"><spring:message code="lbl.enrollment.premiumhistory.electedAPTC"/></th>
                        <th class="txt-center" ng-if="(stateSubsidyEnabled && (memberEnrlmntGroupMaxAptc || memberEnrlmntGroupMaxAptcEditable))"><spring:message code="lbl.enrollment.premiumhistory.groupMaxStateSubsidy"/></th>
						<th class="txt-center" ng-if="(stateSubsidyEnabled && (memberEnrlmntAptc || memberEnrlmntAptcEditable))"><spring:message code="lbl.enrollment.premiumhistory.electedStateSubsidy"/></th>
						<th class="txt-center fitwidth"><spring:message code="lbl.enrollment.premiumhistory.netPremium"/></th>
						<th ng-if="memberEnrlmntAppType"><spring:message code="lbl.enrollment.premiumhistory.applicationType"/></th>
						<th class="fitwidth" ng-if="memberEnrlmntEhbprmum || memberEnrlmntEhbPrmumEditable"><spring:message code="lbl.enrollment.premiumhistory.ehbPremium"/>  </br>({{(vm.enrollment.ehbPercentage != null && vm.enrollment.ehbPercentage != 0) ? (vm.enrollment.ehbPercentage >1 ? vm.enrollment.ehbPercentage: +((vm.enrollment.ehbPercentage*100).toFixed(2))): 0}} %)</th>
					</tr>
				</thead>
				<tr ng-if="vm.enrollment.enrollmentPremiumDtoList === undefined || vm.enrollment.enrollmentPremiumDtoList.length === 0"><td colspan="7">No premium history found.</td></tr>
				
				<tr ng-repeat="premium in vm.enrollment.enrollmentPremiumDtoList | orderBy: 'month'" ng-if="vm.isEditMode === false">
					
						<td class="txt-center">{{premium.month | monthName}}</td>
						
						<td class="txt-center" ng-if="($index+1) < vm.enrollmentBenefitEffectiveStartMonth || ($index+1) > vm.enrollmentBenefitEffectiveEndMonth || vm.enrollment.enrollmentBenefitEffectiveStartDateUI == vm.enrollment.enrollmentBenefitEffectiveEndDateUI">
							<a class="bootstrap-tooltip" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='Premium amounts are not available because this enrollment does not have coverage period for this month.'>
								Not Applicable
							</a>
						</td>
						<td class="currency txt-center" ng-if="($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI" >{{premium.grossPremiumAmount | currency}}</td>
						
						<td class="currency txt-center" ng-if="(memberEnrlmntGroupMaxAptc || memberEnrlmntGroupMaxAptcEditable) && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI" colspan="1"> {{premium.maxAptc == null ? 0 : premium.maxAptc | currency}}</td>
						<td class="currency txt-center" ng-if="((memberEnrlmntGroupMaxAptc || memberEnrlmntGroupMaxAptcEditable) && !(($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI))" colspan="1"> {{ }}</td>

						<td class="currency txt-center" ng-if="(memberEnrlmntAptc || memberEnrlmntAptcEditable) && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI" ng-disabled="memberEnrlmntAptcEditable" colspan="1"> {{premium.aptcAmount | currency}}</td>
						<td class="currency txt-center" ng-if="((memberEnrlmntAptc || memberEnrlmntAptcEditable) && !(($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI))"  > {{ }}</td>

                    <td class="currency txt-center" ng-if="(stateSubsidyEnabled && (memberEnrlmntGroupMaxAptc || memberEnrlmntGroupMaxAptcEditable)) && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI" colspan="1"> {{premium.maxStateSubsidy == null ? 0 : premium.maxStateSubsidy | currency}}</td>
                    <td class="currency txt-center" ng-if="(stateSubsidyEnabled) && (memberEnrlmntGroupMaxAptc || memberEnrlmntGroupMaxAptcEditable) && !(($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI)"  > {{ }}</td>

					<td class="currency txt-center" ng-if="(stateSubsidyEnabled && (memberEnrlmntAptc || memberEnrlmntAptcEditable)) && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI" ng-disabled="memberEnrlmntAptcEditable" colspan="1"> {{premium.stateSubsidyAmount | currency}}</td>
					<td class="currency txt-center" ng-if="(stateSubsidyEnabled) && (memberEnrlmntAptc || memberEnrlmntAptcEditable) && !(($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI)"  > {{ }}</td>

						<td class="currency txt-center" ng-if="($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI" colspan="1" >{{premium.netPremiumAmount | currency}}</td>
						<td class="currency txt-center" ng-if="!(($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI)"  >{{  }}</td>

						<td class="txt-center" ng-if="($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && memberEnrlmntAppType && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI" colspan="1">{{premium.isFinancial? 'Financial' : 'Non-Financial'}}</td>
						<td class="txt-center" ng-if="($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && (memberEnrlmntEhbprmum || memberEnrlmntEhbPrmumEditable) && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI" ng-disabled="memberEnrlmntEhbPrmumEditable" colspan="1" >{{(vm.enrollment.ehbPercentage != null && vm.enrollment.ehbPercentage != 0 && vm.enrollment.ehbPercentage <= 1) ? (premium.grossPremiumAmount * vm.enrollment.ehbPercentage) : premium.grossPremiumAmount| currency}}</td>
						<td class="txt-center" ng-if="!(($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && (memberEnrlmntEhbprmum || memberEnrlmntEhbPrmumEditable) && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI)"  colspan="2" >{{  }}</td>
				</tr>
			
			<!-- #######################edit mode starts################### -->
			<!-- #######################edit mode starts################### -->
			<!-- #######################edit mode starts################### -->
			<!-- #######################edit mode starts################### -->
			<tr ng-repeat="premium in vm.enrollmentCopy.enrollmentPremiumDtoList | orderBy: 'month'" ng-if="vm.isEditMode === true" ng-form="premiumSubForm">
				<td class="month txt-center">{{premium.month | monthName}}</td>
				
				<td ng-if="vm.enrollment.enrollmentBenefitEffectiveStartDateUI == vm.enrollment.enrollmentBenefitEffectiveEndDateUI|| ($index+1) < vm.enrollmentBenefitEffectiveStartMonth || ($index+1) > vm.enrollmentBenefitEffectiveEndMonth" colspan="6">
					<a class="bootstrap-tooltip" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='Premium amounts are not available because this enrollment does not have coverage period for this month.'>
						Not Applicable
					</a>
				</td>
				<td class="currency txt-center" ng-if="($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && ($index+1) >= vm.originalEnrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.originalEnrollmentBenefitEffectiveEndMonth && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI">{{premium.grossPremiumAmount | currency}}</td>				
				<td class="currency txt-center" ng-if="vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && (($index+1) < vm.originalEnrollmentBenefitEffectiveStartMonth || ($index+1) > vm.originalEnrollmentBenefitEffectiveEndMonth)">
					<a class="bootstrap-tooltip" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='Premium amounts are automatically calculated by the system on save.'>Pending Calculation</a>
				</td>
				<td ng-if="(memberEnrlmntGroupMaxAptc || memberEnrlmntGroupMaxAptcEditable) && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth">
					<div class="input-prepend aptc-amount margin50-l txt">
						<span class="add-on">$</span>
						<input type="text" ng-model="premium.maxAptc" class="input-mini" currency-only name="maxAptc" ng-disabled="!premium.isAptcEligible || !memberEnrlmntGroupMaxAptcEditable" ng-change="vm.updateAptc(premium, premiumSubForm, vm.enrollment.ehbPercentageUI)" ng-required='premium.isAptcEligible'>
						<a class="bootstrap-tooltip_question-sign" ng-if="!premium.isFinancial" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='APTC Amounts is not applicable for Non-Financial Application. If you would like to enter APTC amount for this month, then change the application type to Financial.'>
							<i class="icon-question-sign"></i>
						</a>
					</div>
					<div ng-if="premiumSubForm.maxAptc.$dirty && premium.isFinancial">
						<span class="error-message" ng-if="premiumSubForm.maxAptc.$error.currency" > 
						Please enter a valid number.
					</span>
					<span class="error-message" ng-if="premiumSubForm.aptcAmount.$error.highAptc && !premiumSubForm.aptcAmount.$error.currency" > 
						Please pick an Max APTC amount that is greater than or equal to APTC amount.
					</span>
					</div>
					<div ng-if="premium.isAptcEligible">
						<span class="error-message" ng-if="premiumSubForm.maxAptc.$error.required" > 
							Please enter a number.
						</span>
					</div>
				</td>
				
				<td ng-if="(memberEnrlmntAptc || memberEnrlmntAptcEditable) && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth">
					<div class="input-prepend aptc-amount margin50-l">
						<span class="add-on">$</span>
						<input type="text" ng-model="premium.aptcAmount" class="input-mini" currency-only name="aptcAmount" ng-disabled="!premium.isAptcEligible || !memberEnrlmntAptcEditable" ng-change="vm.updateAptc(premium, premiumSubForm, vm.enrollment.ehbPercentageUI)" ng-required='premium.isAptcEligible'>
						<a class="bootstrap-tooltip_question-sign" ng-if="!premium.isFinancial" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='APTC Amounts is not applicable for Non-Financial Application. If you would like to enter APTC amount for this month, then change the application type to Financial.'>
							<i class="icon-question-sign"></i>
						</a>
					</div>
					<div ng-if="premiumSubForm.aptcAmount.$dirty && premium.isFinancial">
						<span class="error-message" ng-if="premiumSubForm.aptcAmount.$error.currency" > 
						Please enter a valid number.
					</span>
						<span class="error-message" ng-if="premiumSubForm.aptcAmount.$error.required" > 
						Please enter a number.
					</span>
					<span class="error-message" ng-if="premiumSubForm.aptcAmount.$error.highAptc && !premiumSubForm.aptcAmount.$error.currency" > 
						Please pick APTC amount that is less than or equal to Max APTC amount.
					</span>
					<span class="error-message" ng-if="premiumSubForm.aptcAmount.$error.ehbPremium && !premiumSubForm.aptcAmount.$error.currency" > 
						Please pick an APTC amount that is less than or equal to the gross premium amount.
					</span>
					</div>
				</td>

                <td ng-if="(stateSubsidyEnabled && (memberEnrlmntGroupMaxAptc || memberEnrlmntGroupMaxAptcEditable)) && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth">
                    <div class="input-prepend aptc-amount margin50-l">
                        <span class="add-on">$</span>
                        <input type="text" ng-model="premium.maxStateSubsidy" class="input-mini" currency-only name="maxStateSubsidy" ng-disabled="!premium.isStateSubsidyEligible || !memberEnrlmntAptcEditable" ng-change="vm.updateStateSubsidy(premium, premiumSubForm, vm.enrollment.ehbPercentageUI)" ng-required='premium.isStateSubsidyEligible'>
                        <a class="bootstrap-tooltip_question-sign" ng-if="!premium.isFinancial" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='State Subsidy Amounts is not applicable for Non-Financial Application. If you would like to enter State Subsidy amount for this month, then change the application type to Financial.'>
                            <i class="icon-question-sign"></i>
                        </a>
                    </div>
                    <div ng-if="premiumSubForm.maxStateSubsidy.$dirty && premium.isFinancial">
                        <span class="error-message" ng-if="premiumSubForm.maxStateSubsidy.$error.currency" >
                            Please enter a valid number.
                        </span>
                        <span class="error-message" ng-if="premiumSubForm.stateSubsidyAmount.$error.highStateSubsidy && !premiumSubForm.stateSubsidyAmount.$error.currency" >
                            Please pick a Max CAPS amount that is greater than or equal to CAPS amount.
                        </span>
                    </div>
                    <div ng-if="premium.isStateSubsidyEligible">
						<span class="error-message" ng-if="premiumSubForm.maxStateSubsidy.$error.required" >
							Please enter a number.
						</span>
                    </div>
                </td>

                <td ng-if="(stateSubsidyEnabled && (memberEnrlmntAptc || memberEnrlmntAptcEditable)) && vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth">
                    <div class="input-prepend aptc-amount margin50-l">
                        <span class="add-on">$</span>
                        <input type="text" ng-model="premium.stateSubsidyAmount" class="input-mini" currency-only name="stateSubsidyAmount" ng-disabled="!premium.isStateSubsidyEligible || !memberEnrlmntAptcEditable" ng-change="vm.updateStateSubsidy(premium, premiumSubForm, vm.enrollment.ehbPercentageUI)" ng-required='premium.isStateSubsidyEligible'>
                        <a class="bootstrap-tooltip_question-sign" ng-if="!premium.isFinancial" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='State Subsidy Amounts is not applicable for Non-Financial Application. If you would like to enter State Subsidy amount for this month, then change the application type to Financial.'>
                            <i class="icon-question-sign"></i>
                        </a>
                    </div>
                    <div ng-if="premiumSubForm.stateSubsidyAmount.$dirty && premium.isFinancial">
						<span class="error-message" ng-if="premiumSubForm.stateSubsidyAmount.$error.currency" >
						    Please enter a valid number.
                        </span>
                        <span class="error-message" ng-if="premiumSubForm.stateSubsidyAmount.$error.required" >
                            Please enter a number.
                        </span>
                        <span class="error-message" ng-if="premiumSubForm.stateSubsidyAmount.$error.highStateSubsidy && !premiumSubForm.stateSubsidyAmount.$error.currency" >
						    Please pick a State Subsidy amount that is less than or equal to Max State Subsidy amount.
					    </span>
                        <span class="error-message" ng-if="premiumSubForm.stateSubsidyAmount.$error.ehbPremium && !premiumSubForm.stateSubsidyAmount.$error.currency" >
						    Please pick a CAPS amount that is less than or equal to the gross premium amount.
					    </span>
                    </div>
                </td>

				<td class="currency txt-center" ng-if="vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && ($index+1) >= vm.originalEnrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.originalEnrollmentBenefitEffectiveEndMonth">{{premium.netPremiumAmount | currency}}</td>
				<td class="currency txt-center" ng-if="vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && (($index+1) < vm.originalEnrollmentBenefitEffectiveStartMonth || ($index+1) > vm.originalEnrollmentBenefitEffectiveEndMonth)">
					<a class="bootstrap-tooltip" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='Premium amounts are automatically calculated by the system on save.'>Pending Calculation</a>
				</td>
				
				<td class="txt-center" ng-if="vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && memberEnrlmntAppType">
					<select ng-model="premium.isFinancial" class="application-type" ng-change="vm.updateApplicationType(premium)" ng-options="applicationType.value as applicationType.label for applicationType in [{ 'label': 'Non-Financial', 'value': false }, { 'label': 'Financial', 'value': true }]"></select>
				</td>
				<td class="currency txt-center" ng-if="vm.enrollment.enrollmentBenefitEffectiveStartDateUI != vm.enrollment.enrollmentBenefitEffectiveEndDateUI && ($index+1) >= vm.enrollmentBenefitEffectiveStartMonth && ($index+1) <= vm.enrollmentBenefitEffectiveEndMonth && (memberEnrlmntEhbprmum || memberEnrlmntEhbPrmumEditable)">
					{{(vm.enrollment.ehbPercentage != null && vm.enrollment.ehbPercentage != 0 && vm.enrollment.ehbPercentage <= 1) ? (premium.grossPremiumAmount * vm.enrollment.ehbPercentage) : premium.grossPremiumAmount| currency}}
				</td>
			</tr>
			<!-- #######################edit mode ends################### -->
			<!-- #######################edit mode ends################### -->
			<!-- #######################edit mode ends################### -->
			<!-- #######################edit mode ends################### -->
		</table>
		
		<div ng-if="vm.isEditMode === true" >
			<p class="alert alert-info">
				<strong>Note:</strong> 
				<spring:message code="lbl.enrollment.premiumhistory.note"/>
			</p>						
		</div>
		
		<div id="premiumOverrideResultModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="premiumOverrideResultModalHeader" aria-hidden="true">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="premiumOverrideResultModalHeader">
					<span ng-if="vm.isUpdatedSuccess === false">Error Occured</span>
					<span ng-if="vm.isUpdatedSuccess === true">Success</span>						
				</h3>
			</div>
			<div class="modal-body">
				{{vm.premiumOverrideResultMessage}}
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>

		</div>
		
		<div class="margin40-t">
			<div class="txt-right">
				<div class="enrollmentEditToolActionDiv" ng-show="vm.isEditMode === false && (memberPrmHistActions && (memberPrmHistCancel || memberPrmHistChange))">
					<!-- Gear Menu Starts -->
					<div class="editToolActionBorderDiv">
						<button class="editToolActionBtnDiv" ng-click="enrollmentEditToolAction($event)">
							<i class="icon-gear editToolActionGear"></i> <span>Actions</span>
						</button>
					</div>
					<!-- Gear Menu Ends -->

					<!-- Menu Options Starts -->
					<div class="editToolButtonDiv" style="display: none" ng-show="vm.isEditMode === false">
						<div class="center editToolBtns">
							<button ng-show="memberPrmHistCancel" ng-click="vm.showCancelEnrollmentModal()" ng-if="vm.enrollment.enrollmentStatus !== 'CANCEL'" class="btn btn-primary"><spring:message code="lbl.enrollment.premiumhistory.cancel"/></button>
							<button ng-show="memberPrmHistChange" ng-click="vm.goToAddOrChangeControlEnrollmentPage()" ng-if="vm.enrollment.enrollmentStatus === 'CANCEL'" class="btn btn-primary">Add</button>
							<button ng-show="memberPrmHistChange" ng-click="vm.editPremium()" class="btn btn-primary">Change</button>
						</div>
					</div>
					<!-- Menu Options Ends -->
				</div>
			</div>
			<div ng-if="vm.isEditMode === true" class="txt-right">
				<button ng-click="vm.validateAndSaveEnrollmentData()" class="btn btn-primary" ng-disabled="(vm.fromPage !== 'addOrChangeEnrollmentTxn' && (premiumHistoryForm.$pristine || premiumHistoryForm.$invalid)) || (vm.fromPage === 'addOrChangeEnrollmentTxn' && premiumHistoryForm.$invalid)">Continue</button>
				<button ng-click="vm.cancelPremiumUpdate()" class="btn">Cancel</button>
			</div>
		</div>

	</div>
</form>

<div id="premiumOverrideReasonModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="premiumOverrideReasonModalHeader" aria-hidden="true">
	<form name="premiumOverrideReasonModal">		
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="vm.emptyPremiumOverrideReason()">x</button>
			<h3 id="premiumOverrideReasonModalHeader">Premium history override</h3>
		</div>
		<div class="modal-body">
			<p class="oRReason">
				Specify the reason for override <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
			</p>
			<textarea class="overrideTextarea margin10-t" ng-model="vm.premiumHistoryComment" placeholder="Please explain the change you are making and why" maxlength="4000" required></textarea>
			<span>{{4000 - vm.premiumHistoryComment.length}} left</span>
		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" data-dismiss="modal" ng-click="vm.savePremiumUpdate()" ng-disabled="premiumOverrideReasonModal.$invalid">Save</button>
			<button class="btn" data-dismiss="modal" aria-hidden="true" ng-click="vm.emptyPremiumOverrideReason()">Close</button>
		</div>
	</form>
</div>
<!-- CANCEL Form Starts here -->
<div id="cancelEnrollmentModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cancelEnrollmentModalHeader" aria-hidden="true">
	<form name="cancelEnrollmentModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="vm.emptyCancelEnrollmentReason()">x</button>
			<h3 id="cancelEnrollmentModalHeader"><spring:message code="lbl.enrollment.premiumhistory.cancel"/></h3>
		</div>
		<div class="modal-body">
			<p class="oRReason">
				Specify the reason for cancel <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />
			</p>
			<select name="vm.cancelEnrollmentReason" ng-model = "vm.cancelEnrollmentReason" class="input-medium" ng-init = 'vm.cancelEnrollmentReason= "AI"'  >
					<option value="AI">No Reason Given</option>
					<option value="59">Non Payment</option>
					<option value="14">Voluntary Withdrawal</option>
			</select>
			
			<textarea class="overrideTextarea margin10-t" ng-model="vm.cancelEnrollmentComment" placeholder="Please explain the change you are making and why" maxlength="4000" required></textarea>
			<span>{{4000 - vm.cancelEnrollmentComment.length}} left</span>
		</div>
		<div class="modal-footer">
			<p ng-if='vm.cancelEnrollmentReason != "59"'>&nbsp;</p>
			<p ng-if='vm.cancelEnrollmentReason == "59"'>
				<strong>Note:</strong> 
				"Send in 834" is disabled when Non-Payment is the reason for termination.
			</p>
			<label class="checkbox inline"> <input type="checkbox" ng-model="vm.isSendIn834" ng-checked='vm.cancelEnrollmentReason != "59"' ng-disabled='vm.cancelEnrollmentReason == "59"' ng-true-value="true" ng-false-value="false" ng-init='vm.isSendIn834=true'> 
				<span>Send In 834</span>
			</label>
			<button class="btn btn-primary" data-dismiss="modal" ng-click="vm.cancelActiveEnrollment()" ng-disabled="cancelEnrollmentModal.$invalid">Submit</button>
			<button class="btn" data-dismiss="modal" aria-hidden="true" ng-click="vm.emptyCancelEnrollmentReason()">Cancel</button>
		</div>
	</form>
</div>
<!-- CANCEL Form ends here -->
