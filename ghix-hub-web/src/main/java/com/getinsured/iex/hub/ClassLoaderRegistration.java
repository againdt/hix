package com.getinsured.iex.hub;

import javax.annotation.PostConstruct;

import com.getinsured.iex.hub.platform.HubServiceBridge;

public class ClassLoaderRegistration {
	
	@PostConstruct
	public void init(){
		HubServiceBridge.registerClassLoader(ClassLoaderRegistration.class.getClassLoader());
	}

}
