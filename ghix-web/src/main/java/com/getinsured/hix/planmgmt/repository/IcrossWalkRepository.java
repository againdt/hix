package com.getinsured.hix.planmgmt.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.CrossWalk;

public interface IcrossWalkRepository extends JpaRepository<CrossWalk, Integer> {

    @Query("SELECT c FROM CrossWalk c WHERE c.isDeleted='N' and c.hisoIssuerId =:hisoIssuerId and c.dentalOnly =:dentalOnly and c.applicableYear =:applicableYear")
	List<CrossWalk> getAllCrossWalkPlansforIssuer(@Param("hisoIssuerId") String hisoIssuerId, @Param("dentalOnly") String dentalOnly, @Param("applicableYear") Integer applicableYear);
}
