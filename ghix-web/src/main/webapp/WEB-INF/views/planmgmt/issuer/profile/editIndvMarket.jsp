<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<div class="row-fluid">
	<ul class="page-breadcrumb">
        <li><a href="<c:url value="/planmgmt/issuer/account/profile/market/individual/info"/>">&lt; <spring:message  code="label.back"/></a></li>
        <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
        <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
        <li><spring:message  code="pgheader.update.inidividualMarketProfile"/></li>
    </ul><!--page-breadcrumb ends-->
	<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
</div>

		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                
                    <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 			<jsp:param name="pageName" value="individualProfile"/>
 	 	 	 	    </jsp:include>
                
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.update.inidividualMarketProfile"/></h4>
                    <a class="btn btn-primary btn-small pull-right margin5-lr" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                    <a class="btn btn-small pull-right" href="<c:url value="/planmgmt/issuer/account/profile/market/individual/info"/>"><spring:message  code="label.cancel"/></a> 
                </div>
				<div class="gutter10">
		
				<form class="form-horizontal" id="frmIssuerAcctProfMarktIndvInfo" name="frmIssuerAcctProfMarktIndvInfo" action="<c:url value="/planmgmt/issuer/profile/update/market/save" />" method="POST">
                    <df:csrfToken/>
                    <input type="hidden" id="market" name="market" value="individual"/>
					<input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/>
                    <div class="control-group">
						<label class="control-label" for="customerServicePhone"><spring:message  code="label.customerServicePhone"/></label>
						<div class="controls">
						  	<input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="input-mini"/> 
							<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" /> 
							<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-mini" /> 
                            <input type="hidden" name="customerServicePhone" id="customerServicePhone">
                            <label class="help-inline" for="customerServiceExt"><spring:message  code="label.phone.ext"/></label>
                            <input type="text" name="customerServiceExt" id="customerServiceExt" maxlength="11" class="input-mini" value="${issuerObj.indvCustServicePhoneExt}"/>
	                        <div id="phone3_error" class=""></div>
                            <div id="customerServiceExt_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="custServiceTollFreeNumber"><spring:message  code="label.customerServiceTollFreeNumber"/></label>
						<div class="controls">
							<input type="text" name="phone4" id="phone4" value="${phone4}" maxlength="3" class="input-mini"/> 
							<input type="text" name="phone5" id="phone5" value="${phone5}" maxlength="3" class="input-mini" /> 
							<input type="text" name="phone6" id="phone6" value="${phone6}" maxlength="4" class="input-mini" /> 
                            <input type="hidden" name="custServiceTollFreeNumber" id="custServiceTollFreeNumber">
                            <div id="phone6_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="custServiceTTY"><spring:message  code="label.customerServiceTTY"/></label>
						<div class="controls">
							<input type="text" name="phone7" id="phone7" value="${phone7}" maxlength="3" class="input-mini"/> 
                            <input type="hidden" name="custServiceTTY" id="custServiceTTY">
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="facingWebSite"><spring:message  code="label.consumerFacingWebSiteURL"/></label>
						<div class="controls">
							<input type="text" name="facingWebSite" id="facingWebSite" class="input-large" value="${issuerObj.indvSiteUrl}" maxlength="200"/>
                            <div id="facingWebSite_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                                        
                    <div class="form-actions paddingLR20">
                        
                    </div>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
<script type="text/javascript">

function submitForm(){
	 $("#frmIssuerAcctProfMarktIndvInfo").submit(); 
}
var digitRegex = new RegExp("^[0-9]+$");
jQuery.validator.addMethod("custServicePhoneUS", function(value, element, param) {
	if($.browser.msie==true && $.browser.version=='8.0'){
		phoneNumber1 = $("#phone1").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone2").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone3").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone1").val().trim();
		phoneNumber2 = $("#phone2").val().trim();
		phoneNumber3 = $("#phone3").val().trim();
	}
	var phoneNumber = phoneNumber1 + phoneNumber2 + phoneNumber3;	
	if(phoneNumber != "" && !digitRegex.test(phoneNumber)){
		return false;
	}
	
	if(phoneNumber != ""){
		if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length == 10){
			$("#customerServicePhone").val(phoneNumber);
			return true;
		}else{
			return false;
		}	
	}else{
		
		return true;
	}
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

jQuery.validator.addMethod("custServiceTollFreePhone", function(value, element, param) {
	if($.browser.msie==true && $.browser.version=='8.0'){
		phoneNumber1 = $("#phone4").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone5").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone6").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone4").val().trim();
		phoneNumber2 = $("#phone5").val().trim();
		phoneNumber3 = $("#phone6").val().trim();
	}
	var phoneNumber = phoneNumber1 + phoneNumber2 + phoneNumber3;
	if(phoneNumber != "" && !digitRegex.test(phoneNumber)){
		return false;
	}
	if(phoneNumber != ""){
		if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length == 10){
			$("#custServiceTollFreeNumber").val(phoneNumber);
			return true;
		}else{
			return false;
		}	
	}else{
		return true;
	}
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

jQuery.validator.addMethod("custServiceTTYPhone", function(value, element, param) {
	if($.browser.msie == true && $.browser.version == '8.0'){
		phoneNumber1 = $("#phone7").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone7").val().trim();
	}
	var phoneNumber = phoneNumber1;
	if(phoneNumber != ""){
		$("#custServiceTTY").val(phoneNumber);
	}
	return true;
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

jQuery.validator.addMethod("checkDepend", function(value, element, param) {
	var phoneNumber = $("#customerServicePhone").val();
	if(phoneNumber == "" && value != ""){
		return false;
	}
	return true;
});

jQuery.validator.addMethod("checkURL", function(value, element, param) {
	var urlregex = new RegExp("/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/");
	if(value == null || value == ''|| urlregex.test(value)){
		return true;
	}else{
		return false;
	}
});
	 	
var validator = $("#frmIssuerAcctProfMarktIndvInfo").validate({
	rules : {
		phone3 : {custServicePhoneUS : true },
		customerServiceExt : {required :false, number : true, checkDepend:true},
		phone6 : {custServiceTollFreePhone : true},
		phone7 : {custServiceTTYPhone : true},
		facingWebSite : {required : false, checkForValidUrl : true}		
	},
	messages : {
		phone3 : { custServicePhone : "<span><em class='excl'>!</em><spring:message  code='err.customerServicePhone' javaScriptEscape='true'/></span>" },
		customerServiceExt: {  required : "<span><em class='excl'>!</em><spring:message  code='err.customerServiceExt' javaScriptEscape='true'/></span>",
			number : "<span><em class='excl'>!</em><spring:message code='err.customerServiceExt' javaScriptEscape='true'/></span>",
			checkDepend: "<span><em class='excl'>!</em><spring:message code='err.customerServicePhoneDepends' javaScriptEscape='true'/></span>"},
		phone6 : { custServiceTollFreePhone : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>" },
		phone7 : { custServiceTTYPhone : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>" },
		facingWebSite : { required : "<span><em class='excl'>!</em><spring:message  code='err.facingWebSite' javaScriptEscape='true'/></span>",
			checkForValidUrl : "<span><em class='excl'>!</em><spring:message  code='err.validfacingWebSite' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) { 
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});

jQuery.validator.addMethod("checkForValidUrl", function (value, element, param) {		
    var urlRegExp1 = new RegExp("(((https?:[/][/])((www)))[.]([-]|[a-z]|[A-Z]|[0-9]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)");
    var urlRegExp2 = new RegExp("^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$");
    if(value == '') {
		return true;
	}else{
		if((value.indexOf('.') == -1)){
			 return false;
		}
	    
	    if(value.match("^(http:\/\/www\.|https:\/\/www\.)")){
	    	if(urlRegExp1.test(value)){
	    		return (true);
		    }else{
		    	return (false);
		    }
	    }else{
	    	if(urlRegExp2.test(value)){
	    		return (true);
	    	}else{
	    		return (false);
	    	}
	    }
    }   	    
});
</script>