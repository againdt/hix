/*
 * Custom script for Plan display request
 */

$(function(){
	
	//Plan display memebr
	PlanDisplayMemberModel = Backbone.Model.extend({

		initialize : function() {
		},
	
		defaults: {
			dob: "",
			firstName: "",
			lastName : "",
			relationship : ""
      	}
		
	});
	
	//Plan Display Members Collection
	PlanDisplayMemberCollection = Backbone.Collection.extend({
		model : PlanDisplayMemberModel
	});
	
	//PlanDisplayRequest Model
	PlanDisplayRequest = Backbone.Model.extend({
		
		initialize : function() {
		},
	
		defaults: {
			aptc: "",
			csr: "",
			coverageStart: "",
			zip: "",
			isSubsidized: "",
			member : new PlanDisplayMemberCollection()
		}
	});
	
	//Creating members Collection
	planDisplayMembers = new PlanDisplayMemberCollection();
	
});