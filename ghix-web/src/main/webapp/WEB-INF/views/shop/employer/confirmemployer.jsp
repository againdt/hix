<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%
String recoveryStatus = (String)request.getAttribute("status");
String showSuccessStyle =  "newrecovery".equals(recoveryStatus)?"display:''":"display:none";
String showFailureStyle =  "failure".equals(recoveryStatus)?"display:''":"display:none";
String showAlreadyUpdatedStyle =  "alreadyupdated".equals(recoveryStatus)?"display:''":"display:none";
%> 
<div class="row">
	<div class="span4"  style=<%=showSuccessStyle%>>
		<h2><spring:message code='label.emplregcomplete'/></h2>
	</div>
	<div class="span12" style=<%=showSuccessStyle%>>
		<form class="form-stacked" action="">
			<df:csrfToken/>
				<spring:message code='label.emplrecoverysuccess'/>
				
				<div class="actions">
					
					<a class="btn large primary" type="submit" href="../../../account/user/login">Employer Login</a>
				</div>
		</form>
	</div>
	<div class="span4"  style=<%=showFailureStyle%>>
		<h2><spring:message code='label.emplrecoveryfail'/></h2>
	</div>
	<div class="span12" style=<%=showFailureStyle %>>
		<div>
			<div class="span">
		      <p><spring:message code='label.emplrecoveryfailure'/> 
		      </p>

		    </div>
	    </div>
	</div>
	<div class="span4"  style=<%=showAlreadyUpdatedStyle%>>
		<h2><spring:message code='label.emplregcomplete'/></h2>
	</div>
	<div class="span12" style=<%=showAlreadyUpdatedStyle%>>
		<form class="form-stacked" action="">
			<df:csrfToken/>
			
				<spring:message code='label.emplrecoveryalreadydone'/>
				
				<div class="actions">
					
					<a class="btn large primary" type="submit" href="../../../account/user/login">Employer Login</a>
				</div>
		</form>
	</div>
</div>