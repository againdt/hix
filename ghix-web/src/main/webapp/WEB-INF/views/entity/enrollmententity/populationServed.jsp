<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="../../resources/js/html5-placeholder-shim.js" />"></script>



<script type="text/javascript">
	$(document).ready(function() {

		
		$("#populationServed").removeClass("link");
		$("#populationServed").addClass("active");
		
		var langTotal = 0;
		$('.langPercentage').each(function(){		    	  
			if(this.value != "" && !isNaN(this.value)) {
				langTotal += parseInt(this.value, 10);
	    	} else if(this.placeholder != "" && !isNaN(this.placeholder)) {
	    		langTotal += parseInt(this.placeholder, 10);
    	  	}
      	});
		$('#langPercentageTotal').val(langTotal + "%");
		$('#hiddenLangPercentageTotal').val(langTotal);
		
		var ethnicityTotal= 0;
		$('.ethnicityPercentage').each(function(){		    	  
			if(this.value != "" && !isNaN(this.value)) {
				ethnicityTotal += parseInt(this.value, 10);
	    	} else if(this.placeholder != "" && !isNaN(this.placeholder)) {
	    		ethnicityTotal += parseInt(this.placeholder, 10);
    	  	}
      	});
		$('#ethnicityPercentageTotal').val(ethnicityTotal + "%");
		$('#hiddenEthnicityPercentageTotal').val(ethnicityTotal);
		
		var indTotal = 0;
		$('.industryPercentage').each(function(){		    	  
			if(this.value != "" && !isNaN(this.value)) {
				indTotal += parseInt(this.value, 10);
	    	} else if(this.placeholder != "" && !isNaN(this.placeholder)) {
	    		indTotal += parseInt(this.placeholder, 10);
    	  	}
      	});

		$('.langStaff,.industryPercentage, .ethnicityPercentage, .langPercentage').keyup(function () {
			var checkId = this.id;
		    if(this.value != this.value.replace(/[^0-9]/g, '')) {
		    	$("#"+checkId+"_error").show();
		       	this.value = this.value.replace(/[^0-9]/g, '');
		       	$("#"+checkId+"_error").hide();
		    }
		});
		
		$('#industryPercentageTotal').val(indTotal + "%");
		$('#hiddenIndustryPercentageTotal').val(indTotal);
	});
	

	

</script>
<style>
	input[type="checkbox"] {
		margin-top:3px;
	}
</style>

<div class="gutter10">
<%-- <div class="row-fluid">
	<div class="page-header">
		<h1><spring:message code="label.entity.step"/> 2: <spring:message code="label.entity.populationsServed"/></h1>
	</div>
</div> --%>


  <div class="row-fluid margin20-t">
    <!-- #sidebar -->      
       <jsp:include page="../leftNavigationMenu.jsp" />
    <!-- #sidebar ENDS -->
        
    <!-- #section -->
        <div class="span9" id="rightpanel">
          <div id="section">
			<div class="header">
            	<h4><spring:message code="label.entity.step"/> 2: <spring:message code="label.entity.populationsServed"/></h4>
            </div>
            <form class="form-horizontal" name="frmPopulationServed" id="frmPopulationServed" action="/hix/entity/enrollmententity/populationServed" method="POST">   
            <df:csrfToken/>
            <input type="hidden" id="userId" name="userId" value="<encryptor:enc value="${enrollmentEntity.user.id}"/>" />
             <input type="hidden" id="entityId" name="entityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>" /> 
<!-- Languages Starts -->         
              <h5 data-toggle="collapse" data-target="#languages" class="accordion-box-custom trigger collapsetoggle"><i class="icon-chevron-right"></i>&nbsp; <spring:message code="label.entity.languages"/></h5>
              <div class="gutter10-lr border-custom-rbl margin-b-4">
			  <div id="hiddenLangPercentageTotal_error" class="help-inline"></div>
			  <input type="hidden" name="validateLangData" id="validateLangData" value="" />
	          <div id="validateLangData_error" class="help-inline"></div>
			  <input type="hidden" name="verifyOtherLang" id="verifyOtherLang" value="" />
	          <div id="verifyOtherLang_error" class="help-inline"></div>
	          
			  <c:set var="populationServedWrapper" value="${populationServedWrapper}" />
              
              <p tabindex="0"><spring:message code="label.entity.checkAllLanguagesOfYourTargetPopulationAndSpecifyPercentagesAccordingly"/></p>
              <div id="languages" class="collapse out">
                
                <table class="table">
                  <thead>
                    <tr>                     
 	                  <th style="min-width: 200px;"><spring:message code="label.entity.language"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
                      <th><spring:message code="label.entity.percentOfInLanguageAssistance"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
                      <th><spring:message code="label.entity.numberOfStaffWhoSpeakTheLanguageFluently"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
                     </tr>
                  </thead>
                  <tbody>
                  <c:forEach items="${languagesLookupList}" var="lookupLanguageList">
	                  <input type="hidden" name="enrollmentEntityId" id="enrollmentEntityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>" />
						<tr class="populationServerRow">
	                      <td>
	                        <label class="checkbox" for="${lookupLanguageList.lookupValueCode}">
	                          <input type="checkbox" class="selectLang" style="margin-top:0;" value="${lookupLanguageList.lookupValueCode}" id="${lookupLanguageList.lookupValueCode}" name="LangChk"  ${fn:contains(populationServedWrapper.populationLanguages[lookupLanguageList.lookupValueLabel].language,lookupLanguageList.lookupValueLabel) ? 'checked="checked"' : '' } >
	                          <span class="aria-hidden"><spring:message code="label.entity.language"/></span> ${lookupLanguageList.lookupValueLabel}   
	                        </label>
	                      </td>
	                      <td>
	                      	<label for="${lookupLanguageList.lookupValueCode}LangPercentage" class="aria-hidden"><spring:message code="label.entity.percentOfInLanguageAssistance"/></label>
	                      	<input type="text" name="LangPercentage[${lookupLanguageList.lookupValueCode}]" class="input-mini langPercentage" id="${lookupLanguageList.lookupValueCode}LangPercentage" placeholder="${populationServedWrapper.populationLanguages[lookupLanguageList.lookupValueLabel].languagePercentage}" value="${populationServedWrapper.populationLanguages[lookupLanguageList.lookupValueLabel].languagePercentage}">
						  	<div id="${lookupLanguageList.lookupValueCode}LangPercentage_error" class="help-inline"></div>
						  </td>
	                      <td>
	                      	<label for="${lookupLanguageList.lookupValueCode}LangStaff" class="aria-hidden"><spring:message code="label.entity.numberOfStaffWhoSpeakTheLanguageFluently"/></label>
	                      	<input type="text" name="${lookupLanguageList.lookupValueCode}LangStaff" class="input-mini langStaff" id="${lookupLanguageList.lookupValueCode}LangStaff" placeholder="${populationServedWrapper.populationLanguages[lookupLanguageList.lookupValueLabel].noOfStaffSpeakLanguage}" value="${populationServedWrapper.populationLanguages[lookupLanguageList.lookupValueLabel].noOfStaffSpeakLanguage}">
	                      	<div id="${lookupLanguageList.lookupValueCode}LangStaff_error" class="help-inline"></div>
	                      </td>
	                    </tr>
	                  <input type="hidden" name="languagesData" id="languagesData" value="" />
                  </c:forEach>
                    <tr class="populationServerRow">
                      <td>
                      	<label class="checkbox" for="other1LangChk">
                        	<input type="checkbox" data-rel="otherCheckbox" name="LangChk" id="other1LangChk" value="other1" style="margin-top:0;" ${not empty langList[0].language ? 'checked="checked"' : ''}/>
                       		<span class="aria-hidden"><spring:message code="label.entity.language"/></span> <spring:message code="label.entity.otherPleaseSpecify"/>
                       	</label>
                       	<label for="other1Lang"><span class="aria-hidden"><spring:message code="label.entity.language"/></span> 
                        	<input type="text" class="input-medium otherTextbox" name="other1Lang" id="other1Lang" value="${langList[0].language}">  
                        </label> 
                        <div id="other1Lang_error" class="help-inline"></div>                
                      </td>
                      <td>
                      	<label for="other1LangPercentage"><span class="aria-hidden"><spring:message code="label.entity.percentOfInLanguageAssistance"/></span>
                      		<input type="text" class="input-mini langPercentage" id="other1LangPercentage" name="other1LangPercentage" placeholder="" value="${langList[0].languagePercentage}">
                      	</label>
                      	<div id="other1LangPercentage_error" class="help-inline"></div>
                      </td>
                      <td>
                      	<label for="other1LangStaff"><span class="aria-hidden"><spring:message code="label.entity.numberOfStaffWhoSpeakTheLanguageFluently"/></span>
                      		<input type="text" class="input-mini langStaff" id="other1LangStaff" name="other1LangStaff" placeholder="" value="${langList[0].noOfStaffSpeakLanguage}">
                      	</label>
					  	<div id="other1LangStaff_error" class="help-inline"></div>
					  </td>
                    </tr>
                    
                    <tr class="populationServerRow">
                      <td>
                      	<label class="checkbox" for="other2LangChk">
                          <input type="checkbox"  data-rel="otherCheckbox" name="LangChk" id="other2LangChk" value="other2" style="margin-top:0;" ${not empty langList[1].language ? 'checked="checked"' : ''}/>
                         <span class="aria-hidden"><spring:message code="label.entity.language"/></span> <spring:message code="label.entity.otherPleaseSpecify"/>
                        </label>
                        <label for="other2Lang"> <span class="aria-hidden"><spring:message code="label.entity.language"/></span> 
                          <input type="text" class="input-medium otherTextbox" name="other2Lang" id="other2Lang" value="${langList[1].language}">
                        </label>
                        <div id="other2Lang_error" class="help-inline"></div>
                      </td>
                      <td>
                      	<label for="other2LangPercentage"><span class="aria-hidden"><spring:message code="label.entity.percentOfInLanguageAssistance"/></span>
                      		<input type="text" class="input-mini langPercentage" id="other2LangPercentage" name="other2LangPercentage" placeholder="" value="${langList[1].languagePercentage}">
                      	</label>
					  	<div id="other2LangPercentage_error" class="help-inline"></div>
					  </td>
                      <td>
                      	<label for="other2LangStaff"><span class="aria-hidden"><spring:message code="label.entity.numberOfStaffWhoSpeakTheLanguageFluently"/></span>
                      		<input type="text" class="input-mini langStaff" id="other2LangStaff" name="other2LangStaff" placeholder="" value="${langList[1].noOfStaffSpeakLanguage}">
                      	</label>
                      	<div id="other2LangStaff_error" class="help-inline"></div>
                      </td>
                    </tr>
                    
                    <tr class="populationServerRow">
                      <td>
                      	<label class="checkbox" for="other3LangChk">
                          <input type="checkbox" data-rel="otherCheckbox" name="LangChk" id="other3LangChk" value="other3" style="margin-top:0;" ${not empty langList[2].language ? 'checked="checked"' : ''}>
                          <span class="aria-hidden"><spring:message code="label.entity.language"/></span> <spring:message code="label.entity.otherPleaseSpecify"/>
                        </label>
                        <label for="other3Lang"><span class="aria-hidden"><spring:message code="label.entity.language"/></span> 
                          <input type="text" class="input-medium otherTextbox" name="other3Lang" id="other3Lang" value="${langList[2].language}">  
                         </label> 
                         <div id="other3Lang_error" class="help-inline"></div>        
                      </td>
                      <td>
                      	<label for="other3LangPercentage"><span class="aria-hidden"><spring:message code="label.entity.percentOfInLanguageAssistance"/></span>
                      		<input type="text" class="input-mini langPercentage" id="other3LangPercentage" name="other3LangPercentage" placeholder="" value="${langList[2].languagePercentage}">
                      	</label>
                      	<div id="other3LangPercentage_error" class="help-inline"></div>
                      </td>
                      <td>
                      	<label for="other3LangStaff"><span class="aria-hidden"><spring:message code="label.entity.numberOfStaffWhoSpeakTheLanguageFluently"/></span>
                      		<input type="text" class="input-mini langStaff" id="other3LangStaff" name="other3LangStaff" placeholder="" value="${langList[2].noOfStaffSpeakLanguage}">
                      	</label>
                      	<div id="other3LangStaff_error" class="help-inline"></div>
                      </td>
                    </tr>
                    
                    <tr class="populationServerRow">
                      <td>
                        <label class="checkbox" for="other4LangChk">
                          <input type="checkbox" data-rel="otherCheckbox" name="LangChk" id="other4LangChk" value="other4" style="margin-top:0;" ${not empty langList[3].language ? 'checked="checked"' : ''}>
                          <span class="aria-hidden"><spring:message code="label.entity.language"/></span> <spring:message code="label.entity.otherPleaseSpecify"/>
                        </label>
                        <label for="other4Lang"><span class="aria-hidden"><spring:message code="label.entity.language"/></span> 
                          <input type="text" class="input-medium otherTextbox" name="other4Lang" id="other4Lang" value="${langList[3].language}">
                         </label>
                         <div id="other4Lang_error" class="help-inline"></div>
                      </td>
                      <td>
                      	<label for="other4LangPercentage"><span class="aria-hidden"><spring:message code="label.entity.percentOfInLanguageAssistance"/></span>
                      		<input type="text" class="input-mini langPercentage" id="other4LangPercentage" name="other4LangPercentage" placeholder="" value="${langList[3].languagePercentage}">
                      	</label>
                      	<div id="other4LangPercentage_error" class="help-inline"></div>
                      </td>
                      <td>
                      	<label for="other4LangStaff"><span class="aria-hidden"><spring:message code="label.entity.numberOfStaffWhoSpeakTheLanguageFluently"/></span>
                      		<input type="text" class="input-mini langStaff" id="other4LangStaff" name="other4LangStaff" placeholder="" value="${langList[3].noOfStaffSpeakLanguage}">
                      	</label>
                      	<div id="other4LangStaff_error" class="help-inline"></div>
                      </td>
                    </tr>
                    
                    <tr>
                      <td><strong><spring:message code="label.entity.total"/></strong></td>
                      <!-- IMPORTANT: total must be 100% -->
                      <td>
                      	<label for="langPercentageTotal" class="aria-hidden"><spring:message code="label.entity.langPerTotal"/></label>
                      		<input type="text" readonly="readonly" class="input-mini langPercentageTotal" name="langPercentageTotal" id="langPercentageTotal" placeholder="100%" value="">
                      	<input type="hidden" name="hiddenLangPercentageTotal" id="hiddenLangPercentageTotal" value="">       
                      </td>
                      <td>&nbsp;</td>
                    </tr>
                  </tbody>
                </table>  
              </div><!-- #languages .collapse out --> 
              </div>  
<!-- Languages Ends --> 

                                                    
<!-- Ethnicity Starts -->                                      
              <h5 data-toggle="collapse" data-target="#ethnicity" class="accordion-box-custom trigger collapsetoggle"><i class="icon-chevron-right"></i>&nbsp; <spring:message code="label.entity.ethnicities"/> </h5>
              <div class="gutter10-lr border-custom-rbl margin-b-4">
			  <div id="hiddenEthnicityPercentageTotal_error" class="help-inline"></div>
			  <input type="hidden" name="verifyOtherEthnicity" id="verifyOtherEthnicity" value="" />
	          <div id="verifyOtherEthnicity_error" class="help-inline"></div>
			  
			  <p tabindex="0"><spring:message code="label.entity.checkAllThatApplyAndSpecifyPerAcc"/></p>
              <div id="ethnicity" class="collapse out">
                
                <table class="table">
                  <thead>
                    <tr>
                      <th><spring:message code="label.entity.ethnicity"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
                      <th><spring:message code="label.entity.estimatedPlannedToServe"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
                     </tr>
                  </thead>
                  <tbody>
	                  <c:forEach items="${ethnicitiesLookupList}" var="ethnicitiesLookupList">
							<tr class="populationServerRow">
		                      <td>
		                        <label class="checkbox" for="${ethnicitiesLookupList.lookupValueCode}">
		                        	<span class="aria-hidden"><spring:message code="label.entity.ethnicity"/></span>
		                          <input type="checkbox" class="selectEthnicity" style="margin-top:0;" value="${ethnicitiesLookupList.lookupValueCode}" id="${ethnicitiesLookupList.lookupValueCode}" name="EthnicityChk"  ${fn:contains(populationServedWrapper.populationEthnicities[ethnicitiesLookupList.lookupValueLabel].ethnicity,ethnicitiesLookupList.lookupValueLabel) ? 'checked="checked"' : '' } >
		                          <%-- <span class="aria-hidden"><spring:message code="label.entity.ethnicity"/></span> --%>${ethnicitiesLookupList.lookupValueLabel}   
		                        </label>
		                      </td>
		                      <td>
		                      	<label for="${ethnicitiesLookupList.lookupValueCode}EthnicityPercentage" class="aria-hidden"><spring:message code="label.entity.estimatedPlannedToServe"/></label>
		                      		<input type="text" name="${ethnicitiesLookupList.lookupValueCode}EthnicityPercentage" class="input-mini ethnicityPercentage" id="${ethnicitiesLookupList.lookupValueCode}EthnicityPercentage" placeholder="${populationServedWrapper.populationEthnicities[ethnicitiesLookupList.lookupValueLabel].ethnicityPercentage}" value="${populationServedWrapper.populationEthnicities[ethnicitiesLookupList.lookupValueLabel].ethnicityPercentage}">
		                      	
		                      	<div id="${ethnicitiesLookupList.lookupValueCode}EthnicityPercentage_error" class="help-inline"></div>
		                      </td>
		                    </tr>
		                  <input type="hidden" name="ethnicitiesData" id="ethnicitiesData" value="" />
	                  </c:forEach>
                     
	                 <tr class="populationServerRow">
                      <td>
                          <label class="checkbox" for="other1EthnicityChk">
                          	<input type="checkbox" data-rel="otherCheckbox" name="EthnicityChk" id="other1EthnicityChk" value="other1" style="margin-top:0;" ${not empty ethnicityList[0].ethnicity ? 'checked="checked"' : ''}>
                          	<span class="aria-hidden"><spring:message code="label.entity.ethnicity"/></span> <spring:message code="label.entity.OtherPleaseTellUs"/>
                          </label>
                          <label for="other1Ethnicity" class="hide">Other Ethnicity 1</label>
                          	<input type="text" class="input-medium otherTextbox" name="other1Ethnicity" id="other1Ethnicity" value="${ethnicityList[0].ethnicity}">
                          
                         <div id="other1Ethnicity_error" class="help-inline"></div>
                      </td>
                      <td>
                      	<label for="other1EthnicityPercentage" class="hide">Other Ethnicity 1 Percentage</label>
                     		<input type="text"  class="input-mini ethnicityPercentage" id="other1EthnicityPercentage" name="other1EthnicityPercentage" placeholder="" value="${ethnicityList[0].ethnicityPercentage}">
                      	
                      	<div id="other1EthnicityPercentage_error" class="help-inline"></div>
                      </td>
                     </tr>
                     
	                 <tr class="populationServerRow">
                      <td>
                          <label class="checkbox" for="other2EthnicityChk">
                          	<input type="checkbox" data-rel="otherCheckbox" name="EthnicityChk" id="other2EthnicityChk" value="other2" style="margin-top:0;" ${not empty ethnicityList[1].ethnicity ? 'checked="checked"' : ''}>
                          	<span class="aria-hidden"><spring:message code="label.entity.ethnicity"/></span> <spring:message code="label.entity.OtherPleaseTellUs"/>
                          </label>
                          <label for="other2Ethnicity" class="hide">Other Ethnicity 2</label>
                          	<input type="text" class="input-medium otherTextbox" name="other2Ethnicity" id="other2Ethnicity" value="${ethnicityList[1].ethnicity}">
                           
                         <div id="other2Ethnicity_error" class="help-inline"></div>
                      </td>
                      <td>
                      	<label for="other2EthnicityPercentage" class="hide">Other Ethnicity 2 Percentage</label>
                      		<input type="text" class="input-mini ethnicityPercentage" id="other2EthnicityPercentage" name="other2EthnicityPercentage" placeholder="" value="${ethnicityList[1].ethnicityPercentage}">
                      	
                      	<div id="other2EthnicityPercentage_error" class="help-inline"></div>
                      </td>
                     </tr>
	                 
	                 <tr>
	                   <td><strong><spring:message code="label.entity.total"/></strong></td>
	                   <!-- IMPORTANT: total must be 100% -->
	                   <td>
	                   	<label for="ethnicityPercentageTotal" class="hide"><spring:message code="label.entity.ethPerTotal"/></label>
	                   		<input type="text" readonly="readonly" class="input-mini ethnicityPercentageTotal" name="ethnicityPercentageTotal" id="ethnicityPercentageTotal" placeholder="100%" value="">
	                   	
	                   	<input type="hidden" name="hiddenEthnicityPercentageTotal" id="hiddenEthnicityPercentageTotal" value="">
	                   </td>
	                 </tr>
                  </tbody>
                </table> 
                </div>
              </div><!-- #ethnicity .collapse out -->     
<!-- Ethnicity Ends -->                      
                    
                    
<!-- Industries Starts -->                                      
              <h5 data-toggle="collapse" data-target="#industries" class="accordion-box-custom trigger collapsetoggle"><i class="icon-chevron-right"></i>&nbsp; <spring:message code="label.entity.industries"/></h5>
              <div class="gutter10-lr border-custom-rbl margin-b-4">
              <div id="hiddenIndustryPercentageTotal_error" class="help-inline"></div>
              <input type="hidden" name="verifyOtherIndustry" id="verifyOtherIndustry" value="" />
	          <div id="verifyOtherIndustry_error" class="help-inline"></div>

			  <p tabindex="0"><spring:message code="label.entity.checkAllIndustriesThtApplyAndProvidePercPerInd"/></p>
              <div id="industries" class="collapse out">
                
                <table class="table">
                  <thead>
                    <tr>
                      <th><spring:message code="label.entity.industries"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
                      <th><spring:message code="label.entity.estimatedPlanneToServe"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></th>
                     </tr>
                  </thead>
                  <tbody>
		                <c:forEach items="${industriesLookupList}" var="industriesLookupList">
							<tr class="populationServerRow">
		                     <td>
		                       <label class="checkbox" for="${industriesLookupList.lookupValueCode}">
		                         <input type="checkbox" class="selectIndustry" style="margin-top:0;" value="${industriesLookupList.lookupValueCode}" id="${industriesLookupList.lookupValueCode}" name="IndustryChk"  ${fn:contains(populationServedWrapper.populationIndustries[industriesLookupList.lookupValueLabel].industry,industriesLookupList.lookupValueLabel) ? 'checked="checked"' : '' } >
		                         <span class="aria-hidden"><spring:message code="label.entity.industries"/></span> ${industriesLookupList.lookupValueLabel}   
		                       </label>
		                     </td>
		                     <td>
		                     	<label for="${industriesLookupList.lookupValueCode}IndustryPercentage" class="aria-hidden"><spring:message code="label.entity.estimatedPlanneToServe"/></label>
		                     		<input type="text" name="${industriesLookupList.lookupValueCode}IndustryPercentage" class="input-mini industryPercentage" id="${industriesLookupList.lookupValueCode}IndustryPercentage" placeholder="${populationServedWrapper.populationIndustries[industriesLookupList.lookupValueLabel].industryPercentage}" value="${populationServedWrapper.populationIndustries[industriesLookupList.lookupValueLabel].industryPercentage}">
		                     	
		                     	<div id="${industriesLookupList.lookupValueCode}IndustryPercentage_error" class="help-inline"></div>
		                     </td>
		                   </tr>
		                <input type="hidden" name="industriesData" id="industriesData" value="" />
		                </c:forEach>
		                
		                <tr class="populationServerRow">
	                      <td>
	                      	<label class="checkbox" for="other1IndustryChk">
	                          <input type="checkbox" data-rel="otherCheckbox" name="IndustryChk" id="other1IndustryChk" value="other1" style="margin-top:0;" ${not empty industryList[0].industry ? 'checked="checked"' : ''}>
	                          <span class="aria-hidden"><spring:message code="label.entity.industries"/></span> <spring:message code="label.entity.otherPleaseSpecify"/>
	                        </label>
	                        <label for="other1Industry" class="hide">Other Industry 1</label>
	                          <input type="text" class="input-medium otherTextbox" name="other1Industry" id="other1Industry" value="${industryList[0].industry}">
	                        
	                        <div id="other1Industry_error" class="help-inline"></div>
	                      </td>
	                      <td>
	                      	<label  for="other1IndustryPercentage" class="hide">Other Industry Percentage 1</label>
	                      		<input type="text" class="input-mini industryPercentage" id="other1IndustryPercentage" name="other1IndustryPercentage" placeholder="" value="${industryList[0].industryPercentage}">
	                      	
	                      	<div id="other1IndustryPercentage_error" class="help-inline"></div>
	                      </td>
	                     </tr>
	                     
	                    <tr class="populationServerRow">
	                      <td>
	                      	<label class="checkbox" for="other2IndustryChk">
	                          <input type="checkbox" data-rel="otherCheckbox" name="IndustryChk" id="other2IndustryChk" value="other2" style="margin-top:0;" ${not empty industryList[1].industry ? 'checked="checked"' : ''}>
	                          <span class="aria-hidden"><spring:message code="label.entity.industries"/></span> <spring:message code="label.entity.otherPleaseSpecify"/>
	                        </label>
	                        <label for="other2Industry" class="hide">Other Industry 2</label>
	                          <input type="text" class="input-medium otherTextbox" name="other2Industry" id="other2Industry" value="${industryList[1].industry}">
	                         
	                         <div id="other2Industry_error" class="help-inline"></div>
	                      </td>
	                      <td>
	                      	<label for="other2IndustryPercentage" class="hide">Other Industry 2 Percentage</label>
	                      		<input type="text"  class="input-mini industryPercentage" id="other2IndustryPercentage" name="other2IndustryPercentage" placeholder="" value="${industryList[1].industryPercentage}">
	                      	
	                      	<div id="other2IndustryPercentage_error" class="help-inline"></div>
	                      </td>
	                    </tr>
                    
			            <tr>
			              <td><strong><spring:message code="label.entity.total"/></strong></td>
			              <!-- IMPORTANT: total must be 100% -->
			              <td>
			              	<label for="industryPercentageTotal" class="hide"><spring:message code="label.entity.indPerTotal"/></label>
			              		<input readonly="readonly" type="text" class="input-mini industryPercentageTotal" name="industryPercentageTotal" id="industryPercentageTotal" placeholder="100%" value="">
			              	
			              	<input type="hidden" name="hiddenIndustryPercentageTotal" id="hiddenIndustryPercentageTotal" value="">
			              </td>
			            </tr>			            
                  </tbody>
                </table>   
                </div> 
              </div><!-- #industries .collapse out -->              
<!-- Industries Ends -->

            <div class="form-actions clear">
              <c:choose>
              	<c:when test="${loggedUser =='entityAdmin'}">
              		<c:set var="useridForUrl" ><encryptor:enc value="${enrollmentEntity.user.id}" isurl="true"/> </c:set>
                   <input type="button" name="cancel" id="cancel" value="<spring:message code="label.entity.cancel"/>" class="btn cancel-btn" onClick="window.location.href='/hix/entity/entityadmin/viewPopulationServed/${useridForUrl}'"/>
                   <input type="submit" name="populationServedSubmit" id="populationServedSubmit" class="btn btn-primary" value="<spring:message code="label.entity.save"/>" />
                </c:when>
                <c:when test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete'}">
                   <input type="button" name="cancel" id="cancel" value="<spring:message code="label.entity.cancel"/>" class="btn cancel-btn" onClick="window.location.href='/hix/entity/enrollmententity/viewPopulationServed'"/>
                   <input type="submit" name="populationServedSubmit" id="populationServedSubmit" class="btn btn-primary" value="<spring:message code="label.entity.save"/>" />
                </c:when>
                <c:otherwise>
                 	<c:set var="enrollmentEntityidForUrl" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true"/> </c:set>
                   <input type="button" name="back" id="back" value="<spring:message code="label.entity.back"/>" class="btn cancel-btn" onClick="window.location='/hix/entity/enrollmententity/entityinformation?isLeftNav=true&entityId=${enrollmentEntityidForUrl}'"/>
                   <input type="submit" name="populationServedSubmit" id="populationServedSubmit" class="btn btn-primary" value="<spring:message code="label.entity.next"/>" />
                </c:otherwise>	  	
              </c:choose>
            </div><!-- .well .well-small .clear -->
              </form><!-- .form-horizontal -->
          </div><!-- #section --> 
        </div><!-- .span9 -->
    <!-- #section ENDS -->
        
      </div><!-- .row-fluid -->
</div>

<script type="text/javascript">

function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
          String.prototype.trim = function() {
           	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
          }
	}
}

$('.ethnicityPercentage, .langPercentage, .industryPercentage, .langStaff, .otherTextbox').attr('readonly',true);
/* $.each($("input[name='EthnicityChk']"), function(){
	//console.log('dfghj')
	$(this).after('<span class="aria-hidden"><spring:message code="label.entity.ethnicity"/> '+$(this).val()+'</span>');
}); */

$.each($('input[name="EthnicityChk"], input[name="LangChk"], input[name="IndustryChk"], input[data-rel="otherCheckbox"]'), function() {
	if($(this).is(":checked")){
		$(this).parents('.populationServerRow').find('input[type="text"]').removeAttr('readonly');
	}
});

$('input[name="EthnicityChk"], input[name="LangChk"], input[name="IndustryChk"], input[data-rel="otherCheckbox"]').each(function(){
	$(this).change(function() {
	    if($(this).is(':checked')){
	    	$(this).parents('.populationServerRow').find('input[type="text"]').removeAttr('readonly').removeAttr('aria-disabled').removeAttr('aria-label');
	    }else{
	    	
 	    	//HIX-32015 (To refresh total when user unchecked the checkBox.)
	    	var langTotal = 0;
	    	$.each($("input[name='LangChk']:checked"), function() {
	    		langTotal = percentageTotal(langTotal, $(this).attr('value'), "LangPercentage");
	    	});
	    	
	    	var ethnicityTotal = 0;
	    	$.each($("input[name='EthnicityChk']:checked"), function() {
	        	ethnicityTotal = percentageTotal(ethnicityTotal, $(this).attr('value'), "EthnicityPercentage");
	        });
	        
	        var industryTotal = 0;
	    	$.each($("input[name='IndustryChk']:checked"), function() {
	        	industryTotal = percentageTotal(industryTotal, $(this).attr('value'), "IndustryPercentage");
	      });
	    	
	      // Update the Langtotal 
	      $('#langPercentageTotal').val(langTotal + "%");
	      $('#hiddenLangPercentageTotal').val(langTotal);
	      
	      // Update the Ethnicitytotal 
	      $('#ethnicityPercentageTotal').val(ethnicityTotal + "%");
	      $('#hiddenEthnicityPercentageTotal').val(ethnicityTotal);
	      
	      // Update the Industrytotal 
	      $('#industryPercentageTotal').val(industryTotal + "%");
	      $('#hiddenIndustryPercentageTotal').val(industryTotal); 
	      
	      
	      $(this).parents('.populationServerRow').find('input[type="text"]').val('').attr('readonly',true);
	    }
	});
});

var percentageTotal = function(total, attValue, percentageType){
	
	var per = attValue + percentageType;
	var percentageVal = $("#"+per).val();
	if(percentageVal == "") {
		$("#"+per).val($("#"+per).attr('placeholder'));
	}					
	if($("#"+per).val() != "") {
		if($("#"+per).val() != "" && !isNaN($("#"+per).val())) {
			total += parseInt($("#"+per).val(), 10);
    	}
	}
	return total;
};

$('.ethnicityPercentage').focusout(function() {
    // Loop through all input's and re-calculate the total
  var total = 0;	
 	
     
    $.each($("input[name='EthnicityChk']:checked"), function() {
		var per = $(this).attr('value') + 'EthnicityPercentage';
		//console.log(per)
		var percentageVal = $("#"+per).val();
		//console.log(percentageVal  + 'sd' + $("#"+per).attr('placeholder'))
		if(percentageVal == "") {
			$("#"+per).val($("#"+per).attr('placeholder'));
			//console.log($("#"+per).attr('placeholder'))
		}				
		if($("#"+per).val() != "") {
			//console.log('here')
			if($("#"+per).val() != "" && !isNaN($("#"+per).val())) {
 				total += parseInt($("#"+per).val(), 10);
	    	}
		}
    });
    // Update the total 
    $('#ethnicityPercentageTotal').val(total + "%");
    $('#hiddenEthnicityPercentageTotal').val(total);
});

$('.langPercentage').focusout(function() {
	var total = 0;
	$.each($("input[name='LangChk']:checked"), function() {
		var per = $(this).attr('value') + 'LangPercentage';				
		var percentageVal = $("#"+per).val();
		if(percentageVal == "") {
			$("#"+per).val($("#"+per).attr('placeholder'));
		}
		if($("#"+per).val() != "") {
			if($("#"+per).val() != "" && !isNaN($("#"+per).val())) {
   				total += parseInt($("#"+per).val(), 10);
	    	}
		}
	});
  // Update the total 
  $('#langPercentageTotal').val(total + "%");
  $('#hiddenLangPercentageTotal').val(total);
});



$('.industryPercentage').focusout(function() {
      // Loop through all input's and re-calculate the total
      var total = 0;
      $.each($("input[name='IndustryChk']:checked"), function() {
			var per = $(this).attr('value') + 'IndustryPercentage';
			var percentageVal = $("#"+per).val();
			if(percentageVal == "") {
				$("#"+per).val($("#"+per).attr('placeholder'));
			}					
			if($("#"+per).val() != "") {
				if($("#"+per).val() != "" && !isNaN($("#"+per).val())) {
	   				total += parseInt($("#"+per).val(), 10);
		    	}
			}
      });
      // Update the total 
      $('#industryPercentageTotal').val(total + "%");
      $('#hiddenIndustryPercentageTotal').val(total);
});

$('.collapsetoggle').click( function() {
		$(this).find('i').toggleClass(
				'icon-chevron-right icon-chevron-down');
});


	$("#populationServedSubmit").click(function(){
		var languages = '';
		var ethnicities = '';
		var industries = '';
		var langTotal = 0;
		var ethnicityTotal = 0;
		var industryTotal = 0;
		
		$.each($('input[name="LangChk"]:checked'), function() {
			var per = $(this).attr('value') + 'LangPercentage';
			var staff = $(this).attr('value') + 'LangStaff';
			
			var percentageVal = $("#"+per).val();
			var staffVal = $("#"+staff).val();
			if(percentageVal == "") {
				$("#"+per).val($("#"+per).attr('placeholder'));
			}
			if(staffVal == "") {
				$("#"+staff).val($("#"+staff).attr('placeholder'));
			}
			if($("#"+per).val() != "" && $("#"+staff).val() != "") {
				if($(this).attr('value').match(/[Oo]ther/)) {						
					var otherLangTxtName = $(this).attr('value') + 'Lang';
					languages += (languages ? '~' : '') + $("#"+otherLangTxtName).val();
				} else {
					languages += (languages ? '~' : '') + $(this).attr('value');
				}
				languages += ',' + $("#"+per).val();
				languages += ',' + $("#"+staff).val();
			}
			if($("#"+per).val() != "") {
				if($("#"+per).val() != "" && !isNaN($("#"+per).val())) {
	   				langTotal += parseInt($("#"+per).val(), 10);
		    	}
			}
		});
		$("#languagesData").val(languages);
	    // Update the total 
	    $('#langPercentageTotal').val(langTotal + "%");
	    $('#hiddenLangPercentageTotal').val(langTotal);			
		
		$.each($('input[name="EthnicityChk"]:checked'), function() {
			var per = $(this).attr('value') + 'EthnicityPercentage';
			
			var percentageVal = $("#"+per).val();
			
			if(percentageVal == "") {
				$("#"+per).val($("#"+per).attr('placeholder'));
			}
			
			if($("#"+per).val() != "") {			
				if($(this).attr('value').match(/[Oo]ther/)) {
					var otherEthnicityTxtName = $(this).attr('value') + 'Ethnicity';
					ethnicities += (ethnicities ? '~' : '') + $("#"+otherEthnicityTxtName).val();
				} else {
					ethnicities += (ethnicities ? '~' : '') + $(this).attr('value');
				}
				ethnicities += ',' + $("#"+per).val();
			}
			if($("#"+per).val() != "") {
				if($("#"+per).val() != "" && !isNaN($("#"+per).val())) {
					ethnicityTotal += parseInt($("#"+per).val(), 10);
		    	}
			}
		});

		$("#ethnicitiesData").val(ethnicities);
		// Update the total 
	    $('#ethnicityPercentageTotal').val(ethnicityTotal + "%");
	    $('#hiddenEthnicityPercentageTotal').val(ethnicityTotal);
		
		$.each($('input[name="IndustryChk"]:checked'), function() {
			var per = $(this).attr('value') + 'IndustryPercentage';
			
			var percentageVal = $("#"+per).val();
			
			if(percentageVal == "") {
				$("#"+per).val($("#"+per).attr('placeholder'));
			}
			
			if($("#"+per).val() != "") {
				if($(this).attr('value').match(/[Oo]ther/)) {
					var otherIndustryTxtName = $(this).attr('value') + 'Industry';
					industries += (industries ? '~' : '') + $("#"+otherIndustryTxtName).val();
				} else {
					industries += (industries ? '~' : '') + $(this).attr('value');
				}
				industries += ',' + $("#"+per).val();
			}
			if($("#"+per).val() != "") {
				if($("#"+per).val() != "" && !isNaN($("#"+per).val())) {
					industryTotal += parseInt($("#"+per).val(), 10);
		    	}
			}
		});
		$("#industriesData").val(industries);
		// Update the total 
	    $('#industryPercentageTotal').val(industryTotal + "%");
	    $('#hiddenIndustryPercentageTotal').val(industryTotal);
		
		$('input[id*="LangPercentage"], input[id*="LangStaff"], input[id*="EthnicityPercentage"], input[id*="IndustryPercentage"]').each(function() {
		    $(this).rules("add", {
		        number: true,
		        integer: true,
		        messages: {
		            number: "<span> <em class='excl'>!</em><spring:message code='err.validateNumber' javaScriptEscape='true'/></span>",
		            integer: "<span> <em class='excl'>!</em><spring:message code='err.validateNumber' javaScriptEscape='true'/></span>"
		        }
		    });
		});
		
		
		
		$.validator.addMethod('integer', function(value, element, param) {
			if (value.match(/[.]/)) {
				return false;
			} else if (value.match(/[-]/)) {
				return false;
			} else {
				return true;
			}
        });
		
	});
	
	
	jQuery.validator.addMethod("nonZeroIndustry", function(value, element ) {
		var checkedCheckboxes = $('input:checked[name=IndustryChk]');//IndustryPercentage   other2IndustryPercentage   transIndustryPercentage
		var validationSuccessful= true;
		for(var i = 0; i<checkedCheckboxes.length; i++){
			var prefixName = $(checkedCheckboxes[i]).attr('value');
			if(prefixName){
				var percentVal = $("#"+prefixName+"IndustryPercentage").val()
				 if( parseInt(percentVal) <= 0 ){
					 validationSuccessful = false;
				 }
			}else{
				validationSuccessful = false;
			}
		}
		return validationSuccessful;
	});
	
	jQuery.validator.addMethod("nonZeroEthnicity", function(value, element ) {
		var checkedCheckboxes = $('input:checked[name=EthnicityChk]');//IndustryPercentage   other2IndustryPercentage   transIndustryPercentage
		var validationSuccessful= true;
		for(var i = 0; i<checkedCheckboxes.length; i++){
			var prefixName = $(checkedCheckboxes[i]).attr('value');
			if(prefixName){
				var percentVal = $("#"+prefixName+"EthnicityPercentage").val()
				 if( parseInt(percentVal) <= 0 ){
					 validationSuccessful = false;
				 }
			}else{
				validationSuccessful = false;
			}
		}
		return validationSuccessful;
	});
	
	jQuery.validator.addMethod("nonZeroLang", function(value, element ) {
		var checkedCheckboxes = $('input:checked[name=LangChk]');//IndustryPercentage   other2IndustryPercentage   transIndustryPercentage
		var validationSuccessful= true;
		for(var i = 0; i<checkedCheckboxes.length; i++){
			var prefixName = $(checkedCheckboxes[i]).attr('value');
			if(prefixName){
				var percentVal = $("#"+prefixName+"LangPercentage").val()
				 if( parseInt(percentVal) <= 0 ){
					 validationSuccessful = false;
				 }
			}else{
				validationSuccessful = false;
			}
		}
		return validationSuccessful;
	});
	
	
	

	var validator = $("#frmPopulationServed").validate({ 
		onkeyup: true,
		onclick: true,
		ignore : "hidden",
		rules : { 'hiddenLangPercentageTotal' : { percentageTotalChk : true, checkLang : true , nonZeroLang : true },
			'hiddenEthnicityPercentageTotal' : { percentageTotalChk : true,echeckEmptyEthnicity : true,checkEthnicity : true , nonZeroEthnicity : true },
			'hiddenIndustryPercentageTotal' : { percentageTotalChk : true,echeckEmptyIndustry : true,checkIndustry : true , nonZeroIndustry : true},
			'validateLangData' : { validateLangData : true},
			'verifyOtherLang' : {verifyOtherLang : true},
			'verifyOtherEthnicity' : {verifyOtherEthnicity : true},
			'verifyOtherIndustry' : {verifyOtherIndustry : true},
			other1Lang : {validationForName : true, checkDuplicateName:true},
			other2Lang : {validationForName : true, checkDuplicateName:true},
			other3Lang : {validationForName : true, checkDuplicateName:true},
			other4Lang : {validationForName : true, checkDuplicateName:true}, 
			other1Ethnicity : {validationForName : true, checkDuplicateEthnicityName:true},
			other2Ethnicity : {validationForName : true, checkDuplicateEthnicityName:true},
			other1Industry : {validationForName : true, checkDuplicateIndustryName:true},
			other2Industry : {validationForName : true, checkDuplicateIndustryName:true}
		},
		messages : {
			'hiddenLangPercentageTotal' : { percentageTotalChk : "<span> <em class='excl'>!</em><spring:message code='err.totalLangAssistance' javaScriptEscape='true'/></span>",
				checkLang : "<span> <em class='excl'>!</em><spring:message code='err.selectonelanguage' javaScriptEscape='true'/> </span>" ,
				nonZeroLang : "<span> <em class='excl'>!</em>  <spring:message code='err.enterNonZero' javaScriptEscape='true'/> </span>" },
			'hiddenEthnicityPercentageTotal' : { percentageTotalChk : "<span> <em class='excl'>!</em><spring:message code='err.totalEthnicitiesServed' javaScriptEscape='true'/></span>",
				echeckEmptyEthnicity: "<span> <em class='excl'>!</em>    <spring:message code='err.enterestimatedpercent' javaScriptEscape='true'/> </span>",
				checkEthnicity : "<span> <em class='excl'>!</em> Please select at least one Ethnicity  </span>" , 
				nonZeroEthnicity: "<span> <em class='excl'>!</em>  <spring:message code='err.enterNonZero' javaScriptEscape='true'/> </span>"  },
			'hiddenIndustryPercentageTotal' : { percentageTotalChk : "<span> <em class='excl'>!</em><spring:message code='err.totalIndustriesServed' javaScriptEscape='true'/></span>",
				echeckEmptyIndustry : "<span> <em class='excl'>!</em>    <spring:message code='err.enterestimatedpercent' javaScriptEscape='true'/>  </span>",
				checkIndustry:"<span> <em class='excl'>!</em> Please select at least one industry</span>",
				nonZeroIndustry : "<span> <em class='excl'>!</em> <spring:message code='err.enterNonZero' javaScriptEscape='true'/></span>" },
			'validateLangData' : { validateLangData : "<span> <em class='excl'>!</em><spring:message code='err.langPercentAndStaffRequired' javaScriptEscape='true'/></span>"},
			'verifyOtherLang' : { verifyOtherLang : "<span> <em class='excl'>!</em><spring:message code='err.otherLang' javaScriptEscape='true'/></span>"},
			'verifyOtherEthnicity' : { verifyOtherEthnicity : "<span> <em class='excl'>!</em><spring:message code='err.otherEthnicity' javaScriptEscape='true'/></span>"},
			'verifyOtherIndustry' : { verifyOtherIndustry : "<span> <em class='excl'>!</em><spring:message code='err.otherIndustry' javaScriptEscape='true'/></span>"},
			other1Lang : { validationForName : "<span> <em class='excl'>!</em><spring:message code='err.validLanguageNameOther' javaScriptEscape='true'/></span>",
				checkDuplicateName:"<span> <em class='excl'>!</em>Duplicate Entry</span>"},
			other2Lang : {validationForName :"<span> <em class='excl'>!</em><spring:message code='err.validLanguageNameOther' javaScriptEscape='true'/></span>",
				checkDuplicateName:"<span> <em class='excl'>!</em>Duplicate Entry</span>"},
			other3Lang : {validationForName :"<span> <em class='excl'>!</em><spring:message code='err.validLanguageNameOther' javaScriptEscape='true'/></span>",
				checkDuplicateName:"<span> <em class='excl'>!</em>Duplicate Entry</span>"},
			other4Lang : {validationForName :"<span> <em class='excl'>!</em><spring:message code='err.validLanguageNameOther' javaScriptEscape='true'/></span>",
				checkDuplicateName:"<span> <em class='excl'>!</em>Duplicate Entry</span>"},
			other1Ethnicity : {validationForName :"<span> <em class='excl'>!</em><spring:message code='err.validEthinicityOther' javaScriptEscape='true'/></span>",
				checkDuplicateEthnicityName:"<span> <em class='excl'>!</em>Duplicate Entry</span>"},
			other2Ethnicity : {validationForName :"<span> <em class='excl'>!</em><spring:message code='err.validEthinicityOther' javaScriptEscape='true'/></span>",
				checkDuplicateEthnicityName:"<span> <em class='excl'>!</em>Duplicate Entry</span>"},
			other1Industry : {validationForName :"<span> <em class='excl'>!</em><spring:message code='err.validIndustriesNameOther' javaScriptEscape='true'/></span>",
				checkDuplicateIndustryName:"<span> <em class='excl'>!</em>Duplicate Entry</span>"},
			other2Industry : {validationForName :"<span> <em class='excl'>!</em><spring:message code='err.validIndustriesNameOther' javaScriptEscape='true'/></span>",
				checkDuplicateIndustryName:"<span> <em class='excl'>!</em>Duplicate Entry</span>"}
			
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		} 
	});
	

	jQuery.validator.addMethod("percentageTotalChk", function(value, element, param) {
		var per=parseInt(value);
		if((per<100)||(per>100)){
			return false; 
		}
		return true;
	});
	
	jQuery.validator.addMethod("validateLangData", function(value, element, param) {
		var flag = true;
		$.each($("input[name='LangChk']:checked"), function() {
			var per = $(this).attr('value') + 'LangPercentage';
			var staff = $(this).attr('value') + 'LangStaff';
			
			var percentageVal = $("#"+per).val();
			var staffVal = $("#"+staff).val();
			
			if(percentageVal == "") {
				$("#"+per).val($("#"+per).attr('placeholder'));
			}
			if(staffVal == "") {
				$("#"+staff).val($("#"+staff).attr('placeholder'));
			}
			
			if($("#"+per).val() == "" || $("#"+staff).val() == "") {
				flag = false;
			}
		});
		if(!flag) {
			return false;
		}
		return true;
	});
	
	$.validator.addMethod('verifyOtherLang', function(value, element, param) {
		var otherTextFlag = true;
		$.each($("input[name='LangChk']:checked"), function() {
			var per = $(this).attr('value') + 'LangPercentage';
			var staff = $(this).attr('value') + 'LangStaff';
			
			var percentageVal = $("#"+per).val();
			var staffVal = $("#"+staff).val();
			
			if(percentageVal == "") {
				$("#"+per).val($("#"+per).attr('placeholder'));
			}
			if(staffVal == "") {
				$("#"+staff).val($("#"+staff).attr('placeholder'));
			}
			if($("#"+per).val() != "" || $("#"+staff).val() != "") {
				if($(this).attr('value').match(/[Oo]ther/)) {
					var otherLangTxtName = $(this).attr('value') + 'Lang';
					var otherLangVal = $("#"+otherLangTxtName).val();
					if(otherLangVal == '' || otherLangVal == null) {
						otherTextFlag = false;
					}
				}
			}
		});
		return otherTextFlag;
	});
	
	$.validator.addMethod('verifyOtherEthnicity', function(value, element, param) {
		var otherTextFlag = true;
		$.each($("input[name='EthnicityChk']:checked"), function() {
			var per = $(this).attr('value') + 'EthnicityPercentage';
			
			var percentageVal = $("#"+per).val();
			
			if(percentageVal == "") {
				$("#"+per).val($("#"+per).attr('placeholder'));
			}
			
			if($("#"+per).val() != "") {			
				if($(this).attr('value').match(/[Oo]ther/)) {
					var otherEthnicityTxtName = $(this).attr('value') + 'Ethnicity';
					var otherEthnicityVal = $("#"+otherEthnicityTxtName).val();
					if(otherEthnicityVal == '' || otherEthnicityVal == null) {
						otherTextFlag = false;
					}
				}
			}
		});
		return otherTextFlag;
	});
		
	$.validator.addMethod('verifyOtherIndustry', function(value, element, param) {
		var otherTextFlag = true;
		$.each($("input[name='IndustryChk']:checked"), function() {
			var per = $(this).attr('value') + 'IndustryPercentage';
			
			var percentageVal = $("#"+per).val();
			
			if(percentageVal == "") {
				$("#"+per).val($("#"+per).attr('placeholder'));
			}
			
			if($("#"+per).val() != "") {
				if($(this).attr('value').match(/[Oo]ther/)) {
					var otherIndustryTxtName = $(this).attr('value') + 'Industry';
					var otherIndustryVal = $("#"+otherIndustryTxtName).val();
					if(otherIndustryVal == '' || otherIndustryVal == null) {
						otherTextFlag = false;
					}
				}
			}
		});			
		return otherTextFlag;
	});

	
	jQuery.validator.addMethod("validationForName", function(value, element) {
		return this.optional(element) || /^[a-z\s]+$/i.test(value);
	});
	
	jQuery.validator.addMethod("checkDuplicateName", function(value, element) {
	    ie8Trim();
	    var v=true;
		$('#languages table td').find('input[type=checkbox]').each(function(e){
			 if(value.trim().toLowerCase()==$(this).parent().text().replace("Language","").trim().toLowerCase()){
				// alert($(this).parent().text().replace("Language","").trim().toLowerCase()+"/"+value.toLowerCase()); 
				v=false;
			}
		});	
		for(var k=0;k<4;k++){
			var ref="other"+ (k+1) +"Lang";
			//alert(ref+"/"+$(element).attr('id'));
			if(($(element).attr('id')==ref)||($(element).val()=="")){
				continue;
			}
			if($("#"+ref).val().trim().toLowerCase()==value.trim().toLowerCase()){
				v=false;
			}
		
		}
		return v;
	 });
  
  jQuery.validator.addMethod("checkDuplicateIndustryName", function(value, element) {
	    ie8Trim();
	    var v=true;
		$('#industries table td').find('input[type=checkbox]').each(function(e){
			 if(value.trim().toLowerCase()==$(this).parent().text().replace("Industries","").trim().toLowerCase()){
				// alert($(this).parent().text().replace("Language","").trim().toLowerCase()+"/"+value.toLowerCase()); 
				v=false;
			}
		});
		for(var k=0;k<2;k++){
			var ref="other"+ (k+1) +"Industry";
			//alert(ref+"/"+$(element).attr('id'));
			if(($(element).attr('id')==ref)||($(element).val()=="")){
				continue;
			}
			if($("#"+ref).val().trim().toLowerCase()==value.trim().toLowerCase()){
				v=false;
			}
			
		}
		return v;
	 });
  
  
  
  jQuery.validator.addMethod("checkDuplicateEthnicityName", function(value, element) {
	    ie8Trim();
	    var v=true;
		$('#ethnicity table td').find('input[type=checkbox]').each(function(e){
			 if(value.trim().toLowerCase()==$(this).parent().text().replace("Ethnicity","").trim().toLowerCase()){
				// alert($(this).parent().text().replace("Language","").trim().toLowerCase()+"/"+value.toLowerCase()); 
				v=false;
			}
		});	
		for(var k=0;k<2;k++){
			var ref="other"+ (k+1) +"Ethnicity";
			//alert(ref+"/"+$(element).attr('id'));
			if(($(element).attr('id')==ref)||($(element).val()=="")){
				continue;
			}
			if($("#"+ref).val().trim().toLowerCase()==value.trim().toLowerCase()){
				v=false;
			}
			
		}
		return v;
	 });
  
  jQuery.validator.addMethod("echeckEmptyEthnicity", function(value, element) {
		
		var returnValue=true;
		$('input[name=EthnicityChk]:checked').each(function() {
		 var tmpVal= $( '#'+$(this).attr('id')+'EthnicityPercentage').val() ;
		 if(   (tmpVal == '') ){
			 returnValue= false;
		 }else if(tmpVal == undefined){
			 
			//var tmpOthrEtName= $('#'+$(this).attr('value') + 'Ethnicity').val();
			var tmpOthrEtPer=$('#'+$(this).attr('value') + 'EthnicityPercentage' ).val();
			
			if(  tmpOthrEtPer =='' ){ // tmpOthrEtName==''||
				returnValue= false;
			}
			 
		 }
		});
		return returnValue;
	});
	
	jQuery.validator.addMethod("echeckEmptyIndustry", function(value, element) {
		
		var returnValue=true;
		$('input[name=IndustryChk]:checked').each(function() {
		 var tmpVal= $( '#'+$(this).attr('id')+'IndustryPercentage').val() ;
		 if(   (tmpVal == '') ){
			 returnValue= false;
		 }else if(tmpVal == undefined){
			 
			//var tmpOthrEtName= $('#'+$(this).attr('value') + 'Industry').val();
			var tmpOthrEtPer=$('#'+$(this).attr('value') + 'IndustryPercentage' ).val();
			
			if(  tmpOthrEtPer =='' ){ // tmpOthrEtName==''||
				returnValue= false;
			}
			 
		 }
		});
		return returnValue;
	});
	
	jQuery.validator.addMethod("checkLang", function(value, element) {
		var LangCheckbox = $('input[name=LangChk]');
		var validationSuccessful= false;
		for(var i = 0; i<LangCheckbox.length; i++){
			if(LangCheckbox[i].checked == true){
				validationSuccessful = true;
				break;
			}
		}
		return validationSuccessful;
	});
	
	jQuery.validator.addMethod("checkIndustry", function(value, element) {
		var industryCheckbox = $('input[name=IndustryChk]');
		var validationSuccessful= false;
		for(var i = 0; i<industryCheckbox.length; i++){
			if(industryCheckbox[i].checked == true){
				validationSuccessful = true;
				break;
			}
		}
		return validationSuccessful;
	});
	
	
	
	jQuery.validator.addMethod("checkEthnicity", function(value, element) {
		var ethnicityCheckbox = $('input[name=EthnicityChk]');
		var validationSuccessful= false;
		for(var i=0;i<ethnicityCheckbox.length;i++){
			if(ethnicityCheckbox[i].checked == true){
				validationSuccessful = true;
				break;
			}
		}
		return validationSuccessful;
	});
	
</script>

