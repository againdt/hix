package com.getinsured.eligibility.at.ref.service.migration;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.LceAllChangesHandlerDetermination;
import com.getinsured.eligibility.at.ref.common.LceAllChangesHandlerDetermination.AllChangesHandlerType;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Component("lceAllChangesHandlerMigrationService")
public class LceAllChangesHandlerMigrationService extends LceAllChangesHandlerBaseMigrationService implements LceProcessHandlerService {
	
	private static final Logger LOGGER = Logger.getLogger(LceAllChangesHandlerMigrationService.class);

	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;
	
	@Override
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceAllChangesHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		
		final SsapApplicant primaryApplicant = ReferralUtil.retreiveApplicant(currentApplication, ReferralConstants.PRIMARY);
		final Map<String, List<ApplicantEvent>> applicantExtensionEvents = ApplicationExtensionEventUtil.populateApplicantEventsFromExtension(currentApplication.getSsapApplicants(), lceProcessRequestDTO.getApplicationExtension(), primaryApplicant);
		final Map<String, Boolean> applicantWithInvalidExtensionEvents = ApplicationExtensionEventUtil.populateInvalidApplicantEventsFromExtension(currentApplication.getSsapApplicants(), lceProcessRequestDTO.getApplicationExtension(), primaryApplicant);
		final AllChangesHandlerType handlerToInvoke = LceAllChangesHandlerDetermination.handlerToInvoke(currentApplication, applicantExtensionEvents, lceProcessRequestDTO.getEnrolledApplicationAttributes(), applicantWithInvalidExtensionEvents);
		LOGGER.info("Handler to invoke - " + handlerToInvoke);
		if (AllChangesHandlerType.SINGLE.equals(handlerToInvoke)) {
			LOGGER.info("Invoking case " + AllChangesHandlerType.SINGLE + " handler for " + currentApplication.getId());
			handleSingle(currentApplication, lceProcessRequestDTO, applicantExtensionEvents);
		} else if (AllChangesHandlerType.PURE_REMOVE.equals(handlerToInvoke)){
			LOGGER.info("Invoking case " + AllChangesHandlerType.PURE_REMOVE + " handler for " + currentApplication.getId());
			handlePureRemove(currentApplication, lceProcessRequestDTO, applicantExtensionEvents);
		} else if (AllChangesHandlerType.PURE_CS_CHANGE.equals(handlerToInvoke)){
			LOGGER.info("Invoking case " + AllChangesHandlerType.PURE_CS_CHANGE + " handler for " + currentApplication.getId());
			handleCSAutomation(currentApplication, lceProcessRequestDTO, applicantExtensionEvents, handlerToInvoke);
		} else if (AllChangesHandlerType.CS_CHANGE.equals(handlerToInvoke)){
			LOGGER.info("Invoking case " + AllChangesHandlerType.CS_CHANGE + " handler for " + currentApplication.getId());
			handleCSAutomation(currentApplication, lceProcessRequestDTO, applicantExtensionEvents, handlerToInvoke);
		} else if (AllChangesHandlerType.MIXED.equals(handlerToInvoke)) {
			LOGGER.info("Invoking case " + AllChangesHandlerType.MIXED + " handler for " + currentApplication.getId());
			handleMixed(currentApplication, lceProcessRequestDTO, applicantExtensionEvents);
		} else if (AllChangesHandlerType.DENIAL.equals(handlerToInvoke)) {
			LOGGER.info("Invoking case " + AllChangesHandlerType.DENIAL + " handler for " + currentApplication.getId());
			handleDenial(currentApplication, applicantExtensionEvents);
		} else {
			LOGGER.info("Invoking case " + AllChangesHandlerType.MANUAL + " handler for " + currentApplication.getId());
			handleManual(currentApplication);
		}
		
		LOGGER.info("LceAllChangesHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}
	
	
	
	private void handleCSAutomation(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents, AllChangesHandlerType handlerToInvoke) {
		
		boolean pureCsChange = handlerToInvoke == AllChangesHandlerType.PURE_CS_CHANGE ? true : false;
		SsapApplicationEvent appEvent = ssapApplicationEventMigrationService.createApplicationEventForCSChanges(currentApplication, applicantExtensionEvents, pureCsChange);
		if (appEvent != null){
			currentApplication = loadCurrentApplication(currentApplication.getId());////HIX-108047
			
			if (handlerToInvoke == AllChangesHandlerType.PURE_CS_CHANGE){
				ignoreGatedNonPrimaryEvents(currentApplication.getCaseNumber());
			}
			handleManual(currentApplication); 
		}
	}

	private void handlePureRemove(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("Handle " + AllChangesHandlerType.PURE_REMOVE + " case for " + currentApplication.getId());
		ssapApplicationEventMigrationService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents, true);
		Optional<ApplicationValidationStatus> validationStatus = getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplication.getId());
		
		if(isApplicationValidated(validationStatus)){
			handleAutomation(currentApplication, lceProcessRequestDTO);
		}else{
			handlePendingValidation(currentApplication);
		}
	}

	private void handleAutomation(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		processMixed(currentApplication, false, lceProcessRequestDTO);
	}

	private void handleSingle(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("Handle " + AllChangesHandlerType.SINGLE + " case for " + currentApplication.getId());
		ssapApplicationEventMigrationService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents);
		Optional<ApplicationValidationStatus> validationStatus = getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplication.getId());////HIX-108047
		
		if(isApplicationValidated(validationStatus)){
			updateCurrentAppToER(currentApplication);
			updateAllowEnrollment(currentApplication, Y);
		}else{
			handlePendingValidation(currentApplication);
		}
	}

	private void handleMixed(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("Handle " + AllChangesHandlerType.MIXED + " case for " + currentApplication.getId());
		ssapApplicationEventMigrationService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents);
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplication.getId());////HIX-108047
		
		processMixed(currentApplication, true, lceProcessRequestDTO);
	}

	private void handleDenial(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("Handle " + AllChangesHandlerType.DENIAL + " case for " + currentApplication.getId());
		ssapApplicationEventMigrationService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents);
	}
	
	private void handleManual(SsapApplication currentApplication) {
		//TODO will comment below allow enrollment line when we capture events in UI
		updateAllowEnrollment(currentApplication, Y);
	}
}

