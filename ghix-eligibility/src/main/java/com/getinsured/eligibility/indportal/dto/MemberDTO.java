package com.getinsured.eligibility.indportal.dto;

import java.util.ArrayList;
import java.util.List;

public class MemberDTO extends HouseholdMemberDTO {

	private String dob;
	private String countyCode;
	private String genderCode;
	private String memberId;
	
	private String mailingZip;
	private String mailingCity;
	private String mailingState;
	private String mailingAddress1;
	private String mailingAddress2;
	
	private String maritalStatusCode;
	private String spokenLanguageCode;
	private String writtenLanguageCode;
	private String citizenshipStatusCode;
	private String maintenanceReasonCode;
	
	private String custodialParentId;
	private String custodialParentFirstName;
	private String custodialParentMiddleName;
	private String custodialParentLastName;
	private String custodialParentSuffix;
	private String custodialParentSsn;
	private String custodialParentPrimaryPhone;
	private String custodialParentSecondaryPhone;
	private String custodialParentPreferredPhone;
	private String custodialParentPreferredEmail;
	private String custodialParentHomeAddress1;
	private String custodialParentHomeAddress2;
	private String custodialParentHomeCity;
	private String custodialParentHomeState;
	private String custodialParentHomeZip;

	private String houseHoldContactRelationship;

	private String existingSADPEnrollmentID;
	private String existingMedicalEnrollmentID;	
	
	private String tobacco;
	private String newPersonFLAG;
	private List<Race> race;
	private String childOnlyPlanEligibile;
	private String financialHardshipExemption;
	private String catastrophicEligible;
	private List<Relationships> relationships;
	
	private String externalMemberId;
	
	

	private String applicantGuid;
	
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getGenderCode() {
		return genderCode;
	}
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMailingZip() {
		return mailingZip;
	}
	public void setMailingZip(String mailingZip) {
		this.mailingZip = mailingZip;
	}
	public String getMailingCity() {
		return mailingCity;
	}
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}
	public String getMailingState() {
		return mailingState;
	}
	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}
	public String getMailingAddress1() {
		return mailingAddress1;
	}
	public void setMailingAddress1(String mailingAddress1) {
		this.mailingAddress1 = mailingAddress1;
	}
	public String getMailingAddress2() {
		return mailingAddress2;
	}
	public void setMailingAddress2(String mailingAddress2) {
		this.mailingAddress2 = mailingAddress2;
	}
	public String getMaritalStatusCode() {
		return maritalStatusCode;
	}
	public void setMaritalStatusCode(String maritalStatusCode) {
		this.maritalStatusCode = maritalStatusCode;
	}
	public String getSpokenLanguageCode() {
		return spokenLanguageCode;
	}
	public void setSpokenLanguageCode(String spokenLanguageCode) {
		this.spokenLanguageCode = spokenLanguageCode;
	}
	public String getWrittenLanguageCode() {
		return writtenLanguageCode;
	}
	public void setWrittenLanguageCode(String writtenLanguageCode) {
		this.writtenLanguageCode = writtenLanguageCode;
	}
	public String getCitizenshipStatusCode() {
		return citizenshipStatusCode;
	}
	public void setCitizenshipStatusCode(String citizenshipStatusCode) {
		this.citizenshipStatusCode = citizenshipStatusCode;
	}
	public String getMaintenanceReasonCode() {
		return maintenanceReasonCode;
	}
	public void setMaintenanceReasonCode(String maintenanceReasonCode) {
		this.maintenanceReasonCode = maintenanceReasonCode;
	}
	public String getCustodialParentId() {
		return custodialParentId;
	}
	public void setCustodialParentId(String custodialParentId) {
		this.custodialParentId = custodialParentId;
	}
	public String getCustodialParentFirstName() {
		return custodialParentFirstName;
	}
	public void setCustodialParentFirstName(String custodialParentFirstName) {
		this.custodialParentFirstName = custodialParentFirstName;
	}
	public String getCustodialParentMiddleName() {
		return custodialParentMiddleName;
	}
	public void setCustodialParentMiddleName(String custodialParentMiddleName) {
		this.custodialParentMiddleName = custodialParentMiddleName;
	}
	public String getCustodialParentLastName() {
		return custodialParentLastName;
	}
	public void setCustodialParentLastName(String custodialParentLastName) {
		this.custodialParentLastName = custodialParentLastName;
	}
	public String getCustodialParentSuffix() {
		return custodialParentSuffix;
	}
	public void setCustodialParentSuffix(String custodialParentSuffix) {
		this.custodialParentSuffix = custodialParentSuffix;
	}
	public String getCustodialParentSsn() {
		return custodialParentSsn;
	}
	public void setCustodialParentSsn(String custodialParentSsn) {
		this.custodialParentSsn = custodialParentSsn;
	}
	public String getCustodialParentPrimaryPhone() {
		return custodialParentPrimaryPhone;
	}
	public void setCustodialParentPrimaryPhone(String custodialParentPrimaryPhone) {
		this.custodialParentPrimaryPhone = custodialParentPrimaryPhone;
	}
	public String getCustodialParentSecondaryPhone() {
		return custodialParentSecondaryPhone;
	}
	public void setCustodialParentSecondaryPhone(String custodialParentSecondaryPhone) {
		this.custodialParentSecondaryPhone = custodialParentSecondaryPhone;
	}
	public String getCustodialParentPreferredPhone() {
		return custodialParentPreferredPhone;
	}
	public void setCustodialParentPreferredPhone(String custodialParentPreferredPhone) {
		this.custodialParentPreferredPhone = custodialParentPreferredPhone;
	}
	public String getCustodialParentPreferredEmail() {
		return custodialParentPreferredEmail;
	}
	public void setCustodialParentPreferredEmail(String custodialParentPreferredEmail) {
		this.custodialParentPreferredEmail = custodialParentPreferredEmail;
	}
	public String getCustodialParentHomeAddress1() {
		return custodialParentHomeAddress1;
	}
	public void setCustodialParentHomeAddress1(String custodialParentHomeAddress1) {
		this.custodialParentHomeAddress1 = custodialParentHomeAddress1;
	}
	public String getCustodialParentHomeAddress2() {
		return custodialParentHomeAddress2;
	}
	public void setCustodialParentHomeAddress2(String custodialParentHomeAddress2) {
		this.custodialParentHomeAddress2 = custodialParentHomeAddress2;
	}
	public String getCustodialParentHomeCity() {
		return custodialParentHomeCity;
	}
	public void setCustodialParentHomeCity(String custodialParentHomeCity) {
		this.custodialParentHomeCity = custodialParentHomeCity;
	}
	public String getCustodialParentHomeState() {
		return custodialParentHomeState;
	}
	public void setCustodialParentHomeState(String custodialParentHomeState) {
		this.custodialParentHomeState = custodialParentHomeState;
	}
	public String getCustodialParentHomeZip() {
		return custodialParentHomeZip;
	}
	public void setCustodialParentHomeZip(String custodialParentHomeZip) {
		this.custodialParentHomeZip = custodialParentHomeZip;
	}
	public String getHouseHoldContactRelationship() {
		return houseHoldContactRelationship;
	}
	public void setHouseHoldContactRelationship(String houseHoldContactRelationship) {
		this.houseHoldContactRelationship = houseHoldContactRelationship;
	}
	public String getExistingMedicalEnrollmentID() {
		return existingMedicalEnrollmentID;
	}
	public void setExistingMedicalEnrollmentID(String existingMedicalEnrollmentID) {
		this.existingMedicalEnrollmentID = existingMedicalEnrollmentID;
	}
	public String getExistingSADPEnrollmentID() {
		return existingSADPEnrollmentID;
	}
	public void setExistingSADPEnrollmentID(String existingSADPEnrollmentID) {
		this.existingSADPEnrollmentID = existingSADPEnrollmentID;
	}
	public String getTobacco() {
		return tobacco;
	}
	public void setTobacco(String tobacco) {
		this.tobacco = tobacco;
	}
	public String getNewPersonFLAG() {
		return newPersonFLAG;
	}
	public void setNewPersonFLAG(String newPersonFLAG) {
		this.newPersonFLAG = newPersonFLAG;
	}	
	public List<Race> getRace() {
		if(race == null){
			race = new ArrayList<Race>();
		}
		return this.race;
	}
	public void setRace(List<Race> race) {
		this.race = race;
	}
	public String getChildOnlyPlanEligibile() {
		return childOnlyPlanEligibile;
	}
	public void setChildOnlyPlanEligibile(String childOnlyPlanEligibile) {
		this.childOnlyPlanEligibile = childOnlyPlanEligibile;
	}
	public String getFinancialHardshipExemption() {
		return financialHardshipExemption;
	}
	public void setFinancialHardshipExemption(String financialHardshipExemption) {
		this.financialHardshipExemption = financialHardshipExemption;
	}
	public String getCatastrophicEligible() {
		return catastrophicEligible;
	}
	public void setCatastrophicEligible(String catastrophicEligible) {
		this.catastrophicEligible = catastrophicEligible;
	}
	public List<Relationships> getRelationships() {
		return relationships;
	}
	public void setRelationships(List<Relationships> relationships) {
		this.relationships = relationships;
	}
	public String getApplicantGuid() {
		return applicantGuid;
	}
	public void setApplicantGuid(String applicantGuid) {
		this.applicantGuid = applicantGuid;
	}

	public String getExternalMemberId() {
		return externalMemberId;
	}
	public void setExternalMemberId(String externalMemberId) {
		this.externalMemberId = externalMemberId;
	}

	public class Race{
		private String raceEthnicityCode;
		private String raceEthnicityDescription;
		
		public String getRaceEthnicityCode() {
			return raceEthnicityCode;
		}
		public void setRaceEthnicityCode(String raceEthnicityCode) {
			this.raceEthnicityCode = raceEthnicityCode;
		}
		public String getRaceEthnicityDescription() {
			return raceEthnicityDescription;
		}
		public void setRaceEthnicityDescription(String raceEthnicityDescription) {
			this.raceEthnicityDescription = raceEthnicityDescription;
		}
	}
	
	public class Relationships{
		private String memberId;
		private String individualId;
		private String relationshipCode;		
		
		public String getMemberId() {
			return memberId;
		}
		public void setMemberId(String memberId) {
			this.memberId = memberId;
		}
		public String getIndividualId() {
			return individualId;
		}
		public void setIndividualId(String individualId) {
			this.individualId = individualId;
		}
		public String getRelationshipCode() {
			return relationshipCode;
		}
		public void setRelationshipCode(String relationshipCode) {
			this.relationshipCode = relationshipCode;
		}		
	}
}
