﻿define(['textbox'], function (txtbox) {
    var currency = new baseComponent();
    currency.type = "currency";
    currency.getHTML = function (cgfObj) {
        var defCfg = {
            etype: "number"
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;
        var vm = this;
        vm.context.registerComponent(txtbox);
        var currency = vm.context.comp["textbox"].getHTML(compData);//// kendo-numeric-text-box
        var $currency = $(currency);
        var $input = $currency.find("input");
        $input.attr("kendo-numeric-text-box", "").removeClass("k-textbox ").attr("k-format","c")
        return $currency[0].outerHTML;
    };
    return currency;
});